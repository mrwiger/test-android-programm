.class final Lru/cn/domain/stores/PurchasePersistor;
.super Ljava/lang/Object;
.source "PurchasePersistor.java"


# instance fields
.field private final context:Landroid/content/Context;

.field private final prefs:Landroid/content/SharedPreferences;

.field private productsSKU:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final storeName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "storeName"    # Ljava/lang/String;

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p2, p0, Lru/cn/domain/stores/PurchasePersistor;->storeName:Ljava/lang/String;

    .line 32
    iput-object p1, p0, Lru/cn/domain/stores/PurchasePersistor;->context:Landroid/content/Context;

    .line 34
    const-string v1, "PurchasePersistor"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lru/cn/domain/stores/PurchasePersistor;->prefs:Landroid/content/SharedPreferences;

    .line 36
    iget-object v1, p0, Lru/cn/domain/stores/PurchasePersistor;->prefs:Landroid/content/SharedPreferences;

    const-string v2, "PurchasePersistor_SKU"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    .line 37
    .local v0, "sku":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-eqz v0, :cond_0

    .line 38
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v1, p0, Lru/cn/domain/stores/PurchasePersistor;->productsSKU:Ljava/util/Set;

    .line 40
    :cond_0
    return-void
.end method

.method private uploadPurchases(Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lru/cn/domain/stores/iabhelper/Purchase;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 74
    .local p1, "purchases":Ljava/util/List;, "Ljava/util/List<Lru/cn/domain/stores/iabhelper/Purchase;>;"
    :try_start_0
    iget-object v5, p0, Lru/cn/domain/stores/PurchasePersistor;->productsSKU:Ljava/util/Set;

    if-nez v5, :cond_0

    .line 75
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    iput-object v5, p0, Lru/cn/domain/stores/PurchasePersistor;->productsSKU:Ljava/util/Set;

    .line 78
    :cond_0
    const/4 v3, 0x0

    .line 79
    .local v3, "persistedCount":I
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/cn/domain/stores/iabhelper/Purchase;

    .line 80
    .local v4, "purchase":Lru/cn/domain/stores/iabhelper/Purchase;
    new-instance v1, Lru/cn/api/userdata/elementclasses/GooglePlayReceipt;

    invoke-direct {v1}, Lru/cn/api/userdata/elementclasses/GooglePlayReceipt;-><init>()V

    .line 81
    .local v1, "info":Lru/cn/api/userdata/elementclasses/GooglePlayReceipt;
    invoke-virtual {v4}, Lru/cn/domain/stores/iabhelper/Purchase;->getPackageName()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v1, Lru/cn/api/userdata/elementclasses/GooglePlayReceipt;->packageName:Ljava/lang/String;

    .line 82
    invoke-virtual {v4}, Lru/cn/domain/stores/iabhelper/Purchase;->getToken()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v1, Lru/cn/api/userdata/elementclasses/GooglePlayReceipt;->token:Ljava/lang/String;

    .line 83
    invoke-virtual {v4}, Lru/cn/domain/stores/iabhelper/Purchase;->getSku()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v1, Lru/cn/api/userdata/elementclasses/GooglePlayReceipt;->productId:Ljava/lang/String;

    .line 85
    iget-object v6, p0, Lru/cn/domain/stores/PurchasePersistor;->context:Landroid/content/Context;

    iget-object v7, p0, Lru/cn/domain/stores/PurchasePersistor;->storeName:Ljava/lang/String;

    invoke-static {v6, v7, v1}, Lru/cn/api/userdata/UserData;->sendPurchase(Landroid/content/Context;Ljava/lang/String;Lru/cn/api/userdata/elementclasses/GooglePlayReceipt;)Z

    move-result v2

    .line 86
    .local v2, "isPersisted":Z
    if-eqz v2, :cond_1

    .line 87
    add-int/lit8 v3, v3, 0x1

    .line 88
    iget-object v6, p0, Lru/cn/domain/stores/PurchasePersistor;->productsSKU:Ljava/util/Set;

    invoke-virtual {v4}, Lru/cn/domain/stores/iabhelper/Purchase;->getSku()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 100
    .end local v1    # "info":Lru/cn/api/userdata/elementclasses/GooglePlayReceipt;
    .end local v2    # "isPersisted":Z
    .end local v3    # "persistedCount":I
    .end local v4    # "purchase":Lru/cn/domain/stores/iabhelper/Purchase;
    :catch_0
    move-exception v0

    .line 101
    .local v0, "e":Ljava/lang/Exception;
    const-string v5, "PurchasePersistor"

    const-string v6, "Failed to save purchase"

    invoke-static {v5, v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 103
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_1
    return-void

    .line 92
    .restart local v3    # "persistedCount":I
    :cond_2
    if-lez v3, :cond_3

    .line 93
    :try_start_1
    invoke-static {}, Lru/cn/ad/AdsManager;->setNeedsUpdate()V

    .line 96
    :cond_3
    iget-object v5, p0, Lru/cn/domain/stores/PurchasePersistor;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    const-string v6, "PurchasePersistor_SKU"

    iget-object v7, p0, Lru/cn/domain/stores/PurchasePersistor;->productsSKU:Ljava/util/Set;

    .line 97
    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    .line 98
    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method


# virtual methods
.method hasPurchase(Ljava/lang/String;)Z
    .locals 1
    .param p1, "sku"    # Ljava/lang/String;

    .prologue
    .line 69
    iget-object v0, p0, Lru/cn/domain/stores/PurchasePersistor;->productsSKU:Ljava/util/Set;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lru/cn/domain/stores/PurchasePersistor;->productsSKU:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method savePurchases(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lru/cn/domain/stores/iabhelper/Purchase;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 43
    .local p1, "purchases":Ljava/util/List;, "Ljava/util/List<Lru/cn/domain/stores/iabhelper/Purchase;>;"
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 45
    iget-object v3, p0, Lru/cn/domain/stores/PurchasePersistor;->productsSKU:Ljava/util/Set;

    if-nez v3, :cond_1

    .line 66
    :cond_0
    :goto_0
    return-void

    .line 48
    :cond_1
    iget-object v3, p0, Lru/cn/domain/stores/PurchasePersistor;->productsSKU:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->clear()V

    .line 49
    iget-object v3, p0, Lru/cn/domain/stores/PurchasePersistor;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "PurchasePersistor_SKU"

    iget-object v5, p0, Lru/cn/domain/stores/PurchasePersistor;->productsSKU:Ljava/util/Set;

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    .line 50
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0

    .line 54
    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 55
    .local v0, "pendingPurchases":Ljava/util/List;, "Ljava/util/List<Lru/cn/domain/stores/iabhelper/Purchase;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/cn/domain/stores/iabhelper/Purchase;

    .line 56
    .local v1, "purchase":Lru/cn/domain/stores/iabhelper/Purchase;
    invoke-virtual {v1}, Lru/cn/domain/stores/iabhelper/Purchase;->getSku()Ljava/lang/String;

    move-result-object v2

    .line 57
    .local v2, "sku":Ljava/lang/String;
    invoke-virtual {p0, v2}, Lru/cn/domain/stores/PurchasePersistor;->hasPurchase(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 58
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 62
    .end local v1    # "purchase":Lru/cn/domain/stores/iabhelper/Purchase;
    .end local v2    # "sku":Ljava/lang/String;
    :cond_4
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 65
    invoke-direct {p0, v0}, Lru/cn/domain/stores/PurchasePersistor;->uploadPurchases(Ljava/util/List;)V

    goto :goto_0
.end method
