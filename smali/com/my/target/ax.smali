.class public Lcom/my/target/ax;
.super Lcom/my/target/aw;
.source "HttpStatRequest.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/my/target/aw",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field private static final dQ:I = 0xa


# instance fields
.field private bT:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/my/target/aw;-><init>()V

    return-void
.end method

.method private a(Ljava/net/HttpURLConnection;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 122
    iget v0, p0, Lcom/my/target/ax;->bT:I

    const/16 v2, 0xa

    if-gt v0, v2, :cond_0

    .line 124
    const-string v0, "Location"

    invoke-virtual {p1, v0}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 130
    :try_start_0
    new-instance v2, Ljava/net/URI;

    invoke-direct {v2, v0}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    .line 131
    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getURL()Ljava/net/URL;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URL;->toURI()Ljava/net/URI;

    move-result-object v0

    .line 132
    invoke-virtual {v0, v2}, Ljava/net/URI;->resolve(Ljava/net/URI;)Ljava/net/URI;

    move-result-object v0

    .line 134
    invoke-virtual {v0}, Ljava/net/URI;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 141
    invoke-static {v0}, Lcom/my/target/cn;->S(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 143
    iput-object v0, p0, Lcom/my/target/ax;->dP:Ljava/lang/Object;

    :cond_0
    move-object v0, v1

    .line 150
    :cond_1
    :goto_0
    return-object v0

    .line 136
    :catch_0
    move-exception v0

    move-object v0, v1

    .line 138
    goto :goto_0
.end method

.method public static am()Lcom/my/target/ax;
    .locals 1

    .prologue
    .line 30
    new-instance v0, Lcom/my/target/ax;

    invoke-direct {v0}, Lcom/my/target/ax;-><init>()V

    return-object v0
.end method

.method private g(Ljava/lang/String;Landroid/content/Context;)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x0

    .line 45
    .line 47
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "send stat request: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 52
    :try_start_0
    invoke-static {p2}, Lcom/my/target/ba;->f(Landroid/content/Context;)Lcom/my/target/ba;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    move-object v2, v0

    .line 63
    :goto_0
    :try_start_1
    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 64
    const/16 v3, 0x2710

    :try_start_2
    invoke-virtual {v0, v3}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 65
    const/16 v3, 0x2710

    invoke-virtual {v0, v3}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 66
    const-string v3, "GET"

    invoke-virtual {v0, v3}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 67
    const-string v3, "User-Agent"

    const-string v4, "http.agent"

    invoke-static {v4}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Ljava/net/HttpURLConnection;->addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Ljava/net/HttpURLConnection;->setInstanceFollowRedirects(Z)V

    .line 69
    const-string v3, "connection"

    const-string v4, "close"

    invoke-virtual {v0, v3, v4}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    if-eqz v2, :cond_0

    .line 73
    invoke-virtual {v2, v0}, Lcom/my/target/ba;->b(Ljava/net/URLConnection;)V

    .line 76
    :cond_0
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v3

    iput v3, p0, Lcom/my/target/ax;->responseCode:I

    .line 77
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v3

    .line 78
    if-eqz v3, :cond_1

    .line 80
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    .line 82
    :cond_1
    iget v3, p0, Lcom/my/target/ax;->responseCode:I

    const/16 v4, 0xc8

    if-eq v3, v4, :cond_2

    iget v3, p0, Lcom/my/target/ax;->responseCode:I

    const/16 v4, 0xcc

    if-eq v3, v4, :cond_2

    iget v3, p0, Lcom/my/target/ax;->responseCode:I

    const/16 v4, 0x194

    if-eq v3, v4, :cond_2

    iget v3, p0, Lcom/my/target/ax;->responseCode:I

    const/16 v4, 0x193

    if-ne v3, v4, :cond_7

    .line 87
    :cond_2
    if-eqz v2, :cond_3

    .line 89
    invoke-virtual {v2, v0}, Lcom/my/target/ba;->a(Ljava/net/URLConnection;)V

    .line 91
    :cond_3
    iput-object p1, p0, Lcom/my/target/ax;->dP:Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_2

    .line 108
    :cond_4
    :goto_1
    if-eqz v0, :cond_5

    .line 110
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 113
    :cond_5
    if-eqz v1, :cond_6

    .line 115
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "redirected to: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 116
    invoke-direct {p0, v1, p2}, Lcom/my/target/ax;->g(Ljava/lang/String;Landroid/content/Context;)V

    .line 118
    :cond_6
    return-void

    .line 54
    :catch_0
    move-exception v0

    .line 56
    const-string v2, "MyTargetCookieManager error"

    invoke-static {v2}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 57
    invoke-static {v0}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    move-object v2, v1

    goto/16 :goto_0

    .line 93
    :cond_7
    :try_start_3
    iget v2, p0, Lcom/my/target/ax;->responseCode:I

    const/16 v3, 0x12e

    if-eq v2, v3, :cond_8

    iget v2, p0, Lcom/my/target/ax;->responseCode:I

    const/16 v3, 0x12d

    if-eq v2, v3, :cond_8

    iget v2, p0, Lcom/my/target/ax;->responseCode:I

    const/16 v3, 0x12f

    if-ne v2, v3, :cond_4

    .line 97
    :cond_8
    iget v2, p0, Lcom/my/target/ax;->bT:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/my/target/ax;->bT:I

    .line 98
    invoke-direct {p0, v0}, Lcom/my/target/ax;->a(Ljava/net/HttpURLConnection;)Ljava/lang/String;
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_2

    move-result-object v1

    goto :goto_1

    .line 102
    :catch_1
    move-exception v0

    move-object v2, v1

    .line 104
    :goto_2
    iput-boolean v5, p0, Lcom/my/target/ax;->dO:Z

    .line 105
    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/ax;->s:Ljava/lang/String;

    .line 106
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "stat request error: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/my/target/ax;->s:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    move-object v0, v2

    goto :goto_1

    .line 102
    :catch_2
    move-exception v2

    move-object v6, v2

    move-object v2, v0

    move-object v0, v6

    goto :goto_2
.end method


# virtual methods
.method protected b(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    iput v0, p0, Lcom/my/target/ax;->bT:I

    .line 39
    invoke-direct {p0, p1, p2}, Lcom/my/target/ax;->g(Ljava/lang/String;Landroid/content/Context;)V

    .line 40
    iget-object v0, p0, Lcom/my/target/ax;->dP:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method protected synthetic c(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0, p1, p2}, Lcom/my/target/ax;->b(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
