.class Lru/cn/tv/player/controller/TouchPlayerController$3;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "TouchPlayerController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/tv/player/controller/TouchPlayerController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private canTrackVolume:Z

.field private cumulativeDistanceX:F

.field private cumulativeDistanceY:F

.field final synthetic this$0:Lru/cn/tv/player/controller/TouchPlayerController;

.field private trackingVolume:Z


# direct methods
.method constructor <init>(Lru/cn/tv/player/controller/TouchPlayerController;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/tv/player/controller/TouchPlayerController;

    .prologue
    .line 198
    iput-object p1, p0, Lru/cn/tv/player/controller/TouchPlayerController$3;->this$0:Lru/cn/tv/player/controller/TouchPlayerController;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 236
    iget-object v0, p0, Lru/cn/tv/player/controller/TouchPlayerController$3;->this$0:Lru/cn/tv/player/controller/TouchPlayerController;

    invoke-static {v0}, Lru/cn/tv/player/controller/TouchPlayerController;->access$000(Lru/cn/tv/player/controller/TouchPlayerController;)Landroid/widget/ImageButton;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ImageButton;->performClick()Z

    .line 238
    const/4 v0, 0x1

    return v0
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 215
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget-object v3, p0, Lru/cn/tv/player/controller/TouchPlayerController$3;->this$0:Lru/cn/tv/player/controller/TouchPlayerController;

    invoke-virtual {v3}, Lru/cn/tv/player/controller/TouchPlayerController;->getWidth()I

    move-result v3

    int-to-float v3, v3

    const v4, 0x3f19999a    # 0.6f

    mul-float/2addr v3, v4

    cmpl-float v0, v0, v3

    if-lez v0, :cond_0

    .line 216
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    const/high16 v3, 0x41f00000    # 30.0f

    cmpl-float v0, v0, v3

    if-lez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iget-object v3, p0, Lru/cn/tv/player/controller/TouchPlayerController$3;->this$0:Lru/cn/tv/player/controller/TouchPlayerController;

    invoke-virtual {v3}, Lru/cn/tv/player/controller/TouchPlayerController;->getHeight()I

    move-result v3

    add-int/lit8 v3, v3, -0x1e

    int-to-float v3, v3

    cmpg-float v0, v0, v3

    if-gez v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lru/cn/tv/player/controller/TouchPlayerController$3;->canTrackVolume:Z

    .line 217
    iput-boolean v2, p0, Lru/cn/tv/player/controller/TouchPlayerController$3;->trackingVolume:Z

    .line 219
    return v1

    :cond_0
    move v0, v2

    .line 216
    goto :goto_0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 6
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    const/high16 v5, 0x42c80000    # 100.0f

    const/4 v2, 0x0

    .line 243
    iget-object v3, p0, Lru/cn/tv/player/controller/TouchPlayerController$3;->this$0:Lru/cn/tv/player/controller/TouchPlayerController;

    invoke-static {v3}, Lru/cn/tv/player/controller/TouchPlayerController;->access$100(Lru/cn/tv/player/controller/TouchPlayerController;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 266
    :cond_0
    :goto_0
    return v2

    .line 247
    :cond_1
    iget-boolean v3, p0, Lru/cn/tv/player/controller/TouchPlayerController$3;->trackingVolume:Z

    if-nez v3, :cond_0

    .line 251
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    sub-float v1, v3, v4

    .line 252
    .local v1, "diffY":F
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    sub-float v0, v3, v4

    .line 253
    .local v0, "diffX":F
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v3

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v4

    cmpl-float v3, v3, v4

    if-lez v3, :cond_0

    .line 254
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpl-float v3, v3, v5

    if-lez v3, :cond_0

    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpl-float v3, v3, v5

    if-lez v3, :cond_0

    .line 255
    const/4 v3, 0x0

    cmpl-float v3, v0, v3

    if-lez v3, :cond_2

    .line 256
    iget-object v3, p0, Lru/cn/tv/player/controller/TouchPlayerController$3;->this$0:Lru/cn/tv/player/controller/TouchPlayerController;

    invoke-static {v3}, Lru/cn/tv/player/controller/TouchPlayerController;->access$200(Lru/cn/tv/player/controller/TouchPlayerController;)V

    .line 261
    :goto_1
    iput-boolean v2, p0, Lru/cn/tv/player/controller/TouchPlayerController$3;->canTrackVolume:Z

    .line 262
    const/4 v2, 0x1

    goto :goto_0

    .line 258
    :cond_2
    iget-object v3, p0, Lru/cn/tv/player/controller/TouchPlayerController$3;->this$0:Lru/cn/tv/player/controller/TouchPlayerController;

    invoke-static {v3}, Lru/cn/tv/player/controller/TouchPlayerController;->access$300(Lru/cn/tv/player/controller/TouchPlayerController;)V

    goto :goto_1
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 9
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "distanceX"    # F
    .param p4, "distanceY"    # F

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 271
    iget-object v3, p0, Lru/cn/tv/player/controller/TouchPlayerController$3;->this$0:Lru/cn/tv/player/controller/TouchPlayerController;

    invoke-static {v3}, Lru/cn/tv/player/controller/TouchPlayerController;->access$100(Lru/cn/tv/player/controller/TouchPlayerController;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 299
    :cond_0
    :goto_0
    return v6

    .line 275
    :cond_1
    iget v3, p0, Lru/cn/tv/player/controller/TouchPlayerController$3;->cumulativeDistanceX:F

    add-float/2addr v3, p3

    iput v3, p0, Lru/cn/tv/player/controller/TouchPlayerController$3;->cumulativeDistanceX:F

    .line 276
    iget v3, p0, Lru/cn/tv/player/controller/TouchPlayerController$3;->cumulativeDistanceY:F

    add-float/2addr v3, p4

    iput v3, p0, Lru/cn/tv/player/controller/TouchPlayerController$3;->cumulativeDistanceY:F

    .line 277
    iget-boolean v3, p0, Lru/cn/tv/player/controller/TouchPlayerController$3;->canTrackVolume:Z

    if-eqz v3, :cond_0

    iget v3, p0, Lru/cn/tv/player/controller/TouchPlayerController$3;->cumulativeDistanceY:F

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    const/high16 v4, 0x41a00000    # 20.0f

    cmpl-float v3, v3, v4

    if-lez v3, :cond_0

    .line 279
    iget v3, p0, Lru/cn/tv/player/controller/TouchPlayerController$3;->cumulativeDistanceX:F

    iget v4, p0, Lru/cn/tv/player/controller/TouchPlayerController$3;->cumulativeDistanceY:F

    div-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 280
    .local v1, "ratio":F
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v3

    const v4, 0x3e4ccccd    # 0.2f

    cmpg-float v3, v3, v4

    if-gez v3, :cond_2

    .line 281
    iget-object v3, p0, Lru/cn/tv/player/controller/TouchPlayerController$3;->this$0:Lru/cn/tv/player/controller/TouchPlayerController;

    invoke-static {v3}, Lru/cn/tv/player/controller/TouchPlayerController;->access$400(Lru/cn/tv/player/controller/TouchPlayerController;)I

    move-result v3

    int-to-float v3, v3

    iget v4, p0, Lru/cn/tv/player/controller/TouchPlayerController$3;->cumulativeDistanceY:F

    mul-float/2addr v3, v4

    iget-object v4, p0, Lru/cn/tv/player/controller/TouchPlayerController$3;->this$0:Lru/cn/tv/player/controller/TouchPlayerController;

    invoke-virtual {v4}, Lru/cn/tv/player/controller/TouchPlayerController;->getHeight()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    float-to-int v0, v3

    .line 282
    .local v0, "dV":I
    if-eqz v0, :cond_0

    .line 283
    iget-object v3, p0, Lru/cn/tv/player/controller/TouchPlayerController$3;->this$0:Lru/cn/tv/player/controller/TouchPlayerController;

    invoke-static {v3}, Lru/cn/tv/player/controller/TouchPlayerController;->access$500(Lru/cn/tv/player/controller/TouchPlayerController;)Landroid/media/AudioManager;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v2

    .line 284
    .local v2, "volume":I
    iget-object v3, p0, Lru/cn/tv/player/controller/TouchPlayerController$3;->this$0:Lru/cn/tv/player/controller/TouchPlayerController;

    invoke-static {v3}, Lru/cn/tv/player/controller/TouchPlayerController;->access$500(Lru/cn/tv/player/controller/TouchPlayerController;)Landroid/media/AudioManager;

    move-result-object v3

    add-int v4, v2, v0

    invoke-virtual {v3, v8, v4, v7}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 286
    iput-boolean v7, p0, Lru/cn/tv/player/controller/TouchPlayerController$3;->trackingVolume:Z

    .line 289
    iput v5, p0, Lru/cn/tv/player/controller/TouchPlayerController$3;->cumulativeDistanceY:F

    .line 290
    iput v5, p0, Lru/cn/tv/player/controller/TouchPlayerController$3;->cumulativeDistanceX:F

    goto :goto_0

    .line 294
    .end local v0    # "dV":I
    .end local v2    # "volume":I
    :cond_2
    iput v5, p0, Lru/cn/tv/player/controller/TouchPlayerController$3;->cumulativeDistanceY:F

    .line 295
    iput v5, p0, Lru/cn/tv/player/controller/TouchPlayerController$3;->cumulativeDistanceX:F

    goto :goto_0
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 224
    iget-object v0, p0, Lru/cn/tv/player/controller/TouchPlayerController$3;->this$0:Lru/cn/tv/player/controller/TouchPlayerController;

    invoke-virtual {v0}, Lru/cn/tv/player/controller/TouchPlayerController;->isShowing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 225
    iget-object v0, p0, Lru/cn/tv/player/controller/TouchPlayerController$3;->this$0:Lru/cn/tv/player/controller/TouchPlayerController;

    invoke-virtual {v0}, Lru/cn/tv/player/controller/TouchPlayerController;->show()V

    .line 230
    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 226
    :cond_1
    iget-object v0, p0, Lru/cn/tv/player/controller/TouchPlayerController$3;->this$0:Lru/cn/tv/player/controller/TouchPlayerController;

    iget-boolean v0, v0, Lru/cn/tv/player/controller/TouchPlayerController;->alwaysShow:Z

    if-nez v0, :cond_0

    .line 227
    iget-object v0, p0, Lru/cn/tv/player/controller/TouchPlayerController$3;->this$0:Lru/cn/tv/player/controller/TouchPlayerController;

    invoke-virtual {v0}, Lru/cn/tv/player/controller/TouchPlayerController;->hide()V

    goto :goto_0
.end method
