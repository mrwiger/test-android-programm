.class public Lcom/yandex/metrica/impl/ba$d;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/yandex/metrica/impl/ba;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "d"
.end annotation


# instance fields
.field private a:Lcom/yandex/metrica/impl/i;

.field private b:Lcom/yandex/metrica/impl/ax;

.field private c:Z

.field private d:Lcom/yandex/metrica/impl/ba$c;


# direct methods
.method constructor <init>(Lcom/yandex/metrica/impl/i;Lcom/yandex/metrica/impl/ax;)V
    .locals 1

    .prologue
    .line 173
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 170
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/yandex/metrica/impl/ba$d;->c:Z

    .line 174
    iput-object p1, p0, Lcom/yandex/metrica/impl/ba$d;->a:Lcom/yandex/metrica/impl/i;

    .line 175
    iput-object p2, p0, Lcom/yandex/metrica/impl/ba$d;->b:Lcom/yandex/metrica/impl/ax;

    .line 176
    return-void
.end method

.method static synthetic a(Lcom/yandex/metrica/impl/ba$d;)Lcom/yandex/metrica/impl/ax;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/yandex/metrica/impl/ba$d;->b:Lcom/yandex/metrica/impl/ax;

    return-object v0
.end method

.method static synthetic b(Lcom/yandex/metrica/impl/ba$d;)Lcom/yandex/metrica/impl/i;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/yandex/metrica/impl/ba$d;->a:Lcom/yandex/metrica/impl/i;

    return-object v0
.end method


# virtual methods
.method a()Lcom/yandex/metrica/impl/ax;
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/yandex/metrica/impl/ba$d;->b:Lcom/yandex/metrica/impl/ax;

    return-object v0
.end method

.method a(Lcom/yandex/metrica/impl/ba$c;)Lcom/yandex/metrica/impl/ba$d;
    .locals 0

    .prologue
    .line 179
    iput-object p1, p0, Lcom/yandex/metrica/impl/ba$d;->d:Lcom/yandex/metrica/impl/ba$c;

    .line 180
    return-object p0
.end method

.method a(Z)Lcom/yandex/metrica/impl/ba$d;
    .locals 0

    .prologue
    .line 184
    iput-boolean p1, p0, Lcom/yandex/metrica/impl/ba$d;->c:Z

    .line 185
    return-object p0
.end method

.method b()Lcom/yandex/metrica/impl/i;
    .locals 2

    .prologue
    .line 193
    iget-object v0, p0, Lcom/yandex/metrica/impl/ba$d;->d:Lcom/yandex/metrica/impl/ba$c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/yandex/metrica/impl/ba$d;->d:Lcom/yandex/metrica/impl/ba$c;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ba$d;->a:Lcom/yandex/metrica/impl/i;

    invoke-interface {v0, v1}, Lcom/yandex/metrica/impl/ba$c;->a(Lcom/yandex/metrica/impl/i;)Lcom/yandex/metrica/impl/i;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ba$d;->a:Lcom/yandex/metrica/impl/i;

    goto :goto_0
.end method

.method c()Z
    .locals 1

    .prologue
    .line 197
    iget-boolean v0, p0, Lcom/yandex/metrica/impl/ba$d;->c:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 202
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ReportToSend{mReport="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/yandex/metrica/impl/ba$d;->a:Lcom/yandex/metrica/impl/i;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mEnvironment="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/yandex/metrica/impl/ba$d;->b:Lcom/yandex/metrica/impl/ax;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mCrash="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/yandex/metrica/impl/ba$d;->c:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mAction="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/yandex/metrica/impl/ba$d;->d:Lcom/yandex/metrica/impl/ba$c;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
