.class public Lru/cn/api/tv/retrofit/ImageProfileSerializer;
.super Ljava/lang/Object;
.source "ImageProfileSerializer.java"

# interfaces
.implements Lcom/google/gson/JsonDeserializer;
.implements Lcom/google/gson/JsonSerializer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/gson/JsonDeserializer",
        "<",
        "Lru/cn/api/tv/replies/TelecastImage$Profile;",
        ">;",
        "Lcom/google/gson/JsonSerializer",
        "<",
        "Lru/cn/api/tv/replies/TelecastImage$Profile;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic deserialize(Lcom/google/gson/JsonElement;Ljava/lang/reflect/Type;Lcom/google/gson/JsonDeserializationContext;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/gson/JsonParseException;
        }
    .end annotation

    .prologue
    .line 13
    invoke-virtual {p0, p1, p2, p3}, Lru/cn/api/tv/retrofit/ImageProfileSerializer;->deserialize(Lcom/google/gson/JsonElement;Ljava/lang/reflect/Type;Lcom/google/gson/JsonDeserializationContext;)Lru/cn/api/tv/replies/TelecastImage$Profile;

    move-result-object v0

    return-object v0
.end method

.method public deserialize(Lcom/google/gson/JsonElement;Ljava/lang/reflect/Type;Lcom/google/gson/JsonDeserializationContext;)Lru/cn/api/tv/replies/TelecastImage$Profile;
    .locals 6
    .param p1, "json"    # Lcom/google/gson/JsonElement;
    .param p2, "typeOfT"    # Ljava/lang/reflect/Type;
    .param p3, "context"    # Lcom/google/gson/JsonDeserializationContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/gson/JsonParseException;
        }
    .end annotation

    .prologue
    .line 25
    invoke-static {}, Lru/cn/api/tv/replies/TelecastImage$Profile;->values()[Lru/cn/api/tv/replies/TelecastImage$Profile;

    move-result-object v2

    .line 26
    .local v2, "profiles":[Lru/cn/api/tv/replies/TelecastImage$Profile;
    invoke-virtual {p1}, Lcom/google/gson/JsonElement;->getAsInt()I

    move-result v0

    .line 27
    .local v0, "intValue":I
    array-length v4, v2

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v1, v2, v3

    .line 28
    .local v1, "profile":Lru/cn/api/tv/replies/TelecastImage$Profile;
    invoke-virtual {v1}, Lru/cn/api/tv/replies/TelecastImage$Profile;->getValue()I

    move-result v5

    if-ne v5, v0, :cond_0

    .line 32
    .end local v1    # "profile":Lru/cn/api/tv/replies/TelecastImage$Profile;
    :goto_1
    return-object v1

    .line 27
    .restart local v1    # "profile":Lru/cn/api/tv/replies/TelecastImage$Profile;
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 32
    .end local v1    # "profile":Lru/cn/api/tv/replies/TelecastImage$Profile;
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public bridge synthetic serialize(Ljava/lang/Object;Ljava/lang/reflect/Type;Lcom/google/gson/JsonSerializationContext;)Lcom/google/gson/JsonElement;
    .locals 1

    .prologue
    .line 13
    check-cast p1, Lru/cn/api/tv/replies/TelecastImage$Profile;

    invoke-virtual {p0, p1, p2, p3}, Lru/cn/api/tv/retrofit/ImageProfileSerializer;->serialize(Lru/cn/api/tv/replies/TelecastImage$Profile;Ljava/lang/reflect/Type;Lcom/google/gson/JsonSerializationContext;)Lcom/google/gson/JsonElement;

    move-result-object v0

    return-object v0
.end method

.method public serialize(Lru/cn/api/tv/replies/TelecastImage$Profile;Ljava/lang/reflect/Type;Lcom/google/gson/JsonSerializationContext;)Lcom/google/gson/JsonElement;
    .locals 2
    .param p1, "src"    # Lru/cn/api/tv/replies/TelecastImage$Profile;
    .param p2, "type"    # Ljava/lang/reflect/Type;
    .param p3, "arg2"    # Lcom/google/gson/JsonSerializationContext;

    .prologue
    .line 18
    new-instance v0, Lcom/google/gson/JsonPrimitive;

    invoke-virtual {p1}, Lru/cn/api/tv/replies/TelecastImage$Profile;->getValue()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/gson/JsonPrimitive;-><init>(Ljava/lang/Number;)V

    return-object v0
.end method
