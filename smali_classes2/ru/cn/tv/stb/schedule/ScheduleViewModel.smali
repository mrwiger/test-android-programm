.class Lru/cn/tv/stb/schedule/ScheduleViewModel;
.super Lru/cn/mvvm/RxViewModel;
.source "ScheduleViewModel.java"


# instance fields
.field private final loader:Lru/cn/mvvm/RxLoader;

.field private final schedule:Landroid/arch/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/arch/lifecycle/MutableLiveData",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lru/cn/mvvm/RxLoader;)V
    .locals 1
    .param p1, "loader"    # Lru/cn/mvvm/RxLoader;

    .prologue
    .line 27
    invoke-direct {p0}, Lru/cn/mvvm/RxViewModel;-><init>()V

    .line 28
    iput-object p1, p0, Lru/cn/tv/stb/schedule/ScheduleViewModel;->loader:Lru/cn/mvvm/RxLoader;

    .line 29
    new-instance v0, Landroid/arch/lifecycle/MutableLiveData;

    invoke-direct {v0}, Landroid/arch/lifecycle/MutableLiveData;-><init>()V

    iput-object v0, p0, Lru/cn/tv/stb/schedule/ScheduleViewModel;->schedule:Landroid/arch/lifecycle/MutableLiveData;

    .line 30
    return-void
.end method

.method private load(JJLjava/util/Calendar;)V
    .locals 5
    .param p1, "channelId"    # J
    .param p3, "territoryId"    # J
    .param p5, "date"    # Ljava/util/Calendar;

    .prologue
    .line 43
    const-string v3, "yyyy-MM-dd"

    invoke-static {p5, v3}, Lru/cn/utils/Utils;->format(Ljava/util/Calendar;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 45
    .local v1, "formattedDate":Ljava/lang/String;
    invoke-static {p1, p2, p3, p4, v1}, Lru/cn/api/provider/TvContentProviderContract;->channelSchedule(JJLjava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 49
    .local v2, "scheduleUri":Landroid/net/Uri;
    iget-object v3, p0, Lru/cn/tv/stb/schedule/ScheduleViewModel;->loader:Lru/cn/mvvm/RxLoader;

    .line 50
    invoke-virtual {v3, v2}, Lru/cn/mvvm/RxLoader;->query(Landroid/net/Uri;)Lio/reactivex/Observable;

    move-result-object v3

    .line 51
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v4

    invoke-virtual {v3, v4}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v3

    .line 52
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v4

    invoke-virtual {v3, v4}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v3

    iget-object v4, p0, Lru/cn/tv/stb/schedule/ScheduleViewModel;->schedule:Landroid/arch/lifecycle/MutableLiveData;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v4}, Lru/cn/tv/stb/schedule/ScheduleViewModel$$Lambda$0;->get$Lambda(Landroid/arch/lifecycle/MutableLiveData;)Lio/reactivex/functions/Consumer;

    move-result-object v4

    .line 53
    invoke-virtual {v3, v4}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 55
    .local v0, "disposable":Lio/reactivex/disposables/Disposable;
    invoke-virtual {p0, v0}, Lru/cn/tv/stb/schedule/ScheduleViewModel;->bind(Lio/reactivex/disposables/Disposable;)V

    .line 56
    return-void
.end method


# virtual methods
.method schedule()Landroid/arch/lifecycle/LiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/arch/lifecycle/LiveData",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 33
    iget-object v0, p0, Lru/cn/tv/stb/schedule/ScheduleViewModel;->schedule:Landroid/arch/lifecycle/MutableLiveData;

    return-object v0
.end method

.method setChannel(JJLjava/util/Calendar;)V
    .locals 1
    .param p1, "channelId"    # J
    .param p3, "territoryId"    # J
    .param p5, "date"    # Ljava/util/Calendar;

    .prologue
    .line 37
    invoke-virtual {p0}, Lru/cn/tv/stb/schedule/ScheduleViewModel;->unbindAll()V

    .line 39
    invoke-direct/range {p0 .. p5}, Lru/cn/tv/stb/schedule/ScheduleViewModel;->load(JJLjava/util/Calendar;)V

    .line 40
    return-void
.end method
