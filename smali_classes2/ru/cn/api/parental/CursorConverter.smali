.class public final Lru/cn/api/parental/CursorConverter;
.super Ljava/lang/Object;
.source "CursorConverter.java"


# direct methods
.method public static convert(Landroid/database/Cursor;)Landroid/content/ContentValues;
    .locals 4
    .param p0, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 13
    if-eqz p0, :cond_0

    invoke-interface {p0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 14
    :cond_0
    const/4 v0, 0x0

    .line 29
    :goto_0
    return-object v0

    .line 16
    :cond_1
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 17
    .local v0, "contentValues":Landroid/content/ContentValues;
    sget-object v1, Lru/cn/api/parental/DataBaseSchema$BlockedChannelContract;->CN_ID:Lru/cn/api/parental/DataBaseSchema$BlockedChannelContract;

    .line 18
    invoke-virtual {v1}, Lru/cn/api/parental/DataBaseSchema$BlockedChannelContract;->getColumnName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "cn_id"

    .line 19
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 17
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 21
    sget-object v1, Lru/cn/api/parental/DataBaseSchema$BlockedChannelContract;->NAME_CHANNEL:Lru/cn/api/parental/DataBaseSchema$BlockedChannelContract;

    .line 22
    invoke-virtual {v1}, Lru/cn/api/parental/DataBaseSchema$BlockedChannelContract;->getColumnName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "title"

    .line 23
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 21
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    sget-object v1, Lru/cn/api/parental/DataBaseSchema$BlockedChannelContract;->IMAGE_URL:Lru/cn/api/parental/DataBaseSchema$BlockedChannelContract;

    .line 26
    invoke-virtual {v1}, Lru/cn/api/parental/DataBaseSchema$BlockedChannelContract;->getColumnName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "image"

    .line 27
    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 25
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
