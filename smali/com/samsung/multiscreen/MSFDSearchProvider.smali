.class public Lcom/samsung/multiscreen/MSFDSearchProvider;
.super Lcom/samsung/multiscreen/SearchProvider;
.source "MSFDSearchProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/multiscreen/MSFDSearchProvider$FutureRunnable;
    }
.end annotation


# static fields
.field private static final DISCOVER_INTERVAL:J = 0x3e8L

.field private static final DISCOVER_START_DELAY:J = 0x64L

.field private static final KEY_DATA:Ljava/lang/String; = "data"

.field private static final KEY_SID:Ljava/lang/String; = "sid"

.field private static final KEY_TTL:Ljava/lang/String; = "ttl"

.field private static final KEY_TYPE_STATE:Ljava/lang/String; = "type"

.field private static final KEY_URI:Ljava/lang/String; = "uri"

.field private static final KEY_VERSION_1:Ljava/lang/String; = "v1"

.field private static final KEY_VERSION_2:Ljava/lang/String; = "v2"

.field private static final MAX_DISCOVER_NUM:I = 0x3

.field private static final MAX_GET_SERVICE_INFO_WAIT_TIME:J = 0x3a98L

.field private static final MULTICAST_GROUP:Ljava/lang/String; = "224.0.0.7"

.field private static final MULTICAST_PORT:I = 0x1f41

.field private static final SERVICE_CHECK_TIMEOUT:I = 0x7d0

.field private static final SOCKET_TIMEOUT:I = 0x2710

.field private static final STATE_ALIVE:Ljava/lang/String; = "alive"

.field private static final STATE_DOWN:Ljava/lang/String; = "down"

.field private static final STATE_UP:Ljava/lang/String; = "up"

.field private static final TAG:Ljava/lang/String; = "MSFDSearchProvider"

.field private static final TYPE_DISCOVER:Ljava/lang/String; = "discover"

.field private static final discoverMessage:Ljava/lang/String;

.field private static volatile multicastInetAddress:Ljava/net/InetAddress;


# instance fields
.field private final aliveMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final context:Landroid/content/Context;

.field private discoverPacket:Ljava/net/DatagramPacket;

.field private executor:Ljava/util/concurrent/ScheduledExecutorService;

.field private volatile multicastLock:Landroid/net/wifi/WifiManager$MulticastLock;

.field private receive:Z

.field private final receiveHandler:Ljava/lang/Runnable;

.field private receiverThread:Ljava/lang/Thread;

.field private volatile socket:Ljava/net/MulticastSocket;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 89
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 90
    .local v0, "discoverMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v1, "type"

    const-string v2, "discover"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    invoke-static {v0}, Lcom/samsung/multiscreen/util/JSONUtil;->toJSONString(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/samsung/multiscreen/MSFDSearchProvider;->discoverMessage:Ljava/lang/String;

    .line 92
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 247
    invoke-direct {p0}, Lcom/samsung/multiscreen/SearchProvider;-><init>()V

    .line 97
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/multiscreen/MSFDSearchProvider;->discoverPacket:Ljava/net/DatagramPacket;

    .line 101
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/multiscreen/MSFDSearchProvider;->receive:Z

    .line 102
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/multiscreen/MSFDSearchProvider;->aliveMap:Ljava/util/Map;

    .line 105
    new-instance v0, Lcom/samsung/multiscreen/MSFDSearchProvider$1;

    invoke-direct {v0, p0}, Lcom/samsung/multiscreen/MSFDSearchProvider$1;-><init>(Lcom/samsung/multiscreen/MSFDSearchProvider;)V

    iput-object v0, p0, Lcom/samsung/multiscreen/MSFDSearchProvider;->receiveHandler:Ljava/lang/Runnable;

    .line 249
    iput-object p1, p0, Lcom/samsung/multiscreen/MSFDSearchProvider;->context:Landroid/content/Context;

    .line 250
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/samsung/multiscreen/Search$SearchListener;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "searchListener"    # Lcom/samsung/multiscreen/Search$SearchListener;

    .prologue
    .line 253
    invoke-direct {p0, p2}, Lcom/samsung/multiscreen/SearchProvider;-><init>(Lcom/samsung/multiscreen/Search$SearchListener;)V

    .line 97
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/multiscreen/MSFDSearchProvider;->discoverPacket:Ljava/net/DatagramPacket;

    .line 101
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/multiscreen/MSFDSearchProvider;->receive:Z

    .line 102
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/multiscreen/MSFDSearchProvider;->aliveMap:Ljava/util/Map;

    .line 105
    new-instance v0, Lcom/samsung/multiscreen/MSFDSearchProvider$1;

    invoke-direct {v0, p0}, Lcom/samsung/multiscreen/MSFDSearchProvider$1;-><init>(Lcom/samsung/multiscreen/MSFDSearchProvider;)V

    iput-object v0, p0, Lcom/samsung/multiscreen/MSFDSearchProvider;->receiveHandler:Ljava/lang/Runnable;

    .line 255
    iput-object p1, p0, Lcom/samsung/multiscreen/MSFDSearchProvider;->context:Landroid/content/Context;

    .line 256
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/multiscreen/MSFDSearchProvider;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/multiscreen/MSFDSearchProvider;

    .prologue
    .line 58
    iget-boolean v0, p0, Lcom/samsung/multiscreen/MSFDSearchProvider;->receive:Z

    return v0
.end method

.method static synthetic access$100(Lcom/samsung/multiscreen/MSFDSearchProvider;)Ljava/net/MulticastSocket;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/multiscreen/MSFDSearchProvider;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/samsung/multiscreen/MSFDSearchProvider;->socket:Ljava/net/MulticastSocket;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/multiscreen/MSFDSearchProvider;)Ljava/util/Map;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/multiscreen/MSFDSearchProvider;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/samsung/multiscreen/MSFDSearchProvider;->aliveMap:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/multiscreen/MSFDSearchProvider;)Ljava/net/DatagramPacket;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/multiscreen/MSFDSearchProvider;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/samsung/multiscreen/MSFDSearchProvider;->discoverPacket:Ljava/net/DatagramPacket;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/multiscreen/MSFDSearchProvider;)Ljava/util/concurrent/ScheduledExecutorService;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/multiscreen/MSFDSearchProvider;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/samsung/multiscreen/MSFDSearchProvider;->executor:Ljava/util/concurrent/ScheduledExecutorService;

    return-object v0
.end method

.method private acquireMulticastLock()V
    .locals 2

    .prologue
    .line 586
    iget-object v0, p0, Lcom/samsung/multiscreen/MSFDSearchProvider;->multicastLock:Landroid/net/wifi/WifiManager$MulticastLock;

    if-nez v0, :cond_1

    .line 587
    iget-object v0, p0, Lcom/samsung/multiscreen/MSFDSearchProvider;->context:Landroid/content/Context;

    const-string v1, "MSFDSearchProvider"

    invoke-static {v0, v1}, Lcom/samsung/multiscreen/util/NetUtil;->acquireMulticastLock(Landroid/content/Context;Ljava/lang/String;)Landroid/net/wifi/WifiManager$MulticastLock;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/multiscreen/MSFDSearchProvider;->multicastLock:Landroid/net/wifi/WifiManager$MulticastLock;

    .line 591
    :cond_0
    :goto_0
    return-void

    .line 588
    :cond_1
    iget-object v0, p0, Lcom/samsung/multiscreen/MSFDSearchProvider;->multicastLock:Landroid/net/wifi/WifiManager$MulticastLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$MulticastLock;->isHeld()Z

    move-result v0

    if-nez v0, :cond_0

    .line 589
    iget-object v0, p0, Lcom/samsung/multiscreen/MSFDSearchProvider;->multicastLock:Landroid/net/wifi/WifiManager$MulticastLock;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager$MulticastLock;->acquire()V

    goto :goto_0
.end method

.method public static create(Landroid/content/Context;)Lcom/samsung/multiscreen/SearchProvider;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 378
    new-instance v0, Lcom/samsung/multiscreen/MSFDSearchProvider;

    invoke-direct {v0, p0}, Lcom/samsung/multiscreen/MSFDSearchProvider;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method static create(Landroid/content/Context;Lcom/samsung/multiscreen/Search$SearchListener;)Lcom/samsung/multiscreen/SearchProvider;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "searchListener"    # Lcom/samsung/multiscreen/Search$SearchListener;

    .prologue
    .line 383
    new-instance v0, Lcom/samsung/multiscreen/MSFDSearchProvider;

    invoke-direct {v0, p0, p1}, Lcom/samsung/multiscreen/MSFDSearchProvider;-><init>(Landroid/content/Context;Lcom/samsung/multiscreen/Search$SearchListener;)V

    return-object v0
.end method

.method static getById(Landroid/content/Context;Ljava/lang/String;Lcom/samsung/multiscreen/Result;)Lcom/samsung/multiscreen/ProviderThread;
    .locals 23
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "searchId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Lcom/samsung/multiscreen/Result",
            "<",
            "Lcom/samsung/multiscreen/Service;",
            ">;)",
            "Lcom/samsung/multiscreen/ProviderThread;"
        }
    .end annotation

    .prologue
    .line 389
    .local p2, "result":Lcom/samsung/multiscreen/Result;, "Lcom/samsung/multiscreen/Result<Lcom/samsung/multiscreen/Service;>;"
    const/16 v21, 0x0

    .line 390
    .local v21, "thread":Lcom/samsung/multiscreen/ProviderThread;
    const/16 v18, 0x0

    .line 392
    .local v18, "socket":Ljava/net/MulticastSocket;
    :try_start_0
    const-string v2, "MSFDSearchProvider"

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/samsung/multiscreen/util/NetUtil;->acquireMulticastLock(Landroid/content/Context;Ljava/lang/String;)Landroid/net/wifi/WifiManager$MulticastLock;

    move-result-object v7

    .line 394
    .local v7, "multicastLock":Landroid/net/wifi/WifiManager$MulticastLock;
    const-string v2, "224.0.0.7"

    invoke-static {v2}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v6

    .line 395
    .local v6, "multicastInetAddress":Ljava/net/InetAddress;
    new-instance v17, Ljava/net/InetSocketAddress;

    const-string v2, "224.0.0.7"

    const/16 v4, 0x1f41

    move-object/from16 v0, v17

    invoke-direct {v0, v2, v4}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    .line 396
    .local v17, "multicastGroup":Ljava/net/InetSocketAddress;
    new-instance v15, Ljava/net/DatagramPacket;

    sget-object v2, Lcom/samsung/multiscreen/MSFDSearchProvider;->discoverMessage:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    sget-object v4, Lcom/samsung/multiscreen/MSFDSearchProvider;->discoverMessage:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    move-object/from16 v0, v17

    invoke-direct {v15, v2, v4, v0}, Ljava/net/DatagramPacket;-><init>([BILjava/net/SocketAddress;)V

    .line 399
    .local v15, "discoverPacket":Ljava/net/DatagramPacket;
    new-instance v19, Ljava/net/MulticastSocket;

    const/16 v2, 0x1f41

    move-object/from16 v0, v19

    invoke-direct {v0, v2}, Ljava/net/MulticastSocket;-><init>(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 402
    .end local v18    # "socket":Ljava/net/MulticastSocket;
    .local v19, "socket":Ljava/net/MulticastSocket;
    :try_start_1
    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Ljava/net/MulticastSocket;->joinGroup(Ljava/net/InetAddress;)V

    .line 404
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadScheduledExecutor()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v8

    .line 405
    .local v8, "executor":Ljava/util/concurrent/ScheduledExecutorService;
    move-object/from16 v3, v19

    .line 408
    .local v3, "threadSocket":Ljava/net/MulticastSocket;
    new-instance v22, Lcom/samsung/multiscreen/MSFDSearchProvider$4;

    new-instance v2, Lcom/samsung/multiscreen/MSFDSearchProvider$3;

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    invoke-direct/range {v2 .. v8}, Lcom/samsung/multiscreen/MSFDSearchProvider$3;-><init>(Ljava/net/MulticastSocket;Ljava/lang/String;Lcom/samsung/multiscreen/Result;Ljava/net/InetAddress;Landroid/net/wifi/WifiManager$MulticastLock;Ljava/util/concurrent/ScheduledExecutorService;)V

    move-object/from16 v0, v22

    invoke-direct {v0, v2}, Lcom/samsung/multiscreen/MSFDSearchProvider$4;-><init>(Ljava/lang/Runnable;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 524
    .end local v21    # "thread":Lcom/samsung/multiscreen/ProviderThread;
    .local v22, "thread":Lcom/samsung/multiscreen/ProviderThread;
    :try_start_2
    invoke-virtual/range {v22 .. v22}, Lcom/samsung/multiscreen/ProviderThread;->start()V

    .line 527
    new-instance v2, Lcom/samsung/multiscreen/MSFDSearchProvider$5;

    move-object/from16 v0, p2

    invoke-direct {v2, v3, v0}, Lcom/samsung/multiscreen/MSFDSearchProvider$5;-><init>(Ljava/net/MulticastSocket;Lcom/samsung/multiscreen/Result;)V

    const-wide/16 v4, 0x3a98

    sget-object v10, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v8, v2, v4, v5, v10}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 543
    new-instance v9, Lcom/samsung/multiscreen/MSFDSearchProvider$6;

    invoke-direct {v9, v3, v15}, Lcom/samsung/multiscreen/MSFDSearchProvider$6;-><init>(Ljava/net/MulticastSocket;Ljava/net/DatagramPacket;)V

    .line 566
    .local v9, "runnable":Lcom/samsung/multiscreen/MSFDSearchProvider$FutureRunnable;
    const-wide/16 v10, 0x64

    const-wide/16 v12, 0x3e8

    sget-object v14, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface/range {v8 .. v14}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleAtFixedRate(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v20

    .line 567
    .local v20, "t":Ljava/util/concurrent/ScheduledFuture;, "Ljava/util/concurrent/ScheduledFuture<*>;"
    move-object/from16 v0, v20

    invoke-interface {v9, v0}, Lcom/samsung/multiscreen/MSFDSearchProvider$FutureRunnable;->setFuture(Ljava/util/concurrent/ScheduledFuture;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    move-object/from16 v18, v19

    .end local v19    # "socket":Ljava/net/MulticastSocket;
    .restart local v18    # "socket":Ljava/net/MulticastSocket;
    move-object/from16 v21, v22

    .line 581
    .end local v3    # "threadSocket":Ljava/net/MulticastSocket;
    .end local v6    # "multicastInetAddress":Ljava/net/InetAddress;
    .end local v7    # "multicastLock":Landroid/net/wifi/WifiManager$MulticastLock;
    .end local v8    # "executor":Ljava/util/concurrent/ScheduledExecutorService;
    .end local v9    # "runnable":Lcom/samsung/multiscreen/MSFDSearchProvider$FutureRunnable;
    .end local v15    # "discoverPacket":Ljava/net/DatagramPacket;
    .end local v17    # "multicastGroup":Ljava/net/InetSocketAddress;
    .end local v20    # "t":Ljava/util/concurrent/ScheduledFuture;, "Ljava/util/concurrent/ScheduledFuture<*>;"
    .end local v22    # "thread":Lcom/samsung/multiscreen/ProviderThread;
    .restart local v21    # "thread":Lcom/samsung/multiscreen/ProviderThread;
    :goto_0
    return-object v21

    .line 568
    :catch_0
    move-exception v16

    .line 569
    .local v16, "e":Ljava/lang/Exception;
    :goto_1
    const-string v2, "MSFDSearchProvider"

    invoke-static/range {v16 .. v16}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 570
    if-eqz v18, :cond_0

    invoke-virtual/range {v18 .. v18}, Ljava/net/MulticastSocket;->isClosed()Z

    move-result v2

    if-nez v2, :cond_0

    .line 571
    invoke-virtual/range {v18 .. v18}, Ljava/net/MulticastSocket;->close()V

    .line 573
    :cond_0
    new-instance v2, Lcom/samsung/multiscreen/MSFDSearchProvider$7;

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-direct {v2, v0, v1}, Lcom/samsung/multiscreen/MSFDSearchProvider$7;-><init>(Lcom/samsung/multiscreen/Result;Ljava/lang/Exception;)V

    invoke-static {v2}, Lcom/samsung/multiscreen/util/RunUtil;->runOnUI(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 568
    .end local v16    # "e":Ljava/lang/Exception;
    .end local v18    # "socket":Ljava/net/MulticastSocket;
    .restart local v6    # "multicastInetAddress":Ljava/net/InetAddress;
    .restart local v7    # "multicastLock":Landroid/net/wifi/WifiManager$MulticastLock;
    .restart local v15    # "discoverPacket":Ljava/net/DatagramPacket;
    .restart local v17    # "multicastGroup":Ljava/net/InetSocketAddress;
    .restart local v19    # "socket":Ljava/net/MulticastSocket;
    :catch_1
    move-exception v16

    move-object/from16 v18, v19

    .end local v19    # "socket":Ljava/net/MulticastSocket;
    .restart local v18    # "socket":Ljava/net/MulticastSocket;
    goto :goto_1

    .end local v18    # "socket":Ljava/net/MulticastSocket;
    .end local v21    # "thread":Lcom/samsung/multiscreen/ProviderThread;
    .restart local v3    # "threadSocket":Ljava/net/MulticastSocket;
    .restart local v8    # "executor":Ljava/util/concurrent/ScheduledExecutorService;
    .restart local v19    # "socket":Ljava/net/MulticastSocket;
    .restart local v22    # "thread":Lcom/samsung/multiscreen/ProviderThread;
    :catch_2
    move-exception v16

    move-object/from16 v18, v19

    .end local v19    # "socket":Ljava/net/MulticastSocket;
    .restart local v18    # "socket":Ljava/net/MulticastSocket;
    move-object/from16 v21, v22

    .end local v22    # "thread":Lcom/samsung/multiscreen/ProviderThread;
    .restart local v21    # "thread":Lcom/samsung/multiscreen/ProviderThread;
    goto :goto_1
.end method

.method private setupDiscovery()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 261
    const-string v1, "224.0.0.7"

    invoke-static {v1}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v1

    sput-object v1, Lcom/samsung/multiscreen/MSFDSearchProvider;->multicastInetAddress:Ljava/net/InetAddress;

    .line 262
    new-instance v0, Ljava/net/InetSocketAddress;

    const-string v1, "224.0.0.7"

    const/16 v2, 0x1f41

    invoke-direct {v0, v1, v2}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    .line 263
    .local v0, "multicastGroup":Ljava/net/InetSocketAddress;
    new-instance v1, Ljava/net/DatagramPacket;

    sget-object v2, Lcom/samsung/multiscreen/MSFDSearchProvider;->discoverMessage:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    sget-object v3, Lcom/samsung/multiscreen/MSFDSearchProvider;->discoverMessage:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    invoke-direct {v1, v2, v3, v0}, Ljava/net/DatagramPacket;-><init>([BILjava/net/SocketAddress;)V

    iput-object v1, p0, Lcom/samsung/multiscreen/MSFDSearchProvider;->discoverPacket:Ljava/net/DatagramPacket;

    .line 264
    return-void
.end method


# virtual methods
.method public start()V
    .locals 8

    .prologue
    .line 269
    iget-boolean v0, p0, Lcom/samsung/multiscreen/MSFDSearchProvider;->searching:Z

    if-eqz v0, :cond_0

    .line 270
    invoke-virtual {p0}, Lcom/samsung/multiscreen/MSFDSearchProvider;->stop()Z

    .line 273
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/multiscreen/MSFDSearchProvider;->clearServices()V

    .line 274
    iget-object v0, p0, Lcom/samsung/multiscreen/MSFDSearchProvider;->aliveMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 276
    :try_start_0
    iget-object v0, p0, Lcom/samsung/multiscreen/MSFDSearchProvider;->discoverPacket:Ljava/net/DatagramPacket;

    if-nez v0, :cond_1

    .line 277
    invoke-direct {p0}, Lcom/samsung/multiscreen/MSFDSearchProvider;->setupDiscovery()V

    .line 280
    :cond_1
    invoke-direct {p0}, Lcom/samsung/multiscreen/MSFDSearchProvider;->acquireMulticastLock()V

    .line 282
    new-instance v0, Ljava/net/MulticastSocket;

    const/16 v1, 0x1f41

    invoke-direct {v0, v1}, Ljava/net/MulticastSocket;-><init>(I)V

    iput-object v0, p0, Lcom/samsung/multiscreen/MSFDSearchProvider;->socket:Ljava/net/MulticastSocket;

    .line 283
    iget-object v0, p0, Lcom/samsung/multiscreen/MSFDSearchProvider;->socket:Ljava/net/MulticastSocket;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/net/MulticastSocket;->setBroadcast(Z)V

    .line 284
    iget-object v0, p0, Lcom/samsung/multiscreen/MSFDSearchProvider;->socket:Ljava/net/MulticastSocket;

    const/16 v1, 0x2710

    invoke-virtual {v0, v1}, Ljava/net/MulticastSocket;->setSoTimeout(I)V

    .line 288
    iget-object v0, p0, Lcom/samsung/multiscreen/MSFDSearchProvider;->socket:Ljava/net/MulticastSocket;

    new-instance v1, Ljava/net/InetSocketAddress;

    sget-object v2, Lcom/samsung/multiscreen/MSFDSearchProvider;->multicastInetAddress:Ljava/net/InetAddress;

    const/16 v3, 0x1f41

    invoke-direct {v1, v2, v3}, Ljava/net/InetSocketAddress;-><init>(Ljava/net/InetAddress;I)V

    const-string v2, "eth0"

    invoke-static {v2}, Ljava/net/NetworkInterface;->getByName(Ljava/lang/String;)Ljava/net/NetworkInterface;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/net/MulticastSocket;->joinGroup(Ljava/net/SocketAddress;Ljava/net/NetworkInterface;)V

    .line 290
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/multiscreen/MSFDSearchProvider;->receive:Z

    .line 291
    new-instance v0, Ljava/lang/Thread;

    iget-object v1, p0, Lcom/samsung/multiscreen/MSFDSearchProvider;->receiveHandler:Ljava/lang/Runnable;

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/samsung/multiscreen/MSFDSearchProvider;->receiverThread:Ljava/lang/Thread;

    .line 292
    iget-object v0, p0, Lcom/samsung/multiscreen/MSFDSearchProvider;->receiverThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 295
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadScheduledExecutor()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/multiscreen/MSFDSearchProvider;->executor:Ljava/util/concurrent/ScheduledExecutorService;

    .line 296
    iget-object v0, p0, Lcom/samsung/multiscreen/MSFDSearchProvider;->executor:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/samsung/multiscreen/MSFDSearchProvider$2;

    invoke-direct {v1, p0}, Lcom/samsung/multiscreen/MSFDSearchProvider$2;-><init>(Lcom/samsung/multiscreen/MSFDSearchProvider;)V

    const-wide/16 v2, 0x64

    const-wide/16 v4, 0x3e8

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface/range {v0 .. v6}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleAtFixedRate(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 318
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/multiscreen/MSFDSearchProvider;->searching:Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 323
    :goto_0
    iget-boolean v0, p0, Lcom/samsung/multiscreen/MSFDSearchProvider;->searching:Z

    if-nez v0, :cond_3

    .line 324
    iget-object v0, p0, Lcom/samsung/multiscreen/MSFDSearchProvider;->socket:Ljava/net/MulticastSocket;

    if-eqz v0, :cond_2

    .line 325
    iget-object v0, p0, Lcom/samsung/multiscreen/MSFDSearchProvider;->socket:Ljava/net/MulticastSocket;

    invoke-virtual {v0}, Ljava/net/MulticastSocket;->close()V

    .line 329
    :cond_2
    iget-object v0, p0, Lcom/samsung/multiscreen/MSFDSearchProvider;->multicastLock:Landroid/net/wifi/WifiManager$MulticastLock;

    invoke-static {v0}, Lcom/samsung/multiscreen/util/NetUtil;->releaseMulticastLock(Landroid/net/wifi/WifiManager$MulticastLock;)V

    .line 331
    :cond_3
    return-void

    .line 319
    :catch_0
    move-exception v7

    .line 320
    .local v7, "e":Ljava/io/IOException;
    const-string v0, "MSFDSearchProvider"

    invoke-static {v7}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public stop()Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 336
    iget-boolean v2, p0, Lcom/samsung/multiscreen/MSFDSearchProvider;->searching:Z

    if-nez v2, :cond_0

    .line 368
    :goto_0
    return v1

    .line 339
    :cond_0
    iput-boolean v1, p0, Lcom/samsung/multiscreen/MSFDSearchProvider;->searching:Z

    .line 341
    iget-object v2, p0, Lcom/samsung/multiscreen/MSFDSearchProvider;->multicastLock:Landroid/net/wifi/WifiManager$MulticastLock;

    invoke-static {v2}, Lcom/samsung/multiscreen/util/NetUtil;->releaseMulticastLock(Landroid/net/wifi/WifiManager$MulticastLock;)V

    .line 344
    iget-object v2, p0, Lcom/samsung/multiscreen/MSFDSearchProvider;->executor:Ljava/util/concurrent/ScheduledExecutorService;

    if-eqz v2, :cond_1

    .line 345
    iget-object v2, p0, Lcom/samsung/multiscreen/MSFDSearchProvider;->executor:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {v2}, Ljava/util/concurrent/ScheduledExecutorService;->shutdown()V

    .line 346
    iput-object v4, p0, Lcom/samsung/multiscreen/MSFDSearchProvider;->executor:Ljava/util/concurrent/ScheduledExecutorService;

    .line 350
    :cond_1
    iput-boolean v1, p0, Lcom/samsung/multiscreen/MSFDSearchProvider;->receive:Z

    .line 351
    iget-object v1, p0, Lcom/samsung/multiscreen/MSFDSearchProvider;->socket:Ljava/net/MulticastSocket;

    if-eqz v1, :cond_2

    sget-object v1, Lcom/samsung/multiscreen/MSFDSearchProvider;->multicastInetAddress:Ljava/net/InetAddress;

    if-eqz v1, :cond_2

    .line 353
    :try_start_0
    iget-object v1, p0, Lcom/samsung/multiscreen/MSFDSearchProvider;->socket:Ljava/net/MulticastSocket;

    sget-object v2, Lcom/samsung/multiscreen/MSFDSearchProvider;->multicastInetAddress:Ljava/net/InetAddress;

    invoke-virtual {v1, v2}, Ljava/net/MulticastSocket;->leaveGroup(Ljava/net/InetAddress;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 359
    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/samsung/multiscreen/MSFDSearchProvider;->receiverThread:Ljava/lang/Thread;

    if-eqz v1, :cond_3

    .line 361
    :try_start_1
    iget-object v1, p0, Lcom/samsung/multiscreen/MSFDSearchProvider;->receiverThread:Ljava/lang/Thread;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v1, v2, v3}, Ljava/lang/Thread;->join(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    .line 365
    :goto_2
    iput-object v4, p0, Lcom/samsung/multiscreen/MSFDSearchProvider;->receiverThread:Ljava/lang/Thread;

    .line 368
    :cond_3
    const/4 v1, 0x1

    goto :goto_0

    .line 354
    :catch_0
    move-exception v0

    .line 355
    .local v0, "e":Ljava/io/IOException;
    const-string v1, "MSFDSearchProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "stop exception: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 362
    .end local v0    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 363
    .local v0, "e":Ljava/lang/InterruptedException;
    const-string v1, "MSFDSearchProvider"

    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method
