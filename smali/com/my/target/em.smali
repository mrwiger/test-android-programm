.class public final Lcom/my/target/em;
.super Lcom/my/target/ee;
.source "CarouselContainerView.java"


# instance fields
.field private final cL:Lcom/my/target/ee;

.field private final cM:Lcom/my/target/ee;

.field private cN:Lcom/my/target/ee;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 29
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/my/target/ee;-><init>(Landroid/content/Context;I)V

    .line 30
    new-instance v0, Lcom/my/target/en;

    invoke-direct {v0, p1}, Lcom/my/target/en;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/em;->cL:Lcom/my/target/ee;

    .line 31
    new-instance v0, Lcom/my/target/ep;

    invoke-direct {v0, p1}, Lcom/my/target/ep;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/em;->cM:Lcom/my/target/ee;

    .line 32
    iget-object v0, p0, Lcom/my/target/em;->cM:Lcom/my/target/ee;

    iput-object v0, p0, Lcom/my/target/em;->cN:Lcom/my/target/ee;

    .line 33
    return-void
.end method


# virtual methods
.method public final G()V
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/my/target/em;->cM:Lcom/my/target/ee;

    invoke-virtual {v0}, Lcom/my/target/ee;->G()V

    .line 64
    iget-object v0, p0, Lcom/my/target/em;->cL:Lcom/my/target/ee;

    invoke-virtual {v0}, Lcom/my/target/ee;->G()V

    .line 65
    return-void
.end method

.method public final b(Lcom/my/target/core/models/banners/h;)V
    .locals 0

    .prologue
    .line 59
    return-void
.end method

.method public final e(I)V
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/my/target/em;->cL:Lcom/my/target/ee;

    invoke-virtual {v0, p1}, Lcom/my/target/ee;->e(I)V

    .line 143
    iget-object v0, p0, Lcom/my/target/em;->cM:Lcom/my/target/ee;

    invoke-virtual {v0, p1}, Lcom/my/target/ee;->e(I)V

    .line 144
    return-void
.end method

.method public final f(Z)V
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/my/target/em;->cL:Lcom/my/target/ee;

    invoke-virtual {v0, p1}, Lcom/my/target/ee;->f(Z)V

    .line 122
    iget-object v0, p0, Lcom/my/target/em;->cM:Lcom/my/target/ee;

    invoke-virtual {v0, p1}, Lcom/my/target/ee;->f(Z)V

    .line 123
    return-void
.end method

.method public final finish()V
    .locals 0

    .prologue
    .line 98
    return-void
.end method

.method public final getCloseButton()Landroid/view/View;
    .locals 1

    .prologue
    .line 86
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getNumbersOfCurrentShowingCards()[I
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/my/target/em;->cN:Lcom/my/target/ee;

    invoke-virtual {v0}, Lcom/my/target/ee;->getNumbersOfCurrentShowingCards()[I

    move-result-object v0

    return-object v0
.end method

.method public final getSoundButton()Lcom/my/target/by;
    .locals 1

    .prologue
    .line 80
    const/4 v0, 0x0

    return-object v0
.end method

.method public final isPaused()Z
    .locals 1

    .prologue
    .line 74
    const/4 v0, 0x0

    return v0
.end method

.method public final isPlaying()Z
    .locals 1

    .prologue
    .line 69
    const/4 v0, 0x0

    return v0
.end method

.method public final pause()V
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/my/target/em;->cL:Lcom/my/target/ee;

    invoke-virtual {v0}, Lcom/my/target/ee;->pause()V

    .line 136
    iget-object v0, p0, Lcom/my/target/em;->cM:Lcom/my/target/ee;

    invoke-virtual {v0}, Lcom/my/target/ee;->pause()V

    .line 137
    return-void
.end method

.method public final play()V
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/my/target/em;->cN:Lcom/my/target/ee;

    invoke-virtual {v0}, Lcom/my/target/ee;->play()V

    .line 92
    return-void
.end method

.method public final resume()V
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/my/target/em;->cL:Lcom/my/target/ee;

    invoke-virtual {v0}, Lcom/my/target/ee;->resume()V

    .line 108
    iget-object v0, p0, Lcom/my/target/em;->cM:Lcom/my/target/ee;

    invoke-virtual {v0}, Lcom/my/target/ee;->resume()V

    .line 109
    return-void
.end method

.method public final setBanner(Lcom/my/target/core/models/banners/h;)V
    .locals 1
    .param p1, "banner"    # Lcom/my/target/core/models/banners/h;

    .prologue
    .line 44
    invoke-super {p0, p1}, Lcom/my/target/ee;->setBanner(Lcom/my/target/core/models/banners/h;)V

    .line 45
    iget-object v0, p0, Lcom/my/target/em;->cM:Lcom/my/target/ee;

    invoke-virtual {v0, p1}, Lcom/my/target/ee;->setBanner(Lcom/my/target/core/models/banners/h;)V

    .line 46
    iget-object v0, p0, Lcom/my/target/em;->cL:Lcom/my/target/ee;

    invoke-virtual {v0, p1}, Lcom/my/target/ee;->setBanner(Lcom/my/target/core/models/banners/h;)V

    .line 47
    return-void
.end method

.method public final setClickArea(Lcom/my/target/af;)V
    .locals 2
    .param p1, "area"    # Lcom/my/target/af;

    .prologue
    .line 114
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Apply click area "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/my/target/af;->O()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " to view"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 115
    iget-object v0, p0, Lcom/my/target/em;->cL:Lcom/my/target/ee;

    invoke-virtual {v0, p1}, Lcom/my/target/ee;->setClickArea(Lcom/my/target/af;)V

    .line 116
    iget-object v0, p0, Lcom/my/target/em;->cM:Lcom/my/target/ee;

    invoke-virtual {v0, p1}, Lcom/my/target/ee;->setClickArea(Lcom/my/target/af;)V

    .line 117
    return-void
.end method

.method public final setInterstitialPromoViewListener(Lcom/my/target/ee$b;)V
    .locals 1
    .param p1, "interstitialPromoViewListener"    # Lcom/my/target/ee$b;

    .prologue
    .line 128
    invoke-super {p0, p1}, Lcom/my/target/ee;->setInterstitialPromoViewListener(Lcom/my/target/ee$b;)V

    .line 129
    iget-object v0, p0, Lcom/my/target/em;->cL:Lcom/my/target/ee;

    invoke-virtual {v0, p1}, Lcom/my/target/ee;->setInterstitialPromoViewListener(Lcom/my/target/ee$b;)V

    .line 130
    iget-object v0, p0, Lcom/my/target/em;->cM:Lcom/my/target/ee;

    invoke-virtual {v0, p1}, Lcom/my/target/ee;->setInterstitialPromoViewListener(Lcom/my/target/ee$b;)V

    .line 131
    return-void
.end method

.method protected final setLayoutOrientation(I)V
    .locals 3
    .param p1, "orientation"    # I

    .prologue
    const/4 v2, -0x1

    .line 149
    invoke-super {p0, p1}, Lcom/my/target/ee;->setLayoutOrientation(I)V

    .line 150
    iget-object v0, p0, Lcom/my/target/em;->cL:Lcom/my/target/ee;

    invoke-virtual {v0}, Lcom/my/target/ee;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 151
    if-eqz v0, :cond_0

    .line 153
    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/my/target/em;->cL:Lcom/my/target/ee;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 156
    :cond_0
    iget-object v0, p0, Lcom/my/target/em;->cM:Lcom/my/target/ee;

    invoke-virtual {v0}, Lcom/my/target/ee;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 157
    if-eqz v0, :cond_1

    .line 159
    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/my/target/em;->cM:Lcom/my/target/ee;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 162
    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_2

    .line 164
    iget-object v0, p0, Lcom/my/target/em;->cL:Lcom/my/target/ee;

    iput-object v0, p0, Lcom/my/target/em;->cN:Lcom/my/target/ee;

    .line 170
    :goto_0
    iget-object v0, p0, Lcom/my/target/em;->cN:Lcom/my/target/ee;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Lcom/my/target/em;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 171
    return-void

    .line 168
    :cond_2
    iget-object v0, p0, Lcom/my/target/em;->cM:Lcom/my/target/ee;

    iput-object v0, p0, Lcom/my/target/em;->cN:Lcom/my/target/ee;

    goto :goto_0
.end method

.method public final setTimeChanged(F)V
    .locals 0

    .prologue
    .line 103
    return-void
.end method
