.class Lru/cn/tv/mobile/FaqWebViewActivity$1;
.super Landroid/webkit/WebChromeClient;
.source "FaqWebViewActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/tv/mobile/FaqWebViewActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/tv/mobile/FaqWebViewActivity;


# direct methods
.method constructor <init>(Lru/cn/tv/mobile/FaqWebViewActivity;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/tv/mobile/FaqWebViewActivity;

    .prologue
    .line 46
    iput-object p1, p0, Lru/cn/tv/mobile/FaqWebViewActivity$1;->this$0:Lru/cn/tv/mobile/FaqWebViewActivity;

    invoke-direct {p0}, Landroid/webkit/WebChromeClient;-><init>()V

    return-void
.end method


# virtual methods
.method public onJsAlert(Landroid/webkit/WebView;Ljava/lang/String;Ljava/lang/String;Landroid/webkit/JsResult;)Z
    .locals 4
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "message"    # Ljava/lang/String;
    .param p4, "result"    # Landroid/webkit/JsResult;

    .prologue
    .line 50
    new-instance v1, Landroid/app/AlertDialog$Builder;

    .line 51
    invoke-virtual {p1}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, p3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x104000a

    new-instance v3, Lru/cn/tv/mobile/FaqWebViewActivity$1$1;

    invoke-direct {v3, p0, p4}, Lru/cn/tv/mobile/FaqWebViewActivity$1$1;-><init>(Lru/cn/tv/mobile/FaqWebViewActivity$1;Landroid/webkit/JsResult;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 60
    .local v0, "b":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 61
    const/4 v1, 0x1

    return v1
.end method

.method public onJsConfirm(Landroid/webkit/WebView;Ljava/lang/String;Ljava/lang/String;Landroid/webkit/JsResult;)Z
    .locals 4
    .param p1, "view"    # Landroid/webkit/WebView;
    .param p2, "url"    # Ljava/lang/String;
    .param p3, "message"    # Ljava/lang/String;
    .param p4, "result"    # Landroid/webkit/JsResult;

    .prologue
    .line 67
    new-instance v1, Landroid/app/AlertDialog$Builder;

    .line 68
    invoke-virtual {p1}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 69
    invoke-virtual {v1, p3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x104000a

    new-instance v3, Lru/cn/tv/mobile/FaqWebViewActivity$1$3;

    invoke-direct {v3, p0, p4}, Lru/cn/tv/mobile/FaqWebViewActivity$1$3;-><init>(Lru/cn/tv/mobile/FaqWebViewActivity$1;Landroid/webkit/JsResult;)V

    .line 70
    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/high16 v2, 0x1040000

    new-instance v3, Lru/cn/tv/mobile/FaqWebViewActivity$1$2;

    invoke-direct {v3, p0, p4}, Lru/cn/tv/mobile/FaqWebViewActivity$1$2;-><init>(Lru/cn/tv/mobile/FaqWebViewActivity$1;Landroid/webkit/JsResult;)V

    .line 78
    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 86
    .local v0, "b":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 87
    const/4 v1, 0x1

    return v1
.end method
