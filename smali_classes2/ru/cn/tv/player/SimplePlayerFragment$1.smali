.class Lru/cn/tv/player/SimplePlayerFragment$1;
.super Ljava/lang/Object;
.source "SimplePlayerFragment.java"

# interfaces
.implements Lru/cn/player/SimplePlayer$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/tv/player/SimplePlayerFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/tv/player/SimplePlayerFragment;


# direct methods
.method constructor <init>(Lru/cn/tv/player/SimplePlayerFragment;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/tv/player/SimplePlayerFragment;

    .prologue
    .line 484
    iput-object p1, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public endBuffering()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 646
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-static {v0}, Lru/cn/tv/player/SimplePlayerFragment;->access$200(Lru/cn/tv/player/SimplePlayerFragment;)Landroid/widget/ProgressBar;

    move-result-object v0

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 647
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-static {v0}, Lru/cn/tv/player/SimplePlayerFragment;->access$1300(Lru/cn/tv/player/SimplePlayerFragment;)Landroid/os/Handler;

    move-result-object v0

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 649
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    iget-object v0, v0, Lru/cn/tv/player/SimplePlayerFragment;->playerView:Lru/cn/player/SimplePlayer;

    invoke-virtual {v0}, Lru/cn/player/SimplePlayer;->getState()Lru/cn/player/AbstractMediaPlayer$PlayerState;

    move-result-object v0

    sget-object v2, Lru/cn/player/AbstractMediaPlayer$PlayerState;->PLAYING:Lru/cn/player/AbstractMediaPlayer$PlayerState;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v0}, Lru/cn/domain/statistics/inetra/InetraTracker;->playbackBuffering(ZZ)V

    .line 650
    return-void

    :cond_0
    move v0, v1

    .line 649
    goto :goto_0
.end method

.method public onComplete()V
    .locals 4

    .prologue
    .line 490
    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-virtual {v1}, Lru/cn/tv/player/SimplePlayerFragment;->getDuration()I

    move-result v1

    iget-object v2, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-virtual {v2}, Lru/cn/tv/player/SimplePlayerFragment;->getCurrentPosition()I

    move-result v2

    sub-int/2addr v1, v2

    const/16 v2, 0x7530

    if-le v1, v2, :cond_0

    .line 491
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "uri="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-static {v2}, Lru/cn/tv/player/SimplePlayerFragment;->access$000(Lru/cn/tv/player/SimplePlayerFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 492
    .local v0, "message":Ljava/lang/String;
    sget-object v1, Lru/cn/domain/statistics/inetra/ErrorCode;->UNKNOWN_ERROR:Lru/cn/domain/statistics/inetra/ErrorCode;

    const-string v2, "PrematureCompletionError"

    const/16 v3, 0x44c

    invoke-static {v1, v2, v3, v0}, Lru/cn/domain/statistics/inetra/InetraTracker;->error(Lru/cn/domain/statistics/inetra/ErrorCode;Ljava/lang/String;ILjava/lang/String;)V

    .line 496
    .end local v0    # "message":Ljava/lang/String;
    :cond_0
    const/16 v1, 0x3e9

    invoke-static {v1}, Lru/cn/domain/statistics/inetra/InetraTracker;->watch(I)V

    .line 498
    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-static {v1}, Lru/cn/tv/player/SimplePlayerFragment;->access$100(Lru/cn/tv/player/SimplePlayerFragment;)Lru/cn/tv/player/controller/PlayerController;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 499
    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-static {v1}, Lru/cn/tv/player/SimplePlayerFragment;->access$100(Lru/cn/tv/player/SimplePlayerFragment;)Lru/cn/tv/player/controller/PlayerController;

    move-result-object v1

    invoke-virtual {v1}, Lru/cn/tv/player/controller/PlayerController;->contentCompleted()V

    .line 501
    :cond_1
    return-void
.end method

.method public onError(II)V
    .locals 11
    .param p1, "playerType"    # I
    .param p2, "code"    # I

    .prologue
    const/4 v10, 0x0

    const/4 v9, -0x1

    .line 510
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-virtual {v0, v10}, Lru/cn/tv/player/SimplePlayerFragment;->playing(Z)V

    .line 511
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-static {v0}, Lru/cn/tv/player/SimplePlayerFragment;->access$200(Lru/cn/tv/player/SimplePlayerFragment;)Landroid/widget/ProgressBar;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 513
    const/16 v0, 0x4b0

    invoke-static {v0}, Lru/cn/domain/statistics/inetra/InetraTracker;->watch(I)V

    .line 514
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-static {v0}, Lru/cn/tv/player/SimplePlayerFragment;->access$300(Lru/cn/tv/player/SimplePlayerFragment;)Z

    move-result v0

    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    .line 515
    invoke-static {v1}, Lru/cn/tv/player/SimplePlayerFragment;->access$400(Lru/cn/tv/player/SimplePlayerFragment;)J

    move-result-wide v3

    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    iget-object v5, v1, Lru/cn/tv/player/SimplePlayerFragment;->currentChannelTitle:Ljava/lang/String;

    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-static {v1}, Lru/cn/tv/player/SimplePlayerFragment;->access$500(Lru/cn/tv/player/SimplePlayerFragment;)J

    move-result-wide v6

    move v1, p1

    move v2, p2

    .line 514
    invoke-static/range {v0 .. v7}, Lru/cn/domain/statistics/inetra/InetraTracker;->playbackError(ZIIJLjava/lang/String;J)V

    .line 517
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-static {v0}, Lru/cn/tv/player/SimplePlayerFragment;->access$300(Lru/cn/tv/player/SimplePlayerFragment;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 519
    const/4 v0, 0x5

    if-ne p1, v0, :cond_0

    .line 520
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    iget-object v0, v0, Lru/cn/tv/player/SimplePlayerFragment;->playerView:Lru/cn/player/SimplePlayer;

    invoke-virtual {v0, v10}, Lru/cn/player/SimplePlayer;->setPlayerType(I)V

    .line 521
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-static {v0}, Lru/cn/tv/player/SimplePlayerFragment;->access$600(Lru/cn/tv/player/SimplePlayerFragment;)V

    .line 555
    :goto_0
    return-void

    .line 523
    :cond_0
    if-nez p1, :cond_1

    .line 524
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    iget-object v0, v0, Lru/cn/tv/player/SimplePlayerFragment;->playerView:Lru/cn/player/SimplePlayer;

    invoke-virtual {v0, v9}, Lru/cn/player/SimplePlayer;->setPlayerType(I)V

    .line 528
    :cond_1
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-static {v0}, Lru/cn/tv/player/SimplePlayerFragment;->access$700(Lru/cn/tv/player/SimplePlayerFragment;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 529
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-static {v0}, Lru/cn/tv/player/SimplePlayerFragment;->access$800(Lru/cn/tv/player/SimplePlayerFragment;)Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_2

    .line 530
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-static {v0}, Lru/cn/tv/player/SimplePlayerFragment;->access$900(Lru/cn/tv/player/SimplePlayerFragment;)V

    goto :goto_0

    .line 532
    :cond_2
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-static {v0}, Lru/cn/tv/player/SimplePlayerFragment;->access$1000(Lru/cn/tv/player/SimplePlayerFragment;)I

    move-result v0

    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-static {v1}, Lru/cn/tv/player/SimplePlayerFragment;->access$800(Lru/cn/tv/player/SimplePlayerFragment;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_3

    .line 533
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-static {v0}, Lru/cn/tv/player/SimplePlayerFragment;->access$1004(Lru/cn/tv/player/SimplePlayerFragment;)I

    .line 535
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-static {v0}, Lru/cn/tv/player/SimplePlayerFragment;->access$800(Lru/cn/tv/player/SimplePlayerFragment;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-static {v1}, Lru/cn/tv/player/SimplePlayerFragment;->access$1000(Lru/cn/tv/player/SimplePlayerFragment;)I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lru/cn/api/iptv/replies/MediaLocation;

    .line 536
    .local v8, "nextLocation":Lru/cn/api/iptv/replies/MediaLocation;
    iget-object v0, v8, Lru/cn/api/iptv/replies/MediaLocation;->access:Lru/cn/api/iptv/replies/MediaLocation$Access;

    sget-object v1, Lru/cn/api/iptv/replies/MediaLocation$Access;->denied:Lru/cn/api/iptv/replies/MediaLocation$Access;

    if-eq v0, v1, :cond_3

    .line 537
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-static {v0}, Lru/cn/tv/player/SimplePlayerFragment;->access$900(Lru/cn/tv/player/SimplePlayerFragment;)V

    goto :goto_0

    .line 544
    .end local v8    # "nextLocation":Lru/cn/api/iptv/replies/MediaLocation;
    :cond_3
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-virtual {v0}, Lru/cn/tv/player/SimplePlayerFragment;->stop()V

    .line 545
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-static {v0, p2, p1}, Lru/cn/tv/player/SimplePlayerFragment;->access$1100(Lru/cn/tv/player/SimplePlayerFragment;II)V

    .line 547
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-static {v0}, Lru/cn/tv/player/SimplePlayerFragment;->access$1200(Lru/cn/tv/player/SimplePlayerFragment;)Lru/cn/domain/statistics/inetra/TimeMeasure;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 548
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-static {v0}, Lru/cn/tv/player/SimplePlayerFragment;->access$1200(Lru/cn/tv/player/SimplePlayerFragment;)Lru/cn/domain/statistics/inetra/TimeMeasure;

    move-result-object v0

    invoke-virtual {v0}, Lru/cn/domain/statistics/inetra/TimeMeasure;->measureEnd()V

    .line 549
    sget-object v0, Lru/cn/domain/statistics/inetra/TypeOfMeasure;->TIME_OF_CONTENT_LOADING:Lru/cn/domain/statistics/inetra/TypeOfMeasure;

    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    .line 550
    invoke-static {v1}, Lru/cn/tv/player/SimplePlayerFragment;->access$1200(Lru/cn/tv/player/SimplePlayerFragment;)Lru/cn/domain/statistics/inetra/TimeMeasure;

    move-result-object v1

    .line 549
    invoke-static {v0, v1}, Lru/cn/domain/statistics/inetra/InetraTracker;->timeMeasure(Lru/cn/domain/statistics/inetra/TypeOfMeasure;Lru/cn/domain/statistics/inetra/TimeMeasure;)V

    .line 551
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lru/cn/tv/player/SimplePlayerFragment;->access$1202(Lru/cn/tv/player/SimplePlayerFragment;Lru/cn/domain/statistics/inetra/TimeMeasure;)Lru/cn/domain/statistics/inetra/TimeMeasure;

    .line 554
    :cond_4
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    iget-object v0, v0, Lru/cn/tv/player/SimplePlayerFragment;->playerView:Lru/cn/player/SimplePlayer;

    invoke-virtual {v0, v9}, Lru/cn/player/SimplePlayer;->setPlayerType(I)V

    goto/16 :goto_0
.end method

.method public onMetadata(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lru/cn/player/metadata/MetadataItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 505
    .local p1, "items":Ljava/util/List;, "Ljava/util/List<Lru/cn/player/metadata/MetadataItem;>;"
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    iget-object v0, v0, Lru/cn/tv/player/SimplePlayerFragment;->adController:Lru/cn/ad/AdPlayController;

    invoke-virtual {v0, p1}, Lru/cn/ad/AdPlayController;->setMetadata(Ljava/util/List;)V

    .line 506
    return-void
.end method

.method public qualityChanged(I)V
    .locals 0
    .param p1, "bitrate"    # I

    .prologue
    .line 656
    invoke-static {p1}, Lru/cn/domain/statistics/inetra/InetraTracker;->qualityChanged(I)V

    .line 657
    return-void
.end method

.method public startBuffering()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 639
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-static {v0}, Lru/cn/tv/player/SimplePlayerFragment;->access$1300(Lru/cn/tv/player/SimplePlayerFragment;)Landroid/os/Handler;

    move-result-object v0

    const/4 v2, 0x2

    const-wide/16 v4, 0x1f4

    invoke-virtual {v0, v2, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 641
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    iget-object v0, v0, Lru/cn/tv/player/SimplePlayerFragment;->playerView:Lru/cn/player/SimplePlayer;

    invoke-virtual {v0}, Lru/cn/player/SimplePlayer;->getState()Lru/cn/player/AbstractMediaPlayer$PlayerState;

    move-result-object v0

    sget-object v2, Lru/cn/player/AbstractMediaPlayer$PlayerState;->PLAYING:Lru/cn/player/AbstractMediaPlayer$PlayerState;

    if-ne v0, v2, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v1, v0}, Lru/cn/domain/statistics/inetra/InetraTracker;->playbackBuffering(ZZ)V

    .line 642
    return-void

    .line 641
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public stateChanged(Lru/cn/player/AbstractMediaPlayer$PlayerState;)V
    .locals 6
    .param p1, "state"    # Lru/cn/player/AbstractMediaPlayer$PlayerState;

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x0

    const/4 v4, 0x1

    .line 559
    sget-object v0, Lru/cn/tv/player/SimplePlayerFragment$7;->$SwitchMap$ru$cn$player$AbstractMediaPlayer$PlayerState:[I

    invoke-virtual {p1}, Lru/cn/player/AbstractMediaPlayer$PlayerState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 621
    :cond_0
    :goto_0
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-static {v0}, Lru/cn/tv/player/SimplePlayerFragment;->access$100(Lru/cn/tv/player/SimplePlayerFragment;)Lru/cn/tv/player/controller/PlayerController;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 622
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-static {v0}, Lru/cn/tv/player/SimplePlayerFragment;->access$100(Lru/cn/tv/player/SimplePlayerFragment;)Lru/cn/tv/player/controller/PlayerController;

    move-result-object v0

    invoke-virtual {v0}, Lru/cn/tv/player/controller/PlayerController;->updateControls()V

    .line 625
    :cond_1
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-static {v0}, Lru/cn/tv/player/SimplePlayerFragment;->access$1600(Lru/cn/tv/player/SimplePlayerFragment;)Lru/cn/peersay/controllers/PlayerRemoteController;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 626
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-static {v0}, Lru/cn/tv/player/SimplePlayerFragment;->access$1600(Lru/cn/tv/player/SimplePlayerFragment;)Lru/cn/peersay/controllers/PlayerRemoteController;

    move-result-object v0

    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-static {v1}, Lru/cn/tv/player/SimplePlayerFragment;->access$1500(Lru/cn/tv/player/SimplePlayerFragment;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lru/cn/peersay/controllers/PlayerRemoteController;->onStateChanged(Landroid/content/Context;Lru/cn/player/AbstractMediaPlayer$PlayerState;)V

    .line 628
    :cond_2
    return-void

    .line 561
    :pswitch_0
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-static {v0}, Lru/cn/tv/player/SimplePlayerFragment;->access$1300(Lru/cn/tv/player/SimplePlayerFragment;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 562
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    iget-object v0, v0, Lru/cn/tv/player/SimplePlayerFragment;->adController:Lru/cn/ad/AdPlayController;

    invoke-virtual {v0, v5}, Lru/cn/ad/AdPlayController;->setContentProgress(Lru/cn/ad/ContentProgress;)V

    .line 564
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-static {v0}, Lru/cn/tv/player/SimplePlayerFragment;->access$100(Lru/cn/tv/player/SimplePlayerFragment;)Lru/cn/tv/player/controller/PlayerController;

    move-result-object v0

    invoke-virtual {v0, v4}, Lru/cn/tv/player/controller/PlayerController;->setAlwaysShow(Z)V

    .line 565
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-virtual {v0}, Lru/cn/tv/player/SimplePlayerFragment;->showPlayerControlIfMaximize()V

    .line 567
    const/16 v0, 0x3e8

    invoke-static {v0}, Lru/cn/domain/statistics/inetra/InetraTracker;->watch(I)V

    .line 569
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-virtual {v0, v2}, Lru/cn/tv/player/SimplePlayerFragment;->playing(Z)V

    goto :goto_0

    .line 573
    :pswitch_1
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-static {v0}, Lru/cn/tv/player/SimplePlayerFragment;->access$100(Lru/cn/tv/player/SimplePlayerFragment;)Lru/cn/tv/player/controller/PlayerController;

    move-result-object v0

    invoke-virtual {v0, v4}, Lru/cn/tv/player/controller/PlayerController;->setAlwaysShow(Z)V

    .line 574
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-static {v0}, Lru/cn/tv/player/SimplePlayerFragment;->access$100(Lru/cn/tv/player/SimplePlayerFragment;)Lru/cn/tv/player/controller/PlayerController;

    move-result-object v0

    invoke-virtual {v0}, Lru/cn/tv/player/controller/PlayerController;->show()V

    .line 576
    const/16 v0, 0x44c

    invoke-static {v0}, Lru/cn/domain/statistics/inetra/InetraTracker;->watch(I)V

    .line 578
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-virtual {v0, v2}, Lru/cn/tv/player/SimplePlayerFragment;->playing(Z)V

    goto :goto_0

    .line 582
    :pswitch_2
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-static {v0}, Lru/cn/tv/player/SimplePlayerFragment;->access$200(Lru/cn/tv/player/SimplePlayerFragment;)Landroid/widget/ProgressBar;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    .line 586
    :pswitch_3
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-static {v0}, Lru/cn/tv/player/SimplePlayerFragment;->access$100(Lru/cn/tv/player/SimplePlayerFragment;)Lru/cn/tv/player/controller/PlayerController;

    move-result-object v0

    invoke-virtual {v0, v2}, Lru/cn/tv/player/controller/PlayerController;->setAlwaysShow(Z)V

    .line 587
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-static {v0}, Lru/cn/tv/player/SimplePlayerFragment;->access$200(Lru/cn/tv/player/SimplePlayerFragment;)Landroid/widget/ProgressBar;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto/16 :goto_0

    .line 591
    :pswitch_4
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-static {v0}, Lru/cn/tv/player/SimplePlayerFragment;->access$100(Lru/cn/tv/player/SimplePlayerFragment;)Lru/cn/tv/player/controller/PlayerController;

    move-result-object v0

    invoke-virtual {v0, v2}, Lru/cn/tv/player/controller/PlayerController;->setAlwaysShow(Z)V

    .line 592
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    iget-object v0, v0, Lru/cn/tv/player/SimplePlayerFragment;->adController:Lru/cn/ad/AdPlayController;

    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-virtual {v0, v1}, Lru/cn/ad/AdPlayController;->setContentProgress(Lru/cn/ad/ContentProgress;)V

    .line 594
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-static {v0}, Lru/cn/tv/player/SimplePlayerFragment;->access$1400(Lru/cn/tv/player/SimplePlayerFragment;)I

    move-result v0

    if-ne v0, v4, :cond_4

    .line 595
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-static {v0}, Lru/cn/tv/player/SimplePlayerFragment;->access$1500(Lru/cn/tv/player/SimplePlayerFragment;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-static {v1}, Lru/cn/tv/player/SimplePlayerFragment;->access$500(Lru/cn/tv/player/SimplePlayerFragment;)J

    move-result-wide v2

    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    iget-object v1, v1, Lru/cn/tv/player/SimplePlayerFragment;->playerView:Lru/cn/player/SimplePlayer;

    invoke-virtual {v1}, Lru/cn/player/SimplePlayer;->getPlayerType()I

    move-result v1

    invoke-static {v0, v2, v3, v1}, Lru/cn/domain/statistics/inetra/InetraTracker;->telecastStart(Landroid/content/Context;JI)V

    .line 601
    :goto_1
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-static {v0}, Lru/cn/tv/player/SimplePlayerFragment;->access$1200(Lru/cn/tv/player/SimplePlayerFragment;)Lru/cn/domain/statistics/inetra/TimeMeasure;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 602
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-static {v0}, Lru/cn/tv/player/SimplePlayerFragment;->access$1200(Lru/cn/tv/player/SimplePlayerFragment;)Lru/cn/domain/statistics/inetra/TimeMeasure;

    move-result-object v0

    invoke-virtual {v0}, Lru/cn/domain/statistics/inetra/TimeMeasure;->measureEnd()V

    .line 603
    sget-object v0, Lru/cn/domain/statistics/inetra/TypeOfMeasure;->TIME_OF_CONTENT_LOADING:Lru/cn/domain/statistics/inetra/TypeOfMeasure;

    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-static {v1}, Lru/cn/tv/player/SimplePlayerFragment;->access$1200(Lru/cn/tv/player/SimplePlayerFragment;)Lru/cn/domain/statistics/inetra/TimeMeasure;

    move-result-object v1

    invoke-static {v0, v1}, Lru/cn/domain/statistics/inetra/InetraTracker;->timeMeasure(Lru/cn/domain/statistics/inetra/TypeOfMeasure;Lru/cn/domain/statistics/inetra/TimeMeasure;)V

    .line 604
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-static {v0, v5}, Lru/cn/tv/player/SimplePlayerFragment;->access$1202(Lru/cn/tv/player/SimplePlayerFragment;Lru/cn/domain/statistics/inetra/TimeMeasure;)Lru/cn/domain/statistics/inetra/TimeMeasure;

    .line 607
    :cond_3
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-static {v0, v4}, Lru/cn/tv/player/SimplePlayerFragment;->access$302(Lru/cn/tv/player/SimplePlayerFragment;Z)Z

    .line 608
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-virtual {v0, v4}, Lru/cn/tv/player/SimplePlayerFragment;->playing(Z)V

    .line 611
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    iget-object v0, v0, Lru/cn/tv/player/SimplePlayerFragment;->playerView:Lru/cn/player/SimplePlayer;

    invoke-virtual {v0}, Lru/cn/player/SimplePlayer;->getPlayerType()I

    move-result v0

    const/16 v1, 0x69

    if-ne v0, v1, :cond_0

    .line 612
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-static {v0}, Lru/cn/tv/player/SimplePlayerFragment;->access$100(Lru/cn/tv/player/SimplePlayerFragment;)Lru/cn/tv/player/controller/PlayerController;

    move-result-object v0

    invoke-virtual {v0, v4}, Lru/cn/tv/player/controller/PlayerController;->setAlwaysShow(Z)V

    .line 613
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-virtual {v0}, Lru/cn/tv/player/SimplePlayerFragment;->showPlayerControlIfMaximize()V

    goto/16 :goto_0

    .line 597
    :cond_4
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-static {v0}, Lru/cn/tv/player/SimplePlayerFragment;->access$400(Lru/cn/tv/player/SimplePlayerFragment;)J

    move-result-wide v0

    iget-object v2, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    iget-object v2, v2, Lru/cn/tv/player/SimplePlayerFragment;->currentChannelTitle:Ljava/lang/String;

    iget-object v3, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    iget-object v3, v3, Lru/cn/tv/player/SimplePlayerFragment;->playerView:Lru/cn/player/SimplePlayer;

    .line 598
    invoke-virtual {v3}, Lru/cn/player/SimplePlayer;->getPlayerType()I

    move-result v3

    .line 597
    invoke-static {v0, v1, v2, v3}, Lru/cn/domain/statistics/inetra/InetraTracker;->channelStart(JLjava/lang/String;I)V

    goto :goto_1

    .line 559
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public videoSizeChanged(II)V
    .locals 1
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    .line 632
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    iget-object v0, v0, Lru/cn/tv/player/SimplePlayerFragment;->listener:Lru/cn/tv/player/SimplePlayerFragment$SimplePlayerFragmentListener;

    if-eqz v0, :cond_0

    .line 633
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment$1;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    iget-object v0, v0, Lru/cn/tv/player/SimplePlayerFragment;->listener:Lru/cn/tv/player/SimplePlayerFragment$SimplePlayerFragmentListener;

    invoke-interface {v0, p1, p2}, Lru/cn/tv/player/SimplePlayerFragment$SimplePlayerFragmentListener;->videoSizeChanged(II)V

    .line 635
    :cond_0
    return-void
.end method
