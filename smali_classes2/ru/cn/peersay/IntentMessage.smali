.class public Lru/cn/peersay/IntentMessage;
.super Ljava/lang/Object;
.source "IntentMessage.java"


# instance fields
.field public final action:Ljava/lang/String;

.field public final extras:Landroid/os/Bundle;

.field public final requestId:J

.field private resultExtras:Landroid/os/Bundle;


# direct methods
.method private constructor <init>(Ljava/lang/String;JLandroid/os/Bundle;)V
    .locals 0
    .param p1, "action"    # Ljava/lang/String;
    .param p2, "requestId"    # J
    .param p4, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lru/cn/peersay/IntentMessage;->action:Ljava/lang/String;

    .line 27
    iput-wide p2, p0, Lru/cn/peersay/IntentMessage;->requestId:J

    .line 28
    iput-object p4, p0, Lru/cn/peersay/IntentMessage;->extras:Landroid/os/Bundle;

    .line 29
    return-void
.end method

.method public static createEventIntent(Landroid/os/Bundle;)Landroid/content/Intent;
    .locals 2
    .param p0, "info"    # Landroid/os/Bundle;

    .prologue
    .line 72
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.inetra.EVENT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 73
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "application/inetra.peerstv"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 74
    invoke-virtual {v0, p0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 76
    return-object v0
.end method

.method static createFromIntent(Landroid/content/Intent;)Lru/cn/peersay/IntentMessage;
    .locals 8
    .param p0, "intent"    # Landroid/content/Intent;

    .prologue
    const-wide/16 v6, 0x0

    const/4 v1, 0x0

    .line 36
    const-string v4, "requestId"

    invoke-virtual {p0, v4, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    .line 37
    .local v2, "requestId":J
    cmp-long v4, v2, v6

    if-nez v4, :cond_1

    .line 44
    :cond_0
    :goto_0
    return-object v1

    .line 40
    :cond_1
    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 41
    .local v0, "action":Ljava/lang/String;
    const-string v4, "com.inetra.COMMAND"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 44
    new-instance v1, Lru/cn/peersay/IntentMessage;

    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    invoke-direct {v1, v0, v2, v3, v4}, Lru/cn/peersay/IntentMessage;-><init>(Ljava/lang/String;JLandroid/os/Bundle;)V

    goto :goto_0
.end method


# virtual methods
.method createResultIntent(Z)Landroid/content/Intent;
    .locals 8
    .param p1, "handled"    # Z

    .prologue
    .line 48
    new-instance v1, Landroid/content/Intent;

    const-string v3, "com.inetra.RESULT"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 49
    .local v1, "result":Landroid/content/Intent;
    const-string v3, "application/inetra.peerstv"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 51
    iget-object v3, p0, Lru/cn/peersay/IntentMessage;->resultExtras:Landroid/os/Bundle;

    if-nez v3, :cond_0

    .line 52
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    iput-object v3, p0, Lru/cn/peersay/IntentMessage;->resultExtras:Landroid/os/Bundle;

    .line 55
    :cond_0
    iget-object v3, p0, Lru/cn/peersay/IntentMessage;->resultExtras:Landroid/os/Bundle;

    const-string v4, "requestId"

    iget-wide v6, p0, Lru/cn/peersay/IntentMessage;->requestId:J

    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 57
    iget-object v3, p0, Lru/cn/peersay/IntentMessage;->extras:Landroid/os/Bundle;

    if-eqz v3, :cond_1

    .line 58
    iget-object v3, p0, Lru/cn/peersay/IntentMessage;->extras:Landroid/os/Bundle;

    const-string v4, "command"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 59
    .local v0, "command":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 60
    iget-object v3, p0, Lru/cn/peersay/IntentMessage;->resultExtras:Landroid/os/Bundle;

    const-string v4, "command"

    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    .end local v0    # "command":Ljava/lang/String;
    :cond_1
    if-eqz p1, :cond_2

    const-string v2, "completed"

    .line 65
    .local v2, "resultStatus":Ljava/lang/String;
    :goto_0
    iget-object v3, p0, Lru/cn/peersay/IntentMessage;->resultExtras:Landroid/os/Bundle;

    const-string v4, "result"

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    iget-object v3, p0, Lru/cn/peersay/IntentMessage;->resultExtras:Landroid/os/Bundle;

    invoke-virtual {v1, v3}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 68
    return-object v1

    .line 64
    .end local v2    # "resultStatus":Ljava/lang/String;
    :cond_2
    const-string v2, "failed"

    goto :goto_0
.end method

.method public setResult(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 32
    iput-object p1, p0, Lru/cn/peersay/IntentMessage;->resultExtras:Landroid/os/Bundle;

    .line 33
    return-void
.end method
