.class Lcom/yandex/metrica/impl/ob/eg;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:J

.field static final b:J


# instance fields
.field private c:Landroid/content/Context;

.field private d:Landroid/os/Looper;

.field private e:Landroid/location/LocationManager;

.field private f:Landroid/location/LocationListener;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 30
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/yandex/metrica/impl/ob/eg;->a:J

    .line 31
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/yandex/metrica/impl/ob/eg;->b:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Landroid/location/LocationManager;Landroid/location/LocationListener;)V
    .locals 2

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/yandex/metrica/impl/ob/eg;->c:Landroid/content/Context;

    .line 49
    iput-object p2, p0, Lcom/yandex/metrica/impl/ob/eg;->d:Landroid/os/Looper;

    .line 50
    iput-object p3, p0, Lcom/yandex/metrica/impl/ob/eg;->e:Landroid/location/LocationManager;

    .line 51
    iput-object p4, p0, Lcom/yandex/metrica/impl/ob/eg;->f:Landroid/location/LocationListener;

    .line 52
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/eg;->d:Landroid/os/Looper;

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 53
    return-void
.end method

.method private a(Ljava/lang/String;FJLandroid/location/LocationListener;Landroid/os/Looper;)V
    .locals 7

    .prologue
    .line 98
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/eg;->e:Landroid/location/LocationManager;

    if-eqz v0, :cond_0

    .line 100
    :try_start_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/eg;->e:Landroid/location/LocationManager;

    move-object v1, p1

    move-wide v2, p3

    move v4, p2

    move-object v5, p5

    move-object v6, p6

    invoke-virtual/range {v0 .. v6}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;Landroid/os/Looper;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 106
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 57
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/eg;->c:Landroid/content/Context;

    .line 1035
    const-string v1, "android.permission.ACCESS_COARSE_LOCATION"

    invoke-static {v0, v1}, Lcom/yandex/metrica/impl/am;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    .line 57
    if-eqz v0, :cond_0

    .line 58
    const-string v2, "network"

    sget-wide v4, Lcom/yandex/metrica/impl/ob/eg;->b:J

    iget-object v6, p0, Lcom/yandex/metrica/impl/ob/eg;->f:Landroid/location/LocationListener;

    iget-object v7, p0, Lcom/yandex/metrica/impl/ob/eg;->d:Landroid/os/Looper;

    move-object v1, p0

    invoke-direct/range {v1 .. v7}, Lcom/yandex/metrica/impl/ob/eg;->a(Ljava/lang/String;FJLandroid/location/LocationListener;Landroid/os/Looper;)V

    .line 65
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/eg;->c:Landroid/content/Context;

    .line 1039
    const-string v1, "android.permission.ACCESS_FINE_LOCATION"

    invoke-static {v0, v1}, Lcom/yandex/metrica/impl/am;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    .line 65
    if-eqz v0, :cond_0

    .line 66
    const-string v2, "passive"

    sget-wide v4, Lcom/yandex/metrica/impl/ob/eg;->a:J

    iget-object v6, p0, Lcom/yandex/metrica/impl/ob/eg;->f:Landroid/location/LocationListener;

    iget-object v7, p0, Lcom/yandex/metrica/impl/ob/eg;->d:Landroid/os/Looper;

    move-object v1, p0

    invoke-direct/range {v1 .. v7}, Lcom/yandex/metrica/impl/ob/eg;->a(Ljava/lang/String;FJLandroid/location/LocationListener;Landroid/os/Looper;)V

    .line 76
    :cond_0
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 81
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/eg;->e:Landroid/location/LocationManager;

    if-eqz v0, :cond_0

    .line 83
    :try_start_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/eg;->e:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/eg;->f:Landroid/location/LocationListener;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 88
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method
