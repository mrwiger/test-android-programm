.class Lru/cn/ad/video/ima/IMARenderingSettings;
.super Ljava/lang/Object;
.source "IMARenderingSettings.java"

# interfaces
.implements Lcom/google/ads/interactivemedia/v3/api/AdsRenderingSettings;


# instance fields
.field private bitrate:I

.field private disableUi:Z

.field private isRenderCompanions:Z

.field private mimeTypes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    const/4 v0, -0x1

    iput v0, p0, Lru/cn/ad/video/ima/IMARenderingSettings;->bitrate:I

    return-void
.end method


# virtual methods
.method public getDisableUi()Z
    .locals 1

    .prologue
    .line 75
    iget-boolean v0, p0, Lru/cn/ad/video/ima/IMARenderingSettings;->disableUi:Z

    return v0
.end method

.method public isRenderCompanions()Z
    .locals 1

    .prologue
    .line 55
    iget-boolean v0, p0, Lru/cn/ad/video/ima/IMARenderingSettings;->isRenderCompanions:Z

    return v0
.end method

.method public setBitrateKbps(I)V
    .locals 0
    .param p1, "bitrateKbps"    # I

    .prologue
    .line 25
    iput p1, p0, Lru/cn/ad/video/ima/IMARenderingSettings;->bitrate:I

    .line 26
    return-void
.end method

.method public setMimeTypes(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 35
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iput-object p1, p0, Lru/cn/ad/video/ima/IMARenderingSettings;->mimeTypes:Ljava/util/List;

    .line 36
    return-void
.end method
