.class final Lcom/google/obf/fj$b;
.super Ljava/util/AbstractSet;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/obf/fj;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/AbstractSet",
        "<TK;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/obf/fj;


# direct methods
.method constructor <init>(Lcom/google/obf/fj;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/google/obf/fj$b;->a:Lcom/google/obf/fj;

    invoke-direct {p0}, Ljava/util/AbstractSet;-><init>()V

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 6
    iget-object v0, p0, Lcom/google/obf/fj$b;->a:Lcom/google/obf/fj;

    invoke-virtual {v0}, Lcom/google/obf/fj;->clear()V

    .line 7
    return-void
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 4
    iget-object v0, p0, Lcom/google/obf/fj$b;->a:Lcom/google/obf/fj;

    invoke-virtual {v0, p1}, Lcom/google/obf/fj;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 3
    new-instance v0, Lcom/google/obf/fj$b$1;

    invoke-direct {v0, p0}, Lcom/google/obf/fj$b$1;-><init>(Lcom/google/obf/fj$b;)V

    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/obf/fj$b;->a:Lcom/google/obf/fj;

    invoke-virtual {v0, p1}, Lcom/google/obf/fj;->b(Ljava/lang/Object;)Lcom/google/obf/fj$d;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 2
    iget-object v0, p0, Lcom/google/obf/fj$b;->a:Lcom/google/obf/fj;

    iget v0, v0, Lcom/google/obf/fj;->c:I

    return v0
.end method
