.class Lru/inetra/bytefog/service/ServiceProxy$2;
.super Ljava/lang/Object;
.source "ServiceProxy.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/inetra/bytefog/service/ServiceProxy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/inetra/bytefog/service/ServiceProxy;


# direct methods
.method constructor <init>(Lru/inetra/bytefog/service/ServiceProxy;)V
    .locals 0
    .param p1, "this$0"    # Lru/inetra/bytefog/service/ServiceProxy;

    .prologue
    .line 108
    iput-object p1, p0, Lru/inetra/bytefog/service/ServiceProxy$2;->this$0:Lru/inetra/bytefog/service/ServiceProxy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 4
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 112
    const-string v1, "ServiceProxy"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Bytefog service is connected ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lru/inetra/bytefog/service/ServiceProxy$2;->this$0:Lru/inetra/bytefog/service/ServiceProxy;

    invoke-static {v3}, Lru/inetra/bytefog/service/ServiceProxy;->access$000(Lru/inetra/bytefog/service/ServiceProxy;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lru/inetra/bytefog/service/ProcessUtils;->myProcessName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    iget-object v1, p0, Lru/inetra/bytefog/service/ServiceProxy$2;->this$0:Lru/inetra/bytefog/service/ServiceProxy;

    invoke-static {p2}, Lru/inetra/bytefog/service/IService$Stub;->asInterface(Landroid/os/IBinder;)Lru/inetra/bytefog/service/IService;

    move-result-object v2

    invoke-static {v1, v2}, Lru/inetra/bytefog/service/ServiceProxy;->access$102(Lru/inetra/bytefog/service/ServiceProxy;Lru/inetra/bytefog/service/IService;)Lru/inetra/bytefog/service/IService;

    .line 114
    iget-object v1, p0, Lru/inetra/bytefog/service/ServiceProxy$2;->this$0:Lru/inetra/bytefog/service/ServiceProxy;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lru/inetra/bytefog/service/ServiceProxy;->access$202(Lru/inetra/bytefog/service/ServiceProxy;Z)Z

    .line 116
    :try_start_0
    iget-object v1, p0, Lru/inetra/bytefog/service/ServiceProxy$2;->this$0:Lru/inetra/bytefog/service/ServiceProxy;

    invoke-static {v1}, Lru/inetra/bytefog/service/ServiceProxy;->access$100(Lru/inetra/bytefog/service/ServiceProxy;)Lru/inetra/bytefog/service/IService;

    move-result-object v1

    iget-object v2, p0, Lru/inetra/bytefog/service/ServiceProxy$2;->this$0:Lru/inetra/bytefog/service/ServiceProxy;

    iget-object v2, v2, Lru/inetra/bytefog/service/ServiceProxy;->serviceListener:Lru/inetra/bytefog/service/IServiceListener$Stub;

    invoke-interface {v1, v2}, Lru/inetra/bytefog/service/IService;->setListener(Lru/inetra/bytefog/service/IServiceListener;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 120
    :goto_0
    iget-object v1, p0, Lru/inetra/bytefog/service/ServiceProxy$2;->this$0:Lru/inetra/bytefog/service/ServiceProxy;

    invoke-static {v1}, Lru/inetra/bytefog/service/ServiceProxy;->access$300(Lru/inetra/bytefog/service/ServiceProxy;)Lru/inetra/bytefog/service/InstanceListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 121
    iget-object v1, p0, Lru/inetra/bytefog/service/ServiceProxy$2;->this$0:Lru/inetra/bytefog/service/ServiceProxy;

    invoke-static {v1}, Lru/inetra/bytefog/service/ServiceProxy;->access$300(Lru/inetra/bytefog/service/ServiceProxy;)Lru/inetra/bytefog/service/InstanceListener;

    move-result-object v1

    invoke-interface {v1}, Lru/inetra/bytefog/service/InstanceListener;->onBytefogServiceReady()V

    .line 123
    :cond_0
    return-void

    .line 117
    :catch_0
    move-exception v0

    .line 118
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "ServiceProxy"

    const-string v2, "RemoteException in BytefogService.setListener()"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 4
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 128
    const-string v1, "ServiceProxy"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Bytefog service is disconnected ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lru/inetra/bytefog/service/ServiceProxy$2;->this$0:Lru/inetra/bytefog/service/ServiceProxy;

    invoke-static {v3}, Lru/inetra/bytefog/service/ServiceProxy;->access$000(Lru/inetra/bytefog/service/ServiceProxy;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lru/inetra/bytefog/service/ProcessUtils;->myProcessName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    iget-object v1, p0, Lru/inetra/bytefog/service/ServiceProxy$2;->this$0:Lru/inetra/bytefog/service/ServiceProxy;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lru/inetra/bytefog/service/ServiceProxy;->access$102(Lru/inetra/bytefog/service/ServiceProxy;Lru/inetra/bytefog/service/IService;)Lru/inetra/bytefog/service/IService;

    .line 130
    iget-object v1, p0, Lru/inetra/bytefog/service/ServiceProxy$2;->this$0:Lru/inetra/bytefog/service/ServiceProxy;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lru/inetra/bytefog/service/ServiceProxy;->access$202(Lru/inetra/bytefog/service/ServiceProxy;Z)Z

    .line 131
    iget-object v1, p0, Lru/inetra/bytefog/service/ServiceProxy$2;->this$0:Lru/inetra/bytefog/service/ServiceProxy;

    invoke-virtual {v1}, Lru/inetra/bytefog/service/ServiceProxy;->scheduleCrashDumpsUpload()V

    .line 132
    const/4 v0, 0x1

    .line 133
    .local v0, "restart":Z
    iget-object v1, p0, Lru/inetra/bytefog/service/ServiceProxy$2;->this$0:Lru/inetra/bytefog/service/ServiceProxy;

    invoke-static {v1}, Lru/inetra/bytefog/service/ServiceProxy;->access$300(Lru/inetra/bytefog/service/ServiceProxy;)Lru/inetra/bytefog/service/InstanceListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 134
    iget-object v1, p0, Lru/inetra/bytefog/service/ServiceProxy$2;->this$0:Lru/inetra/bytefog/service/ServiceProxy;

    invoke-static {v1}, Lru/inetra/bytefog/service/ServiceProxy;->access$300(Lru/inetra/bytefog/service/ServiceProxy;)Lru/inetra/bytefog/service/InstanceListener;

    move-result-object v1

    invoke-interface {v1}, Lru/inetra/bytefog/service/InstanceListener;->onBytefogServiceDown()Z

    move-result v0

    .line 135
    :cond_0
    if-eqz v0, :cond_1

    .line 136
    iget-object v1, p0, Lru/inetra/bytefog/service/ServiceProxy$2;->this$0:Lru/inetra/bytefog/service/ServiceProxy;

    invoke-static {v1}, Lru/inetra/bytefog/service/ServiceProxy;->access$400(Lru/inetra/bytefog/service/ServiceProxy;)V

    .line 137
    :cond_1
    return-void
.end method
