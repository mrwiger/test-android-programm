.class public Lcom/samsung/multiscreen/ble/adparser/TypeUnknown;
.super Lcom/samsung/multiscreen/ble/adparser/AdElement;
.source "TypeUnknown.java"


# instance fields
.field unknownData:[B

.field unknownType:I


# direct methods
.method public constructor <init>(I[BII)V
    .locals 2
    .param p1, "type"    # I
    .param p2, "data"    # [B
    .param p3, "pos"    # I
    .param p4, "len"    # I

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/samsung/multiscreen/ble/adparser/AdElement;-><init>()V

    .line 27
    iput p1, p0, Lcom/samsung/multiscreen/ble/adparser/TypeUnknown;->unknownType:I

    .line 28
    new-array v0, p4, [B

    iput-object v0, p0, Lcom/samsung/multiscreen/ble/adparser/TypeUnknown;->unknownData:[B

    .line 29
    iget-object v0, p0, Lcom/samsung/multiscreen/ble/adparser/TypeUnknown;->unknownData:[B

    const/4 v1, 0x0

    invoke-static {p2, p3, v0, v1, p4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 30
    return-void
.end method


# virtual methods
.method public getBytes()[B
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/samsung/multiscreen/ble/adparser/TypeUnknown;->unknownData:[B

    return-object v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 45
    iget v0, p0, Lcom/samsung/multiscreen/ble/adparser/TypeUnknown;->unknownType:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 33
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 34
    .local v1, "sb":Ljava/lang/StringBuffer;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "unknown type: 0x"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/samsung/multiscreen/ble/adparser/TypeUnknown;->unknownType:I

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 35
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v3, p0, Lcom/samsung/multiscreen/ble/adparser/TypeUnknown;->unknownData:[B

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 36
    if-lez v0, :cond_0

    .line 37
    const-string v3, ","

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 38
    :cond_0
    iget-object v3, p0, Lcom/samsung/multiscreen/ble/adparser/TypeUnknown;->unknownData:[B

    aget-byte v3, v3, v0

    and-int/lit16 v2, v3, 0xff

    .line 39
    .local v2, "v":I
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "0x"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 35
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 41
    .end local v2    # "v":I
    :cond_1
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v1}, Ljava/lang/String;-><init>(Ljava/lang/StringBuffer;)V

    return-object v3
.end method
