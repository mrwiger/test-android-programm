.class Lru/cn/tv/player/SimplePlayerFragment$4;
.super Lcom/google/android/gms/cast/Cast$Listener;
.source "SimplePlayerFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/tv/player/SimplePlayerFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/tv/player/SimplePlayerFragment;


# direct methods
.method constructor <init>(Lru/cn/tv/player/SimplePlayerFragment;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/tv/player/SimplePlayerFragment;

    .prologue
    .line 1186
    iput-object p1, p0, Lru/cn/tv/player/SimplePlayerFragment$4;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-direct {p0}, Lcom/google/android/gms/cast/Cast$Listener;-><init>()V

    return-void
.end method


# virtual methods
.method public onApplicationDisconnected(I)V
    .locals 3
    .param p1, "statusCode"    # I

    .prologue
    .line 1189
    const-string v0, "0"

    invoke-static {v0}, Lru/cn/domain/statistics/inetra/InetraTracker;->setCurrentViewScreen(Ljava/lang/String;)V

    .line 1191
    if-eqz p1, :cond_1

    .line 1192
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragment$4;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    invoke-virtual {v0}, Lru/cn/tv/player/SimplePlayerFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1193
    invoke-static {}, Lcom/google/android/gms/common/GoogleApiAvailability;->getInstance()Lcom/google/android/gms/common/GoogleApiAvailability;

    move-result-object v0

    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragment$4;->this$0:Lru/cn/tv/player/SimplePlayerFragment;

    .line 1194
    invoke-virtual {v1}, Lru/cn/tv/player/SimplePlayerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v0, v1, p1, v2}, Lcom/google/android/gms/common/GoogleApiAvailability;->getErrorDialog(Landroid/app/Activity;II)Landroid/app/Dialog;

    move-result-object v0

    .line 1195
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 1198
    :cond_0
    const/16 v0, 0x7d2

    if-eq p1, v0, :cond_1

    .line 1199
    sget-object v0, Lru/cn/domain/statistics/inetra/ErrorCode;->UNKNOWN_ERROR:Lru/cn/domain/statistics/inetra/ErrorCode;

    const-string v1, "ChromecastConnectionError"

    const/4 v2, 0x0

    invoke-static {v0, v1, p1, v2}, Lru/cn/domain/statistics/inetra/InetraTracker;->error(Lru/cn/domain/statistics/inetra/ErrorCode;Ljava/lang/String;ILjava/lang/String;)V

    .line 1203
    :cond_1
    return-void
.end method
