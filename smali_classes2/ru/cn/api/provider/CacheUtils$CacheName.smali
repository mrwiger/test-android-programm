.class final enum Lru/cn/api/provider/CacheUtils$CacheName;
.super Ljava/lang/Enum;
.source "CacheUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/api/provider/CacheUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "CacheName"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lru/cn/api/provider/CacheUtils$CacheName;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lru/cn/api/provider/CacheUtils$CacheName;

.field public static final enum channels:Lru/cn/api/provider/CacheUtils$CacheName;

.field public static final enum rubricator:Lru/cn/api/provider/CacheUtils$CacheName;

.field public static final enum user_data:Lru/cn/api/provider/CacheUtils$CacheName;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 11
    new-instance v0, Lru/cn/api/provider/CacheUtils$CacheName;

    const-string v1, "user_data"

    invoke-direct {v0, v1, v2}, Lru/cn/api/provider/CacheUtils$CacheName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/api/provider/CacheUtils$CacheName;->user_data:Lru/cn/api/provider/CacheUtils$CacheName;

    new-instance v0, Lru/cn/api/provider/CacheUtils$CacheName;

    const-string v1, "channels"

    invoke-direct {v0, v1, v3}, Lru/cn/api/provider/CacheUtils$CacheName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/api/provider/CacheUtils$CacheName;->channels:Lru/cn/api/provider/CacheUtils$CacheName;

    new-instance v0, Lru/cn/api/provider/CacheUtils$CacheName;

    const-string v1, "rubricator"

    invoke-direct {v0, v1, v4}, Lru/cn/api/provider/CacheUtils$CacheName;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/api/provider/CacheUtils$CacheName;->rubricator:Lru/cn/api/provider/CacheUtils$CacheName;

    .line 10
    const/4 v0, 0x3

    new-array v0, v0, [Lru/cn/api/provider/CacheUtils$CacheName;

    sget-object v1, Lru/cn/api/provider/CacheUtils$CacheName;->user_data:Lru/cn/api/provider/CacheUtils$CacheName;

    aput-object v1, v0, v2

    sget-object v1, Lru/cn/api/provider/CacheUtils$CacheName;->channels:Lru/cn/api/provider/CacheUtils$CacheName;

    aput-object v1, v0, v3

    sget-object v1, Lru/cn/api/provider/CacheUtils$CacheName;->rubricator:Lru/cn/api/provider/CacheUtils$CacheName;

    aput-object v1, v0, v4

    sput-object v0, Lru/cn/api/provider/CacheUtils$CacheName;->$VALUES:[Lru/cn/api/provider/CacheUtils$CacheName;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 10
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lru/cn/api/provider/CacheUtils$CacheName;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 10
    const-class v0, Lru/cn/api/provider/CacheUtils$CacheName;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lru/cn/api/provider/CacheUtils$CacheName;

    return-object v0
.end method

.method public static values()[Lru/cn/api/provider/CacheUtils$CacheName;
    .locals 1

    .prologue
    .line 10
    sget-object v0, Lru/cn/api/provider/CacheUtils$CacheName;->$VALUES:[Lru/cn/api/provider/CacheUtils$CacheName;

    invoke-virtual {v0}, [Lru/cn/api/provider/CacheUtils$CacheName;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lru/cn/api/provider/CacheUtils$CacheName;

    return-object v0
.end method
