.class public Lru/cn/ad/natives/presenters/NativePrerollPresenter;
.super Ljava/lang/Object;
.source "NativePrerollPresenter.java"

# interfaces
.implements Landroid/os/Handler$Callback;
.implements Lru/cn/ad/natives/adapters/NativeAdAdapter$Presenter;


# instance fields
.field private adView:Landroid/view/View;

.field private attributionFormat:Ljava/lang/String;

.field private attributionView:Landroid/widget/TextView;

.field private closeCallback:Ljava/lang/Runnable;

.field private final containerView:Landroid/view/ViewGroup;

.field private final handler:Landroid/os/Handler;

.field private heightMeasureSpec:I

.field private final templateView:Landroid/view/View;

.field private timeRemains:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "containerView"    # Landroid/view/ViewGroup;

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    const/16 v1, 0x8

    iput v1, p0, Lru/cn/ad/natives/presenters/NativePrerollPresenter;->timeRemains:I

    .line 37
    const/4 v1, -0x1

    iput v1, p0, Lru/cn/ad/natives/presenters/NativePrerollPresenter;->heightMeasureSpec:I

    .line 40
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 41
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v1, 0x7f0c0046

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lru/cn/ad/natives/presenters/NativePrerollPresenter;->templateView:Landroid/view/View;

    .line 42
    iput-object p2, p0, Lru/cn/ad/natives/presenters/NativePrerollPresenter;->containerView:Landroid/view/ViewGroup;

    .line 44
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1, p0}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v1, p0, Lru/cn/ad/natives/presenters/NativePrerollPresenter;->handler:Landroid/os/Handler;

    .line 45
    const v1, 0x7f0e0020

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lru/cn/ad/natives/presenters/NativePrerollPresenter;->attributionFormat:Ljava/lang/String;

    .line 46
    iget-object v1, p0, Lru/cn/ad/natives/presenters/NativePrerollPresenter;->templateView:Landroid/view/View;

    const v2, 0x7f09001d

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lru/cn/ad/natives/presenters/NativePrerollPresenter;->attributionView:Landroid/widget/TextView;

    .line 48
    invoke-static {p1}, Lru/cn/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 49
    const/4 v1, -0x2

    iput v1, p0, Lru/cn/ad/natives/presenters/NativePrerollPresenter;->heightMeasureSpec:I

    .line 51
    :cond_0
    return-void
.end method

.method private updateAttributionView()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 91
    iget-object v0, p0, Lru/cn/ad/natives/presenters/NativePrerollPresenter;->attributionView:Landroid/widget/TextView;

    iget-object v1, p0, Lru/cn/ad/natives/presenters/NativePrerollPresenter;->attributionFormat:Ljava/lang/String;

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lru/cn/ad/natives/presenters/NativePrerollPresenter;->timeRemains:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 92
    iget-object v0, p0, Lru/cn/ad/natives/presenters/NativePrerollPresenter;->handler:Landroid/os/Handler;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v5, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 93
    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 2

    .prologue
    .line 97
    iget-object v0, p0, Lru/cn/ad/natives/presenters/NativePrerollPresenter;->handler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 98
    iget-object v0, p0, Lru/cn/ad/natives/presenters/NativePrerollPresenter;->containerView:Landroid/view/ViewGroup;

    iget-object v1, p0, Lru/cn/ad/natives/presenters/NativePrerollPresenter;->adView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 99
    return-void
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 1
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 108
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 120
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 110
    :pswitch_0
    iget v0, p0, Lru/cn/ad/natives/presenters/NativePrerollPresenter;->timeRemains:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lru/cn/ad/natives/presenters/NativePrerollPresenter;->timeRemains:I

    .line 111
    iget v0, p0, Lru/cn/ad/natives/presenters/NativePrerollPresenter;->timeRemains:I

    if-lez v0, :cond_0

    .line 112
    invoke-direct {p0}, Lru/cn/ad/natives/presenters/NativePrerollPresenter;->updateAttributionView()V

    .line 117
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 114
    :cond_0
    iget-object v0, p0, Lru/cn/ad/natives/presenters/NativePrerollPresenter;->closeCallback:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_1

    .line 108
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public presentingView()Landroid/view/View;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lru/cn/ad/natives/presenters/NativePrerollPresenter;->adView:Landroid/view/View;

    return-object v0
.end method

.method public show(Lru/cn/ad/natives/adapters/NativeAdAdapter$ViewBinder;Ljava/lang/Runnable;)V
    .locals 13
    .param p1, "binder"    # Lru/cn/ad/natives/adapters/NativeAdAdapter$ViewBinder;
    .param p2, "closeCallback"    # Ljava/lang/Runnable;

    .prologue
    const/4 v12, 0x0

    const/16 v11, 0x8

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 55
    iget-object v6, p0, Lru/cn/ad/natives/presenters/NativePrerollPresenter;->templateView:Landroid/view/View;

    const v7, 0x7f0901d7

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 56
    .local v5, "titleView":Landroid/widget/TextView;
    invoke-interface {p1, v5, v9}, Lru/cn/ad/natives/adapters/NativeAdAdapter$ViewBinder;->bind(Landroid/widget/TextView;I)Z

    .line 58
    iget-object v6, p0, Lru/cn/ad/natives/presenters/NativePrerollPresenter;->templateView:Landroid/view/View;

    const v7, 0x7f0901ca

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 59
    .local v0, "bodyView":Landroid/widget/TextView;
    invoke-interface {p1, v0, v10}, Lru/cn/ad/natives/adapters/NativeAdAdapter$ViewBinder;->bind(Landroid/widget/TextView;I)Z

    .line 61
    iget-object v6, p0, Lru/cn/ad/natives/presenters/NativePrerollPresenter;->templateView:Landroid/view/View;

    const v7, 0x7f0900f3

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 62
    .local v2, "imageView":Landroid/widget/ImageView;
    invoke-virtual {v2, v12}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 63
    const/4 v6, 0x5

    invoke-interface {p1, v2, v6}, Lru/cn/ad/natives/adapters/NativeAdAdapter$ViewBinder;->bind(Landroid/widget/ImageView;I)Z

    .line 65
    iget-object v6, p0, Lru/cn/ad/natives/presenters/NativePrerollPresenter;->templateView:Landroid/view/View;

    const v7, 0x7f090051

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 66
    .local v1, "ctaButton":Landroid/widget/Button;
    const/4 v6, 0x4

    invoke-interface {p1, v1, v6}, Lru/cn/ad/natives/adapters/NativeAdAdapter$ViewBinder;->bind(Landroid/widget/Button;I)Z

    .line 68
    iget-object v6, p0, Lru/cn/ad/natives/presenters/NativePrerollPresenter;->templateView:Landroid/view/View;

    const v7, 0x7f090115

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 69
    .local v3, "logoView":Landroid/widget/ImageView;
    invoke-interface {p1}, Lru/cn/ad/natives/adapters/NativeAdAdapter$ViewBinder;->getAdType()I

    move-result v6

    if-ne v6, v10, :cond_1

    .line 70
    invoke-virtual {v3, v12}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 71
    const/4 v6, 0x3

    invoke-interface {p1, v3, v6}, Lru/cn/ad/natives/adapters/NativeAdAdapter$ViewBinder;->bind(Landroid/widget/ImageView;I)Z

    .line 72
    invoke-virtual {v3, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 78
    :cond_0
    :goto_0
    iput-object p2, p0, Lru/cn/ad/natives/presenters/NativePrerollPresenter;->closeCallback:Ljava/lang/Runnable;

    .line 79
    iput v11, p0, Lru/cn/ad/natives/presenters/NativePrerollPresenter;->timeRemains:I

    .line 80
    invoke-direct {p0}, Lru/cn/ad/natives/presenters/NativePrerollPresenter;->updateAttributionView()V

    .line 82
    iget-object v6, p0, Lru/cn/ad/natives/presenters/NativePrerollPresenter;->templateView:Landroid/view/View;

    new-array v7, v9, [Landroid/view/View;

    aput-object v1, v7, v8

    invoke-static {v7}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v7

    invoke-interface {p1, v6, v7}, Lru/cn/ad/natives/adapters/NativeAdAdapter$ViewBinder;->registerTemplate(Landroid/view/View;Ljava/util/List;)Landroid/view/View;

    move-result-object v6

    iput-object v6, p0, Lru/cn/ad/natives/presenters/NativePrerollPresenter;->adView:Landroid/view/View;

    .line 84
    new-instance v4, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v6, -0x1

    iget v7, p0, Lru/cn/ad/natives/presenters/NativePrerollPresenter;->heightMeasureSpec:I

    invoke-direct {v4, v6, v7}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 86
    .local v4, "params":Landroid/widget/FrameLayout$LayoutParams;
    const/16 v6, 0x11

    iput v6, v4, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 87
    iget-object v6, p0, Lru/cn/ad/natives/presenters/NativePrerollPresenter;->containerView:Landroid/view/ViewGroup;

    iget-object v7, p0, Lru/cn/ad/natives/presenters/NativePrerollPresenter;->adView:Landroid/view/View;

    invoke-virtual {v6, v7, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 88
    return-void

    .line 74
    .end local v4    # "params":Landroid/widget/FrameLayout$LayoutParams;
    :cond_1
    if-eqz v1, :cond_0

    .line 75
    invoke-virtual {v3, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method
