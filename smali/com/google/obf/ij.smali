.class public Lcom/google/obf/ij;
.super Lcom/google/obf/ii;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/obf/ij$b;,
        Lcom/google/obf/ij$a;
    }
.end annotation


# static fields
.field private static e:Lcom/google/android/gms/ads/identifier/AdvertisingIdClient;

.field private static f:Ljava/util/concurrent/CountDownLatch;

.field private static volatile g:Z


# instance fields
.field private h:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 45
    const/4 v0, 0x0

    sput-object v0, Lcom/google/obf/ij;->e:Lcom/google/android/gms/ads/identifier/AdvertisingIdClient;

    .line 46
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    sput-object v0, Lcom/google/obf/ij;->f:Ljava/util/concurrent/CountDownLatch;

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Lcom/google/obf/im;Lcom/google/obf/in;Z)V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0, p1, p2, p3}, Lcom/google/obf/ii;-><init>(Landroid/content/Context;Lcom/google/obf/im;Lcom/google/obf/in;)V

    .line 11
    iput-boolean p4, p0, Lcom/google/obf/ij;->h:Z

    .line 12
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/ads/identifier/AdvertisingIdClient;)Lcom/google/android/gms/ads/identifier/AdvertisingIdClient;
    .locals 0

    .prologue
    .line 42
    sput-object p0, Lcom/google/obf/ij;->e:Lcom/google/android/gms/ads/identifier/AdvertisingIdClient;

    return-object p0
.end method

.method public static a(Ljava/lang/String;Landroid/content/Context;)Lcom/google/obf/ij;
    .locals 1

    .prologue
    .line 1
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Lcom/google/obf/ij;->a(Ljava/lang/String;Landroid/content/Context;Z)Lcom/google/obf/ij;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Landroid/content/Context;Z)Lcom/google/obf/ij;
    .locals 4

    .prologue
    .line 2
    new-instance v0, Lcom/google/obf/ie;

    invoke-direct {v0}, Lcom/google/obf/ie;-><init>()V

    .line 3
    invoke-static {p0, p1, v0}, Lcom/google/obf/ij;->a(Ljava/lang/String;Landroid/content/Context;Lcom/google/obf/im;)V

    .line 4
    if-eqz p2, :cond_1

    .line 5
    const-class v1, Lcom/google/obf/ij;

    monitor-enter v1

    .line 6
    :try_start_0
    sget-object v2, Lcom/google/obf/ij;->e:Lcom/google/android/gms/ads/identifier/AdvertisingIdClient;

    if-nez v2, :cond_0

    .line 7
    new-instance v2, Ljava/lang/Thread;

    new-instance v3, Lcom/google/obf/ij$b;

    invoke-direct {v3, p1}, Lcom/google/obf/ij$b;-><init>(Landroid/content/Context;)V

    invoke-direct {v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    .line 8
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 9
    :cond_1
    new-instance v1, Lcom/google/obf/ij;

    new-instance v2, Lcom/google/obf/ip;

    const/16 v3, 0xef

    invoke-direct {v2, v3}, Lcom/google/obf/ip;-><init>(I)V

    invoke-direct {v1, p1, v0, v2, p2}, Lcom/google/obf/ij;-><init>(Landroid/content/Context;Lcom/google/obf/im;Lcom/google/obf/in;Z)V

    return-object v1

    .line 8
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method static synthetic a(Z)Z
    .locals 0

    .prologue
    .line 43
    sput-boolean p0, Lcom/google/obf/ij;->g:Z

    return p0
.end method

.method static synthetic f()Lcom/google/android/gms/ads/identifier/AdvertisingIdClient;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/google/obf/ij;->e:Lcom/google/android/gms/ads/identifier/AdvertisingIdClient;

    return-object v0
.end method

.method static synthetic g()Ljava/util/concurrent/CountDownLatch;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/google/obf/ij;->f:Ljava/util/concurrent/CountDownLatch;

    return-object v0
.end method


# virtual methods
.method protected b(Landroid/content/Context;)V
    .locals 6

    .prologue
    .line 26
    invoke-super {p0, p1}, Lcom/google/obf/ii;->b(Landroid/content/Context;)V

    .line 27
    :try_start_0
    sget-boolean v0, Lcom/google/obf/ij;->g:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/obf/ij;->h:Z

    if-nez v0, :cond_2

    .line 28
    :cond_0
    const/16 v0, 0x18

    invoke-static {p1}, Lcom/google/obf/ij;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/obf/ij;->a(ILjava/lang/String;)V

    .line 40
    :cond_1
    :goto_0
    return-void

    .line 29
    :cond_2
    invoke-virtual {p0}, Lcom/google/obf/ij;->e()Lcom/google/obf/ij$a;

    move-result-object v0

    .line 30
    invoke-virtual {v0}, Lcom/google/obf/ij$a;->a()Ljava/lang/String;

    move-result-object v2

    .line 31
    if-eqz v2, :cond_1

    .line 32
    const/16 v3, 0x1c

    invoke-virtual {v0}, Lcom/google/obf/ij$a;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    const-wide/16 v0, 0x1

    :goto_1
    invoke-virtual {p0, v3, v0, v1}, Lcom/google/obf/ij;->a(IJ)V

    .line 33
    const/16 v0, 0x1a

    const-wide/16 v4, 0x5

    invoke-virtual {p0, v0, v4, v5}, Lcom/google/obf/ij;->a(IJ)V

    .line 34
    const/16 v0, 0x18

    invoke-virtual {p0, v0, v2}, Lcom/google/obf/ij;->a(ILjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/obf/ii$a; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 36
    :catch_0
    move-exception v0

    goto :goto_0

    .line 32
    :cond_3
    const-wide/16 v0, 0x0

    goto :goto_1

    .line 38
    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method e()Lcom/google/obf/ij$a;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 13
    :try_start_0
    sget-object v0, Lcom/google/obf/ij;->f:Ljava/util/concurrent/CountDownLatch;

    const-wide/16 v2, 0x2

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    .line 14
    if-nez v0, :cond_0

    .line 15
    new-instance v0, Lcom/google/obf/ij$a;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/google/obf/ij$a;-><init>(Lcom/google/obf/ij;Ljava/lang/String;Z)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 25
    :goto_0
    return-object v0

    .line 17
    :catch_0
    move-exception v0

    .line 18
    new-instance v0, Lcom/google/obf/ij$a;

    invoke-direct {v0, p0, v5, v4}, Lcom/google/obf/ij$a;-><init>(Lcom/google/obf/ij;Ljava/lang/String;Z)V

    goto :goto_0

    .line 19
    :cond_0
    const-class v1, Lcom/google/obf/ij;

    monitor-enter v1

    .line 20
    :try_start_1
    sget-object v0, Lcom/google/obf/ij;->e:Lcom/google/android/gms/ads/identifier/AdvertisingIdClient;

    if-nez v0, :cond_1

    .line 21
    new-instance v0, Lcom/google/obf/ij$a;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v0, p0, v2, v3}, Lcom/google/obf/ij$a;-><init>(Lcom/google/obf/ij;Ljava/lang/String;Z)V

    monitor-exit v1

    goto :goto_0

    .line 23
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 22
    :cond_1
    :try_start_2
    sget-object v0, Lcom/google/obf/ij;->e:Lcom/google/android/gms/ads/identifier/AdvertisingIdClient;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient;->getInfo()Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;

    move-result-object v2

    .line 23
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 24
    invoke-virtual {v2}, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/obf/ij;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 25
    new-instance v0, Lcom/google/obf/ij$a;

    invoke-virtual {v2}, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;->isLimitAdTrackingEnabled()Z

    move-result v2

    invoke-direct {v0, p0, v1, v2}, Lcom/google/obf/ij$a;-><init>(Lcom/google/obf/ij;Ljava/lang/String;Z)V

    goto :goto_0
.end method
