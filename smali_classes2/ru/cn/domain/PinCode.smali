.class public final Lru/cn/domain/PinCode;
.super Ljava/lang/Object;
.source "PinCode.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/domain/PinCode$PinOperationCallbacks;,
        Lru/cn/domain/PinCode$PinCodeSaveCallbacks;,
        Lru/cn/domain/PinCode$PinCodeCheckCallbacks;,
        Lru/cn/domain/PinCode$Status;
    }
.end annotation


# instance fields
.field private final context:Landroid/content/Context;

.field private status:Lru/cn/domain/PinCode$Status;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    sget-object v0, Lru/cn/domain/PinCode$Status;->require:Lru/cn/domain/PinCode$Status;

    iput-object v0, p0, Lru/cn/domain/PinCode;->status:Lru/cn/domain/PinCode$Status;

    .line 45
    iput-object p1, p0, Lru/cn/domain/PinCode;->context:Landroid/content/Context;

    .line 46
    return-void
.end method

.method static synthetic access$000(Lru/cn/domain/PinCode;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lru/cn/domain/PinCode;

    .prologue
    .line 16
    iget-object v0, p0, Lru/cn/domain/PinCode;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lru/cn/domain/PinCode;Lru/cn/domain/PinCode$PinOperationCallbacks;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/domain/PinCode;
    .param p1, "x1"    # Lru/cn/domain/PinCode$PinOperationCallbacks;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lru/cn/domain/PinCode;->createNewPin(Lru/cn/domain/PinCode$PinOperationCallbacks;)V

    return-void
.end method

.method static synthetic access$200(Lru/cn/domain/PinCode;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lru/cn/domain/PinCode;

    .prologue
    .line 16
    invoke-direct {p0}, Lru/cn/domain/PinCode;->privateOfficeURL()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private checkAuthoriseStatus(Lru/cn/domain/PinCode$PinOperationCallbacks;)V
    .locals 2
    .param p1, "callbacks"    # Lru/cn/domain/PinCode$PinOperationCallbacks;

    .prologue
    .line 129
    new-instance v0, Lru/cn/domain/PinCode$4;

    invoke-direct {v0, p0, p1}, Lru/cn/domain/PinCode$4;-><init>(Lru/cn/domain/PinCode;Lru/cn/domain/PinCode$PinOperationCallbacks;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    .line 168
    invoke-virtual {v0, v1}, Lru/cn/domain/PinCode$4;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 169
    return-void
.end method

.method private createNewPin(Lru/cn/domain/PinCode$PinOperationCallbacks;)V
    .locals 2
    .param p1, "callbacks"    # Lru/cn/domain/PinCode$PinOperationCallbacks;

    .prologue
    .line 191
    invoke-direct {p0}, Lru/cn/domain/PinCode;->generatePinCode()Ljava/lang/String;

    move-result-object v0

    .line 192
    .local v0, "newPinCode":Ljava/lang/String;
    new-instance v1, Lru/cn/domain/PinCode$5;

    invoke-direct {v1, p0, p1}, Lru/cn/domain/PinCode$5;-><init>(Lru/cn/domain/PinCode;Lru/cn/domain/PinCode$PinOperationCallbacks;)V

    invoke-virtual {p0, v0, v1}, Lru/cn/domain/PinCode;->savePinCode(Ljava/lang/String;Lru/cn/domain/PinCode$PinCodeSaveCallbacks;)V

    .line 204
    return-void
.end method

.method private generatePinCode()Ljava/lang/String;
    .locals 6

    .prologue
    .line 186
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v2

    const-wide v4, 0x40c1938000000000L    # 8999.0

    mul-double/2addr v2, v4

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    add-double/2addr v2, v4

    double-to-int v1, v2

    add-int/lit16 v0, v1, 0x3e8

    .line 187
    .local v0, "newPin":I
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private privateOfficeURL()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 172
    invoke-static {}, Lru/cn/api/provider/TvContentProviderContract;->contractor()Landroid/net/Uri;

    move-result-object v1

    .line 173
    .local v1, "contractorUri":Landroid/net/Uri;
    iget-object v0, p0, Lru/cn/domain/PinCode;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 175
    .local v6, "contractor":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-gtz v0, :cond_1

    :cond_0
    move-object v7, v2

    .line 182
    :goto_0
    return-object v7

    .line 179
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 180
    const-string v0, "private_office_uri"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 182
    .local v7, "officeUrl":Ljava/lang/String;
    goto :goto_0
.end method


# virtual methods
.method public createPin(Lru/cn/domain/PinCode$PinOperationCallbacks;)V
    .locals 1
    .param p1, "callbacks"    # Lru/cn/domain/PinCode$PinOperationCallbacks;

    .prologue
    .line 111
    new-instance v0, Lru/cn/domain/PinCode$3;

    invoke-direct {v0, p0, p1}, Lru/cn/domain/PinCode$3;-><init>(Lru/cn/domain/PinCode;Lru/cn/domain/PinCode$PinOperationCallbacks;)V

    invoke-direct {p0, v0}, Lru/cn/domain/PinCode;->checkAuthoriseStatus(Lru/cn/domain/PinCode$PinOperationCallbacks;)V

    .line 122
    return-void
.end method

.method public disablePin(Lru/cn/domain/PinCode$PinOperationCallbacks;)V
    .locals 0
    .param p1, "callbacks"    # Lru/cn/domain/PinCode$PinOperationCallbacks;

    .prologue
    .line 125
    invoke-direct {p0, p1}, Lru/cn/domain/PinCode;->checkAuthoriseStatus(Lru/cn/domain/PinCode$PinOperationCallbacks;)V

    .line 126
    return-void
.end method

.method public getPinCode(Lru/cn/domain/PinCode$PinCodeCheckCallbacks;)V
    .locals 2
    .param p1, "pinCodeWrapperListener"    # Lru/cn/domain/PinCode$PinCodeCheckCallbacks;

    .prologue
    .line 49
    new-instance v0, Lru/cn/domain/PinCode$1;

    invoke-direct {v0, p0, p1}, Lru/cn/domain/PinCode$1;-><init>(Lru/cn/domain/PinCode;Lru/cn/domain/PinCode$PinCodeCheckCallbacks;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    .line 70
    invoke-virtual {v0, v1}, Lru/cn/domain/PinCode$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 71
    return-void
.end method

.method public getStatus()Lru/cn/domain/PinCode$Status;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lru/cn/domain/PinCode;->status:Lru/cn/domain/PinCode$Status;

    return-object v0
.end method

.method public resetState()V
    .locals 1

    .prologue
    .line 107
    sget-object v0, Lru/cn/domain/PinCode$Status;->require:Lru/cn/domain/PinCode$Status;

    iput-object v0, p0, Lru/cn/domain/PinCode;->status:Lru/cn/domain/PinCode$Status;

    .line 108
    return-void
.end method

.method public savePinCode(Ljava/lang/String;Lru/cn/domain/PinCode$PinCodeSaveCallbacks;)V
    .locals 2
    .param p1, "pinCode"    # Ljava/lang/String;
    .param p2, "saveCallbacks"    # Lru/cn/domain/PinCode$PinCodeSaveCallbacks;

    .prologue
    .line 74
    new-instance v0, Lru/cn/domain/PinCode$2;

    invoke-direct {v0, p0, p1, p2}, Lru/cn/domain/PinCode$2;-><init>(Lru/cn/domain/PinCode;Ljava/lang/String;Lru/cn/domain/PinCode$PinCodeSaveCallbacks;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    .line 95
    invoke-virtual {v0, v1}, Lru/cn/domain/PinCode$2;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 96
    return-void
.end method

.method public setStatus(Lru/cn/domain/PinCode$Status;)V
    .locals 0
    .param p1, "enterMode"    # Lru/cn/domain/PinCode$Status;

    .prologue
    .line 99
    iput-object p1, p0, Lru/cn/domain/PinCode;->status:Lru/cn/domain/PinCode$Status;

    .line 100
    return-void
.end method
