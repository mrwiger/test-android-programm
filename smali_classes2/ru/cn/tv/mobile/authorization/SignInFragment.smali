.class public Lru/cn/tv/mobile/authorization/SignInFragment;
.super Lru/cn/tv/WebviewFragment;
.source "SignInFragment.java"


# instance fields
.field private challenge:Lru/cn/api/authorization/CodeAuthorizationChallenge;

.field private contractorId:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lru/cn/tv/WebviewFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lru/cn/tv/mobile/authorization/SignInFragment;)J
    .locals 2
    .param p0, "x0"    # Lru/cn/tv/mobile/authorization/SignInFragment;

    .prologue
    .line 16
    iget-wide v0, p0, Lru/cn/tv/mobile/authorization/SignInFragment;->contractorId:J

    return-wide v0
.end method

.method static synthetic access$100(Lru/cn/tv/mobile/authorization/SignInFragment;Lru/cn/api/authorization/CodeAuthorizationChallenge;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/mobile/authorization/SignInFragment;
    .param p1, "x1"    # Lru/cn/api/authorization/CodeAuthorizationChallenge;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lru/cn/tv/mobile/authorization/SignInFragment;->startChallenge(Lru/cn/api/authorization/CodeAuthorizationChallenge;)V

    return-void
.end method

.method static synthetic access$200(Lru/cn/tv/mobile/authorization/SignInFragment;)Lru/cn/api/authorization/CodeAuthorizationChallenge;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/mobile/authorization/SignInFragment;

    .prologue
    .line 16
    iget-object v0, p0, Lru/cn/tv/mobile/authorization/SignInFragment;->challenge:Lru/cn/api/authorization/CodeAuthorizationChallenge;

    return-object v0
.end method

.method static synthetic access$300(Lru/cn/tv/mobile/authorization/SignInFragment;I)V
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/mobile/authorization/SignInFragment;
    .param p1, "x1"    # I

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lru/cn/tv/mobile/authorization/SignInFragment;->sendResult(I)V

    return-void
.end method

.method private load()V
    .locals 2

    .prologue
    .line 50
    new-instance v0, Lru/cn/tv/mobile/authorization/SignInFragment$1;

    invoke-direct {v0, p0}, Lru/cn/tv/mobile/authorization/SignInFragment$1;-><init>(Lru/cn/tv/mobile/authorization/SignInFragment;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    .line 70
    invoke-virtual {v0, v1}, Lru/cn/tv/mobile/authorization/SignInFragment$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 71
    return-void
.end method

.method public static newInstance(J)Lru/cn/tv/mobile/authorization/SignInFragment;
    .locals 4
    .param p0, "contractorId"    # J

    .prologue
    .line 27
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 28
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "contractor"

    invoke-virtual {v0, v2, p0, p1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 30
    new-instance v1, Lru/cn/tv/mobile/authorization/SignInFragment;

    invoke-direct {v1}, Lru/cn/tv/mobile/authorization/SignInFragment;-><init>()V

    .line 31
    .local v1, "fragment":Lru/cn/tv/mobile/authorization/SignInFragment;
    invoke-virtual {v1, v0}, Lru/cn/tv/mobile/authorization/SignInFragment;->setArguments(Landroid/os/Bundle;)V

    .line 32
    return-object v1
.end method

.method private sendResult(I)V
    .locals 1
    .param p1, "resultCode"    # I

    .prologue
    .line 100
    invoke-virtual {p0}, Lru/cn/tv/mobile/authorization/SignInFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 101
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    .line 102
    invoke-virtual {v0, p1}, Landroid/app/Activity;->setResult(I)V

    .line 103
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 105
    :cond_0
    return-void
.end method

.method private startChallenge(Lru/cn/api/authorization/CodeAuthorizationChallenge;)V
    .locals 2
    .param p1, "challenge"    # Lru/cn/api/authorization/CodeAuthorizationChallenge;

    .prologue
    .line 74
    if-nez p1, :cond_0

    .line 75
    const/4 v1, -0x1

    invoke-direct {p0, v1}, Lru/cn/tv/mobile/authorization/SignInFragment;->sendResult(I)V

    .line 83
    :goto_0
    return-void

    .line 79
    :cond_0
    iput-object p1, p0, Lru/cn/tv/mobile/authorization/SignInFragment;->challenge:Lru/cn/api/authorization/CodeAuthorizationChallenge;

    .line 81
    invoke-virtual {p1}, Lru/cn/api/authorization/CodeAuthorizationChallenge;->authorizeUri()Landroid/net/Uri;

    move-result-object v0

    .line 82
    .local v0, "authorizationUri":Landroid/net/Uri;
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lru/cn/tv/mobile/authorization/SignInFragment;->load(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 37
    invoke-super {p0, p1}, Lru/cn/tv/WebviewFragment;->onCreate(Landroid/os/Bundle;)V

    .line 38
    invoke-virtual {p0}, Lru/cn/tv/mobile/authorization/SignInFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "contractor"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lru/cn/tv/mobile/authorization/SignInFragment;->contractorId:J

    .line 40
    const-string v0, "ptv29783051://callback"

    invoke-virtual {p0, v0}, Lru/cn/tv/mobile/authorization/SignInFragment;->addHandledUrl(Ljava/lang/String;)V

    .line 41
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 45
    invoke-super {p0}, Lru/cn/tv/WebviewFragment;->onResume()V

    .line 46
    invoke-direct {p0}, Lru/cn/tv/mobile/authorization/SignInFragment;->load()V

    .line 47
    return-void
.end method

.method public proceedAuthorization(Ljava/lang/String;)V
    .locals 2
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 86
    new-instance v0, Lru/cn/tv/mobile/authorization/SignInFragment$2;

    invoke-direct {v0, p0, p1}, Lru/cn/tv/mobile/authorization/SignInFragment$2;-><init>(Lru/cn/tv/mobile/authorization/SignInFragment;Ljava/lang/String;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    .line 96
    invoke-virtual {v0, v1}, Lru/cn/tv/mobile/authorization/SignInFragment$2;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 97
    return-void
.end method
