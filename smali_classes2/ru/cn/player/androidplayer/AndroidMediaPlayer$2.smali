.class Lru/cn/player/androidplayer/AndroidMediaPlayer$2;
.super Ljava/lang/Object;
.source "AndroidMediaPlayer.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnVideoSizeChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/player/androidplayer/AndroidMediaPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/player/androidplayer/AndroidMediaPlayer;


# direct methods
.method constructor <init>(Lru/cn/player/androidplayer/AndroidMediaPlayer;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/player/androidplayer/AndroidMediaPlayer;

    .prologue
    .line 73
    iput-object p1, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer$2;->this$0:Lru/cn/player/androidplayer/AndroidMediaPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onVideoSizeChanged(Landroid/media/MediaPlayer;II)V
    .locals 5
    .param p1, "mp"    # Landroid/media/MediaPlayer;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 75
    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoWidth()I

    move-result v1

    .line 76
    .local v1, "videoWidth":I
    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoHeight()I

    move-result v0

    .line 78
    .local v0, "videoHeight":I
    iget-object v2, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer$2;->this$0:Lru/cn/player/androidplayer/AndroidMediaPlayer;

    invoke-static {v2}, Lru/cn/player/androidplayer/AndroidMediaPlayer;->access$200(Lru/cn/player/androidplayer/AndroidMediaPlayer;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Video size detected: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "x"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    iget-object v2, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer$2;->this$0:Lru/cn/player/androidplayer/AndroidMediaPlayer;

    invoke-virtual {v2}, Lru/cn/player/androidplayer/AndroidMediaPlayer;->getState()Lru/cn/player/AbstractMediaPlayer$PlayerState;

    move-result-object v2

    sget-object v3, Lru/cn/player/AbstractMediaPlayer$PlayerState;->LOADED:Lru/cn/player/AbstractMediaPlayer$PlayerState;

    if-ne v2, v3, :cond_0

    .line 81
    iget-object v2, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer$2;->this$0:Lru/cn/player/androidplayer/AndroidMediaPlayer;

    sget-object v3, Lru/cn/player/AbstractMediaPlayer$PlayerState;->PLAYING:Lru/cn/player/AbstractMediaPlayer$PlayerState;

    invoke-virtual {v2, v3}, Lru/cn/player/androidplayer/AndroidMediaPlayer;->setState(Lru/cn/player/AbstractMediaPlayer$PlayerState;)V

    .line 84
    :cond_0
    iget-object v2, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer$2;->this$0:Lru/cn/player/androidplayer/AndroidMediaPlayer;

    invoke-static {v2}, Lru/cn/player/androidplayer/AndroidMediaPlayer;->access$300(Lru/cn/player/androidplayer/AndroidMediaPlayer;)Lru/cn/player/AbstractMediaPlayer$MediaPlayerListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 85
    iget-object v2, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer$2;->this$0:Lru/cn/player/androidplayer/AndroidMediaPlayer;

    invoke-static {v2}, Lru/cn/player/androidplayer/AndroidMediaPlayer;->access$500(Lru/cn/player/androidplayer/AndroidMediaPlayer;)Lru/cn/player/AbstractMediaPlayer$MediaPlayerListener;

    move-result-object v2

    iget-object v3, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer$2;->this$0:Lru/cn/player/androidplayer/AndroidMediaPlayer;

    invoke-static {v3}, Lru/cn/player/androidplayer/AndroidMediaPlayer;->access$400(Lru/cn/player/androidplayer/AndroidMediaPlayer;)F

    move-result v3

    invoke-interface {v2, v1, v0, v3}, Lru/cn/player/AbstractMediaPlayer$MediaPlayerListener;->videoSizeChanged(IIF)V

    .line 87
    :cond_1
    return-void
.end method
