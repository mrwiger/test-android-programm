.class public Lcom/my/target/bs;
.super Landroid/graphics/drawable/Drawable;
.source "StarsDrawable.java"


# static fields
.field private static final GRAY:I = -0x333334

.field private static final hN:F = 0.2f

.field private static final hO:I = -0x86ce2


# instance fields
.field private hP:F

.field private hQ:I

.field private hR:I

.field private hS:I

.field private hT:I

.field private hU:Z

.field private hV:F

.field private height:I

.field private rating:F

.field private width:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 107
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 108
    invoke-direct {p0}, Lcom/my/target/bs;->aY()V

    .line 109
    return-void
.end method

.method private static a(IF)Landroid/graphics/Path;
    .locals 8

    .prologue
    const v7, 0x441f4000    # 637.0f

    const/high16 v6, 0x43fa0000    # 500.0f

    const v5, 0x43bf8000    # 383.0f

    const v4, 0x43a48000    # 329.0f

    const/high16 v3, 0x447a0000    # 1000.0f

    .line 39
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    .line 40
    sget-object v1, Landroid/graphics/Path$FillType;->EVEN_ODD:Landroid/graphics/Path$FillType;

    invoke-virtual {v0, v1}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    .line 42
    int-to-float v1, p0

    mul-float v2, v5, p1

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 44
    int-to-float v1, p0

    const v2, 0x43ac8000    # 345.0f

    mul-float/2addr v2, p1

    add-float/2addr v1, v2

    mul-float v2, v4, p1

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 47
    int-to-float v1, p0

    mul-float v2, v6, p1

    add-float/2addr v1, v2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 50
    int-to-float v1, p0

    const v2, 0x4423c000    # 655.0f

    mul-float/2addr v2, p1

    add-float/2addr v1, v2

    mul-float v2, v4, p1

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 53
    int-to-float v1, p0

    mul-float v2, v3, p1

    add-float/2addr v1, v2

    mul-float v2, v5, p1

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 59
    int-to-float v1, p0

    const v2, 0x443b8000    # 750.0f

    mul-float/2addr v2, p1

    add-float/2addr v1, v2

    mul-float v2, v7, p1

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 65
    int-to-float v1, p0

    const v2, 0x444a8000    # 810.0f

    mul-float/2addr v2, p1

    add-float/2addr v1, v2

    mul-float v2, v3, p1

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 71
    int-to-float v1, p0

    mul-float v2, v6, p1

    add-float/2addr v1, v2

    const v2, 0x444f4000    # 829.0f

    mul-float/2addr v2, p1

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 77
    int-to-float v1, p0

    const/high16 v2, 0x433e0000    # 190.0f

    mul-float/2addr v2, p1

    add-float/2addr v1, v2

    mul-float v2, v3, p1

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 83
    int-to-float v1, p0

    const/high16 v2, 0x437a0000    # 250.0f

    mul-float/2addr v2, p1

    add-float/2addr v1, v2

    mul-float v2, v7, p1

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 90
    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 92
    return-object v0
.end method

.method private a(IIFLandroid/graphics/Canvas;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x0

    .line 234
    int-to-float v0, p2

    const/high16 v2, 0x447a0000    # 1000.0f

    div-float/2addr v0, v2

    .line 235
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 236
    const v3, -0x86ce2

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 237
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 238
    sget-object v3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 240
    invoke-static {v6, v0}, Lcom/my/target/bs;->a(IF)Landroid/graphics/Path;

    move-result-object v3

    .line 242
    new-instance v4, Landroid/graphics/Rect;

    int-to-float v0, p1

    int-to-float v5, p2

    mul-float/2addr v5, p3

    add-float/2addr v0, v5

    float-to-int v0, v0

    invoke-direct {v4, p1, v6, v0, p2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 247
    int-to-float v0, p2

    mul-float/2addr v0, p3

    float-to-int v0, v0

    :try_start_0
    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, p2, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 254
    :goto_0
    if-nez v0, :cond_0

    .line 264
    :goto_1
    return-void

    .line 249
    :catch_0
    move-exception v0

    .line 251
    const-string v0, "cannot build icon: OOME"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_0

    .line 259
    :cond_0
    new-instance v5, Landroid/graphics/Canvas;

    invoke-direct {v5, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 261
    invoke-virtual {v5, v3, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 263
    invoke-virtual {p4, v0, v1, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_1
.end method

.method private static a(IIILandroid/graphics/Canvas;)V
    .locals 3

    .prologue
    .line 26
    int-to-float v0, p1

    const/high16 v1, 0x447a0000    # 1000.0f

    div-float/2addr v0, v1

    .line 28
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 29
    invoke-virtual {v1, p2}, Landroid/graphics/Paint;->setColor(I)V

    .line 30
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 31
    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 33
    invoke-static {p0, v0}, Lcom/my/target/bs;->a(IF)Landroid/graphics/Path;

    move-result-object v0

    .line 34
    invoke-virtual {p3, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 35
    return-void
.end method

.method private aY()V
    .locals 4

    .prologue
    .line 210
    iget v0, p0, Lcom/my/target/bs;->hQ:I

    iput v0, p0, Lcom/my/target/bs;->height:I

    .line 211
    iget v0, p0, Lcom/my/target/bs;->hQ:I

    int-to-float v0, v0

    iget v1, p0, Lcom/my/target/bs;->hP:F

    add-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/my/target/bs;->hR:I

    .line 212
    iget v0, p0, Lcom/my/target/bs;->hR:I

    mul-int/lit8 v0, v0, 0x5

    iput v0, p0, Lcom/my/target/bs;->width:I

    .line 214
    iget v0, p0, Lcom/my/target/bs;->rating:F

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p0, Lcom/my/target/bs;->hS:I

    .line 215
    iget v0, p0, Lcom/my/target/bs;->rating:F

    float-to-double v0, v0

    iget v2, p0, Lcom/my/target/bs;->rating:F

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    sub-double/2addr v0, v2

    double-to-float v0, v0

    iput v0, p0, Lcom/my/target/bs;->hV:F

    .line 216
    const/high16 v0, 0x40a00000    # 5.0f

    iget v1, p0, Lcom/my/target/bs;->rating:F

    sub-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p0, Lcom/my/target/bs;->hT:I

    .line 217
    iget v0, p0, Lcom/my/target/bs;->rating:F

    iget v1, p0, Lcom/my/target/bs;->hS:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    .line 218
    const v1, 0x3e4ccccd    # 0.2f

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_1

    .line 220
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/my/target/bs;->hU:Z

    .line 230
    :cond_0
    :goto_0
    return-void

    .line 224
    :cond_1
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/my/target/bs;->hU:Z

    .line 225
    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 227
    iget v0, p0, Lcom/my/target/bs;->hT:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/my/target/bs;->hT:I

    goto :goto_0
.end method


# virtual methods
.method public aV()I
    .locals 1

    .prologue
    .line 113
    iget v0, p0, Lcom/my/target/bs;->hT:I

    return v0
.end method

.method public aW()I
    .locals 1

    .prologue
    .line 123
    iget v0, p0, Lcom/my/target/bs;->hS:I

    return v0
.end method

.method public aX()Z
    .locals 1

    .prologue
    .line 133
    iget-boolean v0, p0, Lcom/my/target/bs;->hU:Z

    return v0
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const v6, -0x333334

    const/4 v1, 0x0

    .line 166
    iget v3, p0, Lcom/my/target/bs;->hT:I

    move v0, v1

    move v2, v1

    .line 168
    :goto_0
    iget v4, p0, Lcom/my/target/bs;->hS:I

    if-ge v0, v4, :cond_0

    .line 170
    iget v4, p0, Lcom/my/target/bs;->hQ:I

    const v5, -0x86ce2

    invoke-static {v2, v4, v5, p1}, Lcom/my/target/bs;->a(IIILandroid/graphics/Canvas;)V

    .line 171
    iget v4, p0, Lcom/my/target/bs;->hR:I

    add-int/2addr v2, v4

    .line 168
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 174
    :cond_0
    iget-boolean v0, p0, Lcom/my/target/bs;->hU:Z

    if-eqz v0, :cond_1

    .line 176
    iget v0, p0, Lcom/my/target/bs;->hQ:I

    invoke-static {v2, v0, v6, p1}, Lcom/my/target/bs;->a(IIILandroid/graphics/Canvas;)V

    .line 178
    iget v0, p0, Lcom/my/target/bs;->hQ:I

    iget v4, p0, Lcom/my/target/bs;->hV:F

    invoke-direct {p0, v2, v0, v4, p1}, Lcom/my/target/bs;->a(IIFLandroid/graphics/Canvas;)V

    .line 179
    iget v0, p0, Lcom/my/target/bs;->hR:I

    add-int/2addr v2, v0

    .line 183
    :cond_1
    :goto_1
    if-ge v1, v3, :cond_2

    .line 185
    iget v0, p0, Lcom/my/target/bs;->hQ:I

    invoke-static {v2, v0, v6, p1}, Lcom/my/target/bs;->a(IIILandroid/graphics/Canvas;)V

    .line 186
    iget v0, p0, Lcom/my/target/bs;->hR:I

    add-int/2addr v2, v0

    .line 183
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 188
    :cond_2
    return-void
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 118
    iget v0, p0, Lcom/my/target/bs;->height:I

    return v0
.end method

.method public getOpacity()I
    .locals 1

    .prologue
    .line 205
    const/4 v0, 0x0

    return v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 128
    iget v0, p0, Lcom/my/target/bs;->width:I

    return v0
.end method

.method public setAlpha(I)V
    .locals 0
    .param p1, "i"    # I

    .prologue
    .line 194
    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 0
    .param p1, "colorFilter"    # Landroid/graphics/ColorFilter;

    .prologue
    .line 200
    return-void
.end method

.method public setRating(F)V
    .locals 3
    .param p1, "rating"    # F

    .prologue
    const/4 v2, 0x0

    .line 138
    const/high16 v0, 0x40a00000    # 5.0f

    cmpl-float v0, p1, v0

    if-gtz v0, :cond_0

    cmpg-float v0, p1, v2

    if-gez v0, :cond_1

    .line 140
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Rating is out of bounds: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 141
    iput v2, p0, Lcom/my/target/bs;->rating:F

    .line 148
    :goto_0
    invoke-direct {p0}, Lcom/my/target/bs;->aY()V

    .line 149
    return-void

    .line 145
    :cond_1
    iput p1, p0, Lcom/my/target/bs;->rating:F

    goto :goto_0
.end method

.method public setStarSize(I)V
    .locals 0
    .param p1, "starSize"    # I

    .prologue
    .line 153
    iput p1, p0, Lcom/my/target/bs;->hQ:I

    .line 154
    invoke-direct {p0}, Lcom/my/target/bs;->aY()V

    .line 155
    return-void
.end method

.method public setStarsPadding(F)V
    .locals 0
    .param p1, "starPadding"    # F

    .prologue
    .line 159
    iput p1, p0, Lcom/my/target/bs;->hP:F

    .line 160
    invoke-direct {p0}, Lcom/my/target/bs;->aY()V

    .line 161
    return-void
.end method
