.class public Lru/cn/player/timeshift/StreamTimeshifter;
.super Ljava/lang/Object;
.source "StreamTimeshifter.java"

# interfaces
.implements Lru/cn/player/timeshift/TimeshiftTask$OnTaskCompletedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/player/timeshift/StreamTimeshifter$Callback;
    }
.end annotation


# instance fields
.field private final callback:Lru/cn/player/timeshift/StreamTimeshifter$Callback;

.field private currentTask:Lru/cn/player/timeshift/TimeshiftTask;

.field public final location:Lru/cn/api/iptv/replies/MediaLocation;


# direct methods
.method public constructor <init>(Lru/cn/api/iptv/replies/MediaLocation;Lru/cn/player/timeshift/StreamTimeshifter$Callback;)V
    .locals 0
    .param p1, "location"    # Lru/cn/api/iptv/replies/MediaLocation;
    .param p2, "callback"    # Lru/cn/player/timeshift/StreamTimeshifter$Callback;

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lru/cn/player/timeshift/StreamTimeshifter;->location:Lru/cn/api/iptv/replies/MediaLocation;

    .line 23
    iput-object p2, p0, Lru/cn/player/timeshift/StreamTimeshifter;->callback:Lru/cn/player/timeshift/StreamTimeshifter$Callback;

    .line 24
    return-void
.end method

.method private createTask(Landroid/content/Context;)Lru/cn/player/timeshift/TimeshiftTask;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 61
    iget-object v2, p0, Lru/cn/player/timeshift/StreamTimeshifter;->location:Lru/cn/api/iptv/replies/MediaLocation;

    iget-wide v2, v2, Lru/cn/api/iptv/replies/MediaLocation;->streamId:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 63
    :try_start_0
    iget-object v2, p0, Lru/cn/player/timeshift/StreamTimeshifter;->location:Lru/cn/api/iptv/replies/MediaLocation;

    iget-wide v2, v2, Lru/cn/api/iptv/replies/MediaLocation;->contractorId:J

    invoke-static {p1, v2, v3}, Lru/cn/api/ServiceLocator;->mediaLocator(Landroid/content/Context;J)Lru/cn/api/medialocator/retrofit/MediaLocatorApi;

    move-result-object v0

    .line 64
    .local v0, "api":Lru/cn/api/medialocator/retrofit/MediaLocatorApi;
    new-instance v2, Lru/cn/player/timeshift/TimeshiftApiTask;

    iget-object v3, p0, Lru/cn/player/timeshift/StreamTimeshifter;->location:Lru/cn/api/iptv/replies/MediaLocation;

    invoke-direct {v2, v0, v3, p0}, Lru/cn/player/timeshift/TimeshiftApiTask;-><init>(Lru/cn/api/medialocator/retrofit/MediaLocatorApi;Lru/cn/api/iptv/replies/MediaLocation;Lru/cn/player/timeshift/TimeshiftTask$OnTaskCompletedListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 70
    .end local v0    # "api":Lru/cn/api/medialocator/retrofit/MediaLocatorApi;
    :goto_0
    return-object v2

    .line 65
    :catch_0
    move-exception v1

    .line 66
    .local v1, "e":Ljava/lang/Exception;
    invoke-static {v1}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    .line 70
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_0
    new-instance v2, Lru/cn/player/timeshift/TimeshiftTask;

    iget-object v3, p0, Lru/cn/player/timeshift/StreamTimeshifter;->location:Lru/cn/api/iptv/replies/MediaLocation;

    invoke-direct {v2, v3, p0}, Lru/cn/player/timeshift/TimeshiftTask;-><init>(Lru/cn/api/iptv/replies/MediaLocation;Lru/cn/player/timeshift/TimeshiftTask$OnTaskCompletedListener;)V

    goto :goto_0
.end method


# virtual methods
.method public final cancelRequests()V
    .locals 2

    .prologue
    .line 54
    iget-object v0, p0, Lru/cn/player/timeshift/StreamTimeshifter;->currentTask:Lru/cn/player/timeshift/TimeshiftTask;

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Lru/cn/player/timeshift/StreamTimeshifter;->currentTask:Lru/cn/player/timeshift/TimeshiftTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lru/cn/player/timeshift/TimeshiftTask;->cancel(Z)Z

    .line 56
    const/4 v0, 0x0

    iput-object v0, p0, Lru/cn/player/timeshift/StreamTimeshifter;->currentTask:Lru/cn/player/timeshift/TimeshiftTask;

    .line 58
    :cond_0
    return-void
.end method

.method public onCompleted(Lru/cn/player/timeshift/TimeshiftTask;Ljava/lang/String;Ljava/lang/Integer;)V
    .locals 3
    .param p1, "task"    # Lru/cn/player/timeshift/TimeshiftTask;
    .param p2, "playbackUrl"    # Ljava/lang/String;
    .param p3, "offset"    # Ljava/lang/Integer;

    .prologue
    .line 42
    iget-object v0, p0, Lru/cn/player/timeshift/StreamTimeshifter;->currentTask:Lru/cn/player/timeshift/TimeshiftTask;

    if-eq p1, v0, :cond_0

    .line 51
    :goto_0
    return-void

    .line 45
    :cond_0
    if-nez p2, :cond_1

    .line 46
    iget-object v0, p0, Lru/cn/player/timeshift/StreamTimeshifter;->callback:Lru/cn/player/timeshift/StreamTimeshifter$Callback;

    invoke-interface {v0}, Lru/cn/player/timeshift/StreamTimeshifter$Callback;->onSeekFailed()V

    goto :goto_0

    .line 49
    :cond_1
    iget-object v0, p0, Lru/cn/player/timeshift/StreamTimeshifter;->callback:Lru/cn/player/timeshift/StreamTimeshifter$Callback;

    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v2, p0, Lru/cn/player/timeshift/StreamTimeshifter;->currentTask:Lru/cn/player/timeshift/TimeshiftTask;

    invoke-virtual {v2}, Lru/cn/player/timeshift/TimeshiftTask;->isStartOver()Z

    move-result v2

    invoke-interface {v0, p2, v1, v2}, Lru/cn/player/timeshift/StreamTimeshifter$Callback;->onSeekCompleted(Ljava/lang/String;IZ)V

    goto :goto_0
.end method

.method public seekTo(Landroid/content/Context;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "offset"    # I

    .prologue
    .line 27
    invoke-virtual {p0}, Lru/cn/player/timeshift/StreamTimeshifter;->cancelRequests()V

    .line 29
    invoke-direct {p0, p1}, Lru/cn/player/timeshift/StreamTimeshifter;->createTask(Landroid/content/Context;)Lru/cn/player/timeshift/TimeshiftTask;

    move-result-object v0

    iput-object v0, p0, Lru/cn/player/timeshift/StreamTimeshifter;->currentTask:Lru/cn/player/timeshift/TimeshiftTask;

    .line 30
    iget-object v0, p0, Lru/cn/player/timeshift/StreamTimeshifter;->currentTask:Lru/cn/player/timeshift/TimeshiftTask;

    invoke-virtual {v0, p2}, Lru/cn/player/timeshift/TimeshiftTask;->seekTo(I)V

    .line 31
    return-void
.end method

.method public startOver(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 34
    invoke-virtual {p0}, Lru/cn/player/timeshift/StreamTimeshifter;->cancelRequests()V

    .line 36
    invoke-direct {p0, p1}, Lru/cn/player/timeshift/StreamTimeshifter;->createTask(Landroid/content/Context;)Lru/cn/player/timeshift/TimeshiftTask;

    move-result-object v0

    iput-object v0, p0, Lru/cn/player/timeshift/StreamTimeshifter;->currentTask:Lru/cn/player/timeshift/TimeshiftTask;

    .line 37
    iget-object v0, p0, Lru/cn/player/timeshift/StreamTimeshifter;->currentTask:Lru/cn/player/timeshift/TimeshiftTask;

    invoke-virtual {v0}, Lru/cn/player/timeshift/TimeshiftTask;->startOver()V

    .line 38
    return-void
.end method
