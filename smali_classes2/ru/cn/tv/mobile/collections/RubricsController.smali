.class public Lru/cn/tv/mobile/collections/RubricsController;
.super Ljava/lang/Object;
.source "RubricsController.java"

# interfaces
.implements Lru/cn/tv/mobile/collections/RubricFragmentController;


# instance fields
.field private final listener:Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastSelectedListener;

.field private rubrics:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lru/cn/api/catalogue/replies/Rubric;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastSelectedListener;)V
    .locals 1
    .param p1, "listener"    # Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastSelectedListener;

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    const/4 v0, 0x0

    iput-object v0, p0, Lru/cn/tv/mobile/collections/RubricsController;->rubrics:Ljava/util/List;

    .line 19
    iput-object p1, p0, Lru/cn/tv/mobile/collections/RubricsController;->listener:Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastSelectedListener;

    .line 20
    return-void
.end method


# virtual methods
.method public createRubricFragment(I)Landroid/support/v4/app/Fragment;
    .locals 5
    .param p1, "position"    # I

    .prologue
    const/4 v0, 0x0

    .line 33
    iget-object v4, p0, Lru/cn/tv/mobile/collections/RubricsController;->rubrics:Ljava/util/List;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lru/cn/tv/mobile/collections/RubricsController;->rubrics:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_0

    .line 34
    iget-object v4, p0, Lru/cn/tv/mobile/collections/RubricsController;->rubrics:Ljava/util/List;

    invoke-interface {v4, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/cn/api/catalogue/replies/Rubric;

    iget-wide v2, v4, Lru/cn/api/catalogue/replies/Rubric;->id:J

    .line 35
    .local v2, "rubricId":J
    invoke-static {v2, v3, v0}, Lru/cn/api/provider/TvContentProviderContract;->rubricItemsUri(JLandroid/support/v4/util/LongSparseArray;)Landroid/net/Uri;

    move-result-object v1

    .line 37
    .local v1, "uri":Landroid/net/Uri;
    invoke-static {v1}, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->newInstance(Landroid/net/Uri;)Lru/cn/tv/mobile/telecasts/TelecastsListFragment;

    move-result-object v0

    .line 38
    .local v0, "f":Lru/cn/tv/mobile/telecasts/TelecastsListFragment;
    iget-object v4, p0, Lru/cn/tv/mobile/collections/RubricsController;->listener:Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastSelectedListener;

    invoke-virtual {v0, v4}, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->setOnSelectedListener(Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastSelectedListener;)V

    .line 42
    .end local v0    # "f":Lru/cn/tv/mobile/telecasts/TelecastsListFragment;
    .end local v1    # "uri":Landroid/net/Uri;
    .end local v2    # "rubricId":J
    :cond_0
    return-object v0
.end method

.method public getRubrics()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lru/cn/api/catalogue/replies/Rubric;",
            ">;"
        }
    .end annotation

    .prologue
    .line 27
    iget-object v0, p0, Lru/cn/tv/mobile/collections/RubricsController;->rubrics:Ljava/util/List;

    return-object v0
.end method

.method public getTitle(I)Ljava/lang/String;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 56
    iget-object v0, p0, Lru/cn/tv/mobile/collections/RubricsController;->rubrics:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lru/cn/tv/mobile/collections/RubricsController;->rubrics:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 57
    iget-object v0, p0, Lru/cn/tv/mobile/collections/RubricsController;->rubrics:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/api/catalogue/replies/Rubric;

    iget-object v0, v0, Lru/cn/api/catalogue/replies/Rubric;->title:Ljava/lang/String;

    .line 60
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public rubricCount()I
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lru/cn/tv/mobile/collections/RubricsController;->rubrics:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p0, Lru/cn/tv/mobile/collections/RubricsController;->rubrics:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 51
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setRubrics(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lru/cn/api/catalogue/replies/Rubric;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 23
    .local p1, "rubrics":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/catalogue/replies/Rubric;>;"
    iput-object p1, p0, Lru/cn/tv/mobile/collections/RubricsController;->rubrics:Ljava/util/List;

    .line 24
    return-void
.end method
