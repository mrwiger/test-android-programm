.class public Lru/cn/tv/mobile/playeroptions/BottomSheetAdapter;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "BottomSheetAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/tv/mobile/playeroptions/BottomSheetAdapter$ViewHolder;,
        Lru/cn/tv/mobile/playeroptions/BottomSheetAdapter$OnItemClickListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$Adapter",
        "<",
        "Lru/cn/tv/mobile/playeroptions/BottomSheetAdapter$ViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private isSelectable:Z

.field private itemClickListener:Lru/cn/tv/mobile/playeroptions/BottomSheetAdapter$OnItemClickListener;

.field private items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lru/cn/tv/mobile/playeroptions/BottomSheetItem;",
            ">;"
        }
    .end annotation
.end field

.field private selectedIndex:I


# direct methods
.method public constructor <init>(Ljava/util/List;Lru/cn/tv/mobile/playeroptions/BottomSheetAdapter$OnItemClickListener;)V
    .locals 1
    .param p2, "itemClickListener"    # Lru/cn/tv/mobile/playeroptions/BottomSheetAdapter$OnItemClickListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lru/cn/tv/mobile/playeroptions/BottomSheetItem;",
            ">;",
            "Lru/cn/tv/mobile/playeroptions/BottomSheetAdapter$OnItemClickListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 25
    .local p1, "items":Ljava/util/List;, "Ljava/util/List<Lru/cn/tv/mobile/playeroptions/BottomSheetItem;>;"
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 22
    const/4 v0, 0x0

    iput-boolean v0, p0, Lru/cn/tv/mobile/playeroptions/BottomSheetAdapter;->isSelectable:Z

    .line 26
    iput-object p1, p0, Lru/cn/tv/mobile/playeroptions/BottomSheetAdapter;->items:Ljava/util/List;

    .line 27
    iput-object p2, p0, Lru/cn/tv/mobile/playeroptions/BottomSheetAdapter;->itemClickListener:Lru/cn/tv/mobile/playeroptions/BottomSheetAdapter$OnItemClickListener;

    .line 28
    return-void
.end method


# virtual methods
.method public getItemCount()I
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lru/cn/tv/mobile/playeroptions/BottomSheetAdapter;->items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method final synthetic lambda$onCreateViewHolder$0$BottomSheetAdapter(Lru/cn/tv/mobile/playeroptions/BottomSheetAdapter$ViewHolder;Landroid/view/View;)V
    .locals 2
    .param p1, "viewHolder"    # Lru/cn/tv/mobile/playeroptions/BottomSheetAdapter$ViewHolder;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 38
    iget-object v0, p0, Lru/cn/tv/mobile/playeroptions/BottomSheetAdapter;->itemClickListener:Lru/cn/tv/mobile/playeroptions/BottomSheetAdapter$OnItemClickListener;

    invoke-virtual {p1}, Lru/cn/tv/mobile/playeroptions/BottomSheetAdapter$ViewHolder;->getAdapterPosition()I

    move-result v1

    invoke-interface {v0, v1}, Lru/cn/tv/mobile/playeroptions/BottomSheetAdapter$OnItemClickListener;->onItemClick(I)V

    .line 40
    iget-boolean v0, p0, Lru/cn/tv/mobile/playeroptions/BottomSheetAdapter;->isSelectable:Z

    if-eqz v0, :cond_0

    .line 41
    invoke-virtual {p1}, Lru/cn/tv/mobile/playeroptions/BottomSheetAdapter$ViewHolder;->getAdapterPosition()I

    move-result v0

    invoke-virtual {p0, v0}, Lru/cn/tv/mobile/playeroptions/BottomSheetAdapter;->setSelection(I)V

    .line 43
    :cond_0
    return-void
.end method

.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .prologue
    .line 14
    check-cast p1, Lru/cn/tv/mobile/playeroptions/BottomSheetAdapter$ViewHolder;

    invoke-virtual {p0, p1, p2}, Lru/cn/tv/mobile/playeroptions/BottomSheetAdapter;->onBindViewHolder(Lru/cn/tv/mobile/playeroptions/BottomSheetAdapter$ViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lru/cn/tv/mobile/playeroptions/BottomSheetAdapter$ViewHolder;I)V
    .locals 4
    .param p1, "holder"    # Lru/cn/tv/mobile/playeroptions/BottomSheetAdapter$ViewHolder;
    .param p2, "position"    # I

    .prologue
    const/4 v3, 0x0

    .line 50
    iget-object v1, p0, Lru/cn/tv/mobile/playeroptions/BottomSheetAdapter;->items:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/tv/mobile/playeroptions/BottomSheetItem;

    .line 51
    .local v0, "item":Lru/cn/tv/mobile/playeroptions/BottomSheetItem;
    invoke-virtual {v0}, Lru/cn/tv/mobile/playeroptions/BottomSheetItem;->getImageResource()I

    move-result v1

    if-lez v1, :cond_1

    .line 52
    iget-object v1, p1, Lru/cn/tv/mobile/playeroptions/BottomSheetAdapter$ViewHolder;->imageView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Lru/cn/tv/mobile/playeroptions/BottomSheetItem;->getImageResource()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 53
    iget-object v1, p1, Lru/cn/tv/mobile/playeroptions/BottomSheetAdapter$ViewHolder;->imageView:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 62
    :cond_0
    :goto_0
    iget-object v1, p1, Lru/cn/tv/mobile/playeroptions/BottomSheetAdapter$ViewHolder;->textView:Landroid/widget/TextView;

    invoke-virtual {v0}, Lru/cn/tv/mobile/playeroptions/BottomSheetItem;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 63
    return-void

    .line 55
    :cond_1
    iget-object v1, p1, Lru/cn/tv/mobile/playeroptions/BottomSheetAdapter$ViewHolder;->imageView:Landroid/widget/ImageView;

    const v2, 0x7f0800ca

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 56
    iget-object v1, p1, Lru/cn/tv/mobile/playeroptions/BottomSheetAdapter$ViewHolder;->imageView:Landroid/widget/ImageView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 57
    iget v1, p0, Lru/cn/tv/mobile/playeroptions/BottomSheetAdapter;->selectedIndex:I

    if-ne p2, v1, :cond_0

    .line 58
    iget-object v1, p1, Lru/cn/tv/mobile/playeroptions/BottomSheetAdapter$ViewHolder;->imageView:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 14
    invoke-virtual {p0, p1, p2}, Lru/cn/tv/mobile/playeroptions/BottomSheetAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lru/cn/tv/mobile/playeroptions/BottomSheetAdapter$ViewHolder;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lru/cn/tv/mobile/playeroptions/BottomSheetAdapter$ViewHolder;
    .locals 5
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    .line 32
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 33
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v3, 0x7f0c00a2

    const/4 v4, 0x0

    invoke-virtual {v0, v3, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 35
    .local v1, "itemView":Landroid/view/View;
    new-instance v2, Lru/cn/tv/mobile/playeroptions/BottomSheetAdapter$ViewHolder;

    invoke-direct {v2, v1}, Lru/cn/tv/mobile/playeroptions/BottomSheetAdapter$ViewHolder;-><init>(Landroid/view/View;)V

    .line 37
    .local v2, "viewHolder":Lru/cn/tv/mobile/playeroptions/BottomSheetAdapter$ViewHolder;
    new-instance v3, Lru/cn/tv/mobile/playeroptions/BottomSheetAdapter$$Lambda$0;

    invoke-direct {v3, p0, v2}, Lru/cn/tv/mobile/playeroptions/BottomSheetAdapter$$Lambda$0;-><init>(Lru/cn/tv/mobile/playeroptions/BottomSheetAdapter;Lru/cn/tv/mobile/playeroptions/BottomSheetAdapter$ViewHolder;)V

    invoke-virtual {v1, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 45
    return-object v2
.end method

.method public setSelection(I)V
    .locals 1
    .param p1, "selectionIndex"    # I

    .prologue
    .line 72
    iget-object v0, p0, Lru/cn/tv/mobile/playeroptions/BottomSheetAdapter;->items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le p1, v0, :cond_0

    .line 73
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    .line 75
    :cond_0
    if-ltz p1, :cond_2

    iget-object v0, p0, Lru/cn/tv/mobile/playeroptions/BottomSheetAdapter;->items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lru/cn/tv/mobile/playeroptions/BottomSheetAdapter;->isSelectable:Z

    .line 77
    iget v0, p0, Lru/cn/tv/mobile/playeroptions/BottomSheetAdapter;->selectedIndex:I

    invoke-virtual {p0, v0}, Lru/cn/tv/mobile/playeroptions/BottomSheetAdapter;->notifyItemChanged(I)V

    .line 78
    iget-boolean v0, p0, Lru/cn/tv/mobile/playeroptions/BottomSheetAdapter;->isSelectable:Z

    if-eqz v0, :cond_1

    .line 79
    iput p1, p0, Lru/cn/tv/mobile/playeroptions/BottomSheetAdapter;->selectedIndex:I

    .line 80
    iget v0, p0, Lru/cn/tv/mobile/playeroptions/BottomSheetAdapter;->selectedIndex:I

    invoke-virtual {p0, v0}, Lru/cn/tv/mobile/playeroptions/BottomSheetAdapter;->notifyItemChanged(I)V

    .line 82
    :cond_1
    return-void

    .line 75
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
