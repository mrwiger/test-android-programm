.class public Lcom/samsung/multiscreen/ble/adparser/TypeSecOOBFlags;
.super Lcom/samsung/multiscreen/ble/adparser/AdElement;
.source "TypeSecOOBFlags.java"


# static fields
.field public static FLAGS_LE_SUPPORTED_HOST:I

.field public static FLAGS_OOB_DATA_PRESENT:I

.field public static FLAGS_RANDOM_ADDRESS:I

.field public static FLAGS_SIMULTANEOUS_LE_BR_EDR:I


# instance fields
.field flags:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x1

    sput v0, Lcom/samsung/multiscreen/ble/adparser/TypeSecOOBFlags;->FLAGS_OOB_DATA_PRESENT:I

    .line 26
    const/4 v0, 0x2

    sput v0, Lcom/samsung/multiscreen/ble/adparser/TypeSecOOBFlags;->FLAGS_LE_SUPPORTED_HOST:I

    .line 27
    const/4 v0, 0x4

    sput v0, Lcom/samsung/multiscreen/ble/adparser/TypeSecOOBFlags;->FLAGS_SIMULTANEOUS_LE_BR_EDR:I

    .line 28
    const/16 v0, 0x8

    sput v0, Lcom/samsung/multiscreen/ble/adparser/TypeSecOOBFlags;->FLAGS_RANDOM_ADDRESS:I

    return-void
.end method

.method public constructor <init>([BI)V
    .locals 1
    .param p1, "data"    # [B
    .param p2, "pos"    # I

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/samsung/multiscreen/ble/adparser/AdElement;-><init>()V

    .line 31
    aget-byte v0, p1, p2

    and-int/lit16 v0, v0, 0xff

    iput v0, p0, Lcom/samsung/multiscreen/ble/adparser/TypeSecOOBFlags;->flags:I

    .line 32
    return-void
.end method


# virtual methods
.method public getFlags()I
    .locals 1

    .prologue
    .line 58
    iget v0, p0, Lcom/samsung/multiscreen/ble/adparser/TypeSecOOBFlags;->flags:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    const/16 v3, 0xa

    .line 35
    new-instance v0, Ljava/lang/StringBuffer;

    const-string v1, "OOB Flags: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 36
    .local v0, "sb":Ljava/lang/StringBuffer;
    iget v1, p0, Lcom/samsung/multiscreen/ble/adparser/TypeSecOOBFlags;->flags:I

    sget v2, Lcom/samsung/multiscreen/ble/adparser/TypeSecOOBFlags;->FLAGS_OOB_DATA_PRESENT:I

    and-int/2addr v1, v2

    if-eqz v1, :cond_0

    .line 37
    const-string v1, "OOB data present"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 38
    :cond_0
    iget v1, p0, Lcom/samsung/multiscreen/ble/adparser/TypeSecOOBFlags;->flags:I

    sget v2, Lcom/samsung/multiscreen/ble/adparser/TypeSecOOBFlags;->FLAGS_LE_SUPPORTED_HOST:I

    and-int/2addr v1, v2

    if-eqz v1, :cond_2

    .line 39
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    if-le v1, v3, :cond_1

    .line 40
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 41
    :cond_1
    const-string v1, "LE supported (Host)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 43
    :cond_2
    iget v1, p0, Lcom/samsung/multiscreen/ble/adparser/TypeSecOOBFlags;->flags:I

    sget v2, Lcom/samsung/multiscreen/ble/adparser/TypeSecOOBFlags;->FLAGS_SIMULTANEOUS_LE_BR_EDR:I

    and-int/2addr v1, v2

    if-eqz v1, :cond_4

    .line 44
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    if-le v1, v3, :cond_3

    .line 45
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 46
    :cond_3
    const-string v1, "Simultaneous LE and BR/EDR to Same Device Capable (Host)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 48
    :cond_4
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    if-le v1, v3, :cond_5

    .line 49
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 50
    :cond_5
    iget v1, p0, Lcom/samsung/multiscreen/ble/adparser/TypeSecOOBFlags;->flags:I

    sget v2, Lcom/samsung/multiscreen/ble/adparser/TypeSecOOBFlags;->FLAGS_RANDOM_ADDRESS:I

    and-int/2addr v1, v2

    if-eqz v1, :cond_6

    .line 51
    const-string v1, "Random Address"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 54
    :goto_0
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/StringBuffer;)V

    return-object v1

    .line 53
    :cond_6
    const-string v1, "Public Address"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0
.end method
