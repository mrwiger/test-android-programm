.class Lru/cn/tv/mobile/search/SearchViewModel;
.super Lru/cn/mvvm/RxViewModel;
.source "SearchViewModel.java"


# static fields
.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private final loader:Lru/cn/mvvm/RxLoader;

.field private final searchResult:Landroid/arch/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/arch/lifecycle/MutableLiveData",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lru/cn/tv/mobile/search/SearchViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lru/cn/tv/mobile/search/SearchViewModel;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lru/cn/mvvm/RxLoader;)V
    .locals 1
    .param p1, "loader"    # Lru/cn/mvvm/RxLoader;

    .prologue
    .line 34
    invoke-direct {p0}, Lru/cn/mvvm/RxViewModel;-><init>()V

    .line 35
    iput-object p1, p0, Lru/cn/tv/mobile/search/SearchViewModel;->loader:Lru/cn/mvvm/RxLoader;

    .line 36
    new-instance v0, Landroid/arch/lifecycle/MutableLiveData;

    invoke-direct {v0}, Landroid/arch/lifecycle/MutableLiveData;-><init>()V

    iput-object v0, p0, Lru/cn/tv/mobile/search/SearchViewModel;->searchResult:Landroid/arch/lifecycle/MutableLiveData;

    .line 37
    return-void
.end method

.method static final synthetic lambda$searchResultsByQuery$3$SearchViewModel(Lru/cn/api/catalogue/replies/RubricOption;)Z
    .locals 2
    .param p0, "rubricOption"    # Lru/cn/api/catalogue/replies/RubricOption;

    .prologue
    .line 89
    iget-object v0, p0, Lru/cn/api/catalogue/replies/RubricOption;->type:Lru/cn/api/catalogue/replies/RubricOption$RubricOptionType;

    sget-object v1, Lru/cn/api/catalogue/replies/RubricOption$RubricOptionType;->INPUT:Lru/cn/api/catalogue/replies/RubricOption$RubricOptionType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static final synthetic lambda$searchRubric$2$SearchViewModel(JLandroid/database/Cursor;)Lru/cn/api/catalogue/replies/Rubric;
    .locals 8
    .param p0, "rubricId"    # J
    .param p2, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 69
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-ge v3, v5, :cond_0

    .line 70
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    const-string v4, "Rubric with id = %d not found"

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 71
    .local v2, "errorMessage":Ljava/lang/String;
    new-instance v3, Ljava/lang/IllegalStateException;

    invoke-direct {v3, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 74
    .end local v2    # "errorMessage":Ljava/lang/String;
    :cond_0
    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    .line 75
    const-string v3, "data"

    invoke-interface {p2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 76
    .local v1, "dataColumnIndex":I
    if-gez v1, :cond_1

    .line 77
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "Can\'t find data column"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 80
    :cond_1
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 81
    .local v0, "d":Ljava/lang/String;
    invoke-static {v0}, Lru/cn/api/catalogue/replies/Rubric;->fromJson(Ljava/lang/String;)Lru/cn/api/catalogue/replies/Rubric;

    move-result-object v3

    return-object v3
.end method

.method private searchInRubric(Ljava/lang/String;J)Lio/reactivex/Observable;
    .locals 2
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "rubricId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "J)",
            "Lio/reactivex/Observable",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59
    invoke-direct {p0, p2, p3}, Lru/cn/tv/mobile/search/SearchViewModel;->searchRubric(J)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lru/cn/tv/mobile/search/SearchViewModel$$Lambda$2;

    invoke-direct {v1, p0, p1}, Lru/cn/tv/mobile/search/SearchViewModel$$Lambda$2;-><init>(Lru/cn/tv/mobile/search/SearchViewModel;Ljava/lang/String;)V

    .line 60
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->switchMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 59
    return-object v0
.end method

.method private searchResultsByQuery(Lru/cn/api/catalogue/replies/Rubric;Ljava/lang/String;)Lio/reactivex/Observable;
    .locals 6
    .param p1, "rubric"    # Lru/cn/api/catalogue/replies/Rubric;
    .param p2, "query"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lru/cn/api/catalogue/replies/Rubric;",
            "Ljava/lang/String;",
            ")",
            "Lio/reactivex/Observable",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 87
    iget-object v3, p1, Lru/cn/api/catalogue/replies/Rubric;->options:Ljava/util/List;

    .line 88
    invoke-static {v3}, Lcom/annimon/stream/Stream;->of(Ljava/lang/Iterable;)Lcom/annimon/stream/Stream;

    move-result-object v3

    sget-object v4, Lru/cn/tv/mobile/search/SearchViewModel$$Lambda$4;->$instance:Lcom/annimon/stream/function/Predicate;

    .line 89
    invoke-virtual {v3, v4}, Lcom/annimon/stream/Stream;->filter(Lcom/annimon/stream/function/Predicate;)Lcom/annimon/stream/Stream;

    move-result-object v3

    .line 90
    invoke-virtual {v3}, Lcom/annimon/stream/Stream;->single()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/api/catalogue/replies/RubricOption;

    .line 92
    .local v0, "currentRubricOption":Lru/cn/api/catalogue/replies/RubricOption;
    if-nez v0, :cond_0

    .line 93
    new-instance v3, Ljava/lang/Exception;

    const-string v4, "Server is not configured correctly"

    invoke-direct {v3, v4}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Lio/reactivex/Observable;->error(Ljava/lang/Throwable;)Lio/reactivex/Observable;

    move-result-object v3

    .line 101
    :goto_0
    return-object v3

    .line 96
    :cond_0
    new-instance v1, Landroid/support/v4/util/LongSparseArray;

    invoke-direct {v1}, Landroid/support/v4/util/LongSparseArray;-><init>()V

    .line 97
    .local v1, "options":Landroid/support/v4/util/LongSparseArray;, "Landroid/support/v4/util/LongSparseArray<Ljava/lang/String;>;"
    iget-wide v4, v0, Lru/cn/api/catalogue/replies/RubricOption;->id:J

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v4, v5, v3}, Landroid/support/v4/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 99
    iget-wide v4, p1, Lru/cn/api/catalogue/replies/Rubric;->id:J

    invoke-static {v4, v5, v1}, Lru/cn/api/provider/TvContentProviderContract;->rubricSearchItemsUri(JLandroid/support/v4/util/LongSparseArray;)Landroid/net/Uri;

    move-result-object v2

    .line 101
    .local v2, "searchRubricUri":Landroid/net/Uri;
    iget-object v3, p0, Lru/cn/tv/mobile/search/SearchViewModel;->loader:Lru/cn/mvvm/RxLoader;

    invoke-virtual {v3, v2}, Lru/cn/mvvm/RxLoader;->query(Landroid/net/Uri;)Lio/reactivex/Observable;

    move-result-object v3

    .line 102
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v4

    invoke-virtual {v3, v4}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v3

    goto :goto_0
.end method

.method private searchRubric(J)Lio/reactivex/Observable;
    .locals 3
    .param p1, "rubricId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lio/reactivex/Observable",
            "<",
            "Lru/cn/api/catalogue/replies/Rubric;",
            ">;"
        }
    .end annotation

    .prologue
    .line 64
    invoke-static {p1, p2}, Lru/cn/api/provider/TvContentProviderContract;->rubricatorUri(J)Landroid/net/Uri;

    move-result-object v0

    .line 66
    .local v0, "rubricUri":Landroid/net/Uri;
    iget-object v1, p0, Lru/cn/tv/mobile/search/SearchViewModel;->loader:Lru/cn/mvvm/RxLoader;

    invoke-virtual {v1, v0}, Lru/cn/mvvm/RxLoader;->query(Landroid/net/Uri;)Lio/reactivex/Observable;

    move-result-object v1

    .line 67
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    new-instance v2, Lru/cn/tv/mobile/search/SearchViewModel$$Lambda$3;

    invoke-direct {v2, p1, p2}, Lru/cn/tv/mobile/search/SearchViewModel$$Lambda$3;-><init>(J)V

    .line 68
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v1

    .line 66
    return-object v1
.end method


# virtual methods
.method final synthetic lambda$searchInRubric$1$SearchViewModel(Ljava/lang/String;Lru/cn/api/catalogue/replies/Rubric;)Lio/reactivex/ObservableSource;
    .locals 1
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "rubric"    # Lru/cn/api/catalogue/replies/Rubric;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 60
    invoke-direct {p0, p2, p1}, Lru/cn/tv/mobile/search/SearchViewModel;->searchResultsByQuery(Lru/cn/api/catalogue/replies/Rubric;Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method final synthetic lambda$setSearchQuery$0$SearchViewModel(Ljava/lang/Throwable;)V
    .locals 2
    .param p1, "throwable"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 51
    sget-object v0, Lru/cn/tv/mobile/search/SearchViewModel;->LOG_TAG:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 52
    iget-object v0, p0, Lru/cn/tv/mobile/search/SearchViewModel;->searchResult:Landroid/arch/lifecycle/MutableLiveData;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/arch/lifecycle/MutableLiveData;->setValue(Ljava/lang/Object;)V

    .line 53
    return-void
.end method

.method searchResult()Landroid/arch/lifecycle/LiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/arch/lifecycle/LiveData",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 40
    iget-object v0, p0, Lru/cn/tv/mobile/search/SearchViewModel;->searchResult:Landroid/arch/lifecycle/MutableLiveData;

    return-object v0
.end method

.method setSearchQuery(Ljava/lang/String;J)V
    .locals 4
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "rubricId"    # J

    .prologue
    .line 44
    invoke-virtual {p0}, Lru/cn/tv/mobile/search/SearchViewModel;->unbindAll()V

    .line 46
    invoke-direct {p0, p1, p2, p3}, Lru/cn/tv/mobile/search/SearchViewModel;->searchInRubric(Ljava/lang/String;J)Lio/reactivex/Observable;

    move-result-object v1

    .line 47
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    iget-object v2, p0, Lru/cn/tv/mobile/search/SearchViewModel;->searchResult:Landroid/arch/lifecycle/MutableLiveData;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v2}, Lru/cn/tv/mobile/search/SearchViewModel$$Lambda$0;->get$Lambda(Landroid/arch/lifecycle/MutableLiveData;)Lio/reactivex/functions/Consumer;

    move-result-object v2

    new-instance v3, Lru/cn/tv/mobile/search/SearchViewModel$$Lambda$1;

    invoke-direct {v3, p0}, Lru/cn/tv/mobile/search/SearchViewModel$$Lambda$1;-><init>(Lru/cn/tv/mobile/search/SearchViewModel;)V

    .line 48
    invoke-virtual {v1, v2, v3}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 55
    .local v0, "disposable":Lio/reactivex/disposables/Disposable;
    invoke-virtual {p0, v0}, Lru/cn/tv/mobile/search/SearchViewModel;->bind(Lio/reactivex/disposables/Disposable;)V

    .line 56
    return-void
.end method
