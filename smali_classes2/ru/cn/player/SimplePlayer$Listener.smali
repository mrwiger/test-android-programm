.class public interface abstract Lru/cn/player/SimplePlayer$Listener;
.super Ljava/lang/Object;
.source "SimplePlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/player/SimplePlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation


# virtual methods
.method public abstract endBuffering()V
.end method

.method public abstract onComplete()V
.end method

.method public abstract onError(II)V
.end method

.method public abstract onMetadata(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lru/cn/player/metadata/MetadataItem;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract qualityChanged(I)V
.end method

.method public abstract startBuffering()V
.end method

.method public abstract stateChanged(Lru/cn/player/AbstractMediaPlayer$PlayerState;)V
.end method

.method public abstract videoSizeChanged(II)V
.end method
