.class public Lcom/yandex/mobile/ads/nativeads/v;
.super Lcom/yandex/mobile/ads/nativeads/q;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/yandex/mobile/ads/nativeads/q",
        "<",
        "Lcom/yandex/mobile/ads/nativeads/NativeAppInstallAdView;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/yandex/mobile/ads/nativeads/NativeAppInstallAdView;Lcom/yandex/mobile/ads/nativeads/d;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Lcom/yandex/mobile/ads/nativeads/q;-><init>(Lcom/yandex/mobile/ads/nativeads/NativeAdView;Lcom/yandex/mobile/ads/nativeads/d;)V

    .line 24
    return-void
.end method


# virtual methods
.method protected bridge synthetic a(Lcom/yandex/mobile/ads/nativeads/NativeAdView;Lcom/yandex/mobile/ads/nativeads/d;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 20
    check-cast p1, Lcom/yandex/mobile/ads/nativeads/NativeAppInstallAdView;

    invoke-virtual {p0, p1, p2}, Lcom/yandex/mobile/ads/nativeads/v;->a(Lcom/yandex/mobile/ads/nativeads/NativeAppInstallAdView;Lcom/yandex/mobile/ads/nativeads/d;)Ljava/util/HashMap;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/yandex/mobile/ads/nativeads/NativeAppInstallAdView;Lcom/yandex/mobile/ads/nativeads/d;)Ljava/util/HashMap;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/yandex/mobile/ads/nativeads/NativeAppInstallAdView;",
            "Lcom/yandex/mobile/ads/nativeads/d;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/yandex/mobile/ads/nativeads/ac;",
            ">;"
        }
    .end annotation

    .prologue
    .line 29
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 31
    const-string v1, "age"

    new-instance v2, Lcom/yandex/mobile/ads/nativeads/ac$c;

    invoke-virtual {p1}, Lcom/yandex/mobile/ads/nativeads/NativeAppInstallAdView;->a()Landroid/widget/TextView;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/yandex/mobile/ads/nativeads/ac$c;-><init>(Landroid/widget/TextView;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    const-string v1, "body"

    new-instance v2, Lcom/yandex/mobile/ads/nativeads/ac$c;

    invoke-virtual {p1}, Lcom/yandex/mobile/ads/nativeads/NativeAppInstallAdView;->b()Landroid/widget/TextView;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/yandex/mobile/ads/nativeads/ac$c;-><init>(Landroid/widget/TextView;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    const-string v1, "call_to_action"

    new-instance v2, Lcom/yandex/mobile/ads/nativeads/ac$c;

    invoke-virtual {p1}, Lcom/yandex/mobile/ads/nativeads/NativeAppInstallAdView;->c()Landroid/widget/Button;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/yandex/mobile/ads/nativeads/ac$c;-><init>(Landroid/widget/TextView;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    const-string v1, "domain"

    new-instance v2, Lcom/yandex/mobile/ads/nativeads/ac$c;

    invoke-virtual {p1}, Lcom/yandex/mobile/ads/nativeads/NativeAppInstallAdView;->d()Landroid/widget/TextView;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/yandex/mobile/ads/nativeads/ac$c;-><init>(Landroid/widget/TextView;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    const-string v1, "icon"

    new-instance v2, Lcom/yandex/mobile/ads/nativeads/ac$a;

    invoke-virtual {p1}, Lcom/yandex/mobile/ads/nativeads/NativeAppInstallAdView;->e()Landroid/widget/ImageView;

    move-result-object v3

    invoke-direct {v2, v3, p2}, Lcom/yandex/mobile/ads/nativeads/ac$a;-><init>(Landroid/widget/ImageView;Lcom/yandex/mobile/ads/nativeads/d;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    const-string v1, "image"

    new-instance v2, Lcom/yandex/mobile/ads/nativeads/ac$a;

    invoke-virtual {p1}, Lcom/yandex/mobile/ads/nativeads/NativeAppInstallAdView;->f()Landroid/widget/ImageView;

    move-result-object v3

    invoke-direct {v2, v3, p2}, Lcom/yandex/mobile/ads/nativeads/ac$a;-><init>(Landroid/widget/ImageView;Lcom/yandex/mobile/ads/nativeads/d;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    const-string v1, "price"

    new-instance v2, Lcom/yandex/mobile/ads/nativeads/ac$c;

    invoke-virtual {p1}, Lcom/yandex/mobile/ads/nativeads/NativeAppInstallAdView;->g()Landroid/widget/TextView;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/yandex/mobile/ads/nativeads/ac$c;-><init>(Landroid/widget/TextView;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    const-string v1, "rating"

    new-instance v2, Lcom/yandex/mobile/ads/nativeads/ac$b;

    invoke-virtual {p1}, Lcom/yandex/mobile/ads/nativeads/NativeAppInstallAdView;->h()Landroid/view/View;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/yandex/mobile/ads/nativeads/ac$b;-><init>(Landroid/view/View;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    const-string v1, "review_count"

    new-instance v2, Lcom/yandex/mobile/ads/nativeads/ac$c;

    invoke-virtual {p1}, Lcom/yandex/mobile/ads/nativeads/NativeAppInstallAdView;->i()Landroid/widget/TextView;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/yandex/mobile/ads/nativeads/ac$c;-><init>(Landroid/widget/TextView;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    const-string v1, "sponsored"

    new-instance v2, Lcom/yandex/mobile/ads/nativeads/ac$c;

    invoke-virtual {p1}, Lcom/yandex/mobile/ads/nativeads/NativeAppInstallAdView;->j()Landroid/widget/TextView;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/yandex/mobile/ads/nativeads/ac$c;-><init>(Landroid/widget/TextView;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    const-string v1, "title"

    new-instance v2, Lcom/yandex/mobile/ads/nativeads/ac$c;

    invoke-virtual {p1}, Lcom/yandex/mobile/ads/nativeads/NativeAppInstallAdView;->k()Landroid/widget/TextView;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/yandex/mobile/ads/nativeads/ac$c;-><init>(Landroid/widget/TextView;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    const-string v1, "warning"

    new-instance v2, Lcom/yandex/mobile/ads/nativeads/ac$c;

    invoke-virtual {p1}, Lcom/yandex/mobile/ads/nativeads/NativeAppInstallAdView;->l()Landroid/widget/TextView;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/yandex/mobile/ads/nativeads/ac$c;-><init>(Landroid/widget/TextView;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 44
    return-object v0
.end method
