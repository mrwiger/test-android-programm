.class Lcom/yandex/metrica/impl/au;
.super Lcom/yandex/metrica/impl/m;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/yandex/metrica/impl/au$a;,
        Lcom/yandex/metrica/impl/au$b;,
        Lcom/yandex/metrica/impl/au$c;
    }
.end annotation


# instance fields
.field m:Lcom/yandex/metrica/c$c;

.field n:Lcom/yandex/metrica/impl/bb;

.field o:Lcom/yandex/metrica/impl/ob/co;

.field p:Lcom/yandex/metrica/impl/ob/v;

.field q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field r:I

.field s:I

.field private t:Lcom/yandex/metrica/impl/au$c;

.field private final u:Lcom/yandex/metrica/impl/utils/f;

.field private v:Z


# direct methods
.method public constructor <init>(Lcom/yandex/metrica/impl/ob/v;)V
    .locals 6

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/yandex/metrica/impl/m;-><init>()V

    .line 71
    const/4 v0, 0x0

    iput v0, p0, Lcom/yandex/metrica/impl/au;->r:I

    .line 72
    const/4 v0, -0x1

    iput v0, p0, Lcom/yandex/metrica/impl/au;->s:I

    .line 76
    new-instance v0, Lcom/yandex/metrica/impl/utils/f;

    invoke-direct {v0}, Lcom/yandex/metrica/impl/utils/f;-><init>()V

    iput-object v0, p0, Lcom/yandex/metrica/impl/au;->u:Lcom/yandex/metrica/impl/utils/f;

    .line 80
    iput-object p1, p0, Lcom/yandex/metrica/impl/au;->p:Lcom/yandex/metrica/impl/ob/v;

    .line 81
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ob/v;->j()Lcom/yandex/metrica/impl/ob/co;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/impl/au;->o:Lcom/yandex/metrica/impl/ob/co;

    .line 82
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ob/v;->i()Lcom/yandex/metrica/impl/bb;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/impl/au;->n:Lcom/yandex/metrica/impl/bb;

    .line 1150
    const/4 v0, 0x1

    .line 2029
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    .line 1151
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 1152
    invoke-static {}, Lcom/yandex/metrica/impl/utils/r;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 2044
    invoke-static {}, Lcom/yandex/metrica/impl/utils/n;->a()Lcom/yandex/metrica/impl/utils/n;

    move-result-object v3

    invoke-virtual {v3}, Lcom/yandex/metrica/impl/utils/n;->d()Z

    move-result v3

    .line 1153
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    .line 1150
    invoke-static {v1, v2, v3}, Lcom/yandex/metrica/impl/aq;->a(Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;)Lcom/yandex/metrica/c$c$f;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/yandex/metrica/impl/ob/b;->b(ILcom/yandex/metrica/impl/ob/d;)I

    move-result v0

    .line 83
    iput v0, p0, Lcom/yandex/metrica/impl/au;->r:I

    .line 84
    return-void
.end method

.method public static G()Lcom/yandex/metrica/impl/au$a;
    .locals 1

    .prologue
    .line 602
    new-instance v0, Lcom/yandex/metrica/impl/au$a;

    invoke-direct {v0}, Lcom/yandex/metrica/impl/au$a;-><init>()V

    return-object v0
.end method

.method private static a(Lcom/yandex/metrica/impl/a$a;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 447
    .line 448
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    iget-object v2, p0, Lcom/yandex/metrica/impl/a$a;->a:Ljava/lang/String;

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 449
    invoke-static {v0}, Lcom/yandex/metrica/impl/au;->a(Lorg/json/JSONObject;)[Lcom/yandex/metrica/c$c$a;

    move-result-object v4

    .line 450
    if-eqz v4, :cond_1

    .line 451
    array-length v5, v4

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v5, :cond_0

    aget-object v3, v4, v2

    .line 452
    const/4 v6, 0x7

    invoke-static {v6, v3}, Lcom/yandex/metrica/impl/ob/b;->b(ILcom/yandex/metrica/impl/ob/d;)I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    add-int/2addr v3, v0

    .line 451
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_0

    .line 459
    :catch_0
    move-exception v0

    move v0, v1

    :cond_0
    :goto_1
    return v0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method private static a(Landroid/net/Uri$Builder;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 306
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 307
    invoke-virtual {p0, p1, p2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 309
    :cond_0
    return-void
.end method

.method private static a(Landroid/net/Uri$Builder;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 301
    invoke-static {p2, p3}, Lcom/yandex/metrica/impl/bj;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 302
    invoke-virtual {p0, p1, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 303
    return-void
.end method

.method private static a(Lorg/json/JSONObject;)[Lcom/yandex/metrica/c$c$a;
    .locals 5

    .prologue
    .line 226
    invoke-virtual {p0}, Lorg/json/JSONObject;->length()I

    move-result v0

    .line 227
    if-lez v0, :cond_1

    .line 228
    new-array v2, v0, [Lcom/yandex/metrica/c$c$a;

    .line 229
    invoke-virtual {p0}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v3

    .line 230
    const/4 v0, 0x0

    move v1, v0

    .line 231
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 232
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 234
    :try_start_0
    new-instance v4, Lcom/yandex/metrica/c$c$a;

    invoke-direct {v4}, Lcom/yandex/metrica/c$c$a;-><init>()V

    .line 235
    iput-object v0, v4, Lcom/yandex/metrica/c$c$a;->b:Ljava/lang/String;

    .line 236
    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/yandex/metrica/c$c$a;->c:Ljava/lang/String;

    .line 237
    aput-object v4, v2, v1
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 241
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 242
    goto :goto_0

    :cond_0
    move-object v0, v2

    .line 245
    :goto_2
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_2

    :catch_0
    move-exception v0

    goto :goto_1
.end method


# virtual methods
.method B()V
    .locals 4

    .prologue
    .line 93
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/au;->s()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    .line 2250
    const-string v0, "report"

    invoke-virtual {v1, v0}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2254
    const-string v0, "deviceid"

    iget-object v2, p0, Lcom/yandex/metrica/impl/au;->c:Lcom/yandex/metrica/impl/bb;

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/bb;->p()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/yandex/metrica/impl/au;->n:Lcom/yandex/metrica/impl/bb;

    invoke-virtual {v3}, Lcom/yandex/metrica/impl/bb;->p()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v0, v2, v3}, Lcom/yandex/metrica/impl/au;->a(Landroid/net/Uri$Builder;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2255
    const-string v0, "uuid"

    iget-object v2, p0, Lcom/yandex/metrica/impl/au;->c:Lcom/yandex/metrica/impl/bb;

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/bb;->q()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/yandex/metrica/impl/au;->n:Lcom/yandex/metrica/impl/bb;

    invoke-virtual {v3}, Lcom/yandex/metrica/impl/bb;->q()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v0, v2, v3}, Lcom/yandex/metrica/impl/au;->a(Landroid/net/Uri$Builder;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2256
    const-string v0, "analytics_sdk_version"

    iget-object v2, p0, Lcom/yandex/metrica/impl/au;->c:Lcom/yandex/metrica/impl/bb;

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/bb;->d()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/yandex/metrica/impl/au;->n:Lcom/yandex/metrica/impl/bb;

    .line 2257
    invoke-virtual {v3}, Lcom/yandex/metrica/impl/bb;->d()Ljava/lang/String;

    move-result-object v3

    .line 2256
    invoke-static {v1, v0, v2, v3}, Lcom/yandex/metrica/impl/au;->a(Landroid/net/Uri$Builder;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2258
    const-string v0, "client_analytics_sdk_version"

    iget-object v2, p0, Lcom/yandex/metrica/impl/au;->c:Lcom/yandex/metrica/impl/bb;

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/bb;->e()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/yandex/metrica/impl/au;->n:Lcom/yandex/metrica/impl/bb;

    .line 2259
    invoke-virtual {v3}, Lcom/yandex/metrica/impl/bb;->e()Ljava/lang/String;

    move-result-object v3

    .line 2258
    invoke-static {v1, v0, v2, v3}, Lcom/yandex/metrica/impl/au;->a(Landroid/net/Uri$Builder;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2260
    const-string v0, "app_version_name"

    iget-object v2, p0, Lcom/yandex/metrica/impl/au;->c:Lcom/yandex/metrica/impl/bb;

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/bb;->n()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/yandex/metrica/impl/au;->n:Lcom/yandex/metrica/impl/bb;

    invoke-virtual {v3}, Lcom/yandex/metrica/impl/bb;->n()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v0, v2, v3}, Lcom/yandex/metrica/impl/au;->a(Landroid/net/Uri$Builder;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2261
    const-string v0, "app_build_number"

    iget-object v2, p0, Lcom/yandex/metrica/impl/au;->c:Lcom/yandex/metrica/impl/bb;

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/bb;->m()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/yandex/metrica/impl/au;->n:Lcom/yandex/metrica/impl/bb;

    invoke-virtual {v3}, Lcom/yandex/metrica/impl/bb;->m()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v0, v2, v3}, Lcom/yandex/metrica/impl/au;->a(Landroid/net/Uri$Builder;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2262
    const-string v0, "os_version"

    iget-object v2, p0, Lcom/yandex/metrica/impl/au;->c:Lcom/yandex/metrica/impl/bb;

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/bb;->k()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/yandex/metrica/impl/au;->n:Lcom/yandex/metrica/impl/bb;

    invoke-virtual {v3}, Lcom/yandex/metrica/impl/bb;->k()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v0, v2, v3}, Lcom/yandex/metrica/impl/au;->a(Landroid/net/Uri$Builder;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2263
    iget-object v0, p0, Lcom/yandex/metrica/impl/au;->c:Lcom/yandex/metrica/impl/bb;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/bb;->l()I

    move-result v0

    if-lez v0, :cond_0

    .line 2264
    iget-object v0, p0, Lcom/yandex/metrica/impl/au;->c:Lcom/yandex/metrica/impl/bb;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/bb;->l()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 2265
    const-string v2, "os_api_level"

    invoke-virtual {v1, v2, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2267
    :cond_0
    const-string v0, "analytics_sdk_build_number"

    iget-object v2, p0, Lcom/yandex/metrica/impl/au;->c:Lcom/yandex/metrica/impl/bb;

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/bb;->g()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v0, v2}, Lcom/yandex/metrica/impl/au;->a(Landroid/net/Uri$Builder;Ljava/lang/String;Ljava/lang/String;)V

    .line 2268
    const-string v0, "analytics_sdk_build_type"

    iget-object v2, p0, Lcom/yandex/metrica/impl/au;->c:Lcom/yandex/metrica/impl/bb;

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/bb;->h()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v0, v2}, Lcom/yandex/metrica/impl/au;->a(Landroid/net/Uri$Builder;Ljava/lang/String;Ljava/lang/String;)V

    .line 2269
    const-string v0, "app_debuggable"

    iget-object v2, p0, Lcom/yandex/metrica/impl/au;->c:Lcom/yandex/metrica/impl/bb;

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/bb;->T()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v0, v2}, Lcom/yandex/metrica/impl/au;->a(Landroid/net/Uri$Builder;Ljava/lang/String;Ljava/lang/String;)V

    .line 2270
    const-string v0, "locale"

    iget-object v2, p0, Lcom/yandex/metrica/impl/au;->c:Lcom/yandex/metrica/impl/bb;

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/bb;->y()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/yandex/metrica/impl/au;->n:Lcom/yandex/metrica/impl/bb;

    invoke-virtual {v3}, Lcom/yandex/metrica/impl/bb;->y()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v0, v2, v3}, Lcom/yandex/metrica/impl/au;->a(Landroid/net/Uri$Builder;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2271
    const-string v0, "is_rooted"

    iget-object v2, p0, Lcom/yandex/metrica/impl/au;->c:Lcom/yandex/metrica/impl/bb;

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/bb;->r()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/yandex/metrica/impl/au;->n:Lcom/yandex/metrica/impl/bb;

    invoke-virtual {v3}, Lcom/yandex/metrica/impl/bb;->r()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v0, v2, v3}, Lcom/yandex/metrica/impl/au;->a(Landroid/net/Uri$Builder;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2272
    const-string v0, "app_framework"

    iget-object v2, p0, Lcom/yandex/metrica/impl/au;->c:Lcom/yandex/metrica/impl/bb;

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/bb;->s()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/yandex/metrica/impl/au;->n:Lcom/yandex/metrica/impl/bb;

    invoke-virtual {v3}, Lcom/yandex/metrica/impl/bb;->s()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v0, v2, v3}, Lcom/yandex/metrica/impl/au;->a(Landroid/net/Uri$Builder;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2275
    iget-object v0, p0, Lcom/yandex/metrica/impl/au;->n:Lcom/yandex/metrica/impl/bb;

    .line 2277
    invoke-virtual {v0}, Lcom/yandex/metrica/impl/bb;->f()I

    move-result v0

    const/16 v2, 0xc8

    if-lt v0, v2, :cond_2

    const-string v0, "api_key_128"

    .line 2280
    :goto_0
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/au;->E()Ljava/lang/String;

    move-result-object v2

    .line 2275
    invoke-virtual {v1, v0, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2281
    const-string v0, "app_id"

    iget-object v2, p0, Lcom/yandex/metrica/impl/au;->p:Lcom/yandex/metrica/impl/ob/v;

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/ob/v;->m()Lcom/yandex/metrica/impl/ob/t;

    move-result-object v2

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/ob/t;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2282
    const-string v0, "app_platform"

    iget-object v2, p0, Lcom/yandex/metrica/impl/au;->n:Lcom/yandex/metrica/impl/bb;

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/bb;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2283
    const-string v0, "model"

    iget-object v2, p0, Lcom/yandex/metrica/impl/au;->n:Lcom/yandex/metrica/impl/bb;

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/bb;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2284
    const-string v0, "manufacturer"

    iget-object v2, p0, Lcom/yandex/metrica/impl/au;->n:Lcom/yandex/metrica/impl/bb;

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/bb;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2285
    const-string v0, "screen_width"

    iget-object v2, p0, Lcom/yandex/metrica/impl/au;->n:Lcom/yandex/metrica/impl/bb;

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/bb;->u()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2286
    const-string v0, "screen_height"

    iget-object v2, p0, Lcom/yandex/metrica/impl/au;->n:Lcom/yandex/metrica/impl/bb;

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/bb;->v()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2287
    const-string v0, "screen_dpi"

    iget-object v2, p0, Lcom/yandex/metrica/impl/au;->n:Lcom/yandex/metrica/impl/bb;

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/bb;->w()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2288
    const-string v0, "scalefactor"

    iget-object v2, p0, Lcom/yandex/metrica/impl/au;->n:Lcom/yandex/metrica/impl/bb;

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/bb;->x()F

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2289
    const-string v0, "device_type"

    iget-object v2, p0, Lcom/yandex/metrica/impl/au;->n:Lcom/yandex/metrica/impl/bb;

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/bb;->A()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2290
    const-string v0, "android_id"

    iget-object v2, p0, Lcom/yandex/metrica/impl/au;->n:Lcom/yandex/metrica/impl/bb;

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/bb;->z()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2291
    const-string v0, "clids_set"

    iget-object v2, p0, Lcom/yandex/metrica/impl/au;->n:Lcom/yandex/metrica/impl/bb;

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/bb;->K()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v0, v2}, Lcom/yandex/metrica/impl/au;->a(Landroid/net/Uri$Builder;Ljava/lang/String;Ljava/lang/String;)V

    .line 2292
    iget-object v0, p0, Lcom/yandex/metrica/impl/au;->n:Lcom/yandex/metrica/impl/bb;

    iget-object v2, p0, Lcom/yandex/metrica/impl/au;->p:Lcom/yandex/metrica/impl/ob/v;

    .line 2293
    invoke-virtual {v2}, Lcom/yandex/metrica/impl/ob/v;->n()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/yandex/metrica/impl/bb;->a(Landroid/content/Context;)Lcom/yandex/metrica/impl/GoogleAdvertisingIdGetter$a;

    move-result-object v2

    .line 2294
    if-nez v2, :cond_3

    const-string v0, ""

    .line 2295
    :goto_1
    const-string v3, "adv_id"

    if-nez v0, :cond_1

    const-string v0, ""

    :cond_1
    invoke-virtual {v1, v3, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 2296
    const-string v3, "limit_ad_tracking"

    if-nez v2, :cond_4

    const-string v0, ""

    :goto_2
    invoke-virtual {v1, v3, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 95
    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/au;->a(Ljava/lang/String;)V

    .line 96
    return-void

    .line 2277
    :cond_2
    const-string v0, "api_key"

    goto/16 :goto_0

    .line 2294
    :cond_3
    iget-object v0, v2, Lcom/yandex/metrica/impl/GoogleAdvertisingIdGetter$a;->a:Ljava/lang/String;

    goto :goto_1

    .line 2296
    :cond_4
    iget-object v0, v2, Lcom/yandex/metrica/impl/GoogleAdvertisingIdGetter$a;->b:Ljava/lang/Boolean;

    .line 2297
    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/au;->a(Ljava/lang/Boolean;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method C()[Lcom/yandex/metrica/c$c$c;
    .locals 5

    .prologue
    .line 216
    iget-object v0, p0, Lcom/yandex/metrica/impl/au;->p:Lcom/yandex/metrica/impl/ob/v;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/v;->n()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/yandex/metrica/impl/aq;->a(Landroid/content/Context;)[Lcom/yandex/metrica/c$c$c;

    move-result-object v1

    .line 217
    if-eqz v1, :cond_0

    .line 218
    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 219
    iget v4, p0, Lcom/yandex/metrica/impl/au;->r:I

    invoke-static {v3}, Lcom/yandex/metrica/impl/ob/b;->b(Lcom/yandex/metrica/impl/ob/d;)I

    move-result v3

    add-int/2addr v3, v4

    iput v3, p0, Lcom/yandex/metrica/impl/au;->r:I

    .line 218
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 222
    :cond_0
    return-object v1
.end method

.method protected D()Lcom/yandex/metrica/impl/au$c;
    .locals 13

    .prologue
    const/4 v0, 0x0

    .line 374
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 375
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 377
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 381
    :try_start_0
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/au;->F()Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    move-object v12, v1

    move-object v1, v0

    move-object v0, v12

    .line 383
    :cond_0
    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 384
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 385
    invoke-static {v2, v3}, Lcom/yandex/metrica/impl/utils/e;->a(Landroid/database/Cursor;Landroid/content/ContentValues;)V

    .line 386
    const-string v6, "id"

    invoke-virtual {v3, v6}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 387
    const-string v8, "type"

    invoke-virtual {v3, v8}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v8

    invoke-static {v8}, Lcom/yandex/metrica/impl/ob/bp;->a(Ljava/lang/Integer;)Lcom/yandex/metrica/impl/ob/bp;

    move-result-object v8

    .line 390
    invoke-virtual {p0, v6, v7}, Lcom/yandex/metrica/impl/au;->a(J)Z

    move-result v9

    if-nez v9, :cond_0

    .line 395
    invoke-static {v3}, Lcom/yandex/metrica/impl/aq;->a(Landroid/content/ContentValues;)Lcom/yandex/metrica/c$c$f;

    move-result-object v3

    .line 396
    invoke-static {v8}, Lcom/yandex/metrica/impl/aq;->a(Lcom/yandex/metrica/impl/ob/bp;)I

    move-result v8

    .line 399
    iget-object v9, p0, Lcom/yandex/metrica/impl/au;->n:Lcom/yandex/metrica/impl/bb;

    .line 400
    invoke-virtual {v9}, Lcom/yandex/metrica/impl/bb;->y()Ljava/lang/String;

    move-result-object v9

    .line 399
    invoke-static {v9, v8, v3}, Lcom/yandex/metrica/impl/aq;->a(Ljava/lang/String;ILcom/yandex/metrica/c$c$f;)Lcom/yandex/metrica/c$c$d$b;

    move-result-object v3

    .line 404
    iget v8, p0, Lcom/yandex/metrica/impl/au;->r:I

    const/4 v9, 0x1

    const-wide v10, 0x7fffffffffffffffL

    invoke-static {v9, v10, v11}, Lcom/yandex/metrica/impl/ob/b;->c(IJ)I

    move-result v9

    add-int/2addr v8, v9

    iput v8, p0, Lcom/yandex/metrica/impl/au;->r:I

    .line 405
    iget v8, p0, Lcom/yandex/metrica/impl/au;->r:I

    const/4 v9, 0x2

    invoke-static {v9, v3}, Lcom/yandex/metrica/impl/ob/b;->b(ILcom/yandex/metrica/impl/ob/d;)I

    move-result v9

    add-int/2addr v8, v9

    iput v8, p0, Lcom/yandex/metrica/impl/au;->r:I

    .line 407
    iget v8, p0, Lcom/yandex/metrica/impl/au;->r:I

    const v9, 0x3d400

    if-ge v8, v9, :cond_1

    .line 411
    invoke-virtual {p0, v6, v7, v3}, Lcom/yandex/metrica/impl/au;->a(JLcom/yandex/metrica/c$c$d$b;)Lcom/yandex/metrica/impl/au$b;

    move-result-object v8

    .line 412
    if-eqz v8, :cond_0

    .line 413
    if-nez v1, :cond_2

    .line 414
    iget-object v1, v8, Lcom/yandex/metrica/impl/au$b;->b:Lcom/yandex/metrica/impl/a$a;

    .line 418
    :goto_0
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 419
    iget-object v3, v8, Lcom/yandex/metrica/impl/au$b;->a:Lcom/yandex/metrica/c$c$d;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 421
    :try_start_2
    new-instance v3, Lorg/json/JSONObject;

    iget-object v6, v8, Lcom/yandex/metrica/impl/au$b;->b:Lcom/yandex/metrica/impl/a$a;

    iget-object v6, v6, Lcom/yandex/metrica/impl/a$a;->a:Ljava/lang/String;

    invoke-direct {v3, v6}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-object v0, v3

    .line 425
    :goto_1
    :try_start_3
    iget-boolean v3, v8, Lcom/yandex/metrica/impl/au$b;->c:Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz v3, :cond_0

    .line 433
    :cond_1
    invoke-static {v2}, Lcom/yandex/metrica/impl/bl;->a(Landroid/database/Cursor;)V

    .line 436
    :goto_2
    new-instance v1, Lcom/yandex/metrica/impl/au$c;

    invoke-direct {v1, v4, v5, v0}, Lcom/yandex/metrica/impl/au$c;-><init>(Ljava/util/List;Ljava/util/List;Lorg/json/JSONObject;)V

    return-object v1

    .line 415
    :cond_2
    :try_start_4
    iget-object v3, v8, Lcom/yandex/metrica/impl/au$b;->b:Lcom/yandex/metrica/impl/a$a;

    invoke-virtual {v1, v3}, Lcom/yandex/metrica/impl/a$a;->equals(Ljava/lang/Object;)Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_0

    .line 433
    :catch_0
    move-exception v2

    move-object v12, v0

    move-object v0, v1

    move-object v1, v12

    :goto_3
    invoke-static {v1}, Lcom/yandex/metrica/impl/bl;->a(Landroid/database/Cursor;)V

    goto :goto_2

    :catchall_0
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    :goto_4
    invoke-static {v2}, Lcom/yandex/metrica/impl/bl;->a(Landroid/database/Cursor;)V

    .line 434
    throw v0

    .line 433
    :catchall_1
    move-exception v0

    goto :goto_4

    :catch_1
    move-exception v1

    move-object v1, v2

    goto :goto_3

    :catch_2
    move-exception v3

    goto :goto_1
.end method

.method protected E()Ljava/lang/String;
    .locals 1

    .prologue
    .line 560
    iget-object v0, p0, Lcom/yandex/metrica/impl/au;->n:Lcom/yandex/metrica/impl/bb;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/bb;->I()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected F()Landroid/database/Cursor;
    .locals 2

    .prologue
    .line 564
    iget-object v0, p0, Lcom/yandex/metrica/impl/au;->o:Lcom/yandex/metrica/impl/ob/co;

    iget-object v1, p0, Lcom/yandex/metrica/impl/au;->b:Ljava/util/Map;

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/co;->a(Ljava/util/Map;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method protected a(JLcom/yandex/metrica/impl/ob/bp;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 568
    iget-object v0, p0, Lcom/yandex/metrica/impl/au;->o:Lcom/yandex/metrica/impl/ob/co;

    invoke-virtual {v0, p1, p2, p3}, Lcom/yandex/metrica/impl/ob/co;->b(JLcom/yandex/metrica/impl/ob/bp;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method a(Lcom/yandex/metrica/impl/au$c;[Lcom/yandex/metrica/c$c$c;)Lcom/yandex/metrica/c$c;
    .locals 3

    .prologue
    .line 100
    new-instance v1, Lcom/yandex/metrica/c$c;

    invoke-direct {v1}, Lcom/yandex/metrica/c$c;-><init>()V

    .line 101
    invoke-virtual {p0, v1}, Lcom/yandex/metrica/impl/au;->a(Lcom/yandex/metrica/c$c;)V

    .line 102
    iget-object v0, p1, Lcom/yandex/metrica/impl/au$c;->a:Ljava/util/List;

    iget-object v2, p1, Lcom/yandex/metrica/impl/au$c;->a:Ljava/util/List;

    .line 103
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Lcom/yandex/metrica/c$c$d;

    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/yandex/metrica/c$c$d;

    iput-object v0, v1, Lcom/yandex/metrica/c$c;->c:[Lcom/yandex/metrica/c$c$d;

    .line 104
    iget-object v0, p1, Lcom/yandex/metrica/impl/au$c;->c:Lorg/json/JSONObject;

    invoke-static {v0}, Lcom/yandex/metrica/impl/au;->a(Lorg/json/JSONObject;)[Lcom/yandex/metrica/c$c$a;

    move-result-object v0

    iput-object v0, v1, Lcom/yandex/metrica/c$c;->d:[Lcom/yandex/metrica/c$c$a;

    .line 105
    iput-object p2, v1, Lcom/yandex/metrica/c$c;->e:[Lcom/yandex/metrica/c$c$c;

    .line 106
    iget v0, p0, Lcom/yandex/metrica/impl/au;->r:I

    const/16 v2, 0x8

    invoke-static {v2}, Lcom/yandex/metrica/impl/ob/b;->g(I)I

    move-result v2

    add-int/2addr v0, v2

    iput v0, p0, Lcom/yandex/metrica/impl/au;->r:I

    .line 107
    return-object v1
.end method

.method protected a(JLcom/yandex/metrica/c$c$d$b;)Lcom/yandex/metrica/impl/au$b;
    .locals 15

    .prologue
    .line 463
    new-instance v7, Lcom/yandex/metrica/c$c$d;

    invoke-direct {v7}, Lcom/yandex/metrica/c$c$d;-><init>()V

    .line 464
    move-wide/from16 v0, p1

    iput-wide v0, v7, Lcom/yandex/metrica/c$c$d;->b:J

    .line 465
    move-object/from16 v0, p3

    iput-object v0, v7, Lcom/yandex/metrica/c$c$d;->c:Lcom/yandex/metrica/c$c$d$b;

    .line 466
    const/4 v4, 0x0

    .line 467
    move-object/from16 v0, p3

    iget v2, v0, Lcom/yandex/metrica/c$c$d$b;->d:I

    invoke-static {v2}, Lcom/yandex/metrica/impl/aq;->a(I)Lcom/yandex/metrica/impl/ob/bp;

    move-result-object v5

    .line 468
    const/4 v2, 0x0

    .line 469
    const/4 v3, 0x0

    .line 472
    :try_start_0
    move-wide/from16 v0, p1

    invoke-virtual {p0, v0, v1, v5}, Lcom/yandex/metrica/impl/au;->a(JLcom/yandex/metrica/impl/ob/bp;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    .line 474
    :try_start_1
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 476
    :cond_0
    :goto_0
    :try_start_2
    invoke-interface {v5}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 477
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 478
    invoke-static {v5, v9}, Lcom/yandex/metrica/impl/utils/e;->a(Landroid/database/Cursor;Landroid/content/ContentValues;)V

    .line 3550
    const/4 v4, 0x0

    .line 4530
    const-string v6, "type"

    invoke-virtual {v9, v6}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    .line 4532
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iget-boolean v10, p0, Lcom/yandex/metrica/impl/au;->v:Z

    invoke-static {v6, v10}, Lcom/yandex/metrica/impl/aq$b;->a(IZ)Lcom/yandex/metrica/impl/aq$b;

    move-result-object v6

    const-string v10, "custom_type"

    .line 4533
    invoke-virtual {v9, v10}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v6, v10}, Lcom/yandex/metrica/impl/aq$b;->b(Ljava/lang/Integer;)Lcom/yandex/metrica/impl/aq$b;

    move-result-object v6

    const-string v10, "name"

    .line 4534
    invoke-virtual {v9, v10}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10}, Lcom/yandex/metrica/impl/aq$b;->a(Ljava/lang/String;)Lcom/yandex/metrica/impl/aq$b;

    move-result-object v6

    const-string v10, "value"

    .line 4535
    invoke-virtual {v9, v10}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10}, Lcom/yandex/metrica/impl/aq$b;->b(Ljava/lang/String;)Lcom/yandex/metrica/impl/aq$b;

    move-result-object v6

    const-string v10, "time"

    .line 4536
    invoke-virtual {v9, v10}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-virtual {v6, v10, v11}, Lcom/yandex/metrica/impl/aq$b;->a(J)Lcom/yandex/metrica/impl/aq$b;

    move-result-object v6

    const-string v10, "number"

    .line 4537
    invoke-virtual {v9, v10}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v10

    invoke-virtual {v6, v10}, Lcom/yandex/metrica/impl/aq$b;->a(I)Lcom/yandex/metrica/impl/aq$b;

    move-result-object v6

    const-string v10, "cell_info"

    .line 4538
    invoke-virtual {v9, v10}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10}, Lcom/yandex/metrica/impl/aq$b;->e(Ljava/lang/String;)Lcom/yandex/metrica/impl/aq$b;

    move-result-object v6

    const-string v10, "location_info"

    .line 4539
    invoke-virtual {v9, v10}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10}, Lcom/yandex/metrica/impl/aq$b;->c(Ljava/lang/String;)Lcom/yandex/metrica/impl/aq$b;

    move-result-object v6

    const-string v10, "wifi_network_info"

    .line 4540
    invoke-virtual {v9, v10}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10}, Lcom/yandex/metrica/impl/aq$b;->d(Ljava/lang/String;)Lcom/yandex/metrica/impl/aq$b;

    move-result-object v6

    const-string v10, "error_environment"

    .line 4541
    invoke-virtual {v9, v10}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10}, Lcom/yandex/metrica/impl/aq$b;->g(Ljava/lang/String;)Lcom/yandex/metrica/impl/aq$b;

    move-result-object v6

    const-string v10, "user_info"

    .line 4542
    invoke-virtual {v9, v10}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10}, Lcom/yandex/metrica/impl/aq$b;->h(Ljava/lang/String;)Lcom/yandex/metrica/impl/aq$b;

    move-result-object v6

    const-string v10, "truncated"

    .line 4543
    invoke-virtual {v9, v10}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v10

    invoke-virtual {v6, v10}, Lcom/yandex/metrica/impl/aq$b;->b(I)Lcom/yandex/metrica/impl/aq$b;

    move-result-object v6

    const-string v10, "connection_type"

    .line 4544
    invoke-virtual {v9, v10}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v10

    invoke-virtual {v6, v10}, Lcom/yandex/metrica/impl/aq$b;->c(I)Lcom/yandex/metrica/impl/aq$b;

    move-result-object v6

    const-string v10, "cellular_connection_type"

    .line 4545
    invoke-virtual {v9, v10}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10}, Lcom/yandex/metrica/impl/aq$b;->i(Ljava/lang/String;)Lcom/yandex/metrica/impl/aq$b;

    move-result-object v6

    const-string v10, "wifi_access_point"

    .line 4546
    invoke-virtual {v9, v10}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v6, v10}, Lcom/yandex/metrica/impl/aq$b;->f(Ljava/lang/String;)Lcom/yandex/metrica/impl/aq$b;

    move-result-object v6

    .line 3553
    invoke-virtual {v6}, Lcom/yandex/metrica/impl/aq$b;->c()Ljava/lang/Integer;

    move-result-object v10

    if-eqz v10, :cond_5

    .line 3554
    invoke-virtual {v6}, Lcom/yandex/metrica/impl/aq$b;->e()Lcom/yandex/metrica/c$c$d$a;

    move-result-object v4

    move-object v6, v4

    .line 480
    :goto_1
    if-eqz v6, :cond_0

    .line 5440
    new-instance v4, Lcom/yandex/metrica/impl/a$a;

    const-string v10, "app_environment"

    .line 5441
    invoke-virtual {v9, v10}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const-string v11, "app_environment_revision"

    .line 5442
    invoke-virtual {v9, v11}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    invoke-direct {v4, v10, v12, v13}, Lcom/yandex/metrica/impl/a$a;-><init>(Ljava/lang/String;J)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 482
    if-nez v2, :cond_3

    .line 484
    :try_start_3
    iget v2, p0, Lcom/yandex/metrica/impl/au;->s:I

    if-gez v2, :cond_7

    .line 485
    invoke-static {v4}, Lcom/yandex/metrica/impl/au;->a(Lcom/yandex/metrica/impl/a$a;)I

    move-result v2

    iput v2, p0, Lcom/yandex/metrica/impl/au;->s:I

    .line 486
    iget v2, p0, Lcom/yandex/metrica/impl/au;->r:I

    iget v9, p0, Lcom/yandex/metrica/impl/au;->s:I

    add-int/2addr v2, v9

    iput v2, p0, Lcom/yandex/metrica/impl/au;->r:I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-object v2, v4

    .line 5522
    :cond_1
    :goto_2
    :try_start_4
    iget-object v4, p0, Lcom/yandex/metrica/impl/au;->u:Lcom/yandex/metrica/impl/utils/f;

    iget-object v9, v6, Lcom/yandex/metrica/c$c$d$a;->f:[B

    const v10, 0x3c000

    invoke-virtual {v4, v9, v10}, Lcom/yandex/metrica/impl/utils/f;->a([BI)[B

    move-result-object v4

    .line 5523
    iget-object v9, v6, Lcom/yandex/metrica/c$c$d$a;->f:[B

    invoke-virtual {v9, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_2

    .line 5524
    iput-object v4, v6, Lcom/yandex/metrica/c$c$d$a;->f:[B

    .line 5525
    iget v9, v6, Lcom/yandex/metrica/c$c$d$a;->k:I

    iget-object v10, v6, Lcom/yandex/metrica/c$c$d$a;->f:[B

    array-length v10, v10

    array-length v4, v4

    sub-int v4, v10, v4

    add-int/2addr v4, v9

    iput v4, v6, Lcom/yandex/metrica/c$c$d$a;->k:I

    .line 495
    :cond_2
    iget v4, p0, Lcom/yandex/metrica/impl/au;->r:I

    const/4 v9, 0x3

    invoke-static {v9, v6}, Lcom/yandex/metrica/impl/ob/b;->b(ILcom/yandex/metrica/impl/ob/d;)I

    move-result v9

    add-int/2addr v4, v9

    iput v4, p0, Lcom/yandex/metrica/impl/au;->r:I

    .line 497
    iget v4, p0, Lcom/yandex/metrica/impl/au;->r:I

    const v9, 0x3d400

    if-ge v4, v9, :cond_6

    .line 501
    invoke-interface {v8, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto/16 :goto_0

    .line 515
    :catch_0
    move-exception v4

    move-object v4, v5

    move-object v14, v2

    move v2, v3

    move-object v3, v14

    :goto_3
    invoke-static {v4}, Lcom/yandex/metrica/impl/bl;->a(Landroid/database/Cursor;)V

    .line 518
    :goto_4
    new-instance v4, Lcom/yandex/metrica/impl/au$b;

    invoke-direct {v4, v7, v3, v2}, Lcom/yandex/metrica/impl/au$b;-><init>(Lcom/yandex/metrica/c$c$d;Lcom/yandex/metrica/impl/a$a;Z)V

    move-object v2, v4

    :goto_5
    return-object v2

    .line 488
    :cond_3
    :try_start_5
    invoke-virtual {v2, v4}, Lcom/yandex/metrica/impl/a$a;->equals(Ljava/lang/Object;)Z
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-result v4

    if-nez v4, :cond_1

    .line 489
    const/4 v3, 0x1

    move-object v4, v2

    .line 505
    :goto_6
    :try_start_6
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_4

    .line 506
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Lcom/yandex/metrica/c$c$d$a;

    invoke-interface {v8, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lcom/yandex/metrica/c$c$d$a;

    iput-object v2, v7, Lcom/yandex/metrica/c$c$d;->d:[Lcom/yandex/metrica/c$c$d$a;
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 515
    invoke-static {v5}, Lcom/yandex/metrica/impl/bl;->a(Landroid/database/Cursor;)V

    move v2, v3

    move-object v3, v4

    .line 516
    goto :goto_4

    .line 515
    :cond_4
    invoke-static {v5}, Lcom/yandex/metrica/impl/bl;->a(Landroid/database/Cursor;)V

    .line 509
    const/4 v2, 0x0

    goto :goto_5

    .line 515
    :catchall_0
    move-exception v2

    move-object v5, v4

    :goto_7
    invoke-static {v5}, Lcom/yandex/metrica/impl/bl;->a(Landroid/database/Cursor;)V

    .line 516
    throw v2

    .line 515
    :catchall_1
    move-exception v2

    goto :goto_7

    :catch_1
    move-exception v5

    move v14, v3

    move-object v3, v2

    move v2, v14

    goto :goto_3

    :catch_2
    move-exception v4

    move-object v4, v5

    move-object v14, v2

    move v2, v3

    move-object v3, v14

    goto :goto_3

    :catch_3
    move-exception v2

    move v2, v3

    move-object v3, v4

    move-object v4, v5

    goto :goto_3

    :cond_5
    move-object v6, v4

    goto/16 :goto_1

    :cond_6
    move-object v4, v2

    goto :goto_6

    :cond_7
    move-object v2, v4

    goto/16 :goto_2
.end method

.method a(Lcom/yandex/metrica/c$c;)V
    .locals 2

    .prologue
    .line 111
    iget-object v0, p0, Lcom/yandex/metrica/impl/au;->p:Lcom/yandex/metrica/impl/ob/v;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/v;->n()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/yandex/metrica/impl/ob/gi;->a(Landroid/content/Context;)Lcom/yandex/metrica/impl/ob/gi;

    move-result-object v0

    new-instance v1, Lcom/yandex/metrica/impl/au$1;

    invoke-direct {v1, p0, p1}, Lcom/yandex/metrica/impl/au$1;-><init>(Lcom/yandex/metrica/impl/au;Lcom/yandex/metrica/c$c;)V

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/gi;->a(Lcom/yandex/metrica/impl/ob/gk;)V

    .line 147
    return-void
.end method

.method protected a(J)Z
    .locals 3

    .prologue
    .line 572
    const-wide/16 v0, -0x2

    cmp-long v0, v0, p1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 159
    iget-object v1, p0, Lcom/yandex/metrica/impl/au;->n:Lcom/yandex/metrica/impl/bb;

    iget-object v2, p0, Lcom/yandex/metrica/impl/au;->p:Lcom/yandex/metrica/impl/ob/v;

    invoke-virtual {v1, v2}, Lcom/yandex/metrica/impl/bb;->b(Lcom/yandex/metrica/impl/ob/v;)V

    .line 160
    iget-object v1, p0, Lcom/yandex/metrica/impl/au;->n:Lcom/yandex/metrica/impl/bb;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/bb;->B()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/yandex/metrica/impl/au;->a(Ljava/util/List;)V

    .line 161
    iget-object v1, p0, Lcom/yandex/metrica/impl/au;->n:Lcom/yandex/metrica/impl/bb;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/bb;->R()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/yandex/metrica/impl/au;->t()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lcom/yandex/metrica/impl/bl;->a(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 182
    :cond_0
    :goto_0
    return v0

    .line 165
    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/yandex/metrica/impl/au;->q:Ljava/util/List;

    .line 167
    iget-object v1, p0, Lcom/yandex/metrica/impl/au;->p:Lcom/yandex/metrica/impl/ob/v;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/v;->I()Z

    move-result v1

    iput-boolean v1, p0, Lcom/yandex/metrica/impl/au;->v:Z

    .line 170
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/au;->C()[Lcom/yandex/metrica/c$c$c;

    move-result-object v1

    .line 171
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/au;->D()Lcom/yandex/metrica/impl/au$c;

    move-result-object v2

    iput-object v2, p0, Lcom/yandex/metrica/impl/au;->t:Lcom/yandex/metrica/impl/au$c;

    .line 174
    iget-object v2, p0, Lcom/yandex/metrica/impl/au;->t:Lcom/yandex/metrica/impl/au$c;

    iget-object v2, v2, Lcom/yandex/metrica/impl/au$c;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 178
    iget-object v0, p0, Lcom/yandex/metrica/impl/au;->t:Lcom/yandex/metrica/impl/au$c;

    invoke-virtual {p0, v0, v1}, Lcom/yandex/metrica/impl/au;->a(Lcom/yandex/metrica/impl/au$c;[Lcom/yandex/metrica/c$c$c;)Lcom/yandex/metrica/c$c;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/impl/au;->m:Lcom/yandex/metrica/c$c;

    .line 180
    iget-object v0, p0, Lcom/yandex/metrica/impl/au;->t:Lcom/yandex/metrica/impl/au$c;

    iget-object v0, v0, Lcom/yandex/metrica/impl/au$c;->b:Ljava/util/List;

    iput-object v0, p0, Lcom/yandex/metrica/impl/au;->q:Ljava/util/List;

    .line 182
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public c()Z
    .locals 9

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 315
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/au;->p()Z

    move-result v0

    iput-boolean v0, p0, Lcom/yandex/metrica/impl/au;->k:Z

    .line 316
    iget-boolean v0, p0, Lcom/yandex/metrica/impl/au;->k:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/yandex/metrica/impl/au;->o()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v3

    .line 318
    :goto_0
    if-eqz v0, :cond_3

    .line 319
    iget-object v0, p0, Lcom/yandex/metrica/impl/au;->m:Lcom/yandex/metrica/c$c;

    iget-object v4, v0, Lcom/yandex/metrica/c$c;->c:[Lcom/yandex/metrica/c$c$d;

    move v1, v2

    .line 321
    :goto_1
    array-length v0, v4

    if-ge v1, v0, :cond_2

    .line 322
    aget-object v5, v4, v1

    .line 323
    iget-object v0, p0, Lcom/yandex/metrica/impl/au;->q:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 324
    iget-object v0, v5, Lcom/yandex/metrica/c$c$d;->c:Lcom/yandex/metrica/c$c$d$b;

    iget v0, v0, Lcom/yandex/metrica/c$c$d$b;->d:I

    invoke-static {v0}, Lcom/yandex/metrica/impl/aq;->a(I)Lcom/yandex/metrica/impl/ob/bp;

    move-result-object v0

    .line 326
    iget-object v8, p0, Lcom/yandex/metrica/impl/au;->o:Lcom/yandex/metrica/impl/ob/co;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/bp;->a()I

    move-result v0

    iget-object v5, v5, Lcom/yandex/metrica/c$c$d;->d:[Lcom/yandex/metrica/c$c$d$a;

    array-length v5, v5

    invoke-virtual {v8, v6, v7, v0, v5}, Lcom/yandex/metrica/impl/ob/co;->a(JII)V

    .line 327
    invoke-static {}, Lcom/yandex/metrica/impl/aq;->a()V

    .line 321
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    move v0, v2

    .line 316
    goto :goto_0

    .line 330
    :cond_2
    iget-object v0, p0, Lcom/yandex/metrica/impl/au;->p:Lcom/yandex/metrica/impl/ob/v;

    .line 331
    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/v;->a()Lcom/yandex/metrica/impl/ob/bl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/bl;->a()Lcom/yandex/metrica/impl/ob/bh;

    move-result-object v1

    .line 333
    if-nez v1, :cond_4

    new-array v0, v2, [J

    .line 336
    :goto_2
    iget-object v1, p0, Lcom/yandex/metrica/impl/au;->o:Lcom/yandex/metrica/impl/ob/co;

    invoke-virtual {v1, v0}, Lcom/yandex/metrica/impl/ob/co;->a([J)I

    .line 340
    :cond_3
    iget-boolean v0, p0, Lcom/yandex/metrica/impl/au;->k:Z

    return v0

    .line 333
    :cond_4
    new-array v0, v3, [J

    .line 335
    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/bh;->c()J

    move-result-wide v4

    aput-wide v4, v0, v2

    goto :goto_2
.end method

.method protected d()Lcom/yandex/metrica/impl/ob/es;
    .locals 2

    .prologue
    .line 345
    new-instance v0, Lcom/yandex/metrica/impl/ob/ev;

    invoke-direct {v0}, Lcom/yandex/metrica/impl/ob/ev;-><init>()V

    invoke-virtual {p0}, Lcom/yandex/metrica/impl/au;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/ev;->a(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/es;

    move-result-object v0

    return-object v0
.end method

.method public e()V
    .locals 6

    .prologue
    .line 187
    invoke-super {p0}, Lcom/yandex/metrica/impl/m;->e()V

    .line 190
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/au;->B()V

    .line 192
    iget-object v0, p0, Lcom/yandex/metrica/impl/au;->m:Lcom/yandex/metrica/c$c;

    .line 3029
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    .line 193
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 194
    invoke-static {}, Lcom/yandex/metrica/impl/utils/r;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 195
    invoke-static {}, Lcom/yandex/metrica/impl/utils/n;->a()Lcom/yandex/metrica/impl/utils/n;

    move-result-object v3

    invoke-virtual {v3}, Lcom/yandex/metrica/impl/utils/n;->d()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    .line 192
    invoke-static {v1, v2, v3}, Lcom/yandex/metrica/impl/aq;->a(Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;)Lcom/yandex/metrica/c$c$f;

    move-result-object v1

    iput-object v1, v0, Lcom/yandex/metrica/c$c;->b:Lcom/yandex/metrica/c$c$f;

    .line 197
    iget-object v0, p0, Lcom/yandex/metrica/impl/au;->m:Lcom/yandex/metrica/c$c;

    invoke-static {v0}, Lcom/yandex/metrica/impl/ob/d;->a(Lcom/yandex/metrica/impl/ob/d;)[B

    move-result-object v0

    .line 3204
    :try_start_0
    invoke-static {v0}, Lcom/yandex/metrica/impl/s;->a([B)[B

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/yandex/metrica/impl/au;->a([B)V

    .line 3205
    const-string v1, "gzip"

    invoke-virtual {p0, v1}, Lcom/yandex/metrica/impl/au;->b(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3211
    :goto_0
    return-void

    .line 3208
    :catch_0
    move-exception v1

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/au;->a([B)V

    .line 3209
    const-string v0, "identity"

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/au;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public f()V
    .locals 4

    .prologue
    .line 357
    iget-boolean v0, p0, Lcom/yandex/metrica/impl/au;->k:Z

    if-eqz v0, :cond_0

    .line 3365
    iget-object v0, p0, Lcom/yandex/metrica/impl/au;->p:Lcom/yandex/metrica/impl/ob/v;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/v;->q()Lcom/yandex/metrica/impl/utils/l;

    move-result-object v2

    .line 3366
    invoke-virtual {v2}, Lcom/yandex/metrica/impl/utils/l;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3367
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/au;->t:Lcom/yandex/metrica/impl/au$c;

    iget-object v0, v0, Lcom/yandex/metrica/impl/au$c;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 3368
    iget-object v0, p0, Lcom/yandex/metrica/impl/au;->t:Lcom/yandex/metrica/impl/au$c;

    iget-object v0, v0, Lcom/yandex/metrica/impl/au$c;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/yandex/metrica/c$c$d;

    const-string v3, "Event sent"

    invoke-virtual {v2, v0, v3}, Lcom/yandex/metrica/impl/utils/l;->a(Lcom/yandex/metrica/c$c$d;Ljava/lang/String;)V

    .line 3367
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 361
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/yandex/metrica/impl/au;->t:Lcom/yandex/metrica/impl/au$c;

    .line 362
    return-void
.end method

.method public u()Z
    .locals 3

    .prologue
    .line 350
    invoke-super {p0}, Lcom/yandex/metrica/impl/m;->u()Z

    move-result v1

    .line 351
    const/16 v0, 0x190

    invoke-virtual {p0}, Lcom/yandex/metrica/impl/au;->k()I

    move-result v2

    if-eq v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    and-int/2addr v0, v1

    .line 352
    return v0

    .line 351
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
