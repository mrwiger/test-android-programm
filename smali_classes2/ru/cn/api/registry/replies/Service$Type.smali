.class public final enum Lru/cn/api/registry/replies/Service$Type;
.super Ljava/lang/Enum;
.source "Service.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/api/registry/replies/Service;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lru/cn/api/registry/replies/Service$Type;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lru/cn/api/registry/replies/Service$Type;

.field public static final enum auth:Lru/cn/api/registry/replies/Service$Type;

.field public static final enum catalogue:Lru/cn/api/registry/replies/Service$Type;

.field public static final enum films:Lru/cn/api/registry/replies/Service$Type;

.field public static final enum iptv:Lru/cn/api/registry/replies/Service$Type;

.field public static final enum media_locator:Lru/cn/api/registry/replies/Service$Type;

.field public static final enum money_miner:Lru/cn/api/registry/replies/Service$Type;

.field public static final enum notification:Lru/cn/api/registry/replies/Service$Type;

.field public static final enum peers:Lru/cn/api/registry/replies/Service$Type;

.field public static final enum tracking:Lru/cn/api/registry/replies/Service$Type;

.field public static final enum tv_guide:Lru/cn/api/registry/replies/Service$Type;

.field public static final enum user_data:Lru/cn/api/registry/replies/Service$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 12
    new-instance v0, Lru/cn/api/registry/replies/Service$Type;

    const-string v1, "films"

    invoke-direct {v0, v1, v3}, Lru/cn/api/registry/replies/Service$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/api/registry/replies/Service$Type;->films:Lru/cn/api/registry/replies/Service$Type;

    .line 13
    new-instance v0, Lru/cn/api/registry/replies/Service$Type;

    const-string v1, "iptv"

    invoke-direct {v0, v1, v4}, Lru/cn/api/registry/replies/Service$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/api/registry/replies/Service$Type;->iptv:Lru/cn/api/registry/replies/Service$Type;

    .line 14
    new-instance v0, Lru/cn/api/registry/replies/Service$Type;

    const-string v1, "peers"

    invoke-direct {v0, v1, v5}, Lru/cn/api/registry/replies/Service$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/api/registry/replies/Service$Type;->peers:Lru/cn/api/registry/replies/Service$Type;

    .line 15
    new-instance v0, Lru/cn/api/registry/replies/Service$Type;

    const-string v1, "tv_guide"

    invoke-direct {v0, v1, v6}, Lru/cn/api/registry/replies/Service$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/api/registry/replies/Service$Type;->tv_guide:Lru/cn/api/registry/replies/Service$Type;

    .line 16
    new-instance v0, Lru/cn/api/registry/replies/Service$Type;

    const-string v1, "notification"

    invoke-direct {v0, v1, v7}, Lru/cn/api/registry/replies/Service$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/api/registry/replies/Service$Type;->notification:Lru/cn/api/registry/replies/Service$Type;

    .line 17
    new-instance v0, Lru/cn/api/registry/replies/Service$Type;

    const-string v1, "tracking"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lru/cn/api/registry/replies/Service$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/api/registry/replies/Service$Type;->tracking:Lru/cn/api/registry/replies/Service$Type;

    .line 18
    new-instance v0, Lru/cn/api/registry/replies/Service$Type;

    const-string v1, "auth"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lru/cn/api/registry/replies/Service$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/api/registry/replies/Service$Type;->auth:Lru/cn/api/registry/replies/Service$Type;

    .line 19
    new-instance v0, Lru/cn/api/registry/replies/Service$Type;

    const-string v1, "user_data"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lru/cn/api/registry/replies/Service$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/api/registry/replies/Service$Type;->user_data:Lru/cn/api/registry/replies/Service$Type;

    .line 20
    new-instance v0, Lru/cn/api/registry/replies/Service$Type;

    const-string v1, "media_locator"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lru/cn/api/registry/replies/Service$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/api/registry/replies/Service$Type;->media_locator:Lru/cn/api/registry/replies/Service$Type;

    .line 21
    new-instance v0, Lru/cn/api/registry/replies/Service$Type;

    const-string v1, "catalogue"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lru/cn/api/registry/replies/Service$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/api/registry/replies/Service$Type;->catalogue:Lru/cn/api/registry/replies/Service$Type;

    .line 22
    new-instance v0, Lru/cn/api/registry/replies/Service$Type;

    const-string v1, "money_miner"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lru/cn/api/registry/replies/Service$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/api/registry/replies/Service$Type;->money_miner:Lru/cn/api/registry/replies/Service$Type;

    .line 11
    const/16 v0, 0xb

    new-array v0, v0, [Lru/cn/api/registry/replies/Service$Type;

    sget-object v1, Lru/cn/api/registry/replies/Service$Type;->films:Lru/cn/api/registry/replies/Service$Type;

    aput-object v1, v0, v3

    sget-object v1, Lru/cn/api/registry/replies/Service$Type;->iptv:Lru/cn/api/registry/replies/Service$Type;

    aput-object v1, v0, v4

    sget-object v1, Lru/cn/api/registry/replies/Service$Type;->peers:Lru/cn/api/registry/replies/Service$Type;

    aput-object v1, v0, v5

    sget-object v1, Lru/cn/api/registry/replies/Service$Type;->tv_guide:Lru/cn/api/registry/replies/Service$Type;

    aput-object v1, v0, v6

    sget-object v1, Lru/cn/api/registry/replies/Service$Type;->notification:Lru/cn/api/registry/replies/Service$Type;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lru/cn/api/registry/replies/Service$Type;->tracking:Lru/cn/api/registry/replies/Service$Type;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lru/cn/api/registry/replies/Service$Type;->auth:Lru/cn/api/registry/replies/Service$Type;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lru/cn/api/registry/replies/Service$Type;->user_data:Lru/cn/api/registry/replies/Service$Type;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lru/cn/api/registry/replies/Service$Type;->media_locator:Lru/cn/api/registry/replies/Service$Type;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lru/cn/api/registry/replies/Service$Type;->catalogue:Lru/cn/api/registry/replies/Service$Type;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lru/cn/api/registry/replies/Service$Type;->money_miner:Lru/cn/api/registry/replies/Service$Type;

    aput-object v2, v0, v1

    sput-object v0, Lru/cn/api/registry/replies/Service$Type;->$VALUES:[Lru/cn/api/registry/replies/Service$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 11
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lru/cn/api/registry/replies/Service$Type;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 11
    const-class v0, Lru/cn/api/registry/replies/Service$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lru/cn/api/registry/replies/Service$Type;

    return-object v0
.end method

.method public static values()[Lru/cn/api/registry/replies/Service$Type;
    .locals 1

    .prologue
    .line 11
    sget-object v0, Lru/cn/api/registry/replies/Service$Type;->$VALUES:[Lru/cn/api/registry/replies/Service$Type;

    invoke-virtual {v0}, [Lru/cn/api/registry/replies/Service$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lru/cn/api/registry/replies/Service$Type;

    return-object v0
.end method
