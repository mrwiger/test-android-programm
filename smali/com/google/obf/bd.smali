.class final Lcom/google/obf/bd;
.super Ljava/lang/Object;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/obf/bd$f;,
        Lcom/google/obf/bd$e;,
        Lcom/google/obf/bd$c;,
        Lcom/google/obf/bd$a;,
        Lcom/google/obf/bd$d;,
        Lcom/google/obf/bd$g;,
        Lcom/google/obf/bd$b;
    }
.end annotation


# static fields
.field private static final a:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 658
    const-string v0, "cenc"

    invoke-static {v0}, Lcom/google/obf/ea;->c(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/google/obf/bd;->a:I

    return-void
.end method

.method private static a(Lcom/google/obf/dw;II)I
    .locals 4

    .prologue
    .line 540
    invoke-virtual {p0}, Lcom/google/obf/dw;->d()I

    move-result v1

    .line 541
    :goto_0
    sub-int v0, v1, p1

    if-ge v0, p2, :cond_2

    .line 542
    invoke-virtual {p0, v1}, Lcom/google/obf/dw;->c(I)V

    .line 543
    invoke-virtual {p0}, Lcom/google/obf/dw;->m()I

    move-result v2

    .line 544
    if-lez v2, :cond_0

    const/4 v0, 0x1

    :goto_1
    const-string v3, "childAtomSize should be positive"

    invoke-static {v0, v3}, Lcom/google/obf/dl;->a(ZLjava/lang/Object;)V

    .line 545
    invoke-virtual {p0}, Lcom/google/obf/dw;->m()I

    move-result v0

    .line 546
    sget v3, Lcom/google/obf/bc;->J:I

    if-ne v0, v3, :cond_1

    move v0, v1

    .line 550
    :goto_2
    return v0

    .line 544
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 548
    :cond_1
    add-int/2addr v1, v2

    .line 549
    goto :goto_0

    .line 550
    :cond_2
    const/4 v0, -0x1

    goto :goto_2
.end method

.method private static a(Lcom/google/obf/dw;IILcom/google/obf/bd$d;I)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 591
    invoke-virtual {p0}, Lcom/google/obf/dw;->d()I

    move-result v0

    move v2, v0

    .line 592
    :goto_0
    sub-int v0, v2, p1

    if-ge v0, p2, :cond_0

    .line 593
    invoke-virtual {p0, v2}, Lcom/google/obf/dw;->c(I)V

    .line 594
    invoke-virtual {p0}, Lcom/google/obf/dw;->m()I

    move-result v3

    .line 595
    if-lez v3, :cond_1

    const/4 v0, 0x1

    :goto_1
    const-string v4, "childAtomSize should be positive"

    invoke-static {v0, v4}, Lcom/google/obf/dl;->a(ZLjava/lang/Object;)V

    .line 596
    invoke-virtual {p0}, Lcom/google/obf/dw;->m()I

    move-result v0

    .line 597
    sget v4, Lcom/google/obf/bc;->V:I

    if-ne v0, v4, :cond_2

    .line 598
    invoke-static {p0, v2, v3}, Lcom/google/obf/bd;->b(Lcom/google/obf/dw;II)Landroid/util/Pair;

    move-result-object v4

    .line 599
    if-eqz v4, :cond_2

    .line 600
    iget-object v1, p3, Lcom/google/obf/bd$d;->a:[Lcom/google/obf/bl;

    iget-object v0, v4, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/google/obf/bl;

    aput-object v0, v1, p4

    .line 601
    iget-object v0, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 604
    :cond_0
    return v1

    :cond_1
    move v0, v1

    .line 595
    goto :goto_1

    .line 602
    :cond_2
    add-int v0, v2, v3

    move v2, v0

    .line 603
    goto :goto_0
.end method

.method private static a(Lcom/google/obf/bc$a;)Landroid/util/Pair;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/obf/bc$a;",
            ")",
            "Landroid/util/Pair",
            "<[J[J>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v8, 0x1

    .line 439
    if-eqz p0, :cond_0

    sget v0, Lcom/google/obf/bc;->Q:I

    invoke-virtual {p0, v0}, Lcom/google/obf/bc$a;->d(I)Lcom/google/obf/bc$b;

    move-result-object v0

    if-nez v0, :cond_1

    .line 440
    :cond_0
    invoke-static {v1, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    .line 457
    :goto_0
    return-object v0

    .line 441
    :cond_1
    iget-object v3, v0, Lcom/google/obf/bc$b;->aP:Lcom/google/obf/dw;

    .line 442
    const/16 v0, 0x8

    invoke-virtual {v3, v0}, Lcom/google/obf/dw;->c(I)V

    .line 443
    invoke-virtual {v3}, Lcom/google/obf/dw;->m()I

    move-result v0

    .line 444
    invoke-static {v0}, Lcom/google/obf/bc;->a(I)I

    move-result v4

    .line 445
    invoke-virtual {v3}, Lcom/google/obf/dw;->s()I

    move-result v5

    .line 446
    new-array v6, v5, [J

    .line 447
    new-array v7, v5, [J

    .line 448
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v5, :cond_5

    .line 450
    if-ne v4, v8, :cond_2

    invoke-virtual {v3}, Lcom/google/obf/dw;->u()J

    move-result-wide v0

    :goto_2
    aput-wide v0, v6, v2

    .line 451
    if-ne v4, v8, :cond_3

    invoke-virtual {v3}, Lcom/google/obf/dw;->o()J

    move-result-wide v0

    :goto_3
    aput-wide v0, v7, v2

    .line 452
    invoke-virtual {v3}, Lcom/google/obf/dw;->i()S

    move-result v0

    .line 453
    if-eq v0, v8, :cond_4

    .line 454
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unsupported media rate."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 450
    :cond_2
    invoke-virtual {v3}, Lcom/google/obf/dw;->k()J

    move-result-wide v0

    goto :goto_2

    .line 451
    :cond_3
    invoke-virtual {v3}, Lcom/google/obf/dw;->m()I

    move-result v0

    int-to-long v0, v0

    goto :goto_3

    .line 455
    :cond_4
    const/4 v0, 0x2

    invoke-virtual {v3, v0}, Lcom/google/obf/dw;->d(I)V

    .line 456
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 457
    :cond_5
    invoke-static {v6, v7}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/google/obf/bc$b;Z)Lcom/google/obf/an;
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/16 v5, 0x8

    .line 183
    if-eqz p1, :cond_1

    .line 196
    :cond_0
    :goto_0
    return-object v0

    .line 185
    :cond_1
    iget-object v1, p0, Lcom/google/obf/bc$b;->aP:Lcom/google/obf/dw;

    .line 186
    invoke-virtual {v1, v5}, Lcom/google/obf/dw;->c(I)V

    .line 187
    :goto_1
    invoke-virtual {v1}, Lcom/google/obf/dw;->b()I

    move-result v2

    if-lt v2, v5, :cond_0

    .line 188
    invoke-virtual {v1}, Lcom/google/obf/dw;->m()I

    move-result v2

    .line 189
    invoke-virtual {v1}, Lcom/google/obf/dw;->m()I

    move-result v3

    .line 190
    sget v4, Lcom/google/obf/bc;->aA:I

    if-ne v3, v4, :cond_2

    .line 191
    invoke-virtual {v1}, Lcom/google/obf/dw;->d()I

    move-result v0

    add-int/lit8 v0, v0, -0x8

    invoke-virtual {v1, v0}, Lcom/google/obf/dw;->c(I)V

    .line 192
    invoke-virtual {v1}, Lcom/google/obf/dw;->d()I

    move-result v0

    add-int/2addr v0, v2

    invoke-virtual {v1, v0}, Lcom/google/obf/dw;->b(I)V

    .line 193
    invoke-static {v1}, Lcom/google/obf/bd;->a(Lcom/google/obf/dw;)Lcom/google/obf/an;

    move-result-object v0

    goto :goto_0

    .line 194
    :cond_2
    add-int/lit8 v2, v2, -0x8

    invoke-virtual {v1, v2}, Lcom/google/obf/dw;->d(I)V

    goto :goto_1
.end method

.method private static a(Lcom/google/obf/dw;)Lcom/google/obf/an;
    .locals 4

    .prologue
    .line 197
    const/16 v0, 0xc

    invoke-virtual {p0, v0}, Lcom/google/obf/dw;->d(I)V

    .line 198
    new-instance v1, Lcom/google/obf/dw;

    invoke-direct {v1}, Lcom/google/obf/dw;-><init>()V

    .line 199
    :goto_0
    invoke-virtual {p0}, Lcom/google/obf/dw;->b()I

    move-result v0

    const/16 v2, 0x8

    if-lt v0, v2, :cond_1

    .line 200
    invoke-virtual {p0}, Lcom/google/obf/dw;->m()I

    move-result v0

    add-int/lit8 v2, v0, -0x8

    .line 201
    invoke-virtual {p0}, Lcom/google/obf/dw;->m()I

    move-result v0

    .line 202
    sget v3, Lcom/google/obf/bc;->aB:I

    if-ne v0, v3, :cond_0

    .line 203
    iget-object v0, p0, Lcom/google/obf/dw;->a:[B

    invoke-virtual {p0}, Lcom/google/obf/dw;->d()I

    move-result v3

    add-int/2addr v3, v2

    invoke-virtual {v1, v0, v3}, Lcom/google/obf/dw;->a([BI)V

    .line 204
    invoke-virtual {p0}, Lcom/google/obf/dw;->d()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/google/obf/dw;->c(I)V

    .line 205
    invoke-static {v1}, Lcom/google/obf/bd;->b(Lcom/google/obf/dw;)Lcom/google/obf/an;

    move-result-object v0

    .line 206
    if-eqz v0, :cond_0

    .line 210
    :goto_1
    return-object v0

    .line 208
    :cond_0
    invoke-virtual {p0, v2}, Lcom/google/obf/dw;->d(I)V

    goto :goto_0

    .line 210
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static a(Lcom/google/obf/dw;I)Lcom/google/obf/bd$a;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 389
    add-int/lit8 v0, p1, 0x8

    add-int/lit8 v0, v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/obf/dw;->c(I)V

    .line 390
    invoke-virtual {p0}, Lcom/google/obf/dw;->f()I

    move-result v0

    and-int/lit8 v0, v0, 0x3

    add-int/lit8 v3, v0, 0x1

    .line 391
    const/4 v0, 0x3

    if-ne v3, v0, :cond_0

    .line 392
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 393
    :cond_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 394
    const/high16 v0, 0x3f800000    # 1.0f

    .line 395
    invoke-virtual {p0}, Lcom/google/obf/dw;->f()I

    move-result v1

    and-int/lit8 v5, v1, 0x1f

    move v1, v2

    .line 396
    :goto_0
    if-ge v1, v5, :cond_1

    .line 397
    invoke-static {p0}, Lcom/google/obf/du;->a(Lcom/google/obf/dw;)[B

    move-result-object v6

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 398
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 399
    :cond_1
    invoke-virtual {p0}, Lcom/google/obf/dw;->f()I

    move-result v6

    move v1, v2

    .line 400
    :goto_1
    if-ge v1, v6, :cond_2

    .line 401
    invoke-static {p0}, Lcom/google/obf/du;->a(Lcom/google/obf/dw;)[B

    move-result-object v7

    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 402
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 403
    :cond_2
    if-lez v5, :cond_3

    .line 404
    new-instance v1, Lcom/google/obf/dv;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    invoke-direct {v1, v0}, Lcom/google/obf/dv;-><init>([B)V

    .line 405
    add-int/lit8 v0, v3, 0x1

    mul-int/lit8 v0, v0, 0x8

    invoke-virtual {v1, v0}, Lcom/google/obf/dv;->a(I)V

    .line 406
    invoke-static {v1}, Lcom/google/obf/du;->a(Lcom/google/obf/dv;)Lcom/google/obf/du$b;

    move-result-object v0

    iget v0, v0, Lcom/google/obf/du$b;->d:F

    .line 407
    :cond_3
    new-instance v1, Lcom/google/obf/bd$a;

    invoke-direct {v1, v4, v3, v0}, Lcom/google/obf/bd$a;-><init>(Ljava/util/List;IF)V

    return-object v1
.end method

.method private static a(Lcom/google/obf/dw;IJILjava/lang/String;Z)Lcom/google/obf/bd$d;
    .locals 26

    .prologue
    .line 290
    const/16 v2, 0xc

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/obf/dw;->c(I)V

    .line 291
    invoke-virtual/range {p0 .. p0}, Lcom/google/obf/dw;->m()I

    move-result v24

    .line 292
    new-instance v11, Lcom/google/obf/bd$d;

    move/from16 v0, v24

    invoke-direct {v11, v0}, Lcom/google/obf/bd$d;-><init>(I)V

    .line 293
    const/4 v12, 0x0

    :goto_0
    move/from16 v0, v24

    if-ge v12, v0, :cond_a

    .line 294
    invoke-virtual/range {p0 .. p0}, Lcom/google/obf/dw;->d()I

    move-result v5

    .line 295
    invoke-virtual/range {p0 .. p0}, Lcom/google/obf/dw;->m()I

    move-result v6

    .line 296
    if-lez v6, :cond_2

    const/4 v2, 0x1

    :goto_1
    const-string v3, "childAtomSize should be positive"

    invoke-static {v2, v3}, Lcom/google/obf/dl;->a(ZLjava/lang/Object;)V

    .line 297
    invoke-virtual/range {p0 .. p0}, Lcom/google/obf/dw;->m()I

    move-result v4

    .line 298
    sget v2, Lcom/google/obf/bc;->b:I

    if-eq v4, v2, :cond_0

    sget v2, Lcom/google/obf/bc;->c:I

    if-eq v4, v2, :cond_0

    sget v2, Lcom/google/obf/bc;->Z:I

    if-eq v4, v2, :cond_0

    sget v2, Lcom/google/obf/bc;->al:I

    if-eq v4, v2, :cond_0

    sget v2, Lcom/google/obf/bc;->d:I

    if-eq v4, v2, :cond_0

    sget v2, Lcom/google/obf/bc;->e:I

    if-eq v4, v2, :cond_0

    sget v2, Lcom/google/obf/bc;->f:I

    if-eq v4, v2, :cond_0

    sget v2, Lcom/google/obf/bc;->aJ:I

    if-eq v4, v2, :cond_0

    sget v2, Lcom/google/obf/bc;->aK:I

    if-ne v4, v2, :cond_3

    :cond_0
    move-object/from16 v3, p0

    move/from16 v7, p1

    move-wide/from16 v8, p2

    move/from16 v10, p4

    .line 299
    invoke-static/range {v3 .. v12}, Lcom/google/obf/bd;->a(Lcom/google/obf/dw;IIIIJILcom/google/obf/bd$d;I)V

    .line 312
    :cond_1
    :goto_2
    add-int v2, v5, v6

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/obf/dw;->c(I)V

    .line 313
    add-int/lit8 v12, v12, 0x1

    goto :goto_0

    .line 296
    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    .line 300
    :cond_3
    sget v2, Lcom/google/obf/bc;->i:I

    if-eq v4, v2, :cond_4

    sget v2, Lcom/google/obf/bc;->aa:I

    if-eq v4, v2, :cond_4

    sget v2, Lcom/google/obf/bc;->n:I

    if-eq v4, v2, :cond_4

    sget v2, Lcom/google/obf/bc;->p:I

    if-eq v4, v2, :cond_4

    sget v2, Lcom/google/obf/bc;->r:I

    if-eq v4, v2, :cond_4

    sget v2, Lcom/google/obf/bc;->u:I

    if-eq v4, v2, :cond_4

    sget v2, Lcom/google/obf/bc;->s:I

    if-eq v4, v2, :cond_4

    sget v2, Lcom/google/obf/bc;->t:I

    if-eq v4, v2, :cond_4

    sget v2, Lcom/google/obf/bc;->ax:I

    if-eq v4, v2, :cond_4

    sget v2, Lcom/google/obf/bc;->ay:I

    if-eq v4, v2, :cond_4

    sget v2, Lcom/google/obf/bc;->l:I

    if-eq v4, v2, :cond_4

    sget v2, Lcom/google/obf/bc;->m:I

    if-eq v4, v2, :cond_4

    sget v2, Lcom/google/obf/bc;->j:I

    if-ne v4, v2, :cond_5

    :cond_4
    move-object/from16 v13, p0

    move v14, v4

    move v15, v5

    move/from16 v16, v6

    move/from16 v17, p1

    move-wide/from16 v18, p2

    move-object/from16 v20, p5

    move/from16 v21, p6

    move-object/from16 v22, v11

    move/from16 v23, v12

    .line 301
    invoke-static/range {v13 .. v23}, Lcom/google/obf/bd;->a(Lcom/google/obf/dw;IIIIJLjava/lang/String;ZLcom/google/obf/bd$d;I)V

    goto :goto_2

    .line 302
    :cond_5
    sget v2, Lcom/google/obf/bc;->aj:I

    if-ne v4, v2, :cond_6

    .line 303
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v13

    const-string v14, "application/ttml+xml"

    const/4 v15, -0x1

    move-wide/from16 v16, p2

    move-object/from16 v18, p5

    invoke-static/range {v13 .. v18}, Lcom/google/obf/q;->a(Ljava/lang/String;Ljava/lang/String;IJLjava/lang/String;)Lcom/google/obf/q;

    move-result-object v2

    iput-object v2, v11, Lcom/google/obf/bd$d;->b:Lcom/google/obf/q;

    goto :goto_2

    .line 304
    :cond_6
    sget v2, Lcom/google/obf/bc;->au:I

    if-ne v4, v2, :cond_7

    .line 305
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v13

    const-string v14, "application/x-quicktime-tx3g"

    const/4 v15, -0x1

    move-wide/from16 v16, p2

    move-object/from16 v18, p5

    invoke-static/range {v13 .. v18}, Lcom/google/obf/q;->a(Ljava/lang/String;Ljava/lang/String;IJLjava/lang/String;)Lcom/google/obf/q;

    move-result-object v2

    iput-object v2, v11, Lcom/google/obf/bd$d;->b:Lcom/google/obf/q;

    goto/16 :goto_2

    .line 306
    :cond_7
    sget v2, Lcom/google/obf/bc;->av:I

    if-ne v4, v2, :cond_8

    .line 307
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v13

    const-string v14, "application/x-mp4vtt"

    const/4 v15, -0x1

    move-wide/from16 v16, p2

    move-object/from16 v18, p5

    invoke-static/range {v13 .. v18}, Lcom/google/obf/q;->a(Ljava/lang/String;Ljava/lang/String;IJLjava/lang/String;)Lcom/google/obf/q;

    move-result-object v2

    iput-object v2, v11, Lcom/google/obf/bd$d;->b:Lcom/google/obf/q;

    goto/16 :goto_2

    .line 308
    :cond_8
    sget v2, Lcom/google/obf/bc;->aw:I

    if-ne v4, v2, :cond_9

    .line 309
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v14

    const-string v15, "application/ttml+xml"

    const/16 v16, -0x1

    const-wide/16 v20, 0x0

    move-wide/from16 v17, p2

    move-object/from16 v19, p5

    invoke-static/range {v14 .. v21}, Lcom/google/obf/q;->a(Ljava/lang/String;Ljava/lang/String;IJLjava/lang/String;J)Lcom/google/obf/q;

    move-result-object v2

    iput-object v2, v11, Lcom/google/obf/bd$d;->b:Lcom/google/obf/q;

    goto/16 :goto_2

    .line 310
    :cond_9
    sget v2, Lcom/google/obf/bc;->aM:I

    if-ne v4, v2, :cond_1

    .line 311
    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "application/x-camera-motion"

    const/4 v4, -0x1

    move-wide/from16 v0, p2

    invoke-static {v2, v3, v4, v0, v1}, Lcom/google/obf/q;->a(Ljava/lang/String;Ljava/lang/String;IJ)Lcom/google/obf/q;

    move-result-object v2

    iput-object v2, v11, Lcom/google/obf/bd$d;->b:Lcom/google/obf/q;

    goto/16 :goto_2

    .line 314
    :cond_a
    return-object v11
.end method

.method public static a(Lcom/google/obf/bc$a;Lcom/google/obf/bc$b;JZ)Lcom/google/obf/bk;
    .locals 28

    .prologue
    .line 1
    sget v2, Lcom/google/obf/bc;->E:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/obf/bc$a;->e(I)Lcom/google/obf/bc$a;

    move-result-object v8

    .line 2
    sget v2, Lcom/google/obf/bc;->S:I

    invoke-virtual {v8, v2}, Lcom/google/obf/bc$a;->d(I)Lcom/google/obf/bc$b;

    move-result-object v2

    iget-object v2, v2, Lcom/google/obf/bc$b;->aP:Lcom/google/obf/dw;

    invoke-static {v2}, Lcom/google/obf/bd;->e(Lcom/google/obf/dw;)I

    move-result v15

    .line 3
    sget v2, Lcom/google/obf/bk;->b:I

    if-eq v15, v2, :cond_0

    sget v2, Lcom/google/obf/bk;->a:I

    if-eq v15, v2, :cond_0

    sget v2, Lcom/google/obf/bk;->c:I

    if-eq v15, v2, :cond_0

    sget v2, Lcom/google/obf/bk;->d:I

    if-eq v15, v2, :cond_0

    sget v2, Lcom/google/obf/bk;->e:I

    if-eq v15, v2, :cond_0

    sget v2, Lcom/google/obf/bk;->f:I

    if-eq v15, v2, :cond_0

    .line 4
    const/4 v13, 0x0

    .line 21
    :goto_0
    return-object v13

    .line 5
    :cond_0
    sget v2, Lcom/google/obf/bc;->O:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/obf/bc$a;->d(I)Lcom/google/obf/bc$b;

    move-result-object v2

    iget-object v2, v2, Lcom/google/obf/bc$b;->aP:Lcom/google/obf/dw;

    invoke-static {v2}, Lcom/google/obf/bd;->d(Lcom/google/obf/dw;)Lcom/google/obf/bd$g;

    move-result-object v16

    .line 6
    const-wide/16 v2, -0x1

    cmp-long v2, p2, v2

    if-nez v2, :cond_3

    .line 7
    invoke-static/range {v16 .. v16}, Lcom/google/obf/bd$g;->a(Lcom/google/obf/bd$g;)J

    move-result-wide v2

    .line 8
    :goto_1
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/obf/bc$b;->aP:Lcom/google/obf/dw;

    invoke-static {v4}, Lcom/google/obf/bd;->c(Lcom/google/obf/dw;)J

    move-result-wide v6

    .line 9
    const-wide/16 v4, -0x1

    cmp-long v4, v2, v4

    if-nez v4, :cond_1

    .line 10
    const-wide/16 v10, -0x1

    .line 12
    :goto_2
    sget v2, Lcom/google/obf/bc;->F:I

    invoke-virtual {v8, v2}, Lcom/google/obf/bc$a;->e(I)Lcom/google/obf/bc$a;

    move-result-object v2

    sget v3, Lcom/google/obf/bc;->G:I

    .line 13
    invoke-virtual {v2, v3}, Lcom/google/obf/bc$a;->e(I)Lcom/google/obf/bc$a;

    move-result-object v2

    .line 14
    sget v3, Lcom/google/obf/bc;->R:I

    invoke-virtual {v8, v3}, Lcom/google/obf/bc$a;->d(I)Lcom/google/obf/bc$b;

    move-result-object v3

    iget-object v3, v3, Lcom/google/obf/bc$b;->aP:Lcom/google/obf/dw;

    invoke-static {v3}, Lcom/google/obf/bd;->f(Lcom/google/obf/dw;)Landroid/util/Pair;

    move-result-object v3

    .line 15
    sget v4, Lcom/google/obf/bc;->T:I

    invoke-virtual {v2, v4}, Lcom/google/obf/bc$a;->d(I)Lcom/google/obf/bc$b;

    move-result-object v2

    iget-object v8, v2, Lcom/google/obf/bc$b;->aP:Lcom/google/obf/dw;

    invoke-static/range {v16 .. v16}, Lcom/google/obf/bd$g;->b(Lcom/google/obf/bd$g;)I

    move-result v9

    .line 16
    invoke-static/range {v16 .. v16}, Lcom/google/obf/bd$g;->c(Lcom/google/obf/bd$g;)I

    move-result v12

    iget-object v13, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v13, Ljava/lang/String;

    move/from16 v14, p4

    .line 17
    invoke-static/range {v8 .. v14}, Lcom/google/obf/bd;->a(Lcom/google/obf/dw;IJILjava/lang/String;Z)Lcom/google/obf/bd$d;

    move-result-object v4

    .line 18
    sget v2, Lcom/google/obf/bc;->P:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/obf/bc$a;->e(I)Lcom/google/obf/bc$a;

    move-result-object v2

    invoke-static {v2}, Lcom/google/obf/bd;->a(Lcom/google/obf/bc$a;)Landroid/util/Pair;

    move-result-object v5

    .line 19
    iget-object v2, v4, Lcom/google/obf/bd$d;->b:Lcom/google/obf/q;

    if-nez v2, :cond_2

    const/4 v13, 0x0

    goto :goto_0

    .line 11
    :cond_1
    const-wide/32 v4, 0xf4240

    invoke-static/range {v2 .. v7}, Lcom/google/obf/ea;->a(JJJ)J

    move-result-wide v10

    goto :goto_2

    .line 20
    :cond_2
    new-instance v13, Lcom/google/obf/bk;

    invoke-static/range {v16 .. v16}, Lcom/google/obf/bd$g;->b(Lcom/google/obf/bd$g;)I

    move-result v14

    iget-object v2, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    iget-object v0, v4, Lcom/google/obf/bd$d;->b:Lcom/google/obf/q;

    move-object/from16 v22, v0

    iget-object v0, v4, Lcom/google/obf/bd$d;->a:[Lcom/google/obf/bl;

    move-object/from16 v23, v0

    iget v0, v4, Lcom/google/obf/bd$d;->c:I

    move/from16 v24, v0

    iget-object v0, v5, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object/from16 v25, v0

    check-cast v25, [J

    iget-object v0, v5, Landroid/util/Pair;->second:Ljava/lang/Object;

    move-object/from16 v26, v0

    check-cast v26, [J

    move-wide/from16 v18, v6

    move-wide/from16 v20, v10

    invoke-direct/range {v13 .. v26}, Lcom/google/obf/bk;-><init>(IIJJJLcom/google/obf/q;[Lcom/google/obf/bl;I[J[J)V

    goto/16 :goto_0

    :cond_3
    move-wide/from16 v2, p2

    goto/16 :goto_1
.end method

.method public static a(Lcom/google/obf/bk;Lcom/google/obf/bc$a;)Lcom/google/obf/bn;
    .locals 34
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/s;
        }
    .end annotation

    .prologue
    .line 22
    sget v2, Lcom/google/obf/bc;->aq:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/obf/bc$a;->d(I)Lcom/google/obf/bc$b;

    move-result-object v3

    .line 23
    if-eqz v3, :cond_0

    .line 24
    new-instance v2, Lcom/google/obf/bd$e;

    invoke-direct {v2, v3}, Lcom/google/obf/bd$e;-><init>(Lcom/google/obf/bc$b;)V

    .line 29
    :goto_0
    invoke-interface {v2}, Lcom/google/obf/bd$c;->a()I

    move-result v26

    .line 30
    if-nez v26, :cond_2

    .line 31
    new-instance v2, Lcom/google/obf/bn;

    const/4 v3, 0x0

    new-array v3, v3, [J

    const/4 v4, 0x0

    new-array v4, v4, [I

    const/4 v5, 0x0

    const/4 v6, 0x0

    new-array v6, v6, [J

    const/4 v7, 0x0

    new-array v7, v7, [I

    invoke-direct/range {v2 .. v7}, Lcom/google/obf/bn;-><init>([J[II[J[I)V

    .line 182
    :goto_1
    return-object v2

    .line 25
    :cond_0
    sget v2, Lcom/google/obf/bc;->ar:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/obf/bc$a;->d(I)Lcom/google/obf/bc$b;

    move-result-object v3

    .line 26
    if-nez v3, :cond_1

    .line 27
    new-instance v2, Lcom/google/obf/s;

    const-string v3, "Track has no sample table size information"

    invoke-direct {v2, v3}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v2

    .line 28
    :cond_1
    new-instance v2, Lcom/google/obf/bd$f;

    invoke-direct {v2, v3}, Lcom/google/obf/bd$f;-><init>(Lcom/google/obf/bc$b;)V

    goto :goto_0

    .line 32
    :cond_2
    const/4 v4, 0x0

    .line 33
    sget v3, Lcom/google/obf/bc;->as:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/google/obf/bc$a;->d(I)Lcom/google/obf/bc$b;

    move-result-object v3

    .line 34
    if-nez v3, :cond_3

    .line 35
    const/4 v4, 0x1

    .line 36
    sget v3, Lcom/google/obf/bc;->at:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/google/obf/bc$a;->d(I)Lcom/google/obf/bc$b;

    move-result-object v3

    .line 37
    :cond_3
    iget-object v6, v3, Lcom/google/obf/bc$b;->aP:Lcom/google/obf/dw;

    .line 38
    sget v3, Lcom/google/obf/bc;->ap:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/google/obf/bc$a;->d(I)Lcom/google/obf/bc$b;

    move-result-object v3

    iget-object v7, v3, Lcom/google/obf/bc$b;->aP:Lcom/google/obf/dw;

    .line 39
    sget v3, Lcom/google/obf/bc;->am:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/google/obf/bc$a;->d(I)Lcom/google/obf/bc$b;

    move-result-object v3

    iget-object v0, v3, Lcom/google/obf/bc$b;->aP:Lcom/google/obf/dw;

    move-object/from16 v27, v0

    .line 40
    sget v3, Lcom/google/obf/bc;->an:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/google/obf/bc$a;->d(I)Lcom/google/obf/bc$b;

    move-result-object v3

    .line 41
    if-eqz v3, :cond_5

    iget-object v3, v3, Lcom/google/obf/bc$b;->aP:Lcom/google/obf/dw;

    .line 42
    :goto_2
    sget v5, Lcom/google/obf/bc;->ao:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/obf/bc$a;->d(I)Lcom/google/obf/bc$b;

    move-result-object v5

    .line 43
    if-eqz v5, :cond_6

    iget-object v5, v5, Lcom/google/obf/bc$b;->aP:Lcom/google/obf/dw;

    .line 44
    :goto_3
    new-instance v28, Lcom/google/obf/bd$b;

    move-object/from16 v0, v28

    invoke-direct {v0, v7, v6, v4}, Lcom/google/obf/bd$b;-><init>(Lcom/google/obf/dw;Lcom/google/obf/dw;Z)V

    .line 45
    const/16 v4, 0xc

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Lcom/google/obf/dw;->c(I)V

    .line 46
    invoke-virtual/range {v27 .. v27}, Lcom/google/obf/dw;->s()I

    move-result v4

    add-int/lit8 v23, v4, -0x1

    .line 47
    invoke-virtual/range {v27 .. v27}, Lcom/google/obf/dw;->s()I

    move-result v22

    .line 48
    invoke-virtual/range {v27 .. v27}, Lcom/google/obf/dw;->s()I

    move-result v21

    .line 49
    const/16 v20, 0x0

    .line 50
    const/16 v17, 0x0

    .line 51
    const/16 v16, 0x0

    .line 52
    if-eqz v5, :cond_4

    .line 53
    const/16 v4, 0xc

    invoke-virtual {v5, v4}, Lcom/google/obf/dw;->c(I)V

    .line 54
    invoke-virtual {v5}, Lcom/google/obf/dw;->s()I

    move-result v17

    .line 55
    :cond_4
    const/4 v6, -0x1

    .line 56
    const/4 v4, 0x0

    .line 57
    if-eqz v3, :cond_2c

    .line 58
    const/16 v4, 0xc

    invoke-virtual {v3, v4}, Lcom/google/obf/dw;->c(I)V

    .line 59
    invoke-virtual {v3}, Lcom/google/obf/dw;->s()I

    move-result v4

    .line 60
    if-lez v4, :cond_7

    .line 61
    invoke-virtual {v3}, Lcom/google/obf/dw;->s()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    move/from16 v32, v4

    move v4, v6

    move-object v6, v3

    move/from16 v3, v32

    .line 63
    :goto_4
    invoke-interface {v2}, Lcom/google/obf/bd$c;->c()Z

    move-result v7

    if-eqz v7, :cond_8

    const-string v7, "audio/raw"

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/obf/bk;->l:Lcom/google/obf/q;

    iget-object v8, v8, Lcom/google/obf/q;->b:Ljava/lang/String;

    .line 64
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    if-nez v23, :cond_8

    if-nez v17, :cond_8

    if-nez v3, :cond_8

    const/4 v7, 0x1

    .line 65
    :goto_5
    const/4 v8, 0x0

    .line 66
    if-nez v7, :cond_15

    .line 67
    move/from16 v0, v26

    new-array v15, v0, [J

    .line 68
    move/from16 v0, v26

    new-array v14, v0, [I

    .line 69
    move/from16 v0, v26

    new-array v13, v0, [J

    .line 70
    move/from16 v0, v26

    new-array v12, v0, [I

    .line 71
    const-wide/16 v10, 0x0

    .line 72
    const-wide/16 v18, 0x0

    .line 73
    const/4 v9, 0x0

    .line 74
    const/4 v7, 0x0

    move-wide/from16 v24, v10

    move/from16 v10, v21

    move/from16 v11, v23

    move/from16 v23, v22

    move/from16 v22, v7

    move/from16 v7, v20

    move/from16 v32, v17

    move/from16 v17, v4

    move/from16 v4, v32

    move/from16 v33, v16

    move/from16 v16, v3

    move/from16 v3, v33

    :goto_6
    move/from16 v0, v22

    move/from16 v1, v26

    if-ge v0, v1, :cond_f

    move-wide/from16 v20, v18

    move/from16 v18, v9

    .line 75
    :goto_7
    if-nez v18, :cond_9

    .line 76
    invoke-virtual/range {v28 .. v28}, Lcom/google/obf/bd$b;->a()Z

    move-result v9

    invoke-static {v9}, Lcom/google/obf/dl;->b(Z)V

    .line 77
    move-object/from16 v0, v28

    iget-wide v0, v0, Lcom/google/obf/bd$b;->d:J

    move-wide/from16 v18, v0

    .line 78
    move-object/from16 v0, v28

    iget v9, v0, Lcom/google/obf/bd$b;->c:I

    move-wide/from16 v20, v18

    move/from16 v18, v9

    goto :goto_7

    .line 41
    :cond_5
    const/4 v3, 0x0

    goto/16 :goto_2

    .line 43
    :cond_6
    const/4 v5, 0x0

    goto/16 :goto_3

    .line 62
    :cond_7
    const/4 v3, 0x0

    move/from16 v32, v4

    move v4, v6

    move-object v6, v3

    move/from16 v3, v32

    goto :goto_4

    .line 64
    :cond_8
    const/4 v7, 0x0

    goto :goto_5

    .line 79
    :cond_9
    if-eqz v5, :cond_b

    .line 80
    :goto_8
    if-nez v7, :cond_a

    if-lez v4, :cond_a

    .line 81
    invoke-virtual {v5}, Lcom/google/obf/dw;->s()I

    move-result v7

    .line 82
    invoke-virtual {v5}, Lcom/google/obf/dw;->m()I

    move-result v3

    .line 83
    add-int/lit8 v4, v4, -0x1

    goto :goto_8

    .line 84
    :cond_a
    add-int/lit8 v7, v7, -0x1

    .line 85
    :cond_b
    aput-wide v20, v15, v22

    .line 86
    invoke-interface {v2}, Lcom/google/obf/bd$c;->b()I

    move-result v9

    aput v9, v14, v22

    .line 87
    aget v9, v14, v22

    if-le v9, v8, :cond_c

    .line 88
    aget v8, v14, v22

    .line 89
    :cond_c
    int-to-long v0, v3

    move-wide/from16 v30, v0

    add-long v30, v30, v24

    aput-wide v30, v13, v22

    .line 90
    if-nez v6, :cond_e

    const/4 v9, 0x1

    :goto_9
    aput v9, v12, v22

    .line 91
    move/from16 v0, v22

    move/from16 v1, v17

    if-ne v0, v1, :cond_d

    .line 92
    const/4 v9, 0x1

    aput v9, v12, v22

    .line 93
    add-int/lit8 v9, v16, -0x1

    .line 94
    if-lez v9, :cond_2b

    .line 95
    invoke-virtual {v6}, Lcom/google/obf/dw;->s()I

    move-result v16

    add-int/lit8 v16, v16, -0x1

    move/from16 v17, v16

    move/from16 v16, v9

    .line 96
    :cond_d
    :goto_a
    int-to-long v0, v10

    move-wide/from16 v30, v0

    add-long v24, v24, v30

    .line 97
    add-int/lit8 v9, v23, -0x1

    .line 98
    if-nez v9, :cond_2a

    if-lez v11, :cond_2a

    .line 99
    invoke-virtual/range {v27 .. v27}, Lcom/google/obf/dw;->s()I

    move-result v10

    .line 100
    invoke-virtual/range {v27 .. v27}, Lcom/google/obf/dw;->s()I

    move-result v9

    .line 101
    add-int/lit8 v11, v11, -0x1

    .line 102
    :goto_b
    aget v19, v14, v22

    move/from16 v0, v19

    int-to-long v0, v0

    move-wide/from16 v30, v0

    add-long v20, v20, v30

    .line 103
    add-int/lit8 v19, v18, -0x1

    .line 104
    add-int/lit8 v18, v22, 0x1

    move/from16 v22, v18

    move/from16 v23, v10

    move v10, v9

    move/from16 v9, v19

    move-wide/from16 v18, v20

    goto/16 :goto_6

    .line 90
    :cond_e
    const/4 v9, 0x0

    goto :goto_9

    .line 105
    :cond_f
    if-nez v7, :cond_10

    const/4 v2, 0x1

    :goto_c
    invoke-static {v2}, Lcom/google/obf/dl;->a(Z)V

    .line 106
    :goto_d
    if-lez v4, :cond_12

    .line 107
    invoke-virtual {v5}, Lcom/google/obf/dw;->s()I

    move-result v2

    if-nez v2, :cond_11

    const/4 v2, 0x1

    :goto_e
    invoke-static {v2}, Lcom/google/obf/dl;->a(Z)V

    .line 108
    invoke-virtual {v5}, Lcom/google/obf/dw;->m()I

    .line 109
    add-int/lit8 v4, v4, -0x1

    goto :goto_d

    .line 105
    :cond_10
    const/4 v2, 0x0

    goto :goto_c

    .line 107
    :cond_11
    const/4 v2, 0x0

    goto :goto_e

    .line 110
    :cond_12
    if-nez v16, :cond_13

    if-nez v23, :cond_13

    if-nez v9, :cond_13

    if-eqz v11, :cond_14

    .line 111
    :cond_13
    const-string v2, "AtomParsers"

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/obf/bk;->g:I

    const/16 v4, 0xd7

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Inconsistent stbl box for track "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": remainingSynchronizationSamples "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", remainingSamplesAtTimestampDelta "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move/from16 v0, v23

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", remainingSamplesInChunk "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", remainingTimestampDeltaChanges "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_14
    move-object v7, v12

    move-object v6, v13

    move-object v4, v14

    move-object v3, v15

    move v5, v8

    .line 125
    :goto_f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/obf/bk;->n:[J

    if-nez v2, :cond_17

    .line 126
    const-wide/32 v8, 0xf4240

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/google/obf/bk;->i:J

    invoke-static {v6, v8, v9, v10, v11}, Lcom/google/obf/ea;->a([JJJ)V

    .line 127
    new-instance v2, Lcom/google/obf/bn;

    invoke-direct/range {v2 .. v7}, Lcom/google/obf/bn;-><init>([J[II[J[I)V

    goto/16 :goto_1

    .line 113
    :cond_15
    move-object/from16 v0, v28

    iget v3, v0, Lcom/google/obf/bd$b;->a:I

    new-array v3, v3, [J

    .line 114
    move-object/from16 v0, v28

    iget v4, v0, Lcom/google/obf/bd$b;->a:I

    new-array v4, v4, [I

    .line 115
    :goto_10
    invoke-virtual/range {v28 .. v28}, Lcom/google/obf/bd$b;->a()Z

    move-result v5

    if-eqz v5, :cond_16

    .line 116
    move-object/from16 v0, v28

    iget v5, v0, Lcom/google/obf/bd$b;->b:I

    move-object/from16 v0, v28

    iget-wide v6, v0, Lcom/google/obf/bd$b;->d:J

    aput-wide v6, v3, v5

    .line 117
    move-object/from16 v0, v28

    iget v5, v0, Lcom/google/obf/bd$b;->b:I

    move-object/from16 v0, v28

    iget v6, v0, Lcom/google/obf/bd$b;->c:I

    aput v6, v4, v5

    goto :goto_10

    .line 118
    :cond_16
    invoke-interface {v2}, Lcom/google/obf/bd$c;->b()I

    move-result v2

    .line 119
    move/from16 v0, v21

    int-to-long v6, v0

    invoke-static {v2, v3, v4, v6, v7}, Lcom/google/obf/bf;->a(I[J[IJ)Lcom/google/obf/bf$a;

    move-result-object v2

    .line 120
    iget-object v3, v2, Lcom/google/obf/bf$a;->a:[J

    .line 121
    iget-object v4, v2, Lcom/google/obf/bf$a;->b:[I

    .line 122
    iget v5, v2, Lcom/google/obf/bf$a;->c:I

    .line 123
    iget-object v6, v2, Lcom/google/obf/bf$a;->d:[J

    .line 124
    iget-object v7, v2, Lcom/google/obf/bf$a;->e:[I

    goto :goto_f

    .line 128
    :cond_17
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/obf/bk;->n:[J

    array-length v2, v2

    const/4 v8, 0x1

    if-ne v2, v8, :cond_19

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/obf/bk;->n:[J

    const/4 v8, 0x0

    aget-wide v8, v2, v8

    const-wide/16 v10, 0x0

    cmp-long v2, v8, v10

    if-nez v2, :cond_19

    .line 129
    const/4 v2, 0x0

    :goto_11
    array-length v8, v6

    if-ge v2, v8, :cond_18

    .line 130
    aget-wide v8, v6, v2

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/obf/bk;->o:[J

    const/4 v11, 0x0

    aget-wide v10, v10, v11

    sub-long/2addr v8, v10

    const-wide/32 v10, 0xf4240

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/google/obf/bk;->i:J

    invoke-static/range {v8 .. v13}, Lcom/google/obf/ea;->a(JJJ)J

    move-result-wide v8

    aput-wide v8, v6, v2

    .line 131
    add-int/lit8 v2, v2, 0x1

    goto :goto_11

    .line 132
    :cond_18
    new-instance v2, Lcom/google/obf/bn;

    invoke-direct/range {v2 .. v7}, Lcom/google/obf/bn;-><init>([J[II[J[I)V

    goto/16 :goto_1

    .line 133
    :cond_19
    const/4 v10, 0x0

    .line 134
    const/4 v9, 0x0

    .line 135
    const/4 v8, 0x0

    .line 136
    const/4 v2, 0x0

    move v14, v8

    move v15, v9

    move/from16 v16, v10

    :goto_12
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/obf/bk;->n:[J

    array-length v8, v8

    if-ge v2, v8, :cond_1b

    .line 137
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/obf/bk;->o:[J

    aget-wide v18, v8, v2

    .line 138
    const-wide/16 v8, -0x1

    cmp-long v8, v18, v8

    if-eqz v8, :cond_29

    .line 139
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/obf/bk;->n:[J

    aget-wide v8, v8, v2

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/google/obf/bk;->i:J

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/google/obf/bk;->j:J

    invoke-static/range {v8 .. v13}, Lcom/google/obf/ea;->a(JJJ)J

    move-result-wide v8

    .line 140
    const/4 v10, 0x1

    const/4 v11, 0x1

    move-wide/from16 v0, v18

    invoke-static {v6, v0, v1, v10, v11}, Lcom/google/obf/ea;->b([JJZZ)I

    move-result v11

    .line 141
    add-long v8, v8, v18

    const/4 v10, 0x1

    const/4 v12, 0x0

    invoke-static {v6, v8, v9, v10, v12}, Lcom/google/obf/ea;->b([JJZZ)I

    move-result v9

    .line 142
    sub-int v8, v9, v11

    add-int v10, v16, v8

    .line 143
    if-eq v15, v11, :cond_1a

    const/4 v8, 0x1

    :goto_13
    or-int/2addr v8, v14

    .line 145
    :goto_14
    add-int/lit8 v2, v2, 0x1

    move v14, v8

    move v15, v9

    move/from16 v16, v10

    goto :goto_12

    .line 143
    :cond_1a
    const/4 v8, 0x0

    goto :goto_13

    .line 146
    :cond_1b
    move/from16 v0, v16

    move/from16 v1, v26

    if-eq v0, v1, :cond_1e

    const/4 v2, 0x1

    :goto_15
    or-int v23, v14, v2

    .line 147
    if-eqz v23, :cond_1f

    move/from16 v0, v16

    new-array v2, v0, [J

    move-object/from16 v22, v2

    .line 148
    :goto_16
    if-eqz v23, :cond_20

    move/from16 v0, v16

    new-array v2, v0, [I

    move-object/from16 v21, v2

    .line 149
    :goto_17
    if-eqz v23, :cond_21

    const/4 v10, 0x0

    .line 150
    :goto_18
    if-eqz v23, :cond_22

    move/from16 v0, v16

    new-array v2, v0, [I

    move-object/from16 v17, v2

    .line 151
    :goto_19
    move/from16 v0, v16

    new-array v0, v0, [J

    move-object/from16 v24, v0

    .line 152
    const-wide/16 v8, 0x0

    .line 153
    const/4 v5, 0x0

    .line 154
    const/4 v2, 0x0

    move v14, v5

    move-wide/from16 v18, v8

    move v5, v10

    :goto_1a
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/obf/bk;->n:[J

    array-length v8, v8

    if-ge v2, v8, :cond_24

    .line 155
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/obf/bk;->o:[J

    aget-wide v26, v8, v2

    .line 156
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/obf/bk;->n:[J

    aget-wide v8, v8, v2

    .line 157
    const-wide/16 v10, -0x1

    cmp-long v10, v26, v10

    if-eqz v10, :cond_28

    .line 158
    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/google/obf/bk;->i:J

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/google/obf/bk;->j:J

    invoke-static/range {v8 .. v13}, Lcom/google/obf/ea;->a(JJJ)J

    move-result-wide v10

    add-long v12, v26, v10

    .line 159
    const/4 v10, 0x1

    const/4 v11, 0x1

    move-wide/from16 v0, v26

    invoke-static {v6, v0, v1, v10, v11}, Lcom/google/obf/ea;->b([JJZZ)I

    move-result v10

    .line 160
    const/4 v11, 0x1

    const/4 v15, 0x0

    invoke-static {v6, v12, v13, v11, v15}, Lcom/google/obf/ea;->b([JJZZ)I

    move-result v25

    .line 161
    if-eqz v23, :cond_1c

    .line 162
    sub-int v11, v25, v10

    .line 163
    move-object/from16 v0, v22

    invoke-static {v3, v10, v0, v14, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 164
    move-object/from16 v0, v21

    invoke-static {v4, v10, v0, v14, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 165
    move-object/from16 v0, v17

    invoke-static {v7, v10, v0, v14, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1c
    move/from16 v20, v10

    move/from16 v16, v14

    .line 166
    :goto_1b
    move/from16 v0, v20

    move/from16 v1, v25

    if-ge v0, v1, :cond_23

    .line 167
    const-wide/32 v12, 0xf4240

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/google/obf/bk;->j:J

    move-wide/from16 v10, v18

    invoke-static/range {v10 .. v15}, Lcom/google/obf/ea;->a(JJJ)J

    move-result-wide v28

    .line 168
    aget-wide v10, v6, v20

    sub-long v10, v10, v26

    const-wide/32 v12, 0xf4240

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/google/obf/bk;->i:J

    invoke-static/range {v10 .. v15}, Lcom/google/obf/ea;->a(JJJ)J

    move-result-wide v10

    .line 169
    add-long v10, v10, v28

    aput-wide v10, v24, v16

    .line 170
    if-eqz v23, :cond_1d

    aget v10, v21, v16

    if-le v10, v5, :cond_1d

    .line 171
    aget v5, v4, v20

    .line 172
    :cond_1d
    add-int/lit8 v16, v16, 0x1

    .line 173
    add-int/lit8 v10, v20, 0x1

    move/from16 v20, v10

    goto :goto_1b

    .line 146
    :cond_1e
    const/4 v2, 0x0

    goto/16 :goto_15

    :cond_1f
    move-object/from16 v22, v3

    .line 147
    goto/16 :goto_16

    :cond_20
    move-object/from16 v21, v4

    .line 148
    goto/16 :goto_17

    :cond_21
    move v10, v5

    .line 149
    goto/16 :goto_18

    :cond_22
    move-object/from16 v17, v7

    .line 150
    goto/16 :goto_19

    :cond_23
    move v10, v5

    move/from16 v5, v16

    .line 174
    :goto_1c
    add-long v8, v8, v18

    .line 175
    add-int/lit8 v2, v2, 0x1

    move v14, v5

    move-wide/from16 v18, v8

    move v5, v10

    goto/16 :goto_1a

    .line 176
    :cond_24
    const/4 v3, 0x0

    .line 177
    const/4 v2, 0x0

    :goto_1d
    move-object/from16 v0, v17

    array-length v4, v0

    if-ge v2, v4, :cond_26

    if-nez v3, :cond_26

    .line 178
    aget v4, v17, v2

    and-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_25

    const/4 v4, 0x1

    :goto_1e
    or-int/2addr v3, v4

    .line 179
    add-int/lit8 v2, v2, 0x1

    goto :goto_1d

    .line 178
    :cond_25
    const/4 v4, 0x0

    goto :goto_1e

    .line 180
    :cond_26
    if-nez v3, :cond_27

    .line 181
    new-instance v2, Lcom/google/obf/s;

    const-string v3, "The edited sample sequence does not contain a sync sample."

    invoke-direct {v2, v3}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v2

    .line 182
    :cond_27
    new-instance v2, Lcom/google/obf/bn;

    move-object/from16 v3, v22

    move-object/from16 v4, v21

    move-object/from16 v6, v24

    move-object/from16 v7, v17

    invoke-direct/range {v2 .. v7}, Lcom/google/obf/bn;-><init>([J[II[J[I)V

    goto/16 :goto_1

    :cond_28
    move v10, v5

    move v5, v14

    goto :goto_1c

    :cond_29
    move v8, v14

    move v9, v15

    move/from16 v10, v16

    goto/16 :goto_14

    :cond_2a
    move/from16 v32, v10

    move v10, v9

    move/from16 v9, v32

    goto/16 :goto_b

    :cond_2b
    move/from16 v16, v9

    goto/16 :goto_a

    :cond_2c
    move/from16 v32, v4

    move v4, v6

    move-object v6, v3

    move/from16 v3, v32

    goto/16 :goto_4
.end method

.method private static a(Lcom/google/obf/dw;IIIIJILcom/google/obf/bd$d;I)V
    .locals 21

    .prologue
    .line 315
    add-int/lit8 v6, p2, 0x8

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/google/obf/dw;->c(I)V

    .line 316
    const/16 v6, 0x18

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/google/obf/dw;->d(I)V

    .line 317
    invoke-virtual/range {p0 .. p0}, Lcom/google/obf/dw;->g()I

    move-result v12

    .line 318
    invoke-virtual/range {p0 .. p0}, Lcom/google/obf/dw;->g()I

    move-result v13

    .line 319
    const/4 v9, 0x0

    .line 320
    const/high16 v16, 0x3f800000    # 1.0f

    .line 321
    const/16 v6, 0x32

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/google/obf/dw;->d(I)V

    .line 322
    invoke-virtual/range {p0 .. p0}, Lcom/google/obf/dw;->d()I

    move-result v6

    .line 323
    sget v7, Lcom/google/obf/bc;->Z:I

    move/from16 v0, p1

    if-ne v0, v7, :cond_0

    .line 324
    move-object/from16 v0, p0

    move/from16 v1, p2

    move/from16 v2, p3

    move-object/from16 v3, p8

    move/from16 v4, p9

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/obf/bd;->a(Lcom/google/obf/dw;IILcom/google/obf/bd$d;I)I

    .line 325
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/google/obf/dw;->c(I)V

    .line 326
    :cond_0
    const/4 v14, 0x0

    .line 327
    const/4 v7, 0x0

    .line 328
    const/16 v17, 0x0

    .line 329
    const/16 v18, -0x1

    move v10, v6

    .line 330
    :goto_0
    sub-int v6, v10, p2

    move/from16 v0, p3

    if-ge v6, v0, :cond_1

    .line 331
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lcom/google/obf/dw;->c(I)V

    .line 332
    invoke-virtual/range {p0 .. p0}, Lcom/google/obf/dw;->d()I

    move-result v11

    .line 333
    invoke-virtual/range {p0 .. p0}, Lcom/google/obf/dw;->m()I

    move-result v15

    .line 334
    if-nez v15, :cond_2

    invoke-virtual/range {p0 .. p0}, Lcom/google/obf/dw;->d()I

    move-result v6

    sub-int v6, v6, p2

    move/from16 v0, p3

    if-ne v6, v0, :cond_2

    .line 385
    :cond_1
    if-nez v7, :cond_13

    .line 388
    :goto_1
    return-void

    .line 336
    :cond_2
    if-lez v15, :cond_4

    const/4 v6, 0x1

    :goto_2
    const-string v8, "childAtomSize should be positive"

    invoke-static {v6, v8}, Lcom/google/obf/dl;->a(ZLjava/lang/Object;)V

    .line 337
    invoke-virtual/range {p0 .. p0}, Lcom/google/obf/dw;->m()I

    move-result v6

    .line 338
    sget v8, Lcom/google/obf/bc;->H:I

    if-ne v6, v8, :cond_6

    .line 339
    if-nez v7, :cond_5

    const/4 v6, 0x1

    :goto_3
    invoke-static {v6}, Lcom/google/obf/dl;->b(Z)V

    .line 340
    const-string v7, "video/avc"

    .line 341
    move-object/from16 v0, p0

    invoke-static {v0, v11}, Lcom/google/obf/bd;->a(Lcom/google/obf/dw;I)Lcom/google/obf/bd$a;

    move-result-object v6

    .line 342
    iget-object v14, v6, Lcom/google/obf/bd$a;->a:Ljava/util/List;

    .line 343
    iget v8, v6, Lcom/google/obf/bd$a;->b:I

    move-object/from16 v0, p8

    iput v8, v0, Lcom/google/obf/bd$d;->c:I

    .line 344
    if-nez v9, :cond_3

    .line 345
    iget v0, v6, Lcom/google/obf/bd$a;->c:F

    move/from16 v16, v0

    :cond_3
    move v6, v9

    .line 383
    :goto_4
    add-int v8, v10, v15

    move v10, v8

    move v9, v6

    .line 384
    goto :goto_0

    .line 336
    :cond_4
    const/4 v6, 0x0

    goto :goto_2

    .line 339
    :cond_5
    const/4 v6, 0x0

    goto :goto_3

    .line 346
    :cond_6
    sget v8, Lcom/google/obf/bc;->I:I

    if-ne v6, v8, :cond_8

    .line 347
    if-nez v7, :cond_7

    const/4 v6, 0x1

    :goto_5
    invoke-static {v6}, Lcom/google/obf/dl;->b(Z)V

    .line 348
    const-string v8, "video/hevc"

    .line 349
    move-object/from16 v0, p0

    invoke-static {v0, v11}, Lcom/google/obf/bd;->b(Lcom/google/obf/dw;I)Landroid/util/Pair;

    move-result-object v7

    .line 350
    iget-object v6, v7, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v6, Ljava/util/List;

    .line 351
    iget-object v7, v7, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    move-object/from16 v0, p8

    iput v7, v0, Lcom/google/obf/bd$d;->c:I

    move-object v7, v8

    move-object v14, v6

    move v6, v9

    .line 352
    goto :goto_4

    .line 347
    :cond_7
    const/4 v6, 0x0

    goto :goto_5

    .line 352
    :cond_8
    sget v8, Lcom/google/obf/bc;->g:I

    if-ne v6, v8, :cond_a

    .line 353
    if-nez v7, :cond_9

    const/4 v6, 0x1

    :goto_6
    invoke-static {v6}, Lcom/google/obf/dl;->b(Z)V

    .line 354
    const-string v7, "video/3gpp"

    move v6, v9

    goto :goto_4

    .line 353
    :cond_9
    const/4 v6, 0x0

    goto :goto_6

    .line 355
    :cond_a
    sget v8, Lcom/google/obf/bc;->J:I

    if-ne v6, v8, :cond_c

    .line 356
    if-nez v7, :cond_b

    const/4 v6, 0x1

    :goto_7
    invoke-static {v6}, Lcom/google/obf/dl;->b(Z)V

    .line 358
    move-object/from16 v0, p0

    invoke-static {v0, v11}, Lcom/google/obf/bd;->d(Lcom/google/obf/dw;I)Landroid/util/Pair;

    move-result-object v7

    .line 359
    iget-object v6, v7, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v6, Ljava/lang/String;

    .line 360
    iget-object v7, v7, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v7, [B

    invoke-static {v7}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v14

    move-object v7, v6

    move v6, v9

    .line 361
    goto :goto_4

    .line 356
    :cond_b
    const/4 v6, 0x0

    goto :goto_7

    .line 361
    :cond_c
    sget v8, Lcom/google/obf/bc;->ai:I

    if-ne v6, v8, :cond_d

    .line 362
    move-object/from16 v0, p0

    invoke-static {v0, v11}, Lcom/google/obf/bd;->c(Lcom/google/obf/dw;I)F

    move-result v16

    .line 363
    const/4 v6, 0x1

    goto :goto_4

    .line 364
    :cond_d
    sget v8, Lcom/google/obf/bc;->aL:I

    if-ne v6, v8, :cond_10

    .line 365
    if-nez v7, :cond_e

    const/4 v6, 0x1

    :goto_8
    invoke-static {v6}, Lcom/google/obf/dl;->b(Z)V

    .line 366
    sget v6, Lcom/google/obf/bc;->aJ:I

    move/from16 v0, p1

    if-ne v0, v6, :cond_f

    const-string v7, "video/x-vnd.on2.vp8"

    :goto_9
    move v6, v9

    goto/16 :goto_4

    .line 365
    :cond_e
    const/4 v6, 0x0

    goto :goto_8

    .line 366
    :cond_f
    const-string v7, "video/x-vnd.on2.vp9"

    goto :goto_9

    .line 367
    :cond_10
    sget v8, Lcom/google/obf/bc;->aH:I

    if-ne v6, v8, :cond_11

    .line 368
    move-object/from16 v0, p0

    invoke-static {v0, v11, v15}, Lcom/google/obf/bd;->d(Lcom/google/obf/dw;II)[B

    move-result-object v17

    move v6, v9

    goto/16 :goto_4

    .line 369
    :cond_11
    sget v8, Lcom/google/obf/bc;->aG:I

    if-ne v6, v8, :cond_12

    .line 370
    invoke-virtual/range {p0 .. p0}, Lcom/google/obf/dw;->f()I

    move-result v6

    .line 371
    const/4 v8, 0x3

    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/google/obf/dw;->d(I)V

    .line 372
    if-nez v6, :cond_12

    .line 373
    invoke-virtual/range {p0 .. p0}, Lcom/google/obf/dw;->f()I

    move-result v6

    .line 374
    packed-switch v6, :pswitch_data_0

    :cond_12
    move v6, v9

    goto/16 :goto_4

    .line 375
    :pswitch_0
    const/16 v18, 0x0

    move v6, v9

    .line 376
    goto/16 :goto_4

    .line 377
    :pswitch_1
    const/16 v18, 0x1

    move v6, v9

    .line 378
    goto/16 :goto_4

    .line 379
    :pswitch_2
    const/16 v18, 0x2

    move v6, v9

    .line 380
    goto/16 :goto_4

    .line 381
    :pswitch_3
    const/16 v18, 0x3

    move v6, v9

    .line 382
    goto/16 :goto_4

    .line 387
    :cond_13
    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v8, -0x1

    const/4 v9, -0x1

    const/16 v19, 0x0

    move-wide/from16 v10, p5

    move/from16 v15, p7

    invoke-static/range {v6 .. v19}, Lcom/google/obf/q;->a(Ljava/lang/String;Ljava/lang/String;IIJIILjava/util/List;IF[BILcom/google/obf/d;)Lcom/google/obf/q;

    move-result-object v6

    move-object/from16 v0, p8

    iput-object v6, v0, Lcom/google/obf/bd$d;->b:Lcom/google/obf/q;

    goto/16 :goto_1

    .line 374
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private static a(Lcom/google/obf/dw;IIIIJLjava/lang/String;ZLcom/google/obf/bd$d;I)V
    .locals 19

    .prologue
    .line 462
    add-int/lit8 v6, p2, 0x8

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/google/obf/dw;->c(I)V

    .line 463
    const/4 v6, 0x0

    .line 464
    if-eqz p8, :cond_7

    .line 465
    const/16 v6, 0x8

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/google/obf/dw;->d(I)V

    .line 466
    invoke-virtual/range {p0 .. p0}, Lcom/google/obf/dw;->g()I

    move-result v6

    .line 467
    const/4 v7, 0x6

    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lcom/google/obf/dw;->d(I)V

    move v8, v6

    .line 469
    :goto_0
    if-eqz v8, :cond_0

    const/4 v6, 0x1

    if-ne v8, v6, :cond_8

    .line 470
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/obf/dw;->g()I

    move-result v7

    .line 471
    const/4 v6, 0x6

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/google/obf/dw;->d(I)V

    .line 472
    invoke-virtual/range {p0 .. p0}, Lcom/google/obf/dw;->q()I

    move-result v6

    .line 473
    const/4 v9, 0x1

    if-ne v8, v9, :cond_1

    .line 474
    const/16 v8, 0x10

    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/google/obf/dw;->d(I)V

    .line 481
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/obf/dw;->d()I

    move-result v16

    .line 482
    sget v8, Lcom/google/obf/bc;->aa:I

    move/from16 v0, p1

    if-ne v0, v8, :cond_2

    .line 483
    move-object/from16 v0, p0

    move/from16 v1, p2

    move/from16 v2, p3

    move-object/from16 v3, p9

    move/from16 v4, p10

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/obf/bd;->a(Lcom/google/obf/dw;IILcom/google/obf/bd$d;I)I

    move-result p1

    .line 484
    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/google/obf/dw;->c(I)V

    .line 485
    :cond_2
    const/4 v8, 0x0

    .line 486
    sget v9, Lcom/google/obf/bc;->n:I

    move/from16 v0, p1

    if-ne v0, v9, :cond_9

    .line 487
    const-string v8, "audio/ac3"

    .line 504
    :cond_3
    :goto_2
    const/16 v17, 0x0

    move v13, v6

    move v12, v7

    move-object v7, v8

    .line 505
    :goto_3
    sub-int v6, v16, p2

    move/from16 v0, p3

    if-ge v6, v0, :cond_18

    .line 506
    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/google/obf/dw;->c(I)V

    .line 507
    invoke-virtual/range {p0 .. p0}, Lcom/google/obf/dw;->m()I

    move-result v18

    .line 508
    if-lez v18, :cond_13

    const/4 v6, 0x1

    :goto_4
    const-string v8, "childAtomSize should be positive"

    invoke-static {v6, v8}, Lcom/google/obf/dl;->a(ZLjava/lang/Object;)V

    .line 509
    invoke-virtual/range {p0 .. p0}, Lcom/google/obf/dw;->m()I

    move-result v6

    .line 510
    sget v8, Lcom/google/obf/bc;->J:I

    if-eq v6, v8, :cond_4

    if-eqz p8, :cond_15

    sget v8, Lcom/google/obf/bc;->k:I

    if-ne v6, v8, :cond_15

    .line 511
    :cond_4
    sget v8, Lcom/google/obf/bc;->J:I

    if-ne v6, v8, :cond_14

    move/from16 v6, v16

    .line 513
    :goto_5
    const/4 v8, -0x1

    if-eq v6, v8, :cond_1c

    .line 515
    move-object/from16 v0, p0

    invoke-static {v0, v6}, Lcom/google/obf/bd;->d(Lcom/google/obf/dw;I)Landroid/util/Pair;

    move-result-object v8

    .line 516
    iget-object v6, v8, Landroid/util/Pair;->first:Ljava/lang/Object;

    move-object v7, v6

    check-cast v7, Ljava/lang/String;

    .line 517
    iget-object v6, v8, Landroid/util/Pair;->second:Ljava/lang/Object;

    move-object v8, v6

    check-cast v8, [B

    .line 518
    const-string v6, "audio/mp4a-latm"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 520
    invoke-static {v8}, Lcom/google/obf/dm;->a([B)Landroid/util/Pair;

    move-result-object v9

    .line 521
    iget-object v6, v9, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v13

    .line 522
    iget-object v6, v9, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v12

    :cond_5
    :goto_6
    move-object/from16 v17, v8

    .line 531
    :cond_6
    :goto_7
    add-int v16, v16, v18

    .line 532
    goto :goto_3

    .line 468
    :cond_7
    const/16 v7, 0x10

    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lcom/google/obf/dw;->d(I)V

    move v8, v6

    goto/16 :goto_0

    .line 475
    :cond_8
    const/4 v6, 0x2

    if-ne v8, v6, :cond_19

    .line 476
    const/16 v6, 0x10

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/google/obf/dw;->d(I)V

    .line 477
    invoke-virtual/range {p0 .. p0}, Lcom/google/obf/dw;->v()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Math;->round(D)J

    move-result-wide v6

    long-to-int v6, v6

    .line 478
    invoke-virtual/range {p0 .. p0}, Lcom/google/obf/dw;->s()I

    move-result v7

    .line 479
    const/16 v8, 0x14

    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/google/obf/dw;->d(I)V

    goto/16 :goto_1

    .line 488
    :cond_9
    sget v9, Lcom/google/obf/bc;->p:I

    move/from16 v0, p1

    if-ne v0, v9, :cond_a

    .line 489
    const-string v8, "audio/eac3"

    goto/16 :goto_2

    .line 490
    :cond_a
    sget v9, Lcom/google/obf/bc;->r:I

    move/from16 v0, p1

    if-ne v0, v9, :cond_b

    .line 491
    const-string v8, "audio/vnd.dts"

    goto/16 :goto_2

    .line 492
    :cond_b
    sget v9, Lcom/google/obf/bc;->s:I

    move/from16 v0, p1

    if-eq v0, v9, :cond_c

    sget v9, Lcom/google/obf/bc;->t:I

    move/from16 v0, p1

    if-ne v0, v9, :cond_d

    .line 493
    :cond_c
    const-string v8, "audio/vnd.dts.hd"

    goto/16 :goto_2

    .line 494
    :cond_d
    sget v9, Lcom/google/obf/bc;->u:I

    move/from16 v0, p1

    if-ne v0, v9, :cond_e

    .line 495
    const-string v8, "audio/vnd.dts.hd;profile=lbr"

    goto/16 :goto_2

    .line 496
    :cond_e
    sget v9, Lcom/google/obf/bc;->ax:I

    move/from16 v0, p1

    if-ne v0, v9, :cond_f

    .line 497
    const-string v8, "audio/3gpp"

    goto/16 :goto_2

    .line 498
    :cond_f
    sget v9, Lcom/google/obf/bc;->ay:I

    move/from16 v0, p1

    if-ne v0, v9, :cond_10

    .line 499
    const-string v8, "audio/amr-wb"

    goto/16 :goto_2

    .line 500
    :cond_10
    sget v9, Lcom/google/obf/bc;->l:I

    move/from16 v0, p1

    if-eq v0, v9, :cond_11

    sget v9, Lcom/google/obf/bc;->m:I

    move/from16 v0, p1

    if-ne v0, v9, :cond_12

    .line 501
    :cond_11
    const-string v8, "audio/raw"

    goto/16 :goto_2

    .line 502
    :cond_12
    sget v9, Lcom/google/obf/bc;->j:I

    move/from16 v0, p1

    if-ne v0, v9, :cond_3

    .line 503
    const-string v8, "audio/mpeg"

    goto/16 :goto_2

    .line 508
    :cond_13
    const/4 v6, 0x0

    goto/16 :goto_4

    .line 512
    :cond_14
    move-object/from16 v0, p0

    move/from16 v1, v16

    move/from16 v2, v18

    invoke-static {v0, v1, v2}, Lcom/google/obf/bd;->a(Lcom/google/obf/dw;II)I

    move-result v6

    goto/16 :goto_5

    .line 523
    :cond_15
    sget v8, Lcom/google/obf/bc;->o:I

    if-ne v6, v8, :cond_16

    .line 524
    add-int/lit8 v6, v16, 0x8

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/google/obf/dw;->c(I)V

    .line 525
    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    move-wide/from16 v1, p5

    move-object/from16 v3, p7

    invoke-static {v0, v6, v1, v2, v3}, Lcom/google/obf/dk;->a(Lcom/google/obf/dw;Ljava/lang/String;JLjava/lang/String;)Lcom/google/obf/q;

    move-result-object v6

    move-object/from16 v0, p9

    iput-object v6, v0, Lcom/google/obf/bd$d;->b:Lcom/google/obf/q;

    goto/16 :goto_7

    .line 526
    :cond_16
    sget v8, Lcom/google/obf/bc;->q:I

    if-ne v6, v8, :cond_17

    .line 527
    add-int/lit8 v6, v16, 0x8

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/google/obf/dw;->c(I)V

    .line 528
    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    move-wide/from16 v1, p5

    move-object/from16 v3, p7

    invoke-static {v0, v6, v1, v2, v3}, Lcom/google/obf/dk;->b(Lcom/google/obf/dw;Ljava/lang/String;JLjava/lang/String;)Lcom/google/obf/q;

    move-result-object v6

    move-object/from16 v0, p9

    iput-object v6, v0, Lcom/google/obf/bd$d;->b:Lcom/google/obf/q;

    goto/16 :goto_7

    .line 529
    :cond_17
    sget v8, Lcom/google/obf/bc;->v:I

    if-ne v6, v8, :cond_6

    .line 530
    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v8, -0x1

    const/4 v9, -0x1

    const/4 v14, 0x0

    move-wide/from16 v10, p5

    move-object/from16 v15, p7

    invoke-static/range {v6 .. v15}, Lcom/google/obf/q;->a(Ljava/lang/String;Ljava/lang/String;IIJIILjava/util/List;Ljava/lang/String;)Lcom/google/obf/q;

    move-result-object v6

    move-object/from16 v0, p9

    iput-object v6, v0, Lcom/google/obf/bd$d;->b:Lcom/google/obf/q;

    goto/16 :goto_7

    .line 533
    :cond_18
    move-object/from16 v0, p9

    iget-object v6, v0, Lcom/google/obf/bd$d;->b:Lcom/google/obf/q;

    if-nez v6, :cond_19

    if-eqz v7, :cond_19

    .line 534
    const-string v6, "audio/raw"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1a

    const/16 v16, 0x2

    .line 536
    :goto_8
    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v8, -0x1

    const/4 v9, -0x1

    .line 537
    if-nez v17, :cond_1b

    const/4 v14, 0x0

    :goto_9
    move-wide/from16 v10, p5

    move-object/from16 v15, p7

    .line 538
    invoke-static/range {v6 .. v16}, Lcom/google/obf/q;->a(Ljava/lang/String;Ljava/lang/String;IIJIILjava/util/List;Ljava/lang/String;I)Lcom/google/obf/q;

    move-result-object v6

    move-object/from16 v0, p9

    iput-object v6, v0, Lcom/google/obf/bd$d;->b:Lcom/google/obf/q;

    .line 539
    :cond_19
    return-void

    .line 535
    :cond_1a
    const/16 v16, -0x1

    goto :goto_8

    .line 537
    :cond_1b
    invoke-static/range {v17 .. v17}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v14

    goto :goto_9

    :cond_1c
    move-object/from16 v8, v17

    goto/16 :goto_6
.end method

.method private static b(Lcom/google/obf/dw;I)Landroid/util/Pair;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/obf/dw;",
            "I)",
            "Landroid/util/Pair",
            "<",
            "Ljava/util/List",
            "<[B>;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v12, 0x1

    const/4 v1, 0x0

    .line 408
    add-int/lit8 v0, p1, 0x8

    add-int/lit8 v0, v0, 0x15

    invoke-virtual {p0, v0}, Lcom/google/obf/dw;->c(I)V

    .line 409
    invoke-virtual {p0}, Lcom/google/obf/dw;->f()I

    move-result v0

    and-int/lit8 v5, v0, 0x3

    .line 410
    invoke-virtual {p0}, Lcom/google/obf/dw;->f()I

    move-result v6

    .line 412
    invoke-virtual {p0}, Lcom/google/obf/dw;->d()I

    move-result v7

    move v3, v1

    move v4, v1

    .line 413
    :goto_0
    if-ge v3, v6, :cond_1

    .line 414
    invoke-virtual {p0, v12}, Lcom/google/obf/dw;->d(I)V

    .line 415
    invoke-virtual {p0}, Lcom/google/obf/dw;->g()I

    move-result v8

    move v0, v1

    move v2, v4

    .line 416
    :goto_1
    if-ge v0, v8, :cond_0

    .line 417
    invoke-virtual {p0}, Lcom/google/obf/dw;->g()I

    move-result v4

    .line 418
    add-int/lit8 v9, v4, 0x4

    add-int/2addr v2, v9

    .line 419
    invoke-virtual {p0, v4}, Lcom/google/obf/dw;->d(I)V

    .line 420
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 421
    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v4, v2

    goto :goto_0

    .line 422
    :cond_1
    invoke-virtual {p0, v7}, Lcom/google/obf/dw;->c(I)V

    .line 423
    new-array v7, v4, [B

    move v3, v1

    move v0, v1

    .line 425
    :goto_2
    if-ge v3, v6, :cond_3

    .line 426
    invoke-virtual {p0, v12}, Lcom/google/obf/dw;->d(I)V

    .line 427
    invoke-virtual {p0}, Lcom/google/obf/dw;->g()I

    move-result v8

    move v2, v0

    move v0, v1

    .line 428
    :goto_3
    if-ge v0, v8, :cond_2

    .line 429
    invoke-virtual {p0}, Lcom/google/obf/dw;->g()I

    move-result v9

    .line 430
    sget-object v10, Lcom/google/obf/du;->a:[B

    sget-object v11, Lcom/google/obf/du;->a:[B

    array-length v11, v11

    invoke-static {v10, v1, v7, v2, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 431
    sget-object v10, Lcom/google/obf/du;->a:[B

    array-length v10, v10

    add-int/2addr v2, v10

    .line 432
    iget-object v10, p0, Lcom/google/obf/dw;->a:[B

    invoke-virtual {p0}, Lcom/google/obf/dw;->d()I

    move-result v11

    invoke-static {v10, v11, v7, v2, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 433
    add-int/2addr v2, v9

    .line 434
    invoke-virtual {p0, v9}, Lcom/google/obf/dw;->d(I)V

    .line 435
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 436
    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v0, v2

    goto :goto_2

    .line 437
    :cond_3
    if-nez v4, :cond_4

    const/4 v0, 0x0

    .line 438
    :goto_4
    add-int/lit8 v1, v5, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0

    .line 437
    :cond_4
    invoke-static {v7}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_4
.end method

.method private static b(Lcom/google/obf/dw;II)Landroid/util/Pair;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/obf/dw;",
            "II)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/obf/bl;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 605
    add-int/lit8 v0, p1, 0x8

    move-object v1, v5

    move v2, v4

    move v6, v0

    move-object v0, v5

    .line 609
    :goto_0
    sub-int v7, v6, p1

    if-ge v7, p2, :cond_4

    .line 610
    invoke-virtual {p0, v6}, Lcom/google/obf/dw;->c(I)V

    .line 611
    invoke-virtual {p0}, Lcom/google/obf/dw;->m()I

    move-result v7

    .line 612
    invoke-virtual {p0}, Lcom/google/obf/dw;->m()I

    move-result v8

    .line 613
    sget v9, Lcom/google/obf/bc;->ab:I

    if-ne v8, v9, :cond_1

    .line 614
    invoke-virtual {p0}, Lcom/google/obf/dw;->m()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 620
    :cond_0
    :goto_1
    add-int/2addr v6, v7

    .line 621
    goto :goto_0

    .line 615
    :cond_1
    sget v9, Lcom/google/obf/bc;->W:I

    if-ne v8, v9, :cond_3

    .line 616
    const/4 v2, 0x4

    invoke-virtual {p0, v2}, Lcom/google/obf/dw;->d(I)V

    .line 617
    invoke-virtual {p0}, Lcom/google/obf/dw;->m()I

    move-result v2

    sget v8, Lcom/google/obf/bd;->a:I

    if-ne v2, v8, :cond_2

    move v2, v3

    goto :goto_1

    :cond_2
    move v2, v4

    goto :goto_1

    .line 618
    :cond_3
    sget v9, Lcom/google/obf/bc;->X:I

    if-ne v8, v9, :cond_0

    .line 619
    invoke-static {p0, v6, v7}, Lcom/google/obf/bd;->c(Lcom/google/obf/dw;II)Lcom/google/obf/bl;

    move-result-object v1

    goto :goto_1

    .line 622
    :cond_4
    if-eqz v2, :cond_7

    .line 623
    if-eqz v0, :cond_5

    move v2, v3

    :goto_2
    const-string v5, "frma atom is mandatory"

    invoke-static {v2, v5}, Lcom/google/obf/dl;->a(ZLjava/lang/Object;)V

    .line 624
    if-eqz v1, :cond_6

    :goto_3
    const-string v2, "schi->tenc atom is mandatory"

    invoke-static {v3, v2}, Lcom/google/obf/dl;->a(ZLjava/lang/Object;)V

    .line 625
    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    .line 626
    :goto_4
    return-object v0

    :cond_5
    move v2, v4

    .line 623
    goto :goto_2

    :cond_6
    move v3, v4

    .line 624
    goto :goto_3

    :cond_7
    move-object v0, v5

    .line 626
    goto :goto_4
.end method

.method private static b(Lcom/google/obf/dw;)Lcom/google/obf/an;
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v3, 0x0

    .line 211
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/google/obf/dw;->b()I

    move-result v0

    if-lez v0, :cond_5

    .line 212
    invoke-virtual {p0}, Lcom/google/obf/dw;->d()I

    move-result v0

    .line 213
    invoke-virtual {p0}, Lcom/google/obf/dw;->m()I

    move-result v1

    add-int v4, v0, v1

    .line 214
    invoke-virtual {p0}, Lcom/google/obf/dw;->m()I

    move-result v0

    .line 215
    sget v1, Lcom/google/obf/bc;->aN:I

    if-ne v0, v1, :cond_6

    move-object v0, v3

    move-object v1, v3

    move-object v2, v3

    .line 219
    :goto_1
    invoke-virtual {p0}, Lcom/google/obf/dw;->d()I

    move-result v5

    if-ge v5, v4, :cond_4

    .line 220
    invoke-virtual {p0}, Lcom/google/obf/dw;->m()I

    move-result v5

    add-int/lit8 v5, v5, -0xc

    .line 221
    invoke-virtual {p0}, Lcom/google/obf/dw;->m()I

    move-result v6

    .line 222
    invoke-virtual {p0, v8}, Lcom/google/obf/dw;->d(I)V

    .line 223
    sget v7, Lcom/google/obf/bc;->aC:I

    if-ne v6, v7, :cond_1

    .line 224
    invoke-virtual {p0, v5}, Lcom/google/obf/dw;->e(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 225
    :cond_1
    sget v7, Lcom/google/obf/bc;->aD:I

    if-ne v6, v7, :cond_2

    .line 226
    invoke-virtual {p0, v5}, Lcom/google/obf/dw;->e(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 227
    :cond_2
    sget v7, Lcom/google/obf/bc;->aE:I

    if-ne v6, v7, :cond_3

    .line 228
    invoke-virtual {p0, v8}, Lcom/google/obf/dw;->d(I)V

    .line 229
    add-int/lit8 v0, v5, -0x4

    invoke-virtual {p0, v0}, Lcom/google/obf/dw;->e(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 230
    :cond_3
    invoke-virtual {p0, v5}, Lcom/google/obf/dw;->d(I)V

    goto :goto_1

    .line 232
    :cond_4
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    const-string v4, "com.apple.iTunes"

    .line 233
    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 234
    invoke-static {v1, v0}, Lcom/google/obf/an;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/obf/an;

    move-result-object v3

    .line 238
    :cond_5
    return-object v3

    .line 236
    :cond_6
    invoke-virtual {p0, v4}, Lcom/google/obf/dw;->c(I)V

    goto :goto_0
.end method

.method private static c(Lcom/google/obf/dw;I)F
    .locals 2

    .prologue
    .line 458
    add-int/lit8 v0, p1, 0x8

    invoke-virtual {p0, v0}, Lcom/google/obf/dw;->c(I)V

    .line 459
    invoke-virtual {p0}, Lcom/google/obf/dw;->s()I

    move-result v0

    .line 460
    invoke-virtual {p0}, Lcom/google/obf/dw;->s()I

    move-result v1

    .line 461
    int-to-float v0, v0

    int-to-float v1, v1

    div-float/2addr v0, v1

    return v0
.end method

.method private static c(Lcom/google/obf/dw;)J
    .locals 2

    .prologue
    const/16 v0, 0x8

    .line 239
    invoke-virtual {p0, v0}, Lcom/google/obf/dw;->c(I)V

    .line 240
    invoke-virtual {p0}, Lcom/google/obf/dw;->m()I

    move-result v1

    .line 241
    invoke-static {v1}, Lcom/google/obf/bc;->a(I)I

    move-result v1

    .line 242
    if-nez v1, :cond_0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/obf/dw;->d(I)V

    .line 243
    invoke-virtual {p0}, Lcom/google/obf/dw;->k()J

    move-result-wide v0

    return-wide v0

    .line 242
    :cond_0
    const/16 v0, 0x10

    goto :goto_0
.end method

.method private static c(Lcom/google/obf/dw;II)Lcom/google/obf/bl;
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 627
    add-int/lit8 v2, p1, 0x8

    .line 628
    :goto_0
    sub-int v3, v2, p1

    if-ge v3, p2, :cond_2

    .line 629
    invoke-virtual {p0, v2}, Lcom/google/obf/dw;->c(I)V

    .line 630
    invoke-virtual {p0}, Lcom/google/obf/dw;->m()I

    move-result v3

    .line 631
    invoke-virtual {p0}, Lcom/google/obf/dw;->m()I

    move-result v4

    .line 632
    sget v5, Lcom/google/obf/bc;->Y:I

    if-ne v4, v5, :cond_1

    .line 633
    const/4 v2, 0x6

    invoke-virtual {p0, v2}, Lcom/google/obf/dw;->d(I)V

    .line 634
    invoke-virtual {p0}, Lcom/google/obf/dw;->f()I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 635
    :goto_1
    invoke-virtual {p0}, Lcom/google/obf/dw;->f()I

    move-result v2

    .line 636
    const/16 v3, 0x10

    new-array v3, v3, [B

    .line 637
    array-length v4, v3

    invoke-virtual {p0, v3, v1, v4}, Lcom/google/obf/dw;->a([BII)V

    .line 638
    new-instance v1, Lcom/google/obf/bl;

    invoke-direct {v1, v0, v2, v3}, Lcom/google/obf/bl;-><init>(ZI[B)V

    move-object v0, v1

    .line 641
    :goto_2
    return-object v0

    :cond_0
    move v0, v1

    .line 634
    goto :goto_1

    .line 639
    :cond_1
    add-int/2addr v2, v3

    .line 640
    goto :goto_0

    .line 641
    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method

.method private static d(Lcom/google/obf/dw;I)Landroid/util/Pair;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/obf/dw;",
            "I)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "[B>;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 551
    add-int/lit8 v1, p1, 0x8

    add-int/lit8 v1, v1, 0x4

    invoke-virtual {p0, v1}, Lcom/google/obf/dw;->c(I)V

    .line 552
    invoke-virtual {p0, v3}, Lcom/google/obf/dw;->d(I)V

    .line 553
    invoke-static {p0}, Lcom/google/obf/bd;->g(Lcom/google/obf/dw;)I

    .line 554
    invoke-virtual {p0, v4}, Lcom/google/obf/dw;->d(I)V

    .line 555
    invoke-virtual {p0}, Lcom/google/obf/dw;->f()I

    move-result v1

    .line 556
    and-int/lit16 v2, v1, 0x80

    if-eqz v2, :cond_0

    .line 557
    invoke-virtual {p0, v4}, Lcom/google/obf/dw;->d(I)V

    .line 558
    :cond_0
    and-int/lit8 v2, v1, 0x40

    if-eqz v2, :cond_1

    .line 559
    invoke-virtual {p0}, Lcom/google/obf/dw;->g()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/obf/dw;->d(I)V

    .line 560
    :cond_1
    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_2

    .line 561
    invoke-virtual {p0, v4}, Lcom/google/obf/dw;->d(I)V

    .line 562
    :cond_2
    invoke-virtual {p0, v3}, Lcom/google/obf/dw;->d(I)V

    .line 563
    invoke-static {p0}, Lcom/google/obf/bd;->g(Lcom/google/obf/dw;)I

    .line 564
    invoke-virtual {p0}, Lcom/google/obf/dw;->f()I

    move-result v1

    .line 565
    sparse-switch v1, :sswitch_data_0

    .line 585
    :goto_0
    const/16 v1, 0xc

    invoke-virtual {p0, v1}, Lcom/google/obf/dw;->d(I)V

    .line 586
    invoke-virtual {p0, v3}, Lcom/google/obf/dw;->d(I)V

    .line 587
    invoke-static {p0}, Lcom/google/obf/bd;->g(Lcom/google/obf/dw;)I

    move-result v1

    .line 588
    new-array v2, v1, [B

    .line 589
    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3, v1}, Lcom/google/obf/dw;->a([BII)V

    .line 590
    invoke-static {v0, v2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    :goto_1
    return-object v0

    .line 566
    :sswitch_0
    const-string v1, "audio/mpeg"

    .line 567
    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_1

    .line 568
    :sswitch_1
    const-string v0, "video/mp4v-es"

    goto :goto_0

    .line 570
    :sswitch_2
    const-string v0, "video/avc"

    goto :goto_0

    .line 572
    :sswitch_3
    const-string v0, "video/hevc"

    goto :goto_0

    .line 574
    :sswitch_4
    const-string v0, "audio/mp4a-latm"

    goto :goto_0

    .line 576
    :sswitch_5
    const-string v0, "audio/ac3"

    goto :goto_0

    .line 578
    :sswitch_6
    const-string v0, "audio/eac3"

    goto :goto_0

    .line 580
    :sswitch_7
    const-string v1, "audio/vnd.dts"

    .line 581
    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_1

    .line 582
    :sswitch_8
    const-string v1, "audio/vnd.dts.hd"

    .line 583
    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_1

    .line 565
    nop

    :sswitch_data_0
    .sparse-switch
        0x20 -> :sswitch_1
        0x21 -> :sswitch_2
        0x23 -> :sswitch_3
        0x40 -> :sswitch_4
        0x66 -> :sswitch_4
        0x67 -> :sswitch_4
        0x68 -> :sswitch_4
        0x6b -> :sswitch_0
        0xa5 -> :sswitch_5
        0xa6 -> :sswitch_6
        0xa9 -> :sswitch_7
        0xaa -> :sswitch_8
        0xab -> :sswitch_8
        0xac -> :sswitch_7
    .end sparse-switch
.end method

.method private static d(Lcom/google/obf/dw;)Lcom/google/obf/bd$g;
    .locals 13

    .prologue
    const-wide/16 v4, -0x1

    const/16 v2, 0x10

    const/16 v1, 0x8

    const/4 v3, 0x4

    const/4 v6, 0x0

    .line 244
    invoke-virtual {p0, v1}, Lcom/google/obf/dw;->c(I)V

    .line 245
    invoke-virtual {p0}, Lcom/google/obf/dw;->m()I

    move-result v0

    .line 246
    invoke-static {v0}, Lcom/google/obf/bc;->a(I)I

    move-result v8

    .line 247
    if-nez v8, :cond_3

    move v0, v1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/obf/dw;->d(I)V

    .line 248
    invoke-virtual {p0}, Lcom/google/obf/dw;->m()I

    move-result v9

    .line 249
    invoke-virtual {p0, v3}, Lcom/google/obf/dw;->d(I)V

    .line 250
    const/4 v0, 0x1

    .line 251
    invoke-virtual {p0}, Lcom/google/obf/dw;->d()I

    move-result v10

    .line 252
    if-nez v8, :cond_0

    move v1, v3

    :cond_0
    move v7, v6

    .line 253
    :goto_1
    if-ge v7, v1, :cond_1

    .line 254
    iget-object v11, p0, Lcom/google/obf/dw;->a:[B

    add-int v12, v10, v7

    aget-byte v11, v11, v12

    const/4 v12, -0x1

    if-eq v11, v12, :cond_4

    move v0, v6

    .line 258
    :cond_1
    if-eqz v0, :cond_5

    .line 259
    invoke-virtual {p0, v1}, Lcom/google/obf/dw;->d(I)V

    move-wide v0, v4

    .line 264
    :cond_2
    :goto_2
    invoke-virtual {p0, v2}, Lcom/google/obf/dw;->d(I)V

    .line 265
    invoke-virtual {p0}, Lcom/google/obf/dw;->m()I

    move-result v2

    .line 266
    invoke-virtual {p0}, Lcom/google/obf/dw;->m()I

    move-result v4

    .line 267
    invoke-virtual {p0, v3}, Lcom/google/obf/dw;->d(I)V

    .line 268
    invoke-virtual {p0}, Lcom/google/obf/dw;->m()I

    move-result v3

    .line 269
    invoke-virtual {p0}, Lcom/google/obf/dw;->m()I

    move-result v5

    .line 270
    const/high16 v7, 0x10000

    .line 271
    if-nez v2, :cond_7

    if-ne v4, v7, :cond_7

    neg-int v8, v7

    if-ne v3, v8, :cond_7

    if-nez v5, :cond_7

    .line 272
    const/16 v2, 0x5a

    .line 278
    :goto_3
    new-instance v3, Lcom/google/obf/bd$g;

    invoke-direct {v3, v9, v0, v1, v2}, Lcom/google/obf/bd$g;-><init>(IJI)V

    return-object v3

    :cond_3
    move v0, v2

    .line 247
    goto :goto_0

    .line 257
    :cond_4
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 261
    :cond_5
    if-nez v8, :cond_6

    invoke-virtual {p0}, Lcom/google/obf/dw;->k()J

    move-result-wide v0

    .line 262
    :goto_4
    const-wide/16 v10, 0x0

    cmp-long v7, v0, v10

    if-nez v7, :cond_2

    move-wide v0, v4

    .line 263
    goto :goto_2

    .line 261
    :cond_6
    invoke-virtual {p0}, Lcom/google/obf/dw;->u()J

    move-result-wide v0

    goto :goto_4

    .line 273
    :cond_7
    if-nez v2, :cond_8

    neg-int v8, v7

    if-ne v4, v8, :cond_8

    if-ne v3, v7, :cond_8

    if-nez v5, :cond_8

    .line 274
    const/16 v2, 0x10e

    goto :goto_3

    .line 275
    :cond_8
    neg-int v8, v7

    if-ne v2, v8, :cond_9

    if-nez v4, :cond_9

    if-nez v3, :cond_9

    neg-int v2, v7

    if-ne v5, v2, :cond_9

    .line 276
    const/16 v2, 0xb4

    goto :goto_3

    :cond_9
    move v2, v6

    .line 277
    goto :goto_3
.end method

.method private static d(Lcom/google/obf/dw;II)[B
    .locals 4

    .prologue
    .line 642
    add-int/lit8 v0, p1, 0x8

    .line 643
    :goto_0
    sub-int v1, v0, p1

    if-ge v1, p2, :cond_1

    .line 644
    invoke-virtual {p0, v0}, Lcom/google/obf/dw;->c(I)V

    .line 645
    invoke-virtual {p0}, Lcom/google/obf/dw;->m()I

    move-result v1

    .line 646
    invoke-virtual {p0}, Lcom/google/obf/dw;->m()I

    move-result v2

    .line 647
    sget v3, Lcom/google/obf/bc;->aI:I

    if-ne v2, v3, :cond_0

    .line 648
    iget-object v2, p0, Lcom/google/obf/dw;->a:[B

    add-int/2addr v1, v0

    invoke-static {v2, v0, v1}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v0

    .line 651
    :goto_1
    return-object v0

    .line 649
    :cond_0
    add-int/2addr v0, v1

    .line 650
    goto :goto_0

    .line 651
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static e(Lcom/google/obf/dw;)I
    .locals 1

    .prologue
    .line 279
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Lcom/google/obf/dw;->c(I)V

    .line 280
    invoke-virtual {p0}, Lcom/google/obf/dw;->m()I

    move-result v0

    return v0
.end method

.method private static f(Lcom/google/obf/dw;)Landroid/util/Pair;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/obf/dw;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v1, 0x8

    .line 281
    invoke-virtual {p0, v1}, Lcom/google/obf/dw;->c(I)V

    .line 282
    invoke-virtual {p0}, Lcom/google/obf/dw;->m()I

    move-result v0

    .line 283
    invoke-static {v0}, Lcom/google/obf/bc;->a(I)I

    move-result v2

    .line 284
    if-nez v2, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/obf/dw;->d(I)V

    .line 285
    invoke-virtual {p0}, Lcom/google/obf/dw;->k()J

    move-result-wide v4

    .line 286
    if-nez v2, :cond_0

    const/4 v1, 0x4

    :cond_0
    invoke-virtual {p0, v1}, Lcom/google/obf/dw;->d(I)V

    .line 287
    invoke-virtual {p0}, Lcom/google/obf/dw;->g()I

    move-result v0

    .line 288
    shr-int/lit8 v1, v0, 0xa

    and-int/lit8 v1, v1, 0x1f

    add-int/lit8 v1, v1, 0x60

    int-to-char v1, v1

    shr-int/lit8 v2, v0, 0x5

    and-int/lit8 v2, v2, 0x1f

    add-int/lit8 v2, v2, 0x60

    int-to-char v2, v2

    and-int/lit8 v0, v0, 0x1f

    add-int/lit8 v0, v0, 0x60

    int-to-char v0, v0

    const/4 v3, 0x3

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 289
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0

    .line 284
    :cond_1
    const/16 v0, 0x10

    goto :goto_0
.end method

.method private static g(Lcom/google/obf/dw;)I
    .locals 3

    .prologue
    .line 652
    invoke-virtual {p0}, Lcom/google/obf/dw;->f()I

    move-result v1

    .line 653
    and-int/lit8 v0, v1, 0x7f

    .line 654
    :goto_0
    and-int/lit16 v1, v1, 0x80

    const/16 v2, 0x80

    if-ne v1, v2, :cond_0

    .line 655
    invoke-virtual {p0}, Lcom/google/obf/dw;->f()I

    move-result v1

    .line 656
    shl-int/lit8 v0, v0, 0x7

    and-int/lit8 v2, v1, 0x7f

    or-int/2addr v0, v2

    goto :goto_0

    .line 657
    :cond_0
    return v0
.end method
