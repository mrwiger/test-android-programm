.class final Lcom/google/obf/cd$a$a;
.super Ljava/lang/Object;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/obf/cd$a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation


# instance fields
.field private a:Z

.field private b:Z

.field private c:Lcom/google/obf/du$b;

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:I

.field private m:I

.field private n:I

.field private o:I

.field private p:I


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/obf/cd$1;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/google/obf/cd$a$a;-><init>()V

    return-void
.end method

.method private a(Lcom/google/obf/cd$a$a;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 26
    iget-boolean v1, p0, Lcom/google/obf/cd$a$a;->a:Z

    if-eqz v1, :cond_5

    iget-boolean v1, p1, Lcom/google/obf/cd$a$a;->a:Z

    if-eqz v1, :cond_4

    iget v1, p0, Lcom/google/obf/cd$a$a;->f:I

    iget v2, p1, Lcom/google/obf/cd$a$a;->f:I

    if-ne v1, v2, :cond_4

    iget v1, p0, Lcom/google/obf/cd$a$a;->g:I

    iget v2, p1, Lcom/google/obf/cd$a$a;->g:I

    if-ne v1, v2, :cond_4

    iget-boolean v1, p0, Lcom/google/obf/cd$a$a;->h:Z

    iget-boolean v2, p1, Lcom/google/obf/cd$a$a;->h:Z

    if-ne v1, v2, :cond_4

    iget-boolean v1, p0, Lcom/google/obf/cd$a$a;->i:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p1, Lcom/google/obf/cd$a$a;->i:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/google/obf/cd$a$a;->j:Z

    iget-boolean v2, p1, Lcom/google/obf/cd$a$a;->j:Z

    if-ne v1, v2, :cond_4

    :cond_0
    iget v1, p0, Lcom/google/obf/cd$a$a;->d:I

    iget v2, p1, Lcom/google/obf/cd$a$a;->d:I

    if-eq v1, v2, :cond_1

    iget v1, p0, Lcom/google/obf/cd$a$a;->d:I

    if-eqz v1, :cond_4

    iget v1, p1, Lcom/google/obf/cd$a$a;->d:I

    if-eqz v1, :cond_4

    :cond_1
    iget-object v1, p0, Lcom/google/obf/cd$a$a;->c:Lcom/google/obf/du$b;

    iget v1, v1, Lcom/google/obf/du$b;->h:I

    if-nez v1, :cond_2

    iget-object v1, p1, Lcom/google/obf/cd$a$a;->c:Lcom/google/obf/du$b;

    iget v1, v1, Lcom/google/obf/du$b;->h:I

    if-nez v1, :cond_2

    iget v1, p0, Lcom/google/obf/cd$a$a;->m:I

    iget v2, p1, Lcom/google/obf/cd$a$a;->m:I

    if-ne v1, v2, :cond_4

    iget v1, p0, Lcom/google/obf/cd$a$a;->n:I

    iget v2, p1, Lcom/google/obf/cd$a$a;->n:I

    if-ne v1, v2, :cond_4

    :cond_2
    iget-object v1, p0, Lcom/google/obf/cd$a$a;->c:Lcom/google/obf/du$b;

    iget v1, v1, Lcom/google/obf/du$b;->h:I

    if-ne v1, v0, :cond_3

    iget-object v1, p1, Lcom/google/obf/cd$a$a;->c:Lcom/google/obf/du$b;

    iget v1, v1, Lcom/google/obf/du$b;->h:I

    if-ne v1, v0, :cond_3

    iget v1, p0, Lcom/google/obf/cd$a$a;->o:I

    iget v2, p1, Lcom/google/obf/cd$a$a;->o:I

    if-ne v1, v2, :cond_4

    iget v1, p0, Lcom/google/obf/cd$a$a;->p:I

    iget v2, p1, Lcom/google/obf/cd$a$a;->p:I

    if-ne v1, v2, :cond_4

    :cond_3
    iget-boolean v1, p0, Lcom/google/obf/cd$a$a;->k:Z

    iget-boolean v2, p1, Lcom/google/obf/cd$a$a;->k:Z

    if-ne v1, v2, :cond_4

    iget-boolean v1, p0, Lcom/google/obf/cd$a$a;->k:Z

    if-eqz v1, :cond_5

    iget-boolean v1, p1, Lcom/google/obf/cd$a$a;->k:Z

    if-eqz v1, :cond_5

    iget v1, p0, Lcom/google/obf/cd$a$a;->l:I

    iget v2, p1, Lcom/google/obf/cd$a$a;->l:I

    if-eq v1, v2, :cond_5

    :cond_4
    :goto_0
    return v0

    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/obf/cd$a$a;Lcom/google/obf/cd$a$a;)Z
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/google/obf/cd$a$a;->a(Lcom/google/obf/cd$a$a;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2
    iput-boolean v0, p0, Lcom/google/obf/cd$a$a;->b:Z

    .line 3
    iput-boolean v0, p0, Lcom/google/obf/cd$a$a;->a:Z

    .line 4
    return-void
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 5
    iput p1, p0, Lcom/google/obf/cd$a$a;->e:I

    .line 6
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/obf/cd$a$a;->b:Z

    .line 7
    return-void
.end method

.method public a(Lcom/google/obf/du$b;IIIIZZZZIIIII)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 8
    iput-object p1, p0, Lcom/google/obf/cd$a$a;->c:Lcom/google/obf/du$b;

    .line 9
    iput p2, p0, Lcom/google/obf/cd$a$a;->d:I

    .line 10
    iput p3, p0, Lcom/google/obf/cd$a$a;->e:I

    .line 11
    iput p4, p0, Lcom/google/obf/cd$a$a;->f:I

    .line 12
    iput p5, p0, Lcom/google/obf/cd$a$a;->g:I

    .line 13
    iput-boolean p6, p0, Lcom/google/obf/cd$a$a;->h:Z

    .line 14
    iput-boolean p7, p0, Lcom/google/obf/cd$a$a;->i:Z

    .line 15
    iput-boolean p8, p0, Lcom/google/obf/cd$a$a;->j:Z

    .line 16
    iput-boolean p9, p0, Lcom/google/obf/cd$a$a;->k:Z

    .line 17
    iput p10, p0, Lcom/google/obf/cd$a$a;->l:I

    .line 18
    iput p11, p0, Lcom/google/obf/cd$a$a;->m:I

    .line 19
    iput p12, p0, Lcom/google/obf/cd$a$a;->n:I

    .line 20
    iput p13, p0, Lcom/google/obf/cd$a$a;->o:I

    .line 21
    iput p14, p0, Lcom/google/obf/cd$a$a;->p:I

    .line 22
    iput-boolean v0, p0, Lcom/google/obf/cd$a$a;->a:Z

    .line 23
    iput-boolean v0, p0, Lcom/google/obf/cd$a$a;->b:Z

    .line 24
    return-void
.end method

.method public b()Z
    .locals 2

    .prologue
    .line 25
    iget-boolean v0, p0, Lcom/google/obf/cd$a$a;->b:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/obf/cd$a$a;->e:I

    const/4 v1, 0x7

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/obf/cd$a$a;->e:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
