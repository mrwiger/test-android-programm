.class final Lcom/google/obf/ap$a;
.super Ljava/lang/Object;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/obf/ap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation


# instance fields
.field private a:I

.field private b:[J

.field private c:[I

.field private d:[I

.field private e:[J

.field private f:[[B

.field private g:I

.field private h:I

.field private i:I

.field private j:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/google/obf/ap$a;->a:I

    .line 3
    iget v0, p0, Lcom/google/obf/ap$a;->a:I

    new-array v0, v0, [J

    iput-object v0, p0, Lcom/google/obf/ap$a;->b:[J

    .line 4
    iget v0, p0, Lcom/google/obf/ap$a;->a:I

    new-array v0, v0, [J

    iput-object v0, p0, Lcom/google/obf/ap$a;->e:[J

    .line 5
    iget v0, p0, Lcom/google/obf/ap$a;->a:I

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/obf/ap$a;->d:[I

    .line 6
    iget v0, p0, Lcom/google/obf/ap$a;->a:I

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/obf/ap$a;->c:[I

    .line 7
    iget v0, p0, Lcom/google/obf/ap$a;->a:I

    new-array v0, v0, [[B

    iput-object v0, p0, Lcom/google/obf/ap$a;->f:[[B

    .line 8
    return-void
.end method


# virtual methods
.method public declared-synchronized a(J)J
    .locals 9

    .prologue
    const/4 v5, -0x1

    const-wide/16 v0, -0x1

    .line 30
    monitor-enter p0

    :try_start_0
    iget v2, p0, Lcom/google/obf/ap$a;->g:I

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/obf/ap$a;->e:[J

    iget v3, p0, Lcom/google/obf/ap$a;->i:I

    aget-wide v2, v2, v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    cmp-long v2, p1, v2

    if-gez v2, :cond_1

    .line 51
    :cond_0
    :goto_0
    monitor-exit p0

    return-wide v0

    .line 32
    :cond_1
    :try_start_1
    iget v2, p0, Lcom/google/obf/ap$a;->j:I

    if-nez v2, :cond_3

    iget v2, p0, Lcom/google/obf/ap$a;->a:I

    :goto_1
    add-int/lit8 v2, v2, -0x1

    .line 33
    iget-object v3, p0, Lcom/google/obf/ap$a;->e:[J

    aget-wide v2, v3, v2

    .line 34
    cmp-long v2, p1, v2

    if-gtz v2, :cond_0

    .line 36
    const/4 v3, 0x0

    .line 38
    iget v2, p0, Lcom/google/obf/ap$a;->i:I

    move v4, v2

    move v2, v5

    .line 39
    :goto_2
    iget v6, p0, Lcom/google/obf/ap$a;->j:I

    if-eq v4, v6, :cond_2

    .line 40
    iget-object v6, p0, Lcom/google/obf/ap$a;->e:[J

    aget-wide v6, v6, v4

    cmp-long v6, v6, p1

    if-lez v6, :cond_4

    .line 46
    :cond_2
    if-eq v2, v5, :cond_0

    .line 48
    iget v0, p0, Lcom/google/obf/ap$a;->g:I

    sub-int/2addr v0, v2

    iput v0, p0, Lcom/google/obf/ap$a;->g:I

    .line 49
    iget v0, p0, Lcom/google/obf/ap$a;->i:I

    add-int/2addr v0, v2

    iget v1, p0, Lcom/google/obf/ap$a;->a:I

    rem-int/2addr v0, v1

    iput v0, p0, Lcom/google/obf/ap$a;->i:I

    .line 50
    iget v0, p0, Lcom/google/obf/ap$a;->h:I

    add-int/2addr v0, v2

    iput v0, p0, Lcom/google/obf/ap$a;->h:I

    .line 51
    iget-object v0, p0, Lcom/google/obf/ap$a;->b:[J

    iget v1, p0, Lcom/google/obf/ap$a;->i:I

    aget-wide v0, v0, v1

    goto :goto_0

    .line 32
    :cond_3
    iget v2, p0, Lcom/google/obf/ap$a;->j:I

    goto :goto_1

    .line 42
    :cond_4
    iget-object v6, p0, Lcom/google/obf/ap$a;->d:[I

    aget v6, v6, v4

    and-int/lit8 v6, v6, 0x1

    if-eqz v6, :cond_5

    move v2, v3

    .line 44
    :cond_5
    add-int/lit8 v4, v4, 0x1

    iget v6, p0, Lcom/google/obf/ap$a;->a:I

    rem-int/2addr v4, v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 45
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 30
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 9
    iput v0, p0, Lcom/google/obf/ap$a;->h:I

    .line 10
    iput v0, p0, Lcom/google/obf/ap$a;->i:I

    .line 11
    iput v0, p0, Lcom/google/obf/ap$a;->j:I

    .line 12
    iput v0, p0, Lcom/google/obf/ap$a;->g:I

    .line 13
    return-void
.end method

.method public declared-synchronized a(JIJI[B)V
    .locals 10

    .prologue
    .line 52
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/obf/ap$a;->e:[J

    iget v1, p0, Lcom/google/obf/ap$a;->j:I

    aput-wide p1, v0, v1

    .line 53
    iget-object v0, p0, Lcom/google/obf/ap$a;->b:[J

    iget v1, p0, Lcom/google/obf/ap$a;->j:I

    aput-wide p4, v0, v1

    .line 54
    iget-object v0, p0, Lcom/google/obf/ap$a;->c:[I

    iget v1, p0, Lcom/google/obf/ap$a;->j:I

    aput p6, v0, v1

    .line 55
    iget-object v0, p0, Lcom/google/obf/ap$a;->d:[I

    iget v1, p0, Lcom/google/obf/ap$a;->j:I

    aput p3, v0, v1

    .line 56
    iget-object v0, p0, Lcom/google/obf/ap$a;->f:[[B

    iget v1, p0, Lcom/google/obf/ap$a;->j:I

    aput-object p7, v0, v1

    .line 57
    iget v0, p0, Lcom/google/obf/ap$a;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/obf/ap$a;->g:I

    .line 58
    iget v0, p0, Lcom/google/obf/ap$a;->g:I

    iget v1, p0, Lcom/google/obf/ap$a;->a:I

    if-ne v0, v1, :cond_1

    .line 59
    iget v0, p0, Lcom/google/obf/ap$a;->a:I

    add-int/lit16 v0, v0, 0x3e8

    .line 60
    new-array v1, v0, [J

    .line 61
    new-array v2, v0, [J

    .line 62
    new-array v3, v0, [I

    .line 63
    new-array v4, v0, [I

    .line 64
    new-array v5, v0, [[B

    .line 65
    iget v6, p0, Lcom/google/obf/ap$a;->a:I

    iget v7, p0, Lcom/google/obf/ap$a;->i:I

    sub-int/2addr v6, v7

    .line 66
    iget-object v7, p0, Lcom/google/obf/ap$a;->b:[J

    iget v8, p0, Lcom/google/obf/ap$a;->i:I

    const/4 v9, 0x0

    invoke-static {v7, v8, v1, v9, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 67
    iget-object v7, p0, Lcom/google/obf/ap$a;->e:[J

    iget v8, p0, Lcom/google/obf/ap$a;->i:I

    const/4 v9, 0x0

    invoke-static {v7, v8, v2, v9, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 68
    iget-object v7, p0, Lcom/google/obf/ap$a;->d:[I

    iget v8, p0, Lcom/google/obf/ap$a;->i:I

    const/4 v9, 0x0

    invoke-static {v7, v8, v3, v9, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 69
    iget-object v7, p0, Lcom/google/obf/ap$a;->c:[I

    iget v8, p0, Lcom/google/obf/ap$a;->i:I

    const/4 v9, 0x0

    invoke-static {v7, v8, v4, v9, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 70
    iget-object v7, p0, Lcom/google/obf/ap$a;->f:[[B

    iget v8, p0, Lcom/google/obf/ap$a;->i:I

    const/4 v9, 0x0

    invoke-static {v7, v8, v5, v9, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 71
    iget v7, p0, Lcom/google/obf/ap$a;->i:I

    .line 72
    iget-object v8, p0, Lcom/google/obf/ap$a;->b:[J

    const/4 v9, 0x0

    invoke-static {v8, v9, v1, v6, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 73
    iget-object v8, p0, Lcom/google/obf/ap$a;->e:[J

    const/4 v9, 0x0

    invoke-static {v8, v9, v2, v6, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 74
    iget-object v8, p0, Lcom/google/obf/ap$a;->d:[I

    const/4 v9, 0x0

    invoke-static {v8, v9, v3, v6, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 75
    iget-object v8, p0, Lcom/google/obf/ap$a;->c:[I

    const/4 v9, 0x0

    invoke-static {v8, v9, v4, v6, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 76
    iget-object v8, p0, Lcom/google/obf/ap$a;->f:[[B

    const/4 v9, 0x0

    invoke-static {v8, v9, v5, v6, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 77
    iput-object v1, p0, Lcom/google/obf/ap$a;->b:[J

    .line 78
    iput-object v2, p0, Lcom/google/obf/ap$a;->e:[J

    .line 79
    iput-object v3, p0, Lcom/google/obf/ap$a;->d:[I

    .line 80
    iput-object v4, p0, Lcom/google/obf/ap$a;->c:[I

    .line 81
    iput-object v5, p0, Lcom/google/obf/ap$a;->f:[[B

    .line 82
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/obf/ap$a;->i:I

    .line 83
    iget v1, p0, Lcom/google/obf/ap$a;->a:I

    iput v1, p0, Lcom/google/obf/ap$a;->j:I

    .line 84
    iget v1, p0, Lcom/google/obf/ap$a;->a:I

    iput v1, p0, Lcom/google/obf/ap$a;->g:I

    .line 85
    iput v0, p0, Lcom/google/obf/ap$a;->a:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 90
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 87
    :cond_1
    :try_start_1
    iget v0, p0, Lcom/google/obf/ap$a;->j:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/obf/ap$a;->j:I

    .line 88
    iget v0, p0, Lcom/google/obf/ap$a;->j:I

    iget v1, p0, Lcom/google/obf/ap$a;->a:I

    if-ne v0, v1, :cond_0

    .line 89
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/obf/ap$a;->j:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 52
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Lcom/google/obf/t;Lcom/google/obf/ap$b;)Z
    .locals 2

    .prologue
    .line 14
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/obf/ap$a;->g:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 15
    const/4 v0, 0x0

    .line 21
    :goto_0
    monitor-exit p0

    return v0

    .line 16
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/obf/ap$a;->e:[J

    iget v1, p0, Lcom/google/obf/ap$a;->i:I

    aget-wide v0, v0, v1

    iput-wide v0, p1, Lcom/google/obf/t;->e:J

    .line 17
    iget-object v0, p0, Lcom/google/obf/ap$a;->c:[I

    iget v1, p0, Lcom/google/obf/ap$a;->i:I

    aget v0, v0, v1

    iput v0, p1, Lcom/google/obf/t;->c:I

    .line 18
    iget-object v0, p0, Lcom/google/obf/ap$a;->d:[I

    iget v1, p0, Lcom/google/obf/ap$a;->i:I

    aget v0, v0, v1

    iput v0, p1, Lcom/google/obf/t;->d:I

    .line 19
    iget-object v0, p0, Lcom/google/obf/ap$a;->b:[J

    iget v1, p0, Lcom/google/obf/ap$a;->i:I

    aget-wide v0, v0, v1

    iput-wide v0, p2, Lcom/google/obf/ap$b;->a:J

    .line 20
    iget-object v0, p0, Lcom/google/obf/ap$a;->f:[[B

    iget v1, p0, Lcom/google/obf/ap$a;->i:I

    aget-object v0, v0, v1

    iput-object v0, p2, Lcom/google/obf/ap$b;->b:[B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 21
    const/4 v0, 0x1

    goto :goto_0

    .line 14
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b()J
    .locals 4

    .prologue
    .line 22
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/obf/ap$a;->g:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/obf/ap$a;->g:I

    .line 23
    iget v0, p0, Lcom/google/obf/ap$a;->i:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/google/obf/ap$a;->i:I

    .line 24
    iget v1, p0, Lcom/google/obf/ap$a;->h:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/obf/ap$a;->h:I

    .line 25
    iget v1, p0, Lcom/google/obf/ap$a;->i:I

    iget v2, p0, Lcom/google/obf/ap$a;->a:I

    if-ne v1, v2, :cond_0

    .line 26
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/obf/ap$a;->i:I

    .line 27
    :cond_0
    iget v1, p0, Lcom/google/obf/ap$a;->g:I

    if-lez v1, :cond_1

    iget-object v0, p0, Lcom/google/obf/ap$a;->b:[J

    iget v1, p0, Lcom/google/obf/ap$a;->i:I

    aget-wide v0, v0, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 29
    :goto_0
    monitor-exit p0

    return-wide v0

    .line 28
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/google/obf/ap$a;->c:[I

    aget v1, v1, v0

    int-to-long v2, v1

    iget-object v1, p0, Lcom/google/obf/ap$a;->b:[J

    aget-wide v0, v1, v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    add-long/2addr v0, v2

    goto :goto_0

    .line 22
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
