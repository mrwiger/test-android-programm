.class Lru/cn/tv/stb/settings/SettingsViewModel;
.super Lru/cn/mvvm/RxViewModel;
.source "SettingsViewModel.java"


# instance fields
.field private final LOG_TAG:Ljava/lang/String;

.field private advancedPlaybackItem:Lru/cn/tv/stb/settings/PreferenceItem;

.field private final applicationContext:Landroid/content/Context;

.field private cachingItem:Lru/cn/tv/stb/settings/PreferenceItem;

.field private final loader:Lru/cn/mvvm/RxLoader;

.field private final preferenceListDialog:Landroid/arch/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/arch/lifecycle/MutableLiveData",
            "<",
            "Landroid/util/Pair",
            "<",
            "Lru/cn/tv/stb/settings/PreferenceItem;",
            "Lru/cn/tv/PreferenceListDialog$PreferenceListListener;",
            ">;>;"
        }
    .end annotation
.end field

.field private qualityItem:Lru/cn/tv/stb/settings/PreferenceItem;

.field private final reloadSignal:Lio/reactivex/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/subjects/PublishSubject",
            "<",
            "Lru/cn/mvvm/Any;",
            ">;"
        }
    .end annotation
.end field

.field private scalingItem:Lru/cn/tv/stb/settings/PreferenceItem;

.field private final settingsItems:Landroid/arch/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/arch/lifecycle/MutableLiveData",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private final userPlaylistDialog:Landroid/arch/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/arch/lifecycle/MutableLiveData",
            "<",
            "Lru/cn/tv/stb/settings/UserPlaylistItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lru/cn/mvvm/RxLoader;Landroid/content/Context;)V
    .locals 4
    .param p1, "loader"    # Lru/cn/mvvm/RxLoader;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 63
    invoke-direct {p0}, Lru/cn/mvvm/RxViewModel;-><init>()V

    .line 43
    const-class v0, Lru/cn/tv/stb/settings/SettingsViewModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lru/cn/tv/stb/settings/SettingsViewModel;->LOG_TAG:Ljava/lang/String;

    .line 64
    iput-object p1, p0, Lru/cn/tv/stb/settings/SettingsViewModel;->loader:Lru/cn/mvvm/RxLoader;

    .line 66
    iput-object p2, p0, Lru/cn/tv/stb/settings/SettingsViewModel;->applicationContext:Landroid/content/Context;

    .line 68
    new-instance v0, Landroid/arch/lifecycle/MutableLiveData;

    invoke-direct {v0}, Landroid/arch/lifecycle/MutableLiveData;-><init>()V

    iput-object v0, p0, Lru/cn/tv/stb/settings/SettingsViewModel;->settingsItems:Landroid/arch/lifecycle/MutableLiveData;

    .line 69
    new-instance v0, Landroid/arch/lifecycle/MutableLiveData;

    invoke-direct {v0}, Landroid/arch/lifecycle/MutableLiveData;-><init>()V

    iput-object v0, p0, Lru/cn/tv/stb/settings/SettingsViewModel;->preferenceListDialog:Landroid/arch/lifecycle/MutableLiveData;

    .line 70
    new-instance v0, Landroid/arch/lifecycle/MutableLiveData;

    invoke-direct {v0}, Landroid/arch/lifecycle/MutableLiveData;-><init>()V

    iput-object v0, p0, Lru/cn/tv/stb/settings/SettingsViewModel;->userPlaylistDialog:Landroid/arch/lifecycle/MutableLiveData;

    .line 72
    new-instance v0, Lru/cn/tv/stb/settings/PreferenceItem;

    const-string v1, "advanced_playback"

    const v2, 0x7f0e012f

    const v3, 0x7f030003

    invoke-direct {v0, v1, v2, v3}, Lru/cn/tv/stb/settings/PreferenceItem;-><init>(Ljava/lang/String;II)V

    iput-object v0, p0, Lru/cn/tv/stb/settings/SettingsViewModel;->advancedPlaybackItem:Lru/cn/tv/stb/settings/PreferenceItem;

    .line 77
    new-instance v0, Lru/cn/tv/stb/settings/PreferenceItem;

    const-string v1, "caching_level"

    const v2, 0x7f0e0130

    const v3, 0x7f030002

    invoke-direct {v0, v1, v2, v3}, Lru/cn/tv/stb/settings/PreferenceItem;-><init>(Ljava/lang/String;II)V

    iput-object v0, p0, Lru/cn/tv/stb/settings/SettingsViewModel;->cachingItem:Lru/cn/tv/stb/settings/PreferenceItem;

    .line 82
    new-instance v0, Lru/cn/tv/stb/settings/PreferenceItem;

    const-string v1, "quality_level"

    const v2, 0x7f0e0134

    const v3, 0x7f030004

    invoke-direct {v0, v1, v2, v3}, Lru/cn/tv/stb/settings/PreferenceItem;-><init>(Ljava/lang/String;II)V

    iput-object v0, p0, Lru/cn/tv/stb/settings/SettingsViewModel;->qualityItem:Lru/cn/tv/stb/settings/PreferenceItem;

    .line 87
    new-instance v0, Lru/cn/tv/stb/settings/PreferenceItem;

    const-string v1, "scaling_level"

    const v2, 0x7f0e0135

    const v3, 0x7f030005

    invoke-direct {v0, v1, v2, v3}, Lru/cn/tv/stb/settings/PreferenceItem;-><init>(Ljava/lang/String;II)V

    iput-object v0, p0, Lru/cn/tv/stb/settings/SettingsViewModel;->scalingItem:Lru/cn/tv/stb/settings/PreferenceItem;

    .line 92
    invoke-static {}, Lio/reactivex/subjects/PublishSubject;->create()Lio/reactivex/subjects/PublishSubject;

    move-result-object v0

    iput-object v0, p0, Lru/cn/tv/stb/settings/SettingsViewModel;->reloadSignal:Lio/reactivex/subjects/PublishSubject;

    .line 94
    invoke-direct {p0}, Lru/cn/tv/stb/settings/SettingsViewModel;->items()Lio/reactivex/Observable;

    move-result-object v0

    .line 95
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lru/cn/tv/stb/settings/SettingsViewModel;->settingsItems:Landroid/arch/lifecycle/MutableLiveData;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v1}, Lru/cn/tv/stb/settings/SettingsViewModel$$Lambda$0;->get$Lambda(Landroid/arch/lifecycle/MutableLiveData;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    new-instance v2, Lru/cn/tv/stb/settings/SettingsViewModel$$Lambda$1;

    invoke-direct {v2, p0}, Lru/cn/tv/stb/settings/SettingsViewModel$$Lambda$1;-><init>(Lru/cn/tv/stb/settings/SettingsViewModel;)V

    .line 96
    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 94
    invoke-virtual {p0, v0}, Lru/cn/tv/stb/settings/SettingsViewModel;->bind(Lio/reactivex/disposables/Disposable;)V

    .line 104
    iget-object v0, p0, Lru/cn/tv/stb/settings/SettingsViewModel;->reloadSignal:Lio/reactivex/subjects/PublishSubject;

    sget-object v1, Lru/cn/mvvm/Any;->INSTANCE:Lru/cn/mvvm/Any;

    invoke-virtual {v0, v1}, Lio/reactivex/subjects/PublishSubject;->onNext(Ljava/lang/Object;)V

    .line 105
    return-void
.end method

.method private account()Lio/reactivex/Observable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Lru/cn/api/authorization/Account;",
            ">;"
        }
    .end annotation

    .prologue
    .line 506
    new-instance v0, Lru/cn/tv/stb/settings/SettingsViewModel$$Lambda$7;

    invoke-direct {v0, p0}, Lru/cn/tv/stb/settings/SettingsViewModel$$Lambda$7;-><init>(Lru/cn/tv/stb/settings/SettingsViewModel;)V

    invoke-static {v0}, Lio/reactivex/Observable;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method private addExoPlayerItems(Landroid/database/MatrixCursor;)V
    .locals 17
    .param p1, "resultCursor"    # Landroid/database/MatrixCursor;

    .prologue
    .line 330
    move-object/from16 v0, p0

    iget-object v14, v0, Lru/cn/tv/stb/settings/SettingsViewModel;->applicationContext:Landroid/content/Context;

    invoke-static {v14}, Lru/cn/player/exoplayer/ExoPlayerUtils;->isAvailable(Landroid/content/Context;)Z

    move-result v14

    if-nez v14, :cond_1

    .line 402
    :cond_0
    :goto_0
    return-void

    .line 333
    :cond_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lru/cn/tv/stb/settings/SettingsViewModel;->applicationContext:Landroid/content/Context;

    invoke-static {v14}, Lru/cn/player/exoplayer/ExoPlayerUtils;->isUsable(Landroid/content/Context;)Z

    move-result v4

    .line 335
    .local v4, "isExoplayerUsable":Z
    move-object/from16 v0, p0

    iget-object v14, v0, Lru/cn/tv/stb/settings/SettingsViewModel;->applicationContext:Landroid/content/Context;

    const v15, 0x7f0e012f

    invoke-virtual {v14, v15}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 337
    .local v9, "playerSetting":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v14, v0, Lru/cn/tv/stb/settings/SettingsViewModel;->applicationContext:Landroid/content/Context;

    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lru/cn/tv/stb/settings/SettingsViewModel;->advancedPlaybackItem:Lru/cn/tv/stb/settings/PreferenceItem;

    iget v15, v15, Lru/cn/tv/stb/settings/PreferenceItem;->optionsArrayId:I

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v8

    .line 338
    .local v8, "playerOptions":[Ljava/lang/String;
    if-eqz v4, :cond_4

    const/4 v7, 0x1

    .line 340
    .local v7, "playerIndex":I
    :goto_1
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " - "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    aget-object v15, v8, v7

    invoke-virtual {v15}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 341
    const/4 v14, 0x6

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    const/16 v16, 0x0

    .line 342
    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x1

    const/16 v16, 0x1

    .line 343
    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x2

    sget-object v16, Lru/cn/tv/stb/settings/SettingFragment$Type;->ADVANCED_PLAYBACK:Lru/cn/tv/stb/settings/SettingFragment$Type;

    aput-object v16, v14, v15

    const/4 v15, 0x3

    aput-object v9, v14, v15

    const/4 v15, 0x4

    const/16 v16, 0x0

    aput-object v16, v14, v15

    const/4 v15, 0x5

    const/16 v16, 0x0

    aput-object v16, v14, v15

    .line 341
    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 350
    if-eqz v4, :cond_5

    const/4 v5, 0x1

    .line 352
    .local v5, "itemType":I
    :goto_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lru/cn/tv/stb/settings/SettingsViewModel;->applicationContext:Landroid/content/Context;

    const v15, 0x7f0e0130

    invoke-virtual {v14, v15}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 353
    .local v3, "cachingLevel":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v14, v0, Lru/cn/tv/stb/settings/SettingsViewModel;->applicationContext:Landroid/content/Context;

    const v15, 0x7f0e0134

    invoke-virtual {v14, v15}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 355
    .local v11, "qualityLevel":Ljava/lang/String;
    if-eqz v4, :cond_2

    .line 356
    move-object/from16 v0, p0

    iget-object v14, v0, Lru/cn/tv/stb/settings/SettingsViewModel;->applicationContext:Landroid/content/Context;

    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lru/cn/tv/stb/settings/SettingsViewModel;->cachingItem:Lru/cn/tv/stb/settings/PreferenceItem;

    iget v15, v15, Lru/cn/tv/stb/settings/PreferenceItem;->optionsArrayId:I

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    .line 357
    .local v2, "cacheOptions":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v14, v0, Lru/cn/tv/stb/settings/SettingsViewModel;->applicationContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v15, v0, Lru/cn/tv/stb/settings/SettingsViewModel;->cachingItem:Lru/cn/tv/stb/settings/PreferenceItem;

    iget-object v15, v15, Lru/cn/tv/stb/settings/PreferenceItem;->key:Ljava/lang/String;

    invoke-static {v14, v15}, Lru/cn/domain/Preferences;->getInt(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    .line 358
    .local v1, "cacheIndex":I
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " - "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    aget-object v15, v2, v1

    invoke-virtual {v15}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 360
    move-object/from16 v0, p0

    iget-object v14, v0, Lru/cn/tv/stb/settings/SettingsViewModel;->applicationContext:Landroid/content/Context;

    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lru/cn/tv/stb/settings/SettingsViewModel;->qualityItem:Lru/cn/tv/stb/settings/PreferenceItem;

    iget v15, v15, Lru/cn/tv/stb/settings/PreferenceItem;->optionsArrayId:I

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v12

    .line 361
    .local v12, "qualityOptions":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v14, v0, Lru/cn/tv/stb/settings/SettingsViewModel;->applicationContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v15, v0, Lru/cn/tv/stb/settings/SettingsViewModel;->qualityItem:Lru/cn/tv/stb/settings/PreferenceItem;

    iget-object v15, v15, Lru/cn/tv/stb/settings/PreferenceItem;->key:Ljava/lang/String;

    invoke-static {v14, v15}, Lru/cn/domain/Preferences;->getInt(Landroid/content/Context;Ljava/lang/String;)I

    move-result v10

    .line 362
    .local v10, "qualityIndex":I
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " - "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    aget-object v15, v12, v10

    invoke-virtual {v15}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    .line 365
    .end local v1    # "cacheIndex":I
    .end local v2    # "cacheOptions":[Ljava/lang/String;
    .end local v10    # "qualityIndex":I
    .end local v12    # "qualityOptions":[Ljava/lang/String;
    :cond_2
    const/4 v14, 0x6

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    const/16 v16, 0x0

    .line 366
    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x1

    .line 367
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x2

    sget-object v16, Lru/cn/tv/stb/settings/SettingFragment$Type;->CACHING_LEVEL:Lru/cn/tv/stb/settings/SettingFragment$Type;

    aput-object v16, v14, v15

    const/4 v15, 0x3

    aput-object v3, v14, v15

    const/4 v15, 0x4

    const/16 v16, 0x0

    aput-object v16, v14, v15

    const/4 v15, 0x5

    const/16 v16, 0x0

    aput-object v16, v14, v15

    .line 365
    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 374
    const/4 v14, 0x6

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    const/16 v16, 0x0

    .line 375
    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x1

    .line 376
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x2

    sget-object v16, Lru/cn/tv/stb/settings/SettingFragment$Type;->QUALITY_LEVEL:Lru/cn/tv/stb/settings/SettingFragment$Type;

    aput-object v16, v14, v15

    const/4 v15, 0x3

    aput-object v11, v14, v15

    const/4 v15, 0x4

    const/16 v16, 0x0

    aput-object v16, v14, v15

    const/4 v15, 0x5

    const/16 v16, 0x0

    aput-object v16, v14, v15

    .line 374
    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 383
    const-string v14, "multicast_block"

    invoke-static {v14}, Lru/cn/api/experiments/Experiments;->eligibleForExperiment(Ljava/lang/String;)Z

    move-result v14

    if-nez v14, :cond_0

    .line 384
    invoke-static {}, Lru/cn/utils/Utils;->maybePlayMulticast()Z

    move-result v14

    if-eqz v14, :cond_0

    .line 386
    move-object/from16 v0, p0

    iget-object v14, v0, Lru/cn/tv/stb/settings/SettingsViewModel;->applicationContext:Landroid/content/Context;

    const v15, 0x7f0e0139

    invoke-virtual {v14, v15}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 387
    .local v13, "udpFilteringOption":Ljava/lang/String;
    if-eqz v4, :cond_3

    .line 388
    move-object/from16 v0, p0

    iget-object v14, v0, Lru/cn/tv/stb/settings/SettingsViewModel;->applicationContext:Landroid/content/Context;

    const-string v15, "multicast_streams"

    invoke-static {v14, v15}, Lru/cn/domain/Preferences;->getBoolean(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v6

    .line 389
    .local v6, "multicastAllowed":Z
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " - "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    if-nez v6, :cond_6

    move-object/from16 v0, p0

    iget-object v14, v0, Lru/cn/tv/stb/settings/SettingsViewModel;->applicationContext:Landroid/content/Context;

    const v16, 0x7f0e0137

    move/from16 v0, v16

    invoke-virtual {v14, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 390
    :goto_3
    invoke-virtual {v15, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 393
    .end local v6    # "multicastAllowed":Z
    :cond_3
    const/4 v14, 0x6

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    const/16 v16, 0x0

    .line 394
    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x1

    .line 395
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    const/4 v15, 0x2

    sget-object v16, Lru/cn/tv/stb/settings/SettingFragment$Type;->MULTICAST:Lru/cn/tv/stb/settings/SettingFragment$Type;

    aput-object v16, v14, v15

    const/4 v15, 0x3

    aput-object v13, v14, v15

    const/4 v15, 0x4

    const/16 v16, 0x0

    aput-object v16, v14, v15

    const/4 v15, 0x5

    const/16 v16, 0x0

    aput-object v16, v14, v15

    .line 393
    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 338
    .end local v3    # "cachingLevel":Ljava/lang/String;
    .end local v5    # "itemType":I
    .end local v7    # "playerIndex":I
    .end local v11    # "qualityLevel":Ljava/lang/String;
    .end local v13    # "udpFilteringOption":Ljava/lang/String;
    :cond_4
    const/4 v7, 0x0

    goto/16 :goto_1

    .line 350
    .restart local v7    # "playerIndex":I
    :cond_5
    const/4 v5, 0x2

    goto/16 :goto_2

    .line 389
    .restart local v3    # "cachingLevel":Ljava/lang/String;
    .restart local v5    # "itemType":I
    .restart local v6    # "multicastAllowed":Z
    .restart local v11    # "qualityLevel":Ljava/lang/String;
    .restart local v13    # "udpFilteringOption":Ljava/lang/String;
    :cond_6
    move-object/from16 v0, p0

    iget-object v14, v0, Lru/cn/tv/stb/settings/SettingsViewModel;->applicationContext:Landroid/content/Context;

    const v16, 0x7f0e0136

    .line 390
    move/from16 v0, v16

    invoke-virtual {v14, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v14

    goto :goto_3
.end method

.method private addGeneralItems(Landroid/database/MatrixCursor;)V
    .locals 8
    .param p1, "resultCursor"    # Landroid/database/MatrixCursor;

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 261
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    .line 262
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    .line 263
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v5

    aput-object v4, v0, v6

    iget-object v1, p0, Lru/cn/tv/stb/settings/SettingsViewModel;->applicationContext:Landroid/content/Context;

    const v2, 0x7f0e0131

    .line 265
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v7

    const/4 v1, 0x4

    aput-object v4, v0, v1

    const/4 v1, 0x5

    aput-object v4, v0, v1

    .line 261
    invoke-virtual {p1, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 270
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    .line 271
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    .line 272
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v5

    sget-object v1, Lru/cn/tv/stb/settings/SettingFragment$Type;->UDP_PROXY:Lru/cn/tv/stb/settings/SettingFragment$Type;

    aput-object v1, v0, v6

    iget-object v1, p0, Lru/cn/tv/stb/settings/SettingsViewModel;->applicationContext:Landroid/content/Context;

    const v2, 0x7f0e0167

    .line 274
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v7

    const/4 v1, 0x4

    aput-object v4, v0, v1

    const/4 v1, 0x5

    aput-object v4, v0, v1

    .line 270
    invoke-virtual {p1, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 279
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    .line 280
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    .line 281
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v5

    sget-object v1, Lru/cn/tv/stb/settings/SettingFragment$Type;->TECHNICAL_INFO:Lru/cn/tv/stb/settings/SettingFragment$Type;

    aput-object v1, v0, v6

    iget-object v1, p0, Lru/cn/tv/stb/settings/SettingsViewModel;->applicationContext:Landroid/content/Context;

    const v2, 0x7f0e0138

    .line 283
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v7

    const/4 v1, 0x4

    aput-object v4, v0, v1

    const/4 v1, 0x5

    aput-object v4, v0, v1

    .line 279
    invoke-virtual {p1, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 287
    return-void
.end method

.method private addPornoItems(ZLandroid/database/MatrixCursor;)V
    .locals 9
    .param p1, "pinCodeExists"    # Z
    .param p2, "resultCursor"    # Landroid/database/MatrixCursor;

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 444
    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Object;

    .line 445
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    .line 446
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    aput-object v5, v1, v7

    iget-object v2, p0, Lru/cn/tv/stb/settings/SettingsViewModel;->applicationContext:Landroid/content/Context;

    const v3, 0x7f0e00ed

    .line 448
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v8

    const/4 v2, 0x4

    aput-object v5, v1, v2

    const/4 v2, 0x5

    aput-object v5, v1, v2

    .line 444
    invoke-virtual {p2, v1}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 453
    const v0, 0x7f0e013c

    .line 454
    .local v0, "titleId":I
    iget-object v1, p0, Lru/cn/tv/stb/settings/SettingsViewModel;->applicationContext:Landroid/content/Context;

    const-string v2, "porno_disabled"

    invoke-static {v1, v2}, Lru/cn/domain/Preferences;->getBoolean(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 455
    const v0, 0x7f0e013a

    .line 457
    :cond_0
    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Object;

    .line 458
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    .line 459
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    sget-object v2, Lru/cn/tv/stb/settings/SettingFragment$Type;->DISABLE_PORNO:Lru/cn/tv/stb/settings/SettingFragment$Type;

    aput-object v2, v1, v7

    iget-object v2, p0, Lru/cn/tv/stb/settings/SettingsViewModel;->applicationContext:Landroid/content/Context;

    .line 461
    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v8

    const/4 v2, 0x4

    aput-object v5, v1, v2

    const/4 v2, 0x5

    aput-object v5, v1, v2

    .line 457
    invoke-virtual {p2, v1}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 466
    const v0, 0x7f0e013d

    .line 467
    iget-object v1, p0, Lru/cn/tv/stb/settings/SettingsViewModel;->applicationContext:Landroid/content/Context;

    const-string v2, "pin_disabled"

    invoke-static {v1, v2}, Lru/cn/domain/Preferences;->getBoolean(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 468
    const v0, 0x7f0e013b

    .line 470
    :cond_1
    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Object;

    .line 471
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    .line 472
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    sget-object v2, Lru/cn/tv/stb/settings/SettingFragment$Type;->DISABLE_PIN:Lru/cn/tv/stb/settings/SettingFragment$Type;

    aput-object v2, v1, v7

    iget-object v2, p0, Lru/cn/tv/stb/settings/SettingsViewModel;->applicationContext:Landroid/content/Context;

    .line 474
    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v8

    const/4 v2, 0x4

    aput-object v5, v1, v2

    const/4 v2, 0x5

    aput-object v5, v1, v2

    .line 470
    invoke-virtual {p2, v1}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 479
    const v0, 0x7f0e0100

    .line 480
    if-eqz p1, :cond_2

    .line 481
    const v0, 0x7f0e0106

    .line 483
    :cond_2
    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Object;

    .line 484
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    .line 485
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    sget-object v2, Lru/cn/tv/stb/settings/SettingFragment$Type;->RENEW_PIN:Lru/cn/tv/stb/settings/SettingFragment$Type;

    aput-object v2, v1, v7

    iget-object v2, p0, Lru/cn/tv/stb/settings/SettingsViewModel;->applicationContext:Landroid/content/Context;

    .line 487
    invoke-virtual {v2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v8

    const/4 v2, 0x4

    aput-object v5, v1, v2

    const/4 v2, 0x5

    aput-object v5, v1, v2

    .line 483
    invoke-virtual {p2, v1}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 492
    if-eqz p1, :cond_3

    .line 493
    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Object;

    .line 494
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    .line 495
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    sget-object v2, Lru/cn/tv/stb/settings/SettingFragment$Type;->CHANGE_PIN:Lru/cn/tv/stb/settings/SettingFragment$Type;

    aput-object v2, v1, v7

    iget-object v2, p0, Lru/cn/tv/stb/settings/SettingsViewModel;->applicationContext:Landroid/content/Context;

    const v3, 0x7f0e00fb

    .line 497
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v8

    const/4 v2, 0x4

    aput-object v5, v1, v2

    const/4 v2, 0x5

    aput-object v5, v1, v2

    .line 493
    invoke-virtual {p2, v1}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 502
    :cond_3
    return-void
.end method

.method private addSynchronizationItems(Lru/cn/api/authorization/Account;Landroid/database/MatrixCursor;)V
    .locals 8
    .param p1, "account"    # Lru/cn/api/authorization/Account;
    .param p2, "resultCursor"    # Landroid/database/MatrixCursor;

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 290
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    .line 291
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    .line 292
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v5

    aput-object v4, v0, v6

    iget-object v1, p0, Lru/cn/tv/stb/settings/SettingsViewModel;->applicationContext:Landroid/content/Context;

    const v2, 0x7f0e0153

    .line 294
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v7

    const/4 v1, 0x4

    aput-object v4, v0, v1

    const/4 v1, 0x5

    aput-object v4, v0, v1

    .line 290
    invoke-virtual {p2, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 299
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lru/cn/api/authorization/Account;->getLogin()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 300
    :cond_0
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    .line 301
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    .line 302
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v5

    sget-object v1, Lru/cn/tv/stb/settings/SettingFragment$Type;->LOGIN:Lru/cn/tv/stb/settings/SettingFragment$Type;

    aput-object v1, v0, v6

    iget-object v1, p0, Lru/cn/tv/stb/settings/SettingsViewModel;->applicationContext:Landroid/content/Context;

    const v2, 0x7f0e0154

    .line 304
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v7

    const/4 v1, 0x4

    aput-object v4, v0, v1

    const/4 v1, 0x5

    aput-object v4, v0, v1

    .line 300
    invoke-virtual {p2, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 327
    :goto_0
    return-void

    .line 309
    :cond_1
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    .line 310
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    .line 311
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v5

    sget-object v1, Lru/cn/tv/stb/settings/SettingFragment$Type;->ACCOUNT:Lru/cn/tv/stb/settings/SettingFragment$Type;

    aput-object v1, v0, v6

    .line 313
    invoke-virtual {p1}, Lru/cn/api/authorization/Account;->getLogin()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v7

    const/4 v1, 0x4

    aput-object v4, v0, v1

    const/4 v1, 0x5

    aput-object v4, v0, v1

    .line 309
    invoke-virtual {p2, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 318
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    .line 319
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    .line 320
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v5

    sget-object v1, Lru/cn/tv/stb/settings/SettingFragment$Type;->LOGOUT:Lru/cn/tv/stb/settings/SettingFragment$Type;

    aput-object v1, v0, v6

    iget-object v1, p0, Lru/cn/tv/stb/settings/SettingsViewModel;->applicationContext:Landroid/content/Context;

    const v2, 0x7f0e0150

    .line 322
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v7

    const/4 v1, 0x4

    aput-object v4, v0, v1

    const/4 v1, 0x5

    aput-object v4, v0, v1

    .line 318
    invoke-virtual {p2, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private addUserPlaylists(Landroid/database/Cursor;Landroid/database/MatrixCursor;)V
    .locals 11
    .param p1, "userPlaylistsCursor"    # Landroid/database/Cursor;
    .param p2, "resultCursor"    # Landroid/database/MatrixCursor;

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v6, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 405
    invoke-static {}, Lru/cn/utils/customization/Config;->restrictions()Lru/cn/utils/customization/Restrictable;

    move-result-object v3

    const-string v4, "playlists"

    invoke-interface {v3, v4}, Lru/cn/utils/customization/Restrictable;->allows(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 406
    const/4 v3, 0x6

    new-array v3, v3, [Ljava/lang/Object;

    .line 407
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    .line 408
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v8

    aput-object v6, v3, v9

    iget-object v4, p0, Lru/cn/tv/stb/settings/SettingsViewModel;->applicationContext:Landroid/content/Context;

    const v5, 0x7f0e0186

    .line 410
    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v10

    const/4 v4, 0x4

    aput-object v6, v3, v4

    const/4 v4, 0x5

    aput-object v6, v3, v4

    .line 406
    invoke-virtual {p2, v3}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 415
    const/4 v3, 0x6

    new-array v3, v3, [Ljava/lang/Object;

    .line 416
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    .line 417
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v8

    sget-object v4, Lru/cn/tv/stb/settings/SettingFragment$Type;->ADD_PLAYLIST:Lru/cn/tv/stb/settings/SettingFragment$Type;

    aput-object v4, v3, v9

    iget-object v4, p0, Lru/cn/tv/stb/settings/SettingsViewModel;->applicationContext:Landroid/content/Context;

    const v5, 0x7f0e0178

    .line 419
    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v10

    const/4 v4, 0x4

    aput-object v6, v3, v4

    const/4 v4, 0x5

    aput-object v6, v3, v4

    .line 415
    invoke-virtual {p2, v3}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 424
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 425
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v3

    if-nez v3, :cond_0

    .line 426
    const-string v3, "title"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 427
    .local v2, "title":Ljava/lang/String;
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    const-string v4, "%d. %s"

    new-array v5, v9, [Ljava/lang/Object;

    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    aput-object v2, v5, v8

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 428
    .local v1, "playlistTitle":Ljava/lang/String;
    const-string v3, "location"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 429
    .local v0, "playlistLocation":Ljava/lang/String;
    const/4 v3, 0x6

    new-array v3, v3, [Ljava/lang/Object;

    .line 430
    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    .line 431
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v8

    sget-object v4, Lru/cn/tv/stb/settings/SettingFragment$Type;->PLAYLIST:Lru/cn/tv/stb/settings/SettingFragment$Type;

    aput-object v4, v3, v9

    aput-object v1, v3, v10

    const/4 v4, 0x4

    aput-object v2, v3, v4

    const/4 v4, 0x5

    aput-object v0, v3, v4

    .line 429
    invoke-virtual {p2, v3}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 438
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_0

    .line 441
    .end local v0    # "playlistLocation":Ljava/lang/String;
    .end local v1    # "playlistTitle":Ljava/lang/String;
    .end local v2    # "title":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private items()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 204
    iget-object v0, p0, Lru/cn/tv/stb/settings/SettingsViewModel;->reloadSignal:Lio/reactivex/subjects/PublishSubject;

    new-instance v1, Lru/cn/tv/stb/settings/SettingsViewModel$$Lambda$6;

    invoke-direct {v1, p0}, Lru/cn/tv/stb/settings/SettingsViewModel$$Lambda$6;-><init>(Lru/cn/tv/stb/settings/SettingsViewModel;)V

    invoke-virtual {v0, v1}, Lio/reactivex/subjects/PublishSubject;->switchMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method static final synthetic lambda$null$5$SettingsViewModel(Ljava/lang/Throwable;)Lcom/annimon/stream/Optional;
    .locals 1
    .param p0, "error"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 212
    invoke-static {p0}, Lru/cn/utils/Logger;->logException(Ljava/lang/Throwable;)V

    .line 213
    invoke-static {}, Lcom/annimon/stream/Optional;->empty()Lcom/annimon/stream/Optional;

    move-result-object v0

    return-object v0
.end method

.method static final synthetic lambda$pinCodeContains$9$SettingsViewModel(Landroid/database/Cursor;)Ljava/lang/Boolean;
    .locals 1
    .param p0, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 533
    invoke-interface {p0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private pinCodeContains()Lio/reactivex/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 529
    invoke-static {}, Lru/cn/api/provider/TvContentProviderContract;->userPornoPinCode()Landroid/net/Uri;

    move-result-object v0

    .line 531
    .local v0, "uri":Landroid/net/Uri;
    iget-object v1, p0, Lru/cn/tv/stb/settings/SettingsViewModel;->loader:Lru/cn/mvvm/RxLoader;

    invoke-virtual {v1, v0}, Lru/cn/mvvm/RxLoader;->query(Landroid/net/Uri;)Lio/reactivex/Observable;

    move-result-object v1

    .line 532
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lru/cn/tv/stb/settings/SettingsViewModel$$Lambda$8;->$instance:Lio/reactivex/functions/Function;

    .line 533
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v1

    .line 531
    return-object v1
.end method

.method private pornoChannels()Lio/reactivex/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 521
    const-string v1, "porno"

    .line 522
    .local v1, "selection":Ljava/lang/String;
    invoke-static {}, Lru/cn/api/provider/TvContentProviderContract;->channels()Landroid/net/Uri;

    move-result-object v0

    .line 524
    .local v0, "channelsUri":Landroid/net/Uri;
    iget-object v2, p0, Lru/cn/tv/stb/settings/SettingsViewModel;->loader:Lru/cn/mvvm/RxLoader;

    invoke-virtual {v2, v0, v1}, Lru/cn/mvvm/RxLoader;->query(Landroid/net/Uri;Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object v2

    .line 525
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v2

    .line 524
    return-object v2
.end method

.method private userPlaylists()Lio/reactivex/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 513
    const-string v1, "contractor_id=0"

    .line 514
    .local v1, "selection":Ljava/lang/String;
    invoke-static {}, Lru/cn/api/provider/TvContentProviderContract;->playlistUri()Landroid/net/Uri;

    move-result-object v0

    .line 516
    .local v0, "playlistUri":Landroid/net/Uri;
    iget-object v2, p0, Lru/cn/tv/stb/settings/SettingsViewModel;->loader:Lru/cn/mvvm/RxLoader;

    invoke-virtual {v2, v0, v1}, Lru/cn/mvvm/RxLoader;->query(Landroid/net/Uri;Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object v2

    .line 517
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v2

    .line 516
    return-object v2
.end method


# virtual methods
.method itemClicked(Lru/cn/tv/stb/settings/SettingFragment$Type;I)V
    .locals 9
    .param p1, "type"    # Lru/cn/tv/stb/settings/SettingFragment$Type;
    .param p2, "position"    # I

    .prologue
    .line 120
    sget-object v5, Lru/cn/tv/stb/settings/SettingsViewModel$1;->$SwitchMap$ru$cn$tv$stb$settings$SettingFragment$Type:[I

    invoke-virtual {p1}, Lru/cn/tv/stb/settings/SettingFragment$Type;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 179
    :cond_0
    :goto_0
    return-void

    .line 122
    :pswitch_0
    iget-object v5, p0, Lru/cn/tv/stb/settings/SettingsViewModel;->preferenceListDialog:Landroid/arch/lifecycle/MutableLiveData;

    new-instance v6, Landroid/util/Pair;

    iget-object v7, p0, Lru/cn/tv/stb/settings/SettingsViewModel;->scalingItem:Lru/cn/tv/stb/settings/PreferenceItem;

    new-instance v8, Lru/cn/tv/stb/settings/SettingsViewModel$$Lambda$2;

    invoke-direct {v8, p0}, Lru/cn/tv/stb/settings/SettingsViewModel$$Lambda$2;-><init>(Lru/cn/tv/stb/settings/SettingsViewModel;)V

    invoke-direct {v6, v7, v8}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v5, v6}, Landroid/arch/lifecycle/MutableLiveData;->setValue(Ljava/lang/Object;)V

    goto :goto_0

    .line 131
    :pswitch_1
    iget-object v5, p0, Lru/cn/tv/stb/settings/SettingsViewModel;->preferenceListDialog:Landroid/arch/lifecycle/MutableLiveData;

    new-instance v6, Landroid/util/Pair;

    iget-object v7, p0, Lru/cn/tv/stb/settings/SettingsViewModel;->advancedPlaybackItem:Lru/cn/tv/stb/settings/PreferenceItem;

    new-instance v8, Lru/cn/tv/stb/settings/SettingsViewModel$$Lambda$3;

    invoke-direct {v8, p0}, Lru/cn/tv/stb/settings/SettingsViewModel$$Lambda$3;-><init>(Lru/cn/tv/stb/settings/SettingsViewModel;)V

    invoke-direct {v6, v7, v8}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v5, v6}, Landroid/arch/lifecycle/MutableLiveData;->setValue(Ljava/lang/Object;)V

    goto :goto_0

    .line 147
    :pswitch_2
    iget-object v5, p0, Lru/cn/tv/stb/settings/SettingsViewModel;->preferenceListDialog:Landroid/arch/lifecycle/MutableLiveData;

    new-instance v6, Landroid/util/Pair;

    iget-object v7, p0, Lru/cn/tv/stb/settings/SettingsViewModel;->cachingItem:Lru/cn/tv/stb/settings/PreferenceItem;

    new-instance v8, Lru/cn/tv/stb/settings/SettingsViewModel$$Lambda$4;

    invoke-direct {v8, p0}, Lru/cn/tv/stb/settings/SettingsViewModel$$Lambda$4;-><init>(Lru/cn/tv/stb/settings/SettingsViewModel;)V

    invoke-direct {v6, v7, v8}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v5, v6}, Landroid/arch/lifecycle/MutableLiveData;->setValue(Ljava/lang/Object;)V

    goto :goto_0

    .line 158
    :pswitch_3
    iget-object v5, p0, Lru/cn/tv/stb/settings/SettingsViewModel;->preferenceListDialog:Landroid/arch/lifecycle/MutableLiveData;

    new-instance v6, Landroid/util/Pair;

    iget-object v7, p0, Lru/cn/tv/stb/settings/SettingsViewModel;->qualityItem:Lru/cn/tv/stb/settings/PreferenceItem;

    new-instance v8, Lru/cn/tv/stb/settings/SettingsViewModel$$Lambda$5;

    invoke-direct {v8, p0}, Lru/cn/tv/stb/settings/SettingsViewModel$$Lambda$5;-><init>(Lru/cn/tv/stb/settings/SettingsViewModel;)V

    invoke-direct {v6, v7, v8}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v5, v6}, Landroid/arch/lifecycle/MutableLiveData;->setValue(Ljava/lang/Object;)V

    goto :goto_0

    .line 168
    :pswitch_4
    iget-object v5, p0, Lru/cn/tv/stb/settings/SettingsViewModel;->settingsItems:Landroid/arch/lifecycle/MutableLiveData;

    invoke-virtual {v5}, Landroid/arch/lifecycle/MutableLiveData;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/database/Cursor;

    .line 169
    .local v2, "settingsCursor":Landroid/database/Cursor;
    if-eqz v2, :cond_0

    invoke-interface {v2, p2}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 170
    const-string v5, "_id"

    invoke-interface {v2, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v2, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 171
    .local v1, "playlistId":I
    const-string v5, "playlist_name"

    invoke-interface {v2, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 172
    .local v3, "title":Ljava/lang/String;
    const-string v5, "location"

    invoke-interface {v2, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 174
    .local v0, "location":Ljava/lang/String;
    new-instance v4, Lru/cn/tv/stb/settings/UserPlaylistItem;

    invoke-direct {v4, v1, v3, v0}, Lru/cn/tv/stb/settings/UserPlaylistItem;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    .line 175
    .local v4, "userPlaylistItem":Lru/cn/tv/stb/settings/UserPlaylistItem;
    iget-object v5, p0, Lru/cn/tv/stb/settings/SettingsViewModel;->userPlaylistDialog:Landroid/arch/lifecycle/MutableLiveData;

    invoke-virtual {v5, v4}, Landroid/arch/lifecycle/MutableLiveData;->setValue(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 120
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method final synthetic lambda$account$8$SettingsViewModel()Lru/cn/api/authorization/Account;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const-wide/16 v2, 0x2

    .line 507
    iget-object v1, p0, Lru/cn/tv/stb/settings/SettingsViewModel;->applicationContext:Landroid/content/Context;

    invoke-static {v1, v2, v3}, Lru/cn/api/ServiceLocator;->auth(Landroid/content/Context;J)Lru/cn/api/authorization/retrofit/AuthApi;

    move-result-object v0

    .line 508
    .local v0, "auth":Lru/cn/api/authorization/retrofit/AuthApi;
    iget-object v1, p0, Lru/cn/tv/stb/settings/SettingsViewModel;->applicationContext:Landroid/content/Context;

    invoke-static {v1, v0, v2, v3}, Lru/cn/api/authorization/Authorization;->getAccount(Landroid/content/Context;Lru/cn/api/authorization/retrofit/AuthApi;J)Lru/cn/api/authorization/Account;

    move-result-object v1

    return-object v1
.end method

.method final synthetic lambda$itemClicked$1$SettingsViewModel(ILjava/lang/String;)V
    .locals 1
    .param p1, "index"    # I
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 125
    iget-object v0, p0, Lru/cn/tv/stb/settings/SettingsViewModel;->applicationContext:Landroid/content/Context;

    invoke-static {v0, p2, p1}, Lru/cn/domain/Preferences;->setInt(Landroid/content/Context;Ljava/lang/String;I)V

    .line 126
    invoke-virtual {p0}, Lru/cn/tv/stb/settings/SettingsViewModel;->reload()V

    .line 127
    return-void
.end method

.method final synthetic lambda$itemClicked$2$SettingsViewModel(ILjava/lang/String;)V
    .locals 5
    .param p1, "index"    # I
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x1

    .line 134
    if-ne p1, v0, :cond_0

    .line 135
    .local v0, "state":Z
    :goto_0
    iget-object v2, p0, Lru/cn/tv/stb/settings/SettingsViewModel;->applicationContext:Landroid/content/Context;

    iget-object v3, p0, Lru/cn/tv/stb/settings/SettingsViewModel;->advancedPlaybackItem:Lru/cn/tv/stb/settings/PreferenceItem;

    iget-object v3, v3, Lru/cn/tv/stb/settings/PreferenceItem;->key:Ljava/lang/String;

    invoke-static {v2, v3, v0}, Lru/cn/domain/Preferences;->setBoolean(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 139
    invoke-static {}, Lru/cn/api/provider/TvContentProviderContract;->channels()Landroid/net/Uri;

    move-result-object v1

    .line 140
    .local v1, "uri":Landroid/net/Uri;
    iget-object v2, p0, Lru/cn/tv/stb/settings/SettingsViewModel;->loader:Lru/cn/mvvm/RxLoader;

    invoke-virtual {v2, v1, v4, v4}, Lru/cn/mvvm/RxLoader;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;)Lio/reactivex/Single;

    .line 142
    invoke-virtual {p0}, Lru/cn/tv/stb/settings/SettingsViewModel;->reload()V

    .line 143
    return-void

    .line 134
    .end local v0    # "state":Z
    .end local v1    # "uri":Landroid/net/Uri;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final synthetic lambda$itemClicked$3$SettingsViewModel(ILjava/lang/String;)V
    .locals 1
    .param p1, "index"    # I
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 150
    iget-object v0, p0, Lru/cn/tv/stb/settings/SettingsViewModel;->applicationContext:Landroid/content/Context;

    invoke-static {v0, p2, p1}, Lru/cn/domain/Preferences;->setInt(Landroid/content/Context;Ljava/lang/String;I)V

    .line 151
    invoke-virtual {p0}, Lru/cn/tv/stb/settings/SettingsViewModel;->reload()V

    .line 152
    return-void
.end method

.method final synthetic lambda$itemClicked$4$SettingsViewModel(ILjava/lang/String;)V
    .locals 1
    .param p1, "index"    # I
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 161
    iget-object v0, p0, Lru/cn/tv/stb/settings/SettingsViewModel;->applicationContext:Landroid/content/Context;

    invoke-static {v0, p2, p1}, Lru/cn/domain/Preferences;->setInt(Landroid/content/Context;Ljava/lang/String;I)V

    .line 162
    invoke-virtual {p0}, Lru/cn/tv/stb/settings/SettingsViewModel;->reload()V

    .line 163
    return-void
.end method

.method final synthetic lambda$items$7$SettingsViewModel(Lru/cn/mvvm/Any;)Lio/reactivex/ObservableSource;
    .locals 5
    .param p1, "it"    # Lru/cn/mvvm/Any;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 204
    .line 206
    invoke-direct {p0}, Lru/cn/tv/stb/settings/SettingsViewModel;->userPlaylists()Lio/reactivex/Observable;

    move-result-object v0

    .line 207
    invoke-direct {p0}, Lru/cn/tv/stb/settings/SettingsViewModel;->pornoChannels()Lio/reactivex/Observable;

    move-result-object v1

    .line 208
    invoke-direct {p0}, Lru/cn/tv/stb/settings/SettingsViewModel;->pinCodeContains()Lio/reactivex/Observable;

    move-result-object v2

    .line 209
    invoke-direct {p0}, Lru/cn/tv/stb/settings/SettingsViewModel;->account()Lio/reactivex/Observable;

    move-result-object v3

    sget-object v4, Lru/cn/tv/stb/settings/SettingsViewModel$$Lambda$9;->$instance:Lio/reactivex/functions/Function;

    .line 210
    invoke-virtual {v3, v4}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v3

    sget-object v4, Lru/cn/tv/stb/settings/SettingsViewModel$$Lambda$10;->$instance:Lio/reactivex/functions/Function;

    .line 211
    invoke-virtual {v3, v4}, Lio/reactivex/Observable;->onErrorReturn(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v3

    new-instance v4, Lru/cn/tv/stb/settings/SettingsViewModel$$Lambda$11;

    invoke-direct {v4, p0}, Lru/cn/tv/stb/settings/SettingsViewModel$$Lambda$11;-><init>(Lru/cn/tv/stb/settings/SettingsViewModel;)V

    .line 205
    invoke-static {v0, v1, v2, v3, v4}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/Function4;)Lio/reactivex/Observable;

    move-result-object v0

    .line 204
    return-object v0
.end method

.method final synthetic lambda$new$0$SettingsViewModel(Ljava/lang/Throwable;)V
    .locals 2
    .param p1, "throwable"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 99
    iget-object v0, p0, Lru/cn/tv/stb/settings/SettingsViewModel;->LOG_TAG:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 100
    iget-object v0, p0, Lru/cn/tv/stb/settings/SettingsViewModel;->settingsItems:Landroid/arch/lifecycle/MutableLiveData;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/arch/lifecycle/MutableLiveData;->setValue(Ljava/lang/Object;)V

    .line 101
    return-void
.end method

.method final synthetic lambda$null$6$SettingsViewModel(Landroid/database/Cursor;Landroid/database/Cursor;Ljava/lang/Boolean;Lcom/annimon/stream/Optional;)Landroid/database/MatrixCursor;
    .locals 8
    .param p1, "userPlaylistsCursor"    # Landroid/database/Cursor;
    .param p2, "pornoChannelsCursor"    # Landroid/database/Cursor;
    .param p3, "pinCodeExists"    # Ljava/lang/Boolean;
    .param p4, "account"    # Lcom/annimon/stream/Optional;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 216
    new-instance v0, Landroid/database/MatrixCursor;

    const/4 v4, 0x6

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "_id"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "item_type"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "type"

    aput-object v6, v4, v5

    const/4 v5, 0x3

    const-string v6, "title"

    aput-object v6, v4, v5

    const/4 v5, 0x4

    const-string v6, "playlist_name"

    aput-object v6, v4, v5

    const/4 v5, 0x5

    const-string v6, "location"

    aput-object v6, v4, v5

    invoke-direct {v0, v4}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 218
    .local v0, "resultCursor":Landroid/database/MatrixCursor;
    invoke-direct {p0, v0}, Lru/cn/tv/stb/settings/SettingsViewModel;->addGeneralItems(Landroid/database/MatrixCursor;)V

    .line 221
    invoke-virtual {p4}, Lcom/annimon/stream/Optional;->isPresent()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 222
    invoke-virtual {p4}, Lcom/annimon/stream/Optional;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/cn/api/authorization/Account;

    invoke-direct {p0, v4, v0}, Lru/cn/tv/stb/settings/SettingsViewModel;->addSynchronizationItems(Lru/cn/api/authorization/Account;Landroid/database/MatrixCursor;)V

    .line 225
    :cond_0
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-lez v4, :cond_1

    .line 226
    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-direct {p0, v4, v0}, Lru/cn/tv/stb/settings/SettingsViewModel;->addPornoItems(ZLandroid/database/MatrixCursor;)V

    .line 229
    :cond_1
    invoke-direct {p0, p1, v0}, Lru/cn/tv/stb/settings/SettingsViewModel;->addUserPlaylists(Landroid/database/Cursor;Landroid/database/MatrixCursor;)V

    .line 231
    const/4 v4, 0x6

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 232
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 233
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const/4 v6, 0x0

    aput-object v6, v4, v5

    const/4 v5, 0x3

    iget-object v6, p0, Lru/cn/tv/stb/settings/SettingsViewModel;->applicationContext:Landroid/content/Context;

    const v7, 0x7f0e0132

    .line 235
    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x4

    const/4 v6, 0x0

    aput-object v6, v4, v5

    const/4 v5, 0x5

    const/4 v6, 0x0

    aput-object v6, v4, v5

    .line 231
    invoke-virtual {v0, v4}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 240
    iget-object v4, p0, Lru/cn/tv/stb/settings/SettingsViewModel;->applicationContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    iget-object v5, p0, Lru/cn/tv/stb/settings/SettingsViewModel;->scalingItem:Lru/cn/tv/stb/settings/PreferenceItem;

    iget v5, v5, Lru/cn/tv/stb/settings/PreferenceItem;->optionsArrayId:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    .line 241
    .local v3, "scalingOptions":[Ljava/lang/String;
    iget-object v4, p0, Lru/cn/tv/stb/settings/SettingsViewModel;->applicationContext:Landroid/content/Context;

    iget-object v5, p0, Lru/cn/tv/stb/settings/SettingsViewModel;->scalingItem:Lru/cn/tv/stb/settings/PreferenceItem;

    iget-object v5, v5, Lru/cn/tv/stb/settings/PreferenceItem;->key:Ljava/lang/String;

    invoke-static {v4, v5}, Lru/cn/domain/Preferences;->getInt(Landroid/content/Context;Ljava/lang/String;)I

    move-result v1

    .line 242
    .local v1, "scalingIndex":I
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lru/cn/tv/stb/settings/SettingsViewModel;->applicationContext:Landroid/content/Context;

    const v6, 0x7f0e0135

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-object v5, v3, v1

    .line 243
    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 245
    .local v2, "scalingLevel":Ljava/lang/String;
    const/4 v4, 0x6

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 246
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const/4 v6, 0x1

    .line 247
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    sget-object v6, Lru/cn/tv/stb/settings/SettingFragment$Type;->SCALING_LEVEL:Lru/cn/tv/stb/settings/SettingFragment$Type;

    aput-object v6, v4, v5

    const/4 v5, 0x3

    aput-object v2, v4, v5

    const/4 v5, 0x4

    const/4 v6, 0x0

    aput-object v6, v4, v5

    const/4 v5, 0x5

    const/4 v6, 0x0

    aput-object v6, v4, v5

    .line 245
    invoke-virtual {v0, v4}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 254
    invoke-direct {p0, v0}, Lru/cn/tv/stb/settings/SettingsViewModel;->addExoPlayerItems(Landroid/database/MatrixCursor;)V

    .line 256
    return-object v0
.end method

.method logout()V
    .locals 4

    .prologue
    .line 182
    iget-object v0, p0, Lru/cn/tv/stb/settings/SettingsViewModel;->applicationContext:Landroid/content/Context;

    const-wide/16 v2, 0x2

    invoke-static {v0, v2, v3}, Lru/cn/api/authorization/Authorization;->logout(Landroid/content/Context;J)V

    .line 183
    invoke-virtual {p0}, Lru/cn/tv/stb/settings/SettingsViewModel;->reload()V

    .line 184
    return-void
.end method

.method preferenceListDialog()Landroid/arch/lifecycle/LiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/arch/lifecycle/LiveData",
            "<",
            "Landroid/util/Pair",
            "<",
            "Lru/cn/tv/stb/settings/PreferenceItem;",
            "Lru/cn/tv/PreferenceListDialog$PreferenceListListener;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 112
    iget-object v0, p0, Lru/cn/tv/stb/settings/SettingsViewModel;->preferenceListDialog:Landroid/arch/lifecycle/MutableLiveData;

    return-object v0
.end method

.method reload()V
    .locals 2

    .prologue
    .line 200
    iget-object v0, p0, Lru/cn/tv/stb/settings/SettingsViewModel;->reloadSignal:Lio/reactivex/subjects/PublishSubject;

    sget-object v1, Lru/cn/mvvm/Any;->INSTANCE:Lru/cn/mvvm/Any;

    invoke-virtual {v0, v1}, Lio/reactivex/subjects/PublishSubject;->onNext(Ljava/lang/Object;)V

    .line 201
    return-void
.end method

.method settingsItems()Landroid/arch/lifecycle/LiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/arch/lifecycle/LiveData",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 108
    iget-object v0, p0, Lru/cn/tv/stb/settings/SettingsViewModel;->settingsItems:Landroid/arch/lifecycle/MutableLiveData;

    return-object v0
.end method

.method toggleMulticastPreference()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 187
    const-string v0, "multicast_streams"

    .line 188
    .local v0, "key":Ljava/lang/String;
    iget-object v3, p0, Lru/cn/tv/stb/settings/SettingsViewModel;->applicationContext:Landroid/content/Context;

    invoke-static {v3, v0}, Lru/cn/domain/Preferences;->getBoolean(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    .line 189
    .local v1, "state":Z
    iget-object v4, p0, Lru/cn/tv/stb/settings/SettingsViewModel;->applicationContext:Landroid/content/Context;

    if-nez v1, :cond_0

    const/4 v3, 0x1

    :goto_0
    invoke-static {v4, v0, v3}, Lru/cn/domain/Preferences;->setBoolean(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 192
    invoke-static {}, Lru/cn/api/provider/TvContentProviderContract;->channels()Landroid/net/Uri;

    move-result-object v2

    .line 193
    .local v2, "uri":Landroid/net/Uri;
    iget-object v3, p0, Lru/cn/tv/stb/settings/SettingsViewModel;->loader:Lru/cn/mvvm/RxLoader;

    invoke-virtual {v3, v2, v5, v5}, Lru/cn/mvvm/RxLoader;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;)Lio/reactivex/Single;

    .line 195
    invoke-virtual {p0}, Lru/cn/tv/stb/settings/SettingsViewModel;->reload()V

    .line 196
    return-void

    .line 189
    .end local v2    # "uri":Landroid/net/Uri;
    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method userPlaylistDialog()Landroid/arch/lifecycle/LiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/arch/lifecycle/LiveData",
            "<",
            "Lru/cn/tv/stb/settings/UserPlaylistItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 116
    iget-object v0, p0, Lru/cn/tv/stb/settings/SettingsViewModel;->userPlaylistDialog:Landroid/arch/lifecycle/MutableLiveData;

    return-object v0
.end method
