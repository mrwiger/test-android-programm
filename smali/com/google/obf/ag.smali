.class public final Lcom/google/obf/ag;
.super Ljava/lang/Object;
.source "IMASDK"

# interfaces
.implements Lcom/google/obf/ak;


# static fields
.field private static final a:[B


# instance fields
.field private final b:Lcom/google/obf/da;

.field private final c:J

.field private d:J

.field private e:[B

.field private f:I

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 84
    const/16 v0, 0x1000

    new-array v0, v0, [B

    sput-object v0, Lcom/google/obf/ag;->a:[B

    return-void
.end method

.method public constructor <init>(Lcom/google/obf/da;JJ)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/google/obf/ag;->b:Lcom/google/obf/da;

    .line 3
    iput-wide p2, p0, Lcom/google/obf/ag;->d:J

    .line 4
    iput-wide p4, p0, Lcom/google/obf/ag;->c:J

    .line 5
    const/16 v0, 0x2000

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/google/obf/ag;->e:[B

    .line 6
    return-void
.end method

.method private a([BIIIZ)I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, -0x1

    .line 73
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 74
    new-instance v0, Ljava/lang/InterruptedException;

    invoke-direct {v0}, Ljava/lang/InterruptedException;-><init>()V

    throw v0

    .line 75
    :cond_0
    iget-object v1, p0, Lcom/google/obf/ag;->b:Lcom/google/obf/da;

    add-int v2, p2, p4

    sub-int v3, p3, p4

    invoke-interface {v1, p1, v2, v3}, Lcom/google/obf/da;->a([BII)I

    move-result v1

    .line 76
    if-ne v1, v0, :cond_2

    .line 77
    if-nez p4, :cond_1

    if-eqz p5, :cond_1

    .line 80
    :goto_0
    return v0

    .line 79
    :cond_1
    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 80
    :cond_2
    add-int v0, p4, v1

    goto :goto_0
.end method

.method private d([BII)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 63
    iget v1, p0, Lcom/google/obf/ag;->g:I

    if-nez v1, :cond_0

    .line 68
    :goto_0
    return v0

    .line 65
    :cond_0
    iget v1, p0, Lcom/google/obf/ag;->g:I

    invoke-static {v1, p3}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 66
    iget-object v2, p0, Lcom/google/obf/ag;->e:[B

    invoke-static {v2, v0, p1, p2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 67
    invoke-direct {p0, v1}, Lcom/google/obf/ag;->f(I)V

    move v0, v1

    .line 68
    goto :goto_0
.end method

.method private d(I)V
    .locals 3

    .prologue
    .line 56
    iget v0, p0, Lcom/google/obf/ag;->f:I

    add-int/2addr v0, p1

    .line 57
    iget-object v1, p0, Lcom/google/obf/ag;->e:[B

    array-length v1, v1

    if-le v0, v1, :cond_0

    .line 58
    iget-object v1, p0, Lcom/google/obf/ag;->e:[B

    iget-object v2, p0, Lcom/google/obf/ag;->e:[B

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x2

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-static {v1, v0}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/obf/ag;->e:[B

    .line 59
    :cond_0
    return-void
.end method

.method private e(I)I
    .locals 1

    .prologue
    .line 60
    iget v0, p0, Lcom/google/obf/ag;->g:I

    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 61
    invoke-direct {p0, v0}, Lcom/google/obf/ag;->f(I)V

    .line 62
    return v0
.end method

.method private f(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 69
    iget v0, p0, Lcom/google/obf/ag;->g:I

    sub-int/2addr v0, p1

    iput v0, p0, Lcom/google/obf/ag;->g:I

    .line 70
    iput v3, p0, Lcom/google/obf/ag;->f:I

    .line 71
    iget-object v0, p0, Lcom/google/obf/ag;->e:[B

    iget-object v1, p0, Lcom/google/obf/ag;->e:[B

    iget v2, p0, Lcom/google/obf/ag;->g:I

    invoke-static {v0, p1, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 72
    return-void
.end method

.method private g(I)V
    .locals 4

    .prologue
    .line 81
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 82
    iget-wide v0, p0, Lcom/google/obf/ag;->d:J

    int-to-long v2, p1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/obf/ag;->d:J

    .line 83
    :cond_0
    return-void
.end method


# virtual methods
.method public a(I)I
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 19
    invoke-direct {p0, p1}, Lcom/google/obf/ag;->e(I)I

    move-result v0

    .line 20
    if-nez v0, :cond_0

    .line 21
    sget-object v1, Lcom/google/obf/ag;->a:[B

    sget-object v0, Lcom/google/obf/ag;->a:[B

    array-length v0, v0

    .line 22
    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v3

    const/4 v5, 0x1

    move-object v0, p0

    move v4, v2

    invoke-direct/range {v0 .. v5}, Lcom/google/obf/ag;->a([BIIIZ)I

    move-result v0

    .line 23
    :cond_0
    invoke-direct {p0, v0}, Lcom/google/obf/ag;->g(I)V

    .line 24
    return v0
.end method

.method public a([BII)I
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 7
    invoke-direct {p0, p1, p2, p3}, Lcom/google/obf/ag;->d([BII)I

    move-result v0

    .line 8
    if-nez v0, :cond_0

    .line 9
    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/obf/ag;->a([BIIIZ)I

    move-result v0

    .line 10
    :cond_0
    invoke-direct {p0, v0}, Lcom/google/obf/ag;->g(I)V

    .line 11
    return v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/obf/ag;->f:I

    .line 52
    return-void
.end method

.method public a(IZ)Z
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v6, -0x1

    .line 25
    invoke-direct {p0, p1}, Lcom/google/obf/ag;->e(I)I

    move-result v4

    .line 26
    :goto_0
    if-ge v4, p1, :cond_0

    if-eq v4, v6, :cond_0

    .line 27
    sget-object v1, Lcom/google/obf/ag;->a:[B

    neg-int v2, v4

    sget-object v0, Lcom/google/obf/ag;->a:[B

    array-length v0, v0

    add-int/2addr v0, v4

    .line 28
    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v3

    move-object v0, p0

    move v5, p2

    .line 29
    invoke-direct/range {v0 .. v5}, Lcom/google/obf/ag;->a([BIIIZ)I

    move-result v4

    goto :goto_0

    .line 30
    :cond_0
    invoke-direct {p0, v4}, Lcom/google/obf/ag;->g(I)V

    .line 31
    if-eq v4, v6, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public a([BIIZ)Z
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v6, -0x1

    .line 12
    invoke-direct {p0, p1, p2, p3}, Lcom/google/obf/ag;->d([BII)I

    move-result v4

    .line 13
    :goto_0
    if-ge v4, p3, :cond_0

    if-eq v4, v6, :cond_0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v5, p4

    .line 14
    invoke-direct/range {v0 .. v5}, Lcom/google/obf/ag;->a([BIIIZ)I

    move-result v4

    goto :goto_0

    .line 15
    :cond_0
    invoke-direct {p0, v4}, Lcom/google/obf/ag;->g(I)V

    .line 16
    if-eq v4, v6, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public b()J
    .locals 4

    .prologue
    .line 53
    iget-wide v0, p0, Lcom/google/obf/ag;->d:J

    iget v2, p0, Lcom/google/obf/ag;->f:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    return-wide v0
.end method

.method public b(I)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 32
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/obf/ag;->a(IZ)Z

    .line 33
    return-void
.end method

.method public b([BII)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 17
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/google/obf/ag;->a([BIIZ)Z

    .line 18
    return-void
.end method

.method public b(IZ)Z
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/google/obf/ag;->d(I)V

    .line 41
    iget v0, p0, Lcom/google/obf/ag;->g:I

    iget v1, p0, Lcom/google/obf/ag;->f:I

    sub-int/2addr v0, v1

    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 42
    :cond_0
    if-ge v4, p1, :cond_1

    .line 43
    iget-object v1, p0, Lcom/google/obf/ag;->e:[B

    iget v2, p0, Lcom/google/obf/ag;->f:I

    move-object v0, p0

    move v3, p1

    move v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/obf/ag;->a([BIIIZ)I

    move-result v4

    .line 44
    const/4 v0, -0x1

    if-ne v4, v0, :cond_0

    .line 45
    const/4 v0, 0x0

    .line 48
    :goto_0
    return v0

    .line 46
    :cond_1
    iget v0, p0, Lcom/google/obf/ag;->f:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/google/obf/ag;->f:I

    .line 47
    iget v0, p0, Lcom/google/obf/ag;->g:I

    iget v1, p0, Lcom/google/obf/ag;->f:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/google/obf/ag;->g:I

    .line 48
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b([BIIZ)Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 34
    invoke-virtual {p0, p3, p4}, Lcom/google/obf/ag;->b(IZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 35
    const/4 v0, 0x0

    .line 37
    :goto_0
    return v0

    .line 36
    :cond_0
    iget-object v0, p0, Lcom/google/obf/ag;->e:[B

    iget v1, p0, Lcom/google/obf/ag;->f:I

    sub-int/2addr v1, p3

    invoke-static {v0, v1, p1, p2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 37
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public c()J
    .locals 2

    .prologue
    .line 54
    iget-wide v0, p0, Lcom/google/obf/ag;->d:J

    return-wide v0
.end method

.method public c(I)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 49
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/obf/ag;->b(IZ)Z

    .line 50
    return-void
.end method

.method public c([BII)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 38
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/google/obf/ag;->b([BIIZ)Z

    .line 39
    return-void
.end method

.method public d()J
    .locals 2

    .prologue
    .line 55
    iget-wide v0, p0, Lcom/google/obf/ag;->c:J

    return-wide v0
.end method
