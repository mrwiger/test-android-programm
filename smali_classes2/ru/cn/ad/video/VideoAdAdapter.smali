.class public abstract Lru/cn/ad/video/VideoAdAdapter;
.super Lru/cn/ad/AdAdapter;
.source "VideoAdAdapter.java"


# instance fields
.field public maxVideoDimension:I

.field public optimalVideoDimension:I

.field private settings:Lru/cn/ad/video/RenderingSettings;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lru/cn/api/money_miner/replies/AdSystem;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "system"    # Lru/cn/api/money_miner/replies/AdSystem;

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Lru/cn/ad/AdAdapter;-><init>(Landroid/content/Context;Lru/cn/api/money_miner/replies/AdSystem;)V

    .line 10
    const/16 v0, 0x2d0

    iput v0, p0, Lru/cn/ad/video/VideoAdAdapter;->maxVideoDimension:I

    .line 11
    const/16 v0, 0x1e0

    iput v0, p0, Lru/cn/ad/video/VideoAdAdapter;->optimalVideoDimension:I

    .line 17
    return-void
.end method


# virtual methods
.method public getRenderingSettings()Lru/cn/ad/video/RenderingSettings;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lru/cn/ad/video/VideoAdAdapter;->settings:Lru/cn/ad/video/RenderingSettings;

    return-object v0
.end method

.method public final setRenderingSettings(Lru/cn/ad/video/RenderingSettings;)V
    .locals 0
    .param p1, "settings"    # Lru/cn/ad/video/RenderingSettings;

    .prologue
    .line 20
    iput-object p1, p0, Lru/cn/ad/video/VideoAdAdapter;->settings:Lru/cn/ad/video/RenderingSettings;

    .line 21
    return-void
.end method
