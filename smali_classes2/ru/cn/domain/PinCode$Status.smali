.class public final enum Lru/cn/domain/PinCode$Status;
.super Ljava/lang/Enum;
.source "PinCode.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/domain/PinCode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Status"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lru/cn/domain/PinCode$Status;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lru/cn/domain/PinCode$Status;

.field public static final enum disable:Lru/cn/domain/PinCode$Status;

.field public static final enum require:Lru/cn/domain/PinCode$Status;

.field public static final enum temporarilyDisable:Lru/cn/domain/PinCode$Status;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 19
    new-instance v0, Lru/cn/domain/PinCode$Status;

    const-string v1, "require"

    invoke-direct {v0, v1, v2}, Lru/cn/domain/PinCode$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/domain/PinCode$Status;->require:Lru/cn/domain/PinCode$Status;

    new-instance v0, Lru/cn/domain/PinCode$Status;

    const-string v1, "temporarilyDisable"

    invoke-direct {v0, v1, v3}, Lru/cn/domain/PinCode$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/domain/PinCode$Status;->temporarilyDisable:Lru/cn/domain/PinCode$Status;

    new-instance v0, Lru/cn/domain/PinCode$Status;

    const-string v1, "disable"

    invoke-direct {v0, v1, v4}, Lru/cn/domain/PinCode$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/domain/PinCode$Status;->disable:Lru/cn/domain/PinCode$Status;

    .line 18
    const/4 v0, 0x3

    new-array v0, v0, [Lru/cn/domain/PinCode$Status;

    sget-object v1, Lru/cn/domain/PinCode$Status;->require:Lru/cn/domain/PinCode$Status;

    aput-object v1, v0, v2

    sget-object v1, Lru/cn/domain/PinCode$Status;->temporarilyDisable:Lru/cn/domain/PinCode$Status;

    aput-object v1, v0, v3

    sget-object v1, Lru/cn/domain/PinCode$Status;->disable:Lru/cn/domain/PinCode$Status;

    aput-object v1, v0, v4

    sput-object v0, Lru/cn/domain/PinCode$Status;->$VALUES:[Lru/cn/domain/PinCode$Status;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lru/cn/domain/PinCode$Status;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 18
    const-class v0, Lru/cn/domain/PinCode$Status;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lru/cn/domain/PinCode$Status;

    return-object v0
.end method

.method public static values()[Lru/cn/domain/PinCode$Status;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lru/cn/domain/PinCode$Status;->$VALUES:[Lru/cn/domain/PinCode$Status;

    invoke-virtual {v0}, [Lru/cn/domain/PinCode$Status;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lru/cn/domain/PinCode$Status;

    return-object v0
.end method
