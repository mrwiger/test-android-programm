.class Lcom/koushikdutta/async/http/spdy/SpdyMiddleware$2$1;
.super Lcom/koushikdutta/async/http/spdy/AsyncSpdyConnection;
.source "SpdyMiddleware.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/koushikdutta/async/http/spdy/SpdyMiddleware$2;->onHandshakeCompleted(Ljava/lang/Exception;Lcom/koushikdutta/async/AsyncSSLSocket;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field hasReceivedSettings:Z

.field final synthetic this$1:Lcom/koushikdutta/async/http/spdy/SpdyMiddleware$2;


# direct methods
.method constructor <init>(Lcom/koushikdutta/async/http/spdy/SpdyMiddleware$2;Lcom/koushikdutta/async/AsyncSocket;Lcom/koushikdutta/async/http/Protocol;)V
    .locals 0
    .param p2, "x0"    # Lcom/koushikdutta/async/AsyncSocket;
    .param p3, "x1"    # Lcom/koushikdutta/async/http/Protocol;

    .prologue
    .line 219
    iput-object p1, p0, Lcom/koushikdutta/async/http/spdy/SpdyMiddleware$2$1;->this$1:Lcom/koushikdutta/async/http/spdy/SpdyMiddleware$2;

    invoke-direct {p0, p2, p3}, Lcom/koushikdutta/async/http/spdy/AsyncSpdyConnection;-><init>(Lcom/koushikdutta/async/AsyncSocket;Lcom/koushikdutta/async/http/Protocol;)V

    return-void
.end method


# virtual methods
.method public settings(ZLcom/koushikdutta/async/http/spdy/Settings;)V
    .locals 5
    .param p1, "clearPrevious"    # Z
    .param p2, "settings"    # Lcom/koushikdutta/async/http/spdy/Settings;

    .prologue
    .line 223
    invoke-super {p0, p1, p2}, Lcom/koushikdutta/async/http/spdy/AsyncSpdyConnection;->settings(ZLcom/koushikdutta/async/http/spdy/Settings;)V

    .line 224
    iget-boolean v2, p0, Lcom/koushikdutta/async/http/spdy/SpdyMiddleware$2$1;->hasReceivedSettings:Z

    if-nez v2, :cond_1

    .line 226
    :try_start_0
    invoke-virtual {p0}, Lcom/koushikdutta/async/http/spdy/SpdyMiddleware$2$1;->sendConnectionPreface()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 230
    :goto_0
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/koushikdutta/async/http/spdy/SpdyMiddleware$2$1;->hasReceivedSettings:Z

    .line 232
    iget-object v2, p0, Lcom/koushikdutta/async/http/spdy/SpdyMiddleware$2$1;->this$1:Lcom/koushikdutta/async/http/spdy/SpdyMiddleware$2;

    iget-object v2, v2, Lcom/koushikdutta/async/http/spdy/SpdyMiddleware$2;->this$0:Lcom/koushikdutta/async/http/spdy/SpdyMiddleware;

    iget-object v2, v2, Lcom/koushikdutta/async/http/spdy/SpdyMiddleware;->connections:Ljava/util/Hashtable;

    iget-object v3, p0, Lcom/koushikdutta/async/http/spdy/SpdyMiddleware$2$1;->this$1:Lcom/koushikdutta/async/http/spdy/SpdyMiddleware$2;

    iget-object v3, v3, Lcom/koushikdutta/async/http/spdy/SpdyMiddleware$2;->val$key:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/koushikdutta/async/http/spdy/SpdyMiddleware$SpdyConnectionWaiter;

    .line 234
    .local v1, "waiter":Lcom/koushikdutta/async/http/spdy/SpdyMiddleware$SpdyConnectionWaiter;
    iget-object v2, v1, Lcom/koushikdutta/async/http/spdy/SpdyMiddleware$SpdyConnectionWaiter;->originalCancellable:Lcom/koushikdutta/async/future/SimpleCancellable;

    invoke-virtual {v2}, Lcom/koushikdutta/async/future/SimpleCancellable;->setComplete()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 235
    iget-object v2, p0, Lcom/koushikdutta/async/http/spdy/SpdyMiddleware$2$1;->this$1:Lcom/koushikdutta/async/http/spdy/SpdyMiddleware$2;

    iget-object v2, v2, Lcom/koushikdutta/async/http/spdy/SpdyMiddleware$2;->val$data:Lcom/koushikdutta/async/http/AsyncHttpClientMiddleware$GetSocketData;

    iget-object v2, v2, Lcom/koushikdutta/async/http/AsyncHttpClientMiddleware$GetSocketData;->request:Lcom/koushikdutta/async/http/AsyncHttpRequest;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "using new spdy connection for host: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/koushikdutta/async/http/spdy/SpdyMiddleware$2$1;->this$1:Lcom/koushikdutta/async/http/spdy/SpdyMiddleware$2;

    iget-object v4, v4, Lcom/koushikdutta/async/http/spdy/SpdyMiddleware$2;->val$data:Lcom/koushikdutta/async/http/AsyncHttpClientMiddleware$GetSocketData;

    iget-object v4, v4, Lcom/koushikdutta/async/http/AsyncHttpClientMiddleware$GetSocketData;->request:Lcom/koushikdutta/async/http/AsyncHttpRequest;

    invoke-virtual {v4}, Lcom/koushikdutta/async/http/AsyncHttpRequest;->getUri()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/koushikdutta/async/http/AsyncHttpRequest;->logv(Ljava/lang/String;)V

    .line 236
    iget-object v2, p0, Lcom/koushikdutta/async/http/spdy/SpdyMiddleware$2$1;->this$1:Lcom/koushikdutta/async/http/spdy/SpdyMiddleware$2;

    iget-object v2, v2, Lcom/koushikdutta/async/http/spdy/SpdyMiddleware$2;->this$0:Lcom/koushikdutta/async/http/spdy/SpdyMiddleware;

    iget-object v3, p0, Lcom/koushikdutta/async/http/spdy/SpdyMiddleware$2$1;->this$1:Lcom/koushikdutta/async/http/spdy/SpdyMiddleware$2;

    iget-object v3, v3, Lcom/koushikdutta/async/http/spdy/SpdyMiddleware$2;->val$data:Lcom/koushikdutta/async/http/AsyncHttpClientMiddleware$GetSocketData;

    iget-object v4, p0, Lcom/koushikdutta/async/http/spdy/SpdyMiddleware$2$1;->this$1:Lcom/koushikdutta/async/http/spdy/SpdyMiddleware$2;

    iget-object v4, v4, Lcom/koushikdutta/async/http/spdy/SpdyMiddleware$2;->val$callback:Lcom/koushikdutta/async/callback/ConnectCallback;

    invoke-static {v2, v3, p0, v4}, Lcom/koushikdutta/async/http/spdy/SpdyMiddleware;->access$400(Lcom/koushikdutta/async/http/spdy/SpdyMiddleware;Lcom/koushikdutta/async/http/AsyncHttpClientMiddleware$GetSocketData;Lcom/koushikdutta/async/http/spdy/AsyncSpdyConnection;Lcom/koushikdutta/async/callback/ConnectCallback;)V

    .line 239
    :cond_0
    invoke-virtual {v1, p0}, Lcom/koushikdutta/async/http/spdy/SpdyMiddleware$SpdyConnectionWaiter;->setComplete(Ljava/lang/Object;)Z

    .line 241
    .end local v1    # "waiter":Lcom/koushikdutta/async/http/spdy/SpdyMiddleware$SpdyConnectionWaiter;
    :cond_1
    return-void

    .line 227
    :catch_0
    move-exception v0

    .line 228
    .local v0, "e1":Ljava/io/IOException;
    invoke-static {v0}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
