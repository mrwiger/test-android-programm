.class public Lru/cn/tv/player/SimplePlayerFragmentEx;
.super Lru/cn/tv/player/SimplePlayerFragment;
.source "SimplePlayerFragmentEx.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/tv/player/SimplePlayerFragmentEx$SubscribeListener;,
        Lru/cn/tv/player/SimplePlayerFragmentEx$ITrackSelectorListener;
    }
.end annotation


# instance fields
.field private allowMakeFavorite:Z

.field private allowShare:Z

.field private castConnectionProgress:Landroid/view/View;

.field private castConnectionText:Landroid/widget/TextView;

.field private castConnectionWrapper:Landroid/view/View;

.field private castContext:Lcom/google/android/gms/cast/framework/CastContext;

.field private final castListener:Lcom/google/android/gms/cast/framework/SessionManagerListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/cast/framework/SessionManagerListener",
            "<",
            "Lcom/google/android/gms/cast/framework/CastSession;",
            ">;"
        }
    .end annotation
.end field

.field private favouriteMenuItem:Landroid/view/MenuItem;

.field private fitModeClickListener:Landroid/view/View$OnClickListener;

.field private fullscreenClickListener:Landroid/view/View$OnClickListener;

.field private isFullScreen:Z

.field private minimized:Z

.field private mustHideControls:Z

.field private nextProgramView:Lru/cn/tv/player/controller/TouchNextProgramView;

.field private playButton:Landroid/widget/ImageButton;

.field private playerController:Lru/cn/tv/player/controller/TouchPlayerController;

.field private shareActionProvider:Landroid/support/v7/widget/ShareActionProvider;

.field private shareMenuItem:Landroid/view/MenuItem;

.field private snapshotImageView:Landroid/widget/ImageView;

.field private snapshotTextView:Landroid/widget/TextView;

.field private snapshotUrl:Ljava/lang/String;

.field private snapshotView:Landroid/view/View;

.field private subscribeListener:Lru/cn/tv/player/SimplePlayerFragmentEx$SubscribeListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 56
    invoke-direct {p0}, Lru/cn/tv/player/SimplePlayerFragment;-><init>()V

    .line 70
    iput-boolean v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->isFullScreen:Z

    .line 83
    iput-boolean v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->mustHideControls:Z

    .line 775
    new-instance v0, Lru/cn/tv/player/SimplePlayerFragmentEx$7;

    invoke-direct {v0, p0}, Lru/cn/tv/player/SimplePlayerFragmentEx$7;-><init>(Lru/cn/tv/player/SimplePlayerFragmentEx;)V

    iput-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->fullscreenClickListener:Landroid/view/View$OnClickListener;

    .line 783
    new-instance v0, Lru/cn/tv/player/SimplePlayerFragmentEx$8;

    invoke-direct {v0, p0}, Lru/cn/tv/player/SimplePlayerFragmentEx$8;-><init>(Lru/cn/tv/player/SimplePlayerFragmentEx;)V

    iput-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->fitModeClickListener:Landroid/view/View$OnClickListener;

    .line 804
    new-instance v0, Lru/cn/tv/player/SimplePlayerFragmentEx$9;

    invoke-direct {v0, p0}, Lru/cn/tv/player/SimplePlayerFragmentEx$9;-><init>(Lru/cn/tv/player/SimplePlayerFragmentEx;)V

    iput-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->castListener:Lcom/google/android/gms/cast/framework/SessionManagerListener;

    return-void
.end method

.method static synthetic access$000(Lru/cn/tv/player/SimplePlayerFragmentEx;)Lru/cn/tv/player/controller/TouchPlayerController;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/player/SimplePlayerFragmentEx;

    .prologue
    .line 56
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->playerController:Lru/cn/tv/player/controller/TouchPlayerController;

    return-object v0
.end method

.method static synthetic access$100(Lru/cn/tv/player/SimplePlayerFragmentEx;)Z
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/player/SimplePlayerFragmentEx;

    .prologue
    .line 56
    iget-boolean v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->minimized:Z

    return v0
.end method

.method static synthetic access$200(Lru/cn/tv/player/SimplePlayerFragmentEx;)Lru/cn/tv/player/controller/TouchNextProgramView;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/player/SimplePlayerFragmentEx;

    .prologue
    .line 56
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->nextProgramView:Lru/cn/tv/player/controller/TouchNextProgramView;

    return-object v0
.end method

.method static synthetic access$300(Lru/cn/tv/player/SimplePlayerFragmentEx;)Landroid/view/MenuItem;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/player/SimplePlayerFragmentEx;

    .prologue
    .line 56
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->favouriteMenuItem:Landroid/view/MenuItem;

    return-object v0
.end method

.method private getAudioTrackNameGenerator()Lru/cn/player/ITrackSelector$TrackNameGenerator;
    .locals 1

    .prologue
    .line 666
    new-instance v0, Lru/cn/tv/player/SimplePlayerFragmentEx$6;

    invoke-direct {v0, p0}, Lru/cn/tv/player/SimplePlayerFragmentEx$6;-><init>(Lru/cn/tv/player/SimplePlayerFragmentEx;)V

    return-object v0
.end method

.method private getSubtitlesTrackNameGenerator()Lru/cn/player/ITrackSelector$TrackNameGenerator;
    .locals 1

    .prologue
    .line 646
    new-instance v0, Lru/cn/tv/player/SimplePlayerFragmentEx$5;

    invoke-direct {v0, p0}, Lru/cn/tv/player/SimplePlayerFragmentEx$5;-><init>(Lru/cn/tv/player/SimplePlayerFragmentEx;)V

    return-object v0
.end method

.method private getVideoTrackNameGenerator()Lru/cn/player/ITrackSelector$TrackNameGenerator;
    .locals 1

    .prologue
    .line 688
    sget-object v0, Lru/cn/tv/player/SimplePlayerFragmentEx$$Lambda$7;->$instance:Lru/cn/player/ITrackSelector$TrackNameGenerator;

    return-object v0
.end method

.method private hideFavoriteAndShareMenuItems()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 741
    iput-boolean v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->allowShare:Z

    .line 742
    iput-boolean v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->allowMakeFavorite:Z

    .line 743
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->favouriteMenuItem:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    .line 744
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->favouriteMenuItem:Landroid/view/MenuItem;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 746
    :cond_0
    invoke-direct {p0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->updateTopPanelButtons()V

    .line 747
    return-void
.end method

.method private hideSnapshot()V
    .locals 2

    .prologue
    .line 737
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->snapshotView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 738
    return-void
.end method

.method static final synthetic lambda$getVideoTrackNameGenerator$6$SimplePlayerFragmentEx(Lru/cn/player/TrackInfo;)Ljava/lang/String;
    .locals 2
    .param p0, "trackInfo"    # Lru/cn/player/TrackInfo;

    .prologue
    .line 688
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, Lru/cn/player/TrackInfo;->height:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "p"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static final synthetic lambda$showQualityDialog$5$SimplePlayerFragmentEx(Lru/cn/player/ITrackSelector;I)V
    .locals 1
    .param p0, "videoTracksSelector"    # Lru/cn/player/ITrackSelector;
    .param p1, "index"    # I

    .prologue
    .line 632
    invoke-interface {p0}, Lru/cn/player/ITrackSelector;->adaptive()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 633
    if-nez p1, :cond_0

    .line 634
    invoke-interface {p0}, Lru/cn/player/ITrackSelector;->setAdaptive()V

    .line 641
    :goto_0
    return-void

    .line 636
    :cond_0
    add-int/lit8 v0, p1, -0x1

    invoke-interface {p0, v0}, Lru/cn/player/ITrackSelector;->selectItem(I)V

    goto :goto_0

    .line 639
    :cond_1
    invoke-interface {p0, p1}, Lru/cn/player/ITrackSelector;->selectItem(I)V

    goto :goto_0
.end method

.method static final synthetic lambda$showSelectableDialog$7$SimplePlayerFragmentEx(Lru/cn/tv/player/SimplePlayerFragmentEx$ITrackSelectorListener;Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog;I)V
    .locals 0
    .param p0, "iTrackSelectorListener"    # Lru/cn/tv/player/SimplePlayerFragmentEx$ITrackSelectorListener;
    .param p1, "bottomSheetQualityDialog"    # Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog;
    .param p2, "position"    # I

    .prologue
    .line 694
    invoke-interface {p0, p2}, Lru/cn/tv/player/SimplePlayerFragmentEx$ITrackSelectorListener;->trackSelected(I)V

    .line 695
    invoke-virtual {p1}, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog;->dismiss()V

    .line 696
    return-void
.end method

.method static final synthetic lambda$showSubtitlesDialog$4$SimplePlayerFragmentEx(Lru/cn/player/ITrackSelector;I)V
    .locals 1
    .param p0, "subtitlesTrackSelector"    # Lru/cn/player/ITrackSelector;
    .param p1, "index"    # I

    .prologue
    .line 597
    invoke-interface {p0}, Lru/cn/player/ITrackSelector;->deactivatable()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 598
    if-nez p1, :cond_0

    .line 599
    invoke-interface {p0}, Lru/cn/player/ITrackSelector;->disable()V

    .line 606
    :goto_0
    return-void

    .line 601
    :cond_0
    add-int/lit8 v0, p1, -0x1

    invoke-interface {p0, v0}, Lru/cn/player/ITrackSelector;->selectItem(I)V

    goto :goto_0

    .line 604
    :cond_1
    invoke-interface {p0, p1}, Lru/cn/player/ITrackSelector;->selectItem(I)V

    goto :goto_0
.end method

.method private setupShareProvider(Lru/cn/api/tv/replies/Telecast;)V
    .locals 5
    .param p1, "telecast"    # Lru/cn/api/tv/replies/Telecast;

    .prologue
    .line 762
    iget-object v2, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->shareActionProvider:Landroid/support/v7/widget/ShareActionProvider;

    if-nez v2, :cond_0

    .line 772
    :goto_0
    return-void

    .line 765
    :cond_0
    invoke-virtual {p0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 766
    .local v0, "context":Landroid/content/Context;
    iget-object v2, p1, Lru/cn/api/tv/replies/Telecast;->title:Ljava/lang/String;

    iget-object v3, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->currentChannelTitle:Ljava/lang/String;

    iget-object v4, p1, Lru/cn/api/tv/replies/Telecast;->URL:Ljava/lang/String;

    invoke-static {v0, v2, v3, v4}, Lru/cn/utils/ShareUtils;->shareIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 768
    .local v1, "intent":Landroid/content/Intent;
    iget-object v2, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->shareActionProvider:Landroid/support/v7/widget/ShareActionProvider;

    invoke-virtual {v2, v1}, Landroid/support/v7/widget/ShareActionProvider;->setShareIntent(Landroid/content/Intent;)V

    .line 770
    const/4 v2, 0x1

    iput-boolean v2, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->allowShare:Z

    .line 771
    invoke-direct {p0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->updateTopPanelButtons()V

    goto :goto_0
.end method

.method private showAudioDialog(Lru/cn/player/ITrackSelector;)V
    .locals 3
    .param p1, "audioTrackSelector"    # Lru/cn/player/ITrackSelector;

    .prologue
    .line 610
    .line 611
    invoke-direct {p0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->getAudioTrackNameGenerator()Lru/cn/player/ITrackSelector$TrackNameGenerator;

    move-result-object v0

    invoke-interface {p1, v0}, Lru/cn/player/ITrackSelector;->getTracksName(Lru/cn/player/ITrackSelector$TrackNameGenerator;)Ljava/util/List;

    move-result-object v0

    .line 612
    invoke-interface {p1}, Lru/cn/player/ITrackSelector;->getCurrentTrackIndex()I

    move-result v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {p1}, Lru/cn/tv/player/SimplePlayerFragmentEx$$Lambda$5;->get$Lambda(Lru/cn/player/ITrackSelector;)Lru/cn/tv/player/SimplePlayerFragmentEx$ITrackSelectorListener;

    move-result-object v2

    .line 610
    invoke-direct {p0, v0, v1, v2}, Lru/cn/tv/player/SimplePlayerFragmentEx;->showSelectableDialog(Ljava/util/List;ILru/cn/tv/player/SimplePlayerFragmentEx$ITrackSelectorListener;)V

    .line 614
    return-void
.end method

.method private showMoreDialog()V
    .locals 8

    .prologue
    .line 534
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 536
    .local v7, "optionTypesList":Ljava/util/List;, "Ljava/util/List<Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;>;"
    invoke-virtual {p0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->getVideoTrackInfoProvider()Lru/cn/player/ITrackSelector;

    move-result-object v4

    .line 537
    .local v4, "videoTracksSelector":Lru/cn/player/ITrackSelector;
    if-eqz v4, :cond_0

    invoke-interface {v4}, Lru/cn/player/ITrackSelector;->containsTracks()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 538
    sget-object v0, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;->QUALITY_TYPE:Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 541
    :cond_0
    invoke-virtual {p0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->getAudioTrackInfoProvider()Lru/cn/player/ITrackSelector;

    move-result-object v5

    .line 542
    .local v5, "audioTrackSelector":Lru/cn/player/ITrackSelector;
    if-eqz v5, :cond_1

    invoke-interface {v5}, Lru/cn/player/ITrackSelector;->containsTracks()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 543
    sget-object v0, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;->AUDIO_TYPE:Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 546
    :cond_1
    invoke-virtual {p0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->getSubtitlesTrackProvider()Lru/cn/player/ITrackSelector;

    move-result-object v6

    .line 547
    .local v6, "subtitlesTrackSelector":Lru/cn/player/ITrackSelector;
    if-eqz v6, :cond_2

    invoke-interface {v6}, Lru/cn/player/ITrackSelector;->containsTracks()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 548
    sget-object v0, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;->SUBTITLE_TYPE:Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 551
    :cond_2
    sget-object v0, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;->COMPLAIN_TYPE:Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 553
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;

    invoke-interface {v7, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;

    .line 555
    .local v3, "optionTypes":[Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;
    invoke-virtual {p0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v3}, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog;->newInstance(Landroid/content/Context;[Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;)Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog;

    move-result-object v2

    .line 556
    .local v2, "bottomSheetMenuDialog":Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog;
    new-instance v0, Lru/cn/tv/player/SimplePlayerFragmentEx$$Lambda$3;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lru/cn/tv/player/SimplePlayerFragmentEx$$Lambda$3;-><init>(Lru/cn/tv/player/SimplePlayerFragmentEx;Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog;[Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;Lru/cn/player/ITrackSelector;Lru/cn/player/ITrackSelector;Lru/cn/player/ITrackSelector;)V

    invoke-virtual {v2, v0}, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog;->setOnItemSelectedListener(Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OnItemSelectedListener;)V

    .line 579
    invoke-virtual {p0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string v1, "MoreDialog"

    invoke-virtual {v2, v0, v1}, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 580
    return-void
.end method

.method private showPaidContentDialog(JJ)V
    .locals 11
    .param p1, "channelId"    # J
    .param p3, "contractorId"    # J

    .prologue
    .line 516
    iget-object v6, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->currentChannelTitle:Ljava/lang/String;

    .line 517
    .local v6, "channelTitle":Ljava/lang/String;
    invoke-virtual {p0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->getPlaybackMode()Lru/cn/tv/player/SimplePlayerFragment$PlaybackMode;

    move-result-object v0

    invoke-virtual {v0}, Lru/cn/tv/player/SimplePlayerFragment$PlaybackMode;->toString()Ljava/lang/String;

    move-result-object v7

    .line 518
    .local v7, "mode":Ljava/lang/String;
    invoke-static {p3, p4, v6, v7}, Lru/cn/domain/statistics/AnalyticsManager;->purchase_overlay(JLjava/lang/String;Ljava/lang/String;)V

    .line 520
    new-instance v0, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0e0115

    .line 521
    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setMessage(I)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v8

    const v9, 0x7f0e011b

    new-instance v0, Lru/cn/tv/player/SimplePlayerFragmentEx$$Lambda$2;

    move-object v1, p0

    move-wide v2, p3

    move-wide v4, p1

    invoke-direct/range {v0 .. v7}, Lru/cn/tv/player/SimplePlayerFragmentEx$$Lambda$2;-><init>(Lru/cn/tv/player/SimplePlayerFragmentEx;JJLjava/lang/String;Ljava/lang/String;)V

    .line 522
    invoke-virtual {v8, v9, v0}, Landroid/support/v7/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0e0113

    const/4 v2, 0x0

    .line 529
    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 530
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->show()Landroid/support/v7/app/AlertDialog;

    .line 531
    return-void
.end method

.method private showQualityDialog(Lru/cn/player/ITrackSelector;)V
    .locals 5
    .param p1, "videoTracksSelector"    # Lru/cn/player/ITrackSelector;

    .prologue
    .line 617
    invoke-direct {p0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->getVideoTrackNameGenerator()Lru/cn/player/ITrackSelector$TrackNameGenerator;

    move-result-object v2

    invoke-interface {p1, v2}, Lru/cn/player/ITrackSelector;->getTracksName(Lru/cn/player/ITrackSelector$TrackNameGenerator;)Ljava/util/List;

    move-result-object v1

    .line 618
    .local v1, "videoTracksName":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {p1}, Lru/cn/player/ITrackSelector;->getCurrentTrackIndex()I

    move-result v0

    .line 619
    .local v0, "selectedTrack":I
    invoke-interface {p1}, Lru/cn/player/ITrackSelector;->adaptive()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 620
    const/4 v2, 0x0

    invoke-virtual {p0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0e010d

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 621
    const/4 v2, -0x1

    if-ne v0, v2, :cond_1

    .line 622
    const/4 v0, 0x0

    .line 628
    :cond_0
    :goto_0
    new-instance v2, Lru/cn/tv/player/SimplePlayerFragmentEx$$Lambda$6;

    invoke-direct {v2, p1}, Lru/cn/tv/player/SimplePlayerFragmentEx$$Lambda$6;-><init>(Lru/cn/player/ITrackSelector;)V

    invoke-direct {p0, v1, v0, v2}, Lru/cn/tv/player/SimplePlayerFragmentEx;->showSelectableDialog(Ljava/util/List;ILru/cn/tv/player/SimplePlayerFragmentEx$ITrackSelectorListener;)V

    .line 642
    return-void

    .line 624
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private showSelectableDialog(Ljava/util/List;ILru/cn/tv/player/SimplePlayerFragmentEx$ITrackSelectorListener;)V
    .locals 3
    .param p2, "selectedItem"    # I
    .param p3, "iTrackSelectorListener"    # Lru/cn/tv/player/SimplePlayerFragmentEx$ITrackSelectorListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;I",
            "Lru/cn/tv/player/SimplePlayerFragmentEx$ITrackSelectorListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 692
    .local p1, "trackNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p2, p1}, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog;->newInstance(ILjava/util/List;)Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog;

    move-result-object v0

    .line 693
    .local v0, "bottomSheetQualityDialog":Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog;
    new-instance v1, Lru/cn/tv/player/SimplePlayerFragmentEx$$Lambda$8;

    invoke-direct {v1, p3, v0}, Lru/cn/tv/player/SimplePlayerFragmentEx$$Lambda$8;-><init>(Lru/cn/tv/player/SimplePlayerFragmentEx$ITrackSelectorListener;Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog;)V

    invoke-virtual {v0, v1}, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog;->setOnItemSelectedListener(Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OnItemSelectedListener;)V

    .line 697
    invoke-virtual {p0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "MoreDialog"

    invoke-virtual {v0, v1, v2}, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 698
    return-void
.end method

.method private showSubtitlesDialog(Lru/cn/player/ITrackSelector;)V
    .locals 5
    .param p1, "subtitlesTrackSelector"    # Lru/cn/player/ITrackSelector;

    .prologue
    .line 583
    invoke-direct {p0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->getSubtitlesTrackNameGenerator()Lru/cn/player/ITrackSelector$TrackNameGenerator;

    move-result-object v2

    invoke-interface {p1, v2}, Lru/cn/player/ITrackSelector;->getTracksName(Lru/cn/player/ITrackSelector$TrackNameGenerator;)Ljava/util/List;

    move-result-object v1

    .line 584
    .local v1, "subtitlesTracksName":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {p1}, Lru/cn/player/ITrackSelector;->getCurrentTrackIndex()I

    move-result v0

    .line 585
    .local v0, "selectedTrack":I
    invoke-interface {p1}, Lru/cn/player/ITrackSelector;->deactivatable()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 586
    const/4 v2, 0x0

    invoke-virtual {p0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0e010b

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 587
    const/4 v2, -0x1

    if-ne v0, v2, :cond_1

    .line 588
    const/4 v0, 0x0

    .line 593
    :cond_0
    :goto_0
    new-instance v2, Lru/cn/tv/player/SimplePlayerFragmentEx$$Lambda$4;

    invoke-direct {v2, p1}, Lru/cn/tv/player/SimplePlayerFragmentEx$$Lambda$4;-><init>(Lru/cn/player/ITrackSelector;)V

    invoke-direct {p0, v1, v0, v2}, Lru/cn/tv/player/SimplePlayerFragmentEx;->showSelectableDialog(Ljava/util/List;ILru/cn/tv/player/SimplePlayerFragmentEx$ITrackSelectorListener;)V

    .line 607
    return-void

    .line 590
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private startEmailIntent()V
    .locals 6

    .prologue
    .line 702
    :try_start_0
    invoke-static {}, Lru/cn/utils/FeedbackBuilder;->create()Lru/cn/utils/FeedbackBuilder;

    move-result-object v3

    .line 703
    invoke-virtual {p0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v3, v4}, Lru/cn/utils/FeedbackBuilder;->setContext(Landroid/content/Context;)Lru/cn/utils/FeedbackBuilder;

    move-result-object v2

    .line 705
    .local v2, "feedbackBuilder":Lru/cn/utils/FeedbackBuilder;
    invoke-virtual {p0, v2}, Lru/cn/tv/player/SimplePlayerFragmentEx;->onFeedbackIntent(Lru/cn/utils/FeedbackBuilder;)V

    .line 707
    invoke-virtual {v2}, Lru/cn/utils/FeedbackBuilder;->build()Landroid/content/Intent;

    move-result-object v1

    .line 709
    .local v1, "emailIntent":Landroid/content/Intent;
    const v3, 0x7f0e0072

    .line 710
    invoke-virtual {p0, v3}, Lru/cn/tv/player/SimplePlayerFragmentEx;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 709
    invoke-static {v1, v3}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {p0, v3}, Lru/cn/tv/player/SimplePlayerFragmentEx;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 716
    .end local v1    # "emailIntent":Landroid/content/Intent;
    .end local v2    # "feedbackBuilder":Lru/cn/utils/FeedbackBuilder;
    :goto_0
    return-void

    .line 711
    :catch_0
    move-exception v0

    .line 712
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    invoke-virtual {p0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const v4, 0x7f0e00b8

    .line 713
    invoke-virtual {p0, v4}, Lru/cn/tv/player/SimplePlayerFragmentEx;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    const/4 v5, 0x0

    .line 712
    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    .line 714
    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method private updateSnapshotImage()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 719
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->snapshotUrl:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 720
    invoke-virtual {p0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/picasso/Picasso;->with(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v0

    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->snapshotUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    sget-object v1, Lcom/squareup/picasso/MemoryPolicy;->NO_STORE:Lcom/squareup/picasso/MemoryPolicy;

    new-array v2, v3, [Lcom/squareup/picasso/MemoryPolicy;

    .line 721
    invoke-virtual {v0, v1, v2}, Lcom/squareup/picasso/RequestCreator;->memoryPolicy(Lcom/squareup/picasso/MemoryPolicy;[Lcom/squareup/picasso/MemoryPolicy;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    sget-object v1, Lcom/squareup/picasso/NetworkPolicy;->NO_STORE:Lcom/squareup/picasso/NetworkPolicy;

    new-array v2, v3, [Lcom/squareup/picasso/NetworkPolicy;

    .line 722
    invoke-virtual {v0, v1, v2}, Lcom/squareup/picasso/RequestCreator;->networkPolicy(Lcom/squareup/picasso/NetworkPolicy;[Lcom/squareup/picasso/NetworkPolicy;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    .line 723
    invoke-virtual {v0}, Lcom/squareup/picasso/RequestCreator;->fit()Lcom/squareup/picasso/RequestCreator;

    move-result-object v0

    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->snapshotImageView:Landroid/widget/ImageView;

    .line 724
    invoke-virtual {v0, v1}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;)V

    .line 730
    :goto_0
    return-void

    .line 727
    :cond_0
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->snapshotImageView:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060086

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 728
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->snapshotImageView:Landroid/widget/ImageView;

    const v1, 0x7f0801a5

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method private updateTopPanelButtons()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 750
    iget-boolean v4, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->minimized:Z

    if-nez v4, :cond_2

    iget-boolean v4, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->allowMakeFavorite:Z

    if-eqz v4, :cond_2

    move v0, v2

    .line 751
    .local v0, "shouldFavoriteBeVisible":Z
    :goto_0
    iget-object v4, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->favouriteMenuItem:Landroid/view/MenuItem;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->favouriteMenuItem:Landroid/view/MenuItem;

    invoke-interface {v4}, Landroid/view/MenuItem;->isVisible()Z

    move-result v4

    if-eq v4, v0, :cond_0

    .line 752
    iget-object v4, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->favouriteMenuItem:Landroid/view/MenuItem;

    invoke-interface {v4, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 755
    :cond_0
    iget-boolean v4, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->minimized:Z

    if-nez v4, :cond_3

    iget-boolean v4, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->allowShare:Z

    if-eqz v4, :cond_3

    move v1, v2

    .line 756
    .local v1, "shouldShareBeVisible":Z
    :goto_1
    iget-object v2, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->shareMenuItem:Landroid/view/MenuItem;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->shareMenuItem:Landroid/view/MenuItem;

    invoke-interface {v2}, Landroid/view/MenuItem;->isVisible()Z

    move-result v2

    if-eq v2, v1, :cond_1

    .line 757
    iget-object v2, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->shareMenuItem:Landroid/view/MenuItem;

    invoke-interface {v2, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 759
    :cond_1
    return-void

    .end local v0    # "shouldFavoriteBeVisible":Z
    .end local v1    # "shouldShareBeVisible":Z
    :cond_2
    move v0, v3

    .line 750
    goto :goto_0

    .restart local v0    # "shouldFavoriteBeVisible":Z
    :cond_3
    move v1, v3

    .line 755
    goto :goto_1
.end method


# virtual methods
.method public canMinimize()Z
    .locals 2

    .prologue
    .line 488
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->playerView:Lru/cn/player/SimplePlayer;

    invoke-virtual {v0}, Lru/cn/player/SimplePlayer;->getState()Lru/cn/player/AbstractMediaPlayer$PlayerState;

    move-result-object v0

    sget-object v1, Lru/cn/player/AbstractMediaPlayer$PlayerState;->STOPPED:Lru/cn/player/AbstractMediaPlayer$PlayerState;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected channelInfoLoaded(JLjava/lang/String;ZIZZ)V
    .locals 3
    .param p1, "cnId"    # J
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "isFavourite"    # Z
    .param p5, "channelNumber"    # I
    .param p6, "isDenied"    # Z
    .param p7, "isIntersections"    # Z

    .prologue
    .line 259
    invoke-super/range {p0 .. p7}, Lru/cn/tv/player/SimplePlayerFragment;->channelInfoLoaded(JLjava/lang/String;ZIZZ)V

    .line 261
    if-nez p6, :cond_0

    .line 262
    invoke-virtual {p0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->hideLockOverlay()V

    .line 265
    :cond_0
    if-nez p7, :cond_1

    .line 266
    if-eqz p4, :cond_2

    const v0, 0x7f0802e2

    .line 268
    .local v0, "favoriteId":I
    :goto_0
    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->favouriteMenuItem:Landroid/view/MenuItem;

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 270
    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->favouriteMenuItem:Landroid/view/MenuItem;

    new-instance v2, Lru/cn/tv/player/SimplePlayerFragmentEx$4;

    invoke-direct {v2, p0, p1, p2, p4}, Lru/cn/tv/player/SimplePlayerFragmentEx$4;-><init>(Lru/cn/tv/player/SimplePlayerFragmentEx;JZ)V

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 286
    const/4 v1, 0x1

    iput-boolean v1, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->allowMakeFavorite:Z

    .line 287
    invoke-direct {p0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->updateTopPanelButtons()V

    .line 289
    .end local v0    # "favoriteId":I
    :cond_1
    return-void

    .line 266
    :cond_2
    const v0, 0x7f0802e4

    goto :goto_0
.end method

.method public fullScreen(Z)V
    .locals 1
    .param p1, "fullScreen"    # Z

    .prologue
    .line 479
    iput-boolean p1, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->isFullScreen:Z

    .line 480
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->playerController:Lru/cn/tv/player/controller/TouchPlayerController;

    invoke-virtual {v0, p1}, Lru/cn/tv/player/controller/TouchPlayerController;->setFullScreen(Z)V

    .line 481
    return-void
.end method

.method protected getDefaultFitMode(Landroid/content/Context;)Lru/cn/player/SimplePlayer$FitMode;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 212
    sget-object v0, Lru/cn/player/SimplePlayer$FitMode;->FIT_TO_WIDTH:Lru/cn/player/SimplePlayer$FitMode;

    return-object v0
.end method

.method public hideLockOverlay()V
    .locals 2

    .prologue
    .line 733
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->playButton:Landroid/widget/ImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 734
    return-void
.end method

.method public isFullScreen()Z
    .locals 1

    .prologue
    .line 484
    iget-boolean v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->isFullScreen:Z

    return v0
.end method

.method final synthetic lambda$onLoadDeniedLocation$1$SimplePlayerFragmentEx(JJLandroid/view/View;)V
    .locals 1
    .param p1, "channelId"    # J
    .param p3, "contractorId"    # J
    .param p5, "v"    # Landroid/view/View;

    .prologue
    .line 295
    invoke-direct {p0, p1, p2, p3, p4}, Lru/cn/tv/player/SimplePlayerFragmentEx;->showPaidContentDialog(JJ)V

    return-void
.end method

.method final synthetic lambda$onViewCreated$0$SimplePlayerFragmentEx(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 163
    invoke-direct {p0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->showMoreDialog()V

    .line 164
    return-void
.end method

.method final synthetic lambda$showMoreDialog$3$SimplePlayerFragmentEx(Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog;[Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;Lru/cn/player/ITrackSelector;Lru/cn/player/ITrackSelector;Lru/cn/player/ITrackSelector;I)V
    .locals 3
    .param p1, "bottomSheetMenuDialog"    # Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog;
    .param p2, "optionTypes"    # [Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;
    .param p3, "videoTracksSelector"    # Lru/cn/player/ITrackSelector;
    .param p4, "audioTrackSelector"    # Lru/cn/player/ITrackSelector;
    .param p5, "subtitlesTrackSelector"    # Lru/cn/player/ITrackSelector;
    .param p6, "position"    # I

    .prologue
    .line 557
    invoke-virtual {p1}, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog;->dismiss()V

    .line 559
    aget-object v0, p2, p6

    .line 560
    .local v0, "optionType":Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;
    sget-object v1, Lru/cn/tv/player/SimplePlayerFragmentEx$10;->$SwitchMap$ru$cn$tv$mobile$playeroptions$BottomSheetMenuDialog$OptionType:[I

    invoke-virtual {v0}, Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 577
    :goto_0
    invoke-static {v0}, Lru/cn/domain/statistics/AnalyticsManager;->player_option(Lru/cn/tv/mobile/playeroptions/BottomSheetMenuDialog$OptionType;)V

    .line 578
    return-void

    .line 562
    :pswitch_0
    invoke-direct {p0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->startEmailIntent()V

    goto :goto_0

    .line 566
    :pswitch_1
    invoke-direct {p0, p3}, Lru/cn/tv/player/SimplePlayerFragmentEx;->showQualityDialog(Lru/cn/player/ITrackSelector;)V

    goto :goto_0

    .line 570
    :pswitch_2
    invoke-direct {p0, p4}, Lru/cn/tv/player/SimplePlayerFragmentEx;->showAudioDialog(Lru/cn/player/ITrackSelector;)V

    goto :goto_0

    .line 574
    :pswitch_3
    invoke-direct {p0, p5}, Lru/cn/tv/player/SimplePlayerFragmentEx;->showSubtitlesDialog(Lru/cn/player/ITrackSelector;)V

    goto :goto_0

    .line 560
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method final synthetic lambda$showPaidContentDialog$2$SimplePlayerFragmentEx(JJLjava/lang/String;Ljava/lang/String;Landroid/content/DialogInterface;I)V
    .locals 1
    .param p1, "contractorId"    # J
    .param p3, "channelId"    # J
    .param p5, "channelTitle"    # Ljava/lang/String;
    .param p6, "mode"    # Ljava/lang/String;
    .param p7, "dialog"    # Landroid/content/DialogInterface;
    .param p8, "which"    # I

    .prologue
    .line 523
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->subscribeListener:Lru/cn/tv/player/SimplePlayerFragmentEx$SubscribeListener;

    if-eqz v0, :cond_0

    .line 524
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->subscribeListener:Lru/cn/tv/player/SimplePlayerFragmentEx$SubscribeListener;

    invoke-interface {v0, p1, p2, p3, p4}, Lru/cn/tv/player/SimplePlayerFragmentEx$SubscribeListener;->onSubscribeClicked(JJ)V

    .line 527
    :cond_0
    invoke-static {p1, p2, p5, p6}, Lru/cn/domain/statistics/AnalyticsManager;->purchase_overlay_lk(JLjava/lang/String;Ljava/lang/String;)V

    .line 528
    return-void
.end method

.method protected loadTelecastWithoutLocation(JJ)V
    .locals 5
    .param p1, "channelId"    # J
    .param p3, "contractorId"    # J

    .prologue
    const/4 v4, 0x0

    .line 305
    invoke-direct {p0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->hideFavoriteAndShareMenuItems()V

    .line 307
    iget-object v2, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->playerController:Lru/cn/tv/player/controller/TouchPlayerController;

    invoke-virtual {v2, v4}, Lru/cn/tv/player/controller/TouchPlayerController;->setAlwaysShow(Z)V

    .line 308
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lru/cn/tv/player/SimplePlayerFragmentEx;->mustHideControls(Z)V

    .line 310
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 311
    .local v1, "now":Ljava/util/Calendar;
    const v0, 0x7f0e015a

    .line 312
    .local v0, "messageId":I
    iget-object v2, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->currentTelecastDate:Ljava/util/Calendar;

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 313
    const v0, 0x7f0e015b

    .line 316
    :cond_0
    iget-object v2, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->snapshotTextView:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 317
    iget-object v2, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->snapshotTextView:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 318
    iget-object v2, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->snapshotView:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 319
    return-void
.end method

.method public mustHideControls(Z)V
    .locals 3
    .param p1, "hide"    # Z

    .prologue
    const/4 v2, 0x0

    .line 443
    iget-boolean v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->mustHideControls:Z

    if-ne p1, v0, :cond_1

    .line 465
    :cond_0
    :goto_0
    return-void

    .line 446
    :cond_1
    iput-boolean p1, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->mustHideControls:Z

    .line 447
    iget-boolean v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->mustHideControls:Z

    if-eqz v0, :cond_2

    .line 448
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->adController:Lru/cn/ad/AdPlayController;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lru/cn/ad/AdPlayController;->setControlsVisibility(I)V

    .line 450
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->playerController:Lru/cn/tv/player/controller/TouchPlayerController;

    invoke-virtual {v0, v2}, Lru/cn/tv/player/controller/TouchPlayerController;->setTouchesEnabled(Z)V

    .line 451
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->playerController:Lru/cn/tv/player/controller/TouchPlayerController;

    invoke-virtual {v0}, Lru/cn/tv/player/controller/TouchPlayerController;->hide()V

    goto :goto_0

    .line 453
    :cond_2
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->adController:Lru/cn/ad/AdPlayController;

    invoke-virtual {v0}, Lru/cn/ad/AdPlayController;->isPresentingAd()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 454
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->adController:Lru/cn/ad/AdPlayController;

    invoke-virtual {v0, v2}, Lru/cn/ad/AdPlayController;->setControlsVisibility(I)V

    .line 457
    :cond_3
    iget-boolean v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->playingAd:Z

    if-nez v0, :cond_4

    .line 458
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->playerController:Lru/cn/tv/player/controller/TouchPlayerController;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lru/cn/tv/player/controller/TouchPlayerController;->setTouchesEnabled(Z)V

    .line 461
    :cond_4
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->playerController:Lru/cn/tv/player/controller/TouchPlayerController;

    iget-boolean v0, v0, Lru/cn/tv/player/controller/TouchPlayerController;->alwaysShow:Z

    if-eqz v0, :cond_0

    .line 462
    invoke-virtual {p0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->showController()V

    goto :goto_0
.end method

.method public onAdBreakEnded(Z)V
    .locals 2
    .param p1, "isMiddleRoll"    # Z

    .prologue
    .line 384
    invoke-super {p0, p1}, Lru/cn/tv/player/SimplePlayerFragment;->onAdBreakEnded(Z)V

    .line 386
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->playerController:Lru/cn/tv/player/controller/TouchPlayerController;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lru/cn/tv/player/controller/TouchPlayerController;->setTouchesEnabled(Z)V

    .line 387
    iget-boolean v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->minimized:Z

    if-nez v0, :cond_0

    .line 388
    invoke-virtual {p0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->showController()V

    .line 390
    :cond_0
    return-void
.end method

.method public onAdCompanion(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 359
    invoke-super {p0, p1}, Lru/cn/tv/player/SimplePlayerFragment;->onAdCompanion(Landroid/view/View;)V

    .line 361
    const/4 v0, 0x0

    .line 362
    .local v0, "companion":Lru/cn/tv/player/controller/PlayerController$Companion;
    if-eqz p1, :cond_0

    .line 363
    new-instance v0, Lru/cn/tv/player/controller/ViewCompanion;

    .end local v0    # "companion":Lru/cn/tv/player/controller/PlayerController$Companion;
    invoke-direct {v0, p1}, Lru/cn/tv/player/controller/ViewCompanion;-><init>(Landroid/view/View;)V

    .line 365
    .restart local v0    # "companion":Lru/cn/tv/player/controller/PlayerController$Companion;
    :cond_0
    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->playerController:Lru/cn/tv/player/controller/TouchPlayerController;

    invoke-virtual {v1, v0}, Lru/cn/tv/player/controller/TouchPlayerController;->setCompanion(Lru/cn/tv/player/controller/PlayerController$Companion;)V

    .line 366
    return-void
.end method

.method public onAdStart()V
    .locals 2

    .prologue
    .line 370
    invoke-super {p0}, Lru/cn/tv/player/SimplePlayerFragment;->onAdStart()V

    .line 373
    iget-boolean v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->minimized:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->mustHideControls:Z

    if-eqz v0, :cond_1

    .line 374
    :cond_0
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->adController:Lru/cn/ad/AdPlayController;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lru/cn/ad/AdPlayController;->setControlsVisibility(I)V

    .line 376
    :cond_1
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->playerController:Lru/cn/tv/player/controller/TouchPlayerController;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lru/cn/tv/player/controller/TouchPlayerController;->setTouchesEnabled(Z)V

    .line 378
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->snapshotView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 379
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->playerController:Lru/cn/tv/player/controller/TouchPlayerController;

    invoke-virtual {v0}, Lru/cn/tv/player/controller/TouchPlayerController;->hide()V

    .line 380
    return-void
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 101
    invoke-super {p0, p1}, Lru/cn/tv/player/SimplePlayerFragment;->onAttach(Landroid/content/Context;)V

    .line 103
    invoke-virtual {p0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lru/cn/utils/SafeCastContext;->getSharedContext(Landroid/content/Context;)Lcom/google/android/gms/cast/framework/CastContext;

    move-result-object v0

    iput-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->castContext:Lcom/google/android/gms/cast/framework/CastContext;

    .line 104
    return-void
.end method

.method protected onCastConnectionInProgress()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 394
    invoke-super {p0}, Lru/cn/tv/player/SimplePlayerFragment;->onCastConnectionInProgress()V

    .line 395
    invoke-virtual {p0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->isAdded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 403
    :goto_0
    return-void

    .line 399
    :cond_0
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->castConnectionProgress:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 400
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->castConnectionText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e0089

    .line 401
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 400
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 402
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->castConnectionWrapper:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method protected onCastConnectionSuccess(Ljava/lang/String;)V
    .locals 6
    .param p1, "castDeviceName"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 407
    invoke-super {p0, p1}, Lru/cn/tv/player/SimplePlayerFragment;->onCastConnectionSuccess(Ljava/lang/String;)V

    .line 408
    invoke-virtual {p0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->isAdded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 417
    :goto_0
    return-void

    .line 412
    :cond_0
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->playerController:Lru/cn/tv/player/controller/TouchPlayerController;

    invoke-virtual {v0, v5}, Lru/cn/tv/player/controller/TouchPlayerController;->setAlwaysShow(Z)V

    .line 413
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->playerController:Lru/cn/tv/player/controller/TouchPlayerController;

    invoke-virtual {v0}, Lru/cn/tv/player/controller/TouchPlayerController;->show()V

    .line 414
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->castConnectionProgress:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 415
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->castConnectionText:Landroid/widget/TextView;

    const-string v1, "%1$s %2$s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const v3, 0x7f0e0088

    invoke-virtual {p0, v3}, Lru/cn/tv/player/SimplePlayerFragmentEx;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    aput-object p1, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 416
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->castConnectionWrapper:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method protected onCastDisconnect()V
    .locals 2

    .prologue
    .line 421
    invoke-super {p0}, Lru/cn/tv/player/SimplePlayerFragment;->onCastDisconnect()V

    .line 422
    invoke-virtual {p0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->isAdded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 428
    :goto_0
    return-void

    .line 426
    :cond_0
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->playerController:Lru/cn/tv/player/controller/TouchPlayerController;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lru/cn/tv/player/controller/TouchPlayerController;->setAlwaysShow(Z)V

    .line 427
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->castConnectionWrapper:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 218
    const v0, 0x7f09011c

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->shareMenuItem:Landroid/view/MenuItem;

    .line 219
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->shareMenuItem:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 221
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->shareMenuItem:Landroid/view/MenuItem;

    invoke-static {v0}, Landroid/support/v4/view/MenuItemCompat;->getActionProvider(Landroid/view/MenuItem;)Landroid/support/v4/view/ActionProvider;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ShareActionProvider;

    iput-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->shareActionProvider:Landroid/support/v7/widget/ShareActionProvider;

    .line 223
    const/4 v0, 0x0

    invoke-interface {p1, v1, v1, v2, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->favouriteMenuItem:Landroid/view/MenuItem;

    .line 224
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->favouriteMenuItem:Landroid/view/MenuItem;

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 225
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->favouriteMenuItem:Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 226
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 107
    const v0, 0x7f0c00b0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected onLoadDeniedLocation(JJ)V
    .locals 9
    .param p1, "channelId"    # J
    .param p3, "contractorId"    # J

    .prologue
    const/4 v7, 0x0

    .line 293
    invoke-super {p0, p1, p2, p3, p4}, Lru/cn/tv/player/SimplePlayerFragment;->onLoadDeniedLocation(JJ)V

    .line 295
    iget-object v6, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->playButton:Landroid/widget/ImageButton;

    new-instance v0, Lru/cn/tv/player/SimplePlayerFragmentEx$$Lambda$1;

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lru/cn/tv/player/SimplePlayerFragmentEx$$Lambda$1;-><init>(Lru/cn/tv/player/SimplePlayerFragmentEx;JJ)V

    invoke-virtual {v6, v0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 297
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->mustHideControls(Z)V

    .line 298
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->snapshotView:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 299
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->snapshotTextView:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 300
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->playButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 301
    return-void
.end method

.method public onMaximize()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 506
    iput-boolean v1, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->minimized:Z

    .line 507
    invoke-direct {p0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->updateTopPanelButtons()V

    .line 510
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->adController:Lru/cn/ad/AdPlayController;

    invoke-virtual {v0}, Lru/cn/ad/AdPlayController;->isPresentingAd()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 511
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->adController:Lru/cn/ad/AdPlayController;

    invoke-virtual {v0, v1}, Lru/cn/ad/AdPlayController;->setControlsVisibility(I)V

    .line 513
    :cond_0
    return-void
.end method

.method public onMinimize()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 492
    iput-boolean v2, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->minimized:Z

    .line 493
    invoke-direct {p0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->updateTopPanelButtons()V

    .line 496
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->nextProgramView:Lru/cn/tv/player/controller/TouchNextProgramView;

    invoke-virtual {v0}, Lru/cn/tv/player/controller/TouchNextProgramView;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 497
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->nextProgramView:Lru/cn/tv/player/controller/TouchNextProgramView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lru/cn/tv/player/controller/TouchNextProgramView;->setVisibility(I)V

    .line 498
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->playerController:Lru/cn/tv/player/controller/TouchPlayerController;

    invoke-virtual {v0, v2}, Lru/cn/tv/player/controller/TouchPlayerController;->setTouchesEnabled(Z)V

    .line 502
    :cond_0
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->adController:Lru/cn/ad/AdPlayController;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lru/cn/ad/AdPlayController;->setControlsVisibility(I)V

    .line 503
    return-void
.end method

.method public onPause()V
    .locals 3

    .prologue
    .line 204
    invoke-super {p0}, Lru/cn/tv/player/SimplePlayerFragment;->onPause()V

    .line 205
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->castContext:Lcom/google/android/gms/cast/framework/CastContext;

    if-eqz v0, :cond_0

    .line 206
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->castContext:Lcom/google/android/gms/cast/framework/CastContext;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/framework/CastContext;->getSessionManager()Lcom/google/android/gms/cast/framework/SessionManager;

    move-result-object v0

    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->castListener:Lcom/google/android/gms/cast/framework/SessionManagerListener;

    const-class v2, Lcom/google/android/gms/cast/framework/CastSession;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/framework/SessionManager;->removeSessionManagerListener(Lcom/google/android/gms/cast/framework/SessionManagerListener;Ljava/lang/Class;)V

    .line 208
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 184
    invoke-super {p0}, Lru/cn/tv/player/SimplePlayerFragment;->onResume()V

    .line 186
    iget-object v2, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->castContext:Lcom/google/android/gms/cast/framework/CastContext;

    if-eqz v2, :cond_0

    .line 187
    iget-object v2, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->castContext:Lcom/google/android/gms/cast/framework/CastContext;

    invoke-virtual {v2}, Lcom/google/android/gms/cast/framework/CastContext;->getSessionManager()Lcom/google/android/gms/cast/framework/SessionManager;

    move-result-object v1

    .line 188
    .local v1, "castSessionManager":Lcom/google/android/gms/cast/framework/SessionManager;
    iget-object v2, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->castListener:Lcom/google/android/gms/cast/framework/SessionManagerListener;

    const-class v3, Lcom/google/android/gms/cast/framework/CastSession;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/cast/framework/SessionManager;->addSessionManagerListener(Lcom/google/android/gms/cast/framework/SessionManagerListener;Ljava/lang/Class;)V

    .line 189
    invoke-virtual {v1}, Lcom/google/android/gms/cast/framework/SessionManager;->getCurrentCastSession()Lcom/google/android/gms/cast/framework/CastSession;

    move-result-object v0

    .line 190
    .local v0, "castSession":Lcom/google/android/gms/cast/framework/CastSession;
    invoke-virtual {p0, v0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->setCastSession(Lcom/google/android/gms/cast/framework/CastSession;)V

    .line 193
    if-nez v0, :cond_0

    iget-object v2, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->castConnectionWrapper:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-nez v2, :cond_0

    .line 194
    iget-object v2, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->playerController:Lru/cn/tv/player/controller/TouchPlayerController;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lru/cn/tv/player/controller/TouchPlayerController;->setAlwaysShow(Z)V

    .line 195
    iget-object v2, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->castConnectionWrapper:Landroid/view/View;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 199
    .end local v0    # "castSession":Lcom/google/android/gms/cast/framework/CastSession;
    .end local v1    # "castSessionManager":Lcom/google/android/gms/cast/framework/SessionManager;
    :cond_0
    iget-object v2, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->playerController:Lru/cn/tv/player/controller/TouchPlayerController;

    invoke-virtual {v2}, Lru/cn/tv/player/controller/TouchPlayerController;->updateControls()V

    .line 200
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 112
    invoke-super {p0, p1, p2}, Lru/cn/tv/player/SimplePlayerFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 114
    const v0, 0x7f090167

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lru/cn/tv/player/controller/TouchNextProgramView;

    iput-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->nextProgramView:Lru/cn/tv/player/controller/TouchNextProgramView;

    .line 115
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->nextProgramView:Lru/cn/tv/player/controller/TouchNextProgramView;

    new-instance v1, Lru/cn/tv/player/SimplePlayerFragmentEx$1;

    invoke-direct {v1, p0}, Lru/cn/tv/player/SimplePlayerFragmentEx$1;-><init>(Lru/cn/tv/player/SimplePlayerFragmentEx;)V

    invoke-virtual {v0, v1}, Lru/cn/tv/player/controller/TouchNextProgramView;->setListener(Lru/cn/tv/player/controller/TouchNextProgramView$EventListener;)V

    .line 127
    const v0, 0x7f090168

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lru/cn/tv/player/controller/TouchPlayerController;

    iput-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->playerController:Lru/cn/tv/player/controller/TouchPlayerController;

    .line 128
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->playerController:Lru/cn/tv/player/controller/TouchPlayerController;

    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->fullscreenClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lru/cn/tv/player/controller/TouchPlayerController;->setOnFullScreenButtonListener(Landroid/view/View$OnClickListener;)V

    .line 129
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->playerController:Lru/cn/tv/player/controller/TouchPlayerController;

    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->fitModeClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lru/cn/tv/player/controller/TouchPlayerController;->setFitModeClickListener(Landroid/view/View$OnClickListener;)V

    .line 130
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->playerController:Lru/cn/tv/player/controller/TouchPlayerController;

    new-instance v1, Lru/cn/tv/player/SimplePlayerFragmentEx$2;

    invoke-direct {v1, p0}, Lru/cn/tv/player/SimplePlayerFragmentEx$2;-><init>(Lru/cn/tv/player/SimplePlayerFragmentEx;)V

    invoke-virtual {v0, v1}, Lru/cn/tv/player/controller/TouchPlayerController;->setCompletionBehaviour(Lru/cn/tv/player/controller/PlayerController$CompletionBehaviour;)V

    .line 150
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->playerController:Lru/cn/tv/player/controller/TouchPlayerController;

    iget-object v0, v0, Lru/cn/tv/player/controller/TouchPlayerController;->minimizeButton:Landroid/widget/ImageButton;

    new-instance v1, Lru/cn/tv/player/SimplePlayerFragmentEx$3;

    invoke-direct {v1, p0}, Lru/cn/tv/player/SimplePlayerFragmentEx$3;-><init>(Lru/cn/tv/player/SimplePlayerFragmentEx;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 162
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->playerController:Lru/cn/tv/player/controller/TouchPlayerController;

    new-instance v1, Lru/cn/tv/player/SimplePlayerFragmentEx$$Lambda$0;

    invoke-direct {v1, p0}, Lru/cn/tv/player/SimplePlayerFragmentEx$$Lambda$0;-><init>(Lru/cn/tv/player/SimplePlayerFragmentEx;)V

    invoke-virtual {v0, v1}, Lru/cn/tv/player/controller/TouchPlayerController;->setMoreMenuClickListener(Landroid/view/View$OnClickListener;)V

    .line 166
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->playerController:Lru/cn/tv/player/controller/TouchPlayerController;

    invoke-virtual {p0, v0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->setMediaController(Lru/cn/tv/player/controller/PlayerController;)V

    .line 168
    const v0, 0x7f09005d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->castConnectionWrapper:Landroid/view/View;

    .line 169
    const v0, 0x7f090062

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->castConnectionText:Landroid/widget/TextView;

    .line 170
    const v0, 0x7f09005c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->castConnectionProgress:Landroid/view/View;

    .line 172
    const v0, 0x7f090165

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->playButton:Landroid/widget/ImageButton;

    .line 174
    const v0, 0x7f0901ab

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->snapshotView:Landroid/view/View;

    .line 175
    const v0, 0x7f0900f6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->snapshotImageView:Landroid/widget/ImageView;

    .line 176
    const v0, 0x7f0901ac

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->snapshotTextView:Landroid/widget/TextView;

    .line 178
    invoke-virtual {p0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->showController()V

    .line 179
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->setHasOptionsMenu(Z)V

    .line 180
    return-void
.end method

.method public playChannel(J)V
    .locals 3
    .param p1, "cnId"    # J

    .prologue
    .line 323
    invoke-virtual {p0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->getChannelId()J

    move-result-wide v0

    cmp-long v0, p1, v0

    if-eqz v0, :cond_0

    .line 324
    invoke-direct {p0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->hideFavoriteAndShareMenuItems()V

    .line 327
    :cond_0
    invoke-virtual {p0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->hideLockOverlay()V

    .line 328
    invoke-direct {p0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->hideSnapshot()V

    .line 330
    invoke-super {p0, p1, p2}, Lru/cn/tv/player/SimplePlayerFragment;->playChannel(J)V

    .line 331
    return-void
.end method

.method protected playing(Z)V
    .locals 2
    .param p1, "playing"    # Z

    .prologue
    .line 230
    invoke-super {p0, p1}, Lru/cn/tv/player/SimplePlayerFragment;->playing(Z)V

    .line 232
    if-eqz p1, :cond_1

    .line 233
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->playerController:Lru/cn/tv/player/controller/TouchPlayerController;

    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->playerView:Lru/cn/player/SimplePlayer;

    invoke-virtual {v1}, Lru/cn/player/SimplePlayer;->getFitMode()Lru/cn/player/SimplePlayer$FitMode;

    move-result-object v1

    invoke-virtual {v0, v1}, Lru/cn/tv/player/controller/TouchPlayerController;->setFitMode(Lru/cn/player/SimplePlayer$FitMode;)V

    .line 234
    invoke-virtual {p0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->showController()V

    .line 241
    :cond_0
    :goto_0
    return-void

    .line 237
    :cond_1
    iget-boolean v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->minimized:Z

    if-eqz v0, :cond_0

    .line 238
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->playerController:Lru/cn/tv/player/controller/TouchPlayerController;

    invoke-virtual {v0}, Lru/cn/tv/player/controller/TouchPlayerController;->hide()V

    goto :goto_0
.end method

.method public setSubscribeListener(Lru/cn/tv/player/SimplePlayerFragmentEx$SubscribeListener;)V
    .locals 0
    .param p1, "listener"    # Lru/cn/tv/player/SimplePlayerFragmentEx$SubscribeListener;

    .prologue
    .line 439
    iput-object p1, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->subscribeListener:Lru/cn/tv/player/SimplePlayerFragmentEx$SubscribeListener;

    .line 440
    return-void
.end method

.method protected setTelecast(J)V
    .locals 3
    .param p1, "telecastId"    # J

    .prologue
    .line 335
    invoke-virtual {p0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->getCurrentTelecastId()J

    move-result-wide v0

    cmp-long v0, p1, v0

    if-eqz v0, :cond_1

    .line 337
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->nextProgramView:Lru/cn/tv/player/controller/TouchNextProgramView;

    invoke-virtual {v0}, Lru/cn/tv/player/controller/TouchNextProgramView;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 338
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->nextProgramView:Lru/cn/tv/player/controller/TouchNextProgramView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lru/cn/tv/player/controller/TouchNextProgramView;->setVisibility(I)V

    .line 339
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->playerController:Lru/cn/tv/player/controller/TouchPlayerController;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lru/cn/tv/player/controller/TouchPlayerController;->setTouchesEnabled(Z)V

    .line 344
    :cond_0
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->snapshotImageView:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 345
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->snapshotImageView:Landroid/widget/ImageView;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 346
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->snapshotTextView:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 348
    invoke-direct {p0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->hideFavoriteAndShareMenuItems()V

    .line 351
    :cond_1
    invoke-virtual {p0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->hideLockOverlay()V

    .line 352
    invoke-direct {p0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->hideSnapshot()V

    .line 354
    invoke-super {p0, p1, p2}, Lru/cn/tv/player/SimplePlayerFragment;->setTelecast(J)V

    .line 355
    return-void
.end method

.method showController()V
    .locals 2

    .prologue
    .line 468
    iget-boolean v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->mustHideControls:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->playingAd:Z

    if-nez v0, :cond_0

    .line 469
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->playerController:Lru/cn/tv/player/controller/TouchPlayerController;

    invoke-virtual {v0}, Lru/cn/tv/player/controller/TouchPlayerController;->show()V

    .line 471
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->nextProgramView:Lru/cn/tv/player/controller/TouchNextProgramView;

    invoke-virtual {v0}, Lru/cn/tv/player/controller/TouchNextProgramView;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 472
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->nextProgramView:Lru/cn/tv/player/controller/TouchNextProgramView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lru/cn/tv/player/controller/TouchNextProgramView;->setVisibility(I)V

    .line 473
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->playerController:Lru/cn/tv/player/controller/TouchPlayerController;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lru/cn/tv/player/controller/TouchPlayerController;->setTouchesEnabled(Z)V

    .line 476
    :cond_0
    return-void
.end method

.method protected showPlayerControlIfMaximize()V
    .locals 1

    .prologue
    .line 432
    invoke-super {p0}, Lru/cn/tv/player/SimplePlayerFragment;->showPlayerControlIfMaximize()V

    .line 433
    iget-boolean v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->mustHideControls:Z

    if-nez v0, :cond_0

    .line 434
    iget-object v0, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->playerController:Lru/cn/tv/player/controller/TouchPlayerController;

    invoke-virtual {v0}, Lru/cn/tv/player/controller/TouchPlayerController;->show()V

    .line 436
    :cond_0
    return-void
.end method

.method protected telecastInfoLoaded(Lru/cn/api/tv/replies/Telecast;)V
    .locals 2
    .param p1, "telecast"    # Lru/cn/api/tv/replies/Telecast;

    .prologue
    .line 245
    invoke-super {p0, p1}, Lru/cn/tv/player/SimplePlayerFragment;->telecastInfoLoaded(Lru/cn/api/tv/replies/Telecast;)V

    .line 248
    invoke-direct {p0, p1}, Lru/cn/tv/player/SimplePlayerFragmentEx;->setupShareProvider(Lru/cn/api/tv/replies/Telecast;)V

    .line 250
    sget-object v1, Lru/cn/api/tv/replies/TelecastImage$Profile;->MAIN:Lru/cn/api/tv/replies/TelecastImage$Profile;

    invoke-virtual {p1, v1}, Lru/cn/api/tv/replies/Telecast;->getImage(Lru/cn/api/tv/replies/TelecastImage$Profile;)Lru/cn/api/tv/replies/TelecastImage;

    move-result-object v0

    .line 251
    .local v0, "telecastImage":Lru/cn/api/tv/replies/TelecastImage;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lru/cn/api/tv/replies/TelecastImage;->location:Ljava/lang/String;

    :goto_0
    iput-object v1, p0, Lru/cn/tv/player/SimplePlayerFragmentEx;->snapshotUrl:Ljava/lang/String;

    .line 253
    invoke-direct {p0}, Lru/cn/tv/player/SimplePlayerFragmentEx;->updateSnapshotImage()V

    .line 254
    return-void

    .line 251
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
