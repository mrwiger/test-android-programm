.class public abstract Lcom/my/target/ak;
.super Ljava/lang/Object;
.source "AdSection.java"


# instance fields
.field private advertisingLabel:Ljava/lang/String;

.field private final bP:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/my/target/aq;",
            ">;"
        }
    .end annotation
.end field

.field protected clickArea:Lcom/my/target/af;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/my/target/ak;->bP:Ljava/util/ArrayList;

    .line 20
    sget-object v0, Lcom/my/target/af;->cd:Lcom/my/target/af;

    iput-object v0, p0, Lcom/my/target/ak;->clickArea:Lcom/my/target/af;

    .line 21
    const-string v0, ""

    iput-object v0, p0, Lcom/my/target/ak;->advertisingLabel:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public F()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/my/target/aq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 52
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/my/target/ak;->bP:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public e(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/my/target/aq;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 47
    iget-object v0, p0, Lcom/my/target/ak;->bP:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 48
    return-void
.end method

.method public getAdvertisingLabel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/my/target/ak;->advertisingLabel:Ljava/lang/String;

    return-object v0
.end method

.method public abstract getBannersCount()I
.end method

.method public getClickArea()Lcom/my/target/af;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/my/target/ak;->clickArea:Lcom/my/target/af;

    return-object v0
.end method

.method public p(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/my/target/aq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 57
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 58
    iget-object v0, p0, Lcom/my/target/ak;->bP:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/aq;

    .line 60
    invoke-virtual {v0}, Lcom/my/target/aq;->getType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 62
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 65
    :cond_1
    return-object v1
.end method

.method public setAdvertisingLabel(Ljava/lang/String;)V
    .locals 0
    .param p1, "advertisingLabel"    # Ljava/lang/String;

    .prologue
    .line 25
    iput-object p1, p0, Lcom/my/target/ak;->advertisingLabel:Ljava/lang/String;

    .line 26
    return-void
.end method

.method public setClickArea(Lcom/my/target/af;)V
    .locals 0
    .param p1, "clickArea"    # Lcom/my/target/af;

    .prologue
    .line 37
    iput-object p1, p0, Lcom/my/target/ak;->clickArea:Lcom/my/target/af;

    .line 38
    return-void
.end method
