.class Lru/cn/tv/mobile/live/LiveNowViewModel;
.super Lru/cn/mvvm/RxViewModel;
.source "LiveNowViewModel.java"


# instance fields
.field private channelsIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private loadedItemsCount:I

.field private final loader:Lru/cn/mvvm/RxLoader;

.field private final telecasts:Landroid/arch/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/arch/lifecycle/MutableLiveData",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lru/cn/mvvm/RxLoader;)V
    .locals 1
    .param p1, "loader"    # Lru/cn/mvvm/RxLoader;

    .prologue
    .line 31
    invoke-direct {p0}, Lru/cn/mvvm/RxViewModel;-><init>()V

    .line 25
    const/16 v0, 0x14

    iput v0, p0, Lru/cn/tv/mobile/live/LiveNowViewModel;->loadedItemsCount:I

    .line 26
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lru/cn/tv/mobile/live/LiveNowViewModel;->channelsIds:Ljava/util/List;

    .line 28
    new-instance v0, Landroid/arch/lifecycle/MutableLiveData;

    invoke-direct {v0}, Landroid/arch/lifecycle/MutableLiveData;-><init>()V

    iput-object v0, p0, Lru/cn/tv/mobile/live/LiveNowViewModel;->telecasts:Landroid/arch/lifecycle/MutableLiveData;

    .line 32
    iput-object p1, p0, Lru/cn/tv/mobile/live/LiveNowViewModel;->loader:Lru/cn/mvvm/RxLoader;

    .line 33
    return-void
.end method

.method private channels()Lio/reactivex/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 77
    invoke-static {}, Lru/cn/api/provider/TvContentProviderContract;->channels()Landroid/net/Uri;

    move-result-object v0

    .line 78
    .local v0, "uri":Landroid/net/Uri;
    iget-object v1, p0, Lru/cn/tv/mobile/live/LiveNowViewModel;->loader:Lru/cn/mvvm/RxLoader;

    invoke-virtual {v1, v0}, Lru/cn/mvvm/RxLoader;->query(Landroid/net/Uri;)Lio/reactivex/Observable;

    move-result-object v1

    .line 79
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    new-instance v2, Lru/cn/tv/mobile/live/LiveNowViewModel$$Lambda$5;

    invoke-direct {v2, p0}, Lru/cn/tv/mobile/live/LiveNowViewModel$$Lambda$5;-><init>(Lru/cn/tv/mobile/live/LiveNowViewModel;)V

    .line 80
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v1

    .line 78
    return-object v1
.end method

.method private currentTelecasts()Lio/reactivex/Observable;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 115
    invoke-static {}, Lru/cn/api/provider/TvContentProviderContract;->currentTelecasts()Landroid/net/Uri;

    move-result-object v3

    .line 116
    .local v3, "uri":Landroid/net/Uri;
    iget-object v4, p0, Lru/cn/tv/mobile/live/LiveNowViewModel;->channelsIds:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    iget v5, p0, Lru/cn/tv/mobile/live/LiveNowViewModel;->loadedItemsCount:I

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 117
    .local v1, "count":I
    new-array v0, v1, [Ljava/lang/String;

    .line 118
    .local v0, "arguments":[Ljava/lang/String;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_0

    .line 119
    iget-object v4, p0, Lru/cn/tv/mobile/live/LiveNowViewModel;->channelsIds:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    aput-object v4, v0, v2

    .line 118
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 122
    :cond_0
    iget-object v4, p0, Lru/cn/tv/mobile/live/LiveNowViewModel;->loader:Lru/cn/mvvm/RxLoader;

    const/4 v5, 0x0

    invoke-virtual {v4, v3, v5, v0}, Lru/cn/mvvm/RxLoader;->query(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object v4

    .line 123
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v5

    invoke-virtual {v4, v5}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v4

    new-instance v5, Lru/cn/tv/mobile/live/LiveNowViewModel$$Lambda$6;

    invoke-direct {v5, p0}, Lru/cn/tv/mobile/live/LiveNowViewModel$$Lambda$6;-><init>(Lru/cn/tv/mobile/live/LiveNowViewModel;)V

    .line 124
    invoke-virtual {v4, v5}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v4

    .line 122
    return-object v4
.end method

.method private mapChannels(Landroid/database/Cursor;)Ljava/util/List;
    .locals 12
    .param p1, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v11, 0x1

    .line 84
    const/4 v7, 0x0

    .line 85
    .local v7, "favouritePosition":I
    const-string v10, "has_schedule"

    invoke-interface {p1, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    .line 86
    .local v9, "hasScheduleIndex":I
    const-string v10, "cn_id"

    invoke-interface {p1, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    .line 87
    .local v4, "cn_idIndex":I
    const-string v10, "favourite"

    invoke-interface {p1, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    .line 89
    .local v5, "favoriteIndex":I
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 91
    .local v0, "channelIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 92
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v10

    if-nez v10, :cond_2

    .line 94
    invoke-interface {p1, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 95
    .local v8, "hasSchedule":I
    if-ne v8, v11, :cond_0

    .line 96
    invoke-interface {p1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 97
    .local v2, "cnId":J
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    .line 99
    .local v1, "cnIdString":Ljava/lang/String;
    invoke-interface {p1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 100
    .local v6, "favourite":I
    if-ne v6, v11, :cond_1

    .line 101
    invoke-interface {v0, v7, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 102
    add-int/lit8 v7, v7, 0x1

    .line 108
    .end local v1    # "cnIdString":Ljava/lang/String;
    .end local v2    # "cnId":J
    .end local v6    # "favourite":I
    :cond_0
    :goto_1
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_0

    .line 104
    .restart local v1    # "cnIdString":Ljava/lang/String;
    .restart local v2    # "cnId":J
    .restart local v6    # "favourite":I
    :cond_1
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 111
    .end local v1    # "cnIdString":Ljava/lang/String;
    .end local v2    # "cnId":J
    .end local v6    # "favourite":I
    .end local v8    # "hasSchedule":I
    :cond_2
    return-object v0
.end method


# virtual methods
.method final bridge synthetic bridge$lambda$0$LiveNowViewModel(Landroid/database/Cursor;)Ljava/util/List;
    .locals 1

    invoke-direct {p0, p1}, Lru/cn/tv/mobile/live/LiveNowViewModel;->mapChannels(Landroid/database/Cursor;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method final synthetic lambda$currentTelecasts$5$LiveNowViewModel(Landroid/database/Cursor;)V
    .locals 6
    .param p1, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 126
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 127
    const/4 v1, 0x0

    .line 128
    .local v1, "needRestart":Z
    iget v2, p0, Lru/cn/tv/mobile/live/LiveNowViewModel;->loadedItemsCount:I

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-eq v2, v3, :cond_0

    .line 129
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    iput v2, p0, Lru/cn/tv/mobile/live/LiveNowViewModel;->loadedItemsCount:I

    .line 135
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lru/cn/tv/mobile/live/LiveNowViewModel;->loadedItemsCount:I

    if-eq v0, v2, :cond_2

    .line 136
    iget-object v2, p0, Lru/cn/tv/mobile/live/LiveNowViewModel;->channelsIds:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v3, "channel_id"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 137
    iget-object v2, p0, Lru/cn/tv/mobile/live/LiveNowViewModel;->channelsIds:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 138
    const/4 v1, 0x1

    .line 141
    :cond_1
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    .line 135
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 144
    :cond_2
    if-eqz v1, :cond_3

    .line 145
    invoke-virtual {p0}, Lru/cn/tv/mobile/live/LiveNowViewModel;->reload()V

    .line 147
    :cond_3
    return-void
.end method

.method final synthetic lambda$load$0$LiveNowViewModel(Ljava/util/List;)V
    .locals 0
    .param p1, "it"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 43
    iput-object p1, p0, Lru/cn/tv/mobile/live/LiveNowViewModel;->channelsIds:Ljava/util/List;

    return-void
.end method

.method final synthetic lambda$load$1$LiveNowViewModel(Ljava/util/List;)Lio/reactivex/ObservableSource;
    .locals 1
    .param p1, "it"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 44
    invoke-direct {p0}, Lru/cn/tv/mobile/live/LiveNowViewModel;->currentTelecasts()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method final synthetic lambda$load$2$LiveNowViewModel(Landroid/database/Cursor;)V
    .locals 1
    .param p1, "it"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 46
    iget-object v0, p0, Lru/cn/tv/mobile/live/LiveNowViewModel;->telecasts:Landroid/arch/lifecycle/MutableLiveData;

    invoke-virtual {v0, p1}, Landroid/arch/lifecycle/MutableLiveData;->setValue(Ljava/lang/Object;)V

    return-void
.end method

.method final synthetic lambda$load$3$LiveNowViewModel(Ljava/lang/Throwable;)V
    .locals 2
    .param p1, "throwable"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 47
    iget-object v0, p0, Lru/cn/tv/mobile/live/LiveNowViewModel;->telecasts:Landroid/arch/lifecycle/MutableLiveData;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/arch/lifecycle/MutableLiveData;->setValue(Ljava/lang/Object;)V

    return-void
.end method

.method final synthetic lambda$reload$4$LiveNowViewModel(Landroid/database/Cursor;)V
    .locals 1
    .param p1, "it"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 56
    iget-object v0, p0, Lru/cn/tv/mobile/live/LiveNowViewModel;->telecasts:Landroid/arch/lifecycle/MutableLiveData;

    invoke-virtual {v0, p1}, Landroid/arch/lifecycle/MutableLiveData;->setValue(Ljava/lang/Object;)V

    return-void
.end method

.method public load()V
    .locals 4

    .prologue
    .line 40
    invoke-virtual {p0}, Lru/cn/tv/mobile/live/LiveNowViewModel;->unbindAll()V

    .line 42
    invoke-direct {p0}, Lru/cn/tv/mobile/live/LiveNowViewModel;->channels()Lio/reactivex/Observable;

    move-result-object v1

    new-instance v2, Lru/cn/tv/mobile/live/LiveNowViewModel$$Lambda$0;

    invoke-direct {v2, p0}, Lru/cn/tv/mobile/live/LiveNowViewModel$$Lambda$0;-><init>(Lru/cn/tv/mobile/live/LiveNowViewModel;)V

    .line 43
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v1

    new-instance v2, Lru/cn/tv/mobile/live/LiveNowViewModel$$Lambda$1;

    invoke-direct {v2, p0}, Lru/cn/tv/mobile/live/LiveNowViewModel$$Lambda$1;-><init>(Lru/cn/tv/mobile/live/LiveNowViewModel;)V

    .line 44
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->switchMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v1

    .line 45
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    new-instance v2, Lru/cn/tv/mobile/live/LiveNowViewModel$$Lambda$2;

    invoke-direct {v2, p0}, Lru/cn/tv/mobile/live/LiveNowViewModel$$Lambda$2;-><init>(Lru/cn/tv/mobile/live/LiveNowViewModel;)V

    new-instance v3, Lru/cn/tv/mobile/live/LiveNowViewModel$$Lambda$3;

    invoke-direct {v3, p0}, Lru/cn/tv/mobile/live/LiveNowViewModel$$Lambda$3;-><init>(Lru/cn/tv/mobile/live/LiveNowViewModel;)V

    .line 46
    invoke-virtual {v1, v2, v3}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 49
    .local v0, "disposable":Lio/reactivex/disposables/Disposable;
    invoke-virtual {p0, v0}, Lru/cn/tv/mobile/live/LiveNowViewModel;->bind(Lio/reactivex/disposables/Disposable;)V

    .line 50
    return-void
.end method

.method public prefetch()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 62
    iget v3, p0, Lru/cn/tv/mobile/live/LiveNowViewModel;->loadedItemsCount:I

    iget-object v4, p0, Lru/cn/tv/mobile/live/LiveNowViewModel;->channelsIds:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v3, v4, :cond_0

    move v0, v2

    .line 63
    .local v0, "canPrefetch":Z
    :goto_0
    if-nez v0, :cond_1

    .line 73
    :goto_1
    return v1

    .end local v0    # "canPrefetch":Z
    :cond_0
    move v0, v1

    .line 62
    goto :goto_0

    .line 66
    .restart local v0    # "canPrefetch":Z
    :cond_1
    iget v1, p0, Lru/cn/tv/mobile/live/LiveNowViewModel;->loadedItemsCount:I

    add-int/lit8 v1, v1, 0x14

    iput v1, p0, Lru/cn/tv/mobile/live/LiveNowViewModel;->loadedItemsCount:I

    .line 67
    iget v1, p0, Lru/cn/tv/mobile/live/LiveNowViewModel;->loadedItemsCount:I

    iget-object v3, p0, Lru/cn/tv/mobile/live/LiveNowViewModel;->channelsIds:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-le v1, v3, :cond_2

    .line 68
    iget-object v1, p0, Lru/cn/tv/mobile/live/LiveNowViewModel;->channelsIds:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    iput v1, p0, Lru/cn/tv/mobile/live/LiveNowViewModel;->loadedItemsCount:I

    .line 71
    :cond_2
    invoke-virtual {p0}, Lru/cn/tv/mobile/live/LiveNowViewModel;->reload()V

    move v1, v2

    .line 73
    goto :goto_1
.end method

.method public reload()V
    .locals 3

    .prologue
    .line 53
    invoke-direct {p0}, Lru/cn/tv/mobile/live/LiveNowViewModel;->currentTelecasts()Lio/reactivex/Observable;

    move-result-object v1

    .line 54
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    .line 55
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    new-instance v2, Lru/cn/tv/mobile/live/LiveNowViewModel$$Lambda$4;

    invoke-direct {v2, p0}, Lru/cn/tv/mobile/live/LiveNowViewModel$$Lambda$4;-><init>(Lru/cn/tv/mobile/live/LiveNowViewModel;)V

    .line 56
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 58
    .local v0, "disposable":Lio/reactivex/disposables/Disposable;
    invoke-virtual {p0, v0}, Lru/cn/tv/mobile/live/LiveNowViewModel;->bind(Lio/reactivex/disposables/Disposable;)V

    .line 59
    return-void
.end method

.method public telecasts()Landroid/arch/lifecycle/LiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/arch/lifecycle/LiveData",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 36
    iget-object v0, p0, Lru/cn/tv/mobile/live/LiveNowViewModel;->telecasts:Landroid/arch/lifecycle/MutableLiveData;

    return-object v0
.end method
