.class public Lcom/samsung/multiscreen/Search;
.super Ljava/lang/Object;
.source "Search.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/multiscreen/Search$SearchListener;,
        Lcom/samsung/multiscreen/Search$OnBleFoundListener;,
        Lcom/samsung/multiscreen/Search$OnServiceLostListener;,
        Lcom/samsung/multiscreen/Search$OnServiceFoundListener;,
        Lcom/samsung/multiscreen/Search$OnStopListener;,
        Lcom/samsung/multiscreen/Search$OnStartListener;
    }
.end annotation


# static fields
.field private static final SERVICE_CHECK_TIMEOUT:I = 0x7d0

.field private static final TAG:Ljava/lang/String; = "Search"

.field private static instance:Lcom/samsung/multiscreen/Search;


# instance fields
.field private bleProvider:Lcom/samsung/multiscreen/SearchProvider;

.field private clearProviders:Z

.field private final context:Landroid/content/Context;

.field private mStandbyDeviceList:Lcom/samsung/multiscreen/StandbyDeviceList;

.field private numRunning:I

.field private volatile onBleFoundListener:Lcom/samsung/multiscreen/Search$OnBleFoundListener;

.field private volatile onServiceFoundListener:Lcom/samsung/multiscreen/Search$OnServiceFoundListener;

.field private volatile onServiceLostListener:Lcom/samsung/multiscreen/Search$OnServiceLostListener;

.field private volatile onStartListener:Lcom/samsung/multiscreen/Search$OnStartListener;

.field private onStartNotified:I

.field private volatile onStopListener:Lcom/samsung/multiscreen/Search$OnStopListener;

.field private final providers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/multiscreen/SearchProvider;",
            ">;"
        }
    .end annotation
.end field

.field private final removedProviders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/multiscreen/SearchProvider;",
            ">;"
        }
    .end annotation
.end field

.field private final searchListener:Lcom/samsung/multiscreen/Search$SearchListener;

.field private services:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/multiscreen/Service;",
            ">;"
        }
    .end annotation
.end field

.field private startingBle:Z

.field private stoppingBle:Z


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 197
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/multiscreen/Search;->providers:Ljava/util/List;

    .line 56
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/multiscreen/Search;->removedProviders:Ljava/util/List;

    .line 57
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/multiscreen/Search;->bleProvider:Lcom/samsung/multiscreen/SearchProvider;

    .line 59
    iput-boolean v1, p0, Lcom/samsung/multiscreen/Search;->startingBle:Z

    .line 60
    iput-boolean v1, p0, Lcom/samsung/multiscreen/Search;->stoppingBle:Z

    .line 68
    new-instance v0, Lcom/samsung/multiscreen/Search$1;

    invoke-direct {v0, p0}, Lcom/samsung/multiscreen/Search$1;-><init>(Lcom/samsung/multiscreen/Search;)V

    iput-object v0, p0, Lcom/samsung/multiscreen/Search;->searchListener:Lcom/samsung/multiscreen/Search$SearchListener;

    .line 154
    iput-boolean v1, p0, Lcom/samsung/multiscreen/Search;->clearProviders:Z

    .line 156
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/multiscreen/Search;->services:Ljava/util/List;

    .line 198
    iput-object p1, p0, Lcom/samsung/multiscreen/Search;->context:Landroid/content/Context;

    .line 199
    return-void
.end method

.method static synthetic access$006(Lcom/samsung/multiscreen/Search;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/multiscreen/Search;

    .prologue
    .line 43
    iget v0, p0, Lcom/samsung/multiscreen/Search;->onStartNotified:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/samsung/multiscreen/Search;->onStartNotified:I

    return v0
.end method

.method static synthetic access$100(Lcom/samsung/multiscreen/Search;)Lcom/samsung/multiscreen/Search$OnStartListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/multiscreen/Search;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/multiscreen/Search;->onStartListener:Lcom/samsung/multiscreen/Search$OnStartListener;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/samsung/multiscreen/Search;Lcom/samsung/multiscreen/Service;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/multiscreen/Search;
    .param p1, "x1"    # Lcom/samsung/multiscreen/Service;

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/samsung/multiscreen/Search;->validateService(Lcom/samsung/multiscreen/Service;)V

    return-void
.end method

.method static synthetic access$1100(Lcom/samsung/multiscreen/Search;)Lcom/samsung/multiscreen/Search$OnBleFoundListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/multiscreen/Search;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/multiscreen/Search;->onBleFoundListener:Lcom/samsung/multiscreen/Search$OnBleFoundListener;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/samsung/multiscreen/Search;)Lcom/samsung/multiscreen/Search$SearchListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/multiscreen/Search;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/multiscreen/Search;->searchListener:Lcom/samsung/multiscreen/Search$SearchListener;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/samsung/multiscreen/Search;)Lcom/samsung/multiscreen/SearchProvider;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/multiscreen/Search;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/multiscreen/Search;->bleProvider:Lcom/samsung/multiscreen/SearchProvider;

    return-object v0
.end method

.method static synthetic access$206(Lcom/samsung/multiscreen/Search;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/multiscreen/Search;

    .prologue
    .line 43
    iget v0, p0, Lcom/samsung/multiscreen/Search;->numRunning:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/samsung/multiscreen/Search;->numRunning:I

    return v0
.end method

.method static synthetic access$300(Lcom/samsung/multiscreen/Search;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/multiscreen/Search;

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/samsung/multiscreen/Search;->clearProviders:Z

    return v0
.end method

.method static synthetic access$400(Lcom/samsung/multiscreen/Search;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/multiscreen/Search;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/samsung/multiscreen/Search;->processRemovedProviders()V

    return-void
.end method

.method static synthetic access$500(Lcom/samsung/multiscreen/Search;)Lcom/samsung/multiscreen/Search$OnStopListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/multiscreen/Search;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/multiscreen/Search;->onStopListener:Lcom/samsung/multiscreen/Search$OnStopListener;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/multiscreen/Search;Lcom/samsung/multiscreen/Service;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/multiscreen/Search;
    .param p1, "x1"    # Lcom/samsung/multiscreen/Service;

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/samsung/multiscreen/Search;->addService(Lcom/samsung/multiscreen/Service;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$700(Lcom/samsung/multiscreen/Search;)Lcom/samsung/multiscreen/StandbyDeviceList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/multiscreen/Search;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/multiscreen/Search;->mStandbyDeviceList:Lcom/samsung/multiscreen/StandbyDeviceList;

    return-object v0
.end method

.method static synthetic access$800(Lcom/samsung/multiscreen/Search;)Lcom/samsung/multiscreen/Search$OnServiceLostListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/multiscreen/Search;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/multiscreen/Search;->onServiceLostListener:Lcom/samsung/multiscreen/Search$OnServiceLostListener;

    return-object v0
.end method

.method static synthetic access$900(Lcom/samsung/multiscreen/Search;)Lcom/samsung/multiscreen/Search$OnServiceFoundListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/multiscreen/Search;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/multiscreen/Search;->onServiceFoundListener:Lcom/samsung/multiscreen/Search$OnServiceFoundListener;

    return-object v0
.end method

.method private addService(Lcom/samsung/multiscreen/Service;)Z
    .locals 6
    .param p1, "service"    # Lcom/samsung/multiscreen/Service;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 473
    if-nez p1, :cond_0

    move v2, v3

    .line 491
    :goto_0
    return v2

    .line 477
    :cond_0
    iget-object v5, p0, Lcom/samsung/multiscreen/Search;->services:Ljava/util/List;

    monitor-enter v5

    .line 478
    const/4 v2, 0x0

    :try_start_0
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 479
    .local v0, "found":Ljava/lang/Boolean;
    const/4 v1, 0x0

    .local v1, "itr":I
    :goto_1
    iget-object v2, p0, Lcom/samsung/multiscreen/Search;->services:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 480
    iget-object v2, p0, Lcom/samsung/multiscreen/Search;->services:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/multiscreen/Service;

    invoke-virtual {v2, p1}, Lcom/samsung/multiscreen/Service;->isEqualTo(Lcom/samsung/multiscreen/Service;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 481
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 485
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_3

    .line 486
    iget-object v2, p0, Lcom/samsung/multiscreen/Search;->services:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 487
    monitor-exit v5

    move v2, v4

    goto :goto_0

    .line 479
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 489
    :cond_3
    monitor-exit v5

    move v2, v3

    .line 491
    goto :goto_0

    .line 489
    .end local v0    # "found":Ljava/lang/Boolean;
    .end local v1    # "itr":I
    :catchall_0
    move-exception v2

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method static getInstance(Landroid/content/Context;)Lcom/samsung/multiscreen/Search;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 191
    sget-object v0, Lcom/samsung/multiscreen/Search;->instance:Lcom/samsung/multiscreen/Search;

    if-nez v0, :cond_0

    .line 192
    new-instance v0, Lcom/samsung/multiscreen/Search;

    invoke-direct {v0, p0}, Lcom/samsung/multiscreen/Search;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/multiscreen/Search;->instance:Lcom/samsung/multiscreen/Search;

    .line 194
    :cond_0
    sget-object v0, Lcom/samsung/multiscreen/Search;->instance:Lcom/samsung/multiscreen/Search;

    return-object v0
.end method

.method private declared-synchronized processRemovedProviders()V
    .locals 3

    .prologue
    .line 373
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/samsung/multiscreen/Search;->removedProviders:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 374
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/samsung/multiscreen/Search;->removedProviders:Ljava/util/List;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/multiscreen/SearchProvider;

    .line 375
    .local v0, "provider":Lcom/samsung/multiscreen/SearchProvider;
    invoke-virtual {v0}, Lcom/samsung/multiscreen/SearchProvider;->isSearching()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/samsung/multiscreen/Search;->providers:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 376
    iget-object v2, p0, Lcom/samsung/multiscreen/Search;->removedProviders:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 373
    .end local v0    # "provider":Lcom/samsung/multiscreen/SearchProvider;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 380
    :cond_1
    monitor-exit p0

    return-void
.end method

.method private removeAndNotify(Lcom/samsung/multiscreen/Service;)V
    .locals 1
    .param p1, "service"    # Lcom/samsung/multiscreen/Service;

    .prologue
    .line 512
    invoke-direct {p0, p1}, Lcom/samsung/multiscreen/Search;->removeService(Lcom/samsung/multiscreen/Service;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 513
    iget-object v0, p0, Lcom/samsung/multiscreen/Search;->onServiceLostListener:Lcom/samsung/multiscreen/Search$OnServiceLostListener;

    if-eqz v0, :cond_0

    .line 514
    new-instance v0, Lcom/samsung/multiscreen/Search$6;

    invoke-direct {v0, p0, p1}, Lcom/samsung/multiscreen/Search$6;-><init>(Lcom/samsung/multiscreen/Search;Lcom/samsung/multiscreen/Service;)V

    invoke-static {v0}, Lcom/samsung/multiscreen/util/RunUtil;->runOnUI(Ljava/lang/Runnable;)V

    .line 530
    :cond_0
    return-void
.end method

.method private removeService(Lcom/samsung/multiscreen/Service;)Z
    .locals 4
    .param p1, "service"    # Lcom/samsung/multiscreen/Service;

    .prologue
    const/4 v2, 0x0

    .line 495
    if-nez p1, :cond_0

    move v1, v2

    .line 508
    :goto_0
    return v1

    .line 499
    :cond_0
    iget-object v3, p0, Lcom/samsung/multiscreen/Search;->services:Ljava/util/List;

    monitor-enter v3

    .line 501
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    :try_start_0
    iget-object v1, p0, Lcom/samsung/multiscreen/Search;->services:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 502
    iget-object v1, p0, Lcom/samsung/multiscreen/Search;->services:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/multiscreen/Service;

    invoke-virtual {v1, p1}, Lcom/samsung/multiscreen/Service;->isEqualTo(Lcom/samsung/multiscreen/Service;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 503
    iget-object v1, p0, Lcom/samsung/multiscreen/Search;->services:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 504
    const/4 v1, 0x1

    monitor-exit v3

    goto :goto_0

    .line 507
    :catchall_0
    move-exception v1

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 501
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 507
    :cond_2
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v1, v2

    .line 508
    goto :goto_0
.end method

.method private startDiscovery()V
    .locals 4

    .prologue
    .line 396
    iget-object v1, p0, Lcom/samsung/multiscreen/Search;->providers:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 397
    const-string v1, "Search"

    const-string v2, "No search providers specified. Adding default providers..."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 398
    iget-object v1, p0, Lcom/samsung/multiscreen/Search;->providers:Ljava/util/List;

    iget-object v2, p0, Lcom/samsung/multiscreen/Search;->context:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/multiscreen/Search;->searchListener:Lcom/samsung/multiscreen/Search$SearchListener;

    invoke-static {v2, v3}, Lcom/samsung/multiscreen/MDNSSearchProvider;->create(Landroid/content/Context;Lcom/samsung/multiscreen/Search$SearchListener;)Lcom/samsung/multiscreen/SearchProvider;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 399
    iget-object v1, p0, Lcom/samsung/multiscreen/Search;->providers:Ljava/util/List;

    iget-object v2, p0, Lcom/samsung/multiscreen/Search;->context:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/multiscreen/Search;->searchListener:Lcom/samsung/multiscreen/Search$SearchListener;

    invoke-static {v2, v3}, Lcom/samsung/multiscreen/MSFDSearchProvider;->create(Landroid/content/Context;Lcom/samsung/multiscreen/Search$SearchListener;)Lcom/samsung/multiscreen/SearchProvider;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 402
    :cond_0
    iget-object v1, p0, Lcom/samsung/multiscreen/Search;->services:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 404
    iget-object v1, p0, Lcom/samsung/multiscreen/Search;->providers:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    iput v1, p0, Lcom/samsung/multiscreen/Search;->numRunning:I

    iput v1, p0, Lcom/samsung/multiscreen/Search;->onStartNotified:I

    .line 405
    iget-object v1, p0, Lcom/samsung/multiscreen/Search;->providers:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/multiscreen/SearchProvider;

    .line 406
    .local v0, "provider":Lcom/samsung/multiscreen/SearchProvider;
    new-instance v2, Lcom/samsung/multiscreen/Search$2;

    invoke-direct {v2, p0, v0}, Lcom/samsung/multiscreen/Search$2;-><init>(Lcom/samsung/multiscreen/Search;Lcom/samsung/multiscreen/SearchProvider;)V

    invoke-static {v2}, Lcom/samsung/multiscreen/util/RunUtil;->runInBackground(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 418
    .end local v0    # "provider":Lcom/samsung/multiscreen/SearchProvider;
    :cond_1
    return-void
.end method

.method private startDiscoveryUsingBle()V
    .locals 2

    .prologue
    .line 441
    iget-object v0, p0, Lcom/samsung/multiscreen/Search;->bleProvider:Lcom/samsung/multiscreen/SearchProvider;

    if-nez v0, :cond_0

    .line 442
    iget-object v0, p0, Lcom/samsung/multiscreen/Search;->context:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/multiscreen/Search;->searchListener:Lcom/samsung/multiscreen/Search$SearchListener;

    invoke-static {v0, v1}, Lcom/samsung/multiscreen/BLESearchProvider;->create(Landroid/content/Context;Lcom/samsung/multiscreen/Search$SearchListener;)Lcom/samsung/multiscreen/SearchProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/multiscreen/Search;->bleProvider:Lcom/samsung/multiscreen/SearchProvider;

    .line 444
    :cond_0
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/multiscreen/Search;->numRunning:I

    iput v0, p0, Lcom/samsung/multiscreen/Search;->onStartNotified:I

    .line 445
    new-instance v0, Lcom/samsung/multiscreen/Search$4;

    invoke-direct {v0, p0}, Lcom/samsung/multiscreen/Search$4;-><init>(Lcom/samsung/multiscreen/Search;)V

    invoke-static {v0}, Lcom/samsung/multiscreen/util/RunUtil;->runInBackground(Ljava/lang/Runnable;)V

    .line 456
    return-void
.end method

.method private stopDiscovery()V
    .locals 4

    .prologue
    .line 421
    iget-object v2, p0, Lcom/samsung/multiscreen/Search;->providers:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/multiscreen/SearchProvider;

    .line 422
    .local v0, "provider":Lcom/samsung/multiscreen/SearchProvider;
    new-instance v1, Lcom/samsung/multiscreen/Search$3;

    invoke-direct {v1, p0, v0}, Lcom/samsung/multiscreen/Search$3;-><init>(Lcom/samsung/multiscreen/Search;Lcom/samsung/multiscreen/SearchProvider;)V

    .line 435
    .local v1, "runnable":Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/samsung/multiscreen/util/RunUtil;->runInBackground(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 437
    .end local v0    # "provider":Lcom/samsung/multiscreen/SearchProvider;
    .end local v1    # "runnable":Ljava/lang/Runnable;
    :cond_0
    return-void
.end method

.method private stopDiscoveryUsingBle()V
    .locals 1

    .prologue
    .line 459
    new-instance v0, Lcom/samsung/multiscreen/Search$5;

    invoke-direct {v0, p0}, Lcom/samsung/multiscreen/Search$5;-><init>(Lcom/samsung/multiscreen/Search;)V

    .line 469
    .local v0, "runnable":Ljava/lang/Runnable;
    invoke-static {v0}, Lcom/samsung/multiscreen/util/RunUtil;->runInBackground(Ljava/lang/Runnable;)V

    .line 470
    return-void
.end method

.method private validateService(Lcom/samsung/multiscreen/Service;)V
    .locals 3
    .param p1, "service"    # Lcom/samsung/multiscreen/Service;

    .prologue
    .line 533
    invoke-direct {p0, p1}, Lcom/samsung/multiscreen/Search;->removeAndNotify(Lcom/samsung/multiscreen/Service;)V

    .line 535
    iget-object v1, p0, Lcom/samsung/multiscreen/Search;->mStandbyDeviceList:Lcom/samsung/multiscreen/StandbyDeviceList;

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/samsung/multiscreen/Service;->isStandbyService:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_0

    .line 536
    iget-object v1, p0, Lcom/samsung/multiscreen/Search;->providers:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/multiscreen/SearchProvider;

    .line 537
    .local v0, "provider":Lcom/samsung/multiscreen/SearchProvider;
    invoke-virtual {v0, p1}, Lcom/samsung/multiscreen/SearchProvider;->removeService(Lcom/samsung/multiscreen/Service;)V

    goto :goto_0

    .line 540
    .end local v0    # "provider":Lcom/samsung/multiscreen/SearchProvider;
    :cond_0
    return-void
.end method


# virtual methods
.method public addProvider(Lcom/samsung/multiscreen/SearchProvider;)V
    .locals 2
    .param p1, "provider"    # Lcom/samsung/multiscreen/SearchProvider;

    .prologue
    .line 344
    if-nez p1, :cond_0

    .line 345
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 347
    :cond_0
    iget-object v1, p0, Lcom/samsung/multiscreen/Search;->providers:Ljava/util/List;

    monitor-enter v1

    .line 348
    :try_start_0
    iget-object v0, p0, Lcom/samsung/multiscreen/Search;->providers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 349
    iget-object v0, p0, Lcom/samsung/multiscreen/Search;->searchListener:Lcom/samsung/multiscreen/Search$SearchListener;

    invoke-virtual {p1, v0}, Lcom/samsung/multiscreen/SearchProvider;->setSearchListener(Lcom/samsung/multiscreen/Search$SearchListener;)V

    .line 350
    monitor-exit v1

    .line 351
    return-void

    .line 350
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public clearStandbyDeviceList()V
    .locals 1

    .prologue
    .line 277
    iget-object v0, p0, Lcom/samsung/multiscreen/Search;->mStandbyDeviceList:Lcom/samsung/multiscreen/StandbyDeviceList;

    if-eqz v0, :cond_0

    .line 278
    iget-object v0, p0, Lcom/samsung/multiscreen/Search;->mStandbyDeviceList:Lcom/samsung/multiscreen/StandbyDeviceList;

    invoke-virtual {v0}, Lcom/samsung/multiscreen/StandbyDeviceList;->clear()V

    .line 280
    :cond_0
    return-void
.end method

.method public getServices()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/multiscreen/Service;",
            ">;"
        }
    .end annotation

    .prologue
    .line 333
    iget-object v0, p0, Lcom/samsung/multiscreen/Search;->services:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public isSearching()Z
    .locals 3

    .prologue
    .line 207
    iget-object v1, p0, Lcom/samsung/multiscreen/Search;->providers:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/multiscreen/SearchProvider;

    .line 208
    .local v0, "provider":Lcom/samsung/multiscreen/SearchProvider;
    invoke-virtual {v0}, Lcom/samsung/multiscreen/SearchProvider;->isSearching()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 209
    const/4 v1, 0x1

    .line 213
    .end local v0    # "provider":Lcom/samsung/multiscreen/SearchProvider;
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isSearchingBle()Z
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lcom/samsung/multiscreen/Search;->bleProvider:Lcom/samsung/multiscreen/SearchProvider;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/multiscreen/Search;->bleProvider:Lcom/samsung/multiscreen/SearchProvider;

    invoke-virtual {v0}, Lcom/samsung/multiscreen/SearchProvider;->isSearching()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 223
    const/4 v0, 0x1

    .line 225
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSupportBle()Z
    .locals 2

    .prologue
    .line 288
    iget-object v0, p0, Lcom/samsung/multiscreen/Search;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "android.hardware.bluetooth_le"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 289
    const/4 v0, 0x1

    .line 291
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized removeAllProviders()V
    .locals 1

    .prologue
    .line 386
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/samsung/multiscreen/Search;->clearProviders:Z

    .line 387
    invoke-virtual {p0}, Lcom/samsung/multiscreen/Search;->isSearching()Z

    move-result v0

    if-nez v0, :cond_0

    .line 388
    iget-object v0, p0, Lcom/samsung/multiscreen/Search;->providers:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 392
    :goto_0
    monitor-exit p0

    return-void

    .line 390
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/samsung/multiscreen/Search;->clearProviders:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 386
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized removeProvider(Lcom/samsung/multiscreen/SearchProvider;)Z
    .locals 1
    .param p1, "provider"    # Lcom/samsung/multiscreen/SearchProvider;

    .prologue
    .line 359
    monitor-enter p0

    if-nez p1, :cond_0

    .line 360
    :try_start_0
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 359
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 363
    :cond_0
    :try_start_1
    invoke-virtual {p1}, Lcom/samsung/multiscreen/SearchProvider;->isSearching()Z

    move-result v0

    if-nez v0, :cond_1

    .line 364
    iget-object v0, p0, Lcom/samsung/multiscreen/Search;->providers:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    .line 369
    :goto_0
    monitor-exit p0

    return v0

    .line 366
    :cond_1
    :try_start_2
    iget-object v0, p0, Lcom/samsung/multiscreen/Search;->removedProviders:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 369
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setOnBleFoundListener(Lcom/samsung/multiscreen/Search$OnBleFoundListener;)V
    .locals 0
    .param p1, "onBleFoundListener"    # Lcom/samsung/multiscreen/Search$OnBleFoundListener;

    .prologue
    .line 187
    iput-object p1, p0, Lcom/samsung/multiscreen/Search;->onBleFoundListener:Lcom/samsung/multiscreen/Search$OnBleFoundListener;

    return-void
.end method

.method public setOnServiceFoundListener(Lcom/samsung/multiscreen/Search$OnServiceFoundListener;)V
    .locals 0
    .param p1, "onServiceFoundListener"    # Lcom/samsung/multiscreen/Search$OnServiceFoundListener;

    .prologue
    .line 173
    iput-object p1, p0, Lcom/samsung/multiscreen/Search;->onServiceFoundListener:Lcom/samsung/multiscreen/Search$OnServiceFoundListener;

    return-void
.end method

.method public setOnServiceLostListener(Lcom/samsung/multiscreen/Search$OnServiceLostListener;)V
    .locals 0
    .param p1, "onServiceLostListener"    # Lcom/samsung/multiscreen/Search$OnServiceLostListener;

    .prologue
    .line 179
    iput-object p1, p0, Lcom/samsung/multiscreen/Search;->onServiceLostListener:Lcom/samsung/multiscreen/Search$OnServiceLostListener;

    return-void
.end method

.method public setOnStartListener(Lcom/samsung/multiscreen/Search$OnStartListener;)V
    .locals 0
    .param p1, "onStartListener"    # Lcom/samsung/multiscreen/Search$OnStartListener;

    .prologue
    .line 161
    iput-object p1, p0, Lcom/samsung/multiscreen/Search;->onStartListener:Lcom/samsung/multiscreen/Search$OnStartListener;

    return-void
.end method

.method public setOnStopListener(Lcom/samsung/multiscreen/Search$OnStopListener;)V
    .locals 0
    .param p1, "onStopListener"    # Lcom/samsung/multiscreen/Search$OnStopListener;

    .prologue
    .line 167
    iput-object p1, p0, Lcom/samsung/multiscreen/Search;->onStopListener:Lcom/samsung/multiscreen/Search$OnStopListener;

    return-void
.end method

.method public start()Z
    .locals 1

    .prologue
    .line 234
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/multiscreen/Search;->start(Ljava/lang/Boolean;)Z

    move-result v0

    return v0
.end method

.method public start(Ljava/lang/Boolean;)Z
    .locals 2
    .param p1, "showStandbyDevices"    # Ljava/lang/Boolean;

    .prologue
    .line 245
    invoke-virtual {p0}, Lcom/samsung/multiscreen/Search;->isSearching()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 246
    const/4 v0, 0x0

    .line 260
    :goto_0
    return v0

    .line 249
    :cond_0
    invoke-direct {p0}, Lcom/samsung/multiscreen/Search;->startDiscovery()V

    .line 250
    const-string v0, "Search"

    const-string v1, "start() called & Discovery started."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 252
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 254
    iget-object v0, p0, Lcom/samsung/multiscreen/Search;->context:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/multiscreen/Search;->searchListener:Lcom/samsung/multiscreen/Search$SearchListener;

    invoke-static {v0, v1}, Lcom/samsung/multiscreen/StandbyDeviceList;->create(Landroid/content/Context;Lcom/samsung/multiscreen/Search$SearchListener;)Lcom/samsung/multiscreen/StandbyDeviceList;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/multiscreen/Search;->mStandbyDeviceList:Lcom/samsung/multiscreen/StandbyDeviceList;

    .line 255
    iget-object v0, p0, Lcom/samsung/multiscreen/Search;->mStandbyDeviceList:Lcom/samsung/multiscreen/StandbyDeviceList;

    invoke-virtual {v0}, Lcom/samsung/multiscreen/StandbyDeviceList;->start()V

    .line 260
    :cond_1
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 256
    :cond_2
    iget-object v0, p0, Lcom/samsung/multiscreen/Search;->mStandbyDeviceList:Lcom/samsung/multiscreen/StandbyDeviceList;

    if-eqz v0, :cond_1

    .line 257
    iget-object v0, p0, Lcom/samsung/multiscreen/Search;->mStandbyDeviceList:Lcom/samsung/multiscreen/StandbyDeviceList;

    invoke-virtual {v0}, Lcom/samsung/multiscreen/StandbyDeviceList;->destruct()V

    .line 258
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/multiscreen/Search;->mStandbyDeviceList:Lcom/samsung/multiscreen/StandbyDeviceList;

    goto :goto_1
.end method

.method public startUsingBle()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 300
    invoke-virtual {p0}, Lcom/samsung/multiscreen/Search;->isSupportBle()Z

    move-result v1

    if-nez v1, :cond_1

    .line 311
    :cond_0
    :goto_0
    return v0

    .line 305
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/multiscreen/Search;->isSearchingBle()Z

    move-result v1

    if-nez v1, :cond_0

    .line 309
    invoke-direct {p0}, Lcom/samsung/multiscreen/Search;->startDiscoveryUsingBle()V

    .line 311
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public stop()Z
    .locals 1

    .prologue
    .line 269
    invoke-direct {p0}, Lcom/samsung/multiscreen/Search;->stopDiscovery()V

    .line 270
    const/4 v0, 0x1

    return v0
.end method

.method public stopUsingBle()Z
    .locals 1

    .prologue
    .line 321
    invoke-direct {p0}, Lcom/samsung/multiscreen/Search;->stopDiscoveryUsingBle()V

    .line 323
    const/4 v0, 0x1

    return v0
.end method
