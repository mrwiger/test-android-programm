.class public Lru/cn/utils/validation/MinLengthValidator;
.super Ljava/lang/Object;
.source "MinLengthValidator.java"

# interfaces
.implements Lru/cn/utils/validation/Validator;


# instance fields
.field private final description:Ljava/lang/String;

.field private final minLength:I

.field private final validationValue:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p1, "validationValue"    # Ljava/lang/String;
    .param p2, "minLength"    # I
    .param p3, "description"    # Ljava/lang/String;

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object p1, p0, Lru/cn/utils/validation/MinLengthValidator;->validationValue:Ljava/lang/String;

    .line 13
    iput p2, p0, Lru/cn/utils/validation/MinLengthValidator;->minLength:I

    .line 14
    iput-object p3, p0, Lru/cn/utils/validation/MinLengthValidator;->description:Ljava/lang/String;

    .line 15
    return-void
.end method


# virtual methods
.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lru/cn/utils/validation/MinLengthValidator;->description:Ljava/lang/String;

    return-object v0
.end method

.method public validate()Z
    .locals 2

    .prologue
    .line 19
    iget-object v0, p0, Lru/cn/utils/validation/MinLengthValidator;->validationValue:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lru/cn/utils/validation/MinLengthValidator;->validationValue:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    iget v1, p0, Lru/cn/utils/validation/MinLengthValidator;->minLength:I

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
