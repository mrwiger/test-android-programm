.class public Lru/cn/events/ScheduleOpenEvent;
.super Ljava/lang/Object;
.source "ScheduleOpenEvent.java"


# instance fields
.field private channelId:J


# direct methods
.method public constructor <init>(J)V
    .locals 1
    .param p1, "channelId"    # J

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    iput-wide p1, p0, Lru/cn/events/ScheduleOpenEvent;->channelId:J

    .line 9
    return-void
.end method


# virtual methods
.method public getChannelId()J
    .locals 2

    .prologue
    .line 12
    iget-wide v0, p0, Lru/cn/events/ScheduleOpenEvent;->channelId:J

    return-wide v0
.end method
