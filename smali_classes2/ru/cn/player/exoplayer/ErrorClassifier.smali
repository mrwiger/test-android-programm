.class final Lru/cn/player/exoplayer/ErrorClassifier;
.super Ljava/lang/Object;
.source "ErrorClassifier.java"


# instance fields
.field private final attributes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final error:Lcom/google/android/exoplayer2/ExoPlaybackException;

.field private errorCode:I


# direct methods
.method constructor <init>(Lcom/google/android/exoplayer2/ExoPlaybackException;)V
    .locals 1
    .param p1, "error"    # Lcom/google/android/exoplayer2/ExoPlaybackException;

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const/4 v0, 0x0

    iput v0, p0, Lru/cn/player/exoplayer/ErrorClassifier;->errorCode:I

    .line 24
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lru/cn/player/exoplayer/ErrorClassifier;->attributes:Ljava/util/Map;

    .line 27
    iput-object p1, p0, Lru/cn/player/exoplayer/ErrorClassifier;->error:Lcom/google/android/exoplayer2/ExoPlaybackException;

    .line 28
    return-void
.end method


# virtual methods
.method attributes()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 143
    iget-object v0, p0, Lru/cn/player/exoplayer/ErrorClassifier;->attributes:Ljava/util/Map;

    return-object v0
.end method

.method final classify(Landroid/net/Uri;Ljava/lang/String;)V
    .locals 7
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "codecs"    # Ljava/lang/String;

    .prologue
    const/16 v6, 0x2ef

    .line 31
    iget-object v3, p0, Lru/cn/player/exoplayer/ErrorClassifier;->attributes:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->clear()V

    .line 32
    iget-object v3, p0, Lru/cn/player/exoplayer/ErrorClassifier;->attributes:Ljava/util/Map;

    const-string v4, "content"

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    if-eqz p2, :cond_0

    .line 34
    iget-object v3, p0, Lru/cn/player/exoplayer/ErrorClassifier;->attributes:Ljava/util/Map;

    const-string v4, "codecs"

    invoke-interface {v3, v4, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    :cond_0
    iget-object v3, p0, Lru/cn/player/exoplayer/ErrorClassifier;->error:Lcom/google/android/exoplayer2/ExoPlaybackException;

    invoke-virtual {v3}, Lcom/google/android/exoplayer2/ExoPlaybackException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    .line 38
    .local v0, "cause":Ljava/lang/Throwable;
    iget-object v3, p0, Lru/cn/player/exoplayer/ErrorClassifier;->error:Lcom/google/android/exoplayer2/ExoPlaybackException;

    iget v3, v3, Lcom/google/android/exoplayer2/ExoPlaybackException;->type:I

    packed-switch v3, :pswitch_data_0

    .line 118
    :cond_1
    :goto_0
    iget v3, p0, Lru/cn/player/exoplayer/ErrorClassifier;->errorCode:I

    if-nez v3, :cond_2

    .line 119
    iget-object v3, p0, Lru/cn/player/exoplayer/ErrorClassifier;->attributes:Ljava/util/Map;

    const-string v4, "trace"

    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    :cond_2
    return-void

    .line 40
    :pswitch_0
    instance-of v3, v0, Lcom/google/android/exoplayer2/upstream/HttpDataSource$InvalidResponseCodeException;

    if-eqz v3, :cond_4

    .line 41
    const/16 v3, 0x2f5

    iput v3, p0, Lru/cn/player/exoplayer/ErrorClassifier;->errorCode:I

    move-object v1, v0

    .line 43
    check-cast v1, Lcom/google/android/exoplayer2/upstream/HttpDataSource$InvalidResponseCodeException;

    .line 44
    .local v1, "e":Lcom/google/android/exoplayer2/upstream/HttpDataSource$InvalidResponseCodeException;
    iget v3, v1, Lcom/google/android/exoplayer2/upstream/HttpDataSource$InvalidResponseCodeException;->responseCode:I

    const/16 v4, 0x193

    if-ne v3, v4, :cond_3

    .line 45
    const/16 v3, 0x2f4

    iput v3, p0, Lru/cn/player/exoplayer/ErrorClassifier;->errorCode:I

    .line 48
    :cond_3
    iget-object v3, p0, Lru/cn/player/exoplayer/ErrorClassifier;->attributes:Ljava/util/Map;

    const-string v4, "code"

    iget v5, v1, Lcom/google/android/exoplayer2/upstream/HttpDataSource$InvalidResponseCodeException;->responseCode:I

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    iget-object v3, p0, Lru/cn/player/exoplayer/ErrorClassifier;->attributes:Ljava/util/Map;

    const-string v4, "uri"

    iget-object v5, v1, Lcom/google/android/exoplayer2/upstream/HttpDataSource$InvalidResponseCodeException;->dataSpec:Lcom/google/android/exoplayer2/upstream/DataSpec;

    iget-object v5, v5, Lcom/google/android/exoplayer2/upstream/DataSpec;->uri:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 50
    .end local v1    # "e":Lcom/google/android/exoplayer2/upstream/HttpDataSource$InvalidResponseCodeException;
    :cond_4
    instance-of v3, v0, Lcom/google/android/exoplayer2/upstream/HttpDataSource$HttpDataSourceException;

    if-eqz v3, :cond_5

    .line 51
    const/16 v3, 0x2f6

    iput v3, p0, Lru/cn/player/exoplayer/ErrorClassifier;->errorCode:I

    move-object v1, v0

    .line 53
    check-cast v1, Lcom/google/android/exoplayer2/upstream/HttpDataSource$HttpDataSourceException;

    .line 54
    .local v1, "e":Lcom/google/android/exoplayer2/upstream/HttpDataSource$HttpDataSourceException;
    iget-object v3, p0, Lru/cn/player/exoplayer/ErrorClassifier;->attributes:Ljava/util/Map;

    const-string v4, "comment"

    invoke-virtual {v1}, Lcom/google/android/exoplayer2/upstream/HttpDataSource$HttpDataSourceException;->getCause()Ljava/lang/Throwable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    iget-object v3, p0, Lru/cn/player/exoplayer/ErrorClassifier;->attributes:Ljava/util/Map;

    const-string v4, "uri"

    iget-object v5, v1, Lcom/google/android/exoplayer2/upstream/HttpDataSource$HttpDataSourceException;->dataSpec:Lcom/google/android/exoplayer2/upstream/DataSpec;

    iget-object v5, v5, Lcom/google/android/exoplayer2/upstream/DataSpec;->uri:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 56
    .end local v1    # "e":Lcom/google/android/exoplayer2/upstream/HttpDataSource$HttpDataSourceException;
    :cond_5
    instance-of v3, v0, Lcom/google/android/exoplayer2/source/BehindLiveWindowException;

    if-eqz v3, :cond_6

    .line 57
    const/16 v3, 0x2f7

    iput v3, p0, Lru/cn/player/exoplayer/ErrorClassifier;->errorCode:I

    goto :goto_0

    .line 59
    :cond_6
    instance-of v3, v0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$PlaylistStuckException;

    if-eqz v3, :cond_7

    .line 60
    const/16 v3, 0x2f9

    iput v3, p0, Lru/cn/player/exoplayer/ErrorClassifier;->errorCode:I

    goto :goto_0

    .line 62
    :cond_7
    instance-of v3, v0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsPlaylistTracker$PlaylistResetException;

    if-eqz v3, :cond_8

    .line 63
    const/16 v3, 0x2fa

    iput v3, p0, Lru/cn/player/exoplayer/ErrorClassifier;->errorCode:I

    goto/16 :goto_0

    .line 65
    :cond_8
    instance-of v3, v0, Lcom/google/android/exoplayer2/source/UnrecognizedInputFormatException;

    if-eqz v3, :cond_1

    .line 66
    const/16 v3, 0x2fb

    iput v3, p0, Lru/cn/player/exoplayer/ErrorClassifier;->errorCode:I

    .line 68
    iget-object v3, p0, Lru/cn/player/exoplayer/ErrorClassifier;->attributes:Ljava/util/Map;

    const-string v4, "trace"

    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 73
    :pswitch_1
    instance-of v3, v0, Lcom/google/android/exoplayer2/mediacodec/MediaCodecRenderer$DecoderInitializationException;

    if-eqz v3, :cond_b

    move-object v1, v0

    .line 74
    check-cast v1, Lcom/google/android/exoplayer2/mediacodec/MediaCodecRenderer$DecoderInitializationException;

    .line 76
    .local v1, "e":Lcom/google/android/exoplayer2/mediacodec/MediaCodecRenderer$DecoderInitializationException;
    iget-object v3, p0, Lru/cn/player/exoplayer/ErrorClassifier;->attributes:Ljava/util/Map;

    const-string v4, "mime"

    iget-object v5, v1, Lcom/google/android/exoplayer2/mediacodec/MediaCodecRenderer$DecoderInitializationException;->mimeType:Ljava/lang/String;

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    const/16 v3, 0x2ee

    iput v3, p0, Lru/cn/player/exoplayer/ErrorClassifier;->errorCode:I

    .line 79
    iget-object v3, v1, Lcom/google/android/exoplayer2/mediacodec/MediaCodecRenderer$DecoderInitializationException;->decoderName:Ljava/lang/String;

    if-nez v3, :cond_a

    .line 80
    invoke-virtual {v1}, Lcom/google/android/exoplayer2/mediacodec/MediaCodecRenderer$DecoderInitializationException;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    instance-of v3, v3, Lcom/google/android/exoplayer2/mediacodec/MediaCodecUtil$DecoderQueryException;

    if-eqz v3, :cond_9

    .line 81
    iput v6, p0, Lru/cn/player/exoplayer/ErrorClassifier;->errorCode:I

    goto/16 :goto_0

    .line 82
    :cond_9
    iget-boolean v3, v1, Lcom/google/android/exoplayer2/mediacodec/MediaCodecRenderer$DecoderInitializationException;->secureDecoderRequired:Z

    if-eqz v3, :cond_1

    .line 83
    const/16 v3, 0x2f3

    iput v3, p0, Lru/cn/player/exoplayer/ErrorClassifier;->errorCode:I

    goto/16 :goto_0

    .line 86
    :cond_a
    iget-object v3, p0, Lru/cn/player/exoplayer/ErrorClassifier;->attributes:Ljava/util/Map;

    const-string v4, "name"

    iget-object v5, v1, Lcom/google/android/exoplayer2/mediacodec/MediaCodecRenderer$DecoderInitializationException;->decoderName:Ljava/lang/String;

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 88
    .end local v1    # "e":Lcom/google/android/exoplayer2/mediacodec/MediaCodecRenderer$DecoderInitializationException;
    :cond_b
    instance-of v3, v0, Lcom/google/android/exoplayer2/mediacodec/MediaCodecUtil$DecoderQueryException;

    if-eqz v3, :cond_c

    .line 90
    iput v6, p0, Lru/cn/player/exoplayer/ErrorClassifier;->errorCode:I

    goto/16 :goto_0

    .line 91
    :cond_c
    instance-of v3, v0, Lcom/google/android/exoplayer2/drm/UnsupportedDrmException;

    if-eqz v3, :cond_f

    .line 92
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x12

    if-ge v3, v4, :cond_d

    .line 93
    const/16 v3, 0x2f0

    iput v3, p0, Lru/cn/player/exoplayer/ErrorClassifier;->errorCode:I

    goto/16 :goto_0

    :cond_d
    move-object v2, v0

    .line 96
    check-cast v2, Lcom/google/android/exoplayer2/drm/UnsupportedDrmException;

    .line 97
    .local v2, "unsupportedDrmException":Lcom/google/android/exoplayer2/drm/UnsupportedDrmException;
    iget v3, v2, Lcom/google/android/exoplayer2/drm/UnsupportedDrmException;->reason:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_e

    .line 98
    const/16 v3, 0x2f2

    iput v3, p0, Lru/cn/player/exoplayer/ErrorClassifier;->errorCode:I

    goto/16 :goto_0

    .line 100
    :cond_e
    const/16 v3, 0x2f1

    iput v3, p0, Lru/cn/player/exoplayer/ErrorClassifier;->errorCode:I

    goto/16 :goto_0

    .line 103
    .end local v2    # "unsupportedDrmException":Lcom/google/android/exoplayer2/drm/UnsupportedDrmException;
    :cond_f
    instance-of v3, v0, Lru/cn/player/exoplayer/ExoPlayerUtils$UnsupportedCodecException;

    if-eqz v3, :cond_10

    .line 104
    const/16 v3, 0x2f8

    iput v3, p0, Lru/cn/player/exoplayer/ErrorClassifier;->errorCode:I

    move-object v1, v0

    .line 106
    check-cast v1, Lru/cn/player/exoplayer/ExoPlayerUtils$UnsupportedCodecException;

    .line 107
    .local v1, "e":Lru/cn/player/exoplayer/ExoPlayerUtils$UnsupportedCodecException;
    iget-object v3, p0, Lru/cn/player/exoplayer/ErrorClassifier;->attributes:Ljava/util/Map;

    const-string v4, "mime"

    iget-object v5, v1, Lru/cn/player/exoplayer/ExoPlayerUtils$UnsupportedCodecException;->mime:Ljava/lang/String;

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 109
    .end local v1    # "e":Lru/cn/player/exoplayer/ExoPlayerUtils$UnsupportedCodecException;
    :cond_10
    instance-of v3, v0, Lcom/google/android/exoplayer2/audio/AudioSink$InitializationException;

    if-eqz v3, :cond_1

    .line 110
    const/16 v3, 0x2fc

    iput v3, p0, Lru/cn/player/exoplayer/ErrorClassifier;->errorCode:I

    goto/16 :goto_0

    .line 38
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method errorCode()I
    .locals 1

    .prologue
    .line 139
    iget v0, p0, Lru/cn/player/exoplayer/ErrorClassifier;->errorCode:I

    return v0
.end method

.method recoverable()Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 124
    iget-object v4, p0, Lru/cn/player/exoplayer/ErrorClassifier;->error:Lcom/google/android/exoplayer2/ExoPlaybackException;

    if-eqz v4, :cond_2

    .line 125
    iget-object v4, p0, Lru/cn/player/exoplayer/ErrorClassifier;->error:Lcom/google/android/exoplayer2/ExoPlaybackException;

    invoke-virtual {v4}, Lcom/google/android/exoplayer2/ExoPlaybackException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    .line 126
    .local v0, "cause":Ljava/lang/Throwable;
    instance-of v4, v0, Lcom/google/android/exoplayer2/source/BehindLiveWindowException;

    if-eqz v4, :cond_1

    .line 135
    .end local v0    # "cause":Ljava/lang/Throwable;
    :cond_0
    :goto_0
    return v2

    .line 129
    .restart local v0    # "cause":Ljava/lang/Throwable;
    :cond_1
    instance-of v4, v0, Lcom/google/android/exoplayer2/upstream/HttpDataSource$InvalidResponseCodeException;

    if-eqz v4, :cond_2

    move-object v1, v0

    .line 130
    check-cast v1, Lcom/google/android/exoplayer2/upstream/HttpDataSource$InvalidResponseCodeException;

    .line 131
    .local v1, "e":Lcom/google/android/exoplayer2/upstream/HttpDataSource$InvalidResponseCodeException;
    iget v4, v1, Lcom/google/android/exoplayer2/upstream/HttpDataSource$InvalidResponseCodeException;->responseCode:I

    const/16 v5, 0x1f4

    if-ge v4, v5, :cond_0

    move v2, v3

    goto :goto_0

    .end local v0    # "cause":Ljava/lang/Throwable;
    .end local v1    # "e":Lcom/google/android/exoplayer2/upstream/HttpDataSource$InvalidResponseCodeException;
    :cond_2
    move v2, v3

    .line 135
    goto :goto_0
.end method
