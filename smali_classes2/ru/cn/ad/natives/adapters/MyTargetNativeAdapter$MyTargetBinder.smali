.class final Lru/cn/ad/natives/adapters/MyTargetNativeAdapter$MyTargetBinder;
.super Ljava/lang/Object;
.source "MyTargetNativeAdapter.java"

# interfaces
.implements Lru/cn/ad/natives/adapters/NativeAdAdapter$ViewBinder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "MyTargetBinder"
.end annotation


# instance fields
.field private final banner:Lcom/my/target/nativeads/banners/NativePromoBanner;

.field private final nativeAd:Lcom/my/target/nativeads/NativeAd;

.field private final type:I


# direct methods
.method constructor <init>(Lcom/my/target/nativeads/NativeAd;)V
    .locals 2
    .param p1, "nativeAd"    # Lcom/my/target/nativeads/NativeAd;

    .prologue
    .line 122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 123
    iput-object p1, p0, Lru/cn/ad/natives/adapters/MyTargetNativeAdapter$MyTargetBinder;->nativeAd:Lcom/my/target/nativeads/NativeAd;

    .line 125
    invoke-virtual {p1}, Lcom/my/target/nativeads/NativeAd;->getBanner()Lcom/my/target/nativeads/banners/NativePromoBanner;

    move-result-object v0

    iput-object v0, p0, Lru/cn/ad/natives/adapters/MyTargetNativeAdapter$MyTargetBinder;->banner:Lcom/my/target/nativeads/banners/NativePromoBanner;

    .line 126
    iget-object v0, p0, Lru/cn/ad/natives/adapters/MyTargetNativeAdapter$MyTargetBinder;->banner:Lcom/my/target/nativeads/banners/NativePromoBanner;

    invoke-virtual {v0}, Lcom/my/target/nativeads/banners/NativePromoBanner;->getNavigationType()Ljava/lang/String;

    move-result-object v0

    const-string v1, "web"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 127
    const/4 v0, 0x1

    iput v0, p0, Lru/cn/ad/natives/adapters/MyTargetNativeAdapter$MyTargetBinder;->type:I

    .line 135
    :goto_0
    return-void

    .line 128
    :cond_0
    iget-object v0, p0, Lru/cn/ad/natives/adapters/MyTargetNativeAdapter$MyTargetBinder;->banner:Lcom/my/target/nativeads/banners/NativePromoBanner;

    invoke-virtual {v0}, Lcom/my/target/nativeads/banners/NativePromoBanner;->getNavigationType()Ljava/lang/String;

    move-result-object v0

    const-string v1, "store"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 129
    const/4 v0, 0x2

    iput v0, p0, Lru/cn/ad/natives/adapters/MyTargetNativeAdapter$MyTargetBinder;->type:I

    goto :goto_0

    .line 132
    :cond_1
    const-string v0, "MyTargetNativeAdapter"

    const-string v1, "Unsupported type of native ad"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    const/4 v0, -0x1

    iput v0, p0, Lru/cn/ad/natives/adapters/MyTargetNativeAdapter$MyTargetBinder;->type:I

    goto :goto_0
.end method


# virtual methods
.method public bind(Landroid/widget/Button;I)Z
    .locals 1
    .param p1, "view"    # Landroid/widget/Button;
    .param p2, "componentType"    # I

    .prologue
    .line 196
    packed-switch p2, :pswitch_data_0

    .line 202
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 198
    :pswitch_0
    iget-object v0, p0, Lru/cn/ad/natives/adapters/MyTargetNativeAdapter$MyTargetBinder;->banner:Lcom/my/target/nativeads/banners/NativePromoBanner;

    invoke-virtual {v0}, Lcom/my/target/nativeads/banners/NativePromoBanner;->getCtaText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 199
    const/4 v0, 0x1

    goto :goto_0

    .line 196
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method public bind(Landroid/widget/ImageView;I)Z
    .locals 5
    .param p1, "view"    # Landroid/widget/ImageView;
    .param p2, "componentType"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 171
    packed-switch p2, :pswitch_data_0

    .line 191
    :cond_0
    :goto_0
    :pswitch_0
    return v2

    .line 173
    :pswitch_1
    iget-object v4, p0, Lru/cn/ad/natives/adapters/MyTargetNativeAdapter$MyTargetBinder;->banner:Lcom/my/target/nativeads/banners/NativePromoBanner;

    invoke-virtual {v4}, Lcom/my/target/nativeads/banners/NativePromoBanner;->getIcon()Lcom/my/target/common/models/ImageData;

    move-result-object v0

    .line 174
    .local v0, "iconData":Lcom/my/target/common/models/ImageData;
    if-eqz v0, :cond_0

    .line 178
    invoke-static {v0, p1}, Lcom/my/target/nativeads/NativeAd;->loadImageToView(Lcom/my/target/common/models/ImageData;Landroid/widget/ImageView;)V

    move v2, v3

    .line 179
    goto :goto_0

    .line 182
    .end local v0    # "iconData":Lcom/my/target/common/models/ImageData;
    :pswitch_2
    iget-object v4, p0, Lru/cn/ad/natives/adapters/MyTargetNativeAdapter$MyTargetBinder;->banner:Lcom/my/target/nativeads/banners/NativePromoBanner;

    invoke-virtual {v4}, Lcom/my/target/nativeads/banners/NativePromoBanner;->getImage()Lcom/my/target/common/models/ImageData;

    move-result-object v1

    .line 183
    .local v1, "imageData":Lcom/my/target/common/models/ImageData;
    if-eqz v1, :cond_0

    .line 187
    invoke-static {v1, p1}, Lcom/my/target/nativeads/NativeAd;->loadImageToView(Lcom/my/target/common/models/ImageData;Landroid/widget/ImageView;)V

    move v2, v3

    .line 188
    goto :goto_0

    .line 171
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public bind(Landroid/widget/TextView;I)Z
    .locals 3
    .param p1, "view"    # Landroid/widget/TextView;
    .param p2, "componentType"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 144
    packed-switch p2, :pswitch_data_0

    :pswitch_0
    move v0, v1

    .line 166
    :goto_0
    return v0

    .line 146
    :pswitch_1
    iget-object v1, p0, Lru/cn/ad/natives/adapters/MyTargetNativeAdapter$MyTargetBinder;->banner:Lcom/my/target/nativeads/banners/NativePromoBanner;

    invoke-virtual {v1}, Lcom/my/target/nativeads/banners/NativePromoBanner;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 150
    :pswitch_2
    iget-object v1, p0, Lru/cn/ad/natives/adapters/MyTargetNativeAdapter$MyTargetBinder;->banner:Lcom/my/target/nativeads/banners/NativePromoBanner;

    invoke-virtual {v1}, Lcom/my/target/nativeads/banners/NativePromoBanner;->getCtaText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 154
    :pswitch_3
    iget-object v1, p0, Lru/cn/ad/natives/adapters/MyTargetNativeAdapter$MyTargetBinder;->banner:Lcom/my/target/nativeads/banners/NativePromoBanner;

    invoke-virtual {v1}, Lcom/my/target/nativeads/banners/NativePromoBanner;->getDescription()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 158
    :pswitch_4
    iget-object v2, p0, Lru/cn/ad/natives/adapters/MyTargetNativeAdapter$MyTargetBinder;->banner:Lcom/my/target/nativeads/banners/NativePromoBanner;

    invoke-virtual {v2}, Lcom/my/target/nativeads/banners/NativePromoBanner;->getAgeRestrictions()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    .line 159
    goto :goto_0

    .line 162
    :cond_0
    iget-object v1, p0, Lru/cn/ad/natives/adapters/MyTargetNativeAdapter$MyTargetBinder;->banner:Lcom/my/target/nativeads/banners/NativePromoBanner;

    invoke-virtual {v1}, Lcom/my/target/nativeads/banners/NativePromoBanner;->getAgeRestrictions()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 144
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public getAdType()I
    .locals 1

    .prologue
    .line 139
    iget v0, p0, Lru/cn/ad/natives/adapters/MyTargetNativeAdapter$MyTargetBinder;->type:I

    return v0
.end method

.method public registerTemplate(Landroid/view/View;Ljava/util/List;)Landroid/view/View;
    .locals 1
    .param p1, "templateView"    # Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)",
            "Landroid/view/View;"
        }
    .end annotation

    .prologue
    .line 207
    .local p2, "clickableViews":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    iget-object v0, p0, Lru/cn/ad/natives/adapters/MyTargetNativeAdapter$MyTargetBinder;->nativeAd:Lcom/my/target/nativeads/NativeAd;

    invoke-virtual {v0, p1, p2}, Lcom/my/target/nativeads/NativeAd;->registerView(Landroid/view/View;Ljava/util/List;)V

    .line 209
    return-object p1
.end method
