.class public Lru/cn/utils/http/HttpClient;
.super Ljava/lang/Object;
.source "HttpClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/utils/http/HttpClient$Builder;,
        Lru/cn/utils/http/HttpClient$Method;
    }
.end annotation


# static fields
.field private static defaultHttpClient:Lokhttp3/OkHttpClient;

.field private static defaultUserAgent:Ljava/lang/String;


# instance fields
.field private content:Ljava/lang/String;

.field private httpClient:Lokhttp3/OkHttpClient;

.field private requestHeaders:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/support/v4/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private response:Lokhttp3/Response;

.field private userAgent:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    const-string v0, "OkHttp3"

    sput-object v0, Lru/cn/utils/http/HttpClient;->defaultUserAgent:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 100
    sget-object v0, Lru/cn/utils/http/HttpClient;->defaultHttpClient:Lokhttp3/OkHttpClient;

    invoke-direct {p0, v0}, Lru/cn/utils/http/HttpClient;-><init>(Lokhttp3/OkHttpClient;)V

    .line 101
    return-void
.end method

.method private constructor <init>(Lokhttp3/OkHttpClient;)V
    .locals 1
    .param p1, "httpClient"    # Lokhttp3/OkHttpClient;

    .prologue
    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lru/cn/utils/http/HttpClient;->requestHeaders:Ljava/util/List;

    .line 95
    iput-object p1, p0, Lru/cn/utils/http/HttpClient;->httpClient:Lokhttp3/OkHttpClient;

    .line 96
    sget-object v0, Lru/cn/utils/http/HttpClient;->defaultUserAgent:Ljava/lang/String;

    iput-object v0, p0, Lru/cn/utils/http/HttpClient;->userAgent:Ljava/lang/String;

    .line 97
    return-void
.end method

.method synthetic constructor <init>(Lokhttp3/OkHttpClient;Lru/cn/utils/http/HttpClient$1;)V
    .locals 0
    .param p1, "x0"    # Lokhttp3/OkHttpClient;
    .param p2, "x1"    # Lru/cn/utils/http/HttpClient$1;

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lru/cn/utils/http/HttpClient;-><init>(Lokhttp3/OkHttpClient;)V

    return-void
.end method

.method static synthetic access$000()Lokhttp3/OkHttpClient;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lru/cn/utils/http/HttpClient;->defaultHttpClient:Lokhttp3/OkHttpClient;

    return-object v0
.end method

.method private createRequest(Ljava/lang/String;Lru/cn/utils/http/HttpClient$Method;Ljava/lang/String;)Lokhttp3/Request;
    .locals 6
    .param p1, "requestUrl"    # Ljava/lang/String;
    .param p2, "method"    # Lru/cn/utils/http/HttpClient$Method;
    .param p3, "body"    # Ljava/lang/String;

    .prologue
    .line 130
    const/4 v2, 0x0

    .line 131
    .local v2, "requestBody":Lokhttp3/RequestBody;
    if-eqz p3, :cond_0

    .line 132
    const/4 v3, 0x0

    invoke-static {v3, p3}, Lokhttp3/RequestBody;->create(Lokhttp3/MediaType;Ljava/lang/String;)Lokhttp3/RequestBody;

    move-result-object v2

    .line 135
    :cond_0
    new-instance v3, Lokhttp3/Request$Builder;

    invoke-direct {v3}, Lokhttp3/Request$Builder;-><init>()V

    .line 136
    invoke-virtual {v3, p1}, Lokhttp3/Request$Builder;->url(Ljava/lang/String;)Lokhttp3/Request$Builder;

    move-result-object v3

    .line 137
    invoke-virtual {p2}, Lru/cn/utils/http/HttpClient$Method;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v2}, Lokhttp3/Request$Builder;->method(Ljava/lang/String;Lokhttp3/RequestBody;)Lokhttp3/Request$Builder;

    move-result-object v3

    const-string v4, "User-Agent"

    iget-object v5, p0, Lru/cn/utils/http/HttpClient;->userAgent:Ljava/lang/String;

    .line 138
    invoke-virtual {v3, v4, v5}, Lokhttp3/Request$Builder;->header(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Request$Builder;

    move-result-object v3

    const-string v4, "Accept-Language"

    .line 139
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lokhttp3/Request$Builder;->header(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Request$Builder;

    move-result-object v0

    .line 141
    .local v0, "builder":Lokhttp3/Request$Builder;
    iget-object v3, p0, Lru/cn/utils/http/HttpClient;->requestHeaders:Ljava/util/List;

    if-eqz v3, :cond_1

    .line 142
    iget-object v3, p0, Lru/cn/utils/http/HttpClient;->requestHeaders:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/util/Pair;

    .line 143
    .local v1, "pair":Landroid/support/v4/util/Pair;, "Landroid/support/v4/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v3, v1, Landroid/support/v4/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    iget-object v4, v1, Landroid/support/v4/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Lokhttp3/Request$Builder;->addHeader(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Request$Builder;

    goto :goto_0

    .line 147
    .end local v1    # "pair":Landroid/support/v4/util/Pair;, "Landroid/support/v4/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_1
    invoke-virtual {v0}, Lokhttp3/Request$Builder;->build()Lokhttp3/Request;

    move-result-object v3

    return-object v3
.end method

.method public static defaultUserAgent()Ljava/lang/String;
    .locals 1

    .prologue
    .line 244
    sget-object v0, Lru/cn/utils/http/HttpClient;->defaultUserAgent:Ljava/lang/String;

    return-object v0
.end method

.method public static initialize(Landroid/content/Context;)V
    .locals 10
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const-wide/16 v8, 0x4e20

    .line 65
    new-instance v1, Lcom/franmontiel/persistentcookiejar/PersistentCookieJar;

    new-instance v4, Lcom/franmontiel/persistentcookiejar/cache/SetCookieCache;

    invoke-direct {v4}, Lcom/franmontiel/persistentcookiejar/cache/SetCookieCache;-><init>()V

    new-instance v5, Lcom/franmontiel/persistentcookiejar/persistence/SharedPrefsCookiePersistor;

    invoke-direct {v5, p0}, Lcom/franmontiel/persistentcookiejar/persistence/SharedPrefsCookiePersistor;-><init>(Landroid/content/Context;)V

    invoke-direct {v1, v4, v5}, Lcom/franmontiel/persistentcookiejar/PersistentCookieJar;-><init>(Lcom/franmontiel/persistentcookiejar/cache/CookieCache;Lcom/franmontiel/persistentcookiejar/persistence/CookiePersistor;)V

    .line 66
    .local v1, "cookieJar":Lokhttp3/CookieJar;
    new-instance v4, Lokhttp3/OkHttpClient$Builder;

    invoke-direct {v4}, Lokhttp3/OkHttpClient$Builder;-><init>()V

    const-wide/16 v6, 0x2710

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 67
    invoke-virtual {v4, v6, v7, v5}, Lokhttp3/OkHttpClient$Builder;->connectTimeout(JLjava/util/concurrent/TimeUnit;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v4

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 68
    invoke-virtual {v4, v8, v9, v5}, Lokhttp3/OkHttpClient$Builder;->readTimeout(JLjava/util/concurrent/TimeUnit;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v4

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 69
    invoke-virtual {v4, v8, v9, v5}, Lokhttp3/OkHttpClient$Builder;->writeTimeout(JLjava/util/concurrent/TimeUnit;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v4

    new-instance v5, Lru/cn/utils/http/HttpLoggingInterceptor;

    invoke-direct {v5}, Lru/cn/utils/http/HttpLoggingInterceptor;-><init>()V

    .line 70
    invoke-virtual {v4, v5}, Lokhttp3/OkHttpClient$Builder;->addInterceptor(Lokhttp3/Interceptor;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v4

    .line 71
    invoke-virtual {v4, v1}, Lokhttp3/OkHttpClient$Builder;->cookieJar(Lokhttp3/CookieJar;)Lokhttp3/OkHttpClient$Builder;

    move-result-object v4

    .line 72
    invoke-virtual {v4}, Lokhttp3/OkHttpClient$Builder;->build()Lokhttp3/OkHttpClient;

    move-result-object v4

    sput-object v4, Lru/cn/utils/http/HttpClient;->defaultHttpClient:Lokhttp3/OkHttpClient;

    .line 75
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    .line 76
    .local v3, "pi":Landroid/content/pm/PackageInfo;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 77
    .local v0, "b":Ljava/lang/StringBuilder;
    iget-object v4, v3, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v4, v4, Landroid/content/pm/ApplicationInfo;->labelRes:I

    .line 78
    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 77
    invoke-static {v4}, Lru/cn/utils/http/HttpClient;->removeSpaces(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 79
    const-string v4, "/"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    iget-object v4, v3, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-static {v4}, Lru/cn/utils/http/HttpClient;->removeSpaces(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    const-string v4, " "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 82
    invoke-static {}, Lru/cn/utils/Utils;->getOSString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lru/cn/utils/http/HttpClient;->removeSpaces(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83
    const-string v4, " "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 84
    invoke-static {p0}, Lru/cn/utils/Utils;->getDeviceTypeString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 85
    const-string v4, " "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 86
    invoke-static {}, Lru/cn/utils/Utils;->getDeviceString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 87
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    sput-object v4, Lru/cn/utils/http/HttpClient;->defaultUserAgent:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 92
    .end local v0    # "b":Ljava/lang/StringBuilder;
    .end local v3    # "pi":Landroid/content/pm/PackageInfo;
    :goto_0
    return-void

    .line 88
    :catch_0
    move-exception v2

    .line 89
    .local v2, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-static {v2}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    .line 90
    const-string v4, "unknown"

    sput-object v4, Lru/cn/utils/http/HttpClient;->defaultUserAgent:Ljava/lang/String;

    goto :goto_0
.end method

.method private static removeSpaces(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    .line 240
    const/16 v0, 0x20

    const/16 v1, 0x5f

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static sendRequestAsync(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "requestUrl"    # Ljava/lang/String;
    .param p1, "userAgent"    # Ljava/lang/String;

    .prologue
    .line 203
    new-instance v0, Lru/cn/utils/http/HttpClient$1;

    invoke-direct {v0, p0}, Lru/cn/utils/http/HttpClient$1;-><init>(Ljava/lang/String;)V

    invoke-static {p0, p1, v0}, Lru/cn/utils/http/HttpClient;->sendRequestAsync(Ljava/lang/String;Ljava/lang/String;Lokhttp3/Callback;)V

    .line 215
    return-void
.end method

.method public static sendRequestAsync(Ljava/lang/String;Ljava/lang/String;Lokhttp3/Callback;)V
    .locals 6
    .param p0, "requestUrl"    # Ljava/lang/String;
    .param p1, "userAgent"    # Ljava/lang/String;
    .param p2, "callback"    # Lokhttp3/Callback;

    .prologue
    const/4 v5, 0x0

    .line 221
    if-nez p1, :cond_0

    .line 222
    invoke-static {}, Lru/cn/utils/http/HttpClient;->defaultUserAgent()Ljava/lang/String;

    move-result-object p1

    .line 227
    :cond_0
    :try_start_0
    new-instance v2, Lokhttp3/Request$Builder;

    invoke-direct {v2}, Lokhttp3/Request$Builder;-><init>()V

    .line 228
    invoke-virtual {v2, p0}, Lokhttp3/Request$Builder;->url(Ljava/lang/String;)Lokhttp3/Request$Builder;

    move-result-object v2

    const-string v3, "User-Agent"

    .line 229
    invoke-virtual {v2, v3, p1}, Lokhttp3/Request$Builder;->header(Ljava/lang/String;Ljava/lang/String;)Lokhttp3/Request$Builder;

    move-result-object v2

    sget-object v3, Lru/cn/utils/http/HttpClient$Method;->GET:Lru/cn/utils/http/HttpClient$Method;

    .line 230
    invoke-virtual {v3}, Lru/cn/utils/http/HttpClient$Method;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lokhttp3/Request$Builder;->method(Ljava/lang/String;Lokhttp3/RequestBody;)Lokhttp3/Request$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lokhttp3/Request$Builder;->build()Lokhttp3/Request;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 236
    .local v1, "request":Lokhttp3/Request;
    sget-object v2, Lru/cn/utils/http/HttpClient;->defaultHttpClient:Lokhttp3/OkHttpClient;

    invoke-virtual {v2, v1}, Lokhttp3/OkHttpClient;->newCall(Lokhttp3/Request;)Lokhttp3/Call;

    move-result-object v2

    invoke-interface {v2, p2}, Lokhttp3/Call;->enqueue(Lokhttp3/Callback;)V

    .line 237
    .end local v1    # "request":Lokhttp3/Request;
    :goto_0
    return-void

    .line 231
    :catch_0
    move-exception v0

    .line 232
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    new-instance v2, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Illegal URL "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-interface {p2, v5, v2}, Lokhttp3/Callback;->onFailure(Lokhttp3/Call;Ljava/io/IOException;)V

    goto :goto_0
.end method


# virtual methods
.method public addHeader(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 198
    new-instance v0, Landroid/support/v4/util/Pair;

    invoke-direct {v0, p1, p2}, Landroid/support/v4/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 199
    .local v0, "pair":Landroid/support/v4/util/Pair;, "Landroid/support/v4/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v1, p0, Lru/cn/utils/http/HttpClient;->requestHeaders:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 200
    return-void
.end method

.method public callFactory()Lokhttp3/Call$Factory;
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lru/cn/utils/http/HttpClient;->httpClient:Lokhttp3/OkHttpClient;

    return-object v0
.end method

.method public close()V
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lru/cn/utils/http/HttpClient;->response:Lokhttp3/Response;

    if-nez v0, :cond_0

    .line 188
    :goto_0
    return-void

    .line 187
    :cond_0
    iget-object v0, p0, Lru/cn/utils/http/HttpClient;->response:Lokhttp3/Response;

    invoke-virtual {v0}, Lokhttp3/Response;->body()Lokhttp3/ResponseBody;

    move-result-object v0

    invoke-virtual {v0}, Lokhttp3/ResponseBody;->close()V

    goto :goto_0
.end method

.method public getContent()Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 163
    iget-object v0, p0, Lru/cn/utils/http/HttpClient;->response:Lokhttp3/Response;

    if-nez v0, :cond_0

    .line 164
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unable to get content \u2014 perform request first"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 166
    :cond_0
    iget-object v0, p0, Lru/cn/utils/http/HttpClient;->content:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 167
    iget-object v0, p0, Lru/cn/utils/http/HttpClient;->response:Lokhttp3/Response;

    invoke-virtual {v0}, Lokhttp3/Response;->body()Lokhttp3/ResponseBody;

    move-result-object v0

    invoke-virtual {v0}, Lokhttp3/ResponseBody;->string()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lru/cn/utils/http/HttpClient;->content:Ljava/lang/String;

    .line 170
    :cond_1
    iget-object v0, p0, Lru/cn/utils/http/HttpClient;->content:Ljava/lang/String;

    return-object v0
.end method

.method public getContentReader()Ljava/io/Reader;
    .locals 2

    .prologue
    .line 174
    iget-object v0, p0, Lru/cn/utils/http/HttpClient;->response:Lokhttp3/Response;

    if-nez v0, :cond_0

    .line 175
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unable to get content \u2014 perform request first"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 177
    :cond_0
    iget-object v0, p0, Lru/cn/utils/http/HttpClient;->response:Lokhttp3/Response;

    invoke-virtual {v0}, Lokhttp3/Response;->body()Lokhttp3/ResponseBody;

    move-result-object v0

    invoke-virtual {v0}, Lokhttp3/ResponseBody;->charStream()Ljava/io/Reader;

    move-result-object v0

    return-object v0
.end method

.method public getHeader(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 191
    iget-object v0, p0, Lru/cn/utils/http/HttpClient;->response:Lokhttp3/Response;

    if-nez v0, :cond_0

    .line 192
    const/4 v0, 0x0

    .line 194
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lru/cn/utils/http/HttpClient;->response:Lokhttp3/Response;

    invoke-virtual {v0, p1}, Lokhttp3/Response;->header(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getStatusCode()I
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lru/cn/utils/http/HttpClient;->response:Lokhttp3/Response;

    invoke-virtual {v0}, Lokhttp3/Response;->code()I

    move-result v0

    return v0
.end method

.method public sendRequest(Ljava/lang/String;)V
    .locals 2
    .param p1, "requestUrl"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/URISyntaxException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 110
    sget-object v0, Lru/cn/utils/http/HttpClient$Method;->GET:Lru/cn/utils/http/HttpClient$Method;

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lru/cn/utils/http/HttpClient;->sendRequest(Ljava/lang/String;Lru/cn/utils/http/HttpClient$Method;Ljava/lang/String;)V

    .line 111
    return-void
.end method

.method public sendRequest(Ljava/lang/String;Lru/cn/utils/http/HttpClient$Method;Ljava/lang/String;)V
    .locals 2
    .param p1, "requestUrl"    # Ljava/lang/String;
    .param p2, "method"    # Lru/cn/utils/http/HttpClient$Method;
    .param p3, "body"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 115
    if-nez p2, :cond_0

    .line 116
    sget-object p2, Lru/cn/utils/http/HttpClient$Method;->GET:Lru/cn/utils/http/HttpClient$Method;

    .line 118
    :cond_0
    iput-object v1, p0, Lru/cn/utils/http/HttpClient;->response:Lokhttp3/Response;

    .line 119
    iput-object v1, p0, Lru/cn/utils/http/HttpClient;->content:Ljava/lang/String;

    .line 120
    invoke-direct {p0, p1, p2, p3}, Lru/cn/utils/http/HttpClient;->createRequest(Ljava/lang/String;Lru/cn/utils/http/HttpClient$Method;Ljava/lang/String;)Lokhttp3/Request;

    move-result-object v0

    .line 122
    .local v0, "request":Lokhttp3/Request;
    iget-object v1, p0, Lru/cn/utils/http/HttpClient;->httpClient:Lokhttp3/OkHttpClient;

    invoke-virtual {v1, v0}, Lokhttp3/OkHttpClient;->newCall(Lokhttp3/Request;)Lokhttp3/Call;

    move-result-object v1

    invoke-interface {v1}, Lokhttp3/Call;->execute()Lokhttp3/Response;

    move-result-object v1

    iput-object v1, p0, Lru/cn/utils/http/HttpClient;->response:Lokhttp3/Response;

    .line 123
    return-void
.end method

.method public setUserAgent(Ljava/lang/String;)Lru/cn/utils/http/HttpClient;
    .locals 0
    .param p1, "userAgent"    # Ljava/lang/String;

    .prologue
    .line 104
    iput-object p1, p0, Lru/cn/utils/http/HttpClient;->userAgent:Ljava/lang/String;

    .line 105
    return-object p0
.end method
