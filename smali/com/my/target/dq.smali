.class public final Lcom/my/target/dq;
.super Lcom/my/target/d;
.source "InterstitialAdResponseParser.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/my/target/d",
        "<",
        "Lcom/my/target/dv;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/my/target/d;-><init>()V

    .line 47
    return-void
.end method

.method public static newParser()Lcom/my/target/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/my/target/d",
            "<",
            "Lcom/my/target/dv;",
            ">;"
        }
    .end annotation

    .prologue
    .line 41
    new-instance v0, Lcom/my/target/dq;

    invoke-direct {v0}, Lcom/my/target/dq;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/String;Lcom/my/target/ae;Lcom/my/target/ak;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/ak;
    .locals 10

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 37
    check-cast p3, Lcom/my/target/dv;

    .line 1056
    invoke-static {p1}, Lcom/my/target/d;->isVast(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1172
    invoke-static {p4, p2, p5}, Lcom/my/target/bi;->a(Lcom/my/target/b;Lcom/my/target/ae;Landroid/content/Context;)Lcom/my/target/bi;

    move-result-object v2

    .line 1173
    invoke-virtual {v2, p1}, Lcom/my/target/bi;->F(Ljava/lang/String;)V

    .line 1175
    invoke-virtual {v2}, Lcom/my/target/bi;->as()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1186
    if-nez p3, :cond_0

    .line 1188
    invoke-static {}, Lcom/my/target/dv;->i()Lcom/my/target/dv;

    move-result-object p3

    .line 1190
    :cond_0
    invoke-virtual {p3, v3}, Lcom/my/target/dv;->setStyle(I)V

    .line 1191
    invoke-virtual {v2}, Lcom/my/target/bi;->as()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/aj;

    .line 1192
    invoke-static {}, Lcom/my/target/core/models/banners/h;->newBanner()Lcom/my/target/core/models/banners/h;

    move-result-object v1

    .line 1193
    invoke-virtual {v0}, Lcom/my/target/aj;->getCtaText()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/my/target/core/models/banners/h;->setCtaText(Ljava/lang/String;)V

    .line 1194
    invoke-virtual {v1, v0}, Lcom/my/target/core/models/banners/h;->setVideoBanner(Lcom/my/target/aj;)V

    .line 1195
    invoke-virtual {v1, v3}, Lcom/my/target/core/models/banners/h;->setStyle(I)V

    .line 1196
    invoke-virtual {p3}, Lcom/my/target/dv;->getVideoSettings()Lcom/my/target/am;

    move-result-object v3

    invoke-virtual {v3}, Lcom/my/target/am;->isAllowClose()Z

    move-result v3

    invoke-virtual {v1, v3}, Lcom/my/target/core/models/banners/h;->setAllowClose(Z)V

    .line 1197
    invoke-virtual {v0}, Lcom/my/target/aj;->getTrackingLink()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/my/target/core/models/banners/h;->setTrackingLink(Ljava/lang/String;)V

    .line 1198
    invoke-virtual {v0}, Lcom/my/target/aj;->getStatHolder()Lcom/my/target/ar;

    move-result-object v0

    const-string v3, "click"

    invoke-virtual {v0, v3}, Lcom/my/target/ar;->w(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1199
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/aq;

    .line 1201
    invoke-virtual {v1}, Lcom/my/target/core/models/banners/h;->getStatHolder()Lcom/my/target/ar;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/my/target/ar;->b(Lcom/my/target/aq;)V

    goto :goto_0

    .line 1203
    :cond_1
    invoke-virtual {v2}, Lcom/my/target/bi;->F()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p3, v0}, Lcom/my/target/dv;->e(Ljava/util/ArrayList;)V

    .line 1204
    invoke-virtual {p3, v1}, Lcom/my/target/dv;->a(Lcom/my/target/core/models/banners/d;)V

    .line 2091
    :cond_2
    :goto_1
    return-object p3

    .line 2070
    :cond_3
    invoke-virtual {p0, p1, p5}, Lcom/my/target/dq;->a(Ljava/lang/String;Landroid/content/Context;)Lorg/json/JSONObject;

    move-result-object v4

    .line 2071
    if-eqz v4, :cond_2

    .line 2076
    invoke-virtual {p4}, Lcom/my/target/b;->getFormat()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 2077
    if-eqz v0, :cond_2

    .line 2082
    if-nez p3, :cond_4

    .line 2084
    invoke-static {}, Lcom/my/target/dv;->i()Lcom/my/target/dv;

    move-result-object p3

    .line 2086
    :cond_4
    invoke-static {p2, p4, p5}, Lcom/my/target/core/parsers/e;->b(Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/core/parsers/e;

    move-result-object v2

    invoke-virtual {v2, v0, p3}, Lcom/my/target/core/parsers/e;->a(Lorg/json/JSONObject;Lcom/my/target/dv;)V

    .line 2088
    const-string v2, "banners"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 2089
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-lez v2, :cond_2

    .line 2094
    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    .line 2095
    if-eqz v5, :cond_2

    .line 2097
    const-string v0, "type"

    const-string v2, ""

    invoke-virtual {v5, v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 2098
    invoke-static {p3, p2, p4, p5}, Lcom/my/target/core/parsers/c;->a(Lcom/my/target/dv;Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/core/parsers/c;

    move-result-object v7

    .line 2102
    const/4 v0, 0x0

    .line 2104
    const/4 v2, -0x1

    invoke-virtual {v6}, Ljava/lang/String;->hashCode()I

    move-result v8

    sparse-switch v8, :sswitch_data_0

    :cond_5
    :goto_2
    packed-switch v2, :pswitch_data_0

    :goto_3
    move v9, v1

    move-object v1, v0

    move v0, v9

    .line 2146
    :goto_4
    if-eqz v0, :cond_2

    .line 2148
    invoke-virtual {p3, v1}, Lcom/my/target/dv;->a(Lcom/my/target/core/models/banners/d;)V

    goto :goto_1

    .line 2104
    :sswitch_0
    const-string v3, "additionalData"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    move v2, v1

    goto :goto_2

    :sswitch_1
    const-string v8, "fullscreen"

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    move v2, v3

    goto :goto_2

    :sswitch_2
    const-string v3, "banner"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v2, 0x2

    goto :goto_2

    :sswitch_3
    const-string v3, "promo"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v2, 0x3

    goto :goto_2

    :sswitch_4
    const-string v3, "html"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v2, 0x4

    goto :goto_2

    .line 2159
    :pswitch_0
    invoke-static {p2, p4, p5}, Lcom/my/target/bd;->a(Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/bd;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/my/target/bd;->c(Lorg/json/JSONObject;)Lcom/my/target/ae;

    move-result-object v2

    .line 2160
    if-eqz v2, :cond_6

    .line 2162
    invoke-virtual {p2, v2}, Lcom/my/target/ae;->b(Lcom/my/target/ae;)V

    :cond_6
    move v9, v1

    move-object v1, v0

    move v0, v9

    .line 2108
    goto :goto_4

    .line 2111
    :pswitch_1
    invoke-static {}, Lcom/my/target/core/models/banners/g;->newBanner()Lcom/my/target/core/models/banners/g;

    move-result-object v1

    .line 2112
    invoke-virtual {v7, v5, v1}, Lcom/my/target/core/parsers/c;->a(Lorg/json/JSONObject;Lcom/my/target/core/models/banners/g;)Z

    move-result v0

    goto :goto_4

    .line 2116
    :pswitch_2
    invoke-static {}, Lcom/my/target/core/models/banners/h;->newBanner()Lcom/my/target/core/models/banners/h;

    move-result-object v1

    .line 2117
    invoke-virtual {v7, v5, v1}, Lcom/my/target/core/parsers/c;->a(Lorg/json/JSONObject;Lcom/my/target/core/models/banners/h;)Z

    move-result v0

    goto :goto_4

    .line 2121
    :pswitch_3
    invoke-static {}, Lcom/my/target/core/models/banners/f;->newBanner()Lcom/my/target/core/models/banners/f;

    move-result-object v0

    .line 2122
    const-string v2, "mraid.js"

    invoke-virtual {v4, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v5, v0, v2}, Lcom/my/target/core/parsers/c;->a(Lorg/json/JSONObject;Lcom/my/target/core/models/banners/f;Ljava/lang/String;)Z

    move-result v2

    .line 2124
    const-string v3, "html_wrapper"

    invoke-virtual {v4, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 2126
    const-string v1, "html_wrapper"

    invoke-virtual {v4, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v1}, Lcom/my/target/dv;->setHtml(Ljava/lang/String;)V

    .line 2127
    const-string v1, "html_wrapper"

    invoke-virtual {v4, v1}, Lorg/json/JSONObject;->remove(Ljava/lang/String;)Ljava/lang/Object;

    .line 2128
    invoke-virtual {p3, v4}, Lcom/my/target/dv;->setRawData(Lorg/json/JSONObject;)V

    move-object v1, v0

    move v0, v2

    goto/16 :goto_4

    .line 2132
    :cond_7
    const-string v2, "Section has no HTML_WRAPPER field required for HTML banner"

    .line 2133
    const-string v3, "Required field"

    invoke-static {v3}, Lcom/my/target/az;->y(Ljava/lang/String;)Lcom/my/target/az;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/my/target/az;->z(Ljava/lang/String;)Lcom/my/target/az;

    move-result-object v2

    .line 2134
    invoke-virtual {p4}, Lcom/my/target/b;->getSlotId()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/my/target/az;->h(I)Lcom/my/target/az;

    move-result-object v2

    .line 2135
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/d;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/my/target/az;->B(Ljava/lang/String;)Lcom/my/target/az;

    move-result-object v2

    .line 2136
    invoke-virtual {p2}, Lcom/my/target/ae;->getUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/my/target/az;->A(Ljava/lang/String;)Lcom/my/target/az;

    move-result-object v2

    .line 2137
    invoke-virtual {v2, p5}, Lcom/my/target/az;->e(Landroid/content/Context;)V

    goto/16 :goto_3

    .line 2104
    nop

    :sswitch_data_0
    .sparse-switch
        -0x533a80d4 -> :sswitch_2
        -0x3a150f8f -> :sswitch_0
        0x3107ab -> :sswitch_4
        0x65fc90f -> :sswitch_3
        0x68f7bbb -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
