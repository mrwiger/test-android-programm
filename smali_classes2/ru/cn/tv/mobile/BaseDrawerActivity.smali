.class public abstract Lru/cn/tv/mobile/BaseDrawerActivity;
.super Lru/cn/tv/FullScreenActivity;
.source "BaseDrawerActivity.java"


# instance fields
.field protected drawerLayout:Landroid/support/v4/widget/DrawerLayout;

.field private isDrawerClosed:Z

.field protected mDrawerToggle:Landroid/support/v7/app/ActionBarDrawerToggle;

.field protected navigationView:Landroid/support/design/widget/NavigationView;

.field private receiver:Lru/cn/network/NetworkChangeReceiver;

.field private rubricMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lru/cn/api/catalogue/replies/Rubric;",
            ">;"
        }
    .end annotation
.end field

.field private rubrics:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lru/cn/api/catalogue/replies/Rubric;",
            ">;"
        }
    .end annotation
.end field

.field private title:Ljava/lang/String;

.field protected toolbar:Landroid/support/v7/widget/Toolbar;

.field protected viewModel:Lru/cn/tv/mobile/DrawerViewModel;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Lru/cn/tv/FullScreenActivity;-><init>()V

    .line 56
    const/4 v0, 0x1

    iput-boolean v0, p0, Lru/cn/tv/mobile/BaseDrawerActivity;->isDrawerClosed:Z

    .line 59
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lru/cn/tv/mobile/BaseDrawerActivity;->rubrics:Ljava/util/List;

    .line 60
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lru/cn/tv/mobile/BaseDrawerActivity;->rubricMap:Ljava/util/Map;

    return-void
.end method

.method private buildDrawerMenu(Lru/cn/api/catalogue/replies/Rubricator;Z)V
    .locals 12
    .param p1, "rubricator"    # Lru/cn/api/catalogue/replies/Rubricator;
    .param p2, "hasIntersections"    # Z

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 281
    if-nez p1, :cond_1

    .line 339
    :cond_0
    return-void

    .line 285
    :cond_1
    iget-object v7, p1, Lru/cn/api/catalogue/replies/Rubricator;->rubrics:Ljava/util/List;

    iput-object v7, p0, Lru/cn/tv/mobile/BaseDrawerActivity;->rubrics:Ljava/util/List;

    .line 286
    iget-object v7, p0, Lru/cn/tv/mobile/BaseDrawerActivity;->rubrics:Ljava/util/List;

    invoke-static {v7}, Lcom/annimon/stream/Stream;->of(Ljava/lang/Iterable;)Lcom/annimon/stream/Stream;

    move-result-object v7

    sget-object v10, Lru/cn/tv/mobile/BaseDrawerActivity$$Lambda$4;->$instance:Lcom/annimon/stream/function/Predicate;

    .line 287
    invoke-virtual {v7, v10}, Lcom/annimon/stream/Stream;->filter(Lcom/annimon/stream/function/Predicate;)Lcom/annimon/stream/Stream;

    move-result-object v7

    sget-object v10, Lru/cn/tv/mobile/BaseDrawerActivity$$Lambda$5;->$instance:Lcom/annimon/stream/function/Function;

    .line 295
    invoke-static {}, Lcom/annimon/stream/function/UnaryOperator$Util;->identity()Lcom/annimon/stream/function/UnaryOperator;

    move-result-object v11

    .line 288
    invoke-static {v10, v11}, Lcom/annimon/stream/Collectors;->toMap(Lcom/annimon/stream/function/Function;Lcom/annimon/stream/function/Function;)Lcom/annimon/stream/Collector;

    move-result-object v10

    invoke-virtual {v7, v10}, Lcom/annimon/stream/Stream;->collect(Lcom/annimon/stream/Collector;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/Map;

    iput-object v7, p0, Lru/cn/tv/mobile/BaseDrawerActivity;->rubricMap:Ljava/util/Map;

    .line 297
    invoke-virtual {p1}, Lru/cn/api/catalogue/replies/Rubricator;->getSearchRubric()Lru/cn/api/catalogue/replies/Rubric;

    move-result-object v5

    .line 298
    .local v5, "searchRubric":Lru/cn/api/catalogue/replies/Rubric;
    if-eqz v5, :cond_2

    .line 299
    iget-wide v10, v5, Lru/cn/api/catalogue/replies/Rubric;->id:J

    invoke-virtual {p0, v10, v11}, Lru/cn/tv/mobile/BaseDrawerActivity;->onSearchRubricAvailable(J)V

    .line 302
    :cond_2
    iget-object v7, p0, Lru/cn/tv/mobile/BaseDrawerActivity;->navigationView:Landroid/support/design/widget/NavigationView;

    invoke-virtual {v7}, Landroid/support/design/widget/NavigationView;->getMenu()Landroid/view/Menu;

    move-result-object v2

    .line 304
    .local v2, "menu":Landroid/view/Menu;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {v2}, Landroid/view/Menu;->size()I

    move-result v7

    if-ge v1, v7, :cond_0

    .line 305
    invoke-interface {v2, v1}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v3

    .line 306
    .local v3, "menuItem":Landroid/view/MenuItem;
    invoke-interface {v3}, Landroid/view/MenuItem;->getItemId()I

    move-result v7

    packed-switch v7, :pswitch_data_0

    .line 304
    :goto_1
    :pswitch_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 308
    :pswitch_1
    iget-object v7, p0, Lru/cn/tv/mobile/BaseDrawerActivity;->rubricMap:Ljava/util/Map;

    const v10, 0x7f0900ae

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-interface {v7, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lru/cn/api/catalogue/replies/Rubric;

    .line 309
    .local v6, "topRubric":Lru/cn/api/catalogue/replies/Rubric;
    if-eqz v6, :cond_3

    .line 310
    iget-object v7, v6, Lru/cn/api/catalogue/replies/Rubric;->title:Ljava/lang/String;

    invoke-interface {v3, v7}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 313
    :cond_3
    if-eqz v6, :cond_4

    move v7, v8

    :goto_2
    invoke-interface {v3, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_1

    :cond_4
    move v7, v9

    goto :goto_2

    .line 317
    .end local v6    # "topRubric":Lru/cn/api/catalogue/replies/Rubric;
    :pswitch_2
    invoke-virtual {p0}, Lru/cn/tv/mobile/BaseDrawerActivity;->getRubricsWithoutHint()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-lez v7, :cond_5

    move v7, v8

    :goto_3
    invoke-interface {v3, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_1

    :cond_5
    move v7, v9

    goto :goto_3

    .line 321
    :pswitch_3
    iget-object v7, p0, Lru/cn/tv/mobile/BaseDrawerActivity;->rubricMap:Ljava/util/Map;

    const v10, 0x7f0900a9

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-interface {v7, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/cn/api/catalogue/replies/Rubric;

    .line 322
    .local v4, "newsRubric":Lru/cn/api/catalogue/replies/Rubric;
    if-eqz v4, :cond_6

    .line 323
    iget-object v7, v4, Lru/cn/api/catalogue/replies/Rubric;->title:Ljava/lang/String;

    invoke-interface {v3, v7}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 326
    :cond_6
    if-eqz v4, :cond_7

    move v7, v8

    :goto_4
    invoke-interface {v3, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_1

    :cond_7
    move v7, v9

    goto :goto_4

    .line 330
    .end local v4    # "newsRubric":Lru/cn/api/catalogue/replies/Rubric;
    :pswitch_4
    invoke-interface {v3, p2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_1

    .line 334
    :pswitch_5
    const v7, 0x7f060095

    invoke-static {p0, v7}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v0

    .line 335
    .local v0, "accentColor":I
    invoke-interface {v3}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v7

    sget-object v10, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v7, v0, v10}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    goto :goto_1

    .line 306
    nop

    :pswitch_data_0
    .packed-switch 0x7f0900a4
        :pswitch_2
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static final synthetic lambda$buildDrawerMenu$4$BaseDrawerActivity(Lru/cn/api/catalogue/replies/Rubric;)Z
    .locals 2
    .param p0, "rubric"    # Lru/cn/api/catalogue/replies/Rubric;

    .prologue
    .line 287
    iget-object v0, p0, Lru/cn/api/catalogue/replies/Rubric;->uiHint:Lru/cn/api/catalogue/replies/Rubric$UiHintType;

    sget-object v1, Lru/cn/api/catalogue/replies/Rubric$UiHintType;->TOP:Lru/cn/api/catalogue/replies/Rubric$UiHintType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lru/cn/api/catalogue/replies/Rubric;->uiHint:Lru/cn/api/catalogue/replies/Rubric$UiHintType;

    sget-object v1, Lru/cn/api/catalogue/replies/Rubric$UiHintType;->STORIES:Lru/cn/api/catalogue/replies/Rubric$UiHintType;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static final synthetic lambda$buildDrawerMenu$5$BaseDrawerActivity(Lru/cn/api/catalogue/replies/Rubric;)Ljava/lang/Integer;
    .locals 2
    .param p0, "rubric"    # Lru/cn/api/catalogue/replies/Rubric;

    .prologue
    .line 289
    iget-object v0, p0, Lru/cn/api/catalogue/replies/Rubric;->uiHint:Lru/cn/api/catalogue/replies/Rubric$UiHintType;

    sget-object v1, Lru/cn/api/catalogue/replies/Rubric$UiHintType;->TOP:Lru/cn/api/catalogue/replies/Rubric$UiHintType;

    if-ne v0, v1, :cond_0

    .line 290
    const v0, 0x7f0900ae

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 294
    :goto_0
    return-object v0

    .line 291
    :cond_0
    iget-object v0, p0, Lru/cn/api/catalogue/replies/Rubric;->uiHint:Lru/cn/api/catalogue/replies/Rubric$UiHintType;

    sget-object v1, Lru/cn/api/catalogue/replies/Rubric$UiHintType;->STORIES:Lru/cn/api/catalogue/replies/Rubric$UiHintType;

    if-ne v0, v1, :cond_1

    .line 292
    const v0, 0x7f0900a9

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 294
    :cond_1
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method static final synthetic lambda$getRubricsWithoutHint$3$BaseDrawerActivity(Lru/cn/api/catalogue/replies/Rubric;)Z
    .locals 1
    .param p0, "rubric"    # Lru/cn/api/catalogue/replies/Rubric;

    .prologue
    .line 231
    iget-object v0, p0, Lru/cn/api/catalogue/replies/Rubric;->uiHint:Lru/cn/api/catalogue/replies/Rubric$UiHintType;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected backPressed()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 244
    invoke-virtual {p0}, Lru/cn/tv/mobile/BaseDrawerActivity;->isFullScreen()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 245
    invoke-virtual {p0}, Lru/cn/tv/mobile/BaseDrawerActivity;->toggleFullScreen()Z

    .line 252
    :goto_0
    return v0

    .line 247
    :cond_0
    iget-boolean v1, p0, Lru/cn/tv/mobile/BaseDrawerActivity;->isDrawerClosed:Z

    if-nez v1, :cond_1

    .line 248
    iget-object v1, p0, Lru/cn/tv/mobile/BaseDrawerActivity;->drawerLayout:Landroid/support/v4/widget/DrawerLayout;

    invoke-virtual {v1}, Landroid/support/v4/widget/DrawerLayout;->closeDrawers()V

    goto :goto_0

    .line 252
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected drawerClosed()V
    .locals 2

    .prologue
    .line 256
    const/4 v0, 0x1

    iput-boolean v0, p0, Lru/cn/tv/mobile/BaseDrawerActivity;->isDrawerClosed:Z

    .line 257
    invoke-virtual {p0}, Lru/cn/tv/mobile/BaseDrawerActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 258
    invoke-virtual {p0}, Lru/cn/tv/mobile/BaseDrawerActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    invoke-virtual {p0}, Lru/cn/tv/mobile/BaseDrawerActivity;->title()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 260
    :cond_0
    return-void
.end method

.method protected drawerOpened()V
    .locals 3

    .prologue
    .line 263
    const/4 v1, 0x0

    iput-boolean v1, p0, Lru/cn/tv/mobile/BaseDrawerActivity;->isDrawerClosed:Z

    .line 264
    invoke-virtual {p0}, Lru/cn/tv/mobile/BaseDrawerActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 265
    invoke-virtual {p0}, Lru/cn/tv/mobile/BaseDrawerActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v1

    const v2, 0x7f0e002b

    invoke-virtual {v1, v2}, Landroid/support/v7/app/ActionBar;->setTitle(I)V

    .line 268
    :cond_0
    iget-object v1, p0, Lru/cn/tv/mobile/BaseDrawerActivity;->navigationView:Landroid/support/design/widget/NavigationView;

    invoke-virtual {v1}, Landroid/support/design/widget/NavigationView;->getMenu()Landroid/view/Menu;

    move-result-object v1

    const v2, 0x7f0900ab

    invoke-interface {v1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 269
    .local v0, "adSubscribeItem":Landroid/view/MenuItem;
    invoke-static {}, Lru/cn/domain/PurchaseManager;->isAdDisabled()Z

    move-result v1

    if-nez v1, :cond_1

    .line 270
    const v1, 0x7f0e0119

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 274
    :goto_0
    return-void

    .line 272
    :cond_1
    const v1, 0x7f0e0118

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public fullScreen(Z)V
    .locals 2
    .param p1, "f"    # Z

    .prologue
    .line 204
    invoke-super {p0, p1}, Lru/cn/tv/FullScreenActivity;->fullScreen(Z)V

    .line 205
    if-eqz p1, :cond_0

    .line 206
    iget-object v0, p0, Lru/cn/tv/mobile/BaseDrawerActivity;->drawerLayout:Landroid/support/v4/widget/DrawerLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->setDrawerLockMode(I)V

    .line 210
    :goto_0
    return-void

    .line 208
    :cond_0
    iget-object v0, p0, Lru/cn/tv/mobile/BaseDrawerActivity;->drawerLayout:Landroid/support/v4/widget/DrawerLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->setDrawerLockMode(I)V

    goto :goto_0
.end method

.method protected getRubric(I)Lru/cn/api/catalogue/replies/Rubric;
    .locals 2
    .param p1, "menuId"    # I

    .prologue
    .line 213
    iget-object v0, p0, Lru/cn/tv/mobile/BaseDrawerActivity;->rubricMap:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/api/catalogue/replies/Rubric;

    return-object v0
.end method

.method protected getRubricsWithoutHint()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lru/cn/api/catalogue/replies/Rubric;",
            ">;"
        }
    .end annotation

    .prologue
    .line 230
    iget-object v0, p0, Lru/cn/tv/mobile/BaseDrawerActivity;->rubrics:Ljava/util/List;

    invoke-static {v0}, Lcom/annimon/stream/Stream;->of(Ljava/lang/Iterable;)Lcom/annimon/stream/Stream;

    move-result-object v0

    sget-object v1, Lru/cn/tv/mobile/BaseDrawerActivity$$Lambda$3;->$instance:Lcom/annimon/stream/function/Predicate;

    .line 231
    invoke-virtual {v0, v1}, Lcom/annimon/stream/Stream;->filter(Lcom/annimon/stream/function/Predicate;)Lcom/annimon/stream/Stream;

    move-result-object v0

    .line 232
    invoke-virtual {v0}, Lcom/annimon/stream/Stream;->toList()Ljava/util/List;

    move-result-object v0

    .line 230
    return-object v0
.end method

.method final synthetic lambda$onCreate$0$BaseDrawerActivity(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 118
    iget-object v0, p0, Lru/cn/tv/mobile/BaseDrawerActivity;->mDrawerToggle:Landroid/support/v7/app/ActionBarDrawerToggle;

    invoke-virtual {v0}, Landroid/support/v7/app/ActionBarDrawerToggle;->isDrawerIndicatorEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 119
    invoke-virtual {p0}, Lru/cn/tv/mobile/BaseDrawerActivity;->onBackPressed()V

    .line 123
    :goto_0
    return-void

    .line 121
    :cond_0
    iget-object v0, p0, Lru/cn/tv/mobile/BaseDrawerActivity;->drawerLayout:Landroid/support/v4/widget/DrawerLayout;

    const v1, 0x800003

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->openDrawer(I)V

    goto :goto_0
.end method

.method final synthetic lambda$onCreate$1$BaseDrawerActivity()V
    .locals 0

    .prologue
    .line 128
    invoke-virtual {p0}, Lru/cn/tv/mobile/BaseDrawerActivity;->onNetworkChange()V

    return-void
.end method

.method final synthetic lambda$onCreate$2$BaseDrawerActivity(Landroid/util/Pair;)V
    .locals 3
    .param p1, "pair"    # Landroid/util/Pair;

    .prologue
    .line 132
    if-eqz p1, :cond_0

    iget-object v2, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Lru/cn/api/catalogue/replies/Rubricator;

    move-object v1, v2

    .line 133
    .local v1, "rubricator":Lru/cn/api/catalogue/replies/Rubricator;
    :goto_0
    if-eqz p1, :cond_1

    iget-object v2, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v0, 0x1

    .line 135
    .local v0, "hasCameras":Z
    :goto_1
    invoke-direct {p0, v1, v0}, Lru/cn/tv/mobile/BaseDrawerActivity;->buildDrawerMenu(Lru/cn/api/catalogue/replies/Rubricator;Z)V

    .line 136
    return-void

    .line 132
    .end local v0    # "hasCameras":Z
    .end local v1    # "rubricator":Lru/cn/api/catalogue/replies/Rubricator;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 133
    .restart local v1    # "rubricator":Lru/cn/api/catalogue/replies/Rubricator;
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected abstract layout()I
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 165
    invoke-super {p0, p1}, Lru/cn/tv/FullScreenActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 166
    iget-object v0, p0, Lru/cn/tv/mobile/BaseDrawerActivity;->mDrawerToggle:Landroid/support/v7/app/ActionBarDrawerToggle;

    invoke-virtual {v0, p1}, Landroid/support/v7/app/ActionBarDrawerToggle;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 167
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x1

    .line 70
    invoke-super {p0, p1}, Lru/cn/tv/FullScreenActivity;->onCreate(Landroid/os/Bundle;)V

    .line 71
    invoke-virtual {p0}, Lru/cn/tv/mobile/BaseDrawerActivity;->layout()I

    move-result v7

    .line 72
    .local v7, "layout":I
    if-nez v7, :cond_0

    .line 73
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Layout not set"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 75
    :cond_0
    invoke-virtual {p0, v7}, Lru/cn/tv/mobile/BaseDrawerActivity;->setContentView(I)V

    .line 77
    const v0, 0x7f0900a7

    invoke-virtual {p0, v0}, Lru/cn/tv/mobile/BaseDrawerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/DrawerLayout;

    iput-object v0, p0, Lru/cn/tv/mobile/BaseDrawerActivity;->drawerLayout:Landroid/support/v4/widget/DrawerLayout;

    .line 78
    iget-object v0, p0, Lru/cn/tv/mobile/BaseDrawerActivity;->drawerLayout:Landroid/support/v4/widget/DrawerLayout;

    if-nez v0, :cond_1

    .line 79
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "No DrawerLayout"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 82
    :cond_1
    invoke-virtual {p0}, Lru/cn/tv/mobile/BaseDrawerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e002b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lru/cn/tv/mobile/BaseDrawerActivity;->title:Ljava/lang/String;

    .line 84
    const v0, 0x7f0901e2

    invoke-virtual {p0, v0}, Lru/cn/tv/mobile/BaseDrawerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p0, Lru/cn/tv/mobile/BaseDrawerActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    .line 85
    const v0, 0x7f0901e0

    invoke-virtual {p0, v0}, Lru/cn/tv/mobile/BaseDrawerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/NavigationView;

    iput-object v0, p0, Lru/cn/tv/mobile/BaseDrawerActivity;->navigationView:Landroid/support/design/widget/NavigationView;

    .line 86
    iget-object v0, p0, Lru/cn/tv/mobile/BaseDrawerActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p0, v0}, Lru/cn/tv/mobile/BaseDrawerActivity;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 87
    invoke-virtual {p0}, Lru/cn/tv/mobile/BaseDrawerActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/support/v7/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 88
    invoke-virtual {p0}, Lru/cn/tv/mobile/BaseDrawerActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 90
    new-instance v0, Lru/cn/tv/mobile/BaseDrawerActivity$1;

    iget-object v3, p0, Lru/cn/tv/mobile/BaseDrawerActivity;->drawerLayout:Landroid/support/v4/widget/DrawerLayout;

    iget-object v4, p0, Lru/cn/tv/mobile/BaseDrawerActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    const v5, 0x7f0e018a

    const v6, 0x7f0e0189

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v6}, Lru/cn/tv/mobile/BaseDrawerActivity$1;-><init>(Lru/cn/tv/mobile/BaseDrawerActivity;Landroid/app/Activity;Landroid/support/v4/widget/DrawerLayout;Landroid/support/v7/widget/Toolbar;II)V

    iput-object v0, p0, Lru/cn/tv/mobile/BaseDrawerActivity;->mDrawerToggle:Landroid/support/v7/app/ActionBarDrawerToggle;

    .line 116
    iget-object v0, p0, Lru/cn/tv/mobile/BaseDrawerActivity;->drawerLayout:Landroid/support/v4/widget/DrawerLayout;

    iget-object v1, p0, Lru/cn/tv/mobile/BaseDrawerActivity;->mDrawerToggle:Landroid/support/v7/app/ActionBarDrawerToggle;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/DrawerLayout;->addDrawerListener(Landroid/support/v4/widget/DrawerLayout$DrawerListener;)V

    .line 117
    iget-object v0, p0, Lru/cn/tv/mobile/BaseDrawerActivity;->mDrawerToggle:Landroid/support/v7/app/ActionBarDrawerToggle;

    new-instance v1, Lru/cn/tv/mobile/BaseDrawerActivity$$Lambda$0;

    invoke-direct {v1, p0}, Lru/cn/tv/mobile/BaseDrawerActivity$$Lambda$0;-><init>(Lru/cn/tv/mobile/BaseDrawerActivity;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBarDrawerToggle;->setToolbarNavigationClickListener(Landroid/view/View$OnClickListener;)V

    .line 125
    iget-object v0, p0, Lru/cn/tv/mobile/BaseDrawerActivity;->navigationView:Landroid/support/design/widget/NavigationView;

    const v1, 0x7f0900a3

    invoke-virtual {v0, v1}, Landroid/support/design/widget/NavigationView;->setCheckedItem(I)V

    .line 127
    new-instance v0, Lru/cn/network/NetworkChangeReceiver;

    invoke-direct {v0}, Lru/cn/network/NetworkChangeReceiver;-><init>()V

    iput-object v0, p0, Lru/cn/tv/mobile/BaseDrawerActivity;->receiver:Lru/cn/network/NetworkChangeReceiver;

    .line 128
    iget-object v0, p0, Lru/cn/tv/mobile/BaseDrawerActivity;->receiver:Lru/cn/network/NetworkChangeReceiver;

    new-instance v1, Lru/cn/tv/mobile/BaseDrawerActivity$$Lambda$1;

    invoke-direct {v1, p0}, Lru/cn/tv/mobile/BaseDrawerActivity$$Lambda$1;-><init>(Lru/cn/tv/mobile/BaseDrawerActivity;)V

    invoke-virtual {v0, v1}, Lru/cn/network/NetworkChangeReceiver;->setListener(Lru/cn/network/NetworkChangeReceiver$NetworkChangeListener;)V

    .line 130
    invoke-static {}, Lru/cn/tv/App;->scope()Ltoothpick/Scope;

    move-result-object v0

    const-class v1, Lru/cn/tv/mobile/DrawerViewModel;

    invoke-static {p0, v0, v1}, Lru/cn/mvvm/ViewModels;->get(Landroid/support/v4/app/FragmentActivity;Ltoothpick/Scope;Ljava/lang/Class;)Landroid/arch/lifecycle/ViewModel;

    move-result-object v0

    check-cast v0, Lru/cn/tv/mobile/DrawerViewModel;

    iput-object v0, p0, Lru/cn/tv/mobile/BaseDrawerActivity;->viewModel:Lru/cn/tv/mobile/DrawerViewModel;

    .line 131
    iget-object v0, p0, Lru/cn/tv/mobile/BaseDrawerActivity;->viewModel:Lru/cn/tv/mobile/DrawerViewModel;

    invoke-virtual {v0}, Lru/cn/tv/mobile/DrawerViewModel;->menuItems()Landroid/arch/lifecycle/LiveData;

    move-result-object v0

    new-instance v1, Lru/cn/tv/mobile/BaseDrawerActivity$$Lambda$2;

    invoke-direct {v1, p0}, Lru/cn/tv/mobile/BaseDrawerActivity$$Lambda$2;-><init>(Lru/cn/tv/mobile/BaseDrawerActivity;)V

    invoke-virtual {v0, p0, v1}, Landroid/arch/lifecycle/LiveData;->observe(Landroid/arch/lifecycle/LifecycleOwner;Landroid/arch/lifecycle/Observer;)V

    .line 137
    return-void
.end method

.method protected onNetworkChange()V
    .locals 0

    .prologue
    .line 278
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 6
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 171
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 194
    iget-object v4, p0, Lru/cn/tv/mobile/BaseDrawerActivity;->mDrawerToggle:Landroid/support/v7/app/ActionBarDrawerToggle;

    invoke-virtual {v4, p1}, Landroid/support/v7/app/ActionBarDrawerToggle;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 195
    const/4 v4, 0x1

    .line 199
    :goto_0
    return v4

    .line 173
    :pswitch_0
    new-instance v2, Landroid/content/Intent;

    const-class v4, Lru/cn/tv/playlists/UserPlaylistsActivity;

    invoke-direct {v2, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 174
    .local v2, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v2}, Lru/cn/tv/mobile/BaseDrawerActivity;->startActivity(Landroid/content/Intent;)V

    .line 199
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_0
    :goto_1
    invoke-super {p0, p1}, Lru/cn/tv/FullScreenActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v4

    goto :goto_0

    .line 179
    :pswitch_1
    :try_start_0
    invoke-static {}, Lru/cn/utils/FeedbackBuilder;->create()Lru/cn/utils/FeedbackBuilder;

    move-result-object v4

    .line 180
    invoke-virtual {v4, p0}, Lru/cn/utils/FeedbackBuilder;->setContext(Landroid/content/Context;)Lru/cn/utils/FeedbackBuilder;

    move-result-object v4

    .line 181
    invoke-virtual {v4}, Lru/cn/utils/FeedbackBuilder;->build()Landroid/content/Intent;

    move-result-object v1

    .line 183
    .local v1, "emailIntent":Landroid/content/Intent;
    const v4, 0x7f0e0072

    invoke-virtual {p0, v4}, Lru/cn/tv/mobile/BaseDrawerActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {p0, v4}, Lru/cn/tv/mobile/BaseDrawerActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 184
    .end local v1    # "emailIntent":Landroid/content/Intent;
    :catch_0
    move-exception v0

    .line 185
    .local v0, "e":Ljava/lang/Exception;
    const v4, 0x7f0e00b8

    .line 186
    invoke-virtual {p0, v4}, Lru/cn/tv/mobile/BaseDrawerActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    const/4 v5, 0x0

    .line 185
    invoke-static {p0, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    .line 188
    .local v3, "toast":Landroid/widget/Toast;
    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 171
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lru/cn/tv/mobile/BaseDrawerActivity;->receiver:Lru/cn/network/NetworkChangeReceiver;

    invoke-virtual {p0, v0}, Lru/cn/tv/mobile/BaseDrawerActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 160
    invoke-super {p0}, Lru/cn/tv/FullScreenActivity;->onPause()V

    .line 161
    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 141
    invoke-super {p0, p1}, Lru/cn/tv/FullScreenActivity;->onPostCreate(Landroid/os/Bundle;)V

    .line 142
    iget-object v0, p0, Lru/cn/tv/mobile/BaseDrawerActivity;->mDrawerToggle:Landroid/support/v7/app/ActionBarDrawerToggle;

    invoke-virtual {v0}, Landroid/support/v7/app/ActionBarDrawerToggle;->syncState()V

    .line 143
    invoke-virtual {p0}, Lru/cn/tv/mobile/BaseDrawerActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 144
    invoke-virtual {p0}, Lru/cn/tv/mobile/BaseDrawerActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    iget-object v1, p0, Lru/cn/tv/mobile/BaseDrawerActivity;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 146
    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 150
    invoke-super {p0}, Lru/cn/tv/FullScreenActivity;->onResume()V

    .line 152
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 153
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 154
    iget-object v1, p0, Lru/cn/tv/mobile/BaseDrawerActivity;->receiver:Lru/cn/network/NetworkChangeReceiver;

    invoke-virtual {p0, v1, v0}, Lru/cn/tv/mobile/BaseDrawerActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 155
    return-void
.end method

.method protected abstract onSearchRubricAvailable(J)V
.end method

.method protected setDefaultTitle(I)V
    .locals 1
    .param p1, "title"    # I

    .prologue
    .line 236
    invoke-virtual {p0}, Lru/cn/tv/mobile/BaseDrawerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lru/cn/tv/mobile/BaseDrawerActivity;->setDefaultTitle(Ljava/lang/String;)V

    .line 237
    return-void
.end method

.method protected setDefaultTitle(Ljava/lang/String;)V
    .locals 2
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 217
    iput-object p1, p0, Lru/cn/tv/mobile/BaseDrawerActivity;->title:Ljava/lang/String;

    .line 218
    iget-boolean v0, p0, Lru/cn/tv/mobile/BaseDrawerActivity;->isDrawerClosed:Z

    if-eqz v0, :cond_1

    .line 219
    invoke-virtual {p0}, Lru/cn/tv/mobile/BaseDrawerActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 220
    invoke-virtual {p0}, Lru/cn/tv/mobile/BaseDrawerActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    invoke-virtual {p0}, Lru/cn/tv/mobile/BaseDrawerActivity;->title()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 227
    :cond_0
    :goto_0
    return-void

    .line 223
    :cond_1
    invoke-virtual {p0}, Lru/cn/tv/mobile/BaseDrawerActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 224
    invoke-virtual {p0}, Lru/cn/tv/mobile/BaseDrawerActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    const v1, 0x7f0e002b

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setTitle(I)V

    goto :goto_0
.end method

.method protected title()Ljava/lang/String;
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lru/cn/tv/mobile/BaseDrawerActivity;->title:Ljava/lang/String;

    return-object v0
.end method
