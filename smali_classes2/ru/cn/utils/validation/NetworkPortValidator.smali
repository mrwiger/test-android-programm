.class public Lru/cn/utils/validation/NetworkPortValidator;
.super Ljava/lang/Object;
.source "NetworkPortValidator.java"

# interfaces
.implements Lru/cn/utils/validation/Validator;


# instance fields
.field private errorDescription:Ljava/lang/String;

.field private portString:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "portString"    # Ljava/lang/String;
    .param p2, "errorDescription"    # Ljava/lang/String;

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lru/cn/utils/validation/NetworkPortValidator;->portString:Ljava/lang/String;

    .line 15
    iput-object p2, p0, Lru/cn/utils/validation/NetworkPortValidator;->errorDescription:Ljava/lang/String;

    .line 16
    return-void
.end method


# virtual methods
.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lru/cn/utils/validation/NetworkPortValidator;->errorDescription:Ljava/lang/String;

    return-object v0
.end method

.method public validate()Z
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v3, 0x0

    .line 22
    :try_start_0
    iget-object v4, p0, Lru/cn/utils/validation/NetworkPortValidator;->portString:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 26
    .local v1, "port":I
    new-instance v2, Lru/cn/utils/validation/CompositeValidator;

    const/4 v4, 0x2

    new-array v4, v4, [Lru/cn/utils/validation/Validator;

    new-instance v5, Lru/cn/utils/validation/MaxValueValidator;

    .line 27
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const v7, 0xffff

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    iget-object v8, p0, Lru/cn/utils/validation/NetworkPortValidator;->errorDescription:Ljava/lang/String;

    invoke-direct {v5, v6, v7, v8}, Lru/cn/utils/validation/MaxValueValidator;-><init>(Ljava/lang/Number;Ljava/lang/Number;Ljava/lang/String;)V

    aput-object v5, v4, v3

    new-instance v3, Lru/cn/utils/validation/MinValueValidator;

    .line 28
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    iget-object v7, p0, Lru/cn/utils/validation/NetworkPortValidator;->errorDescription:Ljava/lang/String;

    invoke-direct {v3, v5, v6, v7}, Lru/cn/utils/validation/MinValueValidator;-><init>(Ljava/lang/Number;Ljava/lang/Number;Ljava/lang/String;)V

    aput-object v3, v4, v9

    invoke-direct {v2, v4}, Lru/cn/utils/validation/CompositeValidator;-><init>([Lru/cn/utils/validation/Validator;)V

    .line 29
    .local v2, "validator":Lru/cn/utils/validation/Validator;
    invoke-interface {v2}, Lru/cn/utils/validation/Validator;->validate()Z

    move-result v3

    .end local v1    # "port":I
    .end local v2    # "validator":Lru/cn/utils/validation/Validator;
    :goto_0
    return v3

    .line 23
    :catch_0
    move-exception v0

    .line 24
    .local v0, "e":Ljava/lang/NumberFormatException;
    goto :goto_0
.end method
