.class Lcom/yandex/metrica/impl/be$c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/yandex/metrica/impl/ai$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/yandex/metrica/impl/be;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "c"
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;)V
    .locals 6

    .prologue
    .line 151
    invoke-static {p1}, Lcom/yandex/metrica/impl/ob/cq;->a(Landroid/content/Context;)Lcom/yandex/metrica/impl/ob/cq;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/cq;->b()Lcom/yandex/metrica/impl/ob/cr;

    move-result-object v0

    .line 152
    new-instance v1, Lcom/yandex/metrica/impl/ob/df;

    invoke-direct {v1, v0}, Lcom/yandex/metrica/impl/ob/df;-><init>(Lcom/yandex/metrica/impl/ob/cr;)V

    .line 1202
    new-instance v0, Lcom/yandex/metrica/impl/ob/fl;

    invoke-direct {v0, p1}, Lcom/yandex/metrica/impl/ob/fl;-><init>(Landroid/content/Context;)V

    .line 1203
    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fl;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1204
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/yandex/metrica/impl/ob/df;->a(Z)V

    .line 1205
    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fl;->b()V

    .line 2193
    :cond_0
    new-instance v0, Lcom/yandex/metrica/impl/ob/fj;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, p1, v2}, Lcom/yandex/metrica/impl/ob/fj;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 2194
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/yandex/metrica/impl/ob/fj;->a(I)J

    move-result-wide v2

    .line 2195
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-eqz v4, :cond_1

    .line 2196
    invoke-virtual {v1, v2, v3}, Lcom/yandex/metrica/impl/ob/df;->a(J)Lcom/yandex/metrica/impl/ob/df;

    .line 2198
    :cond_1
    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fj;->a()V

    .line 3176
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/yandex/metrica/impl/ob/t;->a(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/t;

    move-result-object v0

    .line 3177
    new-instance v2, Lcom/yandex/metrica/impl/ob/fh;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/t;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, p1, v0}, Lcom/yandex/metrica/impl/ob/fh;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 3179
    invoke-virtual {v2}, Lcom/yandex/metrica/impl/ob/fh;->b()Lcom/yandex/metrica/CounterConfiguration$a;

    move-result-object v0

    .line 3180
    sget-object v3, Lcom/yandex/metrica/CounterConfiguration$a;->a:Lcom/yandex/metrica/CounterConfiguration$a;

    if-eq v0, v3, :cond_2

    .line 3181
    invoke-virtual {v1, v0}, Lcom/yandex/metrica/impl/ob/df;->a(Lcom/yandex/metrica/CounterConfiguration$a;)Lcom/yandex/metrica/impl/ob/df;

    .line 3184
    :cond_2
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Lcom/yandex/metrica/impl/ob/fh;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3185
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 3186
    invoke-virtual {v1, v0}, Lcom/yandex/metrica/impl/ob/df;->b(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/df;

    .line 3189
    :cond_3
    invoke-virtual {v2}, Lcom/yandex/metrica/impl/ob/fh;->d()Lcom/yandex/metrica/impl/ob/fh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fh;->c()Lcom/yandex/metrica/impl/ob/fh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fh;->j()V

    .line 158
    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/df;->g()V

    .line 160
    new-instance v0, Lcom/yandex/metrica/impl/ob/fc;

    invoke-direct {v0, p1}, Lcom/yandex/metrica/impl/ob/fc;-><init>(Landroid/content/Context;)V

    .line 161
    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fc;->a()V

    .line 162
    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fc;->b()V

    .line 4168
    new-instance v0, Lcom/yandex/metrica/impl/ob/dg;

    .line 4169
    invoke-static {p1}, Lcom/yandex/metrica/impl/ob/cq;->a(Landroid/content/Context;)Lcom/yandex/metrica/impl/ob/cq;

    move-result-object v1

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/cq;->d()Lcom/yandex/metrica/impl/ob/cr;

    move-result-object v1

    .line 4170
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/yandex/metrica/impl/ob/dg;-><init>(Lcom/yandex/metrica/impl/ob/cr;Ljava/lang/String;)V

    .line 4172
    invoke-static {}, Lcom/yandex/metrica/impl/ob/dl;->a()Lcom/yandex/metrica/impl/ob/dl;

    move-result-object v1

    const-string v2, ""

    invoke-virtual {v0, v2}, Lcom/yandex/metrica/impl/ob/dg;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, p1, v0}, Lcom/yandex/metrica/impl/ob/dl;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    .line 165
    return-void
.end method
