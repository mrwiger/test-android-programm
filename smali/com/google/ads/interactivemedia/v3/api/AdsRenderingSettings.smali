.class public interface abstract Lcom/google/ads/interactivemedia/v3/api/AdsRenderingSettings;
.super Ljava/lang/Object;
.source "IMASDK"


# virtual methods
.method public abstract getDisableUi()Z
.end method

.method public abstract isRenderCompanions()Z
.end method

.method public abstract setBitrateKbps(I)V
.end method

.method public abstract setMimeTypes(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation
.end method
