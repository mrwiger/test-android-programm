.class Lru/cn/ad/video/Wapstart/WapStartRequest$Impression$Video;
.super Ljava/lang/Object;
.source "WapStartRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/ad/video/Wapstart/WapStartRequest$Impression;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Video"
.end annotation


# instance fields
.field canSkip:Ljava/lang/Boolean;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "canskip"
    .end annotation
.end field

.field maxDuration:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "maxduration"
    .end annotation
.end field

.field final synthetic this$1:Lru/cn/ad/video/Wapstart/WapStartRequest$Impression;

.field vType:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "vtype"
    .end annotation
.end field


# direct methods
.method constructor <init>(Lru/cn/ad/video/Wapstart/WapStartRequest$Impression;IZLru/cn/ad/video/Wapstart/WapStartRequest$VideoType;)V
    .locals 1
    .param p1, "this$1"    # Lru/cn/ad/video/Wapstart/WapStartRequest$Impression;
    .param p2, "maxDuration"    # I
    .param p3, "canSkip"    # Z
    .param p4, "vType"    # Lru/cn/ad/video/Wapstart/WapStartRequest$VideoType;

    .prologue
    .line 73
    iput-object p1, p0, Lru/cn/ad/video/Wapstart/WapStartRequest$Impression$Video;->this$1:Lru/cn/ad/video/Wapstart/WapStartRequest$Impression;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    iput p2, p0, Lru/cn/ad/video/Wapstart/WapStartRequest$Impression$Video;->maxDuration:I

    .line 75
    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lru/cn/ad/video/Wapstart/WapStartRequest$Impression$Video;->canSkip:Ljava/lang/Boolean;

    .line 76
    invoke-virtual {p4}, Lru/cn/ad/video/Wapstart/WapStartRequest$VideoType;->getValue()I

    move-result v0

    iput v0, p0, Lru/cn/ad/video/Wapstart/WapStartRequest$Impression$Video;->vType:I

    .line 77
    return-void
.end method
