.class public Lru/cn/api/provider/cursor/TelecastItemCursor;
.super Lru/cn/api/provider/cursor/MatrixCursorEx;
.source "TelecastItemCursor.java"


# static fields
.field private static columnNames:[Ljava/lang/String;


# instance fields
.field private channel_idIndex:I

.field private channel_titleIndex:I

.field private descriptionIndex:I

.field private durationIndex:I

.field private hightlightIndex:I

.field private imageIndex:I

.field private isDenied:I

.field private telecastIdIndex:I

.field private timeIndex:I

.field private titleIndex:I

.field private viewsCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 5
    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "telecastId"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "time"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "image"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "channel_title"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "channel_id"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "is_denied"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "is_highlight"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "views_count"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "description"

    aput-object v2, v0, v1

    sput-object v0, Lru/cn/api/provider/cursor/TelecastItemCursor;->columnNames:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lru/cn/api/provider/cursor/TelecastItemCursor;->columnNames:[Ljava/lang/String;

    invoke-direct {p0, v0}, Lru/cn/api/provider/cursor/MatrixCursorEx;-><init>([Ljava/lang/String;)V

    .line 22
    const-string v0, "telecastId"

    invoke-virtual {p0, v0}, Lru/cn/api/provider/cursor/TelecastItemCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lru/cn/api/provider/cursor/TelecastItemCursor;->telecastIdIndex:I

    .line 23
    const-string v0, "time"

    invoke-virtual {p0, v0}, Lru/cn/api/provider/cursor/TelecastItemCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lru/cn/api/provider/cursor/TelecastItemCursor;->timeIndex:I

    .line 24
    const-string v0, "duration"

    invoke-virtual {p0, v0}, Lru/cn/api/provider/cursor/TelecastItemCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lru/cn/api/provider/cursor/TelecastItemCursor;->durationIndex:I

    .line 25
    const-string v0, "title"

    invoke-virtual {p0, v0}, Lru/cn/api/provider/cursor/TelecastItemCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lru/cn/api/provider/cursor/TelecastItemCursor;->titleIndex:I

    .line 26
    const-string v0, "image"

    invoke-virtual {p0, v0}, Lru/cn/api/provider/cursor/TelecastItemCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lru/cn/api/provider/cursor/TelecastItemCursor;->imageIndex:I

    .line 27
    const-string v0, "channel_title"

    invoke-virtual {p0, v0}, Lru/cn/api/provider/cursor/TelecastItemCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lru/cn/api/provider/cursor/TelecastItemCursor;->channel_titleIndex:I

    .line 28
    const-string v0, "channel_id"

    invoke-virtual {p0, v0}, Lru/cn/api/provider/cursor/TelecastItemCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lru/cn/api/provider/cursor/TelecastItemCursor;->channel_idIndex:I

    .line 29
    const-string v0, "is_denied"

    invoke-virtual {p0, v0}, Lru/cn/api/provider/cursor/TelecastItemCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lru/cn/api/provider/cursor/TelecastItemCursor;->isDenied:I

    .line 30
    const-string v0, "views_count"

    invoke-virtual {p0, v0}, Lru/cn/api/provider/cursor/TelecastItemCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lru/cn/api/provider/cursor/TelecastItemCursor;->viewsCount:I

    .line 31
    const-string v0, "description"

    invoke-virtual {p0, v0}, Lru/cn/api/provider/cursor/TelecastItemCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lru/cn/api/provider/cursor/TelecastItemCursor;->descriptionIndex:I

    .line 32
    const-string v0, "is_highlight"

    invoke-virtual {p0, v0}, Lru/cn/api/provider/cursor/TelecastItemCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lru/cn/api/provider/cursor/TelecastItemCursor;->hightlightIndex:I

    .line 33
    return-void
.end method


# virtual methods
.method public addRow(JJJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;JIZILjava/lang/String;)V
    .locals 3
    .param p1, "telecastId"    # J
    .param p3, "time"    # J
    .param p5, "duration"    # J
    .param p7, "title"    # Ljava/lang/String;
    .param p8, "image"    # Ljava/lang/String;
    .param p9, "channelTitle"    # Ljava/lang/String;
    .param p10, "channelId"    # J
    .param p12, "isDenied"    # I
    .param p13, "isHighlight"    # Z
    .param p14, "viewsCount"    # I
    .param p15, "description"    # Ljava/lang/String;

    .prologue
    .line 37
    const/16 v0, 0xc

    new-array v1, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x1

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x2

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x3

    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x4

    aput-object p7, v1, v0

    const/4 v0, 0x5

    aput-object p8, v1, v0

    const/4 v0, 0x6

    aput-object p9, v1, v0

    const/4 v0, 0x7

    .line 38
    invoke-static {p10, p11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v0

    const/16 v0, 0x8

    invoke-static {p12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    const/16 v2, 0x9

    if-eqz p13, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v1, v2

    const/16 v0, 0xa

    invoke-static/range {p14 .. p14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    const/16 v0, 0xb

    aput-object p15, v1, v0

    .line 37
    invoke-virtual {p0, v1}, Lru/cn/api/provider/cursor/TelecastItemCursor;->addRow([Ljava/lang/Object;)V

    .line 39
    return-void

    .line 38
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getChannelId()J
    .locals 2

    .prologue
    .line 66
    iget v0, p0, Lru/cn/api/provider/cursor/TelecastItemCursor;->channel_idIndex:I

    invoke-virtual {p0, v0}, Lru/cn/api/provider/cursor/TelecastItemCursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getChannelTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget v0, p0, Lru/cn/api/provider/cursor/TelecastItemCursor;->channel_titleIndex:I

    invoke-virtual {p0, v0}, Lru/cn/api/provider/cursor/TelecastItemCursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget v0, p0, Lru/cn/api/provider/cursor/TelecastItemCursor;->descriptionIndex:I

    invoke-virtual {p0, v0}, Lru/cn/api/provider/cursor/TelecastItemCursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDuration()J
    .locals 2

    .prologue
    .line 50
    iget v0, p0, Lru/cn/api/provider/cursor/TelecastItemCursor;->durationIndex:I

    invoke-virtual {p0, v0}, Lru/cn/api/provider/cursor/TelecastItemCursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getImage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget v0, p0, Lru/cn/api/provider/cursor/TelecastItemCursor;->imageIndex:I

    invoke-virtual {p0, v0}, Lru/cn/api/provider/cursor/TelecastItemCursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTelecastId()J
    .locals 2

    .prologue
    .line 42
    iget v0, p0, Lru/cn/api/provider/cursor/TelecastItemCursor;->telecastIdIndex:I

    invoke-virtual {p0, v0}, Lru/cn/api/provider/cursor/TelecastItemCursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getTime()J
    .locals 2

    .prologue
    .line 46
    iget v0, p0, Lru/cn/api/provider/cursor/TelecastItemCursor;->timeIndex:I

    invoke-virtual {p0, v0}, Lru/cn/api/provider/cursor/TelecastItemCursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lru/cn/api/provider/cursor/TelecastItemCursor;->titleIndex:I

    invoke-virtual {p0, v0}, Lru/cn/api/provider/cursor/TelecastItemCursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getViewsCount()I
    .locals 1

    .prologue
    .line 70
    iget v0, p0, Lru/cn/api/provider/cursor/TelecastItemCursor;->viewsCount:I

    invoke-virtual {p0, v0}, Lru/cn/api/provider/cursor/TelecastItemCursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public isPaid()Z
    .locals 1

    .prologue
    .line 74
    iget v0, p0, Lru/cn/api/provider/cursor/TelecastItemCursor;->isDenied:I

    invoke-virtual {p0, v0}, Lru/cn/api/provider/cursor/TelecastItemCursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
