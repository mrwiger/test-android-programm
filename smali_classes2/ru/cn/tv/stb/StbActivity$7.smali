.class Lru/cn/tv/stb/StbActivity$7;
.super Ljava/lang/Object;
.source "StbActivity.java"

# interfaces
.implements Lru/cn/tv/stb/collections/CollectionsFragment$CollectionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/tv/stb/StbActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/tv/stb/StbActivity;


# direct methods
.method constructor <init>(Lru/cn/tv/stb/StbActivity;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 322
    iput-object p1, p0, Lru/cn/tv/stb/StbActivity$7;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(JLandroid/database/Cursor;)V
    .locals 5
    .param p1, "rubricId"    # J
    .param p3, "item"    # Landroid/database/Cursor;

    .prologue
    .line 325
    check-cast p3, Landroid/database/CursorWrapper;

    .end local p3    # "item":Landroid/database/Cursor;
    invoke-virtual {p3}, Landroid/database/CursorWrapper;->getWrappedCursor()Landroid/database/Cursor;

    move-result-object v0

    check-cast v0, Lru/cn/api/provider/cursor/TelecastItemCursor;

    .line 327
    .local v0, "cursor":Lru/cn/api/provider/cursor/TelecastItemCursor;
    invoke-virtual {v0}, Lru/cn/api/provider/cursor/TelecastItemCursor;->isPaid()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 328
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$7;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-virtual {v0}, Lru/cn/api/provider/cursor/TelecastItemCursor;->getTitle()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lru/cn/tv/stb/StbActivity;->access$1000(Lru/cn/tv/stb/StbActivity;Ljava/lang/String;)V

    .line 341
    :goto_0
    return-void

    .line 332
    :cond_0
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$7;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$1100(Lru/cn/tv/stb/StbActivity;)V

    .line 334
    const/4 v1, 0x1

    const/4 v4, 0x0

    invoke-static {v1, v4, p1, p2}, Lru/cn/domain/statistics/inetra/InetraTracker;->setSessionParams(IIJ)V

    .line 339
    invoke-virtual {v0}, Lru/cn/api/provider/cursor/TelecastItemCursor;->getTelecastId()J

    move-result-wide v2

    .line 340
    .local v2, "telecastId":J
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$7;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$000(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/player/SimplePlayerFragment;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Lru/cn/tv/player/SimplePlayerFragment;->playTelecast(J)V

    goto :goto_0
.end method

.method public onScroll()V
    .locals 1

    .prologue
    .line 345
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$7;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$1200(Lru/cn/tv/stb/StbActivity;)V

    .line 346
    return-void
.end method
