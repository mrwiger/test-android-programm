.class public Lru/cn/player/timeshift/TimeshiftApiTask;
.super Lru/cn/player/timeshift/TimeshiftTask;
.source "TimeshiftApiTask.java"


# instance fields
.field private locatorApi:Lru/cn/api/medialocator/retrofit/MediaLocatorApi;

.field private result:Lru/cn/api/medialocator/replies/TimeshiftReply;


# direct methods
.method constructor <init>(Lru/cn/api/medialocator/retrofit/MediaLocatorApi;Lru/cn/api/iptv/replies/MediaLocation;Lru/cn/player/timeshift/TimeshiftTask$OnTaskCompletedListener;)V
    .locals 0
    .param p1, "locatorApi"    # Lru/cn/api/medialocator/retrofit/MediaLocatorApi;
    .param p2, "location"    # Lru/cn/api/iptv/replies/MediaLocation;
    .param p3, "callback"    # Lru/cn/player/timeshift/TimeshiftTask$OnTaskCompletedListener;

    .prologue
    .line 15
    invoke-direct {p0, p2, p3}, Lru/cn/player/timeshift/TimeshiftTask;-><init>(Lru/cn/api/iptv/replies/MediaLocation;Lru/cn/player/timeshift/TimeshiftTask$OnTaskCompletedListener;)V

    .line 16
    iput-object p1, p0, Lru/cn/player/timeshift/TimeshiftApiTask;->locatorApi:Lru/cn/api/medialocator/retrofit/MediaLocatorApi;

    .line 17
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 6
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 22
    const/4 v1, 0x0

    .line 23
    .local v1, "timeshiftCall":Lretrofit2/Call;, "Lretrofit2/Call<Lru/cn/api/medialocator/replies/TimeshiftReply;>;"
    :try_start_0
    iget-boolean v2, p0, Lru/cn/player/timeshift/TimeshiftApiTask;->startOver:Z

    if-eqz v2, :cond_1

    .line 24
    iget-object v2, p0, Lru/cn/player/timeshift/TimeshiftApiTask;->locatorApi:Lru/cn/api/medialocator/retrofit/MediaLocatorApi;

    iget-object v3, p0, Lru/cn/player/timeshift/TimeshiftApiTask;->mLocation:Lru/cn/api/iptv/replies/MediaLocation;

    iget-wide v4, v3, Lru/cn/api/iptv/replies/MediaLocation;->streamId:J

    const/4 v3, 0x1

    invoke-interface {v2, v4, v5, v3}, Lru/cn/api/medialocator/retrofit/MediaLocatorApi;->timeshift(JZ)Lretrofit2/Call;

    move-result-object v1

    .line 30
    :cond_0
    :goto_0
    invoke-interface {v1}, Lretrofit2/Call;->execute()Lretrofit2/Response;

    move-result-object v2

    .line 31
    invoke-virtual {v2}, Lretrofit2/Response;->body()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lru/cn/api/medialocator/replies/TimeshiftReply;

    iput-object v2, p0, Lru/cn/player/timeshift/TimeshiftApiTask;->result:Lru/cn/api/medialocator/replies/TimeshiftReply;

    .line 33
    iget-object v2, p0, Lru/cn/player/timeshift/TimeshiftApiTask;->result:Lru/cn/api/medialocator/replies/TimeshiftReply;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lru/cn/player/timeshift/TimeshiftApiTask;->result:Lru/cn/api/medialocator/replies/TimeshiftReply;

    iget-object v2, v2, Lru/cn/api/medialocator/replies/TimeshiftReply;->playbackUri:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 34
    iget-object v2, p0, Lru/cn/player/timeshift/TimeshiftApiTask;->result:Lru/cn/api/medialocator/replies/TimeshiftReply;

    iget v2, v2, Lru/cn/api/medialocator/replies/TimeshiftReply;->offset:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 41
    :goto_1
    return-object v2

    .line 25
    :cond_1
    iget v2, p0, Lru/cn/player/timeshift/TimeshiftApiTask;->offset:I

    if-lez v2, :cond_0

    .line 26
    iget-object v2, p0, Lru/cn/player/timeshift/TimeshiftApiTask;->locatorApi:Lru/cn/api/medialocator/retrofit/MediaLocatorApi;

    iget-object v3, p0, Lru/cn/player/timeshift/TimeshiftApiTask;->mLocation:Lru/cn/api/iptv/replies/MediaLocation;

    iget-wide v4, v3, Lru/cn/api/iptv/replies/MediaLocation;->streamId:J

    iget v3, p0, Lru/cn/player/timeshift/TimeshiftApiTask;->offset:I

    invoke-interface {v2, v4, v5, v3}, Lru/cn/api/medialocator/retrofit/MediaLocatorApi;->timeshift(JI)Lretrofit2/Call;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 36
    :catch_0
    move-exception v0

    .line 37
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {v0}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    .line 41
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_2
    invoke-super {p0, p1}, Lru/cn/player/timeshift/TimeshiftTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v2

    goto :goto_1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 9
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lru/cn/player/timeshift/TimeshiftApiTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 4
    .param p1, "offset"    # Ljava/lang/Integer;

    .prologue
    .line 46
    iget-object v1, p0, Lru/cn/player/timeshift/TimeshiftApiTask;->result:Lru/cn/api/medialocator/replies/TimeshiftReply;

    if-nez v1, :cond_0

    .line 47
    invoke-super {p0, p1}, Lru/cn/player/timeshift/TimeshiftTask;->onPostExecute(Ljava/lang/Integer;)V

    .line 58
    :goto_0
    return-void

    .line 51
    :cond_0
    iget-object v1, p0, Lru/cn/player/timeshift/TimeshiftApiTask;->result:Lru/cn/api/medialocator/replies/TimeshiftReply;

    iget v0, v1, Lru/cn/api/medialocator/replies/TimeshiftReply;->offset:I

    .line 53
    .local v0, "actualOffset":I
    if-nez v0, :cond_1

    .line 54
    iget v0, p0, Lru/cn/player/timeshift/TimeshiftApiTask;->offset:I

    .line 57
    :cond_1
    iget-object v1, p0, Lru/cn/player/timeshift/TimeshiftApiTask;->callback:Lru/cn/player/timeshift/TimeshiftTask$OnTaskCompletedListener;

    iget-object v2, p0, Lru/cn/player/timeshift/TimeshiftApiTask;->result:Lru/cn/api/medialocator/replies/TimeshiftReply;

    iget-object v2, v2, Lru/cn/api/medialocator/replies/TimeshiftReply;->playbackUri:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, p0, v2, v3}, Lru/cn/player/timeshift/TimeshiftTask$OnTaskCompletedListener;->onCompleted(Lru/cn/player/timeshift/TimeshiftTask;Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 9
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lru/cn/player/timeshift/TimeshiftApiTask;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method
