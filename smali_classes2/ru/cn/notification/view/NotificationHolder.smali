.class public Lru/cn/notification/view/NotificationHolder;
.super Lru/cn/notification/view/BaseNotificationsHolder;
.source "NotificationHolder.java"


# instance fields
.field private close:Landroid/widget/TextView;

.field private listener:Lru/cn/notification/NotificationButtonClickListener;

.field private message:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/View;Lru/cn/notification/NotificationButtonClickListener;)V
    .locals 1
    .param p1, "itemView"    # Landroid/view/View;
    .param p2, "listener"    # Lru/cn/notification/NotificationButtonClickListener;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lru/cn/notification/view/BaseNotificationsHolder;-><init>(Landroid/view/View;)V

    .line 17
    iput-object p2, p0, Lru/cn/notification/view/NotificationHolder;->listener:Lru/cn/notification/NotificationButtonClickListener;

    .line 18
    const v0, 0x7f090153

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lru/cn/notification/view/NotificationHolder;->message:Landroid/widget/TextView;

    .line 19
    const v0, 0x7f09004c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lru/cn/notification/view/NotificationHolder;->close:Landroid/widget/TextView;

    .line 20
    return-void
.end method


# virtual methods
.method final synthetic lambda$set$0$NotificationHolder(Lru/cn/notifications/entity/INotification;Landroid/view/View;)V
    .locals 1
    .param p1, "notification"    # Lru/cn/notifications/entity/INotification;
    .param p2, "v"    # Landroid/view/View;

    .prologue
    .line 24
    iget-object v0, p0, Lru/cn/notification/view/NotificationHolder;->listener:Lru/cn/notification/NotificationButtonClickListener;

    invoke-interface {v0, p1}, Lru/cn/notification/NotificationButtonClickListener;->pressClosed(Lru/cn/notifications/entity/INotification;)V

    return-void
.end method

.method public set(Lru/cn/notifications/entity/INotification;)V
    .locals 2
    .param p1, "notification"    # Lru/cn/notifications/entity/INotification;

    .prologue
    .line 23
    iget-object v0, p0, Lru/cn/notification/view/NotificationHolder;->message:Landroid/widget/TextView;

    invoke-interface {p1}, Lru/cn/notifications/entity/INotification;->message()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 24
    iget-object v0, p0, Lru/cn/notification/view/NotificationHolder;->close:Landroid/widget/TextView;

    new-instance v1, Lru/cn/notification/view/NotificationHolder$$Lambda$0;

    invoke-direct {v1, p0, p1}, Lru/cn/notification/view/NotificationHolder$$Lambda$0;-><init>(Lru/cn/notification/view/NotificationHolder;Lru/cn/notifications/entity/INotification;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 25
    return-void
.end method
