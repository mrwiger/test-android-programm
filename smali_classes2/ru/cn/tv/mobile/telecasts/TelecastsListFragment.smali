.class public Lru/cn/tv/mobile/telecasts/TelecastsListFragment;
.super Lru/cn/view/CustomListFragment;
.source "TelecastsListFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastsLoadedListener;,
        Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastSelectedListener;
    }
.end annotation


# instance fields
.field private adapter:Lru/cn/tv/mobile/telecasts/TelecastsAdapter;

.field private currentTelecastId:J

.field private footerView:Landroid/view/View;

.field private headerView:Landroid/view/View;

.field private list:Landroid/widget/ListView;

.field private needCheckCurrentTelecast:Z

.field noMoreData:Z

.field private onLoadListener:Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastsLoadedListener;

.field private onSelectedListener:Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastSelectedListener;

.field private progress:Landroid/widget/ProgressBar;

.field private uri:Landroid/net/Uri;

.field private viewModel:Lru/cn/tv/mobile/telecasts/TelecastsViewModel;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 22
    invoke-direct {p0}, Lru/cn/view/CustomListFragment;-><init>()V

    .line 42
    iput-boolean v2, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->noMoreData:Z

    .line 43
    const/4 v0, 0x0

    iput-object v0, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->uri:Landroid/net/Uri;

    .line 45
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->currentTelecastId:J

    .line 46
    iput-boolean v2, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->needCheckCurrentTelecast:Z

    return-void
.end method

.method static synthetic access$000(Lru/cn/tv/mobile/telecasts/TelecastsListFragment;)Lru/cn/tv/mobile/telecasts/TelecastsViewModel;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/mobile/telecasts/TelecastsListFragment;

    .prologue
    .line 22
    iget-object v0, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->viewModel:Lru/cn/tv/mobile/telecasts/TelecastsViewModel;

    return-object v0
.end method

.method static synthetic access$100(Lru/cn/tv/mobile/telecasts/TelecastsListFragment;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/mobile/telecasts/TelecastsListFragment;

    .prologue
    .line 22
    iget-object v0, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->list:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$200(Lru/cn/tv/mobile/telecasts/TelecastsListFragment;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/mobile/telecasts/TelecastsListFragment;

    .prologue
    .line 22
    iget-object v0, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->footerView:Landroid/view/View;

    return-object v0
.end method

.method private getItemPosition(J)I
    .locals 7
    .param p1, "telecastId"    # J

    .prologue
    const/4 v2, -0x1

    .line 185
    iget-object v3, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->adapter:Lru/cn/tv/mobile/telecasts/TelecastsAdapter;

    if-nez v3, :cond_1

    .line 195
    :cond_0
    :goto_0
    return v2

    .line 188
    :cond_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v3, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->adapter:Lru/cn/tv/mobile/telecasts/TelecastsAdapter;

    invoke-virtual {v3}, Lru/cn/tv/mobile/telecasts/TelecastsAdapter;->getCount()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 189
    iget-object v3, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->adapter:Lru/cn/tv/mobile/telecasts/TelecastsAdapter;

    .line 190
    invoke-virtual {v3, v1}, Lru/cn/tv/mobile/telecasts/TelecastsAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/api/provider/cursor/TelecastItemCursor;

    .line 191
    .local v0, "c":Lru/cn/api/provider/cursor/TelecastItemCursor;
    invoke-virtual {v0}, Lru/cn/api/provider/cursor/TelecastItemCursor;->getTelecastId()J

    move-result-wide v4

    cmp-long v3, p1, v4

    if-nez v3, :cond_2

    .line 192
    iget-object v2, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->list:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v2

    add-int/2addr v2, v1

    goto :goto_0

    .line 188
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public static newInstance(Landroid/net/Uri;)Lru/cn/tv/mobile/telecasts/TelecastsListFragment;
    .locals 3
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    .line 51
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 53
    .local v0, "args":Landroid/os/Bundle;
    new-instance v1, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;

    invoke-direct {v1}, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;-><init>()V

    .line 54
    .local v1, "fragment":Lru/cn/tv/mobile/telecasts/TelecastsListFragment;
    const-string v2, "uri"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 55
    invoke-virtual {v1, v0}, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->setArguments(Landroid/os/Bundle;)V

    .line 56
    return-object v1
.end method

.method private setTelecasts(Landroid/database/Cursor;)V
    .locals 6
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 199
    iget-object v3, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->progress:Landroid/widget/ProgressBar;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 201
    invoke-interface {p1}, Landroid/database/Cursor;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 202
    .local v1, "extras":Landroid/os/Bundle;
    if-eqz v1, :cond_0

    .line 203
    const-string v3, "no_more_data"

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->noMoreData:Z

    .line 206
    :cond_0
    iget-object v3, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->list:Landroid/widget/ListView;

    invoke-virtual {v3}, Landroid/widget/ListView;->getFooterViewsCount()I

    move-result v3

    if-lez v3, :cond_1

    .line 207
    iget-object v3, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->list:Landroid/widget/ListView;

    iget-object v4, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->footerView:Landroid/view/View;

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->removeFooterView(Landroid/view/View;)Z

    .line 210
    :cond_1
    check-cast p1, Landroid/database/CursorWrapper;

    .end local p1    # "cursor":Landroid/database/Cursor;
    invoke-virtual {p1}, Landroid/database/CursorWrapper;->getWrappedCursor()Landroid/database/Cursor;

    move-result-object v0

    check-cast v0, Lru/cn/api/provider/cursor/TelecastItemCursor;

    .line 211
    .local v0, "c":Lru/cn/api/provider/cursor/TelecastItemCursor;
    invoke-virtual {v0}, Lru/cn/api/provider/cursor/TelecastItemCursor;->getCount()I

    move-result v3

    if-lez v3, :cond_2

    .line 212
    iget-object v3, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->onLoadListener:Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastsLoadedListener;

    if-eqz v3, :cond_2

    .line 213
    iget-object v3, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->onLoadListener:Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastsLoadedListener;

    invoke-interface {v3}, Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastsLoadedListener;->onRecordsLoaded()V

    .line 217
    :cond_2
    iget-object v3, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->adapter:Lru/cn/tv/mobile/telecasts/TelecastsAdapter;

    invoke-virtual {v3, v0}, Lru/cn/tv/mobile/telecasts/TelecastsAdapter;->changeCursor(Landroid/database/Cursor;)V

    .line 219
    iget-boolean v3, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->needCheckCurrentTelecast:Z

    if-eqz v3, :cond_3

    .line 220
    iget-wide v4, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->currentTelecastId:J

    invoke-direct {p0, v4, v5}, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->getItemPosition(J)I

    move-result v2

    .line 221
    .local v2, "pos":I
    const/4 v3, -0x1

    if-eq v2, v3, :cond_3

    .line 222
    iget-object v3, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->list:Landroid/widget/ListView;

    const/4 v4, 0x1

    invoke-virtual {v3, v2, v4}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 223
    invoke-virtual {p0, v2}, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->selectPosition(I)V

    .line 226
    .end local v2    # "pos":I
    :cond_3
    return-void
.end method


# virtual methods
.method final bridge synthetic bridge$lambda$0$TelecastsListFragment(Landroid/database/Cursor;)V
    .locals 0

    invoke-direct {p0, p1}, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->setTelecasts(Landroid/database/Cursor;)V

    return-void
.end method

.method public checkItem(J)V
    .locals 5
    .param p1, "telecastId"    # J

    .prologue
    const/4 v4, 0x1

    .line 174
    iput-wide p1, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->currentTelecastId:J

    .line 175
    iget-wide v2, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->currentTelecastId:J

    invoke-direct {p0, v2, v3}, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->getItemPosition(J)I

    move-result v0

    .line 176
    .local v0, "pos":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 177
    iget-object v1, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->list:Landroid/widget/ListView;

    invoke-virtual {v1, v0, v4}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 178
    invoke-virtual {p0, v0}, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->selectPosition(I)V

    .line 182
    :goto_0
    return-void

    .line 180
    :cond_0
    iput-boolean v4, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->needCheckCurrentTelecast:Z

    goto :goto_0
.end method

.method protected getItemHeight()I
    .locals 1

    .prologue
    .line 158
    const/16 v0, 0x40

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 61
    invoke-super {p0, p1}, Lru/cn/view/CustomListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 63
    invoke-virtual {p0}, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 64
    .local v0, "arguments":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 65
    const-string v1, "uri"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    iput-object v1, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->uri:Landroid/net/Uri;

    .line 68
    :cond_0
    invoke-static {}, Lru/cn/tv/App;->scope()Ltoothpick/Scope;

    move-result-object v1

    const-class v2, Lru/cn/tv/mobile/telecasts/TelecastsViewModel;

    invoke-static {p0, v1, v2}, Lru/cn/mvvm/ViewModels;->get(Landroid/support/v4/app/Fragment;Ltoothpick/Scope;Ljava/lang/Class;)Landroid/arch/lifecycle/ViewModel;

    move-result-object v1

    check-cast v1, Lru/cn/tv/mobile/telecasts/TelecastsViewModel;

    iput-object v1, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->viewModel:Lru/cn/tv/mobile/telecasts/TelecastsViewModel;

    .line 69
    iget-object v1, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->viewModel:Lru/cn/tv/mobile/telecasts/TelecastsViewModel;

    invoke-virtual {v1}, Lru/cn/tv/mobile/telecasts/TelecastsViewModel;->telecasts()Landroid/arch/lifecycle/LiveData;

    move-result-object v1

    new-instance v2, Lru/cn/tv/mobile/telecasts/TelecastsListFragment$$Lambda$0;

    invoke-direct {v2, p0}, Lru/cn/tv/mobile/telecasts/TelecastsListFragment$$Lambda$0;-><init>(Lru/cn/tv/mobile/telecasts/TelecastsListFragment;)V

    invoke-virtual {v1, p0, v2}, Landroid/arch/lifecycle/LiveData;->observe(Landroid/arch/lifecycle/LifecycleOwner;Landroid/arch/lifecycle/Observer;)V

    .line 71
    iget-object v1, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->viewModel:Lru/cn/tv/mobile/telecasts/TelecastsViewModel;

    iget-object v2, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->uri:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Lru/cn/tv/mobile/telecasts/TelecastsViewModel;->setUri(Landroid/net/Uri;)V

    .line 72
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 77
    const v0, 0x7f0c00b6

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 6
    .param p1, "l"    # Landroid/widget/ListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 144
    invoke-super/range {p0 .. p5}, Lru/cn/view/CustomListFragment;->onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V

    .line 145
    iget-object v1, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->adapter:Lru/cn/tv/mobile/telecasts/TelecastsAdapter;

    iget-object v4, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->list:Landroid/widget/ListView;

    .line 146
    invoke-virtual {v4}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v4

    sub-int v4, p3, v4

    invoke-virtual {v1, v4}, Lru/cn/tv/mobile/telecasts/TelecastsAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/api/provider/cursor/TelecastItemCursor;

    .line 148
    .local v0, "c":Lru/cn/api/provider/cursor/TelecastItemCursor;
    invoke-virtual {v0}, Lru/cn/api/provider/cursor/TelecastItemCursor;->getTelecastId()J

    move-result-wide v2

    .line 150
    .local v2, "telecastId":J
    iget-object v1, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->list:Landroid/widget/ListView;

    iget-object v4, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->list:Landroid/widget/ListView;

    invoke-virtual {v4}, Landroid/widget/ListView;->getCheckedItemPosition()I

    move-result v4

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 151
    iget-object v1, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->onSelectedListener:Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastSelectedListener;

    if-eqz v1, :cond_0

    .line 152
    iget-object v1, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->onSelectedListener:Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastSelectedListener;

    invoke-interface {v1, v2, v3}, Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastSelectedListener;->onRecordSelected(J)V

    .line 154
    :cond_0
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 82
    invoke-super {p0, p1, p2}, Lru/cn/view/CustomListFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 84
    const v1, 0x7f090175

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->progress:Landroid/widget/ProgressBar;

    .line 85
    const v1, 0x102000a

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->list:Landroid/widget/ListView;

    .line 86
    iget-object v1, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->list:Landroid/widget/ListView;

    new-instance v2, Lru/cn/tv/mobile/telecasts/TelecastsListFragment$1;

    invoke-direct {v2, p0}, Lru/cn/tv/mobile/telecasts/TelecastsListFragment$1;-><init>(Lru/cn/tv/mobile/telecasts/TelecastsListFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 121
    iget-object v1, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->headerView:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 122
    iget-object v1, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->list:Landroid/widget/ListView;

    iget-object v2, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->headerView:Landroid/view/View;

    invoke-virtual {v1, v2, v3, v4}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 125
    :cond_0
    invoke-virtual {p0}, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 127
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v1, 0x7f0c00a0

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->footerView:Landroid/view/View;

    .line 130
    iget-object v1, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->list:Landroid/widget/ListView;

    iget-object v2, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->footerView:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;)V

    .line 132
    new-instance v1, Lru/cn/tv/mobile/telecasts/TelecastsAdapter;

    invoke-virtual {p0}, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const v3, 0x7f0c00b7

    invoke-direct {v1, v2, v3}, Lru/cn/tv/mobile/telecasts/TelecastsAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->adapter:Lru/cn/tv/mobile/telecasts/TelecastsAdapter;

    .line 133
    iget-object v1, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->adapter:Lru/cn/tv/mobile/telecasts/TelecastsAdapter;

    invoke-virtual {p0, v1}, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    .line 135
    iget-object v1, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->list:Landroid/widget/ListView;

    iget-object v2, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->footerView:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->removeFooterView(Landroid/view/View;)Z

    .line 137
    iget-object v1, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->uri:Landroid/net/Uri;

    if-eqz v1, :cond_1

    .line 138
    iget-object v1, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->progress:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 140
    :cond_1
    return-void
.end method

.method public setHeaderView(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 170
    iput-object p1, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->headerView:Landroid/view/View;

    .line 171
    return-void
.end method

.method public setOnLoadListener(Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastsLoadedListener;)V
    .locals 0
    .param p1, "onLoadListener"    # Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastsLoadedListener;

    .prologue
    .line 162
    iput-object p1, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->onLoadListener:Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastsLoadedListener;

    .line 163
    return-void
.end method

.method public setOnSelectedListener(Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastSelectedListener;)V
    .locals 0
    .param p1, "onSelectedListener"    # Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastSelectedListener;

    .prologue
    .line 166
    iput-object p1, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->onSelectedListener:Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastSelectedListener;

    .line 167
    return-void
.end method
