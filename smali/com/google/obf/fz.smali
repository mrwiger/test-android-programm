.class public final Lcom/google/obf/fz;
.super Lcom/google/obf/ew;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/obf/fz$a;,
        Lcom/google/obf/fz$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/obf/ew",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final a:Lcom/google/obf/et;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/obf/et",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/google/obf/el;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/obf/el",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final c:Lcom/google/obf/eg;

.field private final d:Lcom/google/obf/gd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/obf/gd",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final e:Lcom/google/obf/ex;

.field private final f:Lcom/google/obf/fz$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/obf/fz$a;"
        }
    .end annotation
.end field

.field private g:Lcom/google/obf/ew;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/obf/ew",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/obf/et;Lcom/google/obf/el;Lcom/google/obf/eg;Lcom/google/obf/gd;Lcom/google/obf/ex;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/obf/et",
            "<TT;>;",
            "Lcom/google/obf/el",
            "<TT;>;",
            "Lcom/google/obf/eg;",
            "Lcom/google/obf/gd",
            "<TT;>;",
            "Lcom/google/obf/ex;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1
    invoke-direct {p0}, Lcom/google/obf/ew;-><init>()V

    .line 2
    new-instance v0, Lcom/google/obf/fz$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/obf/fz$a;-><init>(Lcom/google/obf/fz;Lcom/google/obf/fz$1;)V

    iput-object v0, p0, Lcom/google/obf/fz;->f:Lcom/google/obf/fz$a;

    .line 3
    iput-object p1, p0, Lcom/google/obf/fz;->a:Lcom/google/obf/et;

    .line 4
    iput-object p2, p0, Lcom/google/obf/fz;->b:Lcom/google/obf/el;

    .line 5
    iput-object p3, p0, Lcom/google/obf/fz;->c:Lcom/google/obf/eg;

    .line 6
    iput-object p4, p0, Lcom/google/obf/fz;->d:Lcom/google/obf/gd;

    .line 7
    iput-object p5, p0, Lcom/google/obf/fz;->e:Lcom/google/obf/ex;

    .line 8
    return-void
.end method

.method private a()Lcom/google/obf/ew;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/obf/ew",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/obf/fz;->g:Lcom/google/obf/ew;

    .line 29
    if-eqz v0, :cond_0

    .line 30
    :goto_0
    return-object v0

    .line 29
    :cond_0
    iget-object v0, p0, Lcom/google/obf/fz;->c:Lcom/google/obf/eg;

    iget-object v1, p0, Lcom/google/obf/fz;->e:Lcom/google/obf/ex;

    iget-object v2, p0, Lcom/google/obf/fz;->d:Lcom/google/obf/gd;

    .line 30
    invoke-virtual {v0, v1, v2}, Lcom/google/obf/eg;->a(Lcom/google/obf/ex;Lcom/google/obf/gd;)Lcom/google/obf/ew;

    move-result-object v0

    iput-object v0, p0, Lcom/google/obf/fz;->g:Lcom/google/obf/ew;

    goto :goto_0
.end method

.method public static a(Lcom/google/obf/gd;Ljava/lang/Object;)Lcom/google/obf/ex;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/obf/gd",
            "<*>;",
            "Ljava/lang/Object;",
            ")",
            "Lcom/google/obf/ex;"
        }
    .end annotation

    .prologue
    .line 31
    new-instance v0, Lcom/google/obf/fz$b;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, p1, p0, v1, v2}, Lcom/google/obf/fz$b;-><init>(Ljava/lang/Object;Lcom/google/obf/gd;ZLjava/lang/Class;)V

    return-object v0
.end method

.method public static b(Lcom/google/obf/gd;Ljava/lang/Object;)Lcom/google/obf/ex;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/obf/gd",
            "<*>;",
            "Ljava/lang/Object;",
            ")",
            "Lcom/google/obf/ex;"
        }
    .end annotation

    .prologue
    .line 32
    invoke-virtual {p0}, Lcom/google/obf/gd;->b()Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/obf/gd;->a()Ljava/lang/Class;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    .line 33
    :goto_0
    new-instance v1, Lcom/google/obf/fz$b;

    const/4 v2, 0x0

    invoke-direct {v1, p1, p0, v0, v2}, Lcom/google/obf/fz$b;-><init>(Ljava/lang/Object;Lcom/google/obf/gd;ZLjava/lang/Class;)V

    return-object v1

    .line 32
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public read(Lcom/google/obf/ge;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/obf/ge;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 9
    iget-object v0, p0, Lcom/google/obf/fz;->b:Lcom/google/obf/el;

    if-nez v0, :cond_0

    .line 10
    invoke-direct {p0}, Lcom/google/obf/fz;->a()Lcom/google/obf/ew;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/obf/ew;->read(Lcom/google/obf/ge;)Ljava/lang/Object;

    move-result-object v0

    .line 14
    :goto_0
    return-object v0

    .line 11
    :cond_0
    invoke-static {p1}, Lcom/google/obf/fm;->a(Lcom/google/obf/ge;)Lcom/google/obf/em;

    move-result-object v0

    .line 12
    invoke-virtual {v0}, Lcom/google/obf/em;->j()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 13
    const/4 v0, 0x0

    goto :goto_0

    .line 14
    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/google/obf/fz;->b:Lcom/google/obf/el;

    iget-object v2, p0, Lcom/google/obf/fz;->d:Lcom/google/obf/gd;

    invoke-virtual {v2}, Lcom/google/obf/gd;->b()Ljava/lang/reflect/Type;

    move-result-object v2

    iget-object v3, p0, Lcom/google/obf/fz;->f:Lcom/google/obf/fz$a;

    invoke-interface {v1, v0, v2, v3}, Lcom/google/obf/el;->b(Lcom/google/obf/em;Ljava/lang/reflect/Type;Lcom/google/obf/ek;)Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/obf/eq; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    goto :goto_0

    .line 15
    :catch_0
    move-exception v0

    .line 16
    throw v0

    .line 17
    :catch_1
    move-exception v0

    .line 18
    new-instance v1, Lcom/google/obf/eq;

    invoke-direct {v1, v0}, Lcom/google/obf/eq;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public write(Lcom/google/obf/gg;Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/obf/gg;",
            "TT;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/obf/fz;->a:Lcom/google/obf/et;

    if-nez v0, :cond_0

    .line 20
    invoke-direct {p0}, Lcom/google/obf/fz;->a()Lcom/google/obf/ew;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/obf/ew;->write(Lcom/google/obf/gg;Ljava/lang/Object;)V

    .line 27
    :goto_0
    return-void

    .line 22
    :cond_0
    if-nez p2, :cond_1

    .line 23
    invoke-virtual {p1}, Lcom/google/obf/gg;->f()Lcom/google/obf/gg;

    goto :goto_0

    .line 25
    :cond_1
    iget-object v0, p0, Lcom/google/obf/fz;->a:Lcom/google/obf/et;

    iget-object v1, p0, Lcom/google/obf/fz;->d:Lcom/google/obf/gd;

    invoke-virtual {v1}, Lcom/google/obf/gd;->b()Ljava/lang/reflect/Type;

    move-result-object v1

    iget-object v2, p0, Lcom/google/obf/fz;->f:Lcom/google/obf/fz$a;

    invoke-interface {v0, p2, v1, v2}, Lcom/google/obf/et;->a(Ljava/lang/Object;Ljava/lang/reflect/Type;Lcom/google/obf/es;)Lcom/google/obf/em;

    move-result-object v0

    .line 26
    invoke-static {v0, p1}, Lcom/google/obf/fm;->a(Lcom/google/obf/em;Lcom/google/obf/gg;)V

    goto :goto_0
.end method
