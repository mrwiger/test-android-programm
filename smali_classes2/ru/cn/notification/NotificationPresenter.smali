.class public Lru/cn/notification/NotificationPresenter;
.super Ljava/lang/Object;
.source "NotificationPresenter.java"

# interfaces
.implements Lru/cn/notification/NotificationButtonClickListener;


# instance fields
.field private commandMapper:Lru/cn/notification/commands/CommandMapper;

.field private context:Landroid/content/Context;

.field private disposable:Lio/reactivex/disposables/CompositeDisposable;

.field private final repository:Lru/cn/notifications/INotificationsRepository;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lru/cn/notification/view/INotificationsView;Lru/cn/notifications/INotificationsRepository;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "view"    # Lru/cn/notification/view/INotificationsView;
    .param p3, "repository"    # Lru/cn/notifications/INotificationsRepository;

    .prologue
    const-wide/16 v4, 0x1

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Lru/cn/notification/commands/CommandMapper;

    invoke-direct {v0}, Lru/cn/notification/commands/CommandMapper;-><init>()V

    iput-object v0, p0, Lru/cn/notification/NotificationPresenter;->commandMapper:Lru/cn/notification/commands/CommandMapper;

    .line 23
    new-instance v0, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {v0}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object v0, p0, Lru/cn/notification/NotificationPresenter;->disposable:Lio/reactivex/disposables/CompositeDisposable;

    .line 26
    iput-object p1, p0, Lru/cn/notification/NotificationPresenter;->context:Landroid/content/Context;

    .line 27
    iput-object p3, p0, Lru/cn/notification/NotificationPresenter;->repository:Lru/cn/notifications/INotificationsRepository;

    .line 29
    invoke-static {}, Lru/cn/utils/Utils;->isTV()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 30
    iget-object v0, p0, Lru/cn/notification/NotificationPresenter;->disposable:Lio/reactivex/disposables/CompositeDisposable;

    invoke-interface {p3}, Lru/cn/notifications/INotificationsRepository;->notifications()Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lru/cn/notification/NotificationPresenter$$Lambda$0;->$instance:Lio/reactivex/functions/Function;

    .line 31
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->onErrorReturn(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v1

    .line 32
    invoke-virtual {v1}, Lio/reactivex/Observable;->retry()Lio/reactivex/Observable;

    move-result-object v1

    .line 33
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    .line 34
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {p2}, Lru/cn/notification/NotificationPresenter$$Lambda$1;->get$Lambda(Lru/cn/notification/view/INotificationsView;)Lio/reactivex/functions/Consumer;

    move-result-object v2

    .line 35
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 30
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 37
    iget-object v0, p0, Lru/cn/notification/NotificationPresenter;->disposable:Lio/reactivex/disposables/CompositeDisposable;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-static {v4, v5, v4, v5, v1}, Lio/reactivex/Observable;->interval(JJLjava/util/concurrent/TimeUnit;)Lio/reactivex/Observable;

    move-result-object v1

    new-instance v2, Lru/cn/notification/NotificationPresenter$$Lambda$2;

    invoke-direct {v2, p3}, Lru/cn/notification/NotificationPresenter$$Lambda$2;-><init>(Lru/cn/notifications/INotificationsRepository;)V

    .line 38
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v1

    .line 39
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    .line 40
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    .line 41
    invoke-virtual {v1}, Lio/reactivex/Observable;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 37
    invoke-virtual {v0, v1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 43
    :cond_0
    return-void
.end method

.method static final synthetic lambda$new$0$NotificationPresenter(Ljava/lang/Throwable;)Ljava/util/List;
    .locals 1
    .param p0, "it"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 31
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static final synthetic lambda$new$1$NotificationPresenter(Lru/cn/notifications/INotificationsRepository;Ljava/lang/Long;)Lio/reactivex/ObservableSource;
    .locals 1
    .param p0, "repository"    # Lru/cn/notifications/INotificationsRepository;
    .param p1, "it"    # Ljava/lang/Long;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 38
    invoke-interface {p0}, Lru/cn/notifications/INotificationsRepository;->update()Lio/reactivex/Single;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public destroy()V
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lru/cn/notification/NotificationPresenter;->disposable:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    .line 47
    return-void
.end method

.method public pressClosed(Lru/cn/notifications/entity/INotification;)V
    .locals 1
    .param p1, "notification"    # Lru/cn/notifications/entity/INotification;

    .prologue
    .line 51
    iget-object v0, p0, Lru/cn/notification/NotificationPresenter;->repository:Lru/cn/notifications/INotificationsRepository;

    invoke-interface {v0, p1}, Lru/cn/notifications/INotificationsRepository;->delete(Lru/cn/notifications/entity/INotification;)Lio/reactivex/Single;

    move-result-object v0

    invoke-virtual {v0}, Lio/reactivex/Single;->subscribe()Lio/reactivex/disposables/Disposable;

    .line 52
    return-void
.end method

.method public pressPositive(Lru/cn/notifications/entity/INotification;)V
    .locals 2
    .param p1, "notification"    # Lru/cn/notifications/entity/INotification;

    .prologue
    .line 56
    iget-object v1, p0, Lru/cn/notification/NotificationPresenter;->commandMapper:Lru/cn/notification/commands/CommandMapper;

    invoke-virtual {v1, p1}, Lru/cn/notification/commands/CommandMapper;->map(Lru/cn/notifications/entity/INotification;)Lru/cn/notification/commands/Command;

    move-result-object v0

    .line 57
    .local v0, "command":Lru/cn/notification/commands/Command;
    if-eqz v0, :cond_0

    .line 58
    iget-object v1, p0, Lru/cn/notification/NotificationPresenter;->context:Landroid/content/Context;

    invoke-interface {v0, v1}, Lru/cn/notification/commands/Command;->execute(Landroid/content/Context;)V

    .line 59
    iget-object v1, p0, Lru/cn/notification/NotificationPresenter;->repository:Lru/cn/notifications/INotificationsRepository;

    invoke-interface {v1, p1}, Lru/cn/notifications/INotificationsRepository;->delete(Lru/cn/notifications/entity/INotification;)Lio/reactivex/Single;

    move-result-object v1

    invoke-virtual {v1}, Lio/reactivex/Single;->subscribe()Lio/reactivex/disposables/Disposable;

    .line 61
    :cond_0
    return-void
.end method
