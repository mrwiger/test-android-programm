.class final Lcom/my/target/ew$b;
.super Lcom/my/target/er;
.source "RecyclerVerticalView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/my/target/ew;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/my/target/er",
        "<",
        "Lcom/my/target/ew$a;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Ljava/util/List;Landroid/content/Context;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/my/target/core/models/banners/e;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .prologue
    .line 121
    invoke-direct {p0, p1, p2}, Lcom/my/target/er;-><init>(Ljava/util/List;Landroid/content/Context;)V

    .line 122
    return-void
.end method


# virtual methods
.method public final synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/16 v7, 0x8

    .line 117
    check-cast p1, Lcom/my/target/ew$a;

    .line 2133
    invoke-virtual {p1}, Lcom/my/target/ew$a;->O()Lcom/my/target/el;

    move-result-object v1

    .line 3064
    iget-object v0, p0, Lcom/my/target/er;->interstitialAdCards:Ljava/util/List;

    .line 2134
    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/core/models/banners/e;

    .line 3150
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/e;->getImage()Lcom/my/target/common/models/ImageData;

    move-result-object v2

    .line 3151
    if-eqz v2, :cond_0

    .line 3153
    invoke-virtual {v1}, Lcom/my/target/el;->getCacheImageView()Lcom/my/target/bv;

    move-result-object v3

    .line 3154
    invoke-virtual {v2}, Lcom/my/target/common/models/ImageData;->getWidth()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/my/target/bv;->setPlaceholderWidth(I)V

    .line 3155
    invoke-virtual {v2}, Lcom/my/target/common/models/ImageData;->getHeight()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/my/target/bv;->setPlaceholderHeight(I)V

    .line 3156
    invoke-static {v2, v3}, Lcom/my/target/ch;->a(Lcom/my/target/common/models/ImageData;Landroid/widget/ImageView;)V

    .line 3159
    :cond_0
    invoke-virtual {v1}, Lcom/my/target/el;->getTitleTextView()Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v0}, Lcom/my/target/core/models/banners/e;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3160
    invoke-virtual {v1}, Lcom/my/target/el;->getDescriptionTextView()Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v0}, Lcom/my/target/core/models/banners/e;->getDescription()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 3161
    invoke-virtual {v1}, Lcom/my/target/el;->getCtaButtonView()Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v0}, Lcom/my/target/core/models/banners/e;->getCtaText()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 3163
    invoke-virtual {v1}, Lcom/my/target/el;->getDomainTextView()Landroid/widget/TextView;

    move-result-object v2

    .line 3164
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/e;->getDomain()Ljava/lang/String;

    move-result-object v3

    .line 3165
    invoke-virtual {v1}, Lcom/my/target/el;->getRatingView()Lcom/my/target/ca;

    move-result-object v4

    .line 3166
    const-string v5, "web"

    invoke-virtual {v0}, Lcom/my/target/core/models/banners/e;->getNavigationType()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 3168
    invoke-virtual {v4, v7}, Lcom/my/target/ca;->setVisibility(I)V

    .line 3169
    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 3170
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2136
    :goto_0
    iget-object v2, p0, Lcom/my/target/ew$b;->dl:Landroid/view/View$OnClickListener;

    invoke-virtual {v0}, Lcom/my/target/core/models/banners/e;->getClickArea()Lcom/my/target/af;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/my/target/el;->a(Landroid/view/View$OnClickListener;Lcom/my/target/af;)V

    .line 2137
    invoke-virtual {v1}, Lcom/my/target/el;->getCtaButtonView()Landroid/widget/Button;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/ew$b;->dm:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 117
    return-void

    .line 3174
    :cond_1
    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 3175
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/e;->getRating()F

    move-result v2

    .line 3176
    const/4 v3, 0x0

    cmpl-float v3, v2, v3

    if-lez v3, :cond_2

    .line 3178
    invoke-virtual {v4, v8}, Lcom/my/target/ca;->setVisibility(I)V

    .line 3179
    invoke-virtual {v4, v2}, Lcom/my/target/ca;->setRating(F)V

    goto :goto_0

    .line 3183
    :cond_2
    invoke-virtual {v4, v7}, Lcom/my/target/ca;->setVisibility(I)V

    goto :goto_0
.end method

.method public final synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 3

    .prologue
    .line 117
    .line 4127
    new-instance v0, Lcom/my/target/ew$a;

    new-instance v1, Lcom/my/target/el;

    iget-object v2, p0, Lcom/my/target/ew$b;->context:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/my/target/el;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, v1}, Lcom/my/target/ew$a;-><init>(Lcom/my/target/el;)V

    .line 117
    return-object v0
.end method

.method public final synthetic onViewRecycled(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 117
    check-cast p1, Lcom/my/target/ew$a;

    .line 1143
    invoke-virtual {p1}, Lcom/my/target/ew$a;->O()Lcom/my/target/el;

    move-result-object v0

    .line 1144
    invoke-virtual {v0, v1, v1}, Lcom/my/target/el;->a(Landroid/view/View$OnClickListener;Lcom/my/target/af;)V

    .line 1145
    invoke-virtual {v0}, Lcom/my/target/el;->getCtaButtonView()Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 117
    return-void
.end method
