.class Lcom/google/obf/ie;
.super Ljava/lang/Object;
.source "IMASDK"

# interfaces
.implements Lcom/google/obf/im;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a([BZ)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2
    if-eqz p2, :cond_0

    const/16 v0, 0xb

    .line 3
    :goto_0
    invoke-static {p1, v0}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2
    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Z)[B
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 4
    if-eqz p2, :cond_0

    const/16 v0, 0xb

    .line 5
    :goto_0
    invoke-static {p1, v0}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    return-object v0

    .line 4
    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method
