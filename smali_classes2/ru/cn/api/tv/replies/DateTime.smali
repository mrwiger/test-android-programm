.class public final Lru/cn/api/tv/replies/DateTime;
.super Ljava/lang/Object;
.source "DateTime.java"


# instance fields
.field public day:I

.field public hour:I

.field public minute:I

.field public month:I

.field public second:I

.field public timezone:I

.field public year:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public toCalendar()Ljava/util/Calendar;
    .locals 7

    .prologue
    .line 22
    iget v0, p0, Lru/cn/api/tv/replies/DateTime;->year:I

    iget v1, p0, Lru/cn/api/tv/replies/DateTime;->month:I

    iget v2, p0, Lru/cn/api/tv/replies/DateTime;->day:I

    iget v3, p0, Lru/cn/api/tv/replies/DateTime;->hour:I

    iget v4, p0, Lru/cn/api/tv/replies/DateTime;->minute:I

    iget v5, p0, Lru/cn/api/tv/replies/DateTime;->second:I

    iget v6, p0, Lru/cn/api/tv/replies/DateTime;->timezone:I

    invoke-static/range {v0 .. v6}, Lru/cn/utils/Utils;->getCalendar(IIIIIII)Ljava/util/Calendar;

    move-result-object v0

    return-object v0
.end method

.method public toSeconds()J
    .locals 4

    .prologue
    .line 18
    invoke-virtual {p0}, Lru/cn/api/tv/replies/DateTime;->toCalendar()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    return-wide v0
.end method
