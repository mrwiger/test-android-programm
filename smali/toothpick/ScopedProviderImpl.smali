.class public Ltoothpick/ScopedProviderImpl;
.super Ltoothpick/InternalProviderImpl;
.source "ScopedProviderImpl.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ltoothpick/InternalProviderImpl",
        "<TT;>;"
    }
.end annotation


# instance fields
.field protected scope:Ltoothpick/Scope;


# direct methods
.method public constructor <init>(Ltoothpick/Scope;Ljava/lang/Class;ZZZ)V
    .locals 0
    .param p1, "scope"    # Ltoothpick/Scope;
    .param p3, "isProviderFactoryClass"    # Z
    .param p4, "isCreatingSingletonInScope"    # Z
    .param p5, "isProvidingSingletonInScope"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ltoothpick/Scope;",
            "Ljava/lang/Class",
            "<*>;ZZZ)V"
        }
    .end annotation

    .prologue
    .line 21
    .local p0, "this":Ltoothpick/ScopedProviderImpl;, "Ltoothpick/ScopedProviderImpl<TT;>;"
    .local p2, "factoryKeyClass":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-direct {p0, p2, p3, p4, p5}, Ltoothpick/InternalProviderImpl;-><init>(Ljava/lang/Class;ZZZ)V

    .line 25
    iput-object p1, p0, Ltoothpick/ScopedProviderImpl;->scope:Ltoothpick/Scope;

    .line 26
    return-void
.end method

.method public constructor <init>(Ltoothpick/Scope;Ltoothpick/Factory;Z)V
    .locals 0
    .param p1, "scope"    # Ltoothpick/Scope;
    .param p3, "isProviderFactory"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ltoothpick/Scope;",
            "Ltoothpick/Factory",
            "<*>;Z)V"
        }
    .end annotation

    .prologue
    .line 12
    .local p0, "this":Ltoothpick/ScopedProviderImpl;, "Ltoothpick/ScopedProviderImpl<TT;>;"
    .local p2, "factory":Ltoothpick/Factory;, "Ltoothpick/Factory<*>;"
    invoke-direct {p0, p2, p3}, Ltoothpick/InternalProviderImpl;-><init>(Ltoothpick/Factory;Z)V

    .line 14
    iput-object p1, p0, Ltoothpick/ScopedProviderImpl;->scope:Ltoothpick/Scope;

    .line 15
    return-void
.end method


# virtual methods
.method public get(Ltoothpick/Scope;)Ljava/lang/Object;
    .locals 1
    .param p1, "scope"    # Ltoothpick/Scope;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ltoothpick/Scope;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 31
    .local p0, "this":Ltoothpick/ScopedProviderImpl;, "Ltoothpick/ScopedProviderImpl<TT;>;"
    iget-object v0, p0, Ltoothpick/ScopedProviderImpl;->scope:Ltoothpick/Scope;

    invoke-super {p0, v0}, Ltoothpick/InternalProviderImpl;->get(Ltoothpick/Scope;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
