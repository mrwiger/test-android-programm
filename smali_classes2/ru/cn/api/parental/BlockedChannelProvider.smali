.class public final Lru/cn/api/parental/BlockedChannelProvider;
.super Landroid/content/ContentProvider;
.source "BlockedChannelProvider.java"


# instance fields
.field private channelRepo:Lru/cn/api/parental/BlockedChannelRepo;

.field private final uriMatcher:Landroid/content/UriMatcher;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 12
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 17
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    iput-object v0, p0, Lru/cn/api/parental/BlockedChannelProvider;->uriMatcher:Landroid/content/UriMatcher;

    return-void
.end method

.method private notifyChange()V
    .locals 3

    .prologue
    .line 93
    invoke-virtual {p0}, Lru/cn/api/parental/BlockedChannelProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 94
    invoke-virtual {p0}, Lru/cn/api/parental/BlockedChannelProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {}, Lru/cn/api/parental/BlockedChannelProviderContract;->blockedChannelsUri()Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 96
    :cond_0
    return-void
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 4
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 75
    iget-object v1, p0, Lru/cn/api/parental/BlockedChannelProvider;->uriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 81
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Wrong URI: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 77
    :pswitch_0
    iget-object v1, p0, Lru/cn/api/parental/BlockedChannelProvider;->channelRepo:Lru/cn/api/parental/BlockedChannelRepo;

    invoke-virtual {v1, p3}, Lru/cn/api/parental/BlockedChannelRepo;->delete([Ljava/lang/String;)I

    move-result v0

    .line 78
    .local v0, "result":I
    invoke-direct {p0}, Lru/cn/api/parental/BlockedChannelProvider;->notifyChange()V

    .line 79
    return v0

    .line 75
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 57
    const/4 v0, 0x0

    return-object v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 63
    iget-object v0, p0, Lru/cn/api/parental/BlockedChannelProvider;->uriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 69
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Wrong URI: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 65
    :pswitch_0
    iget-object v0, p0, Lru/cn/api/parental/BlockedChannelProvider;->channelRepo:Lru/cn/api/parental/BlockedChannelRepo;

    invoke-virtual {v0, p2}, Lru/cn/api/parental/BlockedChannelRepo;->insert(Landroid/content/ContentValues;)V

    .line 66
    invoke-direct {p0}, Lru/cn/api/parental/BlockedChannelProvider;->notifyChange()V

    .line 67
    return-object p1

    .line 63
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate()Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 21
    new-instance v0, Lru/cn/api/parental/BlockedChannelRepo;

    invoke-virtual {p0}, Lru/cn/api/parental/BlockedChannelProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lru/cn/api/parental/BlockedChannelRepo;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lru/cn/api/parental/BlockedChannelProvider;->channelRepo:Lru/cn/api/parental/BlockedChannelRepo;

    .line 22
    iget-object v0, p0, Lru/cn/api/parental/BlockedChannelProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "ru.cn.api.parental.channels"

    const-string v2, "blocked_channels"

    invoke-virtual {v0, v1, v2, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 24
    iget-object v0, p0, Lru/cn/api/parental/BlockedChannelProvider;->uriMatcher:Landroid/content/UriMatcher;

    const-string v1, "ru.cn.api.parental.channels"

    const-string v2, "blocked_channels/*"

    const/4 v3, 0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 26
    return v4
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 7
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 37
    iget-object v2, p0, Lru/cn/api/parental/BlockedChannelProvider;->uriMatcher:Landroid/content/UriMatcher;

    invoke-virtual {v2, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 50
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Wrong URI: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 39
    :pswitch_0
    iget-object v2, p0, Lru/cn/api/parental/BlockedChannelProvider;->channelRepo:Lru/cn/api/parental/BlockedChannelRepo;

    invoke-virtual {v2, p3, p4, p5}, Lru/cn/api/parental/BlockedChannelRepo;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .local v0, "cursor":Landroid/database/Cursor;
    move-object v1, v0

    .line 47
    .end local v0    # "cursor":Landroid/database/Cursor;
    .local v1, "cursor":Landroid/database/Cursor;
    :goto_0
    return-object v1

    .line 43
    .end local v1    # "cursor":Landroid/database/Cursor;
    :pswitch_1
    iget-object v2, p0, Lru/cn/api/parental/BlockedChannelProvider;->channelRepo:Lru/cn/api/parental/BlockedChannelRepo;

    sget-object v3, Lru/cn/api/parental/DataBaseSchema$BlockedChannelContract;->CN_ID:Lru/cn/api/parental/DataBaseSchema$BlockedChannelContract;

    .line 44
    invoke-virtual {v3}, Lru/cn/api/parental/DataBaseSchema$BlockedChannelContract;->getColumnName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    .line 45
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x0

    .line 43
    invoke-virtual {v2, v3, v4, v5}, Lru/cn/api/parental/BlockedChannelRepo;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .restart local v0    # "cursor":Landroid/database/Cursor;
    move-object v1, v0

    .line 47
    .end local v0    # "cursor":Landroid/database/Cursor;
    .restart local v1    # "cursor":Landroid/database/Cursor;
    goto :goto_0

    .line 37
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 88
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unsupported operation"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
