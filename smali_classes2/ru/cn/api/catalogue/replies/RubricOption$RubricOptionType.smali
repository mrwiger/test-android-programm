.class public final enum Lru/cn/api/catalogue/replies/RubricOption$RubricOptionType;
.super Ljava/lang/Enum;
.source "RubricOption.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/api/catalogue/replies/RubricOption;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "RubricOptionType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lru/cn/api/catalogue/replies/RubricOption$RubricOptionType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lru/cn/api/catalogue/replies/RubricOption$RubricOptionType;

.field public static final enum INPUT:Lru/cn/api/catalogue/replies/RubricOption$RubricOptionType;

.field public static final enum SWITCH:Lru/cn/api/catalogue/replies/RubricOption$RubricOptionType;


# instance fields
.field private value:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 10
    new-instance v0, Lru/cn/api/catalogue/replies/RubricOption$RubricOptionType;

    const-string v1, "SWITCH"

    invoke-direct {v0, v1, v2, v2}, Lru/cn/api/catalogue/replies/RubricOption$RubricOptionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/cn/api/catalogue/replies/RubricOption$RubricOptionType;->SWITCH:Lru/cn/api/catalogue/replies/RubricOption$RubricOptionType;

    new-instance v0, Lru/cn/api/catalogue/replies/RubricOption$RubricOptionType;

    const-string v1, "INPUT"

    invoke-direct {v0, v1, v3, v3}, Lru/cn/api/catalogue/replies/RubricOption$RubricOptionType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/cn/api/catalogue/replies/RubricOption$RubricOptionType;->INPUT:Lru/cn/api/catalogue/replies/RubricOption$RubricOptionType;

    .line 9
    const/4 v0, 0x2

    new-array v0, v0, [Lru/cn/api/catalogue/replies/RubricOption$RubricOptionType;

    sget-object v1, Lru/cn/api/catalogue/replies/RubricOption$RubricOptionType;->SWITCH:Lru/cn/api/catalogue/replies/RubricOption$RubricOptionType;

    aput-object v1, v0, v2

    sget-object v1, Lru/cn/api/catalogue/replies/RubricOption$RubricOptionType;->INPUT:Lru/cn/api/catalogue/replies/RubricOption$RubricOptionType;

    aput-object v1, v0, v3

    sput-object v0, Lru/cn/api/catalogue/replies/RubricOption$RubricOptionType;->$VALUES:[Lru/cn/api/catalogue/replies/RubricOption$RubricOptionType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 15
    iput p3, p0, Lru/cn/api/catalogue/replies/RubricOption$RubricOptionType;->value:I

    .line 16
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lru/cn/api/catalogue/replies/RubricOption$RubricOptionType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 9
    const-class v0, Lru/cn/api/catalogue/replies/RubricOption$RubricOptionType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lru/cn/api/catalogue/replies/RubricOption$RubricOptionType;

    return-object v0
.end method

.method public static values()[Lru/cn/api/catalogue/replies/RubricOption$RubricOptionType;
    .locals 1

    .prologue
    .line 9
    sget-object v0, Lru/cn/api/catalogue/replies/RubricOption$RubricOptionType;->$VALUES:[Lru/cn/api/catalogue/replies/RubricOption$RubricOptionType;

    invoke-virtual {v0}, [Lru/cn/api/catalogue/replies/RubricOption$RubricOptionType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lru/cn/api/catalogue/replies/RubricOption$RubricOptionType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 19
    iget v0, p0, Lru/cn/api/catalogue/replies/RubricOption$RubricOptionType;->value:I

    return v0
.end method
