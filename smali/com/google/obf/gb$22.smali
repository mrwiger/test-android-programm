.class final Lcom/google/obf/gb$22;
.super Lcom/google/obf/ew;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/obf/gb;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/obf/ew",
        "<",
        "Lcom/google/obf/em;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lcom/google/obf/ew;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/obf/ge;)Lcom/google/obf/em;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2
    sget-object v0, Lcom/google/obf/gb$30;->a:[I

    invoke-virtual {p1}, Lcom/google/obf/ge;->f()Lcom/google/obf/gf;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/obf/gf;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 21
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 3
    :pswitch_0
    new-instance v0, Lcom/google/obf/er;

    invoke-virtual {p1}, Lcom/google/obf/ge;->h()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/obf/er;-><init>(Ljava/lang/String;)V

    .line 20
    :goto_0
    return-object v0

    .line 4
    :pswitch_1
    invoke-virtual {p1}, Lcom/google/obf/ge;->h()Ljava/lang/String;

    move-result-object v1

    .line 5
    new-instance v0, Lcom/google/obf/er;

    new-instance v2, Lcom/google/obf/fi;

    invoke-direct {v2, v1}, Lcom/google/obf/fi;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v2}, Lcom/google/obf/er;-><init>(Ljava/lang/Number;)V

    goto :goto_0

    .line 6
    :pswitch_2
    new-instance v0, Lcom/google/obf/er;

    invoke-virtual {p1}, Lcom/google/obf/ge;->i()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/obf/er;-><init>(Ljava/lang/Boolean;)V

    goto :goto_0

    .line 7
    :pswitch_3
    invoke-virtual {p1}, Lcom/google/obf/ge;->j()V

    .line 8
    sget-object v0, Lcom/google/obf/eo;->a:Lcom/google/obf/eo;

    goto :goto_0

    .line 9
    :pswitch_4
    new-instance v0, Lcom/google/obf/ej;

    invoke-direct {v0}, Lcom/google/obf/ej;-><init>()V

    .line 10
    invoke-virtual {p1}, Lcom/google/obf/ge;->a()V

    .line 11
    :goto_1
    invoke-virtual {p1}, Lcom/google/obf/ge;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 12
    invoke-virtual {p0, p1}, Lcom/google/obf/gb$22;->a(Lcom/google/obf/ge;)Lcom/google/obf/em;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/obf/ej;->a(Lcom/google/obf/em;)V

    goto :goto_1

    .line 13
    :cond_0
    invoke-virtual {p1}, Lcom/google/obf/ge;->b()V

    goto :goto_0

    .line 15
    :pswitch_5
    new-instance v0, Lcom/google/obf/ep;

    invoke-direct {v0}, Lcom/google/obf/ep;-><init>()V

    .line 16
    invoke-virtual {p1}, Lcom/google/obf/ge;->c()V

    .line 17
    :goto_2
    invoke-virtual {p1}, Lcom/google/obf/ge;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 18
    invoke-virtual {p1}, Lcom/google/obf/ge;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p1}, Lcom/google/obf/gb$22;->a(Lcom/google/obf/ge;)Lcom/google/obf/em;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/obf/ep;->a(Ljava/lang/String;Lcom/google/obf/em;)V

    goto :goto_2

    .line 19
    :cond_1
    invoke-virtual {p1}, Lcom/google/obf/ge;->d()V

    goto :goto_0

    .line 2
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public a(Lcom/google/obf/gg;Lcom/google/obf/em;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 22
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/google/obf/em;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 23
    :cond_0
    invoke-virtual {p1}, Lcom/google/obf/gg;->f()Lcom/google/obf/gg;

    .line 45
    :goto_0
    return-void

    .line 24
    :cond_1
    invoke-virtual {p2}, Lcom/google/obf/em;->i()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 25
    invoke-virtual {p2}, Lcom/google/obf/em;->m()Lcom/google/obf/er;

    move-result-object v0

    .line 26
    invoke-virtual {v0}, Lcom/google/obf/er;->p()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 27
    invoke-virtual {v0}, Lcom/google/obf/er;->a()Ljava/lang/Number;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/obf/gg;->a(Ljava/lang/Number;)Lcom/google/obf/gg;

    goto :goto_0

    .line 28
    :cond_2
    invoke-virtual {v0}, Lcom/google/obf/er;->o()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 29
    invoke-virtual {v0}, Lcom/google/obf/er;->f()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/obf/gg;->a(Z)Lcom/google/obf/gg;

    goto :goto_0

    .line 30
    :cond_3
    invoke-virtual {v0}, Lcom/google/obf/er;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/obf/gg;->b(Ljava/lang/String;)Lcom/google/obf/gg;

    goto :goto_0

    .line 31
    :cond_4
    invoke-virtual {p2}, Lcom/google/obf/em;->g()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 32
    invoke-virtual {p1}, Lcom/google/obf/gg;->b()Lcom/google/obf/gg;

    .line 33
    invoke-virtual {p2}, Lcom/google/obf/em;->l()Lcom/google/obf/ej;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/obf/ej;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/em;

    .line 34
    invoke-virtual {p0, p1, v0}, Lcom/google/obf/gb$22;->a(Lcom/google/obf/gg;Lcom/google/obf/em;)V

    goto :goto_1

    .line 36
    :cond_5
    invoke-virtual {p1}, Lcom/google/obf/gg;->c()Lcom/google/obf/gg;

    goto :goto_0

    .line 37
    :cond_6
    invoke-virtual {p2}, Lcom/google/obf/em;->h()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 38
    invoke-virtual {p1}, Lcom/google/obf/gg;->d()Lcom/google/obf/gg;

    .line 39
    invoke-virtual {p2}, Lcom/google/obf/em;->k()Lcom/google/obf/ep;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/obf/ep;->o()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 40
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p1, v1}, Lcom/google/obf/gg;->a(Ljava/lang/String;)Lcom/google/obf/gg;

    .line 41
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/em;

    invoke-virtual {p0, p1, v0}, Lcom/google/obf/gb$22;->a(Lcom/google/obf/gg;Lcom/google/obf/em;)V

    goto :goto_2

    .line 43
    :cond_7
    invoke-virtual {p1}, Lcom/google/obf/gg;->e()Lcom/google/obf/gg;

    goto/16 :goto_0

    .line 44
    :cond_8
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Couldn\'t write "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public synthetic read(Lcom/google/obf/ge;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 46
    invoke-virtual {p0, p1}, Lcom/google/obf/gb$22;->a(Lcom/google/obf/ge;)Lcom/google/obf/em;

    move-result-object v0

    return-object v0
.end method

.method public synthetic write(Lcom/google/obf/gg;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 47
    check-cast p2, Lcom/google/obf/em;

    invoke-virtual {p0, p1, p2}, Lcom/google/obf/gb$22;->a(Lcom/google/obf/gg;Lcom/google/obf/em;)V

    return-void
.end method
