.class public final Lru/cn/utils/customization/Config;
.super Ljava/lang/Object;
.source "Config.java"


# static fields
.field public static API_URL:Ljava/lang/String;

.field private static restriction:Lru/cn/utils/customization/Restrictable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const-string v0, "http://api.peers.tv/registry/2/"

    sput-object v0, Lru/cn/utils/customization/Config;->API_URL:Ljava/lang/String;

    .line 11
    new-instance v0, Lru/cn/utils/customization/NoRestriction;

    invoke-direct {v0}, Lru/cn/utils/customization/NoRestriction;-><init>()V

    sput-object v0, Lru/cn/utils/customization/Config;->restriction:Lru/cn/utils/customization/Restrictable;

    return-void
.end method

.method public static restrictions()Lru/cn/utils/customization/Restrictable;
    .locals 1

    .prologue
    .line 13
    sget-object v0, Lru/cn/utils/customization/Config;->restriction:Lru/cn/utils/customization/Restrictable;

    return-object v0
.end method

.method public static setupRestrictions(Landroid/content/Context;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 17
    new-instance v0, Lru/cn/utils/customization/FirmwareCustomization;

    invoke-direct {v0, p0}, Lru/cn/utils/customization/FirmwareCustomization;-><init>(Landroid/content/Context;)V

    sput-object v0, Lru/cn/utils/customization/Config;->restriction:Lru/cn/utils/customization/Restrictable;

    .line 18
    return-void
.end method
