.class public final Lcom/my/target/fb$b;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "SliderRecyclerView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/my/target/fb;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$Adapter",
        "<",
        "Lcom/my/target/fb$d;",
        ">;"
    }
.end annotation


# instance fields
.field private final backgroundColor:I

.field private dl:Landroid/view/View$OnClickListener;

.field private final eh:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/my/target/core/models/banners/g;",
            ">;"
        }
    .end annotation
.end field

.field private final ei:Landroid/content/res/Resources;


# direct methods
.method constructor <init>(Ljava/util/List;ILandroid/content/res/Resources;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/my/target/core/models/banners/g;",
            ">;I",
            "Landroid/content/res/Resources;",
            ")V"
        }
    .end annotation

    .prologue
    .line 236
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 237
    iput-object p1, p0, Lcom/my/target/fb$b;->eh:Ljava/util/List;

    .line 238
    iput p2, p0, Lcom/my/target/fb$b;->backgroundColor:I

    .line 239
    iput-object p3, p0, Lcom/my/target/fb$b;->ei:Landroid/content/res/Resources;

    .line 240
    return-void
.end method


# virtual methods
.method public final getItemCount()I
    .locals 1

    .prologue
    .line 279
    iget-object v0, p0, Lcom/my/target/fb$b;->eh:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getItemViewType(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 245
    if-nez p1, :cond_0

    .line 247
    const/4 v0, 0x1

    .line 255
    :goto_0
    return v0

    .line 249
    :cond_0
    iget-object v0, p0, Lcom/my/target/fb$b;->eh:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_1

    .line 251
    const/4 v0, 0x2

    goto :goto_0

    .line 255
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 226
    check-cast p1, Lcom/my/target/fb$d;

    .line 2272
    iget-object v0, p0, Lcom/my/target/fb$b;->eh:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/core/models/banners/g;

    invoke-virtual {p1}, Lcom/my/target/fb$d;->P()Lcom/my/target/ez;

    move-result-object v4

    .line 2297
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/g;->getOptimalLandscapeImage()Lcom/my/target/common/models/ImageData;

    move-result-object v2

    .line 2298
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/g;->getOptimalPortraitImage()Lcom/my/target/common/models/ImageData;

    move-result-object v3

    .line 2303
    if-eqz v2, :cond_5

    .line 2305
    invoke-virtual {v2}, Lcom/my/target/common/models/ImageData;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    .line 2307
    :goto_0
    if-eqz v3, :cond_0

    .line 2309
    invoke-virtual {v3}, Lcom/my/target/common/models/ImageData;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 2314
    :cond_0
    iget-object v3, p0, Lcom/my/target/fb$b;->ei:Landroid/content/res/Resources;

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    const/4 v5, 0x2

    if-ne v3, v5, :cond_2

    move-object v3, v2

    .line 2323
    :goto_1
    if-nez v3, :cond_4

    .line 2325
    if-nez v2, :cond_3

    .line 2335
    :goto_2
    invoke-virtual {v4, v1}, Lcom/my/target/ez;->setImage(Landroid/graphics/Bitmap;)V

    .line 2337
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/g;->getAgeRestrictions()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2339
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/g;->getAgeRestrictions()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/my/target/ez;->setAgeRestrictions(Ljava/lang/String;)V

    .line 2273
    :cond_1
    invoke-virtual {p1}, Lcom/my/target/fb$d;->P()Lcom/my/target/ez;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/fb$b;->dl:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/my/target/ez;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 226
    return-void

    :cond_2
    move-object v3, v1

    .line 2320
    goto :goto_1

    :cond_3
    move-object v1, v2

    .line 2331
    goto :goto_2

    :cond_4
    move-object v1, v3

    goto :goto_2

    :cond_5
    move-object v2, v1

    goto :goto_0
.end method

.method public final synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 226
    .line 3262
    new-instance v0, Lcom/my/target/ez;

    .line 3263
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lcom/my/target/fb$b;->backgroundColor:I

    invoke-direct {v0, v1, v2}, Lcom/my/target/ez;-><init>(Landroid/content/Context;I)V

    .line 3264
    new-instance v1, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/support/v7/widget/RecyclerView$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/my/target/ez;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3266
    new-instance v1, Lcom/my/target/fb$d;

    invoke-direct {v1, v0}, Lcom/my/target/fb$d;-><init>(Lcom/my/target/ez;)V

    .line 226
    return-object v1
.end method

.method public final synthetic onViewRecycled(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V
    .locals 2

    .prologue
    .line 226
    check-cast p1, Lcom/my/target/fb$d;

    .line 1285
    invoke-virtual {p1}, Lcom/my/target/fb$d;->P()Lcom/my/target/ez;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/my/target/ez;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1286
    invoke-super {p0, p1}, Landroid/support/v7/widget/RecyclerView$Adapter;->onViewRecycled(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    .line 226
    return-void
.end method

.method final setClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0
    .param p1, "cardClickListener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 291
    iput-object p1, p0, Lcom/my/target/fb$b;->dl:Landroid/view/View$OnClickListener;

    .line 292
    return-void
.end method
