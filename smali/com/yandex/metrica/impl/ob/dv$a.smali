.class public final enum Lcom/yandex/metrica/impl/ob/dv$a;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/yandex/metrica/impl/ob/dv;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/yandex/metrica/impl/ob/dv$a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/yandex/metrica/impl/ob/dv$a;

.field public static final enum b:Lcom/yandex/metrica/impl/ob/dv$a;

.field private static final synthetic d:[Lcom/yandex/metrica/impl/ob/dv$a;


# instance fields
.field private final c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 31
    new-instance v0, Lcom/yandex/metrica/impl/ob/dv$a;

    const-string v1, "FOREGROUND"

    const-string v2, "fg"

    invoke-direct {v0, v1, v3, v2}, Lcom/yandex/metrica/impl/ob/dv$a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/dv$a;->a:Lcom/yandex/metrica/impl/ob/dv$a;

    new-instance v0, Lcom/yandex/metrica/impl/ob/dv$a;

    const-string v1, "BACKGROUND"

    const-string v2, "bg"

    invoke-direct {v0, v1, v4, v2}, Lcom/yandex/metrica/impl/ob/dv$a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/dv$a;->b:Lcom/yandex/metrica/impl/ob/dv$a;

    .line 30
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/yandex/metrica/impl/ob/dv$a;

    sget-object v1, Lcom/yandex/metrica/impl/ob/dv$a;->a:Lcom/yandex/metrica/impl/ob/dv$a;

    aput-object v1, v0, v3

    sget-object v1, Lcom/yandex/metrica/impl/ob/dv$a;->b:Lcom/yandex/metrica/impl/ob/dv$a;

    aput-object v1, v0, v4

    sput-object v0, Lcom/yandex/metrica/impl/ob/dv$a;->d:[Lcom/yandex/metrica/impl/ob/dv$a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 37
    iput-object p3, p0, Lcom/yandex/metrica/impl/ob/dv$a;->c:Ljava/lang/String;

    .line 38
    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/dv$a;
    .locals 6

    .prologue
    .line 48
    sget-object v1, Lcom/yandex/metrica/impl/ob/dv$a;->a:Lcom/yandex/metrica/impl/ob/dv$a;

    .line 49
    invoke-static {}, Lcom/yandex/metrica/impl/ob/dv$a;->values()[Lcom/yandex/metrica/impl/ob/dv$a;

    move-result-object v3

    array-length v4, v3

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v0, v3, v2

    .line 50
    iget-object v5, v0, Lcom/yandex/metrica/impl/ob/dv$a;->c:Ljava/lang/String;

    invoke-virtual {v5, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 49
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_0

    .line 54
    :cond_0
    return-object v1

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/dv$a;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 30
    const-class v0, Lcom/yandex/metrica/impl/ob/dv$a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/yandex/metrica/impl/ob/dv$a;

    return-object v0
.end method

.method public static values()[Lcom/yandex/metrica/impl/ob/dv$a;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/yandex/metrica/impl/ob/dv$a;->d:[Lcom/yandex/metrica/impl/ob/dv$a;

    invoke-virtual {v0}, [Lcom/yandex/metrica/impl/ob/dv$a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/yandex/metrica/impl/ob/dv$a;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/dv$a;->c:Ljava/lang/String;

    return-object v0
.end method
