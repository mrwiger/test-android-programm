.class public final Lru/cn/tv/stb/categories/CategoryFragment;
.super Landroid/support/v4/app/ListFragment;
.source "CategoryFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/tv/stb/categories/CategoryFragment$CategoryFragmentListener;
    }
.end annotation


# instance fields
.field private adapter:Lru/cn/tv/stb/categories/CategoryAdapter;

.field private categoriesInfo:Lru/cn/tv/stb/categories/CategoriesInfo;

.field private container:Landroid/widget/FrameLayout;

.field private currentType:Lru/cn/domain/tv/CurrentCategory$Type;

.field private expandedWidth:I

.field private foldedWidth:I

.field private isFolded:Z

.field private isKidsMode:Z

.field private listener:Lru/cn/tv/stb/categories/CategoryFragment$CategoryFragmentListener;

.field private viewModel:Lru/cn/tv/stb/categories/CategoryViewModel;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/support/v4/app/ListFragment;-><init>()V

    .line 33
    const/4 v0, 0x0

    iput-object v0, p0, Lru/cn/tv/stb/categories/CategoryFragment;->adapter:Lru/cn/tv/stb/categories/CategoryAdapter;

    .line 35
    sget-object v0, Lru/cn/domain/tv/CurrentCategory$Type;->all:Lru/cn/domain/tv/CurrentCategory$Type;

    iput-object v0, p0, Lru/cn/tv/stb/categories/CategoryFragment;->currentType:Lru/cn/domain/tv/CurrentCategory$Type;

    .line 36
    const/4 v0, 0x1

    iput-boolean v0, p0, Lru/cn/tv/stb/categories/CategoryFragment;->isFolded:Z

    return-void
.end method

.method static synthetic access$000(Lru/cn/tv/stb/categories/CategoryFragment;)Lru/cn/domain/tv/CurrentCategory$Type;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/stb/categories/CategoryFragment;

    .prologue
    .line 27
    iget-object v0, p0, Lru/cn/tv/stb/categories/CategoryFragment;->currentType:Lru/cn/domain/tv/CurrentCategory$Type;

    return-object v0
.end method

.method static synthetic access$100(Lru/cn/tv/stb/categories/CategoryFragment;)Z
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/stb/categories/CategoryFragment;

    .prologue
    .line 27
    iget-boolean v0, p0, Lru/cn/tv/stb/categories/CategoryFragment;->isFolded:Z

    return v0
.end method

.method static synthetic access$200(Lru/cn/tv/stb/categories/CategoryFragment;I)Lru/cn/domain/tv/CurrentCategory$Type;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/stb/categories/CategoryFragment;
    .param p1, "x1"    # I

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lru/cn/tv/stb/categories/CategoryFragment;->getItemType(I)Lru/cn/domain/tv/CurrentCategory$Type;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lru/cn/tv/stb/categories/CategoryFragment;)Lru/cn/tv/stb/categories/CategoryViewModel;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/stb/categories/CategoryFragment;

    .prologue
    .line 27
    iget-object v0, p0, Lru/cn/tv/stb/categories/CategoryFragment;->viewModel:Lru/cn/tv/stb/categories/CategoryViewModel;

    return-object v0
.end method

.method private buildAdapter()V
    .locals 5

    .prologue
    .line 427
    new-instance v0, Landroid/database/MatrixCursor;

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "item_type"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "type"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "title"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "image_resource"

    aput-object v4, v2, v3

    invoke-direct {v0, v2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 429
    .local v0, "cursor":Landroid/database/MatrixCursor;
    iget-boolean v2, p0, Lru/cn/tv/stb/categories/CategoryFragment;->isFolded:Z

    if-nez v2, :cond_0

    .line 430
    invoke-direct {p0, v0}, Lru/cn/tv/stb/categories/CategoryFragment;->buildStbExpandedCursor(Landroid/database/MatrixCursor;)V

    .line 446
    :goto_0
    iget-object v2, p0, Lru/cn/tv/stb/categories/CategoryFragment;->adapter:Lru/cn/tv/stb/categories/CategoryAdapter;

    invoke-virtual {v2, v0}, Lru/cn/tv/stb/categories/CategoryAdapter;->changeCursor(Landroid/database/Cursor;)V

    .line 447
    return-void

    .line 433
    :cond_0
    iget-object v2, p0, Lru/cn/tv/stb/categories/CategoryFragment;->currentType:Lru/cn/domain/tv/CurrentCategory$Type;

    if-nez v2, :cond_2

    .line 436
    invoke-virtual {p0}, Lru/cn/tv/stb/categories/CategoryFragment;->getSelectedItemPosition()I

    move-result v1

    .line 437
    .local v1, "selectedPosition":I
    if-lez v1, :cond_1

    iget-object v2, p0, Lru/cn/tv/stb/categories/CategoryFragment;->adapter:Lru/cn/tv/stb/categories/CategoryAdapter;

    invoke-virtual {v2}, Lru/cn/tv/stb/categories/CategoryAdapter;->getCount()I

    move-result v2

    if-lt v1, v2, :cond_1

    .line 438
    iget-object v2, p0, Lru/cn/tv/stb/categories/CategoryFragment;->adapter:Lru/cn/tv/stb/categories/CategoryAdapter;

    invoke-virtual {v2}, Lru/cn/tv/stb/categories/CategoryAdapter;->getCount()I

    move-result v2

    add-int/lit8 v1, v2, -0x1

    .line 441
    :cond_1
    invoke-direct {p0, v1}, Lru/cn/tv/stb/categories/CategoryFragment;->getItemType(I)Lru/cn/domain/tv/CurrentCategory$Type;

    move-result-object v2

    iput-object v2, p0, Lru/cn/tv/stb/categories/CategoryFragment;->currentType:Lru/cn/domain/tv/CurrentCategory$Type;

    .line 443
    .end local v1    # "selectedPosition":I
    :cond_2
    invoke-direct {p0, v0}, Lru/cn/tv/stb/categories/CategoryFragment;->buildStbFoldedCursor(Landroid/database/MatrixCursor;)V

    goto :goto_0
.end method

.method private buildStbExpandedCursor(Landroid/database/MatrixCursor;)V
    .locals 11
    .param p1, "cursor"    # Landroid/database/MatrixCursor;

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 215
    iget-object v6, p0, Lru/cn/tv/stb/categories/CategoryFragment;->categoriesInfo:Lru/cn/tv/stb/categories/CategoriesInfo;

    if-nez v6, :cond_1

    .line 284
    :cond_0
    :goto_0
    return-void

    .line 218
    :cond_1
    const v3, 0x7f0802d6

    .line 219
    .local v3, "tvSelector":I
    const v0, 0x7f0802d4

    .line 220
    .local v0, "favSelector":I
    const v1, 0x7f080262

    .line 221
    .local v1, "hdSelector":I
    iget-boolean v6, p0, Lru/cn/tv/stb/categories/CategoryFragment;->isKidsMode:Z

    if-eqz v6, :cond_2

    .line 222
    const v3, 0x7f08025d

    .line 223
    const v0, 0x7f08025c

    .line 224
    const v1, 0x7f080206

    .line 228
    :cond_2
    const/4 v6, 0x5

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v5

    .line 229
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v4

    const/4 v7, 0x0

    aput-object v7, v6, v8

    const v7, 0x7f0e0064

    .line 230
    invoke-virtual {p0, v7}, Lru/cn/tv/stb/categories/CategoryFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v10

    .line 228
    invoke-virtual {p1, v6}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 232
    const/4 v6, 0x5

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v4

    sget-object v7, Lru/cn/domain/tv/CurrentCategory$Type;->fav:Lru/cn/domain/tv/CurrentCategory$Type;

    aput-object v7, v6, v8

    const v7, 0x7f0e0069

    .line 233
    invoke-virtual {p0, v7}, Lru/cn/tv/stb/categories/CategoryFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v10

    .line 232
    invoke-virtual {p1, v6}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 235
    const/4 v6, 0x5

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v4

    sget-object v7, Lru/cn/domain/tv/CurrentCategory$Type;->all:Lru/cn/domain/tv/CurrentCategory$Type;

    aput-object v7, v6, v8

    const v7, 0x7f0e0063

    .line 236
    invoke-virtual {p0, v7}, Lru/cn/tv/stb/categories/CategoryFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v10

    .line 235
    invoke-virtual {p1, v6}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 238
    iget-object v6, p0, Lru/cn/tv/stb/categories/CategoryFragment;->categoriesInfo:Lru/cn/tv/stb/categories/CategoriesInfo;

    iget-boolean v6, v6, Lru/cn/tv/stb/categories/CategoriesInfo;->hasHDChannels:Z

    if-eqz v6, :cond_3

    .line 239
    const/4 v6, 0x5

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v4

    sget-object v7, Lru/cn/domain/tv/CurrentCategory$Type;->hd:Lru/cn/domain/tv/CurrentCategory$Type;

    aput-object v7, v6, v8

    const v7, 0x7f0e006a

    .line 240
    invoke-virtual {p0, v7}, Lru/cn/tv/stb/categories/CategoryFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v10

    .line 239
    invoke-virtual {p1, v6}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 243
    :cond_3
    iget-boolean v6, p0, Lru/cn/tv/stb/categories/CategoryFragment;->isKidsMode:Z

    if-nez v6, :cond_0

    .line 244
    invoke-virtual {p0}, Lru/cn/tv/stb/categories/CategoryFragment;->getContext()Landroid/content/Context;

    move-result-object v6

    const-string v7, "porno_disabled"

    invoke-static {v6, v7}, Lru/cn/domain/Preferences;->getBoolean(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_8

    move v2, v4

    .line 246
    .local v2, "pornoOn":Z
    :goto_1
    iget-object v6, p0, Lru/cn/tv/stb/categories/CategoryFragment;->categoriesInfo:Lru/cn/tv/stb/categories/CategoriesInfo;

    iget-boolean v6, v6, Lru/cn/tv/stb/categories/CategoriesInfo;->hasPorno:Z

    if-eqz v6, :cond_4

    if-eqz v2, :cond_4

    .line 247
    const/4 v6, 0x5

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v4

    sget-object v7, Lru/cn/domain/tv/CurrentCategory$Type;->porno:Lru/cn/domain/tv/CurrentCategory$Type;

    aput-object v7, v6, v8

    const v7, 0x7f0e0062

    .line 248
    invoke-virtual {p0, v7}, Lru/cn/tv/stb/categories/CategoryFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    const v7, 0x7f0802d5

    .line 249
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v10

    .line 247
    invoke-virtual {p1, v6}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 253
    :cond_4
    const/4 v6, 0x5

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v5

    .line 254
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v4

    const/4 v7, 0x0

    aput-object v7, v6, v8

    const v7, 0x7f0e0067

    .line 255
    invoke-virtual {p0, v7}, Lru/cn/tv/stb/categories/CategoryFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v10

    .line 253
    invoke-virtual {p1, v6}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 257
    iget-object v6, p0, Lru/cn/tv/stb/categories/CategoryFragment;->categoriesInfo:Lru/cn/tv/stb/categories/CategoriesInfo;

    iget-boolean v6, v6, Lru/cn/tv/stb/categories/CategoriesInfo;->hasCameras:Z

    if-eqz v6, :cond_5

    .line 258
    const/4 v6, 0x5

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v4

    sget-object v7, Lru/cn/domain/tv/CurrentCategory$Type;->intersections:Lru/cn/domain/tv/CurrentCategory$Type;

    aput-object v7, v6, v8

    const v7, 0x7f0e0068

    .line 259
    invoke-virtual {p0, v7}, Lru/cn/tv/stb/categories/CategoryFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    const v7, 0x7f0802cf

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v10

    .line 258
    invoke-virtual {p1, v6}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 262
    :cond_5
    const/4 v6, 0x5

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v4

    sget-object v7, Lru/cn/domain/tv/CurrentCategory$Type;->collection:Lru/cn/domain/tv/CurrentCategory$Type;

    aput-object v7, v6, v8

    const v7, 0x7f0e0066

    .line 263
    invoke-virtual {p0, v7}, Lru/cn/tv/stb/categories/CategoryFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    const v7, 0x7f08025f

    .line 264
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v10

    .line 262
    invoke-virtual {p1, v6}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 267
    const/4 v6, 0x5

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v5

    .line 268
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v4

    const/4 v7, 0x0

    aput-object v7, v6, v8

    const v7, 0x7f0e006c

    .line 269
    invoke-virtual {p0, v7}, Lru/cn/tv/stb/categories/CategoryFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v10

    .line 267
    invoke-virtual {p1, v6}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 271
    iget-object v6, p0, Lru/cn/tv/stb/categories/CategoryFragment;->categoriesInfo:Lru/cn/tv/stb/categories/CategoriesInfo;

    iget-boolean v6, v6, Lru/cn/tv/stb/categories/CategoriesInfo;->hasPrivateOffice:Z

    if-eqz v6, :cond_6

    .line 272
    const/4 v6, 0x5

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v4

    sget-object v7, Lru/cn/domain/tv/CurrentCategory$Type;->billing:Lru/cn/domain/tv/CurrentCategory$Type;

    aput-object v7, v6, v8

    const v7, 0x7f0e0061

    .line 273
    invoke-virtual {p0, v7}, Lru/cn/tv/stb/categories/CategoryFragment;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v9

    const v7, 0x7f0801f1

    .line 274
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v10

    .line 272
    invoke-virtual {p1, v6}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 277
    :cond_6
    iget-object v6, p0, Lru/cn/tv/stb/categories/CategoryFragment;->categoriesInfo:Lru/cn/tv/stb/categories/CategoriesInfo;

    iget-boolean v6, v6, Lru/cn/tv/stb/categories/CategoriesInfo;->hasPorno:Z

    if-nez v6, :cond_7

    invoke-static {}, Lru/cn/utils/customization/Config;->restrictions()Lru/cn/utils/customization/Restrictable;

    move-result-object v6

    const-string v7, "playlists"

    invoke-interface {v6, v7}, Lru/cn/utils/customization/Restrictable;->allows(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_7

    .line 278
    invoke-virtual {p0}, Lru/cn/tv/stb/categories/CategoryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-static {v6}, Lru/cn/player/exoplayer/ExoPlayerUtils;->isAvailable(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 279
    :cond_7
    const/4 v6, 0x5

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v6, v4

    sget-object v4, Lru/cn/domain/tv/CurrentCategory$Type;->setting:Lru/cn/domain/tv/CurrentCategory$Type;

    aput-object v4, v6, v8

    const v4, 0x7f0e006d

    .line 280
    invoke-virtual {p0, v4}, Lru/cn/tv/stb/categories/CategoryFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v6, v9

    const v4, 0x7f0802d7

    .line 281
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v6, v10

    .line 279
    invoke-virtual {p1, v6}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_0

    .end local v2    # "pornoOn":Z
    :cond_8
    move v2, v5

    .line 244
    goto/16 :goto_1
.end method

.method private buildStbFoldedCursor(Landroid/database/MatrixCursor;)V
    .locals 8
    .param p1, "cursor"    # Landroid/database/MatrixCursor;

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x2

    const/4 v3, 0x0

    .line 288
    iget-object v0, p0, Lru/cn/tv/stb/categories/CategoryFragment;->categoriesInfo:Lru/cn/tv/stb/categories/CategoriesInfo;

    if-nez v0, :cond_0

    .line 424
    :goto_0
    return-void

    .line 291
    :cond_0
    iget-boolean v0, p0, Lru/cn/tv/stb/categories/CategoryFragment;->isKidsMode:Z

    if-eqz v0, :cond_2

    .line 292
    sget-object v0, Lru/cn/tv/stb/categories/CategoryFragment$2;->$SwitchMap$ru$cn$domain$tv$CurrentCategory$Type:[I

    iget-object v1, p0, Lru/cn/tv/stb/categories/CategoryFragment;->currentType:Lru/cn/domain/tv/CurrentCategory$Type;

    invoke-virtual {v1}, Lru/cn/domain/tv/CurrentCategory$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 328
    :cond_1
    :goto_1
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v6

    aput-object v5, v0, v4

    aput-object v5, v0, v7

    const/4 v1, 0x4

    const v2, 0x7f080207

    .line 329
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 328
    invoke-virtual {p1, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto :goto_0

    .line 294
    :pswitch_0
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v6

    sget-object v1, Lru/cn/domain/tv/CurrentCategory$Type;->fav:Lru/cn/domain/tv/CurrentCategory$Type;

    aput-object v1, v0, v4

    aput-object v5, v0, v7

    const/4 v1, 0x4

    const v2, 0x7f08020b

    .line 295
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 294
    invoke-virtual {p1, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 297
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v6

    sget-object v1, Lru/cn/domain/tv/CurrentCategory$Type;->all:Lru/cn/domain/tv/CurrentCategory$Type;

    aput-object v1, v0, v4

    aput-object v5, v0, v7

    const/4 v1, 0x4

    const v2, 0x7f08020c

    .line 298
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 297
    invoke-virtual {p1, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 300
    iget-object v0, p0, Lru/cn/tv/stb/categories/CategoryFragment;->categoriesInfo:Lru/cn/tv/stb/categories/CategoriesInfo;

    iget-boolean v0, v0, Lru/cn/tv/stb/categories/CategoriesInfo;->hasHDChannels:Z

    if-eqz v0, :cond_1

    .line 301
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v6

    sget-object v1, Lru/cn/domain/tv/CurrentCategory$Type;->hd:Lru/cn/domain/tv/CurrentCategory$Type;

    aput-object v1, v0, v4

    aput-object v5, v0, v7

    const/4 v1, 0x4

    const v2, 0x7f080208

    .line 302
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 301
    invoke-virtual {p1, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 308
    :pswitch_1
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v6

    sget-object v1, Lru/cn/domain/tv/CurrentCategory$Type;->fav:Lru/cn/domain/tv/CurrentCategory$Type;

    aput-object v1, v0, v4

    aput-object v5, v0, v7

    const/4 v1, 0x4

    const v2, 0x7f08020a

    .line 309
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 308
    invoke-virtual {p1, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 310
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v6

    sget-object v1, Lru/cn/domain/tv/CurrentCategory$Type;->all:Lru/cn/domain/tv/CurrentCategory$Type;

    aput-object v1, v0, v4

    aput-object v5, v0, v7

    const/4 v1, 0x4

    const v2, 0x7f08020d

    .line 311
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 310
    invoke-virtual {p1, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 312
    iget-object v0, p0, Lru/cn/tv/stb/categories/CategoryFragment;->categoriesInfo:Lru/cn/tv/stb/categories/CategoriesInfo;

    iget-boolean v0, v0, Lru/cn/tv/stb/categories/CategoriesInfo;->hasHDChannels:Z

    if-eqz v0, :cond_1

    .line 313
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v6

    sget-object v1, Lru/cn/domain/tv/CurrentCategory$Type;->hd:Lru/cn/domain/tv/CurrentCategory$Type;

    aput-object v1, v0, v4

    aput-object v5, v0, v7

    const/4 v1, 0x4

    const v2, 0x7f080208

    .line 314
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 313
    invoke-virtual {p1, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 319
    :pswitch_2
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v6

    sget-object v1, Lru/cn/domain/tv/CurrentCategory$Type;->fav:Lru/cn/domain/tv/CurrentCategory$Type;

    aput-object v1, v0, v4

    aput-object v5, v0, v7

    const/4 v1, 0x4

    const v2, 0x7f08020a

    .line 320
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 319
    invoke-virtual {p1, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 321
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v6

    sget-object v1, Lru/cn/domain/tv/CurrentCategory$Type;->all:Lru/cn/domain/tv/CurrentCategory$Type;

    aput-object v1, v0, v4

    aput-object v5, v0, v7

    const/4 v1, 0x4

    const v2, 0x7f08020c

    .line 322
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 321
    invoke-virtual {p1, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 323
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v6

    sget-object v1, Lru/cn/domain/tv/CurrentCategory$Type;->hd:Lru/cn/domain/tv/CurrentCategory$Type;

    aput-object v1, v0, v4

    aput-object v5, v0, v7

    const/4 v1, 0x4

    const v2, 0x7f080209

    .line 324
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 323
    invoke-virtual {p1, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 334
    :cond_2
    iget-object v0, p0, Lru/cn/tv/stb/categories/CategoryFragment;->currentType:Lru/cn/domain/tv/CurrentCategory$Type;

    sget-object v1, Lru/cn/domain/tv/CurrentCategory$Type;->all:Lru/cn/domain/tv/CurrentCategory$Type;

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lru/cn/tv/stb/categories/CategoryFragment;->currentType:Lru/cn/domain/tv/CurrentCategory$Type;

    sget-object v1, Lru/cn/domain/tv/CurrentCategory$Type;->fav:Lru/cn/domain/tv/CurrentCategory$Type;

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lru/cn/tv/stb/categories/CategoryFragment;->currentType:Lru/cn/domain/tv/CurrentCategory$Type;

    sget-object v1, Lru/cn/domain/tv/CurrentCategory$Type;->hd:Lru/cn/domain/tv/CurrentCategory$Type;

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lru/cn/tv/stb/categories/CategoryFragment;->currentType:Lru/cn/domain/tv/CurrentCategory$Type;

    sget-object v1, Lru/cn/domain/tv/CurrentCategory$Type;->porno:Lru/cn/domain/tv/CurrentCategory$Type;

    if-ne v0, v1, :cond_4

    .line 338
    :cond_3
    sget-object v0, Lru/cn/tv/stb/categories/CategoryFragment$2;->$SwitchMap$ru$cn$domain$tv$CurrentCategory$Type:[I

    iget-object v1, p0, Lru/cn/tv/stb/categories/CategoryFragment;->currentType:Lru/cn/domain/tv/CurrentCategory$Type;

    invoke-virtual {v1}, Lru/cn/domain/tv/CurrentCategory$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    .line 357
    :goto_2
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v6

    sget-object v1, Lru/cn/domain/tv/CurrentCategory$Type;->collection:Lru/cn/domain/tv/CurrentCategory$Type;

    aput-object v1, v0, v4

    aput-object v5, v0, v7

    const/4 v1, 0x4

    const v2, 0x7f080269

    .line 359
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 357
    invoke-virtual {p1, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 360
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v6

    sget-object v1, Lru/cn/domain/tv/CurrentCategory$Type;->billing:Lru/cn/domain/tv/CurrentCategory$Type;

    aput-object v1, v0, v4

    aput-object v5, v0, v7

    const/4 v1, 0x4

    const v2, 0x7f080266

    .line 362
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 360
    invoke-virtual {p1, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 421
    :goto_3
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v6

    aput-object v5, v0, v4

    aput-object v5, v0, v7

    const/4 v1, 0x4

    const v2, 0x7f08027a

    .line 423
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 421
    invoke-virtual {p1, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 340
    :pswitch_3
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v6

    sget-object v1, Lru/cn/domain/tv/CurrentCategory$Type;->all:Lru/cn/domain/tv/CurrentCategory$Type;

    aput-object v1, v0, v4

    aput-object v5, v0, v7

    const/4 v1, 0x4

    const v2, 0x7f08027b

    .line 341
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 340
    invoke-virtual {p1, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 344
    :pswitch_4
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v6

    sget-object v1, Lru/cn/domain/tv/CurrentCategory$Type;->fav:Lru/cn/domain/tv/CurrentCategory$Type;

    aput-object v1, v0, v4

    aput-object v5, v0, v7

    const/4 v1, 0x4

    const v2, 0x7f080274

    .line 345
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 344
    invoke-virtual {p1, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 348
    :pswitch_5
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v6

    sget-object v1, Lru/cn/domain/tv/CurrentCategory$Type;->hd:Lru/cn/domain/tv/CurrentCategory$Type;

    aput-object v1, v0, v4

    aput-object v5, v0, v7

    const/4 v1, 0x4

    const v2, 0x7f08026b

    .line 349
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 348
    invoke-virtual {p1, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 352
    :pswitch_6
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v6

    sget-object v1, Lru/cn/domain/tv/CurrentCategory$Type;->hd:Lru/cn/domain/tv/CurrentCategory$Type;

    aput-object v1, v0, v4

    aput-object v5, v0, v7

    const/4 v1, 0x4

    const v2, 0x7f080277

    .line 353
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 352
    invoke-virtual {p1, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 366
    :cond_4
    iget-object v0, p0, Lru/cn/tv/stb/categories/CategoryFragment;->currentType:Lru/cn/domain/tv/CurrentCategory$Type;

    sget-object v1, Lru/cn/domain/tv/CurrentCategory$Type;->intersections:Lru/cn/domain/tv/CurrentCategory$Type;

    if-eq v0, v1, :cond_5

    iget-object v0, p0, Lru/cn/tv/stb/categories/CategoryFragment;->currentType:Lru/cn/domain/tv/CurrentCategory$Type;

    sget-object v1, Lru/cn/domain/tv/CurrentCategory$Type;->collection:Lru/cn/domain/tv/CurrentCategory$Type;

    if-ne v0, v1, :cond_6

    .line 367
    :cond_5
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v6

    sget-object v1, Lru/cn/domain/tv/CurrentCategory$Type;->all:Lru/cn/domain/tv/CurrentCategory$Type;

    aput-object v1, v0, v4

    aput-object v5, v0, v7

    const/4 v1, 0x4

    const v2, 0x7f08027c

    .line 368
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 367
    invoke-virtual {p1, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 370
    sget-object v0, Lru/cn/tv/stb/categories/CategoryFragment$2;->$SwitchMap$ru$cn$domain$tv$CurrentCategory$Type:[I

    iget-object v1, p0, Lru/cn/tv/stb/categories/CategoryFragment;->currentType:Lru/cn/domain/tv/CurrentCategory$Type;

    invoke-virtual {v1}, Lru/cn/domain/tv/CurrentCategory$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_2

    .line 382
    :goto_4
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v6

    sget-object v1, Lru/cn/domain/tv/CurrentCategory$Type;->billing:Lru/cn/domain/tv/CurrentCategory$Type;

    aput-object v1, v0, v4

    aput-object v5, v0, v7

    const/4 v1, 0x4

    const v2, 0x7f080266

    .line 384
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 382
    invoke-virtual {p1, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_3

    .line 372
    :pswitch_7
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v6

    sget-object v1, Lru/cn/domain/tv/CurrentCategory$Type;->intersections:Lru/cn/domain/tv/CurrentCategory$Type;

    aput-object v1, v0, v4

    aput-object v5, v0, v7

    const/4 v1, 0x4

    const v2, 0x7f08026e

    .line 373
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 372
    invoke-virtual {p1, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto :goto_4

    .line 376
    :pswitch_8
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v6

    sget-object v1, Lru/cn/domain/tv/CurrentCategory$Type;->collection:Lru/cn/domain/tv/CurrentCategory$Type;

    aput-object v1, v0, v4

    aput-object v5, v0, v7

    const/4 v1, 0x4

    const v2, 0x7f080268

    .line 378
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 376
    invoke-virtual {p1, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto :goto_4

    .line 389
    :cond_6
    iget-object v0, p0, Lru/cn/tv/stb/categories/CategoryFragment;->currentType:Lru/cn/domain/tv/CurrentCategory$Type;

    sget-object v1, Lru/cn/domain/tv/CurrentCategory$Type;->billing:Lru/cn/domain/tv/CurrentCategory$Type;

    if-eq v0, v1, :cond_7

    iget-object v0, p0, Lru/cn/tv/stb/categories/CategoryFragment;->currentType:Lru/cn/domain/tv/CurrentCategory$Type;

    sget-object v1, Lru/cn/domain/tv/CurrentCategory$Type;->setting:Lru/cn/domain/tv/CurrentCategory$Type;

    if-ne v0, v1, :cond_8

    .line 390
    :cond_7
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v6

    sget-object v1, Lru/cn/domain/tv/CurrentCategory$Type;->all:Lru/cn/domain/tv/CurrentCategory$Type;

    aput-object v1, v0, v4

    aput-object v5, v0, v7

    const/4 v1, 0x4

    const v2, 0x7f08027c

    .line 391
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 390
    invoke-virtual {p1, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 392
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v6

    sget-object v1, Lru/cn/domain/tv/CurrentCategory$Type;->collection:Lru/cn/domain/tv/CurrentCategory$Type;

    aput-object v1, v0, v4

    aput-object v5, v0, v7

    const/4 v1, 0x4

    const v2, 0x7f080269

    .line 394
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 392
    invoke-virtual {p1, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 396
    sget-object v0, Lru/cn/tv/stb/categories/CategoryFragment$2;->$SwitchMap$ru$cn$domain$tv$CurrentCategory$Type:[I

    iget-object v1, p0, Lru/cn/tv/stb/categories/CategoryFragment;->currentType:Lru/cn/domain/tv/CurrentCategory$Type;

    invoke-virtual {v1}, Lru/cn/domain/tv/CurrentCategory$Type;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_3

    goto/16 :goto_3

    .line 398
    :pswitch_9
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v6

    sget-object v1, Lru/cn/domain/tv/CurrentCategory$Type;->billing:Lru/cn/domain/tv/CurrentCategory$Type;

    aput-object v1, v0, v4

    aput-object v5, v0, v7

    const/4 v1, 0x4

    const v2, 0x7f080265

    .line 400
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 398
    invoke-virtual {p1, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_3

    .line 403
    :pswitch_a
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v6

    sget-object v1, Lru/cn/domain/tv/CurrentCategory$Type;->setting:Lru/cn/domain/tv/CurrentCategory$Type;

    aput-object v1, v0, v4

    aput-object v5, v0, v7

    const/4 v1, 0x4

    const v2, 0x7f080271

    .line 405
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 403
    invoke-virtual {p1, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_3

    .line 410
    :cond_8
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v6

    sget-object v1, Lru/cn/domain/tv/CurrentCategory$Type;->all:Lru/cn/domain/tv/CurrentCategory$Type;

    aput-object v1, v0, v4

    aput-object v5, v0, v7

    const/4 v1, 0x4

    const v2, 0x7f08027b

    .line 411
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 410
    invoke-virtual {p1, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 413
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v6

    sget-object v1, Lru/cn/domain/tv/CurrentCategory$Type;->collection:Lru/cn/domain/tv/CurrentCategory$Type;

    aput-object v1, v0, v4

    aput-object v5, v0, v7

    const/4 v1, 0x4

    const v2, 0x7f080269

    .line 415
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 413
    invoke-virtual {p1, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 416
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v6

    sget-object v1, Lru/cn/domain/tv/CurrentCategory$Type;->billing:Lru/cn/domain/tv/CurrentCategory$Type;

    aput-object v1, v0, v4

    aput-object v5, v0, v7

    const/4 v1, 0x4

    const v2, 0x7f080266

    .line 418
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 416
    invoke-virtual {p1, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_3

    .line 292
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 338
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 370
    :pswitch_data_2
    .packed-switch 0x5
        :pswitch_7
        :pswitch_8
    .end packed-switch

    .line 396
    :pswitch_data_3
    .packed-switch 0x7
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method private getItem(I)Landroid/database/Cursor;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 206
    iget-object v0, p0, Lru/cn/tv/stb/categories/CategoryFragment;->adapter:Lru/cn/tv/stb/categories/CategoryAdapter;

    invoke-virtual {v0, p1}, Lru/cn/tv/stb/categories/CategoryAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    return-object v0
.end method

.method private getItemType(I)Lru/cn/domain/tv/CurrentCategory$Type;
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 194
    if-ltz p1, :cond_0

    .line 195
    invoke-direct {p0, p1}, Lru/cn/tv/stb/categories/CategoryFragment;->getItem(I)Landroid/database/Cursor;

    move-result-object v0

    .line 196
    .local v0, "c":Landroid/database/Cursor;
    const-string v2, "type"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 197
    .local v1, "rawType":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 198
    invoke-static {v1}, Lru/cn/domain/tv/CurrentCategory$Type;->valueOf(Ljava/lang/String;)Lru/cn/domain/tv/CurrentCategory$Type;

    move-result-object v2

    .line 202
    .end local v0    # "c":Landroid/database/Cursor;
    .end local v1    # "rawType":Ljava/lang/String;
    :goto_0
    return-object v2

    :cond_0
    sget-object v2, Lru/cn/domain/tv/CurrentCategory$Type;->all:Lru/cn/domain/tv/CurrentCategory$Type;

    goto :goto_0
.end method

.method private setCategories(Lru/cn/tv/stb/categories/CategoriesInfo;)V
    .locals 0
    .param p1, "categoriesInfo"    # Lru/cn/tv/stb/categories/CategoriesInfo;

    .prologue
    .line 210
    iput-object p1, p0, Lru/cn/tv/stb/categories/CategoryFragment;->categoriesInfo:Lru/cn/tv/stb/categories/CategoriesInfo;

    .line 211
    invoke-direct {p0}, Lru/cn/tv/stb/categories/CategoryFragment;->buildAdapter()V

    .line 212
    return-void
.end method

.method private updateConfiguration(II)V
    .locals 2
    .param p1, "width"    # I
    .param p2, "gravity"    # I

    .prologue
    .line 187
    iget-object v1, p0, Lru/cn/tv/stb/categories/CategoryFragment;->container:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 188
    .local v0, "params":Landroid/widget/FrameLayout$LayoutParams;
    iput p2, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 189
    iput p1, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 190
    iget-object v1, p0, Lru/cn/tv/stb/categories/CategoryFragment;->container:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 191
    return-void
.end method


# virtual methods
.method final bridge synthetic bridge$lambda$0$CategoryFragment(Lru/cn/tv/stb/categories/CategoriesInfo;)V
    .locals 0

    invoke-direct {p0, p1}, Lru/cn/tv/stb/categories/CategoryFragment;->setCategories(Lru/cn/tv/stb/categories/CategoriesInfo;)V

    return-void
.end method

.method public categoryReload(Lru/cn/domain/tv/CurrentCategory$Type;)V
    .locals 0
    .param p1, "type"    # Lru/cn/domain/tv/CurrentCategory$Type;

    .prologue
    .line 153
    iput-object p1, p0, Lru/cn/tv/stb/categories/CategoryFragment;->currentType:Lru/cn/domain/tv/CurrentCategory$Type;

    .line 154
    return-void
.end method

.method public expand()V
    .locals 2

    .prologue
    .line 170
    iget-boolean v0, p0, Lru/cn/tv/stb/categories/CategoryFragment;->isFolded:Z

    if-nez v0, :cond_0

    .line 176
    :goto_0
    return-void

    .line 173
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lru/cn/tv/stb/categories/CategoryFragment;->isFolded:Z

    .line 174
    iget v0, p0, Lru/cn/tv/stb/categories/CategoryFragment;->expandedWidth:I

    const/16 v1, 0x30

    invoke-direct {p0, v0, v1}, Lru/cn/tv/stb/categories/CategoryFragment;->updateConfiguration(II)V

    .line 175
    invoke-direct {p0}, Lru/cn/tv/stb/categories/CategoryFragment;->buildAdapter()V

    goto :goto_0
.end method

.method public fold()V
    .locals 2

    .prologue
    .line 161
    iget-boolean v0, p0, Lru/cn/tv/stb/categories/CategoryFragment;->isFolded:Z

    if-eqz v0, :cond_0

    .line 167
    :goto_0
    return-void

    .line 164
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lru/cn/tv/stb/categories/CategoryFragment;->isFolded:Z

    .line 165
    iget v0, p0, Lru/cn/tv/stb/categories/CategoryFragment;->foldedWidth:I

    const/16 v1, 0x10

    invoke-direct {p0, v0, v1}, Lru/cn/tv/stb/categories/CategoryFragment;->updateConfiguration(II)V

    .line 166
    invoke-direct {p0}, Lru/cn/tv/stb/categories/CategoryFragment;->buildAdapter()V

    goto :goto_0
.end method

.method public final getContractorId()J
    .locals 2

    .prologue
    .line 183
    iget-object v0, p0, Lru/cn/tv/stb/categories/CategoryFragment;->categoriesInfo:Lru/cn/tv/stb/categories/CategoriesInfo;

    iget-wide v0, v0, Lru/cn/tv/stb/categories/CategoriesInfo;->contractorId:J

    return-wide v0
.end method

.method public getPositionByType(Lru/cn/domain/tv/CurrentCategory$Type;)I
    .locals 4
    .param p1, "type"    # Lru/cn/domain/tv/CurrentCategory$Type;

    .prologue
    .line 140
    if-eqz p1, :cond_1

    .line 141
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lru/cn/tv/stb/categories/CategoryFragment;->adapter:Lru/cn/tv/stb/categories/CategoryAdapter;

    invoke-virtual {v3}, Lru/cn/tv/stb/categories/CategoryAdapter;->getCount()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 142
    invoke-direct {p0, v1}, Lru/cn/tv/stb/categories/CategoryFragment;->getItem(I)Landroid/database/Cursor;

    move-result-object v0

    .line 143
    .local v0, "c":Landroid/database/Cursor;
    const-string v3, "type"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 144
    .local v2, "rawType":Ljava/lang/String;
    if-eqz v2, :cond_0

    invoke-static {v2}, Lru/cn/domain/tv/CurrentCategory$Type;->valueOf(Ljava/lang/String;)Lru/cn/domain/tv/CurrentCategory$Type;

    move-result-object v3

    if-ne p1, v3, :cond_0

    .line 149
    .end local v0    # "c":Landroid/database/Cursor;
    .end local v1    # "i":I
    .end local v2    # "rawType":Ljava/lang/String;
    :goto_1
    return v1

    .line 141
    .restart local v0    # "c":Landroid/database/Cursor;
    .restart local v1    # "i":I
    .restart local v2    # "rawType":Ljava/lang/String;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 149
    .end local v0    # "c":Landroid/database/Cursor;
    .end local v1    # "i":I
    .end local v2    # "rawType":Ljava/lang/String;
    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method

.method public final hasPrivateOffice()Z
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lru/cn/tv/stb/categories/CategoryFragment;->categoriesInfo:Lru/cn/tv/stb/categories/CategoriesInfo;

    iget-boolean v0, v0, Lru/cn/tv/stb/categories/CategoriesInfo;->hasPrivateOffice:Z

    return v0
.end method

.method public isFolded()Z
    .locals 1

    .prologue
    .line 157
    iget-boolean v0, p0, Lru/cn/tv/stb/categories/CategoryFragment;->isFolded:Z

    return v0
.end method

.method final synthetic lambda$setSelection$0$CategoryFragment(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 136
    invoke-virtual {p0}, Lru/cn/tv/stb/categories/CategoryFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->setSelection(I)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 51
    invoke-super {p0, p1}, Landroid/support/v4/app/ListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 53
    invoke-static {}, Lru/cn/tv/App;->scope()Ltoothpick/Scope;

    move-result-object v1

    const-class v2, Lru/cn/tv/stb/categories/CategoryViewModel;

    invoke-static {p0, v1, v2}, Lru/cn/mvvm/ViewModels;->get(Landroid/support/v4/app/Fragment;Ltoothpick/Scope;Ljava/lang/Class;)Landroid/arch/lifecycle/ViewModel;

    move-result-object v1

    check-cast v1, Lru/cn/tv/stb/categories/CategoryViewModel;

    iput-object v1, p0, Lru/cn/tv/stb/categories/CategoryFragment;->viewModel:Lru/cn/tv/stb/categories/CategoryViewModel;

    .line 54
    iget-object v1, p0, Lru/cn/tv/stb/categories/CategoryFragment;->viewModel:Lru/cn/tv/stb/categories/CategoryViewModel;

    invoke-virtual {v1}, Lru/cn/tv/stb/categories/CategoryViewModel;->categories()Landroid/arch/lifecycle/LiveData;

    move-result-object v1

    new-instance v2, Lru/cn/tv/stb/categories/CategoryFragment$$Lambda$0;

    invoke-direct {v2, p0}, Lru/cn/tv/stb/categories/CategoryFragment$$Lambda$0;-><init>(Lru/cn/tv/stb/categories/CategoryFragment;)V

    invoke-virtual {v1, p0, v2}, Landroid/arch/lifecycle/LiveData;->observe(Landroid/arch/lifecycle/LifecycleOwner;Landroid/arch/lifecycle/Observer;)V

    .line 56
    invoke-virtual {p0}, Lru/cn/tv/stb/categories/CategoryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lru/cn/domain/KidsObject;->isKidsMode(Landroid/content/Context;)Z

    move-result v1

    iput-boolean v1, p0, Lru/cn/tv/stb/categories/CategoryFragment;->isKidsMode:Z

    .line 59
    const v0, 0x7f0700e1

    .line 60
    .local v0, "foldedSizeRes":I
    iget-boolean v1, p0, Lru/cn/tv/stb/categories/CategoryFragment;->isKidsMode:Z

    if-eqz v1, :cond_0

    .line 61
    const v0, 0x7f0700e0

    .line 64
    :cond_0
    const v1, 0x7f0700df

    invoke-virtual {p0}, Lru/cn/tv/stb/categories/CategoryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v1, v2}, Lru/cn/utils/Utils;->dpToPx(ILandroid/content/res/Resources;)I

    move-result v1

    iput v1, p0, Lru/cn/tv/stb/categories/CategoryFragment;->expandedWidth:I

    .line 65
    invoke-virtual {p0}, Lru/cn/tv/stb/categories/CategoryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v0, v1}, Lru/cn/utils/Utils;->dpToPx(ILandroid/content/res/Resources;)I

    move-result v1

    iput v1, p0, Lru/cn/tv/stb/categories/CategoryFragment;->foldedWidth:I

    .line 66
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 71
    const v0, 0x7f0c006e

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 2
    .param p1, "listView"    # Landroid/widget/ListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 120
    iget-object v1, p0, Lru/cn/tv/stb/categories/CategoryFragment;->listener:Lru/cn/tv/stb/categories/CategoryFragment$CategoryFragmentListener;

    if-eqz v1, :cond_0

    .line 121
    invoke-direct {p0, p3}, Lru/cn/tv/stb/categories/CategoryFragment;->getItemType(I)Lru/cn/domain/tv/CurrentCategory$Type;

    move-result-object v0

    .line 122
    .local v0, "type":Lru/cn/domain/tv/CurrentCategory$Type;
    iget-object v1, p0, Lru/cn/tv/stb/categories/CategoryFragment;->listener:Lru/cn/tv/stb/categories/CategoryFragment$CategoryFragmentListener;

    invoke-interface {v1}, Lru/cn/tv/stb/categories/CategoryFragment$CategoryFragmentListener;->itemClicked()V

    .line 124
    iget-object v1, p0, Lru/cn/tv/stb/categories/CategoryFragment;->viewModel:Lru/cn/tv/stb/categories/CategoryViewModel;

    invoke-virtual {v1, v0}, Lru/cn/tv/stb/categories/CategoryViewModel;->setCategory(Lru/cn/domain/tv/CurrentCategory$Type;)V

    .line 127
    .end local v0    # "type":Lru/cn/domain/tv/CurrentCategory$Type;
    :cond_0
    invoke-super/range {p0 .. p5}, Landroid/support/v4/app/ListFragment;->onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V

    .line 128
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 7
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v1, 0x102000a

    .line 76
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/ListFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 77
    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ListView;

    .line 78
    .local v6, "listView":Landroid/widget/ListView;
    const v0, 0x7f090063

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lru/cn/tv/stb/categories/CategoryFragment;->container:Landroid/widget/FrameLayout;

    .line 81
    invoke-virtual {v6, v1}, Landroid/widget/ListView;->setNextFocusUpId(I)V

    .line 82
    invoke-virtual {v6, v1}, Landroid/widget/ListView;->setNextFocusDownId(I)V

    .line 84
    invoke-virtual {p0}, Lru/cn/tv/stb/categories/CategoryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f04007d

    invoke-static {v0, v1}, Lru/cn/utils/Utils;->resolveResourse(Landroid/content/Context;I)I

    move-result v3

    .line 85
    .local v3, "categoryFragmentItem":I
    new-instance v0, Lru/cn/tv/stb/categories/CategoryAdapter;

    invoke-virtual {p0}, Lru/cn/tv/stb/categories/CategoryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x0

    const v4, 0x7f0c0070

    const v5, 0x7f0c006f

    invoke-direct/range {v0 .. v5}, Lru/cn/tv/stb/categories/CategoryAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;III)V

    iput-object v0, p0, Lru/cn/tv/stb/categories/CategoryFragment;->adapter:Lru/cn/tv/stb/categories/CategoryAdapter;

    .line 89
    iget-object v0, p0, Lru/cn/tv/stb/categories/CategoryFragment;->adapter:Lru/cn/tv/stb/categories/CategoryAdapter;

    invoke-virtual {v6, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 90
    new-instance v0, Lru/cn/tv/stb/categories/CategoryFragment$1;

    invoke-direct {v0, p0}, Lru/cn/tv/stb/categories/CategoryFragment$1;-><init>(Lru/cn/tv/stb/categories/CategoryFragment;)V

    invoke-virtual {v6, v0}, Landroid/widget/ListView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 114
    iget v0, p0, Lru/cn/tv/stb/categories/CategoryFragment;->foldedWidth:I

    const/16 v1, 0x10

    invoke-direct {p0, v0, v1}, Lru/cn/tv/stb/categories/CategoryFragment;->updateConfiguration(II)V

    .line 115
    invoke-direct {p0}, Lru/cn/tv/stb/categories/CategoryFragment;->buildAdapter()V

    .line 116
    return-void
.end method

.method public setListener(Lru/cn/tv/stb/categories/CategoryFragment$CategoryFragmentListener;)V
    .locals 0
    .param p1, "l"    # Lru/cn/tv/stb/categories/CategoryFragment$CategoryFragmentListener;

    .prologue
    .line 131
    iput-object p1, p0, Lru/cn/tv/stb/categories/CategoryFragment;->listener:Lru/cn/tv/stb/categories/CategoryFragment$CategoryFragmentListener;

    .line 132
    return-void
.end method

.method public setSelection(Lru/cn/domain/tv/CurrentCategory$Type;)V
    .locals 3
    .param p1, "type"    # Lru/cn/domain/tv/CurrentCategory$Type;

    .prologue
    .line 135
    invoke-virtual {p0, p1}, Lru/cn/tv/stb/categories/CategoryFragment;->getPositionByType(Lru/cn/domain/tv/CurrentCategory$Type;)I

    move-result v0

    .line 136
    .local v0, "position":I
    invoke-virtual {p0}, Lru/cn/tv/stb/categories/CategoryFragment;->getListView()Landroid/widget/ListView;

    move-result-object v1

    new-instance v2, Lru/cn/tv/stb/categories/CategoryFragment$$Lambda$1;

    invoke-direct {v2, p0, v0}, Lru/cn/tv/stb/categories/CategoryFragment$$Lambda$1;-><init>(Lru/cn/tv/stb/categories/CategoryFragment;I)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->post(Ljava/lang/Runnable;)Z

    .line 137
    return-void
.end method
