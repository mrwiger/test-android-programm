.class Lru/cn/domain/statistics/inetra/TrackingSender$1;
.super Ljava/lang/Object;
.source "TrackingSender.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/domain/statistics/inetra/TrackingSender;-><init>(Landroid/content/Context;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/domain/statistics/inetra/TrackingSender;


# direct methods
.method constructor <init>(Lru/cn/domain/statistics/inetra/TrackingSender;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/domain/statistics/inetra/TrackingSender;

    .prologue
    .line 54
    iput-object p1, p0, Lru/cn/domain/statistics/inetra/TrackingSender$1;->this$0:Lru/cn/domain/statistics/inetra/TrackingSender;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v4, 0x1

    .line 58
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 76
    :cond_0
    :goto_0
    return v4

    .line 60
    :pswitch_0
    iget-object v1, p0, Lru/cn/domain/statistics/inetra/TrackingSender$1;->this$0:Lru/cn/domain/statistics/inetra/TrackingSender;

    iget-object v2, p0, Lru/cn/domain/statistics/inetra/TrackingSender$1;->this$0:Lru/cn/domain/statistics/inetra/TrackingSender;

    invoke-static {v2}, Lru/cn/domain/statistics/inetra/TrackingSender;->access$000(Lru/cn/domain/statistics/inetra/TrackingSender;)Lru/cn/domain/statistics/inetra/EventsStorage;

    move-result-object v2

    invoke-interface {v2}, Lru/cn/domain/statistics/inetra/EventsStorage;->allEvents()Ljava/util/List;

    move-result-object v2

    invoke-static {v1, v2}, Lru/cn/domain/statistics/inetra/TrackingSender;->access$100(Lru/cn/domain/statistics/inetra/TrackingSender;Ljava/util/List;)V

    goto :goto_0

    .line 64
    :pswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lru/cn/domain/statistics/inetra/TrackingEvent;

    .line 65
    .local v0, "event":Lru/cn/domain/statistics/inetra/TrackingEvent;
    iget-object v1, p0, Lru/cn/domain/statistics/inetra/TrackingSender$1;->this$0:Lru/cn/domain/statistics/inetra/TrackingSender;

    invoke-static {v1}, Lru/cn/domain/statistics/inetra/TrackingSender;->access$000(Lru/cn/domain/statistics/inetra/TrackingSender;)Lru/cn/domain/statistics/inetra/EventsStorage;

    move-result-object v1

    invoke-interface {v1, v0}, Lru/cn/domain/statistics/inetra/EventsStorage;->contains(Lru/cn/domain/statistics/inetra/TrackingEvent;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 66
    iget-object v1, p0, Lru/cn/domain/statistics/inetra/TrackingSender$1;->this$0:Lru/cn/domain/statistics/inetra/TrackingSender;

    invoke-static {v1}, Lru/cn/domain/statistics/inetra/TrackingSender;->access$000(Lru/cn/domain/statistics/inetra/TrackingSender;)Lru/cn/domain/statistics/inetra/EventsStorage;

    move-result-object v1

    invoke-interface {v1, v0}, Lru/cn/domain/statistics/inetra/EventsStorage;->addEvent(Lru/cn/domain/statistics/inetra/TrackingEvent;)V

    .line 69
    :cond_1
    iget-object v1, p0, Lru/cn/domain/statistics/inetra/TrackingSender$1;->this$0:Lru/cn/domain/statistics/inetra/TrackingSender;

    invoke-static {v1}, Lru/cn/domain/statistics/inetra/TrackingSender;->access$200(Lru/cn/domain/statistics/inetra/TrackingSender;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 70
    iget-object v1, p0, Lru/cn/domain/statistics/inetra/TrackingSender$1;->this$0:Lru/cn/domain/statistics/inetra/TrackingSender;

    invoke-static {v1}, Lru/cn/domain/statistics/inetra/TrackingSender;->access$200(Lru/cn/domain/statistics/inetra/TrackingSender;)Landroid/os/Handler;

    move-result-object v1

    const-wide/32 v2, 0x1d4c0

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 58
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
