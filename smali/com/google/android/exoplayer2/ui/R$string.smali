.class public final Lcom/google/android/exoplayer2/ui/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/exoplayer2/ui/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final exo_controls_fastforward_description:I = 0x7f0e00aa

.field public static final exo_controls_next_description:I = 0x7f0e00ab

.field public static final exo_controls_pause_description:I = 0x7f0e00ac

.field public static final exo_controls_play_description:I = 0x7f0e00ad

.field public static final exo_controls_previous_description:I = 0x7f0e00ae

.field public static final exo_controls_repeat_all_description:I = 0x7f0e00af

.field public static final exo_controls_repeat_off_description:I = 0x7f0e00b0

.field public static final exo_controls_repeat_one_description:I = 0x7f0e00b1

.field public static final exo_controls_rewind_description:I = 0x7f0e00b2

.field public static final exo_controls_shuffle_description:I = 0x7f0e00b3

.field public static final exo_controls_stop_description:I = 0x7f0e00b4
