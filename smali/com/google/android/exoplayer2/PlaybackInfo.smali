.class final Lcom/google/android/exoplayer2/PlaybackInfo;
.super Ljava/lang/Object;
.source "PlaybackInfo.java"


# instance fields
.field public volatile bufferedPositionUs:J

.field public final contentPositionUs:J

.field public final isLoading:Z

.field public final manifest:Ljava/lang/Object;

.field public final periodId:Lcom/google/android/exoplayer2/source/MediaSource$MediaPeriodId;

.field public final playbackState:I

.field public volatile positionUs:J

.field public final startPositionUs:J

.field public final timeline:Lcom/google/android/exoplayer2/Timeline;

.field public final trackSelectorResult:Lcom/google/android/exoplayer2/trackselection/TrackSelectorResult;


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer2/Timeline;JLcom/google/android/exoplayer2/trackselection/TrackSelectorResult;)V
    .locals 12
    .param p1, "timeline"    # Lcom/google/android/exoplayer2/Timeline;
    .param p2, "startPositionUs"    # J
    .param p4, "trackSelectorResult"    # Lcom/google/android/exoplayer2/trackselection/TrackSelectorResult;

    .prologue
    const/4 v9, 0x0

    .line 41
    const/4 v2, 0x0

    new-instance v3, Lcom/google/android/exoplayer2/source/MediaSource$MediaPeriodId;

    invoke-direct {v3, v9}, Lcom/google/android/exoplayer2/source/MediaSource$MediaPeriodId;-><init>(I)V

    const-wide v6, -0x7fffffffffffffffL    # -4.9E-324

    const/4 v8, 0x1

    move-object v0, p0

    move-object v1, p1

    move-wide v4, p2

    move-object/from16 v10, p4

    invoke-direct/range {v0 .. v10}, Lcom/google/android/exoplayer2/PlaybackInfo;-><init>(Lcom/google/android/exoplayer2/Timeline;Ljava/lang/Object;Lcom/google/android/exoplayer2/source/MediaSource$MediaPeriodId;JJIZLcom/google/android/exoplayer2/trackselection/TrackSelectorResult;)V

    .line 50
    return-void
.end method

.method public constructor <init>(Lcom/google/android/exoplayer2/Timeline;Ljava/lang/Object;Lcom/google/android/exoplayer2/source/MediaSource$MediaPeriodId;JJIZLcom/google/android/exoplayer2/trackselection/TrackSelectorResult;)V
    .locals 0
    .param p1, "timeline"    # Lcom/google/android/exoplayer2/Timeline;
    .param p2, "manifest"    # Ljava/lang/Object;
    .param p3, "periodId"    # Lcom/google/android/exoplayer2/source/MediaSource$MediaPeriodId;
    .param p4, "startPositionUs"    # J
    .param p6, "contentPositionUs"    # J
    .param p8, "playbackState"    # I
    .param p9, "isLoading"    # Z
    .param p10, "trackSelectorResult"    # Lcom/google/android/exoplayer2/trackselection/TrackSelectorResult;

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-object p1, p0, Lcom/google/android/exoplayer2/PlaybackInfo;->timeline:Lcom/google/android/exoplayer2/Timeline;

    .line 62
    iput-object p2, p0, Lcom/google/android/exoplayer2/PlaybackInfo;->manifest:Ljava/lang/Object;

    .line 63
    iput-object p3, p0, Lcom/google/android/exoplayer2/PlaybackInfo;->periodId:Lcom/google/android/exoplayer2/source/MediaSource$MediaPeriodId;

    .line 64
    iput-wide p4, p0, Lcom/google/android/exoplayer2/PlaybackInfo;->startPositionUs:J

    .line 65
    iput-wide p6, p0, Lcom/google/android/exoplayer2/PlaybackInfo;->contentPositionUs:J

    .line 66
    iput-wide p4, p0, Lcom/google/android/exoplayer2/PlaybackInfo;->positionUs:J

    .line 67
    iput-wide p4, p0, Lcom/google/android/exoplayer2/PlaybackInfo;->bufferedPositionUs:J

    .line 68
    iput p8, p0, Lcom/google/android/exoplayer2/PlaybackInfo;->playbackState:I

    .line 69
    iput-boolean p9, p0, Lcom/google/android/exoplayer2/PlaybackInfo;->isLoading:Z

    .line 70
    iput-object p10, p0, Lcom/google/android/exoplayer2/PlaybackInfo;->trackSelectorResult:Lcom/google/android/exoplayer2/trackselection/TrackSelectorResult;

    .line 71
    return-void
.end method

.method private static copyMutablePositions(Lcom/google/android/exoplayer2/PlaybackInfo;Lcom/google/android/exoplayer2/PlaybackInfo;)V
    .locals 2
    .param p0, "from"    # Lcom/google/android/exoplayer2/PlaybackInfo;
    .param p1, "to"    # Lcom/google/android/exoplayer2/PlaybackInfo;

    .prologue
    .line 162
    iget-wide v0, p0, Lcom/google/android/exoplayer2/PlaybackInfo;->positionUs:J

    iput-wide v0, p1, Lcom/google/android/exoplayer2/PlaybackInfo;->positionUs:J

    .line 163
    iget-wide v0, p0, Lcom/google/android/exoplayer2/PlaybackInfo;->bufferedPositionUs:J

    iput-wide v0, p1, Lcom/google/android/exoplayer2/PlaybackInfo;->bufferedPositionUs:J

    .line 164
    return-void
.end method


# virtual methods
.method public copyWithIsLoading(Z)Lcom/google/android/exoplayer2/PlaybackInfo;
    .locals 11
    .param p1, "isLoading"    # Z

    .prologue
    .line 132
    new-instance v0, Lcom/google/android/exoplayer2/PlaybackInfo;

    iget-object v1, p0, Lcom/google/android/exoplayer2/PlaybackInfo;->timeline:Lcom/google/android/exoplayer2/Timeline;

    iget-object v2, p0, Lcom/google/android/exoplayer2/PlaybackInfo;->manifest:Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/exoplayer2/PlaybackInfo;->periodId:Lcom/google/android/exoplayer2/source/MediaSource$MediaPeriodId;

    iget-wide v4, p0, Lcom/google/android/exoplayer2/PlaybackInfo;->startPositionUs:J

    iget-wide v6, p0, Lcom/google/android/exoplayer2/PlaybackInfo;->contentPositionUs:J

    iget v8, p0, Lcom/google/android/exoplayer2/PlaybackInfo;->playbackState:I

    iget-object v10, p0, Lcom/google/android/exoplayer2/PlaybackInfo;->trackSelectorResult:Lcom/google/android/exoplayer2/trackselection/TrackSelectorResult;

    move v9, p1

    invoke-direct/range {v0 .. v10}, Lcom/google/android/exoplayer2/PlaybackInfo;-><init>(Lcom/google/android/exoplayer2/Timeline;Ljava/lang/Object;Lcom/google/android/exoplayer2/source/MediaSource$MediaPeriodId;JJIZLcom/google/android/exoplayer2/trackselection/TrackSelectorResult;)V

    .line 142
    .local v0, "playbackInfo":Lcom/google/android/exoplayer2/PlaybackInfo;
    invoke-static {p0, v0}, Lcom/google/android/exoplayer2/PlaybackInfo;->copyMutablePositions(Lcom/google/android/exoplayer2/PlaybackInfo;Lcom/google/android/exoplayer2/PlaybackInfo;)V

    .line 143
    return-object v0
.end method

.method public copyWithPeriodIndex(I)Lcom/google/android/exoplayer2/PlaybackInfo;
    .locals 11
    .param p1, "periodIndex"    # I

    .prologue
    .line 87
    new-instance v0, Lcom/google/android/exoplayer2/PlaybackInfo;

    iget-object v1, p0, Lcom/google/android/exoplayer2/PlaybackInfo;->timeline:Lcom/google/android/exoplayer2/Timeline;

    iget-object v2, p0, Lcom/google/android/exoplayer2/PlaybackInfo;->manifest:Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/exoplayer2/PlaybackInfo;->periodId:Lcom/google/android/exoplayer2/source/MediaSource$MediaPeriodId;

    .line 91
    invoke-virtual {v3, p1}, Lcom/google/android/exoplayer2/source/MediaSource$MediaPeriodId;->copyWithPeriodIndex(I)Lcom/google/android/exoplayer2/source/MediaSource$MediaPeriodId;

    move-result-object v3

    iget-wide v4, p0, Lcom/google/android/exoplayer2/PlaybackInfo;->startPositionUs:J

    iget-wide v6, p0, Lcom/google/android/exoplayer2/PlaybackInfo;->contentPositionUs:J

    iget v8, p0, Lcom/google/android/exoplayer2/PlaybackInfo;->playbackState:I

    iget-boolean v9, p0, Lcom/google/android/exoplayer2/PlaybackInfo;->isLoading:Z

    iget-object v10, p0, Lcom/google/android/exoplayer2/PlaybackInfo;->trackSelectorResult:Lcom/google/android/exoplayer2/trackselection/TrackSelectorResult;

    invoke-direct/range {v0 .. v10}, Lcom/google/android/exoplayer2/PlaybackInfo;-><init>(Lcom/google/android/exoplayer2/Timeline;Ljava/lang/Object;Lcom/google/android/exoplayer2/source/MediaSource$MediaPeriodId;JJIZLcom/google/android/exoplayer2/trackselection/TrackSelectorResult;)V

    .line 97
    .local v0, "playbackInfo":Lcom/google/android/exoplayer2/PlaybackInfo;
    invoke-static {p0, v0}, Lcom/google/android/exoplayer2/PlaybackInfo;->copyMutablePositions(Lcom/google/android/exoplayer2/PlaybackInfo;Lcom/google/android/exoplayer2/PlaybackInfo;)V

    .line 98
    return-object v0
.end method

.method public copyWithPlaybackState(I)Lcom/google/android/exoplayer2/PlaybackInfo;
    .locals 11
    .param p1, "playbackState"    # I

    .prologue
    .line 117
    new-instance v0, Lcom/google/android/exoplayer2/PlaybackInfo;

    iget-object v1, p0, Lcom/google/android/exoplayer2/PlaybackInfo;->timeline:Lcom/google/android/exoplayer2/Timeline;

    iget-object v2, p0, Lcom/google/android/exoplayer2/PlaybackInfo;->manifest:Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/exoplayer2/PlaybackInfo;->periodId:Lcom/google/android/exoplayer2/source/MediaSource$MediaPeriodId;

    iget-wide v4, p0, Lcom/google/android/exoplayer2/PlaybackInfo;->startPositionUs:J

    iget-wide v6, p0, Lcom/google/android/exoplayer2/PlaybackInfo;->contentPositionUs:J

    iget-boolean v9, p0, Lcom/google/android/exoplayer2/PlaybackInfo;->isLoading:Z

    iget-object v10, p0, Lcom/google/android/exoplayer2/PlaybackInfo;->trackSelectorResult:Lcom/google/android/exoplayer2/trackselection/TrackSelectorResult;

    move v8, p1

    invoke-direct/range {v0 .. v10}, Lcom/google/android/exoplayer2/PlaybackInfo;-><init>(Lcom/google/android/exoplayer2/Timeline;Ljava/lang/Object;Lcom/google/android/exoplayer2/source/MediaSource$MediaPeriodId;JJIZLcom/google/android/exoplayer2/trackselection/TrackSelectorResult;)V

    .line 127
    .local v0, "playbackInfo":Lcom/google/android/exoplayer2/PlaybackInfo;
    invoke-static {p0, v0}, Lcom/google/android/exoplayer2/PlaybackInfo;->copyMutablePositions(Lcom/google/android/exoplayer2/PlaybackInfo;Lcom/google/android/exoplayer2/PlaybackInfo;)V

    .line 128
    return-object v0
.end method

.method public copyWithTimeline(Lcom/google/android/exoplayer2/Timeline;Ljava/lang/Object;)Lcom/google/android/exoplayer2/PlaybackInfo;
    .locals 11
    .param p1, "timeline"    # Lcom/google/android/exoplayer2/Timeline;
    .param p2, "manifest"    # Ljava/lang/Object;

    .prologue
    .line 102
    new-instance v0, Lcom/google/android/exoplayer2/PlaybackInfo;

    iget-object v3, p0, Lcom/google/android/exoplayer2/PlaybackInfo;->periodId:Lcom/google/android/exoplayer2/source/MediaSource$MediaPeriodId;

    iget-wide v4, p0, Lcom/google/android/exoplayer2/PlaybackInfo;->startPositionUs:J

    iget-wide v6, p0, Lcom/google/android/exoplayer2/PlaybackInfo;->contentPositionUs:J

    iget v8, p0, Lcom/google/android/exoplayer2/PlaybackInfo;->playbackState:I

    iget-boolean v9, p0, Lcom/google/android/exoplayer2/PlaybackInfo;->isLoading:Z

    iget-object v10, p0, Lcom/google/android/exoplayer2/PlaybackInfo;->trackSelectorResult:Lcom/google/android/exoplayer2/trackselection/TrackSelectorResult;

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v10}, Lcom/google/android/exoplayer2/PlaybackInfo;-><init>(Lcom/google/android/exoplayer2/Timeline;Ljava/lang/Object;Lcom/google/android/exoplayer2/source/MediaSource$MediaPeriodId;JJIZLcom/google/android/exoplayer2/trackselection/TrackSelectorResult;)V

    .line 112
    .local v0, "playbackInfo":Lcom/google/android/exoplayer2/PlaybackInfo;
    invoke-static {p0, v0}, Lcom/google/android/exoplayer2/PlaybackInfo;->copyMutablePositions(Lcom/google/android/exoplayer2/PlaybackInfo;Lcom/google/android/exoplayer2/PlaybackInfo;)V

    .line 113
    return-object v0
.end method

.method public copyWithTrackSelectorResult(Lcom/google/android/exoplayer2/trackselection/TrackSelectorResult;)Lcom/google/android/exoplayer2/PlaybackInfo;
    .locals 11
    .param p1, "trackSelectorResult"    # Lcom/google/android/exoplayer2/trackselection/TrackSelectorResult;

    .prologue
    .line 147
    new-instance v0, Lcom/google/android/exoplayer2/PlaybackInfo;

    iget-object v1, p0, Lcom/google/android/exoplayer2/PlaybackInfo;->timeline:Lcom/google/android/exoplayer2/Timeline;

    iget-object v2, p0, Lcom/google/android/exoplayer2/PlaybackInfo;->manifest:Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/exoplayer2/PlaybackInfo;->periodId:Lcom/google/android/exoplayer2/source/MediaSource$MediaPeriodId;

    iget-wide v4, p0, Lcom/google/android/exoplayer2/PlaybackInfo;->startPositionUs:J

    iget-wide v6, p0, Lcom/google/android/exoplayer2/PlaybackInfo;->contentPositionUs:J

    iget v8, p0, Lcom/google/android/exoplayer2/PlaybackInfo;->playbackState:I

    iget-boolean v9, p0, Lcom/google/android/exoplayer2/PlaybackInfo;->isLoading:Z

    move-object v10, p1

    invoke-direct/range {v0 .. v10}, Lcom/google/android/exoplayer2/PlaybackInfo;-><init>(Lcom/google/android/exoplayer2/Timeline;Ljava/lang/Object;Lcom/google/android/exoplayer2/source/MediaSource$MediaPeriodId;JJIZLcom/google/android/exoplayer2/trackselection/TrackSelectorResult;)V

    .line 157
    .local v0, "playbackInfo":Lcom/google/android/exoplayer2/PlaybackInfo;
    invoke-static {p0, v0}, Lcom/google/android/exoplayer2/PlaybackInfo;->copyMutablePositions(Lcom/google/android/exoplayer2/PlaybackInfo;Lcom/google/android/exoplayer2/PlaybackInfo;)V

    .line 158
    return-object v0
.end method

.method public fromNewPosition(Lcom/google/android/exoplayer2/source/MediaSource$MediaPeriodId;JJ)Lcom/google/android/exoplayer2/PlaybackInfo;
    .locals 12
    .param p1, "periodId"    # Lcom/google/android/exoplayer2/source/MediaSource$MediaPeriodId;
    .param p2, "startPositionUs"    # J
    .param p4, "contentPositionUs"    # J

    .prologue
    .line 75
    new-instance v0, Lcom/google/android/exoplayer2/PlaybackInfo;

    iget-object v1, p0, Lcom/google/android/exoplayer2/PlaybackInfo;->timeline:Lcom/google/android/exoplayer2/Timeline;

    iget-object v2, p0, Lcom/google/android/exoplayer2/PlaybackInfo;->manifest:Ljava/lang/Object;

    .line 80
    invoke-virtual {p1}, Lcom/google/android/exoplayer2/source/MediaSource$MediaPeriodId;->isAd()Z

    move-result v3

    if-eqz v3, :cond_0

    move-wide/from16 v6, p4

    :goto_0
    iget v8, p0, Lcom/google/android/exoplayer2/PlaybackInfo;->playbackState:I

    iget-boolean v9, p0, Lcom/google/android/exoplayer2/PlaybackInfo;->isLoading:Z

    iget-object v10, p0, Lcom/google/android/exoplayer2/PlaybackInfo;->trackSelectorResult:Lcom/google/android/exoplayer2/trackselection/TrackSelectorResult;

    move-object v3, p1

    move-wide v4, p2

    invoke-direct/range {v0 .. v10}, Lcom/google/android/exoplayer2/PlaybackInfo;-><init>(Lcom/google/android/exoplayer2/Timeline;Ljava/lang/Object;Lcom/google/android/exoplayer2/source/MediaSource$MediaPeriodId;JJIZLcom/google/android/exoplayer2/trackselection/TrackSelectorResult;)V

    .line 75
    return-object v0

    .line 80
    :cond_0
    const-wide v6, -0x7fffffffffffffffL    # -4.9E-324

    goto :goto_0
.end method
