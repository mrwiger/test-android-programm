.class public abstract Lru/cn/ad/AdAdapter;
.super Ljava/lang/Object;
.source "AdAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/ad/AdAdapter$Listener;,
        Lru/cn/ad/AdAdapter$Factory;
    }
.end annotation


# instance fields
.field public final adSystem:Lru/cn/api/money_miner/replies/AdSystem;

.field protected final context:Landroid/content/Context;

.field protected listener:Lru/cn/ad/AdAdapter$Listener;

.field private reporter:Lru/cn/ad/AdEventReporter;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lru/cn/api/money_miner/replies/AdSystem;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "system"    # Lru/cn/api/money_miner/replies/AdSystem;

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lru/cn/ad/AdAdapter;->context:Landroid/content/Context;

    .line 49
    iput-object p2, p0, Lru/cn/ad/AdAdapter;->adSystem:Lru/cn/api/money_miner/replies/AdSystem;

    .line 50
    return-void
.end method


# virtual methods
.method public abstract destroy()V
.end method

.method public final eligble(Ljava/util/List;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 53
    .local p1, "metaTags":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    iget-object v0, p0, Lru/cn/ad/AdAdapter;->adSystem:Lru/cn/api/money_miner/replies/AdSystem;

    iget-object v0, v0, Lru/cn/api/money_miner/replies/AdSystem;->predicate:Lru/cn/api/money_miner/replies/AdPredicate;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lru/cn/ad/AdAdapter;->adSystem:Lru/cn/api/money_miner/replies/AdSystem;

    iget-object v0, v0, Lru/cn/api/money_miner/replies/AdSystem;->predicate:Lru/cn/api/money_miner/replies/AdPredicate;

    .line 54
    invoke-virtual {v0, p1}, Lru/cn/api/money_miner/replies/AdPredicate;->matches(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 53
    :goto_0
    return v0

    .line 54
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final load()V
    .locals 2

    .prologue
    .line 65
    invoke-virtual {p0}, Lru/cn/ad/AdAdapter;->onLoad()V

    .line 67
    sget-object v0, Lru/cn/domain/statistics/inetra/AdvEvent;->ADV_EVENT_REQUEST:Lru/cn/domain/statistics/inetra/AdvEvent;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lru/cn/ad/AdAdapter;->reportEvent(Lru/cn/domain/statistics/inetra/AdvEvent;Ljava/util/Map;)V

    .line 68
    return-void
.end method

.method protected abstract onLoad()V
.end method

.method public final reportError(Lru/cn/domain/statistics/inetra/ErrorCode;Ljava/lang/String;ILjava/util/Map;)V
    .locals 6
    .param p1, "code"    # Lru/cn/domain/statistics/inetra/ErrorCode;
    .param p2, "domain"    # Ljava/lang/String;
    .param p3, "errorCode"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lru/cn/domain/statistics/inetra/ErrorCode;",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 104
    .local p4, "extra":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v0, p0, Lru/cn/ad/AdAdapter;->reporter:Lru/cn/ad/AdEventReporter;

    iget-object v2, p0, Lru/cn/ad/AdAdapter;->adSystem:Lru/cn/api/money_miner/replies/AdSystem;

    move-object v1, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lru/cn/ad/AdEventReporter;->reportError(Lru/cn/domain/statistics/inetra/ErrorCode;Lru/cn/api/money_miner/replies/AdSystem;Ljava/lang/String;ILjava/util/Map;)V

    .line 105
    return-void
.end method

.method public final reportEvent(Lru/cn/domain/statistics/inetra/AdvEvent;Ljava/util/Map;)V
    .locals 2
    .param p1, "eventId"    # Lru/cn/domain/statistics/inetra/AdvEvent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lru/cn/domain/statistics/inetra/AdvEvent;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 100
    .local p2, "extra":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v0, p0, Lru/cn/ad/AdAdapter;->reporter:Lru/cn/ad/AdEventReporter;

    iget-object v1, p0, Lru/cn/ad/AdAdapter;->adSystem:Lru/cn/api/money_miner/replies/AdSystem;

    invoke-virtual {v0, p1, v1, p2}, Lru/cn/ad/AdEventReporter;->reportEvent(Lru/cn/domain/statistics/inetra/AdvEvent;Lru/cn/api/money_miner/replies/AdSystem;Ljava/util/Map;)V

    .line 101
    return-void
.end method

.method public final setListener(Lru/cn/ad/AdAdapter$Listener;)V
    .locals 0
    .param p1, "listener"    # Lru/cn/ad/AdAdapter$Listener;

    .prologue
    .line 58
    iput-object p1, p0, Lru/cn/ad/AdAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    .line 59
    return-void
.end method

.method public setReporter(Lru/cn/ad/AdEventReporter;)V
    .locals 0
    .param p1, "reporter"    # Lru/cn/ad/AdEventReporter;

    .prologue
    .line 96
    iput-object p1, p0, Lru/cn/ad/AdAdapter;->reporter:Lru/cn/ad/AdEventReporter;

    .line 97
    return-void
.end method

.method public abstract show()V
.end method

.method public stop()V
    .locals 0

    .prologue
    .line 88
    return-void
.end method
