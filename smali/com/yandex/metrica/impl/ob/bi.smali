.class public Lcom/yandex/metrica/impl/ob/bi;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/yandex/metrica/impl/ob/bi$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/yandex/metrica/impl/ob/bp;

.field private final b:Ljava/lang/Long;

.field private final c:Ljava/lang/Long;

.field private final d:Ljava/lang/Integer;

.field private final e:Ljava/lang/Long;

.field private final f:Ljava/lang/Boolean;

.field private final g:Ljava/lang/Long;


# direct methods
.method private constructor <init>(Lcom/yandex/metrica/impl/ob/bi$a;)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    invoke-static {p1}, Lcom/yandex/metrica/impl/ob/bi$a;->a(Lcom/yandex/metrica/impl/ob/bi$a;)Lcom/yandex/metrica/impl/ob/bp;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/bi;->a:Lcom/yandex/metrica/impl/ob/bp;

    .line 30
    invoke-static {p1}, Lcom/yandex/metrica/impl/ob/bi$a;->b(Lcom/yandex/metrica/impl/ob/bi$a;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/bi;->d:Ljava/lang/Integer;

    .line 31
    invoke-static {p1}, Lcom/yandex/metrica/impl/ob/bi$a;->c(Lcom/yandex/metrica/impl/ob/bi$a;)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/bi;->b:Ljava/lang/Long;

    .line 32
    invoke-static {p1}, Lcom/yandex/metrica/impl/ob/bi$a;->d(Lcom/yandex/metrica/impl/ob/bi$a;)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/bi;->c:Ljava/lang/Long;

    .line 33
    invoke-static {p1}, Lcom/yandex/metrica/impl/ob/bi$a;->e(Lcom/yandex/metrica/impl/ob/bi$a;)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/bi;->e:Ljava/lang/Long;

    .line 34
    invoke-static {p1}, Lcom/yandex/metrica/impl/ob/bi$a;->f(Lcom/yandex/metrica/impl/ob/bi$a;)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/bi;->f:Ljava/lang/Boolean;

    .line 35
    invoke-static {p1}, Lcom/yandex/metrica/impl/ob/bi$a;->g(Lcom/yandex/metrica/impl/ob/bi$a;)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/bi;->g:Ljava/lang/Long;

    .line 36
    return-void
.end method

.method synthetic constructor <init>(Lcom/yandex/metrica/impl/ob/bi$a;B)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/yandex/metrica/impl/ob/bi;-><init>(Lcom/yandex/metrica/impl/ob/bi$a;)V

    return-void
.end method


# virtual methods
.method public a(I)I
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bi;->d:Ljava/lang/Integer;

    if-nez v0, :cond_0

    :goto_0
    return p1

    :cond_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bi;->d:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result p1

    goto :goto_0
.end method

.method public a(J)J
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bi;->b:Ljava/lang/Long;

    if-nez v0, :cond_0

    :goto_0
    return-wide p1

    :cond_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bi;->b:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide p1

    goto :goto_0
.end method

.method public a()Lcom/yandex/metrica/impl/ob/bp;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bi;->a:Lcom/yandex/metrica/impl/ob/bp;

    return-object v0
.end method

.method public a(Z)Z
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bi;->f:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    :goto_0
    return p1

    :cond_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bi;->f:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    goto :goto_0
.end method

.method public b(J)J
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bi;->c:Ljava/lang/Long;

    if-nez v0, :cond_0

    :goto_0
    return-wide p1

    :cond_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bi;->c:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide p1

    goto :goto_0
.end method

.method public c(J)J
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bi;->e:Ljava/lang/Long;

    if-nez v0, :cond_0

    :goto_0
    return-wide p1

    :cond_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bi;->e:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide p1

    goto :goto_0
.end method

.method public d(J)J
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bi;->g:Ljava/lang/Long;

    if-nez v0, :cond_0

    :goto_0
    return-wide p1

    :cond_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bi;->g:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide p1

    goto :goto_0
.end method
