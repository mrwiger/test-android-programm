.class Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter$LiveNowItemViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "LiveNowRecyclerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "LiveNowItemViewHolder"
.end annotation


# instance fields
.field channelTitle:Landroid/widget/TextView;

.field description:Landroid/widget/TextView;

.field startEndTime:Landroid/widget/TextView;

.field telecastImage:Landroid/widget/ImageView;

.field telecastProgress:Landroid/widget/ProgressBar;

.field telecastTitle:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Landroid/view/View;)V
    .locals 1
    .param p1, "itemView"    # Landroid/view/View;

    .prologue
    .line 195
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 196
    const v0, 0x7f09010f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter$LiveNowItemViewHolder;->telecastImage:Landroid/widget/ImageView;

    .line 197
    const v0, 0x7f09008f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter$LiveNowItemViewHolder;->telecastProgress:Landroid/widget/ProgressBar;

    .line 198
    const v0, 0x7f090111

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter$LiveNowItemViewHolder;->telecastTitle:Landroid/widget/TextView;

    .line 199
    const v0, 0x7f090110

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter$LiveNowItemViewHolder;->startEndTime:Landroid/widget/TextView;

    .line 200
    const v0, 0x7f09010d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter$LiveNowItemViewHolder;->channelTitle:Landroid/widget/TextView;

    .line 201
    const v0, 0x7f09010e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter$LiveNowItemViewHolder;->description:Landroid/widget/TextView;

    .line 202
    return-void
.end method
