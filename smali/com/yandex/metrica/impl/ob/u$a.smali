.class Lcom/yandex/metrica/impl/ob/u$a;
.super Lcom/yandex/metrica/impl/ob/u$f;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/yandex/metrica/impl/ob/u;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "a"
.end annotation


# instance fields
.field private final a:Lcom/yandex/metrica/impl/ob/fh;

.field private final b:Lcom/yandex/metrica/impl/ob/dd;


# direct methods
.method constructor <init>(Lcom/yandex/metrica/impl/ob/v;)V
    .locals 3

    .prologue
    .line 189
    invoke-direct {p0, p1}, Lcom/yandex/metrica/impl/ob/u$f;-><init>(Lcom/yandex/metrica/impl/ob/v;)V

    .line 190
    new-instance v0, Lcom/yandex/metrica/impl/ob/fh;

    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ob/v;->n()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ob/v;->m()Lcom/yandex/metrica/impl/ob/t;

    move-result-object v2

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/ob/t;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/yandex/metrica/impl/ob/fh;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/u$a;->a:Lcom/yandex/metrica/impl/ob/fh;

    .line 191
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ob/v;->G()Lcom/yandex/metrica/impl/ob/dd;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/u$a;->b:Lcom/yandex/metrica/impl/ob/dd;

    .line 192
    return-void
.end method


# virtual methods
.method protected a()Z
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/u$a;->a:Lcom/yandex/metrica/impl/ob/fh;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fh;->e()Z

    move-result v0

    return v0
.end method

.method protected b()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const-wide/high16 v4, -0x8000000000000000L

    .line 202
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/u$a;->d()V

    .line 203
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/u$a;->c()V

    .line 1209
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/u$a;->a:Lcom/yandex/metrica/impl/ob/fh;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fh;->a()Lcom/yandex/metrica/impl/a$a;

    move-result-object v0

    .line 1210
    if-eqz v0, :cond_0

    .line 1211
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/u$a;->b:Lcom/yandex/metrica/impl/ob/dd;

    invoke-virtual {v1, v0}, Lcom/yandex/metrica/impl/ob/dd;->a(Lcom/yandex/metrica/impl/a$a;)Lcom/yandex/metrica/impl/ob/dd;

    .line 1213
    :cond_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/u$a;->a:Lcom/yandex/metrica/impl/ob/fh;

    invoke-virtual {v0, v2}, Lcom/yandex/metrica/impl/ob/fh;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1214
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/u$a;->b:Lcom/yandex/metrica/impl/ob/dd;

    invoke-virtual {v1, v2}, Lcom/yandex/metrica/impl/ob/dd;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1215
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/u$a;->b:Lcom/yandex/metrica/impl/ob/dd;

    invoke-virtual {v1, v0}, Lcom/yandex/metrica/impl/ob/dd;->b(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/dd;

    .line 1217
    :cond_1
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/u$a;->a:Lcom/yandex/metrica/impl/ob/fh;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fh;->b()Lcom/yandex/metrica/CounterConfiguration$a;

    move-result-object v0

    .line 1218
    sget-object v1, Lcom/yandex/metrica/CounterConfiguration$a;->a:Lcom/yandex/metrica/CounterConfiguration$a;

    if-eq v0, v1, :cond_2

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/u$a;->b:Lcom/yandex/metrica/impl/ob/dd;

    .line 1219
    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/dd;->c()Lcom/yandex/metrica/CounterConfiguration$a;

    move-result-object v1

    sget-object v2, Lcom/yandex/metrica/CounterConfiguration$a;->a:Lcom/yandex/metrica/CounterConfiguration$a;

    if-ne v1, v2, :cond_2

    .line 1220
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/u$a;->b:Lcom/yandex/metrica/impl/ob/dd;

    invoke-virtual {v1, v0}, Lcom/yandex/metrica/impl/ob/dd;->a(Lcom/yandex/metrica/CounterConfiguration$a;)Lcom/yandex/metrica/impl/ob/dd;

    .line 1222
    :cond_2
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/u$a;->a:Lcom/yandex/metrica/impl/ob/fh;

    invoke-virtual {v0, v4, v5}, Lcom/yandex/metrica/impl/ob/fh;->c(J)J

    move-result-wide v0

    .line 1223
    cmp-long v2, v0, v4

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/u$a;->b:Lcom/yandex/metrica/impl/ob/dd;

    invoke-virtual {v2, v4, v5}, Lcom/yandex/metrica/impl/ob/dd;->a(J)J

    move-result-wide v2

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    .line 1224
    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/u$a;->b:Lcom/yandex/metrica/impl/ob/dd;

    invoke-virtual {v2, v0, v1}, Lcom/yandex/metrica/impl/ob/dd;->c(J)Lcom/yandex/metrica/impl/ob/dd;

    .line 1227
    :cond_3
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/u$a;->b:Lcom/yandex/metrica/impl/ob/dd;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/dd;->g()V

    .line 205
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/u$a;->a:Lcom/yandex/metrica/impl/ob/fh;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fh;->g()V

    .line 206
    return-void
.end method

.method c()V
    .locals 10

    .prologue
    const-wide/16 v8, -0x1

    const-wide/high16 v6, -0x8000000000000000L

    const-wide/16 v4, 0x0

    .line 232
    new-instance v0, Lcom/yandex/metrica/impl/ob/bo;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/u$a;->b:Lcom/yandex/metrica/impl/ob/dd;

    const-string v2, "foreground"

    invoke-direct {v0, v1, v2}, Lcom/yandex/metrica/impl/ob/bo;-><init>(Lcom/yandex/metrica/impl/ob/dd;Ljava/lang/String;)V

    .line 233
    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/bo;->h()Z

    move-result v1

    if-nez v1, :cond_5

    .line 234
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/u$a;->a:Lcom/yandex/metrica/impl/ob/fh;

    invoke-virtual {v1, v8, v9}, Lcom/yandex/metrica/impl/ob/fh;->d(J)J

    move-result-wide v2

    .line 235
    cmp-long v1, v8, v2

    if-eqz v1, :cond_0

    .line 236
    invoke-virtual {v0, v2, v3}, Lcom/yandex/metrica/impl/ob/bo;->c(J)Lcom/yandex/metrica/impl/ob/bo;

    .line 238
    :cond_0
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/u$a;->a:Lcom/yandex/metrica/impl/ob/fh;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/yandex/metrica/impl/ob/fh;->a(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 239
    if-eqz v1, :cond_1

    .line 240
    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/bo;->a(Z)Lcom/yandex/metrica/impl/ob/bo;

    .line 242
    :cond_1
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/u$a;->a:Lcom/yandex/metrica/impl/ob/fh;

    invoke-virtual {v1, v6, v7}, Lcom/yandex/metrica/impl/ob/fh;->a(J)J

    move-result-wide v2

    .line 243
    cmp-long v1, v2, v6

    if-eqz v1, :cond_2

    .line 244
    invoke-virtual {v0, v2, v3}, Lcom/yandex/metrica/impl/ob/bo;->d(J)Lcom/yandex/metrica/impl/ob/bo;

    .line 246
    :cond_2
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/u$a;->a:Lcom/yandex/metrica/impl/ob/fh;

    invoke-virtual {v1, v4, v5}, Lcom/yandex/metrica/impl/ob/fh;->f(J)J

    move-result-wide v2

    .line 247
    cmp-long v1, v2, v4

    if-eqz v1, :cond_3

    .line 248
    invoke-virtual {v0, v2, v3}, Lcom/yandex/metrica/impl/ob/bo;->a(J)Lcom/yandex/metrica/impl/ob/bo;

    .line 250
    :cond_3
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/u$a;->a:Lcom/yandex/metrica/impl/ob/fh;

    invoke-virtual {v1, v4, v5}, Lcom/yandex/metrica/impl/ob/fh;->h(J)J

    move-result-wide v2

    .line 251
    cmp-long v1, v2, v4

    if-eqz v1, :cond_4

    .line 252
    invoke-virtual {v0, v2, v3}, Lcom/yandex/metrica/impl/ob/bo;->b(J)Lcom/yandex/metrica/impl/ob/bo;

    .line 254
    :cond_4
    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/bo;->g()V

    .line 256
    :cond_5
    return-void
.end method

.method d()V
    .locals 10

    .prologue
    const-wide/16 v8, -0x1

    const-wide/high16 v6, -0x8000000000000000L

    const-wide/16 v4, 0x0

    .line 260
    new-instance v0, Lcom/yandex/metrica/impl/ob/bo;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/u$a;->b:Lcom/yandex/metrica/impl/ob/dd;

    const-string v2, "background"

    invoke-direct {v0, v1, v2}, Lcom/yandex/metrica/impl/ob/bo;-><init>(Lcom/yandex/metrica/impl/ob/dd;Ljava/lang/String;)V

    .line 261
    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/bo;->h()Z

    move-result v1

    if-nez v1, :cond_4

    .line 262
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/u$a;->a:Lcom/yandex/metrica/impl/ob/fh;

    invoke-virtual {v1, v8, v9}, Lcom/yandex/metrica/impl/ob/fh;->e(J)J

    move-result-wide v2

    .line 263
    cmp-long v1, v2, v8

    if-eqz v1, :cond_0

    .line 264
    invoke-virtual {v0, v2, v3}, Lcom/yandex/metrica/impl/ob/bo;->c(J)Lcom/yandex/metrica/impl/ob/bo;

    .line 266
    :cond_0
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/u$a;->a:Lcom/yandex/metrica/impl/ob/fh;

    invoke-virtual {v1, v6, v7}, Lcom/yandex/metrica/impl/ob/fh;->b(J)J

    move-result-wide v2

    .line 267
    cmp-long v1, v2, v6

    if-eqz v1, :cond_1

    .line 268
    invoke-virtual {v0, v2, v3}, Lcom/yandex/metrica/impl/ob/bo;->d(J)Lcom/yandex/metrica/impl/ob/bo;

    .line 270
    :cond_1
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/u$a;->a:Lcom/yandex/metrica/impl/ob/fh;

    invoke-virtual {v1, v4, v5}, Lcom/yandex/metrica/impl/ob/fh;->g(J)J

    move-result-wide v2

    .line 271
    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    .line 272
    invoke-virtual {v0, v2, v3}, Lcom/yandex/metrica/impl/ob/bo;->a(J)Lcom/yandex/metrica/impl/ob/bo;

    .line 274
    :cond_2
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/u$a;->a:Lcom/yandex/metrica/impl/ob/fh;

    invoke-virtual {v1, v4, v5}, Lcom/yandex/metrica/impl/ob/fh;->i(J)J

    move-result-wide v2

    .line 275
    cmp-long v1, v2, v4

    if-eqz v1, :cond_3

    .line 276
    invoke-virtual {v0, v2, v3}, Lcom/yandex/metrica/impl/ob/bo;->b(J)Lcom/yandex/metrica/impl/ob/bo;

    .line 278
    :cond_3
    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/bo;->g()V

    .line 280
    :cond_4
    return-void
.end method
