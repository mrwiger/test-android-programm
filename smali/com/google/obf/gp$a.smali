.class Lcom/google/obf/gp$a;
.super Landroid/os/AsyncTask;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/obf/gp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/obf/gp;

.field private b:Lcom/google/ads/interactivemedia/v3/api/AdsRequest;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/obf/gp;Lcom/google/ads/interactivemedia/v3/api/AdsRequest;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/google/obf/gp$a;->a:Lcom/google/obf/gp;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 2
    iput-object p2, p0, Lcom/google/obf/gp$a;->b:Lcom/google/ads/interactivemedia/v3/api/AdsRequest;

    .line 3
    iput-object p3, p0, Lcom/google/obf/gp$a;->c:Ljava/lang/String;

    .line 4
    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 5
    const/4 v0, 0x0

    aget-object v0, p1, v0

    .line 6
    iget-object v1, p0, Lcom/google/obf/gp$a;->a:Lcom/google/obf/gp;

    invoke-static {v1}, Lcom/google/obf/gp;->f(Lcom/google/obf/gp;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 7
    :try_start_0
    iget-object v2, p0, Lcom/google/obf/gp$a;->a:Lcom/google/obf/gp;

    invoke-static {v2}, Lcom/google/obf/gp;->g(Lcom/google/obf/gp;)Lcom/google/obf/ik;

    move-result-object v2

    if-nez v2, :cond_0

    .line 8
    iget-object v2, p0, Lcom/google/obf/gp$a;->a:Lcom/google/obf/gp;

    new-instance v3, Lcom/google/obf/ik;

    const-string v4, "a.3.7.4"

    iget-object v5, p0, Lcom/google/obf/gp$a;->a:Lcom/google/obf/gp;

    .line 9
    invoke-static {v5}, Lcom/google/obf/gp;->c(Lcom/google/obf/gp;)Landroid/content/Context;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/obf/ij;->a(Ljava/lang/String;Landroid/content/Context;)Lcom/google/obf/ij;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/obf/ik;-><init>(Lcom/google/obf/ig;)V

    .line 10
    invoke-static {v2, v3}, Lcom/google/obf/gp;->a(Lcom/google/obf/gp;Lcom/google/obf/ik;)Lcom/google/obf/ik;

    .line 11
    :cond_0
    if-eqz v0, :cond_1

    .line 12
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 13
    iget-object v3, p0, Lcom/google/obf/gp$a;->a:Lcom/google/obf/gp;

    invoke-static {v3}, Lcom/google/obf/gp;->g(Lcom/google/obf/gp;)Lcom/google/obf/ik;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/obf/ik;->b(Landroid/net/Uri;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-eqz v3, :cond_1

    .line 14
    :try_start_1
    iget-object v3, p0, Lcom/google/obf/gp$a;->a:Lcom/google/obf/gp;

    invoke-static {v3}, Lcom/google/obf/gp;->g(Lcom/google/obf/gp;)Lcom/google/obf/ik;

    move-result-object v3

    iget-object v4, p0, Lcom/google/obf/gp$a;->a:Lcom/google/obf/gp;

    invoke-static {v4}, Lcom/google/obf/gp;->c(Lcom/google/obf/gp;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Lcom/google/obf/ik;->a(Landroid/net/Uri;Landroid/content/Context;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;
    :try_end_1
    .catch Lcom/google/obf/il; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 17
    :cond_1
    :goto_0
    :try_start_2
    monitor-exit v1

    .line 18
    return-object v0

    .line 17
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 16
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method protected a(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/obf/gp$a;->b:Lcom/google/ads/interactivemedia/v3/api/AdsRequest;

    invoke-interface {v0, p1}, Lcom/google/ads/interactivemedia/v3/api/AdsRequest;->setAdTagUrl(Ljava/lang/String;)V

    .line 20
    iget-object v0, p0, Lcom/google/obf/gp$a;->b:Lcom/google/ads/interactivemedia/v3/api/AdsRequest;

    iget-object v1, p0, Lcom/google/obf/gp$a;->a:Lcom/google/obf/gp;

    .line 21
    invoke-static {v1}, Lcom/google/obf/gp;->h(Lcom/google/obf/gp;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/obf/gp$a;->a:Lcom/google/obf/gp;

    invoke-static {v2}, Lcom/google/obf/gp;->i(Lcom/google/obf/gp;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/obf/gp$a;->a:Lcom/google/obf/gp;

    invoke-static {v3}, Lcom/google/obf/gp;->j(Lcom/google/obf/gp;)Lcom/google/ads/interactivemedia/v3/api/ImaSdkSettings;

    move-result-object v3

    iget-object v4, p0, Lcom/google/obf/gp$a;->a:Lcom/google/obf/gp;

    invoke-static {v4}, Lcom/google/obf/gp;->k(Lcom/google/obf/gp;)Lcom/google/obf/gp$b;

    move-result-object v4

    iget-object v5, p0, Lcom/google/obf/gp$a;->a:Lcom/google/obf/gp;

    .line 22
    invoke-static {v5}, Lcom/google/obf/gp;->l(Lcom/google/obf/gp;)Z

    move-result v5

    .line 23
    invoke-static/range {v0 .. v5}, Lcom/google/ads/interactivemedia/v3/impl/data/k;->create(Lcom/google/ads/interactivemedia/v3/api/AdsRequest;Ljava/lang/String;Ljava/lang/String;Lcom/google/ads/interactivemedia/v3/api/ImaSdkSettings;Lcom/google/obf/gp$b;Z)Lcom/google/ads/interactivemedia/v3/impl/data/k;

    move-result-object v0

    .line 24
    new-instance v1, Lcom/google/obf/hi;

    sget-object v2, Lcom/google/obf/hi$b;->adsLoader:Lcom/google/obf/hi$b;

    sget-object v3, Lcom/google/obf/hi$c;->requestAds:Lcom/google/obf/hi$c;

    iget-object v4, p0, Lcom/google/obf/gp$a;->c:Ljava/lang/String;

    invoke-direct {v1, v2, v3, v4, v0}, Lcom/google/obf/hi;-><init>(Lcom/google/obf/hi$b;Lcom/google/obf/hi$c;Ljava/lang/String;Ljava/lang/Object;)V

    .line 25
    iget-object v0, p0, Lcom/google/obf/gp$a;->a:Lcom/google/obf/gp;

    invoke-static {v0}, Lcom/google/obf/gp;->b(Lcom/google/obf/gp;)Lcom/google/obf/hj;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/obf/hj;->b(Lcom/google/obf/hi;)V

    .line 26
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 28
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/google/obf/gp$a;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 27
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/google/obf/gp$a;->a(Ljava/lang/String;)V

    return-void
.end method
