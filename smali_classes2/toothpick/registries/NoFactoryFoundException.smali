.class public Ltoothpick/registries/NoFactoryFoundException;
.super Ljava/lang/RuntimeException;
.source "NoFactoryFoundException.java"


# direct methods
.method public constructor <init>(Ljava/lang/Class;)V
    .locals 1
    .param p1, "clazz"    # Ljava/lang/Class;

    .prologue
    .line 7
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Ltoothpick/registries/NoFactoryFoundException;-><init>(Ljava/lang/Class;Ljava/lang/Throwable;)V

    .line 8
    return-void
.end method

.method public constructor <init>(Ljava/lang/Class;Ljava/lang/Throwable;)V
    .locals 4
    .param p1, "clazz"    # Ljava/lang/Class;
    .param p2, "cause"    # Ljava/lang/Throwable;

    .prologue
    .line 11
    const-string v0, "No factory could be found for class %s. Check that the class has either a @Inject annotated constructor or contains @Inject annotated members. If using Registries, check that they are properly setup with annotation processor arguments."

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 15
    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 11
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 16
    return-void
.end method
