.class final Lru/cn/player/exoplayer/MetadataExtractor;
.super Ljava/lang/Object;
.source "MetadataExtractor.java"


# static fields
.field private static final ATTRIBUTE_CLASS:Ljava/util/regex/Pattern;

.field private static final ATTRIBUTE_DURATION:Ljava/util/regex/Pattern;

.field private static final ATTRIBUTE_END_DATE:Ljava/util/regex/Pattern;

.field private static final ATTRIBUTE_ID:Ljava/util/regex/Pattern;

.field private static final ATTRIBUTE_START_DATE:Ljava/util/regex/Pattern;

.field private static final ATTRIBUTE_X_PREFIX:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-string v0, "ID=\"(.+?)\""

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lru/cn/player/exoplayer/MetadataExtractor;->ATTRIBUTE_ID:Ljava/util/regex/Pattern;

    .line 25
    const-string v0, "START-DATE=\"(.+?)\""

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lru/cn/player/exoplayer/MetadataExtractor;->ATTRIBUTE_START_DATE:Ljava/util/regex/Pattern;

    .line 26
    const-string v0, "END-DATE=\"(.+?)\""

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lru/cn/player/exoplayer/MetadataExtractor;->ATTRIBUTE_END_DATE:Ljava/util/regex/Pattern;

    .line 27
    const-string v0, "DURATION=([\\d\\.]+)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lru/cn/player/exoplayer/MetadataExtractor;->ATTRIBUTE_DURATION:Ljava/util/regex/Pattern;

    .line 28
    const-string v0, "CLASS=\"(.+?)\""

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lru/cn/player/exoplayer/MetadataExtractor;->ATTRIBUTE_CLASS:Ljava/util/regex/Pattern;

    .line 29
    const-string v0, "(X-[a-zA-Z-]+)=((\"(.+?)\")|([\\d\\.]+)|(0[xX][0-9A-F]+))"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lru/cn/player/exoplayer/MetadataExtractor;->ATTRIBUTE_X_PREFIX:Ljava/util/regex/Pattern;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    return-void
.end method

.method private static arrayAttribute(Ljava/util/regex/Pattern;Ljava/lang/String;)Ljava/util/Map;
    .locals 5
    .param p0, "pattern"    # Ljava/util/regex/Pattern;
    .param p1, "line"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/regex/Pattern;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 94
    invoke-virtual {p0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 96
    .local v2, "matcher":Ljava/util/regex/Matcher;
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 97
    .local v0, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    :cond_0
    :goto_0
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->find()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 98
    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    .line 99
    .local v1, "key":Ljava/lang/String;
    const/4 v4, 0x4

    invoke-virtual {v2, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    .line 100
    .local v3, "value":Ljava/lang/String;
    if-nez v3, :cond_1

    .line 101
    const/4 v4, 0x5

    invoke-virtual {v2, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    .line 104
    :cond_1
    if-nez v3, :cond_2

    .line 105
    const/4 v4, 0x6

    invoke-virtual {v2, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    .line 108
    :cond_2
    if-eqz v3, :cond_0

    .line 109
    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 113
    .end local v1    # "key":Ljava/lang/String;
    .end local v3    # "value":Ljava/lang/String;
    :cond_3
    return-object v0
.end method

.method private convert(Ljava/lang/String;)Lru/cn/player/metadata/MetadataItem;
    .locals 18
    .param p1, "tag"    # Ljava/lang/String;

    .prologue
    .line 58
    sget-object v3, Lru/cn/player/exoplayer/MetadataExtractor;->ATTRIBUTE_ID:Ljava/util/regex/Pattern;

    move-object/from16 v0, p1

    invoke-static {v3, v0}, Lru/cn/player/exoplayer/MetadataExtractor;->stringAttribute(Ljava/util/regex/Pattern;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 59
    .local v5, "uniqueId":Ljava/lang/String;
    sget-object v3, Lru/cn/player/exoplayer/MetadataExtractor;->ATTRIBUTE_CLASS:Ljava/util/regex/Pattern;

    move-object/from16 v0, p1

    invoke-static {v3, v0}, Lru/cn/player/exoplayer/MetadataExtractor;->stringAttribute(Ljava/util/regex/Pattern;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 60
    .local v4, "classLabel":Ljava/lang/String;
    sget-object v3, Lru/cn/player/exoplayer/MetadataExtractor;->ATTRIBUTE_START_DATE:Ljava/util/regex/Pattern;

    move-object/from16 v0, p1

    invoke-static {v3, v0}, Lru/cn/player/exoplayer/MetadataExtractor;->stringAttribute(Ljava/util/regex/Pattern;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 61
    .local v12, "startDate":Ljava/lang/String;
    sget-object v3, Lru/cn/player/exoplayer/MetadataExtractor;->ATTRIBUTE_END_DATE:Ljava/util/regex/Pattern;

    move-object/from16 v0, p1

    invoke-static {v3, v0}, Lru/cn/player/exoplayer/MetadataExtractor;->stringAttribute(Ljava/util/regex/Pattern;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 62
    .local v11, "endDate":Ljava/lang/String;
    sget-object v3, Lru/cn/player/exoplayer/MetadataExtractor;->ATTRIBUTE_DURATION:Ljava/util/regex/Pattern;

    move-object/from16 v0, p1

    invoke-static {v3, v0}, Lru/cn/player/exoplayer/MetadataExtractor;->stringAttribute(Ljava/util/regex/Pattern;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 63
    .local v2, "duration":Ljava/lang/String;
    sget-object v3, Lru/cn/player/exoplayer/MetadataExtractor;->ATTRIBUTE_X_PREFIX:Ljava/util/regex/Pattern;

    move-object/from16 v0, p1

    invoke-static {v3, v0}, Lru/cn/player/exoplayer/MetadataExtractor;->arrayAttribute(Ljava/util/regex/Pattern;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v10

    .line 66
    .local v10, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    if-eqz v5, :cond_2

    if-eqz v4, :cond_2

    if-eqz v12, :cond_2

    if-nez v2, :cond_0

    if-eqz v11, :cond_2

    .line 67
    :cond_0
    :try_start_0
    invoke-static {v12}, Lcom/google/android/exoplayer2/util/Util;->parseXsDateTime(Ljava/lang/String;)J

    move-result-wide v6

    .line 70
    .local v6, "startDateMs":J
    if-eqz v11, :cond_1

    .line 71
    invoke-static {v11}, Lcom/google/android/exoplayer2/util/Util;->parseXsDateTime(Ljava/lang/String;)J

    move-result-wide v8

    .line 76
    .local v8, "endTimeMs":J
    :goto_0
    new-instance v3, Lru/cn/player/metadata/MetadataItem;

    invoke-direct/range {v3 .. v10}, Lru/cn/player/metadata/MetadataItem;-><init>(Ljava/lang/String;Ljava/lang/String;JJLjava/util/Map;)V

    .line 83
    .end local v6    # "startDateMs":J
    .end local v8    # "endTimeMs":J
    :goto_1
    return-object v3

    .line 73
    .restart local v6    # "startDateMs":J
    :cond_1
    invoke-static {v2}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v14

    const-wide v16, 0x408f400000000000L    # 1000.0

    mul-double v14, v14, v16

    long-to-double v0, v6

    move-wide/from16 v16, v0

    add-double v14, v14, v16

    double-to-long v8, v14

    .restart local v8    # "endTimeMs":J
    goto :goto_0

    .line 79
    .end local v6    # "startDateMs":J
    .end local v8    # "endTimeMs":J
    :catch_0
    move-exception v3

    .line 83
    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private extract(Lcom/google/android/exoplayer2/source/hls/HlsManifest;)Ljava/util/List;
    .locals 3
    .param p1, "manifest"    # Lcom/google/android/exoplayer2/source/hls/HlsManifest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/exoplayer2/source/hls/HlsManifest;",
            ")",
            "Ljava/util/List",
            "<",
            "Lru/cn/player/metadata/MetadataItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 45
    iget-object v0, p1, Lcom/google/android/exoplayer2/source/hls/HlsManifest;->mediaPlaylist:Lcom/google/android/exoplayer2/source/hls/playlist/HlsMediaPlaylist;

    .line 46
    .local v0, "mediaPlaylist":Lcom/google/android/exoplayer2/source/hls/playlist/HlsMediaPlaylist;
    if-nez v0, :cond_0

    .line 47
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    .line 49
    :goto_0
    return-object v1

    :cond_0
    iget-object v1, v0, Lcom/google/android/exoplayer2/source/hls/playlist/HlsMediaPlaylist;->tags:Ljava/util/List;

    invoke-static {v1}, Lcom/annimon/stream/Stream;->of(Ljava/lang/Iterable;)Lcom/annimon/stream/Stream;

    move-result-object v1

    sget-object v2, Lru/cn/player/exoplayer/MetadataExtractor$$Lambda$0;->$instance:Lcom/annimon/stream/function/Predicate;

    .line 50
    invoke-virtual {v1, v2}, Lcom/annimon/stream/Stream;->filter(Lcom/annimon/stream/function/Predicate;)Lcom/annimon/stream/Stream;

    move-result-object v1

    new-instance v2, Lru/cn/player/exoplayer/MetadataExtractor$$Lambda$1;

    invoke-direct {v2, p0}, Lru/cn/player/exoplayer/MetadataExtractor$$Lambda$1;-><init>(Lru/cn/player/exoplayer/MetadataExtractor;)V

    .line 51
    invoke-virtual {v1, v2}, Lcom/annimon/stream/Stream;->map(Lcom/annimon/stream/function/Function;)Lcom/annimon/stream/Stream;

    move-result-object v1

    sget-object v2, Lru/cn/player/exoplayer/MetadataExtractor$$Lambda$2;->$instance:Lcom/annimon/stream/function/Function;

    .line 52
    invoke-virtual {v1, v2}, Lcom/annimon/stream/Stream;->distinctBy(Lcom/annimon/stream/function/Function;)Lcom/annimon/stream/Stream;

    move-result-object v1

    .line 53
    invoke-virtual {v1}, Lcom/annimon/stream/Stream;->withoutNulls()Lcom/annimon/stream/Stream;

    move-result-object v1

    .line 54
    invoke-virtual {v1}, Lcom/annimon/stream/Stream;->toList()Ljava/util/List;

    move-result-object v1

    goto :goto_0
.end method

.method static final synthetic lambda$extract$0$MetadataExtractor(Ljava/lang/String;)Z
    .locals 1
    .param p0, "tag"    # Ljava/lang/String;

    .prologue
    .line 50
    const-string v0, "#EXT-X-DATERANGE"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static final synthetic lambda$extract$1$MetadataExtractor(Lru/cn/player/metadata/MetadataItem;)Ljava/lang/String;
    .locals 1
    .param p0, "item"    # Lru/cn/player/metadata/MetadataItem;

    .prologue
    .line 52
    iget-object v0, p0, Lru/cn/player/metadata/MetadataItem;->uniqueId:Ljava/lang/String;

    return-object v0
.end method

.method private static stringAttribute(Ljava/util/regex/Pattern;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "pattern"    # Ljava/util/regex/Pattern;
    .param p1, "line"    # Ljava/lang/String;

    .prologue
    .line 88
    invoke-virtual {p0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 89
    .local v0, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method final bridge synthetic bridge$lambda$0$MetadataExtractor(Ljava/lang/String;)Lru/cn/player/metadata/MetadataItem;
    .locals 1

    invoke-direct {p0, p1}, Lru/cn/player/exoplayer/MetadataExtractor;->convert(Ljava/lang/String;)Lru/cn/player/metadata/MetadataItem;

    move-result-object v0

    return-object v0
.end method

.method extract(Ljava/lang/Object;)Ljava/util/List;
    .locals 1
    .param p1, "manifest"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Ljava/util/List",
            "<",
            "Lru/cn/player/metadata/MetadataItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 37
    instance-of v0, p1, Lcom/google/android/exoplayer2/source/hls/HlsManifest;

    if-eqz v0, :cond_0

    .line 38
    check-cast p1, Lcom/google/android/exoplayer2/source/hls/HlsManifest;

    .end local p1    # "manifest":Ljava/lang/Object;
    invoke-direct {p0, p1}, Lru/cn/player/exoplayer/MetadataExtractor;->extract(Lcom/google/android/exoplayer2/source/hls/HlsManifest;)Ljava/util/List;

    move-result-object v0

    .line 41
    :goto_0
    return-object v0

    .restart local p1    # "manifest":Ljava/lang/Object;
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method
