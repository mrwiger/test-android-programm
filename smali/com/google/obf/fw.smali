.class public final Lcom/google/obf/fw;
.super Ljava/lang/Object;
.source "IMASDK"

# interfaces
.implements Lcom/google/obf/ex;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/obf/fw$a;,
        Lcom/google/obf/fw$b;
    }
.end annotation


# instance fields
.field private final a:Lcom/google/obf/ff;

.field private final b:Lcom/google/obf/ef;

.field private final c:Lcom/google/obf/fg;

.field private final d:Lcom/google/obf/fr;


# direct methods
.method public constructor <init>(Lcom/google/obf/ff;Lcom/google/obf/ef;Lcom/google/obf/fg;Lcom/google/obf/fr;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/google/obf/fw;->a:Lcom/google/obf/ff;

    .line 3
    iput-object p2, p0, Lcom/google/obf/fw;->b:Lcom/google/obf/ef;

    .line 4
    iput-object p3, p0, Lcom/google/obf/fw;->c:Lcom/google/obf/fg;

    .line 5
    iput-object p4, p0, Lcom/google/obf/fw;->d:Lcom/google/obf/fr;

    .line 6
    return-void
.end method

.method private a(Lcom/google/obf/eg;Ljava/lang/reflect/Field;Ljava/lang/String;Lcom/google/obf/gd;ZZ)Lcom/google/obf/fw$b;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/obf/eg;",
            "Ljava/lang/reflect/Field;",
            "Ljava/lang/String;",
            "Lcom/google/obf/gd",
            "<*>;ZZ)",
            "Lcom/google/obf/fw$b;"
        }
    .end annotation

    .prologue
    .line 28
    invoke-virtual {p4}, Lcom/google/obf/gd;->a()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/google/obf/fl;->a(Ljava/lang/reflect/Type;)Z

    move-result v10

    .line 29
    const-class v0, Lcom/google/obf/ez;

    invoke-virtual {p2, v0}, Ljava/lang/reflect/Field;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/google/obf/ez;

    .line 30
    const/4 v7, 0x0

    .line 31
    if-eqz v0, :cond_0

    .line 32
    iget-object v1, p0, Lcom/google/obf/fw;->d:Lcom/google/obf/fr;

    iget-object v2, p0, Lcom/google/obf/fw;->a:Lcom/google/obf/ff;

    invoke-virtual {v1, v2, p1, p4, v0}, Lcom/google/obf/fr;->a(Lcom/google/obf/ff;Lcom/google/obf/eg;Lcom/google/obf/gd;Lcom/google/obf/ez;)Lcom/google/obf/ew;

    move-result-object v7

    .line 33
    :cond_0
    if-eqz v7, :cond_2

    const/4 v6, 0x1

    .line 34
    :goto_0
    if-nez v7, :cond_1

    invoke-virtual {p1, p4}, Lcom/google/obf/eg;->a(Lcom/google/obf/gd;)Lcom/google/obf/ew;

    move-result-object v7

    .line 36
    :cond_1
    new-instance v0, Lcom/google/obf/fw$1;

    move-object v1, p0

    move-object v2, p3

    move/from16 v3, p5

    move/from16 v4, p6

    move-object v5, p2

    move-object v8, p1

    move-object v9, p4

    invoke-direct/range {v0 .. v10}, Lcom/google/obf/fw$1;-><init>(Lcom/google/obf/fw;Ljava/lang/String;ZZLjava/lang/reflect/Field;ZLcom/google/obf/ew;Lcom/google/obf/eg;Lcom/google/obf/gd;Z)V

    return-object v0

    .line 33
    :cond_2
    const/4 v6, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/reflect/Field;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Field;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 9
    const-class v0, Lcom/google/obf/fa;

    invoke-virtual {p1, v0}, Ljava/lang/reflect/Field;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/google/obf/fa;

    .line 10
    if-nez v0, :cond_1

    .line 11
    iget-object v0, p0, Lcom/google/obf/fw;->b:Lcom/google/obf/ef;

    invoke-interface {v0, p1}, Lcom/google/obf/ef;->a(Ljava/lang/reflect/Field;)Ljava/lang/String;

    move-result-object v0

    .line 12
    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 22
    :cond_0
    :goto_0
    return-object v0

    .line 13
    :cond_1
    invoke-interface {v0}, Lcom/google/obf/fa;->a()Ljava/lang/String;

    move-result-object v1

    .line 14
    invoke-interface {v0}, Lcom/google/obf/fa;->b()[Ljava/lang/String;

    move-result-object v2

    .line 15
    array-length v0, v2

    if-nez v0, :cond_2

    .line 16
    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 17
    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    array-length v3, v2

    add-int/lit8 v3, v3, 0x1

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 18
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 19
    array-length v3, v2

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 20
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 21
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private a(Lcom/google/obf/eg;Lcom/google/obf/gd;Ljava/lang/Class;)Ljava/util/Map;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/obf/eg;",
            "Lcom/google/obf/gd",
            "<*>;",
            "Ljava/lang/Class",
            "<*>;)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/obf/fw$b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 37
    new-instance v10, Ljava/util/LinkedHashMap;

    invoke-direct {v10}, Ljava/util/LinkedHashMap;-><init>()V

    .line 38
    invoke-virtual/range {p3 .. p3}, Ljava/lang/Class;->isInterface()Z

    move-result v1

    if-eqz v1, :cond_0

    move-object v1, v10

    .line 67
    :goto_0
    return-object v1

    .line 40
    :cond_0
    invoke-virtual/range {p2 .. p2}, Lcom/google/obf/gd;->b()Ljava/lang/reflect/Type;

    move-result-object v12

    .line 41
    :goto_1
    const-class v1, Ljava/lang/Object;

    move-object/from16 v0, p3

    if-eq v0, v1, :cond_6

    .line 42
    invoke-virtual/range {p3 .. p3}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object v13

    .line 43
    array-length v14, v13

    const/4 v1, 0x0

    move v11, v1

    :goto_2
    if-ge v11, v14, :cond_5

    aget-object v3, v13, v11

    .line 44
    const/4 v1, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v1}, Lcom/google/obf/fw;->a(Ljava/lang/reflect/Field;Z)Z

    move-result v6

    .line 45
    const/4 v1, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v1}, Lcom/google/obf/fw;->a(Ljava/lang/reflect/Field;Z)Z

    move-result v7

    .line 46
    if-nez v6, :cond_2

    if-nez v7, :cond_2

    .line 63
    :cond_1
    add-int/lit8 v1, v11, 0x1

    move v11, v1

    goto :goto_2

    .line 48
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {v3, v1}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 49
    invoke-virtual/range {p2 .. p2}, Lcom/google/obf/gd;->b()Ljava/lang/reflect/Type;

    move-result-object v1

    invoke-virtual {v3}, Ljava/lang/reflect/Field;->getGenericType()Ljava/lang/reflect/Type;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-static {v1, v0, v2}, Lcom/google/obf/fe;->a(Ljava/lang/reflect/Type;Ljava/lang/Class;Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v15

    .line 50
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/google/obf/fw;->a(Ljava/lang/reflect/Field;)Ljava/util/List;

    move-result-object v16

    .line 51
    const/4 v8, 0x0

    .line 52
    const/4 v1, 0x0

    move v9, v1

    :goto_3
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->size()I

    move-result v1

    if-ge v9, v1, :cond_4

    .line 53
    move-object/from16 v0, v16

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 54
    if-eqz v9, :cond_3

    const/4 v6, 0x0

    .line 56
    :cond_3
    invoke-static {v15}, Lcom/google/obf/gd;->a(Ljava/lang/reflect/Type;)Lcom/google/obf/gd;

    move-result-object v5

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    .line 57
    invoke-direct/range {v1 .. v7}, Lcom/google/obf/fw;->a(Lcom/google/obf/eg;Ljava/lang/reflect/Field;Ljava/lang/String;Lcom/google/obf/gd;ZZ)Lcom/google/obf/fw$b;

    move-result-object v1

    .line 58
    invoke-interface {v10, v4, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/obf/fw$b;

    .line 59
    if-nez v8, :cond_7

    .line 60
    :goto_4
    add-int/lit8 v2, v9, 0x1

    move v9, v2

    move-object v8, v1

    goto :goto_3

    .line 61
    :cond_4
    if-eqz v8, :cond_1

    .line 62
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " declares multiple JSON fields named "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v8, Lcom/google/obf/fw$b;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 64
    :cond_5
    invoke-virtual/range {p2 .. p2}, Lcom/google/obf/gd;->b()Ljava/lang/reflect/Type;

    move-result-object v1

    invoke-virtual/range {p3 .. p3}, Ljava/lang/Class;->getGenericSuperclass()Ljava/lang/reflect/Type;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-static {v1, v0, v2}, Lcom/google/obf/fe;->a(Ljava/lang/reflect/Type;Ljava/lang/Class;Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v1

    invoke-static {v1}, Lcom/google/obf/gd;->a(Ljava/lang/reflect/Type;)Lcom/google/obf/gd;

    move-result-object p2

    .line 65
    invoke-virtual/range {p2 .. p2}, Lcom/google/obf/gd;->a()Ljava/lang/Class;

    move-result-object p3

    goto/16 :goto_1

    :cond_6
    move-object v1, v10

    .line 67
    goto/16 :goto_0

    :cond_7
    move-object v1, v8

    goto :goto_4
.end method

.method static a(Ljava/lang/reflect/Field;ZLcom/google/obf/fg;)Z
    .locals 1

    .prologue
    .line 8
    invoke-virtual {p0}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p2, v0, p1}, Lcom/google/obf/fg;->a(Ljava/lang/Class;Z)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p2, p0, p1}, Lcom/google/obf/fg;->a(Ljava/lang/reflect/Field;Z)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/google/obf/eg;Lcom/google/obf/gd;)Lcom/google/obf/ew;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/obf/eg;",
            "Lcom/google/obf/gd",
            "<TT;>;)",
            "Lcom/google/obf/ew",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 23
    invoke-virtual {p2}, Lcom/google/obf/gd;->a()Ljava/lang/Class;

    move-result-object v1

    .line 24
    const-class v0, Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 25
    const/4 v0, 0x0

    .line 27
    :goto_0
    return-object v0

    .line 26
    :cond_0
    iget-object v0, p0, Lcom/google/obf/fw;->a:Lcom/google/obf/ff;

    invoke-virtual {v0, p2}, Lcom/google/obf/ff;->a(Lcom/google/obf/gd;)Lcom/google/obf/fk;

    move-result-object v2

    .line 27
    new-instance v0, Lcom/google/obf/fw$a;

    invoke-direct {p0, p1, p2, v1}, Lcom/google/obf/fw;->a(Lcom/google/obf/eg;Lcom/google/obf/gd;Ljava/lang/Class;)Ljava/util/Map;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Lcom/google/obf/fw$a;-><init>(Lcom/google/obf/fk;Ljava/util/Map;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/reflect/Field;Z)Z
    .locals 1

    .prologue
    .line 7
    iget-object v0, p0, Lcom/google/obf/fw;->c:Lcom/google/obf/fg;

    invoke-static {p1, p2, v0}, Lcom/google/obf/fw;->a(Ljava/lang/reflect/Field;ZLcom/google/obf/fg;)Z

    move-result v0

    return v0
.end method
