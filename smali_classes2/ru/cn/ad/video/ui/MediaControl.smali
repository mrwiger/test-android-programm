.class public interface abstract Lru/cn/ad/video/ui/MediaControl;
.super Ljava/lang/Object;
.source "MediaControl.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/ad/video/ui/MediaControl$Listener;
    }
.end annotation


# virtual methods
.method public abstract getView()Landroid/view/View;
.end method

.method public abstract setClickable(Z)V
.end method

.method public abstract setDuration(J)V
.end method

.method public abstract setListener(Lru/cn/ad/video/ui/MediaControl$Listener;)V
.end method

.method public abstract setPosition(J)V
.end method

.method public abstract setSkipPositionMs(J)V
.end method
