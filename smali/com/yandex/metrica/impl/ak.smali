.class public abstract Lcom/yandex/metrica/impl/ak;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private b:Z

.field protected d:Ljava/lang/String;

.field protected e:Ljava/lang/String;

.field protected f:I

.field protected g:[B

.field protected h:I

.field protected i:[B

.field protected j:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field protected k:Z

.field protected l:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    const/4 v0, 0x1

    iput v0, p0, Lcom/yandex/metrica/impl/ak;->f:I

    .line 103
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/yandex/metrica/impl/ak;->k:Z

    .line 104
    const/4 v0, -0x1

    iput v0, p0, Lcom/yandex/metrica/impl/ak;->l:I

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 193
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Boolean;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 247
    if-nez p1, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    .line 248
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "1"

    :goto_1
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const-string v0, "0"

    goto :goto_1
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 165
    iput p1, p0, Lcom/yandex/metrica/impl/ak;->h:I

    .line 166
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 140
    iput-object p1, p0, Lcom/yandex/metrica/impl/ak;->d:Ljava/lang/String;

    .line 141
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 197
    iput-object p1, p0, Lcom/yandex/metrica/impl/ak;->a:Ljava/util/List;

    .line 198
    return-void
.end method

.method a(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 181
    iput-object p1, p0, Lcom/yandex/metrica/impl/ak;->j:Ljava/util/Map;

    .line 182
    return-void
.end method

.method public a([B)V
    .locals 1

    .prologue
    .line 156
    const/4 v0, 0x2

    iput v0, p0, Lcom/yandex/metrica/impl/ak;->f:I

    .line 157
    iput-object p1, p0, Lcom/yandex/metrica/impl/ak;->g:[B

    .line 158
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 189
    iput-object p1, p0, Lcom/yandex/metrica/impl/ak;->e:Ljava/lang/String;

    .line 190
    return-void
.end method

.method public b([B)V
    .locals 0

    .prologue
    .line 173
    iput-object p1, p0, Lcom/yandex/metrica/impl/ak;->i:[B

    .line 174
    return-void
.end method

.method public abstract b()Z
.end method

.method protected b(I)Z
    .locals 1

    .prologue
    .line 213
    const/16 v0, 0x190

    if-eq p1, v0, :cond_0

    const/16 v0, 0x1f4

    if-eq p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract c()Z
.end method

.method protected abstract d()Lcom/yandex/metrica/impl/ob/es;
.end method

.method public e()V
    .locals 0

    .prologue
    .line 126
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ak;->v()V

    .line 129
    return-void
.end method

.method public f()V
    .locals 0

    .prologue
    .line 131
    return-void
.end method

.method public g()V
    .locals 0

    .prologue
    .line 133
    return-void
.end method

.method public h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/yandex/metrica/impl/ak;->d:Ljava/lang/String;

    return-object v0
.end method

.method public i()I
    .locals 1

    .prologue
    .line 144
    iget v0, p0, Lcom/yandex/metrica/impl/ak;->f:I

    return v0
.end method

.method public j()[B
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/yandex/metrica/impl/ak;->g:[B

    return-object v0
.end method

.method public k()I
    .locals 1

    .prologue
    .line 161
    iget v0, p0, Lcom/yandex/metrica/impl/ak;->h:I

    return v0
.end method

.method l()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 177
    iget-object v0, p0, Lcom/yandex/metrica/impl/ak;->j:Ljava/util/Map;

    return-object v0
.end method

.method public m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/yandex/metrica/impl/ak;->e:Ljava/lang/String;

    return-object v0
.end method

.method public n()Z
    .locals 1

    .prologue
    .line 201
    const/4 v0, 0x0

    return v0
.end method

.method protected o()Z
    .locals 2

    .prologue
    .line 205
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ak;->k()I

    move-result v0

    const/16 v1, 0x190

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected p()Z
    .locals 2

    .prologue
    .line 209
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ak;->k()I

    move-result v0

    const/16 v1, 0xc8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public q()J
    .locals 2

    .prologue
    .line 218
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public r()I
    .locals 1

    .prologue
    .line 222
    iget v0, p0, Lcom/yandex/metrica/impl/ak;->l:I

    return v0
.end method

.method protected s()Ljava/lang/String;
    .locals 2

    .prologue
    .line 226
    iget-object v0, p0, Lcom/yandex/metrica/impl/ak;->a:Ljava/util/List;

    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ak;->r()I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public t()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 230
    iget-object v0, p0, Lcom/yandex/metrica/impl/ak;->a:Ljava/util/List;

    return-object v0
.end method

.method public u()Z
    .locals 2

    .prologue
    .line 234
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ak;->w()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ak;->r()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/yandex/metrica/impl/ak;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public v()V
    .locals 1

    .prologue
    .line 243
    iget v0, p0, Lcom/yandex/metrica/impl/ak;->l:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/yandex/metrica/impl/ak;->l:I

    .line 244
    return-void
.end method

.method public w()Z
    .locals 1

    .prologue
    .line 252
    iget-boolean v0, p0, Lcom/yandex/metrica/impl/ak;->b:Z

    return v0
.end method

.method public x()V
    .locals 1

    .prologue
    .line 256
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/yandex/metrica/impl/ak;->b:Z

    .line 257
    return-void
.end method
