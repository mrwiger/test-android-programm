.class Lru/cn/tv/mobile/NewActivity$13;
.super Ljava/lang/Object;
.source "NewActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/tv/mobile/NewActivity;->askTvInterfaceTransition()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/tv/mobile/NewActivity;


# direct methods
.method constructor <init>(Lru/cn/tv/mobile/NewActivity;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/tv/mobile/NewActivity;

    .prologue
    .line 745
    iput-object p1, p0, Lru/cn/tv/mobile/NewActivity$13;->this$0:Lru/cn/tv/mobile/NewActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v3, 0x0

    .line 748
    iget-object v1, p0, Lru/cn/tv/mobile/NewActivity$13;->this$0:Lru/cn/tv/mobile/NewActivity;

    invoke-virtual {v1}, Lru/cn/tv/mobile/NewActivity;->finish()V

    .line 749
    iget-object v1, p0, Lru/cn/tv/mobile/NewActivity$13;->this$0:Lru/cn/tv/mobile/NewActivity;

    invoke-static {v1}, Lru/cn/utils/Utils;->changeToTVMode(Landroid/content/Context;)V

    .line 750
    invoke-static {}, Lru/cn/domain/statistics/AnalyticsManager;->trackInterfaceChange()V

    .line 753
    new-instance v1, Landroid/net/Uri$Builder;

    invoke-direct {v1}, Landroid/net/Uri$Builder;-><init>()V

    const-string v2, "content"

    .line 754
    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "ru.cn.api.tv"

    .line 755
    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "clear_cache"

    .line 756
    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    .line 757
    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 758
    .local v0, "reloadUri":Landroid/net/Uri;
    iget-object v1, p0, Lru/cn/tv/mobile/NewActivity$13;->this$0:Lru/cn/tv/mobile/NewActivity;

    invoke-virtual {v1}, Lru/cn/tv/mobile/NewActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {v1, v0, v3, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 759
    return-void
.end method
