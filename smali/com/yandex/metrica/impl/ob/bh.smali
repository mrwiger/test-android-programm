.class public Lcom/yandex/metrica/impl/ob/bh;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/yandex/metrica/impl/ob/bh$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/yandex/metrica/impl/ob/v;

.field private final b:Lcom/yandex/metrica/impl/ob/bn;

.field private final c:Lcom/yandex/metrica/impl/ob/bi;

.field private d:J

.field private e:J

.field private f:Ljava/util/concurrent/atomic/AtomicLong;

.field private g:Z

.field private volatile h:Lcom/yandex/metrica/impl/ob/bh$a;

.field private i:J


# direct methods
.method constructor <init>(Lcom/yandex/metrica/impl/ob/v;Lcom/yandex/metrica/impl/ob/bn;Lcom/yandex/metrica/impl/ob/bi;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/yandex/metrica/impl/ob/bh;->a:Lcom/yandex/metrica/impl/ob/v;

    .line 44
    iput-object p2, p0, Lcom/yandex/metrica/impl/ob/bh;->b:Lcom/yandex/metrica/impl/ob/bn;

    .line 45
    iput-object p3, p0, Lcom/yandex/metrica/impl/ob/bh;->c:Lcom/yandex/metrica/impl/ob/bi;

    .line 1050
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bh;->c:Lcom/yandex/metrica/impl/ob/bi;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/yandex/metrica/impl/ob/bi;->b(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/yandex/metrica/impl/ob/bh;->e:J

    .line 1051
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bh;->c:Lcom/yandex/metrica/impl/ob/bi;

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v2, v3}, Lcom/yandex/metrica/impl/ob/bi;->a(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/yandex/metrica/impl/ob/bh;->d:J

    .line 1052
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/bh;->c:Lcom/yandex/metrica/impl/ob/bi;

    invoke-virtual {v1, v4, v5}, Lcom/yandex/metrica/impl/ob/bi;->c(J)J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/bh;->f:Ljava/util/concurrent/atomic/AtomicLong;

    .line 1053
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bh;->c:Lcom/yandex/metrica/impl/ob/bi;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/bi;->a(Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/yandex/metrica/impl/ob/bh;->g:Z

    .line 1054
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bh;->c:Lcom/yandex/metrica/impl/ob/bi;

    invoke-virtual {v0, v4, v5}, Lcom/yandex/metrica/impl/ob/bi;->d(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/yandex/metrica/impl/ob/bh;->i:J

    .line 47
    return-void
.end method

.method private k()Lcom/yandex/metrica/impl/ob/bh$a;
    .locals 4

    .prologue
    .line 130
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bh;->h:Lcom/yandex/metrica/impl/ob/bh$a;

    if-nez v0, :cond_1

    .line 131
    monitor-enter p0

    .line 132
    :try_start_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bh;->h:Lcom/yandex/metrica/impl/ob/bh$a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 134
    :try_start_1
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bh;->a:Lcom/yandex/metrica/impl/ob/v;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/v;->j()Lcom/yandex/metrica/impl/ob/co;

    move-result-object v0

    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/bh;->c()J

    move-result-wide v2

    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/bh;->a()Lcom/yandex/metrica/impl/ob/bp;

    move-result-object v1

    invoke-virtual {v0, v2, v3, v1}, Lcom/yandex/metrica/impl/ob/co;->c(JLcom/yandex/metrica/impl/ob/bp;)Landroid/content/ContentValues;

    move-result-object v0

    .line 135
    const-string v1, "report_request_parameters"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 137
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 138
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 139
    new-instance v0, Lcom/yandex/metrica/impl/ob/bh$a;

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/bh$a;-><init>(Lorg/json/JSONObject;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/bh;->h:Lcom/yandex/metrica/impl/ob/bh$a;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 145
    :cond_0
    :goto_0
    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 147
    :cond_1
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bh;->h:Lcom/yandex/metrica/impl/ob/bh$a;

    return-object v0

    .line 145
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method protected a()Lcom/yandex/metrica/impl/ob/bp;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bh;->c:Lcom/yandex/metrica/impl/ob/bi;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/bi;->a()Lcom/yandex/metrica/impl/ob/bp;

    move-result-object v0

    return-object v0
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 123
    iget-boolean v0, p0, Lcom/yandex/metrica/impl/ob/bh;->g:Z

    if-eq v0, p1, :cond_0

    .line 124
    iput-boolean p1, p0, Lcom/yandex/metrica/impl/ob/bh;->g:Z

    .line 125
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bh;->b:Lcom/yandex/metrica/impl/ob/bn;

    iget-boolean v1, p0, Lcom/yandex/metrica/impl/ob/bh;->g:Z

    invoke-interface {v0, v1}, Lcom/yandex/metrica/impl/ob/bn;->a(Z)Lcom/yandex/metrica/impl/ob/bo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/bo;->g()V

    .line 127
    :cond_0
    return-void
.end method

.method protected b()I
    .locals 2

    .prologue
    .line 62
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bh;->c:Lcom/yandex/metrica/impl/ob/bi;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/bh;->a:Lcom/yandex/metrica/impl/ob/v;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/v;->k()Lcom/yandex/metrica/CounterConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/yandex/metrica/CounterConfiguration;->d()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/bi;->a(I)I

    move-result v0

    return v0
.end method

.method public c()J
    .locals 2

    .prologue
    .line 66
    iget-wide v0, p0, Lcom/yandex/metrica/impl/ob/bh;->d:J

    return-wide v0
.end method

.method d()J
    .locals 6

    .prologue
    .line 70
    iget-wide v0, p0, Lcom/yandex/metrica/impl/ob/bh;->i:J

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-wide v4, p0, Lcom/yandex/metrica/impl/ob/bh;->e:J

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v2

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method e()Z
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 74
    iget-wide v2, p0, Lcom/yandex/metrica/impl/ob/bh;->d:J

    cmp-long v2, v2, v8

    if-ltz v2, :cond_2

    .line 1078
    invoke-direct {p0}, Lcom/yandex/metrica/impl/ob/bh;->k()Lcom/yandex/metrica/impl/ob/bh$a;

    move-result-object v2

    .line 1080
    if-eqz v2, :cond_3

    .line 1081
    iget-object v3, p0, Lcom/yandex/metrica/impl/ob/bh;->a:Lcom/yandex/metrica/impl/ob/v;

    invoke-virtual {v3}, Lcom/yandex/metrica/impl/ob/v;->i()Lcom/yandex/metrica/impl/bb;

    move-result-object v3

    .line 1082
    invoke-virtual {v2, v3}, Lcom/yandex/metrica/impl/ob/bh$a;->a(Lcom/yandex/metrica/impl/bb;)Z

    move-result v2

    .line 74
    :goto_0
    if-eqz v2, :cond_2

    .line 1092
    iget-wide v2, p0, Lcom/yandex/metrica/impl/ob/bh;->i:J

    .line 2055
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    .line 1093
    sub-long v2, v4, v2

    .line 1094
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/bh;->f()J

    move-result-wide v4

    .line 1099
    cmp-long v6, v2, v8

    if-ltz v6, :cond_0

    .line 1100
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/bh;->b()I

    move-result v6

    int-to-long v6, v6

    cmp-long v2, v2, v6

    if-gez v2, :cond_0

    sget-wide v2, Lcom/yandex/metrica/impl/ob/bj;->c:J

    cmp-long v2, v4, v2

    if-ltz v2, :cond_1

    :cond_0
    move v2, v0

    .line 74
    :goto_1
    if-nez v2, :cond_2

    :goto_2
    return v0

    :cond_1
    move v2, v1

    .line 1100
    goto :goto_1

    :cond_2
    move v0, v1

    .line 74
    goto :goto_2

    :cond_3
    move v2, v1

    goto :goto_0
.end method

.method f()J
    .locals 6

    .prologue
    .line 88
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/yandex/metrica/impl/ob/bh;->e:J

    sub-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v0

    return-wide v0
.end method

.method declared-synchronized g()V
    .locals 1

    .prologue
    .line 104
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bh;->b:Lcom/yandex/metrica/impl/ob/bn;

    invoke-interface {v0}, Lcom/yandex/metrica/impl/ob/bn;->a()V

    .line 105
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/bh;->h:Lcom/yandex/metrica/impl/ob/bh$a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 106
    monitor-exit p0

    return-void

    .line 104
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method h()V
    .locals 6

    .prologue
    .line 109
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bh;->b:Lcom/yandex/metrica/impl/ob/bn;

    .line 3055
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    .line 109
    iput-wide v2, p0, Lcom/yandex/metrica/impl/ob/bh;->i:J

    invoke-interface {v0, v2, v3}, Lcom/yandex/metrica/impl/ob/bn;->b(J)Lcom/yandex/metrica/impl/ob/bo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/bo;->g()V

    .line 110
    return-void
.end method

.method i()J
    .locals 6

    .prologue
    .line 113
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bh;->f:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->getAndIncrement()J

    move-result-wide v0

    .line 114
    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/bh;->b:Lcom/yandex/metrica/impl/ob/bn;

    iget-object v3, p0, Lcom/yandex/metrica/impl/ob/bh;->f:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v4

    invoke-interface {v2, v4, v5}, Lcom/yandex/metrica/impl/ob/bn;->a(J)Lcom/yandex/metrica/impl/ob/bo;

    move-result-object v2

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/ob/bo;->g()V

    .line 115
    return-wide v0
.end method

.method j()Z
    .locals 4

    .prologue
    .line 119
    iget-boolean v0, p0, Lcom/yandex/metrica/impl/ob/bh;->g:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/bh;->c()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
