.class public Lcom/samsung/multiscreen/ble/adparser/Type32BitUUIDs;
.super Lcom/samsung/multiscreen/ble/adparser/AdElement;
.source "Type32BitUUIDs.java"


# instance fields
.field type:I

.field uuids:[I


# direct methods
.method public constructor <init>(I[BII)V
    .locals 5
    .param p1, "type"    # I
    .param p2, "data"    # [B
    .param p3, "pos"    # I
    .param p4, "len"    # I

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/samsung/multiscreen/ble/adparser/AdElement;-><init>()V

    .line 26
    iput p1, p0, Lcom/samsung/multiscreen/ble/adparser/Type32BitUUIDs;->type:I

    .line 27
    div-int/lit8 v1, p4, 0x4

    .line 28
    .local v1, "items":I
    new-array v4, v1, [I

    iput-object v4, p0, Lcom/samsung/multiscreen/ble/adparser/Type32BitUUIDs;->uuids:[I

    .line 29
    move v2, p3

    .line 30
    .local v2, "ptr":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 31
    aget-byte v4, p2, v2

    and-int/lit16 v3, v4, 0xff

    .line 32
    .local v3, "v":I
    add-int/lit8 v2, v2, 0x1

    .line 33
    aget-byte v4, p2, v2

    and-int/lit16 v4, v4, 0xff

    shl-int/lit8 v4, v4, 0x8

    or-int/2addr v3, v4

    .line 34
    add-int/lit8 v2, v2, 0x1

    .line 35
    aget-byte v4, p2, v2

    and-int/lit16 v4, v4, 0xff

    shl-int/lit8 v4, v4, 0x10

    or-int/2addr v3, v4

    .line 36
    add-int/lit8 v2, v2, 0x1

    .line 37
    aget-byte v4, p2, v2

    and-int/lit16 v4, v4, 0xff

    shl-int/lit8 v4, v4, 0x18

    or-int/2addr v3, v4

    .line 38
    add-int/lit8 v2, v2, 0x1

    .line 39
    iget-object v4, p0, Lcom/samsung/multiscreen/ble/adparser/Type32BitUUIDs;->uuids:[I

    aput v3, v4, v0

    .line 30
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 41
    .end local v3    # "v":I
    :cond_0
    return-void
.end method


# virtual methods
.method public getType()I
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lcom/samsung/multiscreen/ble/adparser/Type32BitUUIDs;->type:I

    return v0
.end method

.method public getUUIDs()[I
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/samsung/multiscreen/ble/adparser/Type32BitUUIDs;->uuids:[I

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 45
    new-instance v1, Ljava/lang/StringBuffer;

    const-string v2, "flags: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 46
    .local v1, "sb":Ljava/lang/StringBuffer;
    iget v2, p0, Lcom/samsung/multiscreen/ble/adparser/Type32BitUUIDs;->type:I

    sparse-switch v2, :sswitch_data_0

    .line 57
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown 32Bit UUIDs type: 0x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/multiscreen/ble/adparser/Type32BitUUIDs;->type:I

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 60
    :goto_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v2, p0, Lcom/samsung/multiscreen/ble/adparser/Type32BitUUIDs;->uuids:[I

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 61
    if-lez v0, :cond_0

    .line 62
    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 63
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "0x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/multiscreen/ble/adparser/Type32BitUUIDs;->uuids:[I

    aget v3, v3, v0

    invoke-static {v3}, Lcom/samsung/multiscreen/ble/adparser/Type32BitUUIDs;->hex32(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 60
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 48
    .end local v0    # "i":I
    :sswitch_0
    const-string v2, "More 32-bit UUIDs: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 51
    :sswitch_1
    const-string v2, "Complete list of 32-bit UUIDs: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 54
    :sswitch_2
    const-string v2, "Service UUIDs: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 65
    .restart local v0    # "i":I
    :cond_1
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v1}, Ljava/lang/String;-><init>(Ljava/lang/StringBuffer;)V

    return-object v2

    .line 46
    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x5 -> :sswitch_1
        0x15 -> :sswitch_2
    .end sparse-switch
.end method
