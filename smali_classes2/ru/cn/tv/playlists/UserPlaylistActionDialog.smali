.class public Lru/cn/tv/playlists/UserPlaylistActionDialog;
.super Landroid/support/v4/app/DialogFragment;
.source "UserPlaylistActionDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/tv/playlists/UserPlaylistActionDialog$ChannelActionsItemClickListener;
    }
.end annotation


# static fields
.field private static KEY_PLAYLIST_ID:Ljava/lang/String;

.field private static KEY_PLAYLIST_LOCATION:Ljava/lang/String;

.field private static KEY_PLAYLIST_TITLE:Ljava/lang/String;


# instance fields
.field private viewModel:Lru/cn/tv/playlists/UserPlaylistActionViewModel;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-string v0, "playlistId"

    sput-object v0, Lru/cn/tv/playlists/UserPlaylistActionDialog;->KEY_PLAYLIST_ID:Ljava/lang/String;

    .line 21
    const-string v0, "playlistTitle"

    sput-object v0, Lru/cn/tv/playlists/UserPlaylistActionDialog;->KEY_PLAYLIST_TITLE:Ljava/lang/String;

    .line 22
    const-string v0, "playlistLocation"

    sput-object v0, Lru/cn/tv/playlists/UserPlaylistActionDialog;->KEY_PLAYLIST_LOCATION:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method

.method static synthetic access$100(Lru/cn/tv/playlists/UserPlaylistActionDialog;)Lru/cn/tv/playlists/UserPlaylistActionViewModel;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/playlists/UserPlaylistActionDialog;

    .prologue
    .line 15
    iget-object v0, p0, Lru/cn/tv/playlists/UserPlaylistActionDialog;->viewModel:Lru/cn/tv/playlists/UserPlaylistActionViewModel;

    return-object v0
.end method

.method public static newInstance(JLjava/lang/String;Ljava/lang/String;)Lru/cn/tv/playlists/UserPlaylistActionDialog;
    .locals 4
    .param p0, "playlistId"    # J
    .param p2, "playlistTitle"    # Ljava/lang/String;
    .param p3, "playlistLocation"    # Ljava/lang/String;

    .prologue
    .line 25
    new-instance v1, Lru/cn/tv/playlists/UserPlaylistActionDialog;

    invoke-direct {v1}, Lru/cn/tv/playlists/UserPlaylistActionDialog;-><init>()V

    .line 26
    .local v1, "fragment":Lru/cn/tv/playlists/UserPlaylistActionDialog;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 27
    .local v0, "args":Landroid/os/Bundle;
    sget-object v2, Lru/cn/tv/playlists/UserPlaylistActionDialog;->KEY_PLAYLIST_ID:Ljava/lang/String;

    invoke-virtual {v0, v2, p0, p1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 28
    sget-object v2, Lru/cn/tv/playlists/UserPlaylistActionDialog;->KEY_PLAYLIST_TITLE:Ljava/lang/String;

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    sget-object v2, Lru/cn/tv/playlists/UserPlaylistActionDialog;->KEY_PLAYLIST_LOCATION:Ljava/lang/String;

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    invoke-virtual {v1, v0}, Lru/cn/tv/playlists/UserPlaylistActionDialog;->setArguments(Landroid/os/Bundle;)V

    .line 31
    return-object v1
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 11
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x0

    .line 39
    invoke-static {}, Lru/cn/tv/App;->scope()Ltoothpick/Scope;

    move-result-object v0

    const-class v1, Lru/cn/tv/playlists/UserPlaylistActionViewModel;

    invoke-static {p0, v0, v1}, Lru/cn/mvvm/ViewModels;->get(Landroid/support/v4/app/Fragment;Ltoothpick/Scope;Ljava/lang/Class;)Landroid/arch/lifecycle/ViewModel;

    move-result-object v0

    check-cast v0, Lru/cn/tv/playlists/UserPlaylistActionViewModel;

    iput-object v0, p0, Lru/cn/tv/playlists/UserPlaylistActionDialog;->viewModel:Lru/cn/tv/playlists/UserPlaylistActionViewModel;

    .line 41
    new-instance v7, Landroid/widget/ArrayAdapter;

    .line 42
    invoke-virtual {p0}, Lru/cn/tv/playlists/UserPlaylistActionDialog;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x1090003

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    const v10, 0x7f0e001f

    .line 44
    invoke-virtual {p0, v10}, Lru/cn/tv/playlists/UserPlaylistActionDialog;->getString(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    const v10, 0x7f0e001e

    invoke-virtual {p0, v10}, Lru/cn/tv/playlists/UserPlaylistActionDialog;->getString(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-direct {v7, v0, v1, v8}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 47
    .local v7, "channelActionsAdapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    invoke-virtual {p0}, Lru/cn/tv/playlists/UserPlaylistActionDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    sget-object v1, Lru/cn/tv/playlists/UserPlaylistActionDialog;->KEY_PLAYLIST_ID:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 48
    .local v2, "playlistId":J
    invoke-virtual {p0}, Lru/cn/tv/playlists/UserPlaylistActionDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    sget-object v1, Lru/cn/tv/playlists/UserPlaylistActionDialog;->KEY_PLAYLIST_TITLE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 49
    .local v4, "playlistTitle":Ljava/lang/String;
    invoke-virtual {p0}, Lru/cn/tv/playlists/UserPlaylistActionDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    sget-object v1, Lru/cn/tv/playlists/UserPlaylistActionDialog;->KEY_PLAYLIST_LOCATION:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 51
    .local v5, "playlistLocation":Ljava/lang/String;
    new-instance v0, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lru/cn/tv/playlists/UserPlaylistActionDialog;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0e017c

    .line 52
    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setTitle(I)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v8

    new-instance v0, Lru/cn/tv/playlists/UserPlaylistActionDialog$ChannelActionsItemClickListener;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lru/cn/tv/playlists/UserPlaylistActionDialog$ChannelActionsItemClickListener;-><init>(Lru/cn/tv/playlists/UserPlaylistActionDialog;JLjava/lang/String;Ljava/lang/String;Lru/cn/tv/playlists/UserPlaylistActionDialog$1;)V

    .line 53
    invoke-virtual {v8, v7, v0}, Landroid/support/v7/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0e0039

    .line 55
    invoke-virtual {v0, v1, v6}, Landroid/support/v7/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 56
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->create()Landroid/support/v7/app/AlertDialog;

    move-result-object v0

    .line 51
    return-object v0
.end method
