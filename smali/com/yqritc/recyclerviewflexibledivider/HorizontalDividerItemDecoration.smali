.class public Lcom/yqritc/recyclerviewflexibledivider/HorizontalDividerItemDecoration;
.super Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration;
.source "HorizontalDividerItemDecoration.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/yqritc/recyclerviewflexibledivider/HorizontalDividerItemDecoration$Builder;,
        Lcom/yqritc/recyclerviewflexibledivider/HorizontalDividerItemDecoration$MarginProvider;
    }
.end annotation


# instance fields
.field private mMarginProvider:Lcom/yqritc/recyclerviewflexibledivider/HorizontalDividerItemDecoration$MarginProvider;


# direct methods
.method protected constructor <init>(Lcom/yqritc/recyclerviewflexibledivider/HorizontalDividerItemDecoration$Builder;)V
    .locals 1
    .param p1, "builder"    # Lcom/yqritc/recyclerviewflexibledivider/HorizontalDividerItemDecoration$Builder;

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration;-><init>(Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;)V

    .line 20
    invoke-static {p1}, Lcom/yqritc/recyclerviewflexibledivider/HorizontalDividerItemDecoration$Builder;->access$000(Lcom/yqritc/recyclerviewflexibledivider/HorizontalDividerItemDecoration$Builder;)Lcom/yqritc/recyclerviewflexibledivider/HorizontalDividerItemDecoration$MarginProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/yqritc/recyclerviewflexibledivider/HorizontalDividerItemDecoration;->mMarginProvider:Lcom/yqritc/recyclerviewflexibledivider/HorizontalDividerItemDecoration$MarginProvider;

    .line 21
    return-void
.end method

.method private getDividerSize(ILandroid/support/v7/widget/RecyclerView;)I
    .locals 3
    .param p1, "position"    # I
    .param p2, "parent"    # Landroid/support/v7/widget/RecyclerView;

    .prologue
    .line 84
    iget-object v1, p0, Lcom/yqritc/recyclerviewflexibledivider/HorizontalDividerItemDecoration;->mPaintProvider:Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$PaintProvider;

    if-eqz v1, :cond_0

    .line 85
    iget-object v1, p0, Lcom/yqritc/recyclerviewflexibledivider/HorizontalDividerItemDecoration;->mPaintProvider:Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$PaintProvider;

    invoke-interface {v1, p1, p2}, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$PaintProvider;->dividerPaint(ILandroid/support/v7/widget/RecyclerView;)Landroid/graphics/Paint;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v1

    float-to-int v1, v1

    .line 90
    :goto_0
    return v1

    .line 86
    :cond_0
    iget-object v1, p0, Lcom/yqritc/recyclerviewflexibledivider/HorizontalDividerItemDecoration;->mSizeProvider:Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$SizeProvider;

    if-eqz v1, :cond_1

    .line 87
    iget-object v1, p0, Lcom/yqritc/recyclerviewflexibledivider/HorizontalDividerItemDecoration;->mSizeProvider:Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$SizeProvider;

    invoke-interface {v1, p1, p2}, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$SizeProvider;->dividerSize(ILandroid/support/v7/widget/RecyclerView;)I

    move-result v1

    goto :goto_0

    .line 88
    :cond_1
    iget-object v1, p0, Lcom/yqritc/recyclerviewflexibledivider/HorizontalDividerItemDecoration;->mDrawableProvider:Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$DrawableProvider;

    if-eqz v1, :cond_2

    .line 89
    iget-object v1, p0, Lcom/yqritc/recyclerviewflexibledivider/HorizontalDividerItemDecoration;->mDrawableProvider:Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$DrawableProvider;

    invoke-interface {v1, p1, p2}, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$DrawableProvider;->drawableProvider(ILandroid/support/v7/widget/RecyclerView;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 90
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    goto :goto_0

    .line 92
    .end local v0    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_2
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "failed to get size"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method protected getDividerBound(ILandroid/support/v7/widget/RecyclerView;Landroid/view/View;)Landroid/graphics/Rect;
    .locals 9
    .param p1, "position"    # I
    .param p2, "parent"    # Landroid/support/v7/widget/RecyclerView;
    .param p3, "child"    # Landroid/view/View;

    .prologue
    const/4 v7, 0x0

    .line 25
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v7, v7, v7, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 26
    .local v0, "bounds":Landroid/graphics/Rect;
    invoke-static {p3}, Landroid/support/v4/view/ViewCompat;->getTranslationX(Landroid/view/View;)F

    move-result v7

    float-to-int v5, v7

    .line 27
    .local v5, "transitionX":I
    invoke-static {p3}, Landroid/support/v4/view/ViewCompat;->getTranslationY(Landroid/view/View;)F

    move-result v7

    float-to-int v6, v7

    .line 28
    .local v6, "transitionY":I
    invoke-virtual {p3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    .line 29
    .local v4, "params":Landroid/support/v7/widget/RecyclerView$LayoutParams;
    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView;->getPaddingLeft()I

    move-result v7

    iget-object v8, p0, Lcom/yqritc/recyclerviewflexibledivider/HorizontalDividerItemDecoration;->mMarginProvider:Lcom/yqritc/recyclerviewflexibledivider/HorizontalDividerItemDecoration$MarginProvider;

    .line 30
    invoke-interface {v8, p1, p2}, Lcom/yqritc/recyclerviewflexibledivider/HorizontalDividerItemDecoration$MarginProvider;->dividerLeftMargin(ILandroid/support/v7/widget/RecyclerView;)I

    move-result v8

    add-int/2addr v7, v8

    add-int/2addr v7, v5

    iput v7, v0, Landroid/graphics/Rect;->left:I

    .line 31
    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v7

    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView;->getPaddingRight()I

    move-result v8

    sub-int/2addr v7, v8

    iget-object v8, p0, Lcom/yqritc/recyclerviewflexibledivider/HorizontalDividerItemDecoration;->mMarginProvider:Lcom/yqritc/recyclerviewflexibledivider/HorizontalDividerItemDecoration$MarginProvider;

    .line 32
    invoke-interface {v8, p1, p2}, Lcom/yqritc/recyclerviewflexibledivider/HorizontalDividerItemDecoration$MarginProvider;->dividerRightMargin(ILandroid/support/v7/widget/RecyclerView;)I

    move-result v8

    sub-int/2addr v7, v8

    add-int/2addr v7, v5

    iput v7, v0, Landroid/graphics/Rect;->right:I

    .line 34
    invoke-direct {p0, p1, p2}, Lcom/yqritc/recyclerviewflexibledivider/HorizontalDividerItemDecoration;->getDividerSize(ILandroid/support/v7/widget/RecyclerView;)I

    move-result v1

    .line 35
    .local v1, "dividerSize":I
    invoke-virtual {p0, p2}, Lcom/yqritc/recyclerviewflexibledivider/HorizontalDividerItemDecoration;->isReverseLayout(Landroid/support/v7/widget/RecyclerView;)Z

    move-result v3

    .line 36
    .local v3, "isReverseLayout":Z
    iget-object v7, p0, Lcom/yqritc/recyclerviewflexibledivider/HorizontalDividerItemDecoration;->mDividerType:Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$DividerType;

    sget-object v8, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$DividerType;->DRAWABLE:Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$DividerType;

    if-ne v7, v8, :cond_2

    .line 38
    if-eqz v3, :cond_1

    .line 39
    invoke-virtual {p3}, Landroid/view/View;->getTop()I

    move-result v7

    iget v8, v4, Landroid/support/v7/widget/RecyclerView$LayoutParams;->topMargin:I

    sub-int/2addr v7, v8

    add-int/2addr v7, v6

    iput v7, v0, Landroid/graphics/Rect;->bottom:I

    .line 40
    iget v7, v0, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v7, v1

    iput v7, v0, Landroid/graphics/Rect;->top:I

    .line 56
    :goto_0
    iget-boolean v7, p0, Lcom/yqritc/recyclerviewflexibledivider/HorizontalDividerItemDecoration;->mPositionInsideItem:Z

    if-eqz v7, :cond_0

    .line 57
    if-eqz v3, :cond_4

    .line 58
    iget v7, v0, Landroid/graphics/Rect;->top:I

    add-int/2addr v7, v1

    iput v7, v0, Landroid/graphics/Rect;->top:I

    .line 59
    iget v7, v0, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v7, v1

    iput v7, v0, Landroid/graphics/Rect;->bottom:I

    .line 66
    :cond_0
    :goto_1
    return-object v0

    .line 42
    :cond_1
    invoke-virtual {p3}, Landroid/view/View;->getBottom()I

    move-result v7

    iget v8, v4, Landroid/support/v7/widget/RecyclerView$LayoutParams;->bottomMargin:I

    add-int/2addr v7, v8

    add-int/2addr v7, v6

    iput v7, v0, Landroid/graphics/Rect;->top:I

    .line 43
    iget v7, v0, Landroid/graphics/Rect;->top:I

    add-int/2addr v7, v1

    iput v7, v0, Landroid/graphics/Rect;->bottom:I

    goto :goto_0

    .line 47
    :cond_2
    div-int/lit8 v2, v1, 0x2

    .line 48
    .local v2, "halfSize":I
    if-eqz v3, :cond_3

    .line 49
    invoke-virtual {p3}, Landroid/view/View;->getTop()I

    move-result v7

    iget v8, v4, Landroid/support/v7/widget/RecyclerView$LayoutParams;->topMargin:I

    sub-int/2addr v7, v8

    sub-int/2addr v7, v2

    add-int/2addr v7, v6

    iput v7, v0, Landroid/graphics/Rect;->top:I

    .line 53
    :goto_2
    iget v7, v0, Landroid/graphics/Rect;->top:I

    iput v7, v0, Landroid/graphics/Rect;->bottom:I

    goto :goto_0

    .line 51
    :cond_3
    invoke-virtual {p3}, Landroid/view/View;->getBottom()I

    move-result v7

    iget v8, v4, Landroid/support/v7/widget/RecyclerView$LayoutParams;->bottomMargin:I

    add-int/2addr v7, v8

    add-int/2addr v7, v2

    add-int/2addr v7, v6

    iput v7, v0, Landroid/graphics/Rect;->top:I

    goto :goto_2

    .line 61
    .end local v2    # "halfSize":I
    :cond_4
    iget v7, v0, Landroid/graphics/Rect;->top:I

    sub-int/2addr v7, v1

    iput v7, v0, Landroid/graphics/Rect;->top:I

    .line 62
    iget v7, v0, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v7, v1

    iput v7, v0, Landroid/graphics/Rect;->bottom:I

    goto :goto_1
.end method

.method protected setItemOffsets(Landroid/graphics/Rect;ILandroid/support/v7/widget/RecyclerView;)V
    .locals 2
    .param p1, "outRect"    # Landroid/graphics/Rect;
    .param p2, "position"    # I
    .param p3, "parent"    # Landroid/support/v7/widget/RecyclerView;

    .prologue
    const/4 v1, 0x0

    .line 71
    iget-boolean v0, p0, Lcom/yqritc/recyclerviewflexibledivider/HorizontalDividerItemDecoration;->mPositionInsideItem:Z

    if-eqz v0, :cond_0

    .line 72
    invoke-virtual {p1, v1, v1, v1, v1}, Landroid/graphics/Rect;->set(IIII)V

    .line 81
    :goto_0
    return-void

    .line 76
    :cond_0
    invoke-virtual {p0, p3}, Lcom/yqritc/recyclerviewflexibledivider/HorizontalDividerItemDecoration;->isReverseLayout(Landroid/support/v7/widget/RecyclerView;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 77
    invoke-direct {p0, p2, p3}, Lcom/yqritc/recyclerviewflexibledivider/HorizontalDividerItemDecoration;->getDividerSize(ILandroid/support/v7/widget/RecyclerView;)I

    move-result v0

    invoke-virtual {p1, v1, v0, v1, v1}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_0

    .line 79
    :cond_1
    invoke-direct {p0, p2, p3}, Lcom/yqritc/recyclerviewflexibledivider/HorizontalDividerItemDecoration;->getDividerSize(ILandroid/support/v7/widget/RecyclerView;)I

    move-result v0

    invoke-virtual {p1, v1, v1, v1, v0}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_0
.end method
