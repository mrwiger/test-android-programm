.class final Lru/cn/ad/natives/adapters/FacebookNativeAdapter$FacebookBinder;
.super Ljava/lang/Object;
.source "FacebookNativeAdapter.java"

# interfaces
.implements Lru/cn/ad/natives/adapters/NativeAdAdapter$ViewBinder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/ad/natives/adapters/FacebookNativeAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "FacebookBinder"
.end annotation


# instance fields
.field private final nativeAd:Lcom/facebook/ads/NativeAd;


# direct methods
.method constructor <init>(Lcom/facebook/ads/NativeAd;)V
    .locals 0
    .param p1, "nativeAd"    # Lcom/facebook/ads/NativeAd;

    .prologue
    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 106
    iput-object p1, p0, Lru/cn/ad/natives/adapters/FacebookNativeAdapter$FacebookBinder;->nativeAd:Lcom/facebook/ads/NativeAd;

    .line 107
    return-void
.end method


# virtual methods
.method public bind(Landroid/widget/Button;I)Z
    .locals 1
    .param p1, "view"    # Landroid/widget/Button;
    .param p2, "componentType"    # I

    .prologue
    .line 167
    packed-switch p2, :pswitch_data_0

    .line 173
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 169
    :pswitch_0
    iget-object v0, p0, Lru/cn/ad/natives/adapters/FacebookNativeAdapter$FacebookBinder;->nativeAd:Lcom/facebook/ads/NativeAd;

    invoke-virtual {v0}, Lcom/facebook/ads/NativeAd;->getAdCallToAction()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 170
    const/4 v0, 0x1

    goto :goto_0

    .line 167
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method public bind(Landroid/widget/ImageView;I)Z
    .locals 5
    .param p1, "view"    # Landroid/widget/ImageView;
    .param p2, "componentType"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 144
    packed-switch p2, :pswitch_data_0

    .line 162
    :cond_0
    :goto_0
    :pswitch_0
    return v2

    .line 146
    :pswitch_1
    iget-object v4, p0, Lru/cn/ad/natives/adapters/FacebookNativeAdapter$FacebookBinder;->nativeAd:Lcom/facebook/ads/NativeAd;

    invoke-virtual {v4}, Lcom/facebook/ads/NativeAd;->getAdIcon()Lcom/facebook/ads/NativeAd$Image;

    move-result-object v0

    .line 147
    .local v0, "adIcon":Lcom/facebook/ads/NativeAd$Image;
    if-eqz v0, :cond_0

    .line 150
    invoke-static {v0, p1}, Lcom/facebook/ads/NativeAd;->downloadAndDisplayImage(Lcom/facebook/ads/NativeAd$Image;Landroid/widget/ImageView;)V

    move v2, v3

    .line 151
    goto :goto_0

    .line 153
    .end local v0    # "adIcon":Lcom/facebook/ads/NativeAd$Image;
    :pswitch_2
    iget-object v4, p0, Lru/cn/ad/natives/adapters/FacebookNativeAdapter$FacebookBinder;->nativeAd:Lcom/facebook/ads/NativeAd;

    invoke-virtual {v4}, Lcom/facebook/ads/NativeAd;->getAdCoverImage()Lcom/facebook/ads/NativeAd$Image;

    move-result-object v1

    .line 154
    .local v1, "adImage":Lcom/facebook/ads/NativeAd$Image;
    if-eqz v1, :cond_0

    .line 158
    invoke-static {v1, p1}, Lcom/facebook/ads/NativeAd;->downloadAndDisplayImage(Lcom/facebook/ads/NativeAd$Image;Landroid/widget/ImageView;)V

    move v2, v3

    .line 159
    goto :goto_0

    .line 144
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public bind(Landroid/widget/TextView;I)Z
    .locals 2
    .param p1, "view"    # Landroid/widget/TextView;
    .param p2, "componentType"    # I

    .prologue
    const/4 v0, 0x1

    .line 121
    packed-switch p2, :pswitch_data_0

    .line 139
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 123
    :pswitch_1
    iget-object v1, p0, Lru/cn/ad/natives/adapters/FacebookNativeAdapter$FacebookBinder;->nativeAd:Lcom/facebook/ads/NativeAd;

    invoke-virtual {v1}, Lcom/facebook/ads/NativeAd;->getAdTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 127
    :pswitch_2
    iget-object v1, p0, Lru/cn/ad/natives/adapters/FacebookNativeAdapter$FacebookBinder;->nativeAd:Lcom/facebook/ads/NativeAd;

    invoke-virtual {v1}, Lcom/facebook/ads/NativeAd;->getAdCallToAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 131
    :pswitch_3
    iget-object v1, p0, Lru/cn/ad/natives/adapters/FacebookNativeAdapter$FacebookBinder;->nativeAd:Lcom/facebook/ads/NativeAd;

    invoke-virtual {v1}, Lcom/facebook/ads/NativeAd;->getAdBody()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 135
    :pswitch_4
    const/16 v1, 0x8

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 121
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public destroy()V
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lru/cn/ad/natives/adapters/FacebookNativeAdapter$FacebookBinder;->nativeAd:Lcom/facebook/ads/NativeAd;

    invoke-virtual {v0}, Lcom/facebook/ads/NativeAd;->destroy()V

    .line 111
    iget-object v0, p0, Lru/cn/ad/natives/adapters/FacebookNativeAdapter$FacebookBinder;->nativeAd:Lcom/facebook/ads/NativeAd;

    invoke-virtual {v0}, Lcom/facebook/ads/NativeAd;->unregisterView()V

    .line 112
    return-void
.end method

.method public getAdType()I
    .locals 1

    .prologue
    .line 116
    const/4 v0, 0x2

    return v0
.end method

.method public registerTemplate(Landroid/view/View;Ljava/util/List;)Landroid/view/View;
    .locals 1
    .param p1, "templateView"    # Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)",
            "Landroid/view/View;"
        }
    .end annotation

    .prologue
    .line 178
    .local p2, "clickableViews":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    iget-object v0, p0, Lru/cn/ad/natives/adapters/FacebookNativeAdapter$FacebookBinder;->nativeAd:Lcom/facebook/ads/NativeAd;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/ads/NativeAd;->registerViewForInteraction(Landroid/view/View;Ljava/util/List;)V

    .line 179
    return-object p1
.end method
