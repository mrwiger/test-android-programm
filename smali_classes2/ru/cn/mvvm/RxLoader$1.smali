.class Lru/cn/mvvm/RxLoader$1;
.super Landroid/database/ContentObserver;
.source "RxLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/mvvm/RxLoader;->observableCursor(Landroid/database/Cursor;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Lio/reactivex/Observable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/mvvm/RxLoader;

.field final synthetic val$arguments:[Ljava/lang/String;

.field final synthetic val$requery:Ljava/util/concurrent/atomic/AtomicReference;

.field final synthetic val$selection:Ljava/lang/String;

.field final synthetic val$state:Lio/reactivex/subjects/PublishSubject;

.field final synthetic val$uri:Landroid/net/Uri;


# direct methods
.method constructor <init>(Lru/cn/mvvm/RxLoader;Landroid/os/Handler;Ljava/util/concurrent/atomic/AtomicReference;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Lio/reactivex/subjects/PublishSubject;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/mvvm/RxLoader;
    .param p2, "x0"    # Landroid/os/Handler;

    .prologue
    .line 97
    iput-object p1, p0, Lru/cn/mvvm/RxLoader$1;->this$0:Lru/cn/mvvm/RxLoader;

    iput-object p3, p0, Lru/cn/mvvm/RxLoader$1;->val$requery:Ljava/util/concurrent/atomic/AtomicReference;

    iput-object p4, p0, Lru/cn/mvvm/RxLoader$1;->val$uri:Landroid/net/Uri;

    iput-object p5, p0, Lru/cn/mvvm/RxLoader$1;->val$selection:Ljava/lang/String;

    iput-object p6, p0, Lru/cn/mvvm/RxLoader$1;->val$arguments:[Ljava/lang/String;

    iput-object p7, p0, Lru/cn/mvvm/RxLoader$1;->val$state:Lio/reactivex/subjects/PublishSubject;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 5
    .param p1, "selfChange"    # Z

    .prologue
    .line 100
    if-nez p1, :cond_1

    .line 101
    iget-object v1, p0, Lru/cn/mvvm/RxLoader$1;->val$requery:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/reactivex/disposables/Disposable;

    .line 102
    .local v0, "disposable":Lio/reactivex/disposables/Disposable;
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lio/reactivex/disposables/Disposable;->isDisposed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 103
    invoke-interface {v0}, Lio/reactivex/disposables/Disposable;->dispose()V

    .line 106
    :cond_0
    iget-object v1, p0, Lru/cn/mvvm/RxLoader$1;->this$0:Lru/cn/mvvm/RxLoader;

    iget-object v2, p0, Lru/cn/mvvm/RxLoader$1;->val$uri:Landroid/net/Uri;

    iget-object v3, p0, Lru/cn/mvvm/RxLoader$1;->val$selection:Ljava/lang/String;

    iget-object v4, p0, Lru/cn/mvvm/RxLoader$1;->val$arguments:[Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4}, Lru/cn/mvvm/RxLoader;->query(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object v1

    .line 107
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    iget-object v2, p0, Lru/cn/mvvm/RxLoader$1;->val$state:Lio/reactivex/subjects/PublishSubject;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v2}, Lru/cn/mvvm/RxLoader$1$$Lambda$0;->get$Lambda(Lio/reactivex/subjects/PublishSubject;)Lio/reactivex/functions/Consumer;

    move-result-object v2

    .line 108
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 110
    iget-object v1, p0, Lru/cn/mvvm/RxLoader$1;->val$requery:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 112
    .end local v0    # "disposable":Lio/reactivex/disposables/Disposable;
    :cond_1
    return-void
.end method
