.class public Lcom/yandex/metrica/impl/ob/cu;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/yandex/metrica/impl/ob/cy;

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/yandex/metrica/impl/ob/cp;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, Lcom/yandex/metrica/impl/ob/da;

    invoke-direct {v0, p1}, Lcom/yandex/metrica/impl/ob/da;-><init>(Lcom/yandex/metrica/impl/ob/cp;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/cu;->a:Lcom/yandex/metrica/impl/ob/cy;

    .line 39
    iput-object p2, p0, Lcom/yandex/metrica/impl/ob/cu;->b:Ljava/lang/String;

    .line 40
    return-void
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/yandex/metrica/impl/ob/ey;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 43
    .line 46
    :try_start_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/cu;->a:Lcom/yandex/metrica/impl/ob/cy;

    invoke-interface {v0}, Lcom/yandex/metrica/impl/ob/cy;->a()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 47
    :try_start_1
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/cu;->b:Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v2

    .line 48
    if-eqz v2, :cond_2

    :try_start_2
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 49
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 51
    :cond_0
    new-instance v4, Lcom/yandex/metrica/impl/ob/ey;

    const-string v3, "name"

    .line 52
    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v3, "granted"

    .line 53
    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    const-wide/16 v10, 0x1

    cmp-long v3, v6, v10

    if-nez v3, :cond_1

    const/4 v3, 0x1

    :goto_0
    invoke-direct {v4, v5, v3}, Lcom/yandex/metrica/impl/ob/ey;-><init>(Ljava/lang/String;Z)V

    .line 51
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 55
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result v3

    if-nez v3, :cond_0

    .line 64
    iget-object v3, p0, Lcom/yandex/metrica/impl/ob/cu;->a:Lcom/yandex/metrica/impl/ob/cy;

    invoke-interface {v3, v0}, Lcom/yandex/metrica/impl/ob/cy;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 65
    invoke-static {v2}, Lcom/yandex/metrica/impl/bl;->a(Landroid/database/Cursor;)V

    move-object v0, v1

    .line 62
    :goto_1
    return-object v0

    .line 53
    :cond_1
    const/4 v3, 0x0

    goto :goto_0

    .line 64
    :cond_2
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/cu;->a:Lcom/yandex/metrica/impl/ob/cy;

    invoke-interface {v1, v0}, Lcom/yandex/metrica/impl/ob/cy;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 65
    invoke-static {v2}, Lcom/yandex/metrica/impl/bl;->a(Landroid/database/Cursor;)V

    move-object v0, v8

    .line 58
    goto :goto_1

    .line 64
    :catch_0
    move-exception v0

    move-object v0, v8

    move-object v1, v8

    :goto_2
    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/cu;->a:Lcom/yandex/metrica/impl/ob/cy;

    invoke-interface {v2, v1}, Lcom/yandex/metrica/impl/ob/cy;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 65
    invoke-static {v0}, Lcom/yandex/metrica/impl/bl;->a(Landroid/database/Cursor;)V

    move-object v0, v8

    .line 62
    goto :goto_1

    .line 64
    :catchall_0
    move-exception v0

    move-object v2, v8

    :goto_3
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/cu;->a:Lcom/yandex/metrica/impl/ob/cy;

    invoke-interface {v1, v8}, Lcom/yandex/metrica/impl/ob/cy;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 65
    invoke-static {v2}, Lcom/yandex/metrica/impl/bl;->a(Landroid/database/Cursor;)V

    .line 66
    throw v0

    .line 64
    :catchall_1
    move-exception v1

    move-object v2, v8

    move-object v8, v0

    move-object v0, v1

    goto :goto_3

    :catchall_2
    move-exception v1

    move-object v8, v0

    move-object v0, v1

    goto :goto_3

    :catch_1
    move-exception v1

    move-object v1, v0

    move-object v0, v8

    goto :goto_2

    :catch_2
    move-exception v1

    move-object v1, v0

    move-object v0, v2

    goto :goto_2
.end method

.method public a(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/yandex/metrica/impl/ob/ey;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 70
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/cu;->a:Lcom/yandex/metrica/impl/ob/cy;

    invoke-interface {v0}, Lcom/yandex/metrica/impl/ob/cy;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 72
    :try_start_0
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 73
    const-string v0, "delete from permissions"

    invoke-virtual {v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 74
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/yandex/metrica/impl/ob/ey;

    .line 75
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 76
    const-string v4, "name"

    .line 78
    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/ey;->b()Ljava/lang/String;

    move-result-object v5

    .line 76
    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    const-string v4, "granted"

    .line 82
    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/ey;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 80
    invoke-virtual {v3, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 84
    const-string v0, "permissions"

    const/4 v4, 0x0

    invoke-virtual {v1, v0, v4, v3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 89
    :catch_0
    move-exception v0

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 90
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/cu;->a:Lcom/yandex/metrica/impl/ob/cy;

    invoke-interface {v0, v1}, Lcom/yandex/metrica/impl/ob/cy;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 91
    :goto_2
    return-void

    .line 82
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 86
    :cond_1
    :try_start_1
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 89
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 90
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/cu;->a:Lcom/yandex/metrica/impl/ob/cy;

    invoke-interface {v0, v1}, Lcom/yandex/metrica/impl/ob/cy;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_2

    .line 89
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 90
    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/cu;->a:Lcom/yandex/metrica/impl/ob/cy;

    invoke-interface {v2, v1}, Lcom/yandex/metrica/impl/ob/cy;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 91
    throw v0
.end method
