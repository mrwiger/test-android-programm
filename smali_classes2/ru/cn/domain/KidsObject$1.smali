.class final Lru/cn/domain/KidsObject$1;
.super Ljava/lang/Object;
.source "KidsObject.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/domain/KidsObject;->initCheckTimeLimit(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 158
    iput-object p1, p0, Lru/cn/domain/KidsObject$1;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 161
    iget-object v0, p0, Lru/cn/domain/KidsObject$1;->val$context:Landroid/content/Context;

    invoke-static {v0}, Lru/cn/domain/KidsObject;->isKidsMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 162
    iget-object v0, p0, Lru/cn/domain/KidsObject$1;->val$context:Landroid/content/Context;

    invoke-static {v0}, Lru/cn/domain/KidsObject;->access$000(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 163
    const-string v0, "Utils"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "no limit "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lru/cn/domain/KidsObject$1;->val$context:Landroid/content/Context;

    invoke-static {v2}, Lru/cn/domain/KidsObject;->access$000(Landroid/content/Context;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 170
    :goto_0
    invoke-static {}, Lru/cn/domain/KidsObject;->access$200()Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, p0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 174
    :goto_1
    return-void

    .line 165
    :cond_0
    invoke-static {}, Lru/cn/domain/KidsObject;->access$100()Lru/cn/domain/KidsObject$KidsModeListener;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 166
    invoke-static {}, Lru/cn/domain/KidsObject;->access$100()Lru/cn/domain/KidsObject$KidsModeListener;

    move-result-object v0

    invoke-interface {v0}, Lru/cn/domain/KidsObject$KidsModeListener;->onTimeLimitEnd()V

    .line 168
    :cond_1
    const-string v0, "Utils"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "limit "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lru/cn/domain/KidsObject$1;->val$context:Landroid/content/Context;

    invoke-static {v2}, Lru/cn/domain/KidsObject;->access$000(Landroid/content/Context;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 172
    :cond_2
    const/4 v0, 0x0

    invoke-static {v0}, Lru/cn/domain/KidsObject;->access$302(Z)Z

    goto :goto_1
.end method
