.class final Lcom/google/obf/bj;
.super Ljava/lang/Object;
.source "IMASDK"


# static fields
.field private static final a:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 59
    const/16 v0, 0x18

    new-array v0, v0, [I

    const/4 v1, 0x0

    const-string v2, "isom"

    .line 60
    invoke-static {v2}, Lcom/google/obf/ea;->c(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "iso2"

    .line 61
    invoke-static {v2}, Lcom/google/obf/ea;->c(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "iso3"

    .line 62
    invoke-static {v2}, Lcom/google/obf/ea;->c(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "iso4"

    .line 63
    invoke-static {v2}, Lcom/google/obf/ea;->c(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "iso5"

    .line 64
    invoke-static {v2}, Lcom/google/obf/ea;->c(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "iso6"

    .line 65
    invoke-static {v2}, Lcom/google/obf/ea;->c(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "avc1"

    .line 66
    invoke-static {v2}, Lcom/google/obf/ea;->c(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "hvc1"

    .line 67
    invoke-static {v2}, Lcom/google/obf/ea;->c(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "hev1"

    .line 68
    invoke-static {v2}, Lcom/google/obf/ea;->c(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "mp41"

    .line 69
    invoke-static {v2}, Lcom/google/obf/ea;->c(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "mp42"

    .line 70
    invoke-static {v2}, Lcom/google/obf/ea;->c(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "3g2a"

    .line 71
    invoke-static {v2}, Lcom/google/obf/ea;->c(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "3g2b"

    .line 72
    invoke-static {v2}, Lcom/google/obf/ea;->c(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "3gr6"

    .line 73
    invoke-static {v2}, Lcom/google/obf/ea;->c(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "3gs6"

    .line 74
    invoke-static {v2}, Lcom/google/obf/ea;->c(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "3ge6"

    .line 75
    invoke-static {v2}, Lcom/google/obf/ea;->c(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "3gg6"

    .line 76
    invoke-static {v2}, Lcom/google/obf/ea;->c(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "M4V "

    .line 77
    invoke-static {v2}, Lcom/google/obf/ea;->c(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "M4A "

    .line 78
    invoke-static {v2}, Lcom/google/obf/ea;->c(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "f4v "

    .line 79
    invoke-static {v2}, Lcom/google/obf/ea;->c(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "kddi"

    .line 80
    invoke-static {v2}, Lcom/google/obf/ea;->c(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "M4VP"

    .line 81
    invoke-static {v2}, Lcom/google/obf/ea;->c(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "qt  "

    .line 82
    invoke-static {v2}, Lcom/google/obf/ea;->c(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "MSNV"

    .line 83
    invoke-static {v2}, Lcom/google/obf/ea;->c(Ljava/lang/String;)I

    move-result v2

    aput v2, v0, v1

    sput-object v0, Lcom/google/obf/bj;->a:[I

    .line 84
    return-void
.end method

.method private static a(I)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 52
    ushr-int/lit8 v2, p0, 0x8

    const-string v3, "3gp"

    invoke-static {v3}, Lcom/google/obf/ea;->c(Ljava/lang/String;)I

    move-result v3

    if-ne v2, v3, :cond_1

    .line 58
    :cond_0
    :goto_0
    return v0

    .line 54
    :cond_1
    sget-object v3, Lcom/google/obf/bj;->a:[I

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_2

    aget v5, v3, v2

    .line 55
    if-eq v5, p0, :cond_0

    .line 57
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    move v0, v1

    .line 58
    goto :goto_0
.end method

.method public static a(Lcom/google/obf/ak;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 1
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/google/obf/bj;->a(Lcom/google/obf/ak;Z)Z

    move-result v0

    return v0
.end method

.method private static a(Lcom/google/obf/ak;Z)Z
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 3
    invoke-interface {p0}, Lcom/google/obf/ak;->d()J

    move-result-wide v0

    .line 4
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    const-wide/16 v2, 0x1000

    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    .line 5
    :cond_0
    const-wide/16 v0, 0x1000

    :cond_1
    long-to-int v6, v0

    .line 6
    new-instance v7, Lcom/google/obf/dw;

    const/16 v0, 0x40

    invoke-direct {v7, v0}, Lcom/google/obf/dw;-><init>(I)V

    .line 7
    const/4 v2, 0x0

    .line 8
    const/4 v0, 0x0

    .line 9
    const/4 v1, 0x0

    move v5, v2

    .line 10
    :cond_2
    :goto_0
    if-ge v5, v6, :cond_6

    .line 11
    const/16 v4, 0x8

    .line 12
    iget-object v2, v7, Lcom/google/obf/dw;->a:[B

    const/4 v3, 0x0

    invoke-interface {p0, v2, v3, v4}, Lcom/google/obf/ak;->c([BII)V

    .line 13
    const/4 v2, 0x0

    invoke-virtual {v7, v2}, Lcom/google/obf/dw;->c(I)V

    .line 14
    invoke-virtual {v7}, Lcom/google/obf/dw;->k()J

    move-result-wide v2

    .line 15
    invoke-virtual {v7}, Lcom/google/obf/dw;->m()I

    move-result v8

    .line 16
    const-wide/16 v10, 0x1

    cmp-long v9, v2, v10

    if-nez v9, :cond_3

    .line 17
    const/16 v4, 0x10

    .line 18
    iget-object v2, v7, Lcom/google/obf/dw;->a:[B

    const/16 v3, 0x8

    const/16 v9, 0x8

    invoke-interface {p0, v2, v3, v9}, Lcom/google/obf/ak;->c([BII)V

    .line 19
    invoke-virtual {v7}, Lcom/google/obf/dw;->u()J

    move-result-wide v2

    .line 20
    :cond_3
    int-to-long v10, v4

    cmp-long v9, v2, v10

    if-gez v9, :cond_4

    .line 21
    const/4 v0, 0x0

    .line 51
    :goto_1
    return v0

    .line 22
    :cond_4
    add-int/2addr v5, v4

    .line 23
    sget v9, Lcom/google/obf/bc;->B:I

    if-eq v8, v9, :cond_2

    .line 25
    sget v9, Lcom/google/obf/bc;->K:I

    if-eq v8, v9, :cond_5

    sget v9, Lcom/google/obf/bc;->M:I

    if-ne v8, v9, :cond_7

    .line 26
    :cond_5
    const/4 v1, 0x1

    .line 51
    :cond_6
    if-eqz v0, :cond_f

    if-ne p1, v1, :cond_f

    const/4 v0, 0x1

    goto :goto_1

    .line 28
    :cond_7
    int-to-long v10, v5

    add-long/2addr v10, v2

    int-to-long v12, v4

    sub-long/2addr v10, v12

    int-to-long v12, v6

    cmp-long v9, v10, v12

    if-gez v9, :cond_6

    .line 30
    int-to-long v10, v4

    sub-long/2addr v2, v10

    long-to-int v2, v2

    .line 31
    add-int v3, v5, v2

    .line 32
    sget v4, Lcom/google/obf/bc;->a:I

    if-ne v8, v4, :cond_d

    .line 33
    const/16 v4, 0x8

    if-ge v2, v4, :cond_8

    .line 34
    const/4 v0, 0x0

    goto :goto_1

    .line 35
    :cond_8
    invoke-virtual {v7}, Lcom/google/obf/dw;->e()I

    move-result v4

    if-ge v4, v2, :cond_9

    .line 36
    new-array v4, v2, [B

    invoke-virtual {v7, v4, v2}, Lcom/google/obf/dw;->a([BI)V

    .line 37
    :cond_9
    iget-object v4, v7, Lcom/google/obf/dw;->a:[B

    const/4 v5, 0x0

    invoke-interface {p0, v4, v5, v2}, Lcom/google/obf/ak;->c([BII)V

    .line 38
    div-int/lit8 v4, v2, 0x4

    .line 39
    const/4 v2, 0x0

    :goto_2
    if-ge v2, v4, :cond_c

    .line 40
    const/4 v5, 0x1

    if-ne v2, v5, :cond_b

    .line 41
    const/4 v5, 0x4

    invoke-virtual {v7, v5}, Lcom/google/obf/dw;->d(I)V

    .line 45
    :cond_a
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 42
    :cond_b
    invoke-virtual {v7}, Lcom/google/obf/dw;->m()I

    move-result v5

    invoke-static {v5}, Lcom/google/obf/bj;->a(I)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 43
    const/4 v0, 0x1

    .line 46
    :cond_c
    if-nez v0, :cond_e

    .line 47
    const/4 v0, 0x0

    goto :goto_1

    .line 48
    :cond_d
    if-eqz v2, :cond_e

    .line 49
    invoke-interface {p0, v2}, Lcom/google/obf/ak;->c(I)V

    :cond_e
    move v5, v3

    .line 50
    goto/16 :goto_0

    .line 51
    :cond_f
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static b(Lcom/google/obf/ak;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 2
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/google/obf/bj;->a(Lcom/google/obf/ak;Z)Z

    move-result v0

    return v0
.end method
