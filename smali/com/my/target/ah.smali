.class public abstract Lcom/my/target/ah;
.super Ljava/lang/Object;
.source "AdBanner.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/ah$a;
    }
.end annotation


# static fields
.field public static final DEFAULT_CTA_TEXT_STORE:Ljava/lang/String; = "Install"

.field public static final DEFAULT_CTA_TEXT_WEB:Ljava/lang/String; = "Visit"

.field public static final DEFAULT_OPEN_IN_BROWSER:Z = false

.field public static final DEFAULT_TIMEOUT:I = 0x3c

.field public static final DEFAULT_USE_DIRECT_LINK:Z

.field public static final DEFAULT_USE_PLAY_STORE_ACTION:Z


# instance fields
.field protected advertisingLabel:Ljava/lang/String;

.field protected ageRestrictions:Ljava/lang/String;

.field protected bundleId:Ljava/lang/String;

.field protected category:Ljava/lang/String;

.field protected clickArea:Lcom/my/target/af;

.field protected ctaText:Ljava/lang/String;

.field protected deeplink:Ljava/lang/String;

.field protected description:Ljava/lang/String;

.field protected directLink:Z

.field protected disclaimer:Ljava/lang/String;

.field protected domain:Ljava/lang/String;

.field protected duration:F

.field protected height:I

.field protected icon:Lcom/my/target/common/models/ImageData;

.field protected id:Ljava/lang/String;

.field protected image:Lcom/my/target/common/models/ImageData;

.field protected navigationType:Ljava/lang/String;

.field protected openInBrowser:Z

.field protected rating:F

.field private final statHolder:Lcom/my/target/ar;

.field protected subCategory:Ljava/lang/String;

.field protected timeout:I

.field protected title:Ljava/lang/String;

.field protected trackingLink:Ljava/lang/String;

.field protected type:Ljava/lang/String;

.field protected urlscheme:Ljava/lang/String;

.field protected usePlayStoreAction:Z

.field protected votes:I

.field protected width:I


# direct methods
.method protected constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    invoke-static {}, Lcom/my/target/ar;->ab()Lcom/my/target/ar;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/ah;->statHolder:Lcom/my/target/ar;

    .line 29
    const-string v0, ""

    iput-object v0, p0, Lcom/my/target/ah;->description:Ljava/lang/String;

    .line 31
    const-string v0, ""

    iput-object v0, p0, Lcom/my/target/ah;->title:Ljava/lang/String;

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/my/target/ah;->disclaimer:Ljava/lang/String;

    .line 33
    const-string v0, ""

    iput-object v0, p0, Lcom/my/target/ah;->ageRestrictions:Ljava/lang/String;

    .line 36
    const-string v0, ""

    iput-object v0, p0, Lcom/my/target/ah;->category:Ljava/lang/String;

    .line 37
    const-string v0, ""

    iput-object v0, p0, Lcom/my/target/ah;->subCategory:Ljava/lang/String;

    .line 38
    const-string v0, ""

    iput-object v0, p0, Lcom/my/target/ah;->domain:Ljava/lang/String;

    .line 39
    const-string v0, "web"

    iput-object v0, p0, Lcom/my/target/ah;->navigationType:Ljava/lang/String;

    .line 40
    const-string v0, ""

    iput-object v0, p0, Lcom/my/target/ah;->advertisingLabel:Ljava/lang/String;

    .line 44
    sget-object v0, Lcom/my/target/af;->cd:Lcom/my/target/af;

    iput-object v0, p0, Lcom/my/target/ah;->clickArea:Lcom/my/target/af;

    .line 46
    iput-boolean v1, p0, Lcom/my/target/ah;->openInBrowser:Z

    .line 47
    iput-boolean v1, p0, Lcom/my/target/ah;->directLink:Z

    .line 48
    const/16 v0, 0x3c

    iput v0, p0, Lcom/my/target/ah;->timeout:I

    .line 52
    const-string v0, ""

    iput-object v0, p0, Lcom/my/target/ah;->type:Ljava/lang/String;

    .line 53
    const-string v0, ""

    iput-object v0, p0, Lcom/my/target/ah;->id:Ljava/lang/String;

    .line 61
    return-void
.end method


# virtual methods
.method public getAdvertisingLabel()Ljava/lang/String;
    .locals 1

    .prologue
    .line 337
    iget-object v0, p0, Lcom/my/target/ah;->advertisingLabel:Ljava/lang/String;

    return-object v0
.end method

.method public getAgeRestrictions()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/my/target/ah;->ageRestrictions:Ljava/lang/String;

    return-object v0
.end method

.method public getBundleId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/my/target/ah;->bundleId:Ljava/lang/String;

    return-object v0
.end method

.method public getCategory()Ljava/lang/String;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/my/target/ah;->category:Ljava/lang/String;

    return-object v0
.end method

.method public getClickArea()Lcom/my/target/af;
    .locals 1

    .prologue
    .line 357
    iget-object v0, p0, Lcom/my/target/ah;->clickArea:Lcom/my/target/af;

    return-object v0
.end method

.method public getCtaText()Ljava/lang/String;
    .locals 2

    .prologue
    .line 110
    iget-object v0, p0, Lcom/my/target/ah;->ctaText:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 112
    const-string v0, "store"

    iget-object v1, p0, Lcom/my/target/ah;->navigationType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 114
    const-string v0, "Install"

    .line 121
    :goto_0
    return-object v0

    .line 118
    :cond_0
    const-string v0, "Visit"

    goto :goto_0

    .line 121
    :cond_1
    iget-object v0, p0, Lcom/my/target/ah;->ctaText:Ljava/lang/String;

    goto :goto_0
.end method

.method public getDeeplink()Ljava/lang/String;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/my/target/ah;->deeplink:Ljava/lang/String;

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/my/target/ah;->description:Ljava/lang/String;

    return-object v0
.end method

.method public getDisclaimer()Ljava/lang/String;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/my/target/ah;->disclaimer:Ljava/lang/String;

    return-object v0
.end method

.method public getDomain()Ljava/lang/String;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/my/target/ah;->domain:Ljava/lang/String;

    return-object v0
.end method

.method public getDuration()F
    .locals 1

    .prologue
    .line 342
    iget v0, p0, Lcom/my/target/ah;->duration:F

    return v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 171
    iget v0, p0, Lcom/my/target/ah;->height:I

    return v0
.end method

.method public getIcon()Lcom/my/target/common/models/ImageData;
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/my/target/ah;->icon:Lcom/my/target/common/models/ImageData;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/my/target/ah;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getImage()Lcom/my/target/common/models/ImageData;
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lcom/my/target/ah;->image:Lcom/my/target/common/models/ImageData;

    return-object v0
.end method

.method public getNavigationType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/my/target/ah;->navigationType:Ljava/lang/String;

    return-object v0
.end method

.method public getRating()F
    .locals 1

    .prologue
    .line 212
    iget v0, p0, Lcom/my/target/ah;->rating:F

    return v0
.end method

.method public getStatHolder()Lcom/my/target/ar;
    .locals 1

    .prologue
    .line 327
    iget-object v0, p0, Lcom/my/target/ah;->statHolder:Lcom/my/target/ar;

    return-object v0
.end method

.method public getSubCategory()Ljava/lang/String;
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lcom/my/target/ah;->subCategory:Ljava/lang/String;

    return-object v0
.end method

.method public getTimeout()I
    .locals 1

    .prologue
    .line 232
    iget v0, p0, Lcom/my/target/ah;->timeout:I

    return v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 242
    iget-object v0, p0, Lcom/my/target/ah;->title:Ljava/lang/String;

    return-object v0
.end method

.method public getTrackingLink()Ljava/lang/String;
    .locals 1

    .prologue
    .line 252
    iget-object v0, p0, Lcom/my/target/ah;->trackingLink:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 262
    iget-object v0, p0, Lcom/my/target/ah;->type:Ljava/lang/String;

    return-object v0
.end method

.method public getUrlscheme()Ljava/lang/String;
    .locals 1

    .prologue
    .line 267
    iget-object v0, p0, Lcom/my/target/ah;->urlscheme:Ljava/lang/String;

    return-object v0
.end method

.method public getVotes()I
    .locals 1

    .prologue
    .line 277
    iget v0, p0, Lcom/my/target/ah;->votes:I

    return v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 287
    iget v0, p0, Lcom/my/target/ah;->width:I

    return v0
.end method

.method public isDirectLink()Z
    .locals 1

    .prologue
    .line 297
    iget-boolean v0, p0, Lcom/my/target/ah;->directLink:Z

    return v0
.end method

.method public isOpenInBrowser()Z
    .locals 1

    .prologue
    .line 307
    iget-boolean v0, p0, Lcom/my/target/ah;->openInBrowser:Z

    return v0
.end method

.method public isUsePlayStoreAction()Z
    .locals 1

    .prologue
    .line 317
    iget-boolean v0, p0, Lcom/my/target/ah;->usePlayStoreAction:Z

    return v0
.end method

.method public setAdvertisingLabel(Ljava/lang/String;)V
    .locals 0
    .param p1, "advertisingLabel"    # Ljava/lang/String;

    .prologue
    .line 332
    iput-object p1, p0, Lcom/my/target/ah;->advertisingLabel:Ljava/lang/String;

    .line 333
    return-void
.end method

.method public setAgeRestrictions(Ljava/lang/String;)V
    .locals 0
    .param p1, "ageRestrictions"    # Ljava/lang/String;

    .prologue
    .line 85
    iput-object p1, p0, Lcom/my/target/ah;->ageRestrictions:Ljava/lang/String;

    .line 86
    return-void
.end method

.method public setBundleId(Ljava/lang/String;)V
    .locals 0
    .param p1, "bundleId"    # Ljava/lang/String;

    .prologue
    .line 95
    iput-object p1, p0, Lcom/my/target/ah;->bundleId:Ljava/lang/String;

    .line 96
    return-void
.end method

.method public setCategory(Ljava/lang/String;)V
    .locals 0
    .param p1, "category"    # Ljava/lang/String;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/my/target/ah;->category:Ljava/lang/String;

    .line 106
    return-void
.end method

.method public setClickArea(Lcom/my/target/af;)V
    .locals 0
    .param p1, "clickArea"    # Lcom/my/target/af;

    .prologue
    .line 352
    iput-object p1, p0, Lcom/my/target/ah;->clickArea:Lcom/my/target/af;

    .line 353
    return-void
.end method

.method public setCtaText(Ljava/lang/String;)V
    .locals 0
    .param p1, "ctaText"    # Ljava/lang/String;

    .prologue
    .line 126
    iput-object p1, p0, Lcom/my/target/ah;->ctaText:Ljava/lang/String;

    .line 127
    return-void
.end method

.method public setDeeplink(Ljava/lang/String;)V
    .locals 0
    .param p1, "deeplink"    # Ljava/lang/String;

    .prologue
    .line 136
    iput-object p1, p0, Lcom/my/target/ah;->deeplink:Ljava/lang/String;

    .line 137
    return-void
.end method

.method public setDescription(Ljava/lang/String;)V
    .locals 0
    .param p1, "description"    # Ljava/lang/String;

    .prologue
    .line 146
    iput-object p1, p0, Lcom/my/target/ah;->description:Ljava/lang/String;

    .line 147
    return-void
.end method

.method public setDirectLink(Z)V
    .locals 0
    .param p1, "directLink"    # Z

    .prologue
    .line 302
    iput-boolean p1, p0, Lcom/my/target/ah;->directLink:Z

    .line 303
    return-void
.end method

.method public setDisclaimer(Ljava/lang/String;)V
    .locals 0
    .param p1, "disclaimer"    # Ljava/lang/String;

    .prologue
    .line 156
    iput-object p1, p0, Lcom/my/target/ah;->disclaimer:Ljava/lang/String;

    .line 157
    return-void
.end method

.method public setDomain(Ljava/lang/String;)V
    .locals 0
    .param p1, "domain"    # Ljava/lang/String;

    .prologue
    .line 166
    iput-object p1, p0, Lcom/my/target/ah;->domain:Ljava/lang/String;

    .line 167
    return-void
.end method

.method public setDuration(F)V
    .locals 0
    .param p1, "duration"    # F

    .prologue
    .line 347
    iput p1, p0, Lcom/my/target/ah;->duration:F

    .line 348
    return-void
.end method

.method public setHeight(I)V
    .locals 0
    .param p1, "height"    # I

    .prologue
    .line 176
    iput p1, p0, Lcom/my/target/ah;->height:I

    .line 177
    return-void
.end method

.method public setIcon(Lcom/my/target/common/models/ImageData;)V
    .locals 0
    .param p1, "icon"    # Lcom/my/target/common/models/ImageData;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/my/target/ah;->icon:Lcom/my/target/common/models/ImageData;

    .line 66
    return-void
.end method

.method public setId(Ljava/lang/String;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 191
    iput-object p1, p0, Lcom/my/target/ah;->id:Ljava/lang/String;

    .line 192
    return-void
.end method

.method public setImage(Lcom/my/target/common/models/ImageData;)V
    .locals 0
    .param p1, "image"    # Lcom/my/target/common/models/ImageData;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/my/target/ah;->image:Lcom/my/target/common/models/ImageData;

    .line 71
    return-void
.end method

.method public setNavigationType(Ljava/lang/String;)V
    .locals 0
    .param p1, "navigationType"    # Ljava/lang/String;

    .prologue
    .line 206
    iput-object p1, p0, Lcom/my/target/ah;->navigationType:Ljava/lang/String;

    .line 208
    return-void
.end method

.method public setOpenInBrowser(Z)V
    .locals 0
    .param p1, "openInBrowser"    # Z

    .prologue
    .line 312
    iput-boolean p1, p0, Lcom/my/target/ah;->openInBrowser:Z

    .line 313
    return-void
.end method

.method public setRating(F)V
    .locals 0
    .param p1, "rating"    # F

    .prologue
    .line 217
    iput p1, p0, Lcom/my/target/ah;->rating:F

    .line 218
    return-void
.end method

.method public setSubCategory(Ljava/lang/String;)V
    .locals 0
    .param p1, "subCategory"    # Ljava/lang/String;

    .prologue
    .line 227
    iput-object p1, p0, Lcom/my/target/ah;->subCategory:Ljava/lang/String;

    .line 228
    return-void
.end method

.method public setTimeout(I)V
    .locals 0
    .param p1, "timeout"    # I

    .prologue
    .line 237
    iput p1, p0, Lcom/my/target/ah;->timeout:I

    .line 238
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 247
    iput-object p1, p0, Lcom/my/target/ah;->title:Ljava/lang/String;

    .line 248
    return-void
.end method

.method public setTrackingLink(Ljava/lang/String;)V
    .locals 0
    .param p1, "trackingLink"    # Ljava/lang/String;

    .prologue
    .line 257
    iput-object p1, p0, Lcom/my/target/ah;->trackingLink:Ljava/lang/String;

    .line 258
    return-void
.end method

.method public setType(Ljava/lang/String;)V
    .locals 0
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 75
    iput-object p1, p0, Lcom/my/target/ah;->type:Ljava/lang/String;

    .line 76
    return-void
.end method

.method public setUrlscheme(Ljava/lang/String;)V
    .locals 0
    .param p1, "urlscheme"    # Ljava/lang/String;

    .prologue
    .line 272
    iput-object p1, p0, Lcom/my/target/ah;->urlscheme:Ljava/lang/String;

    .line 273
    return-void
.end method

.method public setUsePlayStoreAction(Z)V
    .locals 0
    .param p1, "usePlayStoreAction"    # Z

    .prologue
    .line 322
    iput-boolean p1, p0, Lcom/my/target/ah;->usePlayStoreAction:Z

    .line 323
    return-void
.end method

.method public setVotes(I)V
    .locals 0
    .param p1, "votes"    # I

    .prologue
    .line 282
    iput p1, p0, Lcom/my/target/ah;->votes:I

    .line 283
    return-void
.end method

.method public setWidth(I)V
    .locals 0
    .param p1, "width"    # I

    .prologue
    .line 292
    iput p1, p0, Lcom/my/target/ah;->width:I

    .line 293
    return-void
.end method
