.class public final Lcom/google/obf/ep;
.super Lcom/google/obf/em;
.source "IMASDK"


# instance fields
.field private final a:Lcom/google/obf/fj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/obf/fj",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/obf/em;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Lcom/google/obf/em;-><init>()V

    .line 2
    new-instance v0, Lcom/google/obf/fj;

    invoke-direct {v0}, Lcom/google/obf/fj;-><init>()V

    iput-object v0, p0, Lcom/google/obf/ep;->a:Lcom/google/obf/fj;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Lcom/google/obf/em;)V
    .locals 1

    .prologue
    .line 3
    if-nez p2, :cond_0

    .line 4
    sget-object p2, Lcom/google/obf/eo;->a:Lcom/google/obf/eo;

    .line 5
    :cond_0
    iget-object v0, p0, Lcom/google/obf/ep;->a:Lcom/google/obf/fj;

    invoke-virtual {v0, p1, p2}, Lcom/google/obf/fj;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 6
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 8
    if-eq p1, p0, :cond_0

    instance-of v0, p1, Lcom/google/obf/ep;

    if-eqz v0, :cond_1

    check-cast p1, Lcom/google/obf/ep;

    iget-object v0, p1, Lcom/google/obf/ep;->a:Lcom/google/obf/fj;

    iget-object v1, p0, Lcom/google/obf/ep;->a:Lcom/google/obf/fj;

    .line 9
    invoke-virtual {v0, v1}, Lcom/google/obf/fj;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 10
    iget-object v0, p0, Lcom/google/obf/ep;->a:Lcom/google/obf/fj;

    invoke-virtual {v0}, Lcom/google/obf/fj;->hashCode()I

    move-result v0

    return v0
.end method

.method public o()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/obf/em;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 7
    iget-object v0, p0, Lcom/google/obf/ep;->a:Lcom/google/obf/fj;

    invoke-virtual {v0}, Lcom/google/obf/fj;->entrySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
