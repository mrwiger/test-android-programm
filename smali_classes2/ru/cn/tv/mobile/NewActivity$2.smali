.class Lru/cn/tv/mobile/NewActivity$2;
.super Landroid/view/OrientationEventListener;
.source "NewActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/tv/mobile/NewActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/tv/mobile/NewActivity;

.field final synthetic val$isTablet:Z


# direct methods
.method constructor <init>(Lru/cn/tv/mobile/NewActivity;Landroid/content/Context;Z)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/tv/mobile/NewActivity;
    .param p2, "x0"    # Landroid/content/Context;

    .prologue
    .line 154
    iput-object p1, p0, Lru/cn/tv/mobile/NewActivity$2;->this$0:Lru/cn/tv/mobile/NewActivity;

    iput-boolean p3, p0, Lru/cn/tv/mobile/NewActivity$2;->val$isTablet:Z

    invoke-direct {p0, p2}, Landroid/view/OrientationEventListener;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public onOrientationChanged(I)V
    .locals 4
    .param p1, "orientation"    # I

    .prologue
    const/4 v3, -0x1

    .line 158
    if-ne p1, v3, :cond_1

    .line 177
    :cond_0
    :goto_0
    return-void

    .line 162
    :cond_1
    iget-object v2, p0, Lru/cn/tv/mobile/NewActivity$2;->this$0:Lru/cn/tv/mobile/NewActivity;

    invoke-virtual {v2}, Lru/cn/tv/mobile/NewActivity;->getRequestedOrientation()I

    move-result v0

    .line 164
    .local v0, "currentOrientation":I
    const/4 v1, 0x0

    .line 165
    .local v1, "needChangeOrientation":Z
    const/16 v2, 0xe1

    if-le p1, v2, :cond_2

    const/16 v2, 0x13b

    if-lt p1, v2, :cond_3

    :cond_2
    const/16 v2, 0x2d

    if-le p1, v2, :cond_5

    const/16 v2, 0x87

    if-ge p1, v2, :cond_5

    .line 167
    :cond_3
    const/4 v2, 0x6

    if-ne v0, v2, :cond_5

    .line 168
    iget-boolean v2, p0, Lru/cn/tv/mobile/NewActivity$2;->val$isTablet:Z

    if-eqz v2, :cond_4

    iget-object v2, p0, Lru/cn/tv/mobile/NewActivity$2;->this$0:Lru/cn/tv/mobile/NewActivity;

    invoke-virtual {v2}, Lru/cn/tv/mobile/NewActivity;->isFullScreen()Z

    move-result v2

    if-nez v2, :cond_5

    .line 169
    :cond_4
    const/4 v1, 0x1

    .line 174
    :cond_5
    if-eqz v1, :cond_0

    .line 175
    iget-object v2, p0, Lru/cn/tv/mobile/NewActivity$2;->this$0:Lru/cn/tv/mobile/NewActivity;

    invoke-virtual {v2, v3}, Lru/cn/tv/mobile/NewActivity;->setRequestedOrientation(I)V

    goto :goto_0
.end method
