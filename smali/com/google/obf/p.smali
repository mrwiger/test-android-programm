.class public Lcom/google/obf/p;
.super Lcom/google/obf/n;
.source "IMASDK"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/obf/p$a;
    }
.end annotation


# instance fields
.field private final c:Lcom/google/obf/y;

.field private final d:Lcom/google/obf/p$a;

.field private final e:J

.field private final f:I

.field private final g:I

.field private h:Landroid/view/Surface;

.field private i:Z

.field private j:Z

.field private k:J

.field private l:J

.field private m:I

.field private n:I

.field private o:I

.field private p:F

.field private q:I

.field private r:I

.field private s:I

.field private t:F

.field private u:I

.field private v:I

.field private w:I

.field private x:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/obf/u;Lcom/google/obf/m;IJLandroid/os/Handler;Lcom/google/obf/p$a;I)V
    .locals 13

    .prologue
    .line 1
    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move/from16 v5, p4

    move-wide/from16 v6, p5

    move-object/from16 v10, p7

    move-object/from16 v11, p8

    move/from16 v12, p9

    invoke-direct/range {v1 .. v12}, Lcom/google/obf/p;-><init>(Landroid/content/Context;Lcom/google/obf/u;Lcom/google/obf/m;IJLcom/google/obf/ac;ZLandroid/os/Handler;Lcom/google/obf/p$a;I)V

    .line 2
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/obf/u;Lcom/google/obf/m;IJLcom/google/obf/ac;ZLandroid/os/Handler;Lcom/google/obf/p$a;I)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/obf/u;",
            "Lcom/google/obf/m;",
            "IJ",
            "Lcom/google/obf/ac",
            "<",
            "Lcom/google/obf/ae;",
            ">;Z",
            "Landroid/os/Handler;",
            "Lcom/google/obf/p$a;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 3
    move-object v2, p0

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p7

    move/from16 v6, p8

    move-object/from16 v7, p9

    move-object/from16 v8, p10

    invoke-direct/range {v2 .. v8}, Lcom/google/obf/n;-><init>(Lcom/google/obf/u;Lcom/google/obf/m;Lcom/google/obf/ac;ZLandroid/os/Handler;Lcom/google/obf/n$b;)V

    .line 4
    new-instance v2, Lcom/google/obf/y;

    invoke-direct {v2, p1}, Lcom/google/obf/y;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/google/obf/p;->c:Lcom/google/obf/y;

    .line 5
    iput p4, p0, Lcom/google/obf/p;->f:I

    .line 6
    const-wide/16 v2, 0x3e8

    mul-long/2addr v2, p5

    iput-wide v2, p0, Lcom/google/obf/p;->e:J

    .line 7
    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/google/obf/p;->d:Lcom/google/obf/p$a;

    .line 8
    move/from16 v0, p11

    iput v0, p0, Lcom/google/obf/p;->g:I

    .line 9
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/google/obf/p;->k:J

    .line 10
    const/4 v2, -0x1

    iput v2, p0, Lcom/google/obf/p;->q:I

    .line 11
    const/4 v2, -0x1

    iput v2, p0, Lcom/google/obf/p;->r:I

    .line 12
    const/high16 v2, -0x40800000    # -1.0f

    iput v2, p0, Lcom/google/obf/p;->t:F

    .line 13
    const/high16 v2, -0x40800000    # -1.0f

    iput v2, p0, Lcom/google/obf/p;->p:F

    .line 14
    const/4 v2, -0x1

    iput v2, p0, Lcom/google/obf/p;->u:I

    .line 15
    const/4 v2, -0x1

    iput v2, p0, Lcom/google/obf/p;->v:I

    .line 16
    const/high16 v2, -0x40800000    # -1.0f

    iput v2, p0, Lcom/google/obf/p;->x:F

    .line 17
    return-void
.end method

.method private A()V
    .locals 7

    .prologue
    .line 217
    iget-object v0, p0, Lcom/google/obf/p;->b:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/obf/p;->d:Lcom/google/obf/p$a;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/obf/p;->m:I

    if-nez v0, :cond_1

    .line 225
    :cond_0
    :goto_0
    return-void

    .line 219
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 220
    iget v2, p0, Lcom/google/obf/p;->m:I

    .line 221
    iget-wide v4, p0, Lcom/google/obf/p;->l:J

    sub-long v4, v0, v4

    .line 222
    iget-object v3, p0, Lcom/google/obf/p;->b:Landroid/os/Handler;

    new-instance v6, Lcom/google/obf/p$3;

    invoke-direct {v6, p0, v2, v4, v5}, Lcom/google/obf/p$3;-><init>(Lcom/google/obf/p;IJ)V

    invoke-virtual {v3, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 223
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/obf/p;->m:I

    .line 224
    iput-wide v0, p0, Lcom/google/obf/p;->l:J

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/obf/p;)Lcom/google/obf/p$a;
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lcom/google/obf/p;->d:Lcom/google/obf/p$a;

    return-object v0
.end method

.method private a()V
    .locals 7

    .prologue
    .line 199
    iget-object v0, p0, Lcom/google/obf/p;->b:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/obf/p;->d:Lcom/google/obf/p$a;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/obf/p;->u:I

    iget v1, p0, Lcom/google/obf/p;->q:I

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/google/obf/p;->v:I

    iget v1, p0, Lcom/google/obf/p;->r:I

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/google/obf/p;->w:I

    iget v1, p0, Lcom/google/obf/p;->s:I

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/google/obf/p;->x:F

    iget v1, p0, Lcom/google/obf/p;->t:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    .line 210
    :cond_0
    :goto_0
    return-void

    .line 201
    :cond_1
    iget v2, p0, Lcom/google/obf/p;->q:I

    .line 202
    iget v3, p0, Lcom/google/obf/p;->r:I

    .line 203
    iget v4, p0, Lcom/google/obf/p;->s:I

    .line 204
    iget v5, p0, Lcom/google/obf/p;->t:F

    .line 205
    iget-object v6, p0, Lcom/google/obf/p;->b:Landroid/os/Handler;

    new-instance v0, Lcom/google/obf/p$1;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/obf/p$1;-><init>(Lcom/google/obf/p;IIIF)V

    invoke-virtual {v6, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 206
    iput v2, p0, Lcom/google/obf/p;->u:I

    .line 207
    iput v3, p0, Lcom/google/obf/p;->v:I

    .line 208
    iput v4, p0, Lcom/google/obf/p;->w:I

    .line 209
    iput v5, p0, Lcom/google/obf/p;->x:F

    goto :goto_0
.end method

.method private a(Landroid/media/MediaFormat;Z)V
    .locals 7
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InlinedApi"
        }
    .end annotation

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    .line 172
    const-string v0, "max-input-size"

    invoke-virtual {p1, v0}, Landroid/media/MediaFormat;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 198
    :cond_0
    :goto_0
    return-void

    .line 174
    :cond_1
    const-string v0, "height"

    invoke-virtual {p1, v0}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v0

    .line 175
    if-eqz p2, :cond_2

    const-string v1, "max-height"

    invoke-virtual {p1, v1}, Landroid/media/MediaFormat;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 176
    const-string v1, "max-height"

    invoke-virtual {p1, v1}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 177
    :cond_2
    const-string v1, "width"

    invoke-virtual {p1, v1}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v1

    .line 178
    if-eqz p2, :cond_3

    const-string v4, "max-width"

    invoke-virtual {p1, v4}, Landroid/media/MediaFormat;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 179
    const-string v1, "max-width"

    invoke-virtual {p1, v1}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 180
    :cond_3
    const-string v4, "mime"

    invoke-virtual {p1, v4}, Landroid/media/MediaFormat;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v4, -0x1

    invoke-virtual {v5}, Ljava/lang/String;->hashCode()I

    move-result v6

    sparse-switch v6, :sswitch_data_0

    :cond_4
    :goto_1
    packed-switch v4, :pswitch_data_0

    goto :goto_0

    .line 181
    :pswitch_0
    mul-int/2addr v0, v1

    move v1, v0

    move v0, v2

    .line 196
    :goto_2
    mul-int/lit8 v1, v1, 0x3

    mul-int/lit8 v0, v0, 0x2

    div-int v0, v1, v0

    .line 197
    const-string v1, "max-input-size"

    invoke-virtual {p1, v1, v0}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    goto :goto_0

    .line 180
    :sswitch_0
    const-string v6, "video/3gpp"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    const/4 v4, 0x0

    goto :goto_1

    :sswitch_1
    const-string v6, "video/mp4v-es"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    const/4 v4, 0x1

    goto :goto_1

    :sswitch_2
    const-string v6, "video/avc"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    move v4, v2

    goto :goto_1

    :sswitch_3
    const-string v6, "video/x-vnd.on2.vp8"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    const/4 v4, 0x3

    goto :goto_1

    :sswitch_4
    const-string v6, "video/hevc"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    move v4, v3

    goto :goto_1

    :sswitch_5
    const-string v6, "video/x-vnd.on2.vp9"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    const/4 v4, 0x5

    goto :goto_1

    .line 184
    :pswitch_1
    const-string v3, "BRAVIA 4K 2015"

    sget-object v4, Lcom/google/obf/ea;->d:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 186
    add-int/lit8 v1, v1, 0xf

    div-int/lit8 v1, v1, 0x10

    add-int/lit8 v0, v0, 0xf

    div-int/lit8 v0, v0, 0x10

    mul-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x10

    mul-int/lit8 v0, v0, 0x10

    move v1, v0

    move v0, v2

    .line 188
    goto :goto_2

    .line 189
    :pswitch_2
    mul-int/2addr v0, v1

    move v1, v0

    move v0, v2

    .line 191
    goto :goto_2

    .line 192
    :pswitch_3
    mul-int/2addr v0, v1

    move v1, v0

    move v0, v3

    .line 194
    goto :goto_2

    .line 180
    :sswitch_data_0
    .sparse-switch
        -0x63306f58 -> :sswitch_0
        -0x63185e82 -> :sswitch_4
        0x46cdc642 -> :sswitch_1
        0x4f62373a -> :sswitch_2
        0x5f50bed8 -> :sswitch_3
        0x5f50bed9 -> :sswitch_5
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method private a(Landroid/view/Surface;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/g;
        }
    .end annotation

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/obf/p;->h:Landroid/view/Surface;

    if-ne v0, p1, :cond_1

    .line 72
    :cond_0
    :goto_0
    return-void

    .line 66
    :cond_1
    iput-object p1, p0, Lcom/google/obf/p;->h:Landroid/view/Surface;

    .line 67
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/obf/p;->i:Z

    .line 68
    invoke-virtual {p0}, Lcom/google/obf/p;->v()I

    move-result v0

    .line 69
    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 70
    :cond_2
    invoke-virtual {p0}, Lcom/google/obf/p;->m()V

    .line 71
    invoke-virtual {p0}, Lcom/google/obf/p;->j()V

    goto :goto_0
.end method

.method private i()V
    .locals 3

    .prologue
    .line 211
    iget-object v0, p0, Lcom/google/obf/p;->b:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/obf/p;->d:Lcom/google/obf/p$a;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/obf/p;->i:Z

    if-eqz v0, :cond_1

    .line 216
    :cond_0
    :goto_0
    return-void

    .line 213
    :cond_1
    iget-object v0, p0, Lcom/google/obf/p;->h:Landroid/view/Surface;

    .line 214
    iget-object v1, p0, Lcom/google/obf/p;->b:Landroid/os/Handler;

    new-instance v2, Lcom/google/obf/p$2;

    invoke-direct {v2, p0, v0}, Lcom/google/obf/p$2;-><init>(Lcom/google/obf/p;Landroid/view/Surface;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 215
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/obf/p;->i:Z

    goto :goto_0
.end method


# virtual methods
.method protected a(IJZ)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/g;
        }
    .end annotation

    .prologue
    .line 22
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/obf/n;->a(IJZ)V

    .line 23
    if-eqz p4, :cond_0

    iget-wide v0, p0, Lcom/google/obf/p;->e:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 24
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    iget-wide v2, p0, Lcom/google/obf/p;->e:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/obf/p;->k:J

    .line 25
    :cond_0
    iget-object v0, p0, Lcom/google/obf/p;->c:Lcom/google/obf/y;

    invoke-virtual {v0}, Lcom/google/obf/y;->a()V

    .line 26
    return-void
.end method

.method public a(ILjava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/g;
        }
    .end annotation

    .prologue
    .line 60
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 61
    check-cast p2, Landroid/view/Surface;

    invoke-direct {p0, p2}, Lcom/google/obf/p;->a(Landroid/view/Surface;)V

    .line 63
    :goto_0
    return-void

    .line 62
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/obf/n;->a(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method protected a(J)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/g;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 27
    invoke-super {p0, p1, p2}, Lcom/google/obf/n;->a(J)V

    .line 28
    iput-boolean v0, p0, Lcom/google/obf/p;->j:Z

    .line 29
    iput v0, p0, Lcom/google/obf/p;->n:I

    .line 30
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/obf/p;->k:J

    .line 31
    return-void
.end method

.method protected a(Landroid/media/MediaCodec;I)V
    .locals 2

    .prologue
    .line 141
    const-string v0, "skipVideoBuffer"

    invoke-static {v0}, Lcom/google/obf/dz;->a(Ljava/lang/String;)V

    .line 142
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V

    .line 143
    invoke-static {}, Lcom/google/obf/dz;->a()V

    .line 144
    iget-object v0, p0, Lcom/google/obf/p;->a:Lcom/google/obf/c;

    iget v1, v0, Lcom/google/obf/c;->g:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/obf/c;->g:I

    .line 145
    return-void
.end method

.method protected a(Landroid/media/MediaCodec;IJ)V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 164
    invoke-direct {p0}, Lcom/google/obf/p;->a()V

    .line 165
    const-string v0, "releaseOutputBuffer"

    invoke-static {v0}, Lcom/google/obf/dz;->a(Ljava/lang/String;)V

    .line 166
    invoke-virtual {p1, p2, p3, p4}, Landroid/media/MediaCodec;->releaseOutputBuffer(IJ)V

    .line 167
    invoke-static {}, Lcom/google/obf/dz;->a()V

    .line 168
    iget-object v0, p0, Lcom/google/obf/p;->a:Lcom/google/obf/c;

    iget v1, v0, Lcom/google/obf/c;->f:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/obf/c;->f:I

    .line 169
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/obf/p;->j:Z

    .line 170
    invoke-direct {p0}, Lcom/google/obf/p;->i()V

    .line 171
    return-void
.end method

.method protected a(Landroid/media/MediaCodec;Landroid/media/MediaFormat;)V
    .locals 3

    .prologue
    .line 83
    const-string v0, "crop-right"

    invoke-virtual {p2, v0}, Landroid/media/MediaFormat;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "crop-left"

    .line 84
    invoke-virtual {p2, v0}, Landroid/media/MediaFormat;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "crop-bottom"

    invoke-virtual {p2, v0}, Landroid/media/MediaFormat;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "crop-top"

    .line 85
    invoke-virtual {p2, v0}, Landroid/media/MediaFormat;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    move v1, v0

    .line 86
    :goto_0
    if-eqz v1, :cond_3

    .line 87
    const-string v0, "crop-right"

    invoke-virtual {p2, v0}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v0

    const-string v2, "crop-left"

    invoke-virtual {p2, v2}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v2

    sub-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x1

    .line 88
    :goto_1
    iput v0, p0, Lcom/google/obf/p;->q:I

    .line 89
    if-eqz v1, :cond_4

    .line 90
    const-string v0, "crop-bottom"

    invoke-virtual {p2, v0}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v0

    const-string v1, "crop-top"

    invoke-virtual {p2, v1}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v1

    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    .line 91
    :goto_2
    iput v0, p0, Lcom/google/obf/p;->r:I

    .line 92
    iget v0, p0, Lcom/google/obf/p;->p:F

    iput v0, p0, Lcom/google/obf/p;->t:F

    .line 93
    sget v0, Lcom/google/obf/ea;->a:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_5

    .line 94
    iget v0, p0, Lcom/google/obf/p;->o:I

    const/16 v1, 0x5a

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/obf/p;->o:I

    const/16 v1, 0x10e

    if-ne v0, v1, :cond_1

    .line 95
    :cond_0
    iget v0, p0, Lcom/google/obf/p;->q:I

    .line 96
    iget v1, p0, Lcom/google/obf/p;->r:I

    iput v1, p0, Lcom/google/obf/p;->q:I

    .line 97
    iput v0, p0, Lcom/google/obf/p;->r:I

    .line 98
    const/high16 v0, 0x3f800000    # 1.0f

    iget v1, p0, Lcom/google/obf/p;->t:F

    div-float/2addr v0, v1

    iput v0, p0, Lcom/google/obf/p;->t:F

    .line 101
    :cond_1
    :goto_3
    iget v0, p0, Lcom/google/obf/p;->f:I

    invoke-virtual {p1, v0}, Landroid/media/MediaCodec;->setVideoScalingMode(I)V

    .line 102
    return-void

    .line 85
    :cond_2
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    .line 88
    :cond_3
    const-string v0, "width"

    invoke-virtual {p2, v0}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 91
    :cond_4
    const-string v0, "height"

    invoke-virtual {p2, v0}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v0

    goto :goto_2

    .line 100
    :cond_5
    iget v0, p0, Lcom/google/obf/p;->o:I

    iput v0, p0, Lcom/google/obf/p;->s:I

    goto :goto_3
.end method

.method protected a(Landroid/media/MediaCodec;ZLandroid/media/MediaFormat;Landroid/media/MediaCrypto;)V
    .locals 2

    .prologue
    .line 74
    invoke-direct {p0, p3, p2}, Lcom/google/obf/p;->a(Landroid/media/MediaFormat;Z)V

    .line 75
    iget-object v0, p0, Lcom/google/obf/p;->h:Landroid/view/Surface;

    const/4 v1, 0x0

    invoke-virtual {p1, p3, v0, p4, v1}, Landroid/media/MediaCodec;->configure(Landroid/media/MediaFormat;Landroid/view/Surface;Landroid/media/MediaCrypto;I)V

    .line 76
    return-void
.end method

.method protected a(Lcom/google/obf/r;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/g;
        }
    .end annotation

    .prologue
    .line 77
    invoke-super {p0, p1}, Lcom/google/obf/n;->a(Lcom/google/obf/r;)V

    .line 78
    iget-object v0, p1, Lcom/google/obf/r;->a:Lcom/google/obf/q;

    iget v0, v0, Lcom/google/obf/q;->m:F

    const/high16 v1, -0x40800000    # -1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    .line 79
    :goto_0
    iput v0, p0, Lcom/google/obf/p;->p:F

    .line 80
    iget-object v0, p1, Lcom/google/obf/r;->a:Lcom/google/obf/q;

    iget v0, v0, Lcom/google/obf/q;->l:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    const/4 v0, 0x0

    .line 81
    :goto_1
    iput v0, p0, Lcom/google/obf/p;->o:I

    .line 82
    return-void

    .line 79
    :cond_0
    iget-object v0, p1, Lcom/google/obf/r;->a:Lcom/google/obf/q;

    iget v0, v0, Lcom/google/obf/q;->m:F

    goto :goto_0

    .line 81
    :cond_1
    iget-object v0, p1, Lcom/google/obf/r;->a:Lcom/google/obf/q;

    iget v0, v0, Lcom/google/obf/q;->l:I

    goto :goto_1
.end method

.method protected a(JJ)Z
    .locals 3

    .prologue
    .line 140
    const-wide/16 v0, -0x7530

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(JJLandroid/media/MediaCodec;Ljava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;IZ)Z
    .locals 11

    .prologue
    .line 104
    if-eqz p9, :cond_0

    .line 105
    move-object/from16 v0, p5

    move/from16 v1, p8

    invoke-virtual {p0, v0, v1}, Lcom/google/obf/p;->a(Landroid/media/MediaCodec;I)V

    .line 106
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/obf/p;->n:I

    .line 107
    const/4 v2, 0x1

    .line 139
    :goto_0
    return v2

    .line 108
    :cond_0
    iget-boolean v2, p0, Lcom/google/obf/p;->j:Z

    if-nez v2, :cond_2

    .line 109
    sget v2, Lcom/google/obf/ea;->a:I

    const/16 v3, 0x15

    if-lt v2, v3, :cond_1

    .line 110
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    move-object/from16 v0, p5

    move/from16 v1, p8

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/obf/p;->a(Landroid/media/MediaCodec;IJ)V

    .line 112
    :goto_1
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/obf/p;->n:I

    .line 113
    const/4 v2, 0x1

    goto :goto_0

    .line 111
    :cond_1
    move-object/from16 v0, p5

    move/from16 v1, p8

    invoke-virtual {p0, v0, v1}, Lcom/google/obf/p;->c(Landroid/media/MediaCodec;I)V

    goto :goto_1

    .line 114
    :cond_2
    invoke-virtual {p0}, Lcom/google/obf/p;->v()I

    move-result v2

    const/4 v3, 0x3

    if-eq v2, v3, :cond_3

    .line 115
    const/4 v2, 0x0

    goto :goto_0

    .line 116
    :cond_3
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    sub-long/2addr v2, p3

    .line 117
    move-object/from16 v0, p7

    iget-wide v4, v0, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    sub-long/2addr v4, p1

    sub-long v2, v4, v2

    .line 118
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v4

    .line 119
    const-wide/16 v6, 0x3e8

    mul-long/2addr v2, v6

    add-long/2addr v2, v4

    .line 120
    iget-object v6, p0, Lcom/google/obf/p;->c:Lcom/google/obf/y;

    move-object/from16 v0, p7

    iget-wide v8, v0, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    invoke-virtual {v6, v8, v9, v2, v3}, Lcom/google/obf/y;->a(JJ)J

    move-result-wide v2

    .line 121
    sub-long v4, v2, v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    .line 122
    invoke-virtual {p0, v4, v5, p3, p4}, Lcom/google/obf/p;->a(JJ)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 123
    move-object/from16 v0, p5

    move/from16 v1, p8

    invoke-virtual {p0, v0, v1}, Lcom/google/obf/p;->b(Landroid/media/MediaCodec;I)V

    .line 124
    const/4 v2, 0x1

    goto :goto_0

    .line 125
    :cond_4
    sget v6, Lcom/google/obf/ea;->a:I

    const/16 v7, 0x15

    if-lt v6, v7, :cond_5

    .line 126
    const-wide/32 v6, 0xc350

    cmp-long v4, v4, v6

    if-gez v4, :cond_7

    .line 127
    move-object/from16 v0, p5

    move/from16 v1, p8

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/obf/p;->a(Landroid/media/MediaCodec;IJ)V

    .line 128
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/obf/p;->n:I

    .line 129
    const/4 v2, 0x1

    goto :goto_0

    .line 130
    :cond_5
    const-wide/16 v2, 0x7530

    cmp-long v2, v4, v2

    if-gez v2, :cond_7

    .line 131
    const-wide/16 v2, 0x2af8

    cmp-long v2, v4, v2

    if-lez v2, :cond_6

    .line 132
    const-wide/16 v2, 0x2710

    sub-long v2, v4, v2

    const-wide/16 v4, 0x3e8

    :try_start_0
    div-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 136
    :cond_6
    :goto_2
    move-object/from16 v0, p5

    move/from16 v1, p8

    invoke-virtual {p0, v0, v1}, Lcom/google/obf/p;->c(Landroid/media/MediaCodec;I)V

    .line 137
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/obf/p;->n:I

    .line 138
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 134
    :catch_0
    move-exception v2

    .line 135
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V

    goto :goto_2

    .line 139
    :cond_7
    const/4 v2, 0x0

    goto/16 :goto_0
.end method

.method protected a(Landroid/media/MediaCodec;ZLcom/google/obf/q;Lcom/google/obf/q;)Z
    .locals 2

    .prologue
    .line 103
    iget-object v0, p4, Lcom/google/obf/q;->b:Ljava/lang/String;

    iget-object v1, p3, Lcom/google/obf/q;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    if-nez p2, :cond_0

    iget v0, p3, Lcom/google/obf/q;->h:I

    iget v1, p4, Lcom/google/obf/q;->h:I

    if-ne v0, v1, :cond_1

    iget v0, p3, Lcom/google/obf/q;->i:I

    iget v1, p4, Lcom/google/obf/q;->i:I

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Lcom/google/obf/m;Lcom/google/obf/q;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/o$b;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 18
    iget-object v1, p2, Lcom/google/obf/q;->b:Ljava/lang/String;

    .line 19
    invoke-static {v1}, Lcom/google/obf/ds;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "video/x-unknown"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 20
    invoke-interface {p1, v1, v0}, Lcom/google/obf/m;->a(Ljava/lang/String;Z)Lcom/google/obf/f;

    move-result-object v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 21
    :cond_1
    return v0
.end method

.method protected b(Landroid/media/MediaCodec;I)V
    .locals 3

    .prologue
    .line 146
    const-string v0, "dropVideoBuffer"

    invoke-static {v0}, Lcom/google/obf/dz;->a(Ljava/lang/String;)V

    .line 147
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V

    .line 148
    invoke-static {}, Lcom/google/obf/dz;->a()V

    .line 149
    iget-object v0, p0, Lcom/google/obf/p;->a:Lcom/google/obf/c;

    iget v1, v0, Lcom/google/obf/c;->h:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/obf/c;->h:I

    .line 150
    iget v0, p0, Lcom/google/obf/p;->m:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/obf/p;->m:I

    .line 151
    iget v0, p0, Lcom/google/obf/p;->n:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/obf/p;->n:I

    .line 152
    iget-object v0, p0, Lcom/google/obf/p;->a:Lcom/google/obf/c;

    iget v1, p0, Lcom/google/obf/p;->n:I

    iget-object v2, p0, Lcom/google/obf/p;->a:Lcom/google/obf/c;

    iget v2, v2, Lcom/google/obf/c;->i:I

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, v0, Lcom/google/obf/c;->i:I

    .line 153
    iget v0, p0, Lcom/google/obf/p;->m:I

    iget v1, p0, Lcom/google/obf/p;->g:I

    if-ne v0, v1, :cond_0

    .line 154
    invoke-direct {p0}, Lcom/google/obf/p;->A()V

    .line 155
    :cond_0
    return-void
.end method

.method protected c()V
    .locals 2

    .prologue
    .line 42
    invoke-super {p0}, Lcom/google/obf/n;->c()V

    .line 43
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/obf/p;->m:I

    .line 44
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/obf/p;->l:J

    .line 45
    return-void
.end method

.method protected c(Landroid/media/MediaCodec;I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 156
    invoke-direct {p0}, Lcom/google/obf/p;->a()V

    .line 157
    const-string v0, "releaseOutputBuffer"

    invoke-static {v0}, Lcom/google/obf/dz;->a(Ljava/lang/String;)V

    .line 158
    invoke-virtual {p1, p2, v2}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V

    .line 159
    invoke-static {}, Lcom/google/obf/dz;->a()V

    .line 160
    iget-object v0, p0, Lcom/google/obf/p;->a:Lcom/google/obf/c;

    iget v1, v0, Lcom/google/obf/c;->f:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/obf/c;->f:I

    .line 161
    iput-boolean v2, p0, Lcom/google/obf/p;->j:Z

    .line 162
    invoke-direct {p0}, Lcom/google/obf/p;->i()V

    .line 163
    return-void
.end method

.method protected d()V
    .locals 2

    .prologue
    .line 46
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/obf/p;->k:J

    .line 47
    invoke-direct {p0}, Lcom/google/obf/p;->A()V

    .line 48
    invoke-super {p0}, Lcom/google/obf/n;->d()V

    .line 49
    return-void
.end method

.method protected f()Z
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const-wide/16 v6, -0x1

    .line 32
    invoke-super {p0}, Lcom/google/obf/n;->f()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lcom/google/obf/p;->j:Z

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/google/obf/p;->l()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 33
    invoke-virtual {p0}, Lcom/google/obf/p;->o()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    .line 34
    :cond_0
    iput-wide v6, p0, Lcom/google/obf/p;->k:J

    .line 41
    :cond_1
    :goto_0
    return v0

    .line 36
    :cond_2
    iget-wide v2, p0, Lcom/google/obf/p;->k:J

    cmp-long v2, v2, v6

    if-nez v2, :cond_3

    move v0, v1

    .line 37
    goto :goto_0

    .line 38
    :cond_3
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    iget-wide v4, p0, Lcom/google/obf/p;->k:J

    cmp-long v2, v2, v4

    if-ltz v2, :cond_1

    .line 40
    iput-wide v6, p0, Lcom/google/obf/p;->k:J

    move v0, v1

    .line 41
    goto :goto_0
.end method

.method protected g()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/g;
        }
    .end annotation

    .prologue
    const/high16 v1, -0x40800000    # -1.0f

    const/4 v0, -0x1

    .line 50
    iput v0, p0, Lcom/google/obf/p;->q:I

    .line 51
    iput v0, p0, Lcom/google/obf/p;->r:I

    .line 52
    iput v1, p0, Lcom/google/obf/p;->t:F

    .line 53
    iput v1, p0, Lcom/google/obf/p;->p:F

    .line 54
    iput v0, p0, Lcom/google/obf/p;->u:I

    .line 55
    iput v0, p0, Lcom/google/obf/p;->v:I

    .line 56
    iput v1, p0, Lcom/google/obf/p;->x:F

    .line 57
    iget-object v0, p0, Lcom/google/obf/p;->c:Lcom/google/obf/y;

    invoke-virtual {v0}, Lcom/google/obf/y;->b()V

    .line 58
    invoke-super {p0}, Lcom/google/obf/n;->g()V

    .line 59
    return-void
.end method

.method protected k()Z
    .locals 1

    .prologue
    .line 73
    invoke-super {p0}, Lcom/google/obf/n;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/obf/p;->h:Landroid/view/Surface;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/obf/p;->h:Landroid/view/Surface;

    invoke-virtual {v0}, Landroid/view/Surface;->isValid()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
