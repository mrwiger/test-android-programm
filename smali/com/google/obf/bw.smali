.class final Lcom/google/obf/bw;
.super Ljava/lang/Object;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/obf/bw$c;,
        Lcom/google/obf/bw$d;,
        Lcom/google/obf/bw$b;,
        Lcom/google/obf/bw$a;
    }
.end annotation


# direct methods
.method public static a(I)I
    .locals 1

    .prologue
    .line 1
    const/4 v0, 0x0

    .line 2
    :goto_0
    if-lez p0, :cond_0

    .line 3
    add-int/lit8 v0, v0, 0x1

    .line 4
    ushr-int/lit8 p0, p0, 0x1

    goto :goto_0

    .line 5
    :cond_0
    return v0
.end method

.method private static a(JJ)J
    .locals 6

    .prologue
    .line 221
    long-to-double v0, p0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    long-to-double v4, p2

    div-double/2addr v2, v4

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-long v0, v0

    return-wide v0
.end method

.method public static a(Lcom/google/obf/dw;)Lcom/google/obf/bw$d;
    .locals 14
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/s;
        }
    .end annotation

    .prologue
    .line 6
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {v0, p0, v1}, Lcom/google/obf/bw;->a(ILcom/google/obf/dw;Z)Z

    .line 7
    invoke-virtual {p0}, Lcom/google/obf/dw;->l()J

    move-result-wide v1

    .line 8
    invoke-virtual {p0}, Lcom/google/obf/dw;->f()I

    move-result v3

    .line 9
    invoke-virtual {p0}, Lcom/google/obf/dw;->l()J

    move-result-wide v4

    .line 10
    invoke-virtual {p0}, Lcom/google/obf/dw;->n()I

    move-result v6

    .line 11
    invoke-virtual {p0}, Lcom/google/obf/dw;->n()I

    move-result v7

    .line 12
    invoke-virtual {p0}, Lcom/google/obf/dw;->n()I

    move-result v8

    .line 13
    invoke-virtual {p0}, Lcom/google/obf/dw;->f()I

    move-result v0

    .line 14
    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    and-int/lit8 v9, v0, 0xf

    int-to-double v12, v9

    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v10

    double-to-int v9, v10

    .line 15
    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    and-int/lit16 v0, v0, 0xf0

    shr-int/lit8 v0, v0, 0x4

    int-to-double v12, v0

    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v10

    double-to-int v10, v10

    .line 16
    invoke-virtual {p0}, Lcom/google/obf/dw;->f()I

    move-result v0

    and-int/lit8 v0, v0, 0x1

    if-lez v0, :cond_0

    const/4 v11, 0x1

    .line 17
    :goto_0
    iget-object v0, p0, Lcom/google/obf/dw;->a:[B

    invoke-virtual {p0}, Lcom/google/obf/dw;->c()I

    move-result v12

    invoke-static {v0, v12}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v12

    .line 18
    new-instance v0, Lcom/google/obf/bw$d;

    invoke-direct/range {v0 .. v12}, Lcom/google/obf/bw$d;-><init>(JIJIIIIIZ[B)V

    return-object v0

    .line 16
    :cond_0
    const/4 v11, 0x0

    goto :goto_0
.end method

.method private static a(ILcom/google/obf/bu;)V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/s;
        }
    .end annotation

    .prologue
    const/4 v9, 0x4

    const/4 v1, 0x1

    const/16 v8, 0x8

    const/4 v3, 0x0

    .line 81
    const/4 v0, 0x6

    invoke-virtual {p1, v0}, Lcom/google/obf/bu;->a(I)I

    move-result v0

    add-int/lit8 v5, v0, 0x1

    move v4, v3

    .line 82
    :goto_0
    if-ge v4, v5, :cond_5

    .line 83
    const/16 v0, 0x10

    invoke-virtual {p1, v0}, Lcom/google/obf/bu;->a(I)I

    move-result v0

    .line 84
    packed-switch v0, :pswitch_data_0

    .line 106
    const-string v2, "VorbisUtil"

    const/16 v6, 0x34

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "mapping type other than 0 not supported: "

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    :cond_0
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    .line 85
    :pswitch_0
    invoke-virtual {p1}, Lcom/google/obf/bu;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 86
    invoke-virtual {p1, v9}, Lcom/google/obf/bu;->a(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 88
    :goto_1
    invoke-virtual {p1}, Lcom/google/obf/bu;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 89
    invoke-virtual {p1, v8}, Lcom/google/obf/bu;->a(I)I

    move-result v2

    add-int/lit8 v6, v2, 0x1

    move v2, v3

    .line 90
    :goto_2
    if-ge v2, v6, :cond_2

    .line 91
    add-int/lit8 v7, p0, -0x1

    invoke-static {v7}, Lcom/google/obf/bw;->a(I)I

    move-result v7

    invoke-virtual {p1, v7}, Lcom/google/obf/bu;->b(I)V

    .line 92
    add-int/lit8 v7, p0, -0x1

    invoke-static {v7}, Lcom/google/obf/bw;->a(I)I

    move-result v7

    invoke-virtual {p1, v7}, Lcom/google/obf/bu;->b(I)V

    .line 93
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_1
    move v0, v1

    .line 87
    goto :goto_1

    .line 94
    :cond_2
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, Lcom/google/obf/bu;->a(I)I

    move-result v2

    if-eqz v2, :cond_3

    .line 95
    new-instance v0, Lcom/google/obf/s;

    const-string v1, "to reserved bits must be zero after mapping coupling steps"

    invoke-direct {v0, v1}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v0

    .line 96
    :cond_3
    if-le v0, v1, :cond_4

    move v2, v3

    .line 97
    :goto_3
    if-ge v2, p0, :cond_4

    .line 98
    invoke-virtual {p1, v9}, Lcom/google/obf/bu;->b(I)V

    .line 99
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_4
    move v2, v3

    .line 100
    :goto_4
    if-ge v2, v0, :cond_0

    .line 101
    invoke-virtual {p1, v8}, Lcom/google/obf/bu;->b(I)V

    .line 102
    invoke-virtual {p1, v8}, Lcom/google/obf/bu;->b(I)V

    .line 103
    invoke-virtual {p1, v8}, Lcom/google/obf/bu;->b(I)V

    .line 104
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 108
    :cond_5
    return-void

    .line 84
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public static a(ILcom/google/obf/dw;Z)Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/s;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 38
    invoke-virtual {p1}, Lcom/google/obf/dw;->f()I

    move-result v1

    if-eq v1, p0, :cond_3

    .line 39
    if-eqz p2, :cond_1

    .line 51
    :cond_0
    :goto_0
    return v0

    .line 41
    :cond_1
    new-instance v1, Lcom/google/obf/s;

    const-string v2, "expected header type "

    invoke-static {p0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-direct {v1, v0}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 42
    :cond_3
    invoke-virtual {p1}, Lcom/google/obf/dw;->f()I

    move-result v1

    const/16 v2, 0x76

    if-ne v1, v2, :cond_4

    .line 43
    invoke-virtual {p1}, Lcom/google/obf/dw;->f()I

    move-result v1

    const/16 v2, 0x6f

    if-ne v1, v2, :cond_4

    .line 44
    invoke-virtual {p1}, Lcom/google/obf/dw;->f()I

    move-result v1

    const/16 v2, 0x72

    if-ne v1, v2, :cond_4

    .line 45
    invoke-virtual {p1}, Lcom/google/obf/dw;->f()I

    move-result v1

    const/16 v2, 0x62

    if-ne v1, v2, :cond_4

    .line 46
    invoke-virtual {p1}, Lcom/google/obf/dw;->f()I

    move-result v1

    const/16 v2, 0x69

    if-ne v1, v2, :cond_4

    .line 47
    invoke-virtual {p1}, Lcom/google/obf/dw;->f()I

    move-result v1

    const/16 v2, 0x73

    if-eq v1, v2, :cond_5

    .line 48
    :cond_4
    if-nez p2, :cond_0

    .line 50
    new-instance v0, Lcom/google/obf/s;

    const-string v1, "expected characters \'vorbis\'"

    invoke-direct {v0, v1}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v0

    .line 51
    :cond_5
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static a(Lcom/google/obf/bu;)[Lcom/google/obf/bw$c;
    .locals 9

    .prologue
    const/16 v8, 0x10

    .line 71
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/google/obf/bu;->a(I)I

    move-result v0

    add-int/lit8 v1, v0, 0x1

    .line 72
    new-array v2, v1, [Lcom/google/obf/bw$c;

    .line 73
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 74
    invoke-virtual {p0}, Lcom/google/obf/bu;->a()Z

    move-result v3

    .line 75
    invoke-virtual {p0, v8}, Lcom/google/obf/bu;->a(I)I

    move-result v4

    .line 76
    invoke-virtual {p0, v8}, Lcom/google/obf/bu;->a(I)I

    move-result v5

    .line 77
    const/16 v6, 0x8

    invoke-virtual {p0, v6}, Lcom/google/obf/bu;->a(I)I

    move-result v6

    .line 78
    new-instance v7, Lcom/google/obf/bw$c;

    invoke-direct {v7, v3, v4, v5, v6}, Lcom/google/obf/bw$c;-><init>(ZIII)V

    aput-object v7, v2, v0

    .line 79
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 80
    :cond_0
    return-object v2
.end method

.method public static a(Lcom/google/obf/dw;I)[Lcom/google/obf/bw$c;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/s;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 52
    const/4 v1, 0x5

    invoke-static {v1, p0, v0}, Lcom/google/obf/bw;->a(ILcom/google/obf/dw;Z)Z

    .line 53
    invoke-virtual {p0}, Lcom/google/obf/dw;->f()I

    move-result v1

    add-int/lit8 v2, v1, 0x1

    .line 54
    new-instance v3, Lcom/google/obf/bu;

    iget-object v1, p0, Lcom/google/obf/dw;->a:[B

    invoke-direct {v3, v1}, Lcom/google/obf/bu;-><init>([B)V

    .line 55
    invoke-virtual {p0}, Lcom/google/obf/dw;->d()I

    move-result v1

    mul-int/lit8 v1, v1, 0x8

    invoke-virtual {v3, v1}, Lcom/google/obf/bu;->b(I)V

    move v1, v0

    .line 56
    :goto_0
    if-ge v1, v2, :cond_0

    .line 57
    invoke-static {v3}, Lcom/google/obf/bw;->d(Lcom/google/obf/bu;)Lcom/google/obf/bw$a;

    .line 58
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 59
    :cond_0
    const/4 v1, 0x6

    invoke-virtual {v3, v1}, Lcom/google/obf/bu;->a(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 60
    :goto_1
    if-ge v0, v1, :cond_2

    .line 61
    const/16 v2, 0x10

    invoke-virtual {v3, v2}, Lcom/google/obf/bu;->a(I)I

    move-result v2

    if-eqz v2, :cond_1

    .line 62
    new-instance v0, Lcom/google/obf/s;

    const-string v1, "placeholder of time domain transforms not zeroed out"

    invoke-direct {v0, v1}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v0

    .line 63
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 64
    :cond_2
    invoke-static {v3}, Lcom/google/obf/bw;->c(Lcom/google/obf/bu;)V

    .line 65
    invoke-static {v3}, Lcom/google/obf/bw;->b(Lcom/google/obf/bu;)V

    .line 66
    invoke-static {p1, v3}, Lcom/google/obf/bw;->a(ILcom/google/obf/bu;)V

    .line 67
    invoke-static {v3}, Lcom/google/obf/bw;->a(Lcom/google/obf/bu;)[Lcom/google/obf/bw$c;

    move-result-object v0

    .line 68
    invoke-virtual {v3}, Lcom/google/obf/bu;->a()Z

    move-result v1

    if-nez v1, :cond_3

    .line 69
    new-instance v0, Lcom/google/obf/s;

    const-string v1, "framing bit after modes not set as expected"

    invoke-direct {v0, v1}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v0

    .line 70
    :cond_3
    return-object v0
.end method

.method public static b(Lcom/google/obf/dw;)Lcom/google/obf/bw$b;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/s;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 19
    const/4 v1, 0x3

    invoke-static {v1, p0, v0}, Lcom/google/obf/bw;->a(ILcom/google/obf/dw;Z)Z

    .line 21
    invoke-virtual {p0}, Lcom/google/obf/dw;->l()J

    move-result-wide v2

    long-to-int v1, v2

    .line 23
    invoke-virtual {p0, v1}, Lcom/google/obf/dw;->e(I)Ljava/lang/String;

    move-result-object v2

    .line 24
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0xb

    .line 25
    invoke-virtual {p0}, Lcom/google/obf/dw;->l()J

    move-result-wide v4

    .line 26
    long-to-int v3, v4

    new-array v3, v3, [Ljava/lang/String;

    .line 27
    add-int/lit8 v1, v1, 0x4

    .line 28
    :goto_0
    int-to-long v6, v0

    cmp-long v6, v6, v4

    if-gez v6, :cond_0

    .line 29
    invoke-virtual {p0}, Lcom/google/obf/dw;->l()J

    move-result-wide v6

    long-to-int v6, v6

    .line 30
    add-int/lit8 v1, v1, 0x4

    .line 31
    invoke-virtual {p0, v6}, Lcom/google/obf/dw;->e(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v0

    .line 32
    aget-object v6, v3, v0

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v1, v6

    .line 33
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 34
    :cond_0
    invoke-virtual {p0}, Lcom/google/obf/dw;->f()I

    move-result v0

    and-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_1

    .line 35
    new-instance v0, Lcom/google/obf/s;

    const-string v1, "framing bit expected to be set"

    invoke-direct {v0, v1}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v0

    .line 36
    :cond_1
    add-int/lit8 v0, v1, 0x1

    .line 37
    new-instance v1, Lcom/google/obf/bw$b;

    invoke-direct {v1, v2, v3, v0}, Lcom/google/obf/bw$b;-><init>(Ljava/lang/String;[Ljava/lang/String;I)V

    return-object v1
.end method

.method private static b(Lcom/google/obf/bu;)V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/s;
        }
    .end annotation

    .prologue
    const/4 v11, 0x6

    const/16 v10, 0x18

    const/16 v9, 0x8

    const/4 v1, 0x0

    .line 109
    invoke-virtual {p0, v11}, Lcom/google/obf/bu;->a(I)I

    move-result v0

    add-int/lit8 v4, v0, 0x1

    move v3, v1

    .line 110
    :goto_0
    if-ge v3, v4, :cond_5

    .line 111
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Lcom/google/obf/bu;->a(I)I

    move-result v0

    .line 112
    const/4 v2, 0x2

    if-le v0, v2, :cond_0

    .line 113
    new-instance v0, Lcom/google/obf/s;

    const-string v1, "residueType greater than 2 is not decodable"

    invoke-direct {v0, v1}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v0

    .line 114
    :cond_0
    invoke-virtual {p0, v10}, Lcom/google/obf/bu;->b(I)V

    .line 115
    invoke-virtual {p0, v10}, Lcom/google/obf/bu;->b(I)V

    .line 116
    invoke-virtual {p0, v10}, Lcom/google/obf/bu;->b(I)V

    .line 117
    invoke-virtual {p0, v11}, Lcom/google/obf/bu;->a(I)I

    move-result v0

    add-int/lit8 v5, v0, 0x1

    .line 118
    invoke-virtual {p0, v9}, Lcom/google/obf/bu;->b(I)V

    .line 119
    new-array v6, v5, [I

    move v2, v1

    .line 120
    :goto_1
    if-ge v2, v5, :cond_1

    .line 122
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/obf/bu;->a(I)I

    move-result v7

    .line 123
    invoke-virtual {p0}, Lcom/google/obf/bu;->a()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 124
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/google/obf/bu;->a(I)I

    move-result v0

    .line 125
    :goto_2
    mul-int/lit8 v0, v0, 0x8

    add-int/2addr v0, v7

    aput v0, v6, v2

    .line 126
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_1
    move v2, v1

    .line 127
    :goto_3
    if-ge v2, v5, :cond_4

    move v0, v1

    .line 128
    :goto_4
    if-ge v0, v9, :cond_3

    .line 129
    aget v7, v6, v2

    const/4 v8, 0x1

    shl-int/2addr v8, v0

    and-int/2addr v7, v8

    if-eqz v7, :cond_2

    .line 130
    invoke-virtual {p0, v9}, Lcom/google/obf/bu;->b(I)V

    .line 131
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 132
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 133
    :cond_4
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 134
    :cond_5
    return-void

    :cond_6
    move v0, v1

    goto :goto_2
.end method

.method private static c(Lcom/google/obf/bu;)V
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/s;
        }
    .end annotation

    .prologue
    const/4 v14, 0x2

    const/16 v13, 0x10

    const/4 v12, 0x4

    const/16 v11, 0x8

    const/4 v1, 0x0

    .line 135
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/google/obf/bu;->a(I)I

    move-result v0

    add-int/lit8 v5, v0, 0x1

    move v4, v1

    .line 136
    :goto_0
    if-ge v4, v5, :cond_7

    .line 137
    invoke-virtual {p0, v13}, Lcom/google/obf/bu;->a(I)I

    move-result v0

    .line 138
    packed-switch v0, :pswitch_data_0

    .line 178
    new-instance v1, Lcom/google/obf/s;

    const/16 v2, 0x34

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "floor type greater than 1 not decodable: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v1

    .line 139
    :pswitch_0
    invoke-virtual {p0, v11}, Lcom/google/obf/bu;->b(I)V

    .line 140
    invoke-virtual {p0, v13}, Lcom/google/obf/bu;->b(I)V

    .line 141
    invoke-virtual {p0, v13}, Lcom/google/obf/bu;->b(I)V

    .line 142
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/google/obf/bu;->b(I)V

    .line 143
    invoke-virtual {p0, v11}, Lcom/google/obf/bu;->b(I)V

    .line 144
    invoke-virtual {p0, v12}, Lcom/google/obf/bu;->a(I)I

    move-result v0

    add-int/lit8 v2, v0, 0x1

    move v0, v1

    .line 145
    :goto_1
    if-ge v0, v2, :cond_6

    .line 146
    invoke-virtual {p0, v11}, Lcom/google/obf/bu;->b(I)V

    .line 147
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 149
    :pswitch_1
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/google/obf/bu;->a(I)I

    move-result v6

    .line 150
    const/4 v0, -0x1

    .line 151
    new-array v7, v6, [I

    move v2, v1

    .line 152
    :goto_2
    if-ge v2, v6, :cond_1

    .line 153
    invoke-virtual {p0, v12}, Lcom/google/obf/bu;->a(I)I

    move-result v3

    aput v3, v7, v2

    .line 154
    aget v3, v7, v2

    if-le v3, v0, :cond_0

    .line 155
    aget v0, v7, v2

    .line 156
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 157
    :cond_1
    add-int/lit8 v0, v0, 0x1

    new-array v8, v0, [I

    move v0, v1

    .line 158
    :goto_3
    array-length v2, v8

    if-ge v0, v2, :cond_4

    .line 159
    const/4 v2, 0x3

    invoke-virtual {p0, v2}, Lcom/google/obf/bu;->a(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    aput v2, v8, v0

    .line 160
    invoke-virtual {p0, v14}, Lcom/google/obf/bu;->a(I)I

    move-result v3

    .line 161
    if-lez v3, :cond_2

    .line 162
    invoke-virtual {p0, v11}, Lcom/google/obf/bu;->b(I)V

    :cond_2
    move v2, v1

    .line 163
    :goto_4
    const/4 v9, 0x1

    shl-int/2addr v9, v3

    if-ge v2, v9, :cond_3

    .line 164
    invoke-virtual {p0, v11}, Lcom/google/obf/bu;->b(I)V

    .line 165
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 166
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 167
    :cond_4
    invoke-virtual {p0, v14}, Lcom/google/obf/bu;->b(I)V

    .line 168
    invoke-virtual {p0, v12}, Lcom/google/obf/bu;->a(I)I

    move-result v9

    move v0, v1

    move v2, v1

    move v3, v1

    .line 170
    :goto_5
    if-ge v2, v6, :cond_6

    .line 171
    aget v10, v7, v2

    .line 172
    aget v10, v8, v10

    add-int/2addr v3, v10

    .line 173
    :goto_6
    if-ge v0, v3, :cond_5

    .line 174
    invoke-virtual {p0, v9}, Lcom/google/obf/bu;->b(I)V

    .line 175
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 176
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 179
    :cond_6
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto/16 :goto_0

    .line 180
    :cond_7
    return-void

    .line 138
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static d(Lcom/google/obf/bu;)Lcom/google/obf/bw$a;
    .locals 15
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/s;
        }
    .end annotation

    .prologue
    const/4 v14, 0x4

    const/4 v13, 0x2

    const/4 v8, 0x5

    const/4 v12, 0x1

    const/4 v0, 0x0

    .line 181
    const/16 v1, 0x18

    invoke-virtual {p0, v1}, Lcom/google/obf/bu;->a(I)I

    move-result v1

    const v2, 0x564342

    if-eq v1, v2, :cond_0

    .line 182
    new-instance v0, Lcom/google/obf/s;

    .line 183
    invoke-virtual {p0}, Lcom/google/obf/bu;->b()I

    move-result v1

    const/16 v2, 0x42

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "expected code book to start with [0x56, 0x43, 0x42] at "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v0

    .line 184
    :cond_0
    const/16 v1, 0x10

    invoke-virtual {p0, v1}, Lcom/google/obf/bu;->a(I)I

    move-result v1

    .line 185
    const/16 v2, 0x18

    invoke-virtual {p0, v2}, Lcom/google/obf/bu;->a(I)I

    move-result v2

    .line 186
    new-array v3, v2, [J

    .line 187
    invoke-virtual {p0}, Lcom/google/obf/bu;->a()Z

    move-result v5

    .line 188
    if-nez v5, :cond_3

    .line 189
    invoke-virtual {p0}, Lcom/google/obf/bu;->a()Z

    move-result v4

    .line 190
    :goto_0
    array-length v6, v3

    if-ge v0, v6, :cond_5

    .line 191
    if-eqz v4, :cond_2

    .line 192
    invoke-virtual {p0}, Lcom/google/obf/bu;->a()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 193
    invoke-virtual {p0, v8}, Lcom/google/obf/bu;->a(I)I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    int-to-long v6, v6

    aput-wide v6, v3, v0

    .line 196
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 194
    :cond_1
    const-wide/16 v6, 0x0

    aput-wide v6, v3, v0

    goto :goto_1

    .line 195
    :cond_2
    invoke-virtual {p0, v8}, Lcom/google/obf/bu;->a(I)I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    int-to-long v6, v6

    aput-wide v6, v3, v0

    goto :goto_1

    .line 198
    :cond_3
    invoke-virtual {p0, v8}, Lcom/google/obf/bu;->a(I)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    move v6, v4

    move v4, v0

    .line 199
    :goto_2
    array-length v7, v3

    if-ge v4, v7, :cond_5

    .line 200
    sub-int v7, v2, v4

    invoke-static {v7}, Lcom/google/obf/bw;->a(I)I

    move-result v7

    invoke-virtual {p0, v7}, Lcom/google/obf/bu;->a(I)I

    move-result v9

    move v7, v0

    .line 201
    :goto_3
    if-ge v7, v9, :cond_4

    array-length v8, v3

    if-ge v4, v8, :cond_4

    .line 202
    int-to-long v10, v6

    aput-wide v10, v3, v4

    .line 203
    add-int/lit8 v8, v4, 0x1

    add-int/lit8 v4, v7, 0x1

    move v7, v4

    move v4, v8

    goto :goto_3

    .line 204
    :cond_4
    add-int/lit8 v6, v6, 0x1

    .line 205
    goto :goto_2

    .line 206
    :cond_5
    invoke-virtual {p0, v14}, Lcom/google/obf/bu;->a(I)I

    move-result v4

    .line 207
    if-le v4, v13, :cond_6

    .line 208
    new-instance v0, Lcom/google/obf/s;

    const/16 v1, 0x35

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "lookup type greater than 2 not decodable: "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v0

    .line 209
    :cond_6
    if-eq v4, v12, :cond_7

    if-ne v4, v13, :cond_8

    .line 210
    :cond_7
    const/16 v0, 0x20

    invoke-virtual {p0, v0}, Lcom/google/obf/bu;->b(I)V

    .line 211
    const/16 v0, 0x20

    invoke-virtual {p0, v0}, Lcom/google/obf/bu;->b(I)V

    .line 212
    invoke-virtual {p0, v14}, Lcom/google/obf/bu;->a(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 213
    invoke-virtual {p0, v12}, Lcom/google/obf/bu;->b(I)V

    .line 214
    if-ne v4, v12, :cond_a

    .line 215
    if-eqz v1, :cond_9

    .line 216
    int-to-long v6, v2

    int-to-long v8, v1

    invoke-static {v6, v7, v8, v9}, Lcom/google/obf/bw;->a(JJ)J

    move-result-wide v6

    .line 219
    :goto_4
    int-to-long v8, v0

    mul-long/2addr v6, v8

    long-to-int v0, v6

    invoke-virtual {p0, v0}, Lcom/google/obf/bu;->b(I)V

    .line 220
    :cond_8
    new-instance v0, Lcom/google/obf/bw$a;

    invoke-direct/range {v0 .. v5}, Lcom/google/obf/bw$a;-><init>(II[JIZ)V

    return-object v0

    .line 217
    :cond_9
    const-wide/16 v6, 0x0

    goto :goto_4

    .line 218
    :cond_a
    mul-int v6, v2, v1

    int-to-long v6, v6

    goto :goto_4
.end method
