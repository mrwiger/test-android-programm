.class Lcom/yandex/metrica/impl/ob/cr$a;
.super Ljava/lang/Thread;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/yandex/metrica/impl/ob/cr;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/yandex/metrica/impl/ob/cr;


# direct methods
.method public constructor <init>(Lcom/yandex/metrica/impl/ob/cr;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 66
    iput-object p1, p0, Lcom/yandex/metrica/impl/ob/cr$a;->a:Lcom/yandex/metrica/impl/ob/cr;

    .line 67
    invoke-direct {p0, p2}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 68
    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 72
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/cr$a;->a:Lcom/yandex/metrica/impl/ob/cr;

    invoke-static {v0}, Lcom/yandex/metrica/impl/ob/cr;->a(Lcom/yandex/metrica/impl/ob/cr;)Ljava/util/Map;

    move-result-object v1

    monitor-enter v1

    .line 73
    :try_start_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/cr$a;->a:Lcom/yandex/metrica/impl/ob/cr;

    invoke-static {v0}, Lcom/yandex/metrica/impl/ob/cr;->b(Lcom/yandex/metrica/impl/ob/cr;)V

    .line 74
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/cr$a;->a:Lcom/yandex/metrica/impl/ob/cr;

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/yandex/metrica/impl/ob/cr;->a(Lcom/yandex/metrica/impl/ob/cr;Z)Z

    .line 75
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/cr$a;->a:Lcom/yandex/metrica/impl/ob/cr;

    invoke-static {v0}, Lcom/yandex/metrica/impl/ob/cr;->a(Lcom/yandex/metrica/impl/ob/cr;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 76
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 79
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/cr$a;->isInterrupted()Z

    move-result v0

    if-nez v0, :cond_2

    .line 80
    monitor-enter p0

    .line 81
    :try_start_1
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/cr$a;->a:Lcom/yandex/metrica/impl/ob/cr;

    invoke-static {v0}, Lcom/yandex/metrica/impl/ob/cr;->c(Lcom/yandex/metrica/impl/ob/cr;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->size()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    if-nez v0, :cond_1

    .line 83
    :try_start_2
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 88
    :cond_1
    :goto_1
    :try_start_3
    new-instance v0, Ljava/util/HashMap;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/cr$a;->a:Lcom/yandex/metrica/impl/ob/cr;

    invoke-static {v1}, Lcom/yandex/metrica/impl/ob/cr;->c(Lcom/yandex/metrica/impl/ob/cr;)Ljava/util/Map;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 89
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/cr$a;->a:Lcom/yandex/metrica/impl/ob/cr;

    invoke-static {v1}, Lcom/yandex/metrica/impl/ob/cr;->c(Lcom/yandex/metrica/impl/ob/cr;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 90
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 92
    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 93
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/cr$a;->a:Lcom/yandex/metrica/impl/ob/cr;

    invoke-static {v1, v0}, Lcom/yandex/metrica/impl/ob/cr;->a(Lcom/yandex/metrica/impl/ob/cr;Ljava/util/Map;)V

    .line 94
    invoke-interface {v0}, Ljava/util/Map;->clear()V

    goto :goto_0

    .line 76
    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v0

    .line 85
    :catch_0
    move-exception v0

    :try_start_5
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/cr$a;->interrupt()V

    goto :goto_1

    .line 90
    :catchall_1
    move-exception v0

    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v0

    .line 97
    :cond_2
    return-void
.end method
