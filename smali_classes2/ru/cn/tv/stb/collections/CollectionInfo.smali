.class Lru/cn/tv/stb/collections/CollectionInfo;
.super Ljava/lang/Object;
.source "CollectionInfo.java"


# instance fields
.field private elements:Landroid/database/Cursor;

.field private final rubric:Lru/cn/api/catalogue/replies/Rubric;

.field private updateConsumer:Lio/reactivex/functions/Consumer;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/functions/Consumer",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lru/cn/api/catalogue/replies/Rubric;)V
    .locals 0
    .param p1, "rubric"    # Lru/cn/api/catalogue/replies/Rubric;

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lru/cn/tv/stb/collections/CollectionInfo;->rubric:Lru/cn/api/catalogue/replies/Rubric;

    .line 17
    return-void
.end method


# virtual methods
.method public getElements()Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lru/cn/tv/stb/collections/CollectionInfo;->elements:Landroid/database/Cursor;

    return-object v0
.end method

.method public getRubric()Lru/cn/api/catalogue/replies/Rubric;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lru/cn/tv/stb/collections/CollectionInfo;->rubric:Lru/cn/api/catalogue/replies/Rubric;

    return-object v0
.end method

.method public setElements(Landroid/database/Cursor;)V
    .locals 3
    .param p1, "elements"    # Landroid/database/Cursor;

    .prologue
    .line 20
    iget-object v2, p0, Lru/cn/tv/stb/collections/CollectionInfo;->elements:Landroid/database/Cursor;

    if-eqz v2, :cond_1

    const/4 v1, 0x1

    .line 22
    .local v1, "notify":Z
    :goto_0
    iput-object p1, p0, Lru/cn/tv/stb/collections/CollectionInfo;->elements:Landroid/database/Cursor;

    .line 25
    if-eqz v1, :cond_0

    .line 27
    :try_start_0
    iget-object v2, p0, Lru/cn/tv/stb/collections/CollectionInfo;->updateConsumer:Lio/reactivex/functions/Consumer;

    invoke-interface {v2, p1}, Lio/reactivex/functions/Consumer;->accept(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 32
    :cond_0
    :goto_1
    return-void

    .line 20
    .end local v1    # "notify":Z
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 28
    .restart local v1    # "notify":Z
    :catch_0
    move-exception v0

    .line 29
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {v0}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public setOnUpdate(Lio/reactivex/functions/Consumer;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/reactivex/functions/Consumer",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 39
    .local p1, "updateConsumer":Lio/reactivex/functions/Consumer;, "Lio/reactivex/functions/Consumer<Landroid/database/Cursor;>;"
    iput-object p1, p0, Lru/cn/tv/stb/collections/CollectionInfo;->updateConsumer:Lio/reactivex/functions/Consumer;

    .line 40
    return-void
.end method
