.class public Lcom/yandex/metrica/impl/utils/h;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/yandex/metrica/impl/utils/h$a;
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Landroid/net/LocalServerSocket;

.field private c:Lcom/yandex/metrica/impl/ob/fr;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 43
    new-instance v0, Lcom/yandex/metrica/impl/ob/fp;

    const/16 v1, 0xc

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fp;-><init>(I)V

    invoke-direct {p0, p1, v0}, Lcom/yandex/metrica/impl/utils/h;-><init>(Ljava/lang/String;Lcom/yandex/metrica/impl/ob/fr;)V

    .line 44
    return-void
.end method

.method constructor <init>(Ljava/lang/String;Lcom/yandex/metrica/impl/ob/fr;)V
    .locals 3

    .prologue
    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    iput-object p1, p0, Lcom/yandex/metrica/impl/utils/h;->a:Ljava/lang/String;

    .line 96
    const-string v0, "[LocalSocketLock:%s]"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 97
    iput-object p2, p0, Lcom/yandex/metrica/impl/utils/h;->c:Lcom/yandex/metrica/impl/ob/fr;

    .line 98
    return-void
.end method


# virtual methods
.method public a(Lcom/yandex/metrica/impl/ob/fr;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/yandex/metrica/impl/utils/h$a;
        }
    .end annotation

    .prologue
    .line 79
    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/yandex/metrica/impl/utils/h;->c:Lcom/yandex/metrica/impl/ob/fr;

    .line 83
    :cond_0
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/utils/h;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 84
    return-void

    .line 86
    :cond_1
    invoke-interface {p1}, Lcom/yandex/metrica/impl/ob/fr;->a()V

    .line 87
    invoke-interface {p1}, Lcom/yandex/metrica/impl/ob/fr;->c()V

    .line 89
    invoke-interface {p1}, Lcom/yandex/metrica/impl/ob/fr;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 91
    new-instance v0, Lcom/yandex/metrica/impl/utils/h$a;

    invoke-direct {v0}, Lcom/yandex/metrica/impl/utils/h$a;-><init>()V

    throw v0
.end method

.method public a()Z
    .locals 2

    .prologue
    .line 49
    :try_start_0
    new-instance v0, Landroid/net/LocalServerSocket;

    iget-object v1, p0, Lcom/yandex/metrica/impl/utils/h;->a:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/net/LocalServerSocket;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/utils/h;->b:Landroid/net/LocalServerSocket;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 51
    const/4 v0, 0x1

    .line 54
    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/yandex/metrica/impl/utils/h;->b:Landroid/net/LocalServerSocket;

    if-eqz v0, :cond_0

    .line 62
    :try_start_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/utils/h;->b:Landroid/net/LocalServerSocket;

    invoke-virtual {v0}, Landroid/net/LocalServerSocket;->close()V

    .line 63
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/yandex/metrica/impl/utils/h;->b:Landroid/net/LocalServerSocket;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 68
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public c()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/yandex/metrica/impl/utils/h$a;
        }
    .end annotation

    .prologue
    .line 71
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/utils/h;->a(Lcom/yandex/metrica/impl/ob/fr;)V

    .line 72
    return-void
.end method
