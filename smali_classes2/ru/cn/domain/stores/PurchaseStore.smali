.class public abstract Lru/cn/domain/stores/PurchaseStore;
.super Ljava/lang/Object;
.source "PurchaseStore.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/domain/stores/PurchaseStore$OnPurchaseFinishListener;,
        Lru/cn/domain/stores/PurchaseStore$Status;
    }
.end annotation


# instance fields
.field final LOG_TAG:Ljava/lang/String;

.field final context:Landroid/content/Context;

.field final persistor:Lru/cn/domain/stores/PurchasePersistor;

.field status:Lru/cn/domain/stores/PurchaseStore$Status;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lru/cn/domain/stores/PurchaseStore;->LOG_TAG:Ljava/lang/String;

    .line 29
    sget-object v0, Lru/cn/domain/stores/PurchaseStore$Status;->UNKNOWN:Lru/cn/domain/stores/PurchaseStore$Status;

    iput-object v0, p0, Lru/cn/domain/stores/PurchaseStore;->status:Lru/cn/domain/stores/PurchaseStore$Status;

    .line 32
    iput-object p1, p0, Lru/cn/domain/stores/PurchaseStore;->context:Landroid/content/Context;

    .line 33
    new-instance v0, Lru/cn/domain/stores/PurchasePersistor;

    invoke-virtual {p0}, Lru/cn/domain/stores/PurchaseStore;->storeName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lru/cn/domain/stores/PurchasePersistor;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lru/cn/domain/stores/PurchaseStore;->persistor:Lru/cn/domain/stores/PurchasePersistor;

    .line 34
    return-void
.end method


# virtual methods
.method public handleResult(IILandroid/content/Intent;)Z
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 39
    const/4 v0, 0x0

    return v0
.end method

.method public hasPurchase(Ljava/lang/String;)Z
    .locals 1
    .param p1, "productId"    # Ljava/lang/String;

    .prologue
    .line 51
    iget-object v0, p0, Lru/cn/domain/stores/PurchaseStore;->persistor:Lru/cn/domain/stores/PurchasePersistor;

    invoke-virtual {v0, p1}, Lru/cn/domain/stores/PurchasePersistor;->hasPurchase(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public abstract purchase(Landroid/app/Activity;Ljava/lang/String;Lru/cn/domain/stores/PurchaseStore$OnPurchaseFinishListener;)V
.end method

.method savePurchaseAsync(ILru/cn/domain/stores/iabhelper/Purchase;Lru/cn/domain/stores/PurchaseStore$OnPurchaseFinishListener;)V
    .locals 2
    .param p1, "resultCode"    # I
    .param p2, "info"    # Lru/cn/domain/stores/iabhelper/Purchase;
    .param p3, "listener"    # Lru/cn/domain/stores/PurchaseStore$OnPurchaseFinishListener;

    .prologue
    .line 56
    new-instance v0, Lru/cn/domain/stores/PurchaseStore$1;

    invoke-direct {v0, p0, p2, p3, p1}, Lru/cn/domain/stores/PurchaseStore$1;-><init>(Lru/cn/domain/stores/PurchaseStore;Lru/cn/domain/stores/iabhelper/Purchase;Lru/cn/domain/stores/PurchaseStore$OnPurchaseFinishListener;I)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    .line 71
    invoke-virtual {v0, v1}, Lru/cn/domain/stores/PurchaseStore$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 72
    return-void
.end method

.method public storeName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    return-object v0
.end method
