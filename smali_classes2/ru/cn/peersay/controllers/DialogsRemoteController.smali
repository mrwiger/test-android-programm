.class public Lru/cn/peersay/controllers/DialogsRemoteController;
.super Ljava/lang/Object;
.source "DialogsRemoteController.java"

# interfaces
.implements Lru/cn/peersay/controllers/RemoteIntentController;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/peersay/controllers/DialogsRemoteController$DialogTag;,
        Lru/cn/peersay/controllers/DialogsRemoteController$DismissListener;
    }
.end annotation


# static fields
.field private static final instance:Lru/cn/peersay/controllers/DialogsRemoteController;


# instance fields
.field private idSequence:J

.field private registeredTags:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lru/cn/peersay/controllers/DialogsRemoteController$DialogTag;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    new-instance v0, Lru/cn/peersay/controllers/DialogsRemoteController;

    invoke-direct {v0}, Lru/cn/peersay/controllers/DialogsRemoteController;-><init>()V

    sput-object v0, Lru/cn/peersay/controllers/DialogsRemoteController;->instance:Lru/cn/peersay/controllers/DialogsRemoteController;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lru/cn/peersay/controllers/DialogsRemoteController;->registeredTags:Ljava/util/HashMap;

    .line 41
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/32 v2, 0x186a0

    rem-long/2addr v0, v2

    const-wide/16 v2, 0x64

    div-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lru/cn/peersay/controllers/DialogsRemoteController;->idSequence:J

    return-void
.end method

.method private findTag(J)Lru/cn/peersay/controllers/DialogsRemoteController$DialogTag;
    .locals 5
    .param p1, "dialogId"    # J

    .prologue
    .line 92
    iget-object v1, p0, Lru/cn/peersay/controllers/DialogsRemoteController;->registeredTags:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/peersay/controllers/DialogsRemoteController$DialogTag;

    .line 93
    .local v0, "tag":Lru/cn/peersay/controllers/DialogsRemoteController$DialogTag;
    iget-wide v2, v0, Lru/cn/peersay/controllers/DialogsRemoteController$DialogTag;->id:J

    cmp-long v2, v2, p1

    if-nez v2, :cond_0

    .line 98
    .end local v0    # "tag":Lru/cn/peersay/controllers/DialogsRemoteController$DialogTag;
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static sharedInstance()Lru/cn/peersay/controllers/DialogsRemoteController;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lru/cn/peersay/controllers/DialogsRemoteController;->instance:Lru/cn/peersay/controllers/DialogsRemoteController;

    return-object v0
.end method


# virtual methods
.method public handleMessage(Landroid/content/Context;Lru/cn/peersay/IntentMessage;)Z
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "message"    # Lru/cn/peersay/IntentMessage;

    .prologue
    const/4 v6, 0x1

    const/4 v8, -0x1

    const/4 v5, 0x0

    .line 105
    iget-object v7, p2, Lru/cn/peersay/IntentMessage;->extras:Landroid/os/Bundle;

    if-nez v7, :cond_1

    .line 141
    :cond_0
    :goto_0
    return v5

    .line 108
    :cond_1
    iget-object v7, p2, Lru/cn/peersay/IntentMessage;->extras:Landroid/os/Bundle;

    const-string v9, "command"

    invoke-virtual {v7, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 109
    .local v0, "command":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 114
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v7

    sparse-switch v7, :sswitch_data_0

    :cond_2
    move v7, v8

    :goto_1
    packed-switch v7, :pswitch_data_0

    goto :goto_0

    .line 116
    :pswitch_0
    iget-object v7, p2, Lru/cn/peersay/IntentMessage;->extras:Landroid/os/Bundle;

    const-string v8, "dialogId"

    invoke-virtual {v7, v8}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 117
    .local v2, "dialogId":J
    invoke-direct {p0, v2, v3}, Lru/cn/peersay/controllers/DialogsRemoteController;->findTag(J)Lru/cn/peersay/controllers/DialogsRemoteController$DialogTag;

    move-result-object v4

    .line 118
    .local v4, "tag":Lru/cn/peersay/controllers/DialogsRemoteController$DialogTag;
    if-eqz v4, :cond_0

    .line 121
    iget-object v5, p0, Lru/cn/peersay/controllers/DialogsRemoteController;->registeredTags:Ljava/util/HashMap;

    iget-object v7, v4, Lru/cn/peersay/controllers/DialogsRemoteController$DialogTag;->tagName:Ljava/lang/String;

    invoke-virtual {v5, v7}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    iget-object v5, p2, Lru/cn/peersay/IntentMessage;->extras:Landroid/os/Bundle;

    const-string v7, "optionIndex"

    invoke-virtual {v5, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 124
    .local v1, "index":I
    iget-object v5, v4, Lru/cn/peersay/controllers/DialogsRemoteController$DialogTag;->listener:Lru/cn/peersay/controllers/DialogsRemoteController$DismissListener;

    invoke-interface {v5, v1}, Lru/cn/peersay/controllers/DialogsRemoteController$DismissListener;->onDismissed(I)V

    .end local v1    # "index":I
    :goto_2
    move v5, v6

    .line 141
    goto :goto_0

    .line 114
    .end local v2    # "dialogId":J
    .end local v4    # "tag":Lru/cn/peersay/controllers/DialogsRemoteController$DialogTag;
    :sswitch_0
    const-string v7, "tv.dialog.select"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    move v7, v5

    goto :goto_1

    :sswitch_1
    const-string v7, "tv.dialog.cancel"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    move v7, v6

    goto :goto_1

    .line 128
    :pswitch_1
    iget-object v7, p2, Lru/cn/peersay/IntentMessage;->extras:Landroid/os/Bundle;

    const-string v9, "dialogId"

    invoke-virtual {v7, v9}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 129
    .restart local v2    # "dialogId":J
    invoke-direct {p0, v2, v3}, Lru/cn/peersay/controllers/DialogsRemoteController;->findTag(J)Lru/cn/peersay/controllers/DialogsRemoteController$DialogTag;

    move-result-object v4

    .line 130
    .restart local v4    # "tag":Lru/cn/peersay/controllers/DialogsRemoteController$DialogTag;
    if-eqz v4, :cond_0

    .line 133
    iget-object v5, p0, Lru/cn/peersay/controllers/DialogsRemoteController;->registeredTags:Ljava/util/HashMap;

    iget-object v7, v4, Lru/cn/peersay/controllers/DialogsRemoteController$DialogTag;->tagName:Ljava/lang/String;

    invoke-virtual {v5, v7}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 134
    iget-object v5, v4, Lru/cn/peersay/controllers/DialogsRemoteController$DialogTag;->listener:Lru/cn/peersay/controllers/DialogsRemoteController$DismissListener;

    invoke-interface {v5, v8}, Lru/cn/peersay/controllers/DialogsRemoteController$DismissListener;->onDismissed(I)V

    goto :goto_2

    .line 114
    nop

    :sswitch_data_0
    .sparse-switch
        0x48e29374 -> :sswitch_1
        0x646799f6 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onDialogDismiss(Landroid/content/Context;Ljava/lang/String;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "tagName"    # Ljava/lang/String;

    .prologue
    .line 73
    invoke-static {p1}, Lru/cn/peersay/RemoteCommandReceiver;->isPeersayInstalled(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 89
    :cond_0
    :goto_0
    return-void

    .line 76
    :cond_1
    iget-object v3, p0, Lru/cn/peersay/controllers/DialogsRemoteController;->registeredTags:Ljava/util/HashMap;

    invoke-virtual {v3, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lru/cn/peersay/controllers/DialogsRemoteController$DialogTag;

    .line 77
    .local v2, "tag":Lru/cn/peersay/controllers/DialogsRemoteController$DialogTag;
    if-eqz v2, :cond_0

    .line 80
    iget-object v3, p0, Lru/cn/peersay/controllers/DialogsRemoteController;->registeredTags:Ljava/util/HashMap;

    invoke-virtual {v3, p2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 83
    .local v1, "info":Landroid/os/Bundle;
    const-string v3, "event"

    const-string v4, "tv.dialog.hidden"

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    const-string v3, "dialogId"

    iget-wide v4, v2, Lru/cn/peersay/controllers/DialogsRemoteController$DialogTag;->id:J

    invoke-virtual {v1, v3, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 85
    invoke-static {v1}, Lru/cn/peersay/IntentMessage;->createEventIntent(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    .line 86
    .local v0, "event":Landroid/content/Intent;
    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 88
    const-string v3, "DialogsRemoteController"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "event sent "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public registerDialog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lru/cn/peersay/controllers/DialogsRemoteController$DismissListener;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "tagName"    # Ljava/lang/String;
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "text"    # Ljava/lang/String;
    .param p6, "listener"    # Lru/cn/peersay/controllers/DialogsRemoteController$DismissListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lru/cn/peersay/controllers/DialogsRemoteController$DismissListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 52
    .local p5, "options":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Lru/cn/peersay/RemoteCommandReceiver;->isPeersayInstalled(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 70
    :goto_0
    return-void

    .line 55
    :cond_0
    iget-wide v4, p0, Lru/cn/peersay/controllers/DialogsRemoteController;->idSequence:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, p0, Lru/cn/peersay/controllers/DialogsRemoteController;->idSequence:J

    .line 56
    new-instance v2, Lru/cn/peersay/controllers/DialogsRemoteController$DialogTag;

    iget-wide v4, p0, Lru/cn/peersay/controllers/DialogsRemoteController;->idSequence:J

    invoke-direct {v2, v4, v5, p2, p6}, Lru/cn/peersay/controllers/DialogsRemoteController$DialogTag;-><init>(JLjava/lang/String;Lru/cn/peersay/controllers/DialogsRemoteController$DismissListener;)V

    .line 57
    .local v2, "tag":Lru/cn/peersay/controllers/DialogsRemoteController$DialogTag;
    iget-object v3, p0, Lru/cn/peersay/controllers/DialogsRemoteController;->registeredTags:Ljava/util/HashMap;

    invoke-virtual {v3, p2, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 60
    .local v1, "info":Landroid/os/Bundle;
    const-string v3, "event"

    const-string v4, "tv.dialog.shown"

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    const-string v3, "dialogId"

    iget-wide v4, v2, Lru/cn/peersay/controllers/DialogsRemoteController$DialogTag;->id:J

    invoke-virtual {v1, v3, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 62
    const-string v3, "title"

    invoke-virtual {v1, v3, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    const-string v3, "text"

    invoke-virtual {v1, v3, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    const-string v3, "options"

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, p5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 65
    const-string v3, "cancelable"

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 66
    invoke-static {v1}, Lru/cn/peersay/IntentMessage;->createEventIntent(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    .line 67
    .local v0, "event":Landroid/content/Intent;
    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 69
    const-string v3, "DialogsRemoteController"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "event sent "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
