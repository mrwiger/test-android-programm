.class public Lru/cn/api/provider/TvContentProviderContract$Helper;
.super Ljava/lang/Object;
.source "TvContentProviderContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/api/provider/TvContentProviderContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Helper"
.end annotation


# direct methods
.method public static getRubricDescription(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 2
    .param p0, "c"    # Landroid/database/Cursor;

    .prologue
    .line 279
    sget-object v1, Lru/cn/api/provider/TvContentProviderContract$RubricColumn;->description:Lru/cn/api/provider/TvContentProviderContract$RubricColumn;

    invoke-virtual {v1}, Lru/cn/api/provider/TvContentProviderContract$RubricColumn;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 280
    .local v0, "index":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 281
    const/4 v1, 0x0

    .line 283
    :goto_0
    return-object v1

    :cond_0
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public static getRubricId(Landroid/database/Cursor;)J
    .locals 2
    .param p0, "c"    # Landroid/database/Cursor;

    .prologue
    .line 271
    sget-object v0, Lru/cn/api/provider/TvContentProviderContract$RubricColumn;->_id:Lru/cn/api/provider/TvContentProviderContract$RubricColumn;

    invoke-virtual {v0}, Lru/cn/api/provider/TvContentProviderContract$RubricColumn;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public static getRubricTitle(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p0, "c"    # Landroid/database/Cursor;

    .prologue
    .line 275
    sget-object v0, Lru/cn/api/provider/TvContentProviderContract$RubricColumn;->title:Lru/cn/api/provider/TvContentProviderContract$RubricColumn;

    invoke-virtual {v0}, Lru/cn/api/provider/TvContentProviderContract$RubricColumn;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getRubricUiHint(Landroid/database/Cursor;)Lru/cn/api/catalogue/replies/Rubric$UiHintType;
    .locals 6
    .param p0, "c"    # Landroid/database/Cursor;

    .prologue
    .line 287
    sget-object v3, Lru/cn/api/provider/TvContentProviderContract$RubricColumn;->ui_hint:Lru/cn/api/provider/TvContentProviderContract$RubricColumn;

    .line 288
    invoke-virtual {v3}, Lru/cn/api/provider/TvContentProviderContract$RubricColumn;->toString()Ljava/lang/String;

    move-result-object v3

    .line 287
    invoke-interface {p0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 289
    .local v2, "uiHintValue":I
    if-lez v2, :cond_1

    .line 290
    invoke-static {}, Lru/cn/api/catalogue/replies/Rubric$UiHintType;->values()[Lru/cn/api/catalogue/replies/Rubric$UiHintType;

    move-result-object v0

    .line 291
    .local v0, "types":[Lru/cn/api/catalogue/replies/Rubric$UiHintType;
    array-length v4, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v1, v0, v3

    .line 292
    .local v1, "uiHintType":Lru/cn/api/catalogue/replies/Rubric$UiHintType;
    invoke-virtual {v1}, Lru/cn/api/catalogue/replies/Rubric$UiHintType;->getValue()I

    move-result v5

    if-ne v5, v2, :cond_0

    .line 296
    .end local v0    # "types":[Lru/cn/api/catalogue/replies/Rubric$UiHintType;
    .end local v1    # "uiHintType":Lru/cn/api/catalogue/replies/Rubric$UiHintType;
    :goto_1
    return-object v1

    .line 291
    .restart local v0    # "types":[Lru/cn/api/catalogue/replies/Rubric$UiHintType;
    .restart local v1    # "uiHintType":Lru/cn/api/catalogue/replies/Rubric$UiHintType;
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 296
    .end local v0    # "types":[Lru/cn/api/catalogue/replies/Rubric$UiHintType;
    .end local v1    # "uiHintType":Lru/cn/api/catalogue/replies/Rubric$UiHintType;
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method
