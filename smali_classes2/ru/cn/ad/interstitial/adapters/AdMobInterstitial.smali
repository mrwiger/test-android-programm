.class public Lru/cn/ad/interstitial/adapters/AdMobInterstitial;
.super Lru/cn/ad/AdAdapter;
.source "AdMobInterstitial.java"


# instance fields
.field private final bannerId:Ljava/lang/String;

.field private interstitial:Lcom/google/android/gms/ads/InterstitialAd;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lru/cn/api/money_miner/replies/AdSystem;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "adSystem"    # Lru/cn/api/money_miner/replies/AdSystem;

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Lru/cn/ad/AdAdapter;-><init>(Landroid/content/Context;Lru/cn/api/money_miner/replies/AdSystem;)V

    .line 24
    const-string v0, "banner_id"

    invoke-virtual {p2, v0}, Lru/cn/api/money_miner/replies/AdSystem;->getParamOrThrow(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lru/cn/ad/interstitial/adapters/AdMobInterstitial;->bannerId:Ljava/lang/String;

    .line 25
    return-void
.end method

.method static synthetic access$000(Lru/cn/ad/interstitial/adapters/AdMobInterstitial;)Lru/cn/ad/AdAdapter$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/interstitial/adapters/AdMobInterstitial;

    .prologue
    .line 16
    iget-object v0, p0, Lru/cn/ad/interstitial/adapters/AdMobInterstitial;->listener:Lru/cn/ad/AdAdapter$Listener;

    return-object v0
.end method

.method static synthetic access$100(Lru/cn/ad/interstitial/adapters/AdMobInterstitial;)Lru/cn/ad/AdAdapter$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/interstitial/adapters/AdMobInterstitial;

    .prologue
    .line 16
    iget-object v0, p0, Lru/cn/ad/interstitial/adapters/AdMobInterstitial;->listener:Lru/cn/ad/AdAdapter$Listener;

    return-object v0
.end method

.method static synthetic access$200(Lru/cn/ad/interstitial/adapters/AdMobInterstitial;)Lru/cn/ad/AdAdapter$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/interstitial/adapters/AdMobInterstitial;

    .prologue
    .line 16
    iget-object v0, p0, Lru/cn/ad/interstitial/adapters/AdMobInterstitial;->listener:Lru/cn/ad/AdAdapter$Listener;

    return-object v0
.end method

.method static synthetic access$300(Lru/cn/ad/interstitial/adapters/AdMobInterstitial;)Lru/cn/ad/AdAdapter$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/interstitial/adapters/AdMobInterstitial;

    .prologue
    .line 16
    iget-object v0, p0, Lru/cn/ad/interstitial/adapters/AdMobInterstitial;->listener:Lru/cn/ad/AdAdapter$Listener;

    return-object v0
.end method

.method static synthetic access$400(Lru/cn/ad/interstitial/adapters/AdMobInterstitial;)Lru/cn/ad/AdAdapter$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/interstitial/adapters/AdMobInterstitial;

    .prologue
    .line 16
    iget-object v0, p0, Lru/cn/ad/interstitial/adapters/AdMobInterstitial;->listener:Lru/cn/ad/AdAdapter$Listener;

    return-object v0
.end method

.method static synthetic access$500(Lru/cn/ad/interstitial/adapters/AdMobInterstitial;)Lru/cn/ad/AdAdapter$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/interstitial/adapters/AdMobInterstitial;

    .prologue
    .line 16
    iget-object v0, p0, Lru/cn/ad/interstitial/adapters/AdMobInterstitial;->listener:Lru/cn/ad/AdAdapter$Listener;

    return-object v0
.end method


# virtual methods
.method public destroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 88
    iget-object v0, p0, Lru/cn/ad/interstitial/adapters/AdMobInterstitial;->interstitial:Lcom/google/android/gms/ads/InterstitialAd;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/InterstitialAd;->setAdListener(Lcom/google/android/gms/ads/AdListener;)V

    .line 89
    iput-object v1, p0, Lru/cn/ad/interstitial/adapters/AdMobInterstitial;->interstitial:Lcom/google/android/gms/ads/InterstitialAd;

    .line 90
    return-void
.end method

.method protected onLoad()V
    .locals 4

    .prologue
    .line 29
    new-instance v2, Lcom/google/android/gms/ads/InterstitialAd;

    iget-object v3, p0, Lru/cn/ad/interstitial/adapters/AdMobInterstitial;->context:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/google/android/gms/ads/InterstitialAd;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lru/cn/ad/interstitial/adapters/AdMobInterstitial;->interstitial:Lcom/google/android/gms/ads/InterstitialAd;

    .line 30
    iget-object v2, p0, Lru/cn/ad/interstitial/adapters/AdMobInterstitial;->interstitial:Lcom/google/android/gms/ads/InterstitialAd;

    iget-object v3, p0, Lru/cn/ad/interstitial/adapters/AdMobInterstitial;->bannerId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/ads/InterstitialAd;->setAdUnitId(Ljava/lang/String;)V

    .line 31
    iget-object v2, p0, Lru/cn/ad/interstitial/adapters/AdMobInterstitial;->interstitial:Lcom/google/android/gms/ads/InterstitialAd;

    new-instance v3, Lru/cn/ad/interstitial/adapters/AdMobInterstitial$1;

    invoke-direct {v3, p0}, Lru/cn/ad/interstitial/adapters/AdMobInterstitial$1;-><init>(Lru/cn/ad/interstitial/adapters/AdMobInterstitial;)V

    invoke-virtual {v2, v3}, Lcom/google/android/gms/ads/InterstitialAd;->setAdListener(Lcom/google/android/gms/ads/AdListener;)V

    .line 70
    new-instance v1, Lcom/google/android/gms/ads/AdRequest$Builder;

    invoke-direct {v1}, Lcom/google/android/gms/ads/AdRequest$Builder;-><init>()V

    .line 71
    .local v1, "adRequestBuilder":Lcom/google/android/gms/ads/AdRequest$Builder;
    iget-object v2, p0, Lru/cn/ad/interstitial/adapters/AdMobInterstitial;->context:Landroid/content/Context;

    invoke-static {v2}, Lru/cn/ad/AdsManager;->getDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 72
    iget-object v2, p0, Lru/cn/ad/interstitial/adapters/AdMobInterstitial;->context:Landroid/content/Context;

    invoke-static {v2}, Lru/cn/ad/AdsManager;->getDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/ads/AdRequest$Builder;->addTestDevice(Ljava/lang/String;)Lcom/google/android/gms/ads/AdRequest$Builder;

    .line 75
    :cond_0
    invoke-virtual {v1}, Lcom/google/android/gms/ads/AdRequest$Builder;->build()Lcom/google/android/gms/ads/AdRequest;

    move-result-object v0

    .line 76
    .local v0, "adRequest":Lcom/google/android/gms/ads/AdRequest;
    iget-object v2, p0, Lru/cn/ad/interstitial/adapters/AdMobInterstitial;->interstitial:Lcom/google/android/gms/ads/InterstitialAd;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/ads/InterstitialAd;->loadAd(Lcom/google/android/gms/ads/AdRequest;)V

    .line 77
    return-void
.end method

.method public show()V
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lru/cn/ad/interstitial/adapters/AdMobInterstitial;->interstitial:Lcom/google/android/gms/ads/InterstitialAd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lru/cn/ad/interstitial/adapters/AdMobInterstitial;->interstitial:Lcom/google/android/gms/ads/InterstitialAd;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/InterstitialAd;->isLoaded()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 82
    iget-object v0, p0, Lru/cn/ad/interstitial/adapters/AdMobInterstitial;->interstitial:Lcom/google/android/gms/ads/InterstitialAd;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/InterstitialAd;->show()V

    .line 84
    :cond_0
    return-void
.end method
