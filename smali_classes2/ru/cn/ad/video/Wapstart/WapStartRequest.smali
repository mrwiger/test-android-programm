.class Lru/cn/ad/video/Wapstart/WapStartRequest;
.super Ljava/lang/Object;
.source "WapStartRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/ad/video/Wapstart/WapStartRequest$Site;,
        Lru/cn/ad/video/Wapstart/WapStartRequest$Impression;,
        Lru/cn/ad/video/Wapstart/WapStartRequest$Device;,
        Lru/cn/ad/video/Wapstart/WapStartRequest$VideoType;
    }
.end annotation


# static fields
.field private static wapStartUrl:Ljava/lang/String;


# instance fields
.field private device:Lru/cn/ad/video/Wapstart/WapStartRequest$Device;

.field private impression:Lru/cn/ad/video/Wapstart/WapStartRequest$Impression;

.field private site:Lru/cn/ad/video/Wapstart/WapStartRequest$Site;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-string v0, "http://api.plus1.wapstart.ru/v1/ads/"

    sput-object v0, Lru/cn/ad/video/Wapstart/WapStartRequest;->wapStartUrl:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "siteId"    # I
    .param p3, "ipAddress"    # Ljava/lang/String;

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Lru/cn/ad/video/Wapstart/WapStartRequest$Impression;

    invoke-direct {v0, p0}, Lru/cn/ad/video/Wapstart/WapStartRequest$Impression;-><init>(Lru/cn/ad/video/Wapstart/WapStartRequest;)V

    iput-object v0, p0, Lru/cn/ad/video/Wapstart/WapStartRequest;->impression:Lru/cn/ad/video/Wapstart/WapStartRequest$Impression;

    .line 40
    new-instance v0, Lru/cn/ad/video/Wapstart/WapStartRequest$Device;

    invoke-direct {v0, p0, p1, p3}, Lru/cn/ad/video/Wapstart/WapStartRequest$Device;-><init>(Lru/cn/ad/video/Wapstart/WapStartRequest;Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lru/cn/ad/video/Wapstart/WapStartRequest;->device:Lru/cn/ad/video/Wapstart/WapStartRequest$Device;

    .line 41
    new-instance v0, Lru/cn/ad/video/Wapstart/WapStartRequest$Site;

    invoke-direct {v0, p0, p2}, Lru/cn/ad/video/Wapstart/WapStartRequest$Site;-><init>(Lru/cn/ad/video/Wapstart/WapStartRequest;I)V

    iput-object v0, p0, Lru/cn/ad/video/Wapstart/WapStartRequest;->site:Lru/cn/ad/video/Wapstart/WapStartRequest$Site;

    .line 42
    return-void
.end method


# virtual methods
.method public getRequestUrl()Ljava/lang/String;
    .locals 2

    .prologue
    .line 107
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lru/cn/ad/video/Wapstart/WapStartRequest;->wapStartUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lru/cn/ad/video/Wapstart/WapStartRequest;->site:Lru/cn/ad/video/Wapstart/WapStartRequest$Site;

    iget v1, v1, Lru/cn/ad/video/Wapstart/WapStartRequest$Site;->id:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toJson()Ljava/lang/String;
    .locals 5

    .prologue
    .line 102
    new-instance v1, Lcom/google/gson/GsonBuilder;

    invoke-direct {v1}, Lcom/google/gson/GsonBuilder;-><init>()V

    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v3, 0x0

    const/16 v4, 0x8

    aput v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/google/gson/GsonBuilder;->excludeFieldsWithModifiers([I)Lcom/google/gson/GsonBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/gson/GsonBuilder;->create()Lcom/google/gson/Gson;

    move-result-object v0

    .line 103
    .local v0, "gson":Lcom/google/gson/Gson;
    invoke-virtual {v0, p0}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
