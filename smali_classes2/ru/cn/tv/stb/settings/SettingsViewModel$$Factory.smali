.class public final Lru/cn/tv/stb/settings/SettingsViewModel$$Factory;
.super Ljava/lang/Object;
.source "SettingsViewModel$$Factory.java"

# interfaces
.implements Ltoothpick/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ltoothpick/Factory",
        "<",
        "Lru/cn/tv/stb/settings/SettingsViewModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic createInstance(Ltoothpick/Scope;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 9
    invoke-virtual {p0, p1}, Lru/cn/tv/stb/settings/SettingsViewModel$$Factory;->createInstance(Ltoothpick/Scope;)Lru/cn/tv/stb/settings/SettingsViewModel;

    move-result-object v0

    return-object v0
.end method

.method public createInstance(Ltoothpick/Scope;)Lru/cn/tv/stb/settings/SettingsViewModel;
    .locals 4
    .param p1, "scope"    # Ltoothpick/Scope;

    .prologue
    .line 12
    invoke-virtual {p0, p1}, Lru/cn/tv/stb/settings/SettingsViewModel$$Factory;->getTargetScope(Ltoothpick/Scope;)Ltoothpick/Scope;

    move-result-object p1

    .line 13
    const-class v3, Lru/cn/mvvm/RxLoader;

    invoke-interface {p1, v3}, Ltoothpick/Scope;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/mvvm/RxLoader;

    .line 14
    .local v0, "param1":Lru/cn/mvvm/RxLoader;
    const-class v3, Landroid/content/Context;

    invoke-interface {p1, v3}, Ltoothpick/Scope;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    .line 15
    .local v1, "param2":Landroid/content/Context;
    new-instance v2, Lru/cn/tv/stb/settings/SettingsViewModel;

    invoke-direct {v2, v0, v1}, Lru/cn/tv/stb/settings/SettingsViewModel;-><init>(Lru/cn/mvvm/RxLoader;Landroid/content/Context;)V

    .line 16
    .local v2, "settingsViewModel":Lru/cn/tv/stb/settings/SettingsViewModel;
    return-object v2
.end method

.method public getTargetScope(Ltoothpick/Scope;)Ltoothpick/Scope;
    .locals 0
    .param p1, "scope"    # Ltoothpick/Scope;

    .prologue
    .line 21
    return-object p1
.end method

.method public hasProvidesSingletonInScopeAnnotation()Z
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    return v0
.end method

.method public hasScopeAnnotation()Z
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x0

    return v0
.end method
