.class public Lru/cn/tv/mobile/NewActivity;
.super Lru/cn/tv/mobile/BaseDrawerActivity;
.source "NewActivity.java"

# interfaces
.implements Lru/cn/tv/player/FloatingPlayerFragment$Listener;
.implements Lru/cn/tv/player/SimplePlayerFragmentEx$SubscribeListener;


# instance fields
.field private currentFragment:Landroid/support/v4/app/Fragment;

.field private defaultTitle:Ljava/lang/String;

.field private orientationEventListener:Landroid/view/OrientationEventListener;

.field private player:Lru/cn/tv/player/FloatingPlayerFragment;

.field private final scheduleListener:Lru/cn/tv/mobile/schedule/ScheduleFragment$ScheduleItemClickListener;

.field private searchMenuItem:Landroid/view/MenuItem;

.field private searchRubricId:J

.field private searchView:Landroid/support/v7/widget/SearchView;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 67
    invoke-direct {p0}, Lru/cn/tv/mobile/BaseDrawerActivity;-><init>()V

    .line 83
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lru/cn/tv/mobile/NewActivity;->searchRubricId:J

    .line 900
    new-instance v0, Lru/cn/tv/mobile/NewActivity$16;

    invoke-direct {v0, p0}, Lru/cn/tv/mobile/NewActivity$16;-><init>(Lru/cn/tv/mobile/NewActivity;)V

    iput-object v0, p0, Lru/cn/tv/mobile/NewActivity;->scheduleListener:Lru/cn/tv/mobile/schedule/ScheduleFragment$ScheduleItemClickListener;

    return-void
.end method

.method static synthetic access$000(Lru/cn/tv/mobile/NewActivity;)Landroid/support/v4/app/Fragment;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/mobile/NewActivity;

    .prologue
    .line 67
    iget-object v0, p0, Lru/cn/tv/mobile/NewActivity;->currentFragment:Landroid/support/v4/app/Fragment;

    return-object v0
.end method

.method static synthetic access$002(Lru/cn/tv/mobile/NewActivity;Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/Fragment;
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/mobile/NewActivity;
    .param p1, "x1"    # Landroid/support/v4/app/Fragment;

    .prologue
    .line 67
    iput-object p1, p0, Lru/cn/tv/mobile/NewActivity;->currentFragment:Landroid/support/v4/app/Fragment;

    return-object p1
.end method

.method static synthetic access$100(Lru/cn/tv/mobile/NewActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/mobile/NewActivity;

    .prologue
    .line 67
    iget-object v0, p0, Lru/cn/tv/mobile/NewActivity;->defaultTitle:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1000(Lru/cn/tv/mobile/NewActivity;J)V
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/mobile/NewActivity;
    .param p1, "x1"    # J

    .prologue
    .line 67
    invoke-direct {p0, p1, p2}, Lru/cn/tv/mobile/NewActivity;->playTelecast(J)V

    return-void
.end method

.method static synthetic access$1100(Lru/cn/tv/mobile/NewActivity;J)V
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/mobile/NewActivity;
    .param p1, "x1"    # J

    .prologue
    .line 67
    invoke-direct {p0, p1, p2}, Lru/cn/tv/mobile/NewActivity;->playChannel(J)V

    return-void
.end method

.method static synthetic access$1200(Lru/cn/tv/mobile/NewActivity;JJ)V
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/mobile/NewActivity;
    .param p1, "x1"    # J
    .param p3, "x2"    # J

    .prologue
    .line 67
    invoke-direct {p0, p1, p2, p3, p4}, Lru/cn/tv/mobile/NewActivity;->playStory(JJ)V

    return-void
.end method

.method static synthetic access$1300(Lru/cn/tv/mobile/NewActivity;JLjava/lang/String;)V
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/mobile/NewActivity;
    .param p1, "x1"    # J
    .param p3, "x2"    # Ljava/lang/String;

    .prologue
    .line 67
    invoke-direct {p0, p1, p2, p3}, Lru/cn/tv/mobile/NewActivity;->openSchedule(JLjava/lang/String;)V

    return-void
.end method

.method static synthetic access$1400(Lru/cn/tv/mobile/NewActivity;)Landroid/support/v7/widget/SearchView;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/mobile/NewActivity;

    .prologue
    .line 67
    iget-object v0, p0, Lru/cn/tv/mobile/NewActivity;->searchView:Landroid/support/v7/widget/SearchView;

    return-object v0
.end method

.method static synthetic access$1500(Lru/cn/tv/mobile/NewActivity;)J
    .locals 2
    .param p0, "x0"    # Lru/cn/tv/mobile/NewActivity;

    .prologue
    .line 67
    iget-wide v0, p0, Lru/cn/tv/mobile/NewActivity;->searchRubricId:J

    return-wide v0
.end method

.method static synthetic access$1600(Lru/cn/tv/mobile/NewActivity;JLjava/lang/String;)V
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/mobile/NewActivity;
    .param p1, "x1"    # J
    .param p3, "x2"    # Ljava/lang/String;

    .prologue
    .line 67
    invoke-direct {p0, p1, p2, p3}, Lru/cn/tv/mobile/NewActivity;->openSearchResult(JLjava/lang/String;)V

    return-void
.end method

.method static synthetic access$1700(Lru/cn/tv/mobile/NewActivity;)Landroid/view/MenuItem;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/mobile/NewActivity;

    .prologue
    .line 67
    iget-object v0, p0, Lru/cn/tv/mobile/NewActivity;->searchMenuItem:Landroid/view/MenuItem;

    return-object v0
.end method

.method static synthetic access$1800(Lru/cn/tv/mobile/NewActivity;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/mobile/NewActivity;

    .prologue
    .line 67
    invoke-direct {p0}, Lru/cn/tv/mobile/NewActivity;->hideBilling()V

    return-void
.end method

.method static synthetic access$1900(Lru/cn/tv/mobile/NewActivity;I)V
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/mobile/NewActivity;
    .param p1, "x1"    # I

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lru/cn/tv/mobile/NewActivity;->showBillingError(I)V

    return-void
.end method

.method static synthetic access$200(Lru/cn/tv/mobile/NewActivity;IZ)V
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/mobile/NewActivity;
    .param p1, "x1"    # I
    .param p2, "x2"    # Z

    .prologue
    .line 67
    invoke-direct {p0, p1, p2}, Lru/cn/tv/mobile/NewActivity;->openChannels(IZ)V

    return-void
.end method

.method static synthetic access$2000(Lru/cn/tv/mobile/NewActivity;)Lru/cn/tv/player/FloatingPlayerFragment;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/mobile/NewActivity;

    .prologue
    .line 67
    iget-object v0, p0, Lru/cn/tv/mobile/NewActivity;->player:Lru/cn/tv/player/FloatingPlayerFragment;

    return-object v0
.end method

.method static synthetic access$300(Lru/cn/tv/mobile/NewActivity;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/mobile/NewActivity;

    .prologue
    .line 67
    invoke-direct {p0}, Lru/cn/tv/mobile/NewActivity;->openLiveNow()V

    return-void
.end method

.method static synthetic access$400(Lru/cn/tv/mobile/NewActivity;JLjava/lang/String;Z)V
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/mobile/NewActivity;
    .param p1, "x1"    # J
    .param p3, "x2"    # Ljava/lang/String;
    .param p4, "x3"    # Z

    .prologue
    .line 67
    invoke-direct {p0, p1, p2, p3, p4}, Lru/cn/tv/mobile/NewActivity;->openCollection(JLjava/lang/String;Z)V

    return-void
.end method

.method static synthetic access$500(Lru/cn/tv/mobile/NewActivity;Landroid/support/v4/app/Fragment;Ljava/lang/String;Z)V
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/mobile/NewActivity;
    .param p1, "x1"    # Landroid/support/v4/app/Fragment;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Z

    .prologue
    .line 67
    invoke-direct {p0, p1, p2, p3}, Lru/cn/tv/mobile/NewActivity;->replaceCurrentFragment(Landroid/support/v4/app/Fragment;Ljava/lang/String;Z)V

    return-void
.end method

.method static synthetic access$600(Lru/cn/tv/mobile/NewActivity;JLjava/lang/String;)V
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/mobile/NewActivity;
    .param p1, "x1"    # J
    .param p3, "x2"    # Ljava/lang/String;

    .prologue
    .line 67
    invoke-direct {p0, p1, p2, p3}, Lru/cn/tv/mobile/NewActivity;->openStoriesList(JLjava/lang/String;)V

    return-void
.end method

.method static synthetic access$700(Lru/cn/tv/mobile/NewActivity;JLjava/lang/String;)V
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/mobile/NewActivity;
    .param p1, "x1"    # J
    .param p3, "x2"    # Ljava/lang/String;

    .prologue
    .line 67
    invoke-direct {p0, p1, p2, p3}, Lru/cn/tv/mobile/NewActivity;->openStories(JLjava/lang/String;)V

    return-void
.end method

.method static synthetic access$800(Lru/cn/tv/mobile/NewActivity;J)V
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/mobile/NewActivity;
    .param p1, "x1"    # J

    .prologue
    .line 67
    invoke-direct {p0, p1, p2}, Lru/cn/tv/mobile/NewActivity;->showBilling(J)V

    return-void
.end method

.method static synthetic access$900(Lru/cn/tv/mobile/NewActivity;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/mobile/NewActivity;

    .prologue
    .line 67
    invoke-direct {p0}, Lru/cn/tv/mobile/NewActivity;->openNoAdsSubscription()V

    return-void
.end method

.method private askTvInterfaceTransition()V
    .locals 3

    .prologue
    .line 743
    new-instance v0, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0e00c4

    .line 744
    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setMessage(I)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0e0038

    new-instance v2, Lru/cn/tv/mobile/NewActivity$13;

    invoke-direct {v2, p0}, Lru/cn/tv/mobile/NewActivity$13;-><init>(Lru/cn/tv/mobile/NewActivity;)V

    .line 745
    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0e0033

    const/4 v2, 0x0

    .line 761
    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 762
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->show()Landroid/support/v7/app/AlertDialog;

    .line 763
    return-void
.end method

.method private doPushEventAction(Landroid/os/Bundle;)V
    .locals 17
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 298
    const-string v14, "type"

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v12

    .line 300
    .local v12, "type":I
    packed-switch v12, :pswitch_data_0

    .line 365
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 302
    :pswitch_1
    const-string v14, "catalogItem"

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_0

    .line 303
    const-string v14, "catalogItem"

    .line 304
    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 303
    invoke-static {v14}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 305
    .local v2, "channelCnId":J
    const/4 v14, 0x4

    const/4 v15, 0x0

    invoke-static {v14, v15}, Lru/cn/domain/statistics/inetra/InetraTracker;->setSessionParams(II)V

    .line 308
    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lru/cn/tv/mobile/NewActivity;->playChannel(J)V

    goto :goto_0

    .line 315
    .end local v2    # "channelCnId":J
    :pswitch_2
    const-string v14, "catalogItem"

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_0

    .line 316
    const/4 v14, 0x4

    const/4 v15, 0x0

    invoke-static {v14, v15}, Lru/cn/domain/statistics/inetra/InetraTracker;->setSessionParams(II)V

    .line 320
    :try_start_0
    const-string v14, "catalogItem"

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v10

    .line 321
    .local v10, "telecastId":J
    move-object/from16 v0, p0

    invoke-direct {v0, v10, v11}, Lru/cn/tv/mobile/NewActivity;->playTelecast(J)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 322
    .end local v10    # "telecastId":J
    :catch_0
    move-exception v6

    .line 323
    .local v6, "e":Ljava/lang/NumberFormatException;
    invoke-static {v6}, Lru/cn/utils/Logger;->logException(Ljava/lang/Throwable;)V

    .line 325
    invoke-static {v6}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 331
    .end local v6    # "e":Ljava/lang/NumberFormatException;
    :pswitch_3
    invoke-direct/range {p0 .. p0}, Lru/cn/tv/mobile/NewActivity;->isPlayServicesAvailable()Z

    move-result v14

    if-eqz v14, :cond_1

    .line 332
    new-instance v9, Landroid/content/Intent;

    const-string v14, "android.intent.action.VIEW"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "market://details?id="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    .line 333
    invoke-virtual/range {p0 .. p0}, Lru/cn/tv/mobile/NewActivity;->getPackageName()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v15

    invoke-direct {v9, v14, v15}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 334
    .local v9, "intentPlay":Landroid/content/Intent;
    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lru/cn/tv/mobile/NewActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 336
    .end local v9    # "intentPlay":Landroid/content/Intent;
    :cond_1
    new-instance v8, Landroid/content/Intent;

    const-string v14, "android.intent.action.VIEW"

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "https://play.google.com/store/apps/details?id="

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    .line 337
    invoke-virtual/range {p0 .. p0}, Lru/cn/tv/mobile/NewActivity;->getPackageName()Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v15

    invoke-direct {v8, v14, v15}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 338
    .local v8, "intentBrowser":Landroid/content/Intent;
    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lru/cn/tv/mobile/NewActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 343
    .end local v8    # "intentBrowser":Landroid/content/Intent;
    :pswitch_4
    const-string v14, "collection"

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_0

    .line 345
    :try_start_1
    const-string v14, "collection"

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-static {v14}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 347
    .local v4, "collection":J
    const v14, 0x7f0e002b

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lru/cn/tv/mobile/NewActivity;->getString(I)Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5, v14, v15}, Lru/cn/tv/mobile/NewActivity;->openCollection(JLjava/lang/String;Z)V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 348
    .end local v4    # "collection":J
    :catch_1
    move-exception v6

    .line 349
    .restart local v6    # "e":Ljava/lang/NumberFormatException;
    invoke-static {v6}, Lru/cn/utils/Logger;->logException(Ljava/lang/Throwable;)V

    .line 351
    invoke-static {v6}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 357
    .end local v6    # "e":Ljava/lang/NumberFormatException;
    :pswitch_5
    const-string v14, "url"

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_0

    .line 358
    const-string v14, "url"

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 359
    .local v13, "url":Ljava/lang/String;
    new-instance v7, Landroid/content/Intent;

    const-string v14, "android.intent.action.VIEW"

    invoke-direct {v7, v14}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 360
    .local v7, "i":Landroid/content/Intent;
    invoke-static {v13}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v14

    invoke-virtual {v7, v14}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 361
    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lru/cn/tv/mobile/NewActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 300
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method private hideBilling()V
    .locals 1

    .prologue
    .line 895
    iget-object v0, p0, Lru/cn/tv/mobile/NewActivity;->currentFragment:Landroid/support/v4/app/Fragment;

    instance-of v0, v0, Lru/cn/tv/billing/BillingFragment;

    if-eqz v0, :cond_0

    .line 896
    invoke-virtual {p0}, Lru/cn/tv/mobile/NewActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->popBackStack()V

    .line 898
    :cond_0
    return-void
.end method

.method private isAutoRotateOn()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 782
    invoke-virtual {p0}, Lru/cn/tv/mobile/NewActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "accelerometer_rotation"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private isHorizontal()Z
    .locals 2

    .prologue
    .line 536
    invoke-virtual {p0}, Lru/cn/tv/mobile/NewActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v0, v1, Landroid/content/res/Configuration;->orientation:I

    .line 537
    .local v0, "orientation":I
    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private isPlayServicesAvailable()Z
    .locals 3

    .prologue
    .line 289
    invoke-static {p0}, Lcom/google/android/gms/common/GooglePlayServicesUtil;->isGooglePlayServicesAvailable(Landroid/content/Context;)I

    move-result v0

    .line 290
    .local v0, "resultCode":I
    if-eqz v0, :cond_0

    .line 291
    const-string v1, "NewActivity"

    const-string v2, "Google Play service is not available"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 292
    const/4 v1, 0x0

    .line 294
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private openChannels(IZ)V
    .locals 2
    .param p1, "viewMode"    # I
    .param p2, "growStack"    # Z

    .prologue
    .line 388
    new-instance v0, Lru/cn/tv/mobile/ChannelsScheduleFragment;

    invoke-direct {v0}, Lru/cn/tv/mobile/ChannelsScheduleFragment;-><init>()V

    .line 390
    .local v0, "f":Lru/cn/tv/mobile/ChannelsScheduleFragment;
    invoke-virtual {v0, p1}, Lru/cn/tv/mobile/ChannelsScheduleFragment;->setViewMode(I)V

    .line 391
    new-instance v1, Lru/cn/tv/mobile/NewActivity$5;

    invoke-direct {v1, p0, v0}, Lru/cn/tv/mobile/NewActivity$5;-><init>(Lru/cn/tv/mobile/NewActivity;Lru/cn/tv/mobile/ChannelsScheduleFragment;)V

    invoke-virtual {v0, v1}, Lru/cn/tv/mobile/ChannelsScheduleFragment;->setListener(Lru/cn/tv/mobile/ChannelsScheduleFragment$ChannelsScheduleFragmentListener;)V

    .line 415
    iget-object v1, p0, Lru/cn/tv/mobile/NewActivity;->scheduleListener:Lru/cn/tv/mobile/schedule/ScheduleFragment$ScheduleItemClickListener;

    invoke-virtual {v0, v1}, Lru/cn/tv/mobile/ChannelsScheduleFragment;->setScheduleListener(Lru/cn/tv/mobile/schedule/ScheduleFragment$ScheduleItemClickListener;)V

    .line 417
    packed-switch p1, :pswitch_data_0

    .line 430
    :goto_0
    return-void

    .line 419
    :pswitch_0
    const v1, 0x7f0e008f

    invoke-direct {p0, v0, v1, p2}, Lru/cn/tv/mobile/NewActivity;->replaceCurrentFragment(Landroid/support/v4/app/Fragment;IZ)V

    goto :goto_0

    .line 423
    :pswitch_1
    const v1, 0x7f0e008d

    invoke-direct {p0, v0, v1, p2}, Lru/cn/tv/mobile/NewActivity;->replaceCurrentFragment(Landroid/support/v4/app/Fragment;IZ)V

    goto :goto_0

    .line 427
    :pswitch_2
    const v1, 0x7f0e008e

    invoke-direct {p0, v0, v1, p2}, Lru/cn/tv/mobile/NewActivity;->replaceCurrentFragment(Landroid/support/v4/app/Fragment;IZ)V

    goto :goto_0

    .line 417
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private openCollection(JLjava/lang/String;Z)V
    .locals 3
    .param p1, "rubricId"    # J
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "growStack"    # Z

    .prologue
    .line 463
    new-instance v0, Lru/cn/tv/mobile/collections/CollectionFragment;

    invoke-direct {v0}, Lru/cn/tv/mobile/collections/CollectionFragment;-><init>()V

    .line 464
    .local v0, "f":Lru/cn/tv/mobile/collections/CollectionFragment;
    invoke-virtual {v0, p1, p2}, Lru/cn/tv/mobile/collections/CollectionFragment;->setRubricId(J)V

    .line 465
    new-instance v1, Lru/cn/tv/mobile/NewActivity$8;

    invoke-direct {v1, p0, p1, p2}, Lru/cn/tv/mobile/NewActivity$8;-><init>(Lru/cn/tv/mobile/NewActivity;J)V

    invoke-virtual {v0, v1}, Lru/cn/tv/mobile/collections/CollectionFragment;->setListener(Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastSelectedListener;)V

    .line 477
    invoke-direct {p0, v0, p3, p4}, Lru/cn/tv/mobile/NewActivity;->replaceCurrentFragment(Landroid/support/v4/app/Fragment;Ljava/lang/String;Z)V

    .line 478
    return-void
.end method

.method private openLiveNow()V
    .locals 3

    .prologue
    .line 373
    new-instance v0, Lru/cn/tv/mobile/live/LiveNowFragment;

    invoke-direct {v0}, Lru/cn/tv/mobile/live/LiveNowFragment;-><init>()V

    .line 374
    .local v0, "f":Lru/cn/tv/mobile/live/LiveNowFragment;
    new-instance v1, Lru/cn/tv/mobile/NewActivity$4;

    invoke-direct {v1, p0}, Lru/cn/tv/mobile/NewActivity$4;-><init>(Lru/cn/tv/mobile/NewActivity;)V

    invoke-virtual {v0, v1}, Lru/cn/tv/mobile/live/LiveNowFragment;->setLiveTelecastListener(Lru/cn/tv/mobile/live/LiveNowFragment$LiveTelecastListener;)V

    .line 384
    const v1, 0x7f0e006b

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lru/cn/tv/mobile/NewActivity;->replaceCurrentFragment(Landroid/support/v4/app/Fragment;IZ)V

    .line 385
    return-void
.end method

.method private openNoAdsSubscription()V
    .locals 7

    .prologue
    .line 787
    invoke-static {}, Lru/cn/domain/PurchaseManager;->isAdDisabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 788
    invoke-static {}, Lru/cn/domain/statistics/AnalyticsManager;->peerstv_plus()V

    .line 791
    :cond_0
    const-wide/16 v2, 0x2

    const-wide/16 v4, -0x1

    const/4 v6, 0x1

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lru/cn/tv/mobile/NewActivity;->showBilling(JJZ)V

    .line 792
    return-void
.end method

.method private openSchedule(JLjava/lang/String;)V
    .locals 3
    .param p1, "channelId"    # J
    .param p3, "title"    # Ljava/lang/String;

    .prologue
    .line 481
    new-instance v0, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;

    invoke-direct {v0}, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;-><init>()V

    .line 482
    .local v0, "f":Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;
    invoke-virtual {v0, p1, p2}, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;->setChannelId(J)V

    .line 483
    iget-object v1, p0, Lru/cn/tv/mobile/NewActivity;->scheduleListener:Lru/cn/tv/mobile/schedule/ScheduleFragment$ScheduleItemClickListener;

    invoke-virtual {v0, v1}, Lru/cn/tv/mobile/calendar/CalendarScheduleFragment;->setListener(Lru/cn/tv/mobile/schedule/ScheduleFragment$ScheduleItemClickListener;)V

    .line 484
    const/4 v1, 0x1

    invoke-direct {p0, v0, p3, v1}, Lru/cn/tv/mobile/NewActivity;->replaceCurrentFragment(Landroid/support/v4/app/Fragment;Ljava/lang/String;Z)V

    .line 485
    return-void
.end method

.method private openSearchResult(JLjava/lang/String;)V
    .locals 3
    .param p1, "rubricId"    # J
    .param p3, "query"    # Ljava/lang/String;

    .prologue
    .line 488
    iget-object v1, p0, Lru/cn/tv/mobile/NewActivity;->player:Lru/cn/tv/player/FloatingPlayerFragment;

    invoke-virtual {v1}, Lru/cn/tv/player/FloatingPlayerFragment;->isMaximized()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 489
    iget-object v1, p0, Lru/cn/tv/mobile/NewActivity;->player:Lru/cn/tv/player/FloatingPlayerFragment;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lru/cn/tv/player/FloatingPlayerFragment;->minimize(Z)V

    .line 492
    :cond_0
    invoke-static {p3, p1, p2}, Lru/cn/tv/mobile/search/SearchFragment;->newInstance(Ljava/lang/String;J)Lru/cn/tv/mobile/search/SearchFragment;

    move-result-object v0

    .line 493
    .local v0, "searchFragment":Lru/cn/tv/mobile/search/SearchFragment;
    new-instance v1, Lru/cn/tv/mobile/NewActivity$9;

    invoke-direct {v1, p0}, Lru/cn/tv/mobile/NewActivity$9;-><init>(Lru/cn/tv/mobile/NewActivity;)V

    invoke-virtual {v0, v1}, Lru/cn/tv/mobile/search/SearchFragment;->setListener(Lru/cn/tv/mobile/search/SearchFragment$SearchFragmentListener;)V

    .line 508
    const/4 v1, 0x1

    invoke-direct {p0, v0, p3, v1}, Lru/cn/tv/mobile/NewActivity;->replaceCurrentFragment(Landroid/support/v4/app/Fragment;Ljava/lang/String;Z)V

    .line 509
    return-void
.end method

.method private openStories(JLjava/lang/String;)V
    .locals 3
    .param p1, "rubricId"    # J
    .param p3, "title"    # Ljava/lang/String;

    .prologue
    .line 445
    new-instance v0, Lru/cn/tv/mobile/stories/StoriesFragment;

    invoke-direct {v0}, Lru/cn/tv/mobile/stories/StoriesFragment;-><init>()V

    .line 446
    .local v0, "storiesFragment":Lru/cn/tv/mobile/stories/StoriesFragment;
    invoke-virtual {v0, p1, p2}, Lru/cn/tv/mobile/stories/StoriesFragment;->setRubricId(J)Lru/cn/tv/mobile/rubric/RubricFragment;

    .line 447
    new-instance v1, Lru/cn/tv/mobile/NewActivity$7;

    invoke-direct {v1, p0, p1, p2}, Lru/cn/tv/mobile/NewActivity$7;-><init>(Lru/cn/tv/mobile/NewActivity;J)V

    invoke-virtual {v0, v1}, Lru/cn/tv/mobile/stories/StoriesFragment;->setListener(Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastSelectedListener;)Lru/cn/tv/mobile/rubric/SimpleRubricFragment;

    .line 459
    const/4 v1, 0x1

    invoke-direct {p0, v0, p3, v1}, Lru/cn/tv/mobile/NewActivity;->replaceCurrentFragment(Landroid/support/v4/app/Fragment;Ljava/lang/String;Z)V

    .line 460
    return-void
.end method

.method private openStoriesList(JLjava/lang/String;)V
    .locals 3
    .param p1, "rubricId"    # J
    .param p3, "title"    # Ljava/lang/String;

    .prologue
    .line 433
    invoke-static {p1, p2}, Lru/cn/tv/mobile/stories/NewsFragment;->newInstance(J)Lru/cn/tv/mobile/stories/NewsFragment;

    move-result-object v0

    .line 434
    .local v0, "newsFragment":Lru/cn/tv/mobile/stories/NewsFragment;
    new-instance v1, Lru/cn/tv/mobile/NewActivity$6;

    invoke-direct {v1, p0}, Lru/cn/tv/mobile/NewActivity$6;-><init>(Lru/cn/tv/mobile/NewActivity;)V

    invoke-virtual {v0, v1}, Lru/cn/tv/mobile/stories/NewsFragment;->setListener(Lru/cn/tv/mobile/stories/NewsFragment$NewsFragmentListener;)V

    .line 441
    const/4 v1, 0x0

    invoke-direct {p0, v0, p3, v1}, Lru/cn/tv/mobile/NewActivity;->replaceCurrentFragment(Landroid/support/v4/app/Fragment;Ljava/lang/String;Z)V

    .line 442
    return-void
.end method

.method private playChannel(J)V
    .locals 3
    .param p1, "cnId"    # J

    .prologue
    const/4 v1, 0x1

    .line 512
    iget-object v0, p0, Lru/cn/tv/mobile/NewActivity;->player:Lru/cn/tv/player/FloatingPlayerFragment;

    invoke-virtual {v0, v1}, Lru/cn/tv/player/FloatingPlayerFragment;->maximize(Z)V

    .line 513
    invoke-static {p0}, Lru/cn/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lru/cn/tv/mobile/NewActivity;->isHorizontal()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 514
    invoke-virtual {p0, v1}, Lru/cn/tv/mobile/NewActivity;->fullScreen(Z)V

    .line 516
    :cond_0
    iget-object v0, p0, Lru/cn/tv/mobile/NewActivity;->player:Lru/cn/tv/player/FloatingPlayerFragment;

    invoke-virtual {v0, p1, p2}, Lru/cn/tv/player/FloatingPlayerFragment;->playChannel(J)V

    .line 517
    return-void
.end method

.method private playStory(JJ)V
    .locals 3
    .param p1, "telecastId"    # J
    .param p3, "rubricId"    # J

    .prologue
    const/4 v1, 0x1

    .line 528
    iget-object v0, p0, Lru/cn/tv/mobile/NewActivity;->player:Lru/cn/tv/player/FloatingPlayerFragment;

    invoke-virtual {v0, v1}, Lru/cn/tv/player/FloatingPlayerFragment;->maximize(Z)V

    .line 529
    invoke-static {p0}, Lru/cn/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lru/cn/tv/mobile/NewActivity;->isHorizontal()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 530
    invoke-virtual {p0, v1}, Lru/cn/tv/mobile/NewActivity;->fullScreen(Z)V

    .line 532
    :cond_0
    iget-object v0, p0, Lru/cn/tv/mobile/NewActivity;->player:Lru/cn/tv/player/FloatingPlayerFragment;

    invoke-virtual {v0, p1, p2, p3, p4}, Lru/cn/tv/player/FloatingPlayerFragment;->playStory(JJ)V

    .line 533
    return-void
.end method

.method private playTelecast(J)V
    .locals 3
    .param p1, "telecastId"    # J

    .prologue
    const/4 v1, 0x1

    .line 520
    iget-object v0, p0, Lru/cn/tv/mobile/NewActivity;->player:Lru/cn/tv/player/FloatingPlayerFragment;

    invoke-virtual {v0, v1}, Lru/cn/tv/player/FloatingPlayerFragment;->maximize(Z)V

    .line 521
    invoke-static {p0}, Lru/cn/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lru/cn/tv/mobile/NewActivity;->isHorizontal()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 522
    invoke-virtual {p0, v1}, Lru/cn/tv/mobile/NewActivity;->fullScreen(Z)V

    .line 524
    :cond_0
    iget-object v0, p0, Lru/cn/tv/mobile/NewActivity;->player:Lru/cn/tv/player/FloatingPlayerFragment;

    invoke-virtual {v0, p1, p2}, Lru/cn/tv/player/FloatingPlayerFragment;->playTelecast(J)V

    .line 525
    return-void
.end method

.method private replaceCurrentFragment(Landroid/support/v4/app/Fragment;IZ)V
    .locals 1
    .param p1, "f"    # Landroid/support/v4/app/Fragment;
    .param p2, "title"    # I
    .param p3, "growStack"    # Z

    .prologue
    .line 110
    invoke-virtual {p0}, Lru/cn/tv/mobile/NewActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p3}, Lru/cn/tv/mobile/NewActivity;->replaceCurrentFragment(Landroid/support/v4/app/Fragment;Ljava/lang/String;Z)V

    .line 111
    return-void
.end method

.method private replaceCurrentFragment(Landroid/support/v4/app/Fragment;Ljava/lang/String;Z)V
    .locals 3
    .param p1, "f"    # Landroid/support/v4/app/Fragment;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "growStack"    # Z

    .prologue
    .line 86
    if-nez p3, :cond_0

    .line 87
    :goto_0
    invoke-virtual {p0}, Lru/cn/tv/mobile/NewActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->getBackStackEntryCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 88
    invoke-virtual {p0}, Lru/cn/tv/mobile/NewActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->popBackStackImmediate()Z

    goto :goto_0

    .line 92
    :cond_0
    iput-object p1, p0, Lru/cn/tv/mobile/NewActivity;->currentFragment:Landroid/support/v4/app/Fragment;

    .line 94
    invoke-virtual {p0}, Lru/cn/tv/mobile/NewActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 95
    .local v0, "tr":Landroid/support/v4/app/FragmentTransaction;
    const v1, 0x7f090085

    const-string v2, "current_fragment"

    invoke-virtual {v0, v1, p1, v2}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 97
    if-eqz p3, :cond_1

    .line 98
    invoke-virtual {v0, p2}, Landroid/support/v4/app/FragmentTransaction;->setBreadCrumbTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/FragmentTransaction;

    .line 99
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    .line 100
    iget-object v1, p0, Lru/cn/tv/mobile/NewActivity;->mDrawerToggle:Landroid/support/v7/app/ActionBarDrawerToggle;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/support/v7/app/ActionBarDrawerToggle;->setDrawerIndicatorEnabled(Z)V

    .line 106
    :goto_1
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 107
    return-void

    .line 102
    :cond_1
    iput-object p2, p0, Lru/cn/tv/mobile/NewActivity;->defaultTitle:Ljava/lang/String;

    .line 103
    invoke-virtual {p0, p2}, Lru/cn/tv/mobile/NewActivity;->setDefaultTitle(Ljava/lang/String;)V

    .line 104
    iget-object v1, p0, Lru/cn/tv/mobile/NewActivity;->mDrawerToggle:Landroid/support/v7/app/ActionBarDrawerToggle;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/support/v7/app/ActionBarDrawerToggle;->setDrawerIndicatorEnabled(Z)V

    goto :goto_1
.end method

.method private showBilling(J)V
    .locals 7
    .param p1, "contractorId"    # J

    .prologue
    .line 799
    const-wide/16 v4, -0x1

    const/4 v6, 0x0

    move-object v1, p0

    move-wide v2, p1

    invoke-direct/range {v1 .. v6}, Lru/cn/tv/mobile/NewActivity;->showBilling(JJZ)V

    .line 800
    return-void
.end method

.method private showBilling(JJ)V
    .locals 7
    .param p1, "contractorId"    # J
    .param p3, "channelId"    # J

    .prologue
    .line 795
    const/4 v6, 0x0

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    invoke-direct/range {v1 .. v6}, Lru/cn/tv/mobile/NewActivity;->showBilling(JJZ)V

    .line 796
    return-void
.end method

.method private showBilling(JJZ)V
    .locals 9
    .param p1, "contractorId"    # J
    .param p3, "channelId"    # J
    .param p5, "peersTVPlus"    # Z

    .prologue
    const/4 v7, 0x1

    .line 803
    iget-object v0, p0, Lru/cn/tv/mobile/NewActivity;->player:Lru/cn/tv/player/FloatingPlayerFragment;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lru/cn/tv/player/FloatingPlayerFragment;->close(Z)V

    .line 806
    if-eqz p5, :cond_0

    .line 807
    invoke-static {v7}, Lru/cn/tv/billing/BillingFragment;->newInstance(Z)Lru/cn/tv/billing/BillingFragment;

    move-result-object v6

    .line 812
    .local v6, "billingFragment":Lru/cn/tv/billing/BillingFragment;
    :goto_0
    const-string v0, "http://close/"

    invoke-virtual {v6, v0}, Lru/cn/tv/billing/BillingFragment;->addHandledUrl(Ljava/lang/String;)V

    .line 814
    new-instance v0, Lru/cn/tv/mobile/NewActivity$14;

    invoke-direct {v0, p0}, Lru/cn/tv/mobile/NewActivity$14;-><init>(Lru/cn/tv/mobile/NewActivity;)V

    invoke-virtual {v6, v0}, Lru/cn/tv/billing/BillingFragment;->setListener(Lru/cn/tv/WebviewFragment$WebviewFragmentListener;)V

    .line 839
    new-instance v0, Lru/cn/tv/mobile/NewActivity$15;

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lru/cn/tv/mobile/NewActivity$15;-><init>(Lru/cn/tv/mobile/NewActivity;JJ)V

    invoke-virtual {v6, v0}, Lru/cn/tv/billing/BillingFragment;->setListener(Lru/cn/tv/billing/BillingFragment$BillingFragmentListener;)V

    .line 880
    const-string v0, ""

    invoke-direct {p0, v6, v0, v7}, Lru/cn/tv/mobile/NewActivity;->replaceCurrentFragment(Landroid/support/v4/app/Fragment;Ljava/lang/String;Z)V

    .line 881
    return-void

    .line 809
    .end local v6    # "billingFragment":Lru/cn/tv/billing/BillingFragment;
    :cond_0
    invoke-static {p1, p2, p3, p4}, Lru/cn/tv/billing/BillingFragment;->newInstance(JJ)Lru/cn/tv/billing/BillingFragment;

    move-result-object v6

    .restart local v6    # "billingFragment":Lru/cn/tv/billing/BillingFragment;
    goto :goto_0
.end method

.method private showBillingError(I)V
    .locals 3
    .param p1, "result"    # I

    .prologue
    .line 884
    const/16 v0, -0x3ed

    if-ne p1, v0, :cond_0

    .line 892
    :goto_0
    return-void

    .line 887
    :cond_0
    new-instance v0, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0e011d

    .line 888
    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setMessage(I)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0e0034

    const/4 v2, 0x0

    .line 889
    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x1

    .line 890
    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setCancelable(Z)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 891
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->show()Landroid/support/v7/app/AlertDialog;

    goto :goto_0
.end method


# virtual methods
.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 647
    invoke-static {p1}, Lru/cn/utils/Utils;->registerDPadKey(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 648
    const-string v0, "NewActivity"

    const-string v1, "Detected D-Pad remote"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 649
    invoke-direct {p0}, Lru/cn/tv/mobile/NewActivity;->askTvInterfaceTransition()V

    .line 652
    :cond_0
    invoke-static {p0, p1}, Lru/cn/utils/SafeCastContext;->onDispatchVolumeKeyEventBeforeJellyBean(Landroid/content/Context;Landroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-super {p0, p1}, Lru/cn/tv/mobile/BaseDrawerActivity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected drawerOpened()V
    .locals 2

    .prologue
    .line 701
    invoke-super {p0}, Lru/cn/tv/mobile/BaseDrawerActivity;->drawerOpened()V

    .line 702
    iget-object v0, p0, Lru/cn/tv/mobile/NewActivity;->player:Lru/cn/tv/player/FloatingPlayerFragment;

    invoke-virtual {v0}, Lru/cn/tv/player/FloatingPlayerFragment;->isMaximized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 703
    iget-object v0, p0, Lru/cn/tv/mobile/NewActivity;->player:Lru/cn/tv/player/FloatingPlayerFragment;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lru/cn/tv/player/FloatingPlayerFragment;->minimize(Z)V

    .line 705
    :cond_0
    return-void
.end method

.method public fullScreen(Z)V
    .locals 2
    .param p1, "f"    # Z

    .prologue
    const/4 v1, 0x0

    .line 709
    invoke-super {p0, p1}, Lru/cn/tv/mobile/BaseDrawerActivity;->fullScreen(Z)V

    .line 710
    if-eqz p1, :cond_2

    .line 711
    invoke-direct {p0}, Lru/cn/tv/mobile/NewActivity;->isAutoRotateOn()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 712
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lru/cn/tv/mobile/NewActivity;->setRequestedOrientation(I)V

    .line 722
    :cond_0
    :goto_0
    iget-object v0, p0, Lru/cn/tv/mobile/NewActivity;->player:Lru/cn/tv/player/FloatingPlayerFragment;

    invoke-virtual {v0, p1}, Lru/cn/tv/player/FloatingPlayerFragment;->fullScreen(Z)V

    .line 723
    return-void

    .line 714
    :cond_1
    invoke-virtual {p0, v1}, Lru/cn/tv/mobile/NewActivity;->setRequestedOrientation(I)V

    goto :goto_0

    .line 717
    :cond_2
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lru/cn/tv/mobile/NewActivity;->setRequestedOrientation(I)V

    .line 718
    invoke-static {p0}, Lru/cn/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lru/cn/tv/mobile/NewActivity;->player:Lru/cn/tv/player/FloatingPlayerFragment;

    invoke-virtual {v0}, Lru/cn/tv/player/FloatingPlayerFragment;->isMaximized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 719
    iget-object v0, p0, Lru/cn/tv/mobile/NewActivity;->player:Lru/cn/tv/player/FloatingPlayerFragment;

    invoke-virtual {v0, v1}, Lru/cn/tv/player/FloatingPlayerFragment;->minimize(Z)V

    goto :goto_0
.end method

.method final synthetic lambda$onCreateOptionsMenu$0$NewActivity(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 570
    invoke-static {}, Lru/cn/domain/statistics/AnalyticsManager;->use_search()V

    .line 571
    iget-object v1, p0, Lru/cn/tv/mobile/NewActivity;->currentFragment:Landroid/support/v4/app/Fragment;

    instance-of v1, v1, Lru/cn/tv/mobile/search/SearchFragment;

    if-eqz v1, :cond_0

    .line 572
    iget-object v1, p0, Lru/cn/tv/mobile/NewActivity;->currentFragment:Landroid/support/v4/app/Fragment;

    check-cast v1, Lru/cn/tv/mobile/search/SearchFragment;

    invoke-virtual {v1}, Lru/cn/tv/mobile/search/SearchFragment;->getQuery()Ljava/lang/String;

    move-result-object v0

    .line 573
    .local v0, "q":Ljava/lang/String;
    iget-object v1, p0, Lru/cn/tv/mobile/NewActivity;->searchView:Landroid/support/v7/widget/SearchView;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/support/v7/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    .line 575
    .end local v0    # "q":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method protected layout()I
    .locals 1

    .prologue
    .line 369
    const v0, 0x7f0c00a6

    return v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 612
    invoke-super {p0, p1, p2, p3}, Lru/cn/tv/mobile/BaseDrawerActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 614
    invoke-static {p1, p2, p3}, Lru/cn/domain/PurchaseManager;->handleActivityResult(IILandroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 632
    :cond_0
    :goto_0
    return-void

    .line 617
    :cond_1
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 619
    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 622
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lru/cn/tv/mobile/NewActivity$12;

    invoke-direct {v1, p0, p3}, Lru/cn/tv/mobile/NewActivity$12;-><init>(Lru/cn/tv/mobile/NewActivity;Landroid/content/Intent;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 617
    nop

    :pswitch_data_0
    .packed-switch 0x160b
        :pswitch_0
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 676
    iget-object v0, p0, Lru/cn/tv/mobile/NewActivity;->currentFragment:Landroid/support/v4/app/Fragment;

    instance-of v0, v0, Lru/cn/tv/billing/BillingFragment;

    if-eqz v0, :cond_1

    .line 677
    iget-object v0, p0, Lru/cn/tv/mobile/NewActivity;->currentFragment:Landroid/support/v4/app/Fragment;

    check-cast v0, Lru/cn/tv/billing/BillingFragment;

    invoke-virtual {v0}, Lru/cn/tv/billing/BillingFragment;->goBack()Z

    move-result v0

    if-nez v0, :cond_0

    .line 678
    invoke-super {p0}, Lru/cn/tv/mobile/BaseDrawerActivity;->onBackPressed()V

    .line 696
    :cond_0
    :goto_0
    return-void

    .line 680
    :cond_1
    invoke-virtual {p0}, Lru/cn/tv/mobile/NewActivity;->isFullScreen()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 681
    invoke-virtual {p0, v1}, Lru/cn/tv/mobile/NewActivity;->fullScreen(Z)V

    .line 682
    iget-object v0, p0, Lru/cn/tv/mobile/NewActivity;->player:Lru/cn/tv/player/FloatingPlayerFragment;

    invoke-virtual {v0}, Lru/cn/tv/player/FloatingPlayerFragment;->isMaximized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 683
    iget-object v0, p0, Lru/cn/tv/mobile/NewActivity;->player:Lru/cn/tv/player/FloatingPlayerFragment;

    invoke-virtual {v0, v1}, Lru/cn/tv/player/FloatingPlayerFragment;->minimize(Z)V

    goto :goto_0

    .line 685
    :cond_2
    invoke-virtual {p0}, Lru/cn/tv/mobile/NewActivity;->backPressed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 686
    iget-object v0, p0, Lru/cn/tv/mobile/NewActivity;->player:Lru/cn/tv/player/FloatingPlayerFragment;

    invoke-virtual {v0}, Lru/cn/tv/player/FloatingPlayerFragment;->isMaximized()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 687
    iget-object v0, p0, Lru/cn/tv/mobile/NewActivity;->player:Lru/cn/tv/player/FloatingPlayerFragment;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lru/cn/tv/player/FloatingPlayerFragment;->minimize(Z)V

    goto :goto_0

    .line 688
    :cond_3
    iget-object v0, p0, Lru/cn/tv/mobile/NewActivity;->currentFragment:Landroid/support/v4/app/Fragment;

    instance-of v0, v0, Lru/cn/tv/mobile/ChannelsScheduleFragment;

    if-eqz v0, :cond_4

    .line 689
    iget-object v0, p0, Lru/cn/tv/mobile/NewActivity;->currentFragment:Landroid/support/v4/app/Fragment;

    check-cast v0, Lru/cn/tv/mobile/ChannelsScheduleFragment;

    invoke-virtual {v0}, Lru/cn/tv/mobile/ChannelsScheduleFragment;->backPressed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 690
    invoke-super {p0}, Lru/cn/tv/mobile/BaseDrawerActivity;->onBackPressed()V

    goto :goto_0

    .line 693
    :cond_4
    invoke-super {p0}, Lru/cn/tv/mobile/BaseDrawerActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 727
    invoke-super {p0, p1}, Lru/cn/tv/mobile/BaseDrawerActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 729
    invoke-static {p0}, Lru/cn/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lru/cn/tv/mobile/NewActivity;->player:Lru/cn/tv/player/FloatingPlayerFragment;

    invoke-virtual {v0}, Lru/cn/tv/player/FloatingPlayerFragment;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 730
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 731
    iget-object v0, p0, Lru/cn/tv/mobile/NewActivity;->player:Lru/cn/tv/player/FloatingPlayerFragment;

    invoke-virtual {v0}, Lru/cn/tv/player/FloatingPlayerFragment;->isMaximized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 732
    invoke-super {p0, v3}, Lru/cn/tv/mobile/BaseDrawerActivity;->fullScreen(Z)V

    .line 733
    iget-object v0, p0, Lru/cn/tv/mobile/NewActivity;->player:Lru/cn/tv/player/FloatingPlayerFragment;

    invoke-virtual {v0, v3}, Lru/cn/tv/player/FloatingPlayerFragment;->fullScreen(Z)V

    .line 740
    :cond_0
    :goto_0
    return-void

    .line 736
    :cond_1
    invoke-super {p0, v2}, Lru/cn/tv/mobile/BaseDrawerActivity;->fullScreen(Z)V

    .line 737
    iget-object v0, p0, Lru/cn/tv/mobile/NewActivity;->player:Lru/cn/tv/player/FloatingPlayerFragment;

    invoke-virtual {v0, v2}, Lru/cn/tv/player/FloatingPlayerFragment;->fullScreen(Z)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    .line 115
    invoke-super {p0, p1}, Lru/cn/tv/mobile/BaseDrawerActivity;->onCreate(Landroid/os/Bundle;)V

    .line 117
    invoke-virtual {p0}, Lru/cn/tv/mobile/NewActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lru/cn/api/googlepush/ApplicationRegistrar;->registerIfNeeded(Landroid/content/Context;)V

    .line 118
    invoke-virtual {p0}, Lru/cn/tv/mobile/NewActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lru/cn/ad/AdsManager;->preload(Landroid/content/Context;)V

    .line 120
    invoke-virtual {p0}, Lru/cn/tv/mobile/NewActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    new-instance v2, Lru/cn/tv/mobile/NewActivity$1;

    invoke-direct {v2, p0}, Lru/cn/tv/mobile/NewActivity$1;-><init>(Lru/cn/tv/mobile/NewActivity;)V

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->addOnBackStackChangedListener(Landroid/support/v4/app/FragmentManager$OnBackStackChangedListener;)V

    .line 146
    invoke-virtual {p0}, Lru/cn/tv/mobile/NewActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const v2, 0x7f0900e4

    .line 147
    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lru/cn/tv/player/FloatingPlayerFragment;

    iput-object v1, p0, Lru/cn/tv/mobile/NewActivity;->player:Lru/cn/tv/player/FloatingPlayerFragment;

    .line 148
    iget-object v1, p0, Lru/cn/tv/mobile/NewActivity;->player:Lru/cn/tv/player/FloatingPlayerFragment;

    invoke-virtual {v1, p0}, Lru/cn/tv/player/FloatingPlayerFragment;->setSubscribeListener(Lru/cn/tv/player/SimplePlayerFragmentEx$SubscribeListener;)V

    .line 149
    iget-object v1, p0, Lru/cn/tv/mobile/NewActivity;->player:Lru/cn/tv/player/FloatingPlayerFragment;

    invoke-virtual {v1, p0}, Lru/cn/tv/player/FloatingPlayerFragment;->setListener(Lru/cn/tv/player/FloatingPlayerFragment$Listener;)V

    .line 151
    invoke-direct {p0, v3, v3}, Lru/cn/tv/mobile/NewActivity;->openChannels(IZ)V

    .line 153
    invoke-static {p0}, Lru/cn/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v0

    .line 154
    .local v0, "isTablet":Z
    new-instance v1, Lru/cn/tv/mobile/NewActivity$2;

    invoke-direct {v1, p0, p0, v0}, Lru/cn/tv/mobile/NewActivity$2;-><init>(Lru/cn/tv/mobile/NewActivity;Landroid/content/Context;Z)V

    iput-object v1, p0, Lru/cn/tv/mobile/NewActivity;->orientationEventListener:Landroid/view/OrientationEventListener;

    .line 180
    iget-object v1, p0, Lru/cn/tv/mobile/NewActivity;->navigationView:Landroid/support/design/widget/NavigationView;

    new-instance v2, Lru/cn/tv/mobile/NewActivity$3;

    invoke-direct {v2, p0}, Lru/cn/tv/mobile/NewActivity$3;-><init>(Lru/cn/tv/mobile/NewActivity;)V

    invoke-virtual {v1, v2}, Landroid/support/design/widget/NavigationView;->setNavigationItemSelectedListener(Landroid/support/design/widget/NavigationView$OnNavigationItemSelectedListener;)V

    .line 261
    new-instance v1, Lru/cn/tv/rating/Rating;

    invoke-direct {v1, p0}, Lru/cn/tv/rating/Rating;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Lru/cn/tv/rating/Rating;->runRatingRequestIfNeeded()V

    .line 262
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 6
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 554
    invoke-virtual {p0}, Lru/cn/tv/mobile/NewActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    const/high16 v2, 0x7f0d0000

    invoke-virtual {v1, v2, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 556
    invoke-virtual {p0}, Lru/cn/tv/mobile/NewActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f090119

    invoke-static {v1, p1, v2}, Lru/cn/utils/SafeCastContext;->setUpMediaRouteButton(Landroid/content/Context;Landroid/view/Menu;I)Landroid/view/MenuItem;

    .line 559
    const-string v1, "search"

    invoke-virtual {p0, v1}, Lru/cn/tv/mobile/NewActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/SearchManager;

    .line 560
    .local v0, "searchManager":Landroid/app/SearchManager;
    const v1, 0x7f09011b

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    iput-object v1, p0, Lru/cn/tv/mobile/NewActivity;->searchMenuItem:Landroid/view/MenuItem;

    .line 562
    iget-object v1, p0, Lru/cn/tv/mobile/NewActivity;->searchMenuItem:Landroid/view/MenuItem;

    invoke-static {v1}, Landroid/support/v4/view/MenuItemCompat;->getActionView(Landroid/view/MenuItem;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/SearchView;

    iput-object v1, p0, Lru/cn/tv/mobile/NewActivity;->searchView:Landroid/support/v7/widget/SearchView;

    .line 565
    iget-object v1, p0, Lru/cn/tv/mobile/NewActivity;->searchView:Landroid/support/v7/widget/SearchView;

    const v2, 0x7f0e012b

    invoke-virtual {p0, v2}, Lru/cn/tv/mobile/NewActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/SearchView;->setQueryHint(Ljava/lang/CharSequence;)V

    .line 567
    iget-object v1, p0, Lru/cn/tv/mobile/NewActivity;->searchView:Landroid/support/v7/widget/SearchView;

    invoke-virtual {p0}, Lru/cn/tv/mobile/NewActivity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/SearchManager;->getSearchableInfo(Landroid/content/ComponentName;)Landroid/app/SearchableInfo;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/SearchView;->setSearchableInfo(Landroid/app/SearchableInfo;)V

    .line 569
    iget-object v1, p0, Lru/cn/tv/mobile/NewActivity;->searchView:Landroid/support/v7/widget/SearchView;

    new-instance v2, Lru/cn/tv/mobile/NewActivity$$Lambda$0;

    invoke-direct {v2, p0}, Lru/cn/tv/mobile/NewActivity$$Lambda$0;-><init>(Lru/cn/tv/mobile/NewActivity;)V

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/SearchView;->setOnSearchClickListener(Landroid/view/View$OnClickListener;)V

    .line 577
    iget-object v1, p0, Lru/cn/tv/mobile/NewActivity;->searchView:Landroid/support/v7/widget/SearchView;

    new-instance v2, Lru/cn/tv/mobile/NewActivity$10;

    invoke-direct {v2, p0}, Lru/cn/tv/mobile/NewActivity$10;-><init>(Lru/cn/tv/mobile/NewActivity;)V

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/SearchView;->setOnQueryTextListener(Landroid/support/v7/widget/SearchView$OnQueryTextListener;)V

    .line 594
    iget-object v1, p0, Lru/cn/tv/mobile/NewActivity;->searchView:Landroid/support/v7/widget/SearchView;

    new-instance v2, Lru/cn/tv/mobile/NewActivity$11;

    invoke-direct {v2, p0}, Lru/cn/tv/mobile/NewActivity$11;-><init>(Lru/cn/tv/mobile/NewActivity;)V

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/SearchView;->setOnQueryTextFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 603
    iget-wide v2, p0, Lru/cn/tv/mobile/NewActivity;->searchRubricId:J

    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 604
    iget-object v1, p0, Lru/cn/tv/mobile/NewActivity;->searchMenuItem:Landroid/view/MenuItem;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 607
    :cond_0
    invoke-super {p0, p1}, Lru/cn/tv/mobile/BaseDrawerActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v1

    return v1
.end method

.method public onMaximize()V
    .locals 2

    .prologue
    .line 657
    iget-object v0, p0, Lru/cn/tv/mobile/NewActivity;->mDrawerToggle:Landroid/support/v7/app/ActionBarDrawerToggle;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBarDrawerToggle;->setDrawerIndicatorEnabled(Z)V

    .line 658
    return-void
.end method

.method public onMinimize()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 663
    invoke-virtual {p0}, Lru/cn/tv/mobile/NewActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->getBackStackEntryCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 664
    iget-object v0, p0, Lru/cn/tv/mobile/NewActivity;->currentFragment:Landroid/support/v4/app/Fragment;

    instance-of v0, v0, Lru/cn/tv/mobile/ChannelsScheduleFragment;

    if-eqz v0, :cond_1

    .line 665
    iget-object v0, p0, Lru/cn/tv/mobile/NewActivity;->currentFragment:Landroid/support/v4/app/Fragment;

    check-cast v0, Lru/cn/tv/mobile/ChannelsScheduleFragment;

    invoke-virtual {v0}, Lru/cn/tv/mobile/ChannelsScheduleFragment;->canGoBack()Z

    move-result v0

    if-nez v0, :cond_0

    .line 666
    iget-object v0, p0, Lru/cn/tv/mobile/NewActivity;->mDrawerToggle:Landroid/support/v7/app/ActionBarDrawerToggle;

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBarDrawerToggle;->setDrawerIndicatorEnabled(Z)V

    .line 672
    :cond_0
    :goto_0
    return-void

    .line 669
    :cond_1
    iget-object v0, p0, Lru/cn/tv/mobile/NewActivity;->mDrawerToggle:Landroid/support/v7/app/ActionBarDrawerToggle;

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBarDrawerToggle;->setDrawerIndicatorEnabled(Z)V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 636
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 641
    invoke-super {p0, p1}, Lru/cn/tv/mobile/BaseDrawerActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 638
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 636
    :pswitch_data_0
    .packed-switch 0x7f09011b
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 548
    iget-object v0, p0, Lru/cn/tv/mobile/NewActivity;->orientationEventListener:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->disable()V

    .line 549
    invoke-super {p0}, Lru/cn/tv/mobile/BaseDrawerActivity;->onPause()V

    .line 550
    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 266
    invoke-super {p0, p1}, Lru/cn/tv/mobile/BaseDrawerActivity;->onPostCreate(Landroid/os/Bundle;)V

    .line 267
    invoke-virtual {p0}, Lru/cn/tv/mobile/NewActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 268
    .local v1, "intent":Landroid/content/Intent;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 269
    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 271
    .local v0, "b":Landroid/os/Bundle;
    const-string v6, "pdid"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 272
    const-string v6, "pdid"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 274
    .local v4, "pdid":J
    invoke-static {v4, v5}, Lru/cn/domain/statistics/inetra/InetraTracker;->pushOpen(J)V

    .line 275
    const-string v6, "type"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 276
    invoke-direct {p0, v0}, Lru/cn/tv/mobile/NewActivity;->doPushEventAction(Landroid/os/Bundle;)V

    .line 286
    .end local v0    # "b":Landroid/os/Bundle;
    .end local v4    # "pdid":J
    :cond_0
    :goto_0
    return-void

    .line 278
    .restart local v0    # "b":Landroid/os/Bundle;
    :cond_1
    const-string v6, "cnId"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 279
    const-string v6, "cnId"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 280
    .local v2, "cnId":J
    const/4 v6, 0x0

    const/4 v7, 0x2

    invoke-static {v6, v7}, Lru/cn/domain/statistics/inetra/InetraTracker;->setSessionParams(II)V

    .line 283
    invoke-direct {p0, v2, v3}, Lru/cn/tv/mobile/NewActivity;->playChannel(J)V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 542
    invoke-super {p0}, Lru/cn/tv/mobile/BaseDrawerActivity;->onResume()V

    .line 543
    iget-object v0, p0, Lru/cn/tv/mobile/NewActivity;->orientationEventListener:Landroid/view/OrientationEventListener;

    invoke-virtual {v0}, Landroid/view/OrientationEventListener;->enable()V

    .line 544
    return-void
.end method

.method protected onSearchRubricAvailable(J)V
    .locals 3
    .param p1, "id"    # J

    .prologue
    .line 767
    iput-wide p1, p0, Lru/cn/tv/mobile/NewActivity;->searchRubricId:J

    .line 768
    iget-object v0, p0, Lru/cn/tv/mobile/NewActivity;->searchMenuItem:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    .line 769
    iget-object v0, p0, Lru/cn/tv/mobile/NewActivity;->searchMenuItem:Landroid/view/MenuItem;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 771
    :cond_0
    return-void
.end method

.method public onSubscribeClicked(JJ)V
    .locals 3
    .param p1, "contractorId"    # J
    .param p3, "channelId"    # J

    .prologue
    .line 775
    const-string v0, "NewActivity"

    const-string v1, "subscribe click"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 776
    iget-object v0, p0, Lru/cn/tv/mobile/NewActivity;->player:Lru/cn/tv/player/FloatingPlayerFragment;

    invoke-virtual {v0}, Lru/cn/tv/player/FloatingPlayerFragment;->stop()V

    .line 778
    invoke-direct {p0, p1, p2, p3, p4}, Lru/cn/tv/mobile/NewActivity;->showBilling(JJ)V

    .line 779
    return-void
.end method
