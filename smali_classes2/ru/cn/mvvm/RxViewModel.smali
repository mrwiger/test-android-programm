.class public Lru/cn/mvvm/RxViewModel;
.super Landroid/arch/lifecycle/ViewModel;
.source "RxViewModel.java"


# instance fields
.field private final disposables:Lio/reactivex/disposables/CompositeDisposable;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Landroid/arch/lifecycle/ViewModel;-><init>()V

    .line 11
    new-instance v0, Lio/reactivex/disposables/CompositeDisposable;

    invoke-direct {v0}, Lio/reactivex/disposables/CompositeDisposable;-><init>()V

    iput-object v0, p0, Lru/cn/mvvm/RxViewModel;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    return-void
.end method


# virtual methods
.method protected final bind(Lio/reactivex/disposables/Disposable;)V
    .locals 1
    .param p1, "disposable"    # Lio/reactivex/disposables/Disposable;

    .prologue
    .line 14
    iget-object v0, p0, Lru/cn/mvvm/RxViewModel;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0, p1}, Lio/reactivex/disposables/CompositeDisposable;->add(Lio/reactivex/disposables/Disposable;)Z

    .line 15
    return-void
.end method

.method protected onCleared()V
    .locals 0

    .prologue
    .line 27
    invoke-virtual {p0}, Lru/cn/mvvm/RxViewModel;->unbindAll()V

    .line 28
    return-void
.end method

.method protected final unbind(Lio/reactivex/disposables/Disposable;)V
    .locals 1
    .param p1, "disposable"    # Lio/reactivex/disposables/Disposable;

    .prologue
    .line 18
    iget-object v0, p0, Lru/cn/mvvm/RxViewModel;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0, p1}, Lio/reactivex/disposables/CompositeDisposable;->delete(Lio/reactivex/disposables/Disposable;)Z

    .line 19
    return-void
.end method

.method protected final unbindAll()V
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lru/cn/mvvm/RxViewModel;->disposables:Lio/reactivex/disposables/CompositeDisposable;

    invoke-virtual {v0}, Lio/reactivex/disposables/CompositeDisposable;->clear()V

    .line 23
    return-void
.end method
