.class public final enum Lru/cn/domain/statistics/inetra/AdvEvent;
.super Ljava/lang/Enum;
.source "AdvEvent.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lru/cn/domain/statistics/inetra/AdvEvent;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lru/cn/domain/statistics/inetra/AdvEvent;

.field public static final enum ADV_EVENT_CLICK:Lru/cn/domain/statistics/inetra/AdvEvent;

.field public static final enum ADV_EVENT_COMPLETE:Lru/cn/domain/statistics/inetra/AdvEvent;

.field public static final enum ADV_EVENT_FIRST_QUARTILE:Lru/cn/domain/statistics/inetra/AdvEvent;

.field public static final enum ADV_EVENT_MIDPOINT:Lru/cn/domain/statistics/inetra/AdvEvent;

.field public static final enum ADV_EVENT_OPPORTUNITY:Lru/cn/domain/statistics/inetra/AdvEvent;

.field public static final enum ADV_EVENT_REQUEST:Lru/cn/domain/statistics/inetra/AdvEvent;

.field public static final enum ADV_EVENT_SKIP:Lru/cn/domain/statistics/inetra/AdvEvent;

.field public static final enum ADV_EVENT_START:Lru/cn/domain/statistics/inetra/AdvEvent;

.field public static final enum ADV_EVENT_THIRD_QUARTILE:Lru/cn/domain/statistics/inetra/AdvEvent;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 6
    new-instance v0, Lru/cn/domain/statistics/inetra/AdvEvent;

    const-string v1, "ADV_EVENT_START"

    const-string v2, "1"

    invoke-direct {v0, v1, v4, v2}, Lru/cn/domain/statistics/inetra/AdvEvent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lru/cn/domain/statistics/inetra/AdvEvent;->ADV_EVENT_START:Lru/cn/domain/statistics/inetra/AdvEvent;

    .line 7
    new-instance v0, Lru/cn/domain/statistics/inetra/AdvEvent;

    const-string v1, "ADV_EVENT_FIRST_QUARTILE"

    const-string v2, "2"

    invoke-direct {v0, v1, v5, v2}, Lru/cn/domain/statistics/inetra/AdvEvent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lru/cn/domain/statistics/inetra/AdvEvent;->ADV_EVENT_FIRST_QUARTILE:Lru/cn/domain/statistics/inetra/AdvEvent;

    .line 8
    new-instance v0, Lru/cn/domain/statistics/inetra/AdvEvent;

    const-string v1, "ADV_EVENT_MIDPOINT"

    const-string v2, "3"

    invoke-direct {v0, v1, v6, v2}, Lru/cn/domain/statistics/inetra/AdvEvent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lru/cn/domain/statistics/inetra/AdvEvent;->ADV_EVENT_MIDPOINT:Lru/cn/domain/statistics/inetra/AdvEvent;

    .line 9
    new-instance v0, Lru/cn/domain/statistics/inetra/AdvEvent;

    const-string v1, "ADV_EVENT_THIRD_QUARTILE"

    const-string v2, "4"

    invoke-direct {v0, v1, v7, v2}, Lru/cn/domain/statistics/inetra/AdvEvent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lru/cn/domain/statistics/inetra/AdvEvent;->ADV_EVENT_THIRD_QUARTILE:Lru/cn/domain/statistics/inetra/AdvEvent;

    .line 10
    new-instance v0, Lru/cn/domain/statistics/inetra/AdvEvent;

    const-string v1, "ADV_EVENT_COMPLETE"

    const-string v2, "5"

    invoke-direct {v0, v1, v8, v2}, Lru/cn/domain/statistics/inetra/AdvEvent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lru/cn/domain/statistics/inetra/AdvEvent;->ADV_EVENT_COMPLETE:Lru/cn/domain/statistics/inetra/AdvEvent;

    .line 11
    new-instance v0, Lru/cn/domain/statistics/inetra/AdvEvent;

    const-string v1, "ADV_EVENT_CLICK"

    const/4 v2, 0x5

    const-string v3, "6"

    invoke-direct {v0, v1, v2, v3}, Lru/cn/domain/statistics/inetra/AdvEvent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lru/cn/domain/statistics/inetra/AdvEvent;->ADV_EVENT_CLICK:Lru/cn/domain/statistics/inetra/AdvEvent;

    .line 12
    new-instance v0, Lru/cn/domain/statistics/inetra/AdvEvent;

    const-string v1, "ADV_EVENT_SKIP"

    const/4 v2, 0x6

    const-string v3, "7"

    invoke-direct {v0, v1, v2, v3}, Lru/cn/domain/statistics/inetra/AdvEvent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lru/cn/domain/statistics/inetra/AdvEvent;->ADV_EVENT_SKIP:Lru/cn/domain/statistics/inetra/AdvEvent;

    .line 13
    new-instance v0, Lru/cn/domain/statistics/inetra/AdvEvent;

    const-string v1, "ADV_EVENT_OPPORTUNITY"

    const/4 v2, 0x7

    const-string v3, "9"

    invoke-direct {v0, v1, v2, v3}, Lru/cn/domain/statistics/inetra/AdvEvent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lru/cn/domain/statistics/inetra/AdvEvent;->ADV_EVENT_OPPORTUNITY:Lru/cn/domain/statistics/inetra/AdvEvent;

    .line 14
    new-instance v0, Lru/cn/domain/statistics/inetra/AdvEvent;

    const-string v1, "ADV_EVENT_REQUEST"

    const/16 v2, 0x8

    const-string v3, "10"

    invoke-direct {v0, v1, v2, v3}, Lru/cn/domain/statistics/inetra/AdvEvent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lru/cn/domain/statistics/inetra/AdvEvent;->ADV_EVENT_REQUEST:Lru/cn/domain/statistics/inetra/AdvEvent;

    .line 4
    const/16 v0, 0x9

    new-array v0, v0, [Lru/cn/domain/statistics/inetra/AdvEvent;

    sget-object v1, Lru/cn/domain/statistics/inetra/AdvEvent;->ADV_EVENT_START:Lru/cn/domain/statistics/inetra/AdvEvent;

    aput-object v1, v0, v4

    sget-object v1, Lru/cn/domain/statistics/inetra/AdvEvent;->ADV_EVENT_FIRST_QUARTILE:Lru/cn/domain/statistics/inetra/AdvEvent;

    aput-object v1, v0, v5

    sget-object v1, Lru/cn/domain/statistics/inetra/AdvEvent;->ADV_EVENT_MIDPOINT:Lru/cn/domain/statistics/inetra/AdvEvent;

    aput-object v1, v0, v6

    sget-object v1, Lru/cn/domain/statistics/inetra/AdvEvent;->ADV_EVENT_THIRD_QUARTILE:Lru/cn/domain/statistics/inetra/AdvEvent;

    aput-object v1, v0, v7

    sget-object v1, Lru/cn/domain/statistics/inetra/AdvEvent;->ADV_EVENT_COMPLETE:Lru/cn/domain/statistics/inetra/AdvEvent;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lru/cn/domain/statistics/inetra/AdvEvent;->ADV_EVENT_CLICK:Lru/cn/domain/statistics/inetra/AdvEvent;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lru/cn/domain/statistics/inetra/AdvEvent;->ADV_EVENT_SKIP:Lru/cn/domain/statistics/inetra/AdvEvent;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lru/cn/domain/statistics/inetra/AdvEvent;->ADV_EVENT_OPPORTUNITY:Lru/cn/domain/statistics/inetra/AdvEvent;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lru/cn/domain/statistics/inetra/AdvEvent;->ADV_EVENT_REQUEST:Lru/cn/domain/statistics/inetra/AdvEvent;

    aput-object v2, v0, v1

    sput-object v0, Lru/cn/domain/statistics/inetra/AdvEvent;->$VALUES:[Lru/cn/domain/statistics/inetra/AdvEvent;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p3, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 19
    iput-object p3, p0, Lru/cn/domain/statistics/inetra/AdvEvent;->value:Ljava/lang/String;

    .line 20
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lru/cn/domain/statistics/inetra/AdvEvent;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 4
    const-class v0, Lru/cn/domain/statistics/inetra/AdvEvent;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lru/cn/domain/statistics/inetra/AdvEvent;

    return-object v0
.end method

.method public static values()[Lru/cn/domain/statistics/inetra/AdvEvent;
    .locals 1

    .prologue
    .line 4
    sget-object v0, Lru/cn/domain/statistics/inetra/AdvEvent;->$VALUES:[Lru/cn/domain/statistics/inetra/AdvEvent;

    invoke-virtual {v0}, [Lru/cn/domain/statistics/inetra/AdvEvent;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lru/cn/domain/statistics/inetra/AdvEvent;

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lru/cn/domain/statistics/inetra/AdvEvent;->value:Ljava/lang/String;

    return-object v0
.end method
