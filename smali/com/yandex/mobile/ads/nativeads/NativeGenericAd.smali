.class public interface abstract Lcom/yandex/mobile/ads/nativeads/NativeGenericAd;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract addImageLoadingListener(Lcom/yandex/mobile/ads/nativeads/NativeAdImageLoadingListener;)V
.end method

.method public abstract getAdAssets()Lcom/yandex/mobile/ads/nativeads/NativeAdAssets;
.end method

.method public abstract getAdType()Lcom/yandex/mobile/ads/nativeads/NativeAdType;
.end method

.method public abstract loadImages()V
.end method

.method public abstract removeImageLoadingListener(Lcom/yandex/mobile/ads/nativeads/NativeAdImageLoadingListener;)V
.end method

.method public abstract setAdEventListener(Lcom/yandex/mobile/ads/nativeads/NativeAdEventListener;)V
.end method

.method public abstract shouldOpenLinksInApp(Z)V
.end method
