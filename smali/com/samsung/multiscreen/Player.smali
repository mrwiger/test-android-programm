.class public Lcom/samsung/multiscreen/Player;
.super Ljava/lang/Object;
.source "Player.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/multiscreen/Player$DMPStatus;,
        Lcom/samsung/multiscreen/Player$PlayerControlStatus;,
        Lcom/samsung/multiscreen/Player$RepeatMode;,
        Lcom/samsung/multiscreen/Player$PlayerApplicationStatusEvents;,
        Lcom/samsung/multiscreen/Player$PlayerContentSubEvents;,
        Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;,
        Lcom/samsung/multiscreen/Player$PlayerControlEvents;,
        Lcom/samsung/multiscreen/Player$ContentType;
    }
.end annotation


# static fields
.field private static final APP_ID:Ljava/lang/String; = "3201412000694"

.field static final CONTENT_URI:Ljava/lang/String; = "uri"

.field private static final DEFAULT_MEDIA_PLAYER:Ljava/lang/String; = "samsung.default.media.player"

.field static final PLAYER_APP_STATUS_EVENT:Ljava/lang/String; = "appStatus"

.field private static final PLAYER_BGIMAGE1:Ljava/lang/String; = "url1"

.field private static final PLAYER_BGIMAGE2:Ljava/lang/String; = "url2"

.field private static final PLAYER_BGIMAGE3:Ljava/lang/String; = "url2"

.field static final PLAYER_CHANGE_SUB_EVENT:Ljava/lang/String; = "playerChange"

.field static final PLAYER_CONTENT_CHANGE_EVENT:Ljava/lang/String; = "playerContentChange"

.field static final PLAYER_CONTROL_EVENT:Ljava/lang/String; = "playerControl"

.field static final PLAYER_CURRENT_PLAYING_EVENT:Ljava/lang/String; = "currentPlaying"

.field static final PLAYER_DATA:Ljava/lang/String; = "data"

.field static final PLAYER_ERROR_MESSAGE_EVENT:Ljava/lang/String; = "error"

.field static final PLAYER_NOTICE_RESPONSE_EVENT:Ljava/lang/String; = "playerNotice"

.field static final PLAYER_QUEUE_EVENT:Ljava/lang/String; = "playerQueueEvent"

.field static final PLAYER_READY_SUB_EVENT:Ljava/lang/String; = "playerReady"

.field static final PLAYER_SUB_EVENT:Ljava/lang/String; = "subEvent"

.field static final PLAYER_TYPE:Ljava/lang/String; = "playerType"

.field private static final PROPERTY_APP_VISIBLE:Ljava/lang/String; = "visible"

.field private static final PROPERTY_DMP_RUNNING:Ljava/lang/String; = "media_player"

.field private static final PROPERTY_ISCONTENTS:Ljava/lang/String; = "isContents"

.field private static final PROPERTY_RUNNING:Ljava/lang/String; = "running"

.field private static final TAG:Ljava/lang/String; = "Player"

.field static mApplication:Lcom/samsung/multiscreen/Application;


# instance fields
.field private debug:Z

.field mAdditionalData:Lorg/json/JSONObject;

.field private mAppName:Ljava/lang/String;

.field private mContentType:Lcom/samsung/multiscreen/Player$ContentType;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/multiscreen/Player;->mApplication:Lcom/samsung/multiscreen/Application;

    return-void
.end method

.method constructor <init>(Lcom/samsung/multiscreen/Service;Landroid/net/Uri;Ljava/lang/String;)V
    .locals 2
    .param p1, "service"    # Lcom/samsung/multiscreen/Service;
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "appName"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 188
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object v0, p0, Lcom/samsung/multiscreen/Player;->mAdditionalData:Lorg/json/JSONObject;

    .line 58
    iput-object v0, p0, Lcom/samsung/multiscreen/Player;->mContentType:Lcom/samsung/multiscreen/Player$ContentType;

    .line 64
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/multiscreen/Player;->debug:Z

    .line 189
    iput-object p3, p0, Lcom/samsung/multiscreen/Player;->mAppName:Ljava/lang/String;

    .line 190
    const-string v0, "samsung.default.media.player"

    invoke-virtual {p1, p2, v0}, Lcom/samsung/multiscreen/Service;->createApplication(Landroid/net/Uri;Ljava/lang/String;)Lcom/samsung/multiscreen/Application;

    move-result-object v0

    sput-object v0, Lcom/samsung/multiscreen/Player;->mApplication:Lcom/samsung/multiscreen/Application;

    .line 191
    invoke-virtual {p0}, Lcom/samsung/multiscreen/Player;->isDebug()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Player"

    const-string v1, "Player Created"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 192
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/multiscreen/Player;Lorg/json/JSONObject;Lcom/samsung/multiscreen/Result;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/multiscreen/Player;
    .param p1, "x1"    # Lorg/json/JSONObject;
    .param p2, "x2"    # Lcom/samsung/multiscreen/Result;

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Lcom/samsung/multiscreen/Player;->startPlay(Lorg/json/JSONObject;Lcom/samsung/multiscreen/Result;)V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/multiscreen/Player;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/multiscreen/Player;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/samsung/multiscreen/Player;->mAppName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/multiscreen/Player;)Lcom/samsung/multiscreen/Player$ContentType;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/multiscreen/Player;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/samsung/multiscreen/Player;->mContentType:Lcom/samsung/multiscreen/Player$ContentType;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/multiscreen/Player;Ljava/lang/String;Lcom/samsung/multiscreen/Result;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/multiscreen/Player;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Lcom/samsung/multiscreen/Result;

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Lcom/samsung/multiscreen/Player;->sendStartDMPApplicationRequest(Ljava/lang/String;Lcom/samsung/multiscreen/Result;)V

    return-void
.end method

.method private connect()V
    .locals 1

    .prologue
    .line 198
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/multiscreen/Player;->connect(Lcom/samsung/multiscreen/Result;)V

    .line 199
    return-void
.end method

.method private connect(Lcom/samsung/multiscreen/Result;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/multiscreen/Result",
            "<",
            "Lcom/samsung/multiscreen/Client;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 207
    .local p1, "result":Lcom/samsung/multiscreen/Result;, "Lcom/samsung/multiscreen/Result<Lcom/samsung/multiscreen/Client;>;"
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lcom/samsung/multiscreen/Player;->connect(Ljava/util/Map;Lcom/samsung/multiscreen/Result;)V

    .line 208
    return-void
.end method

.method private connect(Ljava/util/Map;Lcom/samsung/multiscreen/Result;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/samsung/multiscreen/Result",
            "<",
            "Lcom/samsung/multiscreen/Client;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 217
    .local p1, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .local p2, "callback":Lcom/samsung/multiscreen/Result;, "Lcom/samsung/multiscreen/Result<Lcom/samsung/multiscreen/Client;>;"
    sget-object v0, Lcom/samsung/multiscreen/Player;->mApplication:Lcom/samsung/multiscreen/Application;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/multiscreen/Application;->connectToPlay(Ljava/util/Map;Lcom/samsung/multiscreen/Result;)V

    .line 218
    return-void
.end method

.method private sendStartDMPApplicationRequest(Ljava/lang/String;Lcom/samsung/multiscreen/Result;)V
    .locals 6
    .param p1, "contentUrl"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/samsung/multiscreen/Result",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 394
    .local p2, "callback":Lcom/samsung/multiscreen/Result;, "Lcom/samsung/multiscreen/Result<Ljava/lang/Boolean;>;"
    sget-object v3, Lcom/samsung/multiscreen/Player;->mApplication:Lcom/samsung/multiscreen/Application;

    invoke-virtual {v3}, Lcom/samsung/multiscreen/Application;->getParams()Ljava/util/Map;

    move-result-object v1

    .line 396
    .local v1, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    sget-object v3, Lcom/samsung/multiscreen/Player;->mApplication:Lcom/samsung/multiscreen/Application;

    invoke-virtual {v3}, Lcom/samsung/multiscreen/Application;->getStartArgs()Ljava/util/Map;

    move-result-object v2

    .line 397
    .local v2, "startArgs":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    if-eqz v2, :cond_0

    .line 398
    const-string v3, "args"

    invoke-interface {v1, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 401
    :cond_0
    iget-object v3, p0, Lcom/samsung/multiscreen/Player;->mContentType:Lcom/samsung/multiscreen/Player$ContentType;

    invoke-virtual {v3}, Lcom/samsung/multiscreen/Player$ContentType;->name()Ljava/lang/String;

    move-result-object v0

    .line 402
    .local v0, "contentType":Ljava/lang/String;
    sget-object v3, Lcom/samsung/multiscreen/Player$ContentType;->photo:Lcom/samsung/multiscreen/Player$ContentType;

    invoke-virtual {v3}, Lcom/samsung/multiscreen/Player$ContentType;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 403
    const-string v0, "picture"

    .line 406
    :cond_1
    const-string v3, "isContents"

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 407
    const-string v3, "url"

    invoke-interface {v1, v3, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 409
    const-string v3, "os"

    sget-object v4, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 410
    const-string v3, "library"

    const-string v4, "Android SDK"

    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 411
    const-string v3, "version"

    const-string v4, "2.4.1"

    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 412
    const-string v3, "appName"

    iget-object v4, p0, Lcom/samsung/multiscreen/Player;->mAppName:Ljava/lang/String;

    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 413
    const-string v3, "modelNumber"

    sget-object v4, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 417
    invoke-virtual {p0}, Lcom/samsung/multiscreen/Player;->isDebug()Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "Player"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Send ms.webapplication.start with params "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 418
    :cond_2
    sget-object v3, Lcom/samsung/multiscreen/Player;->mApplication:Lcom/samsung/multiscreen/Application;

    const-string v4, "ms.webapplication.start"

    new-instance v5, Lcom/samsung/multiscreen/Player$3;

    invoke-direct {v5, p0, p2}, Lcom/samsung/multiscreen/Player$3;-><init>(Lcom/samsung/multiscreen/Player;Lcom/samsung/multiscreen/Result;)V

    invoke-virtual {v3, v4, v1, v5}, Lcom/samsung/multiscreen/Application;->invokeMethod(Ljava/lang/String;Ljava/util/Map;Lcom/samsung/multiscreen/Result;)V

    .line 439
    return-void
.end method

.method private startPlay(Lorg/json/JSONObject;Lcom/samsung/multiscreen/Result;)V
    .locals 8
    .param p1, "data"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            "Lcom/samsung/multiscreen/Result",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 448
    .local p2, "result":Lcom/samsung/multiscreen/Result;, "Lcom/samsung/multiscreen/Result<Ljava/lang/Boolean;>;"
    const/4 v3, 0x0

    .line 450
    .local v3, "url":Ljava/lang/String;
    if-nez p1, :cond_2

    .line 451
    new-instance v2, Lcom/samsung/multiscreen/ErrorCode;

    const-string v4, "PLAYER_ERROR_UNKNOWN"

    invoke-direct {v2, v4}, Lcom/samsung/multiscreen/ErrorCode;-><init>(Ljava/lang/String;)V

    .line 452
    .local v2, "error":Lcom/samsung/multiscreen/ErrorCode;
    if-eqz p2, :cond_0

    invoke-virtual {v2}, Lcom/samsung/multiscreen/ErrorCode;->value()I

    move-result v4

    int-to-long v4, v4

    invoke-virtual {v2}, Lcom/samsung/multiscreen/ErrorCode;->name()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2}, Lcom/samsung/multiscreen/ErrorCode;->name()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v5, v6, v7}, Lcom/samsung/multiscreen/Error;->create(JLjava/lang/String;Ljava/lang/String;)Lcom/samsung/multiscreen/Error;

    move-result-object v4

    invoke-interface {p2, v4}, Lcom/samsung/multiscreen/Result;->onError(Lcom/samsung/multiscreen/Error;)V

    .line 453
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/multiscreen/Player;->isDebug()Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "Player"

    const-string v5, "startPlay() Error: \'data\' is NULL."

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 551
    .end local v2    # "error":Lcom/samsung/multiscreen/ErrorCode;
    :cond_1
    :goto_0
    return-void

    .line 457
    :cond_2
    const-string v4, "uri"

    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 459
    :try_start_0
    const-string v4, "uri"

    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 466
    :cond_3
    if-nez v3, :cond_5

    .line 467
    new-instance v2, Lcom/samsung/multiscreen/ErrorCode;

    const-string v4, "PLAYER_ERROR_UNKNOWN"

    invoke-direct {v2, v4}, Lcom/samsung/multiscreen/ErrorCode;-><init>(Ljava/lang/String;)V

    .line 468
    .restart local v2    # "error":Lcom/samsung/multiscreen/ErrorCode;
    if-eqz p2, :cond_4

    invoke-virtual {v2}, Lcom/samsung/multiscreen/ErrorCode;->value()I

    move-result v4

    int-to-long v4, v4

    invoke-virtual {v2}, Lcom/samsung/multiscreen/ErrorCode;->name()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2}, Lcom/samsung/multiscreen/ErrorCode;->name()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v5, v6, v7}, Lcom/samsung/multiscreen/Error;->create(JLjava/lang/String;Ljava/lang/String;)Lcom/samsung/multiscreen/Error;

    move-result-object v4

    invoke-interface {p2, v4}, Lcom/samsung/multiscreen/Result;->onError(Lcom/samsung/multiscreen/Error;)V

    .line 469
    :cond_4
    invoke-virtual {p0}, Lcom/samsung/multiscreen/Player;->isDebug()Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "Player"

    const-string v5, "startPlay() Error: \'url\' is NULL."

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 460
    .end local v2    # "error":Lcom/samsung/multiscreen/ErrorCode;
    :catch_0
    move-exception v1

    .line 461
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {p0}, Lcom/samsung/multiscreen/Player;->isDebug()Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "Player"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "startPlay() : Error in parsing JSON data: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 473
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_5
    invoke-virtual {p0}, Lcom/samsung/multiscreen/Player;->isDebug()Z

    move-result v4

    if-eqz v4, :cond_6

    const-string v4, "Player"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Content Url : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 475
    :cond_6
    move-object v0, v3

    .line 476
    .local v0, "contentUrl":Ljava/lang/String;
    new-instance v4, Lcom/samsung/multiscreen/Player$4;

    invoke-direct {v4, p0, p2, p1, v0}, Lcom/samsung/multiscreen/Player$4;-><init>(Lcom/samsung/multiscreen/Player;Lcom/samsung/multiscreen/Result;Lorg/json/JSONObject;Ljava/lang/String;)V

    invoke-virtual {p0, v4}, Lcom/samsung/multiscreen/Player;->getDMPStatus(Lcom/samsung/multiscreen/Result;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public disconnect()V
    .locals 2

    .prologue
    .line 224
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/samsung/multiscreen/Player;->disconnect(ZLcom/samsung/multiscreen/Result;)V

    .line 225
    return-void
.end method

.method public disconnect(Lcom/samsung/multiscreen/Result;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/multiscreen/Result",
            "<",
            "Lcom/samsung/multiscreen/Client;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 233
    .local p1, "result":Lcom/samsung/multiscreen/Result;, "Lcom/samsung/multiscreen/Result<Lcom/samsung/multiscreen/Client;>;"
    const/4 v0, 0x1

    invoke-virtual {p0, v0, p1}, Lcom/samsung/multiscreen/Player;->disconnect(ZLcom/samsung/multiscreen/Result;)V

    .line 234
    return-void
.end method

.method public disconnect(ZLcom/samsung/multiscreen/Result;)V
    .locals 1
    .param p1, "stopOnDisconnect"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/samsung/multiscreen/Result",
            "<",
            "Lcom/samsung/multiscreen/Client;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 244
    .local p2, "callback":Lcom/samsung/multiscreen/Result;, "Lcom/samsung/multiscreen/Result<Lcom/samsung/multiscreen/Client;>;"
    sget-object v0, Lcom/samsung/multiscreen/Player;->mApplication:Lcom/samsung/multiscreen/Application;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/multiscreen/Application;->disconnect(ZLcom/samsung/multiscreen/Result;)V

    .line 245
    return-void
.end method

.method public getControlStatus()V
    .locals 3

    .prologue
    .line 628
    invoke-virtual {p0}, Lcom/samsung/multiscreen/Player;->isDebug()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Player"

    const-string v1, "Send getControlStatus"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 629
    :cond_0
    sget-object v0, Lcom/samsung/multiscreen/Player;->mApplication:Lcom/samsung/multiscreen/Application;

    const-string v1, "playerControl"

    sget-object v2, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->getControlStatus:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    invoke-virtual {v2}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/multiscreen/Application;->publish(Ljava/lang/String;Ljava/lang/Object;)V

    .line 630
    return-void
.end method

.method final getDMPStatus(Lcom/samsung/multiscreen/Result;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/multiscreen/Result",
            "<",
            "Lcom/samsung/multiscreen/Player$DMPStatus;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 319
    .local p1, "result":Lcom/samsung/multiscreen/Result;, "Lcom/samsung/multiscreen/Result<Lcom/samsung/multiscreen/Player$DMPStatus;>;"
    sget-object v2, Lcom/samsung/multiscreen/Player;->mApplication:Lcom/samsung/multiscreen/Application;

    invoke-virtual {v2}, Lcom/samsung/multiscreen/Application;->getService()Lcom/samsung/multiscreen/Service;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/multiscreen/Service;->getUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 320
    .local v0, "builder":Landroid/net/Uri$Builder;
    const-string v2, "webapplication"

    invoke-virtual {v0, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 321
    const-string v2, ""

    invoke-virtual {v0, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 322
    new-instance v2, Lcom/samsung/multiscreen/Player$1;

    invoke-direct {v2, p0}, Lcom/samsung/multiscreen/Player$1;-><init>(Lcom/samsung/multiscreen/Player;)V

    invoke-static {v2, p1}, Lcom/samsung/multiscreen/HttpHelper;->createHttpCallback(Lcom/samsung/multiscreen/util/HttpUtil$ResultCreator;Lcom/samsung/multiscreen/Result;)Lcom/koushikdutta/async/http/AsyncHttpClient$StringCallback;

    move-result-object v1

    .line 348
    .local v1, "httpStringCallback":Lcom/koushikdutta/async/http/AsyncHttpClient$StringCallback;
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    const-string v3, "GET"

    invoke-static {v2, v3, v1}, Lcom/samsung/multiscreen/util/HttpUtil;->executeJSONRequest(Landroid/net/Uri;Ljava/lang/String;Lcom/koushikdutta/async/http/AsyncHttpClient$StringCallback;)V

    .line 349
    return-void
.end method

.method public isConnected()Z
    .locals 3

    .prologue
    .line 254
    invoke-virtual {p0}, Lcom/samsung/multiscreen/Player;->isDebug()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Player"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Player Connection Status : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/samsung/multiscreen/Player;->mApplication:Lcom/samsung/multiscreen/Application;

    invoke-virtual {v2}, Lcom/samsung/multiscreen/Application;->isConnected()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 255
    :cond_0
    sget-object v0, Lcom/samsung/multiscreen/Player;->mApplication:Lcom/samsung/multiscreen/Application;

    invoke-virtual {v0}, Lcom/samsung/multiscreen/Application;->isConnected()Z

    move-result v0

    return v0
.end method

.method public isDebug()Z
    .locals 1

    .prologue
    .line 68
    iget-boolean v0, p0, Lcom/samsung/multiscreen/Player;->debug:Z

    return v0
.end method

.method public mute()V
    .locals 3

    .prologue
    .line 602
    invoke-virtual {p0}, Lcom/samsung/multiscreen/Player;->isDebug()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Player"

    const-string v1, "Send Mute"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 603
    :cond_0
    sget-object v0, Lcom/samsung/multiscreen/Player;->mApplication:Lcom/samsung/multiscreen/Application;

    const-string v1, "playerControl"

    sget-object v2, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->mute:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    invoke-virtual {v2}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/multiscreen/Application;->publish(Ljava/lang/String;Ljava/lang/Object;)V

    .line 604
    return-void
.end method

.method public next()V
    .locals 3

    .prologue
    .line 660
    invoke-virtual {p0}, Lcom/samsung/multiscreen/Player;->isDebug()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Player"

    const-string v1, "Send Next"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 661
    :cond_0
    sget-object v0, Lcom/samsung/multiscreen/Player;->mApplication:Lcom/samsung/multiscreen/Application;

    const-string v1, "playerControl"

    sget-object v2, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->next:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    invoke-virtual {v2}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/multiscreen/Application;->publish(Ljava/lang/String;Ljava/lang/Object;)V

    .line 662
    return-void
.end method

.method public pause()V
    .locals 3

    .prologue
    .line 586
    invoke-virtual {p0}, Lcom/samsung/multiscreen/Player;->isDebug()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Player"

    const-string v1, "Send Pause"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 587
    :cond_0
    sget-object v0, Lcom/samsung/multiscreen/Player;->mApplication:Lcom/samsung/multiscreen/Application;

    const-string v1, "playerControl"

    sget-object v2, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->pause:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    invoke-virtual {v2}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/multiscreen/Application;->publish(Ljava/lang/String;Ljava/lang/Object;)V

    .line 588
    return-void
.end method

.method public play()V
    .locals 3

    .prologue
    .line 578
    invoke-virtual {p0}, Lcom/samsung/multiscreen/Player;->isDebug()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Player"

    const-string v1, "Send Play"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 579
    :cond_0
    sget-object v0, Lcom/samsung/multiscreen/Player;->mApplication:Lcom/samsung/multiscreen/Application;

    const-string v1, "playerControl"

    sget-object v2, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->play:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    invoke-virtual {v2}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/multiscreen/Application;->publish(Ljava/lang/String;Ljava/lang/Object;)V

    .line 580
    return-void
.end method

.method final playContent(Lorg/json/JSONObject;Lcom/samsung/multiscreen/Player$ContentType;Lcom/samsung/multiscreen/Result;)V
    .locals 3
    .param p1, "contentInfo"    # Lorg/json/JSONObject;
    .param p2, "playerName"    # Lcom/samsung/multiscreen/Player$ContentType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            "Lcom/samsung/multiscreen/Player$ContentType;",
            "Lcom/samsung/multiscreen/Result",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 361
    .local p3, "result":Lcom/samsung/multiscreen/Result;, "Lcom/samsung/multiscreen/Result<Ljava/lang/Boolean;>;"
    invoke-virtual {p0}, Lcom/samsung/multiscreen/Player;->isDebug()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Player"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Is Connected Status : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/multiscreen/Player;->isConnected()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 364
    :cond_0
    iput-object p1, p0, Lcom/samsung/multiscreen/Player;->mAdditionalData:Lorg/json/JSONObject;

    .line 365
    iput-object p2, p0, Lcom/samsung/multiscreen/Player;->mContentType:Lcom/samsung/multiscreen/Player$ContentType;

    .line 369
    invoke-virtual {p0}, Lcom/samsung/multiscreen/Player;->isConnected()Z

    move-result v0

    if-nez v0, :cond_1

    .line 370
    new-instance v0, Lcom/samsung/multiscreen/Player$2;

    invoke-direct {v0, p0, p1, p3}, Lcom/samsung/multiscreen/Player$2;-><init>(Lcom/samsung/multiscreen/Player;Lorg/json/JSONObject;Lcom/samsung/multiscreen/Result;)V

    invoke-direct {p0, v0}, Lcom/samsung/multiscreen/Player;->connect(Lcom/samsung/multiscreen/Result;)V

    .line 384
    :goto_0
    return-void

    .line 382
    :cond_1
    invoke-direct {p0, p1, p3}, Lcom/samsung/multiscreen/Player;->startPlay(Lorg/json/JSONObject;Lcom/samsung/multiscreen/Result;)V

    goto :goto_0
.end method

.method public previous()V
    .locals 3

    .prologue
    .line 652
    invoke-virtual {p0}, Lcom/samsung/multiscreen/Player;->isDebug()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Player"

    const-string v1, "Send Previous"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 653
    :cond_0
    sget-object v0, Lcom/samsung/multiscreen/Player;->mApplication:Lcom/samsung/multiscreen/Application;

    const-string v1, "playerControl"

    sget-object v2, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->previous:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    invoke-virtual {v2}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/multiscreen/Application;->publish(Ljava/lang/String;Ljava/lang/Object;)V

    .line 654
    return-void
.end method

.method public final resumeApplicationInForeground()V
    .locals 2

    .prologue
    .line 557
    const-string v0, "http://DummyUrlToBringAppToForeground.msf"

    .line 558
    .local v0, "dummyUrl":Ljava/lang/String;
    new-instance v1, Lcom/samsung/multiscreen/Player$5;

    invoke-direct {v1, p0}, Lcom/samsung/multiscreen/Player$5;-><init>(Lcom/samsung/multiscreen/Player;)V

    invoke-direct {p0, v0, v1}, Lcom/samsung/multiscreen/Player;->sendStartDMPApplicationRequest(Ljava/lang/String;Lcom/samsung/multiscreen/Result;)V

    .line 568
    return-void
.end method

.method public setDebug(Z)V
    .locals 0
    .param p1, "debug"    # Z

    .prologue
    .line 64
    iput-boolean p1, p0, Lcom/samsung/multiscreen/Player;->debug:Z

    return-void
.end method

.method public setOnClientConnectListener(Lcom/samsung/multiscreen/Channel$OnClientConnectListener;)V
    .locals 1
    .param p1, "onClientConnectListener"    # Lcom/samsung/multiscreen/Channel$OnClientConnectListener;

    .prologue
    .line 282
    sget-object v0, Lcom/samsung/multiscreen/Player;->mApplication:Lcom/samsung/multiscreen/Application;

    invoke-virtual {v0, p1}, Lcom/samsung/multiscreen/Application;->setOnClientConnectListener(Lcom/samsung/multiscreen/Channel$OnClientConnectListener;)V

    .line 283
    return-void
.end method

.method public setOnClientDisconnectListener(Lcom/samsung/multiscreen/Channel$OnClientDisconnectListener;)V
    .locals 1
    .param p1, "onClientDisconnectListener"    # Lcom/samsung/multiscreen/Channel$OnClientDisconnectListener;

    .prologue
    .line 291
    sget-object v0, Lcom/samsung/multiscreen/Player;->mApplication:Lcom/samsung/multiscreen/Application;

    invoke-virtual {v0, p1}, Lcom/samsung/multiscreen/Application;->setOnClientDisconnectListener(Lcom/samsung/multiscreen/Channel$OnClientDisconnectListener;)V

    .line 292
    return-void
.end method

.method public setOnConnectListener(Lcom/samsung/multiscreen/Channel$OnConnectListener;)V
    .locals 1
    .param p1, "onConnectListener"    # Lcom/samsung/multiscreen/Channel$OnConnectListener;

    .prologue
    .line 264
    sget-object v0, Lcom/samsung/multiscreen/Player;->mApplication:Lcom/samsung/multiscreen/Application;

    invoke-virtual {v0, p1}, Lcom/samsung/multiscreen/Application;->setOnConnectListener(Lcom/samsung/multiscreen/Channel$OnConnectListener;)V

    .line 265
    return-void
.end method

.method public setOnDisconnectListener(Lcom/samsung/multiscreen/Channel$OnDisconnectListener;)V
    .locals 1
    .param p1, "onDisconnectListener"    # Lcom/samsung/multiscreen/Channel$OnDisconnectListener;

    .prologue
    .line 273
    sget-object v0, Lcom/samsung/multiscreen/Player;->mApplication:Lcom/samsung/multiscreen/Application;

    invoke-virtual {v0, p1}, Lcom/samsung/multiscreen/Application;->setOnDisconnectListener(Lcom/samsung/multiscreen/Channel$OnDisconnectListener;)V

    .line 274
    return-void
.end method

.method public setOnErrorListener(Lcom/samsung/multiscreen/Channel$OnErrorListener;)V
    .locals 1
    .param p1, "onErrorListener"    # Lcom/samsung/multiscreen/Channel$OnErrorListener;

    .prologue
    .line 309
    sget-object v0, Lcom/samsung/multiscreen/Player;->mApplication:Lcom/samsung/multiscreen/Application;

    invoke-virtual {v0, p1}, Lcom/samsung/multiscreen/Application;->setOnErrorListener(Lcom/samsung/multiscreen/Channel$OnErrorListener;)V

    .line 310
    return-void
.end method

.method public setOnReadyListener(Lcom/samsung/multiscreen/Channel$OnReadyListener;)V
    .locals 1
    .param p1, "OnReadyListener"    # Lcom/samsung/multiscreen/Channel$OnReadyListener;

    .prologue
    .line 300
    sget-object v0, Lcom/samsung/multiscreen/Player;->mApplication:Lcom/samsung/multiscreen/Application;

    invoke-virtual {v0, p1}, Lcom/samsung/multiscreen/Application;->setOnReadyListener(Lcom/samsung/multiscreen/Channel$OnReadyListener;)V

    .line 301
    return-void
.end method

.method public setVolume(I)V
    .locals 4
    .param p1, "volume"    # I

    .prologue
    .line 620
    invoke-virtual {p0}, Lcom/samsung/multiscreen/Player;->isDebug()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Player"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Send SetVolume : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 621
    :cond_0
    sget-object v0, Lcom/samsung/multiscreen/Player;->mApplication:Lcom/samsung/multiscreen/Application;

    const-string v1, "playerControl"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->setVolume:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    invoke-virtual {v3}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/multiscreen/Application;->publish(Ljava/lang/String;Ljava/lang/Object;)V

    .line 622
    return-void
.end method

.method public stop()V
    .locals 3

    .prologue
    .line 594
    invoke-virtual {p0}, Lcom/samsung/multiscreen/Player;->isDebug()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Player"

    const-string v1, "Send Stop"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 595
    :cond_0
    sget-object v0, Lcom/samsung/multiscreen/Player;->mApplication:Lcom/samsung/multiscreen/Application;

    const-string v1, "playerControl"

    sget-object v2, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->stop:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    invoke-virtual {v2}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/multiscreen/Application;->publish(Ljava/lang/String;Ljava/lang/Object;)V

    .line 596
    return-void
.end method

.method public unMute()V
    .locals 3

    .prologue
    .line 610
    invoke-virtual {p0}, Lcom/samsung/multiscreen/Player;->isDebug()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Player"

    const-string v1, "Send Un-Mute"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 611
    :cond_0
    sget-object v0, Lcom/samsung/multiscreen/Player;->mApplication:Lcom/samsung/multiscreen/Application;

    const-string v1, "playerControl"

    sget-object v2, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->unMute:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    invoke-virtual {v2}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/multiscreen/Application;->publish(Ljava/lang/String;Ljava/lang/Object;)V

    .line 612
    return-void
.end method

.method public volumeDown()V
    .locals 3

    .prologue
    .line 644
    invoke-virtual {p0}, Lcom/samsung/multiscreen/Player;->isDebug()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Player"

    const-string v1, "Send VolumeDown"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 645
    :cond_0
    sget-object v0, Lcom/samsung/multiscreen/Player;->mApplication:Lcom/samsung/multiscreen/Application;

    const-string v1, "playerControl"

    sget-object v2, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->volumeDown:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    invoke-virtual {v2}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/multiscreen/Application;->publish(Ljava/lang/String;Ljava/lang/Object;)V

    .line 646
    return-void
.end method

.method public volumeUp()V
    .locals 3

    .prologue
    .line 636
    invoke-virtual {p0}, Lcom/samsung/multiscreen/Player;->isDebug()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Player"

    const-string v1, "Send VolumeUp"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 637
    :cond_0
    sget-object v0, Lcom/samsung/multiscreen/Player;->mApplication:Lcom/samsung/multiscreen/Application;

    const-string v1, "playerControl"

    sget-object v2, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->volumeUp:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    invoke-virtual {v2}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/multiscreen/Application;->publish(Ljava/lang/String;Ljava/lang/Object;)V

    .line 638
    return-void
.end method
