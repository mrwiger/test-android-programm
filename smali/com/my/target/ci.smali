.class public Lcom/my/target/ci;
.super Ljava/lang/Object;
.source "MediaUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(F[F)[F
    .locals 4

    .prologue
    .line 29
    array-length v0, p1

    new-array v1, v0, [F

    .line 30
    const/4 v0, 0x0

    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_0

    .line 32
    const/high16 v2, 0x42c80000    # 100.0f

    div-float v2, p0, v2

    aget v3, p1, v0

    mul-float/2addr v2, v3

    aput v2, v1, v0

    .line 30
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 34
    :cond_0
    return-object v1
.end method

.method public static a(Lcom/my/target/al;[FF)[F
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/my/target/ag;",
            ">(",
            "Lcom/my/target/al",
            "<TT;>;[FF)[F"
        }
    .end annotation

    .prologue
    const/high16 v12, 0x42c80000    # 100.0f

    const/high16 v11, 0x40000000    # 2.0f

    const/high16 v10, 0x41200000    # 10.0f

    const/4 v9, 0x0

    const/high16 v8, -0x40800000    # -1.0f

    .line 41
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 43
    if-eqz p1, :cond_0

    array-length v0, p1

    if-lez v0, :cond_0

    .line 45
    invoke-static {p1}, Ljava/util/Arrays;->sort([F)V

    .line 48
    :cond_0
    const/4 v0, 0x0

    .line 49
    invoke-virtual {p0}, Lcom/my/target/al;->R()Ljava/util/List;

    move-result-object v4

    .line 50
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v0

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/aj;

    .line 53
    if-nez p1, :cond_3

    .line 55
    invoke-virtual {v0}, Lcom/my/target/aj;->getPointP()F

    move-result v2

    cmpl-float v2, v2, v9

    if-lez v2, :cond_1

    .line 57
    invoke-virtual {v0}, Lcom/my/target/aj;->getPointP()F

    move-result v2

    div-float/2addr v2, v12

    mul-float/2addr v2, p2

    .line 67
    :goto_1
    mul-float/2addr v2, v10

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v10

    .line 68
    invoke-virtual {v0, v2}, Lcom/my/target/aj;->setPoint(F)V

    move v0, v1

    move v1, v2

    .line 94
    :goto_2
    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move v1, v0

    .line 95
    goto :goto_0

    .line 59
    :cond_1
    invoke-virtual {v0}, Lcom/my/target/aj;->getPoint()F

    move-result v2

    cmpl-float v2, v2, v9

    if-ltz v2, :cond_2

    invoke-virtual {v0}, Lcom/my/target/aj;->getPoint()F

    move-result v2

    cmpg-float v2, v2, p2

    if-gtz v2, :cond_2

    .line 61
    invoke-virtual {v0}, Lcom/my/target/aj;->getPoint()F

    move-result v2

    goto :goto_1

    .line 65
    :cond_2
    div-float v2, p2, v11

    goto :goto_1

    .line 72
    :cond_3
    array-length v2, p1

    if-ge v1, v2, :cond_6

    .line 74
    aget v2, p1, v1

    .line 75
    invoke-virtual {v0}, Lcom/my/target/aj;->getType()Ljava/lang/String;

    move-result-object v6

    const-string v7, "statistics"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 77
    add-int/lit8 v1, v1, 0x1

    .line 79
    :cond_4
    cmpl-float v6, v2, p2

    if-lez v6, :cond_5

    .line 81
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Cannot set midroll position "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, ": out of duration"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 82
    invoke-virtual {v0, v8}, Lcom/my/target/aj;->setPoint(F)V

    goto :goto_0

    .line 85
    :cond_5
    invoke-virtual {v0, v2}, Lcom/my/target/aj;->setPoint(F)V

    move v0, v1

    move v1, v2

    goto :goto_2

    .line 89
    :cond_6
    invoke-virtual {v0, v8}, Lcom/my/target/aj;->setPoint(F)V

    goto/16 :goto_0

    .line 97
    :cond_7
    if-eqz p1, :cond_8

    array-length v0, p1

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v2

    if-le v0, v2, :cond_e

    .line 99
    :cond_8
    invoke-virtual {p0}, Lcom/my/target/al;->S()Ljava/util/ArrayList;

    move-result-object v0

    .line 100
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/ae;

    .line 104
    if-eqz p1, :cond_b

    .line 106
    array-length v2, p1

    if-ge v1, v2, :cond_a

    .line 108
    add-int/lit8 v2, v1, 0x1

    aget v1, p1, v1

    .line 109
    cmpl-float v5, v1, p2

    if-lez v5, :cond_9

    .line 111
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Cannot set midroll position "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, ": out of duration"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 112
    invoke-virtual {v0, v8}, Lcom/my/target/ae;->setPoint(F)V

    move v1, v2

    .line 113
    goto :goto_3

    .line 115
    :cond_9
    invoke-virtual {v0, v1}, Lcom/my/target/ae;->setPoint(F)V

    move v0, v1

    move v1, v2

    .line 140
    :goto_4
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 119
    :cond_a
    invoke-virtual {v0, v8}, Lcom/my/target/ae;->setPoint(F)V

    goto :goto_3

    .line 125
    :cond_b
    invoke-virtual {v0}, Lcom/my/target/ae;->getPointP()F

    move-result v2

    cmpl-float v2, v2, v9

    if-ltz v2, :cond_c

    .line 127
    invoke-virtual {v0}, Lcom/my/target/ae;->getPointP()F

    move-result v2

    div-float/2addr v2, v12

    mul-float/2addr v2, p2

    .line 137
    :goto_5
    mul-float/2addr v2, v10

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v10

    .line 138
    invoke-virtual {v0, v2}, Lcom/my/target/ae;->setPoint(F)V

    move v0, v2

    goto :goto_4

    .line 129
    :cond_c
    invoke-virtual {v0}, Lcom/my/target/ae;->getPoint()F

    move-result v2

    cmpl-float v2, v2, v9

    if-ltz v2, :cond_d

    invoke-virtual {v0}, Lcom/my/target/ae;->getPoint()F

    move-result v2

    cmpg-float v2, v2, p2

    if-gtz v2, :cond_d

    .line 131
    invoke-virtual {v0}, Lcom/my/target/ae;->getPoint()F

    move-result v2

    goto :goto_5

    .line 135
    :cond_d
    div-float v2, p2, v11

    goto :goto_5

    .line 144
    :cond_e
    invoke-virtual {v3}, Ljava/util/HashSet;->size()I

    move-result v0

    new-array v4, v0, [F

    .line 146
    const/4 v0, 0x0

    .line 147
    invoke-virtual {v3}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_6
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    .line 149
    add-int/lit8 v2, v1, 0x1

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    aput v0, v4, v1

    move v1, v2

    .line 150
    goto :goto_6

    .line 152
    :cond_f
    invoke-static {v4}, Ljava/util/Arrays;->sort([F)V

    .line 154
    return-object v4
.end method
