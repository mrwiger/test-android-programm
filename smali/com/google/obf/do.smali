.class public final Lcom/google/obf/do;
.super Ljava/lang/Object;
.source "IMASDK"


# instance fields
.field private final a:[J

.field private final b:[J


# direct methods
.method private constructor <init>([J[J)V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/google/obf/do;->a:[J

    .line 14
    iput-object p2, p0, Lcom/google/obf/do;->b:[J

    .line 15
    return-void
.end method

.method public static a(Lcom/google/obf/dw;)Lcom/google/obf/do;
    .locals 6

    .prologue
    .line 1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/obf/dw;->d(I)V

    .line 2
    invoke-virtual {p0}, Lcom/google/obf/dw;->j()I

    move-result v0

    .line 3
    div-int/lit8 v1, v0, 0x12

    .line 4
    new-array v2, v1, [J

    .line 5
    new-array v3, v1, [J

    .line 6
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 7
    invoke-virtual {p0}, Lcom/google/obf/dw;->o()J

    move-result-wide v4

    aput-wide v4, v2, v0

    .line 8
    invoke-virtual {p0}, Lcom/google/obf/dw;->o()J

    move-result-wide v4

    aput-wide v4, v3, v0

    .line 9
    const/4 v4, 0x2

    invoke-virtual {p0, v4}, Lcom/google/obf/dw;->d(I)V

    .line 10
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 11
    :cond_0
    new-instance v0, Lcom/google/obf/do;

    invoke-direct {v0, v2, v3}, Lcom/google/obf/do;-><init>([J[J)V

    return-object v0
.end method

.method static synthetic a(Lcom/google/obf/do;)[J
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/google/obf/do;->a:[J

    return-object v0
.end method

.method static synthetic b(Lcom/google/obf/do;)[J
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/google/obf/do;->b:[J

    return-object v0
.end method


# virtual methods
.method public a(JJ)Lcom/google/obf/aq;
    .locals 7

    .prologue
    .line 16
    new-instance v0, Lcom/google/obf/do$1;

    move-object v1, p0

    move-wide v2, p3

    move-wide v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/obf/do$1;-><init>(Lcom/google/obf/do;JJ)V

    return-object v0
.end method
