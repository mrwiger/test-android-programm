.class Lru/cn/ad/AdPlayController$1;
.super Ljava/lang/Object;
.source "AdPlayController.java"

# interfaces
.implements Lru/cn/ad/MiddleRollBanner$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/ad/AdPlayController;-><init>(Landroid/content/Context;Lru/cn/player/SimplePlayer;Landroid/view/ViewGroup;Lru/cn/ad/AdPlayController$Listener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/ad/AdPlayController;

.field final synthetic val$adsContainer:Landroid/view/ViewGroup;

.field final synthetic val$listener:Lru/cn/ad/AdPlayController$Listener;

.field final synthetic val$player:Lru/cn/player/SimplePlayer;


# direct methods
.method constructor <init>(Lru/cn/ad/AdPlayController;Lru/cn/ad/AdPlayController$Listener;Lru/cn/player/SimplePlayer;Landroid/view/ViewGroup;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/ad/AdPlayController;

    .prologue
    .line 75
    iput-object p1, p0, Lru/cn/ad/AdPlayController$1;->this$0:Lru/cn/ad/AdPlayController;

    iput-object p2, p0, Lru/cn/ad/AdPlayController$1;->val$listener:Lru/cn/ad/AdPlayController$Listener;

    iput-object p3, p0, Lru/cn/ad/AdPlayController$1;->val$player:Lru/cn/player/SimplePlayer;

    iput-object p4, p0, Lru/cn/ad/AdPlayController$1;->val$adsContainer:Landroid/view/ViewGroup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public adCompleted(Z)V
    .locals 2
    .param p1, "blocking"    # Z

    .prologue
    .line 93
    if-eqz p1, :cond_0

    .line 94
    iget-object v0, p0, Lru/cn/ad/AdPlayController$1;->val$listener:Lru/cn/ad/AdPlayController$Listener;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lru/cn/ad/AdPlayController$Listener;->onAdBreakEnded(Z)V

    .line 95
    iget-object v0, p0, Lru/cn/ad/AdPlayController$1;->val$player:Lru/cn/player/SimplePlayer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lru/cn/player/SimplePlayer;->setVisibility(I)V

    .line 100
    :goto_0
    iget-object v0, p0, Lru/cn/ad/AdPlayController$1;->val$adsContainer:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 101
    return-void

    .line 97
    :cond_0
    iget-object v0, p0, Lru/cn/ad/AdPlayController$1;->val$listener:Lru/cn/ad/AdPlayController$Listener;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lru/cn/ad/AdPlayController$Listener;->onAdCompanion(Landroid/view/View;)V

    goto :goto_0
.end method

.method public adStarted(ZLandroid/view/View;)V
    .locals 2
    .param p1, "blocking"    # Z
    .param p2, "view"    # Landroid/view/View;

    .prologue
    .line 78
    if-eqz p1, :cond_0

    .line 79
    iget-object v0, p0, Lru/cn/ad/AdPlayController$1;->val$listener:Lru/cn/ad/AdPlayController$Listener;

    invoke-interface {v0}, Lru/cn/ad/AdPlayController$Listener;->onAdBreak()V

    .line 80
    iget-object v0, p0, Lru/cn/ad/AdPlayController$1;->val$listener:Lru/cn/ad/AdPlayController$Listener;

    invoke-interface {v0}, Lru/cn/ad/AdPlayController$Listener;->onAdStart()V

    .line 82
    iget-object v0, p0, Lru/cn/ad/AdPlayController$1;->val$player:Lru/cn/player/SimplePlayer;

    invoke-virtual {v0}, Lru/cn/player/SimplePlayer;->pause()V

    .line 83
    iget-object v0, p0, Lru/cn/ad/AdPlayController$1;->val$player:Lru/cn/player/SimplePlayer;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lru/cn/player/SimplePlayer;->setVisibility(I)V

    .line 88
    :goto_0
    iget-object v0, p0, Lru/cn/ad/AdPlayController$1;->val$adsContainer:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 89
    return-void

    .line 85
    :cond_0
    iget-object v0, p0, Lru/cn/ad/AdPlayController$1;->val$listener:Lru/cn/ad/AdPlayController$Listener;

    invoke-interface {v0, p2}, Lru/cn/ad/AdPlayController$Listener;->onAdCompanion(Landroid/view/View;)V

    goto :goto_0
.end method
