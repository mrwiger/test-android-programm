.class public Lcom/samsung/multiscreen/Application;
.super Lcom/samsung/multiscreen/Channel;
.source "Application.java"


# static fields
.field public static final PROPERTY_VALUE_LIBRARY:Ljava/lang/String; = "Android SDK"

.field public static final ROUTE_APPLICATION:Ljava/lang/String; = "applications"

.field public static final ROUTE_WEBAPPLICATION:Ljava/lang/String; = "webapplication"

.field private static final TAG:Ljava/lang/String; = "Application"


# instance fields
.field private isHostDisconnected:Ljava/lang/Boolean;

.field private isStopping:Z

.field private onConnectListener:Lcom/samsung/multiscreen/Channel$OnConnectListener;

.field private final startArgs:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final webapp:Z


# direct methods
.method private constructor <init>(Lcom/samsung/multiscreen/Service;Landroid/net/Uri;Ljava/lang/String;Ljava/util/Map;)V
    .locals 2
    .param p1, "service"    # Lcom/samsung/multiscreen/Service;
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/multiscreen/Service;",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p4, "startArgs":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const/4 v1, 0x0

    .line 80
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/multiscreen/Channel;-><init>(Lcom/samsung/multiscreen/Service;Landroid/net/Uri;Ljava/lang/String;)V

    .line 65
    iput-boolean v1, p0, Lcom/samsung/multiscreen/Application;->isStopping:Z

    .line 66
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/multiscreen/Application;->isHostDisconnected:Ljava/lang/Boolean;

    .line 82
    const/4 v0, 0x0

    .line 83
    .local v0, "webapp":Z
    invoke-virtual {p2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 84
    const/4 v0, 0x1

    .line 86
    :cond_0
    const-string v1, "samsung.default.media.player"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 87
    const/4 v0, 0x1

    .line 89
    :cond_1
    iput-boolean v0, p0, Lcom/samsung/multiscreen/Application;->webapp:Z

    .line 90
    iput-object p4, p0, Lcom/samsung/multiscreen/Application;->startArgs:Ljava/util/Map;

    .line 91
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/multiscreen/Application;)Ljava/lang/Boolean;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/multiscreen/Application;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/multiscreen/Application;->isHostDisconnected:Ljava/lang/Boolean;

    return-object v0
.end method

.method static synthetic access$002(Lcom/samsung/multiscreen/Application;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/multiscreen/Application;
    .param p1, "x1"    # Ljava/lang/Boolean;

    .prologue
    .line 52
    iput-object p1, p0, Lcom/samsung/multiscreen/Application;->isHostDisconnected:Ljava/lang/Boolean;

    return-object p1
.end method

.method static synthetic access$102(Lcom/samsung/multiscreen/Application;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/multiscreen/Application;
    .param p1, "x1"    # Z

    .prologue
    .line 52
    iput-boolean p1, p0, Lcom/samsung/multiscreen/Application;->isStopping:Z

    return p1
.end method

.method static synthetic access$200(Lcom/samsung/multiscreen/Application;Lcom/samsung/multiscreen/Result;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/multiscreen/Application;
    .param p1, "x1"    # Lcom/samsung/multiscreen/Result;

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/samsung/multiscreen/Application;->realDisconnect(Lcom/samsung/multiscreen/Result;)V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/multiscreen/Application;)Lcom/samsung/multiscreen/Channel$OnConnectListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/multiscreen/Application;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/multiscreen/Application;->onConnectListener:Lcom/samsung/multiscreen/Channel$OnConnectListener;

    return-object v0
.end method

.method static create(Lcom/samsung/multiscreen/Service;Landroid/net/Uri;)Lcom/samsung/multiscreen/Application;
    .locals 3
    .param p0, "service"    # Lcom/samsung/multiscreen/Service;
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 501
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 502
    :cond_0
    new-instance v2, Ljava/lang/NullPointerException;

    invoke-direct {v2}, Ljava/lang/NullPointerException;-><init>()V

    throw v2

    .line 505
    :cond_1
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    .line 506
    .local v1, "id":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 507
    invoke-virtual {p1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v1

    .line 509
    :cond_2
    new-instance v0, Lcom/samsung/multiscreen/Application;

    const/4 v2, 0x0

    invoke-direct {v0, p0, p1, v1, v2}, Lcom/samsung/multiscreen/Application;-><init>(Lcom/samsung/multiscreen/Service;Landroid/net/Uri;Ljava/lang/String;Ljava/util/Map;)V

    .line 510
    .local v0, "application":Lcom/samsung/multiscreen/Application;
    return-object v0
.end method

.method static create(Lcom/samsung/multiscreen/Service;Landroid/net/Uri;Ljava/lang/String;Ljava/util/Map;)Lcom/samsung/multiscreen/Application;
    .locals 2
    .param p0, "service"    # Lcom/samsung/multiscreen/Service;
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/multiscreen/Service;",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/samsung/multiscreen/Application;"
        }
    .end annotation

    .prologue
    .line 515
    .local p3, "startArgs":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 516
    :cond_0
    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    throw v1

    .line 519
    :cond_1
    new-instance v0, Lcom/samsung/multiscreen/Application;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/samsung/multiscreen/Application;-><init>(Lcom/samsung/multiscreen/Service;Landroid/net/Uri;Ljava/lang/String;Ljava/util/Map;)V

    .line 520
    .local v0, "application":Lcom/samsung/multiscreen/Application;
    return-object v0
.end method

.method private doApplicationCallback(Lcom/samsung/multiscreen/Result;Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/multiscreen/Result",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 416
    .local p1, "result":Lcom/samsung/multiscreen/Result;, "Lcom/samsung/multiscreen/Result<Ljava/lang/Object;>;"
    .local p2, "message":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v2, "result"

    invoke-interface {p2, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 417
    .local v1, "obj":Ljava/lang/Object;
    const-string v2, "error"

    invoke-interface {p2, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 418
    .local v0, "errorMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    new-instance v2, Lcom/samsung/multiscreen/Application$6;

    invoke-direct {v2, p0, v0, p1, v1}, Lcom/samsung/multiscreen/Application$6;-><init>(Lcom/samsung/multiscreen/Application;Ljava/util/Map;Lcom/samsung/multiscreen/Result;Ljava/lang/Object;)V

    invoke-static {v2}, Lcom/samsung/multiscreen/util/RunUtil;->runOnUI(Ljava/lang/Runnable;)V

    .line 451
    return-void
.end method

.method private handleApplicationMessage(Ljava/util/Map;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 399
    .local p1, "message":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-virtual {p0}, Lcom/samsung/multiscreen/Application;->isDebug()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 400
    const-string v3, "Application"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "message: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 403
    :cond_0
    const-string v3, "id"

    invoke-interface {p1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 404
    .local v1, "messageId":Ljava/lang/String;
    const-string v3, "error"

    invoke-interface {p1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 407
    .local v0, "errorMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    :try_start_0
    invoke-virtual {p0, v1}, Lcom/samsung/multiscreen/Application;->getCallback(Ljava/lang/String;)Lcom/samsung/multiscreen/Result;

    move-result-object v2

    .line 408
    .local v2, "result":Lcom/samsung/multiscreen/Result;, "Lcom/samsung/multiscreen/Result<Ljava/lang/Object;>;"
    if-eqz v2, :cond_1

    .line 409
    invoke-direct {p0, v2, p1}, Lcom/samsung/multiscreen/Application;->doApplicationCallback(Lcom/samsung/multiscreen/Result;Ljava/util/Map;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 413
    .end local v2    # "result":Lcom/samsung/multiscreen/Result;, "Lcom/samsung/multiscreen/Result<Ljava/lang/Object;>;"
    :cond_1
    :goto_0
    return-void

    .line 411
    :catch_0
    move-exception v3

    goto :goto_0
.end method

.method private invokeMethod(Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Lcom/samsung/multiscreen/Result;)V
    .locals 5
    .param p1, "method"    # Ljava/lang/String;
    .param p3, "messageId"    # Ljava/lang/String;
    .param p4, "callback"    # Lcom/samsung/multiscreen/Result;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/samsung/multiscreen/Result;",
            ")V"
        }
    .end annotation

    .prologue
    .line 358
    .local p2, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-virtual {p0}, Lcom/samsung/multiscreen/Application;->isDebug()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 359
    const-string v2, "Application"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "method: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", params: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 363
    :cond_0
    invoke-super {p0}, Lcom/samsung/multiscreen/Channel;->isConnected()Z

    move-result v2

    if-nez v2, :cond_1

    .line 364
    const-string v2, "Not connected"

    invoke-static {v2}, Lcom/samsung/multiscreen/Error;->create(Ljava/lang/String;)Lcom/samsung/multiscreen/Error;

    move-result-object v2

    invoke-virtual {p0, p3, v2}, Lcom/samsung/multiscreen/Application;->handleError(Ljava/lang/String;Lcom/samsung/multiscreen/Error;)V

    .line 379
    :goto_0
    return-void

    .line 369
    :cond_1
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 371
    .local v1, "message":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v2, "method"

    invoke-interface {v1, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 372
    const-string v2, "id"

    invoke-interface {v1, v2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 373
    const-string v2, "params"

    invoke-interface {v1, v2, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 376
    invoke-static {v1}, Lcom/samsung/multiscreen/util/JSONUtil;->toJSONString(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    .line 378
    .local v0, "json":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/multiscreen/Application;->getWebSocket()Lcom/koushikdutta/async/http/WebSocket;

    move-result-object v2

    invoke-interface {v2, v0}, Lcom/koushikdutta/async/http/WebSocket;->send(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private realDisconnect(Lcom/samsung/multiscreen/Result;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/multiscreen/Result",
            "<",
            "Lcom/samsung/multiscreen/Client;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 345
    .local p1, "result":Lcom/samsung/multiscreen/Result;, "Lcom/samsung/multiscreen/Result<Lcom/samsung/multiscreen/Client;>;"
    invoke-super {p0, p1}, Lcom/samsung/multiscreen/Channel;->disconnect(Lcom/samsung/multiscreen/Result;)V

    .line 346
    return-void
.end method


# virtual methods
.method closeConnection()V
    .locals 2

    .prologue
    .line 274
    invoke-virtual {p0}, Lcom/samsung/multiscreen/Application;->getWebSocket()Lcom/koushikdutta/async/http/WebSocket;

    move-result-object v0

    .line 275
    .local v0, "websocket":Lcom/koushikdutta/async/http/WebSocket;
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/koushikdutta/async/http/WebSocket;->isOpen()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 276
    new-instance v1, Lcom/samsung/multiscreen/Application$4;

    invoke-direct {v1, p0}, Lcom/samsung/multiscreen/Application$4;-><init>(Lcom/samsung/multiscreen/Application;)V

    invoke-interface {v0, v1}, Lcom/koushikdutta/async/http/WebSocket;->setClosedCallback(Lcom/koushikdutta/async/callback/CompletedCallback;)V

    .line 282
    invoke-interface {v0}, Lcom/koushikdutta/async/http/WebSocket;->close()V

    .line 284
    :cond_0
    return-void
.end method

.method public connect(Ljava/util/Map;Lcom/samsung/multiscreen/Result;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/samsung/multiscreen/Result",
            "<",
            "Lcom/samsung/multiscreen/Client;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 235
    .local p1, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .local p2, "result":Lcom/samsung/multiscreen/Result;, "Lcom/samsung/multiscreen/Result<Lcom/samsung/multiscreen/Client;>;"
    new-instance v0, Lcom/samsung/multiscreen/Application$3;

    invoke-direct {v0, p0, p2}, Lcom/samsung/multiscreen/Application$3;-><init>(Lcom/samsung/multiscreen/Application;Lcom/samsung/multiscreen/Result;)V

    .line 270
    .local v0, "connectCallback":Lcom/samsung/multiscreen/Result;, "Lcom/samsung/multiscreen/Result<Lcom/samsung/multiscreen/Client;>;"
    invoke-super {p0, p1, v0}, Lcom/samsung/multiscreen/Channel;->connect(Ljava/util/Map;Lcom/samsung/multiscreen/Result;)V

    .line 271
    return-void
.end method

.method connectToPlay(Ljava/util/Map;Lcom/samsung/multiscreen/Result;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/samsung/multiscreen/Result",
            "<",
            "Lcom/samsung/multiscreen/Client;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 223
    .local p1, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .local p2, "result":Lcom/samsung/multiscreen/Result;, "Lcom/samsung/multiscreen/Result<Lcom/samsung/multiscreen/Client;>;"
    invoke-super {p0, p1, p2}, Lcom/samsung/multiscreen/Channel;->connect(Ljava/util/Map;Lcom/samsung/multiscreen/Result;)V

    .line 224
    return-void
.end method

.method public disconnect()V
    .locals 2

    .prologue
    .line 287
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/samsung/multiscreen/Application;->disconnect(ZLcom/samsung/multiscreen/Result;)V

    .line 288
    return-void
.end method

.method public disconnect(Lcom/samsung/multiscreen/Result;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/multiscreen/Result",
            "<",
            "Lcom/samsung/multiscreen/Client;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 292
    .local p1, "result":Lcom/samsung/multiscreen/Result;, "Lcom/samsung/multiscreen/Result<Lcom/samsung/multiscreen/Client;>;"
    const/4 v0, 0x1

    invoke-virtual {p0, v0, p1}, Lcom/samsung/multiscreen/Application;->disconnect(ZLcom/samsung/multiscreen/Result;)V

    .line 293
    return-void
.end method

.method public disconnect(ZLcom/samsung/multiscreen/Result;)V
    .locals 5
    .param p1, "stopOnDisconnect"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/samsung/multiscreen/Result",
            "<",
            "Lcom/samsung/multiscreen/Client;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "result":Lcom/samsung/multiscreen/Result;, "Lcom/samsung/multiscreen/Result<Lcom/samsung/multiscreen/Client;>;"
    const/4 v4, 0x1

    .line 303
    if-eqz p1, :cond_3

    .line 304
    invoke-virtual {p0}, Lcom/samsung/multiscreen/Application;->getClients()Lcom/samsung/multiscreen/Clients;

    move-result-object v0

    .line 305
    .local v0, "clients":Lcom/samsung/multiscreen/Clients;
    invoke-virtual {v0}, Lcom/samsung/multiscreen/Clients;->size()I

    move-result v2

    .line 306
    .local v2, "numClients":I
    invoke-virtual {v0}, Lcom/samsung/multiscreen/Clients;->me()Lcom/samsung/multiscreen/Client;

    move-result-object v1

    .line 307
    .local v1, "me":Lcom/samsung/multiscreen/Client;
    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    invoke-virtual {v0}, Lcom/samsung/multiscreen/Clients;->getHost()Lcom/samsung/multiscreen/Client;

    move-result-object v3

    if-eqz v3, :cond_0

    if-nez v1, :cond_2

    :cond_0
    if-ne v2, v4, :cond_1

    if-nez v1, :cond_2

    :cond_1
    if-nez v2, :cond_3

    .line 310
    :cond_2
    new-instance v3, Lcom/samsung/multiscreen/Application$5;

    invoke-direct {v3, p0, p2, v1}, Lcom/samsung/multiscreen/Application$5;-><init>(Lcom/samsung/multiscreen/Application;Lcom/samsung/multiscreen/Result;Lcom/samsung/multiscreen/Client;)V

    invoke-virtual {p0, v3}, Lcom/samsung/multiscreen/Application;->stop(Lcom/samsung/multiscreen/Result;)V

    .line 337
    iput-boolean v4, p0, Lcom/samsung/multiscreen/Application;->isStopping:Z

    .line 342
    .end local v0    # "clients":Lcom/samsung/multiscreen/Clients;
    .end local v1    # "me":Lcom/samsung/multiscreen/Client;
    .end local v2    # "numClients":I
    :goto_0
    return-void

    .line 341
    :cond_3
    invoke-direct {p0, p2}, Lcom/samsung/multiscreen/Application;->realDisconnect(Lcom/samsung/multiscreen/Result;)V

    goto :goto_0
.end method

.method public getInfo(Lcom/samsung/multiscreen/Result;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/multiscreen/Result",
            "<",
            "Lcom/samsung/multiscreen/ApplicationInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 117
    .local p1, "result":Lcom/samsung/multiscreen/Result;, "Lcom/samsung/multiscreen/Result<Lcom/samsung/multiscreen/ApplicationInfo;>;"
    invoke-virtual {p0}, Lcom/samsung/multiscreen/Application;->getService()Lcom/samsung/multiscreen/Service;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/multiscreen/Service;->getUri()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 119
    .local v0, "builder":Landroid/net/Uri$Builder;
    iget-boolean v3, p0, Lcom/samsung/multiscreen/Application;->webapp:Z

    if-eqz v3, :cond_1

    .line 120
    const-string v3, "webapplication"

    invoke-virtual {v0, v3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 124
    :goto_0
    const-string v3, ""

    invoke-virtual {v0, v3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 125
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    .line 126
    .local v2, "uri":Landroid/net/Uri;
    iget-boolean v3, p0, Lcom/samsung/multiscreen/Application;->securityMode:Z

    if-eqz v3, :cond_0

    .line 127
    invoke-super {p0, v2}, Lcom/samsung/multiscreen/Channel;->getSecureURL(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v2

    .line 129
    :cond_0
    new-instance v3, Lcom/samsung/multiscreen/Application$1;

    invoke-direct {v3, p0}, Lcom/samsung/multiscreen/Application$1;-><init>(Lcom/samsung/multiscreen/Application;)V

    invoke-static {v3, p1}, Lcom/samsung/multiscreen/HttpHelper;->createHttpCallback(Lcom/samsung/multiscreen/util/HttpUtil$ResultCreator;Lcom/samsung/multiscreen/Result;)Lcom/koushikdutta/async/http/AsyncHttpClient$StringCallback;

    move-result-object v1

    .line 136
    .local v1, "httpStringCallback":Lcom/koushikdutta/async/http/AsyncHttpClient$StringCallback;
    const-string v3, "GET"

    invoke-static {v2, v3, v1}, Lcom/samsung/multiscreen/util/HttpUtil;->executeJSONRequest(Landroid/net/Uri;Ljava/lang/String;Lcom/koushikdutta/async/http/AsyncHttpClient$StringCallback;)V

    .line 137
    return-void

    .line 122
    .end local v1    # "httpStringCallback":Lcom/koushikdutta/async/http/AsyncHttpClient$StringCallback;
    .end local v2    # "uri":Landroid/net/Uri;
    :cond_1
    const-string v3, "applications"

    invoke-virtual {v0, v3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/samsung/multiscreen/Application;->getUri()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_0
.end method

.method getParams()Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 209
    invoke-virtual {p0}, Lcom/samsung/multiscreen/Application;->getUri()Landroid/net/Uri;

    move-result-object v3

    .line 211
    .local v3, "uri":Landroid/net/Uri;
    const-string v1, "id"

    .line 212
    .local v1, "messageKey":Ljava/lang/String;
    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 213
    .local v0, "id":Ljava/lang/String;
    iget-boolean v4, p0, Lcom/samsung/multiscreen/Application;->webapp:Z

    if-eqz v4, :cond_0

    .line 214
    const-string v1, "url"

    .line 217
    :cond_0
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 218
    .local v2, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 219
    return-object v2
.end method

.method getStartArgs()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 94
    iget-object v0, p0, Lcom/samsung/multiscreen/Application;->startArgs:Ljava/util/Map;

    return-object v0
.end method

.method protected handleClientDisconnectMessage(Ljava/util/Map;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 455
    .local p1, "message":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v4, "data"

    invoke-interface {p1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map;

    .line 456
    .local v3, "data":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const/4 v0, 0x0

    .line 457
    .local v0, "client":Lcom/samsung/multiscreen/Client;
    if-eqz v3, :cond_0

    .line 458
    const-string v4, "id"

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 460
    .local v1, "clientId":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/multiscreen/Application;->getClients()Lcom/samsung/multiscreen/Clients;

    move-result-object v2

    .line 461
    .local v2, "clients":Lcom/samsung/multiscreen/Clients;
    invoke-virtual {v2, v1}, Lcom/samsung/multiscreen/Clients;->get(Ljava/lang/String;)Lcom/samsung/multiscreen/Client;

    move-result-object v0

    .line 463
    .end local v1    # "clientId":Ljava/lang/String;
    .end local v2    # "clients":Lcom/samsung/multiscreen/Clients;
    :cond_0
    invoke-super {p0, p1}, Lcom/samsung/multiscreen/Channel;->handleClientDisconnectMessage(Ljava/util/Map;)V

    .line 465
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/samsung/multiscreen/Client;->isHost()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 466
    iget-object v5, p0, Lcom/samsung/multiscreen/Application;->isHostDisconnected:Ljava/lang/Boolean;

    monitor-enter v5

    .line 467
    const/4 v4, 0x1

    :try_start_0
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/multiscreen/Application;->isHostDisconnected:Ljava/lang/Boolean;

    .line 468
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 470
    :cond_1
    iget-boolean v4, p0, Lcom/samsung/multiscreen/Application;->isStopping:Z

    if-nez v4, :cond_2

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/samsung/multiscreen/Client;->isHost()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 471
    const/4 v4, 0x0

    invoke-direct {p0, v4}, Lcom/samsung/multiscreen/Application;->realDisconnect(Lcom/samsung/multiscreen/Result;)V

    .line 473
    :cond_2
    return-void

    .line 468
    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4
.end method

.method protected handleMessage(Ljava/lang/String;Ljava/util/Map;[B)V
    .locals 2
    .param p1, "callbackId"    # Ljava/lang/String;
    .param p3, "payload"    # [B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;[B)V"
        }
    .end annotation

    .prologue
    .line 384
    .local p2, "message":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v1, "event"

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 390
    .local v0, "event":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 391
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/multiscreen/Channel;->handleMessage(Ljava/lang/String;Ljava/util/Map;[B)V

    .line 395
    :goto_0
    return-void

    .line 393
    :cond_0
    invoke-direct {p0, p2}, Lcom/samsung/multiscreen/Application;->handleApplicationMessage(Ljava/util/Map;)V

    goto :goto_0
.end method

.method protected handleReadyMessage(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 478
    .local p1, "message":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    iget-object v1, p0, Lcom/samsung/multiscreen/Application;->onConnectListener:Lcom/samsung/multiscreen/Channel$OnConnectListener;

    if-eqz v1, :cond_0

    .line 479
    new-instance v1, Lcom/samsung/multiscreen/Application$7;

    invoke-direct {v1, p0}, Lcom/samsung/multiscreen/Application$7;-><init>(Lcom/samsung/multiscreen/Application;)V

    invoke-static {v1}, Lcom/samsung/multiscreen/util/RunUtil;->runOnUI(Ljava/lang/Runnable;)V

    .line 488
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/multiscreen/Application;->getOnReadyListener()Lcom/samsung/multiscreen/Channel$OnReadyListener;

    move-result-object v0

    .line 489
    .local v0, "onReadyListener":Lcom/samsung/multiscreen/Channel$OnReadyListener;
    if-eqz v0, :cond_1

    .line 490
    new-instance v1, Lcom/samsung/multiscreen/Application$8;

    invoke-direct {v1, p0, v0}, Lcom/samsung/multiscreen/Application$8;-><init>(Lcom/samsung/multiscreen/Application;Lcom/samsung/multiscreen/Channel$OnReadyListener;)V

    invoke-static {v1}, Lcom/samsung/multiscreen/util/RunUtil;->runOnUI(Ljava/lang/Runnable;)V

    .line 498
    :cond_1
    return-void
.end method

.method public install(Lcom/samsung/multiscreen/Result;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/multiscreen/Result",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 182
    .local p1, "result":Lcom/samsung/multiscreen/Result;, "Lcom/samsung/multiscreen/Result<Ljava/lang/Boolean;>;"
    iget-boolean v4, p0, Lcom/samsung/multiscreen/Application;->webapp:Z

    if-eqz v4, :cond_0

    .line 183
    invoke-virtual {p0}, Lcom/samsung/multiscreen/Application;->getUID()Ljava/lang/String;

    move-result-object v2

    .line 184
    .local v2, "id":Ljava/lang/String;
    invoke-virtual {p0, v2, p1}, Lcom/samsung/multiscreen/Application;->registerCallback(Ljava/lang/String;Lcom/samsung/multiscreen/Result;)V

    .line 185
    const-string v4, "Unsupported method"

    invoke-static {v4}, Lcom/samsung/multiscreen/Error;->create(Ljava/lang/String;)Lcom/samsung/multiscreen/Error;

    move-result-object v4

    invoke-virtual {p0, v2, v4}, Lcom/samsung/multiscreen/Application;->handleError(Ljava/lang/String;Lcom/samsung/multiscreen/Error;)V

    .line 206
    .end local v2    # "id":Ljava/lang/String;
    :goto_0
    return-void

    .line 187
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/multiscreen/Application;->getService()Lcom/samsung/multiscreen/Service;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/multiscreen/Service;->getUri()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v4

    const-string v5, "applications"

    .line 188
    invoke-virtual {v4, v5}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    .line 189
    invoke-virtual {p0}, Lcom/samsung/multiscreen/Application;->getUri()Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    const-string v5, ""

    .line 190
    invoke-virtual {v4, v5}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 191
    .local v0, "builder":Landroid/net/Uri$Builder;
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    .line 192
    .local v3, "uri":Landroid/net/Uri;
    iget-boolean v4, p0, Lcom/samsung/multiscreen/Application;->securityMode:Z

    if-eqz v4, :cond_1

    .line 193
    invoke-super {p0, v3}, Lcom/samsung/multiscreen/Channel;->getSecureURL(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v3

    .line 196
    :cond_1
    new-instance v4, Lcom/samsung/multiscreen/Application$2;

    invoke-direct {v4, p0}, Lcom/samsung/multiscreen/Application$2;-><init>(Lcom/samsung/multiscreen/Application;)V

    invoke-static {v4, p1}, Lcom/samsung/multiscreen/HttpHelper;->createHttpCallback(Lcom/samsung/multiscreen/util/HttpUtil$ResultCreator;Lcom/samsung/multiscreen/Result;)Lcom/koushikdutta/async/http/AsyncHttpClient$StringCallback;

    move-result-object v1

    .line 204
    .local v1, "httpStringCallback":Lcom/koushikdutta/async/http/AsyncHttpClient$StringCallback;
    const-string v4, "PUT"

    invoke-static {v3, v4, v1}, Lcom/samsung/multiscreen/util/HttpUtil;->executeJSONRequest(Landroid/net/Uri;Ljava/lang/String;Lcom/koushikdutta/async/http/AsyncHttpClient$StringCallback;)V

    goto :goto_0
.end method

.method invokeMethod(Ljava/lang/String;Ljava/util/Map;Lcom/samsung/multiscreen/Result;)V
    .locals 1
    .param p1, "method"    # Ljava/lang/String;
    .param p3, "callback"    # Lcom/samsung/multiscreen/Result;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Lcom/samsung/multiscreen/Result;",
            ")V"
        }
    .end annotation

    .prologue
    .line 350
    .local p2, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-virtual {p0}, Lcom/samsung/multiscreen/Application;->getUID()Ljava/lang/String;

    move-result-object v0

    .line 351
    .local v0, "messageId":Ljava/lang/String;
    invoke-virtual {p0, v0, p3}, Lcom/samsung/multiscreen/Application;->registerCallback(Ljava/lang/String;Lcom/samsung/multiscreen/Result;)V

    .line 353
    invoke-direct {p0, p1, p2, v0, p3}, Lcom/samsung/multiscreen/Application;->invokeMethod(Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Lcom/samsung/multiscreen/Result;)V

    .line 354
    return-void
.end method

.method public isConnected()Z
    .locals 1

    .prologue
    .line 104
    invoke-super {p0}, Lcom/samsung/multiscreen/Channel;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/multiscreen/Application;->connected:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/multiscreen/Application;->isHostDisconnected:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isWebapp()Z
    .locals 1

    .prologue
    .line 73
    iget-boolean v0, p0, Lcom/samsung/multiscreen/Application;->webapp:Z

    return v0
.end method

.method public setOnConnectListener(Lcom/samsung/multiscreen/Channel$OnConnectListener;)V
    .locals 0
    .param p1, "onConnectListener"    # Lcom/samsung/multiscreen/Channel$OnConnectListener;

    .prologue
    .line 62
    iput-object p1, p0, Lcom/samsung/multiscreen/Application;->onConnectListener:Lcom/samsung/multiscreen/Channel$OnConnectListener;

    return-void
.end method

.method public start(Lcom/samsung/multiscreen/Result;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/multiscreen/Result",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 146
    .local p1, "result":Lcom/samsung/multiscreen/Result;, "Lcom/samsung/multiscreen/Result<Ljava/lang/Boolean;>;"
    invoke-virtual {p0}, Lcom/samsung/multiscreen/Application;->getParams()Ljava/util/Map;

    move-result-object v0

    .line 148
    .local v0, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v1, "os"

    sget-object v2, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 149
    const-string v1, "library"

    const-string v2, "Android SDK"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 150
    const-string v1, "version"

    const-string v2, "2.4.1"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 151
    const-string v1, "modelNumber"

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 155
    iget-object v1, p0, Lcom/samsung/multiscreen/Application;->startArgs:Ljava/util/Map;

    if-eqz v1, :cond_0

    .line 156
    const-string v1, "data"

    iget-object v2, p0, Lcom/samsung/multiscreen/Application;->startArgs:Ljava/util/Map;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 159
    :cond_0
    iget-boolean v1, p0, Lcom/samsung/multiscreen/Application;->webapp:Z

    if-eqz v1, :cond_1

    const-string v1, "ms.webapplication.start"

    :goto_0
    invoke-virtual {p0, v1, v0, p1}, Lcom/samsung/multiscreen/Application;->invokeMethod(Ljava/lang/String;Ljava/util/Map;Lcom/samsung/multiscreen/Result;)V

    .line 160
    return-void

    .line 159
    :cond_1
    const-string v1, "ms.application.start"

    goto :goto_0
.end method

.method public stop(Lcom/samsung/multiscreen/Result;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/multiscreen/Result",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 169
    .local p1, "result":Lcom/samsung/multiscreen/Result;, "Lcom/samsung/multiscreen/Result<Ljava/lang/Boolean;>;"
    invoke-virtual {p0}, Lcom/samsung/multiscreen/Application;->getParams()Ljava/util/Map;

    move-result-object v0

    .line 170
    .local v0, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    iget-boolean v1, p0, Lcom/samsung/multiscreen/Application;->webapp:Z

    if-eqz v1, :cond_0

    const-string v1, "ms.webapplication.stop"

    :goto_0
    invoke-virtual {p0, v1, v0, p1}, Lcom/samsung/multiscreen/Application;->invokeMethod(Ljava/lang/String;Ljava/util/Map;Lcom/samsung/multiscreen/Result;)V

    .line 171
    return-void

    .line 170
    :cond_0
    const-string v1, "ms.application.stop"

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 51
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Application(super="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-super {p0}, Lcom/samsung/multiscreen/Channel;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", onConnectListener="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/multiscreen/Application;->onConnectListener:Lcom/samsung/multiscreen/Channel$OnConnectListener;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isStopping="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/samsung/multiscreen/Application;->isStopping:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isHostDisconnected="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/multiscreen/Application;->isHostDisconnected:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", webapp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/multiscreen/Application;->isWebapp()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", startArgs="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/multiscreen/Application;->getStartArgs()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
