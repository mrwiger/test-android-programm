.class Lru/cn/tv/stb/StbActivity$13;
.super Ljava/lang/Object;
.source "StbActivity.java"

# interfaces
.implements Lru/cn/peersay/controllers/PlayContentController$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/tv/stb/StbActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/tv/stb/StbActivity;


# direct methods
.method constructor <init>(Lru/cn/tv/stb/StbActivity;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 503
    iput-object p1, p0, Lru/cn/tv/stb/StbActivity$13;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNext()V
    .locals 1

    .prologue
    .line 506
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$13;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$2700(Lru/cn/tv/stb/StbActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 507
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$13;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$100(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/player/controller/StbPlayerController;

    move-result-object v0

    invoke-virtual {v0}, Lru/cn/tv/player/controller/StbPlayerController;->nextMedia()V

    .line 509
    :cond_0
    return-void
.end method

.method public onPrevious()V
    .locals 1

    .prologue
    .line 513
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$13;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$2700(Lru/cn/tv/stb/StbActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 514
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$13;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$100(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/player/controller/StbPlayerController;

    move-result-object v0

    invoke-virtual {v0}, Lru/cn/tv/player/controller/StbPlayerController;->prevMedia()V

    .line 516
    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 520
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$13;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$000(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/player/SimplePlayerFragment;

    move-result-object v0

    invoke-virtual {v0}, Lru/cn/tv/player/SimplePlayerFragment;->stop()V

    .line 521
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$13;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-virtual {v0}, Lru/cn/tv/stb/StbActivity;->finish()V

    .line 522
    return-void
.end method
