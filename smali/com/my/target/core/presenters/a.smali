.class public final Lcom/my/target/core/presenters/a;
.super Ljava/lang/Object;
.source "NativeViewPresenter.java"

# interfaces
.implements Lcom/my/target/core/presenters/b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/core/presenters/a$d;,
        Lcom/my/target/core/presenters/a$c;,
        Lcom/my/target/core/presenters/a$b;,
        Lcom/my/target/core/presenters/a$a;
    }
.end annotation


# static fields
.field static L:J

.field private static M:Landroid/os/Handler;


# instance fields
.field private final N:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/my/target/dm;",
            ">;"
        }
    .end annotation
.end field

.field private final O:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/my/target/dm;",
            ">;"
        }
    .end annotation
.end field

.field private final P:Lcom/my/target/cm;

.field private final Q:Lcom/my/target/dj;

.field private final R:Ljava/lang/String;

.field private final S:Lcom/my/target/core/presenters/a$d;

.field private final T:Lcom/my/target/di;

.field private final U:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/my/target/core/models/banners/c;",
            ">;"
        }
    .end annotation
.end field

.field private final V:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/my/target/core/models/banners/c;",
            ">;"
        }
    .end annotation
.end field

.field private W:Lcom/my/target/dh;

.field private X:Lcom/my/target/dh;

.field private Z:Lcom/my/target/core/presenters/b$a;

.field private a_:Landroid/view/View$OnClickListener;

.field private aa:Z

.field private ab:Z

.field private ac:J

.field private ad:J

.field private ae:J

.field private af:J

.field private ag:I

.field private ah:I

.field private final context:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 41
    const-wide/16 v0, 0x190

    sput-wide v0, Lcom/my/target/core/presenters/a;->L:J

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 162
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 137
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/my/target/core/presenters/a;->N:Ljava/util/ArrayList;

    .line 138
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/my/target/core/presenters/a;->O:Ljava/util/ArrayList;

    .line 145
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/my/target/core/presenters/a;->U:Ljava/util/List;

    .line 146
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/my/target/core/presenters/a;->V:Ljava/util/List;

    .line 163
    iput-object p2, p0, Lcom/my/target/core/presenters/a;->context:Landroid/content/Context;

    .line 164
    new-instance v0, Lcom/my/target/di;

    invoke-direct {v0, p2}, Lcom/my/target/di;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/core/presenters/a;->T:Lcom/my/target/di;

    .line 165
    new-instance v0, Lcom/my/target/dj;

    invoke-direct {v0, p2}, Lcom/my/target/dj;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/core/presenters/a;->Q:Lcom/my/target/dj;

    .line 166
    iget-object v0, p0, Lcom/my/target/core/presenters/a;->Q:Lcom/my/target/dj;

    new-instance v1, Lcom/my/target/core/presenters/a$c;

    invoke-direct {v1, p0, v2}, Lcom/my/target/core/presenters/a$c;-><init>(Lcom/my/target/core/presenters/a;B)V

    invoke-virtual {v0, v1}, Lcom/my/target/dj;->setAnimationEndListener(Lcom/my/target/dj$a;)V

    .line 167
    iget-object v0, p0, Lcom/my/target/core/presenters/a;->T:Lcom/my/target/di;

    iget-object v1, p0, Lcom/my/target/core/presenters/a;->Q:Lcom/my/target/dj;

    invoke-virtual {v0, v1}, Lcom/my/target/di;->addView(Landroid/view/View;)V

    .line 168
    iput-object p1, p0, Lcom/my/target/core/presenters/a;->R:Ljava/lang/String;

    .line 169
    invoke-static {p2}, Lcom/my/target/cm;->x(Landroid/content/Context;)Lcom/my/target/cm;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/core/presenters/a;->P:Lcom/my/target/cm;

    .line 170
    new-instance v0, Lcom/my/target/core/presenters/a$d;

    invoke-direct {v0, p0, v2}, Lcom/my/target/core/presenters/a$d;-><init>(Lcom/my/target/core/presenters/a;B)V

    iput-object v0, p0, Lcom/my/target/core/presenters/a;->S:Lcom/my/target/core/presenters/a$d;

    .line 171
    return-void
.end method

.method public static B()J
    .locals 2

    .prologue
    .line 51
    sget-wide v0, Lcom/my/target/core/presenters/a;->L:J

    return-wide v0
.end method

.method private D()V
    .locals 2

    .prologue
    .line 407
    iget-object v0, p0, Lcom/my/target/core/presenters/a;->Q:Lcom/my/target/dj;

    invoke-virtual {v0}, Lcom/my/target/dj;->getDisplayedBannerNumber()I

    move-result v0

    .line 408
    iget-object v1, p0, Lcom/my/target/core/presenters/a;->N:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 410
    iget-object v1, p0, Lcom/my/target/core/presenters/a;->N:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/dm;

    invoke-interface {v0}, Lcom/my/target/dm;->stop()V

    .line 412
    :cond_0
    return-void
.end method

.method private E()V
    .locals 2

    .prologue
    .line 416
    iget-object v0, p0, Lcom/my/target/core/presenters/a;->Q:Lcom/my/target/dj;

    invoke-virtual {v0}, Lcom/my/target/dj;->getDisplayedBannerNumber()I

    move-result v0

    .line 417
    iget-object v1, p0, Lcom/my/target/core/presenters/a;->N:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 419
    iget-object v1, p0, Lcom/my/target/core/presenters/a;->N:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/dm;

    invoke-interface {v0}, Lcom/my/target/dm;->start()V

    .line 421
    :cond_0
    return-void
.end method

.method private F()V
    .locals 2

    .prologue
    .line 459
    iget-object v0, p0, Lcom/my/target/core/presenters/a;->R:Ljava/lang/String;

    const-string v1, "standard_300x250"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 461
    sget-object v0, Lcom/my/target/core/presenters/a;->M:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 463
    sget-object v0, Lcom/my/target/core/presenters/a;->M:Landroid/os/Handler;

    iget-object v1, p0, Lcom/my/target/core/presenters/a;->S:Lcom/my/target/core/presenters/a$d;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 466
    :cond_0
    return-void
.end method

.method private G()Z
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 488
    :goto_0
    iget-wide v4, p0, Lcom/my/target/core/presenters/a;->af:J

    cmp-long v0, v4, v10

    if-lez v0, :cond_0

    .line 490
    iget v0, p0, Lcom/my/target/core/presenters/a;->ah:I

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v4, v0

    .line 491
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iget-wide v8, p0, Lcom/my/target/core/presenters/a;->ae:J

    sub-long/2addr v6, v8

    sub-long/2addr v4, v6

    iput-wide v4, p0, Lcom/my/target/core/presenters/a;->af:J

    .line 494
    :cond_0
    iget-boolean v0, p0, Lcom/my/target/core/presenters/a;->aa:Z

    if-eqz v0, :cond_5

    .line 497
    iget-object v0, p0, Lcom/my/target/core/presenters/a;->V:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/my/target/core/presenters/a;->X:Lcom/my/target/dh;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/my/target/core/presenters/a;->O:Ljava/util/ArrayList;

    .line 499
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/my/target/core/presenters/a;->Q:Lcom/my/target/dj;

    .line 500
    invoke-virtual {v0}, Lcom/my/target/dj;->getBackgroundFlipper()Landroid/widget/ViewFlipper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ViewFlipper;->getChildCount()I

    move-result v0

    if-nez v0, :cond_2

    .line 502
    :cond_1
    const-string v0, "something gone wrong on the new ad switching"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 503
    iput-boolean v2, p0, Lcom/my/target/core/presenters/a;->aa:Z

    goto :goto_0

    .line 506
    :cond_2
    invoke-direct {p0}, Lcom/my/target/core/presenters/a;->D()V

    .line 507
    invoke-direct {p0}, Lcom/my/target/core/presenters/a;->F()V

    .line 508
    iget-object v0, p0, Lcom/my/target/core/presenters/a;->X:Lcom/my/target/dh;

    iput-object v0, p0, Lcom/my/target/core/presenters/a;->W:Lcom/my/target/dh;

    .line 509
    iget-object v0, p0, Lcom/my/target/core/presenters/a;->U:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 510
    iget-object v0, p0, Lcom/my/target/core/presenters/a;->U:Ljava/util/List;

    iget-object v3, p0, Lcom/my/target/core/presenters/a;->V:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 512
    iget-object v0, p0, Lcom/my/target/core/presenters/a;->N:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/dm;

    .line 514
    invoke-interface {v0}, Lcom/my/target/dm;->stop()V

    goto :goto_1

    .line 516
    :cond_3
    iget-object v0, p0, Lcom/my/target/core/presenters/a;->N:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 517
    iget-object v0, p0, Lcom/my/target/core/presenters/a;->N:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/my/target/core/presenters/a;->O:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 518
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/my/target/core/presenters/a;->X:Lcom/my/target/dh;

    .line 519
    iget-object v0, p0, Lcom/my/target/core/presenters/a;->O:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 520
    iget-object v0, p0, Lcom/my/target/core/presenters/a;->Q:Lcom/my/target/dj;

    invoke-virtual {v0}, Lcom/my/target/dj;->K()V

    .line 521
    iget-object v0, p0, Lcom/my/target/core/presenters/a;->U:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/core/models/banners/c;

    .line 522
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/c;->getTimeout()I

    move-result v0

    iput v0, p0, Lcom/my/target/core/presenters/a;->ag:I

    .line 524
    iget-object v0, p0, Lcom/my/target/core/presenters/a;->W:Lcom/my/target/dh;

    if-eqz v0, :cond_4

    .line 526
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/my/target/core/presenters/a;->ae:J

    .line 527
    iget-object v0, p0, Lcom/my/target/core/presenters/a;->W:Lcom/my/target/dh;

    invoke-virtual {v0}, Lcom/my/target/dh;->x()I

    move-result v0

    iput v0, p0, Lcom/my/target/core/presenters/a;->ah:I

    .line 528
    iget v0, p0, Lcom/my/target/core/presenters/a;->ah:I

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v4, v0

    iput-wide v4, p0, Lcom/my/target/core/presenters/a;->af:J

    .line 531
    :cond_4
    iget v0, p0, Lcom/my/target/core/presenters/a;->ag:I

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v4, v0

    invoke-direct {p0, v4, v5}, Lcom/my/target/core/presenters/a;->a(J)V

    .line 532
    iput-boolean v2, p0, Lcom/my/target/core/presenters/a;->aa:Z

    .line 533
    iput-boolean v2, p0, Lcom/my/target/core/presenters/a;->ab:Z

    move v0, v1

    .line 590
    :goto_2
    return v0

    .line 536
    :cond_5
    iget-object v0, p0, Lcom/my/target/core/presenters/a;->Q:Lcom/my/target/dj;

    invoke-virtual {v0}, Lcom/my/target/dj;->L()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 538
    iget-wide v4, p0, Lcom/my/target/core/presenters/a;->af:J

    cmp-long v0, v4, v10

    if-gtz v0, :cond_8

    .line 540
    iget-object v0, p0, Lcom/my/target/core/presenters/a;->Z:Lcom/my/target/core/presenters/b$a;

    if-eqz v0, :cond_6

    .line 542
    iget-object v0, p0, Lcom/my/target/core/presenters/a;->Z:Lcom/my/target/core/presenters/b$a;

    invoke-interface {v0}, Lcom/my/target/core/presenters/b$a;->f()V

    .line 555
    :cond_6
    :goto_3
    iget-object v0, p0, Lcom/my/target/core/presenters/a;->W:Lcom/my/target/dh;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/my/target/core/presenters/a;->W:Lcom/my/target/dh;

    invoke-virtual {v0}, Lcom/my/target/dh;->z()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 557
    invoke-direct {p0}, Lcom/my/target/core/presenters/a;->F()V

    .line 558
    iget-object v0, p0, Lcom/my/target/core/presenters/a;->N:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/dm;

    invoke-interface {v0}, Lcom/my/target/dm;->P()V

    .line 559
    iget-object v0, p0, Lcom/my/target/core/presenters/a;->Q:Lcom/my/target/dj;

    invoke-virtual {v0}, Lcom/my/target/dj;->N()V

    .line 560
    iput-boolean v2, p0, Lcom/my/target/core/presenters/a;->ab:Z

    .line 561
    iget-object v0, p0, Lcom/my/target/core/presenters/a;->U:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_7

    .line 563
    iget-object v0, p0, Lcom/my/target/core/presenters/a;->U:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/core/models/banners/c;

    invoke-virtual {v0}, Lcom/my/target/core/models/banners/c;->getTimeout()I

    move-result v0

    iput v0, p0, Lcom/my/target/core/presenters/a;->ag:I

    .line 564
    iget v0, p0, Lcom/my/target/core/presenters/a;->ag:I

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v2, v0

    invoke-direct {p0, v2, v3}, Lcom/my/target/core/presenters/a;->a(J)V

    :cond_7
    move v0, v1

    .line 566
    goto :goto_2

    .line 547
    :cond_8
    invoke-direct {p0}, Lcom/my/target/core/presenters/a;->H()Z

    move-result v0

    if-nez v0, :cond_6

    .line 549
    sget-object v0, Lcom/my/target/core/presenters/a;->M:Landroid/os/Handler;

    if-eqz v0, :cond_6

    .line 551
    sget-object v0, Lcom/my/target/core/presenters/a;->M:Landroid/os/Handler;

    iget-object v3, p0, Lcom/my/target/core/presenters/a;->S:Lcom/my/target/core/presenters/a$d;

    iget-wide v4, p0, Lcom/my/target/core/presenters/a;->af:J

    invoke-virtual {v0, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_3

    .line 571
    :cond_9
    iget-object v0, p0, Lcom/my/target/core/presenters/a;->Q:Lcom/my/target/dj;

    invoke-virtual {v0}, Lcom/my/target/dj;->getDisplayedBannerNumber()I

    move-result v0

    add-int/lit8 v3, v0, 0x1

    .line 572
    iput-boolean v2, p0, Lcom/my/target/core/presenters/a;->ab:Z

    .line 573
    invoke-direct {p0}, Lcom/my/target/core/presenters/a;->D()V

    .line 575
    iget-object v0, p0, Lcom/my/target/core/presenters/a;->N:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v3, v0, :cond_a

    .line 577
    iget-object v0, p0, Lcom/my/target/core/presenters/a;->N:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/dm;

    invoke-interface {v0}, Lcom/my/target/dm;->P()V

    .line 580
    :cond_a
    iget-object v0, p0, Lcom/my/target/core/presenters/a;->Q:Lcom/my/target/dj;

    invoke-virtual {v0}, Lcom/my/target/dj;->M()V

    .line 582
    iget-object v0, p0, Lcom/my/target/core/presenters/a;->U:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_b

    .line 584
    invoke-direct {p0}, Lcom/my/target/core/presenters/a;->F()V

    .line 585
    iget-object v0, p0, Lcom/my/target/core/presenters/a;->U:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/core/models/banners/c;

    invoke-virtual {v0}, Lcom/my/target/core/models/banners/c;->getTimeout()I

    move-result v0

    iput v0, p0, Lcom/my/target/core/presenters/a;->ag:I

    .line 586
    iget v0, p0, Lcom/my/target/core/presenters/a;->ag:I

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v2, v0

    invoke-direct {p0, v2, v3}, Lcom/my/target/core/presenters/a;->a(J)V

    :cond_b
    move v0, v1

    .line 588
    goto/16 :goto_2

    :cond_c
    move v0, v2

    .line 590
    goto/16 :goto_2
.end method

.method private H()Z
    .locals 3

    .prologue
    .line 595
    iget-object v0, p0, Lcom/my/target/core/presenters/a;->Q:Lcom/my/target/dj;

    invoke-virtual {v0}, Lcom/my/target/dj;->getCurrentFlipper()Landroid/widget/ViewFlipper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ViewFlipper;->getDisplayedChild()I

    move-result v0

    .line 596
    const/4 v1, 0x0

    .line 597
    if-ltz v0, :cond_0

    iget-object v2, p0, Lcom/my/target/core/presenters/a;->N:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 599
    iget-object v2, p0, Lcom/my/target/core/presenters/a;->N:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/dm;

    invoke-interface {v0}, Lcom/my/target/dm;->H()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 601
    const/4 v0, 0x1

    .line 604
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method static synthetic a(Lcom/my/target/core/presenters/a;J)J
    .locals 1

    .prologue
    .line 37
    iput-wide p1, p0, Lcom/my/target/core/presenters/a;->ac:J

    return-wide p1
.end method

.method public static a(Ljava/lang/String;Landroid/content/Context;)Lcom/my/target/core/presenters/a;
    .locals 1

    .prologue
    .line 46
    new-instance v0, Lcom/my/target/core/presenters/a;

    invoke-direct {v0, p0, p1}, Lcom/my/target/core/presenters/a;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    return-object v0
.end method

.method static synthetic a(Lcom/my/target/core/presenters/a;)Lcom/my/target/dj;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/my/target/core/presenters/a;->Q:Lcom/my/target/dj;

    return-object v0
.end method

.method private a(J)V
    .locals 3

    .prologue
    .line 447
    iget-object v0, p0, Lcom/my/target/core/presenters/a;->R:Ljava/lang/String;

    const-string v1, "standard_300x250"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 449
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/my/target/core/presenters/a;->ac:J

    .line 450
    sget-object v0, Lcom/my/target/core/presenters/a;->M:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 452
    sget-object v0, Lcom/my/target/core/presenters/a;->M:Landroid/os/Handler;

    iget-object v1, p0, Lcom/my/target/core/presenters/a;->S:Lcom/my/target/core/presenters/a$d;

    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 455
    :cond_0
    return-void
.end method

.method private a(Lcom/my/target/core/models/banners/c;Lcom/my/target/dm;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 343
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/c;->getTitle()Ljava/lang/String;

    move-result-object v0

    .line 344
    invoke-interface {p2}, Lcom/my/target/dm;->getDisclaimerTextView()Landroid/widget/TextView;

    move-result-object v1

    .line 345
    if-eqz v1, :cond_0

    .line 347
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/c;->getDisclaimer()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 349
    :cond_0
    invoke-interface {p2}, Lcom/my/target/dm;->getDescriptionTextView()Landroid/widget/TextView;

    move-result-object v1

    .line 350
    if-eqz v1, :cond_1

    .line 352
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/c;->getDescription()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 355
    :cond_1
    invoke-interface {p2}, Lcom/my/target/dm;->getTitleTextView()Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 356
    invoke-interface {p2}, Lcom/my/target/dm;->getCtaButton()Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {p1}, Lcom/my/target/core/models/banners/c;->getCtaText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 358
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/c;->getAgeRestrictions()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 360
    invoke-interface {p2}, Lcom/my/target/dm;->getAgeRestrictionsView()Lcom/my/target/bu;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/my/target/bu;->setVisibility(I)V

    .line 368
    :goto_0
    invoke-interface {p2}, Lcom/my/target/dm;->getDomainTextView()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p1}, Lcom/my/target/core/models/banners/c;->getDomain()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 370
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/c;->getImage()Lcom/my/target/common/models/ImageData;

    move-result-object v0

    .line 371
    const-string v1, "banner"

    invoke-virtual {p1}, Lcom/my/target/core/models/banners/c;->getType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 373
    invoke-interface {p2}, Lcom/my/target/dm;->getBannerImage()Lcom/my/target/bx;

    move-result-object v1

    .line 374
    if-eqz v0, :cond_2

    if-eqz v1, :cond_2

    .line 376
    invoke-virtual {v1, v0}, Lcom/my/target/bx;->setImageData(Lcom/my/target/common/models/ImageData;)V

    .line 393
    :cond_2
    :goto_1
    invoke-interface {p2}, Lcom/my/target/dm;->getRatingTextView()Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p1}, Lcom/my/target/core/models/banners/c;->getVotes()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 394
    invoke-interface {p2}, Lcom/my/target/dm;->getStarsRatingView()Lcom/my/target/ca;

    move-result-object v0

    invoke-virtual {p1}, Lcom/my/target/core/models/banners/c;->getRating()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/my/target/ca;->setRating(F)V

    .line 396
    invoke-interface {p2, p1}, Lcom/my/target/dm;->b(Lcom/my/target/ah;)V

    .line 398
    new-instance v0, Lcom/my/target/core/presenters/a$a;

    invoke-direct {v0, p0, v3}, Lcom/my/target/core/presenters/a$a;-><init>(Lcom/my/target/core/presenters/a;B)V

    iput-object v0, p0, Lcom/my/target/core/presenters/a;->a_:Landroid/view/View$OnClickListener;

    .line 400
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/c;->getClickArea()Lcom/my/target/af;

    move-result-object v0

    .line 401
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/c;->getType()Ljava/lang/String;

    move-result-object v1

    const-string v2, "banner"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    iget-object v2, p0, Lcom/my/target/core/presenters/a;->a_:Landroid/view/View$OnClickListener;

    .line 400
    invoke-interface {p2, v0, v1, v2}, Lcom/my/target/dm;->a(Lcom/my/target/af;ZLandroid/view/View$OnClickListener;)V

    .line 403
    return-void

    .line 364
    :cond_3
    invoke-interface {p2}, Lcom/my/target/dm;->getAgeRestrictionsView()Lcom/my/target/bu;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/my/target/bu;->setVisibility(I)V

    .line 365
    invoke-interface {p2}, Lcom/my/target/dm;->getAgeRestrictionsView()Lcom/my/target/bu;

    move-result-object v0

    invoke-virtual {p1}, Lcom/my/target/core/models/banners/c;->getAgeRestrictions()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/my/target/bu;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 381
    :cond_4
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/c;->getIcon()Lcom/my/target/common/models/ImageData;

    move-result-object v1

    .line 382
    if-eqz v1, :cond_5

    .line 384
    invoke-interface {p2}, Lcom/my/target/dm;->getIconImage()Lcom/my/target/bx;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/my/target/bx;->setImageData(Lcom/my/target/common/models/ImageData;)V

    .line 386
    :cond_5
    invoke-interface {p2}, Lcom/my/target/dm;->getMainImage()Lcom/my/target/bx;

    move-result-object v1

    .line 387
    if-eqz v0, :cond_2

    if-eqz v1, :cond_2

    .line 389
    invoke-virtual {v1, v0}, Lcom/my/target/bx;->setImageData(Lcom/my/target/common/models/ImageData;)V

    goto :goto_1
.end method

.method private static a(Lcom/my/target/dg;Ljava/util/List;Lcom/my/target/cm;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/my/target/dg;",
            "Ljava/util/List",
            "<",
            "Lcom/my/target/dm;",
            ">;",
            "Lcom/my/target/cm;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 56
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/dm;

    .line 58
    invoke-interface {v0}, Lcom/my/target/dm;->getTitleTextView()Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {p0}, Lcom/my/target/dg;->getTitleColor()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 59
    invoke-virtual {p0}, Lcom/my/target/dg;->h()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 61
    invoke-interface {v0}, Lcom/my/target/dm;->getTitleTextView()Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2, v6, v7}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 68
    :goto_1
    invoke-interface {v0}, Lcom/my/target/dm;->getDomainTextView()Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {p0}, Lcom/my/target/dg;->s()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 69
    invoke-virtual {p0}, Lcom/my/target/dg;->j()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 71
    invoke-interface {v0}, Lcom/my/target/dm;->getDomainTextView()Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2, v6, v7}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 78
    :goto_2
    invoke-interface {v0}, Lcom/my/target/dm;->getRatingTextView()Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {p0}, Lcom/my/target/dg;->t()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 79
    invoke-virtual {p0}, Lcom/my/target/dg;->k()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 81
    invoke-interface {v0}, Lcom/my/target/dm;->getRatingTextView()Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2, v6, v7}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 88
    :goto_3
    invoke-virtual {p0}, Lcom/my/target/dg;->getBackgroundColor()I

    move-result v2

    invoke-virtual {p0}, Lcom/my/target/dg;->n()I

    move-result v3

    invoke-interface {v0, v2, v3}, Lcom/my/target/dm;->b(II)V

    .line 90
    invoke-interface {v0}, Lcom/my/target/dm;->getAgeRestrictionsView()Lcom/my/target/bu;

    move-result-object v2

    invoke-virtual {p0}, Lcom/my/target/dg;->p()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/my/target/bu;->setTextColor(I)V

    .line 91
    invoke-interface {v0}, Lcom/my/target/dm;->getAgeRestrictionsView()Lcom/my/target/bu;

    move-result-object v2

    invoke-virtual {p0}, Lcom/my/target/dg;->q()I

    move-result v3

    invoke-virtual {v2, v7, v3}, Lcom/my/target/bu;->c(II)V

    .line 92
    invoke-interface {v0}, Lcom/my/target/dm;->getAgeRestrictionsView()Lcom/my/target/bu;

    move-result-object v2

    invoke-virtual {p0}, Lcom/my/target/dg;->o()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/my/target/bu;->setBackgroundColor(I)V

    .line 94
    invoke-interface {v0}, Lcom/my/target/dm;->getCtaButton()Landroid/widget/Button;

    move-result-object v2

    .line 95
    invoke-virtual {p0}, Lcom/my/target/dg;->getCtaButtonColor()I

    move-result v3

    .line 96
    invoke-virtual {p0}, Lcom/my/target/dg;->getCtaButtonTouchColor()I

    move-result v4

    const/4 v5, 0x2

    invoke-virtual {p2, v5}, Lcom/my/target/cm;->n(I)I

    move-result v5

    .line 94
    invoke-static {v2, v3, v4, v5}, Lcom/my/target/cm;->a(Landroid/view/View;III)V

    .line 97
    invoke-interface {v0}, Lcom/my/target/dm;->getCtaButton()Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {p0}, Lcom/my/target/dg;->getCtaButtonTextColor()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setTextColor(I)V

    .line 98
    invoke-virtual {p0}, Lcom/my/target/dg;->m()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 100
    invoke-interface {v0}, Lcom/my/target/dm;->getCtaButton()Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2, v6, v7}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 107
    :goto_4
    invoke-interface {v0}, Lcom/my/target/dm;->getDescriptionTextView()Landroid/widget/TextView;

    move-result-object v2

    .line 108
    if-eqz v2, :cond_1

    .line 110
    invoke-virtual {p0}, Lcom/my/target/dg;->r()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 111
    invoke-virtual {p0}, Lcom/my/target/dg;->i()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 113
    invoke-virtual {v2, v6, v7}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 121
    :cond_1
    :goto_5
    invoke-interface {v0}, Lcom/my/target/dm;->getDisclaimerTextView()Landroid/widget/TextView;

    move-result-object v0

    .line 122
    if-eqz v0, :cond_0

    .line 124
    invoke-virtual {p0}, Lcom/my/target/dg;->u()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 125
    invoke-virtual {p0}, Lcom/my/target/dg;->l()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 127
    invoke-virtual {v0, v6, v7}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    goto/16 :goto_0

    .line 65
    :cond_2
    invoke-interface {v0}, Lcom/my/target/dm;->getTitleTextView()Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2, v6, v8}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    goto/16 :goto_1

    .line 75
    :cond_3
    invoke-interface {v0}, Lcom/my/target/dm;->getDomainTextView()Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2, v6, v8}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    goto/16 :goto_2

    .line 85
    :cond_4
    invoke-interface {v0}, Lcom/my/target/dm;->getRatingTextView()Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2, v6, v8}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    goto/16 :goto_3

    .line 104
    :cond_5
    invoke-interface {v0}, Lcom/my/target/dm;->getCtaButton()Landroid/widget/Button;

    move-result-object v2

    invoke-virtual {v2, v6, v8}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;I)V

    goto :goto_4

    .line 117
    :cond_6
    invoke-virtual {v2, v6, v8}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    goto :goto_5

    .line 131
    :cond_7
    invoke-virtual {v0, v6, v8}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    goto/16 :goto_0

    .line 135
    :cond_8
    return-void
.end method

.method static synthetic b(Lcom/my/target/core/presenters/a;J)J
    .locals 1

    .prologue
    .line 37
    iput-wide p1, p0, Lcom/my/target/core/presenters/a;->ad:J

    return-wide p1
.end method

.method static synthetic b(Lcom/my/target/core/presenters/a;)Ljava/util/List;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/my/target/core/presenters/a;->U:Ljava/util/List;

    return-object v0
.end method

.method static synthetic c(Lcom/my/target/core/presenters/a;)Lcom/my/target/core/presenters/b$a;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/my/target/core/presenters/a;->Z:Lcom/my/target/core/presenters/b$a;

    return-object v0
.end method

.method private d(Lcom/my/target/dh;)V
    .locals 2

    .prologue
    .line 470
    if-nez p1, :cond_0

    .line 479
    :goto_0
    return-void

    .line 476
    :cond_0
    invoke-virtual {p1}, Lcom/my/target/dh;->A()I

    move-result v0

    .line 478
    iget-object v1, p0, Lcom/my/target/core/presenters/a;->Q:Lcom/my/target/dj;

    invoke-virtual {v1, v0}, Lcom/my/target/dj;->setAnimationType(I)V

    goto :goto_0
.end method

.method static synthetic d(Lcom/my/target/core/presenters/a;)Z
    .locals 1

    .prologue
    .line 37
    iget-boolean v0, p0, Lcom/my/target/core/presenters/a;->ab:Z

    return v0
.end method

.method static synthetic e(Lcom/my/target/core/presenters/a;)Z
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/my/target/core/presenters/a;->G()Z

    move-result v0

    return v0
.end method

.method static synthetic f(Lcom/my/target/core/presenters/a;)Z
    .locals 1

    .prologue
    .line 37
    iget-boolean v0, p0, Lcom/my/target/core/presenters/a;->aa:Z

    return v0
.end method

.method static synthetic g(Lcom/my/target/core/presenters/a;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/my/target/core/presenters/a;->N:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic h(Lcom/my/target/core/presenters/a;)Z
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/my/target/core/presenters/a;->H()Z

    move-result v0

    return v0
.end method

.method static synthetic i(Lcom/my/target/core/presenters/a;)Z
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/my/target/core/presenters/a;->ab:Z

    return v0
.end method


# virtual methods
.method public final C()Lcom/my/target/di;
    .locals 1

    .prologue
    .line 333
    iget-object v0, p0, Lcom/my/target/core/presenters/a;->T:Lcom/my/target/di;

    return-object v0
.end method

.method public final a(Lcom/my/target/core/presenters/b$a;)V
    .locals 0

    .prologue
    .line 338
    iput-object p1, p0, Lcom/my/target/core/presenters/a;->Z:Lcom/my/target/core/presenters/b$a;

    .line 339
    return-void
.end method

.method public final b(Lcom/my/target/dh;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 176
    iget-object v0, p0, Lcom/my/target/core/presenters/a;->Q:Lcom/my/target/dj;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/my/target/dj;->setVisibility(I)V

    .line 177
    iget-object v0, p0, Lcom/my/target/core/presenters/a;->U:Ljava/util/List;

    invoke-virtual {p1}, Lcom/my/target/dh;->R()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 178
    iget-object v0, p0, Lcom/my/target/core/presenters/a;->U:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 219
    :goto_0
    return-void

    .line 182
    :cond_0
    iput-object p1, p0, Lcom/my/target/core/presenters/a;->W:Lcom/my/target/dh;

    .line 183
    invoke-virtual {p1}, Lcom/my/target/dh;->w()Lcom/my/target/dg;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/core/presenters/a;->N:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/my/target/core/presenters/a;->P:Lcom/my/target/cm;

    invoke-static {v0, v1, v2}, Lcom/my/target/core/presenters/a;->a(Lcom/my/target/dg;Ljava/util/List;Lcom/my/target/cm;)V

    .line 185
    iget-object v0, p0, Lcom/my/target/core/presenters/a;->U:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/core/models/banners/c;

    .line 186
    iget-object v1, p0, Lcom/my/target/core/presenters/a;->R:Ljava/lang/String;

    const-string v2, "standard_300x250"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 188
    new-instance v1, Lcom/my/target/dn;

    iget-object v0, p0, Lcom/my/target/core/presenters/a;->context:Landroid/content/Context;

    invoke-direct {v1, v0}, Lcom/my/target/dn;-><init>(Landroid/content/Context;)V

    .line 189
    iget-object v0, p0, Lcom/my/target/core/presenters/a;->N:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 190
    invoke-virtual {p1}, Lcom/my/target/dh;->w()Lcom/my/target/dg;

    move-result-object v0

    iget-object v2, p0, Lcom/my/target/core/presenters/a;->N:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/my/target/core/presenters/a;->P:Lcom/my/target/cm;

    invoke-static {v0, v2, v3}, Lcom/my/target/core/presenters/a;->a(Lcom/my/target/dg;Ljava/util/List;Lcom/my/target/cm;)V

    .line 191
    iget-object v0, p0, Lcom/my/target/core/presenters/a;->U:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/core/models/banners/c;

    invoke-direct {p0, v0, v1}, Lcom/my/target/core/presenters/a;->a(Lcom/my/target/core/models/banners/c;Lcom/my/target/dm;)V

    .line 193
    iget-object v0, p0, Lcom/my/target/core/presenters/a;->Q:Lcom/my/target/dj;

    iget-object v1, p0, Lcom/my/target/core/presenters/a;->N:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/my/target/dj;->a(Ljava/util/ArrayList;)V

    .line 195
    invoke-direct {p0, p1}, Lcom/my/target/core/presenters/a;->d(Lcom/my/target/dh;)V

    goto :goto_0

    .line 199
    :cond_1
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    sput-object v1, Lcom/my/target/core/presenters/a;->M:Landroid/os/Handler;

    .line 200
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/my/target/core/presenters/a;->ae:J

    .line 201
    invoke-virtual {p1}, Lcom/my/target/dh;->x()I

    move-result v1

    iput v1, p0, Lcom/my/target/core/presenters/a;->ah:I

    .line 202
    iget v1, p0, Lcom/my/target/core/presenters/a;->ah:I

    mul-int/lit16 v1, v1, 0x3e8

    int-to-long v2, v1

    iput-wide v2, p0, Lcom/my/target/core/presenters/a;->af:J

    .line 203
    iget-object v1, p0, Lcom/my/target/core/presenters/a;->U:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/my/target/core/models/banners/c;

    .line 205
    new-instance v3, Lcom/my/target/dk;

    iget-object v4, p0, Lcom/my/target/core/presenters/a;->R:Ljava/lang/String;

    iget-object v5, p0, Lcom/my/target/core/presenters/a;->context:Landroid/content/Context;

    invoke-direct {v3, v4, v5}, Lcom/my/target/dk;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    .line 206
    new-instance v4, Lcom/my/target/core/presenters/a$b;

    invoke-direct {v4, p0, v6}, Lcom/my/target/core/presenters/a$b;-><init>(Lcom/my/target/core/presenters/a;B)V

    invoke-virtual {v3, v4}, Lcom/my/target/dk;->setAfterLastSlideListener(Lcom/my/target/dk$a;)V

    .line 207
    invoke-direct {p0, v1, v3}, Lcom/my/target/core/presenters/a;->a(Lcom/my/target/core/models/banners/c;Lcom/my/target/dm;)V

    .line 208
    iget-object v1, p0, Lcom/my/target/core/presenters/a;->N:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 211
    :cond_2
    invoke-direct {p0, p1}, Lcom/my/target/core/presenters/a;->d(Lcom/my/target/dh;)V

    .line 212
    invoke-virtual {p1}, Lcom/my/target/dh;->w()Lcom/my/target/dg;

    move-result-object v1

    iget-object v2, p0, Lcom/my/target/core/presenters/a;->N:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/my/target/core/presenters/a;->P:Lcom/my/target/cm;

    invoke-static {v1, v2, v3}, Lcom/my/target/core/presenters/a;->a(Lcom/my/target/dg;Ljava/util/List;Lcom/my/target/cm;)V

    .line 214
    iget-object v1, p0, Lcom/my/target/core/presenters/a;->Q:Lcom/my/target/dj;

    iget-object v2, p0, Lcom/my/target/core/presenters/a;->N:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Lcom/my/target/dj;->a(Ljava/util/ArrayList;)V

    .line 215
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/c;->getTimeout()I

    move-result v0

    iput v0, p0, Lcom/my/target/core/presenters/a;->ag:I

    .line 216
    iget v0, p0, Lcom/my/target/core/presenters/a;->ag:I

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v0, v0

    invoke-direct {p0, v0, v1}, Lcom/my/target/core/presenters/a;->a(J)V

    .line 217
    invoke-direct {p0}, Lcom/my/target/core/presenters/a;->E()V

    goto/16 :goto_0
.end method

.method public final c(Lcom/my/target/dh;)V
    .locals 9

    .prologue
    const-wide/16 v2, 0x0

    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 224
    iget-object v0, p0, Lcom/my/target/core/presenters/a;->O:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 225
    iget-object v0, p0, Lcom/my/target/core/presenters/a;->V:Ljava/util/List;

    invoke-virtual {p1}, Lcom/my/target/dh;->R()Ljava/util/List;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 226
    iget-object v0, p0, Lcom/my/target/core/presenters/a;->V:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 280
    :cond_0
    :goto_0
    return-void

    .line 230
    :cond_1
    iput-object p1, p0, Lcom/my/target/core/presenters/a;->X:Lcom/my/target/dh;

    .line 232
    iget-object v0, p0, Lcom/my/target/core/presenters/a;->V:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/core/models/banners/c;

    .line 234
    new-instance v5, Lcom/my/target/dk;

    iget-object v6, p0, Lcom/my/target/core/presenters/a;->R:Ljava/lang/String;

    iget-object v7, p0, Lcom/my/target/core/presenters/a;->context:Landroid/content/Context;

    invoke-direct {v5, v6, v7}, Lcom/my/target/dk;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    .line 235
    new-instance v6, Lcom/my/target/core/presenters/a$b;

    invoke-direct {v6, p0, v1}, Lcom/my/target/core/presenters/a$b;-><init>(Lcom/my/target/core/presenters/a;B)V

    invoke-virtual {v5, v6}, Lcom/my/target/dk;->setAfterLastSlideListener(Lcom/my/target/dk$a;)V

    .line 236
    invoke-direct {p0, v0, v5}, Lcom/my/target/core/presenters/a;->a(Lcom/my/target/core/models/banners/c;Lcom/my/target/dm;)V

    .line 237
    iget-object v0, p0, Lcom/my/target/core/presenters/a;->O:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 240
    :cond_2
    iget-object v0, p0, Lcom/my/target/core/presenters/a;->Q:Lcom/my/target/dj;

    iget-object v4, p0, Lcom/my/target/core/presenters/a;->O:Ljava/util/ArrayList;

    .line 1067
    invoke-virtual {v0}, Lcom/my/target/dj;->getBackgroundFlipper()Landroid/widget/ViewFlipper;

    move-result-object v5

    .line 1068
    invoke-virtual {v5}, Landroid/widget/ViewFlipper;->removeAllViews()V

    .line 1069
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/dm;

    .line 1071
    invoke-interface {v0}, Lcom/my/target/dm;->O()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/widget/ViewFlipper;->addView(Landroid/view/View;)V

    goto :goto_2

    .line 242
    :cond_3
    iget-object v0, p0, Lcom/my/target/core/presenters/a;->X:Lcom/my/target/dh;

    invoke-direct {p0, v0}, Lcom/my/target/core/presenters/a;->d(Lcom/my/target/dh;)V

    .line 243
    iget-object v0, p0, Lcom/my/target/core/presenters/a;->X:Lcom/my/target/dh;

    invoke-virtual {v0}, Lcom/my/target/dh;->w()Lcom/my/target/dg;

    move-result-object v0

    iget-object v4, p0, Lcom/my/target/core/presenters/a;->O:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/my/target/core/presenters/a;->P:Lcom/my/target/cm;

    invoke-static {v0, v4, v5}, Lcom/my/target/core/presenters/a;->a(Lcom/my/target/dg;Ljava/util/List;Lcom/my/target/cm;)V

    .line 245
    iput-boolean v8, p0, Lcom/my/target/core/presenters/a;->aa:Z

    .line 248
    iget-object v0, p0, Lcom/my/target/core/presenters/a;->Q:Lcom/my/target/dj;

    invoke-virtual {v0}, Lcom/my/target/dj;->getDisplayedBannerNumber()I

    move-result v4

    .line 249
    iget-object v0, p0, Lcom/my/target/core/presenters/a;->N:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v4, v0, :cond_7

    .line 251
    iget-object v0, p0, Lcom/my/target/core/presenters/a;->N:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/dm;

    invoke-interface {v0}, Lcom/my/target/dm;->H()Z

    move-result v0

    .line 254
    :goto_3
    if-eqz v0, :cond_4

    .line 257
    iput-boolean v8, p0, Lcom/my/target/core/presenters/a;->ab:Z

    goto/16 :goto_0

    .line 264
    :cond_4
    iget-object v0, p0, Lcom/my/target/core/presenters/a;->U:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v4, v0, :cond_6

    .line 266
    iget-object v0, p0, Lcom/my/target/core/presenters/a;->U:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/core/models/banners/c;

    invoke-virtual {v0}, Lcom/my/target/core/models/banners/c;->getTimeout()I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v0, v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/my/target/core/presenters/a;->ac:J

    sub-long/2addr v4, v6

    sub-long/2addr v0, v4

    .line 268
    :goto_4
    cmp-long v2, v0, v2

    if-gtz v2, :cond_5

    .line 270
    invoke-direct {p0}, Lcom/my/target/core/presenters/a;->G()Z

    goto/16 :goto_0

    .line 274
    :cond_5
    sget-object v2, Lcom/my/target/core/presenters/a;->M:Landroid/os/Handler;

    if-eqz v2, :cond_0

    .line 276
    sget-object v2, Lcom/my/target/core/presenters/a;->M:Landroid/os/Handler;

    iget-object v3, p0, Lcom/my/target/core/presenters/a;->S:Lcom/my/target/core/presenters/a$d;

    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    :cond_6
    move-wide v0, v2

    goto :goto_4

    :cond_7
    move v0, v1

    goto :goto_3
.end method

.method public final destroy()V
    .locals 0

    .prologue
    .line 327
    invoke-virtual {p0}, Lcom/my/target/core/presenters/a;->stop()V

    .line 328
    return-void
.end method

.method public final pause()V
    .locals 6

    .prologue
    .line 310
    const-string v0, "Pause native banner"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 2434
    iget-object v0, p0, Lcom/my/target/core/presenters/a;->R:Ljava/lang/String;

    const-string v1, "standard_300x250"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2436
    iget-object v0, p0, Lcom/my/target/core/presenters/a;->Q:Lcom/my/target/dj;

    invoke-virtual {v0}, Lcom/my/target/dj;->isAnimating()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2438
    iget v0, p0, Lcom/my/target/core/presenters/a;->ag:I

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v0, v0

    .line 2439
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/my/target/core/presenters/a;->ac:J

    sub-long/2addr v2, v4

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/my/target/core/presenters/a;->ad:J

    .line 2441
    :cond_0
    invoke-direct {p0}, Lcom/my/target/core/presenters/a;->F()V

    .line 312
    :cond_1
    invoke-direct {p0}, Lcom/my/target/core/presenters/a;->D()V

    .line 313
    return-void
.end method

.method public final resume()V
    .locals 2

    .prologue
    .line 318
    const-string v0, "Resume native banner"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 3425
    iget-object v0, p0, Lcom/my/target/core/presenters/a;->R:Ljava/lang/String;

    const-string v1, "standard_300x250"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3427
    invoke-direct {p0}, Lcom/my/target/core/presenters/a;->F()V

    .line 3428
    iget-wide v0, p0, Lcom/my/target/core/presenters/a;->ad:J

    invoke-direct {p0, v0, v1}, Lcom/my/target/core/presenters/a;->a(J)V

    .line 321
    :cond_0
    invoke-direct {p0}, Lcom/my/target/core/presenters/a;->E()V

    .line 322
    return-void
.end method

.method public final start()V
    .locals 3

    .prologue
    .line 285
    iget-object v0, p0, Lcom/my/target/core/presenters/a;->Q:Lcom/my/target/dj;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/my/target/dj;->setVisibility(I)V

    .line 286
    iget-object v0, p0, Lcom/my/target/core/presenters/a;->Q:Lcom/my/target/dj;

    invoke-virtual {v0}, Lcom/my/target/dj;->getDisplayedBannerNumber()I

    move-result v0

    .line 287
    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/my/target/core/presenters/a;->U:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/my/target/core/presenters/a;->Z:Lcom/my/target/core/presenters/b$a;

    if-eqz v1, :cond_0

    .line 289
    iget-object v1, p0, Lcom/my/target/core/presenters/a;->Z:Lcom/my/target/core/presenters/b$a;

    iget-object v2, p0, Lcom/my/target/core/presenters/a;->U:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/ah;

    invoke-interface {v1, v0}, Lcom/my/target/core/presenters/b$a;->a(Lcom/my/target/ah;)V

    .line 291
    :cond_0
    return-void
.end method

.method public final stop()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 296
    iget-object v0, p0, Lcom/my/target/core/presenters/a;->Q:Lcom/my/target/dj;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/my/target/dj;->setVisibility(I)V

    .line 297
    const-string v0, "Stop native banner"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 298
    invoke-direct {p0}, Lcom/my/target/core/presenters/a;->D()V

    .line 299
    invoke-direct {p0}, Lcom/my/target/core/presenters/a;->F()V

    .line 301
    iget-object v0, p0, Lcom/my/target/core/presenters/a;->Q:Lcom/my/target/dj;

    .line 1170
    invoke-virtual {v0}, Lcom/my/target/dj;->getCurrentFlipper()Landroid/widget/ViewFlipper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ViewFlipper;->removeAllViews()V

    .line 302
    iget-object v0, p0, Lcom/my/target/core/presenters/a;->Q:Lcom/my/target/dj;

    .line 2077
    invoke-virtual {v0}, Lcom/my/target/dj;->getBackgroundFlipper()Landroid/widget/ViewFlipper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ViewFlipper;->removeAllViews()V

    .line 303
    iput-boolean v2, p0, Lcom/my/target/core/presenters/a;->ab:Z

    .line 304
    iput-boolean v2, p0, Lcom/my/target/core/presenters/a;->aa:Z

    .line 305
    return-void
.end method
