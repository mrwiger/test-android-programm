.class public final Lcom/google/android/gms/ads/VideoOptions;
.super Ljava/lang/Object;


# annotations
.annotation runtime Lcom/google/android/gms/internal/zzzn;
.end annotation


# instance fields
.field private final zzsf:Z

.field private final zzsg:Z


# direct methods
.method public constructor <init>(Lcom/google/android/gms/internal/zzlx;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-boolean v0, p1, Lcom/google/android/gms/internal/zzlx;->zzBJ:Z

    iput-boolean v0, p0, Lcom/google/android/gms/ads/VideoOptions;->zzsf:Z

    iget-boolean v0, p1, Lcom/google/android/gms/internal/zzlx;->zzBK:Z

    iput-boolean v0, p0, Lcom/google/android/gms/ads/VideoOptions;->zzsg:Z

    return-void
.end method


# virtual methods
.method public final getCustomControlsRequested()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/ads/VideoOptions;->zzsg:Z

    return v0
.end method

.method public final getStartMuted()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/ads/VideoOptions;->zzsf:Z

    return v0
.end method
