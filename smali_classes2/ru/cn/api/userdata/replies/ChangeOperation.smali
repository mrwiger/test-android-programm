.class public final enum Lru/cn/api/userdata/replies/ChangeOperation;
.super Ljava/lang/Enum;
.source "ChangeOperation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lru/cn/api/userdata/replies/ChangeOperation;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lru/cn/api/userdata/replies/ChangeOperation;

.field public static final enum ADD:Lru/cn/api/userdata/replies/ChangeOperation;

.field public static final enum DELETE:Lru/cn/api/userdata/replies/ChangeOperation;

.field public static final enum REPLACE:Lru/cn/api/userdata/replies/ChangeOperation;


# instance fields
.field private value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 5
    new-instance v0, Lru/cn/api/userdata/replies/ChangeOperation;

    const-string v1, "ADD"

    invoke-direct {v0, v1, v2, v2}, Lru/cn/api/userdata/replies/ChangeOperation;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/cn/api/userdata/replies/ChangeOperation;->ADD:Lru/cn/api/userdata/replies/ChangeOperation;

    .line 6
    new-instance v0, Lru/cn/api/userdata/replies/ChangeOperation;

    const-string v1, "DELETE"

    invoke-direct {v0, v1, v3, v3}, Lru/cn/api/userdata/replies/ChangeOperation;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/cn/api/userdata/replies/ChangeOperation;->DELETE:Lru/cn/api/userdata/replies/ChangeOperation;

    .line 7
    new-instance v0, Lru/cn/api/userdata/replies/ChangeOperation;

    const-string v1, "REPLACE"

    invoke-direct {v0, v1, v4, v4}, Lru/cn/api/userdata/replies/ChangeOperation;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/cn/api/userdata/replies/ChangeOperation;->REPLACE:Lru/cn/api/userdata/replies/ChangeOperation;

    .line 3
    const/4 v0, 0x3

    new-array v0, v0, [Lru/cn/api/userdata/replies/ChangeOperation;

    sget-object v1, Lru/cn/api/userdata/replies/ChangeOperation;->ADD:Lru/cn/api/userdata/replies/ChangeOperation;

    aput-object v1, v0, v2

    sget-object v1, Lru/cn/api/userdata/replies/ChangeOperation;->DELETE:Lru/cn/api/userdata/replies/ChangeOperation;

    aput-object v1, v0, v3

    sget-object v1, Lru/cn/api/userdata/replies/ChangeOperation;->REPLACE:Lru/cn/api/userdata/replies/ChangeOperation;

    aput-object v1, v0, v4

    sput-object v0, Lru/cn/api/userdata/replies/ChangeOperation;->$VALUES:[Lru/cn/api/userdata/replies/ChangeOperation;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 11
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 12
    iput p3, p0, Lru/cn/api/userdata/replies/ChangeOperation;->value:I

    .line 13
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lru/cn/api/userdata/replies/ChangeOperation;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 3
    const-class v0, Lru/cn/api/userdata/replies/ChangeOperation;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lru/cn/api/userdata/replies/ChangeOperation;

    return-object v0
.end method

.method public static values()[Lru/cn/api/userdata/replies/ChangeOperation;
    .locals 1

    .prologue
    .line 3
    sget-object v0, Lru/cn/api/userdata/replies/ChangeOperation;->$VALUES:[Lru/cn/api/userdata/replies/ChangeOperation;

    invoke-virtual {v0}, [Lru/cn/api/userdata/replies/ChangeOperation;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lru/cn/api/userdata/replies/ChangeOperation;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 16
    iget v0, p0, Lru/cn/api/userdata/replies/ChangeOperation;->value:I

    return v0
.end method
