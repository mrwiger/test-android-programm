.class public Lcom/yandex/metrica/impl/ob/ef;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/yandex/metrica/impl/ob/ef$a;
    }
.end annotation


# instance fields
.field private a:Lcom/yandex/metrica/impl/ob/eg;

.field private b:Lcom/yandex/metrica/impl/ob/ek;

.field private c:Lcom/yandex/metrica/impl/ob/dx;

.field private d:Landroid/location/LocationListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Landroid/location/LocationManager;Lcom/yandex/metrica/impl/ob/dv;Lcom/yandex/metrica/impl/ob/en;Lcom/yandex/metrica/impl/ob/dr;)V
    .locals 7

    .prologue
    .line 53
    new-instance v4, Lcom/yandex/metrica/impl/ob/ef$a;

    invoke-direct {v4}, Lcom/yandex/metrica/impl/ob/ef$a;-><init>()V

    new-instance v5, Lcom/yandex/metrica/impl/ob/ek;

    invoke-direct {v5, p1, p4, p5, p6}, Lcom/yandex/metrica/impl/ob/ek;-><init>(Landroid/content/Context;Lcom/yandex/metrica/impl/ob/dv;Lcom/yandex/metrica/impl/ob/en;Lcom/yandex/metrica/impl/ob/dr;)V

    new-instance v6, Lcom/yandex/metrica/impl/ob/dx;

    invoke-direct {v6, p1, p3}, Lcom/yandex/metrica/impl/ob/dx;-><init>(Landroid/content/Context;Landroid/location/LocationManager;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v6}, Lcom/yandex/metrica/impl/ob/ef;-><init>(Landroid/content/Context;Landroid/os/Looper;Landroid/location/LocationManager;Lcom/yandex/metrica/impl/ob/ef$a;Lcom/yandex/metrica/impl/ob/ek;Lcom/yandex/metrica/impl/ob/dx;)V

    .line 56
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Landroid/os/Looper;Landroid/location/LocationManager;Lcom/yandex/metrica/impl/ob/ef$a;Lcom/yandex/metrica/impl/ob/ek;Lcom/yandex/metrica/impl/ob/dx;)V
    .locals 1

    .prologue
    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    new-instance v0, Lcom/yandex/metrica/impl/ob/ef$1;

    invoke-direct {v0, p0}, Lcom/yandex/metrica/impl/ob/ef$1;-><init>(Lcom/yandex/metrica/impl/ob/ef;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/ef;->d:Landroid/location/LocationListener;

    .line 115
    iput-object p6, p0, Lcom/yandex/metrica/impl/ob/ef;->c:Lcom/yandex/metrica/impl/ob/dx;

    .line 116
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ef;->d:Landroid/location/LocationListener;

    invoke-virtual {p4, p1, p2, p3, v0}, Lcom/yandex/metrica/impl/ob/ef$a;->a(Landroid/content/Context;Landroid/os/Looper;Landroid/location/LocationManager;Landroid/location/LocationListener;)Lcom/yandex/metrica/impl/ob/eg;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/ef;->a:Lcom/yandex/metrica/impl/ob/eg;

    .line 118
    iput-object p5, p0, Lcom/yandex/metrica/impl/ob/ef;->b:Lcom/yandex/metrica/impl/ob/ek;

    .line 119
    return-void
.end method

.method static synthetic a(Lcom/yandex/metrica/impl/ob/ef;)Lcom/yandex/metrica/impl/ob/ek;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ef;->b:Lcom/yandex/metrica/impl/ob/ek;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 59
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ef;->c:Lcom/yandex/metrica/impl/ob/dx;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/dx;->a()Landroid/location/Location;

    move-result-object v0

    .line 61
    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ef;->b:Lcom/yandex/metrica/impl/ob/ek;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/ef;->c:Lcom/yandex/metrica/impl/ob/dx;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/dx;->a()Landroid/location/Location;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/ek;->a(Landroid/location/Location;)V

    .line 64
    :cond_0
    return-void
.end method

.method public a(Lcom/yandex/metrica/impl/ob/dv;)V
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ef;->b:Lcom/yandex/metrica/impl/ob/ek;

    invoke-virtual {v0, p1}, Lcom/yandex/metrica/impl/ob/ek;->a(Lcom/yandex/metrica/impl/ob/dv;)V

    .line 86
    return-void
.end method

.method public b()Landroid/location/Location;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ef;->b:Lcom/yandex/metrica/impl/ob/ek;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/ek;->a()Landroid/location/Location;

    move-result-object v0

    return-object v0
.end method

.method public c()Landroid/location/Location;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ef;->c:Lcom/yandex/metrica/impl/ob/dx;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/dx;->a()Landroid/location/Location;

    move-result-object v0

    return-object v0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ef;->a:Lcom/yandex/metrica/impl/ob/eg;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/eg;->a()V

    .line 78
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ef;->a:Lcom/yandex/metrica/impl/ob/eg;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/eg;->b()V

    .line 82
    return-void
.end method
