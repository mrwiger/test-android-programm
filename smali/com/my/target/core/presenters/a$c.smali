.class final Lcom/my/target/core/presenters/a$c;
.super Ljava/lang/Object;
.source "NativeViewPresenter.java"

# interfaces
.implements Lcom/my/target/dj$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/my/target/core/presenters/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "c"
.end annotation


# instance fields
.field final synthetic ai:Lcom/my/target/core/presenters/a;


# direct methods
.method private constructor <init>(Lcom/my/target/core/presenters/a;)V
    .locals 0

    .prologue
    .line 637
    iput-object p1, p0, Lcom/my/target/core/presenters/a$c;->ai:Lcom/my/target/core/presenters/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/my/target/core/presenters/a;B)V
    .locals 0

    .prologue
    .line 637
    invoke-direct {p0, p1}, Lcom/my/target/core/presenters/a$c;-><init>(Lcom/my/target/core/presenters/a;)V

    return-void
.end method


# virtual methods
.method public final j(I)V
    .locals 6

    .prologue
    .line 642
    iget-object v0, p0, Lcom/my/target/core/presenters/a$c;->ai:Lcom/my/target/core/presenters/a;

    invoke-static {v0}, Lcom/my/target/core/presenters/a;->f(Lcom/my/target/core/presenters/a;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/my/target/core/presenters/a$c;->ai:Lcom/my/target/core/presenters/a;

    invoke-static {v0}, Lcom/my/target/core/presenters/a;->a(Lcom/my/target/core/presenters/a;)Lcom/my/target/dj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/my/target/dj;->getBackgroundFlipper()Landroid/widget/ViewFlipper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ViewFlipper;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 644
    iget-object v0, p0, Lcom/my/target/core/presenters/a$c;->ai:Lcom/my/target/core/presenters/a;

    invoke-static {v0}, Lcom/my/target/core/presenters/a;->a(Lcom/my/target/core/presenters/a;)Lcom/my/target/dj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/my/target/dj;->getBackgroundFlipper()Landroid/widget/ViewFlipper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ViewFlipper;->removeAllViews()V

    .line 646
    :cond_0
    iget-object v0, p0, Lcom/my/target/core/presenters/a$c;->ai:Lcom/my/target/core/presenters/a;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/my/target/core/presenters/a;->a(Lcom/my/target/core/presenters/a;J)J

    .line 647
    if-gez p1, :cond_2

    .line 671
    :cond_1
    :goto_0
    return-void

    .line 652
    :cond_2
    iget-object v0, p0, Lcom/my/target/core/presenters/a$c;->ai:Lcom/my/target/core/presenters/a;

    invoke-static {v0}, Lcom/my/target/core/presenters/a;->b(Lcom/my/target/core/presenters/a;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_3

    .line 654
    iget-object v0, p0, Lcom/my/target/core/presenters/a$c;->ai:Lcom/my/target/core/presenters/a;

    invoke-static {v0}, Lcom/my/target/core/presenters/a;->b(Lcom/my/target/core/presenters/a;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/core/models/banners/c;

    .line 656
    if-eqz v0, :cond_3

    .line 658
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/c;->getTimeout()I

    move-result v1

    .line 659
    iget-object v2, p0, Lcom/my/target/core/presenters/a$c;->ai:Lcom/my/target/core/presenters/a;

    mul-int/lit16 v1, v1, 0x3e8

    int-to-long v4, v1

    invoke-static {v2, v4, v5}, Lcom/my/target/core/presenters/a;->b(Lcom/my/target/core/presenters/a;J)J

    .line 660
    iget-object v1, p0, Lcom/my/target/core/presenters/a$c;->ai:Lcom/my/target/core/presenters/a;

    invoke-static {v1}, Lcom/my/target/core/presenters/a;->c(Lcom/my/target/core/presenters/a;)Lcom/my/target/core/presenters/b$a;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 662
    iget-object v1, p0, Lcom/my/target/core/presenters/a$c;->ai:Lcom/my/target/core/presenters/a;

    invoke-static {v1}, Lcom/my/target/core/presenters/a;->c(Lcom/my/target/core/presenters/a;)Lcom/my/target/core/presenters/b$a;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/my/target/core/presenters/b$a;->a(Lcom/my/target/ah;)V

    .line 667
    :cond_3
    iget-object v0, p0, Lcom/my/target/core/presenters/a$c;->ai:Lcom/my/target/core/presenters/a;

    invoke-static {v0}, Lcom/my/target/core/presenters/a;->g(Lcom/my/target/core/presenters/a;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_1

    .line 669
    iget-object v0, p0, Lcom/my/target/core/presenters/a$c;->ai:Lcom/my/target/core/presenters/a;

    invoke-static {v0}, Lcom/my/target/core/presenters/a;->g(Lcom/my/target/core/presenters/a;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/dm;

    invoke-interface {v0}, Lcom/my/target/dm;->start()V

    goto :goto_0
.end method
