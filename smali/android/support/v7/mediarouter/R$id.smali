.class public final Landroid/support/v7/mediarouter/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v7/mediarouter/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final action0:I = 0x7f090007

.field public static final action_bar:I = 0x7f090008

.field public static final action_bar_activity_content:I = 0x7f090009

.field public static final action_bar_container:I = 0x7f09000a

.field public static final action_bar_root:I = 0x7f09000b

.field public static final action_bar_spinner:I = 0x7f09000c

.field public static final action_bar_subtitle:I = 0x7f09000d

.field public static final action_bar_title:I = 0x7f09000e

.field public static final action_container:I = 0x7f09000f

.field public static final action_context_bar:I = 0x7f090010

.field public static final action_divider:I = 0x7f090011

.field public static final action_image:I = 0x7f090012

.field public static final action_menu_divider:I = 0x7f090013

.field public static final action_menu_presenter:I = 0x7f090014

.field public static final action_mode_bar:I = 0x7f090015

.field public static final action_mode_bar_stub:I = 0x7f090016

.field public static final action_mode_close_button:I = 0x7f090017

.field public static final action_text:I = 0x7f090018

.field public static final actions:I = 0x7f090019

.field public static final activity_chooser_view_content:I = 0x7f09001a

.field public static final add:I = 0x7f090026

.field public static final alertTitle:I = 0x7f09002e

.field public static final async:I = 0x7f090032

.field public static final blocking:I = 0x7f09003d

.field public static final bottom:I = 0x7f090040

.field public static final buttonPanel:I = 0x7f090047

.field public static final cancel_action:I = 0x7f090052

.field public static final checkbox:I = 0x7f09006f

.field public static final chronometer:I = 0x7f090071

.field public static final contentPanel:I = 0x7f090086

.field public static final custom:I = 0x7f090092

.field public static final customPanel:I = 0x7f090093

.field public static final decor_content_parent:I = 0x7f090099

.field public static final default_activity_button:I = 0x7f09009a

.field public static final edit_query:I = 0x7f0900b0

.field public static final end:I = 0x7f0900b5

.field public static final end_padder:I = 0x7f0900b6

.field public static final expand_activities_button:I = 0x7f0900ce

.field public static final expanded_menu:I = 0x7f0900d1

.field public static final forever:I = 0x7f0900e5

.field public static final home:I = 0x7f0900eb

.field public static final icon:I = 0x7f0900ed

.field public static final icon_group:I = 0x7f0900ef

.field public static final image:I = 0x7f0900f3

.field public static final info:I = 0x7f0900f7

.field public static final italic:I = 0x7f0900fd

.field public static final left:I = 0x7f090100

.field public static final line1:I = 0x7f090102

.field public static final line3:I = 0x7f090103

.field public static final listMode:I = 0x7f090105

.field public static final list_item:I = 0x7f090107

.field public static final media_actions:I = 0x7f090118

.field public static final message:I = 0x7f09011d

.field public static final mr_art:I = 0x7f090120

.field public static final mr_chooser_list:I = 0x7f090121

.field public static final mr_chooser_route_desc:I = 0x7f090122

.field public static final mr_chooser_route_icon:I = 0x7f090123

.field public static final mr_chooser_route_name:I = 0x7f090124

.field public static final mr_chooser_title:I = 0x7f090125

.field public static final mr_close:I = 0x7f090126

.field public static final mr_control_divider:I = 0x7f090127

.field public static final mr_control_playback_ctrl:I = 0x7f090128

.field public static final mr_control_subtitle:I = 0x7f090129

.field public static final mr_control_title:I = 0x7f09012a

.field public static final mr_control_title_container:I = 0x7f09012b

.field public static final mr_custom_control:I = 0x7f09012c

.field public static final mr_default_control:I = 0x7f09012d

.field public static final mr_dialog_area:I = 0x7f09012e

.field public static final mr_expandable_area:I = 0x7f09012f

.field public static final mr_group_expand_collapse:I = 0x7f090130

.field public static final mr_media_main_control:I = 0x7f090131

.field public static final mr_name:I = 0x7f090132

.field public static final mr_playback_control:I = 0x7f090133

.field public static final mr_title_bar:I = 0x7f090134

.field public static final mr_volume_control:I = 0x7f090135

.field public static final mr_volume_group_list:I = 0x7f090136

.field public static final mr_volume_item_icon:I = 0x7f090137

.field public static final mr_volume_slider:I = 0x7f090138

.field public static final multiply:I = 0x7f090139

.field public static final none:I = 0x7f09014d

.field public static final normal:I = 0x7f09014e

.field public static final notification_background:I = 0x7f09014f

.field public static final notification_main_column:I = 0x7f090151

.field public static final notification_main_column_container:I = 0x7f090152

.field public static final parentPanel:I = 0x7f09015d

.field public static final progress_circular:I = 0x7f090178

.field public static final progress_horizontal:I = 0x7f090179

.field public static final radio:I = 0x7f09017a

.field public static final right:I = 0x7f090180

.field public static final right_icon:I = 0x7f090181

.field public static final right_side:I = 0x7f090182

.field public static final screen:I = 0x7f09018b

.field public static final scrollIndicatorDown:I = 0x7f09018d

.field public static final scrollIndicatorUp:I = 0x7f09018e

.field public static final scrollView:I = 0x7f09018f

.field public static final search_badge:I = 0x7f090191

.field public static final search_bar:I = 0x7f090192

.field public static final search_button:I = 0x7f090193

.field public static final search_close_btn:I = 0x7f090194

.field public static final search_edit_frame:I = 0x7f090195

.field public static final search_go_btn:I = 0x7f090196

.field public static final search_mag_icon:I = 0x7f090197

.field public static final search_plate:I = 0x7f090198

.field public static final search_src_text:I = 0x7f090199

.field public static final search_voice_btn:I = 0x7f09019a

.field public static final select_dialog_listview:I = 0x7f09019f

.field public static final shortcut:I = 0x7f0901a2

.field public static final spacer:I = 0x7f0901ad

.field public static final split_action_bar:I = 0x7f0901af

.field public static final src_atop:I = 0x7f0901b0

.field public static final src_in:I = 0x7f0901b1

.field public static final src_over:I = 0x7f0901b2

.field public static final start:I = 0x7f0901b4

.field public static final status_bar_latest_event_content:I = 0x7f0901b6

.field public static final submenuarrow:I = 0x7f0901bb

.field public static final submit_area:I = 0x7f0901bc

.field public static final tabMode:I = 0x7f0901c2

.field public static final tag_transition_group:I = 0x7f0901c5

.field public static final text:I = 0x7f0901ca

.field public static final text2:I = 0x7f0901cb

.field public static final textSpacerNoButtons:I = 0x7f0901cc

.field public static final textSpacerNoTitle:I = 0x7f0901cd

.field public static final time:I = 0x7f0901d4

.field public static final title:I = 0x7f0901d7

.field public static final titleDividerNoCustom:I = 0x7f0901d8

.field public static final title_template:I = 0x7f0901da

.field public static final top:I = 0x7f0901dd

.field public static final topPanel:I = 0x7f0901de

.field public static final uniform:I = 0x7f0901eb

.field public static final up:I = 0x7f0901ec

.field public static final volume_item_container:I = 0x7f0901f5

.field public static final wrap_content:I = 0x7f0901fb
