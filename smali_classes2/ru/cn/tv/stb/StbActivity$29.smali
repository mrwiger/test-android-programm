.class Lru/cn/tv/stb/StbActivity$29;
.super Ljava/lang/Object;
.source "StbActivity.java"

# interfaces
.implements Lru/cn/tv/stb/schedule/ScheduleFragment$ScheduleItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/tv/stb/StbActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/tv/stb/StbActivity;


# direct methods
.method constructor <init>(Lru/cn/tv/stb/StbActivity;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 1891
    iput-object p1, p0, Lru/cn/tv/stb/StbActivity$29;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private playContent(JJ)V
    .locals 3
    .param p1, "telecastId"    # J
    .param p3, "channelId"    # J

    .prologue
    const/4 v0, 0x0

    .line 1927
    invoke-static {v0, v0}, Lru/cn/domain/statistics/inetra/InetraTracker;->setSessionParams(II)V

    .line 1931
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    .line 1932
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$29;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$000(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/player/SimplePlayerFragment;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lru/cn/tv/player/SimplePlayerFragment;->playTelecast(J)V

    .line 1938
    :goto_0
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$29;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$1100(Lru/cn/tv/stb/StbActivity;)V

    .line 1939
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$29;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$4100(Lru/cn/tv/stb/StbActivity;)V

    .line 1940
    return-void

    .line 1934
    :cond_0
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$29;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$000(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/player/SimplePlayerFragment;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Lru/cn/tv/player/SimplePlayerFragment;->playChannel(J)V

    .line 1935
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$29;->this$0:Lru/cn/tv/stb/StbActivity;

    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$29;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$2900(Lru/cn/tv/stb/StbActivity;)Lru/cn/domain/tv/CurrentCategory$Type;

    move-result-object v1

    invoke-static {v0, v1}, Lru/cn/tv/stb/StbActivity;->access$6302(Lru/cn/tv/stb/StbActivity;Lru/cn/domain/tv/CurrentCategory$Type;)Lru/cn/domain/tv/CurrentCategory$Type;

    goto :goto_0
.end method


# virtual methods
.method public onOnAirClicked(J)V
    .locals 3
    .param p1, "channelId"    # J

    .prologue
    .line 1923
    const-wide/16 v0, 0x0

    invoke-direct {p0, v0, v1, p1, p2}, Lru/cn/tv/stb/StbActivity$29;->playContent(JJ)V

    .line 1924
    return-void
.end method

.method public onScheduleItemClicked(JLru/cn/api/provider/cursor/ScheduleItemCursor;)V
    .locals 12
    .param p1, "telecastId"    # J
    .param p3, "telecast"    # Lru/cn/api/provider/cursor/ScheduleItemCursor;

    .prologue
    const-wide/16 v10, 0x0

    .line 1895
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 1896
    .local v4, "nowMs":J
    const-wide/16 v6, 0x3e8

    div-long v6, v4, v6

    invoke-virtual {p3}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->getTime()J

    move-result-wide v8

    sub-long v0, v6, v8

    .line 1897
    .local v0, "elapsed":J
    invoke-virtual {p3}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->getDuration()J

    move-result-wide v6

    cmp-long v3, v0, v6

    if-lez v3, :cond_1

    invoke-virtual {p3}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->hasRecords()Z

    move-result v3

    if-nez v3, :cond_1

    .line 1919
    :cond_0
    :goto_0
    return-void

    .line 1901
    :cond_1
    cmp-long v3, v0, v10

    if-gez v3, :cond_2

    .line 1902
    invoke-static {}, Lru/cn/utils/Utils;->isLauncherInstalled()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lru/cn/tv/stb/StbActivity$29;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v3}, Lru/cn/tv/stb/StbActivity;->access$5800(Lru/cn/tv/stb/StbActivity;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1903
    iget-object v3, p0, Lru/cn/tv/stb/StbActivity$29;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v3, p3}, Lru/cn/api/provider/NotificationIdsStorage;->scheduleNotification(Landroid/content/Context;Landroid/database/Cursor;)V

    goto :goto_0

    .line 1909
    :cond_2
    invoke-virtual {p3, v4, v5}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->isOnTime(J)Z

    move-result v2

    .line 1910
    .local v2, "live":Z
    if-eqz v2, :cond_3

    .line 1911
    invoke-virtual {p3}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->getChannelId()J

    move-result-wide v6

    invoke-direct {p0, v10, v11, v6, v7}, Lru/cn/tv/stb/StbActivity$29;->playContent(JJ)V

    goto :goto_0

    .line 1913
    :cond_3
    invoke-virtual {p3}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->isDenied()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1914
    iget-object v3, p0, Lru/cn/tv/stb/StbActivity$29;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-virtual {p3}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->getTitle()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lru/cn/tv/stb/StbActivity;->access$1000(Lru/cn/tv/stb/StbActivity;Ljava/lang/String;)V

    goto :goto_0

    .line 1916
    :cond_4
    invoke-virtual {p3}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->hasRecords()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1917
    invoke-virtual {p3}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->getTelecastId()J

    move-result-wide v6

    invoke-virtual {p3}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->getChannelId()J

    move-result-wide v8

    invoke-direct {p0, v6, v7, v8, v9}, Lru/cn/tv/stb/StbActivity$29;->playContent(JJ)V

    goto :goto_0
.end method
