.class Lru/cn/domain/statistics/inetra/TrackingSender;
.super Ljava/lang/Object;
.source "TrackingSender.java"


# instance fields
.field private final appContext:Landroid/content/Context;

.field private final duid:Ljava/lang/String;

.field private final serviceHandler:Landroid/os/Handler;

.field private final storage:Lru/cn/domain/statistics/inetra/EventsStorage;


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "duid"    # Ljava/lang/String;

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p2, p0, Lru/cn/domain/statistics/inetra/TrackingSender;->duid:Ljava/lang/String;

    .line 48
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iput-object v1, p0, Lru/cn/domain/statistics/inetra/TrackingSender;->appContext:Landroid/content/Context;

    .line 49
    new-instance v1, Lru/cn/domain/statistics/inetra/PreferencesStorage;

    iget-object v2, p0, Lru/cn/domain/statistics/inetra/TrackingSender;->appContext:Landroid/content/Context;

    const-string v3, "http://analytics.cn.ru/2/collect/3"

    invoke-direct {v1, v2, v3}, Lru/cn/domain/statistics/inetra/PreferencesStorage;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v1, p0, Lru/cn/domain/statistics/inetra/TrackingSender;->storage:Lru/cn/domain/statistics/inetra/EventsStorage;

    .line 51
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "TrackingApi"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 52
    .local v0, "serviceThread":Landroid/os/HandlerThread;
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 54
    new-instance v1, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    new-instance v3, Lru/cn/domain/statistics/inetra/TrackingSender$1;

    invoke-direct {v3, p0}, Lru/cn/domain/statistics/inetra/TrackingSender$1;-><init>(Lru/cn/domain/statistics/inetra/TrackingSender;)V

    invoke-direct {v1, v2, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v1, p0, Lru/cn/domain/statistics/inetra/TrackingSender;->serviceHandler:Landroid/os/Handler;

    .line 80
    iget-object v1, p0, Lru/cn/domain/statistics/inetra/TrackingSender;->serviceHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    const-wide/16 v4, 0x4e20

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 81
    return-void
.end method

.method static synthetic access$000(Lru/cn/domain/statistics/inetra/TrackingSender;)Lru/cn/domain/statistics/inetra/EventsStorage;
    .locals 1
    .param p0, "x0"    # Lru/cn/domain/statistics/inetra/TrackingSender;

    .prologue
    .line 23
    iget-object v0, p0, Lru/cn/domain/statistics/inetra/TrackingSender;->storage:Lru/cn/domain/statistics/inetra/EventsStorage;

    return-object v0
.end method

.method static synthetic access$100(Lru/cn/domain/statistics/inetra/TrackingSender;Ljava/util/List;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/domain/statistics/inetra/TrackingSender;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lru/cn/domain/statistics/inetra/TrackingSender;->dispatchEvents(Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$200(Lru/cn/domain/statistics/inetra/TrackingSender;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lru/cn/domain/statistics/inetra/TrackingSender;

    .prologue
    .line 23
    iget-object v0, p0, Lru/cn/domain/statistics/inetra/TrackingSender;->serviceHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private dispatchEvents(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lru/cn/domain/statistics/inetra/TrackingEvent;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 176
    .local p1, "eventsToDispatch":Ljava/util/List;, "Ljava/util/List<Lru/cn/domain/statistics/inetra/TrackingEvent;>;"
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_1

    .line 202
    :cond_0
    return-void

    .line 179
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/domain/statistics/inetra/TrackingEvent;

    .line 180
    .local v0, "event":Lru/cn/domain/statistics/inetra/TrackingEvent;
    iget-boolean v3, v0, Lru/cn/domain/statistics/inetra/TrackingEvent;->onFly:Z

    if-nez v3, :cond_2

    .line 183
    invoke-virtual {v0}, Lru/cn/domain/statistics/inetra/TrackingEvent;->produceRequestURL()Ljava/lang/String;

    move-result-object v1

    .line 184
    .local v1, "requestURL":Ljava/lang/String;
    const/4 v3, 0x1

    iput-boolean v3, v0, Lru/cn/domain/statistics/inetra/TrackingEvent;->onFly:Z

    .line 186
    const/4 v3, 0x0

    new-instance v4, Lru/cn/domain/statistics/inetra/TrackingSender$2;

    invoke-direct {v4, p0, v0, v1}, Lru/cn/domain/statistics/inetra/TrackingSender$2;-><init>(Lru/cn/domain/statistics/inetra/TrackingSender;Lru/cn/domain/statistics/inetra/TrackingEvent;Ljava/lang/String;)V

    invoke-static {v1, v3, v4}, Lru/cn/utils/http/HttpClient;->sendRequestAsync(Ljava/lang/String;Ljava/lang/String;Lokhttp3/Callback;)V

    goto :goto_0
.end method

.method private getConnectionType()Ljava/lang/String;
    .locals 5

    .prologue
    .line 84
    iget-object v2, p0, Lru/cn/domain/statistics/inetra/TrackingSender;->appContext:Landroid/content/Context;

    const-string v3, "connectivity"

    .line 85
    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    .line 87
    .local v1, "connMgr":Landroid/net/ConnectivityManager;
    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 88
    .local v0, "activeNetworkInfo":Landroid/net/NetworkInfo;
    if-nez v0, :cond_0

    .line 89
    const-string v2, "0"

    .line 134
    :goto_0
    return-object v2

    .line 92
    :cond_0
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 132
    :pswitch_0
    new-instance v2, Ljava/lang/Exception;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unknown connectionType "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " subType "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 133
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 132
    invoke-static {v2}, Lru/cn/utils/Logger;->logException(Ljava/lang/Throwable;)V

    .line 134
    const/4 v2, 0x0

    goto :goto_0

    .line 94
    :pswitch_1
    const-string v2, "2"

    goto :goto_0

    .line 97
    :pswitch_2
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v2

    packed-switch v2, :pswitch_data_1

    .line 124
    :pswitch_3
    const-string v2, "1"

    goto :goto_0

    .line 103
    :pswitch_4
    const-string v2, "3"

    goto :goto_0

    .line 114
    :pswitch_5
    const-string v2, "4"

    goto :goto_0

    .line 117
    :pswitch_6
    const-string v2, "5"

    goto :goto_0

    .line 127
    :pswitch_7
    const-string v2, "6"

    goto :goto_0

    .line 130
    :pswitch_8
    const-string v2, "5"

    goto :goto_0

    .line 92
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_7
    .end packed-switch

    .line 97
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_4
        :pswitch_5
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_5
        :pswitch_5
    .end packed-switch
.end method


# virtual methods
.method createEvent(Ljava/lang/String;Ljava/lang/String;)Lru/cn/domain/statistics/inetra/TrackingEvent;
    .locals 8
    .param p1, "eventName"    # Ljava/lang/String;
    .param p2, "sessionId"    # Ljava/lang/String;

    .prologue
    .line 139
    iget-object v6, p0, Lru/cn/domain/statistics/inetra/TrackingSender;->appContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    .line 140
    .local v2, "m":Landroid/util/DisplayMetrics;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget v7, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "x"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 142
    .local v3, "res":Ljava/lang/String;
    new-instance v1, Lru/cn/domain/statistics/inetra/TrackingEvent;

    invoke-direct {v1, p1}, Lru/cn/domain/statistics/inetra/TrackingEvent;-><init>(Ljava/lang/String;)V

    .line 143
    .local v1, "event":Lru/cn/domain/statistics/inetra/TrackingEvent;
    const-string v6, "http://analytics.cn.ru/2/collect/3"

    invoke-virtual {v1, v6}, Lru/cn/domain/statistics/inetra/TrackingEvent;->setBaseURL(Ljava/lang/String;)V

    .line 144
    const-string v6, "uid"

    iget-object v7, p0, Lru/cn/domain/statistics/inetra/TrackingSender;->appContext:Landroid/content/Context;

    invoke-static {v7}, Lru/cn/utils/Utils;->getUUID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, Lru/cn/domain/statistics/inetra/TrackingEvent;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    const-string v6, "vext"

    const-string v7, "3"

    invoke-virtual {v1, v6, v7}, Lru/cn/domain/statistics/inetra/TrackingEvent;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    const-string v6, "res"

    invoke-virtual {v1, v6, v3}, Lru/cn/domain/statistics/inetra/TrackingEvent;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    if-eqz p2, :cond_0

    .line 149
    const-string v6, "sid"

    invoke-virtual {v1, v6, p2}, Lru/cn/domain/statistics/inetra/TrackingEvent;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    :cond_0
    sget-object v6, Lru/cn/domain/statistics/inetra/InetraTracker;->currentAppMode:Ljava/lang/Integer;

    if-eqz v6, :cond_1

    .line 153
    const-string v6, "_app_mode"

    sget-object v7, Lru/cn/domain/statistics/inetra/InetraTracker;->currentAppMode:Ljava/lang/Integer;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, Lru/cn/domain/statistics/inetra/TrackingEvent;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 156
    :cond_1
    iget-object v6, p0, Lru/cn/domain/statistics/inetra/TrackingSender;->duid:Ljava/lang/String;

    if-eqz v6, :cond_2

    .line 157
    const-string v6, "_duid"

    iget-object v7, p0, Lru/cn/domain/statistics/inetra/TrackingSender;->duid:Ljava/lang/String;

    invoke-virtual {v1, v6, v7}, Lru/cn/domain/statistics/inetra/TrackingEvent;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    :cond_2
    invoke-direct {p0}, Lru/cn/domain/statistics/inetra/TrackingSender;->getConnectionType()Ljava/lang/String;

    move-result-object v0

    .line 161
    .local v0, "connectionType":Ljava/lang/String;
    if-eqz v0, :cond_3

    .line 162
    const-string v6, "_connection"

    invoke-virtual {v1, v6, v0}, Lru/cn/domain/statistics/inetra/TrackingEvent;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    :cond_3
    invoke-static {}, Lru/cn/utils/Utils;->getCalendar()Ljava/util/Calendar;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/TimeZone;->getRawOffset()I

    move-result v6

    div-int/lit16 v6, v6, 0x3e8

    int-to-long v4, v6

    .line 166
    .local v4, "offset":J
    const-string v6, "tz"

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, Lru/cn/domain/statistics/inetra/TrackingEvent;->addParameter(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    return-object v1
.end method

.method sendEvent(Lru/cn/domain/statistics/inetra/TrackingEvent;)V
    .locals 2
    .param p1, "event"    # Lru/cn/domain/statistics/inetra/TrackingEvent;

    .prologue
    .line 172
    iget-object v0, p0, Lru/cn/domain/statistics/inetra/TrackingSender;->serviceHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-static {v0, v1, p1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 173
    return-void
.end method
