.class public final Lcom/my/target/ey;
.super Landroid/widget/RelativeLayout;
.source "SliderAdView.java"


# static fields
.field private static final dK:I

.field private static final dL:I

.field private static final dM:I


# instance fields
.field private final aw:Lcom/my/target/cm;

.field private final dN:Lcom/my/target/ex;

.field private final dO:Lcom/my/target/by;

.field private final dP:Lcom/my/target/fb;

.field private final dQ:Landroid/widget/FrameLayout;

.field private final dR:Landroid/widget/RelativeLayout$LayoutParams;

.field private final dS:Landroid/widget/RelativeLayout$LayoutParams;

.field private final dT:Landroid/widget/RelativeLayout$LayoutParams;

.field private orientation:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/ey;->dK:I

    .line 26
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/ey;->dL:I

    .line 27
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/ey;->dM:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, -0x2

    const/4 v2, -0x1

    .line 44
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 45
    invoke-static {p1}, Lcom/my/target/cm;->x(Landroid/content/Context;)Lcom/my/target/cm;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/ey;->aw:Lcom/my/target/cm;

    .line 46
    new-instance v0, Lcom/my/target/ex;

    invoke-direct {v0, p1}, Lcom/my/target/ex;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/ey;->dN:Lcom/my/target/ex;

    .line 47
    new-instance v0, Lcom/my/target/by;

    invoke-direct {v0, p1}, Lcom/my/target/by;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/ey;->dO:Lcom/my/target/by;

    .line 48
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-direct {v0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/ey;->dQ:Landroid/widget/FrameLayout;

    .line 50
    new-instance v0, Lcom/my/target/fb;

    invoke-direct {v0, p1}, Lcom/my/target/fb;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/ey;->dP:Lcom/my/target/fb;

    .line 51
    iget-object v0, p0, Lcom/my/target/ey;->dP:Lcom/my/target/fb;

    sget v1, Lcom/my/target/ey;->dK:I

    invoke-virtual {v0, v1}, Lcom/my/target/fb;->setId(I)V

    .line 53
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-object v0, p0, Lcom/my/target/ey;->dS:Landroid/widget/RelativeLayout$LayoutParams;

    .line 55
    iget-object v0, p0, Lcom/my/target/ey;->dO:Lcom/my/target/by;

    sget v1, Lcom/my/target/ey;->dM:I

    invoke-virtual {v0, v1}, Lcom/my/target/by;->setId(I)V

    .line 57
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 60
    const/16 v1, 0x11

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 61
    iget-object v1, p0, Lcom/my/target/ey;->dP:Lcom/my/target/fb;

    invoke-virtual {v1, v0}, Lcom/my/target/fb;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 63
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-object v0, p0, Lcom/my/target/ey;->dR:Landroid/widget/RelativeLayout$LayoutParams;

    .line 65
    iget-object v0, p0, Lcom/my/target/ey;->dR:Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v1, 0xe

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 66
    iget-object v0, p0, Lcom/my/target/ey;->dR:Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v1, 0xc

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 68
    iget-object v0, p0, Lcom/my/target/ey;->dN:Lcom/my/target/ex;

    sget v1, Lcom/my/target/ey;->dL:I

    invoke-virtual {v0, v1}, Lcom/my/target/ex;->setId(I)V

    .line 70
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v2, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-object v0, p0, Lcom/my/target/ey;->dT:Landroid/widget/RelativeLayout$LayoutParams;

    .line 72
    iget-object v0, p0, Lcom/my/target/ey;->dT:Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, 0x2

    sget v2, Lcom/my/target/ey;->dL:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 74
    iget-object v0, p0, Lcom/my/target/ey;->dQ:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/my/target/ey;->dP:Lcom/my/target/fb;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 75
    iget-object v0, p0, Lcom/my/target/ey;->dQ:Landroid/widget/FrameLayout;

    invoke-virtual {p0, v0}, Lcom/my/target/ey;->addView(Landroid/view/View;)V

    .line 76
    iget-object v0, p0, Lcom/my/target/ey;->dN:Lcom/my/target/ex;

    invoke-virtual {p0, v0}, Lcom/my/target/ey;->addView(Landroid/view/View;)V

    .line 77
    iget-object v0, p0, Lcom/my/target/ey;->dO:Lcom/my/target/by;

    invoke-virtual {p0, v0}, Lcom/my/target/ey;->addView(Landroid/view/View;)V

    .line 78
    return-void
.end method

.method private setLayoutOrientation(I)V
    .locals 5
    .param p1, "newOrientation"    # I

    .prologue
    const/4 v4, 0x0

    .line 153
    iput p1, p0, Lcom/my/target/ey;->orientation:I

    .line 155
    iget v0, p0, Lcom/my/target/ey;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 157
    iget-object v0, p0, Lcom/my/target/ey;->dR:Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/my/target/ey;->aw:Lcom/my/target/cm;

    const/16 v2, 0xc

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iget-object v2, p0, Lcom/my/target/ey;->aw:Lcom/my/target/cm;

    const/16 v3, 0x10

    invoke-virtual {v2, v3}, Lcom/my/target/cm;->n(I)I

    move-result v2

    invoke-virtual {v0, v4, v1, v4, v2}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 158
    iget-object v0, p0, Lcom/my/target/ey;->dT:Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/my/target/ey;->aw:Lcom/my/target/cm;

    const/16 v2, 0x38

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 159
    iget-object v0, p0, Lcom/my/target/ey;->dS:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v0, v4, v4, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 167
    :goto_0
    iget-object v0, p0, Lcom/my/target/ey;->dQ:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/my/target/ey;->dT:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 168
    iget-object v0, p0, Lcom/my/target/ey;->dN:Lcom/my/target/ex;

    iget-object v1, p0, Lcom/my/target/ey;->dR:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v0, v1}, Lcom/my/target/ex;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 169
    iget-object v0, p0, Lcom/my/target/ey;->dO:Lcom/my/target/by;

    iget-object v1, p0, Lcom/my/target/ey;->dS:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v0, v1}, Lcom/my/target/by;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 170
    return-void

    .line 163
    :cond_0
    iget-object v0, p0, Lcom/my/target/ey;->dR:Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/my/target/ey;->aw:Lcom/my/target/cm;

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iget-object v2, p0, Lcom/my/target/ey;->aw:Lcom/my/target/cm;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/my/target/cm;->n(I)I

    move-result v2

    invoke-virtual {v0, v4, v1, v4, v2}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 164
    iget-object v0, p0, Lcom/my/target/ey;->dT:Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/my/target/ey;->aw:Lcom/my/target/cm;

    const/16 v2, 0x1c

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 165
    iget-object v0, p0, Lcom/my/target/ey;->dS:Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/my/target/ey;->aw:Lcom/my/target/cm;

    const/4 v2, -0x4

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iget-object v2, p0, Lcom/my/target/ey;->aw:Lcom/my/target/cm;

    const/4 v3, -0x8

    invoke-virtual {v2, v3}, Lcom/my/target/cm;->n(I)I

    move-result v2

    invoke-virtual {v0, v1, v2, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/my/target/dw;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/my/target/dw;",
            "Ljava/util/List",
            "<",
            "Lcom/my/target/core/models/banners/g;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 93
    invoke-virtual {p1}, Lcom/my/target/dw;->getCloseIcon()Lcom/my/target/common/models/ImageData;

    move-result-object v0

    .line 94
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/my/target/common/models/ImageData;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 96
    iget-object v1, p0, Lcom/my/target/ey;->dO:Lcom/my/target/by;

    invoke-virtual {v0}, Lcom/my/target/common/models/ImageData;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v1, v0, v3}, Lcom/my/target/by;->b(Landroid/graphics/Bitmap;Z)V

    .line 104
    :goto_0
    invoke-virtual {p1}, Lcom/my/target/dw;->getBackgroundColor()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/my/target/ey;->setBackgroundColor(I)V

    .line 105
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    .line 106
    if-le v0, v3, :cond_1

    .line 108
    iget-object v1, p0, Lcom/my/target/ey;->dN:Lcom/my/target/ex;

    invoke-virtual {p1}, Lcom/my/target/dw;->n()I

    move-result v2

    invoke-virtual {p1}, Lcom/my/target/dw;->m()I

    move-result v3

    invoke-virtual {v1, v0, v2, v3}, Lcom/my/target/ex;->a(III)V

    .line 115
    :goto_1
    iget-object v0, p0, Lcom/my/target/ey;->dP:Lcom/my/target/fb;

    invoke-virtual {p1}, Lcom/my/target/dw;->getBackgroundColor()I

    move-result v1

    invoke-virtual {v0, p2, v1}, Lcom/my/target/fb;->a(Ljava/util/List;I)V

    .line 116
    return-void

    .line 100
    :cond_0
    iget-object v0, p0, Lcom/my/target/ey;->aw:Lcom/my/target/cm;

    const/16 v1, 0x24

    invoke-virtual {v0, v1}, Lcom/my/target/cm;->n(I)I

    move-result v0

    invoke-static {v0}, Lcom/my/target/core/resources/b;->d(I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 101
    iget-object v1, p0, Lcom/my/target/ey;->dO:Lcom/my/target/by;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/my/target/by;->b(Landroid/graphics/Bitmap;Z)V

    goto :goto_0

    .line 112
    :cond_1
    iget-object v0, p0, Lcom/my/target/ey;->dN:Lcom/my/target/ex;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/my/target/ex;->setVisibility(I)V

    goto :goto_1
.end method

.method public final g(I)V
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/my/target/ey;->dN:Lcom/my/target/ex;

    invoke-virtual {v0, p1}, Lcom/my/target/ex;->g(I)V

    .line 121
    return-void
.end method

.method protected final onMeasure(II)V
    .locals 2
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 126
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 1133
    int-to-float v0, v0

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 1136
    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    .line 1138
    const/4 v0, 0x2

    .line 1145
    :goto_0
    iget v1, p0, Lcom/my/target/ey;->orientation:I

    if-eq v0, v1, :cond_0

    .line 1147
    invoke-direct {p0, v0}, Lcom/my/target/ey;->setLayoutOrientation(I)V

    .line 128
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/RelativeLayout;->onMeasure(II)V

    .line 129
    return-void

    .line 1142
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final setCloseClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1, "closeClickListener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/my/target/ey;->dO:Lcom/my/target/by;

    invoke-virtual {v0, p1}, Lcom/my/target/by;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 83
    return-void
.end method

.method public final setFSSliderCardListener(Lcom/my/target/fb$c;)V
    .locals 1
    .param p1, "sliderCardListener"    # Lcom/my/target/fb$c;

    .prologue
    .line 87
    iget-object v0, p0, Lcom/my/target/ey;->dP:Lcom/my/target/fb;

    invoke-virtual {v0, p1}, Lcom/my/target/fb;->setSliderCardListener(Lcom/my/target/fb$c;)V

    .line 88
    return-void
.end method
