.class abstract Lcom/google/obf/fj$c;
.super Ljava/lang/Object;
.source "IMASDK"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/obf/fj;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<TT;>;"
    }
.end annotation


# instance fields
.field b:Lcom/google/obf/fj$d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/obf/fj$d",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field c:Lcom/google/obf/fj$d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/obf/fj$d",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field d:I

.field final synthetic e:Lcom/google/obf/fj;


# direct methods
.method constructor <init>(Lcom/google/obf/fj;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lcom/google/obf/fj$c;->e:Lcom/google/obf/fj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iget-object v0, p0, Lcom/google/obf/fj$c;->e:Lcom/google/obf/fj;

    iget-object v0, v0, Lcom/google/obf/fj;->e:Lcom/google/obf/fj$d;

    iget-object v0, v0, Lcom/google/obf/fj$d;->d:Lcom/google/obf/fj$d;

    iput-object v0, p0, Lcom/google/obf/fj$c;->b:Lcom/google/obf/fj$d;

    .line 3
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/obf/fj$c;->c:Lcom/google/obf/fj$d;

    .line 4
    iget-object v0, p0, Lcom/google/obf/fj$c;->e:Lcom/google/obf/fj;

    iget v0, v0, Lcom/google/obf/fj;->d:I

    iput v0, p0, Lcom/google/obf/fj$c;->d:I

    .line 5
    return-void
.end method


# virtual methods
.method final b()Lcom/google/obf/fj$d;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/obf/fj$d",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 7
    iget-object v0, p0, Lcom/google/obf/fj$c;->b:Lcom/google/obf/fj$d;

    .line 8
    iget-object v1, p0, Lcom/google/obf/fj$c;->e:Lcom/google/obf/fj;

    iget-object v1, v1, Lcom/google/obf/fj;->e:Lcom/google/obf/fj$d;

    if-ne v0, v1, :cond_0

    .line 9
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 10
    :cond_0
    iget-object v1, p0, Lcom/google/obf/fj$c;->e:Lcom/google/obf/fj;

    iget v1, v1, Lcom/google/obf/fj;->d:I

    iget v2, p0, Lcom/google/obf/fj$c;->d:I

    if-eq v1, v2, :cond_1

    .line 11
    new-instance v0, Ljava/util/ConcurrentModificationException;

    invoke-direct {v0}, Ljava/util/ConcurrentModificationException;-><init>()V

    throw v0

    .line 12
    :cond_1
    iget-object v1, v0, Lcom/google/obf/fj$d;->d:Lcom/google/obf/fj$d;

    iput-object v1, p0, Lcom/google/obf/fj$c;->b:Lcom/google/obf/fj$d;

    .line 13
    iput-object v0, p0, Lcom/google/obf/fj$c;->c:Lcom/google/obf/fj$d;

    return-object v0
.end method

.method public final hasNext()Z
    .locals 2

    .prologue
    .line 6
    iget-object v0, p0, Lcom/google/obf/fj$c;->b:Lcom/google/obf/fj$d;

    iget-object v1, p0, Lcom/google/obf/fj$c;->e:Lcom/google/obf/fj;

    iget-object v1, v1, Lcom/google/obf/fj;->e:Lcom/google/obf/fj$d;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final remove()V
    .locals 3

    .prologue
    .line 14
    iget-object v0, p0, Lcom/google/obf/fj$c;->c:Lcom/google/obf/fj$d;

    if-nez v0, :cond_0

    .line 15
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 16
    :cond_0
    iget-object v0, p0, Lcom/google/obf/fj$c;->e:Lcom/google/obf/fj;

    iget-object v1, p0, Lcom/google/obf/fj$c;->c:Lcom/google/obf/fj$d;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/obf/fj;->a(Lcom/google/obf/fj$d;Z)V

    .line 17
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/obf/fj$c;->c:Lcom/google/obf/fj$d;

    .line 18
    iget-object v0, p0, Lcom/google/obf/fj$c;->e:Lcom/google/obf/fj;

    iget v0, v0, Lcom/google/obf/fj;->d:I

    iput v0, p0, Lcom/google/obf/fj$c;->d:I

    .line 19
    return-void
.end method
