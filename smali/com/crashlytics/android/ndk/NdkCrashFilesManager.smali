.class Lcom/crashlytics/android/ndk/NdkCrashFilesManager;
.super Ljava/lang/Object;
.source "NdkCrashFilesManager.java"

# interfaces
.implements Lcom/crashlytics/android/ndk/CrashFilesManager;


# static fields
.field private static final NATIVE_ROOT_DIRECTORY_SUFFIX:Ljava/lang/String; = "native"

.field private static final ONLY_DIRECTORIES_FILTER:Ljava/io/FileFilter;


# instance fields
.field private final fileStore:Lio/fabric/sdk/android/services/persistence/FileStore;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    new-instance v0, Lcom/crashlytics/android/ndk/NdkCrashFilesManager$1;

    invoke-direct {v0}, Lcom/crashlytics/android/ndk/NdkCrashFilesManager$1;-><init>()V

    sput-object v0, Lcom/crashlytics/android/ndk/NdkCrashFilesManager;->ONLY_DIRECTORIES_FILTER:Ljava/io/FileFilter;

    return-void
.end method

.method public constructor <init>(Lio/fabric/sdk/android/services/persistence/FileStore;)V
    .locals 0
    .param p1, "fileStore"    # Lio/fabric/sdk/android/services/persistence/FileStore;

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/crashlytics/android/ndk/NdkCrashFilesManager;->fileStore:Lio/fabric/sdk/android/services/persistence/FileStore;

    .line 27
    return-void
.end method

.method private static getAllTimestampedDirectories(Ljava/io/File;)Ljava/util/TreeSet;
    .locals 3
    .param p0, "directory"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Ljava/util/TreeSet",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .prologue
    .line 48
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-nez v2, :cond_1

    .line 49
    :cond_0
    new-instance v1, Ljava/util/TreeSet;

    invoke-direct {v1}, Ljava/util/TreeSet;-><init>()V

    .line 61
    :goto_0
    return-object v1

    .line 52
    :cond_1
    sget-object v2, Lcom/crashlytics/android/ndk/NdkCrashFilesManager;->ONLY_DIRECTORIES_FILTER:Ljava/io/FileFilter;

    invoke-virtual {p0, v2}, Ljava/io/File;->listFiles(Ljava/io/FileFilter;)[Ljava/io/File;

    move-result-object v0

    .line 53
    .local v0, "files":[Ljava/io/File;
    new-instance v1, Ljava/util/TreeSet;

    new-instance v2, Lcom/crashlytics/android/ndk/NdkCrashFilesManager$2;

    invoke-direct {v2}, Lcom/crashlytics/android/ndk/NdkCrashFilesManager$2;-><init>()V

    invoke-direct {v1, v2}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    .line 60
    .local v1, "sorted":Ljava/util/TreeSet;, "Ljava/util/TreeSet<Ljava/io/File;>;"
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/TreeSet;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method private getNativeRootFileDirectory()Ljava/io/File;
    .locals 3

    .prologue
    .line 30
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/crashlytics/android/ndk/NdkCrashFilesManager;->fileStore:Lio/fabric/sdk/android/services/persistence/FileStore;

    invoke-interface {v1}, Lio/fabric/sdk/android/services/persistence/FileStore;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "native"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public getAllNativeDirectories()Ljava/util/TreeSet;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/TreeSet",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/crashlytics/android/ndk/NdkCrashFilesManager;->getNativeRootFileDirectory()Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, Lcom/crashlytics/android/ndk/NdkCrashFilesManager;->getAllTimestampedDirectories(Ljava/io/File;)Ljava/util/TreeSet;

    move-result-object v0

    return-object v0
.end method

.method public getNewNativeDirectory()Ljava/io/File;
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 35
    invoke-direct {p0}, Lcom/crashlytics/android/ndk/NdkCrashFilesManager;->getNativeRootFileDirectory()Ljava/io/File;

    move-result-object v0

    .line 37
    .local v0, "root":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    move-result v5

    if-nez v5, :cond_0

    .line 44
    :goto_0
    return-object v4

    .line 41
    :cond_0
    new-instance v5, Lio/fabric/sdk/android/services/common/SystemCurrentTimeProvider;

    invoke-direct {v5}, Lio/fabric/sdk/android/services/common/SystemCurrentTimeProvider;-><init>()V

    invoke-virtual {v5}, Lio/fabric/sdk/android/services/common/SystemCurrentTimeProvider;->getCurrentTimeMillis()J

    move-result-wide v2

    .line 42
    .local v2, "timestamp":J
    new-instance v1, Ljava/io/File;

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v0, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 44
    .local v1, "timestampFolder":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->mkdir()Z

    move-result v5

    if-eqz v5, :cond_1

    .end local v1    # "timestampFolder":Ljava/io/File;
    :goto_1
    move-object v4, v1

    goto :goto_0

    .restart local v1    # "timestampFolder":Ljava/io/File;
    :cond_1
    move-object v1, v4

    goto :goto_1
.end method
