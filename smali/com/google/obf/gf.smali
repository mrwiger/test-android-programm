.class public final enum Lcom/google/obf/gf;
.super Ljava/lang/Enum;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/obf/gf;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/obf/gf;

.field public static final enum b:Lcom/google/obf/gf;

.field public static final enum c:Lcom/google/obf/gf;

.field public static final enum d:Lcom/google/obf/gf;

.field public static final enum e:Lcom/google/obf/gf;

.field public static final enum f:Lcom/google/obf/gf;

.field public static final enum g:Lcom/google/obf/gf;

.field public static final enum h:Lcom/google/obf/gf;

.field public static final enum i:Lcom/google/obf/gf;

.field public static final enum j:Lcom/google/obf/gf;

.field private static final synthetic k:[Lcom/google/obf/gf;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 4
    new-instance v0, Lcom/google/obf/gf;

    const-string v1, "BEGIN_ARRAY"

    invoke-direct {v0, v1, v3}, Lcom/google/obf/gf;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/gf;->a:Lcom/google/obf/gf;

    .line 5
    new-instance v0, Lcom/google/obf/gf;

    const-string v1, "END_ARRAY"

    invoke-direct {v0, v1, v4}, Lcom/google/obf/gf;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/gf;->b:Lcom/google/obf/gf;

    .line 6
    new-instance v0, Lcom/google/obf/gf;

    const-string v1, "BEGIN_OBJECT"

    invoke-direct {v0, v1, v5}, Lcom/google/obf/gf;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/gf;->c:Lcom/google/obf/gf;

    .line 7
    new-instance v0, Lcom/google/obf/gf;

    const-string v1, "END_OBJECT"

    invoke-direct {v0, v1, v6}, Lcom/google/obf/gf;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/gf;->d:Lcom/google/obf/gf;

    .line 8
    new-instance v0, Lcom/google/obf/gf;

    const-string v1, "NAME"

    invoke-direct {v0, v1, v7}, Lcom/google/obf/gf;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/gf;->e:Lcom/google/obf/gf;

    .line 9
    new-instance v0, Lcom/google/obf/gf;

    const-string v1, "STRING"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/obf/gf;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/gf;->f:Lcom/google/obf/gf;

    .line 10
    new-instance v0, Lcom/google/obf/gf;

    const-string v1, "NUMBER"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/obf/gf;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/gf;->g:Lcom/google/obf/gf;

    .line 11
    new-instance v0, Lcom/google/obf/gf;

    const-string v1, "BOOLEAN"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/google/obf/gf;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/gf;->h:Lcom/google/obf/gf;

    .line 12
    new-instance v0, Lcom/google/obf/gf;

    const-string v1, "NULL"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/google/obf/gf;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/gf;->i:Lcom/google/obf/gf;

    .line 13
    new-instance v0, Lcom/google/obf/gf;

    const-string v1, "END_DOCUMENT"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/google/obf/gf;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/gf;->j:Lcom/google/obf/gf;

    .line 14
    const/16 v0, 0xa

    new-array v0, v0, [Lcom/google/obf/gf;

    sget-object v1, Lcom/google/obf/gf;->a:Lcom/google/obf/gf;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/obf/gf;->b:Lcom/google/obf/gf;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/obf/gf;->c:Lcom/google/obf/gf;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/obf/gf;->d:Lcom/google/obf/gf;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/obf/gf;->e:Lcom/google/obf/gf;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/obf/gf;->f:Lcom/google/obf/gf;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/obf/gf;->g:Lcom/google/obf/gf;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/obf/gf;->h:Lcom/google/obf/gf;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/obf/gf;->i:Lcom/google/obf/gf;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/obf/gf;->j:Lcom/google/obf/gf;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/obf/gf;->k:[Lcom/google/obf/gf;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/obf/gf;
    .locals 1

    .prologue
    .line 2
    const-class v0, Lcom/google/obf/gf;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/obf/gf;

    return-object v0
.end method

.method public static values()[Lcom/google/obf/gf;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lcom/google/obf/gf;->k:[Lcom/google/obf/gf;

    invoke-virtual {v0}, [Lcom/google/obf/gf;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/obf/gf;

    return-object v0
.end method
