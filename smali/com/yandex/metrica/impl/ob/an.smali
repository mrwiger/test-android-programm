.class public Lcom/yandex/metrica/impl/ob/an;
.super Lcom/yandex/metrica/impl/ob/ah;
.source "SourceFile"


# instance fields
.field private final a:Lcom/yandex/metrica/impl/ob/cq;

.field private final b:Lcom/yandex/metrica/impl/ob/ez;


# direct methods
.method protected constructor <init>(Lcom/yandex/metrica/impl/ob/v;Lcom/yandex/metrica/impl/ob/cq;Lcom/yandex/metrica/impl/ob/ez;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/yandex/metrica/impl/ob/ah;-><init>(Lcom/yandex/metrica/impl/ob/v;)V

    .line 35
    iput-object p2, p0, Lcom/yandex/metrica/impl/ob/an;->a:Lcom/yandex/metrica/impl/ob/cq;

    .line 36
    iput-object p3, p0, Lcom/yandex/metrica/impl/ob/an;->b:Lcom/yandex/metrica/impl/ob/ez;

    .line 37
    return-void
.end method


# virtual methods
.method public a(Lcom/yandex/metrica/impl/i;)Z
    .locals 5

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/an;->a()Lcom/yandex/metrica/impl/ob/v;

    move-result-object v0

    .line 48
    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/v;->D()Lcom/yandex/metrica/impl/ob/db;

    move-result-object v1

    .line 49
    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/db;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/v;->B()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 52
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/an;->a:Lcom/yandex/metrica/impl/ob/cq;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/cq;->c()Lcom/yandex/metrica/impl/ob/cu;

    move-result-object v1

    .line 53
    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/an;->b:Lcom/yandex/metrica/impl/ob/ez;

    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/an;->a()Lcom/yandex/metrica/impl/ob/v;

    move-result-object v3

    invoke-virtual {v3}, Lcom/yandex/metrica/impl/ob/v;->n()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/cu;->a()Ljava/util/List;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/yandex/metrica/impl/ob/ez;->a(Landroid/content/Context;Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    .line 54
    if-nez v2, :cond_1

    .line 55
    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/v;->r()V

    .line 62
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 57
    :cond_1
    invoke-static {p1, v2}, Lcom/yandex/metrica/impl/i;->a(Lcom/yandex/metrica/impl/i;Ljava/util/List;)Lcom/yandex/metrica/impl/i;

    move-result-object v3

    .line 58
    invoke-virtual {v0, v3}, Lcom/yandex/metrica/impl/ob/v;->g(Lcom/yandex/metrica/impl/i;)V

    .line 59
    invoke-virtual {v1, v2}, Lcom/yandex/metrica/impl/ob/cu;->a(Ljava/util/List;)V

    goto :goto_0
.end method
