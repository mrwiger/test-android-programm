.class public final Lcom/yandex/metrica/c$d;
.super Lcom/yandex/metrica/impl/ob/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/yandex/metrica/c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "d"
.end annotation


# static fields
.field private static volatile f:[Lcom/yandex/metrica/c$d;


# instance fields
.field public b:Ljava/lang/String;

.field public c:I

.field public d:Ljava/lang/String;

.field public e:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3414
    invoke-direct {p0}, Lcom/yandex/metrica/impl/ob/d;-><init>()V

    .line 3415
    invoke-virtual {p0}, Lcom/yandex/metrica/c$d;->e()Lcom/yandex/metrica/c$d;

    .line 3416
    return-void
.end method

.method public static d()[Lcom/yandex/metrica/c$d;
    .locals 2

    .prologue
    .line 3389
    sget-object v0, Lcom/yandex/metrica/c$d;->f:[Lcom/yandex/metrica/c$d;

    if-nez v0, :cond_1

    .line 3390
    sget-object v1, Lcom/yandex/metrica/impl/ob/c;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 3392
    :try_start_0
    sget-object v0, Lcom/yandex/metrica/c$d;->f:[Lcom/yandex/metrica/c$d;

    if-nez v0, :cond_0

    .line 3393
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/yandex/metrica/c$d;

    sput-object v0, Lcom/yandex/metrica/c$d;->f:[Lcom/yandex/metrica/c$d;

    .line 3395
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3397
    :cond_1
    sget-object v0, Lcom/yandex/metrica/c$d;->f:[Lcom/yandex/metrica/c$d;

    return-object v0

    .line 3395
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public a(Lcom/yandex/metrica/impl/ob/b;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 3430
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/yandex/metrica/c$d;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/yandex/metrica/impl/ob/b;->a(ILjava/lang/String;)V

    .line 3431
    iget v0, p0, Lcom/yandex/metrica/c$d;->c:I

    if-eqz v0, :cond_0

    .line 3432
    const/4 v0, 0x2

    iget v1, p0, Lcom/yandex/metrica/c$d;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/yandex/metrica/impl/ob/b;->c(II)V

    .line 3434
    :cond_0
    iget-object v0, p0, Lcom/yandex/metrica/c$d;->d:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 3435
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/yandex/metrica/c$d;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/yandex/metrica/impl/ob/b;->a(ILjava/lang/String;)V

    .line 3437
    :cond_1
    iget-boolean v0, p0, Lcom/yandex/metrica/c$d;->e:Z

    if-eqz v0, :cond_2

    .line 3438
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/yandex/metrica/c$d;->e:Z

    invoke-virtual {p1, v0, v1}, Lcom/yandex/metrica/impl/ob/b;->a(IZ)V

    .line 3440
    :cond_2
    invoke-super {p0, p1}, Lcom/yandex/metrica/impl/ob/d;->a(Lcom/yandex/metrica/impl/ob/b;)V

    .line 3441
    return-void
.end method

.method protected c()I
    .locals 3

    .prologue
    .line 3445
    invoke-super {p0}, Lcom/yandex/metrica/impl/ob/d;->c()I

    move-result v0

    .line 3446
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/yandex/metrica/c$d;->b:Ljava/lang/String;

    .line 3447
    invoke-static {v1, v2}, Lcom/yandex/metrica/impl/ob/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3448
    iget v1, p0, Lcom/yandex/metrica/c$d;->c:I

    if-eqz v1, :cond_0

    .line 3449
    const/4 v1, 0x2

    iget v2, p0, Lcom/yandex/metrica/c$d;->c:I

    .line 3450
    invoke-static {v1, v2}, Lcom/yandex/metrica/impl/ob/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3452
    :cond_0
    iget-object v1, p0, Lcom/yandex/metrica/c$d;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 3453
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/yandex/metrica/c$d;->d:Ljava/lang/String;

    .line 3454
    invoke-static {v1, v2}, Lcom/yandex/metrica/impl/ob/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3456
    :cond_1
    iget-boolean v1, p0, Lcom/yandex/metrica/c$d;->e:Z

    if-eqz v1, :cond_2

    .line 3457
    const/4 v1, 0x4

    .line 3458
    invoke-static {v1}, Lcom/yandex/metrica/impl/ob/b;->e(I)I

    move-result v1

    add-int/2addr v0, v1

    .line 3460
    :cond_2
    return v0
.end method

.method public e()Lcom/yandex/metrica/c$d;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3419
    const-string v0, ""

    iput-object v0, p0, Lcom/yandex/metrica/c$d;->b:Ljava/lang/String;

    .line 3420
    iput v1, p0, Lcom/yandex/metrica/c$d;->c:I

    .line 3421
    const-string v0, ""

    iput-object v0, p0, Lcom/yandex/metrica/c$d;->d:Ljava/lang/String;

    .line 3422
    iput-boolean v1, p0, Lcom/yandex/metrica/c$d;->e:Z

    .line 3423
    const/4 v0, -0x1

    iput v0, p0, Lcom/yandex/metrica/c$d;->a:I

    .line 3424
    return-object p0
.end method
