.class public abstract Ltoothpick/registries/memberinjector/AbstractMemberInjectorRegistry;
.super Ljava/lang/Object;
.source "AbstractMemberInjectorRegistry.java"

# interfaces
.implements Ltoothpick/registries/MemberInjectorRegistry;


# instance fields
.field private childrenRegistries:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ltoothpick/registries/MemberInjectorRegistry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ltoothpick/registries/memberinjector/AbstractMemberInjectorRegistry;->childrenRegistries:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public addChildRegistry(Ltoothpick/registries/MemberInjectorRegistry;)V
    .locals 1
    .param p1, "childRegistry"    # Ltoothpick/registries/MemberInjectorRegistry;

    .prologue
    .line 20
    iget-object v0, p0, Ltoothpick/registries/memberinjector/AbstractMemberInjectorRegistry;->childrenRegistries:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 21
    return-void
.end method

.method protected getMemberInjectorInChildrenRegistries(Ljava/lang/Class;)Ltoothpick/MemberInjector;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Ltoothpick/MemberInjector",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 34
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    iget-object v2, p0, Ltoothpick/registries/memberinjector/AbstractMemberInjectorRegistry;->childrenRegistries:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ltoothpick/registries/MemberInjectorRegistry;

    .line 35
    .local v1, "registry":Ltoothpick/registries/MemberInjectorRegistry;
    invoke-interface {v1, p1}, Ltoothpick/registries/MemberInjectorRegistry;->getMemberInjector(Ljava/lang/Class;)Ltoothpick/MemberInjector;

    move-result-object v0

    .line 36
    .local v0, "memberInjector":Ltoothpick/MemberInjector;, "Ltoothpick/MemberInjector<TT;>;"
    if-eqz v0, :cond_0

    .line 40
    .end local v0    # "memberInjector":Ltoothpick/MemberInjector;, "Ltoothpick/MemberInjector<TT;>;"
    .end local v1    # "registry":Ltoothpick/registries/MemberInjectorRegistry;
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
