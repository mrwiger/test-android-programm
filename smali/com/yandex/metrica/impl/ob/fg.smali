.class public Lcom/yandex/metrica/impl/ob/fg;
.super Lcom/yandex/metrica/impl/ob/ff;
.source "SourceFile"


# static fields
.field private static final c:Lcom/yandex/metrica/impl/ob/fm;

.field private static final d:Lcom/yandex/metrica/impl/ob/fm;

.field private static final e:Lcom/yandex/metrica/impl/ob/fm;

.field private static final f:Lcom/yandex/metrica/impl/ob/fm;

.field private static final g:Lcom/yandex/metrica/impl/ob/fm;

.field private static final h:Lcom/yandex/metrica/impl/ob/fm;

.field private static final i:Lcom/yandex/metrica/impl/ob/fm;

.field private static final j:Lcom/yandex/metrica/impl/ob/fm;

.field private static final k:Lcom/yandex/metrica/impl/ob/fm;

.field private static final l:Lcom/yandex/metrica/impl/ob/fm;


# instance fields
.field private m:Lcom/yandex/metrica/impl/ob/fm;

.field private n:Lcom/yandex/metrica/impl/ob/fm;

.field private o:Lcom/yandex/metrica/impl/ob/fm;

.field private p:Lcom/yandex/metrica/impl/ob/fm;

.field private q:Lcom/yandex/metrica/impl/ob/fm;

.field private r:Lcom/yandex/metrica/impl/ob/fm;

.field private s:Lcom/yandex/metrica/impl/ob/fm;

.field private t:Lcom/yandex/metrica/impl/ob/fm;

.field private u:Lcom/yandex/metrica/impl/ob/fm;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 28
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "UUID"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/fg;->c:Lcom/yandex/metrica/impl/ob/fm;

    .line 29
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "DEVICEID"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/fg;->d:Lcom/yandex/metrica/impl/ob/fm;

    .line 30
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "DEVICEID_2"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/fg;->e:Lcom/yandex/metrica/impl/ob/fm;

    .line 31
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "DEVICEID_3"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/fg;->f:Lcom/yandex/metrica/impl/ob/fm;

    .line 32
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "AD_URL_GET"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/fg;->g:Lcom/yandex/metrica/impl/ob/fm;

    .line 33
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "AD_URL_REPORT"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/fg;->h:Lcom/yandex/metrica/impl/ob/fm;

    .line 34
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "HOST_URL"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/fg;->i:Lcom/yandex/metrica/impl/ob/fm;

    .line 35
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "SERVER_TIME_OFFSET"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/fg;->j:Lcom/yandex/metrica/impl/ob/fm;

    .line 36
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "STARTUP_REQUEST_TIME"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/fg;->k:Lcom/yandex/metrica/impl/ob/fm;

    .line 37
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "CLIDS"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/fg;->l:Lcom/yandex/metrica/impl/ob/fm;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 51
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/yandex/metrica/impl/ob/ff;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 53
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    sget-object v1, Lcom/yandex/metrica/impl/ob/fg;->c:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/fg;->m:Lcom/yandex/metrica/impl/ob/fm;

    .line 54
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    sget-object v1, Lcom/yandex/metrica/impl/ob/fg;->d:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/fg;->n:Lcom/yandex/metrica/impl/ob/fm;

    .line 55
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    sget-object v1, Lcom/yandex/metrica/impl/ob/fg;->e:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/fg;->o:Lcom/yandex/metrica/impl/ob/fm;

    .line 56
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    sget-object v1, Lcom/yandex/metrica/impl/ob/fg;->f:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/fg;->p:Lcom/yandex/metrica/impl/ob/fm;

    .line 57
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    sget-object v1, Lcom/yandex/metrica/impl/ob/fg;->g:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/fg;->q:Lcom/yandex/metrica/impl/ob/fm;

    .line 58
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    sget-object v1, Lcom/yandex/metrica/impl/ob/fg;->h:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/fg;->r:Lcom/yandex/metrica/impl/ob/fm;

    .line 59
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    sget-object v1, Lcom/yandex/metrica/impl/ob/fg;->i:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    .line 60
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    sget-object v1, Lcom/yandex/metrica/impl/ob/fg;->j:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/fg;->s:Lcom/yandex/metrica/impl/ob/fm;

    .line 61
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    sget-object v1, Lcom/yandex/metrica/impl/ob/fg;->k:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/fg;->t:Lcom/yandex/metrica/impl/ob/fm;

    .line 62
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    sget-object v1, Lcom/yandex/metrica/impl/ob/fg;->l:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/fg;->u:Lcom/yandex/metrica/impl/ob/fm;

    .line 63
    return-void
.end method


# virtual methods
.method public a(J)J
    .locals 3

    .prologue
    .line 96
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fg;->b:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fg;->s:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1, p2}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public a()Ljava/lang/String;
    .locals 5

    .prologue
    .line 79
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fg;->b:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fg;->o:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/fg;->b:Landroid/content/SharedPreferences;

    iget-object v3, p0, Lcom/yandex/metrica/impl/ob/fg;->n:Lcom/yandex/metrica/impl/ob/fm;

    .line 80
    invoke-virtual {v3}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 79
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 71
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fg;->b:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fg;->m:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(J)J
    .locals 3

    .prologue
    .line 100
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fg;->b:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fg;->t:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1, p2}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public b()Lcom/yandex/metrica/impl/ob/fg;
    .locals 1

    .prologue
    .line 140
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/fg;->h()Lcom/yandex/metrica/impl/ob/ff;

    move-result-object v0

    check-cast v0, Lcom/yandex/metrica/impl/ob/fg;

    return-object v0
.end method

.method public b(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 75
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fg;->b:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fg;->p:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 84
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fg;->b:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fg;->q:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;"
        }
    .end annotation

    .prologue
    .line 144
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fg;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public d(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 88
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fg;->b:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fg;->r:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 104
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fg;->b:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fg;->u:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    const-string v0, "_startupinfopreferences"

    return-object v0
.end method
