.class public Lcom/samsung/multiscreen/Channel;
.super Ljava/lang/Object;
.source "Channel.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;,
        Lcom/samsung/multiscreen/Channel$OnErrorListener;,
        Lcom/samsung/multiscreen/Channel$OnMessageListener;,
        Lcom/samsung/multiscreen/Channel$OnReadyListener;,
        Lcom/samsung/multiscreen/Channel$OnClientDisconnectListener;,
        Lcom/samsung/multiscreen/Channel$OnClientConnectListener;,
        Lcom/samsung/multiscreen/Channel$OnDisconnectListener;,
        Lcom/samsung/multiscreen/Channel$OnConnectListener;
    }
.end annotation


# static fields
.field private static final CA_CRT:Ljava/lang/String; = "-----BEGIN CERTIFICATE-----\nMIIDhjCCAm6gAwIBAgIJAPm7naJvG91yMA0GCSqGSIb3DQEBCwUAMFcxCzAJBgNV\nBAYTAktSMRUwEwYDVQQKEwxTbWFydFZpZXdTREsxMTAvBgNVBAMTKFNtYXJ0Vmll\nd1NESyBSb290IENlcml0aWZpY2F0ZSBBdXRob3JpdHkwHhcNMTYwNzI5MDUzNDEw\nWhcNMzYwNzI5MDUzNDEwWjBXMQswCQYDVQQGEwJLUjEVMBMGA1UEChMMU21hcnRW\naWV3U0RLMTEwLwYDVQQDEyhTbWFydFZpZXdTREsgUm9vdCBDZXJpdGlmaWNhdGUg\nQXV0aG9yaXR5MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArtBcclIW\nEuObUkeTn+FW3m6Lm/YpwAOeABCtq6RKnBcq6jzEo3I433cSuVC2DrWGiiYi62Qm\niAzOHEtkvRctj+jEuK7ZKneKkxQ5261os0RsvWG7fONVb4m0ZRBydykgfu/PLwUB\nMWeiF3PB6w7YCzN1MJzb9EISFlhEcqMxDHgwGWHZYo/CTWtIwBVZ07mhdrCQaV2r\nLLJInA+4Wh9nXRO82qRnqYqFZfV7psIOW4MqfjWqNcKAHWWZ1gKrdZc9fPb2YVK4\nOIlaT3Qq9DOCveeU5T8d3MGEoiFnXt4Lp5656nI7MbkAsPEFFRHFkBK3o8CE1HLp\nsELQa6GBRe8WPQIDAQABo1UwUzASBgNVHRMBAf8ECDAGAQH/AgEAMB0GA1UdDgQW\nBBRQyhCp74M+t2GwCiH3g3Aau0AX7DALBgNVHQ8EBAMCAQYwEQYJYIZIAYb4QgEB\nBAQDAgAHMA0GCSqGSIb3DQEBCwUAA4IBAQAVIEeJo4vGsKPZBoY19hCXZnqB6Qcm\nOnWZzAZ0am8OQHQ4/LbSJ+Vnxh7eFiLtPQwuSHJ1a95ODA7RlNgnpC8ymHsL5Wl5\nUKOq5jOs3Jfa0aG99H9TsFKBysXlsBHfaHX+8/AoZUJDOksNeQigj3n4wCdLEPvt\nUpI9qJEjuzXeKxVhwnDkc/AvOuSGUaPiSeCSxy+xpcyWCANc4uVXtOxJluQvy8aC\nm6l0yG3Ucg09yCIkPzKtzG/kAadDRrTOYi/x4ZECtdamHQxncEnb3D881veLc6+s\nztEvDx0F77vRtadpeBxNZKivG2kJrymuf47pGIS0FlC5+/5ieV54+1/d\n-----END CERTIFICATE-----"

.field private static final CLIENT_CONNECT_EVENT:Ljava/lang/String; = "ms.channel.clientConnect"

.field private static final CLIENT_DISCONNECT_EVENT:Ljava/lang/String; = "ms.channel.clientDisconnect"

.field private static final CONNECT_EVENT:Ljava/lang/String; = "ms.channel.connect"

.field private static final DISCONNECT_EVENT:Ljava/lang/String; = "ms.channel.disconnect"

.field private static final ERROR_EVENT:Ljava/lang/String; = "ms.error"

.field private static final HTTPS_PROTOCOL:Ljava/lang/String; = "https:"

.field private static final HTTP_PROTOCOL:Ljava/lang/String; = "http:"

.field private static final MULTICAST_PORT:Ljava/lang/String; = "8001"

.field private static final READY_EVENT:Ljava/lang/String; = "ms.channel.ready"

.field private static final ROUTE:Ljava/lang/String; = "channels"

.field private static final SECURE_PORT:Ljava/lang/String; = "8002"

.field private static final TAG:Ljava/lang/String; = "Channel"

.field private static final TLS_PROTOCOL:Ljava/lang/String; = "TLS"

.field private static random:Ljava/security/SecureRandom;


# instance fields
.field private callbacks:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/multiscreen/Result;",
            ">;"
        }
    .end annotation
.end field

.field private clients:Lcom/samsung/multiscreen/Clients;

.field protected connected:Z

.field private final connectionHandler:Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;

.field private debug:Z

.field private disconnecting:Z

.field private final id:Ljava/lang/String;

.field private messageListeners:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/multiscreen/Channel$OnMessageListener;",
            ">;>;"
        }
    .end annotation
.end field

.field private volatile onClientConnectListener:Lcom/samsung/multiscreen/Channel$OnClientConnectListener;

.field private volatile onClientDisconnectListener:Lcom/samsung/multiscreen/Channel$OnClientDisconnectListener;

.field private volatile onConnectListener:Lcom/samsung/multiscreen/Channel$OnConnectListener;

.field private volatile onDisconnectListener:Lcom/samsung/multiscreen/Channel$OnDisconnectListener;

.field private volatile onErrorListener:Lcom/samsung/multiscreen/Channel$OnErrorListener;

.field private onReadyListener:Lcom/samsung/multiscreen/Channel$OnReadyListener;

.field protected securityMode:Z

.field private service:Lcom/samsung/multiscreen/Service;

.field private final uri:Landroid/net/Uri;

.field private webSocket:Lcom/koushikdutta/async/http/WebSocket;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 186
    new-instance v0, Ljava/security/SecureRandom;

    invoke-direct {v0}, Ljava/security/SecureRandom;-><init>()V

    sput-object v0, Lcom/samsung/multiscreen/Channel;->random:Ljava/security/SecureRandom;

    return-void
.end method

.method protected constructor <init>(Lcom/samsung/multiscreen/Service;Landroid/net/Uri;Ljava/lang/String;)V
    .locals 2
    .param p1, "service"    # Lcom/samsung/multiscreen/Service;
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "id"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 132
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 117
    new-instance v0, Lcom/samsung/multiscreen/Clients;

    invoke-direct {v0, p0}, Lcom/samsung/multiscreen/Clients;-><init>(Lcom/samsung/multiscreen/Channel;)V

    iput-object v0, p0, Lcom/samsung/multiscreen/Channel;->clients:Lcom/samsung/multiscreen/Clients;

    .line 125
    iput-boolean v1, p0, Lcom/samsung/multiscreen/Channel;->securityMode:Z

    .line 182
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/multiscreen/Channel;->messageListeners:Ljava/util/Map;

    .line 184
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/multiscreen/Channel;->callbacks:Ljava/util/Map;

    .line 192
    iput-boolean v1, p0, Lcom/samsung/multiscreen/Channel;->disconnecting:Z

    .line 197
    iput-boolean v1, p0, Lcom/samsung/multiscreen/Channel;->debug:Z

    .line 204
    new-instance v0, Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;

    invoke-direct {v0, p0}, Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;-><init>(Lcom/samsung/multiscreen/Channel;)V

    iput-object v0, p0, Lcom/samsung/multiscreen/Channel;->connectionHandler:Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;

    .line 133
    iput-object p1, p0, Lcom/samsung/multiscreen/Channel;->service:Lcom/samsung/multiscreen/Service;

    .line 134
    iput-object p2, p0, Lcom/samsung/multiscreen/Channel;->uri:Landroid/net/Uri;

    .line 135
    iput-object p3, p0, Lcom/samsung/multiscreen/Channel;->id:Ljava/lang/String;

    .line 136
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/multiscreen/Channel;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/multiscreen/Channel;
    .param p1, "x1"    # Landroid/net/Uri;

    .prologue
    .line 80
    invoke-direct {p0, p1}, Lcom/samsung/multiscreen/Channel;->getWebSocketConnectionURL(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/multiscreen/Channel;)Lcom/samsung/multiscreen/Service;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/multiscreen/Channel;

    .prologue
    .line 80
    iget-object v0, p0, Lcom/samsung/multiscreen/Channel;->service:Lcom/samsung/multiscreen/Service;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/samsung/multiscreen/Channel;)Lcom/samsung/multiscreen/Channel$OnConnectListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/multiscreen/Channel;

    .prologue
    .line 80
    iget-object v0, p0, Lcom/samsung/multiscreen/Channel;->onConnectListener:Lcom/samsung/multiscreen/Channel$OnConnectListener;

    return-object v0
.end method

.method static synthetic access$102(Lcom/samsung/multiscreen/Channel;Lcom/samsung/multiscreen/Service;)Lcom/samsung/multiscreen/Service;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/multiscreen/Channel;
    .param p1, "x1"    # Lcom/samsung/multiscreen/Service;

    .prologue
    .line 80
    iput-object p1, p0, Lcom/samsung/multiscreen/Channel;->service:Lcom/samsung/multiscreen/Service;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/samsung/multiscreen/Channel;)Lcom/samsung/multiscreen/Channel$OnClientConnectListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/multiscreen/Channel;

    .prologue
    .line 80
    iget-object v0, p0, Lcom/samsung/multiscreen/Channel;->onClientConnectListener:Lcom/samsung/multiscreen/Channel$OnClientConnectListener;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/samsung/multiscreen/Channel;)Lcom/samsung/multiscreen/Channel$OnClientDisconnectListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/multiscreen/Channel;

    .prologue
    .line 80
    iget-object v0, p0, Lcom/samsung/multiscreen/Channel;->onClientDisconnectListener:Lcom/samsung/multiscreen/Channel$OnClientDisconnectListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/multiscreen/Channel;)Lcom/koushikdutta/async/http/WebSocket;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/multiscreen/Channel;

    .prologue
    .line 80
    iget-object v0, p0, Lcom/samsung/multiscreen/Channel;->webSocket:Lcom/koushikdutta/async/http/WebSocket;

    return-object v0
.end method

.method static synthetic access$202(Lcom/samsung/multiscreen/Channel;Lcom/koushikdutta/async/http/WebSocket;)Lcom/koushikdutta/async/http/WebSocket;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/multiscreen/Channel;
    .param p1, "x1"    # Lcom/koushikdutta/async/http/WebSocket;

    .prologue
    .line 80
    iput-object p1, p0, Lcom/samsung/multiscreen/Channel;->webSocket:Lcom/koushikdutta/async/http/WebSocket;

    return-object p1
.end method

.method static synthetic access$300(Lcom/samsung/multiscreen/Channel;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/multiscreen/Channel;

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/samsung/multiscreen/Channel;->handleSocketClosedAndNotify()V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/multiscreen/Channel;)Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/multiscreen/Channel;

    .prologue
    .line 80
    iget-object v0, p0, Lcom/samsung/multiscreen/Channel;->connectionHandler:Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/multiscreen/Channel;Ljava/util/Map;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/multiscreen/Channel;
    .param p1, "x1"    # Ljava/util/Map;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 80
    invoke-direct {p0, p1, p2}, Lcom/samsung/multiscreen/Channel;->handleConnectMessage(Ljava/util/Map;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$600(Lcom/samsung/multiscreen/Channel;Lcom/koushikdutta/async/DataEmitter;Lcom/koushikdutta/async/ByteBufferList;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/multiscreen/Channel;
    .param p1, "x1"    # Lcom/koushikdutta/async/DataEmitter;
    .param p2, "x2"    # Lcom/koushikdutta/async/ByteBufferList;

    .prologue
    .line 80
    invoke-direct {p0, p1, p2}, Lcom/samsung/multiscreen/Channel;->handleBinaryMessage(Lcom/koushikdutta/async/DataEmitter;Lcom/koushikdutta/async/ByteBufferList;)V

    return-void
.end method

.method static synthetic access$700(Lcom/samsung/multiscreen/Channel;)Lcom/samsung/multiscreen/Channel$OnDisconnectListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/multiscreen/Channel;

    .prologue
    .line 80
    iget-object v0, p0, Lcom/samsung/multiscreen/Channel;->onDisconnectListener:Lcom/samsung/multiscreen/Channel$OnDisconnectListener;

    return-object v0
.end method

.method static synthetic access$800(Lcom/samsung/multiscreen/Channel;)Lcom/samsung/multiscreen/Channel$OnErrorListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/multiscreen/Channel;

    .prologue
    .line 80
    iget-object v0, p0, Lcom/samsung/multiscreen/Channel;->onErrorListener:Lcom/samsung/multiscreen/Channel$OnErrorListener;

    return-object v0
.end method

.method static synthetic access$900(Lcom/samsung/multiscreen/Channel;)Lcom/samsung/multiscreen/Clients;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/multiscreen/Channel;

    .prologue
    .line 80
    iget-object v0, p0, Lcom/samsung/multiscreen/Channel;->clients:Lcom/samsung/multiscreen/Clients;

    return-object v0
.end method

.method static create(Lcom/samsung/multiscreen/Service;Landroid/net/Uri;)Lcom/samsung/multiscreen/Channel;
    .locals 2
    .param p0, "service"    # Lcom/samsung/multiscreen/Service;
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 788
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 789
    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 791
    :cond_1
    new-instance v0, Lcom/samsung/multiscreen/Channel;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, p1, v1}, Lcom/samsung/multiscreen/Channel;-><init>(Lcom/samsung/multiscreen/Service;Landroid/net/Uri;Ljava/lang/String;)V

    return-object v0
.end method

.method private createBinaryMessage(Ljava/lang/String;[B)[B
    .locals 4
    .param p1, "json"    # Ljava/lang/String;
    .param p2, "payload"    # [B

    .prologue
    .line 796
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    array-length v1, v2

    .line 798
    .local v1, "headerLength":I
    add-int/lit8 v2, v1, 0x2

    array-length v3, p2

    add-int/2addr v2, v3

    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 801
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    int-to-short v2, v1

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 804
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 807
    invoke-virtual {v0, p2}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 809
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v2

    return-object v2
.end method

.method private emit(Lcom/samsung/multiscreen/Message;)V
    .locals 8
    .param p1, "message"    # Lcom/samsung/multiscreen/Message;

    .prologue
    .line 1132
    if-nez p1, :cond_0

    .line 1133
    new-instance v3, Ljava/lang/NullPointerException;

    invoke-direct {v3}, Ljava/lang/NullPointerException;-><init>()V

    throw v3

    .line 1136
    :cond_0
    iget-object v3, p0, Lcom/samsung/multiscreen/Channel;->messageListeners:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/samsung/multiscreen/Message;->getEvent()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 1138
    .local v2, "onMessageListeners":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/multiscreen/Channel$OnMessageListener;>;"
    if-eqz v2, :cond_1

    .line 1139
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/multiscreen/Channel$OnMessageListener;

    .line 1140
    .local v0, "listener":Lcom/samsung/multiscreen/Channel$OnMessageListener;
    move-object v1, v0

    .line 1141
    .local v1, "messagelistener":Lcom/samsung/multiscreen/Channel$OnMessageListener;
    new-instance v4, Lcom/samsung/multiscreen/Channel$11;

    invoke-direct {v4, p0, v1, p1}, Lcom/samsung/multiscreen/Channel$11;-><init>(Lcom/samsung/multiscreen/Channel;Lcom/samsung/multiscreen/Channel$OnMessageListener;Lcom/samsung/multiscreen/Message;)V

    const-wide/16 v6, 0x5

    invoke-static {v4, v6, v7}, Lcom/samsung/multiscreen/util/RunUtil;->runOnUiDelayed(Ljava/lang/Runnable;J)V

    goto :goto_0

    .line 1150
    .end local v0    # "listener":Lcom/samsung/multiscreen/Channel$OnMessageListener;
    .end local v1    # "messagelistener":Lcom/samsung/multiscreen/Channel$OnMessageListener;
    :cond_1
    return-void
.end method

.method private getWebSocketConnectionURL(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 11
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v9, 0x0

    .line 325
    iget-boolean v8, p0, Lcom/samsung/multiscreen/Channel;->securityMode:Z

    if-nez v8, :cond_1

    .line 326
    invoke-static {}, Lcom/koushikdutta/async/http/AsyncHttpClient;->getDefaultInstance()Lcom/koushikdutta/async/http/AsyncHttpClient;

    move-result-object v8

    invoke-virtual {v8}, Lcom/koushikdutta/async/http/AsyncHttpClient;->getSSLSocketMiddleware()Lcom/koushikdutta/async/http/spdy/SpdyMiddleware;

    move-result-object v8

    invoke-virtual {v8}, Lcom/koushikdutta/async/http/spdy/SpdyMiddleware;->getSSLContext()Ljavax/net/ssl/SSLContext;

    move-result-object v8

    if-eqz v8, :cond_0

    .line 327
    invoke-static {}, Lcom/koushikdutta/async/http/AsyncHttpClient;->getDefaultInstance()Lcom/koushikdutta/async/http/AsyncHttpClient;

    move-result-object v8

    invoke-virtual {v8}, Lcom/koushikdutta/async/http/AsyncHttpClient;->getSSLSocketMiddleware()Lcom/koushikdutta/async/http/spdy/SpdyMiddleware;

    move-result-object v8

    invoke-virtual {v8, v9}, Lcom/koushikdutta/async/http/spdy/SpdyMiddleware;->setSSLContext(Ljavax/net/ssl/SSLContext;)V

    .line 328
    invoke-static {}, Lcom/koushikdutta/async/http/AsyncHttpClient;->getDefaultInstance()Lcom/koushikdutta/async/http/AsyncHttpClient;

    move-result-object v8

    invoke-virtual {v8}, Lcom/koushikdutta/async/http/AsyncHttpClient;->getSSLSocketMiddleware()Lcom/koushikdutta/async/http/spdy/SpdyMiddleware;

    move-result-object v8

    invoke-virtual {v8, v9}, Lcom/koushikdutta/async/http/spdy/SpdyMiddleware;->setTrustManagers([Ljavax/net/ssl/TrustManager;)V

    .line 329
    invoke-static {}, Lcom/koushikdutta/async/http/AsyncHttpClient;->getDefaultInstance()Lcom/koushikdutta/async/http/AsyncHttpClient;

    move-result-object v8

    invoke-virtual {v8}, Lcom/koushikdutta/async/http/AsyncHttpClient;->getSSLSocketMiddleware()Lcom/koushikdutta/async/http/spdy/SpdyMiddleware;

    move-result-object v8

    invoke-virtual {v8, v9}, Lcom/koushikdutta/async/http/spdy/SpdyMiddleware;->setHostnameVerifier(Ljavax/net/ssl/HostnameVerifier;)V

    .line 385
    :cond_0
    :goto_0
    return-object p1

    .line 332
    :cond_1
    invoke-virtual {p0, p1}, Lcom/samsung/multiscreen/Channel;->getSecureURL(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object p1

    .line 333
    const/4 v5, 0x0

    .line 336
    .local v5, "testCA":Ljava/security/cert/Certificate;
    :try_start_0
    const-string v8, "X509"

    invoke-static {v8}, Ljava/security/cert/CertificateFactory;->getInstance(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;

    move-result-object v0

    .line 337
    .local v0, "certificateFactory":Ljava/security/cert/CertificateFactory;
    new-instance v8, Ljava/io/ByteArrayInputStream;

    const-string v9, "-----BEGIN CERTIFICATE-----\nMIIDhjCCAm6gAwIBAgIJAPm7naJvG91yMA0GCSqGSIb3DQEBCwUAMFcxCzAJBgNV\nBAYTAktSMRUwEwYDVQQKEwxTbWFydFZpZXdTREsxMTAvBgNVBAMTKFNtYXJ0Vmll\nd1NESyBSb290IENlcml0aWZpY2F0ZSBBdXRob3JpdHkwHhcNMTYwNzI5MDUzNDEw\nWhcNMzYwNzI5MDUzNDEwWjBXMQswCQYDVQQGEwJLUjEVMBMGA1UEChMMU21hcnRW\naWV3U0RLMTEwLwYDVQQDEyhTbWFydFZpZXdTREsgUm9vdCBDZXJpdGlmaWNhdGUg\nQXV0aG9yaXR5MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArtBcclIW\nEuObUkeTn+FW3m6Lm/YpwAOeABCtq6RKnBcq6jzEo3I433cSuVC2DrWGiiYi62Qm\niAzOHEtkvRctj+jEuK7ZKneKkxQ5261os0RsvWG7fONVb4m0ZRBydykgfu/PLwUB\nMWeiF3PB6w7YCzN1MJzb9EISFlhEcqMxDHgwGWHZYo/CTWtIwBVZ07mhdrCQaV2r\nLLJInA+4Wh9nXRO82qRnqYqFZfV7psIOW4MqfjWqNcKAHWWZ1gKrdZc9fPb2YVK4\nOIlaT3Qq9DOCveeU5T8d3MGEoiFnXt4Lp5656nI7MbkAsPEFFRHFkBK3o8CE1HLp\nsELQa6GBRe8WPQIDAQABo1UwUzASBgNVHRMBAf8ECDAGAQH/AgEAMB0GA1UdDgQW\nBBRQyhCp74M+t2GwCiH3g3Aau0AX7DALBgNVHQ8EBAMCAQYwEQYJYIZIAYb4QgEB\nBAQDAgAHMA0GCSqGSIb3DQEBCwUAA4IBAQAVIEeJo4vGsKPZBoY19hCXZnqB6Qcm\nOnWZzAZ0am8OQHQ4/LbSJ+Vnxh7eFiLtPQwuSHJ1a95ODA7RlNgnpC8ymHsL5Wl5\nUKOq5jOs3Jfa0aG99H9TsFKBysXlsBHfaHX+8/AoZUJDOksNeQigj3n4wCdLEPvt\nUpI9qJEjuzXeKxVhwnDkc/AvOuSGUaPiSeCSxy+xpcyWCANc4uVXtOxJluQvy8aC\nm6l0yG3Ucg09yCIkPzKtzG/kAadDRrTOYi/x4ZECtdamHQxncEnb3D881veLc6+s\nztEvDx0F77vRtadpeBxNZKivG2kJrymuf47pGIS0FlC5+/5ieV54+1/d\n-----END CERTIFICATE-----"

    invoke-virtual {v9}, Ljava/lang/String;->getBytes()[B

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-virtual {v0, v8}, Ljava/security/cert/CertificateFactory;->generateCertificate(Ljava/io/InputStream;)Ljava/security/cert/Certificate;
    :try_end_0
    .catch Ljava/security/cert/CertificateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 342
    .end local v0    # "certificateFactory":Ljava/security/cert/CertificateFactory;
    :goto_1
    invoke-static {}, Ljava/security/KeyStore;->getDefaultType()Ljava/lang/String;

    move-result-object v3

    .line 343
    .local v3, "keyStoreType":Ljava/lang/String;
    const/4 v2, 0x0

    .line 345
    .local v2, "keyStore":Ljava/security/KeyStore;
    :try_start_1
    invoke-static {v3}, Ljava/security/KeyStore;->getInstance(Ljava/lang/String;)Ljava/security/KeyStore;

    move-result-object v2

    .line 346
    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v2, v8, v9}, Ljava/security/KeyStore;->load(Ljava/io/InputStream;[C)V

    .line 347
    const-string v8, "ca"

    invoke-virtual {v2, v8, v5}, Ljava/security/KeyStore;->setCertificateEntry(Ljava/lang/String;Ljava/security/cert/Certificate;)V
    :try_end_1
    .catch Ljava/security/KeyStoreException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/security/cert/CertificateException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4

    .line 359
    :goto_2
    invoke-static {}, Ljavax/net/ssl/TrustManagerFactory;->getDefaultAlgorithm()Ljava/lang/String;

    move-result-object v7

    .line 360
    .local v7, "tmfAlgorithm":Ljava/lang/String;
    const/4 v6, 0x0

    .line 362
    .local v6, "tmf":Ljavax/net/ssl/TrustManagerFactory;
    :try_start_2
    invoke-static {v7}, Ljavax/net/ssl/TrustManagerFactory;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/TrustManagerFactory;

    move-result-object v6

    .line 363
    invoke-virtual {v6, v2}, Ljavax/net/ssl/TrustManagerFactory;->init(Ljava/security/KeyStore;)V
    :try_end_2
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/security/KeyStoreException; {:try_start_2 .. :try_end_2} :catch_6

    .line 371
    :goto_3
    if-eqz v6, :cond_0

    .line 372
    const/4 v4, 0x0

    .line 374
    .local v4, "sslContext":Ljavax/net/ssl/SSLContext;
    :try_start_3
    const-string v8, "TLS"

    invoke-static {v8}, Ljavax/net/ssl/SSLContext;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/SSLContext;

    move-result-object v4

    .line 375
    const/4 v8, 0x0

    invoke-virtual {v6}, Ljavax/net/ssl/TrustManagerFactory;->getTrustManagers()[Ljavax/net/ssl/TrustManager;

    move-result-object v9

    const/4 v10, 0x0

    invoke-virtual {v4, v8, v9, v10}, Ljavax/net/ssl/SSLContext;->init([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;)V
    :try_end_3
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_3 .. :try_end_3} :catch_7
    .catch Ljava/security/KeyManagementException; {:try_start_3 .. :try_end_3} :catch_8

    .line 381
    :goto_4
    invoke-static {}, Lcom/koushikdutta/async/http/AsyncHttpClient;->getDefaultInstance()Lcom/koushikdutta/async/http/AsyncHttpClient;

    move-result-object v8

    invoke-virtual {v8}, Lcom/koushikdutta/async/http/AsyncHttpClient;->getSSLSocketMiddleware()Lcom/koushikdutta/async/http/spdy/SpdyMiddleware;

    move-result-object v8

    invoke-virtual {v8, v4}, Lcom/koushikdutta/async/http/spdy/SpdyMiddleware;->setSSLContext(Ljavax/net/ssl/SSLContext;)V

    .line 382
    invoke-static {}, Lcom/koushikdutta/async/http/AsyncHttpClient;->getDefaultInstance()Lcom/koushikdutta/async/http/AsyncHttpClient;

    move-result-object v8

    invoke-virtual {v8}, Lcom/koushikdutta/async/http/AsyncHttpClient;->getSSLSocketMiddleware()Lcom/koushikdutta/async/http/spdy/SpdyMiddleware;

    move-result-object v8

    invoke-virtual {v6}, Ljavax/net/ssl/TrustManagerFactory;->getTrustManagers()[Ljavax/net/ssl/TrustManager;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/koushikdutta/async/http/spdy/SpdyMiddleware;->setTrustManagers([Ljavax/net/ssl/TrustManager;)V

    goto :goto_0

    .line 338
    .end local v2    # "keyStore":Ljava/security/KeyStore;
    .end local v3    # "keyStoreType":Ljava/lang/String;
    .end local v4    # "sslContext":Ljavax/net/ssl/SSLContext;
    .end local v6    # "tmf":Ljavax/net/ssl/TrustManagerFactory;
    .end local v7    # "tmfAlgorithm":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 339
    .local v1, "e":Ljava/security/cert/CertificateException;
    invoke-static {v1}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    goto :goto_1

    .line 348
    .end local v1    # "e":Ljava/security/cert/CertificateException;
    .restart local v2    # "keyStore":Ljava/security/KeyStore;
    .restart local v3    # "keyStoreType":Ljava/lang/String;
    :catch_1
    move-exception v1

    .line 349
    .local v1, "e":Ljava/security/KeyStoreException;
    invoke-static {v1}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    goto :goto_2

    .line 350
    .end local v1    # "e":Ljava/security/KeyStoreException;
    :catch_2
    move-exception v1

    .line 351
    .local v1, "e":Ljava/security/cert/CertificateException;
    invoke-static {v1}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    goto :goto_2

    .line 352
    .end local v1    # "e":Ljava/security/cert/CertificateException;
    :catch_3
    move-exception v1

    .line 353
    .local v1, "e":Ljava/security/NoSuchAlgorithmException;
    invoke-static {v1}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    goto :goto_2

    .line 354
    .end local v1    # "e":Ljava/security/NoSuchAlgorithmException;
    :catch_4
    move-exception v1

    .line 355
    .local v1, "e":Ljava/io/IOException;
    invoke-static {v1}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    goto :goto_2

    .line 364
    .end local v1    # "e":Ljava/io/IOException;
    .restart local v6    # "tmf":Ljavax/net/ssl/TrustManagerFactory;
    .restart local v7    # "tmfAlgorithm":Ljava/lang/String;
    :catch_5
    move-exception v1

    .line 365
    .local v1, "e":Ljava/security/NoSuchAlgorithmException;
    invoke-static {v1}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    goto :goto_3

    .line 366
    .end local v1    # "e":Ljava/security/NoSuchAlgorithmException;
    :catch_6
    move-exception v1

    .line 367
    .local v1, "e":Ljava/security/KeyStoreException;
    invoke-static {v1}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    goto :goto_3

    .line 376
    .end local v1    # "e":Ljava/security/KeyStoreException;
    .restart local v4    # "sslContext":Ljavax/net/ssl/SSLContext;
    :catch_7
    move-exception v1

    .line 377
    .local v1, "e":Ljava/security/NoSuchAlgorithmException;
    invoke-static {v1}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    goto :goto_4

    .line 378
    .end local v1    # "e":Ljava/security/NoSuchAlgorithmException;
    :catch_8
    move-exception v1

    .line 379
    .local v1, "e":Ljava/security/KeyManagementException;
    invoke-static {v1}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    goto :goto_4
.end method

.method private handleBinaryMessage(Lcom/koushikdutta/async/DataEmitter;Lcom/koushikdutta/async/ByteBufferList;)V
    .locals 9
    .param p1, "dataEmitter"    # Lcom/koushikdutta/async/DataEmitter;
    .param p2, "byteBufferList"    # Lcom/koushikdutta/async/ByteBufferList;

    .prologue
    .line 815
    invoke-virtual {p2}, Lcom/koushikdutta/async/ByteBufferList;->getShort()S

    move-result v2

    .line 817
    .local v2, "headerLength":I
    invoke-virtual {p2, v2}, Lcom/koushikdutta/async/ByteBufferList;->get(I)Lcom/koushikdutta/async/ByteBufferList;

    move-result-object v1

    .line 818
    .local v1, "headerBufferList":Lcom/koushikdutta/async/ByteBufferList;
    invoke-virtual {v1}, Lcom/koushikdutta/async/ByteBufferList;->readString()Ljava/lang/String;

    move-result-object v3

    .line 820
    .local v3, "json":Ljava/lang/String;
    invoke-virtual {p2}, Lcom/koushikdutta/async/ByteBufferList;->remaining()I

    move-result v6

    new-array v5, v6, [B

    .line 821
    .local v5, "payload":[B
    invoke-virtual {p2, v5}, Lcom/koushikdutta/async/ByteBufferList;->get([B)V

    .line 824
    :try_start_0
    invoke-static {v3}, Lcom/samsung/multiscreen/util/JSONUtil;->parse(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v4

    .line 826
    .local v4, "message":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-direct {p0, v4, v5}, Lcom/samsung/multiscreen/Channel;->handleMessage(Ljava/util/Map;[B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 830
    .end local v4    # "message":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    :goto_0
    return-void

    .line 827
    :catch_0
    move-exception v0

    .line 828
    .local v0, "e":Ljava/lang/Exception;
    const-string v6, "Channel"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "handleBinaryMessage error: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private handleClientConnectMessage(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 968
    .local p1, "message":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v2, "data"

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    .line 970
    .local v1, "data":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-static {p0, v1}, Lcom/samsung/multiscreen/Client;->create(Lcom/samsung/multiscreen/Channel;Ljava/util/Map;)Lcom/samsung/multiscreen/Client;

    move-result-object v0

    .line 972
    .local v0, "client":Lcom/samsung/multiscreen/Client;
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/multiscreen/Channel;->connected:Z

    .line 973
    iget-object v2, p0, Lcom/samsung/multiscreen/Channel;->clients:Lcom/samsung/multiscreen/Clients;

    invoke-virtual {v2, v0}, Lcom/samsung/multiscreen/Clients;->add(Lcom/samsung/multiscreen/Client;)V

    .line 975
    iget-object v2, p0, Lcom/samsung/multiscreen/Channel;->onClientConnectListener:Lcom/samsung/multiscreen/Channel$OnClientConnectListener;

    if-eqz v2, :cond_0

    .line 976
    new-instance v2, Lcom/samsung/multiscreen/Channel$9;

    invoke-direct {v2, p0, v0}, Lcom/samsung/multiscreen/Channel$9;-><init>(Lcom/samsung/multiscreen/Channel;Lcom/samsung/multiscreen/Client;)V

    invoke-static {v2}, Lcom/samsung/multiscreen/util/RunUtil;->runOnUI(Ljava/lang/Runnable;)V

    .line 986
    :cond_0
    return-void
.end method

.method private handleConnect(Ljava/lang/String;)V
    .locals 2
    .param p1, "callbackId"    # Ljava/lang/String;

    .prologue
    .line 939
    invoke-virtual {p0, p1}, Lcom/samsung/multiscreen/Channel;->getCallback(Ljava/lang/String;)Lcom/samsung/multiscreen/Result;

    move-result-object v0

    .line 940
    .local v0, "result":Lcom/samsung/multiscreen/Result;, "Lcom/samsung/multiscreen/Result<Lcom/samsung/multiscreen/Client;>;"
    new-instance v1, Lcom/samsung/multiscreen/Channel$7;

    invoke-direct {v1, p0, v0}, Lcom/samsung/multiscreen/Channel$7;-><init>(Lcom/samsung/multiscreen/Channel;Lcom/samsung/multiscreen/Result;)V

    invoke-static {v1}, Lcom/samsung/multiscreen/util/RunUtil;->runOnUI(Ljava/lang/Runnable;)V

    .line 950
    iget-object v1, p0, Lcom/samsung/multiscreen/Channel;->onConnectListener:Lcom/samsung/multiscreen/Channel$OnConnectListener;

    if-eqz v1, :cond_0

    .line 951
    new-instance v1, Lcom/samsung/multiscreen/Channel$8;

    invoke-direct {v1, p0}, Lcom/samsung/multiscreen/Channel$8;-><init>(Lcom/samsung/multiscreen/Channel;)V

    invoke-static {v1}, Lcom/samsung/multiscreen/util/RunUtil;->runOnUI(Ljava/lang/Runnable;)V

    .line 961
    :cond_0
    return-void
.end method

.method private handleConnectMessage(Ljava/util/Map;Ljava/lang/String;)V
    .locals 8
    .param p2, "callbackId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 912
    .local p1, "message":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v6, "data"

    invoke-interface {p1, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map;

    .line 915
    .local v2, "data":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v6, "id"

    invoke-interface {v2, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 918
    .local v5, "myClientId":Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 919
    .local v1, "clientList":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/multiscreen/Client;>;"
    const-string v6, "clients"

    invoke-interface {v2, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    .line 920
    .local v4, "mapClients":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;>;"
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map;

    .line 921
    .local v3, "mapClient":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-static {p0, v3}, Lcom/samsung/multiscreen/Client;->create(Lcom/samsung/multiscreen/Channel;Ljava/util/Map;)Lcom/samsung/multiscreen/Client;

    move-result-object v0

    .line 922
    .local v0, "client":Lcom/samsung/multiscreen/Client;
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 923
    iget-boolean v6, p0, Lcom/samsung/multiscreen/Channel;->connected:Z

    if-nez v6, :cond_0

    invoke-virtual {v0}, Lcom/samsung/multiscreen/Client;->isHost()Z

    move-result v6

    if-eqz v6, :cond_1

    :cond_0
    const/4 v6, 0x1

    :goto_1
    iput-boolean v6, p0, Lcom/samsung/multiscreen/Channel;->connected:Z

    goto :goto_0

    :cond_1
    const/4 v6, 0x0

    goto :goto_1

    .line 927
    .end local v0    # "client":Lcom/samsung/multiscreen/Client;
    .end local v3    # "mapClient":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    :cond_2
    iget-object v6, p0, Lcom/samsung/multiscreen/Channel;->clients:Lcom/samsung/multiscreen/Clients;

    invoke-virtual {v6}, Lcom/samsung/multiscreen/Clients;->reset()V

    .line 928
    iget-object v6, p0, Lcom/samsung/multiscreen/Channel;->clients:Lcom/samsung/multiscreen/Clients;

    invoke-virtual {v6, v1}, Lcom/samsung/multiscreen/Clients;->add(Ljava/util/List;)V

    .line 929
    iget-object v6, p0, Lcom/samsung/multiscreen/Channel;->clients:Lcom/samsung/multiscreen/Clients;

    invoke-virtual {v6, v5}, Lcom/samsung/multiscreen/Clients;->setMyClientId(Ljava/lang/String;)V

    .line 931
    invoke-direct {p0}, Lcom/samsung/multiscreen/Channel;->isWebSocketOpen()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 932
    iget-object v6, p0, Lcom/samsung/multiscreen/Channel;->connectionHandler:Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;

    invoke-virtual {v6}, Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;->startPing()V

    .line 935
    :cond_3
    invoke-direct {p0, p2}, Lcom/samsung/multiscreen/Channel;->handleConnect(Ljava/lang/String;)V

    .line 936
    return-void
.end method

.method private handleErrorMessage(Ljava/lang/String;Ljava/util/Map;)V
    .locals 3
    .param p1, "callbackId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 873
    .local p2, "message":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v2, "data"

    invoke-interface {p2, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 876
    .local v0, "data":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v2, "message"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 878
    .local v1, "errorMessage":Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/multiscreen/Error;->create(Ljava/lang/String;)Lcom/samsung/multiscreen/Error;

    move-result-object v2

    invoke-virtual {p0, p1, v2}, Lcom/samsung/multiscreen/Channel;->handleError(Ljava/lang/String;Lcom/samsung/multiscreen/Error;)V

    .line 880
    return-void
.end method

.method private handleMessage(Ljava/util/Map;[B)V
    .locals 1
    .param p2, "payload"    # [B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;[B)V"
        }
    .end annotation

    .prologue
    .line 833
    .local p1, "message":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1, p2}, Lcom/samsung/multiscreen/Channel;->handleMessage(Ljava/lang/String;Ljava/util/Map;[B)V

    .line 834
    return-void
.end method

.method private handleSocketClosedAndNotify()V
    .locals 2

    .prologue
    .line 593
    iget-object v1, p0, Lcom/samsung/multiscreen/Channel;->clients:Lcom/samsung/multiscreen/Clients;

    invoke-virtual {v1}, Lcom/samsung/multiscreen/Clients;->me()Lcom/samsung/multiscreen/Client;

    move-result-object v0

    .line 595
    .local v0, "client":Lcom/samsung/multiscreen/Client;
    invoke-virtual {p0}, Lcom/samsung/multiscreen/Channel;->handleSocketClosed()V

    .line 597
    iget-object v1, p0, Lcom/samsung/multiscreen/Channel;->onDisconnectListener:Lcom/samsung/multiscreen/Channel$OnDisconnectListener;

    if-eqz v1, :cond_0

    .line 598
    new-instance v1, Lcom/samsung/multiscreen/Channel$4;

    invoke-direct {v1, p0, v0}, Lcom/samsung/multiscreen/Channel$4;-><init>(Lcom/samsung/multiscreen/Channel;Lcom/samsung/multiscreen/Client;)V

    invoke-static {v1}, Lcom/samsung/multiscreen/util/RunUtil;->runOnUI(Ljava/lang/Runnable;)V

    .line 608
    :cond_0
    return-void
.end method

.method private isWebSocketOpen()Z
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lcom/samsung/multiscreen/Channel;->webSocket:Lcom/koushikdutta/async/http/WebSocket;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/multiscreen/Channel;->webSocket:Lcom/koushikdutta/async/http/WebSocket;

    invoke-interface {v0}, Lcom/koushikdutta/async/http/WebSocket;->isOpen()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private publishMessage(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;[B)V
    .locals 6
    .param p1, "event"    # Ljava/lang/String;
    .param p2, "data"    # Ljava/lang/Object;
    .param p3, "to"    # Ljava/lang/Object;
    .param p4, "payload"    # [B

    .prologue
    .line 727
    const-string v1, "ms.channel.emit"

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/samsung/multiscreen/Channel;->publishMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;[B)V

    .line 728
    return-void
.end method

.method private publishMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;[B)V
    .locals 9
    .param p1, "method"    # Ljava/lang/String;
    .param p2, "event"    # Ljava/lang/String;
    .param p3, "data"    # Ljava/lang/Object;
    .param p4, "to"    # Ljava/lang/Object;
    .param p5, "payload"    # [B

    .prologue
    .line 743
    invoke-virtual {p0}, Lcom/samsung/multiscreen/Channel;->isDebug()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 744
    const-string v5, "Channel"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "method: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ", event: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ", data: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ", to: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ", payload size: "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    if-eqz p5, :cond_2

    array-length v4, p5

    :goto_0
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 748
    :cond_0
    invoke-direct {p0}, Lcom/samsung/multiscreen/Channel;->isWebSocketOpen()Z

    move-result v4

    if-nez v4, :cond_3

    .line 749
    invoke-virtual {p0}, Lcom/samsung/multiscreen/Channel;->isDebug()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 750
    const-string v4, "Channel"

    const-string v5, "Not Connected"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 753
    :cond_1
    new-instance v0, Lcom/samsung/multiscreen/ErrorCode;

    const-string v4, "ERROR_WEBSOCKET_DISCONNECTED"

    invoke-direct {v0, v4}, Lcom/samsung/multiscreen/ErrorCode;-><init>(Ljava/lang/String;)V

    .line 754
    .local v0, "error":Lcom/samsung/multiscreen/ErrorCode;
    const/4 v4, 0x0

    invoke-virtual {v0}, Lcom/samsung/multiscreen/ErrorCode;->value()I

    move-result v5

    int-to-long v6, v5

    invoke-virtual {v0}, Lcom/samsung/multiscreen/ErrorCode;->name()Ljava/lang/String;

    move-result-object v5

    const-string v8, "Not Connected"

    invoke-static {v6, v7, v5, v8}, Lcom/samsung/multiscreen/Error;->create(JLjava/lang/String;Ljava/lang/String;)Lcom/samsung/multiscreen/Error;

    move-result-object v5

    invoke-virtual {p0, v4, v5}, Lcom/samsung/multiscreen/Channel;->handleError(Ljava/lang/String;Lcom/samsung/multiscreen/Error;)V

    .line 785
    .end local v0    # "error":Lcom/samsung/multiscreen/ErrorCode;
    :goto_1
    return-void

    .line 744
    :cond_2
    const/4 v4, 0x0

    goto :goto_0

    .line 759
    :cond_3
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 760
    .local v3, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 761
    const-string v4, "event"

    invoke-interface {v3, v4, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 763
    :cond_4
    if-eqz p3, :cond_5

    .line 764
    const-string v4, "data"

    invoke-interface {v3, v4, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 766
    :cond_5
    if-eqz p4, :cond_6

    .line 767
    const-string v4, "to"

    invoke-interface {v3, v4, p4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 771
    :cond_6
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 773
    .local v2, "message":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v4, "method"

    invoke-interface {v2, v4, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 774
    const-string v4, "params"

    invoke-interface {v2, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 777
    invoke-static {v2}, Lcom/samsung/multiscreen/util/JSONUtil;->toJSONString(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v1

    .line 779
    .local v1, "json":Ljava/lang/String;
    if-eqz p5, :cond_7

    .line 780
    iget-object v4, p0, Lcom/samsung/multiscreen/Channel;->webSocket:Lcom/koushikdutta/async/http/WebSocket;

    invoke-direct {p0, v1, p5}, Lcom/samsung/multiscreen/Channel;->createBinaryMessage(Ljava/lang/String;[B)[B

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/koushikdutta/async/http/WebSocket;->send([B)V

    goto :goto_1

    .line 782
    :cond_7
    iget-object v4, p0, Lcom/samsung/multiscreen/Channel;->webSocket:Lcom/koushikdutta/async/http/WebSocket;

    invoke-interface {v4, v1}, Lcom/koushikdutta/async/http/WebSocket;->send(Ljava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method public addOnMessageListener(Ljava/lang/String;Lcom/samsung/multiscreen/Channel$OnMessageListener;)V
    .locals 2
    .param p1, "event"    # Ljava/lang/String;
    .param p2, "onMessageListener"    # Lcom/samsung/multiscreen/Channel$OnMessageListener;

    .prologue
    .line 1042
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 1043
    :cond_0
    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    throw v1

    .line 1046
    :cond_1
    iget-object v1, p0, Lcom/samsung/multiscreen/Channel;->messageListeners:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1048
    .local v0, "onMessageListeners":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/multiscreen/Channel$OnMessageListener;>;"
    if-nez v0, :cond_2

    .line 1049
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    .end local v0    # "onMessageListeners":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/multiscreen/Channel$OnMessageListener;>;"
    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    .line 1050
    .restart local v0    # "onMessageListeners":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/multiscreen/Channel$OnMessageListener;>;"
    iget-object v1, p0, Lcom/samsung/multiscreen/Channel;->messageListeners:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1053
    :cond_2
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1054
    return-void
.end method

.method public connect()V
    .locals 1

    .prologue
    .line 265
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/multiscreen/Channel;->connect(Lcom/samsung/multiscreen/Result;)V

    .line 266
    return-void
.end method

.method public connect(Landroid/net/Uri;Ljava/util/Map;Lcom/samsung/multiscreen/Result;)V
    .locals 6
    .param p1, "url"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/samsung/multiscreen/Result",
            "<",
            "Lcom/samsung/multiscreen/Client;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 476
    .local p2, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .local p3, "result":Lcom/samsung/multiscreen/Result;, "Lcom/samsung/multiscreen/Result<Lcom/samsung/multiscreen/Client;>;"
    invoke-virtual {p0}, Lcom/samsung/multiscreen/Channel;->getUID()Ljava/lang/String;

    move-result-object v1

    .line 477
    .local v1, "id":Ljava/lang/String;
    invoke-virtual {p0, v1, p3}, Lcom/samsung/multiscreen/Channel;->registerCallback(Ljava/lang/String;Lcom/samsung/multiscreen/Result;)V

    .line 479
    invoke-direct {p0}, Lcom/samsung/multiscreen/Channel;->isWebSocketOpen()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 480
    new-instance v0, Lcom/samsung/multiscreen/ErrorCode;

    const-string v2, "ERROR_ALREADY_CONNECTED"

    invoke-direct {v0, v2}, Lcom/samsung/multiscreen/ErrorCode;-><init>(Ljava/lang/String;)V

    .line 481
    .local v0, "error":Lcom/samsung/multiscreen/ErrorCode;
    invoke-virtual {v0}, Lcom/samsung/multiscreen/ErrorCode;->value()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0}, Lcom/samsung/multiscreen/ErrorCode;->name()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Already Connected"

    invoke-static {v2, v3, v4, v5}, Lcom/samsung/multiscreen/Error;->create(JLjava/lang/String;Ljava/lang/String;)Lcom/samsung/multiscreen/Error;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/samsung/multiscreen/Channel;->handleError(Ljava/lang/String;Lcom/samsung/multiscreen/Error;)V

    .line 548
    .end local v0    # "error":Lcom/samsung/multiscreen/ErrorCode;
    :goto_0
    return-void

    .line 484
    :cond_0
    invoke-static {}, Lcom/koushikdutta/async/http/AsyncHttpClient;->getDefaultInstance()Lcom/koushikdutta/async/http/AsyncHttpClient;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    new-instance v5, Lcom/samsung/multiscreen/Channel$3;

    invoke-direct {v5, p0, v1, p3}, Lcom/samsung/multiscreen/Channel$3;-><init>(Lcom/samsung/multiscreen/Channel;Ljava/lang/String;Lcom/samsung/multiscreen/Result;)V

    invoke-virtual {v2, v3, v4, v5}, Lcom/koushikdutta/async/http/AsyncHttpClient;->websocket(Ljava/lang/String;Ljava/lang/String;Lcom/koushikdutta/async/http/AsyncHttpClient$WebSocketConnectCallback;)Lcom/koushikdutta/async/future/Future;

    goto :goto_0
.end method

.method public connect(Lcom/samsung/multiscreen/Result;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/multiscreen/Result",
            "<",
            "Lcom/samsung/multiscreen/Client;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 274
    .local p1, "result":Lcom/samsung/multiscreen/Result;, "Lcom/samsung/multiscreen/Result<Lcom/samsung/multiscreen/Client;>;"
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Lcom/samsung/multiscreen/Channel;->connect(Ljava/util/Map;Lcom/samsung/multiscreen/Result;)V

    .line 275
    return-void
.end method

.method public connect(Ljava/util/Map;Lcom/samsung/multiscreen/Result;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/samsung/multiscreen/Result",
            "<",
            "Lcom/samsung/multiscreen/Client;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 397
    .local p1, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .local p2, "result":Lcom/samsung/multiscreen/Result;, "Lcom/samsung/multiscreen/Result<Lcom/samsung/multiscreen/Client;>;"
    iget-object v1, p0, Lcom/samsung/multiscreen/Channel;->service:Lcom/samsung/multiscreen/Service;

    iget-object v1, v1, Lcom/samsung/multiscreen/Service;->isStandbyService:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 399
    invoke-static {}, Lcom/samsung/multiscreen/StandbyDeviceList;->getInstance()Lcom/samsung/multiscreen/StandbyDeviceList;

    move-result-object v1

    if-nez v1, :cond_1

    .line 473
    :cond_0
    :goto_0
    return-void

    .line 402
    :cond_1
    invoke-static {}, Lcom/samsung/multiscreen/StandbyDeviceList;->getInstance()Lcom/samsung/multiscreen/StandbyDeviceList;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/multiscreen/Channel;->service:Lcom/samsung/multiscreen/Service;

    invoke-virtual {v1, v2}, Lcom/samsung/multiscreen/StandbyDeviceList;->getMac(Lcom/samsung/multiscreen/Service;)Ljava/lang/String;

    move-result-object v0

    .line 403
    .local v0, "mac":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 407
    iget-object v1, p0, Lcom/samsung/multiscreen/Channel;->service:Lcom/samsung/multiscreen/Service;

    invoke-virtual {v1}, Lcom/samsung/multiscreen/Service;->getUri()Landroid/net/Uri;

    move-result-object v1

    new-instance v2, Lcom/samsung/multiscreen/Channel$2;

    invoke-direct {v2, p0, p1, p2}, Lcom/samsung/multiscreen/Channel$2;-><init>(Lcom/samsung/multiscreen/Channel;Ljava/util/Map;Lcom/samsung/multiscreen/Result;)V

    invoke-static {v0, v1, v2}, Lcom/samsung/multiscreen/Service;->WakeOnWirelessAndConnect(Ljava/lang/String;Landroid/net/Uri;Lcom/samsung/multiscreen/Result;)V

    goto :goto_0

    .line 467
    .end local v0    # "mac":Ljava/lang/String;
    :cond_2
    invoke-static {}, Lcom/samsung/multiscreen/StandbyDeviceList;->getInstance()Lcom/samsung/multiscreen/StandbyDeviceList;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 468
    invoke-static {}, Lcom/samsung/multiscreen/StandbyDeviceList;->getInstance()Lcom/samsung/multiscreen/StandbyDeviceList;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/multiscreen/Channel;->service:Lcom/samsung/multiscreen/Service;

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/samsung/multiscreen/StandbyDeviceList;->update(Lcom/samsung/multiscreen/Service;Ljava/lang/Boolean;)V

    .line 471
    :cond_3
    invoke-virtual {p0, p1}, Lcom/samsung/multiscreen/Channel;->getChannelUri(Ljava/util/Map;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/samsung/multiscreen/Channel;->getWebSocketConnectionURL(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p0, v1, p1, p2}, Lcom/samsung/multiscreen/Channel;->connect(Landroid/net/Uri;Ljava/util/Map;Lcom/samsung/multiscreen/Result;)V

    goto :goto_0
.end method

.method public disconnect()V
    .locals 1

    .prologue
    .line 555
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/multiscreen/Channel;->disconnect(Lcom/samsung/multiscreen/Result;)V

    .line 556
    return-void
.end method

.method public disconnect(Lcom/samsung/multiscreen/Result;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/multiscreen/Result",
            "<",
            "Lcom/samsung/multiscreen/Client;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 564
    .local p1, "result":Lcom/samsung/multiscreen/Result;, "Lcom/samsung/multiscreen/Result<Lcom/samsung/multiscreen/Client;>;"
    invoke-virtual {p0}, Lcom/samsung/multiscreen/Channel;->getUID()Ljava/lang/String;

    move-result-object v0

    .line 565
    .local v0, "id":Ljava/lang/String;
    invoke-virtual {p0, v0, p1}, Lcom/samsung/multiscreen/Channel;->registerCallback(Ljava/lang/String;Lcom/samsung/multiscreen/Result;)V

    .line 567
    const/4 v1, 0x0

    .line 568
    .local v1, "message":Ljava/lang/String;
    invoke-direct {p0}, Lcom/samsung/multiscreen/Channel;->isWebSocketOpen()Z

    move-result v2

    if-nez v2, :cond_0

    .line 569
    const-string v1, "Already Disconnected"

    .line 572
    :cond_0
    iget-boolean v2, p0, Lcom/samsung/multiscreen/Channel;->disconnecting:Z

    if-eqz v2, :cond_1

    .line 573
    const-string v1, "Already Disconnecting"

    .line 576
    :cond_1
    if-eqz v1, :cond_3

    .line 577
    invoke-static {v1}, Lcom/samsung/multiscreen/Error;->create(Ljava/lang/String;)Lcom/samsung/multiscreen/Error;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Lcom/samsung/multiscreen/Channel;->handleError(Ljava/lang/String;Lcom/samsung/multiscreen/Error;)V

    .line 589
    :cond_2
    :goto_0
    return-void

    .line 579
    :cond_3
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/multiscreen/Channel;->disconnecting:Z

    .line 581
    iget-object v2, p0, Lcom/samsung/multiscreen/Channel;->webSocket:Lcom/koushikdutta/async/http/WebSocket;

    invoke-interface {v2}, Lcom/koushikdutta/async/http/WebSocket;->close()V

    .line 582
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/samsung/multiscreen/Channel;->webSocket:Lcom/koushikdutta/async/http/WebSocket;

    .line 584
    invoke-virtual {p0, v0}, Lcom/samsung/multiscreen/Channel;->getCallback(Ljava/lang/String;)Lcom/samsung/multiscreen/Result;

    .line 585
    if-eqz p1, :cond_2

    .line 586
    iget-object v2, p0, Lcom/samsung/multiscreen/Channel;->clients:Lcom/samsung/multiscreen/Clients;

    invoke-virtual {v2}, Lcom/samsung/multiscreen/Clients;->me()Lcom/samsung/multiscreen/Client;

    move-result-object v2

    invoke-interface {p1, v2}, Lcom/samsung/multiscreen/Result;->onSuccess(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected getCallback(Ljava/lang/String;)Lcom/samsung/multiscreen/Result;
    .locals 1
    .param p1, "uid"    # Ljava/lang/String;

    .prologue
    .line 1159
    if-eqz p1, :cond_0

    .line 1160
    iget-object v0, p0, Lcom/samsung/multiscreen/Channel;->callbacks:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/multiscreen/Result;

    .line 1162
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getChannelUri(Ljava/util/Map;)Landroid/net/Uri;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/net/Uri;"
        }
    .end annotation

    .prologue
    .line 248
    .local p1, "queryParams":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/samsung/multiscreen/Channel;->service:Lcom/samsung/multiscreen/Service;

    invoke-virtual {v2}, Lcom/samsung/multiscreen/Service;->getUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "channels"

    .line 249
    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/multiscreen/Channel;->id:Ljava/lang/String;

    .line 250
    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 252
    .local v0, "builder":Landroid/net/Uri$Builder;
    if-eqz p1, :cond_0

    .line 253
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 254
    .local v1, "key":Ljava/lang/String;
    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_0

    .line 258
    .end local v1    # "key":Ljava/lang/String;
    :cond_0
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    return-object v2
.end method

.method public getClients()Lcom/samsung/multiscreen/Clients;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/samsung/multiscreen/Channel;->clients:Lcom/samsung/multiscreen/Clients;

    return-object v0
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/samsung/multiscreen/Channel;->id:Ljava/lang/String;

    return-object v0
.end method

.method protected getOnReadyListener()Lcom/samsung/multiscreen/Channel$OnReadyListener;
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/samsung/multiscreen/Channel;->onReadyListener:Lcom/samsung/multiscreen/Channel$OnReadyListener;

    return-object v0
.end method

.method getSecureURL(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 4
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 313
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "http:"

    const-string v3, "https:"

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "8001"

    const-string v3, "8002"

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 314
    .local v0, "httpsUri":Ljava/lang/String;
    invoke-static {}, Lcom/koushikdutta/async/http/AsyncHttpClient;->getDefaultInstance()Lcom/koushikdutta/async/http/AsyncHttpClient;

    move-result-object v1

    invoke-virtual {v1}, Lcom/koushikdutta/async/http/AsyncHttpClient;->getSSLSocketMiddleware()Lcom/koushikdutta/async/http/spdy/SpdyMiddleware;

    move-result-object v1

    sget-object v2, Lorg/apache/http/conn/ssl/SSLSocketFactory;->ALLOW_ALL_HOSTNAME_VERIFIER:Lorg/apache/http/conn/ssl/X509HostnameVerifier;

    invoke-virtual {v1, v2}, Lcom/koushikdutta/async/http/spdy/SpdyMiddleware;->setHostnameVerifier(Ljavax/net/ssl/HostnameVerifier;)V

    .line 315
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    return-object v1
.end method

.method protected getService()Lcom/samsung/multiscreen/Service;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/samsung/multiscreen/Channel;->service:Lcom/samsung/multiscreen/Service;

    return-object v0
.end method

.method protected getUID()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1166
    sget-object v0, Lcom/samsung/multiscreen/Channel;->random:Ljava/security/SecureRandom;

    const v1, 0x7fffffff

    invoke-virtual {v0, v1}, Ljava/security/SecureRandom;->nextInt(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/samsung/multiscreen/Channel;->uri:Landroid/net/Uri;

    return-object v0
.end method

.method protected getWebSocket()Lcom/koushikdutta/async/http/WebSocket;
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/samsung/multiscreen/Channel;->webSocket:Lcom/koushikdutta/async/http/WebSocket;

    return-object v0
.end method

.method protected handleClientDisconnectMessage(Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 990
    .local p1, "message":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v3, "data"

    invoke-interface {p1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map;

    .line 992
    .local v2, "data":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    if-eqz v2, :cond_0

    .line 993
    const-string v3, "id"

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 995
    .local v1, "clientId":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/multiscreen/Channel;->clients:Lcom/samsung/multiscreen/Clients;

    invoke-virtual {v3, v1}, Lcom/samsung/multiscreen/Clients;->get(Ljava/lang/String;)Lcom/samsung/multiscreen/Client;

    move-result-object v0

    .line 996
    .local v0, "client":Lcom/samsung/multiscreen/Client;
    if-nez v0, :cond_1

    .line 1017
    .end local v0    # "client":Lcom/samsung/multiscreen/Client;
    .end local v1    # "clientId":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 1000
    .restart local v0    # "client":Lcom/samsung/multiscreen/Client;
    .restart local v1    # "clientId":Ljava/lang/String;
    :cond_1
    invoke-virtual {v0}, Lcom/samsung/multiscreen/Client;->isHost()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1001
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/samsung/multiscreen/Channel;->connected:Z

    .line 1003
    :cond_2
    iget-object v3, p0, Lcom/samsung/multiscreen/Channel;->clients:Lcom/samsung/multiscreen/Clients;

    invoke-virtual {v3, v0}, Lcom/samsung/multiscreen/Clients;->remove(Lcom/samsung/multiscreen/Client;)V

    .line 1005
    iget-object v3, p0, Lcom/samsung/multiscreen/Channel;->onClientDisconnectListener:Lcom/samsung/multiscreen/Channel$OnClientDisconnectListener;

    if-eqz v3, :cond_0

    .line 1006
    new-instance v3, Lcom/samsung/multiscreen/Channel$10;

    invoke-direct {v3, p0, v0}, Lcom/samsung/multiscreen/Channel$10;-><init>(Lcom/samsung/multiscreen/Channel;Lcom/samsung/multiscreen/Client;)V

    invoke-static {v3}, Lcom/samsung/multiscreen/util/RunUtil;->runOnUI(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method protected handleClientMessage(Ljava/util/Map;[B)V
    .locals 7
    .param p2, "payload"    # [B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;[B)V"
        }
    .end annotation

    .prologue
    .line 1021
    .local p1, "message":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v1, "event"

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1024
    .local v2, "event":Ljava/lang/String;
    const-string v1, "data"

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 1026
    .local v3, "data":Ljava/lang/Object;
    const-string v1, "from"

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    .line 1027
    .local v6, "fromId":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/multiscreen/Channel;->clients:Lcom/samsung/multiscreen/Clients;

    invoke-virtual {v1, v6}, Lcom/samsung/multiscreen/Clients;->get(Ljava/lang/String;)Lcom/samsung/multiscreen/Client;

    move-result-object v4

    .line 1029
    .local v4, "from":Lcom/samsung/multiscreen/Client;
    new-instance v0, Lcom/samsung/multiscreen/Message;

    move-object v1, p0

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/samsung/multiscreen/Message;-><init>(Lcom/samsung/multiscreen/Channel;Ljava/lang/String;Ljava/lang/Object;Lcom/samsung/multiscreen/Client;[B)V

    .line 1031
    .local v0, "msg":Lcom/samsung/multiscreen/Message;
    invoke-direct {p0, v0}, Lcom/samsung/multiscreen/Channel;->emit(Lcom/samsung/multiscreen/Message;)V

    .line 1032
    return-void
.end method

.method protected handleError(Ljava/lang/String;Lcom/samsung/multiscreen/Error;)V
    .locals 2
    .param p1, "callbackId"    # Ljava/lang/String;
    .param p2, "error"    # Lcom/samsung/multiscreen/Error;

    .prologue
    .line 883
    invoke-virtual {p0, p1}, Lcom/samsung/multiscreen/Channel;->getCallback(Ljava/lang/String;)Lcom/samsung/multiscreen/Result;

    move-result-object v0

    .line 885
    .local v0, "result":Lcom/samsung/multiscreen/Result;
    new-instance v1, Lcom/samsung/multiscreen/Channel$5;

    invoke-direct {v1, p0, v0, p2}, Lcom/samsung/multiscreen/Channel$5;-><init>(Lcom/samsung/multiscreen/Channel;Lcom/samsung/multiscreen/Result;Lcom/samsung/multiscreen/Error;)V

    invoke-static {v1}, Lcom/samsung/multiscreen/util/RunUtil;->runOnUI(Ljava/lang/Runnable;)V

    .line 895
    iget-object v1, p0, Lcom/samsung/multiscreen/Channel;->onErrorListener:Lcom/samsung/multiscreen/Channel$OnErrorListener;

    if-eqz v1, :cond_0

    .line 896
    new-instance v1, Lcom/samsung/multiscreen/Channel$6;

    invoke-direct {v1, p0, p2}, Lcom/samsung/multiscreen/Channel$6;-><init>(Lcom/samsung/multiscreen/Channel;Lcom/samsung/multiscreen/Error;)V

    invoke-static {v1}, Lcom/samsung/multiscreen/util/RunUtil;->runOnUI(Ljava/lang/Runnable;)V

    .line 906
    :cond_0
    return-void
.end method

.method protected handleMessage(Ljava/lang/String;Ljava/util/Map;[B)V
    .locals 4
    .param p1, "callbackId"    # Ljava/lang/String;
    .param p3, "payload"    # [B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;[B)V"
        }
    .end annotation

    .prologue
    .line 839
    .local p2, "message":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v1, "event"

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 841
    .local v0, "event":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/multiscreen/Channel;->isDebug()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 842
    const-string v2, "Channel"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "event: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", message: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", payload size: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-eqz p3, :cond_1

    array-length v1, p3

    :goto_0
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 846
    :cond_0
    if-nez v0, :cond_2

    .line 868
    :goto_1
    return-void

    .line 842
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 848
    :cond_2
    const-string v1, "ms.error"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 849
    invoke-direct {p0, p1, p2}, Lcom/samsung/multiscreen/Channel;->handleErrorMessage(Ljava/lang/String;Ljava/util/Map;)V

    goto :goto_1

    .line 850
    :cond_3
    const-string v1, "ms.channel.clientConnect"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 851
    invoke-direct {p0, p2}, Lcom/samsung/multiscreen/Channel;->handleClientConnectMessage(Ljava/util/Map;)V

    goto :goto_1

    .line 852
    :cond_4
    const-string v1, "ms.channel.clientDisconnect"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 853
    invoke-virtual {p0, p2}, Lcom/samsung/multiscreen/Channel;->handleClientDisconnectMessage(Ljava/util/Map;)V

    goto :goto_1

    .line 854
    :cond_5
    const-string v1, "ms.channel.ready"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 858
    invoke-virtual {p0, p2}, Lcom/samsung/multiscreen/Channel;->handleReadyMessage(Ljava/util/Map;)V

    goto :goto_1

    .line 859
    :cond_6
    const-string v1, "ms.channel.disconnect"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 861
    invoke-virtual {p0}, Lcom/samsung/multiscreen/Channel;->disconnect()V

    goto :goto_1

    .line 866
    :cond_7
    invoke-virtual {p0, p2, p3}, Lcom/samsung/multiscreen/Channel;->handleClientMessage(Ljava/util/Map;[B)V

    goto :goto_1
.end method

.method protected handleReadyMessage(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 964
    .local p1, "message":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    return-void
.end method

.method protected handleSocketClosed()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 611
    iget-object v0, p0, Lcom/samsung/multiscreen/Channel;->connectionHandler:Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;

    invoke-virtual {v0}, Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;->stopPing()V

    .line 613
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/multiscreen/Channel;->webSocket:Lcom/koushikdutta/async/http/WebSocket;

    .line 614
    iput-boolean v1, p0, Lcom/samsung/multiscreen/Channel;->connected:Z

    .line 617
    iget-object v0, p0, Lcom/samsung/multiscreen/Channel;->clients:Lcom/samsung/multiscreen/Clients;

    invoke-virtual {v0}, Lcom/samsung/multiscreen/Clients;->reset()V

    .line 619
    iget-boolean v0, p0, Lcom/samsung/multiscreen/Channel;->disconnecting:Z

    if-eqz v0, :cond_0

    .line 620
    iput-boolean v1, p0, Lcom/samsung/multiscreen/Channel;->disconnecting:Z

    .line 622
    :cond_0
    return-void
.end method

.method public isConnected()Z
    .locals 1

    .prologue
    .line 240
    invoke-direct {p0}, Lcom/samsung/multiscreen/Channel;->isWebSocketOpen()Z

    move-result v0

    return v0
.end method

.method public isDebug()Z
    .locals 1

    .prologue
    .line 201
    iget-boolean v0, p0, Lcom/samsung/multiscreen/Channel;->debug:Z

    return v0
.end method

.method public isSecurityMode()Z
    .locals 1

    .prologue
    .line 125
    iget-boolean v0, p0, Lcom/samsung/multiscreen/Channel;->securityMode:Z

    return v0
.end method

.method public publish(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2
    .param p1, "event"    # Ljava/lang/String;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 631
    const-string v0, "host"

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/samsung/multiscreen/Channel;->publishMessage(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;[B)V

    .line 632
    return-void
.end method

.method public publish(Ljava/lang/String;Ljava/lang/Object;Lcom/samsung/multiscreen/Client;)V
    .locals 2
    .param p1, "event"    # Ljava/lang/String;
    .param p2, "data"    # Ljava/lang/Object;
    .param p3, "client"    # Lcom/samsung/multiscreen/Client;

    .prologue
    .line 676
    invoke-virtual {p3}, Lcom/samsung/multiscreen/Client;->getId()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/samsung/multiscreen/Channel;->publishMessage(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;[B)V

    .line 677
    return-void
.end method

.method public publish(Ljava/lang/String;Ljava/lang/Object;Lcom/samsung/multiscreen/Client;[B)V
    .locals 1
    .param p1, "event"    # Ljava/lang/String;
    .param p2, "data"    # Ljava/lang/Object;
    .param p3, "client"    # Lcom/samsung/multiscreen/Client;
    .param p4, "payload"    # [B

    .prologue
    .line 688
    invoke-virtual {p3}, Lcom/samsung/multiscreen/Client;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0, p4}, Lcom/samsung/multiscreen/Channel;->publishMessage(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;[B)V

    .line 689
    return-void
.end method

.method public publish(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;)V
    .locals 1
    .param p1, "event"    # Ljava/lang/String;
    .param p2, "data"    # Ljava/lang/Object;
    .param p3, "target"    # Ljava/lang/String;

    .prologue
    .line 653
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/samsung/multiscreen/Channel;->publishMessage(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;[B)V

    .line 654
    return-void
.end method

.method public publish(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;[B)V
    .locals 0
    .param p1, "event"    # Ljava/lang/String;
    .param p2, "data"    # Ljava/lang/Object;
    .param p3, "target"    # Ljava/lang/String;
    .param p4, "payload"    # [B

    .prologue
    .line 665
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/multiscreen/Channel;->publishMessage(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;[B)V

    .line 666
    return-void
.end method

.method public publish(Ljava/lang/String;Ljava/lang/Object;Ljava/util/List;)V
    .locals 1
    .param p1, "event"    # Ljava/lang/String;
    .param p2, "data"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/multiscreen/Client;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 699
    .local p3, "clients":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/multiscreen/Client;>;"
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/samsung/multiscreen/Channel;->publish(Ljava/lang/String;Ljava/lang/Object;Ljava/util/List;[B)V

    .line 700
    return-void
.end method

.method public publish(Ljava/lang/String;Ljava/lang/Object;Ljava/util/List;[B)V
    .locals 4
    .param p1, "event"    # Ljava/lang/String;
    .param p2, "data"    # Ljava/lang/Object;
    .param p4, "payload"    # [B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/multiscreen/Client;",
            ">;[B)V"
        }
    .end annotation

    .prologue
    .line 711
    .local p3, "clients":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/multiscreen/Client;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 712
    .local v1, "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/multiscreen/Client;

    .line 713
    .local v0, "client":Lcom/samsung/multiscreen/Client;
    invoke-virtual {v0}, Lcom/samsung/multiscreen/Client;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 715
    .end local v0    # "client":Lcom/samsung/multiscreen/Client;
    :cond_0
    invoke-direct {p0, p1, p2, v1, p4}, Lcom/samsung/multiscreen/Channel;->publishMessage(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;[B)V

    .line 716
    return-void
.end method

.method public publish(Ljava/lang/String;Ljava/lang/Object;[B)V
    .locals 1
    .param p1, "event"    # Ljava/lang/String;
    .param p2, "data"    # Ljava/lang/Object;
    .param p3, "payload"    # [B

    .prologue
    .line 642
    const-string v0, "host"

    invoke-direct {p0, p1, p2, v0, p3}, Lcom/samsung/multiscreen/Channel;->publishMessage(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;[B)V

    .line 643
    return-void
.end method

.method protected registerCallback(Ljava/lang/String;Lcom/samsung/multiscreen/Result;)V
    .locals 1
    .param p1, "uid"    # Ljava/lang/String;
    .param p2, "callback"    # Lcom/samsung/multiscreen/Result;

    .prologue
    .line 1153
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 1154
    iget-object v0, p0, Lcom/samsung/multiscreen/Channel;->callbacks:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1156
    :cond_0
    return-void
.end method

.method public removeAllListeners()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1100
    invoke-virtual {p0, v0}, Lcom/samsung/multiscreen/Channel;->setOnConnectListener(Lcom/samsung/multiscreen/Channel$OnConnectListener;)V

    .line 1101
    invoke-virtual {p0, v0}, Lcom/samsung/multiscreen/Channel;->setOnDisconnectListener(Lcom/samsung/multiscreen/Channel$OnDisconnectListener;)V

    .line 1102
    invoke-virtual {p0, v0}, Lcom/samsung/multiscreen/Channel;->setOnClientConnectListener(Lcom/samsung/multiscreen/Channel$OnClientConnectListener;)V

    .line 1103
    invoke-virtual {p0, v0}, Lcom/samsung/multiscreen/Channel;->setOnClientDisconnectListener(Lcom/samsung/multiscreen/Channel$OnClientDisconnectListener;)V

    .line 1104
    invoke-virtual {p0, v0}, Lcom/samsung/multiscreen/Channel;->setOnReadyListener(Lcom/samsung/multiscreen/Channel$OnReadyListener;)V

    .line 1105
    invoke-virtual {p0, v0}, Lcom/samsung/multiscreen/Channel;->setOnErrorListener(Lcom/samsung/multiscreen/Channel$OnErrorListener;)V

    .line 1106
    invoke-virtual {p0}, Lcom/samsung/multiscreen/Channel;->removeOnMessageListeners()V

    .line 1107
    return-void
.end method

.method public removeOnMessageListener(Ljava/lang/String;Lcom/samsung/multiscreen/Channel$OnMessageListener;)V
    .locals 2
    .param p1, "event"    # Ljava/lang/String;
    .param p2, "onMessageListener"    # Lcom/samsung/multiscreen/Channel$OnMessageListener;

    .prologue
    .line 1077
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 1078
    :cond_0
    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    throw v1

    .line 1081
    :cond_1
    iget-object v1, p0, Lcom/samsung/multiscreen/Channel;->messageListeners:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1083
    .local v0, "onMessageListeners":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/multiscreen/Channel$OnMessageListener;>;"
    if-eqz v0, :cond_2

    .line 1084
    invoke-interface {v0, p2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1086
    :cond_2
    return-void
.end method

.method public removeOnMessageListeners()V
    .locals 1

    .prologue
    .line 1093
    iget-object v0, p0, Lcom/samsung/multiscreen/Channel;->messageListeners:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1094
    return-void
.end method

.method public removeOnMessageListeners(Ljava/lang/String;)V
    .locals 2
    .param p1, "event"    # Ljava/lang/String;

    .prologue
    .line 1062
    if-nez p1, :cond_0

    .line 1063
    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    throw v1

    .line 1066
    :cond_0
    iget-object v1, p0, Lcom/samsung/multiscreen/Channel;->messageListeners:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1067
    .local v0, "onMessageListeners":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/multiscreen/Channel$OnMessageListener;>;"
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1068
    return-void
.end method

.method public setConnectionTimeout(I)V
    .locals 2
    .param p1, "timeout"    # I

    .prologue
    .line 1118
    if-gez p1, :cond_0

    .line 1119
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "timeout < 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1120
    :cond_0
    if-nez p1, :cond_2

    .line 1121
    iget-object v0, p0, Lcom/samsung/multiscreen/Channel;->connectionHandler:Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;

    invoke-virtual {v0}, Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;->stopPing()V

    .line 1129
    :cond_1
    :goto_0
    return-void

    .line 1123
    :cond_2
    iget-object v0, p0, Lcom/samsung/multiscreen/Channel;->connectionHandler:Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;

    invoke-virtual {v0, p1}, Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;->setPingTimeout(I)V

    .line 1125
    invoke-direct {p0}, Lcom/samsung/multiscreen/Channel;->isWebSocketOpen()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1126
    iget-object v0, p0, Lcom/samsung/multiscreen/Channel;->connectionHandler:Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;

    invoke-virtual {v0}, Lcom/samsung/multiscreen/Channel$ChannelConnectionHandler;->startPing()V

    goto :goto_0
.end method

.method public setDebug(Z)V
    .locals 0
    .param p1, "debug"    # Z

    .prologue
    .line 197
    iput-boolean p1, p0, Lcom/samsung/multiscreen/Channel;->debug:Z

    return-void
.end method

.method public setOnClientConnectListener(Lcom/samsung/multiscreen/Channel$OnClientConnectListener;)V
    .locals 0
    .param p1, "onClientConnectListener"    # Lcom/samsung/multiscreen/Channel$OnClientConnectListener;

    .prologue
    .line 156
    iput-object p1, p0, Lcom/samsung/multiscreen/Channel;->onClientConnectListener:Lcom/samsung/multiscreen/Channel$OnClientConnectListener;

    return-void
.end method

.method public setOnClientDisconnectListener(Lcom/samsung/multiscreen/Channel$OnClientDisconnectListener;)V
    .locals 0
    .param p1, "onClientDisconnectListener"    # Lcom/samsung/multiscreen/Channel$OnClientDisconnectListener;

    .prologue
    .line 163
    iput-object p1, p0, Lcom/samsung/multiscreen/Channel;->onClientDisconnectListener:Lcom/samsung/multiscreen/Channel$OnClientDisconnectListener;

    return-void
.end method

.method public setOnConnectListener(Lcom/samsung/multiscreen/Channel$OnConnectListener;)V
    .locals 0
    .param p1, "onConnectListener"    # Lcom/samsung/multiscreen/Channel$OnConnectListener;

    .prologue
    .line 142
    iput-object p1, p0, Lcom/samsung/multiscreen/Channel;->onConnectListener:Lcom/samsung/multiscreen/Channel$OnConnectListener;

    return-void
.end method

.method public setOnDisconnectListener(Lcom/samsung/multiscreen/Channel$OnDisconnectListener;)V
    .locals 0
    .param p1, "onDisconnectListener"    # Lcom/samsung/multiscreen/Channel$OnDisconnectListener;

    .prologue
    .line 149
    iput-object p1, p0, Lcom/samsung/multiscreen/Channel;->onDisconnectListener:Lcom/samsung/multiscreen/Channel$OnDisconnectListener;

    return-void
.end method

.method public setOnErrorListener(Lcom/samsung/multiscreen/Channel$OnErrorListener;)V
    .locals 0
    .param p1, "onErrorListener"    # Lcom/samsung/multiscreen/Channel$OnErrorListener;

    .prologue
    .line 179
    iput-object p1, p0, Lcom/samsung/multiscreen/Channel;->onErrorListener:Lcom/samsung/multiscreen/Channel$OnErrorListener;

    return-void
.end method

.method public setOnReadyListener(Lcom/samsung/multiscreen/Channel$OnReadyListener;)V
    .locals 0
    .param p1, "onReadyListener"    # Lcom/samsung/multiscreen/Channel$OnReadyListener;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 171
    iput-object p1, p0, Lcom/samsung/multiscreen/Channel;->onReadyListener:Lcom/samsung/multiscreen/Channel$OnReadyListener;

    return-void
.end method

.method public setSecurityMode(ZLcom/samsung/multiscreen/Result;)V
    .locals 2
    .param p1, "securityMode"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/samsung/multiscreen/Result",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 284
    .local p2, "result":Lcom/samsung/multiscreen/Result;, "Lcom/samsung/multiscreen/Result<Ljava/lang/Boolean;>;"
    if-eqz p1, :cond_0

    .line 285
    iget-object v0, p0, Lcom/samsung/multiscreen/Channel;->service:Lcom/samsung/multiscreen/Service;

    new-instance v1, Lcom/samsung/multiscreen/Channel$1;

    invoke-direct {v1, p0, p2}, Lcom/samsung/multiscreen/Channel$1;-><init>(Lcom/samsung/multiscreen/Channel;Lcom/samsung/multiscreen/Result;)V

    invoke-virtual {v0, v1}, Lcom/samsung/multiscreen/Service;->isSecurityModeSupported(Lcom/samsung/multiscreen/Result;)V

    .line 304
    :goto_0
    return-void

    .line 301
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/multiscreen/Channel;->securityMode:Z

    .line 302
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {p2, v0}, Lcom/samsung/multiscreen/Result;->onSuccess(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 79
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Channel(service="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/multiscreen/Channel;->service:Lcom/samsung/multiscreen/Service;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", uri="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/multiscreen/Channel;->uri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/multiscreen/Channel;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", clients="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/multiscreen/Channel;->clients:Lcom/samsung/multiscreen/Clients;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", connected="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/samsung/multiscreen/Channel;->connected:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", securityMode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/samsung/multiscreen/Channel;->securityMode:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", onConnectListener="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/multiscreen/Channel;->onConnectListener:Lcom/samsung/multiscreen/Channel$OnConnectListener;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", onDisconnectListener="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/multiscreen/Channel;->onDisconnectListener:Lcom/samsung/multiscreen/Channel$OnDisconnectListener;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", onClientConnectListener="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/multiscreen/Channel;->onClientConnectListener:Lcom/samsung/multiscreen/Channel$OnClientConnectListener;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", onClientDisconnectListener="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/multiscreen/Channel;->onClientDisconnectListener:Lcom/samsung/multiscreen/Channel$OnClientDisconnectListener;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", onReadyListener="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/multiscreen/Channel;->onReadyListener:Lcom/samsung/multiscreen/Channel$OnReadyListener;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", onErrorListener="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/multiscreen/Channel;->onErrorListener:Lcom/samsung/multiscreen/Channel$OnErrorListener;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
