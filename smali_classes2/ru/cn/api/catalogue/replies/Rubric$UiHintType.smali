.class public final enum Lru/cn/api/catalogue/replies/Rubric$UiHintType;
.super Ljava/lang/Enum;
.source "Rubric.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/api/catalogue/replies/Rubric;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "UiHintType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lru/cn/api/catalogue/replies/Rubric$UiHintType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lru/cn/api/catalogue/replies/Rubric$UiHintType;

.field public static final enum FEATURED:Lru/cn/api/catalogue/replies/Rubric$UiHintType;

.field public static final enum RECOMMENDED:Lru/cn/api/catalogue/replies/Rubric$UiHintType;

.field public static final enum SEARCH:Lru/cn/api/catalogue/replies/Rubric$UiHintType;

.field public static final enum STORIES:Lru/cn/api/catalogue/replies/Rubric$UiHintType;

.field public static final enum TOP:Lru/cn/api/catalogue/replies/Rubric$UiHintType;


# instance fields
.field private value:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 22
    new-instance v0, Lru/cn/api/catalogue/replies/Rubric$UiHintType;

    const-string v1, "SEARCH"

    invoke-direct {v0, v1, v7, v3}, Lru/cn/api/catalogue/replies/Rubric$UiHintType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/cn/api/catalogue/replies/Rubric$UiHintType;->SEARCH:Lru/cn/api/catalogue/replies/Rubric$UiHintType;

    .line 23
    new-instance v0, Lru/cn/api/catalogue/replies/Rubric$UiHintType;

    const-string v1, "FEATURED"

    invoke-direct {v0, v1, v3, v4}, Lru/cn/api/catalogue/replies/Rubric$UiHintType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/cn/api/catalogue/replies/Rubric$UiHintType;->FEATURED:Lru/cn/api/catalogue/replies/Rubric$UiHintType;

    .line 24
    new-instance v0, Lru/cn/api/catalogue/replies/Rubric$UiHintType;

    const-string v1, "RECOMMENDED"

    invoke-direct {v0, v1, v4, v5}, Lru/cn/api/catalogue/replies/Rubric$UiHintType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/cn/api/catalogue/replies/Rubric$UiHintType;->RECOMMENDED:Lru/cn/api/catalogue/replies/Rubric$UiHintType;

    .line 25
    new-instance v0, Lru/cn/api/catalogue/replies/Rubric$UiHintType;

    const-string v1, "TOP"

    invoke-direct {v0, v1, v5, v6}, Lru/cn/api/catalogue/replies/Rubric$UiHintType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/cn/api/catalogue/replies/Rubric$UiHintType;->TOP:Lru/cn/api/catalogue/replies/Rubric$UiHintType;

    .line 26
    new-instance v0, Lru/cn/api/catalogue/replies/Rubric$UiHintType;

    const-string v1, "STORIES"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v6, v2}, Lru/cn/api/catalogue/replies/Rubric$UiHintType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/cn/api/catalogue/replies/Rubric$UiHintType;->STORIES:Lru/cn/api/catalogue/replies/Rubric$UiHintType;

    .line 21
    const/4 v0, 0x5

    new-array v0, v0, [Lru/cn/api/catalogue/replies/Rubric$UiHintType;

    sget-object v1, Lru/cn/api/catalogue/replies/Rubric$UiHintType;->SEARCH:Lru/cn/api/catalogue/replies/Rubric$UiHintType;

    aput-object v1, v0, v7

    sget-object v1, Lru/cn/api/catalogue/replies/Rubric$UiHintType;->FEATURED:Lru/cn/api/catalogue/replies/Rubric$UiHintType;

    aput-object v1, v0, v3

    sget-object v1, Lru/cn/api/catalogue/replies/Rubric$UiHintType;->RECOMMENDED:Lru/cn/api/catalogue/replies/Rubric$UiHintType;

    aput-object v1, v0, v4

    sget-object v1, Lru/cn/api/catalogue/replies/Rubric$UiHintType;->TOP:Lru/cn/api/catalogue/replies/Rubric$UiHintType;

    aput-object v1, v0, v5

    sget-object v1, Lru/cn/api/catalogue/replies/Rubric$UiHintType;->STORIES:Lru/cn/api/catalogue/replies/Rubric$UiHintType;

    aput-object v1, v0, v6

    sput-object v0, Lru/cn/api/catalogue/replies/Rubric$UiHintType;->$VALUES:[Lru/cn/api/catalogue/replies/Rubric$UiHintType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 31
    iput p3, p0, Lru/cn/api/catalogue/replies/Rubric$UiHintType;->value:I

    .line 32
    return-void
.end method

.method public static valueOf(I)Lru/cn/api/catalogue/replies/Rubric$UiHintType;
    .locals 5
    .param p0, "v"    # I

    .prologue
    .line 39
    if-lez p0, :cond_1

    .line 40
    invoke-static {}, Lru/cn/api/catalogue/replies/Rubric$UiHintType;->values()[Lru/cn/api/catalogue/replies/Rubric$UiHintType;

    move-result-object v0

    .line 41
    .local v0, "types":[Lru/cn/api/catalogue/replies/Rubric$UiHintType;
    array-length v3, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    .line 42
    .local v1, "uiHintType":Lru/cn/api/catalogue/replies/Rubric$UiHintType;
    invoke-virtual {v1}, Lru/cn/api/catalogue/replies/Rubric$UiHintType;->getValue()I

    move-result v4

    if-ne v4, p0, :cond_0

    .line 46
    .end local v0    # "types":[Lru/cn/api/catalogue/replies/Rubric$UiHintType;
    .end local v1    # "uiHintType":Lru/cn/api/catalogue/replies/Rubric$UiHintType;
    :goto_1
    return-object v1

    .line 41
    .restart local v0    # "types":[Lru/cn/api/catalogue/replies/Rubric$UiHintType;
    .restart local v1    # "uiHintType":Lru/cn/api/catalogue/replies/Rubric$UiHintType;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 46
    .end local v0    # "types":[Lru/cn/api/catalogue/replies/Rubric$UiHintType;
    .end local v1    # "uiHintType":Lru/cn/api/catalogue/replies/Rubric$UiHintType;
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lru/cn/api/catalogue/replies/Rubric$UiHintType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 21
    const-class v0, Lru/cn/api/catalogue/replies/Rubric$UiHintType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lru/cn/api/catalogue/replies/Rubric$UiHintType;

    return-object v0
.end method

.method public static values()[Lru/cn/api/catalogue/replies/Rubric$UiHintType;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lru/cn/api/catalogue/replies/Rubric$UiHintType;->$VALUES:[Lru/cn/api/catalogue/replies/Rubric$UiHintType;

    invoke-virtual {v0}, [Lru/cn/api/catalogue/replies/Rubric$UiHintType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lru/cn/api/catalogue/replies/Rubric$UiHintType;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lru/cn/api/catalogue/replies/Rubric$UiHintType;->value:I

    return v0
.end method
