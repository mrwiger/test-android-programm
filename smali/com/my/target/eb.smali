.class public final Lcom/my/target/eb;
.super Lcom/my/target/ee;
.source "DefaultView.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation


# static fields
.field private static final bf:I

.field private static final bg:I

.field private static final bh:I

.field private static final bi:I

.field private static final bj:I


# instance fields
.field private final F:Lcom/my/target/by;

.field private final aw:Lcom/my/target/cm;

.field private final bk:Lcom/my/target/bv;

.field private final bl:Lcom/my/target/eh;

.field private final bm:Lcom/my/target/eg;

.field private final bn:Lcom/my/target/ef;

.field private final bo:Lcom/my/target/dz;

.field private final bp:Lcom/my/target/by;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/eb;->bf:I

    .line 39
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/eb;->bg:I

    .line 40
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/eb;->bh:I

    .line 41
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/eb;->bi:I

    .line 42
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/eb;->bj:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 7

    .prologue
    const/4 v4, 0x3

    const/4 v6, -0x2

    const/4 v1, 0x0

    const/4 v5, -0x1

    .line 54
    invoke-direct {p0, p1, v1}, Lcom/my/target/ee;-><init>(Landroid/content/Context;I)V

    .line 55
    invoke-virtual {p0}, Lcom/my/target/eb;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v0, v0, 0xf

    if-lt v0, v4, :cond_0

    const/4 v0, 0x1

    .line 59
    :goto_0
    invoke-static {p1}, Lcom/my/target/cm;->x(Landroid/content/Context;)Lcom/my/target/cm;

    move-result-object v2

    iput-object v2, p0, Lcom/my/target/eb;->aw:Lcom/my/target/cm;

    .line 61
    new-instance v2, Lcom/my/target/bv;

    invoke-direct {v2, p1}, Lcom/my/target/bv;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/my/target/eb;->bk:Lcom/my/target/bv;

    .line 62
    iget-object v2, p0, Lcom/my/target/eb;->bk:Lcom/my/target/bv;

    sget v3, Lcom/my/target/eb;->bi:I

    invoke-virtual {v2, v3}, Lcom/my/target/bv;->setId(I)V

    .line 64
    new-instance v2, Lcom/my/target/eh;

    iget-object v3, p0, Lcom/my/target/eb;->aw:Lcom/my/target/cm;

    invoke-direct {v2, p1, v3, v0}, Lcom/my/target/eh;-><init>(Landroid/content/Context;Lcom/my/target/cm;Z)V

    iput-object v2, p0, Lcom/my/target/eb;->bl:Lcom/my/target/eh;

    .line 65
    iget-object v2, p0, Lcom/my/target/eb;->bl:Lcom/my/target/eh;

    sget v3, Lcom/my/target/eb;->bg:I

    invoke-virtual {v2, v3}, Lcom/my/target/eh;->setId(I)V

    .line 66
    new-instance v2, Lcom/my/target/eg;

    iget-object v3, p0, Lcom/my/target/eb;->aw:Lcom/my/target/cm;

    invoke-direct {v2, p1, v3, v0}, Lcom/my/target/eg;-><init>(Landroid/content/Context;Lcom/my/target/cm;Z)V

    iput-object v2, p0, Lcom/my/target/eb;->bm:Lcom/my/target/eg;

    .line 67
    iget-object v0, p0, Lcom/my/target/eb;->bm:Lcom/my/target/eg;

    sget v2, Lcom/my/target/eb;->bf:I

    invoke-virtual {v0, v2}, Lcom/my/target/eg;->setId(I)V

    .line 69
    new-instance v0, Lcom/my/target/by;

    invoke-direct {v0, p1}, Lcom/my/target/by;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/eb;->F:Lcom/my/target/by;

    .line 70
    iget-object v0, p0, Lcom/my/target/eb;->F:Lcom/my/target/by;

    sget v2, Lcom/my/target/eb;->bj:I

    invoke-virtual {v0, v2}, Lcom/my/target/by;->setId(I)V

    .line 71
    new-instance v0, Lcom/my/target/dz;

    invoke-direct {v0, p1}, Lcom/my/target/dz;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/eb;->bo:Lcom/my/target/dz;

    .line 73
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v5, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 75
    sget v2, Lcom/my/target/eb;->bf:I

    invoke-virtual {v0, v4, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 77
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v5, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 79
    const/16 v3, 0xe

    invoke-virtual {v2, v3, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 81
    new-instance v3, Lcom/my/target/ef;

    iget-object v4, p0, Lcom/my/target/eb;->aw:Lcom/my/target/cm;

    invoke-direct {v3, p1, v4}, Lcom/my/target/ef;-><init>(Landroid/content/Context;Lcom/my/target/cm;)V

    iput-object v3, p0, Lcom/my/target/eb;->bn:Lcom/my/target/ef;

    .line 82
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v3, v5, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 84
    const/16 v4, 0xc

    invoke-virtual {v3, v4, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 85
    iget-object v4, p0, Lcom/my/target/eb;->bn:Lcom/my/target/ef;

    invoke-virtual {v4, v3}, Lcom/my/target/ef;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 86
    iget-object v3, p0, Lcom/my/target/eb;->bn:Lcom/my/target/ef;

    sget v4, Lcom/my/target/eb;->bh:I

    invoke-virtual {v3, v4}, Lcom/my/target/ef;->setId(I)V

    .line 88
    new-instance v3, Lcom/my/target/by;

    invoke-direct {v3, p1}, Lcom/my/target/by;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/my/target/eb;->bp:Lcom/my/target/by;

    .line 89
    iget-object v3, p0, Lcom/my/target/eb;->bp:Lcom/my/target/by;

    sget v4, Lcom/my/target/eb;->bw:I

    invoke-virtual {v3, v4}, Lcom/my/target/by;->setId(I)V

    .line 91
    iget-object v3, p0, Lcom/my/target/eb;->bk:Lcom/my/target/bv;

    const-string v4, "icon_image"

    invoke-static {v3, v4}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 92
    iget-object v3, p0, Lcom/my/target/eb;->bp:Lcom/my/target/by;

    const-string v4, "sound_button"

    invoke-static {v3, v4}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 93
    iget-object v3, p0, Lcom/my/target/eb;->bl:Lcom/my/target/eh;

    const-string v4, "vertical_view"

    invoke-static {v3, v4}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 94
    iget-object v3, p0, Lcom/my/target/eb;->bm:Lcom/my/target/eg;

    const-string v4, "media_view"

    invoke-static {v3, v4}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 95
    iget-object v3, p0, Lcom/my/target/eb;->bn:Lcom/my/target/ef;

    const-string v4, "panel_view"

    invoke-static {v3, v4}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 96
    iget-object v3, p0, Lcom/my/target/eb;->F:Lcom/my/target/by;

    const-string v4, "close_button"

    invoke-static {v3, v4}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 97
    iget-object v3, p0, Lcom/my/target/eb;->bo:Lcom/my/target/dz;

    const-string v4, "progress_wheel"

    invoke-static {v3, v4}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 99
    iget-object v3, p0, Lcom/my/target/eb;->bn:Lcom/my/target/ef;

    invoke-virtual {p0, v3, v1}, Lcom/my/target/eb;->addView(Landroid/view/View;I)V

    .line 100
    iget-object v3, p0, Lcom/my/target/eb;->bk:Lcom/my/target/bv;

    invoke-virtual {p0, v3, v1}, Lcom/my/target/eb;->addView(Landroid/view/View;I)V

    .line 101
    iget-object v3, p0, Lcom/my/target/eb;->bl:Lcom/my/target/eh;

    invoke-virtual {p0, v3, v1, v0}, Lcom/my/target/eb;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 102
    iget-object v0, p0, Lcom/my/target/eb;->bm:Lcom/my/target/eg;

    invoke-virtual {p0, v0, v1, v2}, Lcom/my/target/eb;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 103
    iget-object v0, p0, Lcom/my/target/eb;->bp:Lcom/my/target/by;

    invoke-virtual {p0, v0}, Lcom/my/target/eb;->addView(Landroid/view/View;)V

    .line 104
    return-void

    :cond_0
    move v0, v1

    .line 55
    goto/16 :goto_0
.end method

.method static synthetic a(Lcom/my/target/eb;)Lcom/my/target/by;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/my/target/eb;->bp:Lcom/my/target/by;

    return-object v0
.end method

.method static synthetic b(Lcom/my/target/eb;)Lcom/my/target/ef;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/my/target/eb;->bn:Lcom/my/target/ef;

    return-object v0
.end method


# virtual methods
.method public final G()V
    .locals 2

    .prologue
    .line 309
    iget-object v0, p0, Lcom/my/target/eb;->F:Lcom/my/target/by;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/my/target/by;->setVisibility(I)V

    .line 310
    return-void
.end method

.method public final b(Lcom/my/target/core/models/banners/h;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 238
    iget-object v0, p0, Lcom/my/target/eb;->bp:Lcom/my/target/by;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/my/target/by;->setVisibility(I)V

    .line 239
    iget-object v0, p0, Lcom/my/target/eb;->F:Lcom/my/target/by;

    invoke-virtual {v0, v2}, Lcom/my/target/by;->setVisibility(I)V

    .line 240
    invoke-virtual {p0, v2}, Lcom/my/target/eb;->f(Z)V

    .line 241
    iget-object v0, p0, Lcom/my/target/eb;->bm:Lcom/my/target/eg;

    invoke-virtual {v0, p1}, Lcom/my/target/eg;->b(Lcom/my/target/core/models/banners/h;)V

    .line 242
    return-void
.end method

.method public final e(I)V
    .locals 1

    .prologue
    .line 336
    iget-object v0, p0, Lcom/my/target/eb;->bm:Lcom/my/target/eg;

    invoke-virtual {v0, p1}, Lcom/my/target/eg;->e(I)V

    .line 337
    return-void
.end method

.method public final f(Z)V
    .locals 4

    .prologue
    .line 280
    iget-object v0, p0, Lcom/my/target/eb;->bo:Lcom/my/target/dz;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/my/target/dz;->setVisibility(I)V

    .line 281
    iget-object v0, p0, Lcom/my/target/eb;->bn:Lcom/my/target/ef;

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/view/View;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/my/target/eb;->bp:Lcom/my/target/by;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/my/target/ef;->c([Landroid/view/View;)V

    .line 282
    iget-object v0, p0, Lcom/my/target/eb;->bm:Lcom/my/target/eg;

    invoke-virtual {v0, p1}, Lcom/my/target/eg;->h(Z)V

    .line 283
    return-void
.end method

.method public final finish()V
    .locals 0

    .prologue
    .line 276
    return-void
.end method

.method public final getCloseButton()Landroid/view/View;
    .locals 1

    .prologue
    .line 257
    iget-object v0, p0, Lcom/my/target/eb;->F:Lcom/my/target/by;

    return-object v0
.end method

.method public final getSoundButton()Lcom/my/target/by;
    .locals 1

    .prologue
    .line 252
    iget-object v0, p0, Lcom/my/target/eb;->bp:Lcom/my/target/by;

    return-object v0
.end method

.method public final isPaused()Z
    .locals 1

    .prologue
    .line 246
    iget-object v0, p0, Lcom/my/target/eb;->bm:Lcom/my/target/eg;

    invoke-virtual {v0}, Lcom/my/target/eg;->isPaused()Z

    move-result v0

    return v0
.end method

.method public final isPlaying()Z
    .locals 1

    .prologue
    .line 324
    iget-object v0, p0, Lcom/my/target/eb;->bm:Lcom/my/target/eg;

    invoke-virtual {v0}, Lcom/my/target/eg;->isPlaying()Z

    move-result v0

    return v0
.end method

.method public final pause()V
    .locals 4

    .prologue
    .line 329
    iget-object v0, p0, Lcom/my/target/eb;->bn:Lcom/my/target/ef;

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/view/View;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/my/target/eb;->bp:Lcom/my/target/by;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/my/target/ef;->c([Landroid/view/View;)V

    .line 330
    iget-object v0, p0, Lcom/my/target/eb;->bm:Lcom/my/target/eg;

    invoke-virtual {v0}, Lcom/my/target/eg;->pause()V

    .line 331
    return-void
.end method

.method public final play()V
    .locals 4

    .prologue
    .line 262
    iget-object v0, p0, Lcom/my/target/eb;->bn:Lcom/my/target/ef;

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/view/View;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/my/target/eb;->bp:Lcom/my/target/by;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/my/target/ef;->b([Landroid/view/View;)V

    .line 263
    iget-object v0, p0, Lcom/my/target/eb;->bm:Lcom/my/target/eg;

    invoke-virtual {v0}, Lcom/my/target/eg;->I()V

    .line 264
    return-void
.end method

.method public final resume()V
    .locals 4

    .prologue
    .line 268
    iget-object v0, p0, Lcom/my/target/eb;->bn:Lcom/my/target/ef;

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/view/View;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/my/target/eb;->bp:Lcom/my/target/by;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/my/target/ef;->b([Landroid/view/View;)V

    .line 269
    iget-object v0, p0, Lcom/my/target/eb;->bm:Lcom/my/target/eg;

    invoke-virtual {v0}, Lcom/my/target/eg;->resume()V

    .line 270
    return-void
.end method

.method public final setBanner(Lcom/my/target/core/models/banners/h;)V
    .locals 11
    .param p1, "banner"    # Lcom/my/target/core/models/banners/h;

    .prologue
    const/4 v3, 0x1

    const/16 v7, 0x1c

    const/16 v10, 0x8

    const/4 v1, 0x0

    const/4 v8, -0x2

    .line 108
    invoke-super {p0, p1}, Lcom/my/target/ee;->setBanner(Lcom/my/target/core/models/banners/h;)V

    .line 109
    iget-object v0, p0, Lcom/my/target/eb;->bo:Lcom/my/target/dz;

    invoke-virtual {v0, v10}, Lcom/my/target/dz;->setVisibility(I)V

    .line 110
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v2, p0, Lcom/my/target/eb;->aw:Lcom/my/target/cm;

    .line 111
    invoke-virtual {v2, v7}, Lcom/my/target/cm;->n(I)I

    move-result v2

    iget-object v4, p0, Lcom/my/target/eb;->aw:Lcom/my/target/cm;

    .line 112
    invoke-virtual {v4, v7}, Lcom/my/target/cm;->n(I)I

    move-result v4

    invoke-direct {v0, v2, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 113
    const/16 v2, 0x9

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 114
    iget-object v2, p0, Lcom/my/target/eb;->aw:Lcom/my/target/cm;

    const/16 v4, 0xa

    invoke-virtual {v2, v4}, Lcom/my/target/cm;->n(I)I

    move-result v2

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 115
    iget-object v2, p0, Lcom/my/target/eb;->aw:Lcom/my/target/cm;

    const/16 v4, 0xa

    invoke-virtual {v2, v4}, Lcom/my/target/cm;->n(I)I

    move-result v2

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 116
    iget-object v2, p0, Lcom/my/target/eb;->bo:Lcom/my/target/dz;

    invoke-virtual {v2, v0}, Lcom/my/target/dz;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 118
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v8, v8}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 120
    const/16 v2, 0xb

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 121
    iget-object v2, p0, Lcom/my/target/eb;->F:Lcom/my/target/by;

    invoke-virtual {v2, v10}, Lcom/my/target/by;->setVisibility(I)V

    .line 123
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getVideoBanner()Lcom/my/target/aj;

    move-result-object v4

    .line 124
    if-nez v4, :cond_0

    .line 126
    iget-object v2, p0, Lcom/my/target/eb;->bp:Lcom/my/target/by;

    invoke-virtual {v2, v10}, Lcom/my/target/by;->setVisibility(I)V

    .line 128
    :cond_0
    iget-object v2, p0, Lcom/my/target/eb;->F:Lcom/my/target/by;

    invoke-virtual {v2, v0}, Lcom/my/target/by;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 130
    iget-object v0, p0, Lcom/my/target/eb;->F:Lcom/my/target/by;

    invoke-virtual {v0}, Lcom/my/target/by;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_1

    .line 132
    iget-object v0, p0, Lcom/my/target/eb;->F:Lcom/my/target/by;

    invoke-virtual {p0, v0}, Lcom/my/target/eb;->addView(Landroid/view/View;)V

    .line 134
    :cond_1
    iget-object v0, p0, Lcom/my/target/eb;->bo:Lcom/my/target/dz;

    invoke-virtual {v0}, Lcom/my/target/dz;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_2

    .line 136
    iget-object v0, p0, Lcom/my/target/eb;->bo:Lcom/my/target/dz;

    invoke-virtual {p0, v0}, Lcom/my/target/eb;->addView(Landroid/view/View;)V

    .line 139
    :cond_2
    invoke-virtual {p0}, Lcom/my/target/eb;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "window"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 140
    new-instance v5, Landroid/util/DisplayMetrics;

    invoke-direct {v5}, Landroid/util/DisplayMetrics;-><init>()V

    .line 141
    if-eqz v0, :cond_3

    .line 143
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 146
    :cond_3
    iget-object v0, p0, Lcom/my/target/eb;->bn:Lcom/my/target/ef;

    invoke-virtual {v0}, Lcom/my/target/ef;->H()V

    .line 147
    iget-object v0, p0, Lcom/my/target/eb;->bn:Lcom/my/target/ef;

    invoke-virtual {v0, p1}, Lcom/my/target/ef;->setBanner(Lcom/my/target/core/models/banners/h;)V

    .line 149
    iget-object v0, p0, Lcom/my/target/eb;->bl:Lcom/my/target/eh;

    iget v2, v5, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v6, v5, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-virtual {v0, v2, v6}, Lcom/my/target/eh;->c(II)V

    .line 150
    iget-object v0, p0, Lcom/my/target/eb;->bl:Lcom/my/target/eh;

    invoke-virtual {v0, p1}, Lcom/my/target/eh;->setBanner(Lcom/my/target/core/models/banners/h;)V

    .line 152
    iget-object v0, p0, Lcom/my/target/eb;->bm:Lcom/my/target/eg;

    invoke-virtual {v0}, Lcom/my/target/eg;->H()V

    .line 153
    iget-object v0, p0, Lcom/my/target/eb;->bm:Lcom/my/target/eg;

    iget v2, p0, Lcom/my/target/eb;->style:I

    invoke-virtual {v0, p1, v2}, Lcom/my/target/eg;->a(Lcom/my/target/core/models/banners/h;I)V

    .line 155
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getCloseIcon()Lcom/my/target/common/models/ImageData;

    move-result-object v0

    .line 156
    if-eqz v0, :cond_9

    invoke-virtual {v0}, Lcom/my/target/common/models/ImageData;->getData()Landroid/graphics/Bitmap;

    move-result-object v2

    if-eqz v2, :cond_9

    .line 158
    iget-object v2, p0, Lcom/my/target/eb;->F:Lcom/my/target/by;

    invoke-virtual {v0}, Lcom/my/target/common/models/ImageData;->getData()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v2, v0, v3}, Lcom/my/target/by;->b(Landroid/graphics/Bitmap;Z)V

    .line 171
    :cond_4
    :goto_0
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getIcon()Lcom/my/target/common/models/ImageData;

    move-result-object v6

    .line 172
    if-eqz v6, :cond_b

    .line 174
    invoke-virtual {v6}, Lcom/my/target/common/models/ImageData;->getWidth()I

    move-result v2

    .line 175
    invoke-virtual {v6}, Lcom/my/target/common/models/ImageData;->getHeight()I

    move-result v0

    .line 178
    :goto_1
    new-instance v7, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v7, v8, v8}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 180
    iget-object v8, p0, Lcom/my/target/eb;->aw:Lcom/my/target/cm;

    const/4 v9, 0x4

    invoke-virtual {v8, v9}, Lcom/my/target/cm;->n(I)I

    move-result v8

    iput v8, v7, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 182
    if-eqz v2, :cond_6

    if-eqz v0, :cond_6

    .line 184
    int-to-float v0, v0

    int-to-float v2, v2

    div-float/2addr v0, v2

    .line 186
    iget-object v2, p0, Lcom/my/target/eb;->aw:Lcom/my/target/cm;

    const/16 v8, 0x40

    invoke-virtual {v2, v8}, Lcom/my/target/cm;->n(I)I

    move-result v2

    .line 187
    iget-object v8, p0, Lcom/my/target/eb;->aw:Lcom/my/target/cm;

    const/16 v9, 0x40

    invoke-virtual {v8, v9}, Lcom/my/target/cm;->n(I)I

    move-result v8

    int-to-float v8, v8

    mul-float/2addr v0, v8

    float-to-int v0, v0

    .line 189
    iput v2, v7, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 190
    iput v0, v7, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 192
    iget v2, v5, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v5, v5, Landroid/util/DisplayMetrics;->heightPixels:I

    add-int/2addr v2, v5

    const/16 v5, 0x500

    if-ge v2, v5, :cond_5

    move v1, v3

    .line 193
    :cond_5
    if-nez v1, :cond_6

    .line 195
    neg-int v0, v0

    div-int/lit8 v0, v0, 0x2

    iput v0, v7, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 199
    :cond_6
    sget v0, Lcom/my/target/eb;->bf:I

    invoke-virtual {v7, v10, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 200
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_a

    .line 202
    iget-object v0, p0, Lcom/my/target/eb;->aw:Lcom/my/target/cm;

    const/16 v1, 0x14

    invoke-virtual {v0, v1}, Lcom/my/target/cm;->n(I)I

    move-result v0

    invoke-virtual {v7, v0}, Landroid/widget/RelativeLayout$LayoutParams;->setMarginStart(I)V

    .line 208
    :goto_2
    iget-object v0, p0, Lcom/my/target/eb;->bk:Lcom/my/target/bv;

    invoke-virtual {v0, v7}, Lcom/my/target/bv;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 210
    if-eqz v6, :cond_7

    .line 212
    iget-object v0, p0, Lcom/my/target/eb;->bk:Lcom/my/target/bv;

    invoke-virtual {v6}, Lcom/my/target/common/models/ImageData;->getData()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/my/target/bv;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 215
    :cond_7
    if-eqz v4, :cond_8

    invoke-virtual {v4}, Lcom/my/target/aj;->isAutoPlay()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 217
    iget-object v0, p0, Lcom/my/target/eb;->bm:Lcom/my/target/eg;

    invoke-virtual {v0}, Lcom/my/target/eg;->I()V

    .line 218
    new-instance v0, Lcom/my/target/eb$1;

    invoke-direct {v0, p0}, Lcom/my/target/eb$1;-><init>(Lcom/my/target/eb;)V

    invoke-virtual {p0, v0}, Lcom/my/target/eb;->post(Ljava/lang/Runnable;)Z

    .line 227
    :cond_8
    return-void

    .line 162
    :cond_9
    iget-object v0, p0, Lcom/my/target/eb;->aw:Lcom/my/target/cm;

    invoke-virtual {v0, v7}, Lcom/my/target/cm;->n(I)I

    move-result v0

    invoke-static {v0}, Lcom/my/target/bq;->i(I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 163
    if-eqz v0, :cond_4

    .line 165
    iget-object v2, p0, Lcom/my/target/eb;->F:Lcom/my/target/by;

    invoke-virtual {v2, v0, v1}, Lcom/my/target/by;->b(Landroid/graphics/Bitmap;Z)V

    goto/16 :goto_0

    .line 206
    :cond_a
    iget-object v0, p0, Lcom/my/target/eb;->aw:Lcom/my/target/cm;

    const/16 v1, 0x14

    invoke-virtual {v0, v1}, Lcom/my/target/cm;->n(I)I

    move-result v0

    iput v0, v7, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    goto :goto_2

    :cond_b
    move v0, v1

    move v2, v1

    goto/16 :goto_1
.end method

.method public final setClickArea(Lcom/my/target/af;)V
    .locals 2
    .param p1, "area"    # Lcom/my/target/af;

    .prologue
    .line 287
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Apply click area "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/my/target/af;->O()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " to view"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 288
    iget-boolean v0, p1, Lcom/my/target/af;->cu:Z

    if-nez v0, :cond_0

    iget-boolean v0, p1, Lcom/my/target/af;->cE:Z

    if-eqz v0, :cond_2

    .line 290
    :cond_0
    iget-object v0, p0, Lcom/my/target/eb;->bk:Lcom/my/target/bv;

    iget-object v1, p0, Lcom/my/target/eb;->bx:Lcom/my/target/ee$a;

    invoke-virtual {v0, v1}, Lcom/my/target/bv;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 296
    :goto_0
    iget-object v0, p0, Lcom/my/target/eb;->bl:Lcom/my/target/eh;

    iget-object v1, p0, Lcom/my/target/eb;->bx:Lcom/my/target/ee$a;

    invoke-virtual {v0, p1, v1}, Lcom/my/target/eh;->a(Lcom/my/target/af;Landroid/view/View$OnClickListener;)V

    .line 297
    iget-object v0, p0, Lcom/my/target/eb;->bn:Lcom/my/target/ef;

    iget-object v1, p0, Lcom/my/target/eb;->bx:Lcom/my/target/ee$a;

    invoke-virtual {v0, p1, v1}, Lcom/my/target/ef;->a(Lcom/my/target/af;Landroid/view/View$OnClickListener;)V

    .line 298
    iget-object v1, p0, Lcom/my/target/eb;->bm:Lcom/my/target/eg;

    iget-boolean v0, p1, Lcom/my/target/af;->cv:Z

    if-nez v0, :cond_1

    iget-boolean v0, p1, Lcom/my/target/af;->cE:Z

    if-eqz v0, :cond_3

    :cond_1
    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v1, v0}, Lcom/my/target/eg;->setImageClickable(Z)V

    .line 299
    return-void

    .line 294
    :cond_2
    iget-object v0, p0, Lcom/my/target/eb;->bk:Lcom/my/target/bv;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/my/target/bv;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 298
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final setInterstitialPromoViewListener(Lcom/my/target/ee$b;)V
    .locals 1
    .param p1, "interstitialPromoViewListener"    # Lcom/my/target/ee$b;

    .prologue
    .line 303
    invoke-super {p0, p1}, Lcom/my/target/ee;->setInterstitialPromoViewListener(Lcom/my/target/ee$b;)V

    .line 304
    iget-object v0, p0, Lcom/my/target/eb;->bm:Lcom/my/target/eg;

    invoke-virtual {v0, p1}, Lcom/my/target/eg;->setInterstitialPromoViewListener(Lcom/my/target/ee$b;)V

    .line 305
    return-void
.end method

.method protected final setLayoutOrientation(I)V
    .locals 6
    .param p1, "orientation"    # I

    .prologue
    const/4 v5, 0x1

    const/16 v2, 0x8

    const/4 v4, 0x0

    const/4 v1, -0x2

    const/4 v3, -0x1

    .line 342
    invoke-super {p0, p1}, Lcom/my/target/ee;->setLayoutOrientation(I)V

    .line 344
    if-ne p1, v5, :cond_1

    .line 346
    invoke-virtual {p0, v3}, Lcom/my/target/eb;->setBackgroundColor(I)V

    .line 347
    iget-object v0, p0, Lcom/my/target/eb;->bl:Lcom/my/target/eh;

    invoke-virtual {v0, v4}, Lcom/my/target/eh;->setVisibility(I)V

    .line 348
    iget-object v0, p0, Lcom/my/target/eb;->bn:Lcom/my/target/ef;

    invoke-virtual {v0, v2}, Lcom/my/target/ef;->setVisibility(I)V

    .line 349
    iget-object v0, p0, Lcom/my/target/eb;->bk:Lcom/my/target/bv;

    invoke-virtual {v0, v4}, Lcom/my/target/bv;->setVisibility(I)V

    .line 351
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 354
    const/4 v1, 0x2

    sget v2, Lcom/my/target/eb;->bg:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 355
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x11

    if-lt v1, v2, :cond_0

    .line 357
    const/16 v1, 0x15

    invoke-virtual {v0, v1, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 363
    :goto_0
    iget-object v1, p0, Lcom/my/target/eb;->bp:Lcom/my/target/by;

    invoke-virtual {v1, v0}, Lcom/my/target/by;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 364
    iget-object v0, p0, Lcom/my/target/eb;->bn:Lcom/my/target/ef;

    new-array v1, v5, [Landroid/view/View;

    iget-object v2, p0, Lcom/my/target/eb;->bp:Lcom/my/target/by;

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/my/target/ef;->c([Landroid/view/View;)V

    .line 398
    :goto_1
    return-void

    .line 361
    :cond_0
    const/16 v1, 0xb

    invoke-virtual {v0, v1, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    goto :goto_0

    .line 368
    :cond_1
    const/high16 v0, -0x1000000

    invoke-virtual {p0, v0}, Lcom/my/target/eb;->setBackgroundColor(I)V

    .line 369
    iget-object v0, p0, Lcom/my/target/eb;->bl:Lcom/my/target/eh;

    invoke-virtual {v0, v2}, Lcom/my/target/eh;->setVisibility(I)V

    .line 370
    iget-object v0, p0, Lcom/my/target/eb;->bn:Lcom/my/target/ef;

    invoke-virtual {v0, v4}, Lcom/my/target/ef;->setVisibility(I)V

    .line 371
    iget-object v0, p0, Lcom/my/target/eb;->bk:Lcom/my/target/bv;

    invoke-virtual {v0, v2}, Lcom/my/target/bv;->setVisibility(I)V

    .line 372
    iget-object v0, p0, Lcom/my/target/eb;->bm:Lcom/my/target/eg;

    invoke-virtual {v0}, Lcom/my/target/eg;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 374
    new-instance v0, Lcom/my/target/eb$2;

    invoke-direct {v0, p0}, Lcom/my/target/eb$2;-><init>(Lcom/my/target/eb;)V

    invoke-virtual {p0, v0}, Lcom/my/target/eb;->post(Ljava/lang/Runnable;)Z

    .line 384
    :cond_2
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 387
    const/4 v1, 0x2

    sget v2, Lcom/my/target/eb;->bh:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 388
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x11

    if-lt v1, v2, :cond_3

    .line 390
    const/16 v1, 0x15

    invoke-virtual {v0, v1, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 396
    :goto_2
    iget-object v1, p0, Lcom/my/target/eb;->bp:Lcom/my/target/by;

    invoke-virtual {v1, v0}, Lcom/my/target/by;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1

    .line 394
    :cond_3
    const/16 v1, 0xb

    invoke-virtual {v0, v1, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    goto :goto_2
.end method

.method public final setTimeChanged(F)V
    .locals 3
    .param p1, "elapsedTime"    # F

    .prologue
    .line 314
    iget-object v0, p0, Lcom/my/target/eb;->bo:Lcom/my/target/dz;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/my/target/dz;->setVisibility(I)V

    .line 315
    iget v0, p0, Lcom/my/target/eb;->bA:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 317
    iget-object v0, p0, Lcom/my/target/eb;->bo:Lcom/my/target/dz;

    iget v1, p0, Lcom/my/target/eb;->bA:F

    div-float v1, p1, v1

    invoke-virtual {v0, v1}, Lcom/my/target/dz;->setProgress(F)V

    .line 319
    :cond_0
    iget-object v0, p0, Lcom/my/target/eb;->bo:Lcom/my/target/dz;

    iget v1, p0, Lcom/my/target/eb;->bA:F

    sub-float/2addr v1, p1

    const/high16 v2, 0x3f800000    # 1.0f

    add-float/2addr v1, v2

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Lcom/my/target/dz;->setDigit(I)V

    .line 320
    return-void
.end method
