.class synthetic Lru/cn/player/VideoSizeCalculator$1;
.super Ljava/lang/Object;
.source "VideoSizeCalculator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/player/VideoSizeCalculator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$ru$cn$player$SimplePlayer$FitMode:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 58
    invoke-static {}, Lru/cn/player/SimplePlayer$FitMode;->values()[Lru/cn/player/SimplePlayer$FitMode;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lru/cn/player/VideoSizeCalculator$1;->$SwitchMap$ru$cn$player$SimplePlayer$FitMode:[I

    :try_start_0
    sget-object v0, Lru/cn/player/VideoSizeCalculator$1;->$SwitchMap$ru$cn$player$SimplePlayer$FitMode:[I

    sget-object v1, Lru/cn/player/SimplePlayer$FitMode;->FIT_TO_WIDTH:Lru/cn/player/SimplePlayer$FitMode;

    invoke-virtual {v1}, Lru/cn/player/SimplePlayer$FitMode;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_3

    :goto_0
    :try_start_1
    sget-object v0, Lru/cn/player/VideoSizeCalculator$1;->$SwitchMap$ru$cn$player$SimplePlayer$FitMode:[I

    sget-object v1, Lru/cn/player/SimplePlayer$FitMode;->FIT_TO_HEIGHT:Lru/cn/player/SimplePlayer$FitMode;

    invoke-virtual {v1}, Lru/cn/player/SimplePlayer$FitMode;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_2

    :goto_1
    :try_start_2
    sget-object v0, Lru/cn/player/VideoSizeCalculator$1;->$SwitchMap$ru$cn$player$SimplePlayer$FitMode:[I

    sget-object v1, Lru/cn/player/SimplePlayer$FitMode;->FIT_TO_SIZE:Lru/cn/player/SimplePlayer$FitMode;

    invoke-virtual {v1}, Lru/cn/player/SimplePlayer$FitMode;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_1

    :goto_2
    :try_start_3
    sget-object v0, Lru/cn/player/VideoSizeCalculator$1;->$SwitchMap$ru$cn$player$SimplePlayer$FitMode:[I

    sget-object v1, Lru/cn/player/SimplePlayer$FitMode;->FIT_TO_USER:Lru/cn/player/SimplePlayer$FitMode;

    invoke-virtual {v1}, Lru/cn/player/SimplePlayer$FitMode;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_0

    :goto_3
    return-void

    :catch_0
    move-exception v0

    goto :goto_3

    :catch_1
    move-exception v0

    goto :goto_2

    :catch_2
    move-exception v0

    goto :goto_1

    :catch_3
    move-exception v0

    goto :goto_0
.end method
