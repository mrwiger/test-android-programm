.class public final Lcom/google/obf/dw;
.super Ljava/lang/Object;
.source "IMASDK"


# instance fields
.field public a:[B

.field private b:I

.field private c:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    new-array v0, p1, [B

    iput-object v0, p0, Lcom/google/obf/dw;->a:[B

    .line 4
    iget-object v0, p0, Lcom/google/obf/dw;->a:[B

    array-length v0, v0

    iput v0, p0, Lcom/google/obf/dw;->c:I

    .line 5
    return-void
.end method

.method public constructor <init>([B)V
    .locals 1

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    iput-object p1, p0, Lcom/google/obf/dw;->a:[B

    .line 8
    array-length v0, p1

    iput v0, p0, Lcom/google/obf/dw;->c:I

    .line 9
    return-void
.end method

.method public constructor <init>([BI)V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput-object p1, p0, Lcom/google/obf/dw;->a:[B

    .line 12
    iput p2, p0, Lcom/google/obf/dw;->c:I

    .line 13
    return-void
.end method


# virtual methods
.method public a(ILjava/nio/charset/Charset;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 74
    new-instance v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/google/obf/dw;->a:[B

    iget v2, p0, Lcom/google/obf/dw;->b:I

    invoke-direct {v0, v1, v2, p1, p2}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    .line 75
    iget v1, p0, Lcom/google/obf/dw;->b:I

    add-int/2addr v1, p1

    iput v1, p0, Lcom/google/obf/dw;->b:I

    .line 76
    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 20
    iput v0, p0, Lcom/google/obf/dw;->b:I

    .line 21
    iput v0, p0, Lcom/google/obf/dw;->c:I

    .line 22
    return-void
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 14
    invoke-virtual {p0}, Lcom/google/obf/dw;->e()I

    move-result v0

    if-ge v0, p1, :cond_0

    new-array v0, p1, [B

    :goto_0
    invoke-virtual {p0, v0, p1}, Lcom/google/obf/dw;->a([BI)V

    .line 15
    return-void

    .line 14
    :cond_0
    iget-object v0, p0, Lcom/google/obf/dw;->a:[B

    goto :goto_0
.end method

.method public a(Lcom/google/obf/dv;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 35
    iget-object v0, p1, Lcom/google/obf/dv;->a:[B

    invoke-virtual {p0, v0, v1, p2}, Lcom/google/obf/dw;->a([BII)V

    .line 36
    invoke-virtual {p1, v1}, Lcom/google/obf/dv;->a(I)V

    .line 37
    return-void
.end method

.method public a([BI)V
    .locals 1

    .prologue
    .line 16
    iput-object p1, p0, Lcom/google/obf/dw;->a:[B

    .line 17
    iput p2, p0, Lcom/google/obf/dw;->c:I

    .line 18
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/obf/dw;->b:I

    .line 19
    return-void
.end method

.method public a([BII)V
    .locals 2

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/obf/dw;->a:[B

    iget v1, p0, Lcom/google/obf/dw;->b:I

    invoke-static {v0, v1, p1, p2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 39
    iget v0, p0, Lcom/google/obf/dw;->b:I

    add-int/2addr v0, p3

    iput v0, p0, Lcom/google/obf/dw;->b:I

    .line 40
    return-void
.end method

.method public b()I
    .locals 2

    .prologue
    .line 23
    iget v0, p0, Lcom/google/obf/dw;->c:I

    iget v1, p0, Lcom/google/obf/dw;->b:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public b(I)V
    .locals 1

    .prologue
    .line 25
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/google/obf/dw;->a:[B

    array-length v0, v0

    if-gt p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/obf/dl;->a(Z)V

    .line 26
    iput p1, p0, Lcom/google/obf/dw;->c:I

    .line 27
    return-void

    .line 25
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 24
    iget v0, p0, Lcom/google/obf/dw;->c:I

    return v0
.end method

.method public c(I)V
    .locals 1

    .prologue
    .line 30
    if-ltz p1, :cond_0

    iget v0, p0, Lcom/google/obf/dw;->c:I

    if-gt p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/obf/dl;->a(Z)V

    .line 31
    iput p1, p0, Lcom/google/obf/dw;->b:I

    .line 32
    return-void

    .line 30
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 28
    iget v0, p0, Lcom/google/obf/dw;->b:I

    return v0
.end method

.method public d(I)V
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/google/obf/dw;->b:I

    add-int/2addr v0, p1

    invoke-virtual {p0, v0}, Lcom/google/obf/dw;->c(I)V

    .line 34
    return-void
.end method

.method public e()I
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/obf/dw;->a:[B

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/obf/dw;->a:[B

    array-length v0, v0

    goto :goto_0
.end method

.method public e(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    invoke-static {}, Ljava/nio/charset/Charset;->defaultCharset()Ljava/nio/charset/Charset;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/obf/dw;->a(ILjava/nio/charset/Charset;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public f()I
    .locals 3

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/obf/dw;->a:[B

    iget v1, p0, Lcom/google/obf/dw;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/obf/dw;->b:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    return v0
.end method

.method public g()I
    .locals 4

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/obf/dw;->a:[B

    iget v1, p0, Lcom/google/obf/dw;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/obf/dw;->b:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x8

    iget-object v1, p0, Lcom/google/obf/dw;->a:[B

    iget v2, p0, Lcom/google/obf/dw;->b:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/google/obf/dw;->b:I

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    return v0
.end method

.method public h()I
    .locals 4

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/obf/dw;->a:[B

    iget v1, p0, Lcom/google/obf/dw;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/obf/dw;->b:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    iget-object v1, p0, Lcom/google/obf/dw;->a:[B

    iget v2, p0, Lcom/google/obf/dw;->b:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/google/obf/dw;->b:I

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    return v0
.end method

.method public i()S
    .locals 4

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/obf/dw;->a:[B

    iget v1, p0, Lcom/google/obf/dw;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/obf/dw;->b:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x8

    iget-object v1, p0, Lcom/google/obf/dw;->a:[B

    iget v2, p0, Lcom/google/obf/dw;->b:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/google/obf/dw;->b:I

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    int-to-short v0, v0

    return v0
.end method

.method public j()I
    .locals 4

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/obf/dw;->a:[B

    iget v1, p0, Lcom/google/obf/dw;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/obf/dw;->b:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x10

    iget-object v1, p0, Lcom/google/obf/dw;->a:[B

    iget v2, p0, Lcom/google/obf/dw;->b:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/google/obf/dw;->b:I

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/obf/dw;->a:[B

    iget v2, p0, Lcom/google/obf/dw;->b:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/google/obf/dw;->b:I

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    return v0
.end method

.method public k()J
    .locals 8

    .prologue
    const-wide/16 v6, 0xff

    .line 46
    iget-object v0, p0, Lcom/google/obf/dw;->a:[B

    iget v1, p0, Lcom/google/obf/dw;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/obf/dw;->b:I

    aget-byte v0, v0, v1

    int-to-long v0, v0

    and-long/2addr v0, v6

    const/16 v2, 0x18

    shl-long/2addr v0, v2

    iget-object v2, p0, Lcom/google/obf/dw;->a:[B

    iget v3, p0, Lcom/google/obf/dw;->b:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lcom/google/obf/dw;->b:I

    aget-byte v2, v2, v3

    int-to-long v2, v2

    and-long/2addr v2, v6

    const/16 v4, 0x10

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    iget-object v2, p0, Lcom/google/obf/dw;->a:[B

    iget v3, p0, Lcom/google/obf/dw;->b:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lcom/google/obf/dw;->b:I

    aget-byte v2, v2, v3

    int-to-long v2, v2

    and-long/2addr v2, v6

    const/16 v4, 0x8

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    iget-object v2, p0, Lcom/google/obf/dw;->a:[B

    iget v3, p0, Lcom/google/obf/dw;->b:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lcom/google/obf/dw;->b:I

    aget-byte v2, v2, v3

    int-to-long v2, v2

    and-long/2addr v2, v6

    or-long/2addr v0, v2

    return-wide v0
.end method

.method public l()J
    .locals 8

    .prologue
    const-wide/16 v6, 0xff

    .line 47
    iget-object v0, p0, Lcom/google/obf/dw;->a:[B

    iget v1, p0, Lcom/google/obf/dw;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/obf/dw;->b:I

    aget-byte v0, v0, v1

    int-to-long v0, v0

    and-long/2addr v0, v6

    iget-object v2, p0, Lcom/google/obf/dw;->a:[B

    iget v3, p0, Lcom/google/obf/dw;->b:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lcom/google/obf/dw;->b:I

    aget-byte v2, v2, v3

    int-to-long v2, v2

    and-long/2addr v2, v6

    const/16 v4, 0x8

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    iget-object v2, p0, Lcom/google/obf/dw;->a:[B

    iget v3, p0, Lcom/google/obf/dw;->b:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lcom/google/obf/dw;->b:I

    aget-byte v2, v2, v3

    int-to-long v2, v2

    and-long/2addr v2, v6

    const/16 v4, 0x10

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    iget-object v2, p0, Lcom/google/obf/dw;->a:[B

    iget v3, p0, Lcom/google/obf/dw;->b:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lcom/google/obf/dw;->b:I

    aget-byte v2, v2, v3

    int-to-long v2, v2

    and-long/2addr v2, v6

    const/16 v4, 0x18

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    return-wide v0
.end method

.method public m()I
    .locals 4

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/obf/dw;->a:[B

    iget v1, p0, Lcom/google/obf/dw;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/obf/dw;->b:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x18

    iget-object v1, p0, Lcom/google/obf/dw;->a:[B

    iget v2, p0, Lcom/google/obf/dw;->b:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/google/obf/dw;->b:I

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/obf/dw;->a:[B

    iget v2, p0, Lcom/google/obf/dw;->b:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/google/obf/dw;->b:I

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/obf/dw;->a:[B

    iget v2, p0, Lcom/google/obf/dw;->b:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/google/obf/dw;->b:I

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    return v0
.end method

.method public n()I
    .locals 4

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/obf/dw;->a:[B

    iget v1, p0, Lcom/google/obf/dw;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/obf/dw;->b:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    iget-object v1, p0, Lcom/google/obf/dw;->a:[B

    iget v2, p0, Lcom/google/obf/dw;->b:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/google/obf/dw;->b:I

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/obf/dw;->a:[B

    iget v2, p0, Lcom/google/obf/dw;->b:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/google/obf/dw;->b:I

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/obf/dw;->a:[B

    iget v2, p0, Lcom/google/obf/dw;->b:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/google/obf/dw;->b:I

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x18

    or-int/2addr v0, v1

    return v0
.end method

.method public o()J
    .locals 8

    .prologue
    const-wide/16 v6, 0xff

    .line 50
    iget-object v0, p0, Lcom/google/obf/dw;->a:[B

    iget v1, p0, Lcom/google/obf/dw;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/obf/dw;->b:I

    aget-byte v0, v0, v1

    int-to-long v0, v0

    and-long/2addr v0, v6

    const/16 v2, 0x38

    shl-long/2addr v0, v2

    iget-object v2, p0, Lcom/google/obf/dw;->a:[B

    iget v3, p0, Lcom/google/obf/dw;->b:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lcom/google/obf/dw;->b:I

    aget-byte v2, v2, v3

    int-to-long v2, v2

    and-long/2addr v2, v6

    const/16 v4, 0x30

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    iget-object v2, p0, Lcom/google/obf/dw;->a:[B

    iget v3, p0, Lcom/google/obf/dw;->b:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lcom/google/obf/dw;->b:I

    aget-byte v2, v2, v3

    int-to-long v2, v2

    and-long/2addr v2, v6

    const/16 v4, 0x28

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    iget-object v2, p0, Lcom/google/obf/dw;->a:[B

    iget v3, p0, Lcom/google/obf/dw;->b:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lcom/google/obf/dw;->b:I

    aget-byte v2, v2, v3

    int-to-long v2, v2

    and-long/2addr v2, v6

    const/16 v4, 0x20

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    iget-object v2, p0, Lcom/google/obf/dw;->a:[B

    iget v3, p0, Lcom/google/obf/dw;->b:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lcom/google/obf/dw;->b:I

    aget-byte v2, v2, v3

    int-to-long v2, v2

    and-long/2addr v2, v6

    const/16 v4, 0x18

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    iget-object v2, p0, Lcom/google/obf/dw;->a:[B

    iget v3, p0, Lcom/google/obf/dw;->b:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lcom/google/obf/dw;->b:I

    aget-byte v2, v2, v3

    int-to-long v2, v2

    and-long/2addr v2, v6

    const/16 v4, 0x10

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    iget-object v2, p0, Lcom/google/obf/dw;->a:[B

    iget v3, p0, Lcom/google/obf/dw;->b:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lcom/google/obf/dw;->b:I

    aget-byte v2, v2, v3

    int-to-long v2, v2

    and-long/2addr v2, v6

    const/16 v4, 0x8

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    iget-object v2, p0, Lcom/google/obf/dw;->a:[B

    iget v3, p0, Lcom/google/obf/dw;->b:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lcom/google/obf/dw;->b:I

    aget-byte v2, v2, v3

    int-to-long v2, v2

    and-long/2addr v2, v6

    or-long/2addr v0, v2

    return-wide v0
.end method

.method public p()J
    .locals 8

    .prologue
    const-wide/16 v6, 0xff

    .line 51
    iget-object v0, p0, Lcom/google/obf/dw;->a:[B

    iget v1, p0, Lcom/google/obf/dw;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/obf/dw;->b:I

    aget-byte v0, v0, v1

    int-to-long v0, v0

    and-long/2addr v0, v6

    iget-object v2, p0, Lcom/google/obf/dw;->a:[B

    iget v3, p0, Lcom/google/obf/dw;->b:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lcom/google/obf/dw;->b:I

    aget-byte v2, v2, v3

    int-to-long v2, v2

    and-long/2addr v2, v6

    const/16 v4, 0x8

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    iget-object v2, p0, Lcom/google/obf/dw;->a:[B

    iget v3, p0, Lcom/google/obf/dw;->b:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lcom/google/obf/dw;->b:I

    aget-byte v2, v2, v3

    int-to-long v2, v2

    and-long/2addr v2, v6

    const/16 v4, 0x10

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    iget-object v2, p0, Lcom/google/obf/dw;->a:[B

    iget v3, p0, Lcom/google/obf/dw;->b:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lcom/google/obf/dw;->b:I

    aget-byte v2, v2, v3

    int-to-long v2, v2

    and-long/2addr v2, v6

    const/16 v4, 0x18

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    iget-object v2, p0, Lcom/google/obf/dw;->a:[B

    iget v3, p0, Lcom/google/obf/dw;->b:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lcom/google/obf/dw;->b:I

    aget-byte v2, v2, v3

    int-to-long v2, v2

    and-long/2addr v2, v6

    const/16 v4, 0x20

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    iget-object v2, p0, Lcom/google/obf/dw;->a:[B

    iget v3, p0, Lcom/google/obf/dw;->b:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lcom/google/obf/dw;->b:I

    aget-byte v2, v2, v3

    int-to-long v2, v2

    and-long/2addr v2, v6

    const/16 v4, 0x28

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    iget-object v2, p0, Lcom/google/obf/dw;->a:[B

    iget v3, p0, Lcom/google/obf/dw;->b:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lcom/google/obf/dw;->b:I

    aget-byte v2, v2, v3

    int-to-long v2, v2

    and-long/2addr v2, v6

    const/16 v4, 0x30

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    iget-object v2, p0, Lcom/google/obf/dw;->a:[B

    iget v3, p0, Lcom/google/obf/dw;->b:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lcom/google/obf/dw;->b:I

    aget-byte v2, v2, v3

    int-to-long v2, v2

    and-long/2addr v2, v6

    const/16 v4, 0x38

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    return-wide v0
.end method

.method public q()I
    .locals 4

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/obf/dw;->a:[B

    iget v1, p0, Lcom/google/obf/dw;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/obf/dw;->b:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x8

    iget-object v1, p0, Lcom/google/obf/dw;->a:[B

    iget v2, p0, Lcom/google/obf/dw;->b:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/google/obf/dw;->b:I

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    .line 53
    iget v1, p0, Lcom/google/obf/dw;->b:I

    add-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/obf/dw;->b:I

    .line 54
    return v0
.end method

.method public r()I
    .locals 4

    .prologue
    .line 55
    invoke-virtual {p0}, Lcom/google/obf/dw;->f()I

    move-result v0

    .line 56
    invoke-virtual {p0}, Lcom/google/obf/dw;->f()I

    move-result v1

    .line 57
    invoke-virtual {p0}, Lcom/google/obf/dw;->f()I

    move-result v2

    .line 58
    invoke-virtual {p0}, Lcom/google/obf/dw;->f()I

    move-result v3

    .line 59
    shl-int/lit8 v0, v0, 0x15

    shl-int/lit8 v1, v1, 0xe

    or-int/2addr v0, v1

    shl-int/lit8 v1, v2, 0x7

    or-int/2addr v0, v1

    or-int/2addr v0, v3

    return v0
.end method

.method public s()I
    .locals 4

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/google/obf/dw;->m()I

    move-result v0

    .line 61
    if-gez v0, :cond_0

    .line 62
    new-instance v1, Ljava/lang/IllegalStateException;

    const/16 v2, 0x1d

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Top bit not zero: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 63
    :cond_0
    return v0
.end method

.method public t()I
    .locals 4

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/google/obf/dw;->n()I

    move-result v0

    .line 65
    if-gez v0, :cond_0

    .line 66
    new-instance v1, Ljava/lang/IllegalStateException;

    const/16 v2, 0x1d

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Top bit not zero: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 67
    :cond_0
    return v0
.end method

.method public u()J
    .locals 5

    .prologue
    .line 68
    invoke-virtual {p0}, Lcom/google/obf/dw;->o()J

    move-result-wide v0

    .line 69
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    .line 70
    new-instance v2, Ljava/lang/IllegalStateException;

    const/16 v3, 0x26

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Top bit not zero: "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 71
    :cond_0
    return-wide v0
.end method

.method public v()D
    .locals 2

    .prologue
    .line 72
    invoke-virtual {p0}, Lcom/google/obf/dw;->o()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    return-wide v0
.end method

.method public w()J
    .locals 11

    .prologue
    const/4 v5, 0x7

    const/4 v10, 0x6

    const/4 v0, 0x1

    .line 77
    const/4 v1, 0x0

    .line 78
    iget-object v2, p0, Lcom/google/obf/dw;->a:[B

    iget v3, p0, Lcom/google/obf/dw;->b:I

    aget-byte v2, v2, v3

    int-to-long v2, v2

    move v4, v5

    .line 79
    :goto_0
    if-ltz v4, :cond_0

    .line 80
    shl-int v6, v0, v4

    int-to-long v6, v6

    and-long/2addr v6, v2

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-nez v6, :cond_2

    .line 81
    if-ge v4, v10, :cond_1

    .line 82
    shl-int v1, v0, v4

    add-int/lit8 v1, v1, -0x1

    int-to-long v6, v1

    and-long/2addr v2, v6

    .line 83
    rsub-int/lit8 v1, v4, 0x7

    .line 87
    :cond_0
    :goto_1
    if-nez v1, :cond_4

    .line 88
    new-instance v0, Ljava/lang/NumberFormatException;

    const/16 v1, 0x37

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Invalid UTF-8 sequence first byte: "

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 84
    :cond_1
    if-ne v4, v5, :cond_0

    move v1, v0

    .line 85
    goto :goto_1

    .line 86
    :cond_2
    add-int/lit8 v4, v4, -0x1

    goto :goto_0

    .line 93
    :cond_3
    shl-long/2addr v2, v10

    and-int/lit8 v4, v4, 0x3f

    int-to-long v4, v4

    or-long/2addr v2, v4

    .line 94
    add-int/lit8 v0, v0, 0x1

    .line 89
    :cond_4
    if-ge v0, v1, :cond_5

    .line 90
    iget-object v4, p0, Lcom/google/obf/dw;->a:[B

    iget v5, p0, Lcom/google/obf/dw;->b:I

    add-int/2addr v5, v0

    aget-byte v4, v4, v5

    .line 91
    and-int/lit16 v5, v4, 0xc0

    const/16 v6, 0x80

    if-eq v5, v6, :cond_3

    .line 92
    new-instance v0, Ljava/lang/NumberFormatException;

    const/16 v1, 0x3e

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Invalid UTF-8 sequence continuation byte: "

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NumberFormatException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 95
    :cond_5
    iget v0, p0, Lcom/google/obf/dw;->b:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/obf/dw;->b:I

    .line 96
    return-wide v2
.end method
