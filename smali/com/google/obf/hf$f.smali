.class final enum Lcom/google/obf/hf$f;
.super Ljava/lang/Enum;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/obf/hf;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "f"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/obf/hf$f;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/obf/hf$f;

.field public static final enum b:Lcom/google/obf/hf$f;

.field public static final enum c:Lcom/google/obf/hf$f;

.field public static final enum d:Lcom/google/obf/hf$f;

.field private static final synthetic e:[Lcom/google/obf/hf$f;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 4
    new-instance v0, Lcom/google/obf/hf$f;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v2}, Lcom/google/obf/hf$f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hf$f;->a:Lcom/google/obf/hf$f;

    .line 5
    new-instance v0, Lcom/google/obf/hf$f;

    const-string v1, "LOADED"

    invoke-direct {v0, v1, v3}, Lcom/google/obf/hf$f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hf$f;->b:Lcom/google/obf/hf$f;

    .line 6
    new-instance v0, Lcom/google/obf/hf$f;

    const-string v1, "PLAYING"

    invoke-direct {v0, v1, v4}, Lcom/google/obf/hf$f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hf$f;->c:Lcom/google/obf/hf$f;

    .line 7
    new-instance v0, Lcom/google/obf/hf$f;

    const-string v1, "PAUSED"

    invoke-direct {v0, v1, v5}, Lcom/google/obf/hf$f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hf$f;->d:Lcom/google/obf/hf$f;

    .line 8
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/obf/hf$f;

    sget-object v1, Lcom/google/obf/hf$f;->a:Lcom/google/obf/hf$f;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/obf/hf$f;->b:Lcom/google/obf/hf$f;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/obf/hf$f;->c:Lcom/google/obf/hf$f;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/obf/hf$f;->d:Lcom/google/obf/hf$f;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/obf/hf$f;->e:[Lcom/google/obf/hf$f;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/obf/hf$f;
    .locals 1

    .prologue
    .line 2
    const-class v0, Lcom/google/obf/hf$f;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/obf/hf$f;

    return-object v0
.end method

.method public static values()[Lcom/google/obf/hf$f;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lcom/google/obf/hf$f;->e:[Lcom/google/obf/hf$f;

    invoke-virtual {v0}, [Lcom/google/obf/hf$f;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/obf/hf$f;

    return-object v0
.end method
