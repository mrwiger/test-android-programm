.class Lru/cn/tv/mobile/NewActivity$15;
.super Ljava/lang/Object;
.source "NewActivity.java"

# interfaces
.implements Lru/cn/tv/billing/BillingFragment$BillingFragmentListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/tv/mobile/NewActivity;->showBilling(JJZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/tv/mobile/NewActivity;

.field final synthetic val$channelId:J

.field final synthetic val$contractorId:J


# direct methods
.method constructor <init>(Lru/cn/tv/mobile/NewActivity;JJ)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/tv/mobile/NewActivity;

    .prologue
    .line 839
    iput-object p1, p0, Lru/cn/tv/mobile/NewActivity$15;->this$0:Lru/cn/tv/mobile/NewActivity;

    iput-wide p2, p0, Lru/cn/tv/mobile/NewActivity$15;->val$contractorId:J

    iput-wide p4, p0, Lru/cn/tv/mobile/NewActivity$15;->val$channelId:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBillingFragmentStop()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 858
    invoke-static {}, Lru/cn/api/provider/TvContentProviderContract;->channels()Landroid/net/Uri;

    move-result-object v0

    .line 859
    .local v0, "channelsUri":Landroid/net/Uri;
    iget-object v1, p0, Lru/cn/tv/mobile/NewActivity$15;->this$0:Lru/cn/tv/mobile/NewActivity;

    invoke-virtual {v1}, Lru/cn/tv/mobile/NewActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x0

    const-string v3, "contractor"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    iget-wide v6, p0, Lru/cn/tv/mobile/NewActivity$15;->val$contractorId:J

    .line 860
    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    .line 859
    invoke-virtual {v1, v0, v2, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 862
    iget-wide v2, p0, Lru/cn/tv/mobile/NewActivity$15;->val$channelId:J

    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 863
    iget-object v1, p0, Lru/cn/tv/mobile/NewActivity$15;->this$0:Lru/cn/tv/mobile/NewActivity;

    invoke-static {v1}, Lru/cn/tv/mobile/NewActivity;->access$2000(Lru/cn/tv/mobile/NewActivity;)Lru/cn/tv/player/FloatingPlayerFragment;

    move-result-object v1

    invoke-virtual {v1}, Lru/cn/tv/player/FloatingPlayerFragment;->hideLockWrapper()V

    .line 864
    iget-object v1, p0, Lru/cn/tv/mobile/NewActivity$15;->this$0:Lru/cn/tv/mobile/NewActivity;

    invoke-static {v1}, Lru/cn/tv/mobile/NewActivity;->access$2000(Lru/cn/tv/mobile/NewActivity;)Lru/cn/tv/player/FloatingPlayerFragment;

    move-result-object v1

    invoke-virtual {v1, v8}, Lru/cn/tv/player/FloatingPlayerFragment;->maximize(Z)V

    .line 865
    iget-object v1, p0, Lru/cn/tv/mobile/NewActivity$15;->this$0:Lru/cn/tv/mobile/NewActivity;

    invoke-static {v1}, Lru/cn/tv/mobile/NewActivity;->access$2000(Lru/cn/tv/mobile/NewActivity;)Lru/cn/tv/player/FloatingPlayerFragment;

    move-result-object v1

    invoke-virtual {v1}, Lru/cn/tv/player/FloatingPlayerFragment;->restartCurrentLocation()V

    .line 867
    :cond_0
    return-void
.end method

.method public onManageSubscribe(I)V
    .locals 7
    .param p1, "storeId"    # I

    .prologue
    .line 871
    iget-object v2, p0, Lru/cn/tv/mobile/NewActivity$15;->this$0:Lru/cn/tv/mobile/NewActivity;

    invoke-virtual {v2}, Lru/cn/tv/mobile/NewActivity;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 873
    .local v1, "appPackageName":Ljava/lang/String;
    :try_start_0
    iget-object v2, p0, Lru/cn/tv/mobile/NewActivity$15;->this$0:Lru/cn/tv/mobile/NewActivity;

    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "market://details?id="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v2, v3}, Lru/cn/tv/mobile/NewActivity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 877
    :goto_0
    return-void

    .line 874
    :catch_0
    move-exception v0

    .line 875
    .local v0, "anfe":Landroid/content/ActivityNotFoundException;
    iget-object v2, p0, Lru/cn/tv/mobile/NewActivity$15;->this$0:Lru/cn/tv/mobile/NewActivity;

    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "https://play.google.com/store/apps/details?id="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v2, v3}, Lru/cn/tv/mobile/NewActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public onPurchase(Ljava/lang/String;)V
    .locals 2
    .param p1, "productId"    # Ljava/lang/String;

    .prologue
    .line 842
    invoke-static {p1}, Lru/cn/domain/statistics/AnalyticsManager;->purchase_start(Ljava/lang/String;)V

    .line 844
    iget-object v0, p0, Lru/cn/tv/mobile/NewActivity$15;->this$0:Lru/cn/tv/mobile/NewActivity;

    new-instance v1, Lru/cn/tv/mobile/NewActivity$15$1;

    invoke-direct {v1, p0}, Lru/cn/tv/mobile/NewActivity$15$1;-><init>(Lru/cn/tv/mobile/NewActivity$15;)V

    invoke-static {v0, p1, v1}, Lru/cn/domain/PurchaseManager;->makePurchase(Landroid/app/Activity;Ljava/lang/String;Lru/cn/domain/stores/PurchaseStore$OnPurchaseFinishListener;)V

    .line 854
    return-void
.end method
