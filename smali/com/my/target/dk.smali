.class public final Lcom/my/target/dk;
.super Landroid/widget/RelativeLayout;
.source "StandardNativeSlimView.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;
.implements Lcom/my/target/dm;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/dk$b;,
        Lcom/my/target/dk$a;
    }
.end annotation


# static fields
.field private static final aC:I

.field private static final aD:I

.field private static final aE:I

.field private static final aF:I

.field private static final aG:I

.field static aH:J


# instance fields
.field private final P:Lcom/my/target/cm;

.field private final aI:Lcom/my/target/bu;

.field private final aJ:Landroid/widget/TextView;

.field private final aK:Landroid/widget/TextView;

.field private final aL:Landroid/widget/TextView;

.field private final aM:Landroid/widget/LinearLayout;

.field private final aN:Landroid/widget/Button;

.field private final aO:Lcom/my/target/ca;

.field private final aP:Lcom/my/target/bx;

.field private final aQ:Landroid/widget/RelativeLayout;

.field private final aR:Landroid/widget/RelativeLayout;

.field private final aS:Landroid/widget/ViewFlipper;

.field private final aT:Landroid/widget/FrameLayout;

.field private final aU:Landroid/widget/TextView;

.field private final aV:Landroid/widget/TextView;

.field private final aW:Lcom/my/target/dk$b;

.field private final aX:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final aY:I

.field private final aZ:F

.field private a_:Landroid/view/View$OnClickListener;

.field private final ba:I

.field private backgroundColor:I

.field private final bb:I

.field private final bc:I

.field private final bd:I

.field private final be:I

.field private final bf:I

.field private final bg:I

.field private bh:Lcom/my/target/dk$a;

.field private bi:I

.field private bj:I

.field private bk:Z

.field private bl:Z

.field private bm:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 46
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/dk;->aC:I

    .line 47
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/dk;->aD:I

    .line 48
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/dk;->aE:I

    .line 49
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/dk;->aF:I

    .line 50
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/dk;->aG:I

    .line 53
    const-wide/16 v0, 0xfa0

    sput-wide v0, Lcom/my/target/dk;->aH:J

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 88
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/my/target/dk;-><init>(Ljava/lang/String;Landroid/content/Context;B)V

    .line 89
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Landroid/content/Context;B)V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x4

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, -0x2

    .line 94
    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 67
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/my/target/dk;->aX:Ljava/util/HashMap;

    .line 95
    invoke-static {p2}, Lcom/my/target/cm;->x(Landroid/content/Context;)Lcom/my/target/cm;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/dk;->P:Lcom/my/target/cm;

    .line 97
    new-instance v0, Lcom/my/target/bx;

    invoke-direct {v0, p2}, Lcom/my/target/bx;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/dk;->aP:Lcom/my/target/bx;

    .line 98
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-direct {v0, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/dk;->aT:Landroid/widget/FrameLayout;

    .line 99
    new-instance v0, Landroid/widget/ViewFlipper;

    invoke-direct {v0, p2}, Landroid/widget/ViewFlipper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/dk;->aS:Landroid/widget/ViewFlipper;

    .line 100
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/dk;->aM:Landroid/widget/LinearLayout;

    .line 101
    new-instance v0, Landroid/widget/Button;

    invoke-direct {v0, p2}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/dk;->aN:Landroid/widget/Button;

    .line 102
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/dk;->aJ:Landroid/widget/TextView;

    .line 103
    new-instance v0, Lcom/my/target/bu;

    invoke-direct {v0, p2}, Lcom/my/target/bu;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/dk;->aI:Lcom/my/target/bu;

    .line 104
    new-instance v0, Lcom/my/target/ca;

    invoke-direct {v0, p2}, Lcom/my/target/ca;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/dk;->aO:Lcom/my/target/ca;

    .line 105
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/dk;->aK:Landroid/widget/TextView;

    .line 106
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/dk;->aL:Landroid/widget/TextView;

    .line 107
    new-instance v0, Landroid/widget/RelativeLayout;

    invoke-direct {v0, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/dk;->aQ:Landroid/widget/RelativeLayout;

    .line 108
    new-instance v0, Landroid/widget/RelativeLayout;

    invoke-direct {v0, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/dk;->aR:Landroid/widget/RelativeLayout;

    .line 109
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/dk;->aU:Landroid/widget/TextView;

    .line 110
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/dk;->aV:Landroid/widget/TextView;

    .line 112
    const-string v0, "ad_view"

    invoke-static {p0, v0}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 113
    iget-object v0, p0, Lcom/my/target/dk;->aP:Lcom/my/target/bx;

    const-string v1, "icon_image"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 114
    iget-object v0, p0, Lcom/my/target/dk;->aT:Landroid/widget/FrameLayout;

    const-string v1, "icon_layout"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 115
    iget-object v0, p0, Lcom/my/target/dk;->aN:Landroid/widget/Button;

    const-string v1, "cta_button"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 116
    iget-object v0, p0, Lcom/my/target/dk;->aJ:Landroid/widget/TextView;

    const-string v1, "title_text"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 117
    iget-object v0, p0, Lcom/my/target/dk;->aI:Lcom/my/target/bu;

    const-string v1, "age_border"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 118
    iget-object v0, p0, Lcom/my/target/dk;->aO:Lcom/my/target/ca;

    const-string v1, "rating_view"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 119
    iget-object v0, p0, Lcom/my/target/dk;->aK:Landroid/widget/TextView;

    const-string v1, "votes_text"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 120
    iget-object v0, p0, Lcom/my/target/dk;->aL:Landroid/widget/TextView;

    const-string v1, "domain_text"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 121
    iget-object v0, p0, Lcom/my/target/dk;->aU:Landroid/widget/TextView;

    const-string v1, "description_text"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 122
    iget-object v0, p0, Lcom/my/target/dk;->aV:Landroid/widget/TextView;

    const-string v1, "disclaimer_text"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 125
    const-string v0, "standard_728x90"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 127
    const/16 v0, 0x14

    iput v0, p0, Lcom/my/target/dk;->aY:I

    .line 128
    const/16 v0, 0x18

    iput v0, p0, Lcom/my/target/dk;->bb:I

    .line 129
    const/16 v0, 0x20

    iput v0, p0, Lcom/my/target/dk;->ba:I

    .line 130
    const/16 v0, 0x10

    iput v0, p0, Lcom/my/target/dk;->bc:I

    .line 131
    const/16 v0, 0x18

    iput v0, p0, Lcom/my/target/dk;->bd:I

    .line 132
    const/16 v0, 0x18

    iput v0, p0, Lcom/my/target/dk;->be:I

    .line 133
    iget-object v0, p0, Lcom/my/target/dk;->P:Lcom/my/target/cm;

    const/16 v1, 0xb4

    invoke-virtual {v0, v1}, Lcom/my/target/cm;->n(I)I

    move-result v0

    iput v0, p0, Lcom/my/target/dk;->bf:I

    .line 134
    iget-object v0, p0, Lcom/my/target/dk;->P:Lcom/my/target/cm;

    const/16 v1, 0x10e

    invoke-virtual {v0, v1}, Lcom/my/target/cm;->n(I)I

    move-result v0

    iput v0, p0, Lcom/my/target/dk;->bg:I

    .line 148
    :goto_0
    const v0, 0x3f99999a    # 1.2f

    iput v0, p0, Lcom/my/target/dk;->aZ:F

    .line 1539
    iget-object v0, p0, Lcom/my/target/dk;->aP:Lcom/my/target/bx;

    sget v1, Lcom/my/target/dk;->aD:I

    invoke-virtual {v0, v1}, Lcom/my/target/bx;->setId(I)V

    .line 1541
    iget-object v0, p0, Lcom/my/target/dk;->aN:Landroid/widget/Button;

    sget v1, Lcom/my/target/dk;->aF:I

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setId(I)V

    .line 1542
    iget-object v0, p0, Lcom/my/target/dk;->aN:Landroid/widget/Button;

    iget-object v1, p0, Lcom/my/target/dk;->P:Lcom/my/target/cm;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iget-object v2, p0, Lcom/my/target/dk;->P:Lcom/my/target/cm;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/my/target/cm;->n(I)I

    move-result v2

    iget-object v3, p0, Lcom/my/target/dk;->P:Lcom/my/target/cm;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Lcom/my/target/cm;->n(I)I

    move-result v3

    iget-object v4, p0, Lcom/my/target/dk;->P:Lcom/my/target/cm;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Lcom/my/target/cm;->n(I)I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/Button;->setPadding(IIII)V

    .line 1545
    iget-object v0, p0, Lcom/my/target/dk;->aN:Landroid/widget/Button;

    iget v1, p0, Lcom/my/target/dk;->bf:I

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setMinimumWidth(I)V

    .line 1546
    iget-object v0, p0, Lcom/my/target/dk;->aN:Landroid/widget/Button;

    iget v1, p0, Lcom/my/target/dk;->ba:I

    int-to-float v1, v1

    invoke-virtual {v0, v8, v1}, Landroid/widget/Button;->setTextSize(IF)V

    .line 1547
    iget-object v0, p0, Lcom/my/target/dk;->aN:Landroid/widget/Button;

    iget v1, p0, Lcom/my/target/dk;->bg:I

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setMaxWidth(I)V

    .line 1548
    iget-object v0, p0, Lcom/my/target/dk;->aN:Landroid/widget/Button;

    invoke-virtual {v0, v7}, Landroid/widget/Button;->setLines(I)V

    .line 1549
    iget-object v0, p0, Lcom/my/target/dk;->aN:Landroid/widget/Button;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 1550
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {v0, v6, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1551
    const/16 v1, 0xb

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1552
    iget-object v1, p0, Lcom/my/target/dk;->aN:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1554
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {v0, v1, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1556
    sget v1, Lcom/my/target/dk;->aF:I

    invoke-virtual {v0, v10, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1557
    sget v1, Lcom/my/target/dk;->aD:I

    invoke-virtual {v0, v7, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1558
    iget-object v1, p0, Lcom/my/target/dk;->aS:Landroid/widget/ViewFlipper;

    invoke-virtual {v1, v0}, Landroid/widget/ViewFlipper;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1559
    iget-object v0, p0, Lcom/my/target/dk;->aS:Landroid/widget/ViewFlipper;

    invoke-static {}, Lcom/my/target/core/presenters/a;->B()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/my/target/do;->b(J)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setInAnimation(Landroid/view/animation/Animation;)V

    .line 1560
    iget-object v0, p0, Lcom/my/target/dk;->aS:Landroid/widget/ViewFlipper;

    invoke-static {}, Lcom/my/target/core/presenters/a;->B()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/my/target/do;->c(J)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setOutAnimation(Landroid/view/animation/Animation;)V

    .line 1562
    iget-object v0, p0, Lcom/my/target/dk;->aM:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 1564
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {v0, v1, v10}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1566
    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 1567
    iget-object v1, p0, Lcom/my/target/dk;->aQ:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1568
    iget-object v0, p0, Lcom/my/target/dk;->aQ:Landroid/widget/RelativeLayout;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setGravity(I)V

    .line 1570
    iget-object v0, p0, Lcom/my/target/dk;->aJ:Landroid/widget/TextView;

    sget v1, Lcom/my/target/dk;->aE:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setId(I)V

    .line 1571
    iget-object v0, p0, Lcom/my/target/dk;->aJ:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 1572
    iget-object v0, p0, Lcom/my/target/dk;->aJ:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setHorizontallyScrolling(Z)V

    .line 1573
    iget-object v0, p0, Lcom/my/target/dk;->aJ:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 1574
    iget-object v0, p0, Lcom/my/target/dk;->aJ:Landroid/widget/TextView;

    const/16 v1, 0x19

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxEms(I)V

    .line 1575
    iget-object v0, p0, Lcom/my/target/dk;->aJ:Landroid/widget/TextView;

    iget v1, p0, Lcom/my/target/dk;->bb:I

    int-to-float v1, v1

    invoke-virtual {v0, v8, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1576
    iget-object v0, p0, Lcom/my/target/dk;->aJ:Landroid/widget/TextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 1578
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1580
    iget-object v1, p0, Lcom/my/target/dk;->P:Lcom/my/target/cm;

    invoke-virtual {v1, v9}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 1581
    iget-object v1, p0, Lcom/my/target/dk;->P:Lcom/my/target/cm;

    invoke-virtual {v1, v9}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 1582
    iget-object v1, p0, Lcom/my/target/dk;->P:Lcom/my/target/cm;

    invoke-virtual {v1, v8}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 1583
    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1585
    iget-object v1, p0, Lcom/my/target/dk;->aJ:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1587
    iget-object v0, p0, Lcom/my/target/dk;->aI:Lcom/my/target/bu;

    sget v1, Lcom/my/target/dk;->aC:I

    invoke-virtual {v0, v1}, Lcom/my/target/bu;->setId(I)V

    .line 1588
    iget-object v0, p0, Lcom/my/target/dk;->aI:Lcom/my/target/bu;

    const v1, -0x777778

    invoke-virtual {v0, v7, v1}, Lcom/my/target/bu;->c(II)V

    .line 1589
    iget-object v0, p0, Lcom/my/target/dk;->aI:Lcom/my/target/bu;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Lcom/my/target/bu;->setGravity(I)V

    .line 1590
    iget-object v0, p0, Lcom/my/target/dk;->aI:Lcom/my/target/bu;

    iget v1, p0, Lcom/my/target/dk;->bc:I

    int-to-float v1, v1

    invoke-virtual {v0, v8, v1}, Lcom/my/target/bu;->setTextSize(IF)V

    .line 1591
    iget-object v0, p0, Lcom/my/target/dk;->aI:Lcom/my/target/bu;

    iget-object v1, p0, Lcom/my/target/dk;->P:Lcom/my/target/cm;

    invoke-virtual {v1, v8}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iget-object v2, p0, Lcom/my/target/dk;->P:Lcom/my/target/cm;

    invoke-virtual {v2, v9}, Lcom/my/target/cm;->n(I)I

    move-result v2

    invoke-virtual {v0, v1, v2, v10, v10}, Lcom/my/target/bu;->setPadding(IIII)V

    .line 1592
    iget-object v0, p0, Lcom/my/target/dk;->aI:Lcom/my/target/bu;

    invoke-virtual {v0, v7}, Lcom/my/target/bu;->setLines(I)V

    .line 1593
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1595
    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1596
    iget-object v1, p0, Lcom/my/target/dk;->P:Lcom/my/target/cm;

    invoke-virtual {v1, v8}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 1597
    sget v1, Lcom/my/target/dk;->aE:I

    invoke-virtual {v0, v7, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1598
    iget-object v1, p0, Lcom/my/target/dk;->aI:Lcom/my/target/bu;

    invoke-virtual {v1, v0}, Lcom/my/target/bu;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1600
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    invoke-direct {v0, v1, v10}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1602
    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 1603
    iget-object v1, p0, Lcom/my/target/dk;->aR:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1605
    iget-object v0, p0, Lcom/my/target/dk;->aO:Lcom/my/target/ca;

    sget v1, Lcom/my/target/dk;->aG:I

    invoke-virtual {v0, v1}, Lcom/my/target/ca;->setId(I)V

    .line 1606
    iget-object v0, p0, Lcom/my/target/dk;->aO:Lcom/my/target/ca;

    iget-object v1, p0, Lcom/my/target/dk;->P:Lcom/my/target/cm;

    iget v2, p0, Lcom/my/target/dk;->bd:I

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/my/target/ca;->setStarSize(I)V

    .line 1607
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1609
    iget-object v1, p0, Lcom/my/target/dk;->P:Lcom/my/target/cm;

    invoke-virtual {v1, v9}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 1610
    iget-object v1, p0, Lcom/my/target/dk;->P:Lcom/my/target/cm;

    invoke-virtual {v1, v9}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 1611
    iget-object v1, p0, Lcom/my/target/dk;->P:Lcom/my/target/cm;

    invoke-virtual {v1, v9}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 1612
    iget-object v1, p0, Lcom/my/target/dk;->P:Lcom/my/target/cm;

    invoke-virtual {v1, v8}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 1613
    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1614
    iget-object v1, p0, Lcom/my/target/dk;->aO:Lcom/my/target/ca;

    invoke-virtual {v1, v0}, Lcom/my/target/ca;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1616
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1618
    sget v1, Lcom/my/target/dk;->aG:I

    invoke-virtual {v0, v7, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1619
    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1620
    iget-object v1, p0, Lcom/my/target/dk;->aK:Landroid/widget/TextView;

    iget v2, p0, Lcom/my/target/dk;->be:I

    int-to-float v2, v2

    invoke-virtual {v1, v8, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1621
    iget-object v1, p0, Lcom/my/target/dk;->aK:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1623
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1626
    iget-object v1, p0, Lcom/my/target/dk;->P:Lcom/my/target/cm;

    invoke-virtual {v1, v9}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 1627
    iget-object v1, p0, Lcom/my/target/dk;->aL:Landroid/widget/TextView;

    iget v2, p0, Lcom/my/target/dk;->be:I

    int-to-float v2, v2

    invoke-virtual {v1, v8, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1628
    iget-object v1, p0, Lcom/my/target/dk;->aL:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1630
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1632
    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1634
    iget-object v0, p0, Lcom/my/target/dk;->aN:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 1636
    iget-object v0, p0, Lcom/my/target/dk;->aP:Lcom/my/target/bx;

    invoke-virtual {p0, v0}, Lcom/my/target/dk;->addView(Landroid/view/View;)V

    .line 1637
    iget-object v0, p0, Lcom/my/target/dk;->aT:Landroid/widget/FrameLayout;

    invoke-virtual {p0, v0}, Lcom/my/target/dk;->addView(Landroid/view/View;)V

    .line 1638
    iget-object v0, p0, Lcom/my/target/dk;->aN:Landroid/widget/Button;

    invoke-virtual {p0, v0}, Lcom/my/target/dk;->addView(Landroid/view/View;)V

    .line 1640
    iget-object v0, p0, Lcom/my/target/dk;->aR:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/my/target/dk;->aO:Lcom/my/target/ca;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1641
    iget-object v0, p0, Lcom/my/target/dk;->aR:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/my/target/dk;->aK:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1642
    iget-object v0, p0, Lcom/my/target/dk;->aR:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/my/target/dk;->aL:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1644
    iget-object v0, p0, Lcom/my/target/dk;->aQ:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/my/target/dk;->aJ:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1645
    iget-object v0, p0, Lcom/my/target/dk;->aQ:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/my/target/dk;->aI:Lcom/my/target/bu;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 1646
    iget-object v0, p0, Lcom/my/target/dk;->aM:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/my/target/dk;->aQ:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1647
    iget-object v0, p0, Lcom/my/target/dk;->aM:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/my/target/dk;->aR:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1649
    iget-object v0, p0, Lcom/my/target/dk;->aS:Landroid/widget/ViewFlipper;

    iget-object v1, p0, Lcom/my/target/dk;->aM:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->addView(Landroid/view/View;)V

    .line 1651
    iget-object v0, p0, Lcom/my/target/dk;->aS:Landroid/widget/ViewFlipper;

    invoke-virtual {p0, v0}, Lcom/my/target/dk;->addView(Landroid/view/View;)V

    .line 151
    new-instance v0, Lcom/my/target/dk$b;

    invoke-direct {v0, p0, v10}, Lcom/my/target/dk$b;-><init>(Lcom/my/target/dk;B)V

    iput-object v0, p0, Lcom/my/target/dk;->aW:Lcom/my/target/dk$b;

    .line 152
    return-void

    .line 138
    :cond_0
    const/16 v0, 0xf

    iput v0, p0, Lcom/my/target/dk;->aY:I

    .line 139
    const/16 v0, 0x10

    iput v0, p0, Lcom/my/target/dk;->bb:I

    .line 140
    const/16 v0, 0x14

    iput v0, p0, Lcom/my/target/dk;->ba:I

    .line 141
    const/16 v0, 0xa

    iput v0, p0, Lcom/my/target/dk;->bc:I

    .line 142
    const/16 v0, 0xc

    iput v0, p0, Lcom/my/target/dk;->bd:I

    .line 143
    const/16 v0, 0x10

    iput v0, p0, Lcom/my/target/dk;->be:I

    .line 145
    iget-object v0, p0, Lcom/my/target/dk;->P:Lcom/my/target/cm;

    const/16 v1, 0x32

    invoke-virtual {v0, v1}, Lcom/my/target/cm;->n(I)I

    move-result v0

    iput v0, p0, Lcom/my/target/dk;->bf:I

    .line 146
    iget-object v0, p0, Lcom/my/target/dk;->P:Lcom/my/target/cm;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Lcom/my/target/cm;->n(I)I

    move-result v0

    iput v0, p0, Lcom/my/target/dk;->bg:I

    goto/16 :goto_0
.end method

.method static synthetic a(Lcom/my/target/dk;)Lcom/my/target/dk$a;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/my/target/dk;->bh:Lcom/my/target/dk$a;

    return-object v0
.end method

.method static synthetic b(Lcom/my/target/dk;)Landroid/widget/ViewFlipper;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/my/target/dk;->aS:Landroid/widget/ViewFlipper;

    return-object v0
.end method

.method static synthetic c(Lcom/my/target/dk;)Lcom/my/target/dk$b;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/my/target/dk;->aW:Lcom/my/target/dk$b;

    return-object v0
.end method


# virtual methods
.method public final H()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 289
    iget-object v1, p0, Lcom/my/target/dk;->aS:Landroid/widget/ViewFlipper;

    invoke-virtual {v1}, Landroid/widget/ViewFlipper;->getChildCount()I

    move-result v1

    if-le v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final O()Landroid/view/View;
    .locals 0

    .prologue
    .line 261
    return-object p0
.end method

.method public final P()V
    .locals 2

    .prologue
    .line 295
    iget-object v0, p0, Lcom/my/target/dk;->aS:Landroid/widget/ViewFlipper;

    invoke-virtual {v0}, Landroid/widget/ViewFlipper;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 297
    iget-object v0, p0, Lcom/my/target/dk;->aS:Landroid/widget/ViewFlipper;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    .line 299
    :cond_0
    return-void
.end method

.method public final a(Lcom/my/target/af;ZLandroid/view/View$OnClickListener;)V
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ClickableViewAccessibility"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 307
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "Apply click area "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/my/target/af;->O()I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " to view"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 308
    if-nez p2, :cond_0

    iget-boolean v0, p1, Lcom/my/target/af;->cE:Z

    if-eqz v0, :cond_b

    :cond_0
    move v0, v2

    .line 309
    :goto_0
    iput-object p3, p0, Lcom/my/target/dk;->a_:Landroid/view/View$OnClickListener;

    .line 310
    invoke-virtual {p0, p0}, Lcom/my/target/dk;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 312
    iget-object v3, p0, Lcom/my/target/dk;->aJ:Landroid/widget/TextView;

    invoke-virtual {v3, p0}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 313
    iget-object v3, p0, Lcom/my/target/dk;->aP:Lcom/my/target/bx;

    invoke-virtual {v3, p0}, Lcom/my/target/bx;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 314
    iget-object v3, p0, Lcom/my/target/dk;->aO:Lcom/my/target/ca;

    invoke-virtual {v3, p0}, Lcom/my/target/ca;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 315
    iget-object v3, p0, Lcom/my/target/dk;->aK:Landroid/widget/TextView;

    invoke-virtual {v3, p0}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 316
    iget-object v3, p0, Lcom/my/target/dk;->aL:Landroid/widget/TextView;

    invoke-virtual {v3, p0}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 317
    iget-object v3, p0, Lcom/my/target/dk;->aI:Lcom/my/target/bu;

    invoke-virtual {v3, p0}, Lcom/my/target/bu;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 318
    iget-object v3, p0, Lcom/my/target/dk;->aU:Landroid/widget/TextView;

    invoke-virtual {v3, p0}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 320
    iget-object v4, p0, Lcom/my/target/dk;->aX:Ljava/util/HashMap;

    iget-object v5, p0, Lcom/my/target/dk;->aJ:Landroid/widget/TextView;

    iget-boolean v3, p1, Lcom/my/target/af;->cs:Z

    if-nez v3, :cond_1

    if-eqz v0, :cond_c

    :cond_1
    move v3, v2

    :goto_1
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 321
    iget-object v4, p0, Lcom/my/target/dk;->aX:Ljava/util/HashMap;

    iget-object v5, p0, Lcom/my/target/dk;->aP:Lcom/my/target/bx;

    iget-boolean v3, p1, Lcom/my/target/af;->cu:Z

    if-nez v3, :cond_2

    if-eqz v0, :cond_d

    :cond_2
    move v3, v2

    :goto_2
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 322
    iget-object v4, p0, Lcom/my/target/dk;->aX:Ljava/util/HashMap;

    iget-object v5, p0, Lcom/my/target/dk;->aO:Lcom/my/target/ca;

    iget-boolean v3, p1, Lcom/my/target/af;->cw:Z

    if-nez v3, :cond_3

    if-eqz v0, :cond_e

    :cond_3
    move v3, v2

    :goto_3
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 323
    iget-object v4, p0, Lcom/my/target/dk;->aX:Ljava/util/HashMap;

    iget-object v5, p0, Lcom/my/target/dk;->aK:Landroid/widget/TextView;

    iget-boolean v3, p1, Lcom/my/target/af;->cx:Z

    if-nez v3, :cond_4

    if-eqz v0, :cond_f

    :cond_4
    move v3, v2

    :goto_4
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 324
    iget-object v4, p0, Lcom/my/target/dk;->aX:Ljava/util/HashMap;

    iget-object v5, p0, Lcom/my/target/dk;->aI:Lcom/my/target/bu;

    iget-boolean v3, p1, Lcom/my/target/af;->cz:Z

    if-nez v3, :cond_5

    if-eqz v0, :cond_10

    :cond_5
    move v3, v2

    :goto_5
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 325
    iget-object v4, p0, Lcom/my/target/dk;->aX:Ljava/util/HashMap;

    iget-object v5, p0, Lcom/my/target/dk;->aL:Landroid/widget/TextView;

    iget-boolean v3, p1, Lcom/my/target/af;->cB:Z

    if-nez v3, :cond_6

    if-eqz v0, :cond_11

    :cond_6
    move v3, v2

    :goto_6
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 326
    iget-object v4, p0, Lcom/my/target/dk;->aX:Ljava/util/HashMap;

    iget-boolean v3, p1, Lcom/my/target/af;->cD:Z

    if-nez v3, :cond_7

    if-eqz v0, :cond_12

    :cond_7
    move v3, v2

    :goto_7
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v4, p0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 327
    iget-boolean v3, p1, Lcom/my/target/af;->cD:Z

    if-nez v3, :cond_8

    if-eqz v0, :cond_13

    :cond_8
    move v3, v2

    :goto_8
    iput-boolean v3, p0, Lcom/my/target/dk;->bl:Z

    .line 328
    iget-boolean v3, p1, Lcom/my/target/af;->ct:Z

    if-nez v3, :cond_9

    if-eqz v0, :cond_14

    :cond_9
    move v3, v2

    :goto_9
    iput-boolean v3, p0, Lcom/my/target/dk;->bm:Z

    .line 330
    iget-boolean v3, p1, Lcom/my/target/af;->cy:Z

    if-nez v3, :cond_a

    if-eqz v0, :cond_15

    .line 332
    :cond_a
    iget-object v0, p0, Lcom/my/target/dk;->aN:Landroid/widget/Button;

    invoke-virtual {v0, p3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 333
    iget-object v0, p0, Lcom/my/target/dk;->aN:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 340
    :goto_a
    return-void

    :cond_b
    move v0, v1

    .line 308
    goto/16 :goto_0

    :cond_c
    move v3, v1

    .line 320
    goto/16 :goto_1

    :cond_d
    move v3, v1

    .line 321
    goto/16 :goto_2

    :cond_e
    move v3, v1

    .line 322
    goto :goto_3

    :cond_f
    move v3, v1

    .line 323
    goto :goto_4

    :cond_10
    move v3, v1

    .line 324
    goto :goto_5

    :cond_11
    move v3, v1

    .line 325
    goto :goto_6

    :cond_12
    move v3, v1

    .line 326
    goto :goto_7

    :cond_13
    move v3, v1

    .line 327
    goto :goto_8

    :cond_14
    move v3, v1

    .line 328
    goto :goto_9

    .line 337
    :cond_15
    iget-object v0, p0, Lcom/my/target/dk;->aN:Landroid/widget/Button;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 338
    iget-object v0, p0, Lcom/my/target/dk;->aN:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_a
.end method

.method public final b(II)V
    .locals 2

    .prologue
    .line 267
    iput p1, p0, Lcom/my/target/dk;->backgroundColor:I

    .line 268
    iput p2, p0, Lcom/my/target/dk;->bi:I

    .line 270
    iget-object v0, p0, Lcom/my/target/dk;->aP:Lcom/my/target/bx;

    iget v1, p0, Lcom/my/target/dk;->backgroundColor:I

    invoke-virtual {v0, v1}, Lcom/my/target/bx;->setBackgroundColor(I)V

    .line 271
    iget v0, p0, Lcom/my/target/dk;->backgroundColor:I

    invoke-virtual {p0, v0}, Lcom/my/target/dk;->setBackgroundColor(I)V

    .line 272
    return-void
.end method

.method public final b(Lcom/my/target/ah;)V
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v8, -0x1

    const/16 v7, 0x8

    const/4 v6, 0x0

    .line 168
    invoke-virtual {p1}, Lcom/my/target/ah;->getType()Ljava/lang/String;

    move-result-object v0

    .line 1676
    const-string v1, "teaser"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1678
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/my/target/dk;->bk:Z

    .line 1679
    iget v0, p0, Lcom/my/target/dk;->backgroundColor:I

    iget v1, p0, Lcom/my/target/dk;->bi:I

    invoke-static {p0, v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;II)V

    .line 1680
    iget-object v0, p0, Lcom/my/target/dk;->aS:Landroid/widget/ViewFlipper;

    iget v1, p0, Lcom/my/target/dk;->backgroundColor:I

    iget v2, p0, Lcom/my/target/dk;->bi:I

    invoke-static {v0, v1, v2}, Lcom/my/target/cm;->a(Landroid/view/View;II)V

    .line 1681
    iget-object v0, p0, Lcom/my/target/dk;->aT:Landroid/widget/FrameLayout;

    iget v1, p0, Lcom/my/target/dk;->bi:I

    invoke-static {v1}, Lcom/my/target/cm;->m(I)I

    move-result v1

    invoke-static {v0, v6, v1}, Lcom/my/target/cm;->a(Landroid/view/View;II)V

    .line 1682
    iget-object v0, p0, Lcom/my/target/dk;->aN:Landroid/widget/Button;

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setVisibility(I)V

    .line 1683
    iget-object v0, p0, Lcom/my/target/dk;->aS:Landroid/widget/ViewFlipper;

    invoke-virtual {v0, v6}, Landroid/widget/ViewFlipper;->setVisibility(I)V

    .line 1685
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x2

    invoke-direct {v0, v1, v8}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1686
    iget-object v1, p0, Lcom/my/target/dk;->aP:Lcom/my/target/bx;

    iget-object v2, p0, Lcom/my/target/dk;->P:Lcom/my/target/cm;

    invoke-virtual {v2, v9}, Lcom/my/target/cm;->n(I)I

    move-result v2

    iget-object v3, p0, Lcom/my/target/dk;->P:Lcom/my/target/cm;

    invoke-virtual {v3, v9}, Lcom/my/target/cm;->n(I)I

    move-result v3

    iget-object v4, p0, Lcom/my/target/dk;->P:Lcom/my/target/cm;

    invoke-virtual {v4, v9}, Lcom/my/target/cm;->n(I)I

    move-result v4

    iget-object v5, p0, Lcom/my/target/dk;->P:Lcom/my/target/cm;

    invoke-virtual {v5, v9}, Lcom/my/target/cm;->n(I)I

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/my/target/bx;->setPadding(IIII)V

    .line 1687
    iget-object v1, p0, Lcom/my/target/dk;->aP:Lcom/my/target/bx;

    invoke-virtual {v1, v0}, Lcom/my/target/bx;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1688
    iget-object v1, p0, Lcom/my/target/dk;->aP:Lcom/my/target/bx;

    iget v2, p0, Lcom/my/target/dk;->bg:I

    invoke-virtual {v1, v2}, Lcom/my/target/bx;->setMaxWidth(I)V

    .line 1690
    iget-object v1, p0, Lcom/my/target/dk;->aT:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1692
    const/16 v1, 0x9

    invoke-virtual {v0, v1, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1693
    const/16 v1, 0xf

    invoke-virtual {v0, v1, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1694
    iget-object v1, p0, Lcom/my/target/dk;->aP:Lcom/my/target/bx;

    invoke-virtual {v1, v0}, Lcom/my/target/bx;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 169
    :cond_0
    :goto_0
    const-string v0, "store"

    invoke-virtual {p1}, Lcom/my/target/ah;->getNavigationType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 171
    invoke-virtual {p1}, Lcom/my/target/ah;->getRating()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_3

    .line 173
    iget-object v0, p0, Lcom/my/target/dk;->aO:Lcom/my/target/ca;

    invoke-virtual {v0, v6}, Lcom/my/target/ca;->setVisibility(I)V

    .line 174
    invoke-virtual {p1}, Lcom/my/target/ah;->getVotes()I

    move-result v0

    if-lez v0, :cond_2

    .line 176
    iget-object v0, p0, Lcom/my/target/dk;->aK:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 188
    :goto_1
    iget-object v0, p0, Lcom/my/target/dk;->aL:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 196
    :goto_2
    return-void

    .line 1696
    :cond_1
    const-string v1, "banner"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1698
    iput-boolean v6, p0, Lcom/my/target/dk;->bk:Z

    .line 1699
    iget v0, p0, Lcom/my/target/dk;->bi:I

    invoke-static {v0}, Lcom/my/target/cm;->m(I)I

    move-result v0

    invoke-static {p0, v6, v0}, Lcom/my/target/cm;->a(Landroid/view/View;II)V

    .line 1700
    iget-object v0, p0, Lcom/my/target/dk;->aN:Landroid/widget/Button;

    invoke-virtual {v0, v7}, Landroid/widget/Button;->setVisibility(I)V

    .line 1701
    iget-object v0, p0, Lcom/my/target/dk;->aS:Landroid/widget/ViewFlipper;

    invoke-virtual {v0, v7}, Landroid/widget/ViewFlipper;->setVisibility(I)V

    .line 1703
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v8, v8}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1704
    const/16 v1, 0xe

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1705
    iget-object v1, p0, Lcom/my/target/dk;->aP:Lcom/my/target/bx;

    invoke-virtual {v1, v6, v6, v6, v6}, Lcom/my/target/bx;->setPadding(IIII)V

    .line 1706
    iget-object v1, p0, Lcom/my/target/dk;->aP:Lcom/my/target/bx;

    invoke-virtual {v1, v0}, Lcom/my/target/bx;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1707
    iget-object v1, p0, Lcom/my/target/dk;->aP:Lcom/my/target/bx;

    invoke-virtual {v1, v6}, Lcom/my/target/bx;->setMaxWidth(I)V

    .line 1708
    iget-object v1, p0, Lcom/my/target/dk;->aT:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 180
    :cond_2
    iget-object v0, p0, Lcom/my/target/dk;->aK:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 185
    :cond_3
    iget-object v0, p0, Lcom/my/target/dk;->aO:Lcom/my/target/ca;

    invoke-virtual {v0, v7}, Lcom/my/target/ca;->setVisibility(I)V

    .line 186
    iget-object v0, p0, Lcom/my/target/dk;->aK:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 192
    :cond_4
    iget-object v0, p0, Lcom/my/target/dk;->aL:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 193
    iget-object v0, p0, Lcom/my/target/dk;->aO:Lcom/my/target/ca;

    invoke-virtual {v0, v7}, Lcom/my/target/ca;->setVisibility(I)V

    .line 194
    iget-object v0, p0, Lcom/my/target/dk;->aK:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2
.end method

.method public final getAgeRestrictionsView()Lcom/my/target/bu;
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lcom/my/target/dk;->aI:Lcom/my/target/bu;

    return-object v0
.end method

.method public final getBannerImage()Lcom/my/target/bx;
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lcom/my/target/dk;->aP:Lcom/my/target/bx;

    return-object v0
.end method

.method public final getCtaButton()Landroid/widget/Button;
    .locals 1

    .prologue
    .line 231
    iget-object v0, p0, Lcom/my/target/dk;->aN:Landroid/widget/Button;

    return-object v0
.end method

.method public final getDescriptionTextView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 243
    iget-object v0, p0, Lcom/my/target/dk;->aU:Landroid/widget/TextView;

    return-object v0
.end method

.method public final getDisclaimerTextView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 249
    iget-object v0, p0, Lcom/my/target/dk;->aV:Landroid/widget/TextView;

    return-object v0
.end method

.method public final getDomainTextView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/my/target/dk;->aL:Landroid/widget/TextView;

    return-object v0
.end method

.method public final getIconImage()Lcom/my/target/bx;
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/my/target/dk;->aP:Lcom/my/target/bx;

    return-object v0
.end method

.method public final getMainImage()Lcom/my/target/bx;
    .locals 1

    .prologue
    .line 213
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getRatingTextView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Lcom/my/target/dk;->aK:Landroid/widget/TextView;

    return-object v0
.end method

.method public final getStarsRatingView()Lcom/my/target/ca;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/my/target/dk;->aO:Lcom/my/target/ca;

    return-object v0
.end method

.method public final getTitleTextView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Lcom/my/target/dk;->aJ:Landroid/widget/TextView;

    return-object v0
.end method

.method protected final onMeasure(II)V
    .locals 13
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/4 v12, 0x0

    const/4 v11, -0x1

    const/4 v10, 0x0

    const/16 v9, 0x10

    const/4 v8, 0x1

    .line 390
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 391
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 393
    iget-object v1, p0, Lcom/my/target/dk;->aP:Lcom/my/target/bx;

    invoke-virtual {v1}, Lcom/my/target/bx;->getMeasuredWidth()I

    move-result v1

    .line 394
    iget-object v3, p0, Lcom/my/target/dk;->aN:Landroid/widget/Button;

    invoke-virtual {v3}, Landroid/widget/Button;->getMeasuredWidth()I

    move-result v3

    .line 395
    iget-boolean v4, p0, Lcom/my/target/dk;->bk:Z

    if-eqz v4, :cond_8

    if-eqz v0, :cond_8

    iget v4, p0, Lcom/my/target/dk;->bj:I

    if-eq v4, v0, :cond_8

    if-lez v1, :cond_8

    if-lez v3, :cond_8

    .line 397
    iget v4, p0, Lcom/my/target/dk;->bj:I

    if-eqz v4, :cond_1

    .line 7662
    iget-object v4, p0, Lcom/my/target/dk;->aS:Landroid/widget/ViewFlipper;

    invoke-virtual {v4}, Landroid/widget/ViewFlipper;->getChildCount()I

    move-result v4

    if-le v4, v8, :cond_0

    .line 7664
    iget-object v4, p0, Lcom/my/target/dk;->aS:Landroid/widget/ViewFlipper;

    invoke-virtual {v4, v12}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    .line 7667
    :cond_0
    :goto_0
    iget-object v4, p0, Lcom/my/target/dk;->aS:Landroid/widget/ViewFlipper;

    invoke-virtual {v4}, Landroid/widget/ViewFlipper;->getChildCount()I

    move-result v4

    if-le v4, v8, :cond_1

    .line 7669
    iget-object v4, p0, Lcom/my/target/dk;->aS:Landroid/widget/ViewFlipper;

    iget-object v5, p0, Lcom/my/target/dk;->aS:Landroid/widget/ViewFlipper;

    invoke-virtual {v5}, Landroid/widget/ViewFlipper;->getChildCount()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v4, v5}, Landroid/widget/ViewFlipper;->removeViewAt(I)V

    goto :goto_0

    .line 401
    :cond_1
    iput v0, p0, Lcom/my/target/dk;->bj:I

    .line 402
    iget-object v4, p0, Lcom/my/target/dk;->aS:Landroid/widget/ViewFlipper;

    invoke-virtual {v4}, Landroid/widget/ViewFlipper;->getPaddingRight()I

    move-result v4

    sub-int/2addr v0, v4

    iget-object v4, p0, Lcom/my/target/dk;->aS:Landroid/widget/ViewFlipper;

    .line 403
    invoke-virtual {v4}, Landroid/widget/ViewFlipper;->getPaddingLeft()I

    move-result v4

    sub-int/2addr v0, v4

    sub-int/2addr v0, v1

    sub-int v3, v0, v3

    .line 406
    iget-object v0, p0, Lcom/my/target/dk;->aI:Lcom/my/target/bu;

    invoke-virtual {v0}, Lcom/my/target/bu;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 408
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 410
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    iget-object v1, p0, Lcom/my/target/dk;->P:Lcom/my/target/cm;

    const/16 v4, 0xa

    invoke-virtual {v1, v4}, Lcom/my/target/cm;->n(I)I

    move-result v1

    mul-int/2addr v0, v1

    sub-int v0, v3, v0

    iget-object v1, p0, Lcom/my/target/dk;->P:Lcom/my/target/cm;

    const/16 v4, 0xa

    invoke-virtual {v1, v4}, Lcom/my/target/cm;->n(I)I

    move-result v1

    sub-int/2addr v0, v1

    .line 411
    iget-object v1, p0, Lcom/my/target/dk;->aJ:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setMaxWidth(I)V

    .line 8455
    :cond_2
    const/4 v1, 0x0

    .line 8457
    iget-object v0, p0, Lcom/my/target/dk;->aU:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 8458
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 8460
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    .line 8461
    iget-object v5, p0, Lcom/my/target/dk;->P:Lcom/my/target/cm;

    iget v6, p0, Lcom/my/target/dk;->aY:I

    invoke-virtual {v5, v6}, Lcom/my/target/cm;->o(I)I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 8462
    iget-object v5, p0, Lcom/my/target/dk;->aU:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 8464
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    int-to-float v5, v3

    invoke-static {v0, v5, v4}, Lcom/my/target/dl;->a(Ljava/lang/String;FLandroid/graphics/Paint;)Ljava/util/List;

    move-result-object v0

    .line 8466
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 8468
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v5

    if-le v5, v8, :cond_4

    .line 8470
    :cond_3
    new-instance v5, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v5, v11, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 8471
    new-instance v1, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/my/target/dk;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v1, v6}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 8472
    invoke-virtual {v1, v9}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 8473
    invoke-virtual {v1, v8}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 8474
    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 8475
    invoke-virtual {v1, v12}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 8476
    iget-object v5, p0, Lcom/my/target/dk;->aS:Landroid/widget/ViewFlipper;

    invoke-virtual {v5, v1}, Landroid/widget/ViewFlipper;->addView(Landroid/view/View;)V

    .line 8479
    :cond_4
    new-instance v5, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/my/target/dk;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 8480
    iget-object v6, p0, Lcom/my/target/dk;->aX:Ljava/util/HashMap;

    iget-boolean v7, p0, Lcom/my/target/dk;->bm:Z

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v6, v5, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 8481
    invoke-virtual {v5, p0}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 8482
    iget-object v6, p0, Lcom/my/target/dk;->aX:Ljava/util/HashMap;

    iget-boolean v7, p0, Lcom/my/target/dk;->bl:Z

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v6, v1, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 8483
    invoke-virtual {v1, p0}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 8485
    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setGravity(I)V

    .line 8486
    const/4 v6, 0x2

    iget v7, p0, Lcom/my/target/dk;->aY:I

    int-to-float v7, v7

    invoke-virtual {v5, v6, v7}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 8488
    iget v6, p0, Lcom/my/target/dk;->aZ:F

    invoke-virtual {v5, v10, v6}, Landroid/widget/TextView;->setLineSpacing(FF)V

    .line 8489
    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setLines(I)V

    .line 8490
    iget-object v6, p0, Lcom/my/target/dk;->aU:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getCurrentTextColor()I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTextColor(I)V

    .line 8491
    iget-object v6, p0, Lcom/my/target/dk;->aU:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 8492
    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 8493
    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_1

    .line 8497
    :cond_5
    iget-object v0, p0, Lcom/my/target/dk;->aV:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 8498
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_8

    .line 8500
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    .line 8501
    iget-object v5, p0, Lcom/my/target/dk;->P:Lcom/my/target/cm;

    iget v6, p0, Lcom/my/target/dk;->aY:I

    invoke-virtual {v5, v6}, Lcom/my/target/cm;->o(I)I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 8503
    iget-object v5, p0, Lcom/my/target/dk;->aV:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 8506
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    int-to-float v3, v3

    invoke-static {v0, v3, v4}, Lcom/my/target/dl;->a(Ljava/lang/String;FLandroid/graphics/Paint;)Ljava/util/List;

    move-result-object v0

    .line 8508
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 8510
    if-eqz v1, :cond_6

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v4

    if-le v4, v8, :cond_7

    .line 8512
    :cond_6
    new-instance v4, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v4, v11, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 8513
    new-instance v1, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/my/target/dk;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v1, v5}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 8514
    invoke-virtual {v1, v9}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 8515
    invoke-virtual {v1, v8}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 8516
    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 8517
    iget-object v4, p0, Lcom/my/target/dk;->aS:Landroid/widget/ViewFlipper;

    invoke-virtual {v4, v1}, Landroid/widget/ViewFlipper;->addView(Landroid/view/View;)V

    .line 8520
    :cond_7
    new-instance v4, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/my/target/dk;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 8521
    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setGravity(I)V

    .line 8522
    iget v5, p0, Lcom/my/target/dk;->aZ:F

    invoke-virtual {v4, v10, v5}, Landroid/widget/TextView;->setLineSpacing(FF)V

    .line 8523
    const/4 v5, 0x2

    iget v6, p0, Lcom/my/target/dk;->aY:I

    int-to-float v6, v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 8525
    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setLines(I)V

    .line 8526
    iget-object v5, p0, Lcom/my/target/dk;->aV:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getCurrentTextColor()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 8527
    iget-object v5, p0, Lcom/my/target/dk;->aV:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 8529
    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 8530
    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_2

    .line 415
    :cond_8
    invoke-super {p0, p1, p2}, Landroid/widget/RelativeLayout;->onMeasure(II)V

    .line 416
    return-void
.end method

.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 345
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    move v0, v1

    .line 384
    :cond_1
    :goto_1
    return v0

    .line 348
    :pswitch_1
    iget-object v2, p0, Lcom/my/target/dk;->aX:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 352
    iget-object v0, p0, Lcom/my/target/dk;->aX:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 354
    goto :goto_1

    .line 356
    :cond_2
    iget v0, p0, Lcom/my/target/dk;->bi:I

    invoke-virtual {p0, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 357
    invoke-virtual {p0}, Lcom/my/target/dk;->getIconImage()Lcom/my/target/bx;

    move-result-object v0

    iget v2, p0, Lcom/my/target/dk;->backgroundColor:I

    invoke-virtual {v0, v2}, Lcom/my/target/bx;->setBackgroundColor(I)V

    goto :goto_0

    .line 360
    :pswitch_2
    iget v2, p0, Lcom/my/target/dk;->backgroundColor:I

    invoke-virtual {p0, v2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 361
    invoke-virtual {p0}, Lcom/my/target/dk;->getIconImage()Lcom/my/target/bx;

    move-result-object v2

    iget v3, p0, Lcom/my/target/dk;->backgroundColor:I

    invoke-virtual {v2, v3}, Lcom/my/target/bx;->setBackgroundColor(I)V

    .line 363
    iget-object v2, p0, Lcom/my/target/dk;->aX:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 367
    iget-object v0, p0, Lcom/my/target/dk;->aX:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 369
    goto :goto_1

    .line 371
    :cond_3
    invoke-virtual {p0}, Landroid/view/View;->performClick()Z

    .line 372
    iget-object v0, p0, Lcom/my/target/dk;->a_:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    .line 374
    iget-object v0, p0, Lcom/my/target/dk;->a_:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    goto :goto_0

    .line 378
    :pswitch_3
    iget v0, p0, Lcom/my/target/dk;->backgroundColor:I

    invoke-virtual {p0, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 379
    invoke-virtual {p0}, Lcom/my/target/dk;->getIconImage()Lcom/my/target/bx;

    move-result-object v0

    iget v2, p0, Lcom/my/target/dk;->backgroundColor:I

    invoke-virtual {v0, v2}, Lcom/my/target/bx;->setBackgroundColor(I)V

    goto :goto_0

    .line 345
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public final setAfterLastSlideListener(Lcom/my/target/dk$a;)V
    .locals 0
    .param p1, "afterLastSlideListener"    # Lcom/my/target/dk$a;

    .prologue
    .line 156
    iput-object p1, p0, Lcom/my/target/dk;->bh:Lcom/my/target/dk$a;

    .line 157
    return-void
.end method

.method public final start()V
    .locals 4

    .prologue
    .line 277
    .line 2656
    iget-object v0, p0, Lcom/my/target/dk;->aW:Lcom/my/target/dk$b;

    invoke-virtual {p0, v0}, Lcom/my/target/dk;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 2657
    iget-object v0, p0, Lcom/my/target/dk;->aW:Lcom/my/target/dk$b;

    sget-wide v2, Lcom/my/target/dk;->aH:J

    invoke-virtual {p0, v0, v2, v3}, Lcom/my/target/dk;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 278
    return-void
.end method

.method public final stop()V
    .locals 1

    .prologue
    .line 283
    .line 3420
    iget-object v0, p0, Lcom/my/target/dk;->aS:Landroid/widget/ViewFlipper;

    invoke-virtual {v0}, Landroid/widget/ViewFlipper;->stopFlipping()V

    .line 3421
    iget-object v0, p0, Lcom/my/target/dk;->aW:Lcom/my/target/dk$b;

    invoke-virtual {p0, v0}, Lcom/my/target/dk;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 284
    return-void
.end method
