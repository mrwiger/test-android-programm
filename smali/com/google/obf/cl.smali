.class public final Lcom/google/obf/cl;
.super Ljava/lang/Object;
.source "IMASDK"

# interfaces
.implements Lcom/google/obf/aj;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/obf/cl$b;,
        Lcom/google/obf/cl$c;,
        Lcom/google/obf/cl$a;,
        Lcom/google/obf/cl$d;
    }
.end annotation


# static fields
.field private static final d:J

.field private static final e:J

.field private static final f:J


# instance fields
.field final a:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/obf/cl$d;",
            ">;"
        }
    .end annotation
.end field

.field final b:Landroid/util/SparseBooleanArray;

.field c:Lcom/google/obf/cf;

.field private final g:Lcom/google/obf/cj;

.field private final h:I

.field private final i:Lcom/google/obf/dw;

.field private final j:Lcom/google/obf/dv;

.field private final k:Landroid/util/SparseIntArray;

.field private l:Lcom/google/obf/al;

.field private m:Z

.field private n:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 107
    const-string v0, "AC-3"

    invoke-static {v0}, Lcom/google/obf/ea;->c(Ljava/lang/String;)I

    move-result v0

    int-to-long v0, v0

    sput-wide v0, Lcom/google/obf/cl;->d:J

    .line 108
    const-string v0, "EAC3"

    invoke-static {v0}, Lcom/google/obf/ea;->c(Ljava/lang/String;)I

    move-result v0

    int-to-long v0, v0

    sput-wide v0, Lcom/google/obf/cl;->e:J

    .line 109
    const-string v0, "HEVC"

    invoke-static {v0}, Lcom/google/obf/ea;->c(Ljava/lang/String;)I

    move-result v0

    int-to-long v0, v0

    sput-wide v0, Lcom/google/obf/cl;->f:J

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    .line 1
    new-instance v0, Lcom/google/obf/cj;

    const-wide/16 v2, 0x0

    invoke-direct {v0, v2, v3}, Lcom/google/obf/cj;-><init>(J)V

    invoke-direct {p0, v0}, Lcom/google/obf/cl;-><init>(Lcom/google/obf/cj;)V

    .line 2
    return-void
.end method

.method public constructor <init>(Lcom/google/obf/cj;)V
    .locals 1

    .prologue
    .line 3
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/obf/cl;-><init>(Lcom/google/obf/cj;I)V

    .line 4
    return-void
.end method

.method public constructor <init>(Lcom/google/obf/cj;I)V
    .locals 2

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6
    iput-object p1, p0, Lcom/google/obf/cl;->g:Lcom/google/obf/cj;

    .line 7
    iput p2, p0, Lcom/google/obf/cl;->h:I

    .line 8
    new-instance v0, Lcom/google/obf/dw;

    const/16 v1, 0x3ac

    invoke-direct {v0, v1}, Lcom/google/obf/dw;-><init>(I)V

    iput-object v0, p0, Lcom/google/obf/cl;->i:Lcom/google/obf/dw;

    .line 9
    new-instance v0, Lcom/google/obf/dv;

    const/4 v1, 0x3

    new-array v1, v1, [B

    invoke-direct {v0, v1}, Lcom/google/obf/dv;-><init>([B)V

    iput-object v0, p0, Lcom/google/obf/cl;->j:Lcom/google/obf/dv;

    .line 10
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/obf/cl;->a:Landroid/util/SparseArray;

    .line 11
    new-instance v0, Landroid/util/SparseBooleanArray;

    invoke-direct {v0}, Landroid/util/SparseBooleanArray;-><init>()V

    iput-object v0, p0, Lcom/google/obf/cl;->b:Landroid/util/SparseBooleanArray;

    .line 12
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, Lcom/google/obf/cl;->k:Landroid/util/SparseIntArray;

    .line 13
    invoke-direct {p0}, Lcom/google/obf/cl;->f()V

    .line 14
    return-void
.end method

.method static synthetic a(Lcom/google/obf/cl;)I
    .locals 1

    .prologue
    .line 99
    iget v0, p0, Lcom/google/obf/cl;->h:I

    return v0
.end method

.method static synthetic a()J
    .locals 2

    .prologue
    .line 104
    sget-wide v0, Lcom/google/obf/cl;->d:J

    return-wide v0
.end method

.method static synthetic a(Lcom/google/obf/cl;Z)Z
    .locals 0

    .prologue
    .line 103
    iput-boolean p1, p0, Lcom/google/obf/cl;->m:Z

    return p1
.end method

.method static synthetic b(Lcom/google/obf/cl;)I
    .locals 2

    .prologue
    .line 100
    iget v0, p0, Lcom/google/obf/cl;->n:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/google/obf/cl;->n:I

    return v0
.end method

.method static synthetic c(Lcom/google/obf/cl;)Lcom/google/obf/cj;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/obf/cl;->g:Lcom/google/obf/cj;

    return-object v0
.end method

.method static synthetic d()J
    .locals 2

    .prologue
    .line 105
    sget-wide v0, Lcom/google/obf/cl;->e:J

    return-wide v0
.end method

.method static synthetic d(Lcom/google/obf/cl;)Z
    .locals 1

    .prologue
    .line 102
    iget-boolean v0, p0, Lcom/google/obf/cl;->m:Z

    return v0
.end method

.method static synthetic e()J
    .locals 2

    .prologue
    .line 106
    sget-wide v0, Lcom/google/obf/cl;->f:J

    return-wide v0
.end method

.method private f()V
    .locals 3

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/obf/cl;->b:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->clear()V

    .line 94
    iget-object v0, p0, Lcom/google/obf/cl;->a:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 95
    iget-object v0, p0, Lcom/google/obf/cl;->a:Landroid/util/SparseArray;

    const/4 v1, 0x0

    new-instance v2, Lcom/google/obf/cl$a;

    invoke-direct {v2, p0}, Lcom/google/obf/cl$a;-><init>(Lcom/google/obf/cl;)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 96
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/obf/cl;->c:Lcom/google/obf/cf;

    .line 97
    const/16 v0, 0x2000

    iput v0, p0, Lcom/google/obf/cl;->n:I

    .line 98
    return-void
.end method


# virtual methods
.method public a(Lcom/google/obf/ak;Lcom/google/obf/ao;)I
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/16 v7, 0xbc

    const/4 v0, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 36
    iget-object v3, p0, Lcom/google/obf/cl;->i:Lcom/google/obf/dw;

    iget-object v3, v3, Lcom/google/obf/dw;->a:[B

    .line 37
    iget-object v4, p0, Lcom/google/obf/cl;->i:Lcom/google/obf/dw;

    invoke-virtual {v4}, Lcom/google/obf/dw;->d()I

    move-result v4

    rsub-int v4, v4, 0x3ac

    if-ge v4, v7, :cond_1

    .line 38
    iget-object v4, p0, Lcom/google/obf/cl;->i:Lcom/google/obf/dw;

    invoke-virtual {v4}, Lcom/google/obf/dw;->b()I

    move-result v4

    .line 39
    if-lez v4, :cond_0

    .line 40
    iget-object v5, p0, Lcom/google/obf/cl;->i:Lcom/google/obf/dw;

    invoke-virtual {v5}, Lcom/google/obf/dw;->d()I

    move-result v5

    invoke-static {v3, v5, v3, v2, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 41
    :cond_0
    iget-object v5, p0, Lcom/google/obf/cl;->i:Lcom/google/obf/dw;

    invoke-virtual {v5, v3, v4}, Lcom/google/obf/dw;->a([BI)V

    .line 42
    :cond_1
    :goto_0
    iget-object v4, p0, Lcom/google/obf/cl;->i:Lcom/google/obf/dw;

    invoke-virtual {v4}, Lcom/google/obf/dw;->b()I

    move-result v4

    if-ge v4, v7, :cond_4

    .line 43
    iget-object v4, p0, Lcom/google/obf/cl;->i:Lcom/google/obf/dw;

    invoke-virtual {v4}, Lcom/google/obf/dw;->c()I

    move-result v4

    .line 44
    rsub-int v5, v4, 0x3ac

    invoke-interface {p1, v3, v4, v5}, Lcom/google/obf/ak;->a([BII)I

    move-result v5

    .line 45
    if-ne v5, v0, :cond_3

    move v2, v0

    .line 92
    :cond_2
    :goto_1
    return v2

    .line 47
    :cond_3
    iget-object v6, p0, Lcom/google/obf/cl;->i:Lcom/google/obf/dw;

    add-int/2addr v4, v5

    invoke-virtual {v6, v4}, Lcom/google/obf/dw;->b(I)V

    goto :goto_0

    .line 49
    :cond_4
    iget-object v0, p0, Lcom/google/obf/cl;->i:Lcom/google/obf/dw;

    invoke-virtual {v0}, Lcom/google/obf/dw;->c()I

    move-result v4

    .line 50
    iget-object v0, p0, Lcom/google/obf/cl;->i:Lcom/google/obf/dw;

    invoke-virtual {v0}, Lcom/google/obf/dw;->d()I

    move-result v0

    .line 51
    :goto_2
    if-ge v0, v4, :cond_5

    aget-byte v5, v3, v0

    const/16 v6, 0x47

    if-eq v5, v6, :cond_5

    .line 52
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 53
    :cond_5
    iget-object v3, p0, Lcom/google/obf/cl;->i:Lcom/google/obf/dw;

    invoke-virtual {v3, v0}, Lcom/google/obf/dw;->c(I)V

    .line 54
    add-int/lit16 v5, v0, 0xbc

    .line 55
    if-gt v5, v4, :cond_2

    .line 57
    iget-object v0, p0, Lcom/google/obf/cl;->i:Lcom/google/obf/dw;

    invoke-virtual {v0, v1}, Lcom/google/obf/dw;->d(I)V

    .line 58
    iget-object v0, p0, Lcom/google/obf/cl;->i:Lcom/google/obf/dw;

    iget-object v3, p0, Lcom/google/obf/cl;->j:Lcom/google/obf/dv;

    const/4 v6, 0x3

    invoke-virtual {v0, v3, v6}, Lcom/google/obf/dw;->a(Lcom/google/obf/dv;I)V

    .line 59
    iget-object v0, p0, Lcom/google/obf/cl;->j:Lcom/google/obf/dv;

    invoke-virtual {v0}, Lcom/google/obf/dv;->b()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 60
    iget-object v0, p0, Lcom/google/obf/cl;->i:Lcom/google/obf/dw;

    invoke-virtual {v0, v5}, Lcom/google/obf/dw;->c(I)V

    goto :goto_1

    .line 62
    :cond_6
    iget-object v0, p0, Lcom/google/obf/cl;->j:Lcom/google/obf/dv;

    invoke-virtual {v0}, Lcom/google/obf/dv;->b()Z

    move-result v6

    .line 63
    iget-object v0, p0, Lcom/google/obf/cl;->j:Lcom/google/obf/dv;

    invoke-virtual {v0, v1}, Lcom/google/obf/dv;->b(I)V

    .line 64
    iget-object v0, p0, Lcom/google/obf/cl;->j:Lcom/google/obf/dv;

    const/16 v3, 0xd

    invoke-virtual {v0, v3}, Lcom/google/obf/dv;->c(I)I

    move-result v0

    .line 65
    iget-object v3, p0, Lcom/google/obf/cl;->j:Lcom/google/obf/dv;

    const/4 v7, 0x2

    invoke-virtual {v3, v7}, Lcom/google/obf/dv;->b(I)V

    .line 66
    iget-object v3, p0, Lcom/google/obf/cl;->j:Lcom/google/obf/dv;

    invoke-virtual {v3}, Lcom/google/obf/dv;->b()Z

    move-result v7

    .line 67
    iget-object v3, p0, Lcom/google/obf/cl;->j:Lcom/google/obf/dv;

    invoke-virtual {v3}, Lcom/google/obf/dv;->b()Z

    move-result v8

    .line 69
    iget-object v3, p0, Lcom/google/obf/cl;->j:Lcom/google/obf/dv;

    const/4 v9, 0x4

    invoke-virtual {v3, v9}, Lcom/google/obf/dv;->c(I)I

    move-result v3

    .line 70
    iget v9, p0, Lcom/google/obf/cl;->h:I

    and-int/lit8 v9, v9, 0x10

    if-nez v9, :cond_c

    .line 71
    iget-object v9, p0, Lcom/google/obf/cl;->k:Landroid/util/SparseIntArray;

    add-int/lit8 v10, v3, -0x1

    invoke-virtual {v9, v0, v10}, Landroid/util/SparseIntArray;->get(II)I

    move-result v9

    .line 72
    iget-object v10, p0, Lcom/google/obf/cl;->k:Landroid/util/SparseIntArray;

    invoke-virtual {v10, v0, v3}, Landroid/util/SparseIntArray;->put(II)V

    .line 73
    if-ne v9, v3, :cond_7

    .line 74
    if-eqz v8, :cond_c

    .line 75
    iget-object v0, p0, Lcom/google/obf/cl;->i:Lcom/google/obf/dw;

    invoke-virtual {v0, v5}, Lcom/google/obf/dw;->c(I)V

    goto/16 :goto_1

    .line 77
    :cond_7
    add-int/lit8 v9, v9, 0x1

    rem-int/lit8 v9, v9, 0x10

    if-eq v3, v9, :cond_c

    move v3, v1

    .line 79
    :goto_3
    if-eqz v7, :cond_8

    .line 80
    iget-object v7, p0, Lcom/google/obf/cl;->i:Lcom/google/obf/dw;

    invoke-virtual {v7}, Lcom/google/obf/dw;->f()I

    move-result v7

    .line 81
    iget-object v9, p0, Lcom/google/obf/cl;->i:Lcom/google/obf/dw;

    invoke-virtual {v9, v7}, Lcom/google/obf/dw;->d(I)V

    .line 82
    :cond_8
    if-eqz v8, :cond_a

    .line 83
    iget-object v7, p0, Lcom/google/obf/cl;->a:Landroid/util/SparseArray;

    invoke-virtual {v7, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/cl$d;

    .line 84
    if-eqz v0, :cond_a

    .line 85
    if-eqz v3, :cond_9

    .line 86
    invoke-virtual {v0}, Lcom/google/obf/cl$d;->a()V

    .line 87
    :cond_9
    iget-object v3, p0, Lcom/google/obf/cl;->i:Lcom/google/obf/dw;

    invoke-virtual {v3, v5}, Lcom/google/obf/dw;->b(I)V

    .line 88
    iget-object v3, p0, Lcom/google/obf/cl;->i:Lcom/google/obf/dw;

    iget-object v7, p0, Lcom/google/obf/cl;->l:Lcom/google/obf/al;

    invoke-virtual {v0, v3, v6, v7}, Lcom/google/obf/cl$d;->a(Lcom/google/obf/dw;ZLcom/google/obf/al;)V

    .line 89
    iget-object v0, p0, Lcom/google/obf/cl;->i:Lcom/google/obf/dw;

    invoke-virtual {v0}, Lcom/google/obf/dw;->d()I

    move-result v0

    if-gt v0, v5, :cond_b

    move v0, v1

    :goto_4
    invoke-static {v0}, Lcom/google/obf/dl;->b(Z)V

    .line 90
    iget-object v0, p0, Lcom/google/obf/cl;->i:Lcom/google/obf/dw;

    invoke-virtual {v0, v4}, Lcom/google/obf/dw;->b(I)V

    .line 91
    :cond_a
    iget-object v0, p0, Lcom/google/obf/cl;->i:Lcom/google/obf/dw;

    invoke-virtual {v0, v5}, Lcom/google/obf/dw;->c(I)V

    goto/16 :goto_1

    :cond_b
    move v0, v2

    .line 89
    goto :goto_4

    :cond_c
    move v3, v2

    goto :goto_3
.end method

.method public a(Lcom/google/obf/al;)V
    .locals 1

    .prologue
    .line 27
    iput-object p1, p0, Lcom/google/obf/cl;->l:Lcom/google/obf/al;

    .line 28
    sget-object v0, Lcom/google/obf/aq;->f:Lcom/google/obf/aq;

    invoke-interface {p1, v0}, Lcom/google/obf/al;->a(Lcom/google/obf/aq;)V

    .line 29
    return-void
.end method

.method public a(Lcom/google/obf/ak;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 15
    iget-object v1, p0, Lcom/google/obf/cl;->i:Lcom/google/obf/dw;

    iget-object v3, v1, Lcom/google/obf/dw;->a:[B

    .line 16
    const/16 v1, 0x3ac

    invoke-interface {p1, v3, v0, v1}, Lcom/google/obf/ak;->c([BII)V

    move v2, v0

    .line 17
    :goto_0
    const/16 v1, 0xbc

    if-ge v2, v1, :cond_0

    move v1, v0

    .line 19
    :goto_1
    const/4 v4, 0x5

    if-ne v1, v4, :cond_1

    .line 20
    invoke-interface {p1, v2}, Lcom/google/obf/ak;->b(I)V

    .line 21
    const/4 v0, 0x1

    .line 26
    :cond_0
    return v0

    .line 22
    :cond_1
    mul-int/lit16 v4, v1, 0xbc

    add-int/2addr v4, v2

    aget-byte v4, v3, v4

    const/16 v5, 0x47

    if-eq v4, v5, :cond_2

    .line 25
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 24
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public b()V
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/obf/cl;->g:Lcom/google/obf/cj;

    invoke-virtual {v0}, Lcom/google/obf/cj;->a()V

    .line 31
    iget-object v0, p0, Lcom/google/obf/cl;->i:Lcom/google/obf/dw;

    invoke-virtual {v0}, Lcom/google/obf/dw;->a()V

    .line 32
    iget-object v0, p0, Lcom/google/obf/cl;->k:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->clear()V

    .line 33
    invoke-direct {p0}, Lcom/google/obf/cl;->f()V

    .line 34
    return-void
.end method

.method public c()V
    .locals 0

    .prologue
    .line 35
    return-void
.end method
