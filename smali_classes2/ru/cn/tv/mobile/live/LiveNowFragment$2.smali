.class Lru/cn/tv/mobile/live/LiveNowFragment$2;
.super Landroid/support/v7/widget/RecyclerView$OnScrollListener;
.source "LiveNowFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/tv/mobile/live/LiveNowFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/tv/mobile/live/LiveNowFragment;


# direct methods
.method constructor <init>(Lru/cn/tv/mobile/live/LiveNowFragment;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/tv/mobile/live/LiveNowFragment;

    .prologue
    .line 92
    iput-object p1, p0, Lru/cn/tv/mobile/live/LiveNowFragment$2;->this$0:Lru/cn/tv/mobile/live/LiveNowFragment;

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$OnScrollListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onScrollStateChanged(Landroid/support/v7/widget/RecyclerView;I)V
    .locals 5
    .param p1, "recyclerView"    # Landroid/support/v7/widget/RecyclerView;
    .param p2, "newState"    # I

    .prologue
    const/4 v3, 0x1

    .line 95
    iget-object v4, p0, Lru/cn/tv/mobile/live/LiveNowFragment$2;->this$0:Lru/cn/tv/mobile/live/LiveNowFragment;

    if-eqz p2, :cond_1

    move v2, v3

    :goto_0
    invoke-static {v4, v2}, Lru/cn/tv/mobile/live/LiveNowFragment;->access$202(Lru/cn/tv/mobile/live/LiveNowFragment;Z)Z

    .line 97
    if-eqz p2, :cond_0

    .line 99
    const/4 v1, 0x3

    .line 101
    .local v1, "prefetchOffset":I
    iget-object v2, p0, Lru/cn/tv/mobile/live/LiveNowFragment$2;->this$0:Lru/cn/tv/mobile/live/LiveNowFragment;

    invoke-static {v2}, Lru/cn/tv/mobile/live/LiveNowFragment;->access$300(Lru/cn/tv/mobile/live/LiveNowFragment;)Landroid/support/v7/widget/LinearLayoutManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v7/widget/LinearLayoutManager;->findLastVisibleItemPosition()I

    move-result v0

    .line 102
    .local v0, "lastVisibleItemPosition":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    add-int v2, v0, v1

    iget-object v4, p0, Lru/cn/tv/mobile/live/LiveNowFragment$2;->this$0:Lru/cn/tv/mobile/live/LiveNowFragment;

    .line 103
    invoke-static {v4}, Lru/cn/tv/mobile/live/LiveNowFragment;->access$000(Lru/cn/tv/mobile/live/LiveNowFragment;)Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter;->getItemCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-le v2, v4, :cond_0

    .line 104
    iget-object v2, p0, Lru/cn/tv/mobile/live/LiveNowFragment$2;->this$0:Lru/cn/tv/mobile/live/LiveNowFragment;

    invoke-static {v2}, Lru/cn/tv/mobile/live/LiveNowFragment;->access$400(Lru/cn/tv/mobile/live/LiveNowFragment;)Lru/cn/tv/mobile/live/LiveNowViewModel;

    move-result-object v2

    invoke-virtual {v2}, Lru/cn/tv/mobile/live/LiveNowViewModel;->prefetch()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 105
    iget-object v2, p0, Lru/cn/tv/mobile/live/LiveNowFragment$2;->this$0:Lru/cn/tv/mobile/live/LiveNowFragment;

    invoke-static {v2}, Lru/cn/tv/mobile/live/LiveNowFragment;->access$000(Lru/cn/tv/mobile/live/LiveNowFragment;)Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter;

    move-result-object v2

    invoke-virtual {v2, v3}, Lru/cn/tv/mobile/live/LiveNowRecyclerAdapter;->setLoading(Z)V

    .line 109
    .end local v0    # "lastVisibleItemPosition":I
    .end local v1    # "prefetchOffset":I
    :cond_0
    return-void

    .line 95
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method
