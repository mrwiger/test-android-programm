.class final Lcom/my/target/core/engines/a$a;
.super Ljava/lang/Object;
.source "NativeAdEngine.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/my/target/core/engines/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic N:Lcom/my/target/core/engines/a;


# direct methods
.method private constructor <init>(Lcom/my/target/core/engines/a;)V
    .locals 0

    .prologue
    .line 112
    iput-object p1, p0, Lcom/my/target/core/engines/a$a;->N:Lcom/my/target/core/engines/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/my/target/core/engines/a;B)V
    .locals 0

    .prologue
    .line 112
    invoke-direct {p0, p1}, Lcom/my/target/core/engines/a$a;-><init>(Lcom/my/target/core/engines/a;)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 117
    iget-object v0, p0, Lcom/my/target/core/engines/a$a;->N:Lcom/my/target/core/engines/a;

    invoke-static {v0}, Lcom/my/target/core/engines/a;->a(Lcom/my/target/core/engines/a;)Lcom/my/target/core/controllers/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/my/target/core/controllers/b;->r()I

    move-result v0

    .line 118
    iget-object v1, p0, Lcom/my/target/core/engines/a$a;->N:Lcom/my/target/core/engines/a;

    invoke-static {v1}, Lcom/my/target/core/engines/a;->a(Lcom/my/target/core/engines/a;)Lcom/my/target/core/controllers/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/my/target/core/controllers/b;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 120
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    if-nez v1, :cond_2

    .line 122
    :cond_0
    sget-object v0, Lcom/my/target/ck;->km:Lcom/my/target/ck;

    iget-object v1, p0, Lcom/my/target/core/engines/a$a;->N:Lcom/my/target/core/engines/a;

    invoke-static {v1}, Lcom/my/target/core/engines/a;->b(Lcom/my/target/core/engines/a;)Lcom/my/target/core/engines/a$a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/my/target/ck;->e(Ljava/lang/Runnable;)V

    .line 123
    iget-object v0, p0, Lcom/my/target/core/engines/a$a;->N:Lcom/my/target/core/engines/a;

    invoke-static {v0}, Lcom/my/target/core/engines/a;->a(Lcom/my/target/core/engines/a;)Lcom/my/target/core/controllers/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/my/target/core/controllers/b;->s()V

    .line 153
    :cond_1
    :goto_0
    return-void

    .line 127
    :cond_2
    iget-object v2, p0, Lcom/my/target/core/engines/a$a;->N:Lcom/my/target/core/engines/a;

    invoke-static {v2}, Lcom/my/target/core/engines/a;->c(Lcom/my/target/core/engines/a;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/my/target/core/engines/a$a;->N:Lcom/my/target/core/engines/a;

    invoke-static {v2}, Lcom/my/target/core/engines/a;->a(Lcom/my/target/core/engines/a;)Lcom/my/target/core/controllers/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/my/target/core/controllers/b;->q()I

    move-result v2

    if-eq v2, v3, :cond_3

    .line 129
    sget-object v0, Lcom/my/target/ck;->km:Lcom/my/target/ck;

    iget-object v1, p0, Lcom/my/target/core/engines/a$a;->N:Lcom/my/target/core/engines/a;

    invoke-static {v1}, Lcom/my/target/core/engines/a;->b(Lcom/my/target/core/engines/a;)Lcom/my/target/core/engines/a$a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/my/target/ck;->e(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 133
    :cond_3
    if-ne v0, v3, :cond_6

    .line 135
    iget-object v0, p0, Lcom/my/target/core/engines/a$a;->N:Lcom/my/target/core/engines/a;

    invoke-static {v0}, Lcom/my/target/core/engines/a;->c(Lcom/my/target/core/engines/a;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 137
    iget-object v0, p0, Lcom/my/target/core/engines/a$a;->N:Lcom/my/target/core/engines/a;

    invoke-static {v0}, Lcom/my/target/core/engines/a;->d(Lcom/my/target/core/engines/a;)Z

    .line 138
    iget-object v0, p0, Lcom/my/target/core/engines/a$a;->N:Lcom/my/target/core/engines/a;

    invoke-static {v0, v1}, Lcom/my/target/core/engines/a;->a(Lcom/my/target/core/engines/a;Landroid/content/Context;)V

    .line 140
    :cond_4
    iget-object v0, p0, Lcom/my/target/core/engines/a$a;->N:Lcom/my/target/core/engines/a;

    invoke-static {v0}, Lcom/my/target/core/engines/a;->a(Lcom/my/target/core/engines/a;)Lcom/my/target/core/controllers/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/my/target/core/controllers/b;->q()I

    move-result v0

    if-ne v0, v3, :cond_5

    .line 142
    iget-object v0, p0, Lcom/my/target/core/engines/a$a;->N:Lcom/my/target/core/engines/a;

    invoke-static {v0}, Lcom/my/target/core/engines/a;->a(Lcom/my/target/core/engines/a;)Lcom/my/target/core/controllers/b;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/my/target/core/controllers/b;->b(Z)V

    goto :goto_0

    .line 146
    :cond_5
    sget-object v0, Lcom/my/target/ck;->km:Lcom/my/target/ck;

    iget-object v1, p0, Lcom/my/target/core/engines/a$a;->N:Lcom/my/target/core/engines/a;

    invoke-static {v1}, Lcom/my/target/core/engines/a;->b(Lcom/my/target/core/engines/a;)Lcom/my/target/core/engines/a$a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/my/target/ck;->e(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 149
    :cond_6
    iget-object v0, p0, Lcom/my/target/core/engines/a$a;->N:Lcom/my/target/core/engines/a;

    invoke-static {v0}, Lcom/my/target/core/engines/a;->a(Lcom/my/target/core/engines/a;)Lcom/my/target/core/controllers/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/my/target/core/controllers/b;->q()I

    move-result v0

    if-ne v0, v3, :cond_1

    .line 151
    iget-object v0, p0, Lcom/my/target/core/engines/a$a;->N:Lcom/my/target/core/engines/a;

    invoke-static {v0}, Lcom/my/target/core/engines/a;->a(Lcom/my/target/core/engines/a;)Lcom/my/target/core/controllers/b;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/my/target/core/controllers/b;->b(Z)V

    goto :goto_0
.end method
