.class final Lcom/my/target/core/engines/j$a;
.super Ljava/lang/Object;
.source "InstreamAdEngine.java"

# interfaces
.implements Lcom/my/target/core/controllers/d$d;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/my/target/core/engines/j;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic G:Lcom/my/target/core/engines/j;


# direct methods
.method private constructor <init>(Lcom/my/target/core/engines/j;)V
    .locals 0

    .prologue
    .line 458
    iput-object p1, p0, Lcom/my/target/core/engines/j$a;->G:Lcom/my/target/core/engines/j;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/my/target/core/engines/j;B)V
    .locals 0

    .prologue
    .line 458
    invoke-direct {p0, p1}, Lcom/my/target/core/engines/j$a;-><init>(Lcom/my/target/core/engines/j;)V

    return-void
.end method


# virtual methods
.method public final onBannerCompleted(Lcom/my/target/aj;)V
    .locals 3
    .param p1, "banner"    # Lcom/my/target/aj;

    .prologue
    .line 505
    iget-object v0, p0, Lcom/my/target/core/engines/j$a;->G:Lcom/my/target/core/engines/j;

    invoke-static {v0}, Lcom/my/target/core/engines/j;->a(Lcom/my/target/core/engines/j;)Lcom/my/target/al;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/my/target/core/engines/j$a;->G:Lcom/my/target/core/engines/j;

    invoke-static {v0}, Lcom/my/target/core/engines/j;->b(Lcom/my/target/core/engines/j;)Lcom/my/target/aj;

    move-result-object v0

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/my/target/core/engines/j$a;->G:Lcom/my/target/core/engines/j;

    invoke-static {v0}, Lcom/my/target/core/engines/j;->c(Lcom/my/target/core/engines/j;)Lcom/my/target/instreamads/InstreamAd$InstreamAdBanner;

    move-result-object v0

    if-nez v0, :cond_1

    .line 515
    :cond_0
    :goto_0
    return-void

    .line 509
    :cond_1
    iget-object v0, p0, Lcom/my/target/core/engines/j$a;->G:Lcom/my/target/core/engines/j;

    invoke-static {v0}, Lcom/my/target/core/engines/j;->g(Lcom/my/target/core/engines/j;)Lcom/my/target/instreamads/InstreamAd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/my/target/instreamads/InstreamAd;->getListener()Lcom/my/target/instreamads/InstreamAd$InstreamAdListener;

    move-result-object v0

    .line 510
    if-eqz v0, :cond_2

    .line 512
    iget-object v1, p0, Lcom/my/target/core/engines/j$a;->G:Lcom/my/target/core/engines/j;

    invoke-static {v1}, Lcom/my/target/core/engines/j;->g(Lcom/my/target/core/engines/j;)Lcom/my/target/instreamads/InstreamAd;

    move-result-object v1

    iget-object v2, p0, Lcom/my/target/core/engines/j$a;->G:Lcom/my/target/core/engines/j;

    invoke-static {v2}, Lcom/my/target/core/engines/j;->c(Lcom/my/target/core/engines/j;)Lcom/my/target/instreamads/InstreamAd$InstreamAdBanner;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/my/target/instreamads/InstreamAd$InstreamAdListener;->onBannerComplete(Lcom/my/target/instreamads/InstreamAd;Lcom/my/target/instreamads/InstreamAd$InstreamAdBanner;)V

    .line 514
    :cond_2
    iget-object v0, p0, Lcom/my/target/core/engines/j$a;->G:Lcom/my/target/core/engines/j;

    invoke-static {v0}, Lcom/my/target/core/engines/j;->h(Lcom/my/target/core/engines/j;)V

    goto :goto_0
.end method

.method public final onBannerError(Ljava/lang/String;Lcom/my/target/aj;)V
    .locals 2
    .param p1, "error"    # Ljava/lang/String;
    .param p2, "banner"    # Lcom/my/target/aj;

    .prologue
    .line 534
    iget-object v0, p0, Lcom/my/target/core/engines/j$a;->G:Lcom/my/target/core/engines/j;

    invoke-static {v0}, Lcom/my/target/core/engines/j;->a(Lcom/my/target/core/engines/j;)Lcom/my/target/al;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/my/target/core/engines/j$a;->G:Lcom/my/target/core/engines/j;

    invoke-static {v0}, Lcom/my/target/core/engines/j;->b(Lcom/my/target/core/engines/j;)Lcom/my/target/aj;

    move-result-object v0

    if-eq v0, p2, :cond_1

    .line 544
    :cond_0
    :goto_0
    return-void

    .line 538
    :cond_1
    iget-object v0, p0, Lcom/my/target/core/engines/j$a;->G:Lcom/my/target/core/engines/j;

    invoke-static {v0}, Lcom/my/target/core/engines/j;->g(Lcom/my/target/core/engines/j;)Lcom/my/target/instreamads/InstreamAd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/my/target/instreamads/InstreamAd;->getListener()Lcom/my/target/instreamads/InstreamAd$InstreamAdListener;

    move-result-object v0

    .line 539
    if-eqz v0, :cond_2

    .line 541
    iget-object v1, p0, Lcom/my/target/core/engines/j$a;->G:Lcom/my/target/core/engines/j;

    invoke-static {v1}, Lcom/my/target/core/engines/j;->g(Lcom/my/target/core/engines/j;)Lcom/my/target/instreamads/InstreamAd;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/my/target/instreamads/InstreamAd$InstreamAdListener;->onError(Ljava/lang/String;Lcom/my/target/instreamads/InstreamAd;)V

    .line 543
    :cond_2
    iget-object v0, p0, Lcom/my/target/core/engines/j$a;->G:Lcom/my/target/core/engines/j;

    invoke-static {v0}, Lcom/my/target/core/engines/j;->h(Lcom/my/target/core/engines/j;)V

    goto :goto_0
.end method

.method public final onBannerProgressChanged(FFLcom/my/target/aj;)V
    .locals 2
    .param p1, "timeLeft"    # F
    .param p2, "duration"    # F
    .param p3, "banner"    # Lcom/my/target/aj;

    .prologue
    .line 520
    iget-object v0, p0, Lcom/my/target/core/engines/j$a;->G:Lcom/my/target/core/engines/j;

    invoke-static {v0}, Lcom/my/target/core/engines/j;->a(Lcom/my/target/core/engines/j;)Lcom/my/target/al;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/my/target/core/engines/j$a;->G:Lcom/my/target/core/engines/j;

    invoke-static {v0}, Lcom/my/target/core/engines/j;->b(Lcom/my/target/core/engines/j;)Lcom/my/target/aj;

    move-result-object v0

    if-ne v0, p3, :cond_0

    iget-object v0, p0, Lcom/my/target/core/engines/j$a;->G:Lcom/my/target/core/engines/j;

    invoke-static {v0}, Lcom/my/target/core/engines/j;->c(Lcom/my/target/core/engines/j;)Lcom/my/target/instreamads/InstreamAd$InstreamAdBanner;

    move-result-object v0

    if-nez v0, :cond_1

    .line 529
    :cond_0
    :goto_0
    return-void

    .line 524
    :cond_1
    iget-object v0, p0, Lcom/my/target/core/engines/j$a;->G:Lcom/my/target/core/engines/j;

    invoke-static {v0}, Lcom/my/target/core/engines/j;->g(Lcom/my/target/core/engines/j;)Lcom/my/target/instreamads/InstreamAd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/my/target/instreamads/InstreamAd;->getListener()Lcom/my/target/instreamads/InstreamAd$InstreamAdListener;

    move-result-object v0

    .line 525
    if-eqz v0, :cond_0

    .line 527
    iget-object v1, p0, Lcom/my/target/core/engines/j$a;->G:Lcom/my/target/core/engines/j;

    invoke-static {v1}, Lcom/my/target/core/engines/j;->g(Lcom/my/target/core/engines/j;)Lcom/my/target/instreamads/InstreamAd;

    move-result-object v1

    invoke-interface {v0, p1, p2, v1}, Lcom/my/target/instreamads/InstreamAd$InstreamAdListener;->onBannerTimeLeftChange(FFLcom/my/target/instreamads/InstreamAd;)V

    goto :goto_0
.end method

.method public final onBannerStarted(Lcom/my/target/aj;)V
    .locals 3
    .param p1, "banner"    # Lcom/my/target/aj;

    .prologue
    .line 463
    iget-object v0, p0, Lcom/my/target/core/engines/j$a;->G:Lcom/my/target/core/engines/j;

    invoke-static {v0}, Lcom/my/target/core/engines/j;->a(Lcom/my/target/core/engines/j;)Lcom/my/target/al;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/my/target/core/engines/j$a;->G:Lcom/my/target/core/engines/j;

    invoke-static {v0}, Lcom/my/target/core/engines/j;->b(Lcom/my/target/core/engines/j;)Lcom/my/target/aj;

    move-result-object v0

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/my/target/core/engines/j$a;->G:Lcom/my/target/core/engines/j;

    invoke-static {v0}, Lcom/my/target/core/engines/j;->c(Lcom/my/target/core/engines/j;)Lcom/my/target/instreamads/InstreamAd$InstreamAdBanner;

    move-result-object v0

    if-nez v0, :cond_1

    .line 486
    :cond_0
    :goto_0
    return-void

    .line 467
    :cond_1
    iget-object v0, p0, Lcom/my/target/core/engines/j$a;->G:Lcom/my/target/core/engines/j;

    invoke-static {v0}, Lcom/my/target/core/engines/j;->d(Lcom/my/target/core/engines/j;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 469
    iget-object v0, p0, Lcom/my/target/core/engines/j$a;->G:Lcom/my/target/core/engines/j;

    invoke-static {v0}, Lcom/my/target/core/engines/j;->e(Lcom/my/target/core/engines/j;)Z

    .line 470
    iget-object v0, p0, Lcom/my/target/core/engines/j$a;->G:Lcom/my/target/core/engines/j;

    invoke-static {v0}, Lcom/my/target/core/engines/j;->f(Lcom/my/target/core/engines/j;)Lcom/my/target/core/controllers/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/my/target/core/controllers/d;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 471
    if-nez v0, :cond_3

    .line 473
    const-string v0, "can\'t send stat: context is null"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 481
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/my/target/core/engines/j$a;->G:Lcom/my/target/core/engines/j;

    invoke-static {v0}, Lcom/my/target/core/engines/j;->g(Lcom/my/target/core/engines/j;)Lcom/my/target/instreamads/InstreamAd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/my/target/instreamads/InstreamAd;->getListener()Lcom/my/target/instreamads/InstreamAd$InstreamAdListener;

    move-result-object v0

    .line 482
    if-eqz v0, :cond_0

    .line 484
    iget-object v1, p0, Lcom/my/target/core/engines/j$a;->G:Lcom/my/target/core/engines/j;

    invoke-static {v1}, Lcom/my/target/core/engines/j;->g(Lcom/my/target/core/engines/j;)Lcom/my/target/instreamads/InstreamAd;

    move-result-object v1

    iget-object v2, p0, Lcom/my/target/core/engines/j$a;->G:Lcom/my/target/core/engines/j;

    invoke-static {v2}, Lcom/my/target/core/engines/j;->c(Lcom/my/target/core/engines/j;)Lcom/my/target/instreamads/InstreamAd$InstreamAdBanner;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/my/target/instreamads/InstreamAd$InstreamAdListener;->onBannerStart(Lcom/my/target/instreamads/InstreamAd;Lcom/my/target/instreamads/InstreamAd$InstreamAdBanner;)V

    goto :goto_0

    .line 477
    :cond_3
    iget-object v1, p0, Lcom/my/target/core/engines/j$a;->G:Lcom/my/target/core/engines/j;

    invoke-static {v1}, Lcom/my/target/core/engines/j;->a(Lcom/my/target/core/engines/j;)Lcom/my/target/al;

    move-result-object v1

    const-string v2, "impression"

    invoke-virtual {v1, v2}, Lcom/my/target/al;->p(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/my/target/cl;->a(Ljava/util/List;Landroid/content/Context;)V

    goto :goto_1
.end method

.method public final onBannerStopped(Lcom/my/target/aj;)V
    .locals 3
    .param p1, "banner"    # Lcom/my/target/aj;

    .prologue
    .line 491
    iget-object v0, p0, Lcom/my/target/core/engines/j$a;->G:Lcom/my/target/core/engines/j;

    invoke-static {v0}, Lcom/my/target/core/engines/j;->a(Lcom/my/target/core/engines/j;)Lcom/my/target/al;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/my/target/core/engines/j$a;->G:Lcom/my/target/core/engines/j;

    invoke-static {v0}, Lcom/my/target/core/engines/j;->b(Lcom/my/target/core/engines/j;)Lcom/my/target/aj;

    move-result-object v0

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/my/target/core/engines/j$a;->G:Lcom/my/target/core/engines/j;

    invoke-static {v0}, Lcom/my/target/core/engines/j;->c(Lcom/my/target/core/engines/j;)Lcom/my/target/instreamads/InstreamAd$InstreamAdBanner;

    move-result-object v0

    if-nez v0, :cond_1

    .line 500
    :cond_0
    :goto_0
    return-void

    .line 495
    :cond_1
    iget-object v0, p0, Lcom/my/target/core/engines/j$a;->G:Lcom/my/target/core/engines/j;

    invoke-static {v0}, Lcom/my/target/core/engines/j;->g(Lcom/my/target/core/engines/j;)Lcom/my/target/instreamads/InstreamAd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/my/target/instreamads/InstreamAd;->getListener()Lcom/my/target/instreamads/InstreamAd$InstreamAdListener;

    move-result-object v0

    .line 496
    if-eqz v0, :cond_0

    .line 498
    iget-object v1, p0, Lcom/my/target/core/engines/j$a;->G:Lcom/my/target/core/engines/j;

    invoke-static {v1}, Lcom/my/target/core/engines/j;->g(Lcom/my/target/core/engines/j;)Lcom/my/target/instreamads/InstreamAd;

    move-result-object v1

    iget-object v2, p0, Lcom/my/target/core/engines/j$a;->G:Lcom/my/target/core/engines/j;

    invoke-static {v2}, Lcom/my/target/core/engines/j;->c(Lcom/my/target/core/engines/j;)Lcom/my/target/instreamads/InstreamAd$InstreamAdBanner;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/my/target/instreamads/InstreamAd$InstreamAdListener;->onBannerComplete(Lcom/my/target/instreamads/InstreamAd;Lcom/my/target/instreamads/InstreamAd$InstreamAdBanner;)V

    goto :goto_0
.end method
