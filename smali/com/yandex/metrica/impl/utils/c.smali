.class public abstract Lcom/yandex/metrica/impl/utils/c;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private volatile a:Z


# direct methods
.method public constructor <init>(Z)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/yandex/metrica/impl/utils/c;->a:Z

    .line 30
    iput-boolean p1, p0, Lcom/yandex/metrica/impl/utils/c;->a:Z

    .line 31
    return-void
.end method

.method private static d(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 115
    if-nez p0, :cond_0

    const-string p0, ""

    :cond_0
    return-object p0
.end method

.method private d(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 103
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/yandex/metrica/impl/utils/c;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Lcom/yandex/metrica/impl/utils/c;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, p2}, Lcom/yandex/metrica/impl/utils/c;->c(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 105
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    invoke-virtual {p0}, Lcom/yandex/metrica/impl/utils/c;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 22
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/yandex/metrica/impl/utils/c;->a:Z

    .line 23
    return-void
.end method

.method a(ILjava/lang/String;)V
    .locals 3

    .prologue
    .line 74
    iget-boolean v0, p0, Lcom/yandex/metrica/impl/utils/c;->a:Z

    if-eqz v0, :cond_0

    .line 75
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/utils/c;->d()Ljava/lang/String;

    move-result-object v0

    .line 1098
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/yandex/metrica/impl/utils/c;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p2}, Lcom/yandex/metrica/impl/utils/c;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 75
    invoke-static {p1, v0, v1}, Landroid/util/Log;->println(ILjava/lang/String;Ljava/lang/String;)I

    .line 77
    :cond_0
    return-void
.end method

.method varargs a(ILjava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 80
    iget-boolean v0, p0, Lcom/yandex/metrica/impl/utils/c;->a:Z

    if-eqz v0, :cond_0

    .line 81
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/utils/c;->d()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p2, p3}, Lcom/yandex/metrica/impl/utils/c;->d(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, Landroid/util/Log;->println(ILjava/lang/String;Ljava/lang/String;)I

    .line 83
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x4

    invoke-virtual {p0, v0, p1}, Lcom/yandex/metrica/impl/utils/c;->a(ILjava/lang/String;)V

    .line 39
    return-void
.end method

.method public varargs a(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x4

    invoke-virtual {p0, v0, p1, p2}, Lcom/yandex/metrica/impl/utils/c;->a(ILjava/lang/String;[Ljava/lang/Object;)V

    .line 59
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x5

    invoke-virtual {p0, v0, p1}, Lcom/yandex/metrica/impl/utils/c;->a(ILjava/lang/String;)V

    .line 43
    return-void
.end method

.method public varargs b(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x5

    invoke-virtual {p0, v0, p1, p2}, Lcom/yandex/metrica/impl/utils/c;->a(ILjava/lang/String;[Ljava/lang/Object;)V

    .line 63
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/yandex/metrica/impl/utils/c;->a:Z

    return v0
.end method

.method c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 111
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/utils/c;->e()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method abstract c(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
.end method

.method public c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x6

    invoke-virtual {p0, v0, p1}, Lcom/yandex/metrica/impl/utils/c;->a(ILjava/lang/String;)V

    .line 47
    return-void
.end method

.method abstract d()Ljava/lang/String;
.end method

.method abstract e()Ljava/lang/String;
.end method
