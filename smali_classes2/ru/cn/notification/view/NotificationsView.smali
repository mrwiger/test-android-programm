.class public Lru/cn/notification/view/NotificationsView;
.super Landroid/widget/FrameLayout;
.source "NotificationsView.java"

# interfaces
.implements Lru/cn/notification/view/INotificationsView;


# instance fields
.field private adapter:Lru/cn/notification/NotificationAdapter;

.field private presenter:Lru/cn/notification/NotificationPresenter;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 23
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lru/cn/notification/view/NotificationsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 27
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lru/cn/notification/view/NotificationsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    return-void
.end method


# virtual methods
.method public isShown()Z
    .locals 1

    .prologue
    .line 53
    invoke-virtual {p0}, Lru/cn/notification/view/NotificationsView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lru/cn/notification/view/NotificationsView;->adapter:Lru/cn/notification/NotificationAdapter;

    .line 54
    invoke-virtual {v0}, Lru/cn/notification/NotificationAdapter;->getItemCount()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    .line 53
    :goto_0
    return v0

    .line 54
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 36
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 37
    new-instance v1, Lru/cn/notification/NotificationPresenter;

    invoke-virtual {p0}, Lru/cn/notification/view/NotificationsView;->getContext()Landroid/content/Context;

    move-result-object v2

    new-instance v3, Lru/cn/notifications/NotificationsRepository;

    invoke-virtual {p0}, Lru/cn/notification/view/NotificationsView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Lru/cn/notifications/NotificationsRepository;-><init>(Landroid/content/Context;)V

    invoke-direct {v1, v2, p0, v3}, Lru/cn/notification/NotificationPresenter;-><init>(Landroid/content/Context;Lru/cn/notification/view/INotificationsView;Lru/cn/notifications/INotificationsRepository;)V

    iput-object v1, p0, Lru/cn/notification/view/NotificationsView;->presenter:Lru/cn/notification/NotificationPresenter;

    .line 38
    new-instance v1, Lru/cn/notification/NotificationAdapter;

    iget-object v2, p0, Lru/cn/notification/view/NotificationsView;->presenter:Lru/cn/notification/NotificationPresenter;

    invoke-direct {v1, v2}, Lru/cn/notification/NotificationAdapter;-><init>(Lru/cn/notification/NotificationButtonClickListener;)V

    iput-object v1, p0, Lru/cn/notification/view/NotificationsView;->adapter:Lru/cn/notification/NotificationAdapter;

    .line 39
    new-instance v0, Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {p0}, Lru/cn/notification/view/NotificationsView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/widget/RecyclerView;-><init>(Landroid/content/Context;)V

    .line 40
    .local v0, "recyclerView":Landroid/support/v7/widget/RecyclerView;
    new-instance v1, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-virtual {p0}, Lru/cn/notification/view/NotificationsView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, v5, v5}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;IZ)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 41
    iget-object v1, p0, Lru/cn/notification/view/NotificationsView;->adapter:Lru/cn/notification/NotificationAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 42
    invoke-virtual {p0, v0}, Lru/cn/notification/view/NotificationsView;->addView(Landroid/view/View;)V

    .line 43
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 47
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 48
    iget-object v0, p0, Lru/cn/notification/view/NotificationsView;->presenter:Lru/cn/notification/NotificationPresenter;

    invoke-virtual {v0}, Lru/cn/notification/NotificationPresenter;->destroy()V

    .line 49
    return-void
.end method

.method public replace(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lru/cn/notifications/entity/INotification;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 59
    .local p1, "notifications":Ljava/util/List;, "Ljava/util/List<Lru/cn/notifications/entity/INotification;>;"
    iget-object v0, p0, Lru/cn/notification/view/NotificationsView;->adapter:Lru/cn/notification/NotificationAdapter;

    invoke-virtual {v0, p1}, Lru/cn/notification/NotificationAdapter;->replace(Ljava/util/List;)V

    .line 60
    return-void
.end method
