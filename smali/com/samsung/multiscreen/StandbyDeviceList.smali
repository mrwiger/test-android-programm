.class Lcom/samsung/multiscreen/StandbyDeviceList;
.super Ljava/lang/Object;
.source "StandbyDeviceList.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;,
        Lcom/samsung/multiscreen/StandbyDeviceList$StandbyDevice;
    }
.end annotation


# static fields
.field private static DEVICE_LIST_FILE_NAME:Ljava/lang/String; = null

.field private static KEY_STANDBYLIST:Ljava/lang/String; = null

.field private static final SUPPORTED_TV_MODEL_YEAR:I = 0x10

.field private static TAG:Ljava/lang/String; = null

.field private static final TIMEOUT:I = 0x1b58

.field private static VALUE_BSSID:Ljava/lang/String;

.field private static VALUE_DUID:Ljava/lang/String;

.field private static VALUE_MAC:Ljava/lang/String;

.field private static VALUE_NAME:Ljava/lang/String;

.field private static VALUE_URI:Ljava/lang/String;

.field private static mInstance:Lcom/samsung/multiscreen/StandbyDeviceList;


# instance fields
.field private mList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/multiscreen/StandbyDeviceList$StandbyDevice;",
            ">;"
        }
    .end annotation
.end field

.field private mLocalStorage:Landroid/content/SharedPreferences;

.field private mNetworkMonitor:Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;

.field private mSearchListener:Lcom/samsung/multiscreen/Search$SearchListener;

.field private mShowStandbyDevicesTimerExpired:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 141
    const-string v0, "StndbyDLHndlr"

    sput-object v0, Lcom/samsung/multiscreen/StandbyDeviceList;->TAG:Ljava/lang/String;

    .line 142
    const-string v0, "com.samsung.smartviewSDK.standbydevices"

    sput-object v0, Lcom/samsung/multiscreen/StandbyDeviceList;->DEVICE_LIST_FILE_NAME:Ljava/lang/String;

    .line 143
    const-string v0, "STANDBYLIST_KEY"

    sput-object v0, Lcom/samsung/multiscreen/StandbyDeviceList;->KEY_STANDBYLIST:Ljava/lang/String;

    .line 147
    const-string v0, "id"

    sput-object v0, Lcom/samsung/multiscreen/StandbyDeviceList;->VALUE_DUID:Ljava/lang/String;

    .line 148
    const-string v0, "mac"

    sput-object v0, Lcom/samsung/multiscreen/StandbyDeviceList;->VALUE_MAC:Ljava/lang/String;

    .line 149
    const-string v0, "uri"

    sput-object v0, Lcom/samsung/multiscreen/StandbyDeviceList;->VALUE_URI:Ljava/lang/String;

    .line 150
    const-string v0, "name"

    sput-object v0, Lcom/samsung/multiscreen/StandbyDeviceList;->VALUE_NAME:Ljava/lang/String;

    .line 151
    const-string v0, "ssid"

    sput-object v0, Lcom/samsung/multiscreen/StandbyDeviceList;->VALUE_BSSID:Ljava/lang/String;

    .line 153
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/multiscreen/StandbyDeviceList;->mInstance:Lcom/samsung/multiscreen/StandbyDeviceList;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 449
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/samsung/multiscreen/Search$SearchListener;)V
    .locals 13
    .param p1, "appContext"    # Landroid/content/Context;
    .param p2, "searchListener"    # Lcom/samsung/multiscreen/Search$SearchListener;

    .prologue
    const/4 v2, 0x0

    .line 403
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 405
    sget-object v1, Lcom/samsung/multiscreen/StandbyDeviceList;->DEVICE_LIST_FILE_NAME:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/multiscreen/StandbyDeviceList;->mLocalStorage:Landroid/content/SharedPreferences;

    .line 406
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/multiscreen/StandbyDeviceList;->mShowStandbyDevicesTimerExpired:Ljava/lang/Boolean;

    .line 407
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/samsung/multiscreen/StandbyDeviceList;->mList:Ljava/util/List;

    .line 409
    iget-object v1, p0, Lcom/samsung/multiscreen/StandbyDeviceList;->mLocalStorage:Landroid/content/SharedPreferences;

    sget-object v2, Lcom/samsung/multiscreen/StandbyDeviceList;->KEY_STANDBYLIST:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 410
    .local v12, "stringDeviceList":Ljava/lang/String;
    if-eqz v12, :cond_0

    const-string v1, "[]"

    invoke-virtual {v12, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 412
    :cond_0
    new-instance v10, Lorg/json/JSONArray;

    invoke-direct {v10}, Lorg/json/JSONArray;-><init>()V

    .line 424
    .local v10, "jsonDeviceList":Lorg/json/JSONArray;
    :goto_0
    :try_start_0
    invoke-virtual {v10}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-lez v1, :cond_2

    .line 425
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_1
    invoke-virtual {v10}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-ge v9, v1, :cond_2

    .line 426
    invoke-virtual {v10, v9}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lorg/json/JSONObject;

    .line 427
    .local v11, "record":Lorg/json/JSONObject;
    new-instance v0, Lcom/samsung/multiscreen/StandbyDeviceList$StandbyDevice;

    sget-object v1, Lcom/samsung/multiscreen/StandbyDeviceList;->VALUE_DUID:Ljava/lang/String;

    .line 428
    invoke-virtual {v11, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sget-object v1, Lcom/samsung/multiscreen/StandbyDeviceList;->VALUE_BSSID:Ljava/lang/String;

    .line 429
    invoke-virtual {v11, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    sget-object v1, Lcom/samsung/multiscreen/StandbyDeviceList;->VALUE_MAC:Ljava/lang/String;

    .line 430
    invoke-virtual {v11, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    sget-object v1, Lcom/samsung/multiscreen/StandbyDeviceList;->VALUE_URI:Ljava/lang/String;

    .line 431
    invoke-virtual {v11, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    sget-object v1, Lcom/samsung/multiscreen/StandbyDeviceList;->VALUE_NAME:Ljava/lang/String;

    .line 432
    invoke-virtual {v11, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const/4 v1, 0x0

    .line 433
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Lcom/samsung/multiscreen/StandbyDeviceList$StandbyDevice;-><init>(Lcom/samsung/multiscreen/StandbyDeviceList;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 434
    .local v0, "standbyDevice":Lcom/samsung/multiscreen/StandbyDeviceList$StandbyDevice;
    iget-object v1, p0, Lcom/samsung/multiscreen/StandbyDeviceList;->mList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 425
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 415
    .end local v0    # "standbyDevice":Lcom/samsung/multiscreen/StandbyDeviceList$StandbyDevice;
    .end local v9    # "i":I
    .end local v10    # "jsonDeviceList":Lorg/json/JSONArray;
    .end local v11    # "record":Lorg/json/JSONObject;
    :cond_1
    :try_start_1
    new-instance v10, Lorg/json/JSONArray;

    invoke-direct {v10, v12}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .restart local v10    # "jsonDeviceList":Lorg/json/JSONArray;
    goto :goto_0

    .line 416
    .end local v10    # "jsonDeviceList":Lorg/json/JSONArray;
    :catch_0
    move-exception v8

    .line 417
    .local v8, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/samsung/multiscreen/StandbyDeviceList;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "StandbyDeviceListHandler: Error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v8}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 444
    .end local v8    # "e":Ljava/lang/Exception;
    :goto_2
    return-void

    .line 437
    .restart local v10    # "jsonDeviceList":Lorg/json/JSONArray;
    :catch_1
    move-exception v8

    .line 438
    .restart local v8    # "e":Ljava/lang/Exception;
    sget-object v1, Lcom/samsung/multiscreen/StandbyDeviceList;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "StandbyDeviceListHandler: Error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v8}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 443
    .end local v8    # "e":Ljava/lang/Exception;
    :cond_2
    new-instance v1, Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;

    invoke-direct {v1, p0, p1, p2}, Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;-><init>(Lcom/samsung/multiscreen/StandbyDeviceList;Landroid/content/Context;Lcom/samsung/multiscreen/Search$SearchListener;)V

    iput-object v1, p0, Lcom/samsung/multiscreen/StandbyDeviceList;->mNetworkMonitor:Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;

    goto :goto_2
.end method

.method static synthetic access$000(Lcom/samsung/multiscreen/StandbyDeviceList;)Lcom/samsung/multiscreen/Search$SearchListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/multiscreen/StandbyDeviceList;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/samsung/multiscreen/StandbyDeviceList;->mSearchListener:Lcom/samsung/multiscreen/Search$SearchListener;

    return-object v0
.end method

.method static synthetic access$002(Lcom/samsung/multiscreen/StandbyDeviceList;Lcom/samsung/multiscreen/Search$SearchListener;)Lcom/samsung/multiscreen/Search$SearchListener;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/multiscreen/StandbyDeviceList;
    .param p1, "x1"    # Lcom/samsung/multiscreen/Search$SearchListener;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/samsung/multiscreen/StandbyDeviceList;->mSearchListener:Lcom/samsung/multiscreen/Search$SearchListener;

    return-object p1
.end method

.method static synthetic access$1000(Lcom/samsung/multiscreen/StandbyDeviceList;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/multiscreen/StandbyDeviceList;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/samsung/multiscreen/StandbyDeviceList;->commit()V

    return-void
.end method

.method static synthetic access$1100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/samsung/multiscreen/StandbyDeviceList;->VALUE_DUID:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/samsung/multiscreen/StandbyDeviceList;->VALUE_BSSID:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1300()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/samsung/multiscreen/StandbyDeviceList;->VALUE_MAC:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1400()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/samsung/multiscreen/StandbyDeviceList;->VALUE_URI:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1500()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/samsung/multiscreen/StandbyDeviceList;->VALUE_NAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1600()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/samsung/multiscreen/StandbyDeviceList;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/multiscreen/StandbyDeviceList;)Ljava/lang/Boolean;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/multiscreen/StandbyDeviceList;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/samsung/multiscreen/StandbyDeviceList;->mShowStandbyDevicesTimerExpired:Ljava/lang/Boolean;

    return-object v0
.end method

.method static synthetic access$502(Lcom/samsung/multiscreen/StandbyDeviceList;Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/multiscreen/StandbyDeviceList;
    .param p1, "x1"    # Ljava/lang/Boolean;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/samsung/multiscreen/StandbyDeviceList;->mShowStandbyDevicesTimerExpired:Ljava/lang/Boolean;

    return-object p1
.end method

.method static synthetic access$600(Lcom/samsung/multiscreen/StandbyDeviceList;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/multiscreen/StandbyDeviceList;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/samsung/multiscreen/StandbyDeviceList;->get()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/samsung/multiscreen/StandbyDeviceList;Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/multiscreen/StandbyDeviceList;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/samsung/multiscreen/StandbyDeviceList;->remove(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(Lcom/samsung/multiscreen/StandbyDeviceList;)Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/multiscreen/StandbyDeviceList;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/samsung/multiscreen/StandbyDeviceList;->mNetworkMonitor:Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;

    return-object v0
.end method

.method static synthetic access$900(Lcom/samsung/multiscreen/StandbyDeviceList;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/multiscreen/StandbyDeviceList;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/samsung/multiscreen/StandbyDeviceList;->mList:Ljava/util/List;

    return-object v0
.end method

.method private declared-synchronized commit()V
    .locals 8

    .prologue
    .line 455
    monitor-enter p0

    :try_start_0
    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 456
    :try_start_1
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    .line 457
    .local v0, "deviceList":Lorg/json/JSONArray;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v5, p0, Lcom/samsung/multiscreen/StandbyDeviceList;->mList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge v3, v5, :cond_0

    .line 458
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 460
    .local v4, "record":Lorg/json/JSONObject;
    :try_start_2
    sget-object v6, Lcom/samsung/multiscreen/StandbyDeviceList;->VALUE_DUID:Ljava/lang/String;

    iget-object v5, p0, Lcom/samsung/multiscreen/StandbyDeviceList;->mList:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/multiscreen/StandbyDeviceList$StandbyDevice;

    iget-object v5, v5, Lcom/samsung/multiscreen/StandbyDeviceList$StandbyDevice;->duid:Ljava/lang/String;

    invoke-virtual {v4, v6, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 461
    sget-object v6, Lcom/samsung/multiscreen/StandbyDeviceList;->VALUE_BSSID:Ljava/lang/String;

    iget-object v5, p0, Lcom/samsung/multiscreen/StandbyDeviceList;->mList:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/multiscreen/StandbyDeviceList$StandbyDevice;

    iget-object v5, v5, Lcom/samsung/multiscreen/StandbyDeviceList$StandbyDevice;->bssid:Ljava/lang/String;

    invoke-virtual {v4, v6, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 462
    sget-object v6, Lcom/samsung/multiscreen/StandbyDeviceList;->VALUE_MAC:Ljava/lang/String;

    iget-object v5, p0, Lcom/samsung/multiscreen/StandbyDeviceList;->mList:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/multiscreen/StandbyDeviceList$StandbyDevice;

    iget-object v5, v5, Lcom/samsung/multiscreen/StandbyDeviceList$StandbyDevice;->mac:Ljava/lang/String;

    invoke-virtual {v4, v6, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 463
    sget-object v6, Lcom/samsung/multiscreen/StandbyDeviceList;->VALUE_URI:Ljava/lang/String;

    iget-object v5, p0, Lcom/samsung/multiscreen/StandbyDeviceList;->mList:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/multiscreen/StandbyDeviceList$StandbyDevice;

    iget-object v5, v5, Lcom/samsung/multiscreen/StandbyDeviceList$StandbyDevice;->ip:Ljava/lang/String;

    invoke-virtual {v4, v6, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 464
    sget-object v6, Lcom/samsung/multiscreen/StandbyDeviceList;->VALUE_NAME:Ljava/lang/String;

    iget-object v5, p0, Lcom/samsung/multiscreen/StandbyDeviceList;->mList:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/multiscreen/StandbyDeviceList$StandbyDevice;

    iget-object v5, v5, Lcom/samsung/multiscreen/StandbyDeviceList$StandbyDevice;->name:Ljava/lang/String;

    invoke-virtual {v4, v6, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 469
    :try_start_3
    invoke-virtual {v0, v4}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 471
    iget-object v5, p0, Lcom/samsung/multiscreen/StandbyDeviceList;->mLocalStorage:Landroid/content/SharedPreferences;

    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 472
    .local v2, "editor":Landroid/content/SharedPreferences$Editor;
    sget-object v5, Lcom/samsung/multiscreen/StandbyDeviceList;->KEY_STANDBYLIST:Ljava/lang/String;

    invoke-virtual {v0}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v2, v5, v6}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 473
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 457
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 465
    .end local v2    # "editor":Landroid/content/SharedPreferences$Editor;
    :catch_0
    move-exception v1

    .line 466
    .local v1, "e":Ljava/lang/Exception;
    sget-object v5, Lcom/samsung/multiscreen/StandbyDeviceList;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "close(): Error: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 467
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 476
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v4    # "record":Lorg/json/JSONObject;
    :goto_1
    monitor-exit p0

    return-void

    .line 475
    :cond_0
    :try_start_4
    monitor-exit p0

    goto :goto_1

    .end local v0    # "deviceList":Lorg/json/JSONArray;
    .end local v3    # "i":I
    :catchall_0
    move-exception v5

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v5
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 455
    :catchall_1
    move-exception v5

    monitor-exit p0

    throw v5
.end method

.method static create(Landroid/content/Context;Lcom/samsung/multiscreen/Search$SearchListener;)Lcom/samsung/multiscreen/StandbyDeviceList;
    .locals 1
    .param p0, "applicationContext"    # Landroid/content/Context;
    .param p1, "searchListener"    # Lcom/samsung/multiscreen/Search$SearchListener;

    .prologue
    .line 171
    sget-object v0, Lcom/samsung/multiscreen/StandbyDeviceList;->mInstance:Lcom/samsung/multiscreen/StandbyDeviceList;

    if-nez v0, :cond_0

    .line 172
    new-instance v0, Lcom/samsung/multiscreen/StandbyDeviceList;

    invoke-direct {v0, p0, p1}, Lcom/samsung/multiscreen/StandbyDeviceList;-><init>(Landroid/content/Context;Lcom/samsung/multiscreen/Search$SearchListener;)V

    sput-object v0, Lcom/samsung/multiscreen/StandbyDeviceList;->mInstance:Lcom/samsung/multiscreen/StandbyDeviceList;

    .line 174
    :cond_0
    sget-object v0, Lcom/samsung/multiscreen/StandbyDeviceList;->mInstance:Lcom/samsung/multiscreen/StandbyDeviceList;

    return-object v0
.end method

.method private get()Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/multiscreen/Service;",
            ">;"
        }
    .end annotation

    .prologue
    .line 500
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 502
    .local v3, "list":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/multiscreen/Service;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    :try_start_0
    iget-object v6, p0, Lcom/samsung/multiscreen/StandbyDeviceList;->mList:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-ge v2, v6, :cond_1

    .line 503
    iget-object v6, p0, Lcom/samsung/multiscreen/StandbyDeviceList;->mList:Ljava/util/List;

    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/multiscreen/StandbyDeviceList$StandbyDevice;

    .line 504
    .local v0, "device":Lcom/samsung/multiscreen/StandbyDeviceList$StandbyDevice;
    iget-object v6, v0, Lcom/samsung/multiscreen/StandbyDeviceList$StandbyDevice;->isActive:Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/samsung/multiscreen/StandbyDeviceList;->mNetworkMonitor:Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;

    invoke-virtual {v6}, Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;->getCurrentBSSID()Ljava/lang/String;

    move-result-object v6

    iget-object v7, v0, Lcom/samsung/multiscreen/StandbyDeviceList$StandbyDevice;->bssid:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 505
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    .line 506
    .local v4, "record":Lorg/json/JSONObject;
    sget-object v6, Lcom/samsung/multiscreen/StandbyDeviceList;->VALUE_DUID:Ljava/lang/String;

    iget-object v7, v0, Lcom/samsung/multiscreen/StandbyDeviceList$StandbyDevice;->duid:Ljava/lang/String;

    invoke-virtual {v4, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 507
    sget-object v6, Lcom/samsung/multiscreen/StandbyDeviceList;->VALUE_URI:Ljava/lang/String;

    iget-object v7, v0, Lcom/samsung/multiscreen/StandbyDeviceList$StandbyDevice;->ip:Ljava/lang/String;

    invoke-virtual {v4, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 508
    sget-object v6, Lcom/samsung/multiscreen/StandbyDeviceList;->VALUE_NAME:Ljava/lang/String;

    iget-object v7, v0, Lcom/samsung/multiscreen/StandbyDeviceList$StandbyDevice;->name:Ljava/lang/String;

    invoke-virtual {v4, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 509
    invoke-static {v4}, Lcom/samsung/multiscreen/Service;->create(Lorg/json/JSONObject;)Lcom/samsung/multiscreen/Service;

    move-result-object v5

    .line 510
    .local v5, "standbyService":Lcom/samsung/multiscreen/Service;
    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 502
    .end local v4    # "record":Lorg/json/JSONObject;
    .end local v5    # "standbyService":Lcom/samsung/multiscreen/Service;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 513
    .end local v0    # "device":Lcom/samsung/multiscreen/StandbyDeviceList$StandbyDevice;
    :catch_0
    move-exception v1

    .line 514
    .local v1, "e":Ljava/lang/Exception;
    sget-object v6, Lcom/samsung/multiscreen/StandbyDeviceList;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "get(): Error: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 516
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_1
    return-object v3
.end method

.method static getInstance()Lcom/samsung/multiscreen/StandbyDeviceList;
    .locals 1

    .prologue
    .line 194
    sget-object v0, Lcom/samsung/multiscreen/StandbyDeviceList;->mInstance:Lcom/samsung/multiscreen/StandbyDeviceList;

    return-object v0
.end method

.method private isStandbyDevice(Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 3
    .param p1, "duid"    # Ljava/lang/String;

    .prologue
    .line 526
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/samsung/multiscreen/StandbyDeviceList;->mList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 527
    iget-object v1, p0, Lcom/samsung/multiscreen/StandbyDeviceList;->mList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/multiscreen/StandbyDeviceList$StandbyDevice;

    iget-object v1, v1, Lcom/samsung/multiscreen/StandbyDeviceList$StandbyDevice;->duid:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/multiscreen/StandbyDeviceList;->mNetworkMonitor:Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;

    invoke-virtual {v1}, Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;->getCurrentBSSID()Ljava/lang/String;

    move-result-object v2

    iget-object v1, p0, Lcom/samsung/multiscreen/StandbyDeviceList;->mList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/multiscreen/StandbyDeviceList$StandbyDevice;

    iget-object v1, v1, Lcom/samsung/multiscreen/StandbyDeviceList$StandbyDevice;->bssid:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 528
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 531
    :goto_1
    return-object v1

    .line 526
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 531
    :cond_1
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto :goto_1
.end method

.method private remove(Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 3
    .param p1, "duid"    # Ljava/lang/String;

    .prologue
    .line 485
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/samsung/multiscreen/StandbyDeviceList;->mList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 486
    iget-object v1, p0, Lcom/samsung/multiscreen/StandbyDeviceList;->mList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/multiscreen/StandbyDeviceList$StandbyDevice;

    iget-object v1, v1, Lcom/samsung/multiscreen/StandbyDeviceList$StandbyDevice;->duid:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 487
    iget-object v1, p0, Lcom/samsung/multiscreen/StandbyDeviceList;->mList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 488
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 491
    :goto_1
    return-object v1

    .line 485
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 491
    :cond_1
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto :goto_1
.end method


# virtual methods
.method clear()V
    .locals 1

    .prologue
    .line 325
    new-instance v0, Lcom/samsung/multiscreen/StandbyDeviceList$3;

    invoke-direct {v0, p0}, Lcom/samsung/multiscreen/StandbyDeviceList$3;-><init>(Lcom/samsung/multiscreen/StandbyDeviceList;)V

    .line 347
    invoke-virtual {v0}, Lcom/samsung/multiscreen/StandbyDeviceList$3;->run()V

    .line 348
    return-void
.end method

.method destruct()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 178
    sget-object v0, Lcom/samsung/multiscreen/StandbyDeviceList;->mInstance:Lcom/samsung/multiscreen/StandbyDeviceList;

    if-nez v0, :cond_0

    .line 186
    :goto_0
    return-void

    .line 181
    :cond_0
    sput-object v1, Lcom/samsung/multiscreen/StandbyDeviceList;->mInstance:Lcom/samsung/multiscreen/StandbyDeviceList;

    .line 182
    iget-object v0, p0, Lcom/samsung/multiscreen/StandbyDeviceList;->mList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 183
    iget-object v0, p0, Lcom/samsung/multiscreen/StandbyDeviceList;->mNetworkMonitor:Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;

    invoke-virtual {v0}, Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;->stopNetworkMonitoring()V

    .line 184
    iput-object v1, p0, Lcom/samsung/multiscreen/StandbyDeviceList;->mLocalStorage:Landroid/content/SharedPreferences;

    .line 185
    iput-object v1, p0, Lcom/samsung/multiscreen/StandbyDeviceList;->mSearchListener:Lcom/samsung/multiscreen/Search$SearchListener;

    goto :goto_0
.end method

.method get(Ljava/lang/String;)Lcom/samsung/multiscreen/Service;
    .locals 7
    .param p1, "duid"    # Ljava/lang/String;

    .prologue
    .line 239
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    :try_start_0
    iget-object v4, p0, Lcom/samsung/multiscreen/StandbyDeviceList;->mList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v2, v4, :cond_1

    .line 240
    iget-object v4, p0, Lcom/samsung/multiscreen/StandbyDeviceList;->mList:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/multiscreen/StandbyDeviceList$StandbyDevice;

    .line 241
    .local v0, "device":Lcom/samsung/multiscreen/StandbyDeviceList$StandbyDevice;
    iget-object v4, v0, Lcom/samsung/multiscreen/StandbyDeviceList$StandbyDevice;->duid:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 242
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    .line 243
    .local v3, "record":Lorg/json/JSONObject;
    sget-object v4, Lcom/samsung/multiscreen/StandbyDeviceList;->VALUE_DUID:Ljava/lang/String;

    iget-object v5, v0, Lcom/samsung/multiscreen/StandbyDeviceList$StandbyDevice;->duid:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 244
    sget-object v4, Lcom/samsung/multiscreen/StandbyDeviceList;->VALUE_URI:Ljava/lang/String;

    iget-object v5, v0, Lcom/samsung/multiscreen/StandbyDeviceList$StandbyDevice;->ip:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 245
    sget-object v4, Lcom/samsung/multiscreen/StandbyDeviceList;->VALUE_NAME:Ljava/lang/String;

    iget-object v5, v0, Lcom/samsung/multiscreen/StandbyDeviceList$StandbyDevice;->name:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 246
    invoke-static {v3}, Lcom/samsung/multiscreen/Service;->create(Lorg/json/JSONObject;)Lcom/samsung/multiscreen/Service;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 252
    .end local v0    # "device":Lcom/samsung/multiscreen/StandbyDeviceList$StandbyDevice;
    .end local v3    # "record":Lorg/json/JSONObject;
    :goto_1
    return-object v4

    .line 239
    .restart local v0    # "device":Lcom/samsung/multiscreen/StandbyDeviceList$StandbyDevice;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 249
    .end local v0    # "device":Lcom/samsung/multiscreen/StandbyDeviceList$StandbyDevice;
    :catch_0
    move-exception v1

    .line 250
    .local v1, "e":Ljava/lang/Exception;
    sget-object v4, Lcom/samsung/multiscreen/StandbyDeviceList;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "get(Duid): Error: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 252
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method

.method getFoundStandbyService(Lcom/samsung/multiscreen/Service;)Lcom/samsung/multiscreen/Service;
    .locals 1
    .param p1, "service"    # Lcom/samsung/multiscreen/Service;

    .prologue
    .line 376
    iget-object v0, p1, Lcom/samsung/multiscreen/Service;->isStandbyService:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/samsung/multiscreen/Service;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/multiscreen/StandbyDeviceList;->isStandbyDevice(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 377
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/samsung/multiscreen/StandbyDeviceList;->update(Lcom/samsung/multiscreen/Service;Ljava/lang/Boolean;)V

    .line 378
    invoke-virtual {p1}, Lcom/samsung/multiscreen/Service;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/multiscreen/StandbyDeviceList;->get(Ljava/lang/String;)Lcom/samsung/multiscreen/Service;

    move-result-object v0

    .line 380
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method getLostStandbyService(Lcom/samsung/multiscreen/Service;)Lcom/samsung/multiscreen/Service;
    .locals 1
    .param p1, "service"    # Lcom/samsung/multiscreen/Service;

    .prologue
    .line 358
    iget-object v0, p1, Lcom/samsung/multiscreen/Service;->isStandbyService:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/samsung/multiscreen/Service;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/multiscreen/StandbyDeviceList;->isStandbyDevice(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 359
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/samsung/multiscreen/StandbyDeviceList;->update(Lcom/samsung/multiscreen/Service;Ljava/lang/Boolean;)V

    .line 360
    iget-object v0, p0, Lcom/samsung/multiscreen/StandbyDeviceList;->mShowStandbyDevicesTimerExpired:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 361
    invoke-virtual {p1}, Lcom/samsung/multiscreen/Service;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/multiscreen/StandbyDeviceList;->get(Ljava/lang/String;)Lcom/samsung/multiscreen/Service;

    move-result-object v0

    .line 364
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method getMac(Lcom/samsung/multiscreen/Service;)Ljava/lang/String;
    .locals 3
    .param p1, "service"    # Lcom/samsung/multiscreen/Service;

    .prologue
    .line 390
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/samsung/multiscreen/StandbyDeviceList;->mList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 391
    invoke-virtual {p1}, Lcom/samsung/multiscreen/Service;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    iget-object v1, p0, Lcom/samsung/multiscreen/StandbyDeviceList;->mList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/multiscreen/StandbyDeviceList$StandbyDevice;

    iget-object v1, v1, Lcom/samsung/multiscreen/StandbyDeviceList$StandbyDevice;->duid:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 392
    iget-object v1, p0, Lcom/samsung/multiscreen/StandbyDeviceList;->mList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/multiscreen/StandbyDeviceList$StandbyDevice;

    iget-object v1, v1, Lcom/samsung/multiscreen/StandbyDeviceList$StandbyDevice;->mac:Ljava/lang/String;

    .line 395
    :goto_1
    return-object v1

    .line 390
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 395
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method remove(Lcom/samsung/multiscreen/Service;)V
    .locals 1
    .param p1, "service"    # Lcom/samsung/multiscreen/Service;

    .prologue
    .line 313
    iget-object v0, p1, Lcom/samsung/multiscreen/Service;->isStandbyService:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 314
    invoke-virtual {p1}, Lcom/samsung/multiscreen/Service;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/multiscreen/StandbyDeviceList;->remove(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 315
    iget-object v0, p0, Lcom/samsung/multiscreen/StandbyDeviceList;->mSearchListener:Lcom/samsung/multiscreen/Search$SearchListener;

    invoke-interface {v0, p1}, Lcom/samsung/multiscreen/Search$SearchListener;->onLost(Lcom/samsung/multiscreen/Service;)V

    .line 316
    invoke-direct {p0}, Lcom/samsung/multiscreen/StandbyDeviceList;->commit()V

    .line 319
    :cond_0
    return-void
.end method

.method start()V
    .locals 4

    .prologue
    .line 202
    new-instance v1, Ljava/util/Timer;

    const-string v2, "showStandbyTVTimer"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Ljava/util/Timer;-><init>(Ljava/lang/String;Z)V

    .line 203
    .local v1, "timer":Ljava/util/Timer;
    new-instance v0, Lcom/samsung/multiscreen/StandbyDeviceList$1;

    invoke-direct {v0, p0}, Lcom/samsung/multiscreen/StandbyDeviceList$1;-><init>(Lcom/samsung/multiscreen/StandbyDeviceList;)V

    .line 217
    .local v0, "task":Ljava/util/TimerTask;
    const-wide/16 v2, 0x1b58

    invoke-virtual {v1, v0, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 218
    return-void
.end method

.method stop()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 224
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/multiscreen/StandbyDeviceList;->mShowStandbyDevicesTimerExpired:Ljava/lang/Boolean;

    .line 226
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/samsung/multiscreen/StandbyDeviceList;->mList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 227
    iget-object v1, p0, Lcom/samsung/multiscreen/StandbyDeviceList;->mList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/multiscreen/StandbyDeviceList$StandbyDevice;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lcom/samsung/multiscreen/StandbyDeviceList$StandbyDevice;->isActive:Ljava/lang/Boolean;

    .line 226
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 229
    :cond_0
    return-void
.end method

.method update(Lcom/samsung/multiscreen/Service;Ljava/lang/Boolean;)V
    .locals 2
    .param p1, "service"    # Lcom/samsung/multiscreen/Service;
    .param p2, "isActive"    # Ljava/lang/Boolean;

    .prologue
    .line 263
    invoke-virtual {p1}, Lcom/samsung/multiscreen/Service;->getType()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Samsung SmartTV"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 305
    :goto_0
    return-void

    .line 267
    :cond_0
    new-instance v0, Lcom/samsung/multiscreen/StandbyDeviceList$2;

    invoke-direct {v0, p0, p1, p2}, Lcom/samsung/multiscreen/StandbyDeviceList$2;-><init>(Lcom/samsung/multiscreen/StandbyDeviceList;Lcom/samsung/multiscreen/Service;Ljava/lang/Boolean;)V

    invoke-virtual {p1, v0}, Lcom/samsung/multiscreen/Service;->getDeviceInfo(Lcom/samsung/multiscreen/Result;)V

    goto :goto_0
.end method
