.class final Lcom/my/target/core/engines/g$a;
.super Ljava/lang/Object;
.source "InterstitialSliderAdEngine.java"

# interfaces
.implements Lcom/my/target/core/presenters/g$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/my/target/core/engines/g;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic q:Lcom/my/target/core/engines/g;


# direct methods
.method private constructor <init>(Lcom/my/target/core/engines/g;)V
    .locals 0

    .prologue
    .line 211
    iput-object p1, p0, Lcom/my/target/core/engines/g$a;->q:Lcom/my/target/core/engines/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/my/target/core/engines/g;B)V
    .locals 0

    .prologue
    .line 211
    invoke-direct {p0, p1}, Lcom/my/target/core/engines/g$a;-><init>(Lcom/my/target/core/engines/g;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/my/target/core/models/banners/g;)V
    .locals 2

    .prologue
    .line 216
    const/4 v0, 0x0

    .line 217
    iget-object v1, p0, Lcom/my/target/core/engines/g$a;->q:Lcom/my/target/core/engines/g;

    invoke-static {v1}, Lcom/my/target/core/engines/g;->a(Lcom/my/target/core/engines/g;)Ljava/lang/ref/WeakReference;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 219
    iget-object v0, p0, Lcom/my/target/core/engines/g$a;->q:Lcom/my/target/core/engines/g;

    invoke-static {v0}, Lcom/my/target/core/engines/g;->a(Lcom/my/target/core/engines/g;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/core/presenters/g;

    .line 221
    :cond_0
    if-nez v0, :cond_2

    .line 234
    :cond_1
    :goto_0
    return-void

    .line 225
    :cond_2
    invoke-virtual {v0}, Lcom/my/target/core/presenters/g;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 227
    invoke-static {}, Lcom/my/target/ce;->bw()Lcom/my/target/ce;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Lcom/my/target/ce;->a(Lcom/my/target/ah;Landroid/content/Context;)V

    .line 229
    iget-object v0, p0, Lcom/my/target/core/engines/g$a;->q:Lcom/my/target/core/engines/g;

    invoke-static {v0}, Lcom/my/target/core/engines/g;->b(Lcom/my/target/core/engines/g;)Lcom/my/target/ads/InterstitialSliderAd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/my/target/ads/InterstitialSliderAd;->getListener()Lcom/my/target/ads/InterstitialSliderAd$InterstitialSliderAdListener;

    move-result-object v0

    .line 230
    if-eqz v0, :cond_1

    .line 232
    iget-object v1, p0, Lcom/my/target/core/engines/g$a;->q:Lcom/my/target/core/engines/g;

    invoke-static {v1}, Lcom/my/target/core/engines/g;->b(Lcom/my/target/core/engines/g;)Lcom/my/target/ads/InterstitialSliderAd;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/my/target/ads/InterstitialSliderAd$InterstitialSliderAdListener;->onClick(Lcom/my/target/ads/InterstitialSliderAd;)V

    goto :goto_0
.end method

.method public final b(Lcom/my/target/core/models/banners/g;)V
    .locals 3

    .prologue
    .line 245
    const/4 v0, 0x0

    .line 246
    iget-object v1, p0, Lcom/my/target/core/engines/g$a;->q:Lcom/my/target/core/engines/g;

    invoke-static {v1}, Lcom/my/target/core/engines/g;->a(Lcom/my/target/core/engines/g;)Ljava/lang/ref/WeakReference;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 248
    iget-object v0, p0, Lcom/my/target/core/engines/g$a;->q:Lcom/my/target/core/engines/g;

    invoke-static {v0}, Lcom/my/target/core/engines/g;->a(Lcom/my/target/core/engines/g;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/core/presenters/g;

    .line 250
    :cond_0
    if-nez v0, :cond_2

    .line 260
    :cond_1
    :goto_0
    return-void

    .line 254
    :cond_2
    invoke-virtual {v0}, Lcom/my/target/core/presenters/g;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 255
    iget-object v1, p0, Lcom/my/target/core/engines/g$a;->q:Lcom/my/target/core/engines/g;

    invoke-static {v1}, Lcom/my/target/core/engines/g;->c(Lcom/my/target/core/engines/g;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 257
    iget-object v1, p0, Lcom/my/target/core/engines/g$a;->q:Lcom/my/target/core/engines/g;

    invoke-static {v1}, Lcom/my/target/core/engines/g;->c(Lcom/my/target/core/engines/g;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 258
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/g;->getStatHolder()Lcom/my/target/ar;

    move-result-object v1

    const-string v2, "playbackStarted"

    invoke-virtual {v1, v2}, Lcom/my/target/ar;->w(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/my/target/cl;->a(Ljava/util/List;Landroid/content/Context;)V

    goto :goto_0
.end method

.method public final bh()V
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lcom/my/target/core/engines/g$a;->q:Lcom/my/target/core/engines/g;

    invoke-virtual {v0}, Lcom/my/target/core/engines/g;->dismiss()V

    .line 240
    return-void
.end method
