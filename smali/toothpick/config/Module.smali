.class public Ltoothpick/config/Module;
.super Ljava/lang/Object;
.source "Module.java"


# instance fields
.field private bindingSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ltoothpick/config/Binding;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Ltoothpick/config/Module;->bindingSet:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method public bind(Ljava/lang/Class;)Ltoothpick/config/Binding;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Ltoothpick/config/Binding",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 82
    .local p1, "key":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    new-instance v0, Ltoothpick/config/Binding;

    invoke-direct {v0, p1}, Ltoothpick/config/Binding;-><init>(Ljava/lang/Class;)V

    .line 83
    .local v0, "binding":Ltoothpick/config/Binding;, "Ltoothpick/config/Binding<TT;>;"
    iget-object v1, p0, Ltoothpick/config/Module;->bindingSet:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 84
    return-object v0
.end method

.method public getBindingSet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ltoothpick/config/Binding;",
            ">;"
        }
    .end annotation

    .prologue
    .line 88
    iget-object v0, p0, Ltoothpick/config/Module;->bindingSet:Ljava/util/Set;

    return-object v0
.end method
