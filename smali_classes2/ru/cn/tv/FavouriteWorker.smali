.class public Lru/cn/tv/FavouriteWorker;
.super Landroid/os/AsyncTask;
.source "FavouriteWorker.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Boolean;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final channelId:J

.field private final context:Landroid/content/Context;

.field private isFavourite:Z


# direct methods
.method public constructor <init>(JZLandroid/content/Context;)V
    .locals 1
    .param p1, "channelId"    # J
    .param p3, "isFavourite"    # Z
    .param p4, "context"    # Landroid/content/Context;

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 15
    iput-object p4, p0, Lru/cn/tv/FavouriteWorker;->context:Landroid/content/Context;

    .line 16
    iput-wide p1, p0, Lru/cn/tv/FavouriteWorker;->channelId:J

    .line 17
    iput-boolean p3, p0, Lru/cn/tv/FavouriteWorker;->isFavourite:Z

    .line 18
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 8
    check-cast p1, [Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lru/cn/tv/FavouriteWorker;->doInBackground([Ljava/lang/Boolean;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Boolean;)Ljava/lang/Void;
    .locals 5
    .param p1, "params"    # [Ljava/lang/Boolean;

    .prologue
    const/4 v4, 0x0

    .line 22
    const/4 v2, 0x0

    aget-object v2, p1, v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 23
    .local v1, "favourite":Z
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    .line 24
    .local v0, "b":Landroid/net/Uri$Builder;
    const-string v2, "content"

    invoke-virtual {v0, v2}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 25
    const-string v2, "ru.cn.api.tv"

    invoke-virtual {v0, v2}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 26
    const-string v2, "favourite_channels"

    invoke-virtual {v0, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 27
    iget-wide v2, p0, Lru/cn/tv/FavouriteWorker;->channelId:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 29
    if-eqz v1, :cond_0

    .line 30
    iget-object v2, p0, Lru/cn/tv/FavouriteWorker;->context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 34
    :goto_0
    return-object v4

    .line 32
    :cond_0
    iget-object v2, p0, Lru/cn/tv/FavouriteWorker;->context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3, v4, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0
.end method

.method public toggleFavourite()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 38
    iget-boolean v0, p0, Lru/cn/tv/FavouriteWorker;->isFavourite:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lru/cn/tv/FavouriteWorker;->isFavourite:Z

    .line 39
    new-array v0, v1, [Ljava/lang/Boolean;

    iget-boolean v1, p0, Lru/cn/tv/FavouriteWorker;->isFavourite:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-virtual {p0, v0}, Lru/cn/tv/FavouriteWorker;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 40
    return-void

    :cond_0
    move v0, v2

    .line 38
    goto :goto_0
.end method
