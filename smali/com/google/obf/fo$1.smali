.class final Lcom/google/obf/fo$1;
.super Ljava/lang/Object;
.source "IMASDK"

# interfaces
.implements Lcom/google/obf/ex;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/obf/fo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/obf/eg;Lcom/google/obf/gd;)Lcom/google/obf/ew;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/obf/eg;",
            "Lcom/google/obf/gd",
            "<TT;>;)",
            "Lcom/google/obf/ew",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 2
    invoke-virtual {p2}, Lcom/google/obf/gd;->b()Ljava/lang/reflect/Type;

    move-result-object v1

    .line 3
    instance-of v0, v1, Ljava/lang/reflect/GenericArrayType;

    if-nez v0, :cond_1

    instance-of v0, v1, Ljava/lang/Class;

    if-eqz v0, :cond_0

    move-object v0, v1

    check-cast v0, Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->isArray()Z

    move-result v0

    if-nez v0, :cond_1

    .line 4
    :cond_0
    const/4 v0, 0x0

    .line 8
    :goto_0
    return-object v0

    .line 5
    :cond_1
    invoke-static {v1}, Lcom/google/obf/fe;->g(Ljava/lang/reflect/Type;)Ljava/lang/reflect/Type;

    move-result-object v1

    .line 6
    invoke-static {v1}, Lcom/google/obf/gd;->a(Ljava/lang/reflect/Type;)Lcom/google/obf/gd;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/obf/eg;->a(Lcom/google/obf/gd;)Lcom/google/obf/ew;

    move-result-object v2

    .line 7
    new-instance v0, Lcom/google/obf/fo;

    .line 8
    invoke-static {v1}, Lcom/google/obf/fe;->e(Ljava/lang/reflect/Type;)Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, p1, v2, v1}, Lcom/google/obf/fo;-><init>(Lcom/google/obf/eg;Lcom/google/obf/ew;Ljava/lang/Class;)V

    goto :goto_0
.end method
