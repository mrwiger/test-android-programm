.class Lcom/yandex/metrica/impl/ba$b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/yandex/metrica/impl/ba;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final b:Lcom/yandex/metrica/impl/ba$d;

.field c:Z

.field final synthetic d:Lcom/yandex/metrica/impl/ba;


# direct methods
.method private constructor <init>(Lcom/yandex/metrica/impl/ba;Lcom/yandex/metrica/impl/ba$d;)V
    .locals 3

    .prologue
    .line 69
    iput-object p1, p0, Lcom/yandex/metrica/impl/ba$b;->d:Lcom/yandex/metrica/impl/ba;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    iput-object p2, p0, Lcom/yandex/metrica/impl/ba$b;->b:Lcom/yandex/metrica/impl/ba$d;

    .line 71
    invoke-static {}, Lcom/yandex/metrica/impl/ob/g;->a()Lcom/yandex/metrica/impl/ob/g;

    move-result-object v0

    const-class v1, Lcom/yandex/metrica/impl/ap;

    new-instance v2, Lcom/yandex/metrica/impl/ba$b$1;

    invoke-direct {v2, p0}, Lcom/yandex/metrica/impl/ba$b$1;-><init>(Lcom/yandex/metrica/impl/ba$b;)V

    invoke-static {v2}, Lcom/yandex/metrica/impl/ob/k;->a(Lcom/yandex/metrica/impl/ob/j;)Lcom/yandex/metrica/impl/ob/k$a;

    move-result-object v2

    .line 76
    invoke-virtual {v2}, Lcom/yandex/metrica/impl/ob/k$a;->a()Lcom/yandex/metrica/impl/ob/k;

    move-result-object v2

    .line 71
    invoke-virtual {v0, p0, v1, v2}, Lcom/yandex/metrica/impl/ob/g;->a(Ljava/lang/Object;Ljava/lang/Class;Lcom/yandex/metrica/impl/ob/k;)V

    .line 77
    return-void
.end method

.method synthetic constructor <init>(Lcom/yandex/metrica/impl/ba;Lcom/yandex/metrica/impl/ba$d;B)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Lcom/yandex/metrica/impl/ba$b;-><init>(Lcom/yandex/metrica/impl/ba;Lcom/yandex/metrica/impl/ba$d;)V

    return-void
.end method

.method private a(Lcom/yandex/metrica/IMetricaService;Lcom/yandex/metrica/impl/ba$d;)Z
    .locals 3

    .prologue
    .line 114
    :try_start_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ba$b;->d:Lcom/yandex/metrica/impl/ba;

    invoke-static {v0}, Lcom/yandex/metrica/impl/ba;->b(Lcom/yandex/metrica/impl/ba;)Lcom/yandex/metrica/impl/t;

    move-result-object v0

    invoke-virtual {p2}, Lcom/yandex/metrica/impl/ba$d;->b()Lcom/yandex/metrica/impl/i;

    move-result-object v1

    invoke-static {p2}, Lcom/yandex/metrica/impl/ba$d;->a(Lcom/yandex/metrica/impl/ba$d;)Lcom/yandex/metrica/impl/ax;

    move-result-object v2

    invoke-interface {v0, p1, v1, v2}, Lcom/yandex/metrica/impl/t;->a(Lcom/yandex/metrica/IMetricaService;Lcom/yandex/metrica/impl/i;Lcom/yandex/metrica/impl/ax;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 119
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 117
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()Ljava/lang/Void;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 82
    const/4 v0, 0x0

    .line 84
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/yandex/metrica/impl/ba$b;->d:Lcom/yandex/metrica/impl/ba;

    invoke-static {v1}, Lcom/yandex/metrica/impl/ba;->a(Lcom/yandex/metrica/impl/ba;)Lcom/yandex/metrica/impl/ad;

    move-result-object v1

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ad;->e()Lcom/yandex/metrica/IMetricaService;

    move-result-object v1

    .line 85
    if-eqz v1, :cond_1

    iget-object v2, p0, Lcom/yandex/metrica/impl/ba$b;->b:Lcom/yandex/metrica/impl/ba$d;

    invoke-direct {p0, v1, v2}, Lcom/yandex/metrica/impl/ba$b;->a(Lcom/yandex/metrica/IMetricaService;Lcom/yandex/metrica/impl/ba$d;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_1

    .line 101
    invoke-static {}, Lcom/yandex/metrica/impl/ob/g;->a()Lcom/yandex/metrica/impl/ob/g;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/yandex/metrica/impl/ob/g;->a(Ljava/lang/Object;)V

    .line 103
    :goto_0
    return-object v3

    .line 89
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ba$b;->b()Z

    move-result v1

    .line 91
    add-int/lit8 v0, v0, 0x1

    .line 92
    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lcom/yandex/metrica/impl/ba$b;->c:Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v1, :cond_2

    const/4 v1, 0x3

    if-lt v0, v1, :cond_0

    .line 101
    :cond_2
    invoke-static {}, Lcom/yandex/metrica/impl/ob/g;->a()Lcom/yandex/metrica/impl/ob/g;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/yandex/metrica/impl/ob/g;->a(Ljava/lang/Object;)V

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {}, Lcom/yandex/metrica/impl/ob/g;->a()Lcom/yandex/metrica/impl/ob/g;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/yandex/metrica/impl/ob/g;->a(Ljava/lang/Object;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {}, Lcom/yandex/metrica/impl/ob/g;->a()Lcom/yandex/metrica/impl/ob/g;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/yandex/metrica/impl/ob/g;->a(Ljava/lang/Object;)V

    .line 102
    throw v0
.end method

.method b()Z
    .locals 5

    .prologue
    .line 107
    iget-object v0, p0, Lcom/yandex/metrica/impl/ba$b;->d:Lcom/yandex/metrica/impl/ba;

    invoke-static {v0}, Lcom/yandex/metrica/impl/ba;->a(Lcom/yandex/metrica/impl/ba;)Lcom/yandex/metrica/impl/ad;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ad;->a()V

    .line 1123
    iget-object v0, p0, Lcom/yandex/metrica/impl/ba$b;->d:Lcom/yandex/metrica/impl/ba;

    invoke-static {v0}, Lcom/yandex/metrica/impl/ba;->c(Lcom/yandex/metrica/impl/ba;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 1124
    :try_start_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ba$b;->d:Lcom/yandex/metrica/impl/ba;

    invoke-static {v0}, Lcom/yandex/metrica/impl/ba;->a(Lcom/yandex/metrica/impl/ba;)Lcom/yandex/metrica/impl/ad;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ad;->d()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 1126
    :try_start_1
    iget-object v0, p0, Lcom/yandex/metrica/impl/ba$b;->d:Lcom/yandex/metrica/impl/ba;

    invoke-static {v0}, Lcom/yandex/metrica/impl/ba;->c(Lcom/yandex/metrica/impl/ba;)Ljava/lang/Object;

    move-result-object v0

    const-wide/16 v2, 0x1f4

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v4}, Ljava/lang/Object;->wait(JI)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1132
    :cond_0
    :goto_0
    :try_start_2
    monitor-exit v1

    .line 109
    const/4 v0, 0x1

    return v0

    .line 1128
    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/yandex/metrica/impl/ba$b;->d:Lcom/yandex/metrica/impl/ba;

    invoke-static {v0}, Lcom/yandex/metrica/impl/ba;->c(Lcom/yandex/metrica/impl/ba;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 1129
    iget-object v0, p0, Lcom/yandex/metrica/impl/ba$b;->d:Lcom/yandex/metrica/impl/ba;

    invoke-static {v0}, Lcom/yandex/metrica/impl/ba;->d(Lcom/yandex/metrica/impl/ba;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    goto :goto_0

    .line 1132
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 63
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ba$b;->a()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
