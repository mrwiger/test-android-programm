.class public Lcom/my/target/cb;
.super Landroid/widget/LinearLayout;
.source "WebViewBrowser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/cb$b;,
        Lcom/my/target/cb$a;
    }
.end annotation


# static fields
.field public static final iX:I

.field public static final iY:I

.field public static final iZ:Ljava/lang/String; = "Open outside"

.field public static final ja:Ljava/lang/String; = "Close"


# instance fields
.field private final iG:Lcom/my/target/cm;

.field private final jb:Landroid/widget/ImageButton;

.field private final jc:Landroid/widget/LinearLayout;

.field private final jd:Landroid/widget/TextView;

.field private final je:Landroid/widget/TextView;

.field private final jf:Landroid/widget/FrameLayout;

.field private final jg:Landroid/view/View;

.field private final jh:Landroid/widget/FrameLayout;

.field private final ji:Landroid/widget/ImageButton;

.field private final jj:Landroid/widget/RelativeLayout;

.field private final jk:Landroid/webkit/WebView;

.field private final jl:Landroid/widget/ProgressBar;

.field private jm:Lcom/my/target/cb$b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/cb;->iX:I

    .line 50
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/cb;->iY:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 80
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 81
    new-instance v0, Landroid/widget/RelativeLayout;

    invoke-direct {v0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/cb;->jj:Landroid/widget/RelativeLayout;

    .line 82
    new-instance v0, Landroid/webkit/WebView;

    invoke-direct {v0, p1}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/cb;->jk:Landroid/webkit/WebView;

    .line 83
    new-instance v0, Landroid/widget/ImageButton;

    invoke-direct {v0, p1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/cb;->jb:Landroid/widget/ImageButton;

    .line 84
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/cb;->jc:Landroid/widget/LinearLayout;

    .line 85
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/cb;->jd:Landroid/widget/TextView;

    .line 86
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/cb;->je:Landroid/widget/TextView;

    .line 87
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-direct {v0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/cb;->jf:Landroid/widget/FrameLayout;

    .line 88
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-direct {v0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/cb;->jh:Landroid/widget/FrameLayout;

    .line 89
    new-instance v0, Landroid/widget/ImageButton;

    invoke-direct {v0, p1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/cb;->ji:Landroid/widget/ImageButton;

    .line 90
    new-instance v0, Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    const v2, 0x1010078

    invoke-direct {v0, p1, v1, v2}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lcom/my/target/cb;->jl:Landroid/widget/ProgressBar;

    .line 91
    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/cb;->jg:Landroid/view/View;

    .line 93
    invoke-static {p1}, Lcom/my/target/cm;->x(Landroid/content/Context;)Lcom/my/target/cm;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/cb;->iG:Lcom/my/target/cm;

    .line 94
    return-void
.end method

.method private K(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 295
    .line 298
    :try_start_0
    new-instance v0, Ljava/net/URI;

    invoke-direct {v0, p1}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    .line 299
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/net/URI;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "://"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p1

    .line 306
    :goto_0
    return-object p1

    .line 301
    :catch_0
    move-exception v0

    .line 303
    invoke-static {v0}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/my/target/cb;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/my/target/cb;->jd:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic a(Lcom/my/target/cb;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/my/target/cb;->K(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/my/target/cb;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/my/target/cb;->je:Landroid/widget/TextView;

    return-object v0
.end method

.method private bf()V
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x1

    const/4 v7, -0x2

    const/4 v6, -0x1

    .line 178
    invoke-virtual {p0, v8}, Lcom/my/target/cb;->setOrientation(I)V

    .line 179
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Lcom/my/target/cb;->setGravity(I)V

    .line 181
    new-instance v1, Lcom/my/target/cb$a;

    const/4 v0, 0x0

    invoke-direct {v1, p0, v0}, Lcom/my/target/cb$a;-><init>(Lcom/my/target/cb;Lcom/my/target/cb$1;)V

    .line 183
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v6, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 185
    iget-object v2, p0, Lcom/my/target/cb;->jk:Landroid/webkit/WebView;

    invoke-virtual {v2, v0}, Landroid/webkit/WebView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 187
    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    .line 188
    iget-object v0, p0, Lcom/my/target/cb;->iG:Lcom/my/target/cm;

    const/16 v3, 0x32

    invoke-virtual {v0, v3}, Lcom/my/target/cm;->n(I)I

    move-result v0

    .line 189
    invoke-virtual {p0}, Lcom/my/target/cb;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v3

    const v4, 0x10102eb

    invoke-virtual {v3, v4, v2, v8}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 191
    iget v0, v2, Landroid/util/TypedValue;->data:I

    invoke-virtual {p0}, Lcom/my/target/cb;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/TypedValue;->complexToDimensionPixelSize(ILandroid/util/DisplayMetrics;)I

    move-result v0

    .line 194
    :cond_0
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v6, v0}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 196
    iget-object v3, p0, Lcom/my/target/cb;->jj:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v2}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 197
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v0, v0}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 198
    iget-object v3, p0, Lcom/my/target/cb;->jf:Landroid/widget/FrameLayout;

    invoke-virtual {v3, v2}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 199
    iget-object v2, p0, Lcom/my/target/cb;->jf:Landroid/widget/FrameLayout;

    sget v3, Lcom/my/target/cb;->iX:I

    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->setId(I)V

    .line 201
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v2, v6, v6}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 203
    const/16 v3, 0x11

    iput v3, v2, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 204
    iget-object v3, p0, Lcom/my/target/cb;->jb:Landroid/widget/ImageButton;

    invoke-virtual {v3, v2}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 205
    iget-object v2, p0, Lcom/my/target/cb;->jb:Landroid/widget/ImageButton;

    div-int/lit8 v3, v0, 0x4

    iget-object v4, p0, Lcom/my/target/cb;->iG:Lcom/my/target/cm;

    invoke-virtual {v4, v10}, Lcom/my/target/cm;->n(I)I

    move-result v4

    invoke-static {v3, v4}, Lcom/my/target/bq;->b(II)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 206
    iget-object v2, p0, Lcom/my/target/cb;->jb:Landroid/widget/ImageButton;

    const-string v3, "Close"

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 207
    iget-object v2, p0, Lcom/my/target/cb;->jb:Landroid/widget/ImageButton;

    invoke-virtual {v2, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 209
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v0, v0}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 211
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x12

    if-lt v0, v3, :cond_1

    .line 213
    const/16 v0, 0x15

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 220
    :goto_0
    iget-object v0, p0, Lcom/my/target/cb;->jh:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 221
    iget-object v0, p0, Lcom/my/target/cb;->jh:Landroid/widget/FrameLayout;

    sget v2, Lcom/my/target/cb;->iY:I

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setId(I)V

    .line 222
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v6, v6}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 224
    const/16 v2, 0x11

    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 225
    iget-object v2, p0, Lcom/my/target/cb;->ji:Landroid/widget/ImageButton;

    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 226
    iget-object v0, p0, Lcom/my/target/cb;->ji:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/my/target/cb;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/my/target/bq;->q(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 227
    iget-object v0, p0, Lcom/my/target/cb;->ji:Landroid/widget/ImageButton;

    sget-object v2, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 228
    iget-object v0, p0, Lcom/my/target/cb;->ji:Landroid/widget/ImageButton;

    const-string v2, "Open outside"

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 230
    iget-object v0, p0, Lcom/my/target/cb;->ji:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 232
    iget-object v0, p0, Lcom/my/target/cb;->jb:Landroid/widget/ImageButton;

    const v1, -0x333334

    invoke-static {v0, v9, v1}, Lcom/my/target/cm;->a(Landroid/view/View;II)V

    .line 233
    iget-object v0, p0, Lcom/my/target/cb;->ji:Landroid/widget/ImageButton;

    const v1, -0x333334

    invoke-static {v0, v9, v1}, Lcom/my/target/cm;->a(Landroid/view/View;II)V

    .line 235
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v7, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 237
    const/16 v1, 0xf

    invoke-virtual {v0, v1, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 238
    sget v1, Lcom/my/target/cb;->iX:I

    invoke-virtual {v0, v8, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 239
    sget v1, Lcom/my/target/cb;->iY:I

    invoke-virtual {v0, v9, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 240
    iget-object v1, p0, Lcom/my/target/cb;->jc:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 241
    iget-object v0, p0, Lcom/my/target/cb;->jc:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 242
    iget-object v0, p0, Lcom/my/target/cb;->jc:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/my/target/cb;->iG:Lcom/my/target/cm;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iget-object v2, p0, Lcom/my/target/cb;->iG:Lcom/my/target/cm;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lcom/my/target/cm;->n(I)I

    move-result v2

    iget-object v3, p0, Lcom/my/target/cb;->iG:Lcom/my/target/cm;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Lcom/my/target/cm;->n(I)I

    move-result v3

    iget-object v4, p0, Lcom/my/target/cb;->iG:Lcom/my/target/cm;

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Lcom/my/target/cm;->n(I)I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 244
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v7, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 246
    iget-object v1, p0, Lcom/my/target/cb;->je:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 247
    iget-object v1, p0, Lcom/my/target/cb;->je:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 248
    iget-object v0, p0, Lcom/my/target/cb;->je:Landroid/widget/TextView;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 249
    iget-object v0, p0, Lcom/my/target/cb;->je:Landroid/widget/TextView;

    const/high16 v1, 0x41900000    # 18.0f

    invoke-virtual {v0, v10, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 250
    iget-object v0, p0, Lcom/my/target/cb;->je:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->setSingleLine()V

    .line 251
    iget-object v0, p0, Lcom/my/target/cb;->je:Landroid/widget/TextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->MIDDLE:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 253
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v7, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 255
    iget-object v1, p0, Lcom/my/target/cb;->jd:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 256
    iget-object v0, p0, Lcom/my/target/cb;->jd:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->setSingleLine()V

    .line 257
    iget-object v0, p0, Lcom/my/target/cb;->jd:Landroid/widget/TextView;

    const/high16 v1, 0x41400000    # 12.0f

    invoke-virtual {v0, v10, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 258
    iget-object v0, p0, Lcom/my/target/cb;->jd:Landroid/widget/TextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->MIDDLE:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 260
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    const v1, -0xfc560c

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 261
    new-instance v1, Landroid/graphics/drawable/ClipDrawable;

    const v2, 0x800003

    invoke-direct {v1, v0, v2, v8}, Landroid/graphics/drawable/ClipDrawable;-><init>(Landroid/graphics/drawable/Drawable;II)V

    .line 263
    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    const v0, -0x1e0a02

    invoke-direct {v2, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 264
    iget-object v0, p0, Lcom/my/target/cb;->jl:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->getProgressDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/LayerDrawable;

    .line 265
    const/high16 v3, 0x1020000

    invoke-virtual {v0, v3, v2}, Landroid/graphics/drawable/LayerDrawable;->setDrawableByLayerId(ILandroid/graphics/drawable/Drawable;)Z

    .line 266
    const v2, 0x102000d

    invoke-virtual {v0, v2, v1}, Landroid/graphics/drawable/LayerDrawable;->setDrawableByLayerId(ILandroid/graphics/drawable/Drawable;)Z

    .line 268
    iget-object v1, p0, Lcom/my/target/cb;->jl:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setProgressDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 269
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v1, p0, Lcom/my/target/cb;->iG:Lcom/my/target/cm;

    invoke-virtual {v1, v10}, Lcom/my/target/cm;->n(I)I

    move-result v1

    invoke-direct {v0, v6, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 270
    iget-object v1, p0, Lcom/my/target/cb;->jl:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 271
    iget-object v0, p0, Lcom/my/target/cb;->jl:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v9}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 273
    iget-object v0, p0, Lcom/my/target/cb;->jc:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/my/target/cb;->je:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 274
    iget-object v0, p0, Lcom/my/target/cb;->jc:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/my/target/cb;->jd:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 275
    iget-object v0, p0, Lcom/my/target/cb;->jf:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/my/target/cb;->jb:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 276
    iget-object v0, p0, Lcom/my/target/cb;->jh:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/my/target/cb;->ji:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 277
    iget-object v0, p0, Lcom/my/target/cb;->jj:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/my/target/cb;->jf:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 278
    iget-object v0, p0, Lcom/my/target/cb;->jj:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/my/target/cb;->jc:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 279
    iget-object v0, p0, Lcom/my/target/cb;->jj:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/my/target/cb;->jh:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 281
    iget-object v0, p0, Lcom/my/target/cb;->jj:Landroid/widget/RelativeLayout;

    invoke-virtual {p0, v0}, Lcom/my/target/cb;->addView(Landroid/view/View;)V

    .line 283
    iget-object v0, p0, Lcom/my/target/cb;->jg:Landroid/view/View;

    const v1, -0x555556

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 284
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v6, v8}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 285
    iget-object v1, p0, Lcom/my/target/cb;->jg:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 287
    iget-object v1, p0, Lcom/my/target/cb;->jg:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 288
    iget-object v0, p0, Lcom/my/target/cb;->jl:Landroid/widget/ProgressBar;

    invoke-virtual {p0, v0}, Lcom/my/target/cb;->addView(Landroid/view/View;)V

    .line 289
    iget-object v0, p0, Lcom/my/target/cb;->jg:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/my/target/cb;->addView(Landroid/view/View;)V

    .line 290
    iget-object v0, p0, Lcom/my/target/cb;->jk:Landroid/webkit/WebView;

    invoke-virtual {p0, v0}, Lcom/my/target/cb;->addView(Landroid/view/View;)V

    .line 291
    return-void

    .line 217
    :cond_1
    const/16 v0, 0xb

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    goto/16 :goto_0
.end method

.method private bg()V
    .locals 4

    .prologue
    .line 311
    iget-object v0, p0, Lcom/my/target/cb;->jk:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getUrl()Ljava/lang/String;

    move-result-object v0

    .line 312
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 316
    :try_start_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 317
    invoke-virtual {p0}, Lcom/my/target/cb;->getContext()Landroid/content/Context;

    move-result-object v2

    instance-of v2, v2, Landroid/app/Activity;

    if-nez v2, :cond_0

    .line 319
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 321
    :cond_0
    invoke-virtual {p0}, Lcom/my/target/cb;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 328
    :cond_1
    :goto_0
    return-void

    .line 323
    :catch_0
    move-exception v1

    .line 325
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unable to open url "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic c(Lcom/my/target/cb;)Landroid/widget/ProgressBar;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/my/target/cb;->jl:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic d(Lcom/my/target/cb;)Landroid/view/View;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/my/target/cb;->jg:Landroid/view/View;

    return-object v0
.end method

.method static synthetic e(Lcom/my/target/cb;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/my/target/cb;->jb:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic f(Lcom/my/target/cb;)Lcom/my/target/cb$b;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/my/target/cb;->jm:Lcom/my/target/cb$b;

    return-object v0
.end method

.method static synthetic g(Lcom/my/target/cb;)Landroid/widget/ImageButton;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/my/target/cb;->ji:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic h(Lcom/my/target/cb;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/my/target/cb;->bg()V

    return-void
.end method


# virtual methods
.method public be()V
    .locals 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetJavaScriptEnabled"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 122
    iget-object v0, p0, Lcom/my/target/cb;->jk:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    .line 123
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 124
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setLoadWithOverviewMode(Z)V

    .line 125
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setUseWideViewPort(Z)V

    .line 126
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setBuiltInZoomControls(Z)V

    .line 127
    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setDisplayZoomControls(Z)V

    .line 128
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-lt v1, v2, :cond_0

    .line 130
    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setAllowFileAccessFromFileURLs(Z)V

    .line 131
    invoke-virtual {v0, v4}, Landroid/webkit/WebSettings;->setAllowUniversalAccessFromFileURLs(Z)V

    .line 133
    :cond_0
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setDomStorageEnabled(Z)V

    .line 134
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setAppCacheEnabled(Z)V

    .line 135
    invoke-virtual {p0}, Lcom/my/target/cb;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setAppCachePath(Ljava/lang/String;)V

    .line 136
    iget-object v0, p0, Lcom/my/target/cb;->jk:Landroid/webkit/WebView;

    new-instance v1, Lcom/my/target/cb$1;

    invoke-direct {v1, p0}, Lcom/my/target/cb$1;-><init>(Lcom/my/target/cb;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 146
    iget-object v0, p0, Lcom/my/target/cb;->jk:Landroid/webkit/WebView;

    new-instance v1, Lcom/my/target/cb$2;

    invoke-direct {v1, p0}, Lcom/my/target/cb$2;-><init>(Lcom/my/target/cb;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 173
    invoke-direct {p0}, Lcom/my/target/cb;->bf()V

    .line 174
    return-void
.end method

.method public canGoBack()Z
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/my/target/cb;->jk:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->canGoBack()Z

    move-result v0

    return v0
.end method

.method public destroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 108
    iget-object v0, p0, Lcom/my/target/cb;->jk:Landroid/webkit/WebView;

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 109
    iget-object v0, p0, Lcom/my/target/cb;->jk:Landroid/webkit/WebView;

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 110
    iget-object v0, p0, Lcom/my/target/cb;->jk:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->destroy()V

    .line 111
    return-void
.end method

.method public getListener()Lcom/my/target/cb$b;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/my/target/cb;->jm:Lcom/my/target/cb$b;

    return-object v0
.end method

.method public goBack()V
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/my/target/cb;->jk:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->goBack()V

    .line 104
    return-void
.end method

.method public setListener(Lcom/my/target/cb$b;)V
    .locals 0
    .param p1, "listener"    # Lcom/my/target/cb$b;

    .prologue
    .line 75
    iput-object p1, p0, Lcom/my/target/cb;->jm:Lcom/my/target/cb$b;

    .line 76
    return-void
.end method

.method public setUrl(Ljava/lang/String;)V
    .locals 2
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 115
    iget-object v0, p0, Lcom/my/target/cb;->jk:Landroid/webkit/WebView;

    invoke-virtual {v0, p1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 116
    iget-object v0, p0, Lcom/my/target/cb;->jd:Landroid/widget/TextView;

    invoke-direct {p0, p1}, Lcom/my/target/cb;->K(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 117
    return-void
.end method
