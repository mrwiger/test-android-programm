.class public Lru/cn/api/BaseAPI;
.super Ljava/lang/Object;
.source "BaseAPI.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/api/BaseAPI$ParseException;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method protected static getContent(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "requestUri"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/net/URISyntaxException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 28
    new-instance v0, Lru/cn/utils/http/HttpClient;

    invoke-direct {v0}, Lru/cn/utils/http/HttpClient;-><init>()V

    .line 29
    .local v0, "httpClient":Lru/cn/utils/http/HttpClient;
    invoke-virtual {v0, p0}, Lru/cn/utils/http/HttpClient;->sendRequest(Ljava/lang/String;)V

    .line 31
    invoke-virtual {v0}, Lru/cn/utils/http/HttpClient;->getStatusCode()I

    move-result v1

    const/16 v2, 0xc8

    if-eq v1, v2, :cond_0

    .line 32
    invoke-virtual {v0}, Lru/cn/utils/http/HttpClient;->close()V

    .line 34
    new-instance v1, Lru/cn/utils/http/HttpException;

    invoke-virtual {v0}, Lru/cn/utils/http/HttpClient;->getStatusCode()I

    move-result v2

    invoke-direct {v1, v2}, Lru/cn/utils/http/HttpException;-><init>(I)V

    throw v1

    .line 37
    :cond_0
    invoke-virtual {v0}, Lru/cn/utils/http/HttpClient;->getContent()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
