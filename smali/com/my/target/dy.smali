.class public final Lcom/my/target/dy;
.super Landroid/view/ViewGroup;
.source "TextViewWithAgeView.java"


# static fields
.field private static final aA:I

.field private static final az:I


# instance fields
.field private final aB:Landroid/widget/TextView;

.field private final aC:Lcom/my/target/bu;

.field private final aD:I

.field private final aE:I

.field private final aw:Lcom/my/target/cm;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/dy;->az:I

    .line 18
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/dy;->aA:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 30
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 31
    invoke-static {p1}, Lcom/my/target/cm;->x(Landroid/content/Context;)Lcom/my/target/cm;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/dy;->aw:Lcom/my/target/cm;

    .line 32
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/dy;->aB:Landroid/widget/TextView;

    .line 33
    new-instance v0, Lcom/my/target/bu;

    invoke-direct {v0, p1}, Lcom/my/target/bu;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/dy;->aC:Lcom/my/target/bu;

    .line 35
    iget-object v0, p0, Lcom/my/target/dy;->aB:Landroid/widget/TextView;

    sget v1, Lcom/my/target/dy;->az:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setId(I)V

    .line 36
    iget-object v0, p0, Lcom/my/target/dy;->aC:Lcom/my/target/bu;

    sget v1, Lcom/my/target/dy;->aA:I

    invoke-virtual {v0, v1}, Lcom/my/target/bu;->setId(I)V

    .line 37
    iget-object v0, p0, Lcom/my/target/dy;->aC:Lcom/my/target/bu;

    invoke-virtual {v0, v2}, Lcom/my/target/bu;->setLines(I)V

    .line 39
    iget-object v0, p0, Lcom/my/target/dy;->aB:Landroid/widget/TextView;

    const/high16 v1, 0x41900000    # 18.0f

    invoke-virtual {v0, v3, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 40
    iget-object v0, p0, Lcom/my/target/dy;->aB:Landroid/widget/TextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 41
    iget-object v0, p0, Lcom/my/target/dy;->aB:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 42
    iget-object v0, p0, Lcom/my/target/dy;->aB:Landroid/widget/TextView;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 44
    iget-object v0, p0, Lcom/my/target/dy;->aw:Lcom/my/target/cm;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/my/target/cm;->n(I)I

    move-result v0

    iput v0, p0, Lcom/my/target/dy;->aD:I

    .line 45
    iget-object v0, p0, Lcom/my/target/dy;->aw:Lcom/my/target/cm;

    invoke-virtual {v0, v3}, Lcom/my/target/cm;->n(I)I

    move-result v0

    iput v0, p0, Lcom/my/target/dy;->aE:I

    .line 47
    iget-object v0, p0, Lcom/my/target/dy;->aB:Landroid/widget/TextView;

    const-string v1, "title_text"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 48
    iget-object v0, p0, Lcom/my/target/dy;->aC:Lcom/my/target/bu;

    const-string v1, "age_bordering"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 50
    iget-object v0, p0, Lcom/my/target/dy;->aB:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/my/target/dy;->addView(Landroid/view/View;)V

    .line 51
    iget-object v0, p0, Lcom/my/target/dy;->aC:Lcom/my/target/bu;

    invoke-virtual {p0, v0}, Lcom/my/target/dy;->addView(Landroid/view/View;)V

    .line 52
    return-void
.end method


# virtual methods
.method public final getLeftText()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/my/target/dy;->aB:Landroid/widget/TextView;

    return-object v0
.end method

.method public final getRightBorderedView()Lcom/my/target/bu;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/my/target/dy;->aC:Lcom/my/target/bu;

    return-object v0
.end method

.method protected final onLayout(ZIIII)V
    .locals 9

    .prologue
    .line 91
    iget-object v0, p0, Lcom/my/target/dy;->aB:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v0

    .line 92
    iget-object v1, p0, Lcom/my/target/dy;->aB:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v1

    .line 93
    iget-object v2, p0, Lcom/my/target/dy;->aC:Lcom/my/target/bu;

    invoke-virtual {v2}, Lcom/my/target/bu;->getMeasuredWidth()I

    move-result v2

    .line 94
    iget-object v3, p0, Lcom/my/target/dy;->aC:Lcom/my/target/bu;

    invoke-virtual {v3}, Lcom/my/target/bu;->getMeasuredHeight()I

    move-result v3

    .line 95
    invoke-virtual {p0}, Lcom/my/target/dy;->getMeasuredHeight()I

    move-result v4

    .line 97
    sub-int v5, v4, v1

    div-int/lit8 v5, v5, 0x2

    .line 98
    sub-int/2addr v4, v3

    div-int/lit8 v4, v4, 0x2

    .line 99
    iget v6, p0, Lcom/my/target/dy;->aD:I

    add-int/2addr v6, v0

    .line 101
    iget-object v7, p0, Lcom/my/target/dy;->aB:Landroid/widget/TextView;

    const/4 v8, 0x0

    add-int/2addr v1, v5

    invoke-virtual {v7, v8, v5, v0, v1}, Landroid/widget/TextView;->layout(IIII)V

    .line 102
    iget-object v0, p0, Lcom/my/target/dy;->aC:Lcom/my/target/bu;

    add-int v1, v6, v2

    add-int v2, v4, v3

    invoke-virtual {v0, v6, v4, v1, v2}, Lcom/my/target/bu;->layout(IIII)V

    .line 103
    return-void
.end method

.method protected final onMeasure(II)V
    .locals 6
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/high16 v5, -0x80000000

    .line 67
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 68
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 70
    iget-object v2, p0, Lcom/my/target/dy;->aC:Lcom/my/target/bu;

    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    iget v4, p0, Lcom/my/target/dy;->aE:I

    mul-int/lit8 v4, v4, 0x2

    sub-int v4, v1, v4

    .line 71
    invoke-static {v4, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 70
    invoke-virtual {v2, v3, v4}, Lcom/my/target/bu;->measure(II)V

    .line 73
    iget-object v2, p0, Lcom/my/target/dy;->aC:Lcom/my/target/bu;

    invoke-virtual {v2}, Lcom/my/target/bu;->getMeasuredWidth()I

    move-result v2

    div-int/lit8 v3, v0, 0x2

    if-le v2, v3, :cond_0

    .line 75
    iget-object v2, p0, Lcom/my/target/dy;->aC:Lcom/my/target/bu;

    div-int/lit8 v3, v0, 0x2

    invoke-static {v3, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    iget v4, p0, Lcom/my/target/dy;->aE:I

    mul-int/lit8 v4, v4, 0x2

    sub-int v4, v1, v4

    .line 76
    invoke-static {v4, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 75
    invoke-virtual {v2, v3, v4}, Lcom/my/target/bu;->measure(II)V

    .line 78
    :cond_0
    iget-object v2, p0, Lcom/my/target/dy;->aB:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/my/target/dy;->aC:Lcom/my/target/bu;

    invoke-virtual {v3}, Lcom/my/target/bu;->getMeasuredWidth()I

    move-result v3

    sub-int/2addr v0, v3

    iget v3, p0, Lcom/my/target/dy;->aD:I

    sub-int/2addr v0, v3

    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    iget v3, p0, Lcom/my/target/dy;->aE:I

    mul-int/lit8 v3, v3, 0x2

    sub-int/2addr v1, v3

    .line 79
    invoke-static {v1, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 78
    invoke-virtual {v2, v0, v1}, Landroid/widget/TextView;->measure(II)V

    .line 81
    iget-object v0, p0, Lcom/my/target/dy;->aB:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v0

    .line 82
    iget-object v1, p0, Lcom/my/target/dy;->aC:Lcom/my/target/bu;

    invoke-virtual {v1}, Lcom/my/target/bu;->getMeasuredWidth()I

    move-result v1

    .line 84
    add-int/2addr v0, v1

    iget v1, p0, Lcom/my/target/dy;->aD:I

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/my/target/dy;->aB:Landroid/widget/TextView;

    .line 85
    invoke-virtual {v1}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v1

    iget-object v2, p0, Lcom/my/target/dy;->aC:Lcom/my/target/bu;

    invoke-virtual {v2}, Lcom/my/target/bu;->getMeasuredHeight()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 84
    invoke-virtual {p0, v0, v1}, Lcom/my/target/dy;->setMeasuredDimension(II)V

    .line 86
    return-void
.end method
