.class public Lru/cn/api/medialocator/replies/RipPart;
.super Ljava/lang/Object;
.source "RipPart.java"


# instance fields
.field public locations:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "locations"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lru/cn/api/medialocator/replies/Location;",
            ">;"
        }
    .end annotation
.end field

.field public startTime:F
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "playback_start_time"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    const/4 v0, 0x0

    iput v0, p0, Lru/cn/api/medialocator/replies/RipPart;->startTime:F

    return-void
.end method
