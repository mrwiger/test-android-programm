.class public final Lcom/my/target/dg;
.super Ljava/lang/Object;
.source "SectionViewSettings.java"


# instance fields
.field private A:I

.field private B:I

.field private C:I

.field private backgroundColor:I

.field private k:Ljava/lang/String;

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:Z

.field private q:Z

.field private r:I

.field private s:I

.field private t:I

.field private u:I

.field private v:I

.field private w:I

.field private x:I

.field private y:I

.field private z:I


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const v3, -0x777778

    const v2, -0x99999a

    const/4 v1, -0x1

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    const-string v0, "html"

    iput-object v0, p0, Lcom/my/target/dg;->k:Ljava/lang/String;

    .line 1261
    const-string v0, "html"

    iput-object v0, p0, Lcom/my/target/dg;->k:Ljava/lang/String;

    .line 1262
    iput v1, p0, Lcom/my/target/dg;->backgroundColor:I

    .line 1263
    const v0, -0x3a1508

    iput v0, p0, Lcom/my/target/dg;->r:I

    .line 1264
    const v0, -0xffab5a

    iput v0, p0, Lcom/my/target/dg;->s:I

    .line 1265
    iput v1, p0, Lcom/my/target/dg;->t:I

    .line 1266
    iput v2, p0, Lcom/my/target/dg;->u:I

    .line 1267
    const v0, -0x4c4c4d

    iput v0, p0, Lcom/my/target/dg;->v:I

    .line 1268
    const/high16 v0, -0x1000000

    iput v0, p0, Lcom/my/target/dg;->w:I

    .line 1269
    iput v2, p0, Lcom/my/target/dg;->x:I

    .line 1270
    iput v3, p0, Lcom/my/target/dg;->y:I

    .line 1271
    iput v3, p0, Lcom/my/target/dg;->z:I

    .line 1272
    const v0, -0xff5110

    iput v0, p0, Lcom/my/target/dg;->A:I

    .line 1273
    const v0, -0xff8957

    iput v0, p0, Lcom/my/target/dg;->B:I

    .line 1274
    iput v1, p0, Lcom/my/target/dg;->C:I

    .line 1276
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/my/target/dg;->l:Z

    .line 57
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 0

    .prologue
    .line 146
    iput p1, p0, Lcom/my/target/dg;->r:I

    .line 147
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 66
    iput-object p1, p0, Lcom/my/target/dg;->k:Ljava/lang/String;

    .line 67
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 76
    iput-boolean p1, p0, Lcom/my/target/dg;->l:Z

    .line 77
    return-void
.end method

.method public final b(I)V
    .locals 0

    .prologue
    .line 166
    iput p1, p0, Lcom/my/target/dg;->t:I

    .line 167
    return-void
.end method

.method public final b(Z)V
    .locals 0

    .prologue
    .line 86
    iput-boolean p1, p0, Lcom/my/target/dg;->m:Z

    .line 87
    return-void
.end method

.method public final c(I)V
    .locals 0

    .prologue
    .line 176
    iput p1, p0, Lcom/my/target/dg;->u:I

    .line 177
    return-void
.end method

.method public final c(Z)V
    .locals 0

    .prologue
    .line 96
    iput-boolean p1, p0, Lcom/my/target/dg;->n:Z

    .line 97
    return-void
.end method

.method public final d(I)V
    .locals 0

    .prologue
    .line 186
    iput p1, p0, Lcom/my/target/dg;->v:I

    .line 187
    return-void
.end method

.method public final d(Z)V
    .locals 0

    .prologue
    .line 106
    iput-boolean p1, p0, Lcom/my/target/dg;->o:Z

    .line 107
    return-void
.end method

.method public final e(I)V
    .locals 0

    .prologue
    .line 196
    iput p1, p0, Lcom/my/target/dg;->w:I

    .line 197
    return-void
.end method

.method public final e(Z)V
    .locals 0

    .prologue
    .line 116
    iput-boolean p1, p0, Lcom/my/target/dg;->p:Z

    .line 117
    return-void
.end method

.method public final f(I)V
    .locals 0

    .prologue
    .line 206
    iput p1, p0, Lcom/my/target/dg;->x:I

    .line 207
    return-void
.end method

.method public final f(Z)V
    .locals 0

    .prologue
    .line 126
    iput-boolean p1, p0, Lcom/my/target/dg;->q:Z

    .line 127
    return-void
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/my/target/dg;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final g(I)V
    .locals 0

    .prologue
    .line 216
    iput p1, p0, Lcom/my/target/dg;->y:I

    .line 217
    return-void
.end method

.method public final getBackgroundColor()I
    .locals 1

    .prologue
    .line 131
    iget v0, p0, Lcom/my/target/dg;->backgroundColor:I

    return v0
.end method

.method public final getCtaButtonColor()I
    .locals 1

    .prologue
    .line 231
    iget v0, p0, Lcom/my/target/dg;->A:I

    return v0
.end method

.method public final getCtaButtonTextColor()I
    .locals 1

    .prologue
    .line 251
    iget v0, p0, Lcom/my/target/dg;->C:I

    return v0
.end method

.method public final getCtaButtonTouchColor()I
    .locals 1

    .prologue
    .line 241
    iget v0, p0, Lcom/my/target/dg;->B:I

    return v0
.end method

.method public final getTitleColor()I
    .locals 1

    .prologue
    .line 151
    iget v0, p0, Lcom/my/target/dg;->s:I

    return v0
.end method

.method public final h(I)V
    .locals 0

    .prologue
    .line 226
    iput p1, p0, Lcom/my/target/dg;->z:I

    .line 227
    return-void
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/my/target/dg;->l:Z

    return v0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 81
    iget-boolean v0, p0, Lcom/my/target/dg;->m:Z

    return v0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 91
    iget-boolean v0, p0, Lcom/my/target/dg;->n:Z

    return v0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 101
    iget-boolean v0, p0, Lcom/my/target/dg;->o:Z

    return v0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 111
    iget-boolean v0, p0, Lcom/my/target/dg;->p:Z

    return v0
.end method

.method public final m()Z
    .locals 1

    .prologue
    .line 121
    iget-boolean v0, p0, Lcom/my/target/dg;->q:Z

    return v0
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 141
    iget v0, p0, Lcom/my/target/dg;->r:I

    return v0
.end method

.method public final o()I
    .locals 1

    .prologue
    .line 161
    iget v0, p0, Lcom/my/target/dg;->t:I

    return v0
.end method

.method public final p()I
    .locals 1

    .prologue
    .line 171
    iget v0, p0, Lcom/my/target/dg;->u:I

    return v0
.end method

.method public final q()I
    .locals 1

    .prologue
    .line 181
    iget v0, p0, Lcom/my/target/dg;->v:I

    return v0
.end method

.method public final r()I
    .locals 1

    .prologue
    .line 191
    iget v0, p0, Lcom/my/target/dg;->w:I

    return v0
.end method

.method public final s()I
    .locals 1

    .prologue
    .line 201
    iget v0, p0, Lcom/my/target/dg;->x:I

    return v0
.end method

.method public final setBackgroundColor(I)V
    .locals 0
    .param p1, "backgroundColor"    # I

    .prologue
    .line 136
    iput p1, p0, Lcom/my/target/dg;->backgroundColor:I

    .line 137
    return-void
.end method

.method public final setCtaButtonColor(I)V
    .locals 0
    .param p1, "ctaButtonColor"    # I

    .prologue
    .line 236
    iput p1, p0, Lcom/my/target/dg;->A:I

    .line 237
    return-void
.end method

.method public final setCtaButtonTextColor(I)V
    .locals 0
    .param p1, "ctaButtonTextColor"    # I

    .prologue
    .line 256
    iput p1, p0, Lcom/my/target/dg;->C:I

    .line 257
    return-void
.end method

.method public final setCtaButtonTouchColor(I)V
    .locals 0
    .param p1, "ctaButtonTouchColor"    # I

    .prologue
    .line 246
    iput p1, p0, Lcom/my/target/dg;->B:I

    .line 247
    return-void
.end method

.method public final setTitleColor(I)V
    .locals 0
    .param p1, "titleColor"    # I

    .prologue
    .line 156
    iput p1, p0, Lcom/my/target/dg;->s:I

    .line 157
    return-void
.end method

.method public final t()I
    .locals 1

    .prologue
    .line 211
    iget v0, p0, Lcom/my/target/dg;->y:I

    return v0
.end method

.method public final u()I
    .locals 1

    .prologue
    .line 221
    iget v0, p0, Lcom/my/target/dg;->z:I

    return v0
.end method
