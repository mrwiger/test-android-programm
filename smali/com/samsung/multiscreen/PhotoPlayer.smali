.class public Lcom/samsung/multiscreen/PhotoPlayer;
.super Lcom/samsung/multiscreen/Player;
.source "PhotoPlayer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerListener;,
        Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerMessageListener;,
        Lcom/samsung/multiscreen/PhotoPlayer$PhotoPlayerAttributes;
    }
.end annotation


# static fields
.field private static final PHOTO_PLAYER_CONTROL_RESPONSE:Ljava/lang/String; = "state"

.field private static final PHOTO_PLAYER_QUEUE_EVENT_RESPONSE:Ljava/lang/String; = "queue"

.field private static final TAG:Ljava/lang/String; = "PhotoPlayer"


# instance fields
.field private mList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private mPhotoPlayerListener:Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerListener;


# direct methods
.method constructor <init>(Lcom/samsung/multiscreen/Service;Landroid/net/Uri;Ljava/lang/String;)V
    .locals 1
    .param p1, "service"    # Lcom/samsung/multiscreen/Service;
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "appName"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 49
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/multiscreen/Player;-><init>(Lcom/samsung/multiscreen/Service;Landroid/net/Uri;Ljava/lang/String;)V

    .line 31
    iput-object v0, p0, Lcom/samsung/multiscreen/PhotoPlayer;->mPhotoPlayerListener:Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerListener;

    .line 50
    iput-object v0, p0, Lcom/samsung/multiscreen/PhotoPlayer;->mList:Ljava/util/List;

    .line 51
    return-void
.end method

.method static synthetic access$100(Lcom/samsung/multiscreen/PhotoPlayer;)Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/multiscreen/PhotoPlayer;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/samsung/multiscreen/PhotoPlayer;->mPhotoPlayerListener:Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerListener;

    return-object v0
.end method


# virtual methods
.method public addOnMessageListener(Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerListener;

    .prologue
    .line 376
    iput-object p1, p0, Lcom/samsung/multiscreen/PhotoPlayer;->mPhotoPlayerListener:Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerListener;

    .line 377
    sget-object v0, Lcom/samsung/multiscreen/PhotoPlayer;->mApplication:Lcom/samsung/multiscreen/Application;

    const-string v1, "playerNotice"

    new-instance v2, Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerMessageListener;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerMessageListener;-><init>(Lcom/samsung/multiscreen/PhotoPlayer;Lcom/samsung/multiscreen/PhotoPlayer$1;)V

    invoke-virtual {v0, v1, v2}, Lcom/samsung/multiscreen/Application;->addOnMessageListener(Ljava/lang/String;Lcom/samsung/multiscreen/Channel$OnMessageListener;)V

    .line 378
    return-void
.end method

.method public addToList(Landroid/net/Uri;)V
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 189
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/samsung/multiscreen/PhotoPlayer;->addToList(Landroid/net/Uri;Ljava/lang/String;)V

    .line 190
    return-void
.end method

.method public addToList(Landroid/net/Uri;Ljava/lang/String;)V
    .locals 4
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "title"    # Ljava/lang/String;

    .prologue
    .line 201
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 202
    .local v0, "item":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v2, "uri"

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 203
    sget-object v2, Lcom/samsung/multiscreen/PhotoPlayer$PhotoPlayerAttributes;->title:Lcom/samsung/multiscreen/PhotoPlayer$PhotoPlayerAttributes;

    invoke-virtual {v2}, Lcom/samsung/multiscreen/PhotoPlayer$PhotoPlayerAttributes;->name()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 205
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 206
    .local v1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;>;"
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 208
    invoke-virtual {p0, v1}, Lcom/samsung/multiscreen/PhotoPlayer;->addToList(Ljava/util/List;)V

    .line 209
    return-void
.end method

.method public addToList(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 217
    .local p1, "photoList":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;>;"
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 218
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/multiscreen/PhotoPlayer;->isDebug()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "PhotoPlayer"

    const-string v2, "enQueue(): photoList is NULL."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 313
    :cond_1
    :goto_0
    return-void

    .line 225
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/multiscreen/PhotoPlayer;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 226
    new-instance v1, Lcom/samsung/multiscreen/PhotoPlayer$1;

    invoke-direct {v1, p0, p1}, Lcom/samsung/multiscreen/PhotoPlayer$1;-><init>(Lcom/samsung/multiscreen/PhotoPlayer;Ljava/util/List;)V

    invoke-virtual {p0, v1}, Lcom/samsung/multiscreen/PhotoPlayer;->getDMPStatus(Lcom/samsung/multiscreen/Result;)V

    goto :goto_0

    .line 309
    :cond_3
    iget-object v1, p0, Lcom/samsung/multiscreen/PhotoPlayer;->mPhotoPlayerListener:Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerListener;

    if-eqz v1, :cond_1

    .line 310
    new-instance v0, Lcom/samsung/multiscreen/ErrorCode;

    const-string v1, "PLAYER_ERROR_PLAYER_NOT_LOADED"

    invoke-direct {v0, v1}, Lcom/samsung/multiscreen/ErrorCode;-><init>(Ljava/lang/String;)V

    .line 311
    .local v0, "errorCode":Lcom/samsung/multiscreen/ErrorCode;
    iget-object v1, p0, Lcom/samsung/multiscreen/PhotoPlayer;->mPhotoPlayerListener:Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerListener;

    invoke-virtual {v0}, Lcom/samsung/multiscreen/ErrorCode;->value()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0}, Lcom/samsung/multiscreen/ErrorCode;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/samsung/multiscreen/ErrorCode;->name()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v3, v4, v5}, Lcom/samsung/multiscreen/Error;->create(JLjava/lang/String;Ljava/lang/String;)Lcom/samsung/multiscreen/Error;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerListener;->onError(Lcom/samsung/multiscreen/Error;)V

    goto :goto_0
.end method

.method public clearList()V
    .locals 5

    .prologue
    .line 156
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 158
    .local v0, "data":Lorg/json/JSONObject;
    :try_start_0
    const-string v2, "subEvent"

    sget-object v3, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;->clear:Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;

    invoke-virtual {v3}, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 159
    const-string v2, "playerType"

    sget-object v3, Lcom/samsung/multiscreen/Player$ContentType;->photo:Lcom/samsung/multiscreen/Player$ContentType;

    invoke-virtual {v3}, Lcom/samsung/multiscreen/Player$ContentType;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 163
    :goto_0
    sget-object v2, Lcom/samsung/multiscreen/PhotoPlayer;->mApplication:Lcom/samsung/multiscreen/Application;

    const-string v3, "playerQueueEvent"

    invoke-virtual {v2, v3, v0}, Lcom/samsung/multiscreen/Application;->publish(Ljava/lang/String;Ljava/lang/Object;)V

    .line 164
    return-void

    .line 160
    :catch_0
    move-exception v1

    .line 161
    .local v1, "exception":Ljava/lang/Exception;
    const-string v2, "PhotoPlayer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error in parsing JSON object: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getList()V
    .locals 5

    .prologue
    .line 142
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 144
    .local v0, "data":Lorg/json/JSONObject;
    :try_start_0
    const-string v2, "subEvent"

    sget-object v3, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;->fetch:Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;

    invoke-virtual {v3}, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 145
    const-string v2, "playerType"

    sget-object v3, Lcom/samsung/multiscreen/Player$ContentType;->photo:Lcom/samsung/multiscreen/Player$ContentType;

    invoke-virtual {v3}, Lcom/samsung/multiscreen/Player$ContentType;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 149
    :goto_0
    sget-object v2, Lcom/samsung/multiscreen/PhotoPlayer;->mApplication:Lcom/samsung/multiscreen/Application;

    const-string v3, "playerQueueEvent"

    invoke-virtual {v2, v3, v0}, Lcom/samsung/multiscreen/Application;->publish(Ljava/lang/String;Ljava/lang/Object;)V

    .line 150
    return-void

    .line 146
    :catch_0
    move-exception v1

    .line 147
    .local v1, "exception":Ljava/lang/Exception;
    const-string v2, "PhotoPlayer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error in parsing JSON object: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public playContent(Landroid/net/Uri;Lcom/samsung/multiscreen/Result;)V
    .locals 1
    .param p1, "contentUrl"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Lcom/samsung/multiscreen/Result",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 60
    .local p2, "result":Lcom/samsung/multiscreen/Result;, "Lcom/samsung/multiscreen/Result<Ljava/lang/Boolean;>;"
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lcom/samsung/multiscreen/PhotoPlayer;->playContent(Landroid/net/Uri;Ljava/lang/String;Lcom/samsung/multiscreen/Result;)V

    .line 61
    return-void
.end method

.method public playContent(Landroid/net/Uri;Ljava/lang/String;Lcom/samsung/multiscreen/Result;)V
    .locals 7
    .param p1, "contentUrl"    # Landroid/net/Uri;
    .param p2, "title"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            "Lcom/samsung/multiscreen/Result",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 74
    .local p3, "result":Lcom/samsung/multiscreen/Result;, "Lcom/samsung/multiscreen/Result<Ljava/lang/Boolean;>;"
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 76
    .local v0, "contentInfo":Lorg/json/JSONObject;
    if-eqz p1, :cond_2

    :try_start_0
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 77
    const-string v3, "uri"

    invoke-virtual {v0, v3, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 84
    if-eqz p2, :cond_0

    .line 85
    sget-object v3, Lcom/samsung/multiscreen/PhotoPlayer$PhotoPlayerAttributes;->title:Lcom/samsung/multiscreen/PhotoPlayer$PhotoPlayerAttributes;

    invoke-virtual {v3}, Lcom/samsung/multiscreen/PhotoPlayer$PhotoPlayerAttributes;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 92
    :cond_0
    :goto_0
    sget-object v3, Lcom/samsung/multiscreen/Player$ContentType;->photo:Lcom/samsung/multiscreen/Player$ContentType;

    invoke-super {p0, v0, v3, p3}, Lcom/samsung/multiscreen/Player;->playContent(Lorg/json/JSONObject;Lcom/samsung/multiscreen/Player$ContentType;Lcom/samsung/multiscreen/Result;)V

    .line 93
    :cond_1
    :goto_1
    return-void

    .line 79
    :cond_2
    :try_start_1
    new-instance v2, Lcom/samsung/multiscreen/ErrorCode;

    const-string v3, "PLAYER_ERROR_INVALID_URI"

    invoke-direct {v2, v3}, Lcom/samsung/multiscreen/ErrorCode;-><init>(Ljava/lang/String;)V

    .line 80
    .local v2, "error":Lcom/samsung/multiscreen/ErrorCode;
    invoke-virtual {p0}, Lcom/samsung/multiscreen/PhotoPlayer;->isDebug()Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v3, "PhotoPlayer"

    const-string v4, "There\'s no media url to launch!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    :cond_3
    if-eqz p3, :cond_1

    invoke-virtual {v2}, Lcom/samsung/multiscreen/ErrorCode;->value()I

    move-result v3

    int-to-long v4, v3

    invoke-virtual {v2}, Lcom/samsung/multiscreen/ErrorCode;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Lcom/samsung/multiscreen/ErrorCode;->name()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v3, v6}, Lcom/samsung/multiscreen/Error;->create(JLjava/lang/String;Ljava/lang/String;)Lcom/samsung/multiscreen/Error;

    move-result-object v3

    invoke-interface {p3, v3}, Lcom/samsung/multiscreen/Result;->onError(Lcom/samsung/multiscreen/Error;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 87
    .end local v2    # "error":Lcom/samsung/multiscreen/ErrorCode;
    :catch_0
    move-exception v1

    .line 88
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Lcom/samsung/multiscreen/ErrorCode;

    const-string v3, "PLAYER_ERROR_UNKNOWN"

    invoke-direct {v2, v3}, Lcom/samsung/multiscreen/ErrorCode;-><init>(Ljava/lang/String;)V

    .line 89
    .restart local v2    # "error":Lcom/samsung/multiscreen/ErrorCode;
    invoke-virtual {p0}, Lcom/samsung/multiscreen/PhotoPlayer;->isDebug()Z

    move-result v3

    if-eqz v3, :cond_4

    const-string v3, "PhotoPlayer"

    const-string v4, "Unable to create JSONObject!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    :cond_4
    if-eqz p3, :cond_0

    invoke-virtual {v2}, Lcom/samsung/multiscreen/ErrorCode;->value()I

    move-result v3

    int-to-long v4, v3

    invoke-virtual {v2}, Lcom/samsung/multiscreen/ErrorCode;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Lcom/samsung/multiscreen/ErrorCode;->name()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v3, v6}, Lcom/samsung/multiscreen/Error;->create(JLjava/lang/String;Ljava/lang/String;)Lcom/samsung/multiscreen/Error;

    move-result-object v3

    invoke-interface {p3, v3}, Lcom/samsung/multiscreen/Result;->onError(Lcom/samsung/multiscreen/Error;)V

    goto :goto_0
.end method

.method public removeFromList(Landroid/net/Uri;)V
    .locals 5
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 172
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 174
    .local v0, "data":Lorg/json/JSONObject;
    :try_start_0
    const-string v2, "subEvent"

    sget-object v3, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;->dequeue:Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;

    invoke-virtual {v3}, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 175
    const-string v2, "playerType"

    sget-object v3, Lcom/samsung/multiscreen/Player$ContentType;->photo:Lcom/samsung/multiscreen/Player$ContentType;

    invoke-virtual {v3}, Lcom/samsung/multiscreen/Player$ContentType;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 176
    const-string v2, "uri"

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 180
    :goto_0
    sget-object v2, Lcom/samsung/multiscreen/PhotoPlayer;->mApplication:Lcom/samsung/multiscreen/Application;

    const-string v3, "playerQueueEvent"

    invoke-virtual {v2, v3, v0}, Lcom/samsung/multiscreen/Application;->publish(Ljava/lang/String;Ljava/lang/Object;)V

    .line 181
    return-void

    .line 177
    :catch_0
    move-exception v1

    .line 178
    .local v1, "exception":Ljava/lang/Exception;
    const-string v2, "PhotoPlayer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error in parsing JSON object: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setBackgroundMusic(Landroid/net/Uri;)V
    .locals 4
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 111
    sget-object v0, Lcom/samsung/multiscreen/PhotoPlayer;->mApplication:Lcom/samsung/multiscreen/Application;

    const-string v1, "playerControl"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->playMusic:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    invoke-virtual {v3}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/multiscreen/Application;->publish(Ljava/lang/String;Ljava/lang/Object;)V

    .line 112
    return-void
.end method

.method public stopBackgroundMusic()V
    .locals 3

    .prologue
    .line 118
    sget-object v0, Lcom/samsung/multiscreen/PhotoPlayer;->mApplication:Lcom/samsung/multiscreen/Application;

    const-string v1, "playerControl"

    sget-object v2, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->stopMusic:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    invoke-virtual {v2}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/multiscreen/Application;->publish(Ljava/lang/String;Ljava/lang/Object;)V

    .line 119
    return-void
.end method
