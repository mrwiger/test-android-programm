.class public final enum Lru/inetra/bytefog/service/UpdateStatus;
.super Ljava/lang/Enum;
.source "UpdateStatus.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lru/inetra/bytefog/service/UpdateStatus;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lru/inetra/bytefog/service/UpdateStatus;

.field public static final enum HasCriticalUpdates:Lru/inetra/bytefog/service/UpdateStatus;

.field public static final enum HasUpdates:Lru/inetra/bytefog/service/UpdateStatus;

.field public static final enum Pending:Lru/inetra/bytefog/service/UpdateStatus;

.field public static final enum ServiceUnavailable:Lru/inetra/bytefog/service/UpdateStatus;

.field public static final enum Unknown:Lru/inetra/bytefog/service/UpdateStatus;

.field public static final enum Uptodate:Lru/inetra/bytefog/service/UpdateStatus;


# instance fields
.field private value:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 4
    new-instance v0, Lru/inetra/bytefog/service/UpdateStatus;

    const-string v1, "HasCriticalUpdates"

    invoke-direct {v0, v1, v4, v4}, Lru/inetra/bytefog/service/UpdateStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/inetra/bytefog/service/UpdateStatus;->HasCriticalUpdates:Lru/inetra/bytefog/service/UpdateStatus;

    .line 5
    new-instance v0, Lru/inetra/bytefog/service/UpdateStatus;

    const-string v1, "HasUpdates"

    invoke-direct {v0, v1, v5, v5}, Lru/inetra/bytefog/service/UpdateStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/inetra/bytefog/service/UpdateStatus;->HasUpdates:Lru/inetra/bytefog/service/UpdateStatus;

    .line 6
    new-instance v0, Lru/inetra/bytefog/service/UpdateStatus;

    const-string v1, "Uptodate"

    invoke-direct {v0, v1, v6, v6}, Lru/inetra/bytefog/service/UpdateStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/inetra/bytefog/service/UpdateStatus;->Uptodate:Lru/inetra/bytefog/service/UpdateStatus;

    .line 7
    new-instance v0, Lru/inetra/bytefog/service/UpdateStatus;

    const-string v1, "ServiceUnavailable"

    invoke-direct {v0, v1, v7, v7}, Lru/inetra/bytefog/service/UpdateStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/inetra/bytefog/service/UpdateStatus;->ServiceUnavailable:Lru/inetra/bytefog/service/UpdateStatus;

    .line 8
    new-instance v0, Lru/inetra/bytefog/service/UpdateStatus;

    const-string v1, "Pending"

    invoke-direct {v0, v1, v8, v8}, Lru/inetra/bytefog/service/UpdateStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/inetra/bytefog/service/UpdateStatus;->Pending:Lru/inetra/bytefog/service/UpdateStatus;

    .line 9
    new-instance v0, Lru/inetra/bytefog/service/UpdateStatus;

    const-string v1, "Unknown"

    const/4 v2, 0x5

    const/4 v3, -0x1

    invoke-direct {v0, v1, v2, v3}, Lru/inetra/bytefog/service/UpdateStatus;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/inetra/bytefog/service/UpdateStatus;->Unknown:Lru/inetra/bytefog/service/UpdateStatus;

    .line 3
    const/4 v0, 0x6

    new-array v0, v0, [Lru/inetra/bytefog/service/UpdateStatus;

    sget-object v1, Lru/inetra/bytefog/service/UpdateStatus;->HasCriticalUpdates:Lru/inetra/bytefog/service/UpdateStatus;

    aput-object v1, v0, v4

    sget-object v1, Lru/inetra/bytefog/service/UpdateStatus;->HasUpdates:Lru/inetra/bytefog/service/UpdateStatus;

    aput-object v1, v0, v5

    sget-object v1, Lru/inetra/bytefog/service/UpdateStatus;->Uptodate:Lru/inetra/bytefog/service/UpdateStatus;

    aput-object v1, v0, v6

    sget-object v1, Lru/inetra/bytefog/service/UpdateStatus;->ServiceUnavailable:Lru/inetra/bytefog/service/UpdateStatus;

    aput-object v1, v0, v7

    sget-object v1, Lru/inetra/bytefog/service/UpdateStatus;->Pending:Lru/inetra/bytefog/service/UpdateStatus;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lru/inetra/bytefog/service/UpdateStatus;->Unknown:Lru/inetra/bytefog/service/UpdateStatus;

    aput-object v2, v0, v1

    sput-object v0, Lru/inetra/bytefog/service/UpdateStatus;->$VALUES:[Lru/inetra/bytefog/service/UpdateStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 13
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 14
    iput p3, p0, Lru/inetra/bytefog/service/UpdateStatus;->value:I

    .line 15
    return-void
.end method

.method public static fromInteger(I)Lru/inetra/bytefog/service/UpdateStatus;
    .locals 1
    .param p0, "x"    # I

    .prologue
    .line 22
    packed-switch p0, :pswitch_data_0

    .line 34
    sget-object v0, Lru/inetra/bytefog/service/UpdateStatus;->Unknown:Lru/inetra/bytefog/service/UpdateStatus;

    :goto_0
    return-object v0

    .line 24
    :pswitch_0
    sget-object v0, Lru/inetra/bytefog/service/UpdateStatus;->HasCriticalUpdates:Lru/inetra/bytefog/service/UpdateStatus;

    goto :goto_0

    .line 26
    :pswitch_1
    sget-object v0, Lru/inetra/bytefog/service/UpdateStatus;->HasUpdates:Lru/inetra/bytefog/service/UpdateStatus;

    goto :goto_0

    .line 28
    :pswitch_2
    sget-object v0, Lru/inetra/bytefog/service/UpdateStatus;->Uptodate:Lru/inetra/bytefog/service/UpdateStatus;

    goto :goto_0

    .line 30
    :pswitch_3
    sget-object v0, Lru/inetra/bytefog/service/UpdateStatus;->ServiceUnavailable:Lru/inetra/bytefog/service/UpdateStatus;

    goto :goto_0

    .line 32
    :pswitch_4
    sget-object v0, Lru/inetra/bytefog/service/UpdateStatus;->Pending:Lru/inetra/bytefog/service/UpdateStatus;

    goto :goto_0

    .line 22
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lru/inetra/bytefog/service/UpdateStatus;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 3
    const-class v0, Lru/inetra/bytefog/service/UpdateStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lru/inetra/bytefog/service/UpdateStatus;

    return-object v0
.end method

.method public static values()[Lru/inetra/bytefog/service/UpdateStatus;
    .locals 1

    .prologue
    .line 3
    sget-object v0, Lru/inetra/bytefog/service/UpdateStatus;->$VALUES:[Lru/inetra/bytefog/service/UpdateStatus;

    invoke-virtual {v0}, [Lru/inetra/bytefog/service/UpdateStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lru/inetra/bytefog/service/UpdateStatus;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 18
    iget v0, p0, Lru/inetra/bytefog/service/UpdateStatus;->value:I

    return v0
.end method
