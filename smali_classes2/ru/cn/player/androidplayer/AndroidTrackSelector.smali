.class public Lru/cn/player/androidplayer/AndroidTrackSelector;
.super Ljava/lang/Object;
.source "AndroidTrackSelector.java"

# interfaces
.implements Lru/cn/player/Selector;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lru/cn/player/Selector",
        "<",
        "Lru/cn/player/androidplayer/AndroidTrackInfo;",
        ">;"
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private final player:Landroid/media/MediaPlayer;

.field private selectedAudioTrack:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-class v0, Lru/cn/player/androidplayer/AndroidTrackSelector;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lru/cn/player/androidplayer/AndroidTrackSelector;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/media/MediaPlayer;)V
    .locals 1
    .param p1, "player"    # Landroid/media/MediaPlayer;

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const/4 v0, -0x1

    iput v0, p0, Lru/cn/player/androidplayer/AndroidTrackSelector;->selectedAudioTrack:I

    .line 22
    iput-object p1, p0, Lru/cn/player/androidplayer/AndroidTrackSelector;->player:Landroid/media/MediaPlayer;

    .line 23
    return-void
.end method

.method private getCurrentAudioTrackPosition()I
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v2, -0x1

    .line 113
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x10

    if-ge v3, v4, :cond_1

    move v0, v2

    .line 131
    :cond_0
    :goto_0
    return v0

    .line 117
    :cond_1
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x15

    if-lt v3, v4, :cond_2

    .line 118
    iget-object v2, p0, Lru/cn/player/androidplayer/AndroidTrackSelector;->player:Landroid/media/MediaPlayer;

    invoke-virtual {v2, v5}, Landroid/media/MediaPlayer;->getSelectedTrack(I)I

    move-result v0

    goto :goto_0

    .line 120
    :cond_2
    iget v3, p0, Lru/cn/player/androidplayer/AndroidTrackSelector;->selectedAudioTrack:I

    if-eq v3, v2, :cond_3

    .line 121
    iget v0, p0, Lru/cn/player/androidplayer/AndroidTrackSelector;->selectedAudioTrack:I

    goto :goto_0

    .line 123
    :cond_3
    iget-object v3, p0, Lru/cn/player/androidplayer/AndroidTrackSelector;->player:Landroid/media/MediaPlayer;

    invoke-virtual {v3}, Landroid/media/MediaPlayer;->getTrackInfo()[Landroid/media/MediaPlayer$TrackInfo;

    move-result-object v1

    .line 124
    .local v1, "tracksInfo":[Landroid/media/MediaPlayer$TrackInfo;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v3, v1

    if-ge v0, v3, :cond_4

    .line 125
    aget-object v3, v1, v0

    invoke-virtual {v3}, Landroid/media/MediaPlayer$TrackInfo;->getTrackType()I

    move-result v3

    if-eq v3, v5, :cond_0

    .line 124
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    move v0, v2

    .line 131
    goto :goto_0
.end method

.method private getTracks(I)Ljava/util/List;
    .locals 8
    .param p1, "type"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lru/cn/player/androidplayer/AndroidTrackInfo;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 27
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x10

    if-ge v4, v5, :cond_1

    .line 28
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v7}, Ljava/util/ArrayList;-><init>(I)V

    .line 48
    :cond_0
    :goto_0
    return-object v1

    .line 34
    :cond_1
    :try_start_0
    iget-object v4, p0, Lru/cn/player/androidplayer/AndroidTrackSelector;->player:Landroid/media/MediaPlayer;

    invoke-virtual {v4}, Landroid/media/MediaPlayer;->getTrackInfo()[Landroid/media/MediaPlayer$TrackInfo;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 40
    .local v3, "tracks":[Landroid/media/MediaPlayer$TrackInfo;
    new-instance v1, Ljava/util/ArrayList;

    array-length v4, v3

    invoke-direct {v1, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 42
    .local v1, "result":Ljava/util/List;, "Ljava/util/List<Lru/cn/player/androidplayer/AndroidTrackInfo;>;"
    const/4 v2, 0x0

    .local v2, "trackIndex":I
    :goto_1
    array-length v4, v3

    if-ge v2, v4, :cond_0

    .line 43
    aget-object v4, v3, v2

    invoke-virtual {v4}, Landroid/media/MediaPlayer$TrackInfo;->getTrackType()I

    move-result v4

    if-ne v4, p1, :cond_2

    .line 44
    aget-object v4, v3, v2

    invoke-static {v4, v2}, Lru/cn/player/androidplayer/AndroidTrackInfo;->createTrackInfo(Landroid/media/MediaPlayer$TrackInfo;I)Lru/cn/player/androidplayer/AndroidTrackInfo;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 42
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 35
    .end local v1    # "result":Ljava/util/List;, "Ljava/util/List<Lru/cn/player/androidplayer/AndroidTrackInfo;>;"
    .end local v2    # "trackIndex":I
    .end local v3    # "tracks":[Landroid/media/MediaPlayer$TrackInfo;
    :catch_0
    move-exception v0

    .line 36
    .local v0, "e":Ljava/lang/Exception;
    sget-object v4, Lru/cn/player/androidplayer/AndroidTrackSelector;->LOG_TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unable to get tracks of type "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 37
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v7}, Ljava/util/ArrayList;-><init>(I)V

    goto :goto_0
.end method


# virtual methods
.method public canAdaptiveAudioStreaming()Z
    .locals 1

    .prologue
    .line 218
    const/4 v0, 0x0

    return v0
.end method

.method public canAdaptiveVideoStreaming()Z
    .locals 1

    .prologue
    .line 213
    const/4 v0, 0x0

    return v0
.end method

.method public canDeactivateAudio()Z
    .locals 1

    .prologue
    .line 208
    const/4 v0, 0x0

    return v0
.end method

.method public canDeactivateSubtitles()Z
    .locals 1

    .prologue
    .line 198
    const/4 v0, 0x0

    return v0
.end method

.method public disableAudio()V
    .locals 0

    .prologue
    .line 184
    return-void
.end method

.method public disableSubtitles()V
    .locals 0

    .prologue
    .line 194
    return-void
.end method

.method public disableVideo()V
    .locals 0

    .prologue
    .line 189
    return-void
.end method

.method public getAudioTracks()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lru/cn/player/androidplayer/AndroidTrackInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 54
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    .line 55
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 58
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lru/cn/player/androidplayer/AndroidTrackSelector;->getTracks(I)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public getAudioTracksCount()I
    .locals 1

    .prologue
    .line 75
    invoke-virtual {p0}, Lru/cn/player/androidplayer/AndroidTrackSelector;->getAudioTracks()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getCurrentAudioTrackIndex()I
    .locals 7

    .prologue
    const/4 v4, -0x1

    .line 90
    invoke-direct {p0}, Lru/cn/player/androidplayer/AndroidTrackSelector;->getCurrentAudioTrackPosition()I

    move-result v1

    .line 91
    .local v1, "positionInTrackArray":I
    if-ne v1, v4, :cond_1

    move v2, v4

    .line 109
    :cond_0
    :goto_0
    return v2

    .line 94
    :cond_1
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x10

    if-ge v5, v6, :cond_2

    move v2, v4

    .line 95
    goto :goto_0

    .line 98
    :cond_2
    const/4 v2, 0x0

    .line 99
    .local v2, "result":I
    iget-object v5, p0, Lru/cn/player/androidplayer/AndroidTrackSelector;->player:Landroid/media/MediaPlayer;

    invoke-virtual {v5}, Landroid/media/MediaPlayer;->getTrackInfo()[Landroid/media/MediaPlayer$TrackInfo;

    move-result-object v3

    .line 100
    .local v3, "tracksInfo":[Landroid/media/MediaPlayer$TrackInfo;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v5, v3

    if-ge v0, v5, :cond_4

    .line 101
    aget-object v5, v3, v0

    invoke-virtual {v5}, Landroid/media/MediaPlayer$TrackInfo;->getTrackType()I

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_3

    .line 102
    if-eq v1, v0, :cond_0

    .line 105
    add-int/lit8 v2, v2, 0x1

    .line 100
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    move v2, v4

    .line 109
    goto :goto_0
.end method

.method public getCurrentSubtitlesTrackIndex()I
    .locals 1

    .prologue
    .line 141
    const/4 v0, 0x0

    return v0
.end method

.method public getCurrentVideoTrackIndex()I
    .locals 1

    .prologue
    .line 136
    const/4 v0, 0x0

    return v0
.end method

.method public getSubtitleTracks()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lru/cn/player/androidplayer/AndroidTrackInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 70
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    return-object v0
.end method

.method public getSubtitlesTracksCount()I
    .locals 1

    .prologue
    .line 85
    invoke-virtual {p0}, Lru/cn/player/androidplayer/AndroidTrackSelector;->getSubtitleTracks()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getVideoTracks()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lru/cn/player/androidplayer/AndroidTrackInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 64
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    return-object v0
.end method

.method public getVideoTracksCount()I
    .locals 1

    .prologue
    .line 80
    const/4 v0, 0x0

    return v0
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 163
    const/4 v0, -0x1

    iput v0, p0, Lru/cn/player/androidplayer/AndroidTrackSelector;->selectedAudioTrack:I

    .line 164
    return-void
.end method

.method public selectAudioTrack(I)V
    .locals 4
    .param p1, "index"    # I

    .prologue
    .line 146
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-ge v2, v3, :cond_1

    .line 160
    :cond_0
    :goto_0
    return-void

    .line 150
    :cond_1
    iget-object v2, p0, Lru/cn/player/androidplayer/AndroidTrackSelector;->player:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->getTrackInfo()[Landroid/media/MediaPlayer$TrackInfo;

    move-result-object v1

    .line 151
    .local v1, "tracksInfo":[Landroid/media/MediaPlayer$TrackInfo;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v2, v1

    if-ge v0, v2, :cond_0

    .line 152
    aget-object v2, v1, v0

    invoke-virtual {v2}, Landroid/media/MediaPlayer$TrackInfo;->getTrackType()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_3

    .line 153
    if-nez p1, :cond_2

    .line 154
    iget-object v2, p0, Lru/cn/player/androidplayer/AndroidTrackSelector;->player:Landroid/media/MediaPlayer;

    invoke-virtual {v2, v0}, Landroid/media/MediaPlayer;->selectTrack(I)V

    goto :goto_0

    .line 157
    :cond_2
    add-int/lit8 p1, p1, -0x1

    .line 151
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public selectSubtitlesTrack(I)V
    .locals 0
    .param p1, "index"    # I

    .prologue
    .line 174
    return-void
.end method

.method public selectVideoTrack(I)V
    .locals 0
    .param p1, "index"    # I

    .prologue
    .line 169
    return-void
.end method

.method public setAdaptiveVideoStream()V
    .locals 0

    .prologue
    .line 179
    return-void
.end method
