.class public Lru/cn/api/googlepush/ApplicationRegistrar;
.super Ljava/lang/Object;
.source "ApplicationRegistrar.java"


# static fields
.field private static isRegistering:Z


# direct methods
.method static synthetic access$000(Landroid/content/Context;)Z
    .locals 1
    .param p0, "x0"    # Landroid/content/Context;

    .prologue
    .line 16
    invoke-static {p0}, Lru/cn/api/googlepush/ApplicationRegistrar;->isAppRegistered(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$102(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 16
    sput-boolean p0, Lru/cn/api/googlepush/ApplicationRegistrar;->isRegistering:Z

    return p0
.end method

.method static synthetic access$200(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Landroid/content/Context;
    .param p1, "x1"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 16
    invoke-static {p0, p1}, Lru/cn/api/googlepush/ApplicationRegistrar;->updateAppIfNeeded(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method private static isAppRegistered(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 87
    const-string v3, "ApplicationRegistrar"

    invoke-virtual {p0, v3, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 89
    .local v1, "prefs":Landroid/content/SharedPreferences;
    const-string v3, "appVersion"

    const/4 v4, 0x0

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 91
    .local v0, "appVersion":Ljava/lang/String;
    if-eqz v0, :cond_0

    const/4 v2, 0x1

    :cond_0
    return v2
.end method

.method private static isPushRegistered(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 95
    const-string v3, "ApplicationRegistrar"

    invoke-virtual {p0, v3, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 97
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v3, "registration_id"

    const/4 v4, 0x0

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 99
    .local v1, "regId":Ljava/lang/String;
    if-eqz v1, :cond_0

    const/4 v2, 0x1

    :cond_0
    return v2
.end method

.method static register(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 27
    sget-boolean v0, Lru/cn/api/googlepush/ApplicationRegistrar;->isRegistering:Z

    if-eqz v0, :cond_0

    .line 61
    :goto_0
    return-void

    .line 30
    :cond_0
    const/4 v0, 0x1

    sput-boolean v0, Lru/cn/api/googlepush/ApplicationRegistrar;->isRegistering:Z

    .line 32
    sget-object v0, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-instance v1, Lru/cn/api/googlepush/ApplicationRegistrar$1;

    invoke-direct {v1, p0}, Lru/cn/api/googlepush/ApplicationRegistrar$1;-><init>(Landroid/content/Context;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public static registerIfNeeded(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 64
    invoke-static {}, Lcom/google/android/gms/common/GoogleApiAvailability;->getInstance()Lcom/google/android/gms/common/GoogleApiAvailability;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/google/android/gms/common/GoogleApiAvailability;->isGooglePlayServicesAvailable(Landroid/content/Context;)I

    move-result v0

    .line 65
    .local v0, "result":I
    invoke-static {p0}, Lru/cn/api/googlepush/ApplicationRegistrar;->isAppRegistered(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 66
    invoke-static {p0}, Lru/cn/api/googlepush/ApplicationRegistrar;->isPushRegistered(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    if-nez v0, :cond_1

    .line 67
    :cond_0
    invoke-static {p0}, Lru/cn/api/googlepush/ApplicationRegistrar;->register(Landroid/content/Context;)V

    .line 69
    :cond_1
    return-void
.end method

.method private static storeRegistrationId(Landroid/content/Context;Ljava/lang/String;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "googleRegId"    # Ljava/lang/String;

    .prologue
    .line 112
    invoke-static {p0}, Lru/cn/utils/Utils;->getAppVersionName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 113
    .local v0, "appVersion":Ljava/lang/String;
    const-string v3, "ApplicationRegistrar"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Saving regId on app version "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    const-string v3, "ApplicationRegistrar"

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 117
    .local v2, "prefs":Landroid/content/SharedPreferences;
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 119
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    if-eqz p1, :cond_0

    .line 120
    const-string v3, "registration_id"

    invoke-interface {v1, v3, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 123
    :cond_0
    const-string v3, "appVersion"

    invoke-interface {v1, v3, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 124
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 125
    return-void
.end method

.method private static updateAppIfNeeded(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "regId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 73
    invoke-static {p0}, Lru/cn/utils/Utils;->getAndroidId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 75
    .local v0, "deviceId":Ljava/lang/String;
    invoke-static {p0}, Lru/cn/api/googlepush/ApplicationRegistrar;->isAppRegistered(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 76
    invoke-static {p0}, Lru/cn/utils/Utils;->getUUID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 77
    invoke-static {p0}, Lru/cn/utils/Utils;->getAppVersionName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 76
    invoke-static {p0, v0, v1, p1, v2}, Lru/cn/api/userdata/UserData;->addApplication(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 83
    :goto_0
    invoke-static {p0, p1}, Lru/cn/api/googlepush/ApplicationRegistrar;->storeRegistrationId(Landroid/content/Context;Ljava/lang/String;)V

    .line 84
    return-void

    .line 79
    :cond_0
    invoke-static {p0}, Lru/cn/utils/Utils;->getUUID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 80
    invoke-static {p0}, Lru/cn/utils/Utils;->getAppVersionName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 79
    invoke-static {p0, v0, v1, p1, v2}, Lru/cn/api/userdata/UserData;->modifyApplication(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_0
.end method
