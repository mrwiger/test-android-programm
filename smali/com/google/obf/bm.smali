.class final Lcom/google/obf/bm;
.super Ljava/lang/Object;
.source "IMASDK"


# instance fields
.field public a:Lcom/google/obf/be;

.field public b:J

.field public c:J

.field public d:I

.field public e:[I

.field public f:[I

.field public g:[J

.field public h:[Z

.field public i:Z

.field public j:[Z

.field public k:I

.field public l:Lcom/google/obf/dw;

.field public m:Z

.field public n:Lcom/google/obf/bl;

.field public o:J


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2
    iput v2, p0, Lcom/google/obf/bm;->d:I

    .line 3
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/obf/bm;->o:J

    .line 4
    iput-boolean v2, p0, Lcom/google/obf/bm;->i:Z

    .line 5
    iput-boolean v2, p0, Lcom/google/obf/bm;->m:Z

    .line 6
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/obf/bm;->n:Lcom/google/obf/bl;

    .line 7
    return-void
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 8
    iput p1, p0, Lcom/google/obf/bm;->d:I

    .line 9
    iget-object v0, p0, Lcom/google/obf/bm;->e:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/obf/bm;->e:[I

    array-length v0, v0

    iget v1, p0, Lcom/google/obf/bm;->d:I

    if-ge v0, v1, :cond_1

    .line 10
    :cond_0
    mul-int/lit8 v0, p1, 0x7d

    div-int/lit8 v0, v0, 0x64

    .line 11
    new-array v1, v0, [I

    iput-object v1, p0, Lcom/google/obf/bm;->e:[I

    .line 12
    new-array v1, v0, [I

    iput-object v1, p0, Lcom/google/obf/bm;->f:[I

    .line 13
    new-array v1, v0, [J

    iput-object v1, p0, Lcom/google/obf/bm;->g:[J

    .line 14
    new-array v1, v0, [Z

    iput-object v1, p0, Lcom/google/obf/bm;->h:[Z

    .line 15
    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/google/obf/bm;->j:[Z

    .line 16
    :cond_1
    return-void
.end method

.method public a(Lcom/google/obf/ak;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 23
    iget-object v0, p0, Lcom/google/obf/bm;->l:Lcom/google/obf/dw;

    iget-object v0, v0, Lcom/google/obf/dw;->a:[B

    iget v1, p0, Lcom/google/obf/bm;->k:I

    invoke-interface {p1, v0, v2, v1}, Lcom/google/obf/ak;->b([BII)V

    .line 24
    iget-object v0, p0, Lcom/google/obf/bm;->l:Lcom/google/obf/dw;

    invoke-virtual {v0, v2}, Lcom/google/obf/dw;->c(I)V

    .line 25
    iput-boolean v2, p0, Lcom/google/obf/bm;->m:Z

    .line 26
    return-void
.end method

.method public a(Lcom/google/obf/dw;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 27
    iget-object v0, p0, Lcom/google/obf/bm;->l:Lcom/google/obf/dw;

    iget-object v0, v0, Lcom/google/obf/dw;->a:[B

    iget v1, p0, Lcom/google/obf/bm;->k:I

    invoke-virtual {p1, v0, v2, v1}, Lcom/google/obf/dw;->a([BII)V

    .line 28
    iget-object v0, p0, Lcom/google/obf/bm;->l:Lcom/google/obf/dw;

    invoke-virtual {v0, v2}, Lcom/google/obf/dw;->c(I)V

    .line 29
    iput-boolean v2, p0, Lcom/google/obf/bm;->m:Z

    .line 30
    return-void
.end method

.method public b(I)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 17
    iget-object v0, p0, Lcom/google/obf/bm;->l:Lcom/google/obf/dw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/obf/bm;->l:Lcom/google/obf/dw;

    invoke-virtual {v0}, Lcom/google/obf/dw;->c()I

    move-result v0

    if-ge v0, p1, :cond_1

    .line 18
    :cond_0
    new-instance v0, Lcom/google/obf/dw;

    invoke-direct {v0, p1}, Lcom/google/obf/dw;-><init>(I)V

    iput-object v0, p0, Lcom/google/obf/bm;->l:Lcom/google/obf/dw;

    .line 19
    :cond_1
    iput p1, p0, Lcom/google/obf/bm;->k:I

    .line 20
    iput-boolean v1, p0, Lcom/google/obf/bm;->i:Z

    .line 21
    iput-boolean v1, p0, Lcom/google/obf/bm;->m:Z

    .line 22
    return-void
.end method

.method public c(I)J
    .locals 4

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/obf/bm;->g:[J

    aget-wide v0, v0, p1

    iget-object v2, p0, Lcom/google/obf/bm;->f:[I

    aget v2, v2, p1

    int-to-long v2, v2

    add-long/2addr v0, v2

    return-wide v0
.end method
