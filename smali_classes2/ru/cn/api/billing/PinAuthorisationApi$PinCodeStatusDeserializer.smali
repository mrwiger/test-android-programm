.class Lru/cn/api/billing/PinAuthorisationApi$PinCodeStatusDeserializer;
.super Ljava/lang/Object;
.source "PinAuthorisationApi.java"

# interfaces
.implements Lcom/google/gson/JsonDeserializer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/api/billing/PinAuthorisationApi;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PinCodeStatusDeserializer"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/gson/JsonDeserializer",
        "<",
        "Lru/cn/api/billing/PinAuthorisationStatus$Status;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lru/cn/api/billing/PinAuthorisationApi$1;)V
    .locals 0
    .param p1, "x0"    # Lru/cn/api/billing/PinAuthorisationApi$1;

    .prologue
    .line 36
    invoke-direct {p0}, Lru/cn/api/billing/PinAuthorisationApi$PinCodeStatusDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic deserialize(Lcom/google/gson/JsonElement;Ljava/lang/reflect/Type;Lcom/google/gson/JsonDeserializationContext;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/gson/JsonParseException;
        }
    .end annotation

    .prologue
    .line 36
    invoke-virtual {p0, p1, p2, p3}, Lru/cn/api/billing/PinAuthorisationApi$PinCodeStatusDeserializer;->deserialize(Lcom/google/gson/JsonElement;Ljava/lang/reflect/Type;Lcom/google/gson/JsonDeserializationContext;)Lru/cn/api/billing/PinAuthorisationStatus$Status;

    move-result-object v0

    return-object v0
.end method

.method public deserialize(Lcom/google/gson/JsonElement;Ljava/lang/reflect/Type;Lcom/google/gson/JsonDeserializationContext;)Lru/cn/api/billing/PinAuthorisationStatus$Status;
    .locals 6
    .param p1, "json"    # Lcom/google/gson/JsonElement;
    .param p2, "typeOfT"    # Ljava/lang/reflect/Type;
    .param p3, "context"    # Lcom/google/gson/JsonDeserializationContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/gson/JsonParseException;
        }
    .end annotation

    .prologue
    .line 44
    invoke-static {}, Lru/cn/api/billing/PinAuthorisationStatus$Status;->values()[Lru/cn/api/billing/PinAuthorisationStatus$Status;

    move-result-object v1

    .line 45
    .local v1, "statuses":[Lru/cn/api/billing/PinAuthorisationStatus$Status;
    array-length v3, v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v0, v1, v2

    .line 46
    .local v0, "status":Lru/cn/api/billing/PinAuthorisationStatus$Status;
    invoke-virtual {v0}, Lru/cn/api/billing/PinAuthorisationStatus$Status;->getValue()I

    move-result v4

    invoke-virtual {p1}, Lcom/google/gson/JsonElement;->getAsInt()I

    move-result v5

    if-ne v4, v5, :cond_0

    .line 49
    .end local v0    # "status":Lru/cn/api/billing/PinAuthorisationStatus$Status;
    :goto_1
    return-object v0

    .line 45
    .restart local v0    # "status":Lru/cn/api/billing/PinAuthorisationStatus$Status;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 49
    .end local v0    # "status":Lru/cn/api/billing/PinAuthorisationStatus$Status;
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
