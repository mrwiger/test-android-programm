.class final Lcom/google/obf/bx;
.super Lcom/google/obf/cb;
.source "IMASDK"


# instance fields
.field private final b:Z

.field private final c:Lcom/google/obf/dv;

.field private final d:Lcom/google/obf/dw;

.field private e:I

.field private f:I

.field private g:Z

.field private h:J

.field private i:Lcom/google/obf/q;

.field private j:I

.field private k:J


# direct methods
.method public constructor <init>(Lcom/google/obf/ar;Z)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0, p1}, Lcom/google/obf/cb;-><init>(Lcom/google/obf/ar;)V

    .line 2
    iput-boolean p2, p0, Lcom/google/obf/bx;->b:Z

    .line 3
    new-instance v0, Lcom/google/obf/dv;

    const/16 v1, 0x8

    new-array v1, v1, [B

    invoke-direct {v0, v1}, Lcom/google/obf/dv;-><init>([B)V

    iput-object v0, p0, Lcom/google/obf/bx;->c:Lcom/google/obf/dv;

    .line 4
    new-instance v0, Lcom/google/obf/dw;

    iget-object v1, p0, Lcom/google/obf/bx;->c:Lcom/google/obf/dv;

    iget-object v1, v1, Lcom/google/obf/dv;->a:[B

    invoke-direct {v0, v1}, Lcom/google/obf/dw;-><init>([B)V

    iput-object v0, p0, Lcom/google/obf/bx;->d:Lcom/google/obf/dw;

    .line 5
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/obf/bx;->e:I

    .line 6
    return-void
.end method

.method private a(Lcom/google/obf/dw;[BI)Z
    .locals 2

    .prologue
    .line 35
    invoke-virtual {p1}, Lcom/google/obf/dw;->b()I

    move-result v0

    iget v1, p0, Lcom/google/obf/bx;->f:I

    sub-int v1, p3, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 36
    iget v1, p0, Lcom/google/obf/bx;->f:I

    invoke-virtual {p1, p2, v1, v0}, Lcom/google/obf/dw;->a([BII)V

    .line 37
    iget v1, p0, Lcom/google/obf/bx;->f:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/obf/bx;->f:I

    .line 38
    iget v0, p0, Lcom/google/obf/bx;->f:I

    if-ne v0, p3, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Lcom/google/obf/dw;)Z
    .locals 5

    .prologue
    const/16 v4, 0xb

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 39
    :goto_0
    invoke-virtual {p1}, Lcom/google/obf/dw;->b()I

    move-result v0

    if-lez v0, :cond_4

    .line 40
    iget-boolean v0, p0, Lcom/google/obf/bx;->g:Z

    if-nez v0, :cond_1

    .line 41
    invoke-virtual {p1}, Lcom/google/obf/dw;->f()I

    move-result v0

    if-ne v0, v4, :cond_0

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/obf/bx;->g:Z

    goto :goto_0

    :cond_0
    move v0, v2

    goto :goto_1

    .line 43
    :cond_1
    invoke-virtual {p1}, Lcom/google/obf/dw;->f()I

    move-result v0

    .line 44
    const/16 v3, 0x77

    if-ne v0, v3, :cond_2

    .line 45
    iput-boolean v2, p0, Lcom/google/obf/bx;->g:Z

    .line 49
    :goto_2
    return v1

    .line 47
    :cond_2
    if-ne v0, v4, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/google/obf/bx;->g:Z

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_3

    :cond_4
    move v1, v2

    .line 49
    goto :goto_2
.end method

.method private c()V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    const/4 v1, 0x0

    .line 50
    iget-object v0, p0, Lcom/google/obf/bx;->i:Lcom/google/obf/q;

    if-nez v0, :cond_0

    .line 51
    iget-boolean v0, p0, Lcom/google/obf/bx;->b:Z

    if-eqz v0, :cond_1

    .line 52
    iget-object v0, p0, Lcom/google/obf/bx;->c:Lcom/google/obf/dv;

    invoke-static {v0, v1, v2, v3, v1}, Lcom/google/obf/dk;->b(Lcom/google/obf/dv;Ljava/lang/String;JLjava/lang/String;)Lcom/google/obf/q;

    move-result-object v0

    .line 53
    :goto_0
    iput-object v0, p0, Lcom/google/obf/bx;->i:Lcom/google/obf/q;

    .line 54
    iget-object v0, p0, Lcom/google/obf/bx;->a:Lcom/google/obf/ar;

    iget-object v1, p0, Lcom/google/obf/bx;->i:Lcom/google/obf/q;

    invoke-interface {v0, v1}, Lcom/google/obf/ar;->a(Lcom/google/obf/q;)V

    .line 55
    :cond_0
    iget-boolean v0, p0, Lcom/google/obf/bx;->b:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/obf/bx;->c:Lcom/google/obf/dv;

    iget-object v0, v0, Lcom/google/obf/dv;->a:[B

    invoke-static {v0}, Lcom/google/obf/dk;->b([B)I

    move-result v0

    .line 56
    :goto_1
    iput v0, p0, Lcom/google/obf/bx;->j:I

    .line 57
    iget-boolean v0, p0, Lcom/google/obf/bx;->b:Z

    if-eqz v0, :cond_3

    .line 58
    iget-object v0, p0, Lcom/google/obf/bx;->c:Lcom/google/obf/dv;

    iget-object v0, v0, Lcom/google/obf/dv;->a:[B

    invoke-static {v0}, Lcom/google/obf/dk;->c([B)I

    move-result v0

    .line 60
    :goto_2
    const-wide/32 v2, 0xf4240

    int-to-long v0, v0

    mul-long/2addr v0, v2

    iget-object v2, p0, Lcom/google/obf/bx;->i:Lcom/google/obf/q;

    iget v2, v2, Lcom/google/obf/q;->r:I

    int-to-long v2, v2

    div-long/2addr v0, v2

    long-to-int v0, v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/google/obf/bx;->h:J

    .line 61
    return-void

    .line 53
    :cond_1
    iget-object v0, p0, Lcom/google/obf/bx;->c:Lcom/google/obf/dv;

    invoke-static {v0, v1, v2, v3, v1}, Lcom/google/obf/dk;->a(Lcom/google/obf/dv;Ljava/lang/String;JLjava/lang/String;)Lcom/google/obf/q;

    move-result-object v0

    goto :goto_0

    .line 56
    :cond_2
    iget-object v0, p0, Lcom/google/obf/bx;->c:Lcom/google/obf/dv;

    iget-object v0, v0, Lcom/google/obf/dv;->a:[B

    invoke-static {v0}, Lcom/google/obf/dk;->a([B)I

    move-result v0

    goto :goto_1

    .line 59
    :cond_3
    invoke-static {}, Lcom/google/obf/dk;->a()I

    move-result v0

    goto :goto_2
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 7
    iput v0, p0, Lcom/google/obf/bx;->e:I

    .line 8
    iput v0, p0, Lcom/google/obf/bx;->f:I

    .line 9
    iput-boolean v0, p0, Lcom/google/obf/bx;->g:Z

    .line 10
    return-void
.end method

.method public a(JZ)V
    .locals 1

    .prologue
    .line 11
    iput-wide p1, p0, Lcom/google/obf/bx;->k:J

    .line 12
    return-void
.end method

.method public a(Lcom/google/obf/dw;)V
    .locals 10

    .prologue
    const/16 v9, 0x8

    const/4 v8, 0x2

    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 13
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/obf/dw;->b()I

    move-result v0

    if-lez v0, :cond_1

    .line 14
    iget v0, p0, Lcom/google/obf/bx;->e:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 15
    :pswitch_0
    invoke-direct {p0, p1}, Lcom/google/obf/bx;->b(Lcom/google/obf/dw;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 16
    iput v4, p0, Lcom/google/obf/bx;->e:I

    .line 17
    iget-object v0, p0, Lcom/google/obf/bx;->d:Lcom/google/obf/dw;

    iget-object v0, v0, Lcom/google/obf/dw;->a:[B

    const/16 v1, 0xb

    aput-byte v1, v0, v6

    .line 18
    iget-object v0, p0, Lcom/google/obf/bx;->d:Lcom/google/obf/dw;

    iget-object v0, v0, Lcom/google/obf/dw;->a:[B

    const/16 v1, 0x77

    aput-byte v1, v0, v4

    .line 19
    iput v8, p0, Lcom/google/obf/bx;->f:I

    goto :goto_0

    .line 20
    :pswitch_1
    iget-object v0, p0, Lcom/google/obf/bx;->d:Lcom/google/obf/dw;

    iget-object v0, v0, Lcom/google/obf/dw;->a:[B

    invoke-direct {p0, p1, v0, v9}, Lcom/google/obf/bx;->a(Lcom/google/obf/dw;[BI)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 21
    invoke-direct {p0}, Lcom/google/obf/bx;->c()V

    .line 22
    iget-object v0, p0, Lcom/google/obf/bx;->d:Lcom/google/obf/dw;

    invoke-virtual {v0, v6}, Lcom/google/obf/dw;->c(I)V

    .line 23
    iget-object v0, p0, Lcom/google/obf/bx;->a:Lcom/google/obf/ar;

    iget-object v1, p0, Lcom/google/obf/bx;->d:Lcom/google/obf/dw;

    invoke-interface {v0, v1, v9}, Lcom/google/obf/ar;->a(Lcom/google/obf/dw;I)V

    .line 24
    iput v8, p0, Lcom/google/obf/bx;->e:I

    goto :goto_0

    .line 25
    :pswitch_2
    invoke-virtual {p1}, Lcom/google/obf/dw;->b()I

    move-result v0

    iget v1, p0, Lcom/google/obf/bx;->j:I

    iget v2, p0, Lcom/google/obf/bx;->f:I

    sub-int/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 26
    iget-object v1, p0, Lcom/google/obf/bx;->a:Lcom/google/obf/ar;

    invoke-interface {v1, p1, v0}, Lcom/google/obf/ar;->a(Lcom/google/obf/dw;I)V

    .line 27
    iget v1, p0, Lcom/google/obf/bx;->f:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/obf/bx;->f:I

    .line 28
    iget v0, p0, Lcom/google/obf/bx;->f:I

    iget v1, p0, Lcom/google/obf/bx;->j:I

    if-ne v0, v1, :cond_0

    .line 29
    iget-object v1, p0, Lcom/google/obf/bx;->a:Lcom/google/obf/ar;

    iget-wide v2, p0, Lcom/google/obf/bx;->k:J

    iget v5, p0, Lcom/google/obf/bx;->j:I

    const/4 v7, 0x0

    invoke-interface/range {v1 .. v7}, Lcom/google/obf/ar;->a(JIII[B)V

    .line 30
    iget-wide v0, p0, Lcom/google/obf/bx;->k:J

    iget-wide v2, p0, Lcom/google/obf/bx;->h:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/obf/bx;->k:J

    .line 31
    iput v6, p0, Lcom/google/obf/bx;->e:I

    goto :goto_0

    .line 33
    :cond_1
    return-void

    .line 14
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public b()V
    .locals 0

    .prologue
    .line 34
    return-void
.end method
