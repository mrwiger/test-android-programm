.class Lru/cn/tv/stb/categories/CategoryViewModel;
.super Lru/cn/mvvm/RxViewModel;
.source "CategoryViewModel.java"


# instance fields
.field private final ageFilter:Ljava/lang/String;

.field private final categories:Landroid/arch/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/arch/lifecycle/MutableLiveData",
            "<",
            "Lru/cn/tv/stb/categories/CategoriesInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final currentCategory:Lru/cn/domain/tv/CurrentCategory;

.field private final loader:Lru/cn/mvvm/RxLoader;


# direct methods
.method constructor <init>(Landroid/content/Context;Lru/cn/mvvm/RxLoader;Lru/cn/domain/tv/CurrentCategory;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "loader"    # Lru/cn/mvvm/RxLoader;
    .param p3, "currentCategory"    # Lru/cn/domain/tv/CurrentCategory;

    .prologue
    .line 30
    invoke-direct {p0}, Lru/cn/mvvm/RxViewModel;-><init>()V

    .line 31
    iput-object p2, p0, Lru/cn/tv/stb/categories/CategoryViewModel;->loader:Lru/cn/mvvm/RxLoader;

    .line 32
    iput-object p3, p0, Lru/cn/tv/stb/categories/CategoryViewModel;->currentCategory:Lru/cn/domain/tv/CurrentCategory;

    .line 33
    invoke-static {p1}, Lru/cn/domain/KidsObject;->getAgeFilterIsNeed(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lru/cn/tv/stb/categories/CategoryViewModel;->ageFilter:Ljava/lang/String;

    .line 35
    new-instance v0, Landroid/arch/lifecycle/MutableLiveData;

    invoke-direct {v0}, Landroid/arch/lifecycle/MutableLiveData;-><init>()V

    iput-object v0, p0, Lru/cn/tv/stb/categories/CategoryViewModel;->categories:Landroid/arch/lifecycle/MutableLiveData;

    .line 37
    invoke-direct {p0}, Lru/cn/tv/stb/categories/CategoryViewModel;->categoriesInfo()Lio/reactivex/Observable;

    move-result-object v0

    .line 38
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lru/cn/tv/stb/categories/CategoryViewModel;->categories:Landroid/arch/lifecycle/MutableLiveData;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v1}, Lru/cn/tv/stb/categories/CategoryViewModel$$Lambda$0;->get$Lambda(Landroid/arch/lifecycle/MutableLiveData;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    new-instance v2, Lru/cn/tv/stb/categories/CategoryViewModel$$Lambda$1;

    invoke-direct {v2, p0}, Lru/cn/tv/stb/categories/CategoryViewModel$$Lambda$1;-><init>(Lru/cn/tv/stb/categories/CategoryViewModel;)V

    .line 39
    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 37
    invoke-virtual {p0, v0}, Lru/cn/tv/stb/categories/CategoryViewModel;->bind(Lio/reactivex/disposables/Disposable;)V

    .line 44
    return-void
.end method

.method private categoriesInfo()Lio/reactivex/Observable;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Lru/cn/tv/stb/categories/CategoriesInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59
    .line 61
    invoke-direct {p0}, Lru/cn/tv/stb/categories/CategoryViewModel;->intersections()Lio/reactivex/Observable;

    move-result-object v0

    .line 62
    invoke-direct {p0}, Lru/cn/tv/stb/categories/CategoryViewModel;->hdChannels()Lio/reactivex/Observable;

    move-result-object v1

    .line 63
    invoke-direct {p0}, Lru/cn/tv/stb/categories/CategoryViewModel;->pornoChannels()Lio/reactivex/Observable;

    move-result-object v2

    .line 64
    invoke-direct {p0}, Lru/cn/tv/stb/categories/CategoryViewModel;->contractor()Lio/reactivex/Observable;

    move-result-object v3

    new-instance v4, Lru/cn/tv/stb/categories/CategoryViewModel$$Lambda$2;

    invoke-direct {v4, p0}, Lru/cn/tv/stb/categories/CategoryViewModel$$Lambda$2;-><init>(Lru/cn/tv/stb/categories/CategoryViewModel;)V

    .line 60
    invoke-static {v0, v1, v2, v3, v4}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/Function4;)Lio/reactivex/Observable;

    move-result-object v0

    .line 59
    return-object v0
.end method

.method private contractor()Lio/reactivex/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 118
    invoke-static {}, Lru/cn/api/provider/TvContentProviderContract;->contractor()Landroid/net/Uri;

    move-result-object v0

    .line 120
    .local v0, "uri":Landroid/net/Uri;
    iget-object v1, p0, Lru/cn/tv/stb/categories/CategoryViewModel;->loader:Lru/cn/mvvm/RxLoader;

    invoke-virtual {v1, v0}, Lru/cn/mvvm/RxLoader;->query(Landroid/net/Uri;)Lio/reactivex/Observable;

    move-result-object v1

    .line 121
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    .line 120
    return-object v1
.end method

.method private hdChannels()Lio/reactivex/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 108
    const-string v0, "hd"

    .line 109
    .local v0, "selection":Ljava/lang/String;
    iget-object v2, p0, Lru/cn/tv/stb/categories/CategoryViewModel;->ageFilter:Ljava/lang/String;

    invoke-static {v2}, Lru/cn/api/provider/TvContentProviderContract;->channels(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 111
    .local v1, "uri":Landroid/net/Uri;
    iget-object v2, p0, Lru/cn/tv/stb/categories/CategoryViewModel;->loader:Lru/cn/mvvm/RxLoader;

    invoke-virtual {v2, v1, v0}, Lru/cn/mvvm/RxLoader;->query(Landroid/net/Uri;Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object v2

    .line 112
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v2

    sget-object v3, Lru/cn/tv/stb/categories/CategoryViewModel$$Lambda$7;->$instance:Lio/reactivex/functions/Function;

    .line 113
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    sget-object v3, Lru/cn/tv/stb/categories/CategoryViewModel$$Lambda$8;->$instance:Lio/reactivex/functions/Function;

    .line 114
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->onErrorReturn(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    .line 111
    return-object v2
.end method

.method private intersections()Lio/reactivex/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 88
    const-string v0, "intersections"

    .line 89
    .local v0, "selection":Ljava/lang/String;
    iget-object v2, p0, Lru/cn/tv/stb/categories/CategoryViewModel;->ageFilter:Ljava/lang/String;

    invoke-static {v2}, Lru/cn/api/provider/TvContentProviderContract;->channels(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 91
    .local v1, "uri":Landroid/net/Uri;
    iget-object v2, p0, Lru/cn/tv/stb/categories/CategoryViewModel;->loader:Lru/cn/mvvm/RxLoader;

    invoke-virtual {v2, v1, v0}, Lru/cn/mvvm/RxLoader;->query(Landroid/net/Uri;Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object v2

    .line 92
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v2

    sget-object v3, Lru/cn/tv/stb/categories/CategoryViewModel$$Lambda$3;->$instance:Lio/reactivex/functions/Function;

    .line 93
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    sget-object v3, Lru/cn/tv/stb/categories/CategoryViewModel$$Lambda$4;->$instance:Lio/reactivex/functions/Function;

    .line 94
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->onErrorReturn(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    .line 91
    return-object v2
.end method

.method static final synthetic lambda$hdChannels$5$CategoryViewModel(Landroid/database/Cursor;)Ljava/lang/Boolean;
    .locals 1
    .param p0, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 113
    invoke-interface {p0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static final synthetic lambda$hdChannels$6$CategoryViewModel(Ljava/lang/Throwable;)Ljava/lang/Boolean;
    .locals 1
    .param p0, "error"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 114
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method static final synthetic lambda$intersections$1$CategoryViewModel(Landroid/database/Cursor;)Ljava/lang/Boolean;
    .locals 1
    .param p0, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 93
    invoke-interface {p0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static final synthetic lambda$intersections$2$CategoryViewModel(Ljava/lang/Throwable;)Ljava/lang/Boolean;
    .locals 1
    .param p0, "error"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 94
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method static final synthetic lambda$pornoChannels$3$CategoryViewModel(Landroid/database/Cursor;)Ljava/lang/Boolean;
    .locals 1
    .param p0, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 103
    invoke-interface {p0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static final synthetic lambda$pornoChannels$4$CategoryViewModel(Ljava/lang/Throwable;)Ljava/lang/Boolean;
    .locals 1
    .param p0, "error"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 104
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method private mapValues(ZZZLandroid/database/Cursor;)Lru/cn/tv/stb/categories/CategoriesInfo;
    .locals 4
    .param p1, "hasCameras"    # Z
    .param p2, "hasHDChannels"    # Z
    .param p3, "hasPorno"    # Z
    .param p4, "contractor"    # Landroid/database/Cursor;

    .prologue
    .line 70
    new-instance v0, Lru/cn/tv/stb/categories/CategoriesInfo;

    invoke-direct {v0}, Lru/cn/tv/stb/categories/CategoriesInfo;-><init>()V

    .line 71
    .local v0, "info":Lru/cn/tv/stb/categories/CategoriesInfo;
    iput-boolean p1, v0, Lru/cn/tv/stb/categories/CategoriesInfo;->hasCameras:Z

    .line 72
    iput-boolean p2, v0, Lru/cn/tv/stb/categories/CategoriesInfo;->hasHDChannels:Z

    .line 73
    iput-boolean p3, v0, Lru/cn/tv/stb/categories/CategoriesInfo;->hasPorno:Z

    .line 75
    invoke-interface {p4}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_0

    .line 76
    invoke-interface {p4}, Landroid/database/Cursor;->moveToFirst()Z

    .line 78
    const-string v2, "_id"

    invoke-interface {p4, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p4, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, v0, Lru/cn/tv/stb/categories/CategoriesInfo;->contractorId:J

    .line 80
    const-string v2, "private_office_uri"

    invoke-interface {p4, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p4, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 81
    .local v1, "privateOffice":Ljava/lang/String;
    if-eqz v1, :cond_1

    const/4 v2, 0x1

    :goto_0
    iput-boolean v2, v0, Lru/cn/tv/stb/categories/CategoriesInfo;->hasPrivateOffice:Z

    .line 84
    .end local v1    # "privateOffice":Ljava/lang/String;
    :cond_0
    return-object v0

    .line 81
    .restart local v1    # "privateOffice":Ljava/lang/String;
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private pornoChannels()Lio/reactivex/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 98
    const-string v0, "porno"

    .line 99
    .local v0, "selection":Ljava/lang/String;
    iget-object v2, p0, Lru/cn/tv/stb/categories/CategoryViewModel;->ageFilter:Ljava/lang/String;

    invoke-static {v2}, Lru/cn/api/provider/TvContentProviderContract;->channels(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 101
    .local v1, "uri":Landroid/net/Uri;
    iget-object v2, p0, Lru/cn/tv/stb/categories/CategoryViewModel;->loader:Lru/cn/mvvm/RxLoader;

    invoke-virtual {v2, v1, v0}, Lru/cn/mvvm/RxLoader;->query(Landroid/net/Uri;Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object v2

    .line 102
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v2

    sget-object v3, Lru/cn/tv/stb/categories/CategoryViewModel$$Lambda$5;->$instance:Lio/reactivex/functions/Function;

    .line 103
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    sget-object v3, Lru/cn/tv/stb/categories/CategoryViewModel$$Lambda$6;->$instance:Lio/reactivex/functions/Function;

    .line 104
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->onErrorReturn(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    .line 101
    return-object v2
.end method


# virtual methods
.method final bridge synthetic bridge$lambda$0$CategoryViewModel(ZZZLandroid/database/Cursor;)Lru/cn/tv/stb/categories/CategoriesInfo;
    .locals 1

    invoke-direct {p0, p1, p2, p3, p4}, Lru/cn/tv/stb/categories/CategoryViewModel;->mapValues(ZZZLandroid/database/Cursor;)Lru/cn/tv/stb/categories/CategoriesInfo;

    move-result-object v0

    return-object v0
.end method

.method categories()Landroid/arch/lifecycle/LiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/arch/lifecycle/LiveData",
            "<",
            "Lru/cn/tv/stb/categories/CategoriesInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 47
    iget-object v0, p0, Lru/cn/tv/stb/categories/CategoryViewModel;->categories:Landroid/arch/lifecycle/MutableLiveData;

    return-object v0
.end method

.method final synthetic lambda$new$0$CategoryViewModel(Ljava/lang/Throwable;)V
    .locals 2
    .param p1, "throwable"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 41
    const-string v0, "Categories"

    const-string v1, "Unable to load categories "

    invoke-static {v0, v1, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 42
    iget-object v0, p0, Lru/cn/tv/stb/categories/CategoryViewModel;->categories:Landroid/arch/lifecycle/MutableLiveData;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/arch/lifecycle/MutableLiveData;->setValue(Ljava/lang/Object;)V

    .line 43
    return-void
.end method

.method selectCategory(Lru/cn/domain/tv/CurrentCategory$Type;)V
    .locals 1
    .param p1, "type"    # Lru/cn/domain/tv/CurrentCategory$Type;

    .prologue
    .line 55
    iget-object v0, p0, Lru/cn/tv/stb/categories/CategoryViewModel;->currentCategory:Lru/cn/domain/tv/CurrentCategory;

    invoke-virtual {v0, p1}, Lru/cn/domain/tv/CurrentCategory;->selectCategory(Lru/cn/domain/tv/CurrentCategory$Type;)V

    .line 56
    return-void
.end method

.method setCategory(Lru/cn/domain/tv/CurrentCategory$Type;)V
    .locals 1
    .param p1, "type"    # Lru/cn/domain/tv/CurrentCategory$Type;

    .prologue
    .line 51
    iget-object v0, p0, Lru/cn/tv/stb/categories/CategoryViewModel;->currentCategory:Lru/cn/domain/tv/CurrentCategory;

    invoke-virtual {v0, p1}, Lru/cn/domain/tv/CurrentCategory;->setCategory(Lru/cn/domain/tv/CurrentCategory$Type;)V

    .line 52
    return-void
.end method
