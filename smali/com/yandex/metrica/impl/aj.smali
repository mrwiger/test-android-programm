.class Lcom/yandex/metrica/impl/aj;
.super Ljava/lang/Thread;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/yandex/metrica/impl/aj$a;,
        Lcom/yandex/metrica/impl/aj$b;
    }
.end annotation


# instance fields
.field private final a:Ljava/util/concurrent/Executor;

.field private b:Ljava/util/concurrent/Executor;

.field private final c:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue",
            "<",
            "Lcom/yandex/metrica/impl/aj$b;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/lang/Object;

.field private final e:Ljava/lang/Object;

.field private volatile f:Lcom/yandex/metrica/impl/aj$b;

.field private g:Lcom/yandex/metrica/impl/al;

.field private volatile h:Z


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;Lcom/yandex/metrica/impl/ob/t;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 46
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 35
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, Lcom/yandex/metrica/impl/aj;->c:Ljava/util/concurrent/BlockingQueue;

    .line 36
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/yandex/metrica/impl/aj;->d:Ljava/lang/Object;

    .line 37
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/yandex/metrica/impl/aj;->e:Ljava/lang/Object;

    .line 44
    iput-boolean v5, p0, Lcom/yandex/metrica/impl/aj;->h:Z

    .line 47
    iput-object p1, p0, Lcom/yandex/metrica/impl/aj;->a:Ljava/util/concurrent/Executor;

    .line 48
    new-instance v0, Lcom/yandex/metrica/impl/ob/ep;

    invoke-direct {v0}, Lcom/yandex/metrica/impl/ob/ep;-><init>()V

    iput-object v0, p0, Lcom/yandex/metrica/impl/aj;->b:Ljava/util/concurrent/Executor;

    .line 49
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "[%s:%s]"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "NetworkTaskQueue"

    aput-object v4, v2, v3

    invoke-virtual {p2}, Lcom/yandex/metrica/impl/ob/t;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 50
    new-instance v0, Lcom/yandex/metrica/impl/al;

    invoke-direct {v0}, Lcom/yandex/metrica/impl/al;-><init>()V

    iput-object v0, p0, Lcom/yandex/metrica/impl/aj;->g:Lcom/yandex/metrica/impl/al;

    .line 51
    return-void
.end method

.method private a(Lcom/yandex/metrica/impl/aj$b;)Z
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/yandex/metrica/impl/aj;->c:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0, p1}, Ljava/util/concurrent/BlockingQueue;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/yandex/metrica/impl/aj;->f:Lcom/yandex/metrica/impl/aj$b;

    invoke-virtual {p1, v0}, Lcom/yandex/metrica/impl/aj$b;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 68
    iget-object v1, p0, Lcom/yandex/metrica/impl/aj;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 70
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/yandex/metrica/impl/aj;->h:Z

    .line 71
    iget-object v0, p0, Lcom/yandex/metrica/impl/aj;->f:Lcom/yandex/metrica/impl/aj$b;

    .line 72
    if-eqz v0, :cond_0

    .line 73
    iget-object v0, v0, Lcom/yandex/metrica/impl/aj$b;->a:Lcom/yandex/metrica/impl/ak;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ak;->x()V

    .line 75
    :cond_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/aj;->c:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->clear()V

    .line 77
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/aj;->interrupt()V

    .line 78
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lcom/yandex/metrica/impl/ak;)V
    .locals 3

    .prologue
    .line 55
    iget-object v1, p0, Lcom/yandex/metrica/impl/aj;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 56
    :try_start_0
    new-instance v0, Lcom/yandex/metrica/impl/aj$b;

    const/4 v2, 0x0

    invoke-direct {v0, p1, v2}, Lcom/yandex/metrica/impl/aj$b;-><init>(Lcom/yandex/metrica/impl/ak;B)V

    .line 60
    invoke-direct {p0, v0}, Lcom/yandex/metrica/impl/aj;->a(Lcom/yandex/metrica/impl/aj$b;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 61
    iget-object v2, p0, Lcom/yandex/metrica/impl/aj;->c:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v2, v0}, Ljava/util/concurrent/BlockingQueue;->offer(Ljava/lang/Object;)Z

    .line 64
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method b(Lcom/yandex/metrica/impl/ak;)Ljava/util/concurrent/Executor;
    .locals 1

    .prologue
    .line 110
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ak;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 112
    iget-object v0, p0, Lcom/yandex/metrica/impl/aj;->a:Ljava/util/concurrent/Executor;

    .line 115
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/aj;->b:Ljava/util/concurrent/Executor;

    goto :goto_0
.end method

.method public c(Lcom/yandex/metrica/impl/ak;)Z
    .locals 2

    .prologue
    .line 124
    new-instance v0, Lcom/yandex/metrica/impl/aj$b;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/yandex/metrica/impl/aj$b;-><init>(Lcom/yandex/metrica/impl/ak;B)V

    invoke-direct {p0, v0}, Lcom/yandex/metrica/impl/aj;->a(Lcom/yandex/metrica/impl/aj$b;)Z

    move-result v0

    return v0
.end method

.method d(Lcom/yandex/metrica/impl/ak;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 133
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ak;->b()Z

    move-result v0

    .line 135
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ak;->d()Lcom/yandex/metrica/impl/ob/es;

    move-result-object v2

    .line 137
    if-eqz v0, :cond_0

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/ob/es;->b()Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 146
    :cond_0
    :goto_0
    iget-boolean v2, p0, Lcom/yandex/metrica/impl/aj;->h:Z

    if-eqz v2, :cond_2

    if-eqz v0, :cond_2

    .line 154
    iget-object v0, p0, Lcom/yandex/metrica/impl/aj;->g:Lcom/yandex/metrica/impl/al;

    invoke-virtual {v0, p1}, Lcom/yandex/metrica/impl/al;->a(Lcom/yandex/metrica/impl/ak;)V

    .line 156
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ak;->c()Z

    move-result v0

    .line 157
    if-nez v0, :cond_1

    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ak;->u()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 159
    :goto_1
    if-eqz v0, :cond_0

    .line 160
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ak;->q()J

    move-result-wide v2

    .line 167
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 157
    goto :goto_1

    .line 171
    :cond_2
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ak;->f()V

    .line 172
    return-void
.end method

.method public run()V
    .locals 4

    .prologue
    .line 85
    :goto_0
    iget-boolean v0, p0, Lcom/yandex/metrica/impl/aj;->h:Z

    if-eqz v0, :cond_0

    .line 88
    :try_start_0
    iget-object v1, p0, Lcom/yandex/metrica/impl/aj;->e:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 89
    :try_start_2
    iget-object v0, p0, Lcom/yandex/metrica/impl/aj;->c:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->take()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/yandex/metrica/impl/aj$b;

    iput-object v0, p0, Lcom/yandex/metrica/impl/aj;->f:Lcom/yandex/metrica/impl/aj$b;

    .line 91
    iget-object v0, p0, Lcom/yandex/metrica/impl/aj;->f:Lcom/yandex/metrica/impl/aj$b;

    iget-object v0, v0, Lcom/yandex/metrica/impl/aj$b;->a:Lcom/yandex/metrica/impl/ak;

    .line 93
    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/aj;->b(Lcom/yandex/metrica/impl/ak;)Ljava/util/concurrent/Executor;

    move-result-object v1

    .line 1120
    new-instance v2, Lcom/yandex/metrica/impl/aj$a;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v0, v3}, Lcom/yandex/metrica/impl/aj$a;-><init>(Lcom/yandex/metrica/impl/aj;Lcom/yandex/metrica/impl/ak;B)V

    .line 93
    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    .line 99
    iget-object v1, p0, Lcom/yandex/metrica/impl/aj;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 100
    const/4 v0, 0x0

    :try_start_3
    iput-object v0, p0, Lcom/yandex/metrica/impl/aj;->f:Lcom/yandex/metrica/impl/aj$b;

    .line 101
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 88
    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    throw v0
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    .line 97
    :catch_0
    move-exception v0

    :try_start_6
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    .line 99
    iget-object v1, p0, Lcom/yandex/metrica/impl/aj;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 100
    const/4 v0, 0x0

    :try_start_7
    iput-object v0, p0, Lcom/yandex/metrica/impl/aj;->f:Lcom/yandex/metrica/impl/aj$b;

    .line 101
    monitor-exit v1

    goto :goto_0

    :catchall_2
    move-exception v0

    monitor-exit v1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    throw v0

    .line 99
    :catchall_3
    move-exception v0

    iget-object v1, p0, Lcom/yandex/metrica/impl/aj;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 100
    const/4 v2, 0x0

    :try_start_8
    iput-object v2, p0, Lcom/yandex/metrica/impl/aj;->f:Lcom/yandex/metrica/impl/aj$b;

    .line 101
    monitor-exit v1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_4

    .line 102
    throw v0

    .line 101
    :catchall_4
    move-exception v0

    :try_start_9
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    throw v0

    .line 106
    :cond_0
    return-void
.end method
