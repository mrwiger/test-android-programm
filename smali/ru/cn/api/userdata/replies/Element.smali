.class public final Lru/cn/api/userdata/replies/Element;
.super Ljava/lang/Object;
.source "Element.java"


# instance fields
.field public attributes:Lru/cn/api/userdata/replies/Attributes;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "attributes"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getElement(Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 17
    .local p1, "className":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    iget-object v0, p0, Lru/cn/api/userdata/replies/Element;->attributes:Lru/cn/api/userdata/replies/Attributes;

    invoke-virtual {v0, p1}, Lru/cn/api/userdata/replies/Attributes;->getAttributes(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
