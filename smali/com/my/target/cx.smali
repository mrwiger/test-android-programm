.class public final Lcom/my/target/cx;
.super Landroid/view/View;
.source "VideoProgressWheelNative.java"


# instance fields
.field private final aH:Landroid/graphics/Paint;

.field private final aI:Landroid/graphics/Paint;

.field private final aJ:Landroid/graphics/Paint;

.field private aK:Landroid/graphics/RectF;

.field private aL:J

.field private aM:F

.field private aN:F

.field private aO:F

.field private aP:Z

.field private aQ:I

.field private final uiUtils:Lcom/my/target/cm;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 48
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 28
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/my/target/cx;->aH:Landroid/graphics/Paint;

    .line 29
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/my/target/cx;->aI:Landroid/graphics/Paint;

    .line 30
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/my/target/cx;->aJ:Landroid/graphics/Paint;

    .line 33
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/my/target/cx;->aK:Landroid/graphics/RectF;

    .line 36
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/my/target/cx;->aL:J

    .line 38
    iput v2, p0, Lcom/my/target/cx;->aM:F

    .line 39
    iput v2, p0, Lcom/my/target/cx;->aN:F

    .line 40
    const/high16 v0, 0x43660000    # 230.0f

    iput v0, p0, Lcom/my/target/cx;->aO:F

    .line 41
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/my/target/cx;->aP:Z

    .line 49
    invoke-static {p1}, Lcom/my/target/cm;->x(Landroid/content/Context;)Lcom/my/target/cm;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/cx;->uiUtils:Lcom/my/target/cm;

    .line 50
    return-void
.end method


# virtual methods
.method protected final onDraw(Landroid/graphics/Canvas;)V
    .locals 8
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v7, 0x1

    const/4 v4, 0x0

    .line 154
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 156
    iget-object v0, p0, Lcom/my/target/cx;->aK:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/my/target/cx;->aI:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 160
    iget v0, p0, Lcom/my/target/cx;->aM:F

    iget v1, p0, Lcom/my/target/cx;->aN:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_2

    .line 165
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/my/target/cx;->aL:J

    sub-long/2addr v0, v2

    long-to-float v0, v0

    const/high16 v1, 0x447a0000    # 1000.0f

    div-float/2addr v0, v1

    .line 166
    iget v1, p0, Lcom/my/target/cx;->aO:F

    mul-float/2addr v0, v1

    .line 168
    iget v1, p0, Lcom/my/target/cx;->aM:F

    add-float/2addr v0, v1

    iget v1, p0, Lcom/my/target/cx;->aN:F

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iput v0, p0, Lcom/my/target/cx;->aM:F

    .line 169
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/my/target/cx;->aL:J

    move v6, v7

    .line 173
    :goto_0
    iget v3, p0, Lcom/my/target/cx;->aM:F

    .line 175
    invoke-virtual {p0}, Lcom/my/target/cx;->isInEditMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 177
    const/high16 v3, 0x43b40000    # 360.0f

    .line 180
    :cond_0
    iget-object v1, p0, Lcom/my/target/cx;->aK:Landroid/graphics/RectF;

    const/high16 v2, -0x3d4c0000    # -90.0f

    iget-object v5, p0, Lcom/my/target/cx;->aH:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 182
    iget-object v0, p0, Lcom/my/target/cx;->aJ:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 183
    iget-object v0, p0, Lcom/my/target/cx;->aJ:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/my/target/cx;->uiUtils:Lcom/my/target/cm;

    const/16 v2, 0xc

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 184
    iget-object v0, p0, Lcom/my/target/cx;->aJ:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 185
    iget-object v0, p0, Lcom/my/target/cx;->aJ:Landroid/graphics/Paint;

    invoke-virtual {v0, v7}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 187
    iget-object v0, p0, Lcom/my/target/cx;->aK:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerX()F

    move-result v0

    float-to-int v0, v0

    .line 188
    iget-object v1, p0, Lcom/my/target/cx;->aK:Landroid/graphics/RectF;

    .line 189
    invoke-virtual {v1}, Landroid/graphics/RectF;->centerY()F

    move-result v1

    iget-object v2, p0, Lcom/my/target/cx;->aJ:Landroid/graphics/Paint;

    invoke-virtual {v2}, Landroid/graphics/Paint;->descent()F

    move-result v2

    iget-object v3, p0, Lcom/my/target/cx;->aJ:Landroid/graphics/Paint;

    invoke-virtual {v3}, Landroid/graphics/Paint;->ascent()F

    move-result v3

    add-float/2addr v2, v3

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    sub-float/2addr v1, v2

    float-to-int v1, v1

    .line 191
    iget v2, p0, Lcom/my/target/cx;->aQ:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    int-to-float v0, v0

    int-to-float v1, v1

    iget-object v3, p0, Lcom/my/target/cx;->aJ:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v0, v1, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 193
    if-eqz v6, :cond_1

    .line 195
    invoke-virtual {p0}, Lcom/my/target/cx;->invalidate()V

    .line 197
    :cond_1
    return-void

    :cond_2
    move v6, v4

    goto :goto_0
.end method

.method protected final onMeasure(II)V
    .locals 8
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/16 v3, 0x1c

    const/high16 v7, -0x80000000

    const/high16 v6, 0x40000000    # 2.0f

    .line 100
    iget-object v0, p0, Lcom/my/target/cx;->uiUtils:Lcom/my/target/cm;

    .line 101
    invoke-virtual {v0, v3}, Lcom/my/target/cm;->n(I)I

    move-result v0

    invoke-virtual {p0}, Lcom/my/target/cx;->getPaddingLeft()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/my/target/cx;->getPaddingRight()I

    move-result v1

    add-int v2, v0, v1

    .line 102
    iget-object v0, p0, Lcom/my/target/cx;->uiUtils:Lcom/my/target/cm;

    .line 103
    invoke-virtual {v0, v3}, Lcom/my/target/cm;->n(I)I

    move-result v0

    invoke-virtual {p0}, Lcom/my/target/cx;->getPaddingTop()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/my/target/cx;->getPaddingBottom()I

    move-result v1

    add-int/2addr v0, v1

    .line 105
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v4

    .line 106
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 107
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v5

    .line 108
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 113
    if-eq v4, v6, :cond_4

    .line 117
    if-ne v4, v7, :cond_0

    .line 119
    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 126
    :cond_0
    :goto_0
    if-eq v5, v6, :cond_1

    if-ne v4, v6, :cond_3

    :cond_1
    move v0, v1

    .line 139
    :cond_2
    :goto_1
    invoke-virtual {p0, v2, v0}, Lcom/my/target/cx;->setMeasuredDimension(II)V

    .line 140
    return-void

    .line 130
    :cond_3
    if-ne v5, v7, :cond_2

    .line 132
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_1

    :cond_4
    move v2, v3

    goto :goto_0
.end method

.method protected final onSizeChanged(IIII)V
    .locals 7
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    const/4 v6, 0x1

    .line 145
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    .line 1225
    invoke-virtual {p0}, Lcom/my/target/cx;->getPaddingTop()I

    move-result v0

    .line 1226
    invoke-virtual {p0}, Lcom/my/target/cx;->getPaddingBottom()I

    move-result v1

    .line 1227
    invoke-virtual {p0}, Lcom/my/target/cx;->getPaddingLeft()I

    move-result v2

    .line 1228
    invoke-virtual {p0}, Lcom/my/target/cx;->getPaddingRight()I

    move-result v3

    .line 1230
    new-instance v4, Landroid/graphics/RectF;

    iget-object v5, p0, Lcom/my/target/cx;->uiUtils:Lcom/my/target/cm;

    invoke-virtual {v5, v6}, Lcom/my/target/cm;->n(I)I

    move-result v5

    add-int/2addr v2, v5

    int-to-float v2, v2

    iget-object v5, p0, Lcom/my/target/cx;->uiUtils:Lcom/my/target/cm;

    .line 1231
    invoke-virtual {v5, v6}, Lcom/my/target/cm;->n(I)I

    move-result v5

    add-int/2addr v0, v5

    int-to-float v0, v0

    sub-int v3, p1, v3

    iget-object v5, p0, Lcom/my/target/cx;->uiUtils:Lcom/my/target/cm;

    .line 1232
    invoke-virtual {v5, v6}, Lcom/my/target/cm;->n(I)I

    move-result v5

    sub-int/2addr v3, v5

    int-to-float v3, v3

    sub-int v1, p2, v1

    iget-object v5, p0, Lcom/my/target/cx;->uiUtils:Lcom/my/target/cm;

    .line 1233
    invoke-virtual {v5, v6}, Lcom/my/target/cm;->n(I)I

    move-result v5

    sub-int/2addr v1, v5

    int-to-float v1, v1

    invoke-direct {v4, v2, v0, v3, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v4, p0, Lcom/my/target/cx;->aK:Landroid/graphics/RectF;

    .line 2212
    iget-object v0, p0, Lcom/my/target/cx;->aH:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2213
    iget-object v0, p0, Lcom/my/target/cx;->aH:Landroid/graphics/Paint;

    invoke-virtual {v0, v6}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 2214
    iget-object v0, p0, Lcom/my/target/cx;->aH:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2215
    iget-object v0, p0, Lcom/my/target/cx;->aH:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/my/target/cx;->uiUtils:Lcom/my/target/cm;

    invoke-virtual {v1, v6}, Lcom/my/target/cm;->n(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2217
    iget-object v0, p0, Lcom/my/target/cx;->aI:Landroid/graphics/Paint;

    const/high16 v1, -0x78000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2218
    iget-object v0, p0, Lcom/my/target/cx;->aI:Landroid/graphics/Paint;

    invoke-virtual {v0, v6}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 2219
    iget-object v0, p0, Lcom/my/target/cx;->aI:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2220
    iget-object v0, p0, Lcom/my/target/cx;->aI:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/my/target/cx;->uiUtils:Lcom/my/target/cm;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 148
    invoke-virtual {p0}, Lcom/my/target/cx;->invalidate()V

    .line 149
    return-void
.end method

.method protected final onVisibilityChanged(Landroid/view/View;I)V
    .locals 2
    .param p1, "changedView"    # Landroid/view/View;
    .param p2, "visibility"    # I

    .prologue
    .line 202
    invoke-super {p0, p1, p2}, Landroid/view/View;->onVisibilityChanged(Landroid/view/View;I)V

    .line 204
    if-nez p2, :cond_0

    .line 206
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/my/target/cx;->aL:J

    .line 208
    :cond_0
    return-void
.end method

.method public final setDigit(I)V
    .locals 0
    .param p1, "digit"    # I

    .prologue
    .line 54
    iput p1, p0, Lcom/my/target/cx;->aQ:I

    .line 55
    return-void
.end method

.method public final setMax(F)V
    .locals 1
    .param p1, "max"    # F

    .prologue
    .line 91
    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-lez v0, :cond_0

    .line 93
    const/high16 v0, 0x43b40000    # 360.0f

    div-float/2addr v0, p1

    iput v0, p0, Lcom/my/target/cx;->aO:F

    .line 95
    :cond_0
    return-void
.end method

.method public final setProgress(F)V
    .locals 3
    .param p1, "progress"    # F

    .prologue
    const/high16 v2, 0x43b40000    # 360.0f

    const/4 v1, 0x0

    .line 59
    iget-boolean v0, p0, Lcom/my/target/cx;->aP:Z

    if-eqz v0, :cond_0

    .line 61
    iput v1, p0, Lcom/my/target/cx;->aM:F

    .line 62
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/my/target/cx;->aP:Z

    .line 65
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v0, p1, v0

    if-lez v0, :cond_2

    .line 67
    const/high16 p1, 0x3f800000    # 1.0f

    .line 74
    :cond_1
    :goto_0
    iget v0, p0, Lcom/my/target/cx;->aN:F

    cmpl-float v0, p1, v0

    if-nez v0, :cond_3

    .line 87
    :goto_1
    return-void

    .line 69
    :cond_2
    cmpg-float v0, p1, v1

    if-gez v0, :cond_1

    .line 71
    const/4 p1, 0x0

    goto :goto_0

    .line 79
    :cond_3
    iget v0, p0, Lcom/my/target/cx;->aM:F

    iget v1, p0, Lcom/my/target/cx;->aN:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_4

    .line 81
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/my/target/cx;->aL:J

    .line 84
    :cond_4
    mul-float v0, p1, v2

    invoke-static {v0, v2}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iput v0, p0, Lcom/my/target/cx;->aN:F

    .line 86
    invoke-virtual {p0}, Lcom/my/target/cx;->invalidate()V

    goto :goto_1
.end method
