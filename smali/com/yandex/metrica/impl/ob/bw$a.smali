.class Lcom/yandex/metrica/impl/ob/bw$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/yandex/metrica/impl/ob/bw;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/yandex/metrica/impl/ob/bw$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:I

.field private final b:J

.field private final c:Landroid/content/pm/ResolveInfo;


# direct methods
.method constructor <init>(IJLandroid/content/pm/ResolveInfo;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput p1, p0, Lcom/yandex/metrica/impl/ob/bw$a;->a:I

    .line 48
    iput-wide p2, p0, Lcom/yandex/metrica/impl/ob/bw$a;->b:J

    .line 49
    iput-object p4, p0, Lcom/yandex/metrica/impl/ob/bw$a;->c:Landroid/content/pm/ResolveInfo;

    .line 50
    return-void
.end method

.method constructor <init>(Landroid/content/pm/PackageManager;Landroid/content/pm/ResolveInfo;Lcom/yandex/metrica/impl/ob/ch;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 42
    .line 1078
    iget-object v0, p2, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v3, v0, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    .line 1079
    const-string v0, "android.permission.READ_PHONE_STATE"

    .line 1080
    invoke-static {p1, v3, v0}, Lcom/yandex/metrica/impl/ob/bw$a;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    mul-int/lit8 v0, v0, 0x1

    add-int/lit8 v4, v0, 0x0

    .line 1081
    const-string v0, "android.permission.ACCESS_COARSE_LOCATION"

    .line 1082
    invoke-static {p1, v3, v0}, Lcom/yandex/metrica/impl/ob/bw$a;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    mul-int/lit8 v0, v0, 0x2

    add-int/2addr v4, v0

    .line 1083
    const-string v0, "android.permission.ACCESS_FINE_LOCATION"

    .line 1084
    invoke-static {p1, v3, v0}, Lcom/yandex/metrica/impl/ob/bw$a;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    mul-int/lit8 v0, v0, 0x4

    add-int/2addr v4, v0

    .line 1085
    const-string v0, "android.permission.INTERNET"

    invoke-static {p1, v3, v0}, Lcom/yandex/metrica/impl/ob/bw$a;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    mul-int/lit8 v0, v0, 0x8

    add-int v3, v4, v0

    .line 1086
    iget-boolean v0, p3, Lcom/yandex/metrica/impl/ob/ch;->d:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    mul-int/lit8 v0, v0, 0x10

    add-int/2addr v3, v0

    .line 1093
    iget-object v0, p3, Lcom/yandex/metrica/impl/ob/ch;->a:Lcom/yandex/metrica/impl/ob/ck;

    if-nez v0, :cond_5

    move v0, v2

    .line 1087
    :goto_5
    if-eqz v0, :cond_6

    move v0, v1

    :goto_6
    mul-int/lit8 v0, v0, 0x20

    add-int/2addr v3, v0

    .line 1097
    iget-object v0, p3, Lcom/yandex/metrica/impl/ob/ch;->b:Lcom/yandex/metrica/impl/ob/cg;

    if-nez v0, :cond_7

    move v0, v2

    .line 1088
    :goto_7
    if-eqz v0, :cond_8

    :goto_8
    mul-int/lit8 v0, v1, 0x40

    add-int/2addr v0, v3

    .line 43
    invoke-static {p1, p2}, Lcom/yandex/metrica/impl/ob/bw$a;->a(Landroid/content/pm/PackageManager;Landroid/content/pm/ResolveInfo;)J

    move-result-wide v2

    .line 42
    invoke-direct {p0, v0, v2, v3, p2}, Lcom/yandex/metrica/impl/ob/bw$a;-><init>(IJLandroid/content/pm/ResolveInfo;)V

    .line 44
    return-void

    :cond_0
    move v0, v2

    .line 1080
    goto :goto_0

    :cond_1
    move v0, v2

    .line 1082
    goto :goto_1

    :cond_2
    move v0, v2

    .line 1084
    goto :goto_2

    :cond_3
    move v0, v2

    .line 1085
    goto :goto_3

    :cond_4
    move v0, v2

    .line 1086
    goto :goto_4

    .line 1093
    :cond_5
    iget-object v0, p3, Lcom/yandex/metrica/impl/ob/ch;->a:Lcom/yandex/metrica/impl/ob/ck;

    iget-boolean v0, v0, Lcom/yandex/metrica/impl/ob/ck;->i:Z

    goto :goto_5

    :cond_6
    move v0, v2

    .line 1087
    goto :goto_6

    .line 1097
    :cond_7
    iget-object v0, p3, Lcom/yandex/metrica/impl/ob/ch;->b:Lcom/yandex/metrica/impl/ob/cg;

    iget-boolean v0, v0, Lcom/yandex/metrica/impl/ob/cg;->i:Z

    goto :goto_7

    :cond_8
    move v1, v2

    .line 1088
    goto :goto_8
.end method

.method private static a(Landroid/content/pm/PackageManager;Landroid/content/pm/ResolveInfo;)J
    .locals 6

    .prologue
    .line 64
    const-wide/16 v0, -0x1

    .line 66
    :try_start_0
    iget-object v2, p1, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v2, v2, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 67
    iget-wide v4, v2, Landroid/content/pm/PackageInfo;->firstInstallTime:J

    iget-wide v2, v2, Landroid/content/pm/PackageInfo;->lastUpdateTime:J

    invoke-static {v4, v5, v2, v3}, Ljava/lang/Math;->max(JJ)J
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 72
    :goto_0
    return-wide v0

    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method private static a(Landroid/content/pm/PackageManager;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 102
    .line 104
    :try_start_0
    invoke-virtual {p0, p2, p1}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 108
    :cond_0
    :goto_0
    return v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method static synthetic b(Lcom/yandex/metrica/impl/ob/bw$a;)I
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lcom/yandex/metrica/impl/ob/bw$a;->a:I

    return v0
.end method

.method static synthetic c(Lcom/yandex/metrica/impl/ob/bw$a;)Landroid/content/pm/ResolveInfo;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bw$a;->c:Landroid/content/pm/ResolveInfo;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/yandex/metrica/impl/ob/bw$a;)I
    .locals 4

    .prologue
    .line 54
    iget v0, p0, Lcom/yandex/metrica/impl/ob/bw$a;->a:I

    iget v1, p1, Lcom/yandex/metrica/impl/ob/bw$a;->a:I

    if-eq v0, v1, :cond_0

    .line 55
    iget v0, p0, Lcom/yandex/metrica/impl/ob/bw$a;->a:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget v1, p1, Lcom/yandex/metrica/impl/ob/bw$a;->a:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Integer;->compareTo(Ljava/lang/Integer;)I

    move-result v0

    .line 60
    :goto_0
    return v0

    .line 57
    :cond_0
    iget-wide v0, p0, Lcom/yandex/metrica/impl/ob/bw$a;->b:J

    iget-wide v2, p1, Lcom/yandex/metrica/impl/ob/bw$a;->b:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 58
    iget-wide v0, p0, Lcom/yandex/metrica/impl/ob/bw$a;->b:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iget-wide v2, p1, Lcom/yandex/metrica/impl/ob/bw$a;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Long;->compareTo(Ljava/lang/Long;)I

    move-result v0

    goto :goto_0

    .line 60
    :cond_1
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bw$a;->c:Landroid/content/pm/ResolveInfo;

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v0, v0, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    iget-object v1, p1, Lcom/yandex/metrica/impl/ob/bw$a;->c:Landroid/content/pm/ResolveInfo;

    iget-object v1, v1, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v1, v1, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 35
    check-cast p1, Lcom/yandex/metrica/impl/ob/bw$a;

    invoke-virtual {p0, p1}, Lcom/yandex/metrica/impl/ob/bw$a;->a(Lcom/yandex/metrica/impl/ob/bw$a;)I

    move-result v0

    return v0
.end method
