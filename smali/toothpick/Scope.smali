.class public interface abstract Ltoothpick/Scope;
.super Ljava/lang/Object;
.source "Scope.java"


# virtual methods
.method public abstract bindScopeAnnotation(Ljava/lang/Class;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract getInstance(Ljava/lang/Class;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation
.end method

.method public abstract getInstance(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation
.end method

.method public abstract getLazy(Ljava/lang/Class;)Ltoothpick/Lazy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Ltoothpick/Lazy",
            "<TT;>;"
        }
    .end annotation
.end method

.method public abstract getLazy(Ljava/lang/Class;Ljava/lang/String;)Ltoothpick/Lazy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/String;",
            ")",
            "Ltoothpick/Lazy",
            "<TT;>;"
        }
    .end annotation
.end method

.method public abstract getName()Ljava/lang/Object;
.end method

.method public abstract getParentScope()Ltoothpick/Scope;
.end method

.method public abstract getParentScope(Ljava/lang/Class;)Ltoothpick/Scope;
.end method

.method public abstract getProvider(Ljava/lang/Class;)Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Ljavax/inject/Provider",
            "<TT;>;"
        }
    .end annotation
.end method

.method public abstract getProvider(Ljava/lang/Class;Ljava/lang/String;)Ljavax/inject/Provider;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;",
            "Ljava/lang/String;",
            ")",
            "Ljavax/inject/Provider",
            "<TT;>;"
        }
    .end annotation
.end method

.method public abstract getRootScope()Ltoothpick/Scope;
.end method

.method public varargs abstract installModules([Ltoothpick/config/Module;)V
.end method

.method public varargs abstract installTestModules([Ltoothpick/config/Module;)V
.end method

.method public abstract isBoundToScopeAnnotation(Ljava/lang/Class;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Ljava/lang/annotation/Annotation;",
            ">;)Z"
        }
    .end annotation
.end method
