.class public Lru/cn/notifications/entity/INotification$NewTelecastMessage;
.super Ljava/lang/Object;
.source "INotification.java"

# interfaces
.implements Lru/cn/notifications/entity/INotification;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/notifications/entity/INotification;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "NewTelecastMessage"
.end annotation


# instance fields
.field private final channelId:J

.field private final createTime:J

.field private final message:Ljava/lang/String;

.field private final telecastId:J


# direct methods
.method public constructor <init>(Ljava/lang/String;JJJ)V
    .locals 0
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "channelId"    # J
    .param p4, "telecastId"    # J
    .param p6, "createTime"    # J

    .prologue
    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    iput-object p1, p0, Lru/cn/notifications/entity/INotification$NewTelecastMessage;->message:Ljava/lang/String;

    .line 88
    iput-wide p2, p0, Lru/cn/notifications/entity/INotification$NewTelecastMessage;->channelId:J

    .line 89
    iput-wide p4, p0, Lru/cn/notifications/entity/INotification$NewTelecastMessage;->telecastId:J

    .line 90
    iput-wide p6, p0, Lru/cn/notifications/entity/INotification$NewTelecastMessage;->createTime:J

    .line 91
    return-void
.end method


# virtual methods
.method public channelId()J
    .locals 2

    .prologue
    .line 104
    iget-wide v0, p0, Lru/cn/notifications/entity/INotification$NewTelecastMessage;->channelId:J

    return-wide v0
.end method

.method public createTime()J
    .locals 2

    .prologue
    .line 100
    iget-wide v0, p0, Lru/cn/notifications/entity/INotification$NewTelecastMessage;->createTime:J

    return-wide v0
.end method

.method public message()Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lru/cn/notifications/entity/INotification$NewTelecastMessage;->message:Ljava/lang/String;

    return-object v0
.end method

.method public telecastId()J
    .locals 2

    .prologue
    .line 108
    iget-wide v0, p0, Lru/cn/notifications/entity/INotification$NewTelecastMessage;->telecastId:J

    return-wide v0
.end method
