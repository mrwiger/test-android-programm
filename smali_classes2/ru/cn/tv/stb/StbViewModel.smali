.class final Lru/cn/tv/stb/StbViewModel;
.super Lru/cn/mvvm/RxViewModel;
.source "StbViewModel.java"


# instance fields
.field private final category:Landroid/arch/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/arch/lifecycle/MutableLiveData",
            "<",
            "Lru/cn/domain/tv/CurrentCategory$Type;",
            ">;"
        }
    .end annotation
.end field

.field private final context:Landroid/content/Context;

.field private final loader:Lru/cn/mvvm/RxLoader;

.field private final relatedRubric:Landroid/arch/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/arch/lifecycle/MutableLiveData",
            "<",
            "Landroid/util/Pair",
            "<",
            "Lru/cn/api/catalogue/replies/Rubric;",
            "Landroid/database/Cursor;",
            ">;>;"
        }
    .end annotation
.end field

.field private final relatedRubricIn:Lio/reactivex/subjects/PublishSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/subjects/PublishSubject",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/content/Context;Lru/cn/mvvm/RxLoader;Lru/cn/domain/tv/CurrentCategory;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "loader"    # Lru/cn/mvvm/RxLoader;
    .param p3, "currentCategory"    # Lru/cn/domain/tv/CurrentCategory;

    .prologue
    const/4 v3, 0x0

    .line 41
    invoke-direct {p0}, Lru/cn/mvvm/RxViewModel;-><init>()V

    .line 42
    iput-object p1, p0, Lru/cn/tv/stb/StbViewModel;->context:Landroid/content/Context;

    .line 43
    iput-object p2, p0, Lru/cn/tv/stb/StbViewModel;->loader:Lru/cn/mvvm/RxLoader;

    .line 45
    new-instance v1, Landroid/arch/lifecycle/MutableLiveData;

    invoke-direct {v1}, Landroid/arch/lifecycle/MutableLiveData;-><init>()V

    iput-object v1, p0, Lru/cn/tv/stb/StbViewModel;->relatedRubric:Landroid/arch/lifecycle/MutableLiveData;

    .line 46
    new-instance v1, Landroid/arch/lifecycle/MutableLiveData;

    invoke-direct {v1}, Landroid/arch/lifecycle/MutableLiveData;-><init>()V

    iput-object v1, p0, Lru/cn/tv/stb/StbViewModel;->category:Landroid/arch/lifecycle/MutableLiveData;

    .line 49
    invoke-virtual {p3}, Lru/cn/domain/tv/CurrentCategory;->category()Lio/reactivex/Observable;

    move-result-object v1

    .line 50
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    iget-object v2, p0, Lru/cn/tv/stb/StbViewModel;->category:Landroid/arch/lifecycle/MutableLiveData;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v2}, Lru/cn/tv/stb/StbViewModel$$Lambda$0;->get$Lambda(Landroid/arch/lifecycle/MutableLiveData;)Lio/reactivex/functions/Consumer;

    move-result-object v2

    .line 51
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 48
    invoke-virtual {p0, v1}, Lru/cn/tv/stb/StbViewModel;->bind(Lio/reactivex/disposables/Disposable;)V

    .line 53
    invoke-static {}, Lio/reactivex/subjects/PublishSubject;->create()Lio/reactivex/subjects/PublishSubject;

    move-result-object v1

    iput-object v1, p0, Lru/cn/tv/stb/StbViewModel;->relatedRubricIn:Lio/reactivex/subjects/PublishSubject;

    .line 54
    invoke-static {v3, v3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    .line 56
    .local v0, "emptyResult":Landroid/util/Pair;, "Landroid/util/Pair<Lru/cn/api/catalogue/replies/Rubric;Landroid/database/Cursor;>;"
    iget-object v1, p0, Lru/cn/tv/stb/StbViewModel;->relatedRubricIn:Lio/reactivex/subjects/PublishSubject;

    new-instance v2, Lru/cn/tv/stb/StbViewModel$$Lambda$1;

    invoke-direct {v2, p0, v0}, Lru/cn/tv/stb/StbViewModel$$Lambda$1;-><init>(Lru/cn/tv/stb/StbViewModel;Landroid/util/Pair;)V

    .line 58
    invoke-virtual {v1, v2}, Lio/reactivex/subjects/PublishSubject;->doOnNext(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v1

    new-instance v2, Lru/cn/tv/stb/StbViewModel$$Lambda$2;

    invoke-direct {v2, p0}, Lru/cn/tv/stb/StbViewModel$$Lambda$2;-><init>(Lru/cn/tv/stb/StbViewModel;)V

    .line 59
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->switchMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v1

    new-instance v2, Lru/cn/tv/stb/StbViewModel$$Lambda$3;

    invoke-direct {v2, p0, v0}, Lru/cn/tv/stb/StbViewModel$$Lambda$3;-><init>(Lru/cn/tv/stb/StbViewModel;Landroid/util/Pair;)V

    .line 60
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->switchMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v1

    .line 73
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    iget-object v2, p0, Lru/cn/tv/stb/StbViewModel;->relatedRubric:Landroid/arch/lifecycle/MutableLiveData;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v2}, Lru/cn/tv/stb/StbViewModel$$Lambda$4;->get$Lambda(Landroid/arch/lifecycle/MutableLiveData;)Lio/reactivex/functions/Consumer;

    move-result-object v2

    new-instance v3, Lru/cn/tv/stb/StbViewModel$$Lambda$5;

    invoke-direct {v3, p0, v0}, Lru/cn/tv/stb/StbViewModel$$Lambda$5;-><init>(Lru/cn/tv/stb/StbViewModel;Landroid/util/Pair;)V

    .line 74
    invoke-virtual {v1, v2, v3}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 56
    invoke-virtual {p0, v1}, Lru/cn/tv/stb/StbViewModel;->bind(Lio/reactivex/disposables/Disposable;)V

    .line 76
    return-void
.end method

.method private blockChannel(Landroid/database/Cursor;)Lio/reactivex/Single;
    .locals 4
    .param p1, "channel"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")",
            "Lio/reactivex/Single",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 145
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_0

    .line 146
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 148
    invoke-static {p1}, Lru/cn/api/parental/CursorConverter;->convert(Landroid/database/Cursor;)Landroid/content/ContentValues;

    move-result-object v1

    .line 149
    .local v1, "values":Landroid/content/ContentValues;
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 151
    invoke-static {}, Lru/cn/api/parental/BlockedChannelProviderContract;->blockedChannelsUri()Landroid/net/Uri;

    move-result-object v0

    .line 152
    .local v0, "blockedChannelsUri":Landroid/net/Uri;
    iget-object v2, p0, Lru/cn/tv/stb/StbViewModel;->loader:Lru/cn/mvvm/RxLoader;

    invoke-virtual {v2, v0, v1}, Lru/cn/mvvm/RxLoader;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Lio/reactivex/Single;

    move-result-object v2

    .line 153
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v2

    .line 156
    .end local v0    # "blockedChannelsUri":Landroid/net/Uri;
    .end local v1    # "values":Landroid/content/ContentValues;
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v2}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object v2

    goto :goto_0
.end method

.method private channel(J)Lio/reactivex/Observable;
    .locals 3
    .param p1, "channelId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lio/reactivex/Observable",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 138
    invoke-static {p1, p2}, Lru/cn/api/provider/TvContentProviderContract;->channel(J)Landroid/net/Uri;

    move-result-object v0

    .line 140
    .local v0, "uri":Landroid/net/Uri;
    iget-object v1, p0, Lru/cn/tv/stb/StbViewModel;->loader:Lru/cn/mvvm/RxLoader;

    invoke-virtual {v1, v0}, Lru/cn/mvvm/RxLoader;->query(Landroid/net/Uri;)Lio/reactivex/Observable;

    move-result-object v1

    .line 141
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    .line 140
    return-object v1
.end method

.method static final synthetic lambda$null$1$StbViewModel(Landroid/util/Pair;Lcom/annimon/stream/Optional;Landroid/database/Cursor;)Landroid/util/Pair;
    .locals 1
    .param p0, "emptyResult"    # Landroid/util/Pair;
    .param p1, "rubric"    # Lcom/annimon/stream/Optional;
    .param p2, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 66
    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-gtz v0, :cond_1

    .line 70
    .end local p0    # "emptyResult":Landroid/util/Pair;
    :cond_0
    :goto_0
    return-object p0

    .restart local p0    # "emptyResult":Landroid/util/Pair;
    :cond_1
    invoke-virtual {p1}, Lcom/annimon/stream/Optional;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0, p2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object p0

    goto :goto_0
.end method

.method static final synthetic lambda$relatedRubric$6$StbViewModel(Landroid/database/Cursor;)Lcom/annimon/stream/Optional;
    .locals 3
    .param p0, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 117
    invoke-interface {p0}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-gtz v2, :cond_0

    .line 118
    invoke-static {}, Lcom/annimon/stream/Optional;->empty()Lcom/annimon/stream/Optional;

    move-result-object v2

    .line 125
    :goto_0
    return-object v2

    .line 120
    :cond_0
    invoke-interface {p0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 121
    const-string v2, "data"

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 122
    .local v0, "d":Ljava/lang/String;
    invoke-static {v0}, Lru/cn/api/catalogue/replies/Rubric;->fromJson(Ljava/lang/String;)Lru/cn/api/catalogue/replies/Rubric;

    move-result-object v1

    .line 123
    .local v1, "rubric":Lru/cn/api/catalogue/replies/Rubric;
    invoke-interface {p0}, Landroid/database/Cursor;->close()V

    .line 125
    invoke-static {v1}, Lcom/annimon/stream/Optional;->ofNullable(Ljava/lang/Object;)Lcom/annimon/stream/Optional;

    move-result-object v2

    goto :goto_0
.end method

.method private relatedElements(Lru/cn/api/catalogue/replies/Rubric;)Lio/reactivex/Observable;
    .locals 4
    .param p1, "rubric"    # Lru/cn/api/catalogue/replies/Rubric;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lru/cn/api/catalogue/replies/Rubric;",
            ")",
            "Lio/reactivex/Observable",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 130
    invoke-virtual {p1}, Lru/cn/api/catalogue/replies/Rubric;->defaultFilter()Landroid/support/v4/util/LongSparseArray;

    move-result-object v0

    .line 131
    .local v0, "options":Landroid/support/v4/util/LongSparseArray;, "Landroid/support/v4/util/LongSparseArray<Ljava/lang/String;>;"
    iget-wide v2, p1, Lru/cn/api/catalogue/replies/Rubric;->id:J

    invoke-static {v2, v3, v0}, Lru/cn/api/provider/TvContentProviderContract;->rubricItemsUri(JLandroid/support/v4/util/LongSparseArray;)Landroid/net/Uri;

    move-result-object v1

    .line 133
    .local v1, "uri":Landroid/net/Uri;
    iget-object v2, p0, Lru/cn/tv/stb/StbViewModel;->loader:Lru/cn/mvvm/RxLoader;

    invoke-virtual {v2, v1}, Lru/cn/mvvm/RxLoader;->query(Landroid/net/Uri;)Lio/reactivex/Observable;

    move-result-object v2

    .line 134
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v2

    .line 133
    return-object v2
.end method

.method private relatedRubric(J)Lio/reactivex/Observable;
    .locals 5
    .param p1, "telecastId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lio/reactivex/Observable",
            "<",
            "Lcom/annimon/stream/Optional",
            "<",
            "Lru/cn/api/catalogue/replies/Rubric;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 109
    const-wide/16 v2, 0x0

    cmp-long v1, p1, v2

    if-gtz v1, :cond_0

    .line 110
    invoke-static {}, Lcom/annimon/stream/Optional;->empty()Lcom/annimon/stream/Optional;

    move-result-object v1

    invoke-static {v1}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v1

    .line 114
    :goto_0
    return-object v1

    .line 112
    :cond_0
    invoke-static {p1, p2}, Lru/cn/api/provider/TvContentProviderContract;->rubricRelated(J)Landroid/net/Uri;

    move-result-object v0

    .line 114
    .local v0, "uri":Landroid/net/Uri;
    iget-object v1, p0, Lru/cn/tv/stb/StbViewModel;->loader:Lru/cn/mvvm/RxLoader;

    invoke-virtual {v1, v0}, Lru/cn/mvvm/RxLoader;->query(Landroid/net/Uri;)Lio/reactivex/Observable;

    move-result-object v1

    .line 115
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lru/cn/tv/stb/StbViewModel$$Lambda$8;->$instance:Lio/reactivex/functions/Function;

    .line 116
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v1

    goto :goto_0
.end method

.method private unblockChannel(J)Lio/reactivex/Single;
    .locals 7
    .param p1, "channelId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lio/reactivex/Single",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 160
    invoke-static {}, Lru/cn/api/parental/BlockedChannelProviderContract;->blockedChannelsUri()Landroid/net/Uri;

    move-result-object v0

    .line 162
    .local v0, "blockedChannelsUri":Landroid/net/Uri;
    iget-object v1, p0, Lru/cn/tv/stb/StbViewModel;->loader:Lru/cn/mvvm/RxLoader;

    const/4 v2, 0x0

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v0, v2, v3}, Lru/cn/mvvm/RxLoader;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v1

    .line 163
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v1

    .line 162
    return-object v1
.end method


# virtual methods
.method final bridge synthetic bridge$lambda$0$StbViewModel(J)Lio/reactivex/Observable;
    .locals 1

    invoke-direct {p0, p1, p2}, Lru/cn/tv/stb/StbViewModel;->relatedRubric(J)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method category()Landroid/arch/lifecycle/LiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/arch/lifecycle/LiveData",
            "<",
            "Lru/cn/domain/tv/CurrentCategory$Type;",
            ">;"
        }
    .end annotation

    .prologue
    .line 79
    iget-object v0, p0, Lru/cn/tv/stb/StbViewModel;->category:Landroid/arch/lifecycle/MutableLiveData;

    return-object v0
.end method

.method final synthetic lambda$new$0$StbViewModel(Landroid/util/Pair;Ljava/lang/Long;)V
    .locals 1
    .param p1, "emptyResult"    # Landroid/util/Pair;
    .param p2, "it"    # Ljava/lang/Long;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 58
    iget-object v0, p0, Lru/cn/tv/stb/StbViewModel;->relatedRubric:Landroid/arch/lifecycle/MutableLiveData;

    invoke-virtual {v0, p1}, Landroid/arch/lifecycle/MutableLiveData;->setValue(Ljava/lang/Object;)V

    return-void
.end method

.method final synthetic lambda$new$2$StbViewModel(Landroid/util/Pair;Lcom/annimon/stream/Optional;)Lio/reactivex/ObservableSource;
    .locals 2
    .param p1, "emptyResult"    # Landroid/util/Pair;
    .param p2, "rubric"    # Lcom/annimon/stream/Optional;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 61
    invoke-virtual {p2}, Lcom/annimon/stream/Optional;->isPresent()Z

    move-result v0

    if-nez v0, :cond_0

    .line 62
    invoke-static {p1}, Lio/reactivex/Observable;->just(Ljava/lang/Object;)Lio/reactivex/Observable;

    move-result-object v0

    .line 64
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p2}, Lcom/annimon/stream/Optional;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/api/catalogue/replies/Rubric;

    invoke-direct {p0, v0}, Lru/cn/tv/stb/StbViewModel;->relatedElements(Lru/cn/api/catalogue/replies/Rubric;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lru/cn/tv/stb/StbViewModel$$Lambda$9;

    invoke-direct {v1, p1, p2}, Lru/cn/tv/stb/StbViewModel$$Lambda$9;-><init>(Landroid/util/Pair;Lcom/annimon/stream/Optional;)V

    .line 65
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    goto :goto_0
.end method

.method final synthetic lambda$new$3$StbViewModel(Landroid/util/Pair;Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "emptyResult"    # Landroid/util/Pair;
    .param p2, "throwable"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 75
    iget-object v0, p0, Lru/cn/tv/stb/StbViewModel;->relatedRubric:Landroid/arch/lifecycle/MutableLiveData;

    invoke-virtual {v0, p1}, Landroid/arch/lifecycle/MutableLiveData;->setValue(Ljava/lang/Object;)V

    return-void
.end method

.method final synthetic lambda$setBlocked$4$StbViewModel(ZJLandroid/database/Cursor;)Lio/reactivex/SingleSource;
    .locals 2
    .param p1, "blocked"    # Z
    .param p2, "channelId"    # J
    .param p4, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 94
    if-eqz p1, :cond_0

    .line 95
    invoke-direct {p0, p4}, Lru/cn/tv/stb/StbViewModel;->blockChannel(Landroid/database/Cursor;)Lio/reactivex/Single;

    move-result-object v0

    .line 98
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p2, p3}, Lru/cn/tv/stb/StbViewModel;->unblockChannel(J)Lio/reactivex/Single;

    move-result-object v0

    goto :goto_0
.end method

.method final synthetic lambda$setBlocked$5$StbViewModel(Ljava/lang/Integer;)V
    .locals 2
    .param p1, "cursor"    # Ljava/lang/Integer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 101
    invoke-static {}, Lru/cn/api/provider/TvContentProviderContract;->channels()Landroid/net/Uri;

    move-result-object v0

    .line 102
    .local v0, "channels":Landroid/net/Uri;
    iget-object v1, p0, Lru/cn/tv/stb/StbViewModel;->loader:Lru/cn/mvvm/RxLoader;

    invoke-virtual {v1, v0}, Lru/cn/mvvm/RxLoader;->notifyChange(Landroid/net/Uri;)V

    .line 103
    return-void
.end method

.method relatedRubric()Landroid/arch/lifecycle/LiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/arch/lifecycle/LiveData",
            "<",
            "Landroid/util/Pair",
            "<",
            "Lru/cn/api/catalogue/replies/Rubric;",
            "Landroid/database/Cursor;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 83
    iget-object v0, p0, Lru/cn/tv/stb/StbViewModel;->relatedRubric:Landroid/arch/lifecycle/MutableLiveData;

    return-object v0
.end method

.method setBlocked(JZ)V
    .locals 5
    .param p1, "channelId"    # J
    .param p3, "blocked"    # Z

    .prologue
    .line 91
    invoke-direct {p0, p1, p2}, Lru/cn/tv/stb/StbViewModel;->channel(J)Lio/reactivex/Observable;

    move-result-object v1

    const-wide/16 v2, 0x1

    .line 92
    invoke-virtual {v1, v2, v3}, Lio/reactivex/Observable;->take(J)Lio/reactivex/Observable;

    move-result-object v1

    new-instance v2, Lru/cn/tv/stb/StbViewModel$$Lambda$6;

    invoke-direct {v2, p0, p3, p1, p2}, Lru/cn/tv/stb/StbViewModel$$Lambda$6;-><init>(Lru/cn/tv/stb/StbViewModel;ZJ)V

    .line 93
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->switchMapSingle(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v1

    new-instance v2, Lru/cn/tv/stb/StbViewModel$$Lambda$7;

    invoke-direct {v2, p0}, Lru/cn/tv/stb/StbViewModel$$Lambda$7;-><init>(Lru/cn/tv/stb/StbViewModel;)V

    .line 100
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 105
    .local v0, "disposable":Lio/reactivex/disposables/Disposable;
    invoke-virtual {p0, v0}, Lru/cn/tv/stb/StbViewModel;->bind(Lio/reactivex/disposables/Disposable;)V

    .line 106
    return-void
.end method

.method setTelecast(J)V
    .locals 3
    .param p1, "telecast"    # J

    .prologue
    .line 87
    iget-object v0, p0, Lru/cn/tv/stb/StbViewModel;->relatedRubricIn:Lio/reactivex/subjects/PublishSubject;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/subjects/PublishSubject;->onNext(Ljava/lang/Object;)V

    .line 88
    return-void
.end method
