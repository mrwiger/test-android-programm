.class public final Lcom/my/target/ej;
.super Landroid/widget/RelativeLayout;
.source "CardHorizontalView.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# static fields
.field private static final bD:I

.field private static final bf:I


# instance fields
.field private final aX:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final aw:Lcom/my/target/cm;

.field private be:Landroid/view/View$OnClickListener;

.field private final cE:Lcom/my/target/bx;

.field private final cF:Landroid/widget/FrameLayout;

.field private final ch:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/ej;->bf:I

    .line 27
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/ej;->bD:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/4 v8, -0x2

    const/16 v7, 0xc

    const/4 v6, 0x2

    const/4 v5, -0x1

    .line 40
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 34
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/my/target/ej;->aX:Ljava/util/HashMap;

    .line 41
    invoke-static {p1}, Lcom/my/target/cm;->x(Landroid/content/Context;)Lcom/my/target/cm;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/ej;->aw:Lcom/my/target/cm;

    .line 42
    new-instance v0, Lcom/my/target/bx;

    invoke-direct {v0, p1}, Lcom/my/target/bx;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/ej;->cE:Lcom/my/target/bx;

    .line 43
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-direct {v0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/ej;->cF:Landroid/widget/FrameLayout;

    .line 44
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/ej;->ch:Landroid/widget/TextView;

    .line 1115
    iget-object v0, p0, Lcom/my/target/ej;->cE:Lcom/my/target/bx;

    sget v1, Lcom/my/target/ej;->bf:I

    invoke-virtual {v0, v1}, Lcom/my/target/bx;->setId(I)V

    .line 1118
    iget-object v0, p0, Lcom/my/target/ej;->ch:Landroid/widget/TextView;

    sget v1, Lcom/my/target/ej;->bD:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setId(I)V

    .line 1119
    iget-object v0, p0, Lcom/my/target/ej;->ch:Landroid/widget/TextView;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1120
    iget-object v0, p0, Lcom/my/target/ej;->ch:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setLines(I)V

    .line 1121
    iget-object v0, p0, Lcom/my/target/ej;->ch:Landroid/widget/TextView;

    const/high16 v1, 0x41900000    # 18.0f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 1122
    iget-object v0, p0, Lcom/my/target/ej;->ch:Landroid/widget/TextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 1123
    iget-object v0, p0, Lcom/my/target/ej;->ch:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/my/target/ej;->aw:Lcom/my/target/cm;

    invoke-virtual {v1, v7}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iget-object v2, p0, Lcom/my/target/ej;->aw:Lcom/my/target/cm;

    invoke-virtual {v2, v4}, Lcom/my/target/cm;->n(I)I

    move-result v2

    iget-object v3, p0, Lcom/my/target/ej;->aw:Lcom/my/target/cm;

    invoke-virtual {v3, v4}, Lcom/my/target/cm;->n(I)I

    move-result v3

    iget-object v4, p0, Lcom/my/target/ej;->aw:Lcom/my/target/cm;

    invoke-virtual {v4, v7}, Lcom/my/target/cm;->n(I)I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 1124
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v8, v8}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1125
    iget-object v1, p0, Lcom/my/target/ej;->aw:Lcom/my/target/cm;

    invoke-virtual {v1, v6}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 1126
    invoke-virtual {v0, v7, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1127
    iget-object v1, p0, Lcom/my/target/ej;->ch:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1128
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v5, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1129
    sget v1, Lcom/my/target/ej;->bD:I

    invoke-virtual {v0, v6, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 1130
    iget-object v1, p0, Lcom/my/target/ej;->ch:Landroid/widget/TextView;

    invoke-virtual {p0, v1}, Lcom/my/target/ej;->addView(Landroid/view/View;)V

    .line 1131
    iget-object v1, p0, Lcom/my/target/ej;->cF:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1132
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v5, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 1133
    const/16 v1, 0x11

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 1134
    iget-object v1, p0, Lcom/my/target/ej;->cF:Landroid/widget/FrameLayout;

    iget-object v2, p0, Lcom/my/target/ej;->cE:Lcom/my/target/bx;

    invoke-virtual {v1, v2, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1135
    iget-object v0, p0, Lcom/my/target/ej;->cF:Landroid/widget/FrameLayout;

    invoke-virtual {p0, v0}, Lcom/my/target/ej;->addView(Landroid/view/View;)V

    .line 46
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View$OnClickListener;Lcom/my/target/af;)V
    .locals 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ClickableViewAccessibility"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 63
    iput-object p1, p0, Lcom/my/target/ej;->be:Landroid/view/View$OnClickListener;

    .line 64
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 66
    :cond_0
    const/4 v0, 0x0

    invoke-super {p0, v0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 76
    :goto_0
    return-void

    .line 70
    :cond_1
    invoke-virtual {p0, p0}, Lcom/my/target/ej;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 71
    iget-object v0, p0, Lcom/my/target/ej;->cE:Lcom/my/target/bx;

    invoke-virtual {v0, p0}, Lcom/my/target/bx;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 72
    iget-object v0, p0, Lcom/my/target/ej;->ch:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 73
    iget-object v3, p0, Lcom/my/target/ej;->aX:Ljava/util/HashMap;

    iget-object v4, p0, Lcom/my/target/ej;->cE:Lcom/my/target/bx;

    iget-boolean v0, p2, Lcom/my/target/af;->cv:Z

    if-nez v0, :cond_2

    iget-boolean v0, p2, Lcom/my/target/af;->cE:Z

    if-eqz v0, :cond_6

    :cond_2
    move v0, v2

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    iget-object v3, p0, Lcom/my/target/ej;->aX:Ljava/util/HashMap;

    iget-boolean v0, p2, Lcom/my/target/af;->cD:Z

    if-nez v0, :cond_3

    iget-boolean v0, p2, Lcom/my/target/af;->cE:Z

    if-eqz v0, :cond_7

    :cond_3
    move v0, v2

    :goto_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v3, p0, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    iget-object v0, p0, Lcom/my/target/ej;->aX:Ljava/util/HashMap;

    iget-object v3, p0, Lcom/my/target/ej;->ch:Landroid/widget/TextView;

    iget-boolean v4, p2, Lcom/my/target/af;->cs:Z

    if-nez v4, :cond_4

    iget-boolean v4, p2, Lcom/my/target/af;->cE:Z

    if-eqz v4, :cond_5

    :cond_4
    move v1, v2

    :cond_5
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_6
    move v0, v1

    .line 73
    goto :goto_1

    :cond_7
    move v0, v1

    .line 74
    goto :goto_2
.end method

.method public final getImageView()Lcom/my/target/bx;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/my/target/ej;->cE:Lcom/my/target/bx;

    return-object v0
.end method

.method public final getTitleTextView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/my/target/ej;->ch:Landroid/widget/TextView;

    return-object v0
.end method

.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 81
    iget-object v0, p0, Lcom/my/target/ej;->aX:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 109
    :goto_0
    return v0

    .line 85
    :cond_0
    iget-object v0, p0, Lcom/my/target/ej;->aX:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v2

    .line 87
    goto :goto_0

    .line 89
    :cond_1
    invoke-virtual {p1, v2}, Landroid/view/View;->setClickable(Z)V

    .line 90
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_1
    :pswitch_0
    move v0, v2

    .line 109
    goto :goto_0

    .line 93
    :pswitch_1
    const v0, -0x3a1508

    invoke-virtual {p0, v0}, Lcom/my/target/ej;->setBackgroundColor(I)V

    goto :goto_1

    .line 96
    :pswitch_2
    invoke-virtual {p0, v1}, Lcom/my/target/ej;->setBackgroundColor(I)V

    .line 97
    iget-object v0, p0, Lcom/my/target/ej;->be:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_2

    .line 99
    iget-object v0, p0, Lcom/my/target/ej;->be:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 101
    :cond_2
    invoke-virtual {p0, v1}, Lcom/my/target/ej;->setBackgroundColor(I)V

    goto :goto_1

    .line 104
    :pswitch_3
    invoke-virtual {p0, v1}, Lcom/my/target/ej;->setBackgroundColor(I)V

    goto :goto_1

    .line 90
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
