.class Lcom/samsung/multiscreen/PhotoPlayer$1;
.super Ljava/lang/Object;
.source "PhotoPlayer.java"

# interfaces
.implements Lcom/samsung/multiscreen/Result;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/multiscreen/PhotoPlayer;->addToList(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/samsung/multiscreen/Result",
        "<",
        "Lcom/samsung/multiscreen/Player$DMPStatus;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/multiscreen/PhotoPlayer;

.field final synthetic val$photoList:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/samsung/multiscreen/PhotoPlayer;Ljava/util/List;)V
    .locals 0
    .param p1, "this$0"    # Lcom/samsung/multiscreen/PhotoPlayer;

    .prologue
    .line 226
    iput-object p1, p0, Lcom/samsung/multiscreen/PhotoPlayer$1;->this$0:Lcom/samsung/multiscreen/PhotoPlayer;

    iput-object p2, p0, Lcom/samsung/multiscreen/PhotoPlayer$1;->val$photoList:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/samsung/multiscreen/Error;)V
    .locals 3
    .param p1, "error"    # Lcom/samsung/multiscreen/Error;

    .prologue
    .line 274
    iget-object v0, p0, Lcom/samsung/multiscreen/PhotoPlayer$1;->this$0:Lcom/samsung/multiscreen/PhotoPlayer;

    invoke-virtual {v0}, Lcom/samsung/multiscreen/PhotoPlayer;->isDebug()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PhotoPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "enQueue() Error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/multiscreen/Error;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 275
    :cond_0
    return-void
.end method

.method public onSuccess(Lcom/samsung/multiscreen/Player$DMPStatus;)V
    .locals 9
    .param p1, "status"    # Lcom/samsung/multiscreen/Player$DMPStatus;

    .prologue
    .line 229
    if-nez p1, :cond_1

    .line 230
    const-string v6, "PhotoPlayer"

    const-string v7, "Error : Something went wrong with Node server!"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 270
    :cond_0
    :goto_0
    return-void

    .line 238
    :cond_1
    iget-object v6, p1, Lcom/samsung/multiscreen/Player$DMPStatus;->mDMPRunning:Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eqz v6, :cond_4

    iget-object v6, p1, Lcom/samsung/multiscreen/Player$DMPStatus;->mRunning:Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 239
    const/4 v4, 0x0

    .local v4, "it":I
    :goto_1
    iget-object v6, p0, Lcom/samsung/multiscreen/PhotoPlayer$1;->val$photoList:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-ge v4, v6, :cond_0

    .line 240
    const/4 v0, 0x0

    .line 242
    .local v0, "contentTitle":Ljava/lang/String;
    iget-object v6, p0, Lcom/samsung/multiscreen/PhotoPlayer$1;->val$photoList:Ljava/util/List;

    invoke-interface {v6, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map;

    .line 244
    .local v5, "item":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v6, "uri"

    invoke-interface {v5, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 245
    const-string v6, "uri"

    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 252
    .local v1, "contentUri":Landroid/net/Uri;
    sget-object v6, Lcom/samsung/multiscreen/PhotoPlayer$PhotoPlayerAttributes;->title:Lcom/samsung/multiscreen/PhotoPlayer$PhotoPlayerAttributes;

    invoke-virtual {v6}, Lcom/samsung/multiscreen/PhotoPlayer$PhotoPlayerAttributes;->name()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 253
    sget-object v6, Lcom/samsung/multiscreen/PhotoPlayer$PhotoPlayerAttributes;->title:Lcom/samsung/multiscreen/PhotoPlayer$PhotoPlayerAttributes;

    invoke-virtual {v6}, Lcom/samsung/multiscreen/PhotoPlayer$PhotoPlayerAttributes;->name()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "contentTitle":Ljava/lang/String;
    check-cast v0, Ljava/lang/String;

    .line 256
    .restart local v0    # "contentTitle":Ljava/lang/String;
    :cond_2
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 258
    .local v2, "data":Lorg/json/JSONObject;
    :try_start_0
    const-string v6, "subEvent"

    sget-object v7, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;->enqueue:Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;

    invoke-virtual {v7}, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;->name()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 259
    const-string v6, "playerType"

    sget-object v7, Lcom/samsung/multiscreen/Player$ContentType;->photo:Lcom/samsung/multiscreen/Player$ContentType;

    invoke-virtual {v7}, Lcom/samsung/multiscreen/Player$ContentType;->name()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 260
    const-string v6, "uri"

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 261
    sget-object v6, Lcom/samsung/multiscreen/PhotoPlayer$PhotoPlayerAttributes;->title:Lcom/samsung/multiscreen/PhotoPlayer$PhotoPlayerAttributes;

    invoke-virtual {v6}, Lcom/samsung/multiscreen/PhotoPlayer$PhotoPlayerAttributes;->name()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 265
    :goto_2
    sget-object v6, Lcom/samsung/multiscreen/Player;->mApplication:Lcom/samsung/multiscreen/Application;

    const-string v7, "playerQueueEvent"

    invoke-virtual {v6, v7, v2}, Lcom/samsung/multiscreen/Application;->publish(Ljava/lang/String;Ljava/lang/Object;)V

    .line 239
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 247
    .end local v1    # "contentUri":Landroid/net/Uri;
    .end local v2    # "data":Lorg/json/JSONObject;
    :cond_3
    iget-object v6, p0, Lcom/samsung/multiscreen/PhotoPlayer$1;->this$0:Lcom/samsung/multiscreen/PhotoPlayer;

    invoke-virtual {v6}, Lcom/samsung/multiscreen/PhotoPlayer;->isDebug()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 248
    const-string v6, "PhotoPlayer"

    const-string v7, "enQueue(): ContentUrl can not be Optional."

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 262
    .restart local v1    # "contentUri":Landroid/net/Uri;
    .restart local v2    # "data":Lorg/json/JSONObject;
    :catch_0
    move-exception v3

    .line 263
    .local v3, "exception":Ljava/lang/Exception;
    const-string v6, "PhotoPlayer"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "enQueue(): Error in parsing JSON object: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v3}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 268
    .end local v0    # "contentTitle":Ljava/lang/String;
    .end local v1    # "contentUri":Landroid/net/Uri;
    .end local v2    # "data":Lorg/json/JSONObject;
    .end local v3    # "exception":Ljava/lang/Exception;
    .end local v4    # "it":I
    .end local v5    # "item":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_4
    iget-object v6, p0, Lcom/samsung/multiscreen/PhotoPlayer$1;->this$0:Lcom/samsung/multiscreen/PhotoPlayer;

    invoke-virtual {v6}, Lcom/samsung/multiscreen/PhotoPlayer;->isDebug()Z

    move-result v6

    if-eqz v6, :cond_0

    const-string v6, "PhotoPlayer"

    const-string v7, "enQueue() Error: DMP Un-Initialized!"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 226
    check-cast p1, Lcom/samsung/multiscreen/Player$DMPStatus;

    invoke-virtual {p0, p1}, Lcom/samsung/multiscreen/PhotoPlayer$1;->onSuccess(Lcom/samsung/multiscreen/Player$DMPStatus;)V

    return-void
.end method
