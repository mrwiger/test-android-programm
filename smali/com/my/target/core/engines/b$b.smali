.class final Lcom/my/target/core/engines/b$b;
.super Ljava/lang/Object;
.source "StandardAdEngine.java"

# interfaces
.implements Lcom/my/target/core/presenters/b$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/my/target/core/engines/b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "b"
.end annotation


# instance fields
.field final synthetic g:Lcom/my/target/core/engines/b;


# direct methods
.method private constructor <init>(Lcom/my/target/core/engines/b;)V
    .locals 0

    .prologue
    .line 217
    iput-object p1, p0, Lcom/my/target/core/engines/b$b;->g:Lcom/my/target/core/engines/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/my/target/core/engines/b;B)V
    .locals 0

    .prologue
    .line 217
    invoke-direct {p0, p1}, Lcom/my/target/core/engines/b$b;-><init>(Lcom/my/target/core/engines/b;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/my/target/ah;)V
    .locals 2

    .prologue
    .line 241
    invoke-virtual {p1}, Lcom/my/target/ah;->getStatHolder()Lcom/my/target/ar;

    move-result-object v0

    .line 242
    const-string v1, "playbackStarted"

    invoke-virtual {v0, v1}, Lcom/my/target/ar;->w(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/core/engines/b$b;->g:Lcom/my/target/core/engines/b;

    invoke-static {v1}, Lcom/my/target/core/engines/b;->c(Lcom/my/target/core/engines/b;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/my/target/cl;->a(Ljava/util/List;Landroid/content/Context;)V

    .line 243
    return-void
.end method

.method public final a(Lcom/my/target/ah;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 222
    iget-object v0, p0, Lcom/my/target/core/engines/b$b;->g:Lcom/my/target/core/engines/b;

    invoke-static {v0}, Lcom/my/target/core/engines/b;->a(Lcom/my/target/core/engines/b;)Lcom/my/target/ads/MyTargetView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/my/target/ads/MyTargetView;->getListener()Lcom/my/target/ads/MyTargetView$MyTargetViewListener;

    move-result-object v0

    .line 223
    if-eqz v0, :cond_0

    .line 225
    iget-object v1, p0, Lcom/my/target/core/engines/b$b;->g:Lcom/my/target/core/engines/b;

    invoke-static {v1}, Lcom/my/target/core/engines/b;->a(Lcom/my/target/core/engines/b;)Lcom/my/target/ads/MyTargetView;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/my/target/ads/MyTargetView$MyTargetViewListener;->onClick(Lcom/my/target/ads/MyTargetView;)V

    .line 227
    :cond_0
    invoke-static {}, Lcom/my/target/ce;->bw()Lcom/my/target/ce;

    move-result-object v0

    .line 228
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 230
    iget-object v1, p0, Lcom/my/target/core/engines/b$b;->g:Lcom/my/target/core/engines/b;

    invoke-static {v1}, Lcom/my/target/core/engines/b;->c(Lcom/my/target/core/engines/b;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/my/target/ce;->a(Lcom/my/target/ah;Landroid/content/Context;)V

    .line 236
    :goto_0
    return-void

    .line 234
    :cond_1
    iget-object v1, p0, Lcom/my/target/core/engines/b$b;->g:Lcom/my/target/core/engines/b;

    invoke-static {v1}, Lcom/my/target/core/engines/b;->c(Lcom/my/target/core/engines/b;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, p1, p2, v1}, Lcom/my/target/ce;->a(Lcom/my/target/ah;Ljava/lang/String;Landroid/content/Context;)V

    goto :goto_0
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 248
    iget-object v0, p0, Lcom/my/target/core/engines/b$b;->g:Lcom/my/target/core/engines/b;

    invoke-static {v0}, Lcom/my/target/core/engines/b;->d(Lcom/my/target/core/engines/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/my/target/core/engines/b$b;->g:Lcom/my/target/core/engines/b;

    invoke-static {v0}, Lcom/my/target/core/engines/b;->e(Lcom/my/target/core/engines/b;)Lcom/my/target/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/my/target/b;->isRefreshAd()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 250
    const-string v0, "load new standard ad"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 251
    iget-object v0, p0, Lcom/my/target/core/engines/b$b;->g:Lcom/my/target/core/engines/b;

    invoke-static {v0}, Lcom/my/target/core/engines/b;->g(Lcom/my/target/core/engines/b;)Lcom/my/target/c;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/core/engines/b$b;->g:Lcom/my/target/core/engines/b;

    invoke-static {v1}, Lcom/my/target/core/engines/b;->f(Lcom/my/target/core/engines/b;)Lcom/my/target/core/engines/b$a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/my/target/c;->a(Lcom/my/target/c$b;)Lcom/my/target/c;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/core/engines/b$b;->g:Lcom/my/target/core/engines/b;

    invoke-static {v1}, Lcom/my/target/core/engines/b;->c(Lcom/my/target/core/engines/b;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/my/target/c;->a(Landroid/content/Context;)Lcom/my/target/c;

    .line 257
    :goto_0
    return-void

    .line 255
    :cond_0
    const-string v0, "not allowed to load new ad"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto :goto_0
.end method
