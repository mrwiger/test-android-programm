.class Lru/cn/tv/billing/BillingFragment$StoreJsObject;
.super Ljava/lang/Object;
.source "BillingFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/tv/billing/BillingFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "StoreJsObject"
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/tv/billing/BillingFragment;


# direct methods
.method private constructor <init>(Lru/cn/tv/billing/BillingFragment;)V
    .locals 0

    .prologue
    .line 129
    iput-object p1, p0, Lru/cn/tv/billing/BillingFragment$StoreJsObject;->this$0:Lru/cn/tv/billing/BillingFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lru/cn/tv/billing/BillingFragment;Lru/cn/tv/billing/BillingFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lru/cn/tv/billing/BillingFragment;
    .param p2, "x1"    # Lru/cn/tv/billing/BillingFragment$1;

    .prologue
    .line 129
    invoke-direct {p0, p1}, Lru/cn/tv/billing/BillingFragment$StoreJsObject;-><init>(Lru/cn/tv/billing/BillingFragment;)V

    return-void
.end method


# virtual methods
.method public manageSubscriptions(I)V
    .locals 2
    .param p1, "storeId"    # I
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 146
    const-string v0, "BillingFragment"

    const-string v1, "manage subscribe"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    iget-object v0, p0, Lru/cn/tv/billing/BillingFragment$StoreJsObject;->this$0:Lru/cn/tv/billing/BillingFragment;

    invoke-static {v0}, Lru/cn/tv/billing/BillingFragment;->access$000(Lru/cn/tv/billing/BillingFragment;)Lru/cn/tv/billing/BillingFragment$BillingFragmentListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 149
    iget-object v0, p0, Lru/cn/tv/billing/BillingFragment$StoreJsObject;->this$0:Lru/cn/tv/billing/BillingFragment;

    invoke-virtual {v0}, Lru/cn/tv/billing/BillingFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Lru/cn/tv/billing/BillingFragment$StoreJsObject$2;

    invoke-direct {v1, p0, p1}, Lru/cn/tv/billing/BillingFragment$StoreJsObject$2;-><init>(Lru/cn/tv/billing/BillingFragment$StoreJsObject;I)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 156
    :cond_0
    return-void
.end method

.method public purchase(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "store_id"    # Ljava/lang/String;
    .param p2, "product_id"    # Ljava/lang/String;
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 132
    const-string v0, "BillingFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "get purchase store: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " product: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    iget-object v0, p0, Lru/cn/tv/billing/BillingFragment$StoreJsObject;->this$0:Lru/cn/tv/billing/BillingFragment;

    invoke-static {v0}, Lru/cn/tv/billing/BillingFragment;->access$000(Lru/cn/tv/billing/BillingFragment;)Lru/cn/tv/billing/BillingFragment$BillingFragmentListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 135
    iget-object v0, p0, Lru/cn/tv/billing/BillingFragment$StoreJsObject;->this$0:Lru/cn/tv/billing/BillingFragment;

    invoke-virtual {v0}, Lru/cn/tv/billing/BillingFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Lru/cn/tv/billing/BillingFragment$StoreJsObject$1;

    invoke-direct {v1, p0, p2}, Lru/cn/tv/billing/BillingFragment$StoreJsObject$1;-><init>(Lru/cn/tv/billing/BillingFragment$StoreJsObject;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 142
    :cond_0
    return-void
.end method
