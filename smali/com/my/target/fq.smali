.class public final Lcom/my/target/fq;
.super Lcom/my/target/ak;
.source "InstreamAdSection.java"


# instance fields
.field private final H:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/my/target/al",
            "<",
            "Lcom/my/target/common/models/VideoData;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 3

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/my/target/ak;-><init>()V

    .line 26
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/my/target/fq;->H:Ljava/util/HashMap;

    .line 30
    iget-object v0, p0, Lcom/my/target/fq;->H:Ljava/util/HashMap;

    const-string v1, "preroll"

    const-string v2, "preroll"

    invoke-static {v2}, Lcom/my/target/al;->q(Ljava/lang/String;)Lcom/my/target/al;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    iget-object v0, p0, Lcom/my/target/fq;->H:Ljava/util/HashMap;

    const-string v1, "pauseroll"

    const-string v2, "pauseroll"

    invoke-static {v2}, Lcom/my/target/al;->q(Ljava/lang/String;)Lcom/my/target/al;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    iget-object v0, p0, Lcom/my/target/fq;->H:Ljava/util/HashMap;

    const-string v1, "midroll"

    const-string v2, "midroll"

    invoke-static {v2}, Lcom/my/target/al;->q(Ljava/lang/String;)Lcom/my/target/al;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    iget-object v0, p0, Lcom/my/target/fq;->H:Ljava/util/HashMap;

    const-string v1, "postroll"

    const-string v2, "postroll"

    invoke-static {v2}, Lcom/my/target/al;->q(Ljava/lang/String;)Lcom/my/target/al;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    return-void
.end method

.method public static h()Lcom/my/target/fq;
    .locals 1

    .prologue
    .line 23
    new-instance v0, Lcom/my/target/fq;

    invoke-direct {v0}, Lcom/my/target/fq;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/my/target/al;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/my/target/al",
            "<",
            "Lcom/my/target/common/models/VideoData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 38
    iget-object v0, p0, Lcom/my/target/fq;->H:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/al;

    return-object v0
.end method

.method public final getBannersCount()I
    .locals 3

    .prologue
    .line 49
    const/4 v0, 0x0

    .line 50
    iget-object v1, p0, Lcom/my/target/fq;->H:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/al;

    .line 52
    invoke-virtual {v0}, Lcom/my/target/al;->getBannersCount()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    .line 53
    goto :goto_0

    .line 54
    :cond_0
    return v1
.end method

.method public final hasContent()Z
    .locals 3

    .prologue
    .line 59
    iget-object v0, p0, Lcom/my/target/fq;->H:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/al;

    .line 61
    invoke-virtual {v0}, Lcom/my/target/al;->getBannersCount()I

    move-result v2

    if-gtz v2, :cond_1

    invoke-virtual {v0}, Lcom/my/target/al;->V()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    :cond_1
    const/4 v0, 0x1

    .line 66
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/my/target/al",
            "<",
            "Lcom/my/target/common/models/VideoData;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 43
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/my/target/fq;->H:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method
