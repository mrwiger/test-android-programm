.class Lru/cn/ad/natives/adapters/AdmobNativeAdapter$2;
.super Ljava/lang/Object;
.source "AdmobNativeAdapter.java"

# interfaces
.implements Lcom/google/android/gms/ads/formats/NativeContentAd$OnContentAdLoadedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/ad/natives/adapters/AdmobNativeAdapter;->onLoad()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/ad/natives/adapters/AdmobNativeAdapter;


# direct methods
.method constructor <init>(Lru/cn/ad/natives/adapters/AdmobNativeAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/ad/natives/adapters/AdmobNativeAdapter;

    .prologue
    .line 53
    iput-object p1, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$2;->this$0:Lru/cn/ad/natives/adapters/AdmobNativeAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onContentAdLoaded(Lcom/google/android/gms/ads/formats/NativeContentAd;)V
    .locals 3
    .param p1, "nativeContentAd"    # Lcom/google/android/gms/ads/formats/NativeContentAd;

    .prologue
    .line 56
    const-string v0, "AdmobNativeAdapter"

    const-string v1, "content ad"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 57
    iget-object v0, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$2;->this$0:Lru/cn/ad/natives/adapters/AdmobNativeAdapter;

    new-instance v1, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$ContentBinder;

    iget-object v2, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$2;->this$0:Lru/cn/ad/natives/adapters/AdmobNativeAdapter;

    invoke-static {v2}, Lru/cn/ad/natives/adapters/AdmobNativeAdapter;->access$500(Lru/cn/ad/natives/adapters/AdmobNativeAdapter;)Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$ContentBinder;-><init>(Landroid/content/Context;Lcom/google/android/gms/ads/formats/NativeContentAd;)V

    invoke-static {v0, v1}, Lru/cn/ad/natives/adapters/AdmobNativeAdapter;->access$402(Lru/cn/ad/natives/adapters/AdmobNativeAdapter;Lru/cn/ad/natives/adapters/AdmobNativeAdapter$AdmobBinder;)Lru/cn/ad/natives/adapters/AdmobNativeAdapter$AdmobBinder;

    .line 59
    iget-object v0, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$2;->this$0:Lru/cn/ad/natives/adapters/AdmobNativeAdapter;

    invoke-static {v0}, Lru/cn/ad/natives/adapters/AdmobNativeAdapter;->access$600(Lru/cn/ad/natives/adapters/AdmobNativeAdapter;)Lru/cn/ad/AdAdapter$Listener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 60
    iget-object v0, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$2;->this$0:Lru/cn/ad/natives/adapters/AdmobNativeAdapter;

    invoke-static {v0}, Lru/cn/ad/natives/adapters/AdmobNativeAdapter;->access$700(Lru/cn/ad/natives/adapters/AdmobNativeAdapter;)Lru/cn/ad/AdAdapter$Listener;

    move-result-object v0

    invoke-interface {v0}, Lru/cn/ad/AdAdapter$Listener;->onAdLoaded()V

    .line 61
    :cond_0
    return-void
.end method
