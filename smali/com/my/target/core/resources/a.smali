.class public final Lcom/my/target/core/resources/a;
.super Ljava/lang/Object;
.source "NativeAdResources.java"


# direct methods
.method private static a(FILandroid/graphics/Paint;Landroid/graphics/Canvas;)Landroid/graphics/Canvas;
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/high16 v10, 0x42700000    # 60.0f

    const/high16 v9, 0x41b80000    # 23.0f

    const/high16 v8, 0x40000000    # 2.0f

    const/high16 v7, 0x42180000    # 38.0f

    .line 232
    const/high16 v0, 0x40400000    # 3.0f

    mul-float/2addr v0, p0

    .line 234
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 236
    invoke-virtual {v1, v11}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 237
    const/high16 v2, -0x78000000

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 238
    new-instance v2, Landroid/graphics/RectF;

    const/4 v3, 0x0

    const/4 v4, 0x0

    int-to-float v5, p1

    int-to-float v6, p1

    invoke-direct {v2, v3, v4, v5, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 239
    invoke-virtual {p3, v2, v1}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 241
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 242
    invoke-virtual {v1, v11}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 243
    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 244
    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 245
    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 246
    int-to-float v2, p1

    div-float/2addr v2, v8

    int-to-float v3, p1

    div-float/2addr v3, v8

    int-to-float v4, p1

    div-float/2addr v4, v8

    div-float v5, v0, v8

    sub-float/2addr v4, v5

    invoke-virtual {p3, v2, v3, v4, v1}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 248
    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 249
    const/4 v0, -0x1

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 250
    invoke-virtual {p2, v11}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 252
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    .line 253
    sget-object v1, Landroid/graphics/Path$FillType;->EVEN_ODD:Landroid/graphics/Path$FillType;

    invoke-virtual {v0, v1}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    .line 254
    mul-float v1, v9, p0

    mul-float v2, v7, p0

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 255
    mul-float v1, v9, p0

    mul-float v2, v10, p0

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 256
    mul-float v1, v7, p0

    mul-float v2, v10, p0

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 257
    mul-float v1, v7, p0

    mul-float v2, v7, p0

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 259
    const/high16 v1, 0x42600000    # 56.0f

    mul-float/2addr v1, p0

    const/high16 v2, 0x41d80000    # 27.0f

    mul-float/2addr v2, p0

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 260
    const/high16 v1, 0x42600000    # 56.0f

    mul-float/2addr v1, p0

    const/high16 v2, 0x428e0000    # 71.0f

    mul-float/2addr v2, p0

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 261
    mul-float v1, v7, p0

    mul-float v2, v10, p0

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 262
    mul-float v1, v7, p0

    mul-float v2, v7, p0

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 263
    mul-float v1, v9, p0

    mul-float v2, v7, p0

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 265
    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 267
    invoke-virtual {p3, v0, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 268
    return-object p3
.end method

.method public static getPlayIcon(I)Landroid/graphics/Bitmap;
    .locals 10
    .param p0, "d"    # I

    .prologue
    const/4 v0, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 65
    div-int/lit8 v1, p0, 0x4

    div-int/lit8 v2, p0, 0x20

    add-int/2addr v2, v1

    .line 66
    div-int/lit8 v3, p0, 0x8

    .line 68
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 73
    :try_start_0
    invoke-static {p0, p0, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 79
    :goto_0
    if-nez v1, :cond_0

    .line 117
    :goto_1
    return-object v0

    .line 77
    :catch_0
    move-exception v1

    const-string v1, "cannot build play icon: OOME"

    invoke-static {v1}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    move-object v1, v0

    goto :goto_0

    .line 84
    :cond_0
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 86
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    .line 88
    invoke-virtual {v4, v9}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 89
    const/high16 v5, -0x78000000

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 90
    new-instance v5, Landroid/graphics/RectF;

    int-to-float v6, p0

    int-to-float v7, p0

    invoke-direct {v5, v8, v8, v6, v7}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 91
    invoke-virtual {v0, v5, v4}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 93
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    .line 95
    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 96
    invoke-virtual {v0, v4}, Landroid/graphics/Canvas;->drawPaint(Landroid/graphics/Paint;)V

    .line 98
    const/high16 v5, 0x40800000    # 4.0f

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 99
    const v5, -0xff540e

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 100
    sget-object v5, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 101
    invoke-virtual {v4, v9}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 103
    new-instance v5, Landroid/graphics/Point;

    mul-int/lit8 v6, v3, 0x3

    invoke-direct {v5, v6, v2}, Landroid/graphics/Point;-><init>(II)V

    .line 104
    new-instance v6, Landroid/graphics/Point;

    mul-int/lit8 v7, v3, 0x3

    sub-int v2, p0, v2

    invoke-direct {v6, v7, v2}, Landroid/graphics/Point;-><init>(II)V

    .line 105
    new-instance v2, Landroid/graphics/Point;

    mul-int/lit8 v3, v3, 0x2

    sub-int v3, p0, v3

    div-int/lit8 v7, p0, 0x2

    invoke-direct {v2, v3, v7}, Landroid/graphics/Point;-><init>(II)V

    .line 107
    new-instance v3, Landroid/graphics/Path;

    invoke-direct {v3}, Landroid/graphics/Path;-><init>()V

    .line 108
    sget-object v7, Landroid/graphics/Path$FillType;->EVEN_ODD:Landroid/graphics/Path$FillType;

    invoke-virtual {v3, v7}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    .line 109
    iget v7, v5, Landroid/graphics/Point;->x:I

    int-to-float v7, v7

    iget v8, v5, Landroid/graphics/Point;->y:I

    int-to-float v8, v8

    invoke-virtual {v3, v7, v8}, Landroid/graphics/Path;->moveTo(FF)V

    .line 110
    iget v7, v6, Landroid/graphics/Point;->x:I

    int-to-float v7, v7

    iget v6, v6, Landroid/graphics/Point;->y:I

    int-to-float v6, v6

    invoke-virtual {v3, v7, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 111
    iget v6, v2, Landroid/graphics/Point;->x:I

    int-to-float v6, v6

    iget v2, v2, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    invoke-virtual {v3, v6, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 112
    iget v2, v5, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    iget v5, v5, Landroid/graphics/Point;->y:I

    int-to-float v5, v5

    invoke-virtual {v3, v2, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 113
    invoke-virtual {v3}, Landroid/graphics/Path;->close()V

    .line 115
    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    move-object v0, v1

    .line 117
    goto/16 :goto_1
.end method

.method public static getVolumeOffIcon(I)Landroid/graphics/Bitmap;
    .locals 11
    .param p0, "diameter"    # I

    .prologue
    const/4 v0, 0x0

    const/high16 v10, 0x42a40000    # 82.0f

    const/high16 v9, 0x42780000    # 62.0f

    const/high16 v8, 0x42700000    # 60.0f

    const/high16 v7, 0x42200000    # 40.0f

    .line 152
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 157
    :try_start_0
    invoke-static {p0, p0, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 163
    :goto_0
    if-nez v1, :cond_0

    .line 187
    :goto_1
    return-object v0

    .line 161
    :catch_0
    move-exception v1

    const-string v1, "cannot build icon: OOME"

    invoke-static {v1}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    move-object v1, v0

    goto :goto_0

    .line 168
    :cond_0
    int-to-float v0, p0

    const/high16 v2, 0x42c80000    # 100.0f

    div-float/2addr v0, v2

    .line 170
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 171
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 172
    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 174
    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 176
    invoke-static {v0, p0, v2, v3}, Lcom/my/target/core/resources/a;->a(FILandroid/graphics/Paint;Landroid/graphics/Canvas;)Landroid/graphics/Canvas;

    .line 178
    new-instance v4, Landroid/graphics/Path;

    invoke-direct {v4}, Landroid/graphics/Path;-><init>()V

    .line 179
    sget-object v5, Landroid/graphics/Path$FillType;->EVEN_ODD:Landroid/graphics/Path$FillType;

    invoke-virtual {v4, v5}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    .line 180
    mul-float v5, v9, v0

    mul-float v6, v7, v0

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Path;->moveTo(FF)V

    .line 181
    mul-float v5, v10, v0

    mul-float v6, v8, v0

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Path;->lineTo(FF)V

    .line 182
    mul-float v5, v9, v0

    mul-float v6, v8, v0

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Path;->moveTo(FF)V

    .line 183
    mul-float v5, v10, v0

    mul-float/2addr v0, v7

    invoke-virtual {v4, v5, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 185
    invoke-virtual {v3, v4, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    move-object v0, v1

    .line 187
    goto :goto_1
.end method

.method public static getVolumeOnIcon(I)Landroid/graphics/Bitmap;
    .locals 12
    .param p0, "diameter"    # I

    .prologue
    const/4 v0, 0x0

    const/high16 v2, 0x42b40000    # 90.0f

    const/high16 v3, -0x3ccc0000    # -180.0f

    const/4 v4, 0x0

    .line 192
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 197
    :try_start_0
    invoke-static {p0, p0, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 203
    :goto_0
    if-nez v6, :cond_0

    .line 224
    :goto_1
    return-object v0

    .line 201
    :catch_0
    move-exception v1

    const-string v1, "cannot build icon: OOME"

    invoke-static {v1}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    move-object v6, v0

    goto :goto_0

    .line 208
    :cond_0
    int-to-float v0, p0

    const/high16 v1, 0x42c80000    # 100.0f

    div-float v7, v0, v1

    .line 210
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    .line 211
    invoke-virtual {v5, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 212
    sget-object v0, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 214
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v6}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 216
    invoke-static {v7, p0, v5, v0}, Lcom/my/target/core/resources/a;->a(FILandroid/graphics/Paint;Landroid/graphics/Canvas;)Landroid/graphics/Canvas;

    .line 218
    new-instance v1, Landroid/graphics/RectF;

    const/high16 v8, 0x42640000    # 57.0f

    mul-float/2addr v8, v7

    const/high16 v9, 0x42340000    # 45.0f

    mul-float/2addr v9, v7

    const/high16 v10, 0x42860000    # 67.0f

    mul-float/2addr v10, v7

    const/high16 v11, 0x425c0000    # 55.0f

    mul-float/2addr v11, v7

    invoke-direct {v1, v8, v9, v10, v11}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 219
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 221
    new-instance v1, Landroid/graphics/RectF;

    const/high16 v8, 0x42500000    # 52.0f

    mul-float/2addr v8, v7

    const/high16 v9, 0x42200000    # 40.0f

    mul-float/2addr v9, v7

    const/high16 v10, 0x42900000    # 72.0f

    mul-float/2addr v10, v7

    const/high16 v11, 0x42700000    # 60.0f

    mul-float/2addr v7, v11

    invoke-direct {v1, v8, v9, v10, v7}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 222
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    move-object v0, v6

    .line 224
    goto :goto_1
.end method
