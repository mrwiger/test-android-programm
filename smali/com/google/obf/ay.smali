.class final Lcom/google/obf/ay;
.super Ljava/lang/Object;
.source "IMASDK"


# static fields
.field private static final a:I

.field private static final b:[Ljava/nio/charset/Charset;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 161
    const-string v0, "ID3"

    invoke-static {v0}, Lcom/google/obf/ea;->c(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/google/obf/ay;->a:I

    .line 162
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/nio/charset/Charset;

    const/4 v1, 0x0

    const-string v2, "ISO-8859-1"

    invoke-static {v2}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "UTF-16LE"

    .line 163
    invoke-static {v2}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "UTF-16BE"

    invoke-static {v2}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "UTF-8"

    invoke-static {v2}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v2

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/obf/ay;->b:[Ljava/nio/charset/Charset;

    .line 164
    return-void
.end method

.method private static a(ILcom/google/obf/dw;)Landroid/util/Pair;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/obf/dw;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x2

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v7, 0x4

    const/4 v1, 0x0

    .line 56
    :goto_0
    if-ne p0, v8, :cond_3

    .line 57
    invoke-virtual {p1}, Lcom/google/obf/dw;->b()I

    move-result v0

    const/4 v2, 0x6

    if-ge v0, v2, :cond_1

    .line 86
    :cond_0
    :goto_1
    return-object v1

    .line 59
    :cond_1
    const/4 v0, 0x3

    const-string v2, "US-ASCII"

    invoke-static {v2}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v2

    invoke-virtual {p1, v0, v2}, Lcom/google/obf/dw;->a(ILjava/nio/charset/Charset;)Ljava/lang/String;

    move-result-object v2

    .line 60
    const-string v0, "\u0000\u0000\u0000"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 62
    invoke-virtual {p1}, Lcom/google/obf/dw;->j()I

    move-result v0

    .line 63
    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/obf/dw;->b()I

    move-result v5

    if-gt v0, v5, :cond_0

    .line 65
    const-string v5, "COM"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 81
    :cond_2
    invoke-virtual {p1}, Lcom/google/obf/dw;->f()I

    move-result v2

    .line 82
    if-ltz v2, :cond_0

    sget-object v5, Lcom/google/obf/ay;->b:[Ljava/nio/charset/Charset;

    array-length v5, v5

    if-ge v2, v5, :cond_0

    .line 84
    sget-object v5, Lcom/google/obf/ay;->b:[Ljava/nio/charset/Charset;

    aget-object v2, v5, v2

    .line 85
    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v0, v2}, Lcom/google/obf/dw;->a(ILjava/nio/charset/Charset;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "\u0000"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 86
    array-length v2, v0

    if-ne v2, v8, :cond_9

    aget-object v1, v0, v4

    aget-object v0, v0, v3

    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    :goto_2
    move-object v1, v0

    goto :goto_1

    .line 68
    :cond_3
    invoke-virtual {p1}, Lcom/google/obf/dw;->b()I

    move-result v0

    const/16 v2, 0xa

    if-lt v0, v2, :cond_0

    .line 70
    const-string v0, "US-ASCII"

    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    invoke-virtual {p1, v7, v0}, Lcom/google/obf/dw;->a(ILjava/nio/charset/Charset;)Ljava/lang/String;

    move-result-object v5

    .line 71
    const-string v0, "\u0000\u0000\u0000\u0000"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 73
    if-ne p0, v7, :cond_7

    invoke-virtual {p1}, Lcom/google/obf/dw;->r()I

    move-result v0

    .line 74
    :goto_3
    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/obf/dw;->b()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    if-gt v0, v2, :cond_0

    .line 76
    invoke-virtual {p1}, Lcom/google/obf/dw;->g()I

    move-result v2

    .line 77
    if-ne p0, v7, :cond_4

    and-int/lit8 v6, v2, 0xc

    if-nez v6, :cond_5

    :cond_4
    const/4 v6, 0x3

    if-ne p0, v6, :cond_8

    and-int/lit16 v2, v2, 0xc0

    if-eqz v2, :cond_8

    :cond_5
    move v2, v3

    .line 78
    :goto_4
    if-nez v2, :cond_6

    const-string v2, "COMM"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 80
    :cond_6
    invoke-virtual {p1, v0}, Lcom/google/obf/dw;->d(I)V

    goto/16 :goto_0

    .line 73
    :cond_7
    invoke-virtual {p1}, Lcom/google/obf/dw;->s()I

    move-result v0

    goto :goto_3

    :cond_8
    move v2, v4

    .line 77
    goto :goto_4

    :cond_9
    move-object v0, v1

    .line 86
    goto :goto_2
.end method

.method public static a(Lcom/google/obf/ak;)Lcom/google/obf/an;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/16 v8, 0xa

    const/4 v2, 0x0

    .line 1
    new-instance v3, Lcom/google/obf/dw;

    invoke-direct {v3, v8}, Lcom/google/obf/dw;-><init>(I)V

    .line 3
    const/4 v0, 0x0

    move v1, v2

    .line 4
    :goto_0
    iget-object v4, v3, Lcom/google/obf/dw;->a:[B

    invoke-interface {p0, v4, v2, v8}, Lcom/google/obf/ak;->c([BII)V

    .line 5
    invoke-virtual {v3, v2}, Lcom/google/obf/dw;->c(I)V

    .line 6
    invoke-virtual {v3}, Lcom/google/obf/dw;->j()I

    move-result v4

    sget v5, Lcom/google/obf/ay;->a:I

    if-eq v4, v5, :cond_0

    .line 20
    invoke-interface {p0}, Lcom/google/obf/ak;->a()V

    .line 21
    invoke-interface {p0, v1}, Lcom/google/obf/ak;->c(I)V

    .line 22
    return-object v0

    .line 8
    :cond_0
    invoke-virtual {v3}, Lcom/google/obf/dw;->f()I

    move-result v4

    .line 9
    invoke-virtual {v3}, Lcom/google/obf/dw;->f()I

    move-result v5

    .line 10
    invoke-virtual {v3}, Lcom/google/obf/dw;->f()I

    move-result v6

    .line 11
    invoke-virtual {v3}, Lcom/google/obf/dw;->r()I

    move-result v7

    .line 12
    if-nez v0, :cond_1

    invoke-static {v4, v5, v6, v7}, Lcom/google/obf/ay;->a(IIII)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 13
    new-array v0, v7, [B

    .line 14
    invoke-interface {p0, v0, v2, v7}, Lcom/google/obf/ak;->c([BII)V

    .line 15
    new-instance v5, Lcom/google/obf/dw;

    invoke-direct {v5, v0}, Lcom/google/obf/dw;-><init>([B)V

    invoke-static {v5, v4, v6}, Lcom/google/obf/ay;->a(Lcom/google/obf/dw;II)Lcom/google/obf/an;

    move-result-object v0

    .line 18
    :goto_1
    add-int/lit8 v4, v7, 0xa

    add-int/2addr v1, v4

    .line 19
    goto :goto_0

    .line 17
    :cond_1
    invoke-interface {p0, v7}, Lcom/google/obf/ak;->c(I)V

    goto :goto_1
.end method

.method private static a(Lcom/google/obf/dw;II)Lcom/google/obf/an;
    .locals 6

    .prologue
    const/4 v5, 0x6

    const/4 v4, 0x3

    const/4 v3, 0x4

    const/4 v1, 0x0

    .line 24
    invoke-static {p0, p1, p2}, Lcom/google/obf/ay;->b(Lcom/google/obf/dw;II)Z

    .line 25
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/obf/dw;->c(I)V

    .line 26
    if-ne p1, v4, :cond_4

    and-int/lit8 v0, p2, 0x40

    if-eqz v0, :cond_4

    .line 27
    invoke-virtual {p0}, Lcom/google/obf/dw;->b()I

    move-result v0

    if-ge v0, v3, :cond_0

    move-object v0, v1

    .line 55
    :goto_0
    return-object v0

    .line 29
    :cond_0
    invoke-virtual {p0}, Lcom/google/obf/dw;->s()I

    move-result v0

    .line 30
    invoke-virtual {p0}, Lcom/google/obf/dw;->b()I

    move-result v2

    if-le v0, v2, :cond_1

    move-object v0, v1

    .line 31
    goto :goto_0

    .line 33
    :cond_1
    if-lt v0, v5, :cond_2

    .line 34
    const/4 v2, 0x2

    invoke-virtual {p0, v2}, Lcom/google/obf/dw;->d(I)V

    .line 35
    invoke-virtual {p0}, Lcom/google/obf/dw;->s()I

    move-result v2

    .line 36
    invoke-virtual {p0, v3}, Lcom/google/obf/dw;->c(I)V

    .line 37
    invoke-virtual {p0}, Lcom/google/obf/dw;->c()I

    move-result v3

    sub-int v2, v3, v2

    invoke-virtual {p0, v2}, Lcom/google/obf/dw;->b(I)V

    .line 38
    invoke-virtual {p0}, Lcom/google/obf/dw;->b()I

    move-result v2

    if-ge v2, v0, :cond_2

    move-object v0, v1

    .line 39
    goto :goto_0

    .line 40
    :cond_2
    invoke-virtual {p0, v0}, Lcom/google/obf/dw;->d(I)V

    .line 48
    :cond_3
    :goto_1
    invoke-static {p1, p0}, Lcom/google/obf/ay;->a(ILcom/google/obf/dw;)Landroid/util/Pair;

    move-result-object v2

    if-eqz v2, :cond_8

    .line 49
    iget-object v0, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v4, :cond_3

    .line 50
    iget-object v0, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 51
    invoke-virtual {v0, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-static {v3, v0}, Lcom/google/obf/an;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/obf/an;

    move-result-object v0

    .line 52
    if-eqz v0, :cond_3

    goto :goto_0

    .line 41
    :cond_4
    if-ne p1, v3, :cond_3

    and-int/lit8 v0, p2, 0x40

    if-eqz v0, :cond_3

    .line 42
    invoke-virtual {p0}, Lcom/google/obf/dw;->b()I

    move-result v0

    if-ge v0, v3, :cond_5

    move-object v0, v1

    .line 43
    goto :goto_0

    .line 44
    :cond_5
    invoke-virtual {p0}, Lcom/google/obf/dw;->r()I

    move-result v0

    .line 45
    if-lt v0, v5, :cond_6

    invoke-virtual {p0}, Lcom/google/obf/dw;->b()I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    if-le v0, v2, :cond_7

    :cond_6
    move-object v0, v1

    .line 46
    goto :goto_0

    .line 47
    :cond_7
    invoke-virtual {p0, v0}, Lcom/google/obf/dw;->c(I)V

    goto :goto_1

    :cond_8
    move-object v0, v1

    .line 55
    goto :goto_0
.end method

.method private static a([BII)V
    .locals 2

    .prologue
    .line 156
    shr-int/lit8 v0, p2, 0x15

    and-int/lit8 v0, v0, 0x7f

    int-to-byte v0, v0

    aput-byte v0, p0, p1

    .line 157
    add-int/lit8 v0, p1, 0x1

    shr-int/lit8 v1, p2, 0xe

    and-int/lit8 v1, v1, 0x7f

    int-to-byte v1, v1

    aput-byte v1, p0, v0

    .line 158
    add-int/lit8 v0, p1, 0x2

    shr-int/lit8 v1, p2, 0x7

    and-int/lit8 v1, v1, 0x7f

    int-to-byte v1, v1

    aput-byte v1, p0, v0

    .line 159
    add-int/lit8 v0, p1, 0x3

    and-int/lit8 v1, p2, 0x7f

    int-to-byte v1, v1

    aput-byte v1, p0, v0

    .line 160
    return-void
.end method

.method private static a(IIII)Z
    .locals 3

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x2

    .line 23
    const/16 v0, 0xff

    if-eq p1, v0, :cond_3

    if-lt p0, v1, :cond_3

    if-gt p0, v2, :cond_3

    const/high16 v0, 0x300000

    if-gt p3, v0, :cond_3

    if-ne p0, v1, :cond_0

    and-int/lit8 v0, p2, 0x3f

    if-nez v0, :cond_3

    and-int/lit8 v0, p2, 0x40

    if-nez v0, :cond_3

    :cond_0
    const/4 v0, 0x3

    if-ne p0, v0, :cond_1

    and-int/lit8 v0, p2, 0x1f

    if-nez v0, :cond_3

    :cond_1
    if-ne p0, v2, :cond_2

    and-int/lit8 v0, p2, 0xf

    if-nez v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Lcom/google/obf/dw;Z)Z
    .locals 12

    .prologue
    const/4 v2, 0x1

    const-wide/16 v10, 0x7f

    const/4 v3, 0x0

    .line 104
    invoke-virtual {p0, v3}, Lcom/google/obf/dw;->c(I)V

    .line 105
    :goto_0
    invoke-virtual {p0}, Lcom/google/obf/dw;->b()I

    move-result v0

    const/16 v1, 0xa

    if-lt v0, v1, :cond_5

    .line 106
    invoke-virtual {p0}, Lcom/google/obf/dw;->m()I

    move-result v0

    if-nez v0, :cond_0

    move v0, v2

    .line 121
    :goto_1
    return v0

    .line 108
    :cond_0
    invoke-virtual {p0}, Lcom/google/obf/dw;->k()J

    move-result-wide v0

    .line 109
    if-nez p1, :cond_2

    .line 110
    const-wide/32 v4, 0x808080

    and-long/2addr v4, v0

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-eqz v4, :cond_1

    move v0, v3

    .line 111
    goto :goto_1

    .line 112
    :cond_1
    and-long v4, v0, v10

    const/16 v6, 0x8

    shr-long v6, v0, v6

    and-long/2addr v6, v10

    const/4 v8, 0x7

    shl-long/2addr v6, v8

    or-long/2addr v4, v6

    const/16 v6, 0x10

    shr-long v6, v0, v6

    and-long/2addr v6, v10

    const/16 v8, 0xe

    shl-long/2addr v6, v8

    or-long/2addr v4, v6

    const/16 v6, 0x18

    shr-long/2addr v0, v6

    and-long/2addr v0, v10

    const/16 v6, 0x15

    shl-long/2addr v0, v6

    or-long/2addr v0, v4

    .line 113
    :cond_2
    invoke-virtual {p0}, Lcom/google/obf/dw;->b()I

    move-result v4

    add-int/lit8 v4, v4, -0x2

    int-to-long v4, v4

    cmp-long v4, v0, v4

    if-lez v4, :cond_3

    move v0, v3

    .line 114
    goto :goto_1

    .line 115
    :cond_3
    invoke-virtual {p0}, Lcom/google/obf/dw;->g()I

    move-result v4

    .line 116
    and-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_4

    .line 117
    invoke-virtual {p0}, Lcom/google/obf/dw;->b()I

    move-result v4

    const/4 v5, 0x4

    if-ge v4, v5, :cond_4

    move v0, v3

    .line 118
    goto :goto_1

    .line 119
    :cond_4
    long-to-int v0, v0

    invoke-virtual {p0, v0}, Lcom/google/obf/dw;->d(I)V

    goto :goto_0

    :cond_5
    move v0, v2

    .line 121
    goto :goto_1
.end method

.method private static b(Lcom/google/obf/dw;Z)V
    .locals 10

    .prologue
    const/4 v4, 0x0

    .line 122
    invoke-virtual {p0, v4}, Lcom/google/obf/dw;->c(I)V

    .line 123
    iget-object v9, p0, Lcom/google/obf/dw;->a:[B

    .line 124
    :goto_0
    invoke-virtual {p0}, Lcom/google/obf/dw;->b()I

    move-result v0

    const/16 v1, 0xa

    if-lt v0, v1, :cond_0

    .line 125
    invoke-virtual {p0}, Lcom/google/obf/dw;->m()I

    move-result v0

    if-nez v0, :cond_1

    .line 155
    :cond_0
    return-void

    .line 127
    :cond_1
    if-eqz p1, :cond_3

    invoke-virtual {p0}, Lcom/google/obf/dw;->s()I

    move-result v0

    .line 128
    :goto_1
    invoke-virtual {p0}, Lcom/google/obf/dw;->g()I

    move-result v8

    .line 130
    and-int/lit8 v1, v8, 0x1

    if-eqz v1, :cond_8

    .line 131
    invoke-virtual {p0}, Lcom/google/obf/dw;->d()I

    move-result v1

    .line 132
    add-int/lit8 v2, v1, 0x4

    invoke-virtual {p0}, Lcom/google/obf/dw;->b()I

    move-result v3

    add-int/lit8 v3, v3, -0x4

    invoke-static {v9, v2, v9, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 133
    add-int/lit8 v1, v0, -0x4

    .line 134
    and-int/lit8 v2, v8, -0x2

    .line 135
    invoke-virtual {p0}, Lcom/google/obf/dw;->c()I

    move-result v0

    add-int/lit8 v0, v0, -0x4

    invoke-virtual {p0, v0}, Lcom/google/obf/dw;->b(I)V

    .line 136
    :goto_2
    and-int/lit8 v0, v2, 0x2

    if-eqz v0, :cond_7

    .line 137
    invoke-virtual {p0}, Lcom/google/obf/dw;->d()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    move v3, v4

    move v5, v0

    .line 139
    :goto_3
    add-int/lit8 v6, v3, 0x1

    if-ge v6, v1, :cond_4

    .line 140
    add-int/lit8 v6, v0, -0x1

    aget-byte v6, v9, v6

    and-int/lit16 v6, v6, 0xff

    const/16 v7, 0xff

    if-ne v6, v7, :cond_2

    aget-byte v6, v9, v0

    if-nez v6, :cond_2

    .line 141
    add-int/lit8 v0, v0, 0x1

    .line 142
    add-int/lit8 v1, v1, -0x1

    .line 143
    :cond_2
    add-int/lit8 v6, v5, 0x1

    add-int/lit8 v7, v0, 0x1

    aget-byte v0, v9, v0

    aput-byte v0, v9, v5

    .line 144
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v5, v6

    move v0, v7

    goto :goto_3

    .line 127
    :cond_3
    invoke-virtual {p0}, Lcom/google/obf/dw;->r()I

    move-result v0

    goto :goto_1

    .line 145
    :cond_4
    invoke-virtual {p0}, Lcom/google/obf/dw;->c()I

    move-result v3

    sub-int v6, v0, v5

    sub-int/2addr v3, v6

    invoke-virtual {p0, v3}, Lcom/google/obf/dw;->b(I)V

    .line 146
    invoke-virtual {p0}, Lcom/google/obf/dw;->b()I

    move-result v3

    sub-int/2addr v3, v0

    invoke-static {v9, v0, v9, v5, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 147
    and-int/lit8 v0, v2, -0x3

    .line 148
    :goto_4
    if-ne v0, v8, :cond_5

    if-eqz p1, :cond_6

    .line 149
    :cond_5
    invoke-virtual {p0}, Lcom/google/obf/dw;->d()I

    move-result v2

    add-int/lit8 v2, v2, -0x6

    .line 150
    invoke-static {v9, v2, v1}, Lcom/google/obf/ay;->a([BII)V

    .line 151
    add-int/lit8 v3, v2, 0x4

    shr-int/lit8 v5, v0, 0x8

    int-to-byte v5, v5

    aput-byte v5, v9, v3

    .line 152
    add-int/lit8 v2, v2, 0x5

    and-int/lit16 v0, v0, 0xff

    int-to-byte v0, v0

    aput-byte v0, v9, v2

    .line 153
    :cond_6
    invoke-virtual {p0, v1}, Lcom/google/obf/dw;->d(I)V

    goto/16 :goto_0

    :cond_7
    move v0, v2

    goto :goto_4

    :cond_8
    move v2, v8

    move v1, v0

    goto :goto_2
.end method

.method private static b(Lcom/google/obf/dw;II)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 87
    const/4 v2, 0x4

    if-eq p1, v2, :cond_4

    .line 88
    and-int/lit16 v2, p2, 0x80

    if-eqz v2, :cond_2

    .line 89
    iget-object v3, p0, Lcom/google/obf/dw;->a:[B

    .line 90
    array-length v2, v3

    move v7, v0

    move v0, v2

    move v2, v7

    .line 91
    :goto_0
    add-int/lit8 v4, v2, 0x1

    if-ge v4, v0, :cond_1

    .line 92
    aget-byte v4, v3, v2

    and-int/lit16 v4, v4, 0xff

    const/16 v5, 0xff

    if-ne v4, v5, :cond_0

    add-int/lit8 v4, v2, 0x1

    aget-byte v4, v3, v4

    if-nez v4, :cond_0

    .line 93
    add-int/lit8 v4, v2, 0x2

    add-int/lit8 v5, v2, 0x1

    sub-int v6, v0, v2

    add-int/lit8 v6, v6, -0x2

    invoke-static {v3, v4, v3, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 94
    add-int/lit8 v0, v0, -0x1

    .line 95
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 96
    :cond_1
    invoke-virtual {p0, v0}, Lcom/google/obf/dw;->b(I)V

    :cond_2
    :goto_1
    move v0, v1

    .line 103
    :cond_3
    return v0

    .line 98
    :cond_4
    invoke-static {p0, v0}, Lcom/google/obf/ay;->a(Lcom/google/obf/dw;Z)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 99
    invoke-static {p0, v0}, Lcom/google/obf/ay;->b(Lcom/google/obf/dw;Z)V

    goto :goto_1

    .line 100
    :cond_5
    invoke-static {p0, v1}, Lcom/google/obf/ay;->a(Lcom/google/obf/dw;Z)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 101
    invoke-static {p0, v1}, Lcom/google/obf/ay;->b(Lcom/google/obf/dw;Z)V

    goto :goto_1
.end method
