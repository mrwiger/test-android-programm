.class final Lcom/google/android/exoplayer2/source/dash/DashMediaSource$PeriodSeekInfo;
.super Ljava/lang/Object;
.source "DashMediaSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/exoplayer2/source/dash/DashMediaSource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "PeriodSeekInfo"
.end annotation


# instance fields
.field public final availableEndTimeUs:J

.field public final availableStartTimeUs:J

.field public final isIndexExplicit:Z


# direct methods
.method private constructor <init>(ZJJ)V
    .locals 0
    .param p1, "isIndexExplicit"    # Z
    .param p2, "availableStartTimeUs"    # J
    .param p4, "availableEndTimeUs"    # J

    .prologue
    .line 921
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 922
    iput-boolean p1, p0, Lcom/google/android/exoplayer2/source/dash/DashMediaSource$PeriodSeekInfo;->isIndexExplicit:Z

    .line 923
    iput-wide p2, p0, Lcom/google/android/exoplayer2/source/dash/DashMediaSource$PeriodSeekInfo;->availableStartTimeUs:J

    .line 924
    iput-wide p4, p0, Lcom/google/android/exoplayer2/source/dash/DashMediaSource$PeriodSeekInfo;->availableEndTimeUs:J

    .line 925
    return-void
.end method

.method public static createPeriodSeekInfo(Lcom/google/android/exoplayer2/source/dash/manifest/Period;J)Lcom/google/android/exoplayer2/source/dash/DashMediaSource$PeriodSeekInfo;
    .locals 31
    .param p0, "period"    # Lcom/google/android/exoplayer2/source/dash/manifest/Period;
    .param p1, "durationUs"    # J

    .prologue
    .line 885
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/exoplayer2/source/dash/manifest/Period;->adaptationSets:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v16

    .line 886
    .local v16, "adaptationSetCount":I
    const-wide/16 v8, 0x0

    .line 887
    .local v8, "availableStartTimeUs":J
    const-wide v10, 0x7fffffffffffffffL

    .line 888
    .local v10, "availableEndTimeUs":J
    const/4 v7, 0x0

    .line 889
    .local v7, "isIndexExplicit":Z
    const/16 v21, 0x0

    .line 890
    .local v21, "seenEmptyIndex":Z
    const/16 v17, 0x0

    .local v17, "i":I
    :goto_0
    move/from16 v0, v17

    move/from16 v1, v16

    if-ge v0, v1, :cond_3

    .line 891
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/exoplayer2/source/dash/manifest/Period;->adaptationSets:Ljava/util/List;

    move/from16 v0, v17

    invoke-interface {v6, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/exoplayer2/source/dash/manifest/AdaptationSet;

    iget-object v6, v6, Lcom/google/android/exoplayer2/source/dash/manifest/AdaptationSet;->representations:Ljava/util/List;

    const/16 v25, 0x0

    move/from16 v0, v25

    invoke-interface {v6, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/exoplayer2/source/dash/manifest/Representation;

    invoke-virtual {v6}, Lcom/google/android/exoplayer2/source/dash/manifest/Representation;->getIndex()Lcom/google/android/exoplayer2/source/dash/DashSegmentIndex;

    move-result-object v20

    .line 892
    .local v20, "index":Lcom/google/android/exoplayer2/source/dash/DashSegmentIndex;
    if-nez v20, :cond_0

    .line 893
    new-instance v6, Lcom/google/android/exoplayer2/source/dash/DashMediaSource$PeriodSeekInfo;

    const/4 v7, 0x1

    const-wide/16 v8, 0x0

    move-wide/from16 v10, p1

    invoke-direct/range {v6 .. v11}, Lcom/google/android/exoplayer2/source/dash/DashMediaSource$PeriodSeekInfo;-><init>(ZJJ)V

    .line 913
    .end local v7    # "isIndexExplicit":Z
    .end local v8    # "availableStartTimeUs":J
    .end local v10    # "availableEndTimeUs":J
    .end local v20    # "index":Lcom/google/android/exoplayer2/source/dash/DashSegmentIndex;
    :goto_1
    return-object v6

    .line 895
    .restart local v7    # "isIndexExplicit":Z
    .restart local v8    # "availableStartTimeUs":J
    .restart local v10    # "availableEndTimeUs":J
    .restart local v20    # "index":Lcom/google/android/exoplayer2/source/dash/DashSegmentIndex;
    :cond_0
    invoke-interface/range {v20 .. v20}, Lcom/google/android/exoplayer2/source/dash/DashSegmentIndex;->isExplicit()Z

    move-result v6

    or-int/2addr v7, v6

    .line 896
    move-object/from16 v0, v20

    move-wide/from16 v1, p1

    invoke-interface {v0, v1, v2}, Lcom/google/android/exoplayer2/source/dash/DashSegmentIndex;->getSegmentCount(J)I

    move-result v24

    .line 897
    .local v24, "segmentCount":I
    if-nez v24, :cond_2

    .line 898
    const/16 v21, 0x1

    .line 899
    const-wide/16 v8, 0x0

    .line 900
    const-wide/16 v10, 0x0

    .line 890
    :cond_1
    :goto_2
    add-int/lit8 v17, v17, 0x1

    goto :goto_0

    .line 901
    :cond_2
    if-nez v21, :cond_1

    .line 902
    invoke-interface/range {v20 .. v20}, Lcom/google/android/exoplayer2/source/dash/DashSegmentIndex;->getFirstSegmentNum()J

    move-result-wide v18

    .line 903
    .local v18, "firstSegmentNum":J
    move-object/from16 v0, v20

    move-wide/from16 v1, v18

    invoke-interface {v0, v1, v2}, Lcom/google/android/exoplayer2/source/dash/DashSegmentIndex;->getTimeUs(J)J

    move-result-wide v14

    .line 904
    .local v14, "adaptationSetAvailableStartTimeUs":J
    invoke-static {v8, v9, v14, v15}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v8

    .line 905
    const/4 v6, -0x1

    move/from16 v0, v24

    if-eq v0, v6, :cond_1

    .line 906
    move/from16 v0, v24

    int-to-long v0, v0

    move-wide/from16 v26, v0

    add-long v26, v26, v18

    const-wide/16 v28, 0x1

    sub-long v22, v26, v28

    .line 907
    .local v22, "lastSegmentNum":J
    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    invoke-interface {v0, v1, v2}, Lcom/google/android/exoplayer2/source/dash/DashSegmentIndex;->getTimeUs(J)J

    move-result-wide v26

    .line 908
    move-object/from16 v0, v20

    move-wide/from16 v1, v22

    move-wide/from16 v3, p1

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/exoplayer2/source/dash/DashSegmentIndex;->getDurationUs(JJ)J

    move-result-wide v28

    add-long v12, v26, v28

    .line 909
    .local v12, "adaptationSetAvailableEndTimeUs":J
    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v10

    goto :goto_2

    .line 913
    .end local v12    # "adaptationSetAvailableEndTimeUs":J
    .end local v14    # "adaptationSetAvailableStartTimeUs":J
    .end local v18    # "firstSegmentNum":J
    .end local v20    # "index":Lcom/google/android/exoplayer2/source/dash/DashSegmentIndex;
    .end local v22    # "lastSegmentNum":J
    .end local v24    # "segmentCount":I
    :cond_3
    new-instance v6, Lcom/google/android/exoplayer2/source/dash/DashMediaSource$PeriodSeekInfo;

    invoke-direct/range {v6 .. v11}, Lcom/google/android/exoplayer2/source/dash/DashMediaSource$PeriodSeekInfo;-><init>(ZJJ)V

    goto :goto_1
.end method
