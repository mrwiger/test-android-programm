.class Lcom/my/target/bl$a;
.super Ljava/lang/Object;
.source "EnvironmentParamsDataProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/my/target/bl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "a"
.end annotation


# instance fields
.field private hb:Ljava/lang/String;

.field private hc:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/my/target/bl$b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 292
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 293
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/my/target/bl$a;->hc:Ljava/util/ArrayList;

    .line 294
    const-string v0, "phone"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 295
    if-nez v0, :cond_1

    .line 321
    :cond_0
    :goto_0
    return-void

    .line 301
    :cond_1
    :try_start_0
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x11

    if-lt v1, v2, :cond_2

    const-string v1, "android.permission.ACCESS_COARSE_LOCATION"

    .line 302
    invoke-static {v1, p1}, Lcom/my/target/bl;->i(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 304
    invoke-direct {p0, v0}, Lcom/my/target/bl$a;->b(Landroid/telephony/TelephonyManager;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/my/target/bl$a;->hc:Ljava/util/ArrayList;

    .line 307
    :cond_2
    iget-object v1, p0, Lcom/my/target/bl$a;->hc:Ljava/util/ArrayList;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/my/target/bl$a;->hc:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 309
    :cond_3
    const-string v1, "android.permission.ACCESS_FINE_LOCATION"

    invoke-static {v1, p1}, Lcom/my/target/bl;->i(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "android.permission.ACCESS_COARSE_LOCATION"

    .line 310
    invoke-static {v1, p1}, Lcom/my/target/bl;->i(Ljava/lang/String;Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 313
    :cond_4
    invoke-direct {p0, v0}, Lcom/my/target/bl$a;->a(Landroid/telephony/TelephonyManager;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/bl$a;->hc:Ljava/util/ArrayList;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 317
    :catch_0
    move-exception v0

    .line 319
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Environment provider exception "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(Landroid/telephony/TelephonyManager;)Ljava/util/ArrayList;
    .locals 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "MissingPermission"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/telephony/TelephonyManager;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/my/target/bl$b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 326
    invoke-virtual {p1}, Landroid/telephony/TelephonyManager;->getCellLocation()Landroid/telephony/CellLocation;

    move-result-object v0

    .line 327
    const/4 v1, 0x0

    .line 328
    if-eqz v0, :cond_1

    instance-of v2, v0, Landroid/telephony/gsm/GsmCellLocation;

    if-eqz v2, :cond_1

    .line 330
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 331
    check-cast v0, Landroid/telephony/gsm/GsmCellLocation;

    .line 332
    new-instance v2, Lcom/my/target/bl$b;

    const-string v3, "gsm"

    invoke-direct {v2, v3}, Lcom/my/target/bl$b;-><init>(Ljava/lang/String;)V

    .line 333
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 334
    invoke-virtual {v0}, Landroid/telephony/gsm/GsmCellLocation;->getCid()I

    move-result v3

    iput v3, v2, Lcom/my/target/bl$b;->hh:I

    .line 335
    invoke-virtual {v0}, Landroid/telephony/gsm/GsmCellLocation;->getLac()I

    move-result v0

    iput v0, v2, Lcom/my/target/bl$b;->hi:I

    .line 337
    invoke-virtual {p1}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/bl$a;->hb:Ljava/lang/String;

    .line 339
    iget-object v0, p0, Lcom/my/target/bl$a;->hb:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 343
    :try_start_0
    iget-object v0, p0, Lcom/my/target/bl$a;->hb:Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x3

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, v2, Lcom/my/target/bl$b;->mcc:I

    .line 344
    iget-object v0, p0, Lcom/my/target/bl$a;->hb:Ljava/lang/String;

    const/4 v3, 0x3

    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, v2, Lcom/my/target/bl$b;->mnc:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 351
    :cond_0
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "current cell: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v3, v2, Lcom/my/target/bl$b;->hh:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ","

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v3, v2, Lcom/my/target/bl$b;->hi:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ","

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v3, v2, Lcom/my/target/bl$b;->mcc:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ","

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, v2, Lcom/my/target/bl$b;->mnc:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    move-object v0, v1

    .line 353
    :goto_1
    return-object v0

    .line 346
    :catch_0
    move-exception v0

    .line 348
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unable to substring network operator "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/my/target/bl$a;->hb:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method static synthetic a(Lcom/my/target/bl$a;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 286
    iget-object v0, p0, Lcom/my/target/bl$a;->hc:Ljava/util/ArrayList;

    return-object v0
.end method

.method private b(Landroid/telephony/TelephonyManager;)Ljava/util/ArrayList;
    .locals 9
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "MissingPermission"
        }
    .end annotation

    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/telephony/TelephonyManager;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/my/target/bl$b;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v8, 0x18

    const v7, 0x7fffffff

    .line 360
    const/4 v0, 0x0

    .line 361
    invoke-virtual {p1}, Landroid/telephony/TelephonyManager;->getAllCellInfo()Ljava/util/List;

    move-result-object v1

    .line 362
    if-eqz v1, :cond_9

    .line 364
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 365
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/CellInfo;

    .line 367
    invoke-virtual {v0}, Landroid/telephony/CellInfo;->isRegistered()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 370
    instance-of v1, v0, Landroid/telephony/CellInfoLte;

    if-eqz v1, :cond_2

    .line 372
    new-instance v2, Lcom/my/target/bl$b;

    const-string v1, "lte"

    invoke-direct {v2, v1}, Lcom/my/target/bl$b;-><init>(Ljava/lang/String;)V

    move-object v1, v0

    .line 373
    check-cast v1, Landroid/telephony/CellInfoLte;

    invoke-virtual {v1}, Landroid/telephony/CellInfoLte;->getCellIdentity()Landroid/telephony/CellIdentityLte;

    move-result-object v1

    .line 374
    check-cast v0, Landroid/telephony/CellInfoLte;

    invoke-virtual {v0}, Landroid/telephony/CellInfoLte;->getCellSignalStrength()Landroid/telephony/CellSignalStrengthLte;

    move-result-object v0

    .line 375
    invoke-virtual {v1}, Landroid/telephony/CellIdentityLte;->getCi()I

    move-result v5

    iput v5, v2, Lcom/my/target/bl$b;->hh:I

    .line 376
    iput v7, v2, Lcom/my/target/bl$b;->hi:I

    .line 377
    invoke-virtual {v1}, Landroid/telephony/CellIdentityLte;->getMcc()I

    move-result v5

    iput v5, v2, Lcom/my/target/bl$b;->mcc:I

    .line 378
    invoke-virtual {v1}, Landroid/telephony/CellIdentityLte;->getMnc()I

    move-result v5

    iput v5, v2, Lcom/my/target/bl$b;->mnc:I

    .line 379
    invoke-virtual {v0}, Landroid/telephony/CellSignalStrengthLte;->getLevel()I

    move-result v5

    iput v5, v2, Lcom/my/target/bl$b;->level:I

    .line 380
    invoke-virtual {v0}, Landroid/telephony/CellSignalStrengthLte;->getDbm()I

    move-result v5

    iput v5, v2, Lcom/my/target/bl$b;->hj:I

    .line 381
    invoke-virtual {v0}, Landroid/telephony/CellSignalStrengthLte;->getAsuLevel()I

    move-result v5

    iput v5, v2, Lcom/my/target/bl$b;->hk:I

    .line 382
    invoke-virtual {v0}, Landroid/telephony/CellSignalStrengthLte;->getTimingAdvance()I

    move-result v0

    iput v0, v2, Lcom/my/target/bl$b;->hl:I

    .line 383
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v8, :cond_1

    .line 385
    invoke-virtual {v1}, Landroid/telephony/CellIdentityLte;->getEarfcn()I

    move-result v0

    iput v0, v2, Lcom/my/target/bl$b;->hm:I

    .line 387
    :cond_1
    iput v7, v2, Lcom/my/target/bl$b;->hn:I

    .line 388
    iput v7, v2, Lcom/my/target/bl$b;->ho:I

    .line 389
    invoke-virtual {v1}, Landroid/telephony/CellIdentityLte;->getTac()I

    move-result v0

    iput v0, v2, Lcom/my/target/bl$b;->hp:I

    move-object v0, v2

    .line 470
    :goto_1
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 391
    :cond_2
    instance-of v1, v0, Landroid/telephony/CellInfoGsm;

    if-eqz v1, :cond_5

    .line 393
    new-instance v2, Lcom/my/target/bl$b;

    const-string v1, "gsm"

    invoke-direct {v2, v1}, Lcom/my/target/bl$b;-><init>(Ljava/lang/String;)V

    move-object v1, v0

    .line 394
    check-cast v1, Landroid/telephony/CellInfoGsm;

    invoke-virtual {v1}, Landroid/telephony/CellInfoGsm;->getCellIdentity()Landroid/telephony/CellIdentityGsm;

    move-result-object v1

    .line 395
    check-cast v0, Landroid/telephony/CellInfoGsm;

    invoke-virtual {v0}, Landroid/telephony/CellInfoGsm;->getCellSignalStrength()Landroid/telephony/CellSignalStrengthGsm;

    move-result-object v0

    .line 396
    invoke-virtual {v1}, Landroid/telephony/CellIdentityGsm;->getCid()I

    move-result v5

    iput v5, v2, Lcom/my/target/bl$b;->hh:I

    .line 397
    invoke-virtual {v1}, Landroid/telephony/CellIdentityGsm;->getLac()I

    move-result v5

    iput v5, v2, Lcom/my/target/bl$b;->hi:I

    .line 398
    invoke-virtual {v1}, Landroid/telephony/CellIdentityGsm;->getMcc()I

    move-result v5

    iput v5, v2, Lcom/my/target/bl$b;->mcc:I

    .line 399
    invoke-virtual {v1}, Landroid/telephony/CellIdentityGsm;->getMnc()I

    move-result v5

    iput v5, v2, Lcom/my/target/bl$b;->mnc:I

    .line 400
    invoke-virtual {v0}, Landroid/telephony/CellSignalStrengthGsm;->getLevel()I

    move-result v5

    iput v5, v2, Lcom/my/target/bl$b;->level:I

    .line 401
    invoke-virtual {v0}, Landroid/telephony/CellSignalStrengthGsm;->getDbm()I

    move-result v5

    iput v5, v2, Lcom/my/target/bl$b;->hj:I

    .line 402
    invoke-virtual {v0}, Landroid/telephony/CellSignalStrengthGsm;->getAsuLevel()I

    move-result v5

    iput v5, v2, Lcom/my/target/bl$b;->hk:I

    .line 403
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x1a

    if-lt v5, v6, :cond_4

    .line 405
    invoke-virtual {v0}, Landroid/telephony/CellSignalStrengthGsm;->getTimingAdvance()I

    move-result v0

    iput v0, v2, Lcom/my/target/bl$b;->hl:I

    .line 411
    :goto_2
    iput v7, v2, Lcom/my/target/bl$b;->hm:I

    .line 412
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v8, :cond_3

    .line 414
    invoke-virtual {v1}, Landroid/telephony/CellIdentityGsm;->getBsic()I

    move-result v0

    iput v0, v2, Lcom/my/target/bl$b;->hn:I

    .line 417
    :cond_3
    invoke-virtual {v1}, Landroid/telephony/CellIdentityGsm;->getPsc()I

    move-result v0

    iput v0, v2, Lcom/my/target/bl$b;->ho:I

    .line 418
    iput v7, v2, Lcom/my/target/bl$b;->hp:I

    move-object v0, v2

    .line 419
    goto :goto_1

    .line 409
    :cond_4
    iput v7, v2, Lcom/my/target/bl$b;->hl:I

    goto :goto_2

    .line 420
    :cond_5
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x12

    if-lt v1, v2, :cond_7

    instance-of v1, v0, Landroid/telephony/CellInfoWcdma;

    if-eqz v1, :cond_7

    .line 422
    new-instance v2, Lcom/my/target/bl$b;

    const-string v1, "wcdma"

    invoke-direct {v2, v1}, Lcom/my/target/bl$b;-><init>(Ljava/lang/String;)V

    move-object v1, v0

    .line 423
    check-cast v1, Landroid/telephony/CellInfoWcdma;

    invoke-virtual {v1}, Landroid/telephony/CellInfoWcdma;->getCellIdentity()Landroid/telephony/CellIdentityWcdma;

    move-result-object v1

    .line 424
    check-cast v0, Landroid/telephony/CellInfoWcdma;

    invoke-virtual {v0}, Landroid/telephony/CellInfoWcdma;->getCellSignalStrength()Landroid/telephony/CellSignalStrengthWcdma;

    move-result-object v0

    .line 425
    invoke-virtual {v1}, Landroid/telephony/CellIdentityWcdma;->getCid()I

    move-result v5

    iput v5, v2, Lcom/my/target/bl$b;->hh:I

    .line 426
    invoke-virtual {v1}, Landroid/telephony/CellIdentityWcdma;->getLac()I

    move-result v5

    iput v5, v2, Lcom/my/target/bl$b;->hi:I

    .line 427
    invoke-virtual {v1}, Landroid/telephony/CellIdentityWcdma;->getMcc()I

    move-result v5

    iput v5, v2, Lcom/my/target/bl$b;->mcc:I

    .line 428
    invoke-virtual {v1}, Landroid/telephony/CellIdentityWcdma;->getMnc()I

    move-result v5

    iput v5, v2, Lcom/my/target/bl$b;->mnc:I

    .line 429
    invoke-virtual {v0}, Landroid/telephony/CellSignalStrengthWcdma;->getLevel()I

    move-result v5

    iput v5, v2, Lcom/my/target/bl$b;->level:I

    .line 430
    invoke-virtual {v0}, Landroid/telephony/CellSignalStrengthWcdma;->getDbm()I

    move-result v5

    iput v5, v2, Lcom/my/target/bl$b;->hj:I

    .line 431
    invoke-virtual {v0}, Landroid/telephony/CellSignalStrengthWcdma;->getAsuLevel()I

    move-result v0

    iput v0, v2, Lcom/my/target/bl$b;->hk:I

    .line 432
    iput v7, v2, Lcom/my/target/bl$b;->hl:I

    .line 433
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v8, :cond_6

    .line 435
    invoke-virtual {v1}, Landroid/telephony/CellIdentityWcdma;->getUarfcn()I

    move-result v0

    iput v0, v2, Lcom/my/target/bl$b;->hm:I

    .line 437
    :cond_6
    iput v7, v2, Lcom/my/target/bl$b;->hn:I

    .line 438
    invoke-virtual {v1}, Landroid/telephony/CellIdentityWcdma;->getPsc()I

    move-result v0

    iput v0, v2, Lcom/my/target/bl$b;->ho:I

    .line 439
    iput v7, v2, Lcom/my/target/bl$b;->hp:I

    move-object v0, v2

    .line 440
    goto/16 :goto_1

    .line 441
    :cond_7
    instance-of v1, v0, Landroid/telephony/CellInfoCdma;

    if-eqz v1, :cond_0

    .line 443
    new-instance v2, Lcom/my/target/bl$b;

    const-string v1, "cdma"

    invoke-direct {v2, v1}, Lcom/my/target/bl$b;-><init>(Ljava/lang/String;)V

    move-object v1, v0

    .line 444
    check-cast v1, Landroid/telephony/CellInfoCdma;

    invoke-virtual {v1}, Landroid/telephony/CellInfoCdma;->getCellIdentity()Landroid/telephony/CellIdentityCdma;

    move-result-object v1

    .line 445
    check-cast v0, Landroid/telephony/CellInfoCdma;

    invoke-virtual {v0}, Landroid/telephony/CellInfoCdma;->getCellSignalStrength()Landroid/telephony/CellSignalStrengthCdma;

    move-result-object v0

    .line 446
    invoke-virtual {v1}, Landroid/telephony/CellIdentityCdma;->getNetworkId()I

    move-result v5

    iput v5, v2, Lcom/my/target/bl$b;->hq:I

    .line 447
    invoke-virtual {v1}, Landroid/telephony/CellIdentityCdma;->getSystemId()I

    move-result v5

    iput v5, v2, Lcom/my/target/bl$b;->hr:I

    .line 448
    invoke-virtual {v1}, Landroid/telephony/CellIdentityCdma;->getBasestationId()I

    move-result v5

    iput v5, v2, Lcom/my/target/bl$b;->hs:I

    .line 449
    invoke-virtual {v1}, Landroid/telephony/CellIdentityCdma;->getLatitude()I

    move-result v5

    iput v5, v2, Lcom/my/target/bl$b;->ht:I

    .line 450
    invoke-virtual {v1}, Landroid/telephony/CellIdentityCdma;->getLongitude()I

    move-result v1

    iput v1, v2, Lcom/my/target/bl$b;->hu:I

    .line 452
    invoke-virtual {v0}, Landroid/telephony/CellSignalStrengthCdma;->getCdmaLevel()I

    move-result v1

    iput v1, v2, Lcom/my/target/bl$b;->hv:I

    .line 453
    invoke-virtual {v0}, Landroid/telephony/CellSignalStrengthCdma;->getLevel()I

    move-result v1

    iput v1, v2, Lcom/my/target/bl$b;->level:I

    .line 454
    invoke-virtual {v0}, Landroid/telephony/CellSignalStrengthCdma;->getEvdoLevel()I

    move-result v1

    iput v1, v2, Lcom/my/target/bl$b;->hw:I

    .line 455
    invoke-virtual {v0}, Landroid/telephony/CellSignalStrengthCdma;->getAsuLevel()I

    move-result v1

    iput v1, v2, Lcom/my/target/bl$b;->hk:I

    .line 457
    invoke-virtual {v0}, Landroid/telephony/CellSignalStrengthCdma;->getCdmaDbm()I

    move-result v1

    iput v1, v2, Lcom/my/target/bl$b;->hx:I

    .line 458
    invoke-virtual {v0}, Landroid/telephony/CellSignalStrengthCdma;->getDbm()I

    move-result v1

    iput v1, v2, Lcom/my/target/bl$b;->hj:I

    .line 459
    invoke-virtual {v0}, Landroid/telephony/CellSignalStrengthCdma;->getEvdoDbm()I

    move-result v1

    iput v1, v2, Lcom/my/target/bl$b;->hy:I

    .line 461
    invoke-virtual {v0}, Landroid/telephony/CellSignalStrengthCdma;->getEvdoEcio()I

    move-result v1

    iput v1, v2, Lcom/my/target/bl$b;->hz:I

    .line 462
    invoke-virtual {v0}, Landroid/telephony/CellSignalStrengthCdma;->getCdmaEcio()I

    move-result v1

    iput v1, v2, Lcom/my/target/bl$b;->hA:I

    .line 464
    invoke-virtual {v0}, Landroid/telephony/CellSignalStrengthCdma;->getEvdoSnr()I

    move-result v0

    iput v0, v2, Lcom/my/target/bl$b;->hB:I

    move-object v0, v2

    goto/16 :goto_1

    :cond_8
    move-object v0, v3

    .line 474
    :cond_9
    return-object v0
.end method
