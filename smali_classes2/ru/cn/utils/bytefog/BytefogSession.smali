.class public Lru/cn/utils/bytefog/BytefogSession;
.super Ljava/lang/Object;
.source "BytefogSession.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/utils/bytefog/BytefogSession$BytefogWrapper;,
        Lru/cn/utils/bytefog/BytefogSession$OnErrorListener;,
        Lru/cn/utils/bytefog/BytefogSession$OnReadyListener;
    }
.end annotation


# static fields
.field private static initialized:Z


# instance fields
.field private bytefogWrapper:Lru/cn/utils/bytefog/BytefogSession$BytefogWrapper;

.field private final noRestartListener:Lru/inetra/bytefog/service/InstanceListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "magnetUri"    # Landroid/net/Uri;

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 130
    new-instance v1, Lru/cn/utils/bytefog/BytefogSession$1;

    invoke-direct {v1, p0}, Lru/cn/utils/bytefog/BytefogSession$1;-><init>(Lru/cn/utils/bytefog/BytefogSession;)V

    iput-object v1, p0, Lru/cn/utils/bytefog/BytefogSession;->noRestartListener:Lru/inetra/bytefog/service/InstanceListener;

    .line 28
    sget-boolean v1, Lru/cn/utils/bytefog/BytefogSession;->initialized:Z

    if-nez v1, :cond_0

    .line 29
    new-instance v0, Lru/inetra/bytefog/service/BytefogConfig;

    invoke-direct {v0}, Lru/inetra/bytefog/service/BytefogConfig;-><init>()V

    .line 30
    .local v0, "config":Lru/inetra/bytefog/service/BytefogConfig;
    invoke-static {p1}, Lru/cn/utils/Utils;->getUUID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lru/inetra/bytefog/service/BytefogConfig;->setUid(Ljava/lang/String;)V

    .line 32
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lru/inetra/bytefog/service/Bytefog;->initialize(Landroid/content/Context;Lru/inetra/bytefog/service/BytefogConfig;)V

    .line 33
    const/4 v1, 0x1

    sput-boolean v1, Lru/cn/utils/bytefog/BytefogSession;->initialized:Z

    .line 36
    .end local v0    # "config":Lru/inetra/bytefog/service/BytefogConfig;
    :cond_0
    new-instance v1, Lru/cn/utils/bytefog/BytefogSession$BytefogWrapper;

    invoke-direct {v1, p2}, Lru/cn/utils/bytefog/BytefogSession$BytefogWrapper;-><init>(Landroid/net/Uri;)V

    iput-object v1, p0, Lru/cn/utils/bytefog/BytefogSession;->bytefogWrapper:Lru/cn/utils/bytefog/BytefogSession$BytefogWrapper;

    .line 37
    invoke-static {}, Lru/inetra/bytefog/service/Bytefog;->instance()Lru/inetra/bytefog/service/Instance;

    move-result-object v1

    iget-object v2, p0, Lru/cn/utils/bytefog/BytefogSession;->bytefogWrapper:Lru/cn/utils/bytefog/BytefogSession$BytefogWrapper;

    invoke-interface {v1, v2}, Lru/inetra/bytefog/service/Instance;->setListener(Lru/inetra/bytefog/service/InstanceListener;)V

    .line 38
    return-void
.end method


# virtual methods
.method public setOnErrorListener(Lru/cn/utils/bytefog/BytefogSession$OnErrorListener;)V
    .locals 1
    .param p1, "errorListener"    # Lru/cn/utils/bytefog/BytefogSession$OnErrorListener;

    .prologue
    .line 41
    iget-object v0, p0, Lru/cn/utils/bytefog/BytefogSession;->bytefogWrapper:Lru/cn/utils/bytefog/BytefogSession$BytefogWrapper;

    invoke-virtual {v0, p1}, Lru/cn/utils/bytefog/BytefogSession$BytefogWrapper;->setOnErrorListener(Lru/cn/utils/bytefog/BytefogSession$OnErrorListener;)V

    .line 42
    return-void
.end method

.method public start(Lru/cn/utils/bytefog/BytefogSession$OnReadyListener;)V
    .locals 1
    .param p1, "readyListener"    # Lru/cn/utils/bytefog/BytefogSession$OnReadyListener;

    .prologue
    .line 45
    iget-object v0, p0, Lru/cn/utils/bytefog/BytefogSession;->bytefogWrapper:Lru/cn/utils/bytefog/BytefogSession$BytefogWrapper;

    invoke-virtual {v0, p1}, Lru/cn/utils/bytefog/BytefogSession$BytefogWrapper;->waitReady(Lru/cn/utils/bytefog/BytefogSession$OnReadyListener;)V

    .line 46
    return-void
.end method

.method public stop()V
    .locals 2

    .prologue
    .line 49
    invoke-static {}, Lru/inetra/bytefog/service/Bytefog;->instance()Lru/inetra/bytefog/service/Instance;

    move-result-object v0

    .line 50
    .local v0, "bytefog":Lru/inetra/bytefog/service/Instance;
    iget-object v1, p0, Lru/cn/utils/bytefog/BytefogSession;->noRestartListener:Lru/inetra/bytefog/service/InstanceListener;

    invoke-interface {v0, v1}, Lru/inetra/bytefog/service/Instance;->setListener(Lru/inetra/bytefog/service/InstanceListener;)V

    .line 51
    invoke-interface {v0}, Lru/inetra/bytefog/service/Instance;->cancelAllRequests()V

    .line 52
    return-void
.end method
