.class Lru/cn/player/chromecast/ChromecastPlayer$1;
.super Ljava/lang/Object;
.source "ChromecastPlayer.java"

# interfaces
.implements Lcom/google/android/gms/common/api/ResultCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/player/chromecast/ChromecastPlayer;->prepareToPlay()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/gms/common/api/ResultCallback",
        "<",
        "Lcom/google/android/gms/cast/framework/media/RemoteMediaClient$MediaChannelResult;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/player/chromecast/ChromecastPlayer;


# direct methods
.method constructor <init>(Lru/cn/player/chromecast/ChromecastPlayer;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/player/chromecast/ChromecastPlayer;

    .prologue
    .line 72
    iput-object p1, p0, Lru/cn/player/chromecast/ChromecastPlayer$1;->this$0:Lru/cn/player/chromecast/ChromecastPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResult(Lcom/google/android/gms/cast/framework/media/RemoteMediaClient$MediaChannelResult;)V
    .locals 4
    .param p1, "result"    # Lcom/google/android/gms/cast/framework/media/RemoteMediaClient$MediaChannelResult;

    .prologue
    .line 75
    invoke-interface {p1}, Lcom/google/android/gms/cast/framework/media/RemoteMediaClient$MediaChannelResult;->getStatus()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    .line 76
    .local v0, "status":Lcom/google/android/gms/common/api/Status;
    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->isSuccess()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 77
    iget-object v1, p0, Lru/cn/player/chromecast/ChromecastPlayer$1;->this$0:Lru/cn/player/chromecast/ChromecastPlayer;

    invoke-static {v1}, Lru/cn/player/chromecast/ChromecastPlayer;->access$000(Lru/cn/player/chromecast/ChromecastPlayer;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "Media loaded successfully"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    iget-object v1, p0, Lru/cn/player/chromecast/ChromecastPlayer$1;->this$0:Lru/cn/player/chromecast/ChromecastPlayer;

    sget-object v2, Lru/cn/player/AbstractMediaPlayer$PlayerState;->LOADED:Lru/cn/player/AbstractMediaPlayer$PlayerState;

    invoke-static {v1, v2}, Lru/cn/player/chromecast/ChromecastPlayer;->access$100(Lru/cn/player/chromecast/ChromecastPlayer;Lru/cn/player/AbstractMediaPlayer$PlayerState;)V

    .line 88
    :goto_0
    return-void

    .line 80
    :cond_0
    iget-object v1, p0, Lru/cn/player/chromecast/ChromecastPlayer$1;->this$0:Lru/cn/player/chromecast/ChromecastPlayer;

    invoke-static {v1}, Lru/cn/player/chromecast/ChromecastPlayer;->access$200(Lru/cn/player/chromecast/ChromecastPlayer;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to load media: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 81
    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->getStatusMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 80
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    iget-object v1, p0, Lru/cn/player/chromecast/ChromecastPlayer$1;->this$0:Lru/cn/player/chromecast/ChromecastPlayer;

    invoke-static {v1}, Lru/cn/player/chromecast/ChromecastPlayer;->access$300(Lru/cn/player/chromecast/ChromecastPlayer;)Lru/cn/player/AbstractMediaPlayer$MediaPlayerListener;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 83
    iget-object v1, p0, Lru/cn/player/chromecast/ChromecastPlayer$1;->this$0:Lru/cn/player/chromecast/ChromecastPlayer;

    invoke-static {v1}, Lru/cn/player/chromecast/ChromecastPlayer;->access$400(Lru/cn/player/chromecast/ChromecastPlayer;)Lru/cn/player/AbstractMediaPlayer$MediaPlayerListener;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->getStatusCode()I

    move-result v2

    invoke-interface {v1, v2}, Lru/cn/player/AbstractMediaPlayer$MediaPlayerListener;->onError(I)V

    .line 86
    :cond_1
    iget-object v1, p0, Lru/cn/player/chromecast/ChromecastPlayer$1;->this$0:Lru/cn/player/chromecast/ChromecastPlayer;

    invoke-virtual {v1}, Lru/cn/player/chromecast/ChromecastPlayer;->stop()V

    goto :goto_0
.end method

.method public bridge synthetic onResult(Lcom/google/android/gms/common/api/Result;)V
    .locals 0

    .prologue
    .line 72
    check-cast p1, Lcom/google/android/gms/cast/framework/media/RemoteMediaClient$MediaChannelResult;

    invoke-virtual {p0, p1}, Lru/cn/player/chromecast/ChromecastPlayer$1;->onResult(Lcom/google/android/gms/cast/framework/media/RemoteMediaClient$MediaChannelResult;)V

    return-void
.end method
