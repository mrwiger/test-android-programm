.class public Lcom/yandex/metrica/impl/as;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Z

.field private final b:Lcom/yandex/metrica/impl/az;

.field private final c:Lcom/yandex/metrica/impl/ob/dc;

.field private d:Ljava/lang/String;

.field private e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcom/yandex/metrica/DeferredDeeplinkParametersListener;

.field private g:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Lcom/yandex/metrica/impl/az;Lcom/yandex/metrica/impl/ob/dc;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/yandex/metrica/impl/as;->b:Lcom/yandex/metrica/impl/az;

    .line 30
    iput-object p2, p0, Lcom/yandex/metrica/impl/as;->c:Lcom/yandex/metrica/impl/ob/dc;

    .line 31
    invoke-virtual {p2}, Lcom/yandex/metrica/impl/ob/dc;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/impl/as;->d:Ljava/lang/String;

    .line 32
    invoke-virtual {p2}, Lcom/yandex/metrica/impl/ob/dc;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/yandex/metrica/impl/as;->a:Z

    .line 34
    iget-boolean v0, p0, Lcom/yandex/metrica/impl/as;->a:Z

    if-eqz v0, :cond_0

    .line 35
    iget-object v0, p0, Lcom/yandex/metrica/impl/as;->c:Lcom/yandex/metrica/impl/ob/dc;

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/dc;->p(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/dc;

    .line 36
    iput-object v1, p0, Lcom/yandex/metrica/impl/as;->d:Ljava/lang/String;

    .line 41
    :goto_0
    return-void

    .line 38
    :cond_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/as;->d:Ljava/lang/String;

    invoke-static {v0}, Lcom/yandex/metrica/impl/as;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 39
    invoke-direct {p0, v0}, Lcom/yandex/metrica/impl/as;->c(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 119
    iget-object v0, p0, Lcom/yandex/metrica/impl/as;->e:Ljava/util/Map;

    invoke-static {v0}, Lcom/yandex/metrica/impl/bl;->a(Ljava/util/Map;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 120
    iget-object v0, p0, Lcom/yandex/metrica/impl/as;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 121
    sget-object v0, Lcom/yandex/metrica/DeferredDeeplinkParametersListener$Error;->PARSE_ERROR:Lcom/yandex/metrica/DeferredDeeplinkParametersListener$Error;

    invoke-direct {p0, v0}, Lcom/yandex/metrica/impl/as;->a(Lcom/yandex/metrica/DeferredDeeplinkParametersListener$Error;)V

    .line 129
    :cond_0
    :goto_0
    return-void

    .line 124
    :cond_1
    iget-object v0, p0, Lcom/yandex/metrica/impl/as;->f:Lcom/yandex/metrica/DeferredDeeplinkParametersListener;

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/yandex/metrica/impl/as;->f:Lcom/yandex/metrica/DeferredDeeplinkParametersListener;

    iget-object v1, p0, Lcom/yandex/metrica/impl/as;->e:Ljava/util/Map;

    invoke-interface {v0, v1}, Lcom/yandex/metrica/DeferredDeeplinkParametersListener;->onParametersLoaded(Ljava/util/Map;)V

    .line 126
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/yandex/metrica/impl/as;->f:Lcom/yandex/metrica/DeferredDeeplinkParametersListener;

    goto :goto_0
.end method

.method private a(Lcom/yandex/metrica/DeferredDeeplinkParametersListener$Error;)V
    .locals 2

    .prologue
    .line 132
    iget-object v0, p0, Lcom/yandex/metrica/impl/as;->f:Lcom/yandex/metrica/DeferredDeeplinkParametersListener;

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/yandex/metrica/impl/as;->f:Lcom/yandex/metrica/DeferredDeeplinkParametersListener;

    iget-object v1, p0, Lcom/yandex/metrica/impl/as;->d:Ljava/lang/String;

    invoke-interface {v0, p1, v1}, Lcom/yandex/metrica/DeferredDeeplinkParametersListener;->onError(Lcom/yandex/metrica/DeferredDeeplinkParametersListener$Error;Ljava/lang/String;)V

    .line 134
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/yandex/metrica/impl/as;->f:Lcom/yandex/metrica/DeferredDeeplinkParametersListener;

    .line 136
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/yandex/metrica/impl/as;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/yandex/metrica/impl/as;->a()V

    return-void
.end method

.method static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 71
    invoke-static {p0}, Lcom/yandex/metrica/impl/as;->d(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    .line 72
    const-string v1, "appmetrica_deep_link"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method private c(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 64
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1077
    invoke-static {p1}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/yandex/metrica/impl/as;->d(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    .line 1078
    new-instance v2, Ljava/util/HashMap;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v1

    invoke-direct {v2, v1}, Ljava/util/HashMap;-><init>(I)V

    .line 1079
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1080
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 65
    :cond_0
    iput-object v2, p0, Lcom/yandex/metrica/impl/as;->e:Ljava/util/Map;

    .line 67
    :cond_1
    return-void
.end method

.method private static d(Ljava/lang/String;)Ljava/util/Map;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 87
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 88
    if-eqz p0, :cond_2

    .line 1106
    const/16 v0, 0x3f

    invoke-virtual {p0, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 1107
    if-ltz v0, :cond_0

    .line 1108
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    .line 1115
    :cond_0
    const-string v0, "="

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    .line 90
    if-eqz v0, :cond_2

    .line 91
    const-string v0, "&"

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 92
    array-length v4, v3

    move v0, v1

    :goto_0
    if-ge v0, v4, :cond_2

    aget-object v5, v3, v0

    .line 93
    const-string v6, "="

    invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    .line 94
    if-ltz v6, :cond_1

    .line 95
    invoke-virtual {v5, v1, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    add-int/lit8 v6, v6, 0x1

    invoke-virtual {v5, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v7, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 97
    :cond_1
    const-string v6, ""

    invoke-virtual {v2, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 102
    :cond_2
    return-object v2
.end method


# virtual methods
.method public declared-synchronized a(Lcom/yandex/metrica/DeferredDeeplinkParametersListener;)V
    .locals 2

    .prologue
    .line 140
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/yandex/metrica/impl/as;->f:Lcom/yandex/metrica/DeferredDeeplinkParametersListener;

    .line 141
    iget-boolean v0, p0, Lcom/yandex/metrica/impl/as;->a:Z

    if-eqz v0, :cond_0

    .line 142
    sget-object v0, Lcom/yandex/metrica/DeferredDeeplinkParametersListener$Error;->NOT_A_FIRST_LAUNCH:Lcom/yandex/metrica/DeferredDeeplinkParametersListener$Error;

    invoke-direct {p0, v0}, Lcom/yandex/metrica/impl/as;->a(Lcom/yandex/metrica/DeferredDeeplinkParametersListener$Error;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 147
    :goto_0
    :try_start_1
    iget-object v0, p0, Lcom/yandex/metrica/impl/as;->c:Lcom/yandex/metrica/impl/ob/dc;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/dc;->e()Lcom/yandex/metrica/impl/ob/dc;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 148
    monitor-exit p0

    return-void

    .line 144
    :cond_0
    :try_start_2
    invoke-direct {p0}, Lcom/yandex/metrica/impl/as;->a()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 147
    :catchall_0
    move-exception v0

    :try_start_3
    iget-object v1, p0, Lcom/yandex/metrica/impl/as;->c:Lcom/yandex/metrica/impl/ob/dc;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/dc;->e()Lcom/yandex/metrica/impl/ob/dc;

    .line 148
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 140
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 44
    iget-object v0, p0, Lcom/yandex/metrica/impl/as;->b:Lcom/yandex/metrica/impl/az;

    invoke-virtual {v0, p1}, Lcom/yandex/metrica/impl/az;->b(Ljava/lang/String;)V

    .line 45
    iget-boolean v0, p0, Lcom/yandex/metrica/impl/as;->a:Z

    if-nez v0, :cond_1

    .line 46
    monitor-enter p0

    .line 47
    :try_start_0
    iput-object p1, p0, Lcom/yandex/metrica/impl/as;->d:Ljava/lang/String;

    .line 48
    iget-object v0, p0, Lcom/yandex/metrica/impl/as;->c:Lcom/yandex/metrica/impl/ob/dc;

    iget-object v1, p0, Lcom/yandex/metrica/impl/as;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/dc;->p(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/dc;

    .line 49
    invoke-static {p1}, Lcom/yandex/metrica/impl/as;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 50
    invoke-direct {p0, v0}, Lcom/yandex/metrica/impl/as;->c(Ljava/lang/String;)V

    .line 51
    iget-object v0, p0, Lcom/yandex/metrica/impl/as;->g:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 52
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/as;->g:Landroid/os/Handler;

    .line 54
    :cond_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/as;->g:Landroid/os/Handler;

    new-instance v1, Lcom/yandex/metrica/impl/as$1;

    invoke-direct {v1, p0}, Lcom/yandex/metrica/impl/as$1;-><init>(Lcom/yandex/metrica/impl/as;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 59
    monitor-exit p0

    .line 61
    :cond_1
    return-void

    .line 59
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
