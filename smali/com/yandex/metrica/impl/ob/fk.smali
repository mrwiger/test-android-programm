.class public Lcom/yandex/metrica/impl/ob/fk;
.super Lcom/yandex/metrica/impl/ob/ff;
.source "SourceFile"


# static fields
.field static final c:Lcom/yandex/metrica/impl/ob/fm;

.field static final d:Lcom/yandex/metrica/impl/ob/fm;

.field static final e:Lcom/yandex/metrica/impl/ob/fm;

.field static final f:Lcom/yandex/metrica/impl/ob/fm;

.field private static final g:Lcom/yandex/metrica/impl/ob/fm;

.field private static final h:Lcom/yandex/metrica/impl/ob/fm;

.field private static final i:Lcom/yandex/metrica/impl/ob/fm;

.field private static final j:Lcom/yandex/metrica/impl/ob/fm;

.field private static final k:Lcom/yandex/metrica/impl/ob/fm;

.field private static final l:Lcom/yandex/metrica/impl/ob/fm;

.field private static final m:Lcom/yandex/metrica/impl/ob/fm;

.field private static final n:Lcom/yandex/metrica/impl/ob/fm;


# instance fields
.field private o:Lcom/yandex/metrica/impl/ob/fm;

.field private p:Lcom/yandex/metrica/impl/ob/fm;

.field private q:Lcom/yandex/metrica/impl/ob/fm;

.field private r:Lcom/yandex/metrica/impl/ob/fm;

.field private s:Lcom/yandex/metrica/impl/ob/fm;

.field private t:Lcom/yandex/metrica/impl/ob/fm;

.field private u:Lcom/yandex/metrica/impl/ob/fm;

.field private v:Lcom/yandex/metrica/impl/ob/fm;

.field private w:Lcom/yandex/metrica/impl/ob/fm;

.field private x:Lcom/yandex/metrica/impl/ob/fm;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 26
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "PREF_KEY_DEVICE_ID_"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/fk;->c:Lcom/yandex/metrica/impl/ob/fm;

    .line 27
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "PREF_KEY_UID_"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/fk;->d:Lcom/yandex/metrica/impl/ob/fm;

    .line 28
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "PREF_KEY_HOST_URL_"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/fk;->g:Lcom/yandex/metrica/impl/ob/fm;

    .line 29
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "PREF_KEY_REPORT_URL_"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/fk;->h:Lcom/yandex/metrica/impl/ob/fm;

    .line 30
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "PREF_KEY_GET_AD_URL"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/fk;->i:Lcom/yandex/metrica/impl/ob/fm;

    .line 31
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "PREF_KEY_REPORT_AD_URL"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/fk;->j:Lcom/yandex/metrica/impl/ob/fm;

    .line 32
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "PREF_KEY_STARTUP_OBTAIN_TIME_"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/fk;->k:Lcom/yandex/metrica/impl/ob/fm;

    .line 33
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "PREF_KEY_STARTUP_ENCODED_CLIDS_"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/fk;->l:Lcom/yandex/metrica/impl/ob/fm;

    .line 34
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "PREF_KEY_DISTRIBUTION_REFERRER_"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/fk;->m:Lcom/yandex/metrica/impl/ob/fm;

    .line 35
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "STARTUP_CLIDS_MATCH_WITH_APP_CLIDS_"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/fk;->e:Lcom/yandex/metrica/impl/ob/fm;

    .line 36
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "PREF_KEY_PINNING_UPDATE_URL"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/fk;->f:Lcom/yandex/metrica/impl/ob/fm;

    .line 37
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "PREF_KEY_EASY_COLLECTING_ENABLED_"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/fk;->n:Lcom/yandex/metrica/impl/ob/fm;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/yandex/metrica/impl/ob/fk;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 53
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Lcom/yandex/metrica/impl/ob/ff;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 58
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    sget-object v1, Lcom/yandex/metrica/impl/ob/fk;->c:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/fk;->o:Lcom/yandex/metrica/impl/ob/fm;

    .line 59
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    sget-object v1, Lcom/yandex/metrica/impl/ob/fk;->d:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/fk;->i()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/fk;->p:Lcom/yandex/metrica/impl/ob/fm;

    .line 60
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    sget-object v1, Lcom/yandex/metrica/impl/ob/fk;->g:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/fk;->i()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/fk;->q:Lcom/yandex/metrica/impl/ob/fm;

    .line 61
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    sget-object v1, Lcom/yandex/metrica/impl/ob/fk;->h:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/fk;->i()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/fk;->r:Lcom/yandex/metrica/impl/ob/fm;

    .line 62
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    sget-object v1, Lcom/yandex/metrica/impl/ob/fk;->i:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/fk;->i()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/fk;->s:Lcom/yandex/metrica/impl/ob/fm;

    .line 63
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    sget-object v1, Lcom/yandex/metrica/impl/ob/fk;->j:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/fk;->i()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/fk;->t:Lcom/yandex/metrica/impl/ob/fm;

    .line 64
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    sget-object v1, Lcom/yandex/metrica/impl/ob/fk;->k:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/fk;->i()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/fk;->u:Lcom/yandex/metrica/impl/ob/fm;

    .line 65
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    sget-object v1, Lcom/yandex/metrica/impl/ob/fk;->l:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/fk;->i()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/fk;->v:Lcom/yandex/metrica/impl/ob/fm;

    .line 66
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    sget-object v1, Lcom/yandex/metrica/impl/ob/fk;->m:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/fk;->i()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/fk;->w:Lcom/yandex/metrica/impl/ob/fm;

    .line 67
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    sget-object v1, Lcom/yandex/metrica/impl/ob/fk;->n:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/fk;->i()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/fk;->x:Lcom/yandex/metrica/impl/ob/fm;

    .line 68
    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 177
    const-string v0, "_startupserviceinfopreferences"

    invoke-static {p0, v0}, Lcom/yandex/metrica/impl/ob/fn;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 178
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    sget-object v1, Lcom/yandex/metrica/impl/ob/fk;->c:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 179
    return-void
.end method


# virtual methods
.method public a(J)J
    .locals 3

    .prologue
    .line 76
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fk;->b:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fk;->u:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1, p2}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 108
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fk;->b:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fk;->w:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->a()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 80
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fk;->b:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fk;->o:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 84
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fk;->b:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fk;->p:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 182
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fk;->o:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/ob/fk;->h(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/ff;

    move-result-object v0

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fk;->p:Lcom/yandex/metrica/impl/ob/fm;

    .line 183
    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/ff;->h(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/ff;

    move-result-object v0

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fk;->q:Lcom/yandex/metrica/impl/ob/fm;

    .line 184
    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/ff;->h(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/ff;

    move-result-object v0

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fk;->r:Lcom/yandex/metrica/impl/ob/fm;

    .line 185
    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/ff;->h(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/ff;

    move-result-object v0

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fk;->s:Lcom/yandex/metrica/impl/ob/fm;

    .line 186
    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/ff;->h(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/ff;

    move-result-object v0

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fk;->t:Lcom/yandex/metrica/impl/ob/fm;

    .line 187
    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/ff;->h(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/ff;

    move-result-object v0

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fk;->u:Lcom/yandex/metrica/impl/ob/fm;

    .line 188
    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/ff;->h(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/ff;

    move-result-object v0

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fk;->x:Lcom/yandex/metrica/impl/ob/fm;

    .line 189
    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/ff;->h(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/ff;

    move-result-object v0

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fk;->v:Lcom/yandex/metrica/impl/ob/fm;

    .line 190
    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/ff;->h(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/ff;

    move-result-object v0

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fk;->w:Lcom/yandex/metrica/impl/ob/fm;

    .line 191
    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/ff;->h(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/ff;

    move-result-object v0

    sget-object v1, Lcom/yandex/metrica/impl/ob/fk;->e:Lcom/yandex/metrica/impl/ob/fm;

    .line 192
    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/ff;->h(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/ff;

    move-result-object v0

    sget-object v1, Lcom/yandex/metrica/impl/ob/fk;->f:Lcom/yandex/metrica/impl/ob/fm;

    .line 193
    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/ff;->h(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/ff;

    move-result-object v0

    .line 194
    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/ff;->j()V

    .line 195
    return-void
.end method

.method public c(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 88
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fk;->b:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fk;->q:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 92
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fk;->b:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fk;->v:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 96
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fk;->b:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fk;->r:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    const-string v0, "_startupserviceinfopreferences"

    return-object v0
.end method

.method public f(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 100
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fk;->b:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fk;->s:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public g(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 104
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fk;->b:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fk;->t:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public i(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/fk;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fk;->p:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/yandex/metrica/impl/ob/fk;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/yandex/metrica/impl/ob/ff;

    move-result-object v0

    check-cast v0, Lcom/yandex/metrica/impl/ob/fk;

    return-object v0
.end method

.method public j(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/fk;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fk;->o:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/yandex/metrica/impl/ob/fk;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/yandex/metrica/impl/ob/ff;

    move-result-object v0

    check-cast v0, Lcom/yandex/metrica/impl/ob/fk;

    return-object v0
.end method
