.class public abstract Lcom/yandex/metrica/impl/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/yandex/metrica/b;


# static fields
.field private static final c:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field protected final a:Lcom/yandex/metrica/impl/ax;

.field protected final b:Lcom/yandex/metrica/impl/az;

.field private d:Lcom/yandex/metrica/impl/x;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 37
    new-instance v0, Ljava/util/HashSet;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Integer;

    const/4 v2, 0x0

    const/16 v3, 0xe

    .line 38
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const/16 v3, 0xf

    .line 39
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    .line 38
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lcom/yandex/metrica/impl/b;->c:Ljava/util/Collection;

    .line 37
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/yandex/metrica/impl/az;Lcom/yandex/metrica/impl/ax;)V
    .locals 2

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    .line 53
    iput-object p3, p0, Lcom/yandex/metrica/impl/b;->b:Lcom/yandex/metrica/impl/az;

    .line 55
    iput-object p4, p0, Lcom/yandex/metrica/impl/b;->a:Lcom/yandex/metrica/impl/ax;

    .line 56
    iget-object v0, p0, Lcom/yandex/metrica/impl/b;->a:Lcom/yandex/metrica/impl/ax;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ax;->b()Lcom/yandex/metrica/CounterConfiguration;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/yandex/metrica/CounterConfiguration;->a(Ljava/lang/String;)V

    .line 57
    iget-object v0, p0, Lcom/yandex/metrica/impl/b;->a:Lcom/yandex/metrica/impl/ax;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ax;->b()Lcom/yandex/metrica/CounterConfiguration;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/CounterConfiguration;->c(Ljava/lang/String;)V

    .line 59
    iget-object v0, p0, Lcom/yandex/metrica/impl/b;->a:Lcom/yandex/metrica/impl/ax;

    invoke-static {}, Lcom/yandex/metrica/impl/utils/f$a;->d()Lcom/yandex/metrica/impl/utils/f$a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ax;->a(Lcom/yandex/metrica/impl/utils/f$a;)V

    .line 60
    return-void
.end method

.method private a(Lcom/yandex/metrica/impl/i;)V
    .locals 2

    .prologue
    .line 281
    iget-object v0, p0, Lcom/yandex/metrica/impl/b;->b:Lcom/yandex/metrica/impl/az;

    iget-object v1, p0, Lcom/yandex/metrica/impl/b;->a:Lcom/yandex/metrica/impl/ax;

    invoke-virtual {v0, p1, v1}, Lcom/yandex/metrica/impl/az;->a(Lcom/yandex/metrica/impl/i;Lcom/yandex/metrica/impl/ax;)V

    .line 282
    return-void
.end method


# virtual methods
.method protected a()V
    .locals 2

    .prologue
    .line 63
    iget-object v0, p0, Lcom/yandex/metrica/impl/b;->b:Lcom/yandex/metrica/impl/az;

    iget-object v1, p0, Lcom/yandex/metrica/impl/b;->a:Lcom/yandex/metrica/impl/ax;

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/az;->a(Lcom/yandex/metrica/impl/ax;)V

    .line 64
    return-void
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 238
    iget-object v0, p0, Lcom/yandex/metrica/impl/b;->a:Lcom/yandex/metrica/impl/ax;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ax;->b()Lcom/yandex/metrica/CounterConfiguration;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/yandex/metrica/CounterConfiguration;->b(I)V

    .line 239
    return-void
.end method

.method public a(ILjava/lang/String;Ljava/lang/String;Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1194
    sget-object v0, Lcom/yandex/metrica/impl/b;->c:Ljava/util/Collection;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    if-lez p1, :cond_1

    const/16 v0, 0x63

    if-gt p1, v0, :cond_1

    const/4 v0, 0x1

    .line 184
    :goto_0
    if-nez v0, :cond_0

    .line 188
    if-nez p4, :cond_2

    const/4 v0, 0x0

    :goto_1
    invoke-static {p1, p2, p3, v0}, Lcom/yandex/metrica/impl/q;->a(ILjava/lang/String;Ljava/lang/String;Ljava/util/Map;)Lcom/yandex/metrica/impl/i;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/yandex/metrica/impl/b;->a(Lcom/yandex/metrica/impl/i;)V

    .line 191
    :cond_0
    return-void

    .line 1194
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 188
    :cond_2
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, p4}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    goto :goto_1
.end method

.method a(Lcom/yandex/metrica/impl/k;)V
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/yandex/metrica/impl/b;->a:Lcom/yandex/metrica/impl/ax;

    invoke-virtual {v0, p1}, Lcom/yandex/metrica/impl/ax;->a(Lcom/yandex/metrica/impl/k;)V

    .line 72
    return-void
.end method

.method a(Lcom/yandex/metrica/impl/ob/fz;)V
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/yandex/metrica/impl/b;->a:Lcom/yandex/metrica/impl/ax;

    invoke-virtual {v0, p1}, Lcom/yandex/metrica/impl/ax;->b(Lcom/yandex/metrica/impl/ob/fz;)V

    .line 68
    return-void
.end method

.method a(Lcom/yandex/metrica/impl/x;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/yandex/metrica/impl/b;->d:Lcom/yandex/metrica/impl/x;

    .line 76
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/yandex/metrica/impl/b;->a:Lcom/yandex/metrica/impl/ax;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ax;->b()Lcom/yandex/metrica/CounterConfiguration;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/yandex/metrica/CounterConfiguration;->g(Ljava/lang/String;)V

    .line 80
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 83
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/yandex/metrica/impl/b;->a:Lcom/yandex/metrica/impl/ax;

    invoke-virtual {v0, p1, p2}, Lcom/yandex/metrica/impl/ax;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    :cond_0
    return-void
.end method

.method public a(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 89
    invoke-static {p1}, Lcom/yandex/metrica/impl/bl;->a(Ljava/util/Map;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 90
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 91
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v1, v0}, Lcom/yandex/metrica/impl/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 94
    :cond_0
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 113
    iget-object v0, p0, Lcom/yandex/metrica/impl/b;->b:Lcom/yandex/metrica/impl/az;

    iget-object v1, p0, Lcom/yandex/metrica/impl/b;->a:Lcom/yandex/metrica/impl/ax;

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/az;->b(Lcom/yandex/metrica/impl/ax;)V

    .line 114
    return-void
.end method

.method b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 123
    iget-object v0, p0, Lcom/yandex/metrica/impl/b;->b:Lcom/yandex/metrica/impl/az;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/az;->d()V

    .line 127
    iget-object v0, p0, Lcom/yandex/metrica/impl/b;->d:Lcom/yandex/metrica/impl/x;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/x;->b()V

    .line 129
    iget-object v0, p0, Lcom/yandex/metrica/impl/b;->b:Lcom/yandex/metrica/impl/az;

    invoke-static {p1}, Lcom/yandex/metrica/impl/q;->b(Ljava/lang/String;)Lcom/yandex/metrica/impl/i;

    move-result-object v1

    iget-object v2, p0, Lcom/yandex/metrica/impl/b;->a:Lcom/yandex/metrica/impl/ax;

    invoke-virtual {v0, v1, v2}, Lcom/yandex/metrica/impl/az;->a(Lcom/yandex/metrica/impl/i;Lcom/yandex/metrica/impl/ax;)V

    .line 131
    iget-object v0, p0, Lcom/yandex/metrica/impl/b;->a:Lcom/yandex/metrica/impl/ax;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ax;->e()Z

    move-result v0

    .line 1140
    if-eqz v0, :cond_0

    .line 1141
    iget-object v0, p0, Lcom/yandex/metrica/impl/b;->b:Lcom/yandex/metrica/impl/az;

    sget-object v1, Lcom/yandex/metrica/impl/q$a;->m:Lcom/yandex/metrica/impl/q$a;

    invoke-static {v1}, Lcom/yandex/metrica/impl/q;->d(Lcom/yandex/metrica/impl/q$a;)Lcom/yandex/metrica/impl/i;

    move-result-object v1

    iget-object v2, p0, Lcom/yandex/metrica/impl/b;->a:Lcom/yandex/metrica/impl/ax;

    invoke-virtual {v0, v1, v2}, Lcom/yandex/metrica/impl/az;->a(Lcom/yandex/metrica/impl/i;Lcom/yandex/metrica/impl/ax;)V

    .line 134
    :cond_0
    return-void
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 106
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 107
    iget-object v0, p0, Lcom/yandex/metrica/impl/b;->b:Lcom/yandex/metrica/impl/az;

    iget-object v1, p0, Lcom/yandex/metrica/impl/b;->a:Lcom/yandex/metrica/impl/ax;

    invoke-virtual {v0, p1, p2, v1}, Lcom/yandex/metrica/impl/az;->a(Ljava/lang/String;Ljava/lang/String;Lcom/yandex/metrica/impl/ax;)V

    .line 109
    :cond_0
    return-void
.end method

.method public b(Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 97
    invoke-static {p1}, Lcom/yandex/metrica/impl/bl;->a(Ljava/util/Map;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 98
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 99
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v1, v0}, Lcom/yandex/metrica/impl/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 102
    :cond_0
    return-void
.end method

.method c(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 151
    iget-object v0, p0, Lcom/yandex/metrica/impl/b;->a:Lcom/yandex/metrica/impl/ax;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ax;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 152
    iget-object v0, p0, Lcom/yandex/metrica/impl/b;->b:Lcom/yandex/metrica/impl/az;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/az;->e()V

    .line 155
    iget-object v0, p0, Lcom/yandex/metrica/impl/b;->d:Lcom/yandex/metrica/impl/x;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/x;->a()V

    .line 157
    iget-object v0, p0, Lcom/yandex/metrica/impl/b;->b:Lcom/yandex/metrica/impl/az;

    invoke-static {p1}, Lcom/yandex/metrica/impl/q;->c(Ljava/lang/String;)Lcom/yandex/metrica/impl/i;

    move-result-object v1

    iget-object v2, p0, Lcom/yandex/metrica/impl/b;->a:Lcom/yandex/metrica/impl/ax;

    invoke-virtual {v0, v1, v2}, Lcom/yandex/metrica/impl/az;->a(Lcom/yandex/metrica/impl/i;Lcom/yandex/metrica/impl/ax;)V

    .line 159
    iget-object v0, p0, Lcom/yandex/metrica/impl/b;->a:Lcom/yandex/metrica/impl/ax;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ax;->d()V

    .line 161
    :cond_0
    return-void
.end method

.method c()Z
    .locals 4

    .prologue
    .line 267
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/b;->e()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 268
    :goto_0
    if-eqz v0, :cond_0

    .line 269
    sget-object v1, Lcom/yandex/metrica/impl/q$a;->e:Lcom/yandex/metrica/impl/q$a;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/q$a;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/yandex/metrica/impl/q;->c(Ljava/lang/String;)Lcom/yandex/metrica/impl/i;

    move-result-object v1

    .line 270
    iget-object v2, p0, Lcom/yandex/metrica/impl/b;->b:Lcom/yandex/metrica/impl/az;

    iget-object v3, p0, Lcom/yandex/metrica/impl/b;->a:Lcom/yandex/metrica/impl/ax;

    invoke-virtual {v2, v1, v3}, Lcom/yandex/metrica/impl/az;->a(Lcom/yandex/metrica/impl/i;Lcom/yandex/metrica/impl/ax;)V

    .line 273
    :cond_0
    return v0

    .line 267
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method d()Lcom/yandex/metrica/impl/ax;
    .locals 1

    .prologue
    .line 277
    iget-object v0, p0, Lcom/yandex/metrica/impl/b;->a:Lcom/yandex/metrica/impl/ax;

    return-object v0
.end method

.method public d(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 219
    const-string v0, "Native Crash"

    invoke-static {p1, v0}, Lcom/yandex/metrica/impl/bl;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1289
    iget-object v0, p0, Lcom/yandex/metrica/impl/b;->b:Lcom/yandex/metrica/impl/az;

    iget-object v1, p0, Lcom/yandex/metrica/impl/b;->a:Lcom/yandex/metrica/impl/ax;

    invoke-virtual {v0, p1, v1}, Lcom/yandex/metrica/impl/az;->a(Ljava/lang/String;Lcom/yandex/metrica/impl/ax;)V

    .line 222
    return-void
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 293
    iget-object v0, p0, Lcom/yandex/metrica/impl/b;->a:Lcom/yandex/metrica/impl/ax;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ax;->a()Z

    move-result v0

    return v0
.end method

.method public onPauseSession()V
    .locals 1

    .prologue
    .line 147
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/b;->c(Ljava/lang/String;)V

    .line 148
    return-void
.end method

.method public onResumeSession()V
    .locals 1

    .prologue
    .line 118
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/b;->b(Ljava/lang/String;)V

    .line 119
    return-void
.end method

.method public reportError(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "error"    # Ljava/lang/Throwable;

    .prologue
    .line 200
    const-string v0, "Message"

    invoke-static {p1, v0}, Lcom/yandex/metrica/impl/bl;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 202
    const/4 v0, 0x0

    invoke-static {v0, p2}, Lcom/yandex/metrica/impl/bl;->a(Ljava/lang/String;Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    .line 203
    invoke-static {p1, v0}, Lcom/yandex/metrica/impl/q;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/yandex/metrica/impl/i;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/yandex/metrica/impl/b;->a(Lcom/yandex/metrica/impl/i;)V

    .line 204
    return-void
.end method

.method public reportEvent(Ljava/lang/String;)V
    .locals 1
    .param p1, "eventName"    # Ljava/lang/String;

    .prologue
    .line 165
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/yandex/metrica/impl/b;->reportEvent(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    return-void
.end method

.method public reportEvent(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "eventName"    # Ljava/lang/String;
    .param p2, "jsonValue"    # Ljava/lang/String;

    .prologue
    .line 170
    const-string v0, "Event Name"

    invoke-static {p1, v0}, Lcom/yandex/metrica/impl/bl;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 171
    invoke-static {p1, p2}, Lcom/yandex/metrica/impl/q;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/yandex/metrica/impl/i;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/yandex/metrica/impl/b;->a(Lcom/yandex/metrica/impl/i;)V

    .line 172
    return-void
.end method

.method public reportEvent(Ljava/lang/String;Ljava/util/Map;)V
    .locals 4
    .param p1, "eventName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 176
    .local p2, "attributes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v0, "Event Name"

    invoke-static {p1, v0}, Lcom/yandex/metrica/impl/bl;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 177
    invoke-static {p2}, Lcom/yandex/metrica/impl/bl;->a(Ljava/util/Map;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 178
    :goto_0
    iget-object v1, p0, Lcom/yandex/metrica/impl/b;->b:Lcom/yandex/metrica/impl/az;

    invoke-static {p1}, Lcom/yandex/metrica/impl/q;->a(Ljava/lang/String;)Lcom/yandex/metrica/impl/i;

    move-result-object v2

    invoke-virtual {p0}, Lcom/yandex/metrica/impl/b;->d()Lcom/yandex/metrica/impl/ax;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, Lcom/yandex/metrica/impl/az;->a(Lcom/yandex/metrica/impl/i;Lcom/yandex/metrica/impl/ax;Ljava/util/Map;)Ljava/util/concurrent/Future;

    .line 179
    return-void

    .line 177
    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, p2}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    goto :goto_0
.end method

.method public reportUnhandledException(Ljava/lang/Throwable;)V
    .locals 2
    .param p1, "exception"    # Ljava/lang/Throwable;

    .prologue
    .line 213
    const-string v0, "Exception"

    invoke-static {p1, v0}, Lcom/yandex/metrica/impl/bl;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 1285
    iget-object v0, p0, Lcom/yandex/metrica/impl/b;->b:Lcom/yandex/metrica/impl/az;

    iget-object v1, p0, Lcom/yandex/metrica/impl/b;->a:Lcom/yandex/metrica/impl/ax;

    invoke-virtual {v0, p1, v1}, Lcom/yandex/metrica/impl/az;->a(Ljava/lang/Throwable;Lcom/yandex/metrica/impl/ax;)V

    .line 216
    return-void
.end method

.method public setSessionTimeout(I)V
    .locals 1
    .param p1, "sessionTimeOut"    # I

    .prologue
    .line 208
    iget-object v0, p0, Lcom/yandex/metrica/impl/b;->a:Lcom/yandex/metrica/impl/ax;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ax;->b()Lcom/yandex/metrica/CounterConfiguration;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/yandex/metrica/CounterConfiguration;->c(I)V

    .line 209
    return-void
.end method
