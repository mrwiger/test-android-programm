.class Lcom/samsung/multiscreen/MSFDSearchProvider$1;
.super Ljava/lang/Object;
.source "MSFDSearchProvider.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/multiscreen/MSFDSearchProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/multiscreen/MSFDSearchProvider;


# direct methods
.method constructor <init>(Lcom/samsung/multiscreen/MSFDSearchProvider;)V
    .locals 0
    .param p1, "this$0"    # Lcom/samsung/multiscreen/MSFDSearchProvider;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/samsung/multiscreen/MSFDSearchProvider$1;->this$0:Lcom/samsung/multiscreen/MSFDSearchProvider;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/multiscreen/MSFDSearchProvider$1;Ljava/lang/String;J)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/multiscreen/MSFDSearchProvider$1;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # J

    .prologue
    .line 105
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/multiscreen/MSFDSearchProvider$1;->updateAlive(Ljava/lang/String;J)V

    return-void
.end method

.method private declared-synchronized reapServices()V
    .locals 8

    .prologue
    .line 226
    monitor-enter p0

    :try_start_0
    new-instance v6, Ljava/util/Date;

    invoke-direct {v6}, Ljava/util/Date;-><init>()V

    invoke-virtual {v6}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    .line 227
    .local v4, "now":J
    iget-object v6, p0, Lcom/samsung/multiscreen/MSFDSearchProvider$1;->this$0:Lcom/samsung/multiscreen/MSFDSearchProvider;

    invoke-static {v6}, Lcom/samsung/multiscreen/MSFDSearchProvider;->access$200(Lcom/samsung/multiscreen/MSFDSearchProvider;)Ljava/util/Map;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 228
    .local v2, "id":Ljava/lang/String;
    iget-object v6, p0, Lcom/samsung/multiscreen/MSFDSearchProvider$1;->this$0:Lcom/samsung/multiscreen/MSFDSearchProvider;

    invoke-static {v6}, Lcom/samsung/multiscreen/MSFDSearchProvider;->access$200(Lcom/samsung/multiscreen/MSFDSearchProvider;)Ljava/util/Map;

    move-result-object v6

    invoke-interface {v6, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 229
    .local v0, "expires":J
    cmp-long v6, v0, v4

    if-gez v6, :cond_0

    .line 230
    iget-object v6, p0, Lcom/samsung/multiscreen/MSFDSearchProvider$1;->this$0:Lcom/samsung/multiscreen/MSFDSearchProvider;

    invoke-virtual {v6, v2}, Lcom/samsung/multiscreen/MSFDSearchProvider;->getServiceById(Ljava/lang/String;)Lcom/samsung/multiscreen/Service;

    move-result-object v3

    .line 231
    .local v3, "service":Lcom/samsung/multiscreen/Service;
    iget-object v6, p0, Lcom/samsung/multiscreen/MSFDSearchProvider$1;->this$0:Lcom/samsung/multiscreen/MSFDSearchProvider;

    invoke-static {v6}, Lcom/samsung/multiscreen/MSFDSearchProvider;->access$200(Lcom/samsung/multiscreen/MSFDSearchProvider;)Ljava/util/Map;

    move-result-object v6

    invoke-interface {v6, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 232
    if-eqz v3, :cond_0

    .line 233
    iget-object v6, p0, Lcom/samsung/multiscreen/MSFDSearchProvider$1;->this$0:Lcom/samsung/multiscreen/MSFDSearchProvider;

    invoke-virtual {v6, v3}, Lcom/samsung/multiscreen/MSFDSearchProvider;->removeServiceAndNotify(Lcom/samsung/multiscreen/Service;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 226
    .end local v0    # "expires":J
    .end local v2    # "id":Ljava/lang/String;
    .end local v3    # "service":Lcom/samsung/multiscreen/Service;
    .end local v4    # "now":J
    :catchall_0
    move-exception v6

    monitor-exit p0

    throw v6

    .line 237
    .restart local v4    # "now":J
    :cond_1
    monitor-exit p0

    return-void
.end method

.method private declared-synchronized updateAlive(Ljava/lang/String;J)V
    .locals 6
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "ttl"    # J

    .prologue
    .line 240
    monitor-enter p0

    :try_start_0
    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    .line 241
    .local v2, "now":J
    add-long v0, v2, p2

    .line 242
    .local v0, "expires":J
    iget-object v4, p0, Lcom/samsung/multiscreen/MSFDSearchProvider$1;->this$0:Lcom/samsung/multiscreen/MSFDSearchProvider;

    invoke-static {v4}, Lcom/samsung/multiscreen/MSFDSearchProvider;->access$200(Lcom/samsung/multiscreen/MSFDSearchProvider;)Ljava/util/Map;

    move-result-object v4

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v4, p1, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 243
    monitor-exit p0

    return-void

    .line 240
    .end local v0    # "expires":J
    .end local v2    # "now":J
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4
.end method


# virtual methods
.method public run()V
    .locals 22

    .prologue
    .line 110
    const/16 v18, 0x400

    :try_start_0
    move/from16 v0, v18

    new-array v4, v0, [B

    .line 111
    .local v4, "buf":[B
    new-instance v9, Ljava/net/DatagramPacket;

    array-length v0, v4

    move/from16 v18, v0

    move/from16 v0, v18

    invoke-direct {v9, v4, v0}, Ljava/net/DatagramPacket;-><init>([BI)V

    .line 113
    .local v9, "receivePacket":Ljava/net/DatagramPacket;
    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/multiscreen/MSFDSearchProvider$1;->this$0:Lcom/samsung/multiscreen/MSFDSearchProvider;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/samsung/multiscreen/MSFDSearchProvider;->access$000(Lcom/samsung/multiscreen/MSFDSearchProvider;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v18

    if-eqz v18, :cond_2

    .line 115
    :try_start_1
    invoke-direct/range {p0 .. p0}, Lcom/samsung/multiscreen/MSFDSearchProvider$1;->reapServices()V

    .line 118
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/multiscreen/MSFDSearchProvider$1;->this$0:Lcom/samsung/multiscreen/MSFDSearchProvider;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/samsung/multiscreen/MSFDSearchProvider;->access$100(Lcom/samsung/multiscreen/MSFDSearchProvider;)Ljava/net/MulticastSocket;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v9}, Ljava/net/MulticastSocket;->receive(Ljava/net/DatagramPacket;)V

    .line 119
    invoke-virtual {v9}, Ljava/net/DatagramPacket;->getLength()I
    :try_end_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/net/SocketException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v18

    if-lez v18, :cond_0

    .line 128
    :try_start_2
    new-instance v10, Ljava/lang/String;

    invoke-virtual {v9}, Ljava/net/DatagramPacket;->getData()[B

    move-result-object v18

    const/16 v19, 0x0

    invoke-virtual {v9}, Ljava/net/DatagramPacket;->getLength()I

    move-result v20

    const-string v21, "UTF-8"

    move-object/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    move-object/from16 v3, v21

    invoke-direct {v10, v0, v1, v2, v3}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    .line 129
    .local v10, "response":Ljava/lang/String;
    invoke-static {v10}, Lcom/samsung/multiscreen/util/JSONUtil;->parse(Ljava/lang/String;)Ljava/util/Map;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/net/SocketTimeoutException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/net/SocketException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v11

    .line 137
    .local v11, "responseMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    if-eqz v11, :cond_0

    .line 138
    :try_start_3
    invoke-interface {v11}, Ljava/util/Map;->isEmpty()Z

    move-result v18

    if-nez v18, :cond_0

    const-string v18, "discover"

    const-string v19, "type"

    .line 139
    move-object/from16 v0, v19

    invoke-interface {v11, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    .local v13, "state":Ljava/lang/String;
    move-object/from16 v0, v18

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_0

    .line 143
    const-string v18, "sid"

    move-object/from16 v0, v18

    invoke-interface {v11, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 144
    .local v8, "id":Ljava/lang/String;
    if-eqz v8, :cond_0

    .line 146
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/multiscreen/MSFDSearchProvider$1;->this$0:Lcom/samsung/multiscreen/MSFDSearchProvider;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v8}, Lcom/samsung/multiscreen/MSFDSearchProvider;->getServiceById(Ljava/lang/String;)Lcom/samsung/multiscreen/Service;

    move-result-object v12

    .line 148
    .local v12, "service":Lcom/samsung/multiscreen/Service;
    const-string v18, "alive"

    move-object/from16 v0, v18

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_1

    const-string v18, "up"

    .line 149
    move-object/from16 v0, v18

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_6

    .line 151
    :cond_1
    const-string v18, "ttl"

    move-object/from16 v0, v18

    invoke-interface {v11, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Ljava/lang/Long;

    invoke-virtual/range {v18 .. v18}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    .line 153
    .local v14, "ttl":J
    if-nez v12, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/multiscreen/MSFDSearchProvider$1;->this$0:Lcom/samsung/multiscreen/MSFDSearchProvider;

    move-object/from16 v18, v0

    .line 154
    invoke-static/range {v18 .. v18}, Lcom/samsung/multiscreen/MSFDSearchProvider;->access$200(Lcom/samsung/multiscreen/MSFDSearchProvider;)Ljava/util/Map;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-interface {v0, v8}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_4

    .line 162
    move-object/from16 v0, p0

    invoke-direct {v0, v8, v14, v15}, Lcom/samsung/multiscreen/MSFDSearchProvider$1;->updateAlive(Ljava/lang/String;J)V

    .line 165
    const-string v18, "data"

    move-object/from16 v0, v18

    invoke-interface {v11, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map;

    .line 166
    .local v5, "dataMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    if-eqz v5, :cond_0

    .line 167
    const-string v18, "v2"

    move-object/from16 v0, v18

    invoke-interface {v5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/Map;

    .line 168
    .local v7, "endpointMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    if-eqz v7, :cond_0

    .line 169
    const-string v18, "uri"

    move-object/from16 v0, v18

    invoke-interface {v7, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/String;

    .line 170
    .local v17, "url":Ljava/lang/String;
    if-eqz v17, :cond_0

    .line 171
    invoke-static/range {v17 .. v17}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v16

    .line 173
    .local v16, "uri":Landroid/net/Uri;
    const/16 v18, 0x7d0

    new-instance v19, Lcom/samsung/multiscreen/MSFDSearchProvider$1$1;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v8, v14, v15}, Lcom/samsung/multiscreen/MSFDSearchProvider$1$1;-><init>(Lcom/samsung/multiscreen/MSFDSearchProvider$1;Ljava/lang/String;J)V

    move-object/from16 v0, v16

    move/from16 v1, v18

    move-object/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/samsung/multiscreen/Service;->getByURI(Landroid/net/Uri;ILcom/samsung/multiscreen/Result;)V

    goto/16 :goto_0

    .line 202
    .end local v5    # "dataMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v7    # "endpointMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v8    # "id":Ljava/lang/String;
    .end local v10    # "response":Ljava/lang/String;
    .end local v11    # "responseMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v12    # "service":Lcom/samsung/multiscreen/Service;
    .end local v13    # "state":Ljava/lang/String;
    .end local v14    # "ttl":J
    .end local v16    # "uri":Landroid/net/Uri;
    .end local v17    # "url":Ljava/lang/String;
    :catch_0
    move-exception v18

    goto/16 :goto_0

    .line 130
    :catch_1
    move-exception v6

    .line 131
    .local v6, "e":Ljava/lang/Exception;
    const-string v18, "MSFDSearchProvider"

    invoke-static {v6}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/net/SocketTimeoutException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/net/SocketException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_4
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 207
    .end local v6    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v6

    .line 217
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/multiscreen/MSFDSearchProvider$1;->this$0:Lcom/samsung/multiscreen/MSFDSearchProvider;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/samsung/multiscreen/MSFDSearchProvider;->access$100(Lcom/samsung/multiscreen/MSFDSearchProvider;)Ljava/net/MulticastSocket;

    move-result-object v18

    if-eqz v18, :cond_3

    .line 218
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/multiscreen/MSFDSearchProvider$1;->this$0:Lcom/samsung/multiscreen/MSFDSearchProvider;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/samsung/multiscreen/MSFDSearchProvider;->access$100(Lcom/samsung/multiscreen/MSFDSearchProvider;)Ljava/net/MulticastSocket;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/net/MulticastSocket;->close()V

    .line 223
    :cond_3
    return-void

    .line 193
    .restart local v8    # "id":Ljava/lang/String;
    .restart local v10    # "response":Ljava/lang/String;
    .restart local v11    # "responseMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    .restart local v12    # "service":Lcom/samsung/multiscreen/Service;
    .restart local v13    # "state":Ljava/lang/String;
    .restart local v14    # "ttl":J
    :cond_4
    :try_start_4
    move-object/from16 v0, p0

    invoke-direct {v0, v8, v14, v15}, Lcom/samsung/multiscreen/MSFDSearchProvider$1;->updateAlive(Ljava/lang/String;J)V
    :try_end_4
    .catch Ljava/net/SocketTimeoutException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/net/SocketException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 210
    .end local v8    # "id":Ljava/lang/String;
    .end local v10    # "response":Ljava/lang/String;
    .end local v11    # "responseMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v12    # "service":Lcom/samsung/multiscreen/Service;
    .end local v13    # "state":Ljava/lang/String;
    .end local v14    # "ttl":J
    :catch_3
    move-exception v6

    .line 211
    .local v6, "e":Ljava/io/IOException;
    :try_start_5
    const-string v18, "MSFDSearchProvider"

    invoke-static {v6}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0

    .line 217
    .end local v4    # "buf":[B
    .end local v6    # "e":Ljava/io/IOException;
    .end local v9    # "receivePacket":Ljava/net/DatagramPacket;
    :catchall_0
    move-exception v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/multiscreen/MSFDSearchProvider$1;->this$0:Lcom/samsung/multiscreen/MSFDSearchProvider;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/samsung/multiscreen/MSFDSearchProvider;->access$100(Lcom/samsung/multiscreen/MSFDSearchProvider;)Ljava/net/MulticastSocket;

    move-result-object v19

    if-eqz v19, :cond_5

    .line 218
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/multiscreen/MSFDSearchProvider$1;->this$0:Lcom/samsung/multiscreen/MSFDSearchProvider;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/samsung/multiscreen/MSFDSearchProvider;->access$100(Lcom/samsung/multiscreen/MSFDSearchProvider;)Ljava/net/MulticastSocket;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/net/MulticastSocket;->close()V

    :cond_5
    throw v18

    .line 195
    .restart local v4    # "buf":[B
    .restart local v8    # "id":Ljava/lang/String;
    .restart local v9    # "receivePacket":Ljava/net/DatagramPacket;
    .restart local v10    # "response":Ljava/lang/String;
    .restart local v11    # "responseMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    .restart local v12    # "service":Lcom/samsung/multiscreen/Service;
    .restart local v13    # "state":Ljava/lang/String;
    :cond_6
    if-eqz v12, :cond_0

    :try_start_6
    const-string v18, "down"

    .line 196
    move-object/from16 v0, v18

    invoke-virtual {v0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_0

    .line 198
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/multiscreen/MSFDSearchProvider$1;->this$0:Lcom/samsung/multiscreen/MSFDSearchProvider;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/samsung/multiscreen/MSFDSearchProvider;->access$200(Lcom/samsung/multiscreen/MSFDSearchProvider;)Ljava/util/Map;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-interface {v0, v8}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 199
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/multiscreen/MSFDSearchProvider$1;->this$0:Lcom/samsung/multiscreen/MSFDSearchProvider;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v12}, Lcom/samsung/multiscreen/MSFDSearchProvider;->removeServiceAndNotify(Lcom/samsung/multiscreen/Service;)V
    :try_end_6
    .catch Ljava/net/SocketTimeoutException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/net/SocketException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_4
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_0

    .line 212
    .end local v8    # "id":Ljava/lang/String;
    .end local v10    # "response":Ljava/lang/String;
    .end local v11    # "responseMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v12    # "service":Lcom/samsung/multiscreen/Service;
    .end local v13    # "state":Ljava/lang/String;
    :catch_4
    move-exception v6

    .line 213
    .local v6, "e":Ljava/lang/Exception;
    :try_start_7
    const-string v18, "MSFDSearchProvider"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "receiveHandler exception: "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual {v6}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_0
.end method
