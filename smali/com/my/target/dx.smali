.class public final Lcom/my/target/dx;
.super Landroid/widget/RelativeLayout;
.source "InterstitialImageView.java"


# static fields
.field private static final ar:I


# instance fields
.field private final F:Lcom/my/target/by;

.field private final as:Landroid/widget/RelativeLayout$LayoutParams;

.field private final at:Lcom/my/target/bx;

.field private final au:Landroid/widget/RelativeLayout$LayoutParams;

.field private final av:Lcom/my/target/bu;

.field private final aw:Lcom/my/target/cm;

.field private ax:Lcom/my/target/common/models/ImageData;

.field private ay:Lcom/my/target/common/models/ImageData;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/dx;->ar:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, -0x2

    .line 34
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 36
    invoke-virtual {p0, v5}, Lcom/my/target/dx;->setBackgroundColor(I)V

    .line 37
    invoke-static {p1}, Lcom/my/target/cm;->x(Landroid/content/Context;)Lcom/my/target/cm;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/dx;->aw:Lcom/my/target/cm;

    .line 38
    new-instance v0, Lcom/my/target/bx;

    invoke-direct {v0, p1}, Lcom/my/target/bx;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/dx;->at:Lcom/my/target/bx;

    .line 39
    iget-object v0, p0, Lcom/my/target/dx;->at:Lcom/my/target/bx;

    sget v1, Lcom/my/target/dx;->ar:I

    invoke-virtual {v0, v1}, Lcom/my/target/bx;->setId(I)V

    .line 40
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-object v0, p0, Lcom/my/target/dx;->au:Landroid/widget/RelativeLayout$LayoutParams;

    .line 41
    iget-object v0, p0, Lcom/my/target/dx;->au:Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 42
    iget-object v0, p0, Lcom/my/target/dx;->at:Lcom/my/target/bx;

    iget-object v1, p0, Lcom/my/target/dx;->au:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v0, v1}, Lcom/my/target/bx;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 44
    iget-object v0, p0, Lcom/my/target/dx;->at:Lcom/my/target/bx;

    invoke-virtual {p0, v0}, Lcom/my/target/dx;->addView(Landroid/view/View;)V

    .line 46
    new-instance v0, Lcom/my/target/by;

    invoke-direct {v0, p1}, Lcom/my/target/by;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/dx;->F:Lcom/my/target/by;

    .line 47
    iget-object v0, p0, Lcom/my/target/dx;->F:Lcom/my/target/by;

    const/4 v1, 0x1

    const/high16 v2, 0x41e00000    # 28.0f

    .line 49
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    .line 48
    invoke-static {v1, v2, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v1, v1

    .line 47
    invoke-static {v1}, Lcom/my/target/bq;->i(I)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1, v5}, Lcom/my/target/by;->b(Landroid/graphics/Bitmap;Z)V

    .line 50
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-object v0, p0, Lcom/my/target/dx;->as:Landroid/widget/RelativeLayout$LayoutParams;

    .line 51
    iget-object v0, p0, Lcom/my/target/dx;->as:Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, 0x7

    sget v2, Lcom/my/target/dx;->ar:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 52
    iget-object v0, p0, Lcom/my/target/dx;->as:Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, 0x6

    sget v2, Lcom/my/target/dx;->ar:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 53
    iget-object v0, p0, Lcom/my/target/dx;->F:Lcom/my/target/by;

    iget-object v1, p0, Lcom/my/target/dx;->as:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v0, v1}, Lcom/my/target/by;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 54
    new-instance v0, Lcom/my/target/bu;

    invoke-direct {v0, p1}, Lcom/my/target/bu;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/dx;->av:Lcom/my/target/bu;

    .line 56
    iget-object v0, p0, Lcom/my/target/dx;->F:Lcom/my/target/by;

    invoke-virtual {p0, v0}, Lcom/my/target/dx;->addView(Landroid/view/View;)V

    .line 57
    iget-object v0, p0, Lcom/my/target/dx;->av:Lcom/my/target/bu;

    invoke-virtual {p0, v0}, Lcom/my/target/dx;->addView(Landroid/view/View;)V

    .line 58
    return-void
.end method

.method private F()V
    .locals 2

    .prologue
    .line 115
    invoke-virtual {p0}, Lcom/my/target/dx;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "window"

    .line 116
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 117
    if-eqz v0, :cond_1

    .line 119
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 120
    invoke-virtual {v0}, Landroid/view/Display;->getWidth()I

    move-result v1

    .line 121
    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v0

    .line 123
    int-to-float v1, v1

    int-to-float v0, v0

    div-float v0, v1, v0

    .line 125
    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_2

    iget-object v0, p0, Lcom/my/target/dx;->ay:Lcom/my/target/common/models/ImageData;

    .line 127
    :goto_0
    if-nez v0, :cond_0

    .line 129
    iget-object v0, p0, Lcom/my/target/dx;->ay:Lcom/my/target/common/models/ImageData;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/my/target/dx;->ay:Lcom/my/target/common/models/ImageData;

    .line 132
    :cond_0
    :goto_1
    if-nez v0, :cond_4

    .line 139
    :cond_1
    :goto_2
    return-void

    .line 125
    :cond_2
    iget-object v0, p0, Lcom/my/target/dx;->ax:Lcom/my/target/common/models/ImageData;

    goto :goto_0

    .line 129
    :cond_3
    iget-object v0, p0, Lcom/my/target/dx;->ax:Lcom/my/target/common/models/ImageData;

    goto :goto_1

    .line 137
    :cond_4
    iget-object v1, p0, Lcom/my/target/dx;->at:Lcom/my/target/bx;

    invoke-virtual {v1, v0}, Lcom/my/target/bx;->setImageData(Lcom/my/target/common/models/ImageData;)V

    goto :goto_2
.end method


# virtual methods
.method public final a(Lcom/my/target/common/models/ImageData;Lcom/my/target/common/models/ImageData;Lcom/my/target/common/models/ImageData;)V
    .locals 3

    .prologue
    .line 72
    iput-object p1, p0, Lcom/my/target/dx;->ay:Lcom/my/target/common/models/ImageData;

    .line 73
    iput-object p2, p0, Lcom/my/target/dx;->ax:Lcom/my/target/common/models/ImageData;

    .line 75
    const/4 v0, 0x0

    .line 76
    if-eqz p3, :cond_0

    .line 78
    invoke-virtual {p3}, Lcom/my/target/common/models/ImageData;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 80
    :cond_0
    if-eqz v0, :cond_1

    .line 82
    iget-object v1, p0, Lcom/my/target/dx;->F:Lcom/my/target/by;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lcom/my/target/by;->b(Landroid/graphics/Bitmap;Z)V

    .line 83
    iget-object v0, p0, Lcom/my/target/dx;->as:Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/my/target/dx;->as:Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v2, p0, Lcom/my/target/dx;->F:Lcom/my/target/by;

    invoke-virtual {v2}, Lcom/my/target/by;->getMeasuredWidth()I

    move-result v2

    neg-int v2, v2

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 85
    :cond_1
    invoke-direct {p0}, Lcom/my/target/dx;->F()V

    .line 86
    return-void
.end method

.method public final getCloseButton()Lcom/my/target/by;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/my/target/dx;->F:Lcom/my/target/by;

    return-object v0
.end method

.method public final getImageView()Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/my/target/dx;->at:Lcom/my/target/bx;

    return-object v0
.end method

.method protected final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 109
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 110
    invoke-direct {p0}, Lcom/my/target/dx;->F()V

    .line 111
    return-void
.end method

.method public final setAgeRestrictions(Ljava/lang/String;)V
    .locals 7
    .param p1, "ageRestrictions"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    const/4 v5, -0x2

    const v4, -0x111112

    const/4 v3, 0x0

    .line 91
    iget-object v0, p0, Lcom/my/target/dx;->av:Lcom/my/target/bu;

    const v1, -0x777778

    invoke-virtual {v0, v6, v1}, Lcom/my/target/bu;->c(II)V

    .line 92
    iget-object v0, p0, Lcom/my/target/dx;->av:Lcom/my/target/bu;

    iget-object v1, p0, Lcom/my/target/dx;->aw:Lcom/my/target/cm;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    invoke-virtual {v0, v1, v3, v3, v3}, Lcom/my/target/bu;->setPadding(IIII)V

    .line 94
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v5, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 95
    iget-object v1, p0, Lcom/my/target/dx;->aw:Lcom/my/target/cm;

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 96
    const/4 v1, 0x5

    sget v2, Lcom/my/target/dx;->ar:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 97
    const/4 v1, 0x6

    sget v2, Lcom/my/target/dx;->ar:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 98
    iget-object v1, p0, Lcom/my/target/dx;->av:Lcom/my/target/bu;

    invoke-virtual {v1, v0}, Lcom/my/target/bu;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 100
    iget-object v0, p0, Lcom/my/target/dx;->av:Lcom/my/target/bu;

    invoke-virtual {v0, v4}, Lcom/my/target/bu;->setTextColor(I)V

    .line 101
    iget-object v0, p0, Lcom/my/target/dx;->av:Lcom/my/target/bu;

    iget-object v1, p0, Lcom/my/target/dx;->aw:Lcom/my/target/cm;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    invoke-virtual {v0, v6, v4, v1}, Lcom/my/target/bu;->a(III)V

    .line 102
    iget-object v0, p0, Lcom/my/target/dx;->av:Lcom/my/target/bu;

    const/high16 v1, 0x66000000

    invoke-virtual {v0, v1}, Lcom/my/target/bu;->setBackgroundColor(I)V

    .line 103
    iget-object v0, p0, Lcom/my/target/dx;->av:Lcom/my/target/bu;

    invoke-virtual {v0, p1}, Lcom/my/target/bu;->setText(Ljava/lang/CharSequence;)V

    .line 104
    return-void
.end method
