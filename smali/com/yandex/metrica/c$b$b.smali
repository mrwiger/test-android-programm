.class public final Lcom/yandex/metrica/c$b$b;
.super Lcom/yandex/metrica/impl/ob/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/yandex/metrica/c$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation


# static fields
.field private static volatile m:[Lcom/yandex/metrica/c$b$b;


# instance fields
.field public b:J

.field public c:J

.field public d:J

.field public e:D

.field public f:D

.field public g:I

.field public h:I

.field public i:I

.field public j:I

.field public k:I

.field public l:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2655
    invoke-direct {p0}, Lcom/yandex/metrica/impl/ob/d;-><init>()V

    .line 2656
    invoke-virtual {p0}, Lcom/yandex/metrica/c$b$b;->e()Lcom/yandex/metrica/c$b$b;

    .line 2657
    return-void
.end method

.method public static d()[Lcom/yandex/metrica/c$b$b;
    .locals 2

    .prologue
    .line 2609
    sget-object v0, Lcom/yandex/metrica/c$b$b;->m:[Lcom/yandex/metrica/c$b$b;

    if-nez v0, :cond_1

    .line 2610
    sget-object v1, Lcom/yandex/metrica/impl/ob/c;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 2612
    :try_start_0
    sget-object v0, Lcom/yandex/metrica/c$b$b;->m:[Lcom/yandex/metrica/c$b$b;

    if-nez v0, :cond_0

    .line 2613
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/yandex/metrica/c$b$b;

    sput-object v0, Lcom/yandex/metrica/c$b$b;->m:[Lcom/yandex/metrica/c$b$b;

    .line 2615
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2617
    :cond_1
    sget-object v0, Lcom/yandex/metrica/c$b$b;->m:[Lcom/yandex/metrica/c$b$b;

    return-object v0

    .line 2615
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public a(Lcom/yandex/metrica/impl/ob/b;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2678
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/yandex/metrica/c$b$b;->b:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/yandex/metrica/impl/ob/b;->a(IJ)V

    .line 2679
    const/4 v0, 0x2

    iget-wide v2, p0, Lcom/yandex/metrica/c$b$b;->c:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/yandex/metrica/impl/ob/b;->a(IJ)V

    .line 2680
    iget-wide v0, p0, Lcom/yandex/metrica/c$b$b;->d:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 2681
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/yandex/metrica/c$b$b;->d:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/yandex/metrica/impl/ob/b;->a(IJ)V

    .line 2683
    :cond_0
    const/4 v0, 0x4

    iget-wide v2, p0, Lcom/yandex/metrica/c$b$b;->e:D

    invoke-virtual {p1, v0, v2, v3}, Lcom/yandex/metrica/impl/ob/b;->a(ID)V

    .line 2684
    const/4 v0, 0x5

    iget-wide v2, p0, Lcom/yandex/metrica/c$b$b;->f:D

    invoke-virtual {p1, v0, v2, v3}, Lcom/yandex/metrica/impl/ob/b;->a(ID)V

    .line 2685
    iget v0, p0, Lcom/yandex/metrica/c$b$b;->g:I

    if-eqz v0, :cond_1

    .line 2686
    const/4 v0, 0x6

    iget v1, p0, Lcom/yandex/metrica/c$b$b;->g:I

    invoke-virtual {p1, v0, v1}, Lcom/yandex/metrica/impl/ob/b;->b(II)V

    .line 2688
    :cond_1
    iget v0, p0, Lcom/yandex/metrica/c$b$b;->h:I

    if-eqz v0, :cond_2

    .line 2689
    const/4 v0, 0x7

    iget v1, p0, Lcom/yandex/metrica/c$b$b;->h:I

    invoke-virtual {p1, v0, v1}, Lcom/yandex/metrica/impl/ob/b;->b(II)V

    .line 2691
    :cond_2
    iget v0, p0, Lcom/yandex/metrica/c$b$b;->i:I

    if-eqz v0, :cond_3

    .line 2692
    const/16 v0, 0x8

    iget v1, p0, Lcom/yandex/metrica/c$b$b;->i:I

    invoke-virtual {p1, v0, v1}, Lcom/yandex/metrica/impl/ob/b;->b(II)V

    .line 2694
    :cond_3
    iget v0, p0, Lcom/yandex/metrica/c$b$b;->j:I

    if-eqz v0, :cond_4

    .line 2695
    const/16 v0, 0x9

    iget v1, p0, Lcom/yandex/metrica/c$b$b;->j:I

    invoke-virtual {p1, v0, v1}, Lcom/yandex/metrica/impl/ob/b;->a(II)V

    .line 2697
    :cond_4
    iget v0, p0, Lcom/yandex/metrica/c$b$b;->k:I

    if-eqz v0, :cond_5

    .line 2698
    const/16 v0, 0xa

    iget v1, p0, Lcom/yandex/metrica/c$b$b;->k:I

    invoke-virtual {p1, v0, v1}, Lcom/yandex/metrica/impl/ob/b;->a(II)V

    .line 2700
    :cond_5
    iget v0, p0, Lcom/yandex/metrica/c$b$b;->l:I

    if-eqz v0, :cond_6

    .line 2701
    const/16 v0, 0xb

    iget v1, p0, Lcom/yandex/metrica/c$b$b;->l:I

    invoke-virtual {p1, v0, v1}, Lcom/yandex/metrica/impl/ob/b;->a(II)V

    .line 2703
    :cond_6
    invoke-super {p0, p1}, Lcom/yandex/metrica/impl/ob/d;->a(Lcom/yandex/metrica/impl/ob/b;)V

    .line 2704
    return-void
.end method

.method protected c()I
    .locals 6

    .prologue
    .line 2708
    invoke-super {p0}, Lcom/yandex/metrica/impl/ob/d;->c()I

    move-result v0

    .line 2709
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/yandex/metrica/c$b$b;->b:J

    .line 2710
    invoke-static {v1, v2, v3}, Lcom/yandex/metrica/impl/ob/b;->c(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2711
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/yandex/metrica/c$b$b;->c:J

    .line 2712
    invoke-static {v1, v2, v3}, Lcom/yandex/metrica/impl/ob/b;->c(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2713
    iget-wide v2, p0, Lcom/yandex/metrica/c$b$b;->d:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 2714
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/yandex/metrica/c$b$b;->d:J

    .line 2715
    invoke-static {v1, v2, v3}, Lcom/yandex/metrica/impl/ob/b;->c(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2717
    :cond_0
    const/4 v1, 0x4

    .line 2718
    invoke-static {v1}, Lcom/yandex/metrica/impl/ob/b;->d(I)I

    move-result v1

    add-int/2addr v0, v1

    .line 2719
    const/4 v1, 0x5

    .line 2720
    invoke-static {v1}, Lcom/yandex/metrica/impl/ob/b;->d(I)I

    move-result v1

    add-int/2addr v0, v1

    .line 2721
    iget v1, p0, Lcom/yandex/metrica/c$b$b;->g:I

    if-eqz v1, :cond_1

    .line 2722
    const/4 v1, 0x6

    iget v2, p0, Lcom/yandex/metrica/c$b$b;->g:I

    .line 2723
    invoke-static {v1, v2}, Lcom/yandex/metrica/impl/ob/b;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2725
    :cond_1
    iget v1, p0, Lcom/yandex/metrica/c$b$b;->h:I

    if-eqz v1, :cond_2

    .line 2726
    const/4 v1, 0x7

    iget v2, p0, Lcom/yandex/metrica/c$b$b;->h:I

    .line 2727
    invoke-static {v1, v2}, Lcom/yandex/metrica/impl/ob/b;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2729
    :cond_2
    iget v1, p0, Lcom/yandex/metrica/c$b$b;->i:I

    if-eqz v1, :cond_3

    .line 2730
    const/16 v1, 0x8

    iget v2, p0, Lcom/yandex/metrica/c$b$b;->i:I

    .line 2731
    invoke-static {v1, v2}, Lcom/yandex/metrica/impl/ob/b;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2733
    :cond_3
    iget v1, p0, Lcom/yandex/metrica/c$b$b;->j:I

    if-eqz v1, :cond_4

    .line 2734
    const/16 v1, 0x9

    iget v2, p0, Lcom/yandex/metrica/c$b$b;->j:I

    .line 2735
    invoke-static {v1, v2}, Lcom/yandex/metrica/impl/ob/b;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2737
    :cond_4
    iget v1, p0, Lcom/yandex/metrica/c$b$b;->k:I

    if-eqz v1, :cond_5

    .line 2738
    const/16 v1, 0xa

    iget v2, p0, Lcom/yandex/metrica/c$b$b;->k:I

    .line 2739
    invoke-static {v1, v2}, Lcom/yandex/metrica/impl/ob/b;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2741
    :cond_5
    iget v1, p0, Lcom/yandex/metrica/c$b$b;->l:I

    if-eqz v1, :cond_6

    .line 2742
    const/16 v1, 0xb

    iget v2, p0, Lcom/yandex/metrica/c$b$b;->l:I

    .line 2743
    invoke-static {v1, v2}, Lcom/yandex/metrica/impl/ob/b;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2745
    :cond_6
    return v0
.end method

.method public e()Lcom/yandex/metrica/c$b$b;
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 2660
    iput-wide v2, p0, Lcom/yandex/metrica/c$b$b;->b:J

    .line 2661
    iput-wide v2, p0, Lcom/yandex/metrica/c$b$b;->c:J

    .line 2662
    iput-wide v2, p0, Lcom/yandex/metrica/c$b$b;->d:J

    .line 2663
    iput-wide v4, p0, Lcom/yandex/metrica/c$b$b;->e:D

    .line 2664
    iput-wide v4, p0, Lcom/yandex/metrica/c$b$b;->f:D

    .line 2665
    iput v0, p0, Lcom/yandex/metrica/c$b$b;->g:I

    .line 2666
    iput v0, p0, Lcom/yandex/metrica/c$b$b;->h:I

    .line 2667
    iput v0, p0, Lcom/yandex/metrica/c$b$b;->i:I

    .line 2668
    iput v0, p0, Lcom/yandex/metrica/c$b$b;->j:I

    .line 2669
    iput v0, p0, Lcom/yandex/metrica/c$b$b;->k:I

    .line 2670
    iput v0, p0, Lcom/yandex/metrica/c$b$b;->l:I

    .line 2671
    const/4 v0, -0x1

    iput v0, p0, Lcom/yandex/metrica/c$b$b;->a:I

    .line 2672
    return-object p0
.end method
