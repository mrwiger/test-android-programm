.class public abstract Lcom/my/target/common/BaseAd;
.super Ljava/lang/Object;
.source "BaseAd.java"


# instance fields
.field protected final adConfig:Lcom/my/target/b;


# direct methods
.method protected constructor <init>(ILjava/lang/String;)V
    .locals 1
    .param p1, "slotId"    # I
    .param p2, "format"    # Ljava/lang/String;

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    invoke-static {p1, p2}, Lcom/my/target/b;->newConfig(ILjava/lang/String;)Lcom/my/target/b;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/common/BaseAd;->adConfig:Lcom/my/target/b;

    .line 56
    return-void
.end method

.method public static setDebugMode(Z)V
    .locals 1
    .param p0, "debugMode"    # Z

    .prologue
    .line 19
    sput-boolean p0, Lcom/my/target/g;->enabled:Z

    .line 20
    if-eqz p0, :cond_0

    .line 22
    const-string v0, "Debug mode enabled"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 24
    :cond_0
    return-void
.end method


# virtual methods
.method public getCustomParams()Lcom/my/target/common/CustomParams;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/my/target/common/BaseAd;->adConfig:Lcom/my/target/b;

    invoke-virtual {v0}, Lcom/my/target/b;->getCustomParams()Lcom/my/target/common/CustomParams;

    move-result-object v0

    return-object v0
.end method

.method public isTrackingEnvironmentEnabled()Z
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/my/target/common/BaseAd;->adConfig:Lcom/my/target/b;

    invoke-virtual {v0}, Lcom/my/target/b;->isTrackingEnvironmentEnabled()Z

    move-result v0

    return v0
.end method

.method public isTrackingLocationEnabled()Z
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/my/target/common/BaseAd;->adConfig:Lcom/my/target/b;

    invoke-virtual {v0}, Lcom/my/target/b;->isTrackingLocationEnabled()Z

    move-result v0

    return v0
.end method

.method public setTrackingEnvironmentEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 30
    iget-object v0, p0, Lcom/my/target/common/BaseAd;->adConfig:Lcom/my/target/b;

    invoke-virtual {v0, p1}, Lcom/my/target/b;->setTrackingEnvironmentEnabled(Z)V

    .line 31
    return-void
.end method

.method public setTrackingLocationEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 40
    iget-object v0, p0, Lcom/my/target/common/BaseAd;->adConfig:Lcom/my/target/b;

    invoke-virtual {v0, p1}, Lcom/my/target/b;->setTrackingLocationEnabled(Z)V

    .line 41
    return-void
.end method
