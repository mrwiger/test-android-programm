.class public Lru/cn/tv/mobile/linkdevice/LinkDeviceFragment;
.super Lru/cn/tv/WebviewFragment;
.source "LinkDeviceFragment.java"


# instance fields
.field private viewModel:Lru/cn/tv/mobile/linkdevice/LinkDeviceViewModel;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lru/cn/tv/WebviewFragment;-><init>()V

    return-void
.end method

.method private load(Landroid/net/Uri;)V
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 30
    invoke-virtual {p0}, Lru/cn/tv/mobile/linkdevice/LinkDeviceFragment;->isAdded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 38
    :goto_0
    return-void

    .line 33
    :cond_0
    if-nez p1, :cond_1

    .line 34
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lru/cn/tv/mobile/linkdevice/LinkDeviceFragment;->sendResult(I)V

    goto :goto_0

    .line 36
    :cond_1
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lru/cn/tv/mobile/linkdevice/LinkDeviceFragment;->load(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private sendResult(I)V
    .locals 1
    .param p1, "resultCode"    # I

    .prologue
    .line 41
    invoke-virtual {p0}, Lru/cn/tv/mobile/linkdevice/LinkDeviceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 42
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    .line 43
    invoke-virtual {v0, p1}, Landroid/app/Activity;->setResult(I)V

    .line 44
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 46
    :cond_0
    return-void
.end method


# virtual methods
.method final bridge synthetic bridge$lambda$0$LinkDeviceFragment(Landroid/net/Uri;)V
    .locals 0

    invoke-direct {p0, p1}, Lru/cn/tv/mobile/linkdevice/LinkDeviceFragment;->load(Landroid/net/Uri;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 21
    invoke-super {p0, p1}, Lru/cn/tv/WebviewFragment;->onCreate(Landroid/os/Bundle;)V

    .line 23
    const-string v0, "ptv29783051://callback"

    invoke-virtual {p0, v0}, Lru/cn/tv/mobile/linkdevice/LinkDeviceFragment;->addHandledUrl(Ljava/lang/String;)V

    .line 25
    invoke-static {}, Lru/cn/tv/App;->scope()Ltoothpick/Scope;

    move-result-object v0

    const-class v1, Lru/cn/tv/mobile/linkdevice/LinkDeviceViewModel;

    invoke-static {p0, v0, v1}, Lru/cn/mvvm/ViewModels;->get(Landroid/support/v4/app/Fragment;Ltoothpick/Scope;Ljava/lang/Class;)Landroid/arch/lifecycle/ViewModel;

    move-result-object v0

    check-cast v0, Lru/cn/tv/mobile/linkdevice/LinkDeviceViewModel;

    iput-object v0, p0, Lru/cn/tv/mobile/linkdevice/LinkDeviceFragment;->viewModel:Lru/cn/tv/mobile/linkdevice/LinkDeviceViewModel;

    .line 26
    iget-object v0, p0, Lru/cn/tv/mobile/linkdevice/LinkDeviceFragment;->viewModel:Lru/cn/tv/mobile/linkdevice/LinkDeviceViewModel;

    invoke-virtual {v0}, Lru/cn/tv/mobile/linkdevice/LinkDeviceViewModel;->linkUri()Landroid/arch/lifecycle/LiveData;

    move-result-object v0

    new-instance v1, Lru/cn/tv/mobile/linkdevice/LinkDeviceFragment$$Lambda$0;

    invoke-direct {v1, p0}, Lru/cn/tv/mobile/linkdevice/LinkDeviceFragment$$Lambda$0;-><init>(Lru/cn/tv/mobile/linkdevice/LinkDeviceFragment;)V

    invoke-virtual {v0, p0, v1}, Landroid/arch/lifecycle/LiveData;->observe(Landroid/arch/lifecycle/LifecycleOwner;Landroid/arch/lifecycle/Observer;)V

    .line 27
    return-void
.end method
