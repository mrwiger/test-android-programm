.class Lru/cn/tv/mobile/channels/ChannelsAdapter$ViewHolder;
.super Ljava/lang/Object;
.source "ChannelsAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/tv/mobile/channels/ChannelsAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ViewHolder"
.end annotation


# instance fields
.field public accessIndicator:Landroid/view/View;

.field public archiveIndicator:Landroid/view/View;

.field public currentTelecast:Landroid/widget/TextView;

.field public currentTelecastProgressIndicator:Landroid/view/View;

.field public currentTelecastProgressWrapper:Landroid/view/View;

.field public favouriteIndicator:Lru/cn/tv/FavouriteStar;

.field public image:Landroid/widget/ImageView;

.field public telecastLoader:Lru/cn/tv/mobile/channels/CurrentTelecastLoader;

.field public title:Landroid/widget/TextView;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 131
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lru/cn/tv/mobile/channels/ChannelsAdapter$1;)V
    .locals 0
    .param p1, "x0"    # Lru/cn/tv/mobile/channels/ChannelsAdapter$1;

    .prologue
    .line 131
    invoke-direct {p0}, Lru/cn/tv/mobile/channels/ChannelsAdapter$ViewHolder;-><init>()V

    return-void
.end method
