.class public Lcom/yandex/metrica/impl/ob/fh;
.super Lcom/yandex/metrica/impl/ob/ff;
.source "SourceFile"


# static fields
.field public static final c:Lcom/yandex/metrica/impl/ob/fm;

.field public static final d:Lcom/yandex/metrica/impl/ob/fm;

.field private static final e:Lcom/yandex/metrica/impl/ob/fm;

.field private static final f:Lcom/yandex/metrica/impl/ob/fm;

.field private static final g:Lcom/yandex/metrica/impl/ob/fm;

.field private static final h:Lcom/yandex/metrica/impl/ob/fm;

.field private static final i:Lcom/yandex/metrica/impl/ob/fm;

.field private static final j:Lcom/yandex/metrica/impl/ob/fm;

.field private static final k:Lcom/yandex/metrica/impl/ob/fm;

.field private static final l:Lcom/yandex/metrica/impl/ob/fm;

.field private static final m:Lcom/yandex/metrica/impl/ob/fm;

.field private static final n:Lcom/yandex/metrica/impl/ob/fm;

.field private static final o:Lcom/yandex/metrica/impl/ob/fm;

.field private static final p:Lcom/yandex/metrica/impl/ob/fm;

.field private static final q:Lcom/yandex/metrica/impl/ob/fm;

.field private static final r:Lcom/yandex/metrica/impl/ob/fm;

.field private static final s:Lcom/yandex/metrica/impl/ob/fm;

.field private static final t:Lcom/yandex/metrica/impl/ob/fm;


# instance fields
.field private A:Lcom/yandex/metrica/impl/ob/fm;

.field private B:Lcom/yandex/metrica/impl/ob/fm;

.field private C:Lcom/yandex/metrica/impl/ob/fm;

.field private D:Lcom/yandex/metrica/impl/ob/fm;

.field private E:Lcom/yandex/metrica/impl/ob/fm;

.field private F:Lcom/yandex/metrica/impl/ob/fm;

.field private G:Lcom/yandex/metrica/impl/ob/fm;

.field private H:Lcom/yandex/metrica/impl/ob/fm;

.field private I:Lcom/yandex/metrica/impl/ob/fm;

.field private J:Lcom/yandex/metrica/impl/ob/fm;

.field private u:Lcom/yandex/metrica/impl/ob/fm;

.field private v:Lcom/yandex/metrica/impl/ob/fm;

.field private w:Lcom/yandex/metrica/impl/ob/fm;

.field private x:Lcom/yandex/metrica/impl/ob/fm;

.field private y:Lcom/yandex/metrica/impl/ob/fm;

.field private z:Lcom/yandex/metrica/impl/ob/fm;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 31
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "SESSION_SLEEP_START_"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/fh;->e:Lcom/yandex/metrica/impl/ob/fm;

    .line 32
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "SESSION_ID_"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/fh;->f:Lcom/yandex/metrica/impl/ob/fm;

    .line 33
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "SESSION_COUNTER_ID_"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/fh;->g:Lcom/yandex/metrica/impl/ob/fm;

    .line 34
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "SESSION_INIT_TIME_"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/fh;->h:Lcom/yandex/metrica/impl/ob/fm;

    .line 35
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "SESSION_ALIVE_TIME_"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/fh;->i:Lcom/yandex/metrica/impl/ob/fm;

    .line 36
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "SESSION_IS_ALIVE_REPORT_NEEDED_"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/fh;->j:Lcom/yandex/metrica/impl/ob/fm;

    .line 40
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "BG_SESSION_ID_"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/fh;->k:Lcom/yandex/metrica/impl/ob/fm;

    .line 41
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "BG_SESSION_SLEEP_START_"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/fh;->l:Lcom/yandex/metrica/impl/ob/fm;

    .line 42
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "BG_SESSION_COUNTER_ID_"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/fh;->m:Lcom/yandex/metrica/impl/ob/fm;

    .line 43
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "BG_SESSION_INIT_TIME_"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/fh;->n:Lcom/yandex/metrica/impl/ob/fm;

    .line 46
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "COLLECT_INSTALLED_APPS_"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/fh;->o:Lcom/yandex/metrica/impl/ob/fm;

    .line 47
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "IDENTITY_SEND_TIME_"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/fh;->p:Lcom/yandex/metrica/impl/ob/fm;

    .line 48
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "USER_INFO_"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/fh;->q:Lcom/yandex/metrica/impl/ob/fm;

    .line 49
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "REFERRER_"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/fh;->r:Lcom/yandex/metrica/impl/ob/fm;

    .line 52
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "APP_ENVIRONMENT"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/fh;->c:Lcom/yandex/metrica/impl/ob/fm;

    .line 54
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "APP_ENVIRONMENT_REVISION"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/fh;->d:Lcom/yandex/metrica/impl/ob/fm;

    .line 55
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "APP_ENVIRONMENT_"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/fh;->s:Lcom/yandex/metrica/impl/ob/fm;

    .line 56
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "APP_ENVIRONMENT_REVISION_"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/fh;->t:Lcom/yandex/metrica/impl/ob/fm;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 82
    invoke-direct {p0, p1, p2}, Lcom/yandex/metrica/impl/ob/ff;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 85
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    sget-object v1, Lcom/yandex/metrica/impl/ob/fh;->e:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/fh;->i()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/fh;->u:Lcom/yandex/metrica/impl/ob/fm;

    .line 86
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    sget-object v1, Lcom/yandex/metrica/impl/ob/fh;->f:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/fh;->i()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/fh;->v:Lcom/yandex/metrica/impl/ob/fm;

    .line 87
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    sget-object v1, Lcom/yandex/metrica/impl/ob/fh;->g:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/fh;->i()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/fh;->w:Lcom/yandex/metrica/impl/ob/fm;

    .line 88
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    sget-object v1, Lcom/yandex/metrica/impl/ob/fh;->h:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/fh;->i()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/fh;->x:Lcom/yandex/metrica/impl/ob/fm;

    .line 89
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    sget-object v1, Lcom/yandex/metrica/impl/ob/fh;->i:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/fh;->i()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/fh;->y:Lcom/yandex/metrica/impl/ob/fm;

    .line 90
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    sget-object v1, Lcom/yandex/metrica/impl/ob/fh;->j:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/fh;->i()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/fh;->z:Lcom/yandex/metrica/impl/ob/fm;

    .line 94
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    sget-object v1, Lcom/yandex/metrica/impl/ob/fh;->k:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/fh;->i()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/fh;->A:Lcom/yandex/metrica/impl/ob/fm;

    .line 95
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    sget-object v1, Lcom/yandex/metrica/impl/ob/fh;->l:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/fh;->i()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/fh;->B:Lcom/yandex/metrica/impl/ob/fm;

    .line 96
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    sget-object v1, Lcom/yandex/metrica/impl/ob/fh;->m:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/fh;->i()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/fh;->C:Lcom/yandex/metrica/impl/ob/fm;

    .line 97
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    sget-object v1, Lcom/yandex/metrica/impl/ob/fh;->n:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/fh;->i()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/fh;->D:Lcom/yandex/metrica/impl/ob/fm;

    .line 100
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    sget-object v1, Lcom/yandex/metrica/impl/ob/fh;->p:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/fh;->i()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/fh;->E:Lcom/yandex/metrica/impl/ob/fm;

    .line 101
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    sget-object v1, Lcom/yandex/metrica/impl/ob/fh;->o:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/fh;->i()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/fh;->F:Lcom/yandex/metrica/impl/ob/fm;

    .line 102
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    sget-object v1, Lcom/yandex/metrica/impl/ob/fh;->q:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/fh;->i()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/fh;->G:Lcom/yandex/metrica/impl/ob/fm;

    .line 103
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    sget-object v1, Lcom/yandex/metrica/impl/ob/fh;->r:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/fh;->i()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/fh;->H:Lcom/yandex/metrica/impl/ob/fm;

    .line 105
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    sget-object v1, Lcom/yandex/metrica/impl/ob/fh;->s:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/fh;->i()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/fh;->I:Lcom/yandex/metrica/impl/ob/fm;

    .line 106
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    sget-object v1, Lcom/yandex/metrica/impl/ob/fh;->t:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/fh;->i()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/fh;->J:Lcom/yandex/metrica/impl/ob/fm;

    .line 1275
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fh;->b:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fh;->F:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    .line 2072
    if-eqz v0, :cond_0

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2075
    const/4 v2, 0x0

    :try_start_0
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    .line 2076
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    sget-object v2, Lcom/yandex/metrica/CounterConfiguration$a;->a:Lcom/yandex/metrica/CounterConfiguration$a;

    iget v2, v2, Lcom/yandex/metrica/CounterConfiguration$a;->d:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2263
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fh;->b:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fh;->y:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    const/4 v2, -0x1

    invoke-static {v0, v1, v2}, Lcom/yandex/metrica/impl/ob/fn;->a(Landroid/content/SharedPreferences;Ljava/lang/String;I)V

    .line 2267
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fh;->b:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fh;->u:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Lcom/yandex/metrica/impl/ob/fn;->a(Landroid/content/SharedPreferences;Ljava/lang/String;I)V

    .line 2271
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fh;->b:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fh;->w:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Lcom/yandex/metrica/impl/ob/fn;->a(Landroid/content/SharedPreferences;Ljava/lang/String;I)V

    .line 112
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;J)J
    .locals 2

    .prologue
    .line 148
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fh;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0, p1, p2, p3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public a(J)J
    .locals 3

    .prologue
    .line 120
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fh;->x:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p1, p2}, Lcom/yandex/metrica/impl/ob/fh;->a(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public a()Lcom/yandex/metrica/impl/a$a;
    .locals 6

    .prologue
    .line 157
    monitor-enter p0

    .line 158
    :try_start_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fh;->b:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fh;->I:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fh;->b:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fh;->J:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 159
    new-instance v0, Lcom/yandex/metrica/impl/a$a;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fh;->b:Landroid/content/SharedPreferences;

    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/fh;->I:Lcom/yandex/metrica/impl/ob/fm;

    .line 160
    invoke-virtual {v2}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "{}"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/fh;->b:Landroid/content/SharedPreferences;

    iget-object v3, p0, Lcom/yandex/metrica/impl/ob/fh;->J:Lcom/yandex/metrica/impl/ob/fm;

    .line 161
    invoke-virtual {v3}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v3

    const-wide/16 v4, 0x0

    invoke-interface {v2, v3, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-direct {v0, v1, v2, v3}, Lcom/yandex/metrica/impl/a$a;-><init>(Ljava/lang/String;J)V

    monitor-exit p0

    .line 164
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    monitor-exit p0

    goto :goto_0

    .line 165
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lcom/yandex/metrica/impl/a$a;)Lcom/yandex/metrica/impl/ob/fh;
    .locals 4

    .prologue
    .line 203
    monitor-enter p0

    .line 204
    :try_start_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fh;->I:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, Lcom/yandex/metrica/impl/a$a;->a:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/yandex/metrica/impl/ob/fh;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/yandex/metrica/impl/ob/ff;

    .line 205
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fh;->J:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    iget-wide v2, p1, Lcom/yandex/metrica/impl/a$a;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/yandex/metrica/impl/ob/fh;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/yandex/metrica/impl/ob/ff;

    .line 206
    monitor-exit p0

    .line 207
    return-object p0

    .line 206
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Z)Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 177
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fh;->b:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fh;->z:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 187
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fh;->b:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fh;->G:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(J)J
    .locals 3

    .prologue
    .line 124
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fh;->D:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p1, p2}, Lcom/yandex/metrica/impl/ob/fh;->a(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public b()Lcom/yandex/metrica/CounterConfiguration$a;
    .locals 3

    .prologue
    .line 181
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fh;->b:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fh;->F:Lcom/yandex/metrica/impl/ob/fm;

    .line 182
    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/yandex/metrica/CounterConfiguration$a;->a:Lcom/yandex/metrica/CounterConfiguration$a;

    iget v2, v2, Lcom/yandex/metrica/CounterConfiguration$a;->d:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 181
    invoke-static {v0}, Lcom/yandex/metrica/CounterConfiguration$a;->a(I)Lcom/yandex/metrica/CounterConfiguration$a;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 191
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fh;->b:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fh;->H:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c(J)J
    .locals 3

    .prologue
    .line 132
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fh;->E:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p1, p2}, Lcom/yandex/metrica/impl/ob/fh;->a(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public c()Lcom/yandex/metrica/impl/ob/fh;
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fh;->H:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/ob/fh;->h(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/ff;

    move-result-object v0

    check-cast v0, Lcom/yandex/metrica/impl/ob/fh;

    return-object v0
.end method

.method public d(J)J
    .locals 3

    .prologue
    .line 136
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fh;->v:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p1, p2}, Lcom/yandex/metrica/impl/ob/fh;->a(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public d()Lcom/yandex/metrica/impl/ob/fh;
    .locals 1

    .prologue
    .line 279
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fh;->F:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/ob/fh;->h(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/ff;

    move-result-object v0

    check-cast v0, Lcom/yandex/metrica/impl/ob/fh;

    return-object v0
.end method

.method public e(J)J
    .locals 3

    .prologue
    .line 140
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fh;->A:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p1, p2}, Lcom/yandex/metrica/impl/ob/fh;->a(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public e()Z
    .locals 2

    .prologue
    .line 287
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fh;->b:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fh;->x:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fh;->b:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fh;->y:Lcom/yandex/metrica/impl/ob/fm;

    .line 288
    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fh;->b:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fh;->z:Lcom/yandex/metrica/impl/ob/fm;

    .line 289
    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fh;->b:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fh;->u:Lcom/yandex/metrica/impl/ob/fm;

    .line 290
    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fh;->b:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fh;->v:Lcom/yandex/metrica/impl/ob/fm;

    .line 291
    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fh;->b:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fh;->w:Lcom/yandex/metrica/impl/ob/fm;

    .line 292
    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fh;->b:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fh;->D:Lcom/yandex/metrica/impl/ob/fm;

    .line 293
    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fh;->b:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fh;->B:Lcom/yandex/metrica/impl/ob/fm;

    .line 294
    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fh;->b:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fh;->A:Lcom/yandex/metrica/impl/ob/fm;

    .line 295
    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fh;->b:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fh;->C:Lcom/yandex/metrica/impl/ob/fm;

    .line 296
    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fh;->b:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fh;->I:Lcom/yandex/metrica/impl/ob/fm;

    .line 297
    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fh;->b:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fh;->F:Lcom/yandex/metrica/impl/ob/fm;

    .line 298
    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fh;->b:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fh;->G:Lcom/yandex/metrica/impl/ob/fm;

    .line 299
    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fh;->b:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fh;->H:Lcom/yandex/metrica/impl/ob/fm;

    .line 300
    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fh;->b:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fh;->E:Lcom/yandex/metrica/impl/ob/fm;

    .line 301
    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    .line 287
    goto :goto_0
.end method

.method public f(J)J
    .locals 3

    .prologue
    .line 144
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fh;->w:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p1, p2}, Lcom/yandex/metrica/impl/ob/fh;->a(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method protected f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 116
    const-string v0, "_boundentrypreferences"

    return-object v0
.end method

.method public g(J)J
    .locals 3

    .prologue
    .line 152
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fh;->C:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p1, p2}, Lcom/yandex/metrica/impl/ob/fh;->a(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public g()V
    .locals 2

    .prologue
    .line 305
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fh;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fh;->D:Lcom/yandex/metrica/impl/ob/fm;

    .line 306
    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fh;->C:Lcom/yandex/metrica/impl/ob/fm;

    .line 307
    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fh;->A:Lcom/yandex/metrica/impl/ob/fm;

    .line 308
    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fh;->B:Lcom/yandex/metrica/impl/ob/fm;

    .line 309
    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fh;->x:Lcom/yandex/metrica/impl/ob/fm;

    .line 310
    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fh;->w:Lcom/yandex/metrica/impl/ob/fm;

    .line 311
    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fh;->v:Lcom/yandex/metrica/impl/ob/fm;

    .line 312
    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fh;->u:Lcom/yandex/metrica/impl/ob/fm;

    .line 313
    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fh;->z:Lcom/yandex/metrica/impl/ob/fm;

    .line 314
    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fh;->y:Lcom/yandex/metrica/impl/ob/fm;

    .line 315
    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fh;->G:Lcom/yandex/metrica/impl/ob/fm;

    .line 316
    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fh;->F:Lcom/yandex/metrica/impl/ob/fm;

    .line 317
    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fh;->I:Lcom/yandex/metrica/impl/ob/fm;

    .line 318
    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fh;->J:Lcom/yandex/metrica/impl/ob/fm;

    .line 319
    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fh;->H:Lcom/yandex/metrica/impl/ob/fm;

    .line 320
    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fh;->E:Lcom/yandex/metrica/impl/ob/fm;

    .line 321
    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 322
    return-void
.end method

.method public h(J)J
    .locals 3

    .prologue
    .line 169
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fh;->u:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p1, p2}, Lcom/yandex/metrica/impl/ob/fh;->a(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public i(J)J
    .locals 3

    .prologue
    .line 173
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fh;->B:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p1, p2}, Lcom/yandex/metrica/impl/ob/fh;->a(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method
