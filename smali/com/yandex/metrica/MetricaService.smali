.class public Lcom/yandex/metrica/MetricaService;
.super Landroid/app/Service;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/yandex/metrica/MetricaService$d;,
        Lcom/yandex/metrica/MetricaService$b;,
        Lcom/yandex/metrica/MetricaService$c;,
        Lcom/yandex/metrica/MetricaService$a;
    }
.end annotation


# instance fields
.field private a:Lcom/yandex/metrica/MetricaService$d;

.field private b:Lcom/yandex/metrica/impl/ae;

.field private final c:Lcom/yandex/metrica/IMetricaService$Stub;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 55
    new-instance v0, Lcom/yandex/metrica/MetricaService$1;

    invoke-direct {v0, p0}, Lcom/yandex/metrica/MetricaService$1;-><init>(Lcom/yandex/metrica/MetricaService;)V

    iput-object v0, p0, Lcom/yandex/metrica/MetricaService;->a:Lcom/yandex/metrica/MetricaService$d;

    .line 136
    new-instance v0, Lcom/yandex/metrica/MetricaService$2;

    invoke-direct {v0, p0}, Lcom/yandex/metrica/MetricaService$2;-><init>(Lcom/yandex/metrica/MetricaService;)V

    iput-object v0, p0, Lcom/yandex/metrica/MetricaService;->c:Lcom/yandex/metrica/IMetricaService$Stub;

    return-void
.end method

.method static synthetic a(Lcom/yandex/metrica/MetricaService;)Lcom/yandex/metrica/impl/ae;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/yandex/metrica/MetricaService;->b:Lcom/yandex/metrica/impl/ae;

    return-object v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 87
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 89
    const-string v1, "com.yandex.metrica.ACTION_BIND_TO_LOCAL_SERVER"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 90
    new-instance v0, Lcom/yandex/metrica/MetricaService$c;

    invoke-direct {v0}, Lcom/yandex/metrica/MetricaService$c;-><init>()V

    .line 97
    :goto_0
    iget-object v1, p0, Lcom/yandex/metrica/MetricaService;->b:Lcom/yandex/metrica/impl/ae;

    invoke-interface {v1, p1}, Lcom/yandex/metrica/impl/ae;->a(Landroid/content/Intent;)V

    .line 99
    return-object v0

    .line 91
    :cond_0
    const-string v1, "com.yandex.metrica.ACTION_C_BG_L"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 92
    new-instance v0, Lcom/yandex/metrica/MetricaService$b;

    invoke-direct {v0}, Lcom/yandex/metrica/MetricaService$b;-><init>()V

    goto :goto_0

    .line 95
    :cond_1
    iget-object v0, p0, Lcom/yandex/metrica/MetricaService;->c:Lcom/yandex/metrica/IMetricaService$Stub;

    goto :goto_0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 65
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 66
    invoke-virtual {p0}, Lcom/yandex/metrica/MetricaService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/yandex/metrica/impl/utils/l;->a(Landroid/content/Context;)V

    .line 68
    new-instance v0, Lcom/yandex/metrica/impl/ag;

    invoke-virtual {p0}, Lcom/yandex/metrica/MetricaService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/yandex/metrica/MetricaService;->a:Lcom/yandex/metrica/MetricaService$d;

    invoke-direct {v0, v1, v2}, Lcom/yandex/metrica/impl/ag;-><init>(Landroid/content/Context;Lcom/yandex/metrica/MetricaService$d;)V

    .line 69
    new-instance v1, Lcom/yandex/metrica/impl/af;

    invoke-direct {v1, v0}, Lcom/yandex/metrica/impl/af;-><init>(Lcom/yandex/metrica/impl/ae;)V

    iput-object v1, p0, Lcom/yandex/metrica/MetricaService;->b:Lcom/yandex/metrica/impl/ae;

    .line 70
    iget-object v0, p0, Lcom/yandex/metrica/MetricaService;->b:Lcom/yandex/metrica/impl/ae;

    invoke-interface {v0}, Lcom/yandex/metrica/impl/ae;->a()V

    .line 71
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 111
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 113
    iget-object v0, p0, Lcom/yandex/metrica/MetricaService;->b:Lcom/yandex/metrica/impl/ae;

    invoke-interface {v0}, Lcom/yandex/metrica/impl/ae;->b()V

    .line 114
    return-void
.end method

.method public onRebind(Landroid/content/Intent;)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 104
    invoke-super {p0, p1}, Landroid/app/Service;->onRebind(Landroid/content/Intent;)V

    .line 106
    iget-object v0, p0, Lcom/yandex/metrica/MetricaService;->b:Lcom/yandex/metrica/impl/ae;

    invoke-interface {v0, p1}, Lcom/yandex/metrica/impl/ae;->b(Landroid/content/Intent;)V

    .line 107
    return-void
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "startId"    # I

    .prologue
    .line 75
    iget-object v0, p0, Lcom/yandex/metrica/MetricaService;->b:Lcom/yandex/metrica/impl/ae;

    invoke-interface {v0, p1, p2}, Lcom/yandex/metrica/impl/ae;->a(Landroid/content/Intent;I)V

    .line 76
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 80
    iget-object v0, p0, Lcom/yandex/metrica/MetricaService;->b:Lcom/yandex/metrica/impl/ae;

    invoke-interface {v0, p1, p2, p3}, Lcom/yandex/metrica/impl/ae;->a(Landroid/content/Intent;II)V

    .line 81
    const/4 v0, 0x2

    return v0
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 119
    iget-object v2, p0, Lcom/yandex/metrica/MetricaService;->b:Lcom/yandex/metrica/impl/ae;

    invoke-interface {v2, p1}, Lcom/yandex/metrica/impl/ae;->c(Landroid/content/Intent;)V

    .line 120
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    .line 121
    const-string v3, "com.yandex.metrica.ACTION_BIND_TO_LOCAL_SERVER"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 131
    :cond_0
    :goto_0
    return v0

    .line 123
    :cond_1
    const-string v3, "com.yandex.metrica.ACTION_C_BG_L"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v0, v1

    .line 124
    goto :goto_0

    .line 1151
    :cond_2
    if-eqz p1, :cond_3

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    if-nez v2, :cond_4

    :cond_3
    move v2, v1

    .line 126
    :goto_1
    if-nez v2, :cond_0

    move v0, v1

    .line 131
    goto :goto_0

    :cond_4
    move v2, v0

    .line 1151
    goto :goto_1
.end method
