.class public final Lcom/yandex/metrica/c$c$b;
.super Lcom/yandex/metrica/impl/ob/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/yandex/metrica/c$c;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation


# instance fields
.field public b:D

.field public c:D

.field public d:J

.field public e:I

.field public f:I

.field public g:I

.field public h:I

.field public i:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 200
    invoke-direct {p0}, Lcom/yandex/metrica/impl/ob/d;-><init>()V

    .line 201
    invoke-virtual {p0}, Lcom/yandex/metrica/c$c$b;->d()Lcom/yandex/metrica/c$c$b;

    .line 202
    return-void
.end method


# virtual methods
.method public a(Lcom/yandex/metrica/impl/ob/b;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 220
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/yandex/metrica/c$c$b;->b:D

    invoke-virtual {p1, v0, v2, v3}, Lcom/yandex/metrica/impl/ob/b;->a(ID)V

    .line 221
    const/4 v0, 0x2

    iget-wide v2, p0, Lcom/yandex/metrica/c$c$b;->c:D

    invoke-virtual {p1, v0, v2, v3}, Lcom/yandex/metrica/impl/ob/b;->a(ID)V

    .line 222
    iget-wide v0, p0, Lcom/yandex/metrica/c$c$b;->d:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 223
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/yandex/metrica/c$c$b;->d:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/yandex/metrica/impl/ob/b;->a(IJ)V

    .line 225
    :cond_0
    iget v0, p0, Lcom/yandex/metrica/c$c$b;->e:I

    if-eqz v0, :cond_1

    .line 226
    const/4 v0, 0x4

    iget v1, p0, Lcom/yandex/metrica/c$c$b;->e:I

    invoke-virtual {p1, v0, v1}, Lcom/yandex/metrica/impl/ob/b;->b(II)V

    .line 228
    :cond_1
    iget v0, p0, Lcom/yandex/metrica/c$c$b;->f:I

    if-eqz v0, :cond_2

    .line 229
    const/4 v0, 0x5

    iget v1, p0, Lcom/yandex/metrica/c$c$b;->f:I

    invoke-virtual {p1, v0, v1}, Lcom/yandex/metrica/impl/ob/b;->b(II)V

    .line 231
    :cond_2
    iget v0, p0, Lcom/yandex/metrica/c$c$b;->g:I

    if-eqz v0, :cond_3

    .line 232
    const/4 v0, 0x6

    iget v1, p0, Lcom/yandex/metrica/c$c$b;->g:I

    invoke-virtual {p1, v0, v1}, Lcom/yandex/metrica/impl/ob/b;->b(II)V

    .line 234
    :cond_3
    iget v0, p0, Lcom/yandex/metrica/c$c$b;->h:I

    if-eqz v0, :cond_4

    .line 235
    const/4 v0, 0x7

    iget v1, p0, Lcom/yandex/metrica/c$c$b;->h:I

    invoke-virtual {p1, v0, v1}, Lcom/yandex/metrica/impl/ob/b;->a(II)V

    .line 237
    :cond_4
    iget v0, p0, Lcom/yandex/metrica/c$c$b;->i:I

    if-eqz v0, :cond_5

    .line 238
    const/16 v0, 0x8

    iget v1, p0, Lcom/yandex/metrica/c$c$b;->i:I

    invoke-virtual {p1, v0, v1}, Lcom/yandex/metrica/impl/ob/b;->a(II)V

    .line 240
    :cond_5
    invoke-super {p0, p1}, Lcom/yandex/metrica/impl/ob/d;->a(Lcom/yandex/metrica/impl/ob/b;)V

    .line 241
    return-void
.end method

.method protected c()I
    .locals 6

    .prologue
    .line 245
    invoke-super {p0}, Lcom/yandex/metrica/impl/ob/d;->c()I

    move-result v0

    .line 246
    const/4 v1, 0x1

    .line 247
    invoke-static {v1}, Lcom/yandex/metrica/impl/ob/b;->d(I)I

    move-result v1

    add-int/2addr v0, v1

    .line 248
    const/4 v1, 0x2

    .line 249
    invoke-static {v1}, Lcom/yandex/metrica/impl/ob/b;->d(I)I

    move-result v1

    add-int/2addr v0, v1

    .line 250
    iget-wide v2, p0, Lcom/yandex/metrica/c$c$b;->d:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 251
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/yandex/metrica/c$c$b;->d:J

    .line 252
    invoke-static {v1, v2, v3}, Lcom/yandex/metrica/impl/ob/b;->c(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 254
    :cond_0
    iget v1, p0, Lcom/yandex/metrica/c$c$b;->e:I

    if-eqz v1, :cond_1

    .line 255
    const/4 v1, 0x4

    iget v2, p0, Lcom/yandex/metrica/c$c$b;->e:I

    .line 256
    invoke-static {v1, v2}, Lcom/yandex/metrica/impl/ob/b;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 258
    :cond_1
    iget v1, p0, Lcom/yandex/metrica/c$c$b;->f:I

    if-eqz v1, :cond_2

    .line 259
    const/4 v1, 0x5

    iget v2, p0, Lcom/yandex/metrica/c$c$b;->f:I

    .line 260
    invoke-static {v1, v2}, Lcom/yandex/metrica/impl/ob/b;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 262
    :cond_2
    iget v1, p0, Lcom/yandex/metrica/c$c$b;->g:I

    if-eqz v1, :cond_3

    .line 263
    const/4 v1, 0x6

    iget v2, p0, Lcom/yandex/metrica/c$c$b;->g:I

    .line 264
    invoke-static {v1, v2}, Lcom/yandex/metrica/impl/ob/b;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 266
    :cond_3
    iget v1, p0, Lcom/yandex/metrica/c$c$b;->h:I

    if-eqz v1, :cond_4

    .line 267
    const/4 v1, 0x7

    iget v2, p0, Lcom/yandex/metrica/c$c$b;->h:I

    .line 268
    invoke-static {v1, v2}, Lcom/yandex/metrica/impl/ob/b;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 270
    :cond_4
    iget v1, p0, Lcom/yandex/metrica/c$c$b;->i:I

    if-eqz v1, :cond_5

    .line 271
    const/16 v1, 0x8

    iget v2, p0, Lcom/yandex/metrica/c$c$b;->i:I

    .line 272
    invoke-static {v1, v2}, Lcom/yandex/metrica/impl/ob/b;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 274
    :cond_5
    return v0
.end method

.method public d()Lcom/yandex/metrica/c$c$b;
    .locals 3

    .prologue
    const-wide/16 v0, 0x0

    const/4 v2, 0x0

    .line 205
    iput-wide v0, p0, Lcom/yandex/metrica/c$c$b;->b:D

    .line 206
    iput-wide v0, p0, Lcom/yandex/metrica/c$c$b;->c:D

    .line 207
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/yandex/metrica/c$c$b;->d:J

    .line 208
    iput v2, p0, Lcom/yandex/metrica/c$c$b;->e:I

    .line 209
    iput v2, p0, Lcom/yandex/metrica/c$c$b;->f:I

    .line 210
    iput v2, p0, Lcom/yandex/metrica/c$c$b;->g:I

    .line 211
    iput v2, p0, Lcom/yandex/metrica/c$c$b;->h:I

    .line 212
    iput v2, p0, Lcom/yandex/metrica/c$c$b;->i:I

    .line 213
    const/4 v0, -0x1

    iput v0, p0, Lcom/yandex/metrica/c$c$b;->a:I

    .line 214
    return-object p0
.end method
