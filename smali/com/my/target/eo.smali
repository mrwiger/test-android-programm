.class public final Lcom/my/target/eo;
.super Lcom/my/target/ee;
.source "CarouselTabletView.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/eo$a;
    }
.end annotation


# static fields
.field private static final az:I

.field private static final bD:I

.field private static final bi:I

.field private static final cO:I

.field private static final cQ:I

.field private static final cS:I

.field private static final cW:I

.field static cX:I

.field static textSize:I


# instance fields
.field private final F:Lcom/my/target/by;

.field private final aX:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final aw:Lcom/my/target/cm;

.field private final bk:Lcom/my/target/bv;

.field private final bp:Lcom/my/target/by;

.field private final cT:Landroid/widget/TextView;

.field private final cY:Lcom/my/target/ev;

.field private final cZ:Landroid/widget/TextView;

.field private final ch:Landroid/widget/TextView;

.field private da:I

.field private db:F


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/16 v1, 0x12

    .line 39
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/eo;->cO:I

    .line 40
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/eo;->bi:I

    .line 41
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/eo;->az:I

    .line 42
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/eo;->bD:I

    .line 43
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/eo;->cQ:I

    .line 44
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/eo;->cS:I

    .line 45
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/eo;->cW:I

    .line 54
    sput v1, Lcom/my/target/eo;->textSize:I

    .line 55
    sput v1, Lcom/my/target/eo;->cX:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 13

    .prologue
    const/4 v12, 0x1

    const/4 v11, -0x1

    const/16 v10, 0x14

    const/4 v9, -0x2

    const/4 v8, 0x0

    .line 71
    invoke-direct {p0, p1, v8}, Lcom/my/target/ee;-><init>(Landroid/content/Context;I)V

    .line 66
    const/16 v0, 0x60

    iput v0, p0, Lcom/my/target/eo;->da:I

    .line 67
    const/high16 v0, 0x3fc00000    # 1.5f

    iput v0, p0, Lcom/my/target/eo;->db:F

    .line 72
    const v0, -0x3a1508

    invoke-static {p0, v11, v0}, Lcom/my/target/cm;->a(Landroid/view/View;II)V

    .line 74
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    .line 75
    const-string v0, "window"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 76
    if-eqz v0, :cond_0

    .line 78
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 80
    :cond_0
    iget v0, v1, Landroid/util/DisplayMetrics;->density:F

    .line 81
    iget v2, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 82
    iget v1, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 83
    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v3

    int-to-float v3, v3

    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    float-to-double v4, v3

    const-wide/high16 v6, 0x3ff8000000000000L    # 1.5

    cmpg-double v3, v4, v6

    if-gez v3, :cond_1

    .line 85
    const/high16 v3, 0x40200000    # 2.5f

    iput v3, p0, Lcom/my/target/eo;->db:F

    .line 91
    :goto_0
    int-to-float v2, v2

    div-float/2addr v2, v0

    .line 92
    int-to-float v1, v1

    div-float v0, v1, v0

    .line 93
    invoke-static {v2, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 95
    const/high16 v1, 0x3f800000    # 1.0f

    const/high16 v2, 0x44160000    # 600.0f

    sub-float/2addr v0, v2

    const/high16 v2, 0x3f800000    # 1.0f

    mul-float/2addr v0, v2

    const/high16 v2, 0x44160000    # 600.0f

    div-float/2addr v0, v2

    add-float/2addr v0, v1

    .line 97
    const/high16 v1, 0x41f00000    # 30.0f

    mul-float/2addr v1, v0

    float-to-int v1, v1

    .line 98
    const/high16 v2, 0x41900000    # 18.0f

    mul-float/2addr v2, v0

    float-to-int v2, v2

    sput v2, Lcom/my/target/eo;->textSize:I

    .line 99
    const/high16 v2, 0x42c00000    # 96.0f

    mul-float/2addr v2, v0

    float-to-int v2, v2

    iput v2, p0, Lcom/my/target/eo;->da:I

    .line 100
    const/high16 v2, 0x41900000    # 18.0f

    mul-float/2addr v0, v2

    float-to-int v0, v0

    sput v0, Lcom/my/target/eo;->cX:I

    .line 102
    new-instance v0, Lcom/my/target/by;

    invoke-direct {v0, p1}, Lcom/my/target/by;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/eo;->F:Lcom/my/target/by;

    .line 103
    invoke-static {p1}, Lcom/my/target/cm;->x(Landroid/content/Context;)Lcom/my/target/cm;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/eo;->aw:Lcom/my/target/cm;

    .line 104
    new-instance v0, Lcom/my/target/by;

    invoke-direct {v0, p1}, Lcom/my/target/by;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/eo;->bp:Lcom/my/target/by;

    .line 105
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/eo;->ch:Landroid/widget/TextView;

    .line 106
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/eo;->cT:Landroid/widget/TextView;

    .line 107
    new-instance v0, Lcom/my/target/bv;

    invoke-direct {v0, p1}, Lcom/my/target/bv;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/eo;->bk:Lcom/my/target/bv;

    .line 109
    new-instance v0, Landroid/widget/RelativeLayout;

    invoke-direct {v0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 110
    sget v2, Lcom/my/target/eo;->cQ:I

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setId(I)V

    .line 111
    iget-object v2, p0, Lcom/my/target/eo;->aw:Lcom/my/target/cm;

    invoke-virtual {v2, v10}, Lcom/my/target/cm;->n(I)I

    move-result v2

    iget-object v3, p0, Lcom/my/target/eo;->aw:Lcom/my/target/cm;

    .line 112
    invoke-virtual {v3, v8}, Lcom/my/target/cm;->n(I)I

    move-result v3

    iget-object v4, p0, Lcom/my/target/eo;->aw:Lcom/my/target/cm;

    .line 113
    invoke-virtual {v4, v10}, Lcom/my/target/cm;->n(I)I

    move-result v4

    iget-object v5, p0, Lcom/my/target/eo;->aw:Lcom/my/target/cm;

    .line 114
    invoke-virtual {v5, v8}, Lcom/my/target/cm;->n(I)I

    move-result v5

    .line 111
    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/widget/RelativeLayout;->setPadding(IIII)V

    .line 116
    iget-object v2, p0, Lcom/my/target/eo;->F:Lcom/my/target/by;

    sget v3, Lcom/my/target/eo;->cO:I

    invoke-virtual {v2, v3}, Lcom/my/target/by;->setId(I)V

    .line 117
    iget-object v2, p0, Lcom/my/target/eo;->F:Lcom/my/target/by;

    const-string v3, "close"

    invoke-virtual {v2, v3}, Lcom/my/target/by;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 118
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v9, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 120
    iget-object v3, p0, Lcom/my/target/eo;->aw:Lcom/my/target/cm;

    const/16 v4, 0xa

    invoke-virtual {v3, v4}, Lcom/my/target/cm;->n(I)I

    move-result v3

    iget-object v4, p0, Lcom/my/target/eo;->aw:Lcom/my/target/cm;

    const/16 v5, 0xa

    .line 121
    invoke-virtual {v4, v5}, Lcom/my/target/cm;->n(I)I

    move-result v4

    iget-object v5, p0, Lcom/my/target/eo;->aw:Lcom/my/target/cm;

    const/16 v6, 0xa

    .line 122
    invoke-virtual {v5, v6}, Lcom/my/target/cm;->n(I)I

    move-result v5

    iget-object v6, p0, Lcom/my/target/eo;->aw:Lcom/my/target/cm;

    const/16 v7, 0xa

    .line 123
    invoke-virtual {v6, v7}, Lcom/my/target/cm;->n(I)I

    move-result v6

    .line 120
    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 124
    const/16 v3, 0xb

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 125
    iget-object v3, p0, Lcom/my/target/eo;->F:Lcom/my/target/by;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Lcom/my/target/by;->setVisibility(I)V

    .line 126
    iget-object v3, p0, Lcom/my/target/eo;->F:Lcom/my/target/by;

    invoke-virtual {v3, v2}, Lcom/my/target/by;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 128
    new-instance v2, Landroid/widget/TextView;

    invoke-direct {v2, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/my/target/eo;->cZ:Landroid/widget/TextView;

    .line 129
    iget-object v2, p0, Lcom/my/target/eo;->cZ:Landroid/widget/TextView;

    sget v3, Lcom/my/target/eo;->bD:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setId(I)V

    .line 130
    iget-object v2, p0, Lcom/my/target/eo;->cZ:Landroid/widget/TextView;

    const/high16 v3, -0x1000000

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 131
    iget-object v2, p0, Lcom/my/target/eo;->cZ:Landroid/widget/TextView;

    sget v3, Lcom/my/target/eo;->textSize:I

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextSize(F)V

    .line 132
    iget-object v2, p0, Lcom/my/target/eo;->cZ:Landroid/widget/TextView;

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 133
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v9, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 134
    iget-object v3, p0, Lcom/my/target/eo;->aw:Lcom/my/target/cm;

    invoke-virtual {v3, v10}, Lcom/my/target/cm;->n(I)I

    move-result v3

    iget-object v4, p0, Lcom/my/target/eo;->aw:Lcom/my/target/cm;

    invoke-virtual {v4, v10}, Lcom/my/target/cm;->n(I)I

    move-result v4

    invoke-virtual {v2, v3, v8, v4, v8}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 135
    const/4 v3, 0x3

    sget v4, Lcom/my/target/eo;->cQ:I

    invoke-virtual {v2, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 136
    iget-object v3, p0, Lcom/my/target/eo;->cZ:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 138
    iget-object v2, p0, Lcom/my/target/eo;->bk:Lcom/my/target/bv;

    sget v3, Lcom/my/target/eo;->bi:I

    invoke-virtual {v2, v3}, Lcom/my/target/bv;->setId(I)V

    .line 139
    iget-object v2, p0, Lcom/my/target/eo;->bk:Lcom/my/target/bv;

    const-string v3, "icon"

    invoke-virtual {v2, v3}, Lcom/my/target/bv;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 141
    iget-object v2, p0, Lcom/my/target/eo;->ch:Landroid/widget/TextView;

    sget v3, Lcom/my/target/eo;->az:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setId(I)V

    .line 142
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v9, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 143
    iget-object v3, p0, Lcom/my/target/eo;->aw:Lcom/my/target/cm;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Lcom/my/target/cm;->n(I)I

    move-result v3

    iget-object v4, p0, Lcom/my/target/eo;->aw:Lcom/my/target/cm;

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Lcom/my/target/cm;->n(I)I

    move-result v4

    invoke-virtual {v2, v3, v8, v4, v8}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 144
    iget-object v3, p0, Lcom/my/target/eo;->ch:Landroid/widget/TextView;

    invoke-virtual {v3, v12}, Landroid/widget/TextView;->setLines(I)V

    .line 145
    iget-object v3, p0, Lcom/my/target/eo;->ch:Landroid/widget/TextView;

    sget-object v4, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 146
    iget-object v3, p0, Lcom/my/target/eo;->ch:Landroid/widget/TextView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4, v12}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 147
    sget v3, Lcom/my/target/eo;->bi:I

    invoke-virtual {v2, v12, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 148
    iget-object v3, p0, Lcom/my/target/eo;->ch:Landroid/widget/TextView;

    int-to-float v1, v1

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 149
    iget-object v1, p0, Lcom/my/target/eo;->ch:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 151
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v9, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 152
    sget v2, Lcom/my/target/eo;->bi:I

    invoke-virtual {v1, v12, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 153
    const/4 v2, 0x3

    sget v3, Lcom/my/target/eo;->az:I

    invoke-virtual {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 154
    iget-object v2, p0, Lcom/my/target/eo;->aw:Lcom/my/target/cm;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lcom/my/target/cm;->n(I)I

    move-result v2

    iget-object v3, p0, Lcom/my/target/eo;->aw:Lcom/my/target/cm;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Lcom/my/target/cm;->n(I)I

    move-result v3

    invoke-virtual {v1, v2, v8, v3, v8}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 155
    iget-object v2, p0, Lcom/my/target/eo;->cT:Landroid/widget/TextView;

    sget v3, Lcom/my/target/eo;->cS:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setId(I)V

    .line 156
    iget-object v2, p0, Lcom/my/target/eo;->cT:Landroid/widget/TextView;

    sget v3, Lcom/my/target/eo;->textSize:I

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextSize(F)V

    .line 157
    iget-object v2, p0, Lcom/my/target/eo;->cT:Landroid/widget/TextView;

    invoke-virtual {v2, v12}, Landroid/widget/TextView;->setLines(I)V

    .line 158
    iget-object v2, p0, Lcom/my/target/eo;->cT:Landroid/widget/TextView;

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 159
    iget-object v2, p0, Lcom/my/target/eo;->cT:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 161
    new-instance v1, Lcom/my/target/ev;

    invoke-direct {v1, p1}, Lcom/my/target/ev;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/my/target/eo;->cY:Lcom/my/target/ev;

    .line 163
    iget-object v1, p0, Lcom/my/target/eo;->cY:Lcom/my/target/ev;

    sget v2, Lcom/my/target/eo;->cW:I

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->setId(I)V

    .line 165
    iget-object v1, p0, Lcom/my/target/eo;->cY:Lcom/my/target/ev;

    iget-object v2, p0, Lcom/my/target/eo;->aw:Lcom/my/target/cm;

    const/16 v3, 0x10

    invoke-virtual {v2, v3}, Lcom/my/target/cm;->n(I)I

    move-result v2

    iget-object v3, p0, Lcom/my/target/eo;->aw:Lcom/my/target/cm;

    const/16 v4, 0xc

    invoke-virtual {v3, v4}, Lcom/my/target/cm;->n(I)I

    move-result v3

    invoke-virtual {v1, v8, v2, v8, v3}, Landroid/support/v7/widget/RecyclerView;->setPadding(IIII)V

    .line 166
    iget-object v1, p0, Lcom/my/target/eo;->cY:Lcom/my/target/ev;

    iget-object v2, p0, Lcom/my/target/eo;->aw:Lcom/my/target/cm;

    invoke-virtual {v2, v10}, Lcom/my/target/cm;->n(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/my/target/ev;->setSideSlidesMargins(I)V

    .line 167
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v11, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 168
    const/4 v2, 0x3

    sget v3, Lcom/my/target/eo;->bD:I

    invoke-virtual {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 170
    iget-object v2, p0, Lcom/my/target/eo;->cY:Lcom/my/target/ev;

    invoke-virtual {v2, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 172
    new-instance v1, Landroid/widget/RelativeLayout;

    invoke-direct {v1, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 173
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v11, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 174
    const/16 v3, 0xf

    invoke-virtual {v2, v3, v11}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 175
    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 177
    iget-object v2, p0, Lcom/my/target/eo;->bk:Lcom/my/target/bv;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 178
    iget-object v2, p0, Lcom/my/target/eo;->ch:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 179
    iget-object v2, p0, Lcom/my/target/eo;->cT:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 180
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v11, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 181
    iget-object v3, p0, Lcom/my/target/eo;->aw:Lcom/my/target/cm;

    invoke-virtual {v3, v10}, Lcom/my/target/cm;->n(I)I

    move-result v3

    invoke-virtual {v2, v8, v3, v8, v8}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 182
    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 184
    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 185
    iget-object v0, p0, Lcom/my/target/eo;->cZ:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 186
    iget-object v0, p0, Lcom/my/target/eo;->cY:Lcom/my/target/ev;

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 187
    invoke-virtual {p0, v1}, Lcom/my/target/eo;->addView(Landroid/view/View;)V

    .line 188
    iget-object v0, p0, Lcom/my/target/eo;->F:Lcom/my/target/by;

    invoke-virtual {p0, v0}, Lcom/my/target/eo;->addView(Landroid/view/View;)V

    .line 189
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/my/target/eo;->aX:Ljava/util/HashMap;

    .line 190
    return-void

    .line 89
    :cond_1
    const/high16 v3, 0x3fc00000    # 1.5f

    iput v3, p0, Lcom/my/target/eo;->db:F

    goto/16 :goto_0
.end method


# virtual methods
.method public final G()V
    .locals 2

    .prologue
    .line 291
    iget-object v0, p0, Lcom/my/target/eo;->F:Lcom/my/target/by;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/my/target/by;->setVisibility(I)V

    .line 292
    return-void
.end method

.method public final b(Lcom/my/target/core/models/banners/h;)V
    .locals 0

    .prologue
    .line 287
    return-void
.end method

.method public final e(I)V
    .locals 0

    .prologue
    .line 396
    return-void
.end method

.method public final f(Z)V
    .locals 0

    .prologue
    .line 368
    return-void
.end method

.method public final finish()V
    .locals 0

    .prologue
    .line 325
    return-void
.end method

.method public final getCloseButton()Landroid/view/View;
    .locals 1

    .prologue
    .line 313
    iget-object v0, p0, Lcom/my/target/eo;->F:Lcom/my/target/by;

    return-object v0
.end method

.method public final getNumbersOfCurrentShowingCards()[I
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v3, -0x1

    .line 378
    iget-object v0, p0, Lcom/my/target/eo;->cY:Lcom/my/target/ev;

    invoke-virtual {v0}, Lcom/my/target/ev;->getCardLayoutManager()Lcom/my/target/eq;

    move-result-object v0

    invoke-virtual {v0}, Lcom/my/target/eq;->findFirstVisibleItemPosition()I

    move-result v2

    .line 379
    iget-object v0, p0, Lcom/my/target/eo;->cY:Lcom/my/target/ev;

    invoke-virtual {v0}, Lcom/my/target/ev;->getCardLayoutManager()Lcom/my/target/eq;

    move-result-object v0

    invoke-virtual {v0}, Lcom/my/target/eq;->findLastCompletelyVisibleItemPosition()I

    move-result v0

    .line 380
    if-eq v2, v3, :cond_0

    if-ne v0, v3, :cond_2

    .line 382
    :cond_0
    new-array v0, v1, [I

    .line 390
    :cond_1
    return-object v0

    .line 384
    :cond_2
    sub-int/2addr v0, v2

    add-int/lit8 v4, v0, 0x1

    .line 385
    new-array v0, v4, [I

    .line 386
    :goto_0
    if-ge v1, v4, :cond_1

    .line 388
    add-int/lit8 v3, v2, 0x1

    aput v2, v0, v1

    .line 386
    add-int/lit8 v1, v1, 0x1

    move v2, v3

    goto :goto_0
.end method

.method public final getSoundButton()Lcom/my/target/by;
    .locals 1

    .prologue
    .line 308
    iget-object v0, p0, Lcom/my/target/eo;->bp:Lcom/my/target/by;

    return-object v0
.end method

.method public final isPaused()Z
    .locals 1

    .prologue
    .line 301
    const/4 v0, 0x0

    return v0
.end method

.method public final isPlaying()Z
    .locals 1

    .prologue
    .line 296
    const/4 v0, 0x0

    return v0
.end method

.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x1

    const/4 v2, -0x1

    .line 401
    iget-object v0, p0, Lcom/my/target/eo;->aX:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 403
    const/4 v0, 0x0

    .line 424
    :goto_0
    return v0

    .line 405
    :cond_0
    iget-object v0, p0, Lcom/my/target/eo;->aX:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 407
    goto :goto_0

    .line 409
    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_1
    :pswitch_0
    move v0, v1

    .line 424
    goto :goto_0

    .line 412
    :pswitch_1
    const v0, -0x3a1508

    invoke-virtual {p0, v0}, Lcom/my/target/eo;->setBackgroundColor(I)V

    goto :goto_1

    .line 415
    :pswitch_2
    invoke-virtual {p0, v2}, Lcom/my/target/eo;->setBackgroundColor(I)V

    .line 416
    iget-object v0, p0, Lcom/my/target/eo;->bx:Lcom/my/target/ee$a;

    invoke-virtual {v0, p1}, Lcom/my/target/ee$a;->onClick(Landroid/view/View;)V

    goto :goto_1

    .line 419
    :pswitch_3
    invoke-virtual {p0, v2}, Lcom/my/target/eo;->setBackgroundColor(I)V

    goto :goto_1

    .line 409
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public final pause()V
    .locals 0

    .prologue
    .line 373
    return-void
.end method

.method public final play()V
    .locals 0

    .prologue
    .line 319
    return-void
.end method

.method public final resume()V
    .locals 0

    .prologue
    .line 335
    return-void
.end method

.method public final setBanner(Lcom/my/target/core/models/banners/h;)V
    .locals 7
    .param p1, "banner"    # Lcom/my/target/core/models/banners/h;

    .prologue
    const/4 v4, -0x2

    const/4 v1, 0x0

    .line 195
    invoke-super {p0, p1}, Lcom/my/target/ee;->setBanner(Lcom/my/target/core/models/banners/h;)V

    .line 197
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getCloseIcon()Lcom/my/target/common/models/ImageData;

    move-result-object v0

    .line 198
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lcom/my/target/common/models/ImageData;->getData()Landroid/graphics/Bitmap;

    move-result-object v2

    if-eqz v2, :cond_6

    .line 200
    iget-object v2, p0, Lcom/my/target/eo;->F:Lcom/my/target/by;

    invoke-virtual {v0}, Lcom/my/target/common/models/ImageData;->getData()Landroid/graphics/Bitmap;

    move-result-object v0

    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, Lcom/my/target/by;->b(Landroid/graphics/Bitmap;Z)V

    .line 211
    :cond_0
    :goto_0
    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v3, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 214
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getIcon()Lcom/my/target/common/models/ImageData;

    move-result-object v4

    .line 215
    if-eqz v4, :cond_8

    .line 217
    invoke-virtual {v4}, Lcom/my/target/common/models/ImageData;->getWidth()I

    move-result v2

    .line 218
    invoke-virtual {v4}, Lcom/my/target/common/models/ImageData;->getHeight()I

    move-result v0

    .line 221
    :goto_1
    if-eqz v2, :cond_1

    if-eqz v0, :cond_1

    .line 223
    int-to-float v0, v0

    int-to-float v2, v2

    div-float/2addr v0, v2

    .line 225
    iget-object v2, p0, Lcom/my/target/eo;->aw:Lcom/my/target/cm;

    iget v5, p0, Lcom/my/target/eo;->da:I

    invoke-virtual {v2, v5}, Lcom/my/target/cm;->n(I)I

    move-result v2

    .line 226
    iget-object v5, p0, Lcom/my/target/eo;->aw:Lcom/my/target/cm;

    iget v6, p0, Lcom/my/target/eo;->da:I

    invoke-virtual {v5, v6}, Lcom/my/target/cm;->n(I)I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v0, v5

    float-to-int v0, v0

    .line 228
    iput v2, v3, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 229
    iput v0, v3, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 232
    :cond_1
    iget-object v0, p0, Lcom/my/target/eo;->bk:Lcom/my/target/bv;

    invoke-virtual {v0, v3}, Lcom/my/target/bv;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 234
    if-eqz v4, :cond_2

    .line 236
    iget-object v0, p0, Lcom/my/target/eo;->bk:Lcom/my/target/bv;

    invoke-virtual {v4}, Lcom/my/target/common/models/ImageData;->getData()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/my/target/bv;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 239
    :cond_2
    iget-object v0, p0, Lcom/my/target/eo;->ch:Landroid/widget/TextView;

    const/high16 v2, -0x1000000

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 240
    iget-object v0, p0, Lcom/my/target/eo;->ch:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 242
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getCategory()Ljava/lang/String;

    move-result-object v2

    .line 243
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getSubCategory()Ljava/lang/String;

    move-result-object v3

    .line 245
    const-string v0, ""

    .line 246
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 248
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 251
    :cond_3
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 253
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 256
    :cond_4
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 258
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 261
    :cond_5
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 263
    iget-object v2, p0, Lcom/my/target/eo;->cT:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 264
    iget-object v0, p0, Lcom/my/target/eo;->cT:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 271
    :goto_2
    iget-object v0, p0, Lcom/my/target/eo;->cZ:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getDescription()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 273
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getInterstitialAdCards()Ljava/util/List;

    move-result-object v0

    .line 274
    iget-object v1, p0, Lcom/my/target/eo;->cY:Lcom/my/target/ev;

    invoke-virtual {v1, v0}, Lcom/my/target/ev;->c(Ljava/util/List;)V

    .line 275
    return-void

    .line 204
    :cond_6
    iget-object v0, p0, Lcom/my/target/eo;->aw:Lcom/my/target/cm;

    const/16 v2, 0x1c

    invoke-virtual {v0, v2}, Lcom/my/target/cm;->n(I)I

    move-result v0

    invoke-static {v0}, Lcom/my/target/bq;->i(I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 205
    if-eqz v0, :cond_0

    .line 207
    iget-object v2, p0, Lcom/my/target/eo;->F:Lcom/my/target/by;

    invoke-virtual {v2, v0, v1}, Lcom/my/target/by;->b(Landroid/graphics/Bitmap;Z)V

    goto/16 :goto_0

    .line 268
    :cond_7
    iget-object v0, p0, Lcom/my/target/eo;->cT:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    :cond_8
    move v0, v1

    move v2, v1

    goto/16 :goto_1
.end method

.method public final setClickArea(Lcom/my/target/af;)V
    .locals 3
    .param p1, "area"    # Lcom/my/target/af;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ClickableViewAccessibility"
        }
    .end annotation

    .prologue
    .line 341
    iget-boolean v0, p1, Lcom/my/target/af;->cE:Z

    if-eqz v0, :cond_0

    .line 343
    iget-object v0, p0, Lcom/my/target/eo;->bx:Lcom/my/target/ee$a;

    invoke-virtual {p0, v0}, Lcom/my/target/eo;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 344
    const/4 v0, -0x1

    const v1, -0x3a1508

    invoke-static {p0, v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;II)V

    .line 345
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/my/target/eo;->setClickable(Z)V

    .line 362
    :goto_0
    iget-object v0, p0, Lcom/my/target/eo;->cY:Lcom/my/target/ev;

    iget-object v1, p0, Lcom/my/target/eo;->ad:Lcom/my/target/ee$b;

    invoke-virtual {v0, v1}, Lcom/my/target/ev;->setOnPromoCardListener(Lcom/my/target/ee$b;)V

    .line 363
    return-void

    .line 349
    :cond_0
    iget-object v0, p0, Lcom/my/target/eo;->ch:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 350
    iget-object v0, p0, Lcom/my/target/eo;->cT:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 351
    iget-object v0, p0, Lcom/my/target/eo;->bk:Lcom/my/target/bv;

    invoke-virtual {v0, p0}, Lcom/my/target/bv;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 352
    iget-object v0, p0, Lcom/my/target/eo;->cZ:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 353
    invoke-virtual {p0, p0}, Lcom/my/target/eo;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 355
    iget-object v0, p0, Lcom/my/target/eo;->aX:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/my/target/eo;->ch:Landroid/widget/TextView;

    iget-boolean v2, p1, Lcom/my/target/af;->cs:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 356
    iget-object v0, p0, Lcom/my/target/eo;->aX:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/my/target/eo;->cT:Landroid/widget/TextView;

    iget-boolean v2, p1, Lcom/my/target/af;->cC:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 357
    iget-object v0, p0, Lcom/my/target/eo;->aX:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/my/target/eo;->bk:Lcom/my/target/bv;

    iget-boolean v2, p1, Lcom/my/target/af;->cu:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 358
    iget-object v0, p0, Lcom/my/target/eo;->aX:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/my/target/eo;->cZ:Landroid/widget/TextView;

    iget-boolean v2, p1, Lcom/my/target/af;->ct:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 359
    iget-object v0, p0, Lcom/my/target/eo;->aX:Ljava/util/HashMap;

    iget-boolean v1, p1, Lcom/my/target/af;->cD:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method protected final setLayoutOrientation(I)V
    .locals 2
    .param p1, "orientation"    # I

    .prologue
    .line 430
    invoke-super {p0, p1}, Lcom/my/target/ee;->setLayoutOrientation(I)V

    .line 431
    iget-object v0, p0, Lcom/my/target/eo;->cY:Lcom/my/target/ev;

    invoke-virtual {v0}, Lcom/my/target/ev;->getCardLayoutManager()Lcom/my/target/eq;

    move-result-object v0

    .line 432
    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    .line 434
    check-cast v0, Lcom/my/target/eo$a;

    iget v1, p0, Lcom/my/target/eo;->db:F

    invoke-virtual {v0, v1}, Lcom/my/target/eo$a;->a(F)V

    .line 440
    :goto_0
    return-void

    .line 438
    :cond_0
    check-cast v0, Lcom/my/target/eo$a;

    const v1, 0x40733333    # 3.8f

    invoke-virtual {v0, v1}, Lcom/my/target/eo$a;->a(F)V

    goto :goto_0
.end method

.method public final setTimeChanged(F)V
    .locals 0

    .prologue
    .line 330
    return-void
.end method
