.class public Lru/cn/ad/AdPlayController;
.super Ljava/lang/Object;
.source "AdPlayController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/ad/AdPlayController$Listener;
    }
.end annotation


# instance fields
.field private final adsContainer:Landroid/view/ViewGroup;

.field private adsEnabled:Z

.field private final context:Landroid/content/Context;

.field private final listener:Lru/cn/ad/AdPlayController$Listener;

.field private metaTags:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final middleRollBanner:Lru/cn/ad/MiddleRollBanner;

.field private needsPreRoll:Z

.field private final player:Lru/cn/player/SimplePlayer;

.field private preRollBanner:Lru/cn/ad/PreRollBanner;

.field private preRollPlaceId:Ljava/lang/String;

.field private prerollPresenter:Lru/cn/ad/natives/adapters/NativeAdAdapter$Presenter;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lru/cn/player/SimplePlayer;Landroid/view/ViewGroup;Lru/cn/ad/AdPlayController$Listener;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "player"    # Lru/cn/player/SimplePlayer;
    .param p3, "adsContainer"    # Landroid/view/ViewGroup;
    .param p4, "listener"    # Lru/cn/ad/AdPlayController$Listener;

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object p1, p0, Lru/cn/ad/AdPlayController;->context:Landroid/content/Context;

    .line 58
    iput-object p2, p0, Lru/cn/ad/AdPlayController;->player:Lru/cn/player/SimplePlayer;

    .line 59
    iput-object p3, p0, Lru/cn/ad/AdPlayController;->adsContainer:Landroid/view/ViewGroup;

    .line 60
    iput-object p4, p0, Lru/cn/ad/AdPlayController;->listener:Lru/cn/ad/AdPlayController$Listener;

    .line 62
    new-instance v3, Lru/cn/ad/natives/presenters/NativePrerollPresenter;

    invoke-direct {v3, p1, p3}, Lru/cn/ad/natives/presenters/NativePrerollPresenter;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;)V

    iput-object v3, p0, Lru/cn/ad/AdPlayController;->prerollPresenter:Lru/cn/ad/natives/adapters/NativeAdAdapter$Presenter;

    .line 66
    invoke-static {}, Lru/cn/utils/Utils;->isTV()Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v1, "0sCyVJeb"

    .line 67
    .local v1, "middleRollPlaceId":Ljava/lang/String;
    :goto_0
    new-instance v0, Lru/cn/ad/MiddleRollFactory;

    invoke-static {}, Lru/cn/utils/Utils;->isTV()Z

    move-result v3

    invoke-direct {v0, p1, v3}, Lru/cn/ad/MiddleRollFactory;-><init>(Landroid/content/Context;Z)V

    .line 68
    .local v0, "factory":Lru/cn/ad/AdAdapter$Factory;
    new-instance v3, Lru/cn/ad/MiddleRollBanner;

    invoke-direct {v3, p1, p3, v1, v0}, Lru/cn/ad/MiddleRollBanner;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;Ljava/lang/String;Lru/cn/ad/AdAdapter$Factory;)V

    iput-object v3, p0, Lru/cn/ad/AdPlayController;->middleRollBanner:Lru/cn/ad/MiddleRollBanner;

    .line 70
    invoke-static {}, Lru/cn/utils/Utils;->isTV()Z

    move-result v3

    if-nez v3, :cond_0

    .line 71
    new-instance v2, Lru/cn/ad/natives/presenters/NativeContextualPresenter;

    invoke-direct {v2, p1, p3}, Lru/cn/ad/natives/presenters/NativeContextualPresenter;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;)V

    .line 72
    .local v2, "presenter":Lru/cn/ad/natives/adapters/NativeAdAdapter$Presenter;
    iget-object v3, p0, Lru/cn/ad/AdPlayController;->middleRollBanner:Lru/cn/ad/MiddleRollBanner;

    invoke-virtual {v3, v2}, Lru/cn/ad/MiddleRollBanner;->setContextPresenter(Lru/cn/ad/natives/adapters/NativeAdAdapter$Presenter;)V

    .line 75
    .end local v2    # "presenter":Lru/cn/ad/natives/adapters/NativeAdAdapter$Presenter;
    :cond_0
    iget-object v3, p0, Lru/cn/ad/AdPlayController;->middleRollBanner:Lru/cn/ad/MiddleRollBanner;

    new-instance v4, Lru/cn/ad/AdPlayController$1;

    invoke-direct {v4, p0, p4, p2, p3}, Lru/cn/ad/AdPlayController$1;-><init>(Lru/cn/ad/AdPlayController;Lru/cn/ad/AdPlayController$Listener;Lru/cn/player/SimplePlayer;Landroid/view/ViewGroup;)V

    invoke-virtual {v3, v4}, Lru/cn/ad/MiddleRollBanner;->setListener(Lru/cn/ad/MiddleRollBanner$Listener;)V

    .line 103
    return-void

    .line 66
    .end local v0    # "factory":Lru/cn/ad/AdAdapter$Factory;
    .end local v1    # "middleRollPlaceId":Ljava/lang/String;
    :cond_1
    const-string v1, "PIbEnlAp"

    goto :goto_0
.end method

.method static synthetic access$000(Lru/cn/ad/AdPlayController;)Lru/cn/ad/AdPlayController$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/AdPlayController;

    .prologue
    .line 23
    iget-object v0, p0, Lru/cn/ad/AdPlayController;->listener:Lru/cn/ad/AdPlayController$Listener;

    return-object v0
.end method

.method static synthetic access$100(Lru/cn/ad/AdPlayController;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/ad/AdPlayController;

    .prologue
    .line 23
    invoke-direct {p0}, Lru/cn/ad/AdPlayController;->play()V

    return-void
.end method

.method static synthetic access$200(Lru/cn/ad/AdPlayController;)Landroid/view/ViewGroup;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/AdPlayController;

    .prologue
    .line 23
    iget-object v0, p0, Lru/cn/ad/AdPlayController;->adsContainer:Landroid/view/ViewGroup;

    return-object v0
.end method

.method private play()V
    .locals 4

    .prologue
    const/16 v3, 0x64

    .line 237
    iget-object v1, p0, Lru/cn/ad/AdPlayController;->preRollBanner:Lru/cn/ad/PreRollBanner;

    invoke-virtual {v1}, Lru/cn/ad/PreRollBanner;->isReady()Z

    move-result v1

    if-nez v1, :cond_0

    .line 238
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Pre-roll ad is not ready"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 242
    :cond_0
    iget-object v1, p0, Lru/cn/ad/AdPlayController;->preRollBanner:Lru/cn/ad/PreRollBanner;

    invoke-virtual {v1}, Lru/cn/ad/PreRollBanner;->getPlaceId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lru/cn/ad/AdsManager;->consumeOpportunity(Ljava/lang/String;)V

    .line 245
    iget-object v1, p0, Lru/cn/ad/AdPlayController;->player:Lru/cn/player/SimplePlayer;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Lru/cn/player/SimplePlayer;->setPlayerType(I)V

    .line 246
    iget-object v1, p0, Lru/cn/ad/AdPlayController;->player:Lru/cn/player/SimplePlayer;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lru/cn/player/SimplePlayer;->setAspectRatio(F)V

    .line 247
    iget-object v1, p0, Lru/cn/ad/AdPlayController;->player:Lru/cn/player/SimplePlayer;

    invoke-virtual {v1, v3, v3}, Lru/cn/player/SimplePlayer;->setAutoCrop(II)V

    .line 249
    new-instance v0, Lru/cn/ad/video/RenderingSettings;

    invoke-direct {v0}, Lru/cn/ad/video/RenderingSettings;-><init>()V

    .line 250
    .local v0, "settings":Lru/cn/ad/video/RenderingSettings;
    iget-object v1, p0, Lru/cn/ad/AdPlayController;->player:Lru/cn/player/SimplePlayer;

    iput-object v1, v0, Lru/cn/ad/video/RenderingSettings;->player:Lru/cn/player/SimplePlayer;

    .line 251
    iget-object v1, p0, Lru/cn/ad/AdPlayController;->adsContainer:Landroid/view/ViewGroup;

    iput-object v1, v0, Lru/cn/ad/video/RenderingSettings;->container:Landroid/view/ViewGroup;

    .line 252
    iget-object v1, p0, Lru/cn/ad/AdPlayController;->preRollBanner:Lru/cn/ad/PreRollBanner;

    invoke-virtual {v1, v0}, Lru/cn/ad/PreRollBanner;->setRenderingSettings(Lru/cn/ad/video/RenderingSettings;)V

    .line 253
    iget-object v1, p0, Lru/cn/ad/AdPlayController;->preRollBanner:Lru/cn/ad/PreRollBanner;

    invoke-virtual {v1}, Lru/cn/ad/PreRollBanner;->show()V

    .line 254
    return-void
.end method


# virtual methods
.method public isPresentingAd()Z
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lru/cn/ad/AdPlayController;->preRollBanner:Lru/cn/ad/PreRollBanner;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lru/cn/ad/AdPlayController;->preRollBanner:Lru/cn/ad/PreRollBanner;

    invoke-virtual {v0}, Lru/cn/ad/PreRollBanner;->isReady()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lru/cn/ad/AdPlayController;->middleRollBanner:Lru/cn/ad/MiddleRollBanner;

    invoke-virtual {v0}, Lru/cn/ad/MiddleRollBanner;->isPresenting()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onActivityResume()V
    .locals 2

    .prologue
    .line 230
    iget-object v1, p0, Lru/cn/ad/AdPlayController;->preRollBanner:Lru/cn/ad/PreRollBanner;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lru/cn/ad/AdPlayController;->preRollBanner:Lru/cn/ad/PreRollBanner;

    invoke-virtual {v1}, Lru/cn/ad/PreRollBanner;->isReady()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    .line 231
    .local v0, "playingAd":Z
    :goto_0
    if-eqz v0, :cond_0

    .line 232
    iget-object v1, p0, Lru/cn/ad/AdPlayController;->player:Lru/cn/player/SimplePlayer;

    invoke-virtual {v1}, Lru/cn/player/SimplePlayer;->resume()V

    .line 234
    :cond_0
    return-void

    .line 230
    .end local v0    # "playingAd":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public resetPreRoll(Ljava/lang/String;)V
    .locals 2
    .param p1, "placeId"    # Ljava/lang/String;

    .prologue
    .line 127
    iget-object v0, p0, Lru/cn/ad/AdPlayController;->player:Lru/cn/player/SimplePlayer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lru/cn/player/SimplePlayer;->setVisibility(I)V

    .line 128
    iput-object p1, p0, Lru/cn/ad/AdPlayController;->preRollPlaceId:Ljava/lang/String;

    .line 129
    const/4 v0, 0x1

    iput-boolean v0, p0, Lru/cn/ad/AdPlayController;->needsPreRoll:Z

    .line 130
    return-void
.end method

.method public setAdsEnabled(Z)V
    .locals 0
    .param p1, "adsEnabled"    # Z

    .prologue
    .line 133
    iput-boolean p1, p0, Lru/cn/ad/AdPlayController;->adsEnabled:Z

    .line 134
    return-void
.end method

.method public setContentProgress(Lru/cn/ad/ContentProgress;)V
    .locals 2
    .param p1, "progress"    # Lru/cn/ad/ContentProgress;

    .prologue
    .line 200
    invoke-static {}, Lru/cn/utils/Utils;->isTV()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 201
    if-eqz p1, :cond_1

    invoke-interface {p1}, Lru/cn/ad/ContentProgress;->allowBlocking()Z

    move-result v0

    if-nez v0, :cond_1

    .line 214
    :goto_0
    return-void

    .line 204
    :cond_0
    const-string v0, "blocking_middle_roll"

    invoke-static {v0}, Lru/cn/api/experiments/Experiments;->eligibleForExperiment(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 205
    iget-object v0, p0, Lru/cn/ad/AdPlayController;->middleRollBanner:Lru/cn/ad/MiddleRollBanner;

    iget-object v1, p0, Lru/cn/ad/AdPlayController;->prerollPresenter:Lru/cn/ad/natives/adapters/NativeAdAdapter$Presenter;

    invoke-virtual {v0, v1}, Lru/cn/ad/MiddleRollBanner;->setBlockingPresenter(Lru/cn/ad/natives/adapters/NativeAdAdapter$Presenter;)V

    .line 209
    :cond_1
    iget-boolean v0, p0, Lru/cn/ad/AdPlayController;->adsEnabled:Z

    if-nez v0, :cond_2

    .line 210
    const/4 p1, 0x0

    .line 213
    :cond_2
    iget-object v0, p0, Lru/cn/ad/AdPlayController;->middleRollBanner:Lru/cn/ad/MiddleRollBanner;

    invoke-virtual {v0, p1}, Lru/cn/ad/MiddleRollBanner;->setContentProgress(Lru/cn/ad/ContentProgress;)V

    goto :goto_0
.end method

.method public setControlsVisibility(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 106
    iget-object v0, p0, Lru/cn/ad/AdPlayController;->adsContainer:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 107
    return-void
.end method

.method public setMetaTags(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 110
    .local p1, "tags":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    const-string v0, "AdPlayController"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Set meta tags "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    if-nez p1, :cond_0

    .line 112
    new-instance p1, Ljava/util/ArrayList;

    .end local p1    # "tags":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    .line 115
    .restart local p1    # "tags":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    :cond_0
    iput-object p1, p0, Lru/cn/ad/AdPlayController;->metaTags:Ljava/util/List;

    .line 116
    iget-object v0, p0, Lru/cn/ad/AdPlayController;->middleRollBanner:Lru/cn/ad/MiddleRollBanner;

    iget-object v1, p0, Lru/cn/ad/AdPlayController;->metaTags:Ljava/util/List;

    invoke-virtual {v0, v1}, Lru/cn/ad/MiddleRollBanner;->setMetaTags(Ljava/util/List;)V

    .line 117
    return-void
.end method

.method public setMetadata(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lru/cn/player/metadata/MetadataItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 120
    .local p1, "items":Ljava/util/List;, "Ljava/util/List<Lru/cn/player/metadata/MetadataItem;>;"
    invoke-static {}, Lru/cn/utils/Utils;->isTV()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 124
    :goto_0
    return-void

    .line 123
    :cond_0
    iget-object v0, p0, Lru/cn/ad/AdPlayController;->middleRollBanner:Lru/cn/ad/MiddleRollBanner;

    invoke-virtual {v0, p1}, Lru/cn/ad/MiddleRollBanner;->setMetadata(Ljava/util/List;)V

    goto :goto_0
.end method

.method public startPreRoll()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 137
    iget-boolean v2, p0, Lru/cn/ad/AdPlayController;->adsEnabled:Z

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lru/cn/ad/AdPlayController;->needsPreRoll:Z

    if-nez v2, :cond_1

    .line 138
    :cond_0
    iget-object v2, p0, Lru/cn/ad/AdPlayController;->listener:Lru/cn/ad/AdPlayController$Listener;

    invoke-interface {v2, v5}, Lru/cn/ad/AdPlayController$Listener;->onAdBreakEnded(Z)V

    .line 193
    :goto_0
    return-void

    .line 142
    :cond_1
    iput-boolean v5, p0, Lru/cn/ad/AdPlayController;->needsPreRoll:Z

    .line 144
    iget-object v0, p0, Lru/cn/ad/AdPlayController;->preRollPlaceId:Ljava/lang/String;

    .line 146
    .local v0, "placeId":Ljava/lang/String;
    const-string v2, "AdPlayController"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Check ad networks for place "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    invoke-virtual {p0}, Lru/cn/ad/AdPlayController;->stop()V

    .line 149
    invoke-static {v0}, Lru/cn/ad/AdsManager;->hasOpportunity(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 150
    iget-object v2, p0, Lru/cn/ad/AdPlayController;->listener:Lru/cn/ad/AdPlayController$Listener;

    invoke-interface {v2, v5}, Lru/cn/ad/AdPlayController$Listener;->onAdBreakEnded(Z)V

    goto :goto_0

    .line 155
    :cond_2
    sget-object v2, Lru/cn/domain/statistics/inetra/AdvEvent;->ADV_EVENT_OPPORTUNITY:Lru/cn/domain/statistics/inetra/AdvEvent;

    invoke-static {v2, v0, v6, v6}, Lru/cn/domain/statistics/inetra/InetraTracker;->advEvent(Lru/cn/domain/statistics/inetra/AdvEvent;Ljava/lang/String;Lru/cn/api/money_miner/replies/AdSystem;Ljava/util/Map;)V

    .line 158
    new-instance v1, Lru/cn/ad/PreRollFactory;

    iget-object v2, p0, Lru/cn/ad/AdPlayController;->context:Landroid/content/Context;

    invoke-static {}, Lru/cn/utils/Utils;->isTV()Z

    move-result v3

    invoke-direct {v1, v2, v3}, Lru/cn/ad/PreRollFactory;-><init>(Landroid/content/Context;Z)V

    .line 159
    .local v1, "preRollFactory":Lru/cn/ad/AdAdapter$Factory;
    new-instance v2, Lru/cn/ad/PreRollBanner;

    iget-object v3, p0, Lru/cn/ad/AdPlayController;->context:Landroid/content/Context;

    iget-object v4, p0, Lru/cn/ad/AdPlayController;->metaTags:Ljava/util/List;

    invoke-direct {v2, v3, v0, v1, v4}, Lru/cn/ad/PreRollBanner;-><init>(Landroid/content/Context;Ljava/lang/String;Lru/cn/ad/AdAdapter$Factory;Ljava/util/List;)V

    iput-object v2, p0, Lru/cn/ad/AdPlayController;->preRollBanner:Lru/cn/ad/PreRollBanner;

    .line 162
    invoke-static {v0}, Lru/cn/ad/AdsManager;->consumeOpportunity(Ljava/lang/String;)V

    .line 164
    iget-object v2, p0, Lru/cn/ad/AdPlayController;->preRollBanner:Lru/cn/ad/PreRollBanner;

    iget-object v3, p0, Lru/cn/ad/AdPlayController;->prerollPresenter:Lru/cn/ad/natives/adapters/NativeAdAdapter$Presenter;

    invoke-virtual {v2, v3}, Lru/cn/ad/PreRollBanner;->setNativePresenter(Lru/cn/ad/natives/adapters/NativeAdAdapter$Presenter;)V

    .line 165
    iget-object v2, p0, Lru/cn/ad/AdPlayController;->preRollBanner:Lru/cn/ad/PreRollBanner;

    new-instance v3, Lru/cn/ad/AdPlayController$2;

    invoke-direct {v3, p0, v0}, Lru/cn/ad/AdPlayController$2;-><init>(Lru/cn/ad/AdPlayController;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Lru/cn/ad/PreRollBanner;->setListener(Lru/cn/ad/PreRollBanner$Listener;)V

    .line 192
    iget-object v2, p0, Lru/cn/ad/AdPlayController;->preRollBanner:Lru/cn/ad/PreRollBanner;

    invoke-virtual {v2}, Lru/cn/ad/PreRollBanner;->load()V

    goto :goto_0
.end method

.method public stop()V
    .locals 2

    .prologue
    .line 217
    iget-object v0, p0, Lru/cn/ad/AdPlayController;->preRollBanner:Lru/cn/ad/PreRollBanner;

    if-eqz v0, :cond_0

    .line 218
    iget-object v0, p0, Lru/cn/ad/AdPlayController;->preRollBanner:Lru/cn/ad/PreRollBanner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lru/cn/ad/PreRollBanner;->setListener(Lru/cn/ad/PreRollBanner$Listener;)V

    .line 219
    iget-object v0, p0, Lru/cn/ad/AdPlayController;->preRollBanner:Lru/cn/ad/PreRollBanner;

    invoke-virtual {v0}, Lru/cn/ad/PreRollBanner;->destroy()V

    .line 222
    :cond_0
    iget-object v0, p0, Lru/cn/ad/AdPlayController;->middleRollBanner:Lru/cn/ad/MiddleRollBanner;

    invoke-virtual {v0}, Lru/cn/ad/MiddleRollBanner;->dismiss()V

    .line 224
    iget-object v0, p0, Lru/cn/ad/AdPlayController;->adsContainer:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 225
    iget-object v0, p0, Lru/cn/ad/AdPlayController;->player:Lru/cn/player/SimplePlayer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lru/cn/player/SimplePlayer;->setVisibility(I)V

    .line 226
    return-void
.end method
