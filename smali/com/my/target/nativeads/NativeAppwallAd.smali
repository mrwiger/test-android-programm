.class public final Lcom/my/target/nativeads/NativeAppwallAd;
.super Lcom/my/target/common/BaseAd;
.source "NativeAppwallAd.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/nativeads/NativeAppwallAd$AppwallAdListener;
    }
.end annotation


# static fields
.field private static final DEFAULT_TITLE:Ljava/lang/String; = "Apps"

.field private static final DEFAULT_TITLE_BACKGROUND_COLOR:I = -0xbaa59d

.field private static final DEFAULT_TITLE_SUPPLEMENTARY_COLOR:I = -0xc9bab3

.field private static final DEFAULT_TITLE_TEXT_COLOR:I = -0x1


# instance fields
.field private final banners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/my/target/nativeads/banners/NativeAppwallBanner;",
            ">;"
        }
    .end annotation
.end field

.field private final bannersMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/my/target/nativeads/banners/NativeAppwallBanner;",
            "Lcom/my/target/core/models/banners/i;",
            ">;"
        }
    .end annotation
.end field

.field private final clickHandler:Lcom/my/target/ce;

.field private final context:Landroid/content/Context;

.field private engine:Lcom/my/target/core/engines/h;

.field private hideStatusBarInDialog:Z

.field private listener:Lcom/my/target/nativeads/NativeAppwallAd$AppwallAdListener;

.field private section:Lcom/my/target/fg;

.field private title:Ljava/lang/String;

.field private titleBackgroundColor:I

.field private titleSupplementaryColor:I

.field private titleTextColor:I

.field private viewWeakReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/my/target/nativeads/views/AppwallAdView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(ILandroid/content/Context;)V
    .locals 2
    .param p1, "slotId"    # I
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 70
    const-string v0, "appwall"

    invoke-direct {p0, p1, v0}, Lcom/my/target/common/BaseAd;-><init>(ILjava/lang/String;)V

    .line 55
    invoke-static {}, Lcom/my/target/ce;->bw()Lcom/my/target/ce;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/nativeads/NativeAppwallAd;->clickHandler:Lcom/my/target/ce;

    .line 56
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/my/target/nativeads/NativeAppwallAd;->bannersMap:Ljava/util/HashMap;

    .line 57
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/my/target/nativeads/NativeAppwallAd;->banners:Ljava/util/ArrayList;

    .line 62
    const-string v0, "Apps"

    iput-object v0, p0, Lcom/my/target/nativeads/NativeAppwallAd;->title:Ljava/lang/String;

    .line 63
    const v0, -0xbaa59d

    iput v0, p0, Lcom/my/target/nativeads/NativeAppwallAd;->titleBackgroundColor:I

    .line 64
    const v0, -0xc9bab3

    iput v0, p0, Lcom/my/target/nativeads/NativeAppwallAd;->titleSupplementaryColor:I

    .line 65
    const/4 v0, -0x1

    iput v0, p0, Lcom/my/target/nativeads/NativeAppwallAd;->titleTextColor:I

    .line 66
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/my/target/nativeads/NativeAppwallAd;->hideStatusBarInDialog:Z

    .line 71
    iput-object p2, p0, Lcom/my/target/nativeads/NativeAppwallAd;->context:Landroid/content/Context;

    .line 72
    iget-object v0, p0, Lcom/my/target/nativeads/NativeAppwallAd;->adConfig:Lcom/my/target/b;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/my/target/b;->setAutoLoadImages(Z)V

    .line 73
    const-string v0, "NativeAppwallAd created. Version: 5.1.0"

    invoke-static {v0}, Lcom/my/target/g;->c(Ljava/lang/String;)V

    .line 74
    return-void
.end method

.method static synthetic access$000(Lcom/my/target/nativeads/NativeAppwallAd;Lcom/my/target/fg;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/my/target/nativeads/NativeAppwallAd;
    .param p1, "x1"    # Lcom/my/target/fg;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Lcom/my/target/nativeads/NativeAppwallAd;->handleResult(Lcom/my/target/fg;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/my/target/nativeads/NativeAppwallAd;)Ljava/lang/ref/WeakReference;
    .locals 1
    .param p0, "x0"    # Lcom/my/target/nativeads/NativeAppwallAd;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/my/target/nativeads/NativeAppwallAd;->viewWeakReference:Ljava/lang/ref/WeakReference;

    return-object v0
.end method

.method private handleResult(Lcom/my/target/fg;Ljava/lang/String;)V
    .locals 4
    .param p1, "result"    # Lcom/my/target/fg;
    .param p2, "error"    # Ljava/lang/String;

    .prologue
    .line 361
    iget-object v0, p0, Lcom/my/target/nativeads/NativeAppwallAd;->listener:Lcom/my/target/nativeads/NativeAppwallAd$AppwallAdListener;

    if-eqz v0, :cond_1

    .line 363
    if-nez p1, :cond_2

    .line 365
    iget-object v0, p0, Lcom/my/target/nativeads/NativeAppwallAd;->listener:Lcom/my/target/nativeads/NativeAppwallAd$AppwallAdListener;

    if-nez p2, :cond_0

    const-string p2, "no ad"

    .end local p2    # "error":Ljava/lang/String;
    :cond_0
    invoke-interface {v0, p2, p0}, Lcom/my/target/nativeads/NativeAppwallAd$AppwallAdListener;->onNoAd(Ljava/lang/String;Lcom/my/target/nativeads/NativeAppwallAd;)V

    .line 379
    :cond_1
    :goto_0
    return-void

    .line 369
    .restart local p2    # "error":Ljava/lang/String;
    :cond_2
    iput-object p1, p0, Lcom/my/target/nativeads/NativeAppwallAd;->section:Lcom/my/target/fg;

    .line 370
    invoke-virtual {p1}, Lcom/my/target/fg;->R()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/core/models/banners/i;

    .line 372
    invoke-static {v0}, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->newBanner(Lcom/my/target/core/models/banners/i;)Lcom/my/target/nativeads/banners/NativeAppwallBanner;

    move-result-object v2

    .line 373
    iget-object v3, p0, Lcom/my/target/nativeads/NativeAppwallAd;->banners:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 374
    iget-object v3, p0, Lcom/my/target/nativeads/NativeAppwallAd;->bannersMap:Ljava/util/HashMap;

    invoke-virtual {v3, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 376
    :cond_3
    iget-object v0, p0, Lcom/my/target/nativeads/NativeAppwallAd;->listener:Lcom/my/target/nativeads/NativeAppwallAd$AppwallAdListener;

    invoke-interface {v0, p0}, Lcom/my/target/nativeads/NativeAppwallAd$AppwallAdListener;->onLoad(Lcom/my/target/nativeads/NativeAppwallAd;)V

    goto :goto_0
.end method

.method public static loadImageToView(Lcom/my/target/common/models/ImageData;Landroid/widget/ImageView;)V
    .locals 0
    .param p0, "imageData"    # Lcom/my/target/common/models/ImageData;
    .param p1, "view"    # Landroid/widget/ImageView;

    .prologue
    .line 51
    invoke-static {p0, p1}, Lcom/my/target/ch;->a(Lcom/my/target/common/models/ImageData;Landroid/widget/ImageView;)V

    .line 52
    return-void
.end method


# virtual methods
.method public final destroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 269
    invoke-virtual {p0}, Lcom/my/target/nativeads/NativeAppwallAd;->unregisterAppwallAdView()V

    .line 270
    iget-object v0, p0, Lcom/my/target/nativeads/NativeAppwallAd;->engine:Lcom/my/target/core/engines/h;

    if-eqz v0, :cond_0

    .line 272
    iget-object v0, p0, Lcom/my/target/nativeads/NativeAppwallAd;->engine:Lcom/my/target/core/engines/h;

    invoke-virtual {v0}, Lcom/my/target/core/engines/h;->destroy()V

    .line 273
    iput-object v1, p0, Lcom/my/target/nativeads/NativeAppwallAd;->engine:Lcom/my/target/core/engines/h;

    .line 275
    :cond_0
    iput-object v1, p0, Lcom/my/target/nativeads/NativeAppwallAd;->listener:Lcom/my/target/nativeads/NativeAppwallAd$AppwallAdListener;

    .line 276
    return-void
.end method

.method public final dismiss()V
    .locals 1

    .prologue
    .line 261
    iget-object v0, p0, Lcom/my/target/nativeads/NativeAppwallAd;->engine:Lcom/my/target/core/engines/h;

    if-eqz v0, :cond_0

    .line 263
    iget-object v0, p0, Lcom/my/target/nativeads/NativeAppwallAd;->engine:Lcom/my/target/core/engines/h;

    invoke-virtual {v0}, Lcom/my/target/core/engines/h;->dismiss()V

    .line 265
    :cond_0
    return-void
.end method

.method public final getBanners()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/my/target/nativeads/banners/NativeAppwallBanner;",
            ">;"
        }
    .end annotation

    .prologue
    .line 118
    iget-object v0, p0, Lcom/my/target/nativeads/NativeAppwallAd;->banners:Ljava/util/ArrayList;

    return-object v0
.end method

.method public final getCachePeriod()J
    .locals 2

    .prologue
    .line 108
    iget-object v0, p0, Lcom/my/target/nativeads/NativeAppwallAd;->adConfig:Lcom/my/target/b;

    invoke-virtual {v0}, Lcom/my/target/b;->getCachePeriod()J

    move-result-wide v0

    return-wide v0
.end method

.method public final getListener()Lcom/my/target/nativeads/NativeAppwallAd$AppwallAdListener;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/my/target/nativeads/NativeAppwallAd;->listener:Lcom/my/target/nativeads/NativeAppwallAd$AppwallAdListener;

    return-object v0
.end method

.method public final getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/my/target/nativeads/NativeAppwallAd;->title:Ljava/lang/String;

    return-object v0
.end method

.method public final getTitleBackgroundColor()I
    .locals 1

    .prologue
    .line 133
    iget v0, p0, Lcom/my/target/nativeads/NativeAppwallAd;->titleBackgroundColor:I

    return v0
.end method

.method public final getTitleSupplementaryColor()I
    .locals 1

    .prologue
    .line 143
    iget v0, p0, Lcom/my/target/nativeads/NativeAppwallAd;->titleSupplementaryColor:I

    return v0
.end method

.method public final getTitleTextColor()I
    .locals 1

    .prologue
    .line 153
    iget v0, p0, Lcom/my/target/nativeads/NativeAppwallAd;->titleTextColor:I

    return v0
.end method

.method public final handleBannerClick(Lcom/my/target/nativeads/banners/NativeAppwallBanner;)V
    .locals 3
    .param p1, "banner"    # Lcom/my/target/nativeads/banners/NativeAppwallBanner;

    .prologue
    .line 280
    iget-object v0, p0, Lcom/my/target/nativeads/NativeAppwallAd;->bannersMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/core/models/banners/i;

    .line 281
    if-eqz v0, :cond_2

    .line 283
    iget-object v1, p0, Lcom/my/target/nativeads/NativeAppwallAd;->clickHandler:Lcom/my/target/ce;

    iget-object v2, p0, Lcom/my/target/nativeads/NativeAppwallAd;->context:Landroid/content/Context;

    invoke-virtual {v1, v0, v2}, Lcom/my/target/ce;->a(Lcom/my/target/ah;Landroid/content/Context;)V

    .line 284
    iget-object v1, p0, Lcom/my/target/nativeads/NativeAppwallAd;->section:Lcom/my/target/fg;

    if-eqz v1, :cond_0

    .line 286
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->setHasNotification(Z)V

    .line 287
    iget-object v1, p0, Lcom/my/target/nativeads/NativeAppwallAd;->section:Lcom/my/target/fg;

    iget-object v2, p0, Lcom/my/target/nativeads/NativeAppwallAd;->adConfig:Lcom/my/target/b;

    invoke-static {v1, v2}, Lcom/my/target/fi;->a(Lcom/my/target/fg;Lcom/my/target/b;)Lcom/my/target/fi;

    move-result-object v1

    iget-object v2, p0, Lcom/my/target/nativeads/NativeAppwallAd;->context:Landroid/content/Context;

    .line 288
    invoke-virtual {v1, v0, v2}, Lcom/my/target/fi;->a(Lcom/my/target/core/models/banners/i;Landroid/content/Context;)V

    .line 291
    :cond_0
    iget-object v0, p0, Lcom/my/target/nativeads/NativeAppwallAd;->listener:Lcom/my/target/nativeads/NativeAppwallAd$AppwallAdListener;

    if-eqz v0, :cond_1

    .line 293
    iget-object v0, p0, Lcom/my/target/nativeads/NativeAppwallAd;->listener:Lcom/my/target/nativeads/NativeAppwallAd$AppwallAdListener;

    invoke-interface {v0, p1, p0}, Lcom/my/target/nativeads/NativeAppwallAd$AppwallAdListener;->onClick(Lcom/my/target/nativeads/banners/NativeAppwallBanner;Lcom/my/target/nativeads/NativeAppwallAd;)V

    .line 300
    :cond_1
    :goto_0
    return-void

    .line 298
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "unable to handle banner click: no internal banner for id "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final handleBannerShow(Lcom/my/target/nativeads/banners/NativeAppwallBanner;)V
    .locals 2
    .param p1, "banner"    # Lcom/my/target/nativeads/banners/NativeAppwallBanner;

    .prologue
    .line 347
    iget-object v0, p0, Lcom/my/target/nativeads/NativeAppwallAd;->bannersMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/core/models/banners/i;

    .line 348
    if-eqz v0, :cond_0

    .line 350
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/i;->getStatHolder()Lcom/my/target/ar;

    move-result-object v0

    .line 351
    const-string v1, "playbackStarted"

    invoke-virtual {v0, v1}, Lcom/my/target/ar;->w(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/nativeads/NativeAppwallAd;->context:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/my/target/cl;->a(Ljava/util/List;Landroid/content/Context;)V

    .line 357
    :goto_0
    return-void

    .line 355
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "unable to handle banner show: no internal banner for id "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final handleBannersShow(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/my/target/nativeads/banners/NativeAppwallBanner;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 325
    .local p1, "banners":Ljava/util/List;, "Ljava/util/List<Lcom/my/target/nativeads/banners/NativeAppwallBanner;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 326
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/nativeads/banners/NativeAppwallBanner;

    .line 328
    iget-object v1, p0, Lcom/my/target/nativeads/NativeAppwallAd;->bannersMap:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/my/target/core/models/banners/i;

    .line 329
    if-eqz v1, :cond_0

    .line 331
    invoke-virtual {v1}, Lcom/my/target/core/models/banners/i;->getStatHolder()Lcom/my/target/ar;

    move-result-object v0

    .line 332
    const-string v1, "playbackStarted"

    invoke-virtual {v0, v1}, Lcom/my/target/ar;->w(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 336
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "unable to handle banner show: no internal banner for id "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 339
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 341
    iget-object v0, p0, Lcom/my/target/nativeads/NativeAppwallAd;->context:Landroid/content/Context;

    invoke-static {v2, v0}, Lcom/my/target/cl;->a(Ljava/util/List;Landroid/content/Context;)V

    .line 343
    :cond_2
    return-void
.end method

.method public final hasNotifications()Z
    .locals 2

    .prologue
    .line 175
    iget-object v0, p0, Lcom/my/target/nativeads/NativeAppwallAd;->bannersMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/nativeads/banners/NativeAppwallBanner;

    .line 177
    invoke-virtual {v0}, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->isHasNotification()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 179
    const/4 v0, 0x1

    .line 182
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isAutoLoadImages()Z
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/my/target/nativeads/NativeAppwallAd;->adConfig:Lcom/my/target/b;

    invoke-virtual {v0}, Lcom/my/target/b;->isAutoLoadImages()Z

    move-result v0

    return v0
.end method

.method public final isHideStatusBarInDialog()Z
    .locals 1

    .prologue
    .line 88
    iget-boolean v0, p0, Lcom/my/target/nativeads/NativeAppwallAd;->hideStatusBarInDialog:Z

    return v0
.end method

.method public final load()V
    .locals 2

    .prologue
    .line 163
    iget-object v0, p0, Lcom/my/target/nativeads/NativeAppwallAd;->adConfig:Lcom/my/target/b;

    invoke-static {v0}, Lcom/my/target/fd;->newFactory(Lcom/my/target/b;)Lcom/my/target/c;

    move-result-object v0

    new-instance v1, Lcom/my/target/nativeads/NativeAppwallAd$1;

    invoke-direct {v1, p0}, Lcom/my/target/nativeads/NativeAppwallAd$1;-><init>(Lcom/my/target/nativeads/NativeAppwallAd;)V

    invoke-virtual {v0, v1}, Lcom/my/target/c;->a(Lcom/my/target/c$b;)Lcom/my/target/c;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/nativeads/NativeAppwallAd;->context:Landroid/content/Context;

    .line 170
    invoke-virtual {v0, v1}, Lcom/my/target/c;->a(Landroid/content/Context;)Lcom/my/target/c;

    .line 171
    return-void
.end method

.method public final prepareBannerClickLink(Lcom/my/target/nativeads/banners/NativeAppwallBanner;)Ljava/lang/String;
    .locals 3
    .param p1, "banner"    # Lcom/my/target/nativeads/banners/NativeAppwallBanner;

    .prologue
    .line 304
    iget-object v0, p0, Lcom/my/target/nativeads/NativeAppwallAd;->bannersMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/core/models/banners/i;

    .line 305
    if-eqz v0, :cond_1

    .line 307
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/i;->getStatHolder()Lcom/my/target/ar;

    move-result-object v1

    .line 308
    const-string v2, "click"

    invoke-virtual {v1, v2}, Lcom/my/target/ar;->w(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/my/target/nativeads/NativeAppwallAd;->context:Landroid/content/Context;

    invoke-static {v1, v2}, Lcom/my/target/cl;->a(Ljava/util/List;Landroid/content/Context;)V

    .line 309
    iget-object v1, p0, Lcom/my/target/nativeads/NativeAppwallAd;->section:Lcom/my/target/fg;

    if-eqz v1, :cond_0

    .line 311
    iget-object v1, p0, Lcom/my/target/nativeads/NativeAppwallAd;->section:Lcom/my/target/fg;

    iget-object v2, p0, Lcom/my/target/nativeads/NativeAppwallAd;->adConfig:Lcom/my/target/b;

    invoke-static {v1, v2}, Lcom/my/target/fi;->a(Lcom/my/target/fg;Lcom/my/target/b;)Lcom/my/target/fi;

    move-result-object v1

    iget-object v2, p0, Lcom/my/target/nativeads/NativeAppwallAd;->context:Landroid/content/Context;

    .line 312
    invoke-virtual {v1, v0, v2}, Lcom/my/target/fi;->a(Lcom/my/target/core/models/banners/i;Landroid/content/Context;)V

    .line 314
    :cond_0
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/i;->getTrackingLink()Ljava/lang/String;

    move-result-object v0

    .line 320
    :goto_0
    return-object v0

    .line 318
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "unable to handle banner click: no internal banner for id "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/my/target/nativeads/banners/NativeAppwallBanner;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 320
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final registerAppwallAdView(Lcom/my/target/nativeads/views/AppwallAdView;)V
    .locals 1
    .param p1, "view"    # Lcom/my/target/nativeads/views/AppwallAdView;

    .prologue
    .line 187
    invoke-virtual {p0}, Lcom/my/target/nativeads/NativeAppwallAd;->unregisterAppwallAdView()V

    .line 188
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/my/target/nativeads/NativeAppwallAd;->viewWeakReference:Ljava/lang/ref/WeakReference;

    .line 189
    new-instance v0, Lcom/my/target/nativeads/NativeAppwallAd$2;

    invoke-direct {v0, p0}, Lcom/my/target/nativeads/NativeAppwallAd$2;-><init>(Lcom/my/target/nativeads/NativeAppwallAd;)V

    invoke-virtual {p1, v0}, Lcom/my/target/nativeads/views/AppwallAdView;->setAppwallAdViewListener(Lcom/my/target/nativeads/views/AppwallAdView$AppwallAdViewListener;)V

    .line 211
    return-void
.end method

.method public final setAutoLoadImages(Z)V
    .locals 1
    .param p1, "autoLoadImages"    # Z

    .prologue
    .line 98
    iget-object v0, p0, Lcom/my/target/nativeads/NativeAppwallAd;->adConfig:Lcom/my/target/b;

    invoke-virtual {v0, p1}, Lcom/my/target/b;->setAutoLoadImages(Z)V

    .line 99
    return-void
.end method

.method public final setCachePeriod(J)V
    .locals 1
    .param p1, "cachePeriod"    # J

    .prologue
    .line 113
    iget-object v0, p0, Lcom/my/target/nativeads/NativeAppwallAd;->adConfig:Lcom/my/target/b;

    invoke-virtual {v0, p1, p2}, Lcom/my/target/b;->setCachePeriod(J)V

    .line 114
    return-void
.end method

.method public final setHideStatusBarInDialog(Z)V
    .locals 0
    .param p1, "hideStatusBarInDialog"    # Z

    .prologue
    .line 93
    iput-boolean p1, p0, Lcom/my/target/nativeads/NativeAppwallAd;->hideStatusBarInDialog:Z

    .line 94
    return-void
.end method

.method public final setListener(Lcom/my/target/nativeads/NativeAppwallAd$AppwallAdListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/my/target/nativeads/NativeAppwallAd$AppwallAdListener;

    .prologue
    .line 83
    iput-object p1, p0, Lcom/my/target/nativeads/NativeAppwallAd;->listener:Lcom/my/target/nativeads/NativeAppwallAd$AppwallAdListener;

    .line 84
    return-void
.end method

.method public final setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 128
    iput-object p1, p0, Lcom/my/target/nativeads/NativeAppwallAd;->title:Ljava/lang/String;

    .line 129
    return-void
.end method

.method public final setTitleBackgroundColor(I)V
    .locals 0
    .param p1, "titleBackgroundColor"    # I

    .prologue
    .line 138
    iput p1, p0, Lcom/my/target/nativeads/NativeAppwallAd;->titleBackgroundColor:I

    .line 139
    return-void
.end method

.method public final setTitleSupplementaryColor(I)V
    .locals 0
    .param p1, "titleSupplementaryColor"    # I

    .prologue
    .line 148
    iput p1, p0, Lcom/my/target/nativeads/NativeAppwallAd;->titleSupplementaryColor:I

    .line 149
    return-void
.end method

.method public final setTitleTextColor(I)V
    .locals 0
    .param p1, "titleTextColor"    # I

    .prologue
    .line 158
    iput p1, p0, Lcom/my/target/nativeads/NativeAppwallAd;->titleTextColor:I

    .line 159
    return-void
.end method

.method public final show()V
    .locals 3

    .prologue
    .line 229
    iget-object v0, p0, Lcom/my/target/nativeads/NativeAppwallAd;->section:Lcom/my/target/fg;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/my/target/nativeads/NativeAppwallAd;->banners:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 231
    iget-object v0, p0, Lcom/my/target/nativeads/NativeAppwallAd;->engine:Lcom/my/target/core/engines/h;

    if-nez v0, :cond_0

    .line 233
    invoke-static {p0}, Lcom/my/target/core/engines/h;->a(Lcom/my/target/nativeads/NativeAppwallAd;)Lcom/my/target/core/engines/h;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/nativeads/NativeAppwallAd;->engine:Lcom/my/target/core/engines/h;

    .line 235
    :cond_0
    iget-object v0, p0, Lcom/my/target/nativeads/NativeAppwallAd;->engine:Lcom/my/target/core/engines/h;

    iget-object v1, p0, Lcom/my/target/nativeads/NativeAppwallAd;->context:Landroid/content/Context;

    .line 1060
    sput-object v0, Lcom/my/target/common/MyTargetActivity;->activityEngine:Lcom/my/target/common/MyTargetActivity$ActivityEngine;

    .line 1061
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/my/target/common/MyTargetActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1062
    instance-of v2, v1, Landroid/app/Activity;

    if-nez v2, :cond_1

    .line 1064
    const/high16 v2, 0x10000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1066
    :cond_1
    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 241
    :goto_0
    return-void

    .line 239
    :cond_2
    const-string v0, "NativeAppwallAd.show: No ad"

    invoke-static {v0}, Lcom/my/target/g;->c(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final showDialog()V
    .locals 2

    .prologue
    .line 245
    iget-object v0, p0, Lcom/my/target/nativeads/NativeAppwallAd;->section:Lcom/my/target/fg;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/my/target/nativeads/NativeAppwallAd;->banners:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 247
    iget-object v0, p0, Lcom/my/target/nativeads/NativeAppwallAd;->engine:Lcom/my/target/core/engines/h;

    if-nez v0, :cond_0

    .line 249
    invoke-static {p0}, Lcom/my/target/core/engines/h;->a(Lcom/my/target/nativeads/NativeAppwallAd;)Lcom/my/target/core/engines/h;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/nativeads/NativeAppwallAd;->engine:Lcom/my/target/core/engines/h;

    .line 251
    :cond_0
    iget-object v0, p0, Lcom/my/target/nativeads/NativeAppwallAd;->engine:Lcom/my/target/core/engines/h;

    iget-object v1, p0, Lcom/my/target/nativeads/NativeAppwallAd;->context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/my/target/core/engines/h;->showDialog(Landroid/content/Context;)V

    .line 257
    :goto_0
    return-void

    .line 255
    :cond_1
    const-string v0, "NativeAppwallAd.show: No ad"

    invoke-static {v0}, Lcom/my/target/g;->c(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final unregisterAppwallAdView()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 215
    iget-object v0, p0, Lcom/my/target/nativeads/NativeAppwallAd;->viewWeakReference:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_1

    .line 217
    iget-object v0, p0, Lcom/my/target/nativeads/NativeAppwallAd;->viewWeakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/nativeads/views/AppwallAdView;

    .line 218
    if-eqz v0, :cond_0

    .line 220
    invoke-virtual {v0, v1}, Lcom/my/target/nativeads/views/AppwallAdView;->setAppwallAdViewListener(Lcom/my/target/nativeads/views/AppwallAdView$AppwallAdViewListener;)V

    .line 222
    :cond_0
    iget-object v0, p0, Lcom/my/target/nativeads/NativeAppwallAd;->viewWeakReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->clear()V

    .line 223
    iput-object v1, p0, Lcom/my/target/nativeads/NativeAppwallAd;->viewWeakReference:Ljava/lang/ref/WeakReference;

    .line 225
    :cond_1
    return-void
.end method
