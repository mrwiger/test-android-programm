.class public Lcom/google/obf/gs;
.super Ljava/lang/Object;
.source "IMASDK"

# interfaces
.implements Lcom/google/obf/hn$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/obf/gs$a;
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private b:Lcom/google/obf/hj;

.field private c:Lcom/google/ads/interactivemedia/v3/api/BaseDisplayContainer;

.field private d:Lcom/google/obf/hz;

.field private e:Lcom/google/obf/ia;

.field private f:Landroid/content/Context;

.field private g:Lcom/google/ads/interactivemedia/v3/impl/data/b;

.field private h:Lcom/google/obf/gs$a;

.field private i:Lcom/google/obf/hl;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/obf/hl;Lcom/google/obf/hj;Lcom/google/ads/interactivemedia/v3/api/BaseDisplayContainer;Landroid/content/Context;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/ads/interactivemedia/v3/api/AdError;
        }
    .end annotation

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-virtual {p2}, Lcom/google/obf/hl;->b()Lcom/google/obf/hi$a;

    move-result-object v0

    sget-object v1, Lcom/google/obf/hi$a;->a:Lcom/google/obf/hi$a;

    if-eq v0, v1, :cond_0

    .line 3
    invoke-virtual {p2}, Lcom/google/obf/hl;->b()Lcom/google/obf/hi$a;

    move-result-object v0

    sget-object v1, Lcom/google/obf/hi$a;->b:Lcom/google/obf/hi$a;

    if-eq v0, v1, :cond_0

    .line 4
    new-instance v0, Lcom/google/ads/interactivemedia/v3/api/AdError;

    sget-object v1, Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;->PLAY:Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;

    sget-object v2, Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;->INTERNAL_ERROR:Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;

    .line 5
    invoke-virtual {p2}, Lcom/google/obf/hl;->b()Lcom/google/obf/hi$a;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x32

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "AdsManagerUi is used for an unsupported UI style: "

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/ads/interactivemedia/v3/api/AdError;-><init>(Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;Ljava/lang/String;)V

    throw v0

    .line 6
    :cond_0
    iput-object p2, p0, Lcom/google/obf/gs;->i:Lcom/google/obf/hl;

    .line 7
    iput-object p3, p0, Lcom/google/obf/gs;->b:Lcom/google/obf/hj;

    .line 8
    iput-object p5, p0, Lcom/google/obf/gs;->f:Landroid/content/Context;

    .line 9
    iput-object p1, p0, Lcom/google/obf/gs;->a:Ljava/lang/String;

    .line 10
    iput-object p4, p0, Lcom/google/obf/gs;->c:Lcom/google/ads/interactivemedia/v3/api/BaseDisplayContainer;

    .line 11
    new-instance v0, Lcom/google/obf/gs$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/obf/gs$a;-><init>(Lcom/google/obf/gs;Lcom/google/obf/gs$1;)V

    iput-object v0, p0, Lcom/google/obf/gs;->h:Lcom/google/obf/gs$a;

    .line 12
    return-void
.end method

.method static synthetic a(Lcom/google/obf/gs;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/obf/gs;->a:Ljava/lang/String;

    return-object v0
.end method

.method private a(Lcom/google/ads/interactivemedia/v3/api/Ad;)V
    .locals 5

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/obf/gs;->i:Lcom/google/obf/hl;

    invoke-virtual {v0}, Lcom/google/obf/hl;->b()Lcom/google/obf/hi$a;

    move-result-object v0

    sget-object v1, Lcom/google/obf/hi$a;->a:Lcom/google/obf/hi$a;

    if-ne v0, v1, :cond_0

    .line 34
    invoke-static {p1}, Lcom/google/obf/hy;->a(Lcom/google/ads/interactivemedia/v3/api/Ad;)Lcom/google/obf/hy;

    move-result-object v0

    .line 35
    new-instance v1, Lcom/google/obf/hz;

    iget-object v2, p0, Lcom/google/obf/gs;->f:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/obf/gs;->b:Lcom/google/obf/hj;

    iget-object v4, p0, Lcom/google/obf/gs;->a:Ljava/lang/String;

    invoke-direct {v1, v2, v0, v3, v4}, Lcom/google/obf/hz;-><init>(Landroid/content/Context;Lcom/google/obf/hy;Lcom/google/obf/hj;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/obf/gs;->d:Lcom/google/obf/hz;

    .line 36
    iget-object v0, p0, Lcom/google/obf/gs;->b:Lcom/google/obf/hj;

    iget-object v1, p0, Lcom/google/obf/gs;->d:Lcom/google/obf/hz;

    iget-object v2, p0, Lcom/google/obf/gs;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/obf/hj;->a(Lcom/google/obf/hj$e;Ljava/lang/String;)V

    .line 37
    iget-object v0, p0, Lcom/google/obf/gs;->d:Lcom/google/obf/hz;

    iget-object v1, p0, Lcom/google/obf/gs;->h:Lcom/google/obf/gs$a;

    invoke-virtual {v0, v1}, Lcom/google/obf/hz;->a(Lcom/google/obf/hz$a;)V

    .line 38
    iget-object v0, p0, Lcom/google/obf/gs;->c:Lcom/google/ads/interactivemedia/v3/api/BaseDisplayContainer;

    invoke-interface {v0}, Lcom/google/ads/interactivemedia/v3/api/BaseDisplayContainer;->getAdContainer()Landroid/view/ViewGroup;

    move-result-object v0

    iget-object v1, p0, Lcom/google/obf/gs;->d:Lcom/google/obf/hz;

    invoke-virtual {v1}, Lcom/google/obf/hz;->a()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 39
    iget-object v0, p0, Lcom/google/obf/gs;->d:Lcom/google/obf/hz;

    invoke-virtual {v0, p1}, Lcom/google/obf/hz;->a(Lcom/google/ads/interactivemedia/v3/api/Ad;)V

    .line 43
    :goto_0
    return-void

    .line 41
    :cond_0
    new-instance v0, Lcom/google/obf/ia;

    iget-object v1, p0, Lcom/google/obf/gs;->b:Lcom/google/obf/hj;

    iget-object v2, p0, Lcom/google/obf/gs;->c:Lcom/google/ads/interactivemedia/v3/api/BaseDisplayContainer;

    invoke-interface {v2}, Lcom/google/ads/interactivemedia/v3/api/BaseDisplayContainer;->getAdContainer()Landroid/view/ViewGroup;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/obf/ia;-><init>(Lcom/google/obf/hj;Landroid/view/ViewGroup;)V

    iput-object v0, p0, Lcom/google/obf/gs;->e:Lcom/google/obf/ia;

    .line 42
    iget-object v0, p0, Lcom/google/obf/gs;->e:Lcom/google/obf/ia;

    invoke-virtual {v0}, Lcom/google/obf/ia;->a()V

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/obf/gs;)Lcom/google/obf/hj;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/obf/gs;->b:Lcom/google/obf/hj;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 19
    iget-object v0, p0, Lcom/google/obf/gs;->d:Lcom/google/obf/hz;

    if-eqz v0, :cond_1

    .line 20
    iget-object v0, p0, Lcom/google/obf/gs;->d:Lcom/google/obf/hz;

    invoke-virtual {v0}, Lcom/google/obf/hz;->b()V

    .line 21
    iget-object v0, p0, Lcom/google/obf/gs;->c:Lcom/google/ads/interactivemedia/v3/api/BaseDisplayContainer;

    invoke-interface {v0}, Lcom/google/ads/interactivemedia/v3/api/BaseDisplayContainer;->getAdContainer()Landroid/view/ViewGroup;

    move-result-object v0

    iget-object v1, p0, Lcom/google/obf/gs;->d:Lcom/google/obf/hz;

    invoke-virtual {v1}, Lcom/google/obf/hz;->a()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 22
    iput-object v2, p0, Lcom/google/obf/gs;->d:Lcom/google/obf/hz;

    .line 23
    iget-object v0, p0, Lcom/google/obf/gs;->b:Lcom/google/obf/hj;

    iget-object v1, p0, Lcom/google/obf/gs;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/obf/hj;->a(Ljava/lang/String;)V

    .line 26
    :cond_0
    :goto_0
    iput-object v2, p0, Lcom/google/obf/gs;->g:Lcom/google/ads/interactivemedia/v3/impl/data/b;

    .line 27
    return-void

    .line 24
    :cond_1
    iget-object v0, p0, Lcom/google/obf/gs;->e:Lcom/google/obf/ia;

    if-eqz v0, :cond_0

    .line 25
    iget-object v0, p0, Lcom/google/obf/gs;->e:Lcom/google/obf/ia;

    invoke-virtual {v0}, Lcom/google/obf/ia;->b()V

    goto :goto_0
.end method

.method public a(Lcom/google/ads/interactivemedia/v3/api/player/VideoProgressUpdate;)V
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/obf/gs;->d:Lcom/google/obf/hz;

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/google/obf/gs;->d:Lcom/google/obf/hz;

    invoke-virtual {v0, p1}, Lcom/google/obf/hz;->a(Lcom/google/ads/interactivemedia/v3/api/player/VideoProgressUpdate;)V

    .line 46
    :cond_0
    return-void
.end method

.method public a(Lcom/google/ads/interactivemedia/v3/impl/data/b;)V
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/google/obf/gs;->g:Lcom/google/ads/interactivemedia/v3/impl/data/b;

    if-eqz v0, :cond_0

    .line 14
    invoke-virtual {p0}, Lcom/google/obf/gs;->b()V

    .line 15
    :cond_0
    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/b;->isLinear()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 16
    iput-object p1, p0, Lcom/google/obf/gs;->g:Lcom/google/ads/interactivemedia/v3/impl/data/b;

    .line 17
    invoke-direct {p0, p1}, Lcom/google/obf/gs;->a(Lcom/google/ads/interactivemedia/v3/api/Ad;)V

    .line 18
    :cond_1
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 28
    invoke-virtual {p0}, Lcom/google/obf/gs;->a()V

    .line 29
    iget-object v0, p0, Lcom/google/obf/gs;->e:Lcom/google/obf/ia;

    if-eqz v0, :cond_0

    .line 30
    iget-object v0, p0, Lcom/google/obf/gs;->e:Lcom/google/obf/ia;

    invoke-virtual {v0}, Lcom/google/obf/ia;->c()V

    .line 31
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/obf/gs;->e:Lcom/google/obf/ia;

    .line 32
    return-void
.end method
