.class Lru/cn/tv/mobile/NewActivity$14;
.super Ljava/lang/Object;
.source "NewActivity.java"

# interfaces
.implements Lru/cn/tv/WebviewFragment$WebviewFragmentListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/tv/mobile/NewActivity;->showBilling(JJZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/tv/mobile/NewActivity;


# direct methods
.method constructor <init>(Lru/cn/tv/mobile/NewActivity;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/tv/mobile/NewActivity;

    .prologue
    .line 814
    iput-object p1, p0, Lru/cn/tv/mobile/NewActivity$14;->this$0:Lru/cn/tv/mobile/NewActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleUrl(Ljava/lang/String;)V
    .locals 1
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 817
    const-string v0, "http://close/"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 818
    iget-object v0, p0, Lru/cn/tv/mobile/NewActivity$14;->this$0:Lru/cn/tv/mobile/NewActivity;

    invoke-static {v0}, Lru/cn/tv/mobile/NewActivity;->access$1800(Lru/cn/tv/mobile/NewActivity;)V

    .line 820
    :cond_0
    return-void
.end method

.method public onError(ILjava/lang/String;)V
    .locals 0
    .param p1, "code"    # I
    .param p2, "description"    # Ljava/lang/String;

    .prologue
    .line 836
    return-void
.end method

.method public onPageFinishLoading(Landroid/webkit/WebView;)V
    .locals 3
    .param p1, "webView"    # Landroid/webkit/WebView;

    .prologue
    .line 829
    const-string v0, "NewActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Billing history size:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/webkit/WebView;->copyBackForwardList()Landroid/webkit/WebBackForwardList;

    move-result-object v2

    invoke-virtual {v2}, Landroid/webkit/WebBackForwardList;->getSize()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 830
    iget-object v0, p0, Lru/cn/tv/mobile/NewActivity$14;->this$0:Lru/cn/tv/mobile/NewActivity;

    iget-object v0, v0, Lru/cn/tv/mobile/NewActivity;->mDrawerToggle:Landroid/support/v7/app/ActionBarDrawerToggle;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBarDrawerToggle;->setDrawerIndicatorEnabled(Z)V

    .line 831
    return-void
.end method

.method public onReceivedTitle(Ljava/lang/String;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 824
    iget-object v0, p0, Lru/cn/tv/mobile/NewActivity$14;->this$0:Lru/cn/tv/mobile/NewActivity;

    invoke-virtual {v0, p1}, Lru/cn/tv/mobile/NewActivity;->setDefaultTitle(Ljava/lang/String;)V

    .line 825
    return-void
.end method
