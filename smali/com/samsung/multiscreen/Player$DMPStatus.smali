.class Lcom/samsung/multiscreen/Player$DMPStatus;
.super Ljava/lang/Object;
.source "Player.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/multiscreen/Player;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DMPStatus"
.end annotation


# instance fields
.field mAppName:Ljava/lang/String;

.field mDMPRunning:Ljava/lang/Boolean;

.field mRunning:Ljava/lang/Boolean;

.field mVisible:Ljava/lang/Boolean;

.field final synthetic this$0:Lcom/samsung/multiscreen/Player;


# direct methods
.method constructor <init>(Lcom/samsung/multiscreen/Player;)V
    .locals 2
    .param p1, "this$0"    # Lcom/samsung/multiscreen/Player;

    .prologue
    const/4 v1, 0x0

    .line 157
    iput-object p1, p0, Lcom/samsung/multiscreen/Player$DMPStatus;->this$0:Lcom/samsung/multiscreen/Player;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 158
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/multiscreen/Player$DMPStatus;->mVisible:Ljava/lang/Boolean;

    .line 159
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/multiscreen/Player$DMPStatus;->mDMPRunning:Ljava/lang/Boolean;

    .line 160
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/multiscreen/Player$DMPStatus;->mRunning:Ljava/lang/Boolean;

    .line 161
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/multiscreen/Player$DMPStatus;->mAppName:Ljava/lang/String;

    .line 162
    return-void
.end method

.method constructor <init>(Lcom/samsung/multiscreen/Player;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/samsung/multiscreen/Player;
    .param p2, "visible"    # Ljava/lang/Boolean;
    .param p3, "dmpRunning"    # Ljava/lang/Boolean;
    .param p4, "running"    # Ljava/lang/Boolean;
    .param p5, "appName"    # Ljava/lang/String;

    .prologue
    .line 174
    iput-object p1, p0, Lcom/samsung/multiscreen/Player$DMPStatus;->this$0:Lcom/samsung/multiscreen/Player;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 175
    iput-object p2, p0, Lcom/samsung/multiscreen/Player$DMPStatus;->mVisible:Ljava/lang/Boolean;

    .line 176
    iput-object p4, p0, Lcom/samsung/multiscreen/Player$DMPStatus;->mRunning:Ljava/lang/Boolean;

    .line 177
    iput-object p3, p0, Lcom/samsung/multiscreen/Player$DMPStatus;->mDMPRunning:Ljava/lang/Boolean;

    .line 178
    iput-object p5, p0, Lcom/samsung/multiscreen/Player$DMPStatus;->mAppName:Ljava/lang/String;

    .line 179
    return-void
.end method
