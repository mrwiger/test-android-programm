.class Lru/cn/draggableview/Transformer;
.super Ljava/lang/Object;
.source "Transformer.java"


# instance fields
.field dragVectorEndX:F

.field dragVectorEndY:F

.field dragVectorStartX:F

.field dragVectorStartY:F

.field private final firstView:Landroid/view/View;

.field private horizontalDrugOffset:F

.field private isSecondViewVisible:Z

.field private isThirdViewVisible:Z

.field lastDragOrientation:Lru/cn/draggableview/DraggableView$DragOrientation;

.field private marginBottom:F

.field private marginRight:F

.field private maxHeight:I

.field private maxWidth:I

.field private minHeight:I

.field private minWidth:I

.field private final parent:Landroid/view/View;

.field private secondView:Landroid/view/View;

.field private thirdView:Landroid/view/View;

.field private verticalDragOffset:F


# direct methods
.method public constructor <init>(Landroid/view/View;Landroid/view/View;Landroid/view/View;Landroid/view/View;)V
    .locals 2
    .param p1, "parent"    # Landroid/view/View;
    .param p2, "firstView"    # Landroid/view/View;
    .param p3, "secondView"    # Landroid/view/View;
    .param p4, "thirdView"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput v0, p0, Lru/cn/draggableview/Transformer;->verticalDragOffset:F

    .line 17
    iput v0, p0, Lru/cn/draggableview/Transformer;->horizontalDrugOffset:F

    .line 27
    iput-boolean v1, p0, Lru/cn/draggableview/Transformer;->isSecondViewVisible:Z

    .line 28
    iput-boolean v1, p0, Lru/cn/draggableview/Transformer;->isThirdViewVisible:Z

    .line 128
    sget-object v0, Lru/cn/draggableview/DraggableView$DragOrientation;->ORIENTATION_UNSPEC:Lru/cn/draggableview/DraggableView$DragOrientation;

    iput-object v0, p0, Lru/cn/draggableview/Transformer;->lastDragOrientation:Lru/cn/draggableview/DraggableView$DragOrientation;

    .line 48
    iput-object p1, p0, Lru/cn/draggableview/Transformer;->parent:Landroid/view/View;

    .line 49
    iput-object p2, p0, Lru/cn/draggableview/Transformer;->firstView:Landroid/view/View;

    .line 50
    iput-object p3, p0, Lru/cn/draggableview/Transformer;->secondView:Landroid/view/View;

    .line 51
    iput-object p4, p0, Lru/cn/draggableview/Transformer;->thirdView:Landroid/view/View;

    .line 52
    return-void
.end method

.method private static calculateSize(FFF)F
    .locals 2
    .param p0, "min"    # F
    .param p1, "max"    # F
    .param p2, "offset"    # F

    .prologue
    .line 131
    sub-float v0, p1, p0

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr v1, p2

    mul-float/2addr v0, v1

    add-float/2addr v0, p0

    return v0
.end method


# virtual methods
.method computeOffsets(FF)V
    .locals 8
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    const/high16 v7, 0x3f800000    # 1.0f

    .line 201
    iget-object v5, p0, Lru/cn/draggableview/Transformer;->lastDragOrientation:Lru/cn/draggableview/DraggableView$DragOrientation;

    sget-object v6, Lru/cn/draggableview/DraggableView$DragOrientation;->ORIENTATION_VERTICAL:Lru/cn/draggableview/DraggableView$DragOrientation;

    if-ne v5, v6, :cond_1

    .line 202
    iget-object v5, p0, Lru/cn/draggableview/Transformer;->parent:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    int-to-float v5, v5

    iget v6, p0, Lru/cn/draggableview/Transformer;->marginBottom:F

    sub-float/2addr v5, v6

    invoke-virtual {p0}, Lru/cn/draggableview/Transformer;->getMinHeight()I

    move-result v6

    int-to-float v6, v6

    sub-float/2addr v5, v6

    float-to-int v1, v5

    .line 203
    .local v1, "maxY":I
    const/4 v3, 0x0

    .line 205
    .local v3, "minY":I
    int-to-float v5, v1

    int-to-float v6, v3

    sub-float/2addr v5, v6

    div-float v4, p2, v5

    .line 206
    .local v4, "off":F
    const/4 v5, 0x0

    invoke-virtual {p0, v5}, Lru/cn/draggableview/Transformer;->setHorizontalDragOffset(F)V

    .line 207
    invoke-virtual {p0, v4}, Lru/cn/draggableview/Transformer;->setVerticalDragOffset(F)V

    .line 216
    .end local v1    # "maxY":I
    .end local v3    # "minY":I
    .end local v4    # "off":F
    :cond_0
    :goto_0
    return-void

    .line 208
    :cond_1
    iget-object v5, p0, Lru/cn/draggableview/Transformer;->lastDragOrientation:Lru/cn/draggableview/DraggableView$DragOrientation;

    sget-object v6, Lru/cn/draggableview/DraggableView$DragOrientation;->ORIENTATION_HORIZONTAL:Lru/cn/draggableview/DraggableView$DragOrientation;

    if-ne v5, v6, :cond_0

    .line 209
    iget-object v5, p0, Lru/cn/draggableview/Transformer;->parent:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    int-to-float v5, v5

    iget v6, p0, Lru/cn/draggableview/Transformer;->marginRight:F

    sub-float/2addr v5, v6

    invoke-virtual {p0}, Lru/cn/draggableview/Transformer;->getMinWidth()I

    move-result v6

    int-to-float v6, v6

    sub-float/2addr v5, v6

    float-to-int v0, v5

    .line 210
    .local v0, "maxX":I
    const/4 v2, 0x0

    .line 212
    .local v2, "minX":I
    int-to-float v5, v0

    int-to-float v6, v2

    sub-float/2addr v5, v6

    div-float v5, p1, v5

    sub-float v4, v7, v5

    .line 213
    .restart local v4    # "off":F
    invoke-virtual {p0, v4}, Lru/cn/draggableview/Transformer;->setHorizontalDragOffset(F)V

    .line 214
    invoke-virtual {p0, v7}, Lru/cn/draggableview/Transformer;->setVerticalDragOffset(F)V

    goto :goto_0
.end method

.method drag(Landroid/view/MotionEvent;)V
    .locals 6
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 179
    new-instance v0, Lru/cn/draggableview/Vector2f;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    iget v4, p0, Lru/cn/draggableview/Transformer;->dragVectorStartX:F

    sub-float/2addr v3, v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    iget v5, p0, Lru/cn/draggableview/Transformer;->dragVectorStartY:F

    sub-float/2addr v4, v5

    invoke-direct {v0, v3, v4}, Lru/cn/draggableview/Vector2f;-><init>(FF)V

    .line 181
    .local v0, "a":Lru/cn/draggableview/Vector2f;
    new-instance v1, Lru/cn/draggableview/Vector2f;

    iget v3, p0, Lru/cn/draggableview/Transformer;->dragVectorEndX:F

    iget v4, p0, Lru/cn/draggableview/Transformer;->dragVectorStartX:F

    sub-float/2addr v3, v4

    iget v4, p0, Lru/cn/draggableview/Transformer;->dragVectorEndY:F

    iget v5, p0, Lru/cn/draggableview/Transformer;->dragVectorStartY:F

    sub-float/2addr v4, v5

    invoke-direct {v1, v3, v4}, Lru/cn/draggableview/Vector2f;-><init>(FF)V

    .line 184
    .local v1, "b":Lru/cn/draggableview/Vector2f;
    invoke-virtual {v0, v1}, Lru/cn/draggableview/Vector2f;->projection(Lru/cn/draggableview/Vector2f;)F

    move-result v3

    invoke-virtual {v1}, Lru/cn/draggableview/Vector2f;->abs()F

    move-result v4

    div-float v2, v3, v4

    .line 185
    .local v2, "off":F
    const/4 v3, 0x0

    cmpg-float v3, v2, v3

    if-gez v3, :cond_2

    .line 186
    const/4 v2, 0x0

    .line 187
    invoke-virtual {p0, p1}, Lru/cn/draggableview/Transformer;->initDragVector(Landroid/view/MotionEvent;)V

    .line 193
    :cond_0
    :goto_0
    iget-object v3, p0, Lru/cn/draggableview/Transformer;->lastDragOrientation:Lru/cn/draggableview/DraggableView$DragOrientation;

    sget-object v4, Lru/cn/draggableview/DraggableView$DragOrientation;->ORIENTATION_VERTICAL:Lru/cn/draggableview/DraggableView$DragOrientation;

    if-ne v3, v4, :cond_3

    .line 194
    invoke-virtual {p0, v2}, Lru/cn/draggableview/Transformer;->setVerticalDragOffset(F)V

    .line 198
    :cond_1
    :goto_1
    return-void

    .line 188
    :cond_2
    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v3, v2, v3

    if-lez v3, :cond_0

    .line 189
    const/high16 v2, 0x3f800000    # 1.0f

    .line 190
    invoke-virtual {p0, p1}, Lru/cn/draggableview/Transformer;->initDragVector(Landroid/view/MotionEvent;)V

    goto :goto_0

    .line 195
    :cond_3
    iget-object v3, p0, Lru/cn/draggableview/Transformer;->lastDragOrientation:Lru/cn/draggableview/DraggableView$DragOrientation;

    sget-object v4, Lru/cn/draggableview/DraggableView$DragOrientation;->ORIENTATION_HORIZONTAL:Lru/cn/draggableview/DraggableView$DragOrientation;

    if-ne v3, v4, :cond_1

    .line 196
    invoke-virtual {p0, v2}, Lru/cn/draggableview/Transformer;->setHorizontalDragOffset(F)V

    goto :goto_1
.end method

.method public getHorizontalDragOffset()F
    .locals 1

    .prologue
    .line 109
    iget v0, p0, Lru/cn/draggableview/Transformer;->horizontalDrugOffset:F

    return v0
.end method

.method public getMaxHeight()I
    .locals 1

    .prologue
    .line 70
    iget-boolean v0, p0, Lru/cn/draggableview/Transformer;->isSecondViewVisible:Z

    if-nez v0, :cond_0

    .line 71
    iget-object v0, p0, Lru/cn/draggableview/Transformer;->parent:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    .line 73
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lru/cn/draggableview/Transformer;->maxHeight:I

    goto :goto_0
.end method

.method public getMaxWidth()I
    .locals 1

    .prologue
    .line 63
    iget-boolean v0, p0, Lru/cn/draggableview/Transformer;->isThirdViewVisible:Z

    if-nez v0, :cond_0

    .line 64
    iget-object v0, p0, Lru/cn/draggableview/Transformer;->parent:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    .line 66
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lru/cn/draggableview/Transformer;->maxWidth:I

    goto :goto_0
.end method

.method public getMinHeight()I
    .locals 1

    .prologue
    .line 89
    iget v0, p0, Lru/cn/draggableview/Transformer;->minHeight:I

    return v0
.end method

.method public getMinWidth()I
    .locals 1

    .prologue
    .line 85
    iget v0, p0, Lru/cn/draggableview/Transformer;->minWidth:I

    return v0
.end method

.method public getVerticalDragOffset()F
    .locals 1

    .prologue
    .line 101
    iget v0, p0, Lru/cn/draggableview/Transformer;->verticalDragOffset:F

    return v0
.end method

.method getXDistanceToClose()I
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Lru/cn/draggableview/Transformer;->parent:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    neg-int v0, v0

    mul-int/lit8 v0, v0, 0x2

    return v0
.end method

.method getXDistanceToMaximize()I
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lru/cn/draggableview/Transformer;->firstView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getX()F

    move-result v0

    neg-float v0, v0

    float-to-int v0, v0

    return v0
.end method

.method getXDistanceToMinimize()I
    .locals 2

    .prologue
    .line 227
    iget-object v0, p0, Lru/cn/draggableview/Transformer;->parent:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lru/cn/draggableview/Transformer;->marginRight:F

    sub-float/2addr v0, v1

    iget v1, p0, Lru/cn/draggableview/Transformer;->minWidth:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iget-object v1, p0, Lru/cn/draggableview/Transformer;->firstView:Landroid/view/View;

    .line 228
    invoke-virtual {v1}, Landroid/view/View;->getX()F

    move-result v1

    sub-float/2addr v0, v1

    float-to-int v0, v0

    .line 227
    return v0
.end method

.method public getXPosition()I
    .locals 5

    .prologue
    .line 301
    iget-object v3, p0, Lru/cn/draggableview/Transformer;->parent:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    int-to-float v1, v3

    .line 302
    .local v1, "parentW":F
    invoke-virtual {p0}, Lru/cn/draggableview/Transformer;->getMinWidth()I

    move-result v3

    int-to-float v0, v3

    .line 303
    .local v0, "minW":F
    const/4 v2, 0x0

    .line 304
    .local v2, "relativeLeft":F
    iget v3, p0, Lru/cn/draggableview/Transformer;->verticalDragOffset:F

    const/high16 v4, 0x3f800000    # 1.0f

    cmpl-float v3, v3, v4

    if-nez v3, :cond_0

    .line 305
    sub-float v3, v1, v0

    iget v4, p0, Lru/cn/draggableview/Transformer;->marginRight:F

    sub-float/2addr v3, v4

    iget v4, p0, Lru/cn/draggableview/Transformer;->horizontalDrugOffset:F

    mul-float v2, v3, v4

    .line 308
    :cond_0
    sub-float v3, v1, v0

    iget v4, p0, Lru/cn/draggableview/Transformer;->marginRight:F

    sub-float/2addr v3, v4

    iget v4, p0, Lru/cn/draggableview/Transformer;->verticalDragOffset:F

    mul-float/2addr v3, v4

    sub-float/2addr v3, v2

    float-to-int v3, v3

    return v3
.end method

.method getYDistanceToClose()I
    .locals 2

    .prologue
    .line 241
    iget-object v0, p0, Lru/cn/draggableview/Transformer;->parent:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lru/cn/draggableview/Transformer;->marginBottom:F

    sub-float/2addr v0, v1

    iget v1, p0, Lru/cn/draggableview/Transformer;->minHeight:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iget-object v1, p0, Lru/cn/draggableview/Transformer;->firstView:Landroid/view/View;

    .line 242
    invoke-virtual {v1}, Landroid/view/View;->getY()F

    move-result v1

    sub-float/2addr v0, v1

    float-to-int v0, v0

    .line 241
    return v0
.end method

.method getYDistanceToMaximize()I
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Lru/cn/draggableview/Transformer;->firstView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getY()F

    move-result v0

    neg-float v0, v0

    float-to-int v0, v0

    return v0
.end method

.method getYDistanceToMinimize()I
    .locals 2

    .prologue
    .line 232
    iget-object v0, p0, Lru/cn/draggableview/Transformer;->parent:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lru/cn/draggableview/Transformer;->marginBottom:F

    sub-float/2addr v0, v1

    iget v1, p0, Lru/cn/draggableview/Transformer;->minHeight:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iget-object v1, p0, Lru/cn/draggableview/Transformer;->firstView:Landroid/view/View;

    .line 233
    invoke-virtual {v1}, Landroid/view/View;->getY()F

    move-result v1

    sub-float/2addr v0, v1

    float-to-int v0, v0

    .line 232
    return v0
.end method

.method public getYPosition()I
    .locals 4

    .prologue
    .line 312
    iget-object v2, p0, Lru/cn/draggableview/Transformer;->parent:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    int-to-float v1, v2

    .line 313
    .local v1, "parentH":F
    invoke-virtual {p0}, Lru/cn/draggableview/Transformer;->getMinHeight()I

    move-result v2

    int-to-float v0, v2

    .line 314
    .local v0, "minH":F
    sub-float v2, v1, v0

    iget v3, p0, Lru/cn/draggableview/Transformer;->marginBottom:F

    sub-float/2addr v2, v3

    iget v3, p0, Lru/cn/draggableview/Transformer;->verticalDragOffset:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    return v2
.end method

.method initDragVector(Landroid/view/MotionEvent;)V
    .locals 9
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 140
    iget-object v7, p0, Lru/cn/draggableview/Transformer;->lastDragOrientation:Lru/cn/draggableview/DraggableView$DragOrientation;

    sget-object v8, Lru/cn/draggableview/DraggableView$DragOrientation;->ORIENTATION_VERTICAL:Lru/cn/draggableview/DraggableView$DragOrientation;

    if-ne v7, v8, :cond_1

    .line 141
    invoke-virtual {p0}, Lru/cn/draggableview/Transformer;->getVerticalDragOffset()F

    move-result v1

    .line 142
    .local v1, "offset":F
    iget v7, p0, Lru/cn/draggableview/Transformer;->minWidth:I

    int-to-float v7, v7

    invoke-virtual {p0}, Lru/cn/draggableview/Transformer;->getMaxWidth()I

    move-result v8

    int-to-float v8, v8

    invoke-static {v7, v8, v1}, Lru/cn/draggableview/Transformer;->calculateSize(FFF)F

    move-result v6

    .line 143
    .local v6, "w":F
    iget v7, p0, Lru/cn/draggableview/Transformer;->minHeight:I

    int-to-float v7, v7

    invoke-virtual {p0}, Lru/cn/draggableview/Transformer;->getMaxHeight()I

    move-result v8

    int-to-float v8, v8

    invoke-static {v7, v8, v1}, Lru/cn/draggableview/Transformer;->calculateSize(FFF)F

    move-result v0

    .line 145
    .local v0, "h":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v7

    iget-object v8, p0, Lru/cn/draggableview/Transformer;->firstView:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getX()F

    move-result v8

    sub-float/2addr v7, v8

    div-float/2addr v7, v6

    .line 146
    invoke-virtual {p0}, Lru/cn/draggableview/Transformer;->getMaxWidth()I

    move-result v8

    int-to-float v8, v8

    mul-float v3, v7, v8

    .line 147
    .local v3, "relativeXStart":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    iget-object v8, p0, Lru/cn/draggableview/Transformer;->firstView:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getY()F

    move-result v8

    sub-float/2addr v7, v8

    div-float/2addr v7, v0

    .line 148
    invoke-virtual {p0}, Lru/cn/draggableview/Transformer;->getMaxHeight()I

    move-result v8

    int-to-float v8, v8

    mul-float v5, v7, v8

    .line 150
    .local v5, "relativeYStart":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v7

    iget-object v8, p0, Lru/cn/draggableview/Transformer;->firstView:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getX()F

    move-result v8

    sub-float/2addr v7, v8

    div-float/2addr v7, v6

    .line 151
    invoke-virtual {p0}, Lru/cn/draggableview/Transformer;->getMinWidth()I

    move-result v8

    int-to-float v8, v8

    mul-float v2, v7, v8

    .line 152
    .local v2, "relativeXEnd":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    iget-object v8, p0, Lru/cn/draggableview/Transformer;->firstView:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getY()F

    move-result v8

    sub-float/2addr v7, v8

    div-float/2addr v7, v0

    .line 153
    invoke-virtual {p0}, Lru/cn/draggableview/Transformer;->getMinHeight()I

    move-result v8

    int-to-float v8, v8

    mul-float v4, v7, v8

    .line 155
    .local v4, "relativeYEnd":F
    iput v3, p0, Lru/cn/draggableview/Transformer;->dragVectorStartX:F

    .line 156
    iput v5, p0, Lru/cn/draggableview/Transformer;->dragVectorStartY:F

    .line 158
    iget-object v7, p0, Lru/cn/draggableview/Transformer;->parent:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    invoke-virtual {p0}, Lru/cn/draggableview/Transformer;->getMinWidth()I

    move-result v8

    sub-int/2addr v7, v8

    int-to-float v7, v7

    iget v8, p0, Lru/cn/draggableview/Transformer;->marginRight:F

    sub-float/2addr v7, v8

    add-float/2addr v7, v2

    iput v7, p0, Lru/cn/draggableview/Transformer;->dragVectorEndX:F

    .line 160
    iget-object v7, p0, Lru/cn/draggableview/Transformer;->parent:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    invoke-virtual {p0}, Lru/cn/draggableview/Transformer;->getMinHeight()I

    move-result v8

    sub-int/2addr v7, v8

    int-to-float v7, v7

    iget v8, p0, Lru/cn/draggableview/Transformer;->marginBottom:F

    sub-float/2addr v7, v8

    add-float/2addr v7, v4

    iput v7, p0, Lru/cn/draggableview/Transformer;->dragVectorEndY:F

    .line 175
    .end local v0    # "h":F
    .end local v1    # "offset":F
    .end local v2    # "relativeXEnd":F
    .end local v3    # "relativeXStart":F
    .end local v4    # "relativeYEnd":F
    .end local v5    # "relativeYStart":F
    .end local v6    # "w":F
    :cond_0
    :goto_0
    return-void

    .line 162
    :cond_1
    iget-object v7, p0, Lru/cn/draggableview/Transformer;->lastDragOrientation:Lru/cn/draggableview/DraggableView$DragOrientation;

    sget-object v8, Lru/cn/draggableview/DraggableView$DragOrientation;->ORIENTATION_HORIZONTAL:Lru/cn/draggableview/DraggableView$DragOrientation;

    if-ne v7, v8, :cond_0

    .line 164
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v7

    iget-object v8, p0, Lru/cn/draggableview/Transformer;->firstView:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getX()F

    move-result v8

    sub-float v3, v7, v8

    .line 165
    .restart local v3    # "relativeXStart":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    iget-object v8, p0, Lru/cn/draggableview/Transformer;->firstView:Landroid/view/View;

    invoke-virtual {v8}, Landroid/view/View;->getY()F

    move-result v8

    sub-float v5, v7, v8

    .line 167
    .restart local v5    # "relativeYStart":F
    iget-object v7, p0, Lru/cn/draggableview/Transformer;->parent:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    invoke-virtual {p0}, Lru/cn/draggableview/Transformer;->getMinWidth()I

    move-result v8

    sub-int/2addr v7, v8

    int-to-float v7, v7

    iget v8, p0, Lru/cn/draggableview/Transformer;->marginRight:F

    sub-float/2addr v7, v8

    add-float/2addr v7, v3

    iput v7, p0, Lru/cn/draggableview/Transformer;->dragVectorStartX:F

    .line 169
    iget-object v7, p0, Lru/cn/draggableview/Transformer;->parent:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    invoke-virtual {p0}, Lru/cn/draggableview/Transformer;->getMinHeight()I

    move-result v8

    sub-int/2addr v7, v8

    int-to-float v7, v7

    iget v8, p0, Lru/cn/draggableview/Transformer;->marginBottom:F

    sub-float/2addr v7, v8

    add-float/2addr v7, v5

    iput v7, p0, Lru/cn/draggableview/Transformer;->dragVectorStartY:F

    .line 172
    iput v3, p0, Lru/cn/draggableview/Transformer;->dragVectorEndX:F

    .line 173
    iget v7, p0, Lru/cn/draggableview/Transformer;->dragVectorStartY:F

    iput v7, p0, Lru/cn/draggableview/Transformer;->dragVectorEndY:F

    goto :goto_0
.end method

.method setDragOrientation(Lru/cn/draggableview/DraggableView$DragOrientation;)V
    .locals 0
    .param p1, "dragOrientation"    # Lru/cn/draggableview/DraggableView$DragOrientation;

    .prologue
    .line 135
    iput-object p1, p0, Lru/cn/draggableview/Transformer;->lastDragOrientation:Lru/cn/draggableview/DraggableView$DragOrientation;

    .line 136
    return-void
.end method

.method public setHorizontalDragOffset(F)V
    .locals 0
    .param p1, "offset"    # F

    .prologue
    .line 113
    iput p1, p0, Lru/cn/draggableview/Transformer;->horizontalDrugOffset:F

    .line 114
    return-void
.end method

.method public setMarginBottom(F)V
    .locals 0
    .param p1, "margin"    # F

    .prologue
    .line 121
    iput p1, p0, Lru/cn/draggableview/Transformer;->marginBottom:F

    .line 122
    return-void
.end method

.method public setMarginRight(F)V
    .locals 0
    .param p1, "margin"    # F

    .prologue
    .line 117
    iput p1, p0, Lru/cn/draggableview/Transformer;->marginRight:F

    .line 118
    return-void
.end method

.method public setMaxHeight(I)V
    .locals 0
    .param p1, "h"    # I

    .prologue
    .line 81
    iput p1, p0, Lru/cn/draggableview/Transformer;->maxHeight:I

    .line 82
    return-void
.end method

.method public setMaxWidth(I)V
    .locals 0
    .param p1, "w"    # I

    .prologue
    .line 77
    iput p1, p0, Lru/cn/draggableview/Transformer;->maxWidth:I

    .line 78
    return-void
.end method

.method public setMinHeight(I)V
    .locals 0
    .param p1, "h"    # I

    .prologue
    .line 97
    iput p1, p0, Lru/cn/draggableview/Transformer;->minHeight:I

    .line 98
    return-void
.end method

.method public setMinWidth(I)V
    .locals 0
    .param p1, "w"    # I

    .prologue
    .line 93
    iput p1, p0, Lru/cn/draggableview/Transformer;->minWidth:I

    .line 94
    return-void
.end method

.method public setSecondView(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 55
    iput-object p1, p0, Lru/cn/draggableview/Transformer;->secondView:Landroid/view/View;

    .line 56
    return-void
.end method

.method public setSecondViewVisible(Z)V
    .locals 0
    .param p1, "visible"    # Z

    .prologue
    .line 31
    iput-boolean p1, p0, Lru/cn/draggableview/Transformer;->isSecondViewVisible:Z

    .line 32
    return-void
.end method

.method public setThirdView(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 59
    iput-object p1, p0, Lru/cn/draggableview/Transformer;->thirdView:Landroid/view/View;

    .line 60
    return-void
.end method

.method public setThirdViewVisible(Z)V
    .locals 0
    .param p1, "visible"    # Z

    .prologue
    .line 35
    iput-boolean p1, p0, Lru/cn/draggableview/Transformer;->isThirdViewVisible:Z

    .line 36
    return-void
.end method

.method public setVerticalDragOffset(F)V
    .locals 0
    .param p1, "offset"    # F

    .prologue
    .line 105
    iput p1, p0, Lru/cn/draggableview/Transformer;->verticalDragOffset:F

    .line 106
    return-void
.end method

.method public updateViews()V
    .locals 20

    .prologue
    .line 250
    invoke-virtual/range {p0 .. p0}, Lru/cn/draggableview/Transformer;->getMaxWidth()I

    move-result v16

    move/from16 v0, v16

    int-to-float v5, v0

    .line 251
    .local v5, "maxW":F
    invoke-virtual/range {p0 .. p0}, Lru/cn/draggableview/Transformer;->getMaxHeight()I

    move-result v16

    move/from16 v0, v16

    int-to-float v4, v0

    .line 252
    .local v4, "maxH":F
    invoke-virtual/range {p0 .. p0}, Lru/cn/draggableview/Transformer;->getMinWidth()I

    move-result v16

    move/from16 v0, v16

    int-to-float v7, v0

    .line 253
    .local v7, "minW":F
    invoke-virtual/range {p0 .. p0}, Lru/cn/draggableview/Transformer;->getMinHeight()I

    move-result v16

    move/from16 v0, v16

    int-to-float v6, v0

    .line 254
    .local v6, "minH":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/cn/draggableview/Transformer;->parent:Landroid/view/View;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Landroid/view/View;->getMeasuredWidth()I

    move-result v11

    .line 255
    .local v11, "parentW":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/cn/draggableview/Transformer;->parent:Landroid/view/View;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Landroid/view/View;->getMeasuredHeight()I

    move-result v10

    .line 257
    .local v10, "parentH":I
    sub-float v16, v5, v7

    move-object/from16 v0, p0

    iget v0, v0, Lru/cn/draggableview/Transformer;->verticalDragOffset:F

    move/from16 v17, v0

    mul-float v16, v16, v17

    sub-float v16, v5, v16

    move/from16 v0, v16

    float-to-int v9, v0

    .line 258
    .local v9, "newWidth":I
    sub-float v16, v4, v6

    move-object/from16 v0, p0

    iget v0, v0, Lru/cn/draggableview/Transformer;->verticalDragOffset:F

    move/from16 v17, v0

    mul-float v16, v16, v17

    sub-float v16, v4, v16

    move/from16 v0, v16

    float-to-int v8, v0

    .line 260
    .local v8, "newHeight":I
    invoke-virtual/range {p0 .. p0}, Lru/cn/draggableview/Transformer;->getXPosition()I

    move-result v3

    .line 261
    .local v3, "left":I
    invoke-virtual/range {p0 .. p0}, Lru/cn/draggableview/Transformer;->getYPosition()I

    move-result v13

    .line 262
    .local v13, "top":I
    add-int v12, v3, v9

    .line 263
    .local v12, "right":I
    add-int v2, v13, v8

    .line 267
    .local v2, "bottom":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/cn/draggableview/Transformer;->firstView:Landroid/view/View;

    move-object/from16 v16, v0

    const/high16 v17, 0x40000000    # 2.0f

    sub-int v18, v12, v3

    or-int v17, v17, v18

    const/high16 v18, 0x40000000    # 2.0f

    sub-int v19, v2, v13

    or-int v18, v18, v19

    invoke-virtual/range {v16 .. v18}, Landroid/view/View;->measure(II)V

    .line 269
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/cn/draggableview/Transformer;->firstView:Landroid/view/View;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v3, v13, v12, v2}, Landroid/view/View;->layout(IIII)V

    .line 271
    int-to-double v0, v10

    move-wide/from16 v16, v0

    const-wide v18, 0x3fd3333333333333L    # 0.3

    mul-double v16, v16, v18

    move-object/from16 v0, p0

    iget v0, v0, Lru/cn/draggableview/Transformer;->verticalDragOffset:F

    move/from16 v18, v0

    move/from16 v0, v18

    float-to-double v0, v0

    move-wide/from16 v18, v0

    mul-double v16, v16, v18

    move-wide/from16 v0, v16

    double-to-int v15, v0

    .line 272
    .local v15, "yDistanse":I
    int-to-double v0, v11

    move-wide/from16 v16, v0

    const-wide v18, 0x3fd3333333333333L    # 0.3

    mul-double v16, v16, v18

    move-object/from16 v0, p0

    iget v0, v0, Lru/cn/draggableview/Transformer;->verticalDragOffset:F

    move/from16 v18, v0

    move/from16 v0, v18

    float-to-double v0, v0

    move-wide/from16 v18, v0

    mul-double v16, v16, v18

    move-wide/from16 v0, v16

    double-to-int v14, v0

    .line 273
    .local v14, "xDistanse":I
    move v3, v14

    .line 274
    add-int v13, v2, v15

    .line 275
    add-int/2addr v12, v14

    .line 276
    add-int v16, v13, v10

    float-to-int v0, v4

    move/from16 v17, v0

    sub-int v2, v16, v17

    .line 278
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/cn/draggableview/Transformer;->secondView:Landroid/view/View;

    move-object/from16 v16, v0

    const/high16 v17, 0x40000000    # 2.0f

    sub-int v18, v12, v3

    or-int v17, v17, v18

    const/high16 v18, 0x40000000    # 2.0f

    sub-int v19, v2, v13

    or-int v18, v18, v19

    invoke-virtual/range {v16 .. v18}, Landroid/view/View;->measure(II)V

    .line 280
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/cn/draggableview/Transformer;->secondView:Landroid/view/View;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v3, v13, v12, v2}, Landroid/view/View;->layout(IIII)V

    .line 282
    move v3, v12

    .line 283
    move-object/from16 v0, p0

    iget v0, v0, Lru/cn/draggableview/Transformer;->verticalDragOffset:F

    move/from16 v16, v0

    const/high16 v17, 0x3f800000    # 1.0f

    cmpl-float v16, v16, v17

    if-nez v16, :cond_0

    .line 284
    move v3, v11

    .line 286
    :cond_0
    const/4 v13, 0x0

    .line 287
    add-int v16, v3, v11

    float-to-int v0, v5

    move/from16 v17, v0

    sub-int v12, v16, v17

    .line 288
    move v2, v10

    .line 289
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/cn/draggableview/Transformer;->thirdView:Landroid/view/View;

    move-object/from16 v16, v0

    const/high16 v17, 0x40000000    # 2.0f

    sub-int v18, v12, v3

    or-int v17, v17, v18

    const/high16 v18, 0x40000000    # 2.0f

    sub-int v19, v2, v13

    or-int v18, v18, v19

    invoke-virtual/range {v16 .. v18}, Landroid/view/View;->measure(II)V

    .line 291
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/cn/draggableview/Transformer;->thirdView:Landroid/view/View;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v3, v13, v12, v2}, Landroid/view/View;->layout(IIII)V

    .line 293
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/cn/draggableview/Transformer;->firstView:Landroid/view/View;

    move-object/from16 v16, v0

    const/high16 v17, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget v0, v0, Lru/cn/draggableview/Transformer;->horizontalDrugOffset:F

    move/from16 v18, v0

    const v19, 0x3f4ccccd    # 0.8f

    mul-float v18, v18, v19

    sub-float v17, v17, v18

    invoke-virtual/range {v16 .. v17}, Landroid/view/View;->setAlpha(F)V

    .line 294
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/cn/draggableview/Transformer;->secondView:Landroid/view/View;

    move-object/from16 v16, v0

    const/high16 v17, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget v0, v0, Lru/cn/draggableview/Transformer;->verticalDragOffset:F

    move/from16 v18, v0

    sub-float v17, v17, v18

    invoke-virtual/range {v16 .. v17}, Landroid/view/View;->setAlpha(F)V

    .line 295
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/cn/draggableview/Transformer;->thirdView:Landroid/view/View;

    move-object/from16 v16, v0

    const/high16 v17, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget v0, v0, Lru/cn/draggableview/Transformer;->verticalDragOffset:F

    move/from16 v18, v0

    sub-float v17, v17, v18

    invoke-virtual/range {v16 .. v17}, Landroid/view/View;->setAlpha(F)V

    .line 297
    move-object/from16 v0, p0

    iget-object v0, v0, Lru/cn/draggableview/Transformer;->parent:Landroid/view/View;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v16

    const/high16 v17, 0x437f0000    # 255.0f

    const/high16 v18, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget v0, v0, Lru/cn/draggableview/Transformer;->verticalDragOffset:F

    move/from16 v19, v0

    sub-float v18, v18, v19

    mul-float v17, v17, v18

    move/from16 v0, v17

    float-to-int v0, v0

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 298
    return-void
.end method
