.class public final Lcom/google/android/exoplayer2/ui/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/exoplayer2/ui/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final AspectRatioFrameLayout:[I

.field public static final AspectRatioFrameLayout_resize_mode:I = 0x0

.field public static final DefaultTimeBar:[I

.field public static final DefaultTimeBar_ad_marker_color:I = 0x0

.field public static final DefaultTimeBar_ad_marker_width:I = 0x1

.field public static final DefaultTimeBar_bar_height:I = 0x2

.field public static final DefaultTimeBar_buffered_color:I = 0x3

.field public static final DefaultTimeBar_played_ad_marker_color:I = 0x4

.field public static final DefaultTimeBar_played_color:I = 0x5

.field public static final DefaultTimeBar_scrubber_color:I = 0x6

.field public static final DefaultTimeBar_scrubber_disabled_size:I = 0x7

.field public static final DefaultTimeBar_scrubber_dragged_size:I = 0x8

.field public static final DefaultTimeBar_scrubber_drawable:I = 0x9

.field public static final DefaultTimeBar_scrubber_enabled_size:I = 0xa

.field public static final DefaultTimeBar_touch_target_height:I = 0xb

.field public static final DefaultTimeBar_unplayed_color:I = 0xc

.field public static final PlayerControlView:[I

.field public static final PlayerControlView_controller_layout_id:I = 0x0

.field public static final PlayerControlView_fastforward_increment:I = 0x1

.field public static final PlayerControlView_repeat_toggle_modes:I = 0x2

.field public static final PlayerControlView_rewind_increment:I = 0x3

.field public static final PlayerControlView_show_shuffle_button:I = 0x4

.field public static final PlayerControlView_show_timeout:I = 0x5

.field public static final PlayerView:[I

.field public static final PlayerView_auto_show:I = 0x0

.field public static final PlayerView_controller_layout_id:I = 0x1

.field public static final PlayerView_default_artwork:I = 0x2

.field public static final PlayerView_fastforward_increment:I = 0x3

.field public static final PlayerView_hide_during_ads:I = 0x4

.field public static final PlayerView_hide_on_touch:I = 0x5

.field public static final PlayerView_player_layout_id:I = 0x6

.field public static final PlayerView_repeat_toggle_modes:I = 0x7

.field public static final PlayerView_resize_mode:I = 0x8

.field public static final PlayerView_rewind_increment:I = 0x9

.field public static final PlayerView_show_shuffle_button:I = 0xa

.field public static final PlayerView_show_timeout:I = 0xb

.field public static final PlayerView_shutter_background_color:I = 0xc

.field public static final PlayerView_surface_type:I = 0xd

.field public static final PlayerView_use_artwork:I = 0xe

.field public static final PlayerView_use_controller:I = 0xf


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 121
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x7f040183

    aput v2, v0, v1

    sput-object v0, Lcom/google/android/exoplayer2/ui/R$styleable;->AspectRatioFrameLayout:[I

    .line 123
    const/16 v0, 0xd

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/exoplayer2/ui/R$styleable;->DefaultTimeBar:[I

    .line 137
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/android/exoplayer2/ui/R$styleable;->PlayerControlView:[I

    .line 144
    const/16 v0, 0x10

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/google/android/exoplayer2/ui/R$styleable;->PlayerView:[I

    return-void

    .line 123
    :array_0
    .array-data 4
        0x7f040028
        0x7f040029
        0x7f040044
        0x7f04004e
        0x7f04015e
        0x7f04015f
        0x7f040190
        0x7f040191
        0x7f040192
        0x7f040193
        0x7f040194
        0x7f040203
        0x7f040207
    .end array-data

    .line 137
    :array_1
    .array-data 4
        0x7f0400a1
        0x7f0400d9
        0x7f040182
        0x7f040185
        0x7f0401a5
        0x7f0401a6
    .end array-data

    .line 144
    :array_2
    .array-data 4
        0x7f04003d
        0x7f0400a1
        0x7f0400aa
        0x7f0400d9
        0x7f0400f2
        0x7f0400f3
        0x7f040164
        0x7f040182
        0x7f040183
        0x7f040185
        0x7f0401a5
        0x7f0401a6
        0x7f0401a7
        0x7f0401c1
        0x7f040209
        0x7f04020a
    .end array-data
.end method
