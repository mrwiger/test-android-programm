.class public Lru/cn/api/userdata/retrofit/AttributesDeserializer;
.super Ljava/lang/Object;
.source "AttributesDeserializer.java"

# interfaces
.implements Lcom/google/gson/JsonDeserializer;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/gson/JsonDeserializer",
        "<",
        "Lru/cn/api/userdata/replies/Attributes;",
        ">;"
    }
.end annotation


# instance fields
.field private final gson:Lcom/google/gson/Gson;

.field private final types:Lru/cn/api/userdata/elementclasses/ClassTypes;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Lcom/google/gson/Gson;

    invoke-direct {v0}, Lcom/google/gson/Gson;-><init>()V

    iput-object v0, p0, Lru/cn/api/userdata/retrofit/AttributesDeserializer;->gson:Lcom/google/gson/Gson;

    .line 23
    new-instance v0, Lru/cn/api/userdata/elementclasses/ClassTypes;

    invoke-direct {v0}, Lru/cn/api/userdata/elementclasses/ClassTypes;-><init>()V

    iput-object v0, p0, Lru/cn/api/userdata/retrofit/AttributesDeserializer;->types:Lru/cn/api/userdata/elementclasses/ClassTypes;

    .line 24
    return-void
.end method


# virtual methods
.method public bridge synthetic deserialize(Lcom/google/gson/JsonElement;Ljava/lang/reflect/Type;Lcom/google/gson/JsonDeserializationContext;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/gson/JsonParseException;
        }
    .end annotation

    .prologue
    .line 16
    invoke-virtual {p0, p1, p2, p3}, Lru/cn/api/userdata/retrofit/AttributesDeserializer;->deserialize(Lcom/google/gson/JsonElement;Ljava/lang/reflect/Type;Lcom/google/gson/JsonDeserializationContext;)Lru/cn/api/userdata/replies/Attributes;

    move-result-object v0

    return-object v0
.end method

.method public deserialize(Lcom/google/gson/JsonElement;Ljava/lang/reflect/Type;Lcom/google/gson/JsonDeserializationContext;)Lru/cn/api/userdata/replies/Attributes;
    .locals 6
    .param p1, "json"    # Lcom/google/gson/JsonElement;
    .param p2, "typeOfT"    # Ljava/lang/reflect/Type;
    .param p3, "context"    # Lcom/google/gson/JsonDeserializationContext;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/gson/JsonParseException;
        }
    .end annotation

    .prologue
    .line 30
    iget-object v4, p0, Lru/cn/api/userdata/retrofit/AttributesDeserializer;->gson:Lcom/google/gson/Gson;

    invoke-virtual {v4, p1, p2}, Lcom/google/gson/Gson;->fromJson(Lcom/google/gson/JsonElement;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/api/userdata/replies/Attributes;

    .line 31
    .local v0, "attributes":Lru/cn/api/userdata/replies/Attributes;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, v0, Lru/cn/api/userdata/replies/Attributes;->attributedObjects:Ljava/util/List;

    .line 33
    iget-object v4, v0, Lru/cn/api/userdata/replies/Attributes;->classNames:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 34
    .local v3, "name":Ljava/lang/String;
    iget-object v5, p0, Lru/cn/api/userdata/retrofit/AttributesDeserializer;->types:Lru/cn/api/userdata/elementclasses/ClassTypes;

    invoke-virtual {v5, v3}, Lru/cn/api/userdata/elementclasses/ClassTypes;->classType(Ljava/lang/String;)Ljava/lang/reflect/Type;

    move-result-object v2

    .line 35
    .local v2, "classType":Ljava/lang/reflect/Type;
    invoke-virtual {p1}, Lcom/google/gson/JsonElement;->getAsJsonObject()Lcom/google/gson/JsonObject;

    move-result-object v5

    invoke-interface {p3, v5, v2}, Lcom/google/gson/JsonDeserializationContext;->deserialize(Lcom/google/gson/JsonElement;Ljava/lang/reflect/Type;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/cn/api/userdata/elementclasses/BaseClass;

    .line 37
    .local v1, "classObject":Lru/cn/api/userdata/elementclasses/BaseClass;
    iget-object v5, v0, Lru/cn/api/userdata/replies/Attributes;->attributedObjects:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 40
    .end local v1    # "classObject":Lru/cn/api/userdata/elementclasses/BaseClass;
    .end local v2    # "classType":Ljava/lang/reflect/Type;
    .end local v3    # "name":Ljava/lang/String;
    :cond_0
    return-object v0
.end method
