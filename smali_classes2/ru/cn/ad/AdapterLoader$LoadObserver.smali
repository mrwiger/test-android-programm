.class final Lru/cn/ad/AdapterLoader$LoadObserver;
.super Ljava/lang/Object;
.source "AdapterLoader.java"

# interfaces
.implements Lru/cn/ad/AdAdapter$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/ad/AdapterLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "LoadObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/ad/AdapterLoader;


# direct methods
.method private constructor <init>(Lru/cn/ad/AdapterLoader;)V
    .locals 0

    .prologue
    .line 197
    iput-object p1, p0, Lru/cn/ad/AdapterLoader$LoadObserver;->this$0:Lru/cn/ad/AdapterLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lru/cn/ad/AdapterLoader;Lru/cn/ad/AdapterLoader$1;)V
    .locals 0
    .param p1, "x0"    # Lru/cn/ad/AdapterLoader;
    .param p2, "x1"    # Lru/cn/ad/AdapterLoader$1;

    .prologue
    .line 197
    invoke-direct {p0, p1}, Lru/cn/ad/AdapterLoader$LoadObserver;-><init>(Lru/cn/ad/AdapterLoader;)V

    return-void
.end method


# virtual methods
.method public onAdEnded()V
    .locals 0

    .prologue
    .line 225
    return-void
.end method

.method public onAdLoaded()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 201
    iget-object v1, p0, Lru/cn/ad/AdapterLoader$LoadObserver;->this$0:Lru/cn/ad/AdapterLoader;

    invoke-static {v1}, Lru/cn/ad/AdapterLoader;->access$100(Lru/cn/ad/AdapterLoader;)Lru/cn/ad/AdAdapter;

    move-result-object v0

    .line 203
    .local v0, "adapter":Lru/cn/ad/AdAdapter;
    iget-object v1, p0, Lru/cn/ad/AdapterLoader$LoadObserver;->this$0:Lru/cn/ad/AdapterLoader;

    invoke-static {v1}, Lru/cn/ad/AdapterLoader;->access$100(Lru/cn/ad/AdapterLoader;)Lru/cn/ad/AdAdapter;

    move-result-object v1

    invoke-virtual {v1, v2}, Lru/cn/ad/AdAdapter;->setReporter(Lru/cn/ad/AdEventReporter;)V

    .line 204
    iget-object v1, p0, Lru/cn/ad/AdapterLoader$LoadObserver;->this$0:Lru/cn/ad/AdapterLoader;

    invoke-static {v1}, Lru/cn/ad/AdapterLoader;->access$100(Lru/cn/ad/AdapterLoader;)Lru/cn/ad/AdAdapter;

    move-result-object v1

    invoke-virtual {v1, v2}, Lru/cn/ad/AdAdapter;->setListener(Lru/cn/ad/AdAdapter$Listener;)V

    .line 205
    iget-object v1, p0, Lru/cn/ad/AdapterLoader$LoadObserver;->this$0:Lru/cn/ad/AdapterLoader;

    invoke-static {v1, v2}, Lru/cn/ad/AdapterLoader;->access$102(Lru/cn/ad/AdapterLoader;Lru/cn/ad/AdAdapter;)Lru/cn/ad/AdAdapter;

    .line 207
    iget-object v1, p0, Lru/cn/ad/AdapterLoader$LoadObserver;->this$0:Lru/cn/ad/AdapterLoader;

    invoke-static {v1, v0}, Lru/cn/ad/AdapterLoader;->access$200(Lru/cn/ad/AdapterLoader;Lru/cn/ad/AdAdapter;)V

    .line 208
    return-void
.end method

.method public onAdStarted()V
    .locals 0

    .prologue
    .line 221
    return-void
.end method

.method public onError()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 212
    iget-object v0, p0, Lru/cn/ad/AdapterLoader$LoadObserver;->this$0:Lru/cn/ad/AdapterLoader;

    invoke-static {v0}, Lru/cn/ad/AdapterLoader;->access$100(Lru/cn/ad/AdapterLoader;)Lru/cn/ad/AdAdapter;

    move-result-object v0

    invoke-virtual {v0, v1}, Lru/cn/ad/AdAdapter;->setListener(Lru/cn/ad/AdAdapter$Listener;)V

    .line 213
    iget-object v0, p0, Lru/cn/ad/AdapterLoader$LoadObserver;->this$0:Lru/cn/ad/AdapterLoader;

    invoke-static {v0, v1}, Lru/cn/ad/AdapterLoader;->access$102(Lru/cn/ad/AdapterLoader;Lru/cn/ad/AdAdapter;)Lru/cn/ad/AdAdapter;

    .line 215
    iget-object v0, p0, Lru/cn/ad/AdapterLoader$LoadObserver;->this$0:Lru/cn/ad/AdapterLoader;

    iget-object v1, p0, Lru/cn/ad/AdapterLoader$LoadObserver;->this$0:Lru/cn/ad/AdapterLoader;

    invoke-static {v1}, Lru/cn/ad/AdapterLoader;->access$300(Lru/cn/ad/AdapterLoader;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Lru/cn/ad/AdapterLoader;->access$302(Lru/cn/ad/AdapterLoader;I)I

    .line 216
    iget-object v0, p0, Lru/cn/ad/AdapterLoader$LoadObserver;->this$0:Lru/cn/ad/AdapterLoader;

    invoke-static {v0}, Lru/cn/ad/AdapterLoader;->access$400(Lru/cn/ad/AdapterLoader;)V

    .line 217
    return-void
.end method
