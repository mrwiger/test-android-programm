.class public final Lcom/my/target/core/models/banners/e;
.super Lcom/my/target/ah;
.source "InterstitialAdCard.java"


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/my/target/ah;-><init>()V

    .line 31
    sget-object v0, Lcom/my/target/af;->ce:Lcom/my/target/af;

    iput-object v0, p0, Lcom/my/target/core/models/banners/e;->clickArea:Lcom/my/target/af;

    .line 32
    return-void
.end method

.method public static newCard(Lcom/my/target/core/models/banners/d;)Lcom/my/target/core/models/banners/e;
    .locals 2
    .param p0, "interstitialAdBanner"    # Lcom/my/target/core/models/banners/d;

    .prologue
    .line 11
    new-instance v0, Lcom/my/target/core/models/banners/e;

    invoke-direct {v0}, Lcom/my/target/core/models/banners/e;-><init>()V

    .line 12
    iget-object v1, p0, Lcom/my/target/core/models/banners/d;->ctaText:Ljava/lang/String;

    iput-object v1, v0, Lcom/my/target/core/models/banners/e;->ctaText:Ljava/lang/String;

    .line 13
    iget-object v1, p0, Lcom/my/target/core/models/banners/d;->navigationType:Ljava/lang/String;

    iput-object v1, v0, Lcom/my/target/core/models/banners/e;->navigationType:Ljava/lang/String;

    .line 14
    iget-object v1, p0, Lcom/my/target/core/models/banners/d;->urlscheme:Ljava/lang/String;

    iput-object v1, v0, Lcom/my/target/core/models/banners/e;->urlscheme:Ljava/lang/String;

    .line 15
    iget-object v1, p0, Lcom/my/target/core/models/banners/d;->bundleId:Ljava/lang/String;

    iput-object v1, v0, Lcom/my/target/core/models/banners/e;->bundleId:Ljava/lang/String;

    .line 16
    iget-boolean v1, p0, Lcom/my/target/core/models/banners/d;->directLink:Z

    iput-boolean v1, v0, Lcom/my/target/core/models/banners/e;->directLink:Z

    .line 17
    iget-boolean v1, p0, Lcom/my/target/core/models/banners/d;->openInBrowser:Z

    iput-boolean v1, v0, Lcom/my/target/core/models/banners/e;->openInBrowser:Z

    .line 18
    iget-boolean v1, p0, Lcom/my/target/core/models/banners/d;->usePlayStoreAction:Z

    iput-boolean v1, v0, Lcom/my/target/core/models/banners/e;->usePlayStoreAction:Z

    .line 19
    iget-object v1, p0, Lcom/my/target/core/models/banners/d;->deeplink:Ljava/lang/String;

    iput-object v1, v0, Lcom/my/target/core/models/banners/e;->deeplink:Ljava/lang/String;

    .line 20
    iget-object v1, p0, Lcom/my/target/core/models/banners/d;->clickArea:Lcom/my/target/af;

    iput-object v1, v0, Lcom/my/target/core/models/banners/e;->clickArea:Lcom/my/target/af;

    .line 21
    iget v1, p0, Lcom/my/target/core/models/banners/d;->rating:F

    iput v1, v0, Lcom/my/target/core/models/banners/e;->rating:F

    .line 22
    iget v1, p0, Lcom/my/target/core/models/banners/d;->votes:I

    iput v1, v0, Lcom/my/target/core/models/banners/e;->votes:I

    .line 23
    iget-object v1, p0, Lcom/my/target/core/models/banners/d;->domain:Ljava/lang/String;

    iput-object v1, v0, Lcom/my/target/core/models/banners/e;->domain:Ljava/lang/String;

    .line 24
    iget-object v1, p0, Lcom/my/target/core/models/banners/d;->category:Ljava/lang/String;

    iput-object v1, v0, Lcom/my/target/core/models/banners/e;->category:Ljava/lang/String;

    .line 25
    iget-object v1, p0, Lcom/my/target/core/models/banners/d;->subCategory:Ljava/lang/String;

    iput-object v1, v0, Lcom/my/target/core/models/banners/e;->subCategory:Ljava/lang/String;

    .line 26
    return-object v0
.end method
