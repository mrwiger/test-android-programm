.class Lru/cn/tv/playlists/PlaylistActionViewModel;
.super Lru/cn/mvvm/RxViewModel;
.source "PlaylistActionViewModel.java"


# instance fields
.field private final loader:Lru/cn/mvvm/RxLoader;


# direct methods
.method constructor <init>(Lru/cn/mvvm/RxLoader;)V
    .locals 0
    .param p1, "loader"    # Lru/cn/mvvm/RxLoader;

    .prologue
    .line 18
    invoke-direct {p0}, Lru/cn/mvvm/RxViewModel;-><init>()V

    .line 19
    iput-object p1, p0, Lru/cn/tv/playlists/PlaylistActionViewModel;->loader:Lru/cn/mvvm/RxLoader;

    .line 20
    return-void
.end method


# virtual methods
.method removePlaylist(Ljava/lang/String;)V
    .locals 6
    .param p1, "location"    # Ljava/lang/String;

    .prologue
    .line 23
    invoke-static {}, Lru/cn/api/provider/TvContentProviderContract;->playlistUri()Landroid/net/Uri;

    move-result-object v1

    .line 24
    .local v1, "playlistUri":Landroid/net/Uri;
    iget-object v2, p0, Lru/cn/tv/playlists/PlaylistActionViewModel;->loader:Lru/cn/mvvm/RxLoader;

    const-string v3, "location"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-virtual {v2, v1, v3, v4}, Lru/cn/mvvm/RxLoader;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v2

    .line 25
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v2

    .line 26
    invoke-virtual {v2}, Lio/reactivex/Single;->subscribe()Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 27
    .local v0, "disposable":Lio/reactivex/disposables/Disposable;
    invoke-virtual {p0, v0}, Lru/cn/tv/playlists/PlaylistActionViewModel;->bind(Lio/reactivex/disposables/Disposable;)V

    .line 28
    return-void
.end method
