.class Lcom/my/target/nativeads/NativeAppwallAd$2;
.super Ljava/lang/Object;
.source "NativeAppwallAd.java"

# interfaces
.implements Lcom/my/target/nativeads/views/AppwallAdView$AppwallAdViewListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/my/target/nativeads/NativeAppwallAd;->registerAppwallAdView(Lcom/my/target/nativeads/views/AppwallAdView;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/my/target/nativeads/NativeAppwallAd;


# direct methods
.method constructor <init>(Lcom/my/target/nativeads/NativeAppwallAd;)V
    .locals 0
    .param p1, "this$0"    # Lcom/my/target/nativeads/NativeAppwallAd;

    .prologue
    .line 190
    iput-object p1, p0, Lcom/my/target/nativeads/NativeAppwallAd$2;->this$0:Lcom/my/target/nativeads/NativeAppwallAd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBannerClick(Lcom/my/target/nativeads/banners/NativeAppwallBanner;)V
    .locals 1
    .param p1, "banner"    # Lcom/my/target/nativeads/banners/NativeAppwallBanner;

    .prologue
    .line 200
    iget-object v0, p0, Lcom/my/target/nativeads/NativeAppwallAd$2;->this$0:Lcom/my/target/nativeads/NativeAppwallAd;

    invoke-virtual {v0, p1}, Lcom/my/target/nativeads/NativeAppwallAd;->handleBannerClick(Lcom/my/target/nativeads/banners/NativeAppwallBanner;)V

    .line 201
    iget-object v0, p0, Lcom/my/target/nativeads/NativeAppwallAd$2;->this$0:Lcom/my/target/nativeads/NativeAppwallAd;

    invoke-static {v0}, Lcom/my/target/nativeads/NativeAppwallAd;->access$100(Lcom/my/target/nativeads/NativeAppwallAd;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 203
    iget-object v0, p0, Lcom/my/target/nativeads/NativeAppwallAd$2;->this$0:Lcom/my/target/nativeads/NativeAppwallAd;

    invoke-static {v0}, Lcom/my/target/nativeads/NativeAppwallAd;->access$100(Lcom/my/target/nativeads/NativeAppwallAd;)Ljava/lang/ref/WeakReference;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/nativeads/views/AppwallAdView;

    .line 204
    if-eqz v0, :cond_0

    .line 206
    invoke-virtual {v0}, Lcom/my/target/nativeads/views/AppwallAdView;->notifyDataSetChanged()V

    .line 209
    :cond_0
    return-void
.end method

.method public onBannersShow(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/my/target/nativeads/banners/NativeAppwallBanner;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 194
    .local p1, "banners":Ljava/util/List;, "Ljava/util/List<Lcom/my/target/nativeads/banners/NativeAppwallBanner;>;"
    iget-object v0, p0, Lcom/my/target/nativeads/NativeAppwallAd$2;->this$0:Lcom/my/target/nativeads/NativeAppwallAd;

    invoke-virtual {v0, p1}, Lcom/my/target/nativeads/NativeAppwallAd;->handleBannersShow(Ljava/util/List;)V

    .line 195
    return-void
.end method
