.class public Lcom/my/target/al;
.super Lcom/my/target/ak;
.source "MediaSection.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/al$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/my/target/ag;",
        ">",
        "Lcom/my/target/ak;"
    }
.end annotation


# static fields
.field public static final cP:F = 0.0f

.field public static final cQ:I = 0xa

.field public static final cR:I = -0x1


# instance fields
.field private allowClose:Z

.field private allowCloseDelay:F

.field private final cS:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/my/target/aj",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field private final cT:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/my/target/ae;",
            ">;"
        }
    .end annotation
.end field

.field private final cU:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/my/target/ae;",
            ">;"
        }
    .end annotation
.end field

.field private final cV:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/my/target/ae;",
            ">;"
        }
    .end annotation
.end field

.field private cW:I

.field private cX:I

.field private final name:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/my/target/ak;-><init>()V

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/my/target/al;->cS:Ljava/util/ArrayList;

    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/my/target/al;->cT:Ljava/util/ArrayList;

    .line 46
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/my/target/al;->cU:Ljava/util/ArrayList;

    .line 47
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/my/target/al;->cV:Ljava/util/ArrayList;

    .line 51
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/my/target/al;->allowClose:Z

    .line 52
    const/4 v0, 0x0

    iput v0, p0, Lcom/my/target/al;->allowCloseDelay:F

    .line 53
    const/16 v0, 0xa

    iput v0, p0, Lcom/my/target/al;->cW:I

    .line 54
    const/4 v0, -0x1

    iput v0, p0, Lcom/my/target/al;->cX:I

    .line 58
    iput-object p1, p0, Lcom/my/target/al;->name:Ljava/lang/String;

    .line 59
    return-void
.end method

.method public static q(Ljava/lang/String;)Lcom/my/target/al;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/my/target/al",
            "<",
            "Lcom/my/target/common/models/VideoData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 30
    invoke-static {p0}, Lcom/my/target/al;->s(Ljava/lang/String;)Lcom/my/target/al;

    move-result-object v0

    return-object v0
.end method

.method public static r(Ljava/lang/String;)Lcom/my/target/al;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/my/target/al",
            "<",
            "Lcom/my/target/common/models/AudioData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 35
    invoke-static {p0}, Lcom/my/target/al;->s(Ljava/lang/String;)Lcom/my/target/al;

    move-result-object v0

    return-object v0
.end method

.method private static s(Ljava/lang/String;)Lcom/my/target/al;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/my/target/ag;",
            ">(",
            "Ljava/lang/String;",
            ")",
            "Lcom/my/target/al",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 40
    new-instance v0, Lcom/my/target/al;

    invoke-direct {v0, p0}, Lcom/my/target/al;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public P()I
    .locals 1

    .prologue
    .line 93
    iget v0, p0, Lcom/my/target/al;->cW:I

    return v0
.end method

.method public Q()I
    .locals 1

    .prologue
    .line 98
    iget v0, p0, Lcom/my/target/al;->cX:I

    return v0
.end method

.method public R()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/my/target/aj",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 132
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/my/target/al;->cS:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public S()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/my/target/ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 137
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/my/target/al;->cU:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public T()Lcom/my/target/ae;
    .locals 2

    .prologue
    .line 148
    iget-object v0, p0, Lcom/my/target/al;->cT:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 150
    iget-object v0, p0, Lcom/my/target/al;->cT:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/ae;

    .line 152
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public U()V
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/my/target/al;->cV:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 191
    return-void
.end method

.method public V()Z
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcom/my/target/al;->cU:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/my/target/al;->cT:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(F)Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(F)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/my/target/ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 157
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 158
    iget-object v0, p0, Lcom/my/target/al;->cU:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/ae;

    .line 160
    invoke-virtual {v0}, Lcom/my/target/ae;->getPoint()F

    move-result v3

    cmpl-float v3, v3, p1

    if-nez v3, :cond_0

    .line 162
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 165
    :cond_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 167
    iget-object v0, p0, Lcom/my/target/al;->cU:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    .line 169
    :cond_2
    return-object v1
.end method

.method public a(Lcom/my/target/aj;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/my/target/aj",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 108
    iget-object v0, p0, Lcom/my/target/al;->cS:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 109
    return-void
.end method

.method public a(Lcom/my/target/aj;I)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/my/target/aj",
            "<TT;>;I)V"
        }
    .end annotation

    .prologue
    .line 113
    iget-object v0, p0, Lcom/my/target/al;->cS:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 114
    if-ltz p2, :cond_0

    if-le p2, v0, :cond_1

    .line 128
    :cond_0
    return-void

    .line 118
    :cond_1
    iget-object v0, p0, Lcom/my/target/al;->cS:Ljava/util/ArrayList;

    invoke-virtual {v0, p2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 120
    iget-object v0, p0, Lcom/my/target/al;->cV:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/ae;

    .line 122
    invoke-virtual {v0}, Lcom/my/target/ae;->getPosition()I

    move-result v2

    .line 123
    if-lt v2, p2, :cond_2

    .line 125
    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v0, v2}, Lcom/my/target/ae;->b(I)V

    goto :goto_0
.end method

.method public a(Lcom/my/target/al;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/my/target/al",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 195
    iget-object v0, p0, Lcom/my/target/al;->cS:Ljava/util/ArrayList;

    iget-object v1, p1, Lcom/my/target/al;->cS:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 196
    iget-object v0, p0, Lcom/my/target/al;->cT:Ljava/util/ArrayList;

    iget-object v1, p1, Lcom/my/target/al;->cT:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 197
    iget-object v0, p0, Lcom/my/target/al;->cU:Ljava/util/ArrayList;

    iget-object v1, p1, Lcom/my/target/al;->cU:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 198
    invoke-virtual {p1}, Lcom/my/target/al;->F()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/my/target/al;->e(Ljava/util/ArrayList;)V

    .line 199
    return-void
.end method

.method public c(Lcom/my/target/ae;)V
    .locals 1

    .prologue
    .line 174
    invoke-virtual {p1}, Lcom/my/target/ae;->A()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 176
    iget-object v0, p0, Lcom/my/target/al;->cU:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 186
    :goto_0
    return-void

    .line 178
    :cond_0
    invoke-virtual {p1}, Lcom/my/target/ae;->y()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 180
    iget-object v0, p0, Lcom/my/target/al;->cT:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 184
    :cond_1
    iget-object v0, p0, Lcom/my/target/al;->cV:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public e(I)V
    .locals 0

    .prologue
    .line 88
    iput p1, p0, Lcom/my/target/al;->cW:I

    .line 89
    return-void
.end method

.method public f(I)V
    .locals 0

    .prologue
    .line 103
    iput p1, p0, Lcom/my/target/al;->cX:I

    .line 104
    return-void
.end method

.method public getAllowCloseDelay()F
    .locals 1

    .prologue
    .line 83
    iget v0, p0, Lcom/my/target/al;->allowCloseDelay:F

    return v0
.end method

.method public getBannersCount()I
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/my/target/al;->cS:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/my/target/al;->name:Ljava/lang/String;

    return-object v0
.end method

.method public isAllowClose()Z
    .locals 1

    .prologue
    .line 78
    iget-boolean v0, p0, Lcom/my/target/al;->allowClose:Z

    return v0
.end method

.method public setAllowClose(Z)V
    .locals 0
    .param p1, "allowClose"    # Z

    .prologue
    .line 68
    .local p0, "this":Lcom/my/target/al;, "Lcom/my/target/al<TT;>;"
    iput-boolean p1, p0, Lcom/my/target/al;->allowClose:Z

    .line 69
    return-void
.end method

.method public setAllowCloseDelay(F)V
    .locals 0
    .param p1, "allowCloseDelay"    # F

    .prologue
    .line 73
    .local p0, "this":Lcom/my/target/al;, "Lcom/my/target/al<TT;>;"
    iput p1, p0, Lcom/my/target/al;->allowCloseDelay:F

    .line 74
    return-void
.end method
