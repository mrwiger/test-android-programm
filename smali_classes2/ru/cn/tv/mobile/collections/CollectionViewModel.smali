.class final Lru/cn/tv/mobile/collections/CollectionViewModel;
.super Lru/cn/mvvm/RxViewModel;
.source "CollectionViewModel.java"


# instance fields
.field private final collections:Lru/cn/domain/Collections;

.field private final rubric:Landroid/arch/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/arch/lifecycle/MutableLiveData",
            "<",
            "Lru/cn/api/catalogue/replies/Rubric;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lru/cn/domain/Collections;)V
    .locals 1
    .param p1, "collections"    # Lru/cn/domain/Collections;

    .prologue
    .line 20
    invoke-direct {p0}, Lru/cn/mvvm/RxViewModel;-><init>()V

    .line 21
    iput-object p1, p0, Lru/cn/tv/mobile/collections/CollectionViewModel;->collections:Lru/cn/domain/Collections;

    .line 23
    new-instance v0, Landroid/arch/lifecycle/MutableLiveData;

    invoke-direct {v0}, Landroid/arch/lifecycle/MutableLiveData;-><init>()V

    iput-object v0, p0, Lru/cn/tv/mobile/collections/CollectionViewModel;->rubric:Landroid/arch/lifecycle/MutableLiveData;

    .line 24
    return-void
.end method


# virtual methods
.method final synthetic lambda$setRubricId$0$CollectionViewModel(Ljava/lang/Throwable;)V
    .locals 2
    .param p1, "throwable"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 31
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Failed to get rubric"

    invoke-static {v0, v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 32
    iget-object v0, p0, Lru/cn/tv/mobile/collections/CollectionViewModel;->rubric:Landroid/arch/lifecycle/MutableLiveData;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/arch/lifecycle/MutableLiveData;->setValue(Ljava/lang/Object;)V

    .line 33
    return-void
.end method

.method rubric()Landroid/arch/lifecycle/LiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/arch/lifecycle/LiveData",
            "<",
            "Lru/cn/api/catalogue/replies/Rubric;",
            ">;"
        }
    .end annotation

    .prologue
    .line 37
    iget-object v0, p0, Lru/cn/tv/mobile/collections/CollectionViewModel;->rubric:Landroid/arch/lifecycle/MutableLiveData;

    return-object v0
.end method

.method public setRubricId(J)V
    .locals 3
    .param p1, "rubricId"    # J

    .prologue
    .line 27
    iget-object v0, p0, Lru/cn/tv/mobile/collections/CollectionViewModel;->collections:Lru/cn/domain/Collections;

    invoke-virtual {v0, p1, p2}, Lru/cn/domain/Collections;->rubric(J)Lio/reactivex/Observable;

    move-result-object v0

    .line 28
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lru/cn/tv/mobile/collections/CollectionViewModel;->rubric:Landroid/arch/lifecycle/MutableLiveData;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v1}, Lru/cn/tv/mobile/collections/CollectionViewModel$$Lambda$0;->get$Lambda(Landroid/arch/lifecycle/MutableLiveData;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    new-instance v2, Lru/cn/tv/mobile/collections/CollectionViewModel$$Lambda$1;

    invoke-direct {v2, p0}, Lru/cn/tv/mobile/collections/CollectionViewModel$$Lambda$1;-><init>(Lru/cn/tv/mobile/collections/CollectionViewModel;)V

    .line 29
    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 27
    invoke-virtual {p0, v0}, Lru/cn/tv/mobile/collections/CollectionViewModel;->bind(Lio/reactivex/disposables/Disposable;)V

    .line 34
    return-void
.end method
