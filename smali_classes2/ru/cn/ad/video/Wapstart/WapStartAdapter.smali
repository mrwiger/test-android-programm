.class public Lru/cn/ad/video/Wapstart/WapStartAdapter;
.super Lru/cn/ad/video/VAST/VastBaseAdapter;
.source "WapStartAdapter.java"


# instance fields
.field private final siteId:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lru/cn/api/money_miner/replies/AdSystem;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "system"    # Lru/cn/api/money_miner/replies/AdSystem;

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Lru/cn/ad/video/VAST/VastBaseAdapter;-><init>(Landroid/content/Context;Lru/cn/api/money_miner/replies/AdSystem;)V

    .line 29
    const-string v2, "site_id"

    invoke-virtual {p2, v2}, Lru/cn/api/money_miner/replies/AdSystem;->getParamOrThrow(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 32
    .local v1, "siteParam":Ljava/lang/String;
    :try_start_0
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lru/cn/ad/video/Wapstart/WapStartAdapter;->siteId:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 36
    return-void

    .line 33
    :catch_0
    move-exception v0

    .line 34
    .local v0, "exc":Ljava/lang/NumberFormatException;
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Incorrect or missing param \'site_id\'"

    invoke-direct {v2, v3, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method

.method static synthetic access$000(Lru/cn/ad/video/Wapstart/WapStartAdapter;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/video/Wapstart/WapStartAdapter;

    .prologue
    .line 22
    iget-object v0, p0, Lru/cn/ad/video/Wapstart/WapStartAdapter;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lru/cn/ad/video/Wapstart/WapStartAdapter;Landroid/content/Context;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/ad/video/Wapstart/WapStartAdapter;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lru/cn/ad/video/Wapstart/WapStartAdapter;->loadSync(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$200(Lru/cn/ad/video/Wapstart/WapStartAdapter;)Lru/cn/ad/AdAdapter$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/video/Wapstart/WapStartAdapter;

    .prologue
    .line 22
    iget-object v0, p0, Lru/cn/ad/video/Wapstart/WapStartAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    return-object v0
.end method

.method static synthetic access$300(Lru/cn/ad/video/Wapstart/WapStartAdapter;)Lru/cn/ad/AdAdapter$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/video/Wapstart/WapStartAdapter;

    .prologue
    .line 22
    iget-object v0, p0, Lru/cn/ad/video/Wapstart/WapStartAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    return-object v0
.end method

.method static synthetic access$400(Lru/cn/ad/video/Wapstart/WapStartAdapter;)Lru/cn/ad/AdAdapter$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/video/Wapstart/WapStartAdapter;

    .prologue
    .line 22
    iget-object v0, p0, Lru/cn/ad/video/Wapstart/WapStartAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    return-object v0
.end method

.method private loadSync(Landroid/content/Context;)V
    .locals 17
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 69
    const/4 v8, 0x0

    .line 71
    .local v8, "ipAddress":Ljava/lang/String;
    :try_start_0
    invoke-static/range {p1 .. p1}, Lru/cn/api/ServiceLocator;->getWhereAmI(Landroid/content/Context;)Lru/cn/api/registry/replies/WhereAmI;

    move-result-object v11

    .line 72
    .local v11, "whereAmI":Lru/cn/api/registry/replies/WhereAmI;
    iget-object v13, v11, Lru/cn/api/registry/replies/WhereAmI;->ipAddress:Ljava/lang/String;

    if-eqz v13, :cond_0

    .line 73
    iget-object v8, v11, Lru/cn/api/registry/replies/WhereAmI;->ipAddress:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 78
    .end local v11    # "whereAmI":Lru/cn/api/registry/replies/WhereAmI;
    :cond_0
    :goto_0
    new-instance v12, Lru/cn/ad/video/Wapstart/WapStartRequest;

    move-object/from16 v0, p0

    iget v13, v0, Lru/cn/ad/video/Wapstart/WapStartAdapter;->siteId:I

    move-object/from16 v0, p1

    invoke-direct {v12, v0, v13, v8}, Lru/cn/ad/video/Wapstart/WapStartRequest;-><init>(Landroid/content/Context;ILjava/lang/String;)V

    .line 79
    .local v12, "wpr":Lru/cn/ad/video/Wapstart/WapStartRequest;
    invoke-virtual {v12}, Lru/cn/ad/video/Wapstart/WapStartRequest;->toJson()Ljava/lang/String;

    move-result-object v5

    .line 80
    .local v5, "data":Ljava/lang/String;
    new-instance v13, Lru/cn/utils/http/HttpClient$Builder;

    invoke-direct {v13}, Lru/cn/utils/http/HttpClient$Builder;-><init>()V

    const-wide/16 v14, 0xbb8

    .line 81
    invoke-virtual {v13, v14, v15}, Lru/cn/utils/http/HttpClient$Builder;->setConnectionTimeoutMillis(J)Lru/cn/utils/http/HttpClient$Builder;

    move-result-object v13

    const-wide/16 v14, 0x1388

    .line 82
    invoke-virtual {v13, v14, v15}, Lru/cn/utils/http/HttpClient$Builder;->setSocketTimeoutMillis(J)Lru/cn/utils/http/HttpClient$Builder;

    move-result-object v13

    .line 83
    invoke-virtual {v13}, Lru/cn/utils/http/HttpClient$Builder;->build()Lru/cn/utils/http/HttpClient;

    move-result-object v3

    .line 84
    .local v3, "client":Lru/cn/utils/http/HttpClient;
    sget-object v13, Lru/cn/ad/video/Wapstart/WapStartAdapter;->defaultUserAgent:Ljava/lang/String;

    invoke-virtual {v3, v13}, Lru/cn/utils/http/HttpClient;->setUserAgent(Ljava/lang/String;)Lru/cn/utils/http/HttpClient;

    .line 87
    :try_start_1
    const-string v13, "Content-Type"

    const-string v14, "application/json"

    invoke-virtual {v3, v13, v14}, Lru/cn/utils/http/HttpClient;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    invoke-virtual {v12}, Lru/cn/ad/video/Wapstart/WapStartRequest;->getRequestUrl()Ljava/lang/String;

    move-result-object v13

    sget-object v14, Lru/cn/utils/http/HttpClient$Method;->POST:Lru/cn/utils/http/HttpClient$Method;

    invoke-virtual {v3, v13, v14, v5}, Lru/cn/utils/http/HttpClient;->sendRequest(Ljava/lang/String;Lru/cn/utils/http/HttpClient$Method;Ljava/lang/String;)V

    .line 90
    invoke-virtual {v3}, Lru/cn/utils/http/HttpClient;->getStatusCode()I

    move-result v13

    const/16 v14, 0xc8

    if-eq v13, v14, :cond_1

    .line 91
    invoke-virtual {v3}, Lru/cn/utils/http/HttpClient;->close()V

    .line 92
    move-object/from16 v0, p0

    iget-object v13, v0, Lru/cn/ad/video/Wapstart/WapStartAdapter;->LOG_TAG:Ljava/lang/String;

    const-string v14, "WapStart no adv"

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    :goto_1
    return-void

    .line 96
    :cond_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lru/cn/ad/video/Wapstart/WapStartAdapter;->LOG_TAG:Ljava/lang/String;

    const-string v14, "WapStart adv found"

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    invoke-virtual {v3}, Lru/cn/utils/http/HttpClient;->getContent()Ljava/lang/String;

    move-result-object v4

    .line 98
    .local v4, "content":Ljava/lang/String;
    invoke-static {v4}, Lru/cn/ad/video/Wapstart/WapStartResponse;->fromJson(Ljava/lang/String;)Lru/cn/ad/video/Wapstart/WapStartResponse;

    move-result-object v9

    .line 100
    .local v9, "responseObj":Lru/cn/ad/video/Wapstart/WapStartResponse;
    new-instance v10, Lru/cn/ad/video/VAST/parser/VastParser;

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-direct {v10, v13, v14}, Lru/cn/ad/video/VAST/parser/VastParser;-><init>(Ljava/io/Reader;Ljava/lang/String;)V

    .line 101
    .local v10, "vastObj":Lru/cn/ad/video/VAST/parser/VastParser;
    iget-object v13, v9, Lru/cn/ad/video/Wapstart/WapStartResponse;->seat:Ljava/util/List;

    const/4 v14, 0x0

    invoke-interface {v13, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lru/cn/ad/video/Wapstart/WapStartResponse$Seat;

    iget-object v7, v13, Lru/cn/ad/video/Wapstart/WapStartResponse$Seat;->videoTrackUri:Ljava/lang/String;

    .line 102
    .local v7, "impressionUri":Ljava/lang/String;
    invoke-virtual {v10, v7}, Lru/cn/ad/video/VAST/parser/VastParser;->setImpressionUri(Ljava/lang/String;)V

    .line 103
    iget-object v13, v9, Lru/cn/ad/video/Wapstart/WapStartResponse;->seat:Ljava/util/List;

    const/4 v14, 0x0

    invoke-interface {v13, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lru/cn/ad/video/Wapstart/WapStartResponse$Seat;

    iget-object v13, v13, Lru/cn/ad/video/Wapstart/WapStartResponse$Seat;->video:Lru/cn/ad/video/Wapstart/WapStartResponse$Seat$Video;

    iget-object v13, v13, Lru/cn/ad/video/Wapstart/WapStartResponse$Seat$Video;->vastLink:Ljava/lang/String;

    invoke-virtual {v10, v13}, Lru/cn/ad/video/VAST/parser/VastParser;->setVastAdTagUri(Ljava/lang/String;)V

    .line 104
    iget-object v13, v9, Lru/cn/ad/video/Wapstart/WapStartResponse;->seat:Ljava/util/List;

    const/4 v14, 0x0

    invoke-interface {v13, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lru/cn/ad/video/Wapstart/WapStartResponse$Seat;

    iget-object v2, v13, Lru/cn/ad/video/Wapstart/WapStartResponse$Seat;->clickTrackingUri:Ljava/lang/String;

    .line 105
    .local v2, "cLink":Ljava/lang/String;
    if-eqz v2, :cond_2

    .line 106
    invoke-virtual {v10, v2}, Lru/cn/ad/video/VAST/parser/VastParser;->addClickTrackUrl(Ljava/lang/String;)V

    .line 109
    :cond_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lru/cn/ad/video/Wapstart/WapStartAdapter;->parsers:Ljava/util/List;

    const/4 v14, 0x0

    invoke-interface {v13, v14, v10}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 110
    invoke-virtual {v10}, Lru/cn/ad/video/VAST/parser/VastParser;->getVastAdTagUri()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v13}, Lru/cn/ad/video/Wapstart/WapStartAdapter;->getVastResult(Landroid/content/Context;Ljava/lang/String;)Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 112
    .end local v2    # "cLink":Ljava/lang/String;
    .end local v4    # "content":Ljava/lang/String;
    .end local v7    # "impressionUri":Ljava/lang/String;
    .end local v9    # "responseObj":Lru/cn/ad/video/Wapstart/WapStartResponse;
    .end local v10    # "vastObj":Lru/cn/ad/video/VAST/parser/VastParser;
    :catch_0
    move-exception v6

    .line 114
    .local v6, "e":Ljava/io/IOException;
    move-object/from16 v0, p0

    iget-object v13, v0, Lru/cn/ad/video/Wapstart/WapStartAdapter;->LOG_TAG:Ljava/lang/String;

    const-string v14, "Network error"

    invoke-static {v13, v14, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 115
    .end local v6    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v6

    .line 116
    .local v6, "e":Ljava/lang/Exception;
    sget-object v13, Lru/cn/domain/statistics/inetra/ErrorCode;->ADV_ERROR_LOAD_BANNER:Lru/cn/domain/statistics/inetra/ErrorCode;

    const-string v14, "WapStartError"

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-virtual {v0, v13, v14, v15, v1}, Lru/cn/ad/video/Wapstart/WapStartAdapter;->reportError(Lru/cn/domain/statistics/inetra/ErrorCode;Ljava/lang/String;ILjava/util/Map;)V

    .line 118
    new-instance v13, Lru/cn/ad/video/AdvertisementException;

    const-string v14, "WapStart"

    invoke-direct {v13, v14, v6}, Lru/cn/ad/video/AdvertisementException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-static {v13}, Lru/cn/utils/Logger;->logException(Ljava/lang/Throwable;)V

    .line 120
    invoke-static {v6}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 75
    .end local v3    # "client":Lru/cn/utils/http/HttpClient;
    .end local v5    # "data":Ljava/lang/String;
    .end local v6    # "e":Ljava/lang/Exception;
    .end local v12    # "wpr":Lru/cn/ad/video/Wapstart/WapStartRequest;
    :catch_2
    move-exception v13

    goto/16 :goto_0
.end method


# virtual methods
.method protected onLoad()V
    .locals 2

    .prologue
    .line 40
    const/4 v0, 0x0

    iput-object v0, p0, Lru/cn/ad/video/Wapstart/WapStartAdapter;->videoAdInfo:Lru/cn/ad/video/VAST/VastBaseAdapter$VideoAdInfo;

    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lru/cn/ad/video/Wapstart/WapStartAdapter;->parsers:Ljava/util/List;

    .line 43
    new-instance v0, Lru/cn/ad/video/Wapstart/WapStartAdapter$1;

    invoke-direct {v0, p0}, Lru/cn/ad/video/Wapstart/WapStartAdapter$1;-><init>(Lru/cn/ad/video/Wapstart/WapStartAdapter;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    .line 65
    invoke-virtual {v0, v1}, Lru/cn/ad/video/Wapstart/WapStartAdapter$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 66
    return-void
.end method
