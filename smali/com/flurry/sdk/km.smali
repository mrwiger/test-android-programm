.class public Lcom/flurry/sdk/km;
.super Lcom/flurry/sdk/my;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/flurry/sdk/km$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/flurry/sdk/my",
        "<",
        "Lcom/flurry/sdk/kn;",
        ">;"
    }
.end annotation


# static fields
.field public static a:J

.field private static final e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/flurry/sdk/km;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/flurry/sdk/km;->e:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/flurry/sdk/my;-><init>()V

    .line 1197
    const-wide/16 v0, 0x7530

    sput-wide v0, Lcom/flurry/sdk/my;->b:J

    .line 1198
    sget-wide v0, Lcom/flurry/sdk/my;->b:J

    iput-wide v0, p0, Lcom/flurry/sdk/my;->d:J

    .line 39
    return-void
.end method

.method static synthetic a(Lcom/flurry/sdk/km;Lcom/flurry/sdk/ko;Lcom/flurry/sdk/kn;)V
    .locals 1

    .prologue
    .line 20228
    invoke-static {}, Lcom/flurry/sdk/kq;->c()Lcom/flurry/sdk/kq;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/flurry/sdk/kq;->b(Lcom/flurry/sdk/ko;)V

    .line 20230
    invoke-virtual {p0, p2}, Lcom/flurry/sdk/km;->c(Lcom/flurry/sdk/mx;)V

    .line 27
    return-void
.end method

.method static synthetic a(Lcom/flurry/sdk/km;Lcom/flurry/sdk/ko;Lcom/flurry/sdk/kn;Lcom/flurry/sdk/mt;)V
    .locals 6

    .prologue
    const/4 v5, 0x3

    .line 22263
    const/4 v0, 0x0

    .line 22265
    const-string v1, "Location"

    invoke-virtual {p3, v1}, Lcom/flurry/sdk/mt;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 22266
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 22267
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 23040
    iget-object v1, p2, Lcom/flurry/sdk/mx;->q:Ljava/lang/String;

    .line 22267
    invoke-static {v0, v1}, Lcom/flurry/sdk/ob;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 22271
    :cond_0
    invoke-static {}, Lcom/flurry/sdk/kq;->c()Lcom/flurry/sdk/kq;

    move-result-object v1

    .line 22272
    invoke-virtual {v1, p1, v0}, Lcom/flurry/sdk/kq;->a(Lcom/flurry/sdk/ko;Ljava/lang/String;)Z

    move-result v1

    .line 22274
    if-eqz v1, :cond_2

    .line 22275
    sget-object v2, Lcom/flurry/sdk/km;->e:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Received redirect url. Retrying: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v2, v3}, Lcom/flurry/sdk/mm;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 22281
    :goto_0
    if-eqz v1, :cond_3

    .line 23060
    iput-object v0, p2, Lcom/flurry/sdk/mx;->r:Ljava/lang/String;

    .line 23077
    iput-object v0, p3, Lcom/flurry/sdk/mv;->g:Ljava/lang/String;

    .line 22284
    const-string v0, "Location"

    .line 23252
    iget-object v1, p3, Lcom/flurry/sdk/mv;->f:Lcom/flurry/sdk/md;

    if-eqz v1, :cond_1

    iget-object v1, p3, Lcom/flurry/sdk/mv;->f:Lcom/flurry/sdk/md;

    .line 24123
    iget-object v1, v1, Lcom/flurry/sdk/md;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    .line 23252
    if-eqz v1, :cond_1

    .line 23253
    iget-object v1, p3, Lcom/flurry/sdk/mv;->f:Lcom/flurry/sdk/md;

    invoke-virtual {v1, v0}, Lcom/flurry/sdk/md;->b(Ljava/lang/Object;)Z

    .line 22285
    :cond_1
    invoke-static {}, Lcom/flurry/sdk/lw;->a()Lcom/flurry/sdk/lw;

    move-result-object v0

    invoke-virtual {v0, p0, p3}, Lcom/flurry/sdk/lw;->a(Ljava/lang/Object;Lcom/flurry/sdk/oa;)V

    :goto_1
    return-void

    .line 22277
    :cond_2
    sget-object v2, Lcom/flurry/sdk/km;->e:Ljava/lang/String;

    const-string v3, "Received redirect url. Retrying: false"

    invoke-static {v5, v2, v3}, Lcom/flurry/sdk/mm;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 22287
    :cond_3
    invoke-virtual {p0, p2}, Lcom/flurry/sdk/km;->c(Lcom/flurry/sdk/mx;)V

    goto :goto_1
.end method

.method static synthetic a(Lcom/flurry/sdk/km;Lcom/flurry/sdk/ko;Lcom/flurry/sdk/kn;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 20238
    invoke-static {}, Lcom/flurry/sdk/kq;->c()Lcom/flurry/sdk/kq;

    move-result-object v0

    invoke-virtual {v0, p1, p3}, Lcom/flurry/sdk/kq;->b(Lcom/flurry/sdk/ko;Ljava/lang/String;)Z

    move-result v0

    .line 20240
    const/4 v1, 0x3

    sget-object v2, Lcom/flurry/sdk/km;->e:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed report retrying: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/flurry/sdk/mm;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 20242
    if-eqz v0, :cond_0

    .line 20243
    invoke-virtual {p0, p2}, Lcom/flurry/sdk/km;->d(Lcom/flurry/sdk/mx;)V

    :goto_0
    return-void

    .line 20245
    :cond_0
    invoke-virtual {p0, p2}, Lcom/flurry/sdk/km;->c(Lcom/flurry/sdk/mx;)V

    goto :goto_0
.end method

.method static synthetic b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/flurry/sdk/km;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/flurry/sdk/km;Lcom/flurry/sdk/ko;Lcom/flurry/sdk/kn;)V
    .locals 4

    .prologue
    .line 20252
    const/4 v0, 0x3

    sget-object v1, Lcom/flurry/sdk/km;->e:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 21138
    iget-object v3, p2, Lcom/flurry/sdk/kn;->g:Lcom/flurry/sdk/kr;

    .line 22095
    iget-object v3, v3, Lcom/flurry/sdk/kr;->d:Ljava/lang/String;

    .line 20252
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " report sent successfully to : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 22122
    iget-object v3, p2, Lcom/flurry/sdk/kn;->l:Ljava/lang/String;

    .line 20253
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 20252
    invoke-static {v0, v1, v2}, Lcom/flurry/sdk/mm;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 20255
    invoke-static {}, Lcom/flurry/sdk/kq;->c()Lcom/flurry/sdk/kq;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/flurry/sdk/kq;->a(Lcom/flurry/sdk/ko;)V

    .line 20256
    invoke-virtual {p0, p2}, Lcom/flurry/sdk/km;->c(Lcom/flurry/sdk/mx;)V

    .line 27
    return-void
.end method


# virtual methods
.method protected final a()Lcom/flurry/sdk/mf;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/flurry/sdk/mf",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/flurry/sdk/kn;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 43
    invoke-static {}, Lcom/flurry/sdk/ly;->a()Lcom/flurry/sdk/ly;

    move-result-object v0

    .line 2098
    iget-object v0, v0, Lcom/flurry/sdk/ly;->a:Landroid/content/Context;

    .line 44
    new-instance v1, Lcom/flurry/sdk/mf;

    const-string v2, ".yflurryanpulsecallbackreporter"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    const-string v2, ".yflurryanpulsecallbackreporter"

    const/4 v3, 0x2

    new-instance v4, Lcom/flurry/sdk/km$1;

    invoke-direct {v4, p0}, Lcom/flurry/sdk/km$1;-><init>(Lcom/flurry/sdk/km;)V

    invoke-direct {v1, v0, v2, v3, v4}, Lcom/flurry/sdk/mf;-><init>(Ljava/io/File;Ljava/lang/String;ILcom/flurry/sdk/nk;)V

    return-object v1
.end method

.method protected final synthetic a(Lcom/flurry/sdk/mx;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v7, 0x1

    .line 27
    move-object v1, p1

    check-cast v1, Lcom/flurry/sdk/kn;

    .line 5094
    const/4 v0, 0x3

    sget-object v2, Lcom/flurry/sdk/km;->e:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Sending next pulse report to "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 6122
    iget-object v4, v1, Lcom/flurry/sdk/kn;->l:Ljava/lang/String;

    .line 5094
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " at: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 7056
    iget-object v4, v1, Lcom/flurry/sdk/mx;->r:Ljava/lang/String;

    .line 5095
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 5094
    invoke-static {v0, v2, v3}, Lcom/flurry/sdk/mm;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 5097
    invoke-static {}, Lcom/flurry/sdk/lj;->a()Lcom/flurry/sdk/lj;

    invoke-static {}, Lcom/flurry/sdk/lj;->d()J

    move-result-wide v2

    .line 5100
    cmp-long v0, v2, v8

    if-nez v0, :cond_0

    .line 5101
    sget-wide v2, Lcom/flurry/sdk/km;->a:J

    .line 5104
    :cond_0
    invoke-static {}, Lcom/flurry/sdk/lj;->a()Lcom/flurry/sdk/lj;

    invoke-static {}, Lcom/flurry/sdk/lj;->g()J

    move-result-wide v4

    .line 5107
    cmp-long v0, v4, v8

    if-nez v0, :cond_1

    .line 5108
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v4, v2

    .line 8032
    :cond_1
    iget v6, v1, Lcom/flurry/sdk/mx;->p:I

    .line 5114
    new-instance v0, Lcom/flurry/sdk/ko;

    invoke-direct/range {v0 .. v6}, Lcom/flurry/sdk/ko;-><init>(Lcom/flurry/sdk/kn;JJI)V

    .line 5119
    new-instance v4, Lcom/flurry/sdk/mt;

    invoke-direct {v4}, Lcom/flurry/sdk/mt;-><init>()V

    .line 8056
    iget-object v2, v1, Lcom/flurry/sdk/mx;->r:Ljava/lang/String;

    .line 8077
    iput-object v2, v4, Lcom/flurry/sdk/mv;->g:Ljava/lang/String;

    .line 9028
    const v2, 0x186a0

    iput v2, v4, Lcom/flurry/sdk/oa;->u:I

    .line 9110
    iget-object v2, v1, Lcom/flurry/sdk/kn;->d:Lcom/flurry/sdk/ku;

    .line 5124
    sget-object v3, Lcom/flurry/sdk/ku;->c:Lcom/flurry/sdk/ku;

    invoke-virtual {v2, v3}, Lcom/flurry/sdk/ku;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 5125
    new-instance v2, Lcom/flurry/sdk/nd;

    invoke-direct {v2}, Lcom/flurry/sdk/nd;-><init>()V

    .line 10027
    iput-object v2, v4, Lcom/flurry/sdk/mt;->c:Lcom/flurry/sdk/nh;

    .line 10134
    iget-object v2, v1, Lcom/flurry/sdk/kn;->k:Ljava/lang/String;

    .line 5127
    if-eqz v2, :cond_2

    .line 11134
    iget-object v2, v1, Lcom/flurry/sdk/kn;->k:Ljava/lang/String;

    .line 5128
    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    .line 12023
    iput-object v2, v4, Lcom/flurry/sdk/mt;->b:Ljava/lang/Object;

    .line 5132
    :cond_2
    sget-object v2, Lcom/flurry/sdk/mv$a;->c:Lcom/flurry/sdk/mv$a;

    .line 12085
    iput-object v2, v4, Lcom/flurry/sdk/mv;->h:Lcom/flurry/sdk/mv$a;

    .line 13126
    :goto_0
    iget v2, v1, Lcom/flurry/sdk/kn;->i:I

    .line 5138
    mul-int/lit16 v2, v2, 0x3e8

    .line 14093
    iput v2, v4, Lcom/flurry/sdk/mv;->i:I

    .line 14130
    iget v2, v1, Lcom/flurry/sdk/kn;->j:I

    .line 5140
    mul-int/lit16 v2, v2, 0x3e8

    .line 15101
    iput v2, v4, Lcom/flurry/sdk/mv;->j:I

    .line 15141
    iput-boolean v7, v4, Lcom/flurry/sdk/mv;->m:Z

    .line 16109
    iput-boolean v7, v4, Lcom/flurry/sdk/mv;->r:Z

    .line 16126
    iget v2, v1, Lcom/flurry/sdk/kn;->i:I

    .line 16130
    iget v3, v1, Lcom/flurry/sdk/kn;->j:I

    .line 5148
    add-int/2addr v2, v3

    mul-int/lit16 v2, v2, 0x3e8

    .line 17117
    iput v2, v4, Lcom/flurry/sdk/mv;->s:I

    .line 18118
    iget-object v5, v1, Lcom/flurry/sdk/kn;->e:Ljava/util/Map;

    .line 5152
    if-eqz v5, :cond_4

    .line 19118
    iget-object v2, v1, Lcom/flurry/sdk/kn;->e:Ljava/util/Map;

    .line 5153
    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    .line 5155
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 5156
    invoke-interface {v5, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v4, v2, v3}, Lcom/flurry/sdk/mt;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 5134
    :cond_3
    sget-object v2, Lcom/flurry/sdk/mv$a;->b:Lcom/flurry/sdk/mv$a;

    .line 13085
    iput-object v2, v4, Lcom/flurry/sdk/mv;->h:Lcom/flurry/sdk/mv$a;

    goto :goto_0

    .line 19137
    :cond_4
    const/4 v2, 0x0

    iput-boolean v2, v4, Lcom/flurry/sdk/mv;->k:Z

    .line 5162
    new-instance v2, Lcom/flurry/sdk/km$2;

    invoke-direct {v2, p0, v1, v0}, Lcom/flurry/sdk/km$2;-><init>(Lcom/flurry/sdk/km;Lcom/flurry/sdk/kn;Lcom/flurry/sdk/ko;)V

    .line 20035
    iput-object v2, v4, Lcom/flurry/sdk/mt;->a:Lcom/flurry/sdk/mt$a;

    .line 5222
    invoke-static {}, Lcom/flurry/sdk/lw;->a()Lcom/flurry/sdk/lw;

    move-result-object v0

    invoke-virtual {v0, p0, v4}, Lcom/flurry/sdk/lw;->a(Ljava/lang/Object;Lcom/flurry/sdk/oa;)V

    .line 27
    return-void
.end method

.method protected final declared-synchronized a(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/flurry/sdk/kn;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 59
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/flurry/sdk/kq;->c()Lcom/flurry/sdk/kq;

    invoke-static {}, Lcom/flurry/sdk/kq;->d()Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 61
    if-nez v0, :cond_1

    .line 90
    :cond_0
    monitor-exit p0

    return-void

    .line 65
    :cond_1
    :try_start_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-eqz v1, :cond_0

    .line 69
    const/4 v1, 0x3

    sget-object v2, Lcom/flurry/sdk/km;->e:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Restoring "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " from report queue."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/flurry/sdk/mm;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 71
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flurry/sdk/kr;

    .line 72
    invoke-static {}, Lcom/flurry/sdk/kq;->c()Lcom/flurry/sdk/kq;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/flurry/sdk/kq;->b(Lcom/flurry/sdk/kr;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 59
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 76
    :cond_2
    :try_start_2
    invoke-static {}, Lcom/flurry/sdk/kq;->c()Lcom/flurry/sdk/kq;

    invoke-static {}, Lcom/flurry/sdk/kq;->b()Ljava/util/List;

    move-result-object v0

    .line 79
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flurry/sdk/kr;

    .line 80
    invoke-virtual {v0}, Lcom/flurry/sdk/kr;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flurry/sdk/kn;

    .line 2146
    iget-boolean v3, v0, Lcom/flurry/sdk/kn;->m:Z

    .line 81
    if-nez v3, :cond_4

    .line 82
    const/4 v3, 0x3

    sget-object v4, Lcom/flurry/sdk/km;->e:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Callback for "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 3138
    iget-object v6, v0, Lcom/flurry/sdk/kn;->g:Lcom/flurry/sdk/kr;

    .line 4095
    iget-object v6, v6, Lcom/flurry/sdk/kr;->d:Ljava/lang/String;

    .line 82
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " to "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 4122
    iget-object v6, v0, Lcom/flurry/sdk/kn;->l:Ljava/lang/String;

    .line 83
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " not completed.  Adding to reporter queue."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 82
    invoke-static {v3, v4, v5}, Lcom/flurry/sdk/mm;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 85
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method protected final declared-synchronized b(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/flurry/sdk/kn;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 294
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/flurry/sdk/kq;->c()Lcom/flurry/sdk/kq;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flurry/sdk/kq;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 295
    monitor-exit p0

    return-void

    .line 294
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
