.class final Lcom/google/android/exoplayer2/source/dash/EventSampleStream;
.super Ljava/lang/Object;
.source "EventSampleStream.java"

# interfaces
.implements Lcom/google/android/exoplayer2/source/SampleStream;


# instance fields
.field private currentIndex:I

.field private final eventMessageEncoder:Lcom/google/android/exoplayer2/metadata/emsg/EventMessageEncoder;

.field private eventStream:Lcom/google/android/exoplayer2/source/dash/manifest/EventStream;

.field private eventStreamUpdatable:Z

.field private eventTimesUs:[J

.field private isFormatSentDownstream:Z

.field private pendingSeekPositionUs:J

.field private final upstreamFormat:Lcom/google/android/exoplayer2/Format;


# direct methods
.method constructor <init>(Lcom/google/android/exoplayer2/source/dash/manifest/EventStream;Lcom/google/android/exoplayer2/Format;Z)V
    .locals 2
    .param p1, "eventStream"    # Lcom/google/android/exoplayer2/source/dash/manifest/EventStream;
    .param p2, "upstreamFormat"    # Lcom/google/android/exoplayer2/Format;
    .param p3, "eventStreamUpdatable"    # Z

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p2, p0, Lcom/google/android/exoplayer2/source/dash/EventSampleStream;->upstreamFormat:Lcom/google/android/exoplayer2/Format;

    .line 48
    new-instance v0, Lcom/google/android/exoplayer2/metadata/emsg/EventMessageEncoder;

    invoke-direct {v0}, Lcom/google/android/exoplayer2/metadata/emsg/EventMessageEncoder;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer2/source/dash/EventSampleStream;->eventMessageEncoder:Lcom/google/android/exoplayer2/metadata/emsg/EventMessageEncoder;

    .line 49
    const-wide v0, -0x7fffffffffffffffL    # -4.9E-324

    iput-wide v0, p0, Lcom/google/android/exoplayer2/source/dash/EventSampleStream;->pendingSeekPositionUs:J

    .line 50
    invoke-virtual {p0, p1, p3}, Lcom/google/android/exoplayer2/source/dash/EventSampleStream;->updateEventStream(Lcom/google/android/exoplayer2/source/dash/manifest/EventStream;Z)V

    .line 51
    return-void
.end method


# virtual methods
.method eventStreamId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/exoplayer2/source/dash/EventSampleStream;->eventStream:Lcom/google/android/exoplayer2/source/dash/manifest/EventStream;

    invoke-virtual {v0}, Lcom/google/android/exoplayer2/source/dash/manifest/EventStream;->id()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isReady()Z
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x1

    return v0
.end method

.method public maybeThrowError()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 78
    return-void
.end method

.method public readData(Lcom/google/android/exoplayer2/FormatHolder;Lcom/google/android/exoplayer2/decoder/DecoderInputBuffer;Z)I
    .locals 9
    .param p1, "formatHolder"    # Lcom/google/android/exoplayer2/FormatHolder;
    .param p2, "buffer"    # Lcom/google/android/exoplayer2/decoder/DecoderInputBuffer;
    .param p3, "formatRequired"    # Z

    .prologue
    const/4 v8, 0x1

    const/4 v3, -0x3

    const/4 v2, -0x4

    .line 83
    if-nez p3, :cond_0

    iget-boolean v4, p0, Lcom/google/android/exoplayer2/source/dash/EventSampleStream;->isFormatSentDownstream:Z

    if-nez v4, :cond_1

    .line 84
    :cond_0
    iget-object v2, p0, Lcom/google/android/exoplayer2/source/dash/EventSampleStream;->upstreamFormat:Lcom/google/android/exoplayer2/Format;

    iput-object v2, p1, Lcom/google/android/exoplayer2/FormatHolder;->format:Lcom/google/android/exoplayer2/Format;

    .line 85
    iput-boolean v8, p0, Lcom/google/android/exoplayer2/source/dash/EventSampleStream;->isFormatSentDownstream:Z

    .line 86
    const/4 v2, -0x5

    .line 106
    :goto_0
    return v2

    .line 88
    :cond_1
    iget v4, p0, Lcom/google/android/exoplayer2/source/dash/EventSampleStream;->currentIndex:I

    iget-object v5, p0, Lcom/google/android/exoplayer2/source/dash/EventSampleStream;->eventTimesUs:[J

    array-length v5, v5

    if-ne v4, v5, :cond_3

    .line 89
    iget-boolean v4, p0, Lcom/google/android/exoplayer2/source/dash/EventSampleStream;->eventStreamUpdatable:Z

    if-nez v4, :cond_2

    .line 90
    const/4 v3, 0x4

    invoke-virtual {p2, v3}, Lcom/google/android/exoplayer2/decoder/DecoderInputBuffer;->setFlags(I)V

    goto :goto_0

    :cond_2
    move v2, v3

    .line 93
    goto :goto_0

    .line 96
    :cond_3
    iget v0, p0, Lcom/google/android/exoplayer2/source/dash/EventSampleStream;->currentIndex:I

    add-int/lit8 v4, v0, 0x1

    iput v4, p0, Lcom/google/android/exoplayer2/source/dash/EventSampleStream;->currentIndex:I

    .line 97
    .local v0, "sampleIndex":I
    iget-object v4, p0, Lcom/google/android/exoplayer2/source/dash/EventSampleStream;->eventMessageEncoder:Lcom/google/android/exoplayer2/metadata/emsg/EventMessageEncoder;

    iget-object v5, p0, Lcom/google/android/exoplayer2/source/dash/EventSampleStream;->eventStream:Lcom/google/android/exoplayer2/source/dash/manifest/EventStream;

    iget-object v5, v5, Lcom/google/android/exoplayer2/source/dash/manifest/EventStream;->events:[Lcom/google/android/exoplayer2/metadata/emsg/EventMessage;

    aget-object v5, v5, v0

    iget-object v6, p0, Lcom/google/android/exoplayer2/source/dash/EventSampleStream;->eventStream:Lcom/google/android/exoplayer2/source/dash/manifest/EventStream;

    iget-wide v6, v6, Lcom/google/android/exoplayer2/source/dash/manifest/EventStream;->timescale:J

    invoke-virtual {v4, v5, v6, v7}, Lcom/google/android/exoplayer2/metadata/emsg/EventMessageEncoder;->encode(Lcom/google/android/exoplayer2/metadata/emsg/EventMessage;J)[B

    move-result-object v1

    .line 99
    .local v1, "serializedEvent":[B
    if-eqz v1, :cond_4

    .line 100
    array-length v3, v1

    invoke-virtual {p2, v3}, Lcom/google/android/exoplayer2/decoder/DecoderInputBuffer;->ensureSpaceForWrite(I)V

    .line 101
    invoke-virtual {p2, v8}, Lcom/google/android/exoplayer2/decoder/DecoderInputBuffer;->setFlags(I)V

    .line 102
    iget-object v3, p2, Lcom/google/android/exoplayer2/decoder/DecoderInputBuffer;->data:Ljava/nio/ByteBuffer;

    invoke-virtual {v3, v1}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 103
    iget-object v3, p0, Lcom/google/android/exoplayer2/source/dash/EventSampleStream;->eventTimesUs:[J

    aget-wide v4, v3, v0

    iput-wide v4, p2, Lcom/google/android/exoplayer2/decoder/DecoderInputBuffer;->timeUs:J

    goto :goto_0

    :cond_4
    move v2, v3

    .line 106
    goto :goto_0
.end method

.method public seekToUs(J)V
    .locals 5
    .param p1, "positionUs"    # J

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 125
    iget-object v2, p0, Lcom/google/android/exoplayer2/source/dash/EventSampleStream;->eventTimesUs:[J

    invoke-static {v2, p1, p2, v0, v1}, Lcom/google/android/exoplayer2/util/Util;->binarySearchCeil([JJZZ)I

    move-result v2

    iput v2, p0, Lcom/google/android/exoplayer2/source/dash/EventSampleStream;->currentIndex:I

    .line 126
    iget-boolean v2, p0, Lcom/google/android/exoplayer2/source/dash/EventSampleStream;->eventStreamUpdatable:Z

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/google/android/exoplayer2/source/dash/EventSampleStream;->currentIndex:I

    iget-object v3, p0, Lcom/google/android/exoplayer2/source/dash/EventSampleStream;->eventTimesUs:[J

    array-length v3, v3

    if-ne v2, v3, :cond_0

    .line 127
    .local v0, "isPendingSeek":Z
    :goto_0
    if-eqz v0, :cond_1

    .end local p1    # "positionUs":J
    :goto_1
    iput-wide p1, p0, Lcom/google/android/exoplayer2/source/dash/EventSampleStream;->pendingSeekPositionUs:J

    .line 128
    return-void

    .end local v0    # "isPendingSeek":Z
    .restart local p1    # "positionUs":J
    :cond_0
    move v0, v1

    .line 126
    goto :goto_0

    .line 127
    .restart local v0    # "isPendingSeek":Z
    :cond_1
    const-wide p1, -0x7fffffffffffffffL    # -4.9E-324

    goto :goto_1
.end method

.method public skipData(J)I
    .locals 7
    .param p1, "positionUs"    # J

    .prologue
    .line 112
    iget v2, p0, Lcom/google/android/exoplayer2/source/dash/EventSampleStream;->currentIndex:I

    iget-object v3, p0, Lcom/google/android/exoplayer2/source/dash/EventSampleStream;->eventTimesUs:[J

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 113
    invoke-static {v3, p1, p2, v4, v5}, Lcom/google/android/exoplayer2/util/Util;->binarySearchCeil([JJZZ)I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 114
    .local v0, "newIndex":I
    iget v2, p0, Lcom/google/android/exoplayer2/source/dash/EventSampleStream;->currentIndex:I

    sub-int v1, v0, v2

    .line 115
    .local v1, "skipped":I
    iput v0, p0, Lcom/google/android/exoplayer2/source/dash/EventSampleStream;->currentIndex:I

    .line 116
    return v1
.end method

.method updateEventStream(Lcom/google/android/exoplayer2/source/dash/manifest/EventStream;Z)V
    .locals 7
    .param p1, "eventStream"    # Lcom/google/android/exoplayer2/source/dash/manifest/EventStream;
    .param p2, "eventStreamUpdatable"    # Z

    .prologue
    const/4 v6, 0x0

    const-wide v2, -0x7fffffffffffffffL    # -4.9E-324

    .line 54
    iget v4, p0, Lcom/google/android/exoplayer2/source/dash/EventSampleStream;->currentIndex:I

    if-nez v4, :cond_1

    move-wide v0, v2

    .line 56
    .local v0, "lastReadPositionUs":J
    :goto_0
    iput-boolean p2, p0, Lcom/google/android/exoplayer2/source/dash/EventSampleStream;->eventStreamUpdatable:Z

    .line 57
    iput-object p1, p0, Lcom/google/android/exoplayer2/source/dash/EventSampleStream;->eventStream:Lcom/google/android/exoplayer2/source/dash/manifest/EventStream;

    .line 58
    iget-object v4, p1, Lcom/google/android/exoplayer2/source/dash/manifest/EventStream;->presentationTimesUs:[J

    iput-object v4, p0, Lcom/google/android/exoplayer2/source/dash/EventSampleStream;->eventTimesUs:[J

    .line 59
    iget-wide v4, p0, Lcom/google/android/exoplayer2/source/dash/EventSampleStream;->pendingSeekPositionUs:J

    cmp-long v4, v4, v2

    if-eqz v4, :cond_2

    .line 60
    iget-wide v2, p0, Lcom/google/android/exoplayer2/source/dash/EventSampleStream;->pendingSeekPositionUs:J

    invoke-virtual {p0, v2, v3}, Lcom/google/android/exoplayer2/source/dash/EventSampleStream;->seekToUs(J)V

    .line 64
    :cond_0
    :goto_1
    return-void

    .line 54
    .end local v0    # "lastReadPositionUs":J
    :cond_1
    iget-object v4, p0, Lcom/google/android/exoplayer2/source/dash/EventSampleStream;->eventTimesUs:[J

    iget v5, p0, Lcom/google/android/exoplayer2/source/dash/EventSampleStream;->currentIndex:I

    add-int/lit8 v5, v5, -0x1

    aget-wide v0, v4, v5

    goto :goto_0

    .line 61
    .restart local v0    # "lastReadPositionUs":J
    :cond_2
    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    .line 62
    iget-object v2, p0, Lcom/google/android/exoplayer2/source/dash/EventSampleStream;->eventTimesUs:[J

    invoke-static {v2, v0, v1, v6, v6}, Lcom/google/android/exoplayer2/util/Util;->binarySearchCeil([JJZZ)I

    move-result v2

    iput v2, p0, Lcom/google/android/exoplayer2/source/dash/EventSampleStream;->currentIndex:I

    goto :goto_1
.end method
