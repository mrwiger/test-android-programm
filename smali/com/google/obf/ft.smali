.class public final Lcom/google/obf/ft;
.super Lcom/google/obf/gg;
.source "IMASDK"


# static fields
.field private static final a:Ljava/io/Writer;

.field private static final b:Lcom/google/obf/er;


# instance fields
.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/obf/em;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/lang/String;

.field private e:Lcom/google/obf/em;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 77
    new-instance v0, Lcom/google/obf/ft$1;

    invoke-direct {v0}, Lcom/google/obf/ft$1;-><init>()V

    sput-object v0, Lcom/google/obf/ft;->a:Ljava/io/Writer;

    .line 78
    new-instance v0, Lcom/google/obf/er;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Lcom/google/obf/er;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/obf/ft;->b:Lcom/google/obf/er;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lcom/google/obf/ft;->a:Ljava/io/Writer;

    invoke-direct {p0, v0}, Lcom/google/obf/gg;-><init>(Ljava/io/Writer;)V

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/obf/ft;->c:Ljava/util/List;

    .line 3
    sget-object v0, Lcom/google/obf/eo;->a:Lcom/google/obf/eo;

    iput-object v0, p0, Lcom/google/obf/ft;->e:Lcom/google/obf/em;

    .line 4
    return-void
.end method

.method private a(Lcom/google/obf/em;)V
    .locals 2

    .prologue
    .line 9
    iget-object v0, p0, Lcom/google/obf/ft;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 10
    invoke-virtual {p1}, Lcom/google/obf/em;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/obf/ft;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 11
    :cond_0
    invoke-direct {p0}, Lcom/google/obf/ft;->j()Lcom/google/obf/em;

    move-result-object v0

    check-cast v0, Lcom/google/obf/ep;

    .line 12
    iget-object v1, p0, Lcom/google/obf/ft;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/google/obf/ep;->a(Ljava/lang/String;Lcom/google/obf/em;)V

    .line 13
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/obf/ft;->d:Ljava/lang/String;

    .line 20
    :goto_0
    return-void

    .line 14
    :cond_2
    iget-object v0, p0, Lcom/google/obf/ft;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 15
    iput-object p1, p0, Lcom/google/obf/ft;->e:Lcom/google/obf/em;

    goto :goto_0

    .line 16
    :cond_3
    invoke-direct {p0}, Lcom/google/obf/ft;->j()Lcom/google/obf/em;

    move-result-object v0

    .line 17
    instance-of v1, v0, Lcom/google/obf/ej;

    if-eqz v1, :cond_4

    .line 18
    check-cast v0, Lcom/google/obf/ej;

    invoke-virtual {v0, p1}, Lcom/google/obf/ej;->a(Lcom/google/obf/em;)V

    goto :goto_0

    .line 19
    :cond_4
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method private j()Lcom/google/obf/em;
    .locals 2

    .prologue
    .line 8
    iget-object v0, p0, Lcom/google/obf/ft;->c:Ljava/util/List;

    iget-object v1, p0, Lcom/google/obf/ft;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/em;

    return-object v0
.end method


# virtual methods
.method public a()Lcom/google/obf/em;
    .locals 3

    .prologue
    .line 5
    iget-object v0, p0, Lcom/google/obf/ft;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 6
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected one JSON element but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/obf/ft;->c:Ljava/util/List;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 7
    :cond_0
    iget-object v0, p0, Lcom/google/obf/ft;->e:Lcom/google/obf/em;

    return-object v0
.end method

.method public a(J)Lcom/google/obf/gg;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 62
    new-instance v0, Lcom/google/obf/er;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/obf/er;-><init>(Ljava/lang/Number;)V

    invoke-direct {p0, v0}, Lcom/google/obf/ft;->a(Lcom/google/obf/em;)V

    .line 63
    return-object p0
.end method

.method public a(Ljava/lang/Boolean;)Lcom/google/obf/gg;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 58
    if-nez p1, :cond_0

    .line 59
    invoke-virtual {p0}, Lcom/google/obf/ft;->f()Lcom/google/obf/gg;

    move-result-object p0

    .line 61
    :goto_0
    return-object p0

    .line 60
    :cond_0
    new-instance v0, Lcom/google/obf/er;

    invoke-direct {v0, p1}, Lcom/google/obf/er;-><init>(Ljava/lang/Boolean;)V

    invoke-direct {p0, v0}, Lcom/google/obf/ft;->a(Lcom/google/obf/em;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/Number;)Lcom/google/obf/gg;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 64
    if-nez p1, :cond_0

    .line 65
    invoke-virtual {p0}, Lcom/google/obf/ft;->f()Lcom/google/obf/gg;

    move-result-object p0

    .line 71
    :goto_0
    return-object p0

    .line 66
    :cond_0
    invoke-virtual {p0}, Lcom/google/obf/ft;->g()Z

    move-result v0

    if-nez v0, :cond_2

    .line 67
    invoke-virtual {p1}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v0

    .line 68
    invoke-static {v0, v1}, Ljava/lang/Double;->isNaN(D)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {v0, v1}, Ljava/lang/Double;->isInfinite(D)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 69
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "JSON forbids NaN and infinities: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 70
    :cond_2
    new-instance v0, Lcom/google/obf/er;

    invoke-direct {v0, p1}, Lcom/google/obf/er;-><init>(Ljava/lang/Number;)V

    invoke-direct {p0, v0}, Lcom/google/obf/ft;->a(Lcom/google/obf/em;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Lcom/google/obf/gg;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/obf/ft;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/obf/ft;->d:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 44
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 45
    :cond_1
    invoke-direct {p0}, Lcom/google/obf/ft;->j()Lcom/google/obf/em;

    move-result-object v0

    .line 46
    instance-of v0, v0, Lcom/google/obf/ep;

    if-eqz v0, :cond_2

    .line 47
    iput-object p1, p0, Lcom/google/obf/ft;->d:Ljava/lang/String;

    .line 48
    return-object p0

    .line 49
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method public a(Z)Lcom/google/obf/gg;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 56
    new-instance v0, Lcom/google/obf/er;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/obf/er;-><init>(Ljava/lang/Boolean;)V

    invoke-direct {p0, v0}, Lcom/google/obf/ft;->a(Lcom/google/obf/em;)V

    .line 57
    return-object p0
.end method

.method public b()Lcom/google/obf/gg;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 21
    new-instance v0, Lcom/google/obf/ej;

    invoke-direct {v0}, Lcom/google/obf/ej;-><init>()V

    .line 22
    invoke-direct {p0, v0}, Lcom/google/obf/ft;->a(Lcom/google/obf/em;)V

    .line 23
    iget-object v1, p0, Lcom/google/obf/ft;->c:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 24
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/google/obf/gg;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 50
    if-nez p1, :cond_0

    .line 51
    invoke-virtual {p0}, Lcom/google/obf/ft;->f()Lcom/google/obf/gg;

    move-result-object p0

    .line 53
    :goto_0
    return-object p0

    .line 52
    :cond_0
    new-instance v0, Lcom/google/obf/er;

    invoke-direct {v0, p1}, Lcom/google/obf/er;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/google/obf/ft;->a(Lcom/google/obf/em;)V

    goto :goto_0
.end method

.method public c()Lcom/google/obf/gg;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/obf/ft;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/obf/ft;->d:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 26
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 27
    :cond_1
    invoke-direct {p0}, Lcom/google/obf/ft;->j()Lcom/google/obf/em;

    move-result-object v0

    .line 28
    instance-of v0, v0, Lcom/google/obf/ej;

    if-eqz v0, :cond_2

    .line 29
    iget-object v0, p0, Lcom/google/obf/ft;->c:Ljava/util/List;

    iget-object v1, p0, Lcom/google/obf/ft;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 30
    return-object p0

    .line 31
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method public close()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/obf/ft;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 74
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Incomplete document"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 75
    :cond_0
    iget-object v0, p0, Lcom/google/obf/ft;->c:Ljava/util/List;

    sget-object v1, Lcom/google/obf/ft;->b:Lcom/google/obf/er;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 76
    return-void
.end method

.method public d()Lcom/google/obf/gg;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 32
    new-instance v0, Lcom/google/obf/ep;

    invoke-direct {v0}, Lcom/google/obf/ep;-><init>()V

    .line 33
    invoke-direct {p0, v0}, Lcom/google/obf/ft;->a(Lcom/google/obf/em;)V

    .line 34
    iget-object v1, p0, Lcom/google/obf/ft;->c:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 35
    return-object p0
.end method

.method public e()Lcom/google/obf/gg;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/obf/ft;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/obf/ft;->d:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 37
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 38
    :cond_1
    invoke-direct {p0}, Lcom/google/obf/ft;->j()Lcom/google/obf/em;

    move-result-object v0

    .line 39
    instance-of v0, v0, Lcom/google/obf/ep;

    if-eqz v0, :cond_2

    .line 40
    iget-object v0, p0, Lcom/google/obf/ft;->c:Ljava/util/List;

    iget-object v1, p0, Lcom/google/obf/ft;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 41
    return-object p0

    .line 42
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method public f()Lcom/google/obf/gg;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 54
    sget-object v0, Lcom/google/obf/eo;->a:Lcom/google/obf/eo;

    invoke-direct {p0, v0}, Lcom/google/obf/ft;->a(Lcom/google/obf/em;)V

    .line 55
    return-object p0
.end method

.method public flush()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 72
    return-void
.end method
