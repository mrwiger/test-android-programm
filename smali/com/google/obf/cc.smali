.class final Lcom/google/obf/cc;
.super Lcom/google/obf/cb;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/obf/cc$a;
    }
.end annotation


# static fields
.field private static final b:[D


# instance fields
.field private c:Z

.field private d:J

.field private final e:[Z

.field private final f:Lcom/google/obf/cc$a;

.field private g:Z

.field private h:J

.field private i:J

.field private j:Z

.field private k:Z

.field private l:J

.field private m:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 86
    const/16 v0, 0x8

    new-array v0, v0, [D

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/obf/cc;->b:[D

    return-void

    :array_0
    .array-data 8
        0x4037f9dcb5112287L    # 23.976023976023978
        0x4038000000000000L    # 24.0
        0x4039000000000000L    # 25.0
        0x403df853e2556b28L    # 29.97002997002997
        0x403e000000000000L    # 30.0
        0x4049000000000000L    # 50.0
        0x404df853e2556b28L    # 59.94005994005994
        0x404e000000000000L    # 60.0
    .end array-data
.end method

.method public constructor <init>(Lcom/google/obf/ar;)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0, p1}, Lcom/google/obf/cb;-><init>(Lcom/google/obf/ar;)V

    .line 2
    const/4 v0, 0x4

    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/google/obf/cc;->e:[Z

    .line 3
    new-instance v0, Lcom/google/obf/cc$a;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Lcom/google/obf/cc$a;-><init>(I)V

    iput-object v0, p0, Lcom/google/obf/cc;->f:Lcom/google/obf/cc$a;

    .line 4
    return-void
.end method

.method private static a(Lcom/google/obf/cc$a;)Landroid/util/Pair;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/obf/cc$a;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/obf/q;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v12, 0x7

    const/4 v2, -0x1

    .line 57
    iget-object v0, p0, Lcom/google/obf/cc$a;->c:[B

    iget v1, p0, Lcom/google/obf/cc$a;->a:I

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v11

    .line 58
    const/4 v0, 0x4

    aget-byte v0, v11, v0

    and-int/lit16 v0, v0, 0xff

    .line 59
    const/4 v1, 0x5

    aget-byte v1, v11, v1

    and-int/lit16 v1, v1, 0xff

    .line 60
    const/4 v3, 0x6

    aget-byte v3, v11, v3

    and-int/lit16 v3, v3, 0xff

    .line 61
    shl-int/lit8 v0, v0, 0x4

    shr-int/lit8 v4, v1, 0x4

    or-int v6, v0, v4

    .line 62
    and-int/lit8 v0, v1, 0xf

    shl-int/lit8 v0, v0, 0x8

    or-int v7, v0, v3

    .line 63
    const/high16 v10, 0x3f800000    # 1.0f

    .line 64
    aget-byte v0, v11, v12

    and-int/lit16 v0, v0, 0xf0

    shr-int/lit8 v0, v0, 0x4

    .line 65
    packed-switch v0, :pswitch_data_0

    .line 72
    :goto_0
    const/4 v0, 0x0

    const-string v1, "video/mpeg2"

    const-wide/16 v4, -0x1

    .line 73
    invoke-static {v11}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v8

    move v3, v2

    move v9, v2

    .line 74
    invoke-static/range {v0 .. v10}, Lcom/google/obf/q;->a(Ljava/lang/String;Ljava/lang/String;IIJIILjava/util/List;IF)Lcom/google/obf/q;

    move-result-object v2

    .line 75
    const-wide/16 v0, 0x0

    .line 76
    aget-byte v3, v11, v12

    and-int/lit8 v3, v3, 0xf

    add-int/lit8 v3, v3, -0x1

    .line 77
    if-ltz v3, :cond_1

    sget-object v4, Lcom/google/obf/cc;->b:[D

    array-length v4, v4

    if-ge v3, v4, :cond_1

    .line 78
    sget-object v0, Lcom/google/obf/cc;->b:[D

    aget-wide v0, v0, v3

    .line 79
    iget v3, p0, Lcom/google/obf/cc$a;->b:I

    .line 80
    add-int/lit8 v4, v3, 0x9

    aget-byte v4, v11, v4

    and-int/lit8 v4, v4, 0x60

    shr-int/lit8 v4, v4, 0x5

    .line 81
    add-int/lit8 v3, v3, 0x9

    aget-byte v3, v11, v3

    and-int/lit8 v3, v3, 0x1f

    .line 82
    if-eq v4, v3, :cond_0

    .line 83
    int-to-double v4, v4

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    add-double/2addr v4, v6

    add-int/lit8 v3, v3, 0x1

    int-to-double v6, v3

    div-double/2addr v4, v6

    mul-double/2addr v0, v4

    .line 84
    :cond_0
    const-wide v4, 0x412e848000000000L    # 1000000.0

    div-double v0, v4, v0

    double-to-long v0, v0

    .line 85
    :cond_1
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0

    .line 66
    :pswitch_0
    mul-int/lit8 v0, v7, 0x4

    int-to-float v0, v0

    mul-int/lit8 v1, v6, 0x3

    int-to-float v1, v1

    div-float v10, v0, v1

    .line 67
    goto :goto_0

    .line 68
    :pswitch_1
    mul-int/lit8 v0, v7, 0x10

    int-to-float v0, v0

    mul-int/lit8 v1, v6, 0x9

    int-to-float v1, v1

    div-float v10, v0, v1

    .line 69
    goto :goto_0

    .line 70
    :pswitch_2
    mul-int/lit8 v0, v7, 0x79

    int-to-float v0, v0

    mul-int/lit8 v1, v6, 0x64

    int-to-float v1, v1

    div-float v10, v0, v1

    .line 71
    goto :goto_0

    .line 65
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 5
    iget-object v0, p0, Lcom/google/obf/cc;->e:[Z

    invoke-static {v0}, Lcom/google/obf/du;->a([Z)V

    .line 6
    iget-object v0, p0, Lcom/google/obf/cc;->f:Lcom/google/obf/cc$a;

    invoke-virtual {v0}, Lcom/google/obf/cc$a;->a()V

    .line 7
    iput-boolean v1, p0, Lcom/google/obf/cc;->j:Z

    .line 8
    iput-boolean v1, p0, Lcom/google/obf/cc;->g:Z

    .line 9
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/obf/cc;->h:J

    .line 10
    return-void
.end method

.method public a(JZ)V
    .locals 3

    .prologue
    .line 11
    const-wide/16 v0, -0x1

    cmp-long v0, p1, v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/obf/cc;->j:Z

    .line 12
    iget-boolean v0, p0, Lcom/google/obf/cc;->j:Z

    if-eqz v0, :cond_0

    .line 13
    iput-wide p1, p0, Lcom/google/obf/cc;->i:J

    .line 14
    :cond_0
    return-void

    .line 11
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/google/obf/dw;)V
    .locals 12

    .prologue
    .line 15
    invoke-virtual {p1}, Lcom/google/obf/dw;->b()I

    move-result v0

    if-lez v0, :cond_0

    .line 16
    invoke-virtual {p1}, Lcom/google/obf/dw;->d()I

    move-result v0

    .line 17
    invoke-virtual {p1}, Lcom/google/obf/dw;->c()I

    move-result v9

    .line 18
    iget-object v10, p1, Lcom/google/obf/dw;->a:[B

    .line 19
    iget-wide v2, p0, Lcom/google/obf/cc;->h:J

    invoke-virtual {p1}, Lcom/google/obf/dw;->b()I

    move-result v1

    int-to-long v4, v1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/obf/cc;->h:J

    .line 20
    iget-object v1, p0, Lcom/google/obf/cc;->a:Lcom/google/obf/ar;

    invoke-virtual {p1}, Lcom/google/obf/dw;->b()I

    move-result v2

    invoke-interface {v1, p1, v2}, Lcom/google/obf/ar;->a(Lcom/google/obf/dw;I)V

    move v1, v0

    .line 22
    :goto_0
    iget-object v2, p0, Lcom/google/obf/cc;->e:[Z

    invoke-static {v10, v0, v9, v2}, Lcom/google/obf/du;->a([BII[Z)I

    move-result v8

    .line 23
    if-ne v8, v9, :cond_1

    .line 24
    iget-boolean v0, p0, Lcom/google/obf/cc;->c:Z

    if-nez v0, :cond_0

    .line 25
    iget-object v0, p0, Lcom/google/obf/cc;->f:Lcom/google/obf/cc$a;

    invoke-virtual {v0, v10, v1, v9}, Lcom/google/obf/cc$a;->a([BII)V

    .line 55
    :cond_0
    return-void

    .line 27
    :cond_1
    iget-object v0, p1, Lcom/google/obf/dw;->a:[B

    add-int/lit8 v2, v8, 0x3

    aget-byte v0, v0, v2

    and-int/lit16 v11, v0, 0xff

    .line 28
    iget-boolean v0, p0, Lcom/google/obf/cc;->c:Z

    if-nez v0, :cond_3

    .line 29
    sub-int v0, v8, v1

    .line 30
    if-lez v0, :cond_2

    .line 31
    iget-object v2, p0, Lcom/google/obf/cc;->f:Lcom/google/obf/cc$a;

    invoke-virtual {v2, v10, v1, v8}, Lcom/google/obf/cc$a;->a([BII)V

    .line 32
    :cond_2
    if-gez v0, :cond_7

    neg-int v0, v0

    .line 33
    :goto_1
    iget-object v1, p0, Lcom/google/obf/cc;->f:Lcom/google/obf/cc$a;

    invoke-virtual {v1, v11, v0}, Lcom/google/obf/cc$a;->a(II)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 34
    iget-object v0, p0, Lcom/google/obf/cc;->f:Lcom/google/obf/cc$a;

    invoke-static {v0}, Lcom/google/obf/cc;->a(Lcom/google/obf/cc$a;)Landroid/util/Pair;

    move-result-object v1

    .line 35
    iget-object v2, p0, Lcom/google/obf/cc;->a:Lcom/google/obf/ar;

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/google/obf/q;

    invoke-interface {v2, v0}, Lcom/google/obf/ar;->a(Lcom/google/obf/q;)V

    .line 36
    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/obf/cc;->d:J

    .line 37
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/obf/cc;->c:Z

    .line 38
    :cond_3
    iget-boolean v0, p0, Lcom/google/obf/cc;->c:Z

    if-eqz v0, :cond_6

    const/16 v0, 0xb8

    if-eq v11, v0, :cond_4

    if-nez v11, :cond_6

    .line 39
    :cond_4
    sub-int v6, v9, v8

    .line 40
    iget-boolean v0, p0, Lcom/google/obf/cc;->g:Z

    if-eqz v0, :cond_5

    .line 41
    iget-boolean v0, p0, Lcom/google/obf/cc;->k:Z

    if-eqz v0, :cond_8

    const/4 v4, 0x1

    .line 42
    :goto_2
    iget-wide v0, p0, Lcom/google/obf/cc;->h:J

    iget-wide v2, p0, Lcom/google/obf/cc;->l:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    sub-int v5, v0, v6

    .line 43
    iget-object v1, p0, Lcom/google/obf/cc;->a:Lcom/google/obf/ar;

    iget-wide v2, p0, Lcom/google/obf/cc;->m:J

    const/4 v7, 0x0

    invoke-interface/range {v1 .. v7}, Lcom/google/obf/ar;->a(JIII[B)V

    .line 44
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/obf/cc;->k:Z

    .line 45
    :cond_5
    const/16 v0, 0xb8

    if-ne v11, v0, :cond_9

    .line 46
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/obf/cc;->g:Z

    .line 47
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/obf/cc;->k:Z

    .line 53
    :cond_6
    :goto_3
    add-int/lit8 v0, v8, 0x3

    move v1, v8

    .line 54
    goto/16 :goto_0

    .line 32
    :cond_7
    const/4 v0, 0x0

    goto :goto_1

    .line 41
    :cond_8
    const/4 v4, 0x0

    goto :goto_2

    .line 48
    :cond_9
    iget-boolean v0, p0, Lcom/google/obf/cc;->j:Z

    if-eqz v0, :cond_a

    iget-wide v0, p0, Lcom/google/obf/cc;->i:J

    :goto_4
    iput-wide v0, p0, Lcom/google/obf/cc;->m:J

    .line 49
    iget-wide v0, p0, Lcom/google/obf/cc;->h:J

    int-to-long v2, v6

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/obf/cc;->l:J

    .line 50
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/obf/cc;->j:Z

    .line 51
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/obf/cc;->g:Z

    goto :goto_3

    .line 48
    :cond_a
    iget-wide v0, p0, Lcom/google/obf/cc;->m:J

    iget-wide v2, p0, Lcom/google/obf/cc;->d:J

    add-long/2addr v0, v2

    goto :goto_4
.end method

.method public b()V
    .locals 0

    .prologue
    .line 56
    return-void
.end method
