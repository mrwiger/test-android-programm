.class public Lru/cn/player/chromecast/ChromecastPlayer;
.super Lru/cn/player/AbstractMediaPlayer;
.source "ChromecastPlayer.java"

# interfaces
.implements Lcom/google/android/gms/cast/framework/media/RemoteMediaClient$Listener;


# instance fields
.field private castSession:Lcom/google/android/gms/cast/framework/CastSession;

.field private contentUri:Landroid/net/Uri;

.field private lastPosition:I

.field private mediaStatus:Lcom/google/android/gms/cast/MediaStatus;

.field private pendingSeek:Z

.field private remoteMediaPlayer:Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/cast/framework/CastSession;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "session"    # Lcom/google/android/gms/cast/framework/CastSession;

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lru/cn/player/AbstractMediaPlayer;-><init>(Landroid/content/Context;)V

    .line 35
    iput-object p2, p0, Lru/cn/player/chromecast/ChromecastPlayer;->castSession:Lcom/google/android/gms/cast/framework/CastSession;

    .line 37
    iget-object v1, p0, Lru/cn/player/chromecast/ChromecastPlayer;->castSession:Lcom/google/android/gms/cast/framework/CastSession;

    invoke-virtual {v1}, Lcom/google/android/gms/cast/framework/CastSession;->getRemoteMediaClient()Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;

    move-result-object v1

    iput-object v1, p0, Lru/cn/player/chromecast/ChromecastPlayer;->remoteMediaPlayer:Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;

    .line 39
    iget-object v1, p0, Lru/cn/player/chromecast/ChromecastPlayer;->remoteMediaPlayer:Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;

    invoke-virtual {v1}, Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;->getMediaStatus()Lcom/google/android/gms/cast/MediaStatus;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 40
    iget-object v1, p0, Lru/cn/player/chromecast/ChromecastPlayer;->remoteMediaPlayer:Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;

    invoke-virtual {v1}, Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;->stop()Lcom/google/android/gms/common/api/PendingResult;

    .line 42
    :cond_0
    iget-object v1, p0, Lru/cn/player/chromecast/ChromecastPlayer;->remoteMediaPlayer:Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;

    invoke-virtual {v1, p0}, Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;->addListener(Lcom/google/android/gms/cast/framework/media/RemoteMediaClient$Listener;)V

    .line 44
    :try_start_0
    iget-object v1, p0, Lru/cn/player/chromecast/ChromecastPlayer;->castSession:Lcom/google/android/gms/cast/framework/CastSession;

    iget-object v2, p0, Lru/cn/player/chromecast/ChromecastPlayer;->remoteMediaPlayer:Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;

    invoke-virtual {v2}, Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;->getNamespace()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lru/cn/player/chromecast/ChromecastPlayer;->remoteMediaPlayer:Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/cast/framework/CastSession;->setMessageReceivedCallbacks(Ljava/lang/String;Lcom/google/android/gms/cast/Cast$MessageReceivedCallback;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 49
    :goto_0
    return-void

    .line 46
    :catch_0
    move-exception v0

    .line 47
    .local v0, "e":Ljava/io/IOException;
    iget-object v1, p0, Lru/cn/player/chromecast/ChromecastPlayer;->LOG_TAG:Ljava/lang/String;

    const-string v2, "Exception while creating media channel"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method static synthetic access$000(Lru/cn/player/chromecast/ChromecastPlayer;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lru/cn/player/chromecast/ChromecastPlayer;

    .prologue
    .line 21
    iget-object v0, p0, Lru/cn/player/chromecast/ChromecastPlayer;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lru/cn/player/chromecast/ChromecastPlayer;Lru/cn/player/AbstractMediaPlayer$PlayerState;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/player/chromecast/ChromecastPlayer;
    .param p1, "x1"    # Lru/cn/player/AbstractMediaPlayer$PlayerState;

    .prologue
    .line 21
    invoke-virtual {p0, p1}, Lru/cn/player/chromecast/ChromecastPlayer;->setState(Lru/cn/player/AbstractMediaPlayer$PlayerState;)V

    return-void
.end method

.method static synthetic access$200(Lru/cn/player/chromecast/ChromecastPlayer;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lru/cn/player/chromecast/ChromecastPlayer;

    .prologue
    .line 21
    iget-object v0, p0, Lru/cn/player/chromecast/ChromecastPlayer;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lru/cn/player/chromecast/ChromecastPlayer;)Lru/cn/player/AbstractMediaPlayer$MediaPlayerListener;
    .locals 1
    .param p0, "x0"    # Lru/cn/player/chromecast/ChromecastPlayer;

    .prologue
    .line 21
    iget-object v0, p0, Lru/cn/player/chromecast/ChromecastPlayer;->listener:Lru/cn/player/AbstractMediaPlayer$MediaPlayerListener;

    return-object v0
.end method

.method static synthetic access$400(Lru/cn/player/chromecast/ChromecastPlayer;)Lru/cn/player/AbstractMediaPlayer$MediaPlayerListener;
    .locals 1
    .param p0, "x0"    # Lru/cn/player/chromecast/ChromecastPlayer;

    .prologue
    .line 21
    iget-object v0, p0, Lru/cn/player/chromecast/ChromecastPlayer;->listener:Lru/cn/player/AbstractMediaPlayer$MediaPlayerListener;

    return-object v0
.end method

.method static synthetic access$500(Lru/cn/player/chromecast/ChromecastPlayer;Lru/cn/player/AbstractMediaPlayer$PlayerState;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/player/chromecast/ChromecastPlayer;
    .param p1, "x1"    # Lru/cn/player/AbstractMediaPlayer$PlayerState;

    .prologue
    .line 21
    invoke-virtual {p0, p1}, Lru/cn/player/chromecast/ChromecastPlayer;->setState(Lru/cn/player/AbstractMediaPlayer$PlayerState;)V

    return-void
.end method

.method static synthetic access$600(Lru/cn/player/chromecast/ChromecastPlayer;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lru/cn/player/chromecast/ChromecastPlayer;

    .prologue
    .line 21
    iget-object v0, p0, Lru/cn/player/chromecast/ChromecastPlayer;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lru/cn/player/chromecast/ChromecastPlayer;Lru/cn/player/AbstractMediaPlayer$PlayerState;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/player/chromecast/ChromecastPlayer;
    .param p1, "x1"    # Lru/cn/player/AbstractMediaPlayer$PlayerState;

    .prologue
    .line 21
    invoke-virtual {p0, p1}, Lru/cn/player/chromecast/ChromecastPlayer;->setState(Lru/cn/player/AbstractMediaPlayer$PlayerState;)V

    return-void
.end method

.method static synthetic access$802(Lru/cn/player/chromecast/ChromecastPlayer;Z)Z
    .locals 0
    .param p0, "x0"    # Lru/cn/player/chromecast/ChromecastPlayer;
    .param p1, "x1"    # Z

    .prologue
    .line 21
    iput-boolean p1, p0, Lru/cn/player/chromecast/ChromecastPlayer;->pendingSeek:Z

    return p1
.end method

.method private prepareToPlay()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 52
    iget-object v6, p0, Lru/cn/player/chromecast/ChromecastPlayer;->castSession:Lcom/google/android/gms/cast/framework/CastSession;

    invoke-virtual {v6}, Lcom/google/android/gms/cast/framework/CastSession;->isConnected()Z

    move-result v6

    if-nez v6, :cond_1

    .line 53
    iget-object v6, p0, Lru/cn/player/chromecast/ChromecastPlayer;->listener:Lru/cn/player/AbstractMediaPlayer$MediaPlayerListener;

    if-eqz v6, :cond_0

    .line 54
    iget-object v6, p0, Lru/cn/player/chromecast/ChromecastPlayer;->listener:Lru/cn/player/AbstractMediaPlayer$MediaPlayerListener;

    invoke-interface {v6, v8}, Lru/cn/player/AbstractMediaPlayer$MediaPlayerListener;->onError(I)V

    .line 56
    :cond_0
    sget-object v6, Lru/cn/player/AbstractMediaPlayer$PlayerState;->STOPPED:Lru/cn/player/AbstractMediaPlayer$PlayerState;

    invoke-virtual {p0, v6}, Lru/cn/player/chromecast/ChromecastPlayer;->setState(Lru/cn/player/AbstractMediaPlayer$PlayerState;)V

    .line 100
    :goto_0
    return-void

    .line 60
    :cond_1
    new-instance v3, Lcom/google/android/gms/cast/MediaMetadata;

    const/4 v6, 0x2

    invoke-direct {v3, v6}, Lcom/google/android/gms/cast/MediaMetadata;-><init>(I)V

    .line 61
    .local v3, "metadata":Lcom/google/android/gms/cast/MediaMetadata;
    const-string v6, "com.google.android.gms.cast.metadata.TITLE"

    const-string v7, "Peers.TV"

    invoke-virtual {v3, v6, v7}, Lcom/google/android/gms/cast/MediaMetadata;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    new-instance v6, Lcom/google/android/gms/cast/MediaInfo$Builder;

    iget-object v7, p0, Lru/cn/player/chromecast/ChromecastPlayer;->contentUri:Landroid/net/Uri;

    invoke-virtual {v7}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Lcom/google/android/gms/cast/MediaInfo$Builder;-><init>(Ljava/lang/String;)V

    const-string v7, "application/x-mpegurl"

    .line 68
    invoke-virtual {v6, v7}, Lcom/google/android/gms/cast/MediaInfo$Builder;->setContentType(Ljava/lang/String;)Lcom/google/android/gms/cast/MediaInfo$Builder;

    move-result-object v6

    .line 69
    invoke-virtual {v6, v8}, Lcom/google/android/gms/cast/MediaInfo$Builder;->setStreamType(I)Lcom/google/android/gms/cast/MediaInfo$Builder;

    move-result-object v6

    .line 70
    invoke-virtual {v6, v3}, Lcom/google/android/gms/cast/MediaInfo$Builder;->setMetadata(Lcom/google/android/gms/cast/MediaMetadata;)Lcom/google/android/gms/cast/MediaInfo$Builder;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gms/cast/MediaInfo$Builder;->build()Lcom/google/android/gms/cast/MediaInfo;

    move-result-object v2

    .line 72
    .local v2, "mediaInfo":Lcom/google/android/gms/cast/MediaInfo;
    new-instance v1, Lru/cn/player/chromecast/ChromecastPlayer$1;

    invoke-direct {v1, p0}, Lru/cn/player/chromecast/ChromecastPlayer$1;-><init>(Lru/cn/player/chromecast/ChromecastPlayer;)V

    .line 92
    .local v1, "loadCallback":Lcom/google/android/gms/common/api/ResultCallback;
    :try_start_0
    iget-boolean v6, p0, Lru/cn/player/chromecast/ChromecastPlayer;->pendingSeek:Z

    if-eqz v6, :cond_2

    iget v6, p0, Lru/cn/player/chromecast/ChromecastPlayer;->lastPosition:I

    int-to-long v4, v6

    .line 93
    .local v4, "position":J
    :goto_1
    iget-object v6, p0, Lru/cn/player/chromecast/ChromecastPlayer;->remoteMediaPlayer:Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;

    const/4 v7, 0x1

    invoke-virtual {v6, v2, v7, v4, v5}, Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;->load(Lcom/google/android/gms/cast/MediaInfo;ZJ)Lcom/google/android/gms/common/api/PendingResult;

    move-result-object v6

    invoke-virtual {v6, v1}, Lcom/google/android/gms/common/api/PendingResult;->setResultCallback(Lcom/google/android/gms/common/api/ResultCallback;)V

    .line 94
    const/4 v6, 0x0

    iput-boolean v6, p0, Lru/cn/player/chromecast/ChromecastPlayer;->pendingSeek:Z
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 95
    .end local v4    # "position":J
    :catch_0
    move-exception v0

    .line 96
    .local v0, "e":Ljava/lang/IllegalStateException;
    iget-object v6, p0, Lru/cn/player/chromecast/ChromecastPlayer;->LOG_TAG:Ljava/lang/String;

    const-string v7, "Problem occurred with media during loading"

    invoke-static {v6, v7, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 92
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :cond_2
    const-wide/16 v4, 0x0

    goto :goto_1

    .line 97
    :catch_1
    move-exception v0

    .line 98
    .local v0, "e":Ljava/lang/Exception;
    iget-object v6, p0, Lru/cn/player/chromecast/ChromecastPlayer;->LOG_TAG:Ljava/lang/String;

    const-string v7, "Problem opening media during loading"

    invoke-static {v6, v7, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method


# virtual methods
.method public destroy()V
    .locals 3

    .prologue
    .line 164
    invoke-virtual {p0}, Lru/cn/player/chromecast/ChromecastPlayer;->stop()V

    .line 167
    :try_start_0
    iget-object v1, p0, Lru/cn/player/chromecast/ChromecastPlayer;->castSession:Lcom/google/android/gms/cast/framework/CastSession;

    iget-object v2, p0, Lru/cn/player/chromecast/ChromecastPlayer;->remoteMediaPlayer:Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;

    invoke-virtual {v2}, Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;->getNamespace()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/cast/framework/CastSession;->removeMessageReceivedCallbacks(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 172
    :goto_0
    iget-object v1, p0, Lru/cn/player/chromecast/ChromecastPlayer;->remoteMediaPlayer:Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;

    invoke-virtual {v1, p0}, Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;->removeListener(Lcom/google/android/gms/cast/framework/media/RemoteMediaClient$Listener;)V

    .line 173
    const/4 v1, 0x0

    iput-object v1, p0, Lru/cn/player/chromecast/ChromecastPlayer;->remoteMediaPlayer:Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;

    .line 174
    return-void

    .line 168
    :catch_0
    move-exception v0

    .line 169
    .local v0, "e":Ljava/io/IOException;
    iget-object v1, p0, Lru/cn/player/chromecast/ChromecastPlayer;->LOG_TAG:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getCurrentPosition()I
    .locals 2

    .prologue
    .line 187
    iget-boolean v0, p0, Lru/cn/player/chromecast/ChromecastPlayer;->pendingSeek:Z

    if-eqz v0, :cond_0

    .line 188
    iget v0, p0, Lru/cn/player/chromecast/ChromecastPlayer;->lastPosition:I

    .line 193
    :goto_0
    return v0

    .line 190
    :cond_0
    iget-object v0, p0, Lru/cn/player/chromecast/ChromecastPlayer;->remoteMediaPlayer:Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;

    if-eqz v0, :cond_1

    .line 191
    iget-object v0, p0, Lru/cn/player/chromecast/ChromecastPlayer;->remoteMediaPlayer:Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;->getApproximateStreamPosition()J

    move-result-wide v0

    long-to-int v0, v0

    goto :goto_0

    .line 193
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDuration()I
    .locals 2

    .prologue
    .line 178
    iget-object v0, p0, Lru/cn/player/chromecast/ChromecastPlayer;->remoteMediaPlayer:Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;

    if-eqz v0, :cond_0

    .line 179
    iget-object v0, p0, Lru/cn/player/chromecast/ChromecastPlayer;->remoteMediaPlayer:Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;->getStreamDuration()J

    move-result-wide v0

    long-to-int v0, v0

    .line 182
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public onAdBreakStatusUpdated()V
    .locals 0

    .prologue
    .line 317
    return-void
.end method

.method public onAudioBecomingNoisy()V
    .locals 0

    .prologue
    .line 234
    return-void
.end method

.method public onMetadataUpdated()V
    .locals 0

    .prologue
    .line 301
    return-void
.end method

.method public onPreloadStatusUpdated()V
    .locals 0

    .prologue
    .line 309
    return-void
.end method

.method public onQueueStatusUpdated()V
    .locals 0

    .prologue
    .line 305
    return-void
.end method

.method public onSendingRemoteMediaRequest()V
    .locals 0

    .prologue
    .line 313
    return-void
.end method

.method public onStatusUpdated()V
    .locals 5

    .prologue
    .line 238
    iget-object v2, p0, Lru/cn/player/chromecast/ChromecastPlayer;->remoteMediaPlayer:Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;

    if-nez v2, :cond_1

    .line 297
    :cond_0
    :goto_0
    return-void

    .line 241
    :cond_1
    iget-object v2, p0, Lru/cn/player/chromecast/ChromecastPlayer;->remoteMediaPlayer:Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;

    invoke-virtual {v2}, Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;->getMediaStatus()Lcom/google/android/gms/cast/MediaStatus;

    move-result-object v1

    .line 244
    .local v1, "status":Lcom/google/android/gms/cast/MediaStatus;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/android/gms/cast/MediaStatus;->getMediaInfo()Lcom/google/android/gms/cast/MediaInfo;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 247
    iget-object v2, p0, Lru/cn/player/chromecast/ChromecastPlayer;->mediaStatus:Lcom/google/android/gms/cast/MediaStatus;

    if-nez v2, :cond_2

    .line 248
    iput-object v1, p0, Lru/cn/player/chromecast/ChromecastPlayer;->mediaStatus:Lcom/google/android/gms/cast/MediaStatus;

    .line 251
    :cond_2
    invoke-virtual {v1}, Lcom/google/android/gms/cast/MediaStatus;->getPlayerState()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 269
    :pswitch_0
    invoke-virtual {v1}, Lcom/google/android/gms/cast/MediaStatus;->getIdleReason()I

    move-result v0

    .line 270
    .local v0, "idleReason":I
    iget-object v2, p0, Lru/cn/player/chromecast/ChromecastPlayer;->LOG_TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Idle state:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    packed-switch v0, :pswitch_data_1

    :pswitch_1
    goto :goto_0

    .line 274
    :pswitch_2
    iget-object v2, p0, Lru/cn/player/chromecast/ChromecastPlayer;->listener:Lru/cn/player/AbstractMediaPlayer$MediaPlayerListener;

    if-eqz v2, :cond_3

    .line 275
    iget-object v2, p0, Lru/cn/player/chromecast/ChromecastPlayer;->listener:Lru/cn/player/AbstractMediaPlayer$MediaPlayerListener;

    invoke-interface {v2}, Lru/cn/player/AbstractMediaPlayer$MediaPlayerListener;->onComplete()V

    .line 278
    :cond_3
    sget-object v2, Lru/cn/player/AbstractMediaPlayer$PlayerState;->STOPPED:Lru/cn/player/AbstractMediaPlayer$PlayerState;

    invoke-virtual {p0, v2}, Lru/cn/player/chromecast/ChromecastPlayer;->setState(Lru/cn/player/AbstractMediaPlayer$PlayerState;)V

    goto :goto_0

    .line 253
    .end local v0    # "idleReason":I
    :pswitch_3
    sget-object v2, Lru/cn/player/AbstractMediaPlayer$PlayerState;->PLAYING:Lru/cn/player/AbstractMediaPlayer$PlayerState;

    invoke-virtual {p0, v2}, Lru/cn/player/chromecast/ChromecastPlayer;->setState(Lru/cn/player/AbstractMediaPlayer$PlayerState;)V

    .line 255
    iget-boolean v2, p0, Lru/cn/player/chromecast/ChromecastPlayer;->pendingSeek:Z

    if-eqz v2, :cond_0

    .line 256
    iget v2, p0, Lru/cn/player/chromecast/ChromecastPlayer;->lastPosition:I

    invoke-virtual {p0, v2}, Lru/cn/player/chromecast/ChromecastPlayer;->seekTo(I)V

    goto :goto_0

    .line 261
    :pswitch_4
    sget-object v2, Lru/cn/player/AbstractMediaPlayer$PlayerState;->PAUSED:Lru/cn/player/AbstractMediaPlayer$PlayerState;

    invoke-virtual {p0, v2}, Lru/cn/player/chromecast/ChromecastPlayer;->setState(Lru/cn/player/AbstractMediaPlayer$PlayerState;)V

    .line 263
    iget-boolean v2, p0, Lru/cn/player/chromecast/ChromecastPlayer;->pendingSeek:Z

    if-eqz v2, :cond_0

    .line 264
    iget v2, p0, Lru/cn/player/chromecast/ChromecastPlayer;->lastPosition:I

    invoke-virtual {p0, v2}, Lru/cn/player/chromecast/ChromecastPlayer;->seekTo(I)V

    goto :goto_0

    .line 282
    .restart local v0    # "idleReason":I
    :pswitch_5
    iget-object v2, p0, Lru/cn/player/chromecast/ChromecastPlayer;->listener:Lru/cn/player/AbstractMediaPlayer$MediaPlayerListener;

    if-eqz v2, :cond_4

    .line 283
    iget-object v2, p0, Lru/cn/player/chromecast/ChromecastPlayer;->listener:Lru/cn/player/AbstractMediaPlayer$MediaPlayerListener;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Lru/cn/player/AbstractMediaPlayer$MediaPlayerListener;->onError(I)V

    .line 286
    :cond_4
    sget-object v2, Lru/cn/player/AbstractMediaPlayer$PlayerState;->STOPPED:Lru/cn/player/AbstractMediaPlayer$PlayerState;

    invoke-virtual {p0, v2}, Lru/cn/player/chromecast/ChromecastPlayer;->setState(Lru/cn/player/AbstractMediaPlayer$PlayerState;)V

    goto :goto_0

    .line 291
    :pswitch_6
    const/4 v2, 0x0

    iput-object v2, p0, Lru/cn/player/chromecast/ChromecastPlayer;->mediaStatus:Lcom/google/android/gms/cast/MediaStatus;

    .line 292
    invoke-virtual {p0}, Lru/cn/player/chromecast/ChromecastPlayer;->stop()V

    goto/16 :goto_0

    .line 251
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 272
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_6
        :pswitch_5
    .end packed-switch
.end method

.method public pause()V
    .locals 2

    .prologue
    .line 116
    iget-object v0, p0, Lru/cn/player/chromecast/ChromecastPlayer;->castSession:Lcom/google/android/gms/cast/framework/CastSession;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/framework/CastSession;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 128
    :goto_0
    return-void

    .line 120
    :cond_0
    iget-object v0, p0, Lru/cn/player/chromecast/ChromecastPlayer;->remoteMediaPlayer:Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;->pause()Lcom/google/android/gms/common/api/PendingResult;

    move-result-object v0

    new-instance v1, Lru/cn/player/chromecast/ChromecastPlayer$2;

    invoke-direct {v1, p0}, Lru/cn/player/chromecast/ChromecastPlayer$2;-><init>(Lru/cn/player/chromecast/ChromecastPlayer;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/PendingResult;->setResultCallback(Lcom/google/android/gms/common/api/ResultCallback;)V

    goto :goto_0
.end method

.method public play(Landroid/net/Uri;)V
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 104
    iget-object v0, p0, Lru/cn/player/chromecast/ChromecastPlayer;->contentUri:Landroid/net/Uri;

    invoke-virtual {p1, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lru/cn/player/chromecast/ChromecastPlayer;->mediaStatus:Lcom/google/android/gms/cast/MediaStatus;

    if-nez v0, :cond_1

    .line 105
    :cond_0
    iput-object p1, p0, Lru/cn/player/chromecast/ChromecastPlayer;->contentUri:Landroid/net/Uri;

    .line 106
    const/4 v0, 0x0

    iput v0, p0, Lru/cn/player/chromecast/ChromecastPlayer;->lastPosition:I

    .line 107
    const/4 v0, 0x0

    iput-object v0, p0, Lru/cn/player/chromecast/ChromecastPlayer;->mediaStatus:Lcom/google/android/gms/cast/MediaStatus;

    .line 109
    sget-object v0, Lru/cn/player/AbstractMediaPlayer$PlayerState;->LOADING:Lru/cn/player/AbstractMediaPlayer$PlayerState;

    invoke-virtual {p0, v0}, Lru/cn/player/chromecast/ChromecastPlayer;->setState(Lru/cn/player/AbstractMediaPlayer$PlayerState;)V

    .line 110
    invoke-direct {p0}, Lru/cn/player/chromecast/ChromecastPlayer;->prepareToPlay()V

    .line 112
    :cond_1
    return-void
.end method

.method public resume()V
    .locals 2

    .prologue
    .line 132
    iget-object v0, p0, Lru/cn/player/chromecast/ChromecastPlayer;->castSession:Lcom/google/android/gms/cast/framework/CastSession;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/framework/CastSession;->isConnected()Z

    move-result v0

    if-nez v0, :cond_0

    .line 145
    :goto_0
    return-void

    .line 136
    :cond_0
    iget-object v0, p0, Lru/cn/player/chromecast/ChromecastPlayer;->remoteMediaPlayer:Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;->play()Lcom/google/android/gms/common/api/PendingResult;

    move-result-object v0

    new-instance v1, Lru/cn/player/chromecast/ChromecastPlayer$3;

    invoke-direct {v1, p0}, Lru/cn/player/chromecast/ChromecastPlayer$3;-><init>(Lru/cn/player/chromecast/ChromecastPlayer;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/PendingResult;->setResultCallback(Lcom/google/android/gms/common/api/ResultCallback;)V

    goto :goto_0
.end method

.method public seekTo(I)V
    .locals 6
    .param p1, "msec"    # I

    .prologue
    .line 203
    iput p1, p0, Lru/cn/player/chromecast/ChromecastPlayer;->lastPosition:I

    .line 204
    const/4 v2, 0x1

    iput-boolean v2, p0, Lru/cn/player/chromecast/ChromecastPlayer;->pendingSeek:Z

    .line 206
    iget-object v2, p0, Lru/cn/player/chromecast/ChromecastPlayer;->mediaStatus:Lcom/google/android/gms/cast/MediaStatus;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lru/cn/player/chromecast/ChromecastPlayer;->remoteMediaPlayer:Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;

    if-nez v2, :cond_1

    .line 224
    :cond_0
    :goto_0
    return-void

    .line 209
    :cond_1
    const/4 v0, 0x2

    .line 210
    .local v0, "resumeState":I
    invoke-virtual {p0}, Lru/cn/player/chromecast/ChromecastPlayer;->getState()Lru/cn/player/AbstractMediaPlayer$PlayerState;

    move-result-object v2

    sget-object v3, Lru/cn/player/AbstractMediaPlayer$PlayerState;->PLAYING:Lru/cn/player/AbstractMediaPlayer$PlayerState;

    if-ne v2, v3, :cond_2

    .line 211
    const/4 v0, 0x1

    .line 214
    :cond_2
    new-instance v1, Lru/cn/player/chromecast/ChromecastPlayer$4;

    invoke-direct {v1, p0}, Lru/cn/player/chromecast/ChromecastPlayer$4;-><init>(Lru/cn/player/chromecast/ChromecastPlayer;)V

    .line 223
    .local v1, "seekCallback":Lcom/google/android/gms/common/api/ResultCallback;
    iget-object v2, p0, Lru/cn/player/chromecast/ChromecastPlayer;->remoteMediaPlayer:Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;

    int-to-long v4, p1

    invoke-virtual {v2, v4, v5, v0}, Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;->seek(JI)Lcom/google/android/gms/common/api/PendingResult;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/api/PendingResult;->setResultCallback(Lcom/google/android/gms/common/api/ResultCallback;)V

    goto :goto_0
.end method

.method public setChangeQualityListener(Lru/cn/player/ChangeQualityListener;)V
    .locals 0
    .param p1, "changeQualityListener"    # Lru/cn/player/ChangeQualityListener;

    .prologue
    .line 229
    return-void
.end method

.method public setVolume(F)V
    .locals 0
    .param p1, "volume"    # F

    .prologue
    .line 199
    return-void
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lru/cn/player/chromecast/ChromecastPlayer;->castSession:Lcom/google/android/gms/cast/framework/CastSession;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/framework/CastSession;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 150
    iget-object v0, p0, Lru/cn/player/chromecast/ChromecastPlayer;->remoteMediaPlayer:Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p0, Lru/cn/player/chromecast/ChromecastPlayer;->mediaStatus:Lcom/google/android/gms/cast/MediaStatus;

    if-eqz v0, :cond_0

    .line 152
    iget-object v0, p0, Lru/cn/player/chromecast/ChromecastPlayer;->remoteMediaPlayer:Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/framework/media/RemoteMediaClient;->stop()Lcom/google/android/gms/common/api/PendingResult;

    .line 156
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lru/cn/player/chromecast/ChromecastPlayer;->mediaStatus:Lcom/google/android/gms/cast/MediaStatus;

    .line 159
    :cond_1
    sget-object v0, Lru/cn/player/AbstractMediaPlayer$PlayerState;->STOPPED:Lru/cn/player/AbstractMediaPlayer$PlayerState;

    invoke-virtual {p0, v0}, Lru/cn/player/chromecast/ChromecastPlayer;->setState(Lru/cn/player/AbstractMediaPlayer$PlayerState;)V

    .line 160
    return-void
.end method
