.class public final Lcom/my/target/df;
.super Ljava/lang/Object;
.source "StandardAdSectionParser.java"


# instance fields
.field private final adConfig:Lcom/my/target/b;

.field private final context:Landroid/content/Context;

.field private final i:Lcom/my/target/ae;


# direct methods
.method private constructor <init>(Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/my/target/df;->i:Lcom/my/target/ae;

    .line 32
    iput-object p2, p0, Lcom/my/target/df;->adConfig:Lcom/my/target/b;

    .line 33
    iput-object p3, p0, Lcom/my/target/df;->context:Landroid/content/Context;

    .line 34
    return-void
.end method

.method public static a(Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/df;
    .locals 1

    .prologue
    .line 22
    new-instance v0, Lcom/my/target/df;

    invoke-direct {v0, p0, p1, p2}, Lcom/my/target/df;-><init>(Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method public final a(Lorg/json/JSONObject;Lcom/my/target/dh;)V
    .locals 4

    .prologue
    .line 38
    iget-object v0, p0, Lcom/my/target/df;->i:Lcom/my/target/ae;

    iget-object v1, p0, Lcom/my/target/df;->adConfig:Lcom/my/target/b;

    iget-object v2, p0, Lcom/my/target/df;->context:Landroid/content/Context;

    invoke-static {v0, v1, v2}, Lcom/my/target/bf;->b(Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/bf;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/my/target/bf;->a(Lorg/json/JSONObject;Lcom/my/target/ak;)V

    .line 39
    const-string v0, "settings"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 40
    if-eqz v0, :cond_1

    .line 1048
    const-string v1, "refreshTime"

    invoke-virtual {p2}, Lcom/my/target/dh;->x()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v1

    .line 1049
    if-ltz v1, :cond_0

    .line 1051
    invoke-virtual {p2, v1}, Lcom/my/target/dh;->i(I)V

    .line 1053
    :cond_0
    const-string v1, "hasAdditionalAds"

    invoke-virtual {p2}, Lcom/my/target/dh;->y()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {p2, v1}, Lcom/my/target/dh;->g(Z)V

    .line 1054
    const-string v1, "loopRotation"

    invoke-virtual {p2}, Lcom/my/target/dh;->z()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {p2, v1}, Lcom/my/target/dh;->h(Z)V

    .line 1055
    const-string v1, "animationType"

    invoke-virtual {p2}, Lcom/my/target/dh;->A()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {p2, v1}, Lcom/my/target/dh;->setAnimationType(I)V

    .line 1057
    const-string v1, "view"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 1058
    if-eqz v1, :cond_1

    .line 1060
    invoke-virtual {p2}, Lcom/my/target/dh;->w()Lcom/my/target/dg;

    move-result-object v2

    .line 1066
    const-string v0, "type"

    invoke-virtual {v2}, Lcom/my/target/dg;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/my/target/dg;->a(Ljava/lang/String;)V

    .line 1067
    const-string v0, "backgroundColor"

    invoke-virtual {v2}, Lcom/my/target/dg;->getBackgroundColor()I

    move-result v3

    invoke-static {v1, v0, v3}, Lcom/my/target/bh;->a(Lorg/json/JSONObject;Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {v2, v0}, Lcom/my/target/dg;->setBackgroundColor(I)V

    .line 1068
    const-string v0, "backgroundTouchColor"

    invoke-virtual {v2}, Lcom/my/target/dg;->n()I

    move-result v3

    invoke-static {v1, v0, v3}, Lcom/my/target/bh;->a(Lorg/json/JSONObject;Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {v2, v0}, Lcom/my/target/dg;->a(I)V

    .line 1070
    iget-object v0, p0, Lcom/my/target/df;->adConfig:Lcom/my/target/b;

    invoke-virtual {v0}, Lcom/my/target/b;->getFormat()Ljava/lang/String;

    move-result-object v0

    const-string v3, "standard_300x250"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/high16 v0, -0x1000000

    .line 1071
    :goto_0
    const-string v3, "titleColor"

    invoke-static {v1, v3, v0}, Lcom/my/target/bh;->a(Lorg/json/JSONObject;Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {v2, v0}, Lcom/my/target/dg;->setTitleColor(I)V

    .line 1072
    const-string v0, "ageRestrictionsBackgroundColor"

    invoke-virtual {v2}, Lcom/my/target/dg;->o()I

    move-result v3

    invoke-static {v1, v0, v3}, Lcom/my/target/bh;->a(Lorg/json/JSONObject;Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {v2, v0}, Lcom/my/target/dg;->b(I)V

    .line 1073
    const-string v0, "ageRestrictionsTextColor"

    invoke-virtual {v2}, Lcom/my/target/dg;->p()I

    move-result v3

    invoke-static {v1, v0, v3}, Lcom/my/target/bh;->a(Lorg/json/JSONObject;Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {v2, v0}, Lcom/my/target/dg;->c(I)V

    .line 1074
    const-string v0, "ageRestrictionsBorderColor"

    invoke-virtual {v2}, Lcom/my/target/dg;->q()I

    move-result v3

    invoke-static {v1, v0, v3}, Lcom/my/target/bh;->a(Lorg/json/JSONObject;Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {v2, v0}, Lcom/my/target/dg;->d(I)V

    .line 1075
    const-string v0, "descriptionColor"

    invoke-virtual {v2}, Lcom/my/target/dg;->r()I

    move-result v3

    invoke-static {v1, v0, v3}, Lcom/my/target/bh;->a(Lorg/json/JSONObject;Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {v2, v0}, Lcom/my/target/dg;->e(I)V

    .line 1076
    const-string v0, "domainColor"

    invoke-virtual {v2}, Lcom/my/target/dg;->s()I

    move-result v3

    invoke-static {v1, v0, v3}, Lcom/my/target/bh;->a(Lorg/json/JSONObject;Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {v2, v0}, Lcom/my/target/dg;->f(I)V

    .line 1077
    const-string v0, "votesColor"

    invoke-virtual {v2}, Lcom/my/target/dg;->t()I

    move-result v3

    invoke-static {v1, v0, v3}, Lcom/my/target/bh;->a(Lorg/json/JSONObject;Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {v2, v0}, Lcom/my/target/dg;->g(I)V

    .line 1078
    const-string v0, "disclaimerColor"

    invoke-virtual {v2}, Lcom/my/target/dg;->u()I

    move-result v3

    invoke-static {v1, v0, v3}, Lcom/my/target/bh;->a(Lorg/json/JSONObject;Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {v2, v0}, Lcom/my/target/dg;->h(I)V

    .line 1079
    const-string v0, "ctaButtonColor"

    invoke-virtual {v2}, Lcom/my/target/dg;->getCtaButtonColor()I

    move-result v3

    invoke-static {v1, v0, v3}, Lcom/my/target/bh;->a(Lorg/json/JSONObject;Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {v2, v0}, Lcom/my/target/dg;->setCtaButtonColor(I)V

    .line 1080
    const-string v0, "ctaButtonTouchColor"

    invoke-virtual {v2}, Lcom/my/target/dg;->getCtaButtonTouchColor()I

    move-result v3

    invoke-static {v1, v0, v3}, Lcom/my/target/bh;->a(Lorg/json/JSONObject;Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {v2, v0}, Lcom/my/target/dg;->setCtaButtonTouchColor(I)V

    .line 1081
    const-string v0, "ctaButtonTextColor"

    invoke-virtual {v2}, Lcom/my/target/dg;->getCtaButtonTextColor()I

    move-result v3

    invoke-static {v1, v0, v3}, Lcom/my/target/bh;->a(Lorg/json/JSONObject;Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {v2, v0}, Lcom/my/target/dg;->setCtaButtonTextColor(I)V

    .line 1083
    const-string v0, "titleBold"

    invoke-virtual {v2}, Lcom/my/target/dg;->h()Z

    move-result v3

    invoke-virtual {v1, v0, v3}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {v2, v0}, Lcom/my/target/dg;->a(Z)V

    .line 1084
    const-string v0, "descriptionBold"

    invoke-virtual {v2}, Lcom/my/target/dg;->i()Z

    move-result v3

    invoke-virtual {v1, v0, v3}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {v2, v0}, Lcom/my/target/dg;->b(Z)V

    .line 1085
    const-string v0, "domainBold"

    invoke-virtual {v2}, Lcom/my/target/dg;->j()Z

    move-result v3

    invoke-virtual {v1, v0, v3}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {v2, v0}, Lcom/my/target/dg;->c(Z)V

    .line 1086
    const-string v0, "votesBold"

    invoke-virtual {v2}, Lcom/my/target/dg;->k()Z

    move-result v3

    invoke-virtual {v1, v0, v3}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {v2, v0}, Lcom/my/target/dg;->d(Z)V

    .line 1087
    const-string v0, "disclaimerBold"

    invoke-virtual {v2}, Lcom/my/target/dg;->l()Z

    move-result v3

    invoke-virtual {v1, v0, v3}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {v2, v0}, Lcom/my/target/dg;->e(Z)V

    .line 1088
    const-string v0, "ctaButtonTextBold"

    invoke-virtual {v2}, Lcom/my/target/dg;->m()Z

    move-result v3

    invoke-virtual {v1, v0, v3}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {v2, v0}, Lcom/my/target/dg;->f(Z)V

    .line 44
    :cond_1
    return-void

    .line 1070
    :cond_2
    const v0, -0xffab5a

    goto/16 :goto_0
.end method
