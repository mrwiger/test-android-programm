.class Lru/cn/ad/natives/adapters/AdmobNativeAdapter$1;
.super Lcom/google/android/gms/ads/AdListener;
.source "AdmobNativeAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/ad/natives/adapters/AdmobNativeAdapter;->onLoad()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/ad/natives/adapters/AdmobNativeAdapter;


# direct methods
.method constructor <init>(Lru/cn/ad/natives/adapters/AdmobNativeAdapter;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/ad/natives/adapters/AdmobNativeAdapter;

    .prologue
    .line 63
    iput-object p1, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$1;->this$0:Lru/cn/ad/natives/adapters/AdmobNativeAdapter;

    invoke-direct {p0}, Lcom/google/android/gms/ads/AdListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onAdClosed()V
    .locals 2

    .prologue
    .line 95
    const-string v0, "AdmobNativeAdapter"

    const-string v1, "closed"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    return-void
.end method

.method public onAdFailedToLoad(I)V
    .locals 2
    .param p1, "errorCode"    # I

    .prologue
    .line 71
    const-string v0, "AdmobNativeAdapter"

    const-string v1, "failed"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    iget-object v0, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$1;->this$0:Lru/cn/ad/natives/adapters/AdmobNativeAdapter;

    invoke-static {v0}, Lru/cn/ad/natives/adapters/AdmobNativeAdapter;->access$000(Lru/cn/ad/natives/adapters/AdmobNativeAdapter;)Lru/cn/ad/AdAdapter$Listener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$1;->this$0:Lru/cn/ad/natives/adapters/AdmobNativeAdapter;

    invoke-static {v0}, Lru/cn/ad/natives/adapters/AdmobNativeAdapter;->access$100(Lru/cn/ad/natives/adapters/AdmobNativeAdapter;)Lru/cn/ad/AdAdapter$Listener;

    move-result-object v0

    invoke-interface {v0}, Lru/cn/ad/AdAdapter$Listener;->onError()V

    .line 75
    :cond_0
    return-void
.end method

.method public onAdLeftApplication()V
    .locals 3

    .prologue
    .line 79
    const-string v0, "AdmobNativeAdapter"

    const-string v1, "app left"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    iget-object v0, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$1;->this$0:Lru/cn/ad/natives/adapters/AdmobNativeAdapter;

    sget-object v1, Lru/cn/domain/statistics/inetra/AdvEvent;->ADV_EVENT_CLICK:Lru/cn/domain/statistics/inetra/AdvEvent;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lru/cn/ad/natives/adapters/AdmobNativeAdapter;->reportEvent(Lru/cn/domain/statistics/inetra/AdvEvent;Ljava/util/Map;)V

    .line 84
    iget-object v0, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$1;->this$0:Lru/cn/ad/natives/adapters/AdmobNativeAdapter;

    invoke-static {v0}, Lru/cn/ad/natives/adapters/AdmobNativeAdapter;->access$200(Lru/cn/ad/natives/adapters/AdmobNativeAdapter;)Lru/cn/ad/AdAdapter$Listener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lru/cn/ad/natives/adapters/AdmobNativeAdapter$1;->this$0:Lru/cn/ad/natives/adapters/AdmobNativeAdapter;

    invoke-static {v0}, Lru/cn/ad/natives/adapters/AdmobNativeAdapter;->access$300(Lru/cn/ad/natives/adapters/AdmobNativeAdapter;)Lru/cn/ad/AdAdapter$Listener;

    move-result-object v0

    invoke-interface {v0}, Lru/cn/ad/AdAdapter$Listener;->onAdEnded()V

    .line 86
    :cond_0
    return-void
.end method

.method public onAdLoaded()V
    .locals 2

    .prologue
    .line 66
    const-string v0, "AdmobNativeAdapter"

    const-string v1, "loaded"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    return-void
.end method

.method public onAdOpened()V
    .locals 2

    .prologue
    .line 90
    const-string v0, "AdmobNativeAdapter"

    const-string v1, "opened"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    return-void
.end method
