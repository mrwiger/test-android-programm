.class public Lcom/yqritc/recyclerviewflexibledivider/HorizontalDividerItemDecoration$Builder;
.super Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;
.source "HorizontalDividerItemDecoration.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/yqritc/recyclerviewflexibledivider/HorizontalDividerItemDecoration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder",
        "<",
        "Lcom/yqritc/recyclerviewflexibledivider/HorizontalDividerItemDecoration$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field private mMarginProvider:Lcom/yqritc/recyclerviewflexibledivider/HorizontalDividerItemDecoration$MarginProvider;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 134
    invoke-direct {p0, p1}, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;-><init>(Landroid/content/Context;)V

    .line 121
    new-instance v0, Lcom/yqritc/recyclerviewflexibledivider/HorizontalDividerItemDecoration$Builder$1;

    invoke-direct {v0, p0}, Lcom/yqritc/recyclerviewflexibledivider/HorizontalDividerItemDecoration$Builder$1;-><init>(Lcom/yqritc/recyclerviewflexibledivider/HorizontalDividerItemDecoration$Builder;)V

    iput-object v0, p0, Lcom/yqritc/recyclerviewflexibledivider/HorizontalDividerItemDecoration$Builder;->mMarginProvider:Lcom/yqritc/recyclerviewflexibledivider/HorizontalDividerItemDecoration$MarginProvider;

    .line 135
    return-void
.end method

.method static synthetic access$000(Lcom/yqritc/recyclerviewflexibledivider/HorizontalDividerItemDecoration$Builder;)Lcom/yqritc/recyclerviewflexibledivider/HorizontalDividerItemDecoration$MarginProvider;
    .locals 1
    .param p0, "x0"    # Lcom/yqritc/recyclerviewflexibledivider/HorizontalDividerItemDecoration$Builder;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/yqritc/recyclerviewflexibledivider/HorizontalDividerItemDecoration$Builder;->mMarginProvider:Lcom/yqritc/recyclerviewflexibledivider/HorizontalDividerItemDecoration$MarginProvider;

    return-object v0
.end method


# virtual methods
.method public build()Lcom/yqritc/recyclerviewflexibledivider/HorizontalDividerItemDecoration;
    .locals 1

    .prologue
    .line 170
    invoke-virtual {p0}, Lcom/yqritc/recyclerviewflexibledivider/HorizontalDividerItemDecoration$Builder;->checkBuilderParams()V

    .line 171
    new-instance v0, Lcom/yqritc/recyclerviewflexibledivider/HorizontalDividerItemDecoration;

    invoke-direct {v0, p0}, Lcom/yqritc/recyclerviewflexibledivider/HorizontalDividerItemDecoration;-><init>(Lcom/yqritc/recyclerviewflexibledivider/HorizontalDividerItemDecoration$Builder;)V

    return-object v0
.end method
