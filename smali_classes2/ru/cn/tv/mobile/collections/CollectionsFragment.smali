.class public Lru/cn/tv/mobile/collections/CollectionsFragment;
.super Landroid/support/v4/app/Fragment;
.source "CollectionsFragment.java"


# instance fields
.field private controller:Lru/cn/tv/mobile/collections/RubricsController;

.field private listener:Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastSelectedListener;

.field private pager:Landroid/support/v4/view/ViewPager;

.field private pagerAdapter:Lru/cn/tv/mobile/collections/PagerAdapter;

.field private tabs:Landroid/support/design/widget/TabLayout;

.field private viewModel:Lru/cn/tv/mobile/collections/CollectionsViewModel;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method private getTabPosition()I
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lru/cn/tv/mobile/collections/CollectionsFragment;->pager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    return v0
.end method

.method private setTabs(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lru/cn/api/catalogue/replies/Rubric;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "rubrics":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/catalogue/replies/Rubric;>;"
    const/16 v4, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 67
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_1

    .line 84
    :cond_0
    :goto_0
    return-void

    .line 71
    :cond_1
    iget-object v3, p0, Lru/cn/tv/mobile/collections/CollectionsFragment;->tabs:Landroid/support/design/widget/TabLayout;

    if-eqz v3, :cond_2

    .line 72
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-le v3, v1, :cond_3

    move v0, v1

    .line 73
    .local v0, "hasRubrics":Z
    :goto_1
    iget-object v5, p0, Lru/cn/tv/mobile/collections/CollectionsFragment;->tabs:Landroid/support/design/widget/TabLayout;

    if-eqz v0, :cond_4

    move v3, v2

    :goto_2
    invoke-virtual {v5, v3}, Landroid/support/design/widget/TabLayout;->setVisibility(I)V

    .line 76
    .end local v0    # "hasRubrics":Z
    :cond_2
    new-instance v3, Lru/cn/tv/mobile/collections/RubricsController;

    iget-object v5, p0, Lru/cn/tv/mobile/collections/CollectionsFragment;->listener:Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastSelectedListener;

    invoke-direct {v3, v5}, Lru/cn/tv/mobile/collections/RubricsController;-><init>(Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastSelectedListener;)V

    iput-object v3, p0, Lru/cn/tv/mobile/collections/CollectionsFragment;->controller:Lru/cn/tv/mobile/collections/RubricsController;

    .line 77
    iget-object v3, p0, Lru/cn/tv/mobile/collections/CollectionsFragment;->controller:Lru/cn/tv/mobile/collections/RubricsController;

    invoke-virtual {v3, p1}, Lru/cn/tv/mobile/collections/RubricsController;->setRubrics(Ljava/util/List;)V

    .line 79
    new-instance v3, Lru/cn/tv/mobile/collections/PagerAdapter;

    invoke-virtual {p0}, Lru/cn/tv/mobile/collections/CollectionsFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v5

    iget-object v6, p0, Lru/cn/tv/mobile/collections/CollectionsFragment;->controller:Lru/cn/tv/mobile/collections/RubricsController;

    invoke-direct {v3, v5, v6}, Lru/cn/tv/mobile/collections/PagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;Lru/cn/tv/mobile/collections/RubricFragmentController;)V

    iput-object v3, p0, Lru/cn/tv/mobile/collections/CollectionsFragment;->pagerAdapter:Lru/cn/tv/mobile/collections/PagerAdapter;

    .line 80
    iget-object v3, p0, Lru/cn/tv/mobile/collections/CollectionsFragment;->pager:Landroid/support/v4/view/ViewPager;

    iget-object v5, p0, Lru/cn/tv/mobile/collections/CollectionsFragment;->pagerAdapter:Lru/cn/tv/mobile/collections/PagerAdapter;

    invoke-virtual {v3, v5}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 82
    iget-object v3, p0, Lru/cn/tv/mobile/collections/CollectionsFragment;->controller:Lru/cn/tv/mobile/collections/RubricsController;

    invoke-virtual {v3}, Lru/cn/tv/mobile/collections/RubricsController;->rubricCount()I

    move-result v3

    if-le v3, v1, :cond_5

    move v0, v1

    .line 83
    .restart local v0    # "hasRubrics":Z
    :goto_3
    iget-object v1, p0, Lru/cn/tv/mobile/collections/CollectionsFragment;->tabs:Landroid/support/design/widget/TabLayout;

    if-eqz v0, :cond_6

    :goto_4
    invoke-virtual {v1, v2}, Landroid/support/design/widget/TabLayout;->setVisibility(I)V

    goto :goto_0

    .end local v0    # "hasRubrics":Z
    :cond_3
    move v0, v2

    .line 72
    goto :goto_1

    .restart local v0    # "hasRubrics":Z
    :cond_4
    move v3, v4

    .line 73
    goto :goto_2

    .end local v0    # "hasRubrics":Z
    :cond_5
    move v0, v2

    .line 82
    goto :goto_3

    .restart local v0    # "hasRubrics":Z
    :cond_6
    move v2, v4

    .line 83
    goto :goto_4
.end method


# virtual methods
.method final bridge synthetic bridge$lambda$0$CollectionsFragment(Ljava/util/List;)V
    .locals 0

    invoke-direct {p0, p1}, Lru/cn/tv/mobile/collections/CollectionsFragment;->setTabs(Ljava/util/List;)V

    return-void
.end method

.method public getRubricId()J
    .locals 4

    .prologue
    .line 54
    iget-object v1, p0, Lru/cn/tv/mobile/collections/CollectionsFragment;->controller:Lru/cn/tv/mobile/collections/RubricsController;

    invoke-virtual {v1}, Lru/cn/tv/mobile/collections/RubricsController;->getRubrics()Ljava/util/List;

    move-result-object v0

    .line 55
    .local v0, "rubrics":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/catalogue/replies/Rubric;>;"
    invoke-direct {p0}, Lru/cn/tv/mobile/collections/CollectionsFragment;->getTabPosition()I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/cn/api/catalogue/replies/Rubric;

    iget-wide v2, v1, Lru/cn/api/catalogue/replies/Rubric;->id:J

    return-wide v2
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 33
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 34
    invoke-static {}, Lru/cn/tv/App;->scope()Ltoothpick/Scope;

    move-result-object v0

    const-class v1, Lru/cn/tv/mobile/collections/CollectionsViewModel;

    invoke-static {p0, v0, v1}, Lru/cn/mvvm/ViewModels;->get(Landroid/support/v4/app/Fragment;Ltoothpick/Scope;Ljava/lang/Class;)Landroid/arch/lifecycle/ViewModel;

    move-result-object v0

    check-cast v0, Lru/cn/tv/mobile/collections/CollectionsViewModel;

    iput-object v0, p0, Lru/cn/tv/mobile/collections/CollectionsFragment;->viewModel:Lru/cn/tv/mobile/collections/CollectionsViewModel;

    .line 35
    iget-object v0, p0, Lru/cn/tv/mobile/collections/CollectionsFragment;->viewModel:Lru/cn/tv/mobile/collections/CollectionsViewModel;

    invoke-virtual {v0}, Lru/cn/tv/mobile/collections/CollectionsViewModel;->rubrics()Landroid/arch/lifecycle/LiveData;

    move-result-object v0

    new-instance v1, Lru/cn/tv/mobile/collections/CollectionsFragment$$Lambda$0;

    invoke-direct {v1, p0}, Lru/cn/tv/mobile/collections/CollectionsFragment$$Lambda$0;-><init>(Lru/cn/tv/mobile/collections/CollectionsFragment;)V

    invoke-virtual {v0, p0, v1}, Landroid/arch/lifecycle/LiveData;->observe(Landroid/arch/lifecycle/LifecycleOwner;Landroid/arch/lifecycle/Observer;)V

    .line 36
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 40
    const v0, 0x7f0c00b3

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 45
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 47
    const v0, 0x7f09015a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lru/cn/tv/mobile/collections/CollectionsFragment;->pager:Landroid/support/v4/view/ViewPager;

    .line 48
    const v0, 0x7f0901c4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TabLayout;

    iput-object v0, p0, Lru/cn/tv/mobile/collections/CollectionsFragment;->tabs:Landroid/support/design/widget/TabLayout;

    .line 49
    iget-object v0, p0, Lru/cn/tv/mobile/collections/CollectionsFragment;->tabs:Landroid/support/design/widget/TabLayout;

    iget-object v1, p0, Lru/cn/tv/mobile/collections/CollectionsFragment;->pager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TabLayout;->setupWithViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 50
    iget-object v0, p0, Lru/cn/tv/mobile/collections/CollectionsFragment;->tabs:Landroid/support/design/widget/TabLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TabLayout;->setTabMode(I)V

    .line 51
    return-void
.end method

.method public setListener(Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastSelectedListener;)V
    .locals 0
    .param p1, "listener"    # Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastSelectedListener;

    .prologue
    .line 59
    iput-object p1, p0, Lru/cn/tv/mobile/collections/CollectionsFragment;->listener:Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastSelectedListener;

    .line 60
    return-void
.end method
