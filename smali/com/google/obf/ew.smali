.class public abstract Lcom/google/obf/ew;
.super Ljava/lang/Object;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final nullSafe()Lcom/google/obf/ew;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/obf/ew",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 5
    new-instance v0, Lcom/google/obf/ew$1;

    invoke-direct {v0, p0}, Lcom/google/obf/ew$1;-><init>(Lcom/google/obf/ew;)V

    return-object v0
.end method

.method public abstract read(Lcom/google/obf/ge;)Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/obf/ge;",
            ")TT;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public final toJsonTree(Ljava/lang/Object;)Lcom/google/obf/em;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Lcom/google/obf/em;"
        }
    .end annotation

    .prologue
    .line 12
    :try_start_0
    new-instance v0, Lcom/google/obf/ft;

    invoke-direct {v0}, Lcom/google/obf/ft;-><init>()V

    .line 13
    invoke-virtual {p0, v0, p1}, Lcom/google/obf/ew;->write(Lcom/google/obf/gg;Ljava/lang/Object;)V

    .line 14
    invoke-virtual {v0}, Lcom/google/obf/ft;->a()Lcom/google/obf/em;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 15
    :catch_0
    move-exception v0

    .line 16
    new-instance v1, Lcom/google/obf/en;

    invoke-direct {v1, v0}, Lcom/google/obf/en;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public abstract write(Lcom/google/obf/gg;Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/obf/gg;",
            "TT;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method
