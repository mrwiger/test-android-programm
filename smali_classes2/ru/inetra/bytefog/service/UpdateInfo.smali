.class public Lru/inetra/bytefog/service/UpdateInfo;
.super Ljava/lang/Object;
.source "UpdateInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lru/inetra/bytefog/service/UpdateInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private availableVersion:Ljava/lang/String;

.field private changelog:Ljava/lang/String;

.field private downloadUrl:Ljava/lang/String;

.field private status:Lru/inetra/bytefog/service/UpdateStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 66
    new-instance v0, Lru/inetra/bytefog/service/UpdateInfo$1;

    invoke-direct {v0}, Lru/inetra/bytefog/service/UpdateInfo$1;-><init>()V

    sput-object v0, Lru/inetra/bytefog/service/UpdateInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    sget-object v0, Lru/inetra/bytefog/service/UpdateStatus;->Unknown:Lru/inetra/bytefog/service/UpdateStatus;

    iput-object v0, p0, Lru/inetra/bytefog/service/UpdateInfo;->status:Lru/inetra/bytefog/service/UpdateStatus;

    .line 8
    const-string v0, ""

    iput-object v0, p0, Lru/inetra/bytefog/service/UpdateInfo;->availableVersion:Ljava/lang/String;

    .line 9
    const-string v0, ""

    iput-object v0, p0, Lru/inetra/bytefog/service/UpdateInfo;->downloadUrl:Ljava/lang/String;

    .line 10
    const-string v0, ""

    iput-object v0, p0, Lru/inetra/bytefog/service/UpdateInfo;->changelog:Ljava/lang/String;

    .line 14
    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    sget-object v0, Lru/inetra/bytefog/service/UpdateStatus;->Unknown:Lru/inetra/bytefog/service/UpdateStatus;

    iput-object v0, p0, Lru/inetra/bytefog/service/UpdateInfo;->status:Lru/inetra/bytefog/service/UpdateStatus;

    .line 8
    const-string v0, ""

    iput-object v0, p0, Lru/inetra/bytefog/service/UpdateInfo;->availableVersion:Ljava/lang/String;

    .line 9
    const-string v0, ""

    iput-object v0, p0, Lru/inetra/bytefog/service/UpdateInfo;->downloadUrl:Ljava/lang/String;

    .line 10
    const-string v0, ""

    iput-object v0, p0, Lru/inetra/bytefog/service/UpdateInfo;->changelog:Ljava/lang/String;

    .line 17
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lru/inetra/bytefog/service/UpdateInfo;->availableVersion:Ljava/lang/String;

    .line 18
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lru/inetra/bytefog/service/UpdateInfo;->downloadUrl:Ljava/lang/String;

    .line 19
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lru/inetra/bytefog/service/UpdateInfo;->changelog:Ljava/lang/String;

    .line 20
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x0

    return v0
.end method

.method public getAvailableVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lru/inetra/bytefog/service/UpdateInfo;->availableVersion:Ljava/lang/String;

    return-object v0
.end method

.method public getChangelog()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lru/inetra/bytefog/service/UpdateInfo;->changelog:Ljava/lang/String;

    return-object v0
.end method

.method public getDownloadUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lru/inetra/bytefog/service/UpdateInfo;->downloadUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getStatus()Lru/inetra/bytefog/service/UpdateStatus;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lru/inetra/bytefog/service/UpdateInfo;->status:Lru/inetra/bytefog/service/UpdateStatus;

    return-object v0
.end method

.method public setAvailableVersion(Ljava/lang/String;)V
    .locals 0
    .param p1, "availableVersion"    # Ljava/lang/String;

    .prologue
    .line 35
    iput-object p1, p0, Lru/inetra/bytefog/service/UpdateInfo;->availableVersion:Ljava/lang/String;

    .line 36
    return-void
.end method

.method public setChangelog(Ljava/lang/String;)V
    .locals 0
    .param p1, "changelog"    # Ljava/lang/String;

    .prologue
    .line 51
    iput-object p1, p0, Lru/inetra/bytefog/service/UpdateInfo;->changelog:Ljava/lang/String;

    .line 52
    return-void
.end method

.method public setDownloadUrl(Ljava/lang/String;)V
    .locals 0
    .param p1, "downloadUrl"    # Ljava/lang/String;

    .prologue
    .line 43
    iput-object p1, p0, Lru/inetra/bytefog/service/UpdateInfo;->downloadUrl:Ljava/lang/String;

    .line 44
    return-void
.end method

.method public setStatus(Lru/inetra/bytefog/service/UpdateStatus;)V
    .locals 0
    .param p1, "status"    # Lru/inetra/bytefog/service/UpdateStatus;

    .prologue
    .line 27
    iput-object p1, p0, Lru/inetra/bytefog/service/UpdateInfo;->status:Lru/inetra/bytefog/service/UpdateStatus;

    .line 28
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "parcel"    # Landroid/os/Parcel;
    .param p2, "i"    # I

    .prologue
    .line 61
    iget-object v0, p0, Lru/inetra/bytefog/service/UpdateInfo;->availableVersion:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 62
    iget-object v0, p0, Lru/inetra/bytefog/service/UpdateInfo;->downloadUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 63
    iget-object v0, p0, Lru/inetra/bytefog/service/UpdateInfo;->changelog:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 64
    return-void
.end method
