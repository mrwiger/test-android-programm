.class public Lcom/my/target/cn;
.super Ljava/lang/Object;
.source "UrlResolver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/cn$a;
    }
.end annotation


# static fields
.field private static final kz:[Ljava/lang/String;


# instance fields
.field private kA:Lcom/my/target/cn$a;

.field private final url:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 20
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "http://play.google.com"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "https://play.google.com"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "http://market.android.com"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "https://market.android.com"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "market://"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "samsungapps://"

    aput-object v2, v0, v1

    sput-object v0, Lcom/my/target/cn;->kz:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p1, p0, Lcom/my/target/cn;->url:Ljava/lang/String;

    .line 53
    return-void
.end method

.method public static S(Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 24
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 26
    sget-object v2, Lcom/my/target/cn;->kz:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 28
    invoke-virtual {p0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 30
    const/4 v0, 0x1

    .line 34
    :cond_0
    return v0

    .line 26
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static T(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 39
    const-string v0, "samsungapps://"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static U(Ljava/lang/String;)Lcom/my/target/cn;
    .locals 1

    .prologue
    .line 44
    new-instance v0, Lcom/my/target/cn;

    invoke-direct {v0, p0}, Lcom/my/target/cn;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method static synthetic a(Lcom/my/target/cn;Lcom/my/target/cn$a;)Lcom/my/target/cn$a;
    .locals 0

    .prologue
    .line 18
    iput-object p1, p0, Lcom/my/target/cn;->kA:Lcom/my/target/cn$a;

    return-object p1
.end method

.method static synthetic a(Lcom/my/target/cn;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/my/target/cn;->url:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/my/target/cn;)Lcom/my/target/cn$a;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/my/target/cn;->kA:Lcom/my/target/cn$a;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/my/target/cn$a;)Lcom/my/target/cn;
    .locals 0

    .prologue
    .line 57
    iput-object p1, p0, Lcom/my/target/cn;->kA:Lcom/my/target/cn$a;

    .line 58
    return-object p0
.end method

.method public y(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 63
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 64
    new-instance v1, Lcom/my/target/cn$1;

    invoke-direct {v1, p0, v0}, Lcom/my/target/cn$1;-><init>(Lcom/my/target/cn;Landroid/content/Context;)V

    invoke-static {v1}, Lcom/my/target/h;->b(Ljava/lang/Runnable;)V

    .line 89
    return-void
.end method
