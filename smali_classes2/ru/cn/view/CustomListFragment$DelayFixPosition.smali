.class Lru/cn/view/CustomListFragment$DelayFixPosition;
.super Ljava/lang/Object;
.source "CustomListFragment.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/view/CustomListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DelayFixPosition"
.end annotation


# instance fields
.field public position:I

.field final synthetic this$0:Lru/cn/view/CustomListFragment;


# direct methods
.method private constructor <init>(Lru/cn/view/CustomListFragment;)V
    .locals 0

    .prologue
    .line 13
    iput-object p1, p0, Lru/cn/view/CustomListFragment$DelayFixPosition;->this$0:Lru/cn/view/CustomListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lru/cn/view/CustomListFragment;Lru/cn/view/CustomListFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lru/cn/view/CustomListFragment;
    .param p2, "x1"    # Lru/cn/view/CustomListFragment$1;

    .prologue
    .line 13
    invoke-direct {p0, p1}, Lru/cn/view/CustomListFragment$DelayFixPosition;-><init>(Lru/cn/view/CustomListFragment;)V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 20
    iget-object v1, p0, Lru/cn/view/CustomListFragment$DelayFixPosition;->this$0:Lru/cn/view/CustomListFragment;

    invoke-virtual {v1}, Lru/cn/view/CustomListFragment;->getView()Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_0

    .line 30
    :goto_0
    return-void

    .line 23
    :cond_0
    iget-object v1, p0, Lru/cn/view/CustomListFragment$DelayFixPosition;->this$0:Lru/cn/view/CustomListFragment;

    invoke-virtual {v1}, Lru/cn/view/CustomListFragment;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 24
    .local v0, "vto":Landroid/view/ViewTreeObserver;
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-ge v1, v2, :cond_1

    .line 25
    iget-object v1, p0, Lru/cn/view/CustomListFragment$DelayFixPosition;->this$0:Lru/cn/view/CustomListFragment;

    invoke-static {v1}, Lru/cn/view/CustomListFragment;->access$000(Lru/cn/view/CustomListFragment;)Lru/cn/view/CustomListFragment$DelayFixPosition;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 29
    :goto_1
    iget-object v1, p0, Lru/cn/view/CustomListFragment$DelayFixPosition;->this$0:Lru/cn/view/CustomListFragment;

    iget v2, p0, Lru/cn/view/CustomListFragment$DelayFixPosition;->position:I

    invoke-virtual {v1, v2}, Lru/cn/view/CustomListFragment;->selectPosition(I)V

    goto :goto_0

    .line 27
    :cond_1
    iget-object v1, p0, Lru/cn/view/CustomListFragment$DelayFixPosition;->this$0:Lru/cn/view/CustomListFragment;

    invoke-static {v1}, Lru/cn/view/CustomListFragment;->access$000(Lru/cn/view/CustomListFragment;)Lru/cn/view/CustomListFragment$DelayFixPosition;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_1
.end method
