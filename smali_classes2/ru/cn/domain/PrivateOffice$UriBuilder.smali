.class public Lru/cn/domain/PrivateOffice$UriBuilder;
.super Ljava/lang/Object;
.source "PrivateOffice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/domain/PrivateOffice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UriBuilder"
.end annotation


# instance fields
.field private callbackUri:Ljava/lang/String;

.field private channelId:Ljava/lang/String;

.field private hash:Ljava/lang/String;

.field private final idiom:Ljava/lang/String;

.field private final officeAddress:Ljava/lang/String;

.field private reason:Ljava/lang/String;

.field private token:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "officeAddress"    # Ljava/lang/String;

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    invoke-static {}, Lru/cn/utils/Utils;->isTV()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 27
    const-string v0, "tv"

    iput-object v0, p0, Lru/cn/domain/PrivateOffice$UriBuilder;->idiom:Ljava/lang/String;

    .line 32
    :goto_0
    iput-object p1, p0, Lru/cn/domain/PrivateOffice$UriBuilder;->officeAddress:Ljava/lang/String;

    .line 33
    return-void

    .line 29
    :cond_0
    const-string v0, "mobile"

    iput-object v0, p0, Lru/cn/domain/PrivateOffice$UriBuilder;->idiom:Ljava/lang/String;

    goto :goto_0
.end method

.method private setReason(Ljava/lang/String;)V
    .locals 2
    .param p1, "reason"    # Ljava/lang/String;

    .prologue
    .line 66
    iget-object v0, p0, Lru/cn/domain/PrivateOffice$UriBuilder;->reason:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 67
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "reason already set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 69
    :cond_0
    iput-object p1, p0, Lru/cn/domain/PrivateOffice$UriBuilder;->reason:Ljava/lang/String;

    .line 70
    return-void
.end method


# virtual methods
.method public build()Landroid/net/Uri;
    .locals 4

    .prologue
    .line 73
    iget-object v2, p0, Lru/cn/domain/PrivateOffice$UriBuilder;->officeAddress:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 74
    .local v0, "b":Landroid/net/Uri$Builder;
    const-string v2, "inetra-platform-idiom"

    iget-object v3, p0, Lru/cn/domain/PrivateOffice$UriBuilder;->idiom:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 76
    iget-object v2, p0, Lru/cn/domain/PrivateOffice$UriBuilder;->callbackUri:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 77
    const-string v2, "inetra-callback-url"

    iget-object v3, p0, Lru/cn/domain/PrivateOffice$UriBuilder;->callbackUri:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 80
    :cond_0
    iget-object v2, p0, Lru/cn/domain/PrivateOffice$UriBuilder;->token:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 81
    const-string v2, "token"

    iget-object v3, p0, Lru/cn/domain/PrivateOffice$UriBuilder;->token:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 84
    :cond_1
    invoke-static {}, Lru/cn/domain/PurchaseManager;->externalStoreName()Ljava/lang/String;

    move-result-object v1

    .line 85
    .local v1, "externalStore":Ljava/lang/String;
    if-eqz v1, :cond_2

    .line 86
    const-string v2, "inetra-stores"

    invoke-virtual {v0, v2, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 89
    :cond_2
    iget-object v2, p0, Lru/cn/domain/PrivateOffice$UriBuilder;->reason:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 90
    const-string v2, "inetra-visit-reason"

    iget-object v3, p0, Lru/cn/domain/PrivateOffice$UriBuilder;->reason:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 93
    :cond_3
    iget-object v2, p0, Lru/cn/domain/PrivateOffice$UriBuilder;->channelId:Ljava/lang/String;

    if-eqz v2, :cond_4

    .line 94
    const-string v2, "inetra-channel-ids"

    iget-object v3, p0, Lru/cn/domain/PrivateOffice$UriBuilder;->channelId:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 97
    :cond_4
    iget-object v2, p0, Lru/cn/domain/PrivateOffice$UriBuilder;->hash:Ljava/lang/String;

    if-eqz v2, :cond_5

    .line 98
    iget-object v2, p0, Lru/cn/domain/PrivateOffice$UriBuilder;->hash:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/net/Uri$Builder;->encodedFragment(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 101
    :cond_5
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    return-object v2
.end method

.method public callback(Ljava/lang/String;)Lru/cn/domain/PrivateOffice$UriBuilder;
    .locals 0
    .param p1, "callbackUri"    # Ljava/lang/String;

    .prologue
    .line 41
    iput-object p1, p0, Lru/cn/domain/PrivateOffice$UriBuilder;->callbackUri:Ljava/lang/String;

    .line 42
    return-object p0
.end method

.method public channel(J)Lru/cn/domain/PrivateOffice$UriBuilder;
    .locals 3
    .param p1, "channelId"    # J

    .prologue
    .line 46
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gtz v0, :cond_0

    .line 47
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "channel id is incorrect"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 49
    :cond_0
    const-string v0, "buy-subscription"

    invoke-direct {p0, v0}, Lru/cn/domain/PrivateOffice$UriBuilder;->setReason(Ljava/lang/String;)V

    .line 50
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lru/cn/domain/PrivateOffice$UriBuilder;->channelId:Ljava/lang/String;

    .line 51
    return-object p0
.end method

.method public peersTVPlus()Lru/cn/domain/PrivateOffice$UriBuilder;
    .locals 1

    .prologue
    .line 60
    const-string v0, "buy-subscription"

    invoke-direct {p0, v0}, Lru/cn/domain/PrivateOffice$UriBuilder;->setReason(Ljava/lang/String;)V

    .line 61
    const-string v0, "/packet/2/"

    iput-object v0, p0, Lru/cn/domain/PrivateOffice$UriBuilder;->hash:Ljava/lang/String;

    .line 62
    return-object p0
.end method

.method public pinAuthorize()Lru/cn/domain/PrivateOffice$UriBuilder;
    .locals 1

    .prologue
    .line 55
    const-string v0, "authorize-for-parental-pincode"

    invoke-direct {p0, v0}, Lru/cn/domain/PrivateOffice$UriBuilder;->setReason(Ljava/lang/String;)V

    .line 56
    return-object p0
.end method

.method public token(Ljava/lang/String;)Lru/cn/domain/PrivateOffice$UriBuilder;
    .locals 0
    .param p1, "token"    # Ljava/lang/String;

    .prologue
    .line 36
    iput-object p1, p0, Lru/cn/domain/PrivateOffice$UriBuilder;->token:Ljava/lang/String;

    .line 37
    return-object p0
.end method
