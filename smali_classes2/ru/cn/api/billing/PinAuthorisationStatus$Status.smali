.class public final enum Lru/cn/api/billing/PinAuthorisationStatus$Status;
.super Ljava/lang/Enum;
.source "PinAuthorisationStatus.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/api/billing/PinAuthorisationStatus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Status"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lru/cn/api/billing/PinAuthorisationStatus$Status;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lru/cn/api/billing/PinAuthorisationStatus$Status;

.field public static final enum ACCEPTED:Lru/cn/api/billing/PinAuthorisationStatus$Status;

.field public static final enum NOT_REQUESTED:Lru/cn/api/billing/PinAuthorisationStatus$Status;

.field public static final enum REJECTED:Lru/cn/api/billing/PinAuthorisationStatus$Status;


# instance fields
.field private value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 7
    new-instance v0, Lru/cn/api/billing/PinAuthorisationStatus$Status;

    const-string v1, "ACCEPTED"

    invoke-direct {v0, v1, v2, v2}, Lru/cn/api/billing/PinAuthorisationStatus$Status;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/cn/api/billing/PinAuthorisationStatus$Status;->ACCEPTED:Lru/cn/api/billing/PinAuthorisationStatus$Status;

    .line 8
    new-instance v0, Lru/cn/api/billing/PinAuthorisationStatus$Status;

    const-string v1, "REJECTED"

    invoke-direct {v0, v1, v3, v3}, Lru/cn/api/billing/PinAuthorisationStatus$Status;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/cn/api/billing/PinAuthorisationStatus$Status;->REJECTED:Lru/cn/api/billing/PinAuthorisationStatus$Status;

    .line 9
    new-instance v0, Lru/cn/api/billing/PinAuthorisationStatus$Status;

    const-string v1, "NOT_REQUESTED"

    invoke-direct {v0, v1, v4, v4}, Lru/cn/api/billing/PinAuthorisationStatus$Status;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/cn/api/billing/PinAuthorisationStatus$Status;->NOT_REQUESTED:Lru/cn/api/billing/PinAuthorisationStatus$Status;

    .line 6
    const/4 v0, 0x3

    new-array v0, v0, [Lru/cn/api/billing/PinAuthorisationStatus$Status;

    sget-object v1, Lru/cn/api/billing/PinAuthorisationStatus$Status;->ACCEPTED:Lru/cn/api/billing/PinAuthorisationStatus$Status;

    aput-object v1, v0, v2

    sget-object v1, Lru/cn/api/billing/PinAuthorisationStatus$Status;->REJECTED:Lru/cn/api/billing/PinAuthorisationStatus$Status;

    aput-object v1, v0, v3

    sget-object v1, Lru/cn/api/billing/PinAuthorisationStatus$Status;->NOT_REQUESTED:Lru/cn/api/billing/PinAuthorisationStatus$Status;

    aput-object v1, v0, v4

    sput-object v0, Lru/cn/api/billing/PinAuthorisationStatus$Status;->$VALUES:[Lru/cn/api/billing/PinAuthorisationStatus$Status;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 13
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 14
    iput p3, p0, Lru/cn/api/billing/PinAuthorisationStatus$Status;->value:I

    .line 15
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lru/cn/api/billing/PinAuthorisationStatus$Status;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 6
    const-class v0, Lru/cn/api/billing/PinAuthorisationStatus$Status;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lru/cn/api/billing/PinAuthorisationStatus$Status;

    return-object v0
.end method

.method public static values()[Lru/cn/api/billing/PinAuthorisationStatus$Status;
    .locals 1

    .prologue
    .line 6
    sget-object v0, Lru/cn/api/billing/PinAuthorisationStatus$Status;->$VALUES:[Lru/cn/api/billing/PinAuthorisationStatus$Status;

    invoke-virtual {v0}, [Lru/cn/api/billing/PinAuthorisationStatus$Status;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lru/cn/api/billing/PinAuthorisationStatus$Status;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 18
    iget v0, p0, Lru/cn/api/billing/PinAuthorisationStatus$Status;->value:I

    return v0
.end method
