.class final Lcom/my/target/ei$a;
.super Ljava/lang/Object;
.source "VideoStyleView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/my/target/ei;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic cD:Lcom/my/target/ei;


# direct methods
.method private constructor <init>(Lcom/my/target/ei;)V
    .locals 0

    .prologue
    .line 777
    iput-object p1, p0, Lcom/my/target/ei$a;->cD:Lcom/my/target/ei;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/my/target/ei;B)V
    .locals 0

    .prologue
    .line 777
    invoke-direct {p0, p1}, Lcom/my/target/ei$a;-><init>(Lcom/my/target/ei;)V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 782
    iget-object v0, p0, Lcom/my/target/ei$a;->cD:Lcom/my/target/ei;

    invoke-static {v0}, Lcom/my/target/ei;->a(Lcom/my/target/ei;)Landroid/widget/LinearLayout;

    move-result-object v0

    if-ne p1, v0, :cond_2

    .line 784
    iget-object v0, p0, Lcom/my/target/ei$a;->cD:Lcom/my/target/ei;

    iget-object v0, v0, Lcom/my/target/ei;->ad:Lcom/my/target/ee$b;

    if-eqz v0, :cond_0

    .line 786
    iget-object v0, p0, Lcom/my/target/ei$a;->cD:Lcom/my/target/ei;

    iget-object v0, v0, Lcom/my/target/ei;->ad:Lcom/my/target/ee$b;

    invoke-interface {v0}, Lcom/my/target/ee$b;->onPlayClicked()V

    .line 788
    :cond_0
    iget-object v0, p0, Lcom/my/target/ei$a;->cD:Lcom/my/target/ei;

    invoke-static {v0}, Lcom/my/target/ei;->b(Lcom/my/target/ei;)V

    .line 815
    :cond_1
    :goto_0
    return-void

    .line 790
    :cond_2
    iget-object v0, p0, Lcom/my/target/ei$a;->cD:Lcom/my/target/ei;

    invoke-static {v0}, Lcom/my/target/ei;->c(Lcom/my/target/ei;)Lcom/my/target/ed;

    move-result-object v0

    if-ne p1, v0, :cond_3

    .line 792
    iget-object v0, p0, Lcom/my/target/ei$a;->cD:Lcom/my/target/ei;

    invoke-static {v0}, Lcom/my/target/ei;->d(Lcom/my/target/ei;)Lcom/my/target/eg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/my/target/eg;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 794
    iget-object v0, p0, Lcom/my/target/ei$a;->cD:Lcom/my/target/ei;

    iget-object v0, v0, Lcom/my/target/ei;->ad:Lcom/my/target/ee$b;

    if-eqz v0, :cond_1

    .line 796
    iget-object v0, p0, Lcom/my/target/ei$a;->cD:Lcom/my/target/ei;

    iget-object v0, v0, Lcom/my/target/ei;->ad:Lcom/my/target/ee$b;

    invoke-interface {v0}, Lcom/my/target/ee$b;->onPauseClicked()V

    goto :goto_0

    .line 800
    :cond_3
    iget-object v0, p0, Lcom/my/target/ei$a;->cD:Lcom/my/target/ei;

    invoke-static {v0}, Lcom/my/target/ei;->e(Lcom/my/target/ei;)Lcom/my/target/ed;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 802
    iget-object v0, p0, Lcom/my/target/ei$a;->cD:Lcom/my/target/ei;

    iget-object v0, v0, Lcom/my/target/ei;->ad:Lcom/my/target/ee$b;

    if-eqz v0, :cond_4

    .line 804
    iget-object v0, p0, Lcom/my/target/ei$a;->cD:Lcom/my/target/ei;

    invoke-virtual {v0}, Lcom/my/target/ei;->isPaused()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 806
    iget-object v0, p0, Lcom/my/target/ei$a;->cD:Lcom/my/target/ei;

    iget-object v0, v0, Lcom/my/target/ei;->ad:Lcom/my/target/ee$b;

    invoke-interface {v0}, Lcom/my/target/ee$b;->D()V

    .line 813
    :cond_4
    :goto_1
    iget-object v0, p0, Lcom/my/target/ei$a;->cD:Lcom/my/target/ei;

    invoke-static {v0}, Lcom/my/target/ei;->b(Lcom/my/target/ei;)V

    goto :goto_0

    .line 810
    :cond_5
    iget-object v0, p0, Lcom/my/target/ei$a;->cD:Lcom/my/target/ei;

    iget-object v0, v0, Lcom/my/target/ei;->ad:Lcom/my/target/ee$b;

    invoke-interface {v0}, Lcom/my/target/ee$b;->onPlayClicked()V

    goto :goto_1
.end method
