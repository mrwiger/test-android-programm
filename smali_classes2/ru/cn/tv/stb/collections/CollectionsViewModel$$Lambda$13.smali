.class final synthetic Lru/cn/tv/stb/collections/CollectionsViewModel$$Lambda$13;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/annimon/stream/function/Function;


# static fields
.field static final $instance:Lcom/annimon/stream/function/Function;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lru/cn/tv/stb/collections/CollectionsViewModel$$Lambda$13;

    invoke-direct {v0}, Lru/cn/tv/stb/collections/CollectionsViewModel$$Lambda$13;-><init>()V

    sput-object v0, Lru/cn/tv/stb/collections/CollectionsViewModel$$Lambda$13;->$instance:Lcom/annimon/stream/function/Function;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, Lru/cn/api/catalogue/replies/Rubric;

    invoke-static {p1}, Lru/cn/domain/Collections;->sortPredicate(Lru/cn/api/catalogue/replies/Rubric;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method
