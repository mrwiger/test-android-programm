.class public final Lcom/my/target/fo;
.super Lcom/my/target/d;
.source "InstreamAdResponseParser.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/my/target/d",
        "<",
        "Lcom/my/target/fq;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/my/target/d;-><init>()V

    .line 42
    return-void
.end method

.method private static a(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/my/target/ae;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/my/target/ae;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 309
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/ae;

    .line 311
    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/my/target/ae;

    .line 313
    invoke-virtual {v0}, Lcom/my/target/ae;->z()I

    move-result v4

    invoke-virtual {v1}, Lcom/my/target/ae;->getId()I

    move-result v5

    if-ne v4, v5, :cond_1

    .line 315
    invoke-virtual {v1, v0}, Lcom/my/target/ae;->a(Lcom/my/target/ae;)V

    goto :goto_0

    .line 320
    :cond_2
    return-void
.end method

.method private static a(Lorg/json/JSONObject;Lcom/my/target/bd;Lcom/my/target/al;Lcom/my/target/bg;Lcom/my/target/ae;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            "Lcom/my/target/bd;",
            "Lcom/my/target/al",
            "<",
            "Lcom/my/target/common/models/VideoData;",
            ">;",
            "Lcom/my/target/bg;",
            "Lcom/my/target/ae;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 219
    invoke-virtual {p2}, Lcom/my/target/al;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 220
    if-nez v3, :cond_0

    .line 269
    :goto_0
    return-void

    .line 225
    :cond_0
    invoke-virtual {p4}, Lcom/my/target/ae;->getPosition()I

    move-result v1

    .line 226
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 227
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 228
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v0, v2, :cond_9

    .line 230
    invoke-virtual {v3, v0}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    .line 231
    if-eqz v2, :cond_1

    .line 236
    const-string v6, "type"

    invoke-virtual {v2, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 237
    const-string v7, "additionalData"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1278
    invoke-virtual {p1, v2}, Lcom/my/target/bd;->c(Lorg/json/JSONObject;)Lcom/my/target/ae;

    move-result-object v2

    .line 1279
    if-eqz v2, :cond_1

    .line 1283
    invoke-virtual {p2}, Lcom/my/target/al;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/my/target/ae;->o(Ljava/lang/String;)V

    .line 1284
    invoke-virtual {v2}, Lcom/my/target/ae;->z()I

    move-result v6

    const/4 v7, -0x1

    if-eq v6, v7, :cond_2

    .line 1286
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 228
    :cond_1
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1289
    :cond_2
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1290
    invoke-virtual {v2}, Lcom/my/target/ae;->A()Z

    move-result v6

    if-nez v6, :cond_3

    invoke-virtual {v2}, Lcom/my/target/ae;->y()Z

    move-result v6

    if-nez v6, :cond_3

    .line 1292
    invoke-virtual {p4, v2}, Lcom/my/target/ae;->b(Lcom/my/target/ae;)V

    .line 1293
    invoke-virtual {p4}, Lcom/my/target/ae;->getPosition()I

    move-result v6

    .line 1294
    if-ltz v6, :cond_4

    .line 1296
    invoke-virtual {v2, v6}, Lcom/my/target/ae;->b(I)V

    .line 1303
    :cond_3
    :goto_3
    invoke-virtual {p2, v2}, Lcom/my/target/al;->c(Lcom/my/target/ae;)V

    goto :goto_2

    .line 1300
    :cond_4
    invoke-virtual {p2}, Lcom/my/target/al;->getBannersCount()I

    move-result v6

    invoke-virtual {v2, v6}, Lcom/my/target/ae;->b(I)V

    goto :goto_3

    .line 243
    :cond_5
    invoke-static {}, Lcom/my/target/aj;->newVideoBanner()Lcom/my/target/aj;

    move-result-object v6

    .line 244
    const/4 v7, 0x0

    invoke-virtual {p3, v2, v6, v7}, Lcom/my/target/bg;->a(Lorg/json/JSONObject;Lcom/my/target/aj;Lcom/my/target/am;)Z

    move-result v2

    .line 245
    if-eqz v2, :cond_1

    .line 247
    invoke-virtual {p4}, Lcom/my/target/ae;->getPoint()F

    move-result v2

    .line 248
    cmpl-float v7, v2, v8

    if-ltz v7, :cond_6

    .line 250
    invoke-virtual {v6, v2}, Lcom/my/target/aj;->setPoint(F)V

    .line 252
    :cond_6
    invoke-virtual {p4}, Lcom/my/target/ae;->getPointP()F

    move-result v2

    .line 253
    cmpl-float v7, v2, v8

    if-ltz v7, :cond_7

    .line 255
    invoke-virtual {v6, v2}, Lcom/my/target/aj;->setPointP(F)V

    .line 257
    :cond_7
    if-ltz v1, :cond_8

    .line 259
    add-int/lit8 v2, v1, 0x1

    invoke-virtual {p2, v6, v1}, Lcom/my/target/al;->a(Lcom/my/target/aj;I)V

    move v1, v2

    goto :goto_2

    .line 263
    :cond_8
    invoke-virtual {p2, v6}, Lcom/my/target/al;->a(Lcom/my/target/aj;)V

    goto :goto_2

    .line 268
    :cond_9
    invoke-static {v5, v4}, Lcom/my/target/fo;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    goto/16 :goto_0
.end method

.method public static newParser()Lcom/my/target/d;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/my/target/d",
            "<",
            "Lcom/my/target/fq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 36
    new-instance v0, Lcom/my/target/fo;

    invoke-direct {v0}, Lcom/my/target/fo;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/String;Lcom/my/target/ae;Lcom/my/target/ak;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/ak;
    .locals 6

    .prologue
    .line 32
    check-cast p3, Lcom/my/target/fq;

    .line 2051
    invoke-static {p1}, Lcom/my/target/fo;->isVast(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 2065
    invoke-static {p4, p2, p5}, Lcom/my/target/bi;->a(Lcom/my/target/b;Lcom/my/target/ae;Landroid/content/Context;)Lcom/my/target/bi;

    move-result-object v1

    .line 2066
    invoke-virtual {v1, p1}, Lcom/my/target/bi;->F(Ljava/lang/String;)V

    .line 2068
    invoke-virtual {p2}, Lcom/my/target/ae;->H()Ljava/lang/String;

    move-result-object v0

    .line 2069
    if-nez v0, :cond_0

    .line 2071
    const-string v0, "preroll"

    .line 2073
    :cond_0
    if-nez p3, :cond_1

    .line 2075
    invoke-static {}, Lcom/my/target/fq;->h()Lcom/my/target/fq;

    move-result-object p3

    .line 2078
    :cond_1
    invoke-virtual {p3, v0}, Lcom/my/target/fq;->a(Ljava/lang/String;)Lcom/my/target/al;

    move-result-object v3

    .line 2079
    if-eqz v3, :cond_7

    .line 2084
    invoke-virtual {v1}, Lcom/my/target/bi;->as()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 2115
    invoke-virtual {v1}, Lcom/my/target/bi;->F()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/my/target/al;->e(Ljava/util/ArrayList;)V

    .line 2117
    invoke-virtual {p2}, Lcom/my/target/ae;->getPosition()I

    move-result v0

    .line 2118
    invoke-virtual {v1}, Lcom/my/target/bi;->as()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/aj;

    .line 2120
    invoke-virtual {p2}, Lcom/my/target/ae;->J()Ljava/lang/Boolean;

    move-result-object v2

    .line 2121
    if-eqz v2, :cond_3

    .line 2123
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/my/target/aj;->setAllowClose(Z)V

    .line 2130
    :goto_1
    invoke-virtual {p2}, Lcom/my/target/ae;->getAllowCloseDelay()F

    move-result v2

    .line 2131
    const/4 v5, 0x0

    cmpl-float v5, v2, v5

    if-lez v5, :cond_4

    .line 2133
    invoke-virtual {v0, v2}, Lcom/my/target/aj;->setAllowCloseDelay(F)V

    .line 2140
    :goto_2
    invoke-virtual {p2}, Lcom/my/target/ae;->K()Ljava/lang/Boolean;

    move-result-object v2

    .line 2141
    if-eqz v2, :cond_2

    .line 2143
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/my/target/aj;->setAllowPause(Z)V

    .line 2146
    :cond_2
    const-string v2, "Close"

    invoke-virtual {v0, v2}, Lcom/my/target/aj;->setCloseActionText(Ljava/lang/String;)V

    .line 2148
    invoke-virtual {p2}, Lcom/my/target/ae;->getPoint()F

    move-result v2

    invoke-virtual {v0, v2}, Lcom/my/target/aj;->setPoint(F)V

    .line 2149
    invoke-virtual {p2}, Lcom/my/target/ae;->getPointP()F

    move-result v2

    invoke-virtual {v0, v2}, Lcom/my/target/aj;->setPointP(F)V

    .line 2150
    if-ltz v1, :cond_5

    .line 2152
    add-int/lit8 v2, v1, 0x1

    invoke-virtual {v3, v0, v1}, Lcom/my/target/al;->a(Lcom/my/target/aj;I)V

    move v1, v2

    goto :goto_0

    .line 2127
    :cond_3
    invoke-virtual {v3}, Lcom/my/target/al;->isAllowClose()Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/my/target/aj;->setAllowClose(Z)V

    goto :goto_1

    .line 2137
    :cond_4
    invoke-virtual {v3}, Lcom/my/target/al;->getAllowCloseDelay()F

    move-result v2

    invoke-virtual {v0, v2}, Lcom/my/target/aj;->setAllowCloseDelay(F)V

    goto :goto_2

    .line 2156
    :cond_5
    invoke-virtual {v3, v0}, Lcom/my/target/al;->a(Lcom/my/target/aj;)V

    goto :goto_0

    .line 2090
    :cond_6
    invoke-virtual {v1}, Lcom/my/target/bi;->at()Lcom/my/target/ae;

    move-result-object v0

    .line 2091
    if-eqz v0, :cond_7

    .line 2093
    invoke-virtual {v3}, Lcom/my/target/al;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/my/target/ae;->o(Ljava/lang/String;)V

    .line 2094
    invoke-virtual {p2}, Lcom/my/target/ae;->getPosition()I

    move-result v1

    .line 2095
    if-ltz v1, :cond_8

    .line 2097
    invoke-virtual {v0, v1}, Lcom/my/target/ae;->b(I)V

    .line 2103
    :goto_3
    invoke-virtual {v3, v0}, Lcom/my/target/al;->c(Lcom/my/target/ae;)V

    .line 2053
    :cond_7
    :goto_4
    return-object p3

    .line 2101
    :cond_8
    invoke-virtual {v3}, Lcom/my/target/al;->getBannersCount()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/my/target/ae;->b(I)V

    goto :goto_3

    .line 2167
    :cond_9
    invoke-virtual {p0, p1, p5}, Lcom/my/target/fo;->a(Ljava/lang/String;Landroid/content/Context;)Lorg/json/JSONObject;

    move-result-object v0

    .line 2168
    if-eqz v0, :cond_7

    .line 2173
    invoke-virtual {p4}, Lcom/my/target/b;->getFormat()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 2174
    if-eqz v0, :cond_7

    .line 2179
    if-nez p3, :cond_a

    .line 2181
    invoke-static {}, Lcom/my/target/fq;->h()Lcom/my/target/fq;

    move-result-object p3

    .line 2183
    :cond_a
    invoke-static {p2, p4, p5}, Lcom/my/target/core/parsers/l;->a(Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/core/parsers/l;

    move-result-object v1

    invoke-virtual {v1, v0, p3}, Lcom/my/target/core/parsers/l;->a(Lorg/json/JSONObject;Lcom/my/target/fq;)V

    .line 2185
    invoke-static {p2, p4, p5}, Lcom/my/target/bd;->a(Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/bd;

    move-result-object v1

    .line 2187
    const-string v2, "sections"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 2188
    if-eqz v2, :cond_7

    .line 2190
    invoke-virtual {p2}, Lcom/my/target/ae;->H()Ljava/lang/String;

    move-result-object v0

    .line 2191
    if-eqz v0, :cond_b

    .line 2193
    invoke-virtual {p3, v0}, Lcom/my/target/fq;->a(Ljava/lang/String;)Lcom/my/target/al;

    move-result-object v0

    .line 2194
    if-eqz v0, :cond_7

    .line 2196
    invoke-static {v0, p2, p4, p5}, Lcom/my/target/bg;->b(Lcom/my/target/ak;Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/bg;

    move-result-object v3

    .line 2197
    invoke-static {v2, v1, v0, v3, p2}, Lcom/my/target/fo;->a(Lorg/json/JSONObject;Lcom/my/target/bd;Lcom/my/target/al;Lcom/my/target/bg;Lcom/my/target/ae;)V

    goto :goto_4

    .line 2202
    :cond_b
    invoke-virtual {p3}, Lcom/my/target/fq;->i()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/al;

    .line 2204
    invoke-static {v0, p2, p4, p5}, Lcom/my/target/bg;->b(Lcom/my/target/ak;Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/bg;

    move-result-object v4

    .line 2205
    invoke-static {v2, v1, v0, v4, p2}, Lcom/my/target/fo;->a(Lorg/json/JSONObject;Lcom/my/target/bd;Lcom/my/target/al;Lcom/my/target/bg;Lcom/my/target/ae;)V

    goto :goto_5
.end method
