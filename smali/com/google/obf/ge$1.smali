.class final Lcom/google/obf/ge$1;
.super Lcom/google/obf/fh;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/obf/ge;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lcom/google/obf/fh;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/obf/ge;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2
    instance-of v0, p1, Lcom/google/obf/fs;

    if-eqz v0, :cond_0

    .line 3
    check-cast p1, Lcom/google/obf/fs;

    invoke-virtual {p1}, Lcom/google/obf/fs;->o()V

    .line 16
    :goto_0
    return-void

    .line 5
    :cond_0
    iget v0, p1, Lcom/google/obf/ge;->a:I

    .line 6
    if-nez v0, :cond_1

    .line 7
    invoke-virtual {p1}, Lcom/google/obf/ge;->r()I

    move-result v0

    .line 8
    :cond_1
    const/16 v1, 0xd

    if-ne v0, v1, :cond_2

    .line 9
    const/16 v0, 0x9

    iput v0, p1, Lcom/google/obf/ge;->a:I

    goto :goto_0

    .line 10
    :cond_2
    const/16 v1, 0xc

    if-ne v0, v1, :cond_3

    .line 11
    const/16 v0, 0x8

    iput v0, p1, Lcom/google/obf/ge;->a:I

    goto :goto_0

    .line 12
    :cond_3
    const/16 v1, 0xe

    if-ne v0, v1, :cond_4

    .line 13
    const/16 v0, 0xa

    iput v0, p1, Lcom/google/obf/ge;->a:I

    goto :goto_0

    .line 14
    :cond_4
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected a name but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 15
    invoke-virtual {p1}, Lcom/google/obf/ge;->f()Lcom/google/obf/gf;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Lcom/google/obf/ge;->a(Lcom/google/obf/ge;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
