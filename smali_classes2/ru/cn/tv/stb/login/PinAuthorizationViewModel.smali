.class Lru/cn/tv/stb/login/PinAuthorizationViewModel;
.super Lru/cn/mvvm/RxViewModel;
.source "PinAuthorizationViewModel.java"


# instance fields
.field private completed:Landroid/arch/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/arch/lifecycle/MutableLiveData",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private expired:Landroid/arch/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/arch/lifecycle/MutableLiveData",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private pinCode:Landroid/arch/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/arch/lifecycle/MutableLiveData",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Lru/cn/mvvm/RxViewModel;-><init>()V

    .line 30
    new-instance v0, Landroid/arch/lifecycle/MutableLiveData;

    invoke-direct {v0}, Landroid/arch/lifecycle/MutableLiveData;-><init>()V

    iput-object v0, p0, Lru/cn/tv/stb/login/PinAuthorizationViewModel;->pinCode:Landroid/arch/lifecycle/MutableLiveData;

    .line 31
    new-instance v0, Landroid/arch/lifecycle/MutableLiveData;

    invoke-direct {v0}, Landroid/arch/lifecycle/MutableLiveData;-><init>()V

    iput-object v0, p0, Lru/cn/tv/stb/login/PinAuthorizationViewModel;->completed:Landroid/arch/lifecycle/MutableLiveData;

    .line 32
    new-instance v0, Landroid/arch/lifecycle/MutableLiveData;

    invoke-direct {v0}, Landroid/arch/lifecycle/MutableLiveData;-><init>()V

    iput-object v0, p0, Lru/cn/tv/stb/login/PinAuthorizationViewModel;->expired:Landroid/arch/lifecycle/MutableLiveData;

    .line 33
    return-void
.end method

.method private deviceCode(Landroid/content/Context;)Lio/reactivex/Single;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lio/reactivex/Single",
            "<",
            "Lru/cn/api/authorization/replies/DeviceCodeReply;",
            ">;"
        }
    .end annotation

    .prologue
    .line 77
    const-wide/16 v0, 0x2

    invoke-static {p1, v0, v1}, Lru/cn/api/authorization/Authorization;->deviceCode(Landroid/content/Context;J)Lio/reactivex/Single;

    move-result-object v0

    .line 78
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v0

    .line 77
    return-object v0
.end method

.method static final synthetic lambda$null$2$PinAuthorizationViewModel(Landroid/content/Context;Lru/cn/api/authorization/replies/DeviceCodeReply;Ljava/lang/Long;)Ljava/lang/Boolean;
    .locals 4
    .param p0, "appContext"    # Landroid/content/Context;
    .param p1, "it"    # Lru/cn/api/authorization/replies/DeviceCodeReply;
    .param p2, "step"    # Ljava/lang/Long;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 64
    iget-object v0, p1, Lru/cn/api/authorization/replies/DeviceCodeReply;->deviceCode:Ljava/lang/String;

    const-wide/16 v2, 0x2

    invoke-static {p0, v0, v2, v3}, Lru/cn/api/authorization/Authorization;->completeDeviceCode(Landroid/content/Context;Ljava/lang/String;J)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method static final synthetic lambda$null$3$PinAuthorizationViewModel(Ljava/lang/Boolean;)Z
    .locals 1
    .param p0, "success"    # Ljava/lang/Boolean;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 65
    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method static final synthetic lambda$start$0$PinAuthorizationViewModel(Lru/cn/api/authorization/replies/DeviceCodeReply;)Ljava/lang/String;
    .locals 1
    .param p0, "it"    # Lru/cn/api/authorization/replies/DeviceCodeReply;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 57
    iget-object v0, p0, Lru/cn/api/authorization/replies/DeviceCodeReply;->pinCode:Ljava/lang/String;

    return-object v0
.end method

.method static final synthetic lambda$start$4$PinAuthorizationViewModel(Landroid/content/Context;Lru/cn/api/authorization/replies/DeviceCodeReply;)Lio/reactivex/ObservableSource;
    .locals 4
    .param p0, "appContext"    # Landroid/content/Context;
    .param p1, "it"    # Lru/cn/api/authorization/replies/DeviceCodeReply;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 62
    iget v0, p1, Lru/cn/api/authorization/replies/DeviceCodeReply;->interval:I

    int-to-long v0, v0

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    .line 63
    invoke-static {v0, v1, v2}, Lio/reactivex/Observable;->interval(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lru/cn/tv/stb/login/PinAuthorizationViewModel$$Lambda$6;

    invoke-direct {v1, p0, p1}, Lru/cn/tv/stb/login/PinAuthorizationViewModel$$Lambda$6;-><init>(Landroid/content/Context;Lru/cn/api/authorization/replies/DeviceCodeReply;)V

    .line 64
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lru/cn/tv/stb/login/PinAuthorizationViewModel$$Lambda$7;->$instance:Lio/reactivex/functions/Predicate;

    .line 65
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->filter(Lio/reactivex/functions/Predicate;)Lio/reactivex/Observable;

    move-result-object v0

    const-wide/16 v2, 0x1

    .line 66
    invoke-virtual {v0, v2, v3}, Lio/reactivex/Observable;->take(J)Lio/reactivex/Observable;

    move-result-object v0

    .line 62
    return-object v0
.end method

.method static final synthetic lambda$start$6$PinAuthorizationViewModel(Lru/cn/api/authorization/replies/DeviceCodeReply;)Lio/reactivex/ObservableSource;
    .locals 3
    .param p0, "it"    # Lru/cn/api/authorization/replies/DeviceCodeReply;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 71
    iget v0, p0, Lru/cn/api/authorization/replies/DeviceCodeReply;->expiresIn:I

    int-to-long v0, v0

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {v0, v1, v2}, Lio/reactivex/Observable;->timer(JLjava/util/concurrent/TimeUnit;)Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method completed()Landroid/arch/lifecycle/LiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/arch/lifecycle/LiveData",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 40
    iget-object v0, p0, Lru/cn/tv/stb/login/PinAuthorizationViewModel;->completed:Landroid/arch/lifecycle/MutableLiveData;

    return-object v0
.end method

.method expired()Landroid/arch/lifecycle/LiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/arch/lifecycle/LiveData",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, Lru/cn/tv/stb/login/PinAuthorizationViewModel;->expired:Landroid/arch/lifecycle/MutableLiveData;

    return-object v0
.end method

.method final synthetic lambda$start$1$PinAuthorizationViewModel(Ljava/lang/String;)V
    .locals 1
    .param p1, "it"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 59
    iget-object v0, p0, Lru/cn/tv/stb/login/PinAuthorizationViewModel;->pinCode:Landroid/arch/lifecycle/MutableLiveData;

    invoke-virtual {v0, p1}, Landroid/arch/lifecycle/MutableLiveData;->setValue(Ljava/lang/Object;)V

    return-void
.end method

.method final synthetic lambda$start$5$PinAuthorizationViewModel(Ljava/lang/Boolean;)V
    .locals 1
    .param p1, "it"    # Ljava/lang/Boolean;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 68
    iget-object v0, p0, Lru/cn/tv/stb/login/PinAuthorizationViewModel;->completed:Landroid/arch/lifecycle/MutableLiveData;

    invoke-virtual {v0, p1}, Landroid/arch/lifecycle/MutableLiveData;->setValue(Ljava/lang/Object;)V

    return-void
.end method

.method final synthetic lambda$start$7$PinAuthorizationViewModel(Ljava/lang/Long;)V
    .locals 2
    .param p1, "it"    # Ljava/lang/Long;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 73
    iget-object v0, p0, Lru/cn/tv/stb/login/PinAuthorizationViewModel;->expired:Landroid/arch/lifecycle/MutableLiveData;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/arch/lifecycle/MutableLiveData;->setValue(Ljava/lang/Object;)V

    return-void
.end method

.method pinCode()Landroid/arch/lifecycle/LiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/arch/lifecycle/LiveData",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 36
    iget-object v0, p0, Lru/cn/tv/stb/login/PinAuthorizationViewModel;->pinCode:Landroid/arch/lifecycle/MutableLiveData;

    return-object v0
.end method

.method start(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 48
    invoke-virtual {p0}, Lru/cn/tv/stb/login/PinAuthorizationViewModel;->unbindAll()V

    .line 50
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 52
    .local v0, "appContext":Landroid/content/Context;
    invoke-direct {p0, p1}, Lru/cn/tv/stb/login/PinAuthorizationViewModel;->deviceCode(Landroid/content/Context;)Lio/reactivex/Single;

    move-result-object v2

    .line 53
    invoke-virtual {v2}, Lio/reactivex/Single;->toObservable()Lio/reactivex/Observable;

    move-result-object v2

    .line 54
    invoke-virtual {v2}, Lio/reactivex/Observable;->share()Lio/reactivex/Observable;

    move-result-object v1

    .line 56
    .local v1, "deviceCode":Lio/reactivex/Observable;, "Lio/reactivex/Observable<Lru/cn/api/authorization/replies/DeviceCodeReply;>;"
    sget-object v2, Lru/cn/tv/stb/login/PinAuthorizationViewModel$$Lambda$0;->$instance:Lio/reactivex/functions/Function;

    .line 57
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    .line 58
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v2

    new-instance v3, Lru/cn/tv/stb/login/PinAuthorizationViewModel$$Lambda$1;

    invoke-direct {v3, p0}, Lru/cn/tv/stb/login/PinAuthorizationViewModel$$Lambda$1;-><init>(Lru/cn/tv/stb/login/PinAuthorizationViewModel;)V

    .line 59
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v2

    .line 56
    invoke-virtual {p0, v2}, Lru/cn/tv/stb/login/PinAuthorizationViewModel;->bind(Lio/reactivex/disposables/Disposable;)V

    .line 61
    new-instance v2, Lru/cn/tv/stb/login/PinAuthorizationViewModel$$Lambda$2;

    invoke-direct {v2, v0}, Lru/cn/tv/stb/login/PinAuthorizationViewModel$$Lambda$2;-><init>(Landroid/content/Context;)V

    .line 62
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    .line 67
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v2

    new-instance v3, Lru/cn/tv/stb/login/PinAuthorizationViewModel$$Lambda$3;

    invoke-direct {v3, p0}, Lru/cn/tv/stb/login/PinAuthorizationViewModel$$Lambda$3;-><init>(Lru/cn/tv/stb/login/PinAuthorizationViewModel;)V

    .line 68
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v2

    .line 61
    invoke-virtual {p0, v2}, Lru/cn/tv/stb/login/PinAuthorizationViewModel;->bind(Lio/reactivex/disposables/Disposable;)V

    .line 70
    sget-object v2, Lru/cn/tv/stb/login/PinAuthorizationViewModel$$Lambda$4;->$instance:Lio/reactivex/functions/Function;

    .line 71
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    .line 72
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v2

    new-instance v3, Lru/cn/tv/stb/login/PinAuthorizationViewModel$$Lambda$5;

    invoke-direct {v3, p0}, Lru/cn/tv/stb/login/PinAuthorizationViewModel$$Lambda$5;-><init>(Lru/cn/tv/stb/login/PinAuthorizationViewModel;)V

    .line 73
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v2

    .line 70
    invoke-virtual {p0, v2}, Lru/cn/tv/stb/login/PinAuthorizationViewModel;->bind(Lio/reactivex/disposables/Disposable;)V

    .line 74
    return-void
.end method
