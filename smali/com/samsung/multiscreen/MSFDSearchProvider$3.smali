.class final Lcom/samsung/multiscreen/MSFDSearchProvider$3;
.super Ljava/lang/Object;
.source "MSFDSearchProvider.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/multiscreen/MSFDSearchProvider;->getById(Landroid/content/Context;Ljava/lang/String;Lcom/samsung/multiscreen/Result;)Lcom/samsung/multiscreen/ProviderThread;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field private processing:Z

.field private searching:Z

.field final synthetic val$executor:Ljava/util/concurrent/ScheduledExecutorService;

.field final synthetic val$multicastInetAddress:Ljava/net/InetAddress;

.field final synthetic val$multicastLock:Landroid/net/wifi/WifiManager$MulticastLock;

.field final synthetic val$result:Lcom/samsung/multiscreen/Result;

.field final synthetic val$searchId:Ljava/lang/String;

.field final synthetic val$threadSocket:Ljava/net/MulticastSocket;


# direct methods
.method constructor <init>(Ljava/net/MulticastSocket;Ljava/lang/String;Lcom/samsung/multiscreen/Result;Ljava/net/InetAddress;Landroid/net/wifi/WifiManager$MulticastLock;Ljava/util/concurrent/ScheduledExecutorService;)V
    .locals 1

    .prologue
    .line 408
    iput-object p1, p0, Lcom/samsung/multiscreen/MSFDSearchProvider$3;->val$threadSocket:Ljava/net/MulticastSocket;

    iput-object p2, p0, Lcom/samsung/multiscreen/MSFDSearchProvider$3;->val$searchId:Ljava/lang/String;

    iput-object p3, p0, Lcom/samsung/multiscreen/MSFDSearchProvider$3;->val$result:Lcom/samsung/multiscreen/Result;

    iput-object p4, p0, Lcom/samsung/multiscreen/MSFDSearchProvider$3;->val$multicastInetAddress:Ljava/net/InetAddress;

    iput-object p5, p0, Lcom/samsung/multiscreen/MSFDSearchProvider$3;->val$multicastLock:Landroid/net/wifi/WifiManager$MulticastLock;

    iput-object p6, p0, Lcom/samsung/multiscreen/MSFDSearchProvider$3;->val$executor:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 410
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/multiscreen/MSFDSearchProvider$3;->searching:Z

    .line 411
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/multiscreen/MSFDSearchProvider$3;->processing:Z

    return-void
.end method

.method static synthetic access$602(Lcom/samsung/multiscreen/MSFDSearchProvider$3;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/multiscreen/MSFDSearchProvider$3;
    .param p1, "x1"    # Z

    .prologue
    .line 408
    iput-boolean p1, p0, Lcom/samsung/multiscreen/MSFDSearchProvider$3;->searching:Z

    return p1
.end method

.method static synthetic access$702(Lcom/samsung/multiscreen/MSFDSearchProvider$3;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/multiscreen/MSFDSearchProvider$3;
    .param p1, "x1"    # Z

    .prologue
    .line 408
    iput-boolean p1, p0, Lcom/samsung/multiscreen/MSFDSearchProvider$3;->processing:Z

    return p1
.end method


# virtual methods
.method public run()V
    .locals 15

    .prologue
    .line 416
    const/16 v11, 0x400

    :try_start_0
    new-array v0, v11, [B

    .line 417
    .local v0, "buf":[B
    new-instance v5, Ljava/net/DatagramPacket;

    array-length v11, v0

    invoke-direct {v5, v0, v11}, Ljava/net/DatagramPacket;-><init>([BI)V

    .line 419
    .local v5, "receivePacket":Ljava/net/DatagramPacket;
    :cond_0
    :goto_0
    iget-boolean v11, p0, Lcom/samsung/multiscreen/MSFDSearchProvider$3;->searching:Z

    if-eqz v11, :cond_1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Thread;->isInterrupted()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v11

    if-nez v11, :cond_1

    .line 422
    :try_start_1
    iget-object v11, p0, Lcom/samsung/multiscreen/MSFDSearchProvider$3;->val$threadSocket:Ljava/net/MulticastSocket;

    invoke-virtual {v11, v5}, Ljava/net/MulticastSocket;->receive(Ljava/net/DatagramPacket;)V

    .line 423
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Thread;->isInterrupted()Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v11

    if-eqz v11, :cond_3

    .line 502
    :cond_1
    :goto_1
    :try_start_2
    iget-object v11, p0, Lcom/samsung/multiscreen/MSFDSearchProvider$3;->val$threadSocket:Ljava/net/MulticastSocket;

    iget-object v12, p0, Lcom/samsung/multiscreen/MSFDSearchProvider$3;->val$multicastInetAddress:Ljava/net/InetAddress;

    invoke-virtual {v11, v12}, Ljava/net/MulticastSocket;->leaveGroup(Ljava/net/InetAddress;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 507
    :goto_2
    :try_start_3
    iget-object v11, p0, Lcom/samsung/multiscreen/MSFDSearchProvider$3;->val$multicastLock:Landroid/net/wifi/WifiManager$MulticastLock;

    invoke-static {v11}, Lcom/samsung/multiscreen/util/NetUtil;->releaseMulticastLock(Landroid/net/wifi/WifiManager$MulticastLock;)V

    .line 509
    iget-object v11, p0, Lcom/samsung/multiscreen/MSFDSearchProvider$3;->val$executor:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {v11}, Ljava/util/concurrent/ScheduledExecutorService;->shutdown()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 511
    iget-object v11, p0, Lcom/samsung/multiscreen/MSFDSearchProvider$3;->val$threadSocket:Ljava/net/MulticastSocket;

    invoke-virtual {v11}, Ljava/net/MulticastSocket;->isClosed()Z

    move-result v11

    if-nez v11, :cond_2

    .line 512
    iget-object v11, p0, Lcom/samsung/multiscreen/MSFDSearchProvider$3;->val$threadSocket:Ljava/net/MulticastSocket;

    invoke-virtual {v11}, Ljava/net/MulticastSocket;->close()V

    .line 515
    :cond_2
    return-void

    .line 425
    :cond_3
    :try_start_4
    iget-boolean v11, p0, Lcom/samsung/multiscreen/MSFDSearchProvider$3;->processing:Z

    if-nez v11, :cond_0

    .line 426
    invoke-virtual {v5}, Ljava/net/DatagramPacket;->getLength()I
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result v11

    if-lez v11, :cond_0

    .line 435
    :try_start_5
    new-instance v6, Ljava/lang/String;

    invoke-virtual {v5}, Ljava/net/DatagramPacket;->getData()[B

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v5}, Ljava/net/DatagramPacket;->getLength()I

    move-result v13

    const-string v14, "UTF-8"

    invoke-direct {v6, v11, v12, v13, v14}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    .line 436
    .local v6, "response":Ljava/lang/String;
    invoke-static {v6}, Lcom/samsung/multiscreen/util/JSONUtil;->parse(Ljava/lang/String;)Ljava/util/Map;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v7

    .line 444
    .local v7, "responseMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    if-eqz v7, :cond_0

    .line 445
    :try_start_6
    invoke-interface {v7}, Ljava/util/Map;->isEmpty()Z

    move-result v11

    if-nez v11, :cond_0

    const-string v11, "discover"

    const-string v12, "type"

    .line 446
    invoke-interface {v7, v12}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .local v8, "state":Ljava/lang/String;
    invoke-virtual {v11, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_0

    .line 450
    const-string v11, "sid"

    invoke-interface {v7, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 451
    .local v4, "id":Ljava/lang/String;
    if-eqz v4, :cond_0

    iget-object v11, p0, Lcom/samsung/multiscreen/MSFDSearchProvider$3;->val$searchId:Ljava/lang/String;

    .line 452
    invoke-virtual {v11, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 453
    const/4 v11, 0x1

    iput-boolean v11, p0, Lcom/samsung/multiscreen/MSFDSearchProvider$3;->processing:Z

    .line 455
    const-string v11, "alive"

    invoke-virtual {v11, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_4

    const-string v11, "up"

    .line 456
    invoke-virtual {v11, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 459
    :cond_4
    const-string v11, "data"

    invoke-interface {v7, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    .line 460
    .local v1, "dataMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    if-eqz v1, :cond_6

    .line 461
    const-string v11, "v2"

    invoke-interface {v1, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map;

    .line 462
    .local v3, "endpointMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    if-eqz v3, :cond_6

    .line 463
    const-string v11, "uri"

    invoke-interface {v3, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    .line 464
    .local v10, "url":Ljava/lang/String;
    if-eqz v10, :cond_6

    .line 465
    invoke-static {v10}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    .line 467
    .local v9, "uri":Landroid/net/Uri;
    const/16 v11, 0x7d0

    new-instance v12, Lcom/samsung/multiscreen/MSFDSearchProvider$3$1;

    invoke-direct {v12, p0}, Lcom/samsung/multiscreen/MSFDSearchProvider$3$1;-><init>(Lcom/samsung/multiscreen/MSFDSearchProvider$3;)V

    invoke-static {v9, v11, v12}, Lcom/samsung/multiscreen/Service;->getByURI(Landroid/net/Uri;ILcom/samsung/multiscreen/Result;)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_0

    .line 495
    .end local v1    # "dataMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v3    # "endpointMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v4    # "id":Ljava/lang/String;
    .end local v6    # "response":Ljava/lang/String;
    .end local v7    # "responseMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v8    # "state":Ljava/lang/String;
    .end local v9    # "uri":Landroid/net/Uri;
    .end local v10    # "url":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 496
    .local v2, "e":Ljava/io/IOException;
    :try_start_7
    const-string v11, "MSFDSearchProvider"

    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_1

    .line 511
    .end local v0    # "buf":[B
    .end local v2    # "e":Ljava/io/IOException;
    .end local v5    # "receivePacket":Ljava/net/DatagramPacket;
    :catchall_0
    move-exception v11

    iget-object v12, p0, Lcom/samsung/multiscreen/MSFDSearchProvider$3;->val$threadSocket:Ljava/net/MulticastSocket;

    invoke-virtual {v12}, Ljava/net/MulticastSocket;->isClosed()Z

    move-result v12

    if-nez v12, :cond_5

    .line 512
    iget-object v12, p0, Lcom/samsung/multiscreen/MSFDSearchProvider$3;->val$threadSocket:Ljava/net/MulticastSocket;

    invoke-virtual {v12}, Ljava/net/MulticastSocket;->close()V

    :cond_5
    throw v11

    .line 437
    .restart local v0    # "buf":[B
    .restart local v5    # "receivePacket":Ljava/net/DatagramPacket;
    :catch_1
    move-exception v2

    .line 438
    .local v2, "e":Ljava/lang/Exception;
    :try_start_8
    const-string v11, "MSFDSearchProvider"

    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 493
    .end local v2    # "e":Ljava/lang/Exception;
    .restart local v4    # "id":Ljava/lang/String;
    .restart local v6    # "response":Ljava/lang/String;
    .restart local v7    # "responseMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    .restart local v8    # "state":Ljava/lang/String;
    :cond_6
    const/4 v11, 0x0

    iput-boolean v11, p0, Lcom/samsung/multiscreen/MSFDSearchProvider$3;->processing:Z
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_0

    .line 503
    .end local v4    # "id":Ljava/lang/String;
    .end local v6    # "response":Ljava/lang/String;
    .end local v7    # "responseMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v8    # "state":Ljava/lang/String;
    :catch_2
    move-exception v2

    .line 504
    .local v2, "e":Ljava/io/IOException;
    :try_start_9
    const-string v11, "MSFDSearchProvider"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "ProviderThread exception: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto/16 :goto_2
.end method
