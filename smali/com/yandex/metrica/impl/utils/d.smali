.class public Lcom/yandex/metrica/impl/utils/d;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Ljava/util/Collection;Ljava/util/Collection;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;",
            "Ljava/util/Collection",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 22
    if-nez p0, :cond_0

    if-nez p1, :cond_0

    move v0, v1

    .line 46
    :goto_0
    return v0

    .line 24
    :cond_0
    if-eqz p0, :cond_1

    if-nez p1, :cond_2

    :cond_1
    move v0, v2

    .line 25
    goto :goto_0

    .line 26
    :cond_2
    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v3

    if-ne v0, v3, :cond_7

    .line 29
    instance-of v0, p0, Ljava/util/HashSet;

    if-eqz v0, :cond_4

    .line 30
    check-cast p0, Ljava/util/HashSet;

    .line 39
    :goto_1
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 40
    invoke-virtual {p0, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    move v0, v2

    .line 41
    goto :goto_0

    .line 32
    :cond_4
    instance-of v0, p1, Ljava/util/HashSet;

    if-eqz v0, :cond_5

    .line 33
    check-cast p1, Ljava/util/HashSet;

    move-object v4, p0

    move-object p0, p1

    move-object p1, v4

    .line 34
    goto :goto_1

    .line 36
    :cond_5
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0, p0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    move-object p0, v0

    .line 37
    goto :goto_1

    :cond_6
    move v0, v1

    .line 44
    goto :goto_0

    :cond_7
    move v0, v2

    .line 46
    goto :goto_0
.end method
