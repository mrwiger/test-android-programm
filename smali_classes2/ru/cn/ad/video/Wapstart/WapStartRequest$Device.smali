.class Lru/cn/ad/video/Wapstart/WapStartRequest$Device;
.super Ljava/lang/Object;
.source "WapStartRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/ad/video/Wapstart/WapStartRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Device"
.end annotation


# instance fields
.field adid:Ljava/lang/String;

.field ip:Ljava/lang/String;

.field final synthetic this$0:Lru/cn/ad/video/Wapstart/WapStartRequest;

.field ua:Ljava/lang/String;


# direct methods
.method constructor <init>(Lru/cn/ad/video/Wapstart/WapStartRequest;Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p1, "this$0"    # Lru/cn/ad/video/Wapstart/WapStartRequest;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "IPAddress"    # Ljava/lang/String;

    .prologue
    .line 49
    iput-object p1, p0, Lru/cn/ad/video/Wapstart/WapStartRequest$Device;->this$0:Lru/cn/ad/video/Wapstart/WapStartRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    const-string v2, ""

    iput-object v2, p0, Lru/cn/ad/video/Wapstart/WapStartRequest$Device;->adid:Ljava/lang/String;

    .line 50
    iput-object p3, p0, Lru/cn/ad/video/Wapstart/WapStartRequest$Device;->ip:Ljava/lang/String;

    .line 53
    :try_start_0
    invoke-static {p2}, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient;->getAdvertisingIdInfo(Landroid/content/Context;)Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;

    move-result-object v0

    .line 54
    .local v0, "adInfo":Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;->isLimitAdTrackingEnabled()Z

    move-result v2

    if-nez v2, :cond_0

    .line 55
    invoke-virtual {v0}, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;->getId()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lru/cn/ad/video/Wapstart/WapStartRequest$Device;->adid:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 61
    .end local v0    # "adInfo":Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;
    :cond_0
    :goto_0
    invoke-static {}, Lru/cn/utils/http/HttpClient;->defaultUserAgent()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lru/cn/ad/video/Wapstart/WapStartRequest$Device;->ua:Ljava/lang/String;

    .line 62
    return-void

    .line 57
    :catch_0
    move-exception v1

    .line 58
    .local v1, "e":Ljava/lang/Exception;
    invoke-static {v1}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
