.class public interface abstract Lru/cn/ad/AdsManager$AdapterCache;
.super Ljava/lang/Object;
.source "AdsManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/ad/AdsManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "AdapterCache"
.end annotation


# static fields
.field public static final NONE:Lru/cn/ad/AdsManager$AdapterCache;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    new-instance v0, Lru/cn/ad/AdsManager$AdapterCache$1;

    invoke-direct {v0}, Lru/cn/ad/AdsManager$AdapterCache$1;-><init>()V

    sput-object v0, Lru/cn/ad/AdsManager$AdapterCache;->NONE:Lru/cn/ad/AdsManager$AdapterCache;

    return-void
.end method


# virtual methods
.method public abstract consume(Lru/cn/ad/AdAdapter;)V
.end method

.method public abstract getAdapter(Ljava/lang/String;Ljava/util/List;)Lru/cn/ad/AdAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)",
            "Lru/cn/ad/AdAdapter;"
        }
    .end annotation
.end method

.method public abstract invalidate()V
.end method
