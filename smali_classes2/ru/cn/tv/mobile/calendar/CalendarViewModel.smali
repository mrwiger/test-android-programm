.class Lru/cn/tv/mobile/calendar/CalendarViewModel;
.super Lru/cn/mvvm/RxViewModel;
.source "CalendarViewModel.java"


# instance fields
.field private final dates:Landroid/arch/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/arch/lifecycle/MutableLiveData",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/util/Calendar;",
            ">;>;"
        }
    .end annotation
.end field

.field private final loader:Lru/cn/mvvm/RxLoader;


# direct methods
.method constructor <init>(Lru/cn/mvvm/RxLoader;)V
    .locals 1
    .param p1, "loader"    # Lru/cn/mvvm/RxLoader;

    .prologue
    .line 28
    invoke-direct {p0}, Lru/cn/mvvm/RxViewModel;-><init>()V

    .line 29
    iput-object p1, p0, Lru/cn/tv/mobile/calendar/CalendarViewModel;->loader:Lru/cn/mvvm/RxLoader;

    .line 30
    new-instance v0, Landroid/arch/lifecycle/MutableLiveData;

    invoke-direct {v0}, Landroid/arch/lifecycle/MutableLiveData;-><init>()V

    iput-object v0, p0, Lru/cn/tv/mobile/calendar/CalendarViewModel;->dates:Landroid/arch/lifecycle/MutableLiveData;

    .line 31
    return-void
.end method

.method static final synthetic lambda$loadDates$0$CalendarViewModel(Landroid/database/Cursor;)Ljava/util/List;
    .locals 11
    .param p0, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 55
    sget-object v10, Lru/cn/api/provider/TvContentProviderContract$ChannelDateColumn;->year:Lru/cn/api/provider/TvContentProviderContract$ChannelDateColumn;

    .line 56
    invoke-virtual {v10}, Lru/cn/api/provider/TvContentProviderContract$ChannelDateColumn;->toString()Ljava/lang/String;

    move-result-object v10

    .line 55
    invoke-interface {p0, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    .line 57
    .local v9, "yearIndex":I
    sget-object v10, Lru/cn/api/provider/TvContentProviderContract$ChannelDateColumn;->month:Lru/cn/api/provider/TvContentProviderContract$ChannelDateColumn;

    .line 58
    invoke-virtual {v10}, Lru/cn/api/provider/TvContentProviderContract$ChannelDateColumn;->toString()Ljava/lang/String;

    move-result-object v10

    .line 57
    invoke-interface {p0, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    .line 59
    .local v5, "monthIndex":I
    sget-object v10, Lru/cn/api/provider/TvContentProviderContract$ChannelDateColumn;->day:Lru/cn/api/provider/TvContentProviderContract$ChannelDateColumn;

    .line 60
    invoke-virtual {v10}, Lru/cn/api/provider/TvContentProviderContract$ChannelDateColumn;->toString()Ljava/lang/String;

    move-result-object v10

    .line 59
    invoke-interface {p0, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 61
    .local v3, "dayIndex":I
    sget-object v10, Lru/cn/api/provider/TvContentProviderContract$ChannelDateColumn;->timezone:Lru/cn/api/provider/TvContentProviderContract$ChannelDateColumn;

    .line 62
    invoke-virtual {v10}, Lru/cn/api/provider/TvContentProviderContract$ChannelDateColumn;->toString()Ljava/lang/String;

    move-result-object v10

    .line 61
    invoke-interface {p0, v10}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    .line 64
    .local v7, "timezoneIndex":I
    invoke-interface {p0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 66
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 67
    .local v1, "dates":Ljava/util/List;, "Ljava/util/List<Ljava/util/Calendar;>;"
    :goto_0
    invoke-interface {p0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v10

    if-nez v10, :cond_0

    .line 69
    invoke-interface {p0, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 70
    .local v8, "year":I
    invoke-interface {p0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 71
    .local v4, "month":I
    invoke-interface {p0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 72
    .local v2, "day":I
    invoke-interface {p0, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 74
    .local v6, "timezone":I
    invoke-static {v8, v4, v2, v6}, Lru/cn/utils/Utils;->getCalendar(IIII)Ljava/util/Calendar;

    move-result-object v0

    .line 75
    .local v0, "calendar":Ljava/util/Calendar;
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 77
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_0

    .line 80
    .end local v0    # "calendar":Ljava/util/Calendar;
    .end local v2    # "day":I
    .end local v4    # "month":I
    .end local v6    # "timezone":I
    .end local v8    # "year":I
    :cond_0
    return-object v1
.end method

.method private loadDates(J)V
    .locals 5
    .param p1, "channelId"    # J

    .prologue
    .line 49
    invoke-static {p1, p2}, Lru/cn/api/provider/TvContentProviderContract;->dates(J)Landroid/net/Uri;

    move-result-object v0

    .line 51
    .local v0, "datesUri":Landroid/net/Uri;
    iget-object v2, p0, Lru/cn/tv/mobile/calendar/CalendarViewModel;->loader:Lru/cn/mvvm/RxLoader;

    .line 52
    invoke-virtual {v2, v0}, Lru/cn/mvvm/RxLoader;->query(Landroid/net/Uri;)Lio/reactivex/Observable;

    move-result-object v2

    .line 53
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v2

    sget-object v3, Lru/cn/tv/mobile/calendar/CalendarViewModel$$Lambda$0;->$instance:Lio/reactivex/functions/Function;

    .line 54
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v2

    .line 82
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v2

    new-instance v3, Lru/cn/tv/mobile/calendar/CalendarViewModel$$Lambda$1;

    invoke-direct {v3, p0}, Lru/cn/tv/mobile/calendar/CalendarViewModel$$Lambda$1;-><init>(Lru/cn/tv/mobile/calendar/CalendarViewModel;)V

    .line 83
    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 85
    .local v1, "disposable":Lio/reactivex/disposables/Disposable;
    invoke-virtual {p0, v1}, Lru/cn/tv/mobile/calendar/CalendarViewModel;->bind(Lio/reactivex/disposables/Disposable;)V

    .line 86
    return-void
.end method


# virtual methods
.method public dates()Landroid/arch/lifecycle/LiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/arch/lifecycle/LiveData",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/util/Calendar;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 34
    iget-object v0, p0, Lru/cn/tv/mobile/calendar/CalendarViewModel;->dates:Landroid/arch/lifecycle/MutableLiveData;

    return-object v0
.end method

.method final synthetic lambda$loadDates$1$CalendarViewModel(Ljava/util/List;)V
    .locals 1
    .param p1, "it"    # Ljava/util/List;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 83
    iget-object v0, p0, Lru/cn/tv/mobile/calendar/CalendarViewModel;->dates:Landroid/arch/lifecycle/MutableLiveData;

    invoke-virtual {v0, p1}, Landroid/arch/lifecycle/MutableLiveData;->setValue(Ljava/lang/Object;)V

    return-void
.end method

.method public setChannelId(J)V
    .locals 3
    .param p1, "channelId"    # J

    .prologue
    .line 38
    invoke-virtual {p0}, Lru/cn/tv/mobile/calendar/CalendarViewModel;->unbindAll()V

    .line 40
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-eqz v0, :cond_0

    .line 41
    invoke-direct {p0, p1, p2}, Lru/cn/tv/mobile/calendar/CalendarViewModel;->loadDates(J)V

    .line 46
    :goto_0
    return-void

    .line 44
    :cond_0
    iget-object v0, p0, Lru/cn/tv/mobile/calendar/CalendarViewModel;->dates:Landroid/arch/lifecycle/MutableLiveData;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0, v1}, Landroid/arch/lifecycle/MutableLiveData;->setValue(Ljava/lang/Object;)V

    goto :goto_0
.end method
