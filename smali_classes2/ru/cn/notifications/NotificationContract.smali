.class public final Lru/cn/notifications/NotificationContract;
.super Ljava/lang/Object;
.source "NotificationContract.java"


# static fields
.field private static AUTHORITY:Ljava/lang/String;

.field public static channelIdKey:Ljava/lang/String;

.field public static createTimeKey:Ljava/lang/String;

.field public static deepLinkHostKey:Ljava/lang/String;

.field public static deepLinkSchemeKey:Ljava/lang/String;

.field public static filmIdKey:Ljava/lang/String;

.field public static messageKey:Ljava/lang/String;

.field private static query:Ljava/lang/String;

.field public static telecastIdKey:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    const-string v0, "ru.cn.notification"

    sput-object v0, Lru/cn/notifications/NotificationContract;->AUTHORITY:Ljava/lang/String;

    .line 12
    const-string v0, "notification"

    sput-object v0, Lru/cn/notifications/NotificationContract;->query:Ljava/lang/String;

    .line 14
    const-string v0, "message"

    sput-object v0, Lru/cn/notifications/NotificationContract;->messageKey:Ljava/lang/String;

    .line 15
    const-string v0, "filmId"

    sput-object v0, Lru/cn/notifications/NotificationContract;->filmIdKey:Ljava/lang/String;

    .line 16
    const-string v0, "channelId"

    sput-object v0, Lru/cn/notifications/NotificationContract;->channelIdKey:Ljava/lang/String;

    .line 17
    const-string v0, "telecastId"

    sput-object v0, Lru/cn/notifications/NotificationContract;->telecastIdKey:Ljava/lang/String;

    .line 18
    const-string v0, "createTime"

    sput-object v0, Lru/cn/notifications/NotificationContract;->createTimeKey:Ljava/lang/String;

    .line 19
    const-string v0, "app"

    sput-object v0, Lru/cn/notifications/NotificationContract;->deepLinkSchemeKey:Ljava/lang/String;

    .line 20
    const-string v0, "functionality"

    sput-object v0, Lru/cn/notifications/NotificationContract;->deepLinkHostKey:Ljava/lang/String;

    return-void
.end method

.method public static deepLinkFunctionality(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1
    .param p0, "scheme"    # Ljava/lang/String;
    .param p1, "authority"    # Ljava/lang/String;

    .prologue
    .line 28
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    .line 29
    invoke-virtual {v0, p0}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 30
    invoke-virtual {v0, p1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 31
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 28
    return-object v0
.end method

.method public static notifications()Landroid/net/Uri;
    .locals 2

    .prologue
    .line 23
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "content"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    sget-object v1, Lru/cn/notifications/NotificationContract;->AUTHORITY:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    sget-object v1, Lru/cn/notifications/NotificationContract;->query:Ljava/lang/String;

    .line 24
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 23
    return-object v0
.end method
