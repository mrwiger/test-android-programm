.class final Lru/cn/tv/mobile/collections/CollectionsViewModel;
.super Lru/cn/mvvm/RxViewModel;
.source "CollectionsViewModel.java"


# instance fields
.field private final rubrics:Landroid/arch/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/arch/lifecycle/MutableLiveData",
            "<",
            "Ljava/util/List",
            "<",
            "Lru/cn/api/catalogue/replies/Rubric;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lru/cn/domain/Collections;)V
    .locals 3
    .param p1, "collections"    # Lru/cn/domain/Collections;

    .prologue
    .line 20
    invoke-direct {p0}, Lru/cn/mvvm/RxViewModel;-><init>()V

    .line 21
    new-instance v0, Landroid/arch/lifecycle/MutableLiveData;

    invoke-direct {v0}, Landroid/arch/lifecycle/MutableLiveData;-><init>()V

    iput-object v0, p0, Lru/cn/tv/mobile/collections/CollectionsViewModel;->rubrics:Landroid/arch/lifecycle/MutableLiveData;

    .line 23
    const/4 v0, 0x0

    .line 24
    invoke-virtual {p1, v0}, Lru/cn/domain/Collections;->rubrics(Lru/cn/api/catalogue/replies/Rubric$UiHintType;)Lio/reactivex/Observable;

    move-result-object v0

    .line 25
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lru/cn/tv/mobile/collections/CollectionsViewModel;->rubrics:Landroid/arch/lifecycle/MutableLiveData;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v1}, Lru/cn/tv/mobile/collections/CollectionsViewModel$$Lambda$0;->get$Lambda(Landroid/arch/lifecycle/MutableLiveData;)Lio/reactivex/functions/Consumer;

    move-result-object v1

    new-instance v2, Lru/cn/tv/mobile/collections/CollectionsViewModel$$Lambda$1;

    invoke-direct {v2, p0}, Lru/cn/tv/mobile/collections/CollectionsViewModel$$Lambda$1;-><init>(Lru/cn/tv/mobile/collections/CollectionsViewModel;)V

    .line 26
    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 23
    invoke-virtual {p0, v0}, Lru/cn/tv/mobile/collections/CollectionsViewModel;->bind(Lio/reactivex/disposables/Disposable;)V

    .line 31
    return-void
.end method


# virtual methods
.method final synthetic lambda$new$0$CollectionsViewModel(Ljava/lang/Throwable;)V
    .locals 2
    .param p1, "throwable"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 29
    iget-object v0, p0, Lru/cn/tv/mobile/collections/CollectionsViewModel;->rubrics:Landroid/arch/lifecycle/MutableLiveData;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/arch/lifecycle/MutableLiveData;->setValue(Ljava/lang/Object;)V

    .line 30
    return-void
.end method

.method public rubrics()Landroid/arch/lifecycle/LiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/arch/lifecycle/LiveData",
            "<",
            "Ljava/util/List",
            "<",
            "Lru/cn/api/catalogue/replies/Rubric;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 34
    iget-object v0, p0, Lru/cn/tv/mobile/collections/CollectionsViewModel;->rubrics:Landroid/arch/lifecycle/MutableLiveData;

    return-object v0
.end method
