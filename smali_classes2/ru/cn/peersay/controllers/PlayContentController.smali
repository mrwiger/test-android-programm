.class public Lru/cn/peersay/controllers/PlayContentController;
.super Ljava/lang/Object;
.source "PlayContentController.java"

# interfaces
.implements Lru/cn/peersay/controllers/RemoteIntentController;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/peersay/controllers/PlayContentController$Listener;
    }
.end annotation


# instance fields
.field private currentChannelId:J

.field private currentTelecastId:J

.field private listener:Lru/cn/peersay/controllers/PlayContentController$Listener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/content/Context;Lru/cn/peersay/IntentMessage;)Z
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "message"    # Lru/cn/peersay/IntentMessage;

    .prologue
    .line 57
    iget-object v8, p2, Lru/cn/peersay/IntentMessage;->extras:Landroid/os/Bundle;

    if-nez v8, :cond_0

    .line 58
    const/4 v8, 0x0

    .line 126
    :goto_0
    return v8

    .line 60
    :cond_0
    iget-object v8, p2, Lru/cn/peersay/IntentMessage;->extras:Landroid/os/Bundle;

    const-string v9, "command"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 61
    .local v1, "command":Ljava/lang/String;
    if-nez v1, :cond_1

    .line 62
    const/4 v8, 0x0

    goto :goto_0

    .line 64
    :cond_1
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 65
    .local v5, "result":Landroid/os/Bundle;
    const/4 v8, -0x1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v9

    sparse-switch v9, :sswitch_data_0

    :cond_2
    :goto_1
    packed-switch v8, :pswitch_data_0

    .line 121
    const/4 v8, 0x0

    goto :goto_0

    .line 65
    :sswitch_0
    const-string v9, "tv.playback.play.content"

    invoke-virtual {v1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v8, 0x0

    goto :goto_1

    :sswitch_1
    const-string v9, "tv.now_playing"

    invoke-virtual {v1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v8, 0x1

    goto :goto_1

    :sswitch_2
    const-string v9, "tv.playback.stop"

    invoke-virtual {v1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v8, 0x2

    goto :goto_1

    :sswitch_3
    const-string v9, "tv.playback.next"

    invoke-virtual {v1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v8, 0x3

    goto :goto_1

    :sswitch_4
    const-string v9, "tv.playback.prev"

    invoke-virtual {v1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    const/4 v8, 0x4

    goto :goto_1

    .line 67
    :pswitch_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 68
    .local v0, "args":Landroid/os/Bundle;
    iget-object v8, p2, Lru/cn/peersay/IntentMessage;->extras:Landroid/os/Bundle;

    const-string v9, "channelId"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 69
    .local v2, "channelId":J
    const-wide/16 v8, 0x0

    cmp-long v8, v2, v8

    if-eqz v8, :cond_3

    .line 70
    const-string v8, "channelId"

    invoke-virtual {v0, v8, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 73
    :cond_3
    iget-object v8, p2, Lru/cn/peersay/IntentMessage;->extras:Landroid/os/Bundle;

    const-string v9, "telecastId"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v6

    .line 74
    .local v6, "telecastId":J
    const-wide/16 v8, 0x0

    cmp-long v8, v6, v8

    if-lez v8, :cond_4

    .line 75
    const-string v8, "telecastId"

    invoke-virtual {v0, v8, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 78
    :cond_4
    invoke-virtual {v0}, Landroid/os/Bundle;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_5

    .line 79
    const-string v8, "PlayContentController"

    const-string v9, "No suitable content to start"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 83
    :cond_5
    new-instance v4, Landroid/content/Intent;

    const-class v8, Lru/cn/tv/stb/StbActivity;

    invoke-direct {v4, p1, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 84
    .local v4, "intent":Landroid/content/Intent;
    const/high16 v8, 0x30000000

    invoke-virtual {v4, v8}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 85
    invoke-virtual {v4, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 86
    invoke-virtual {p1, v4}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 124
    .end local v0    # "args":Landroid/os/Bundle;
    .end local v2    # "channelId":J
    .end local v4    # "intent":Landroid/content/Intent;
    .end local v6    # "telecastId":J
    :cond_6
    :goto_2
    invoke-virtual {p2, v5}, Lru/cn/peersay/IntentMessage;->setResult(Landroid/os/Bundle;)V

    .line 126
    const/4 v8, 0x1

    goto/16 :goto_0

    .line 90
    :pswitch_1
    iget-wide v8, p0, Lru/cn/peersay/controllers/PlayContentController;->currentChannelId:J

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-eqz v8, :cond_7

    .line 91
    const-string v8, "channelId"

    iget-wide v10, p0, Lru/cn/peersay/controllers/PlayContentController;->currentChannelId:J

    invoke-virtual {v5, v8, v10, v11}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 94
    :cond_7
    iget-wide v8, p0, Lru/cn/peersay/controllers/PlayContentController;->currentTelecastId:J

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-lez v8, :cond_6

    .line 95
    const-string v8, "telecastId"

    iget-wide v10, p0, Lru/cn/peersay/controllers/PlayContentController;->currentTelecastId:J

    invoke-virtual {v5, v8, v10, v11}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    goto :goto_2

    .line 100
    :pswitch_2
    iget-object v8, p0, Lru/cn/peersay/controllers/PlayContentController;->listener:Lru/cn/peersay/controllers/PlayContentController$Listener;

    if-nez v8, :cond_8

    .line 101
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 103
    :cond_8
    iget-object v8, p0, Lru/cn/peersay/controllers/PlayContentController;->listener:Lru/cn/peersay/controllers/PlayContentController$Listener;

    invoke-interface {v8}, Lru/cn/peersay/controllers/PlayContentController$Listener;->onStop()V

    goto :goto_2

    .line 107
    :pswitch_3
    iget-object v8, p0, Lru/cn/peersay/controllers/PlayContentController;->listener:Lru/cn/peersay/controllers/PlayContentController$Listener;

    if-nez v8, :cond_9

    .line 108
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 110
    :cond_9
    iget-object v8, p0, Lru/cn/peersay/controllers/PlayContentController;->listener:Lru/cn/peersay/controllers/PlayContentController$Listener;

    invoke-interface {v8}, Lru/cn/peersay/controllers/PlayContentController$Listener;->onNext()V

    goto :goto_2

    .line 114
    :pswitch_4
    iget-object v8, p0, Lru/cn/peersay/controllers/PlayContentController;->listener:Lru/cn/peersay/controllers/PlayContentController$Listener;

    if-nez v8, :cond_a

    .line 115
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 117
    :cond_a
    iget-object v8, p0, Lru/cn/peersay/controllers/PlayContentController;->listener:Lru/cn/peersay/controllers/PlayContentController$Listener;

    invoke-interface {v8}, Lru/cn/peersay/controllers/PlayContentController$Listener;->onPrevious()V

    goto :goto_2

    .line 65
    nop

    :sswitch_data_0
    .sparse-switch
        -0x4cc061ba -> :sswitch_0
        -0x141f01a7 -> :sswitch_1
        -0x267bb86 -> :sswitch_3
        -0x266a446 -> :sswitch_4
        -0x2653e77 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public onContentChanged(Landroid/content/Context;JJ)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "channelId"    # J
    .param p4, "telecastId"    # J

    .prologue
    const-wide/16 v4, 0x0

    .line 38
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 39
    .local v1, "info":Landroid/os/Bundle;
    const-string v2, "event"

    const-string v3, "tv.now_playing.changed"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    iput-wide p2, p0, Lru/cn/peersay/controllers/PlayContentController;->currentChannelId:J

    .line 42
    iput-wide p4, p0, Lru/cn/peersay/controllers/PlayContentController;->currentTelecastId:J

    .line 43
    cmp-long v2, p2, v4

    if-eqz v2, :cond_0

    .line 44
    const-string v2, "channelId"

    invoke-virtual {v1, v2, p2, p3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 47
    :cond_0
    cmp-long v2, p4, v4

    if-lez v2, :cond_1

    .line 48
    const-string v2, "telecastId"

    invoke-virtual {v1, v2, p4, p5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 51
    :cond_1
    invoke-static {v1}, Lru/cn/peersay/IntentMessage;->createEventIntent(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    .line 52
    .local v0, "event":Landroid/content/Intent;
    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 53
    const-string v2, "PlayContentController"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "event sent "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    return-void
.end method

.method public setListener(Lru/cn/peersay/controllers/PlayContentController$Listener;)V
    .locals 0
    .param p1, "listener"    # Lru/cn/peersay/controllers/PlayContentController$Listener;

    .prologue
    .line 34
    iput-object p1, p0, Lru/cn/peersay/controllers/PlayContentController;->listener:Lru/cn/peersay/controllers/PlayContentController$Listener;

    .line 35
    return-void
.end method
