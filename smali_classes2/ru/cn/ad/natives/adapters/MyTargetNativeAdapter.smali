.class public Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;
.super Lru/cn/ad/natives/adapters/NativeAdAdapter;
.source "MyTargetNativeAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/ad/natives/adapters/MyTargetNativeAdapter$MyTargetBinder;
    }
.end annotation


# instance fields
.field private final slotId:I

.field private viewBinder:Lru/cn/ad/natives/adapters/MyTargetNativeAdapter$MyTargetBinder;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lru/cn/api/money_miner/replies/AdSystem;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "system"    # Lru/cn/api/money_miner/replies/AdSystem;

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Lru/cn/ad/natives/adapters/NativeAdAdapter;-><init>(Landroid/content/Context;Lru/cn/api/money_miner/replies/AdSystem;)V

    .line 32
    iget-object v1, p0, Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;->adSystem:Lru/cn/api/money_miner/replies/AdSystem;

    const-string v2, "slot_id"

    invoke-virtual {v1, v2}, Lru/cn/api/money_miner/replies/AdSystem;->getParamOrThrow(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 33
    .local v0, "s":Ljava/lang/String;
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;->slotId:I

    .line 34
    return-void
.end method

.method static synthetic access$002(Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;Lru/cn/ad/natives/adapters/MyTargetNativeAdapter$MyTargetBinder;)Lru/cn/ad/natives/adapters/MyTargetNativeAdapter$MyTargetBinder;
    .locals 0
    .param p0, "x0"    # Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;
    .param p1, "x1"    # Lru/cn/ad/natives/adapters/MyTargetNativeAdapter$MyTargetBinder;

    .prologue
    .line 22
    iput-object p1, p0, Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;->viewBinder:Lru/cn/ad/natives/adapters/MyTargetNativeAdapter$MyTargetBinder;

    return-object p1
.end method

.method static synthetic access$100(Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;)Lru/cn/ad/AdAdapter$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;

    .prologue
    .line 22
    iget-object v0, p0, Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    return-object v0
.end method

.method static synthetic access$200(Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;)Lru/cn/ad/AdAdapter$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;

    .prologue
    .line 22
    iget-object v0, p0, Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    return-object v0
.end method

.method static synthetic access$300(Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;)Lru/cn/ad/AdAdapter$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;

    .prologue
    .line 22
    iget-object v0, p0, Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    return-object v0
.end method

.method static synthetic access$400(Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;)Lru/cn/ad/AdAdapter$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;

    .prologue
    .line 22
    iget-object v0, p0, Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    return-object v0
.end method

.method static synthetic access$500(Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;)Lru/cn/ad/AdAdapter$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;

    .prologue
    .line 22
    iget-object v0, p0, Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    return-object v0
.end method

.method static synthetic access$600(Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;)Lru/cn/ad/AdAdapter$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;

    .prologue
    .line 22
    iget-object v0, p0, Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    return-object v0
.end method

.method static synthetic access$700(Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;)Lru/cn/ad/AdAdapter$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;

    .prologue
    .line 22
    iget-object v0, p0, Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    return-object v0
.end method


# virtual methods
.method public destroy()V
    .locals 1

    .prologue
    .line 110
    invoke-virtual {p0}, Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;->getPresenter()Lru/cn/ad/natives/adapters/NativeAdAdapter$Presenter;

    move-result-object v0

    .line 111
    .local v0, "presenter":Lru/cn/ad/natives/adapters/NativeAdAdapter$Presenter;
    if-eqz v0, :cond_0

    .line 112
    invoke-interface {v0}, Lru/cn/ad/natives/adapters/NativeAdAdapter$Presenter;->destroy()V

    .line 114
    :cond_0
    return-void
.end method

.method protected onLoad()V
    .locals 3

    .prologue
    .line 38
    new-instance v0, Lcom/my/target/nativeads/NativeAd;

    iget v1, p0, Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;->slotId:I

    iget-object v2, p0, Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;->context:Landroid/content/Context;

    invoke-direct {v0, v1, v2}, Lcom/my/target/nativeads/NativeAd;-><init>(ILandroid/content/Context;)V

    .line 39
    .local v0, "nativeAd":Lcom/my/target/nativeads/NativeAd;
    new-instance v1, Lru/cn/ad/natives/adapters/MyTargetNativeAdapter$1;

    invoke-direct {v1, p0}, Lru/cn/ad/natives/adapters/MyTargetNativeAdapter$1;-><init>(Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;)V

    invoke-virtual {v0, v1}, Lcom/my/target/nativeads/NativeAd;->setListener(Lcom/my/target/nativeads/NativeAd$NativeAdListener;)V

    .line 86
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/my/target/nativeads/NativeAd;->setAutoLoadImages(Z)V

    .line 88
    invoke-virtual {v0}, Lcom/my/target/nativeads/NativeAd;->load()V

    .line 89
    return-void
.end method

.method public show()V
    .locals 3

    .prologue
    .line 93
    invoke-virtual {p0}, Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;->getPresenter()Lru/cn/ad/natives/adapters/NativeAdAdapter$Presenter;

    move-result-object v0

    .line 94
    .local v0, "presenter":Lru/cn/ad/natives/adapters/NativeAdAdapter$Presenter;
    iget-object v1, p0, Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;->viewBinder:Lru/cn/ad/natives/adapters/MyTargetNativeAdapter$MyTargetBinder;

    new-instance v2, Lru/cn/ad/natives/adapters/MyTargetNativeAdapter$2;

    invoke-direct {v2, p0}, Lru/cn/ad/natives/adapters/MyTargetNativeAdapter$2;-><init>(Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;)V

    invoke-interface {v0, v1, v2}, Lru/cn/ad/natives/adapters/NativeAdAdapter$Presenter;->show(Lru/cn/ad/natives/adapters/NativeAdAdapter$ViewBinder;Ljava/lang/Runnable;)V

    .line 101
    iget-object v1, p0, Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    if-eqz v1, :cond_0

    .line 102
    iget-object v1, p0, Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;->listener:Lru/cn/ad/AdAdapter$Listener;

    invoke-interface {v1}, Lru/cn/ad/AdAdapter$Listener;->onAdStarted()V

    .line 105
    :cond_0
    sget-object v1, Lru/cn/domain/statistics/inetra/AdvEvent;->ADV_EVENT_START:Lru/cn/domain/statistics/inetra/AdvEvent;

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lru/cn/ad/natives/adapters/MyTargetNativeAdapter;->reportEvent(Lru/cn/domain/statistics/inetra/AdvEvent;Ljava/util/Map;)V

    .line 106
    return-void
.end method
