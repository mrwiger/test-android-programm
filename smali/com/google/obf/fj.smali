.class public final Lcom/google/obf/fj;
.super Ljava/util/AbstractMap;
.source "IMASDK"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/obf/fj$b;,
        Lcom/google/obf/fj$a;,
        Lcom/google/obf/fj$c;,
        Lcom/google/obf/fj$d;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/AbstractMap",
        "<TK;TV;>;",
        "Ljava/io/Serializable;"
    }
.end annotation


# static fields
.field static final synthetic f:Z

.field private static final g:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Ljava/lang/Comparable;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field a:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<-TK;>;"
        }
    .end annotation
.end field

.field b:Lcom/google/obf/fj$d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/obf/fj$d",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field c:I

.field d:I

.field final e:Lcom/google/obf/fj$d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/obf/fj$d",
            "<TK;TV;>;"
        }
    .end annotation
.end field

.field private h:Lcom/google/obf/fj$a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/obf/fj$a;"
        }
    .end annotation
.end field

.field private i:Lcom/google/obf/fj$b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/obf/fj$b;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 189
    const-class v0, Lcom/google/obf/fj;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/obf/fj;->f:Z

    .line 190
    new-instance v0, Lcom/google/obf/fj$1;

    invoke-direct {v0}, Lcom/google/obf/fj$1;-><init>()V

    sput-object v0, Lcom/google/obf/fj;->g:Ljava/util/Comparator;

    return-void

    .line 189
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lcom/google/obf/fj;->g:Ljava/util/Comparator;

    invoke-direct {p0, v0}, Lcom/google/obf/fj;-><init>(Ljava/util/Comparator;)V

    .line 2
    return-void
.end method

.method public constructor <init>(Ljava/util/Comparator;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Comparator",
            "<-TK;>;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 3
    invoke-direct {p0}, Ljava/util/AbstractMap;-><init>()V

    .line 4
    iput v0, p0, Lcom/google/obf/fj;->c:I

    .line 5
    iput v0, p0, Lcom/google/obf/fj;->d:I

    .line 6
    new-instance v0, Lcom/google/obf/fj$d;

    invoke-direct {v0}, Lcom/google/obf/fj$d;-><init>()V

    iput-object v0, p0, Lcom/google/obf/fj;->e:Lcom/google/obf/fj$d;

    .line 7
    if-eqz p1, :cond_0

    :goto_0
    iput-object p1, p0, Lcom/google/obf/fj;->a:Ljava/util/Comparator;

    .line 8
    return-void

    .line 7
    :cond_0
    sget-object p1, Lcom/google/obf/fj;->g:Ljava/util/Comparator;

    goto :goto_0
.end method

.method private a(Lcom/google/obf/fj$d;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/obf/fj$d",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 159
    iget-object v0, p1, Lcom/google/obf/fj$d;->b:Lcom/google/obf/fj$d;

    .line 160
    iget-object v3, p1, Lcom/google/obf/fj$d;->c:Lcom/google/obf/fj$d;

    .line 161
    iget-object v4, v3, Lcom/google/obf/fj$d;->b:Lcom/google/obf/fj$d;

    .line 162
    iget-object v5, v3, Lcom/google/obf/fj$d;->c:Lcom/google/obf/fj$d;

    .line 163
    iput-object v4, p1, Lcom/google/obf/fj$d;->c:Lcom/google/obf/fj$d;

    .line 164
    if-eqz v4, :cond_0

    .line 165
    iput-object p1, v4, Lcom/google/obf/fj$d;->a:Lcom/google/obf/fj$d;

    .line 166
    :cond_0
    invoke-direct {p0, p1, v3}, Lcom/google/obf/fj;->a(Lcom/google/obf/fj$d;Lcom/google/obf/fj$d;)V

    .line 167
    iput-object p1, v3, Lcom/google/obf/fj$d;->b:Lcom/google/obf/fj$d;

    .line 168
    iput-object v3, p1, Lcom/google/obf/fj$d;->a:Lcom/google/obf/fj$d;

    .line 169
    if-eqz v0, :cond_2

    iget v0, v0, Lcom/google/obf/fj$d;->h:I

    move v2, v0

    :goto_0
    if-eqz v4, :cond_3

    iget v0, v4, Lcom/google/obf/fj$d;->h:I

    :goto_1
    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iput v0, p1, Lcom/google/obf/fj$d;->h:I

    .line 170
    iget v0, p1, Lcom/google/obf/fj$d;->h:I

    if-eqz v5, :cond_1

    iget v1, v5, Lcom/google/obf/fj$d;->h:I

    :cond_1
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iput v0, v3, Lcom/google/obf/fj$d;->h:I

    .line 171
    return-void

    :cond_2
    move v2, v1

    .line 169
    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method private a(Lcom/google/obf/fj$d;Lcom/google/obf/fj$d;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/obf/fj$d",
            "<TK;TV;>;",
            "Lcom/google/obf/fj$d",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 106
    iget-object v0, p1, Lcom/google/obf/fj$d;->a:Lcom/google/obf/fj$d;

    .line 107
    const/4 v1, 0x0

    iput-object v1, p1, Lcom/google/obf/fj$d;->a:Lcom/google/obf/fj$d;

    .line 108
    if-eqz p2, :cond_0

    .line 109
    iput-object v0, p2, Lcom/google/obf/fj$d;->a:Lcom/google/obf/fj$d;

    .line 110
    :cond_0
    if-eqz v0, :cond_3

    .line 111
    iget-object v1, v0, Lcom/google/obf/fj$d;->b:Lcom/google/obf/fj$d;

    if-ne v1, p1, :cond_1

    .line 112
    iput-object p2, v0, Lcom/google/obf/fj$d;->b:Lcom/google/obf/fj$d;

    .line 116
    :goto_0
    return-void

    .line 113
    :cond_1
    sget-boolean v1, Lcom/google/obf/fj;->f:Z

    if-nez v1, :cond_2

    iget-object v1, v0, Lcom/google/obf/fj$d;->c:Lcom/google/obf/fj$d;

    if-eq v1, p1, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 114
    :cond_2
    iput-object p2, v0, Lcom/google/obf/fj$d;->c:Lcom/google/obf/fj$d;

    goto :goto_0

    .line 115
    :cond_3
    iput-object p2, p0, Lcom/google/obf/fj;->b:Lcom/google/obf/fj$d;

    goto :goto_0
.end method

.method private a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 64
    if-eq p1, p2, :cond_0

    if-eqz p1, :cond_1

    invoke-virtual {p1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Lcom/google/obf/fj$d;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/obf/fj$d",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 172
    iget-object v3, p1, Lcom/google/obf/fj$d;->b:Lcom/google/obf/fj$d;

    .line 173
    iget-object v0, p1, Lcom/google/obf/fj$d;->c:Lcom/google/obf/fj$d;

    .line 174
    iget-object v4, v3, Lcom/google/obf/fj$d;->b:Lcom/google/obf/fj$d;

    .line 175
    iget-object v5, v3, Lcom/google/obf/fj$d;->c:Lcom/google/obf/fj$d;

    .line 176
    iput-object v5, p1, Lcom/google/obf/fj$d;->b:Lcom/google/obf/fj$d;

    .line 177
    if-eqz v5, :cond_0

    .line 178
    iput-object p1, v5, Lcom/google/obf/fj$d;->a:Lcom/google/obf/fj$d;

    .line 179
    :cond_0
    invoke-direct {p0, p1, v3}, Lcom/google/obf/fj;->a(Lcom/google/obf/fj$d;Lcom/google/obf/fj$d;)V

    .line 180
    iput-object p1, v3, Lcom/google/obf/fj$d;->c:Lcom/google/obf/fj$d;

    .line 181
    iput-object v3, p1, Lcom/google/obf/fj$d;->a:Lcom/google/obf/fj$d;

    .line 182
    if-eqz v0, :cond_2

    iget v0, v0, Lcom/google/obf/fj$d;->h:I

    move v2, v0

    :goto_0
    if-eqz v5, :cond_3

    iget v0, v5, Lcom/google/obf/fj$d;->h:I

    :goto_1
    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iput v0, p1, Lcom/google/obf/fj$d;->h:I

    .line 183
    iget v0, p1, Lcom/google/obf/fj$d;->h:I

    if-eqz v4, :cond_1

    iget v1, v4, Lcom/google/obf/fj$d;->h:I

    :cond_1
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iput v0, v3, Lcom/google/obf/fj$d;->h:I

    .line 184
    return-void

    :cond_2
    move v2, v1

    .line 182
    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method private b(Lcom/google/obf/fj$d;Z)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/obf/fj$d",
            "<TK;TV;>;Z)V"
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    const/4 v7, -0x1

    const/4 v1, 0x0

    .line 117
    :goto_0
    if-eqz p1, :cond_1

    .line 118
    iget-object v3, p1, Lcom/google/obf/fj$d;->b:Lcom/google/obf/fj$d;

    .line 119
    iget-object v4, p1, Lcom/google/obf/fj$d;->c:Lcom/google/obf/fj$d;

    .line 120
    if-eqz v3, :cond_2

    iget v0, v3, Lcom/google/obf/fj$d;->h:I

    move v2, v0

    .line 121
    :goto_1
    if-eqz v4, :cond_3

    iget v0, v4, Lcom/google/obf/fj$d;->h:I

    .line 122
    :goto_2
    sub-int v5, v2, v0

    .line 123
    const/4 v6, -0x2

    if-ne v5, v6, :cond_8

    .line 124
    iget-object v3, v4, Lcom/google/obf/fj$d;->b:Lcom/google/obf/fj$d;

    .line 125
    iget-object v0, v4, Lcom/google/obf/fj$d;->c:Lcom/google/obf/fj$d;

    .line 126
    if-eqz v0, :cond_4

    iget v0, v0, Lcom/google/obf/fj$d;->h:I

    move v2, v0

    .line 127
    :goto_3
    if-eqz v3, :cond_5

    iget v0, v3, Lcom/google/obf/fj$d;->h:I

    .line 128
    :goto_4
    sub-int/2addr v0, v2

    .line 129
    if-eq v0, v7, :cond_0

    if-nez v0, :cond_6

    if-nez p2, :cond_6

    .line 130
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/obf/fj;->a(Lcom/google/obf/fj$d;)V

    .line 134
    :goto_5
    if-eqz p2, :cond_a

    .line 158
    :cond_1
    :goto_6
    return-void

    :cond_2
    move v2, v1

    .line 120
    goto :goto_1

    :cond_3
    move v0, v1

    .line 121
    goto :goto_2

    :cond_4
    move v2, v1

    .line 126
    goto :goto_3

    :cond_5
    move v0, v1

    .line 127
    goto :goto_4

    .line 131
    :cond_6
    sget-boolean v2, Lcom/google/obf/fj;->f:Z

    if-nez v2, :cond_7

    if-eq v0, v8, :cond_7

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 132
    :cond_7
    invoke-direct {p0, v4}, Lcom/google/obf/fj;->b(Lcom/google/obf/fj$d;)V

    .line 133
    invoke-direct {p0, p1}, Lcom/google/obf/fj;->a(Lcom/google/obf/fj$d;)V

    goto :goto_5

    .line 136
    :cond_8
    const/4 v4, 0x2

    if-ne v5, v4, :cond_f

    .line 137
    iget-object v4, v3, Lcom/google/obf/fj$d;->b:Lcom/google/obf/fj$d;

    .line 138
    iget-object v0, v3, Lcom/google/obf/fj$d;->c:Lcom/google/obf/fj$d;

    .line 139
    if-eqz v0, :cond_b

    iget v0, v0, Lcom/google/obf/fj$d;->h:I

    move v2, v0

    .line 140
    :goto_7
    if-eqz v4, :cond_c

    iget v0, v4, Lcom/google/obf/fj$d;->h:I

    .line 141
    :goto_8
    sub-int/2addr v0, v2

    .line 142
    if-eq v0, v8, :cond_9

    if-nez v0, :cond_d

    if-nez p2, :cond_d

    .line 143
    :cond_9
    invoke-direct {p0, p1}, Lcom/google/obf/fj;->b(Lcom/google/obf/fj$d;)V

    .line 147
    :goto_9
    if-nez p2, :cond_1

    .line 157
    :cond_a
    iget-object p1, p1, Lcom/google/obf/fj$d;->a:Lcom/google/obf/fj$d;

    goto :goto_0

    :cond_b
    move v2, v1

    .line 139
    goto :goto_7

    :cond_c
    move v0, v1

    .line 140
    goto :goto_8

    .line 144
    :cond_d
    sget-boolean v2, Lcom/google/obf/fj;->f:Z

    if-nez v2, :cond_e

    if-eq v0, v7, :cond_e

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 145
    :cond_e
    invoke-direct {p0, v3}, Lcom/google/obf/fj;->a(Lcom/google/obf/fj$d;)V

    .line 146
    invoke-direct {p0, p1}, Lcom/google/obf/fj;->b(Lcom/google/obf/fj$d;)V

    goto :goto_9

    .line 149
    :cond_f
    if-nez v5, :cond_10

    .line 150
    add-int/lit8 v0, v2, 0x1

    iput v0, p1, Lcom/google/obf/fj$d;->h:I

    .line 151
    if-eqz p2, :cond_a

    goto :goto_6

    .line 153
    :cond_10
    sget-boolean v3, Lcom/google/obf/fj;->f:Z

    if-nez v3, :cond_11

    if-eq v5, v7, :cond_11

    if-eq v5, v8, :cond_11

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 154
    :cond_11
    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iput v0, p1, Lcom/google/obf/fj$d;->h:I

    .line 155
    if-nez p2, :cond_a

    goto :goto_6
.end method


# virtual methods
.method a(Ljava/lang/Object;)Lcom/google/obf/fj$d;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Lcom/google/obf/fj$d",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 58
    if-eqz p1, :cond_0

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p0, p1, v1}, Lcom/google/obf/fj;->a(Ljava/lang/Object;Z)Lcom/google/obf/fj$d;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 60
    :cond_0
    :goto_0
    return-object v0

    .line 59
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method a(Ljava/lang/Object;Z)Lcom/google/obf/fj$d;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;Z)",
            "Lcom/google/obf/fj$d",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 27
    iget-object v5, p0, Lcom/google/obf/fj;->a:Ljava/util/Comparator;

    .line 28
    iget-object v2, p0, Lcom/google/obf/fj;->b:Lcom/google/obf/fj$d;

    .line 29
    const/4 v0, 0x0

    .line 30
    if-eqz v2, :cond_9

    .line 31
    sget-object v0, Lcom/google/obf/fj;->g:Ljava/util/Comparator;

    if-ne v5, v0, :cond_1

    move-object v0, p1

    check-cast v0, Ljava/lang/Comparable;

    .line 32
    :goto_0
    if-eqz v0, :cond_2

    iget-object v3, v2, Lcom/google/obf/fj$d;->f:Ljava/lang/Object;

    .line 33
    invoke-interface {v0, v3}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v3

    .line 35
    :goto_1
    if-nez v3, :cond_3

    move-object v1, v2

    .line 57
    :cond_0
    :goto_2
    return-object v1

    :cond_1
    move-object v0, v1

    .line 31
    goto :goto_0

    .line 33
    :cond_2
    iget-object v3, v2, Lcom/google/obf/fj$d;->f:Ljava/lang/Object;

    .line 34
    invoke-interface {v5, p1, v3}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v3

    goto :goto_1

    .line 37
    :cond_3
    if-gez v3, :cond_4

    iget-object v4, v2, Lcom/google/obf/fj$d;->b:Lcom/google/obf/fj$d;

    .line 38
    :goto_3
    if-nez v4, :cond_5

    move v6, v3

    move-object v3, v2

    move v2, v6

    .line 42
    :goto_4
    if-eqz p2, :cond_0

    .line 44
    iget-object v1, p0, Lcom/google/obf/fj;->e:Lcom/google/obf/fj$d;

    .line 45
    if-nez v3, :cond_7

    .line 46
    sget-object v0, Lcom/google/obf/fj;->g:Ljava/util/Comparator;

    if-ne v5, v0, :cond_6

    instance-of v0, p1, Ljava/lang/Comparable;

    if-nez v0, :cond_6

    .line 47
    new-instance v0, Ljava/lang/ClassCastException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not Comparable"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 37
    :cond_4
    iget-object v4, v2, Lcom/google/obf/fj$d;->c:Lcom/google/obf/fj$d;

    goto :goto_3

    :cond_5
    move-object v2, v4

    .line 41
    goto :goto_0

    .line 48
    :cond_6
    new-instance v0, Lcom/google/obf/fj$d;

    iget-object v2, v1, Lcom/google/obf/fj$d;->e:Lcom/google/obf/fj$d;

    invoke-direct {v0, v3, p1, v1, v2}, Lcom/google/obf/fj$d;-><init>(Lcom/google/obf/fj$d;Ljava/lang/Object;Lcom/google/obf/fj$d;Lcom/google/obf/fj$d;)V

    .line 49
    iput-object v0, p0, Lcom/google/obf/fj;->b:Lcom/google/obf/fj$d;

    .line 55
    :goto_5
    iget v1, p0, Lcom/google/obf/fj;->c:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/obf/fj;->c:I

    .line 56
    iget v1, p0, Lcom/google/obf/fj;->d:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/obf/fj;->d:I

    move-object v1, v0

    .line 57
    goto :goto_2

    .line 50
    :cond_7
    new-instance v0, Lcom/google/obf/fj$d;

    iget-object v4, v1, Lcom/google/obf/fj$d;->e:Lcom/google/obf/fj$d;

    invoke-direct {v0, v3, p1, v1, v4}, Lcom/google/obf/fj$d;-><init>(Lcom/google/obf/fj$d;Ljava/lang/Object;Lcom/google/obf/fj$d;Lcom/google/obf/fj$d;)V

    .line 51
    if-gez v2, :cond_8

    .line 52
    iput-object v0, v3, Lcom/google/obf/fj$d;->b:Lcom/google/obf/fj$d;

    .line 54
    :goto_6
    const/4 v1, 0x1

    invoke-direct {p0, v3, v1}, Lcom/google/obf/fj;->b(Lcom/google/obf/fj$d;Z)V

    goto :goto_5

    .line 53
    :cond_8
    iput-object v0, v3, Lcom/google/obf/fj$d;->c:Lcom/google/obf/fj$d;

    goto :goto_6

    :cond_9
    move-object v3, v2

    move v2, v0

    goto :goto_4
.end method

.method a(Ljava/util/Map$Entry;)Lcom/google/obf/fj$d;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map$Entry",
            "<**>;)",
            "Lcom/google/obf/fj$d",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 61
    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/obf/fj;->a(Ljava/lang/Object;)Lcom/google/obf/fj$d;

    move-result-object v0

    .line 62
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/google/obf/fj$d;->g:Ljava/lang/Object;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/google/obf/fj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    .line 63
    :goto_0
    if-eqz v1, :cond_1

    :goto_1
    return-object v0

    .line 62
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 63
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method a(Lcom/google/obf/fj$d;Z)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/obf/fj$d",
            "<TK;TV;>;Z)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x0

    .line 65
    if-eqz p2, :cond_0

    .line 66
    iget-object v0, p1, Lcom/google/obf/fj$d;->e:Lcom/google/obf/fj$d;

    iget-object v1, p1, Lcom/google/obf/fj$d;->d:Lcom/google/obf/fj$d;

    iput-object v1, v0, Lcom/google/obf/fj$d;->d:Lcom/google/obf/fj$d;

    .line 67
    iget-object v0, p1, Lcom/google/obf/fj$d;->d:Lcom/google/obf/fj$d;

    iget-object v1, p1, Lcom/google/obf/fj$d;->e:Lcom/google/obf/fj$d;

    iput-object v1, v0, Lcom/google/obf/fj$d;->e:Lcom/google/obf/fj$d;

    .line 68
    :cond_0
    iget-object v0, p1, Lcom/google/obf/fj$d;->b:Lcom/google/obf/fj$d;

    .line 69
    iget-object v1, p1, Lcom/google/obf/fj$d;->c:Lcom/google/obf/fj$d;

    .line 70
    iget-object v3, p1, Lcom/google/obf/fj$d;->a:Lcom/google/obf/fj$d;

    .line 71
    if-eqz v0, :cond_3

    if-eqz v1, :cond_3

    .line 72
    iget v3, v0, Lcom/google/obf/fj$d;->h:I

    iget v4, v1, Lcom/google/obf/fj$d;->h:I

    if-le v3, v4, :cond_2

    invoke-virtual {v0}, Lcom/google/obf/fj$d;->b()Lcom/google/obf/fj$d;

    move-result-object v0

    .line 73
    :goto_0
    invoke-virtual {p0, v0, v2}, Lcom/google/obf/fj;->a(Lcom/google/obf/fj$d;Z)V

    .line 75
    iget-object v3, p1, Lcom/google/obf/fj$d;->b:Lcom/google/obf/fj$d;

    .line 76
    if-eqz v3, :cond_6

    .line 77
    iget v1, v3, Lcom/google/obf/fj$d;->h:I

    .line 78
    iput-object v3, v0, Lcom/google/obf/fj$d;->b:Lcom/google/obf/fj$d;

    .line 79
    iput-object v0, v3, Lcom/google/obf/fj$d;->a:Lcom/google/obf/fj$d;

    .line 80
    iput-object v5, p1, Lcom/google/obf/fj$d;->b:Lcom/google/obf/fj$d;

    .line 82
    :goto_1
    iget-object v3, p1, Lcom/google/obf/fj$d;->c:Lcom/google/obf/fj$d;

    .line 83
    if-eqz v3, :cond_1

    .line 84
    iget v2, v3, Lcom/google/obf/fj$d;->h:I

    .line 85
    iput-object v3, v0, Lcom/google/obf/fj$d;->c:Lcom/google/obf/fj$d;

    .line 86
    iput-object v0, v3, Lcom/google/obf/fj$d;->a:Lcom/google/obf/fj$d;

    .line 87
    iput-object v5, p1, Lcom/google/obf/fj$d;->c:Lcom/google/obf/fj$d;

    .line 88
    :cond_1
    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/obf/fj$d;->h:I

    .line 89
    invoke-direct {p0, p1, v0}, Lcom/google/obf/fj;->a(Lcom/google/obf/fj$d;Lcom/google/obf/fj$d;)V

    .line 101
    :goto_2
    return-void

    .line 72
    :cond_2
    invoke-virtual {v1}, Lcom/google/obf/fj$d;->a()Lcom/google/obf/fj$d;

    move-result-object v0

    goto :goto_0

    .line 91
    :cond_3
    if-eqz v0, :cond_4

    .line 92
    invoke-direct {p0, p1, v0}, Lcom/google/obf/fj;->a(Lcom/google/obf/fj$d;Lcom/google/obf/fj$d;)V

    .line 93
    iput-object v5, p1, Lcom/google/obf/fj$d;->b:Lcom/google/obf/fj$d;

    .line 98
    :goto_3
    invoke-direct {p0, v3, v2}, Lcom/google/obf/fj;->b(Lcom/google/obf/fj$d;Z)V

    .line 99
    iget v0, p0, Lcom/google/obf/fj;->c:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/obf/fj;->c:I

    .line 100
    iget v0, p0, Lcom/google/obf/fj;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/obf/fj;->d:I

    goto :goto_2

    .line 94
    :cond_4
    if-eqz v1, :cond_5

    .line 95
    invoke-direct {p0, p1, v1}, Lcom/google/obf/fj;->a(Lcom/google/obf/fj$d;Lcom/google/obf/fj$d;)V

    .line 96
    iput-object v5, p1, Lcom/google/obf/fj$d;->c:Lcom/google/obf/fj$d;

    goto :goto_3

    .line 97
    :cond_5
    invoke-direct {p0, p1, v5}, Lcom/google/obf/fj;->a(Lcom/google/obf/fj$d;Lcom/google/obf/fj$d;)V

    goto :goto_3

    :cond_6
    move v1, v2

    goto :goto_1
.end method

.method b(Ljava/lang/Object;)Lcom/google/obf/fj$d;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Lcom/google/obf/fj$d",
            "<TK;TV;>;"
        }
    .end annotation

    .prologue
    .line 102
    invoke-virtual {p0, p1}, Lcom/google/obf/fj;->a(Ljava/lang/Object;)Lcom/google/obf/fj$d;

    move-result-object v0

    .line 103
    if-eqz v0, :cond_0

    .line 104
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/obf/fj;->a(Lcom/google/obf/fj$d;Z)V

    .line 105
    :cond_0
    return-object v0
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 19
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/obf/fj;->b:Lcom/google/obf/fj$d;

    .line 20
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/obf/fj;->c:I

    .line 21
    iget v0, p0, Lcom/google/obf/fj;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/obf/fj;->d:I

    .line 22
    iget-object v0, p0, Lcom/google/obf/fj;->e:Lcom/google/obf/fj$d;

    .line 23
    iput-object v0, v0, Lcom/google/obf/fj$d;->e:Lcom/google/obf/fj$d;

    iput-object v0, v0, Lcom/google/obf/fj$d;->d:Lcom/google/obf/fj$d;

    .line 24
    return-void
.end method

.method public containsKey(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p0, p1}, Lcom/google/obf/fj;->a(Ljava/lang/Object;)Lcom/google/obf/fj$d;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public entrySet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 185
    iget-object v0, p0, Lcom/google/obf/fj;->h:Lcom/google/obf/fj$a;

    .line 186
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/obf/fj$a;

    invoke-direct {v0, p0}, Lcom/google/obf/fj$a;-><init>(Lcom/google/obf/fj;)V

    iput-object v0, p0, Lcom/google/obf/fj;->h:Lcom/google/obf/fj$a;

    goto :goto_0
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 10
    invoke-virtual {p0, p1}, Lcom/google/obf/fj;->a(Ljava/lang/Object;)Lcom/google/obf/fj$d;

    move-result-object v0

    .line 11
    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/google/obf/fj$d;->g:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public keySet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<TK;>;"
        }
    .end annotation

    .prologue
    .line 187
    iget-object v0, p0, Lcom/google/obf/fj;->i:Lcom/google/obf/fj$b;

    .line 188
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/obf/fj$b;

    invoke-direct {v0, p0}, Lcom/google/obf/fj$b;-><init>(Lcom/google/obf/fj;)V

    iput-object v0, p0, Lcom/google/obf/fj;->i:Lcom/google/obf/fj$b;

    goto :goto_0
.end method

.method public put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    .prologue
    .line 13
    if-nez p1, :cond_0

    .line 14
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "key == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 15
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/google/obf/fj;->a(Ljava/lang/Object;Z)Lcom/google/obf/fj$d;

    move-result-object v0

    .line 16
    iget-object v1, v0, Lcom/google/obf/fj$d;->g:Ljava/lang/Object;

    .line 17
    iput-object p2, v0, Lcom/google/obf/fj$d;->g:Ljava/lang/Object;

    .line 18
    return-object v1
.end method

.method public remove(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 25
    invoke-virtual {p0, p1}, Lcom/google/obf/fj;->b(Ljava/lang/Object;)Lcom/google/obf/fj$d;

    move-result-object v0

    .line 26
    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/google/obf/fj$d;->g:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 9
    iget v0, p0, Lcom/google/obf/fj;->c:I

    return v0
.end method
