.class public Lcom/yandex/metrica/impl/ob/fi;
.super Lcom/yandex/metrica/impl/ob/ff;
.source "SourceFile"


# static fields
.field private static final c:Lcom/yandex/metrica/impl/ob/fm;

.field private static final d:Lcom/yandex/metrica/impl/ob/fm;


# instance fields
.field private e:Lcom/yandex/metrica/impl/ob/fm;

.field private f:Lcom/yandex/metrica/impl/ob/fm;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 17
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "SERVICE_API_LEVEL"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/fi;->c:Lcom/yandex/metrica/impl/ob/fm;

    .line 18
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "CLIENT_API_LEVEL"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/fi;->d:Lcom/yandex/metrica/impl/ob/fm;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 25
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/yandex/metrica/impl/ob/ff;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 27
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    sget-object v1, Lcom/yandex/metrica/impl/ob/fi;->c:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/fi;->e:Lcom/yandex/metrica/impl/ob/fm;

    .line 28
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    sget-object v1, Lcom/yandex/metrica/impl/ob/fi;->d:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/fi;->f:Lcom/yandex/metrica/impl/ob/fm;

    .line 29
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 40
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fi;->b:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fi;->e:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v1

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public b()Lcom/yandex/metrica/impl/ob/fi;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fi;->e:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/ob/fi;->h(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/ff;

    .line 54
    return-object p0
.end method

.method public c()Lcom/yandex/metrica/impl/ob/fi;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fi;->f:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/ob/fi;->h(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/ff;

    .line 59
    return-object p0
.end method

.method protected f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    const-string v0, "_migrationpreferences"

    return-object v0
.end method
