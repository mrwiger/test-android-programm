.class public Lru/cn/api/catalogue/replies/CatalogueItem;
.super Ljava/lang/Object;
.source "CatalogueItem.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/api/catalogue/replies/CatalogueItem$MediaGuideType;
    }
.end annotation


# instance fields
.field public guideType:Lru/cn/api/catalogue/replies/CatalogueItem$MediaGuideType;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "guide_type"
    .end annotation
.end field

.field public id:J
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "id"
    .end annotation
.end field

.field public relatedRubrics:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "related_rubrics"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lru/cn/api/catalogue/replies/Rubric;",
            ">;"
        }
    .end annotation
.end field

.field public selectionAttributes:Lru/cn/api/catalogue/replies/CatalogueItemSelectionAttributes;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "selection_attributes"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
