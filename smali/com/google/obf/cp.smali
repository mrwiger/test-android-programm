.class final Lcom/google/obf/cp;
.super Ljava/lang/Object;
.source "IMASDK"

# interfaces
.implements Lcom/google/obf/cq;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/obf/cp$a;
    }
.end annotation


# instance fields
.field private final a:[B

.field private final b:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Lcom/google/obf/cp$a;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/google/obf/ct;

.field private d:Lcom/google/obf/cr;

.field private e:I

.field private f:I

.field private g:J


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const/16 v0, 0x8

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/google/obf/cp;->a:[B

    .line 3
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/google/obf/cp;->b:Ljava/util/Stack;

    .line 4
    new-instance v0, Lcom/google/obf/ct;

    invoke-direct {v0}, Lcom/google/obf/ct;-><init>()V

    iput-object v0, p0, Lcom/google/obf/cp;->c:Lcom/google/obf/ct;

    return-void
.end method

.method private a(Lcom/google/obf/ak;I)J
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 68
    iget-object v1, p0, Lcom/google/obf/cp;->a:[B

    invoke-interface {p1, v1, v0, p2}, Lcom/google/obf/ak;->b([BII)V

    .line 69
    const-wide/16 v2, 0x0

    .line 70
    :goto_0
    if-ge v0, p2, :cond_0

    .line 71
    const/16 v1, 0x8

    shl-long/2addr v2, v1

    iget-object v1, p0, Lcom/google/obf/cp;->a:[B

    aget-byte v1, v1, v0

    and-int/lit16 v1, v1, 0xff

    int-to-long v4, v1

    or-long/2addr v2, v4

    .line 72
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 73
    :cond_0
    return-wide v2
.end method

.method private b(Lcom/google/obf/ak;I)D
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 74
    invoke-direct {p0, p1, p2}, Lcom/google/obf/cp;->a(Lcom/google/obf/ak;I)J

    move-result-wide v0

    .line 75
    const/4 v2, 0x4

    if-ne p2, v2, :cond_0

    .line 76
    long-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    float-to-double v0, v0

    .line 78
    :goto_0
    return-wide v0

    .line 77
    :cond_0
    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    goto :goto_0
.end method

.method private b(Lcom/google/obf/ak;)J
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/EOFException;,
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x0

    .line 58
    invoke-interface {p1}, Lcom/google/obf/ak;->a()V

    .line 59
    :goto_0
    iget-object v0, p0, Lcom/google/obf/cp;->a:[B

    invoke-interface {p1, v0, v4, v5}, Lcom/google/obf/ak;->c([BII)V

    .line 60
    iget-object v0, p0, Lcom/google/obf/cp;->a:[B

    aget-byte v0, v0, v4

    invoke-static {v0}, Lcom/google/obf/ct;->a(I)I

    move-result v0

    .line 61
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    if-gt v0, v5, :cond_0

    .line 62
    iget-object v1, p0, Lcom/google/obf/cp;->a:[B

    invoke-static {v1, v0, v4}, Lcom/google/obf/ct;->a([BIZ)J

    move-result-wide v2

    long-to-int v1, v2

    .line 63
    iget-object v2, p0, Lcom/google/obf/cp;->d:Lcom/google/obf/cr;

    invoke-interface {v2, v1}, Lcom/google/obf/cr;->b(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 64
    invoke-interface {p1, v0}, Lcom/google/obf/ak;->b(I)V

    .line 65
    int-to-long v0, v1

    return-wide v0

    .line 66
    :cond_0
    const/4 v0, 0x1

    invoke-interface {p1, v0}, Lcom/google/obf/ak;->b(I)V

    goto :goto_0
.end method

.method private c(Lcom/google/obf/ak;I)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 79
    if-nez p2, :cond_0

    .line 80
    const-string v0, ""

    .line 83
    :goto_0
    return-object v0

    .line 81
    :cond_0
    new-array v1, p2, [B

    .line 82
    const/4 v0, 0x0

    invoke-interface {p1, v1, v0, p2}, Lcom/google/obf/ak;->b([BII)V

    .line 83
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([B)V

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 7
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/obf/cp;->e:I

    .line 8
    iget-object v0, p0, Lcom/google/obf/cp;->b:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->clear()V

    .line 9
    iget-object v0, p0, Lcom/google/obf/cp;->c:Lcom/google/obf/ct;

    invoke-virtual {v0}, Lcom/google/obf/ct;->a()V

    .line 10
    return-void
.end method

.method public a(Lcom/google/obf/cr;)V
    .locals 0

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/obf/cp;->d:Lcom/google/obf/cr;

    .line 6
    return-void
.end method

.method public a(Lcom/google/obf/ak;)Z
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x8

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 11
    iget-object v0, p0, Lcom/google/obf/cp;->d:Lcom/google/obf/cr;

    if-eqz v0, :cond_0

    move v0, v6

    :goto_0
    invoke-static {v0}, Lcom/google/obf/dl;->b(Z)V

    .line 12
    :goto_1
    iget-object v0, p0, Lcom/google/obf/cp;->b:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 13
    invoke-interface {p1}, Lcom/google/obf/ak;->c()J

    move-result-wide v2

    iget-object v0, p0, Lcom/google/obf/cp;->b:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/cp$a;

    invoke-static {v0}, Lcom/google/obf/cp$a;->a(Lcom/google/obf/cp$a;)J

    move-result-wide v0

    cmp-long v0, v2, v0

    if-ltz v0, :cond_1

    .line 14
    iget-object v1, p0, Lcom/google/obf/cp;->d:Lcom/google/obf/cr;

    iget-object v0, p0, Lcom/google/obf/cp;->b:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/cp$a;

    invoke-static {v0}, Lcom/google/obf/cp$a;->b(Lcom/google/obf/cp$a;)I

    move-result v0

    invoke-interface {v1, v0}, Lcom/google/obf/cr;->c(I)V

    .line 52
    :goto_2
    return v6

    :cond_0
    move v0, v7

    .line 11
    goto :goto_0

    .line 16
    :cond_1
    iget v0, p0, Lcom/google/obf/cp;->e:I

    if-nez v0, :cond_4

    .line 17
    iget-object v0, p0, Lcom/google/obf/cp;->c:Lcom/google/obf/ct;

    const/4 v1, 0x4

    invoke-virtual {v0, p1, v6, v7, v1}, Lcom/google/obf/ct;->a(Lcom/google/obf/ak;ZZI)J

    move-result-wide v0

    .line 18
    const-wide/16 v2, -0x2

    cmp-long v2, v0, v2

    if-nez v2, :cond_2

    .line 19
    invoke-direct {p0, p1}, Lcom/google/obf/cp;->b(Lcom/google/obf/ak;)J

    move-result-wide v0

    .line 20
    :cond_2
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-nez v2, :cond_3

    move v6, v7

    .line 21
    goto :goto_2

    .line 22
    :cond_3
    long-to-int v0, v0

    iput v0, p0, Lcom/google/obf/cp;->f:I

    .line 23
    iput v6, p0, Lcom/google/obf/cp;->e:I

    .line 24
    :cond_4
    iget v0, p0, Lcom/google/obf/cp;->e:I

    if-ne v0, v6, :cond_5

    .line 25
    iget-object v0, p0, Lcom/google/obf/cp;->c:Lcom/google/obf/ct;

    const/16 v1, 0x8

    invoke-virtual {v0, p1, v7, v6, v1}, Lcom/google/obf/ct;->a(Lcom/google/obf/ak;ZZI)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/obf/cp;->g:J

    .line 26
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/obf/cp;->e:I

    .line 27
    :cond_5
    iget-object v0, p0, Lcom/google/obf/cp;->d:Lcom/google/obf/cr;

    iget v1, p0, Lcom/google/obf/cp;->f:I

    invoke-interface {v0, v1}, Lcom/google/obf/cr;->a(I)I

    move-result v0

    .line 28
    packed-switch v0, :pswitch_data_0

    .line 56
    new-instance v1, Lcom/google/obf/s;

    const/16 v2, 0x20

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Invalid element type "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v1

    .line 29
    :pswitch_0
    invoke-interface {p1}, Lcom/google/obf/ak;->c()J

    move-result-wide v2

    .line 30
    iget-wide v0, p0, Lcom/google/obf/cp;->g:J

    add-long/2addr v0, v2

    .line 31
    iget-object v4, p0, Lcom/google/obf/cp;->b:Ljava/util/Stack;

    new-instance v5, Lcom/google/obf/cp$a;

    iget v8, p0, Lcom/google/obf/cp;->f:I

    const/4 v9, 0x0

    invoke-direct {v5, v8, v0, v1, v9}, Lcom/google/obf/cp$a;-><init>(IJLcom/google/obf/cp$1;)V

    invoke-virtual {v4, v5}, Ljava/util/Stack;->add(Ljava/lang/Object;)Z

    .line 32
    iget-object v0, p0, Lcom/google/obf/cp;->d:Lcom/google/obf/cr;

    iget v1, p0, Lcom/google/obf/cp;->f:I

    iget-wide v4, p0, Lcom/google/obf/cp;->g:J

    invoke-interface/range {v0 .. v5}, Lcom/google/obf/cr;->a(IJJ)V

    .line 33
    iput v7, p0, Lcom/google/obf/cp;->e:I

    goto :goto_2

    .line 35
    :pswitch_1
    iget-wide v0, p0, Lcom/google/obf/cp;->g:J

    cmp-long v0, v0, v4

    if-lez v0, :cond_6

    .line 36
    new-instance v0, Lcom/google/obf/s;

    iget-wide v2, p0, Lcom/google/obf/cp;->g:J

    const/16 v1, 0x2a

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Invalid integer size: "

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v0

    .line 37
    :cond_6
    iget-object v0, p0, Lcom/google/obf/cp;->d:Lcom/google/obf/cr;

    iget v1, p0, Lcom/google/obf/cp;->f:I

    iget-wide v2, p0, Lcom/google/obf/cp;->g:J

    long-to-int v2, v2

    invoke-direct {p0, p1, v2}, Lcom/google/obf/cp;->a(Lcom/google/obf/ak;I)J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Lcom/google/obf/cr;->a(IJ)V

    .line 38
    iput v7, p0, Lcom/google/obf/cp;->e:I

    goto/16 :goto_2

    .line 40
    :pswitch_2
    iget-wide v0, p0, Lcom/google/obf/cp;->g:J

    const-wide/16 v2, 0x4

    cmp-long v0, v0, v2

    if-eqz v0, :cond_7

    iget-wide v0, p0, Lcom/google/obf/cp;->g:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_7

    .line 41
    new-instance v0, Lcom/google/obf/s;

    iget-wide v2, p0, Lcom/google/obf/cp;->g:J

    const/16 v1, 0x28

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Invalid float size: "

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v0

    .line 42
    :cond_7
    iget-object v0, p0, Lcom/google/obf/cp;->d:Lcom/google/obf/cr;

    iget v1, p0, Lcom/google/obf/cp;->f:I

    iget-wide v2, p0, Lcom/google/obf/cp;->g:J

    long-to-int v2, v2

    invoke-direct {p0, p1, v2}, Lcom/google/obf/cp;->b(Lcom/google/obf/ak;I)D

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Lcom/google/obf/cr;->a(ID)V

    .line 43
    iput v7, p0, Lcom/google/obf/cp;->e:I

    goto/16 :goto_2

    .line 45
    :pswitch_3
    iget-wide v0, p0, Lcom/google/obf/cp;->g:J

    const-wide/32 v2, 0x7fffffff

    cmp-long v0, v0, v2

    if-lez v0, :cond_8

    .line 46
    new-instance v0, Lcom/google/obf/s;

    iget-wide v2, p0, Lcom/google/obf/cp;->g:J

    const/16 v1, 0x29

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "String element size: "

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v0

    .line 47
    :cond_8
    iget-object v0, p0, Lcom/google/obf/cp;->d:Lcom/google/obf/cr;

    iget v1, p0, Lcom/google/obf/cp;->f:I

    iget-wide v2, p0, Lcom/google/obf/cp;->g:J

    long-to-int v2, v2

    invoke-direct {p0, p1, v2}, Lcom/google/obf/cp;->c(Lcom/google/obf/ak;I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/obf/cr;->a(ILjava/lang/String;)V

    .line 48
    iput v7, p0, Lcom/google/obf/cp;->e:I

    goto/16 :goto_2

    .line 50
    :pswitch_4
    iget-object v0, p0, Lcom/google/obf/cp;->d:Lcom/google/obf/cr;

    iget v1, p0, Lcom/google/obf/cp;->f:I

    iget-wide v2, p0, Lcom/google/obf/cp;->g:J

    long-to-int v2, v2

    invoke-interface {v0, v1, v2, p1}, Lcom/google/obf/cr;->a(IILcom/google/obf/ak;)V

    .line 51
    iput v7, p0, Lcom/google/obf/cp;->e:I

    goto/16 :goto_2

    .line 53
    :pswitch_5
    iget-wide v0, p0, Lcom/google/obf/cp;->g:J

    long-to-int v0, v0

    invoke-interface {p1, v0}, Lcom/google/obf/ak;->b(I)V

    .line 54
    iput v7, p0, Lcom/google/obf/cp;->e:I

    goto/16 :goto_1

    .line 28
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_2
    .end packed-switch
.end method
