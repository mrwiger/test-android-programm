.class Lru/cn/tv/player/FloatingPlayerFragment$2;
.super Ljava/lang/Object;
.source "FloatingPlayerFragment.java"

# interfaces
.implements Lru/cn/tv/player/SimplePlayerFragment$SimplePlayerFragmentListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/tv/player/FloatingPlayerFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/tv/player/FloatingPlayerFragment;


# direct methods
.method constructor <init>(Lru/cn/tv/player/FloatingPlayerFragment;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/tv/player/FloatingPlayerFragment;

    .prologue
    .line 136
    iput-object p1, p0, Lru/cn/tv/player/FloatingPlayerFragment$2;->this$0:Lru/cn/tv/player/FloatingPlayerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public adStarted()V
    .locals 0

    .prologue
    .line 180
    return-void
.end method

.method public adStopped()V
    .locals 0

    .prologue
    .line 176
    return-void
.end method

.method public channelChanged(J)V
    .locals 0
    .param p1, "cnId"    # J

    .prologue
    .line 172
    return-void
.end method

.method public contractorChanged(J)V
    .locals 1
    .param p1, "contractorId"    # J

    .prologue
    .line 164
    iget-object v0, p0, Lru/cn/tv/player/FloatingPlayerFragment$2;->this$0:Lru/cn/tv/player/FloatingPlayerFragment;

    invoke-static {v0}, Lru/cn/tv/player/FloatingPlayerFragment;->access$400(Lru/cn/tv/player/FloatingPlayerFragment;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    instance-of v0, v0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;

    if-eqz v0, :cond_0

    .line 165
    iget-object v0, p0, Lru/cn/tv/player/FloatingPlayerFragment$2;->this$0:Lru/cn/tv/player/FloatingPlayerFragment;

    invoke-static {v0}, Lru/cn/tv/player/FloatingPlayerFragment;->access$400(Lru/cn/tv/player/FloatingPlayerFragment;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;

    .line 166
    invoke-virtual {v0, p1, p2}, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->setContractor(J)V

    .line 168
    :cond_0
    return-void
.end method

.method public hasSchedule(Z)V
    .locals 1
    .param p1, "hasSchedule"    # Z

    .prologue
    .line 193
    iget-object v0, p0, Lru/cn/tv/player/FloatingPlayerFragment$2;->this$0:Lru/cn/tv/player/FloatingPlayerFragment;

    invoke-static {v0}, Lru/cn/tv/player/FloatingPlayerFragment;->access$400(Lru/cn/tv/player/FloatingPlayerFragment;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    instance-of v0, v0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;

    if-eqz v0, :cond_0

    .line 194
    if-nez p1, :cond_1

    .line 195
    iget-object v0, p0, Lru/cn/tv/player/FloatingPlayerFragment$2;->this$0:Lru/cn/tv/player/FloatingPlayerFragment;

    invoke-static {v0}, Lru/cn/tv/player/FloatingPlayerFragment;->access$400(Lru/cn/tv/player/FloatingPlayerFragment;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;

    invoke-virtual {v0}, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->showNoScheduleText()V

    .line 200
    :cond_0
    :goto_0
    return-void

    .line 197
    :cond_1
    iget-object v0, p0, Lru/cn/tv/player/FloatingPlayerFragment$2;->this$0:Lru/cn/tv/player/FloatingPlayerFragment;

    invoke-static {v0}, Lru/cn/tv/player/FloatingPlayerFragment;->access$400(Lru/cn/tv/player/FloatingPlayerFragment;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;

    invoke-virtual {v0}, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->hideNoScheduleText()V

    goto :goto_0
.end method

.method public minimize()V
    .locals 2

    .prologue
    .line 204
    iget-object v0, p0, Lru/cn/tv/player/FloatingPlayerFragment$2;->this$0:Lru/cn/tv/player/FloatingPlayerFragment;

    invoke-virtual {v0}, Lru/cn/tv/player/FloatingPlayerFragment;->isMaximized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 205
    iget-object v0, p0, Lru/cn/tv/player/FloatingPlayerFragment$2;->this$0:Lru/cn/tv/player/FloatingPlayerFragment;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lru/cn/tv/player/FloatingPlayerFragment;->minimize(Z)V

    .line 207
    :cond_0
    return-void
.end method

.method public onChannelInfoLoaded(JLjava/lang/String;ZIZ)V
    .locals 0
    .param p1, "cnId"    # J
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "isFavourite"    # Z
    .param p5, "channelNumber"    # I
    .param p6, "isDenied"    # Z

    .prologue
    .line 156
    return-void
.end method

.method public onTelecastInfoLoaded(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "descriprion"    # Ljava/lang/String;

    .prologue
    .line 160
    return-void
.end method

.method public playing(Z)V
    .locals 0
    .param p1, "playing"    # Z

    .prologue
    .line 150
    return-void
.end method

.method public telecastChanged(J)V
    .locals 1
    .param p1, "telecastId"    # J

    .prologue
    .line 140
    iget-object v0, p0, Lru/cn/tv/player/FloatingPlayerFragment$2;->this$0:Lru/cn/tv/player/FloatingPlayerFragment;

    invoke-static {v0}, Lru/cn/tv/player/FloatingPlayerFragment;->access$400(Lru/cn/tv/player/FloatingPlayerFragment;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    instance-of v0, v0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;

    if-eqz v0, :cond_1

    .line 141
    iget-object v0, p0, Lru/cn/tv/player/FloatingPlayerFragment$2;->this$0:Lru/cn/tv/player/FloatingPlayerFragment;

    invoke-static {v0}, Lru/cn/tv/player/FloatingPlayerFragment;->access$400(Lru/cn/tv/player/FloatingPlayerFragment;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;

    invoke-virtual {v0, p1, p2}, Lru/cn/tv/mobile/telecast/TelecastInfoFragment;->setTelecast(J)V

    .line 146
    :cond_0
    :goto_0
    return-void

    .line 143
    :cond_1
    iget-object v0, p0, Lru/cn/tv/player/FloatingPlayerFragment$2;->this$0:Lru/cn/tv/player/FloatingPlayerFragment;

    invoke-static {v0}, Lru/cn/tv/player/FloatingPlayerFragment;->access$400(Lru/cn/tv/player/FloatingPlayerFragment;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    instance-of v0, v0, Lru/cn/tv/mobile/rubric/SimpleRubricFragment;

    if-eqz v0, :cond_0

    .line 144
    iget-object v0, p0, Lru/cn/tv/player/FloatingPlayerFragment$2;->this$0:Lru/cn/tv/player/FloatingPlayerFragment;

    invoke-static {v0}, Lru/cn/tv/player/FloatingPlayerFragment;->access$400(Lru/cn/tv/player/FloatingPlayerFragment;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lru/cn/tv/mobile/rubric/SimpleRubricFragment;

    invoke-virtual {v0, p1, p2}, Lru/cn/tv/mobile/rubric/SimpleRubricFragment;->checkTelecast(J)V

    goto :goto_0
.end method

.method public videoSizeChanged(II)V
    .locals 4
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    .line 184
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 185
    iget-object v2, p0, Lru/cn/tv/player/FloatingPlayerFragment$2;->this$0:Lru/cn/tv/player/FloatingPlayerFragment;

    invoke-static {v2}, Lru/cn/tv/player/FloatingPlayerFragment;->access$500(Lru/cn/tv/player/FloatingPlayerFragment;)Lru/cn/draggableview/DraggableView;

    move-result-object v2

    invoke-virtual {v2}, Lru/cn/draggableview/DraggableView;->getFirstViewMinWidth()I

    move-result v1

    .line 186
    .local v1, "minWidth":I
    int-to-float v2, v1

    int-to-float v3, p1

    div-float/2addr v2, v3

    int-to-float v3, p2

    mul-float/2addr v2, v3

    float-to-int v0, v2

    .line 187
    .local v0, "minHeight":I
    iget-object v2, p0, Lru/cn/tv/player/FloatingPlayerFragment$2;->this$0:Lru/cn/tv/player/FloatingPlayerFragment;

    invoke-static {v2}, Lru/cn/tv/player/FloatingPlayerFragment;->access$500(Lru/cn/tv/player/FloatingPlayerFragment;)Lru/cn/draggableview/DraggableView;

    move-result-object v2

    invoke-virtual {v2, v0}, Lru/cn/draggableview/DraggableView;->setFirstViewMinHeight(I)V

    .line 189
    .end local v0    # "minHeight":I
    .end local v1    # "minWidth":I
    :cond_0
    return-void
.end method
