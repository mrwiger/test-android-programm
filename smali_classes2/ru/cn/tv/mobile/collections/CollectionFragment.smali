.class public Lru/cn/tv/mobile/collections/CollectionFragment;
.super Landroid/support/v4/app/Fragment;
.source "CollectionFragment.java"


# instance fields
.field private listener:Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastSelectedListener;

.field private pager:Landroid/support/v4/view/ViewPager;

.field private pagerAdapter:Lru/cn/tv/mobile/collections/PagerAdapter;

.field private rubricId:J

.field private tabs:Landroid/support/design/widget/TabLayout;

.field private viewModel:Lru/cn/tv/mobile/collections/CollectionViewModel;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method private setRubric(Lru/cn/api/catalogue/replies/Rubric;)V
    .locals 5
    .param p1, "rubric"    # Lru/cn/api/catalogue/replies/Rubric;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 69
    new-instance v0, Lru/cn/tv/mobile/collections/RubricOptionsController;

    iget-object v3, p0, Lru/cn/tv/mobile/collections/CollectionFragment;->listener:Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastSelectedListener;

    invoke-direct {v0, v3}, Lru/cn/tv/mobile/collections/RubricOptionsController;-><init>(Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastSelectedListener;)V

    .line 70
    .local v0, "controller":Lru/cn/tv/mobile/collections/RubricOptionsController;
    invoke-virtual {v0, p1}, Lru/cn/tv/mobile/collections/RubricOptionsController;->setRubric(Lru/cn/api/catalogue/replies/Rubric;)V

    .line 71
    new-instance v3, Lru/cn/tv/mobile/collections/PagerAdapter;

    invoke-virtual {p0}, Lru/cn/tv/mobile/collections/CollectionFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    invoke-direct {v3, v4, v0}, Lru/cn/tv/mobile/collections/PagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;Lru/cn/tv/mobile/collections/RubricFragmentController;)V

    iput-object v3, p0, Lru/cn/tv/mobile/collections/CollectionFragment;->pagerAdapter:Lru/cn/tv/mobile/collections/PagerAdapter;

    .line 73
    iget-object v3, p0, Lru/cn/tv/mobile/collections/CollectionFragment;->pager:Landroid/support/v4/view/ViewPager;

    iget-object v4, p0, Lru/cn/tv/mobile/collections/CollectionFragment;->pagerAdapter:Lru/cn/tv/mobile/collections/PagerAdapter;

    invoke-virtual {v3, v4}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 74
    iget-object v3, p0, Lru/cn/tv/mobile/collections/CollectionFragment;->tabs:Landroid/support/design/widget/TabLayout;

    iget-object v4, p0, Lru/cn/tv/mobile/collections/CollectionFragment;->pager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v3, v4}, Landroid/support/design/widget/TabLayout;->setupWithViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 76
    invoke-virtual {v0}, Lru/cn/tv/mobile/collections/RubricOptionsController;->rubricCount()I

    move-result v3

    if-le v3, v1, :cond_0

    .line 77
    .local v1, "hasRubrics":Z
    :goto_0
    iget-object v3, p0, Lru/cn/tv/mobile/collections/CollectionFragment;->tabs:Landroid/support/design/widget/TabLayout;

    if-eqz v1, :cond_1

    :goto_1
    invoke-virtual {v3, v2}, Landroid/support/design/widget/TabLayout;->setVisibility(I)V

    .line 78
    return-void

    .end local v1    # "hasRubrics":Z
    :cond_0
    move v1, v2

    .line 76
    goto :goto_0

    .line 77
    .restart local v1    # "hasRubrics":Z
    :cond_1
    const/16 v2, 0x8

    goto :goto_1
.end method


# virtual methods
.method final bridge synthetic bridge$lambda$0$CollectionFragment(Lru/cn/api/catalogue/replies/Rubric;)V
    .locals 0

    invoke-direct {p0, p1}, Lru/cn/tv/mobile/collections/CollectionFragment;->setRubric(Lru/cn/api/catalogue/replies/Rubric;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 32
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 34
    invoke-static {}, Lru/cn/tv/App;->scope()Ltoothpick/Scope;

    move-result-object v0

    const-class v1, Lru/cn/tv/mobile/collections/CollectionViewModel;

    invoke-static {p0, v0, v1}, Lru/cn/mvvm/ViewModels;->get(Landroid/support/v4/app/Fragment;Ltoothpick/Scope;Ljava/lang/Class;)Landroid/arch/lifecycle/ViewModel;

    move-result-object v0

    check-cast v0, Lru/cn/tv/mobile/collections/CollectionViewModel;

    iput-object v0, p0, Lru/cn/tv/mobile/collections/CollectionFragment;->viewModel:Lru/cn/tv/mobile/collections/CollectionViewModel;

    .line 35
    iget-object v0, p0, Lru/cn/tv/mobile/collections/CollectionFragment;->viewModel:Lru/cn/tv/mobile/collections/CollectionViewModel;

    invoke-virtual {v0}, Lru/cn/tv/mobile/collections/CollectionViewModel;->rubric()Landroid/arch/lifecycle/LiveData;

    move-result-object v0

    new-instance v1, Lru/cn/tv/mobile/collections/CollectionFragment$$Lambda$0;

    invoke-direct {v1, p0}, Lru/cn/tv/mobile/collections/CollectionFragment$$Lambda$0;-><init>(Lru/cn/tv/mobile/collections/CollectionFragment;)V

    invoke-virtual {v0, p0, v1}, Landroid/arch/lifecycle/LiveData;->observe(Landroid/arch/lifecycle/LifecycleOwner;Landroid/arch/lifecycle/Observer;)V

    .line 37
    iget-object v0, p0, Lru/cn/tv/mobile/collections/CollectionFragment;->viewModel:Lru/cn/tv/mobile/collections/CollectionViewModel;

    iget-wide v2, p0, Lru/cn/tv/mobile/collections/CollectionFragment;->rubricId:J

    invoke-virtual {v0, v2, v3}, Lru/cn/tv/mobile/collections/CollectionViewModel;->setRubricId(J)V

    .line 38
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 43
    const v0, 0x7f0c00b3

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 48
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 50
    const v0, 0x7f09015a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lru/cn/tv/mobile/collections/CollectionFragment;->pager:Landroid/support/v4/view/ViewPager;

    .line 51
    new-instance v0, Lru/cn/tv/mobile/collections/PagerAdapter;

    invoke-virtual {p0}, Lru/cn/tv/mobile/collections/CollectionFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lru/cn/tv/mobile/collections/PagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;Lru/cn/tv/mobile/collections/RubricFragmentController;)V

    iput-object v0, p0, Lru/cn/tv/mobile/collections/CollectionFragment;->pagerAdapter:Lru/cn/tv/mobile/collections/PagerAdapter;

    .line 52
    iget-object v0, p0, Lru/cn/tv/mobile/collections/CollectionFragment;->pager:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lru/cn/tv/mobile/collections/CollectionFragment;->pagerAdapter:Lru/cn/tv/mobile/collections/PagerAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 54
    const v0, 0x7f0901c4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TabLayout;

    iput-object v0, p0, Lru/cn/tv/mobile/collections/CollectionFragment;->tabs:Landroid/support/design/widget/TabLayout;

    .line 55
    iget-object v0, p0, Lru/cn/tv/mobile/collections/CollectionFragment;->tabs:Landroid/support/design/widget/TabLayout;

    iget-object v1, p0, Lru/cn/tv/mobile/collections/CollectionFragment;->pager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TabLayout;->setupWithViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 56
    iget-object v0, p0, Lru/cn/tv/mobile/collections/CollectionFragment;->tabs:Landroid/support/design/widget/TabLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TabLayout;->setTabMode(I)V

    .line 57
    iget-object v0, p0, Lru/cn/tv/mobile/collections/CollectionFragment;->tabs:Landroid/support/design/widget/TabLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TabLayout;->setVisibility(I)V

    .line 58
    return-void
.end method

.method public setListener(Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastSelectedListener;)V
    .locals 0
    .param p1, "listener"    # Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastSelectedListener;

    .prologue
    .line 65
    iput-object p1, p0, Lru/cn/tv/mobile/collections/CollectionFragment;->listener:Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastSelectedListener;

    .line 66
    return-void
.end method

.method public setRubricId(J)V
    .locals 1
    .param p1, "rubricId"    # J

    .prologue
    .line 61
    iput-wide p1, p0, Lru/cn/tv/mobile/collections/CollectionFragment;->rubricId:J

    .line 62
    return-void
.end method
