.class public Lru/cn/notifications/entity/INotification$NewChannelMessage;
.super Ljava/lang/Object;
.source "INotification.java"

# interfaces
.implements Lru/cn/notifications/entity/INotification;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/notifications/entity/INotification;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "NewChannelMessage"
.end annotation


# instance fields
.field private final channelId:J

.field private final createTime:J

.field private final message:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;JJ)V
    .locals 0
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "channelId"    # J
    .param p4, "createTime"    # J

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object p1, p0, Lru/cn/notifications/entity/INotification$NewChannelMessage;->message:Ljava/lang/String;

    .line 61
    iput-wide p2, p0, Lru/cn/notifications/entity/INotification$NewChannelMessage;->channelId:J

    .line 62
    iput-wide p4, p0, Lru/cn/notifications/entity/INotification$NewChannelMessage;->createTime:J

    .line 63
    return-void
.end method


# virtual methods
.method public channelId()J
    .locals 2

    .prologue
    .line 76
    iget-wide v0, p0, Lru/cn/notifications/entity/INotification$NewChannelMessage;->channelId:J

    return-wide v0
.end method

.method public createTime()J
    .locals 2

    .prologue
    .line 72
    iget-wide v0, p0, Lru/cn/notifications/entity/INotification$NewChannelMessage;->createTime:J

    return-wide v0
.end method

.method public message()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lru/cn/notifications/entity/INotification$NewChannelMessage;->message:Ljava/lang/String;

    return-object v0
.end method
