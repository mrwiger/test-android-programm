.class public Lcom/my/target/nativeads/banners/NativePromoCard;
.super Ljava/lang/Object;
.source "NativePromoCard.java"


# instance fields
.field private final ctaText:Ljava/lang/String;

.field private final description:Ljava/lang/String;

.field private final image:Lcom/my/target/common/models/ImageData;

.field private final title:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/my/target/core/models/banners/b;)V
    .locals 2
    .param p1, "nativeAdCard"    # Lcom/my/target/core/models/banners/b;

    .prologue
    const/4 v1, 0x0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/b;->getTitle()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 26
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/b;->getTitle()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/nativeads/banners/NativePromoCard;->title:Ljava/lang/String;

    .line 32
    :goto_0
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/b;->getDescription()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 34
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/b;->getDescription()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/nativeads/banners/NativePromoCard;->description:Ljava/lang/String;

    .line 40
    :goto_1
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/b;->getCtaText()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 42
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/b;->getCtaText()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/nativeads/banners/NativePromoCard;->ctaText:Ljava/lang/String;

    .line 49
    :goto_2
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/b;->getImage()Lcom/my/target/common/models/ImageData;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/nativeads/banners/NativePromoCard;->image:Lcom/my/target/common/models/ImageData;

    .line 50
    return-void

    .line 30
    :cond_0
    iput-object v1, p0, Lcom/my/target/nativeads/banners/NativePromoCard;->title:Ljava/lang/String;

    goto :goto_0

    .line 38
    :cond_1
    iput-object v1, p0, Lcom/my/target/nativeads/banners/NativePromoCard;->description:Ljava/lang/String;

    goto :goto_1

    .line 46
    :cond_2
    iput-object v1, p0, Lcom/my/target/nativeads/banners/NativePromoCard;->ctaText:Ljava/lang/String;

    goto :goto_2
.end method

.method static newCard(Lcom/my/target/core/models/banners/b;)Lcom/my/target/nativeads/banners/NativePromoCard;
    .locals 1
    .param p0, "nativeAdCard"    # Lcom/my/target/core/models/banners/b;

    .prologue
    .line 14
    new-instance v0, Lcom/my/target/nativeads/banners/NativePromoCard;

    invoke-direct {v0, p0}, Lcom/my/target/nativeads/banners/NativePromoCard;-><init>(Lcom/my/target/core/models/banners/b;)V

    return-object v0
.end method


# virtual methods
.method public getCtaText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/my/target/nativeads/banners/NativePromoCard;->ctaText:Ljava/lang/String;

    return-object v0
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/my/target/nativeads/banners/NativePromoCard;->description:Ljava/lang/String;

    return-object v0
.end method

.method public getImage()Lcom/my/target/common/models/ImageData;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/my/target/nativeads/banners/NativePromoCard;->image:Lcom/my/target/common/models/ImageData;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/my/target/nativeads/banners/NativePromoCard;->title:Ljava/lang/String;

    return-object v0
.end method
