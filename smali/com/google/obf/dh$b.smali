.class final Lcom/google/obf/dh$b;
.super Landroid/os/Handler;
.source "IMASDK"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "HandlerLeak"
    }
.end annotation

.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/obf/dh;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "b"
.end annotation


# instance fields
.field final synthetic a:Lcom/google/obf/dh;

.field private final b:Lcom/google/obf/dh$c;

.field private final c:Lcom/google/obf/dh$a;

.field private volatile d:Ljava/lang/Thread;


# direct methods
.method public constructor <init>(Lcom/google/obf/dh;Landroid/os/Looper;Lcom/google/obf/dh$c;Lcom/google/obf/dh$a;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/google/obf/dh$b;->a:Lcom/google/obf/dh;

    .line 2
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 3
    iput-object p3, p0, Lcom/google/obf/dh$b;->b:Lcom/google/obf/dh$c;

    .line 4
    iput-object p4, p0, Lcom/google/obf/dh$b;->c:Lcom/google/obf/dh$a;

    .line 5
    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/obf/dh$b;->a:Lcom/google/obf/dh;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/obf/dh;->a(Lcom/google/obf/dh;Z)Z

    .line 45
    iget-object v0, p0, Lcom/google/obf/dh$b;->a:Lcom/google/obf/dh;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/obf/dh;->a(Lcom/google/obf/dh;Lcom/google/obf/dh$b;)Lcom/google/obf/dh$b;

    .line 46
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 6
    iget-object v0, p0, Lcom/google/obf/dh$b;->b:Lcom/google/obf/dh$c;

    invoke-interface {v0}, Lcom/google/obf/dh$c;->a()V

    .line 7
    iget-object v0, p0, Lcom/google/obf/dh$b;->d:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    .line 8
    iget-object v0, p0, Lcom/google/obf/dh$b;->d:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 9
    :cond_0
    return-void
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 33
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 34
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Error;

    throw v0

    .line 35
    :cond_0
    invoke-direct {p0}, Lcom/google/obf/dh$b;->b()V

    .line 36
    iget-object v0, p0, Lcom/google/obf/dh$b;->b:Lcom/google/obf/dh$c;

    invoke-interface {v0}, Lcom/google/obf/dh$c;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 37
    iget-object v0, p0, Lcom/google/obf/dh$b;->c:Lcom/google/obf/dh$a;

    iget-object v1, p0, Lcom/google/obf/dh$b;->b:Lcom/google/obf/dh$c;

    invoke-interface {v0, v1}, Lcom/google/obf/dh$a;->b(Lcom/google/obf/dh$c;)V

    .line 43
    :goto_0
    return-void

    .line 39
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 40
    :pswitch_0
    iget-object v0, p0, Lcom/google/obf/dh$b;->c:Lcom/google/obf/dh$a;

    iget-object v1, p0, Lcom/google/obf/dh$b;->b:Lcom/google/obf/dh$c;

    invoke-interface {v0, v1}, Lcom/google/obf/dh$a;->a(Lcom/google/obf/dh$c;)V

    goto :goto_0

    .line 42
    :pswitch_1
    iget-object v1, p0, Lcom/google/obf/dh$b;->c:Lcom/google/obf/dh$a;

    iget-object v2, p0, Lcom/google/obf/dh$b;->b:Lcom/google/obf/dh$c;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/io/IOException;

    invoke-interface {v1, v2, v0}, Lcom/google/obf/dh$a;->a(Lcom/google/obf/dh$c;Ljava/io/IOException;)V

    goto :goto_0

    .line 39
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 10
    :try_start_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iput-object v0, p0, Lcom/google/obf/dh$b;->d:Ljava/lang/Thread;

    .line 11
    iget-object v0, p0, Lcom/google/obf/dh$b;->b:Lcom/google/obf/dh$c;

    invoke-interface {v0}, Lcom/google/obf/dh$c;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 12
    iget-object v0, p0, Lcom/google/obf/dh$b;->b:Lcom/google/obf/dh$c;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ".load()"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/obf/dz;->a(Ljava/lang/String;)V

    .line 13
    iget-object v0, p0, Lcom/google/obf/dh$b;->b:Lcom/google/obf/dh$c;

    invoke-interface {v0}, Lcom/google/obf/dh$c;->c()V

    .line 14
    invoke-static {}, Lcom/google/obf/dz;->a()V

    .line 15
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/obf/dh$b;->sendEmptyMessage(I)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_3

    .line 32
    :goto_0
    return-void

    .line 17
    :catch_0
    move-exception v0

    .line 18
    invoke-virtual {p0, v3, v0}, Lcom/google/obf/dh$b;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    .line 20
    :catch_1
    move-exception v0

    .line 21
    iget-object v0, p0, Lcom/google/obf/dh$b;->b:Lcom/google/obf/dh$c;

    invoke-interface {v0}, Lcom/google/obf/dh$c;->b()Z

    move-result v0

    invoke-static {v0}, Lcom/google/obf/dl;->b(Z)V

    .line 22
    invoke-virtual {p0, v2}, Lcom/google/obf/dh$b;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 24
    :catch_2
    move-exception v0

    .line 25
    const-string v1, "LoadTask"

    const-string v2, "Unexpected exception loading stream"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 26
    new-instance v1, Lcom/google/obf/dh$d;

    invoke-direct {v1, v0}, Lcom/google/obf/dh$d;-><init>(Ljava/lang/Exception;)V

    invoke-virtual {p0, v3, v1}, Lcom/google/obf/dh$b;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    .line 28
    :catch_3
    move-exception v0

    .line 29
    const-string v1, "LoadTask"

    const-string v2, "Unexpected error loading stream"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 30
    const/4 v1, 0x2

    invoke-virtual {p0, v1, v0}, Lcom/google/obf/dh$b;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 31
    throw v0
.end method
