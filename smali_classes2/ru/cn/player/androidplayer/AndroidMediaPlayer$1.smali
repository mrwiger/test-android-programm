.class Lru/cn/player/androidplayer/AndroidMediaPlayer$1;
.super Ljava/lang/Object;
.source "AndroidMediaPlayer.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/player/androidplayer/AndroidMediaPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/player/androidplayer/AndroidMediaPlayer;


# direct methods
.method constructor <init>(Lru/cn/player/androidplayer/AndroidMediaPlayer;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/player/androidplayer/AndroidMediaPlayer;

    .prologue
    .line 55
    iput-object p1, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer$1;->this$0:Lru/cn/player/androidplayer/AndroidMediaPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 2
    .param p1, "mp"    # Landroid/media/MediaPlayer;

    .prologue
    .line 57
    iget-object v0, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer$1;->this$0:Lru/cn/player/androidplayer/AndroidMediaPlayer;

    sget-object v1, Lru/cn/player/AbstractMediaPlayer$PlayerState;->LOADED:Lru/cn/player/AbstractMediaPlayer$PlayerState;

    invoke-virtual {v0, v1}, Lru/cn/player/androidplayer/AndroidMediaPlayer;->setState(Lru/cn/player/AbstractMediaPlayer$PlayerState;)V

    .line 59
    iget-object v0, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer$1;->this$0:Lru/cn/player/androidplayer/AndroidMediaPlayer;

    invoke-static {v0}, Lru/cn/player/androidplayer/AndroidMediaPlayer;->access$000(Lru/cn/player/androidplayer/AndroidMediaPlayer;)I

    move-result v0

    if-eqz v0, :cond_0

    .line 60
    iget-object v0, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer$1;->this$0:Lru/cn/player/androidplayer/AndroidMediaPlayer;

    iget-object v1, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer$1;->this$0:Lru/cn/player/androidplayer/AndroidMediaPlayer;

    invoke-static {v1}, Lru/cn/player/androidplayer/AndroidMediaPlayer;->access$000(Lru/cn/player/androidplayer/AndroidMediaPlayer;)I

    move-result v1

    invoke-virtual {v0, v1}, Lru/cn/player/androidplayer/AndroidMediaPlayer;->seekTo(I)V

    .line 61
    iget-object v0, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer$1;->this$0:Lru/cn/player/androidplayer/AndroidMediaPlayer;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lru/cn/player/androidplayer/AndroidMediaPlayer;->access$002(Lru/cn/player/androidplayer/AndroidMediaPlayer;I)I

    .line 64
    :cond_0
    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoWidth()I

    move-result v0

    if-lez v0, :cond_1

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoHeight()I

    move-result v0

    if-lez v0, :cond_1

    .line 65
    iget-object v0, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer$1;->this$0:Lru/cn/player/androidplayer/AndroidMediaPlayer;

    sget-object v1, Lru/cn/player/AbstractMediaPlayer$PlayerState;->PLAYING:Lru/cn/player/AbstractMediaPlayer$PlayerState;

    invoke-virtual {v0, v1}, Lru/cn/player/androidplayer/AndroidMediaPlayer;->setState(Lru/cn/player/AbstractMediaPlayer$PlayerState;)V

    .line 68
    :cond_1
    iget-object v0, p0, Lru/cn/player/androidplayer/AndroidMediaPlayer$1;->this$0:Lru/cn/player/androidplayer/AndroidMediaPlayer;

    invoke-static {v0}, Lru/cn/player/androidplayer/AndroidMediaPlayer;->access$100(Lru/cn/player/androidplayer/AndroidMediaPlayer;)Lru/cn/player/AudioFocus;

    move-result-object v0

    invoke-virtual {v0}, Lru/cn/player/AudioFocus;->request()Z

    .line 69
    invoke-virtual {p1}, Landroid/media/MediaPlayer;->start()V

    .line 70
    return-void
.end method
