.class public Lru/cn/tv/playlists/UserPlaylistsAdapter;
.super Landroid/widget/SimpleCursorAdapter;
.source "UserPlaylistsAdapter.java"


# static fields
.field private static from:[Ljava/lang/String;

.field private static to:[I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 13
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    sget-object v2, Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;->title:Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;

    .line 14
    invoke-virtual {v2}, Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;->location:Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;

    invoke-virtual {v2}, Lru/cn/api/provider/TvContentProviderContract$PlaylistColumn;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    sput-object v0, Lru/cn/tv/playlists/UserPlaylistsAdapter;->from:[Ljava/lang/String;

    .line 15
    new-array v0, v3, [I

    fill-array-data v0, :array_0

    sput-object v0, Lru/cn/tv/playlists/UserPlaylistsAdapter;->to:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f0901d7
        0x7f090114
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;ILandroid/database/Cursor;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layout"    # I
    .param p3, "c"    # Landroid/database/Cursor;

    .prologue
    .line 18
    sget-object v4, Lru/cn/tv/playlists/UserPlaylistsAdapter;->from:[Ljava/lang/String;

    sget-object v5, Lru/cn/tv/playlists/UserPlaylistsAdapter;->to:[I

    const/4 v6, 0x2

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v6}, Landroid/widget/SimpleCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[II)V

    .line 20
    return-void
.end method
