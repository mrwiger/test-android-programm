.class public Ltoothpick/registries/MemberInjectorRegistryLocator;
.super Ljava/lang/Object;
.source "MemberInjectorRegistryLocator.java"


# static fields
.field private static registry:Ltoothpick/registries/MemberInjectorRegistry;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    return-void
.end method

.method public static getMemberInjector(Ljava/lang/Class;)Ltoothpick/MemberInjector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Ltoothpick/MemberInjector",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 26
    .local p0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    sget-object v0, Ltoothpick/configuration/ConfigurationHolder;->configuration:Ltoothpick/configuration/Configuration;

    invoke-virtual {v0, p0}, Ltoothpick/configuration/Configuration;->getMemberInjector(Ljava/lang/Class;)Ltoothpick/MemberInjector;

    move-result-object v0

    return-object v0
.end method

.method public static getMemberInjectorUsingReflection(Ljava/lang/Class;)Ltoothpick/MemberInjector;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Ltoothpick/MemberInjector",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 42
    .local p0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 43
    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "$$MemberInjector"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    .line 44
    .local v1, "memberInjectorClass":Ljava/lang/Class;, "Ljava/lang/Class<+Ltoothpick/MemberInjector<TT;>;>;"
    invoke-virtual {v1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ltoothpick/MemberInjector;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 46
    .end local v1    # "memberInjectorClass":Ljava/lang/Class;, "Ljava/lang/Class<+Ltoothpick/MemberInjector<TT;>;>;"
    :goto_0
    return-object v2

    .line 45
    :catch_0
    move-exception v0

    .line 46
    .local v0, "e":Ljava/lang/Exception;
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static getMemberInjectorUsingRegistries(Ljava/lang/Class;)Ltoothpick/MemberInjector;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Ltoothpick/MemberInjector",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 31
    .local p0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    sget-object v1, Ltoothpick/registries/MemberInjectorRegistryLocator;->registry:Ltoothpick/registries/MemberInjectorRegistry;

    if-eqz v1, :cond_0

    .line 32
    sget-object v1, Ltoothpick/registries/MemberInjectorRegistryLocator;->registry:Ltoothpick/registries/MemberInjectorRegistry;

    invoke-interface {v1, p0}, Ltoothpick/registries/MemberInjectorRegistry;->getMemberInjector(Ljava/lang/Class;)Ltoothpick/MemberInjector;

    move-result-object v0

    .line 33
    .local v0, "memberInjector":Ltoothpick/MemberInjector;, "Ltoothpick/MemberInjector<TT;>;"
    if-eqz v0, :cond_0

    .line 37
    .end local v0    # "memberInjector":Ltoothpick/MemberInjector;, "Ltoothpick/MemberInjector<TT;>;"
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static setRootRegistry(Ltoothpick/registries/MemberInjectorRegistry;)V
    .locals 0
    .param p0, "registry"    # Ltoothpick/registries/MemberInjectorRegistry;

    .prologue
    .line 22
    sput-object p0, Ltoothpick/registries/MemberInjectorRegistryLocator;->registry:Ltoothpick/registries/MemberInjectorRegistry;

    .line 23
    return-void
.end method
