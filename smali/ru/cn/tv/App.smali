.class public final Lru/cn/tv/App;
.super Landroid/app/Application;
.source "App.java"


# static fields
.field private static INSTANCE:Lru/cn/tv/App;


# instance fields
.field private scope:Ltoothpick/Scope;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    return-void
.end method

.method private bindDependencies()V
    .locals 1

    .prologue
    .line 75
    invoke-static {}, Lru/cn/utils/Utils;->isTV()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    invoke-direct {p0}, Lru/cn/tv/App;->bindTvDependencies()V

    .line 78
    :cond_0
    return-void
.end method

.method private bindTvDependencies()V
    .locals 4

    .prologue
    .line 82
    iget-object v0, p0, Lru/cn/tv/App;->scope:Ltoothpick/Scope;

    const/4 v1, 0x1

    new-array v1, v1, [Ltoothpick/config/Module;

    const/4 v2, 0x0

    new-instance v3, Lru/cn/tv/App$2;

    invoke-direct {v3, p0}, Lru/cn/tv/App$2;-><init>(Lru/cn/tv/App;)V

    aput-object v3, v1, v2

    invoke-interface {v0, v1}, Ltoothpick/Scope;->installModules([Ltoothpick/config/Module;)V

    .line 86
    return-void
.end method

.method private isMainProcess()Z
    .locals 7

    .prologue
    .line 89
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v1

    .line 90
    .local v1, "id":I
    invoke-virtual {p0}, Lru/cn/tv/App;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 92
    .local v2, "myProcessName":Ljava/lang/String;
    const-string v5, "activity"

    invoke-virtual {p0, v5}, Lru/cn/tv/App;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 93
    .local v0, "actvityManager":Landroid/app/ActivityManager;
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v4

    .line 95
    .local v4, "procInfos":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningAppProcessInfo;>;"
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 96
    .local v3, "procInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
    iget v6, v3, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    if-ne v1, v6, :cond_0

    .line 97
    iget-object v2, v3, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    .line 102
    .end local v3    # "procInfo":Landroid/app/ActivityManager$RunningAppProcessInfo;
    :cond_1
    invoke-virtual {p0}, Lru/cn/tv/App;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    return v5
.end method

.method public static scope()Ltoothpick/Scope;
    .locals 1

    .prologue
    .line 71
    sget-object v0, Lru/cn/tv/App;->INSTANCE:Lru/cn/tv/App;

    iget-object v0, v0, Lru/cn/tv/App;->scope:Ltoothpick/Scope;

    return-object v0
.end method


# virtual methods
.method protected attachBaseContext(Landroid/content/Context;)V
    .locals 4
    .param p1, "base"    # Landroid/content/Context;

    .prologue
    .line 58
    invoke-super {p0, p1}, Landroid/app/Application;->attachBaseContext(Landroid/content/Context;)V

    .line 60
    invoke-static {p0}, Landroid/support/multidex/MultiDex;->install(Landroid/content/Context;)V

    .line 62
    sput-object p0, Lru/cn/tv/App;->INSTANCE:Lru/cn/tv/App;

    .line 63
    invoke-static {p0}, Ltoothpick/Toothpick;->openScope(Ljava/lang/Object;)Ltoothpick/Scope;

    move-result-object v0

    iput-object v0, p0, Lru/cn/tv/App;->scope:Ltoothpick/Scope;

    .line 64
    iget-object v0, p0, Lru/cn/tv/App;->scope:Ltoothpick/Scope;

    const/4 v1, 0x1

    new-array v1, v1, [Ltoothpick/config/Module;

    const/4 v2, 0x0

    new-instance v3, Lru/cn/tv/App$1;

    invoke-direct {v3, p0}, Lru/cn/tv/App$1;-><init>(Lru/cn/tv/App;)V

    aput-object v3, v1, v2

    invoke-interface {v0, v1}, Ltoothpick/Scope;->installModules([Ltoothpick/config/Module;)V

    .line 68
    return-void
.end method

.method public onCreate()V
    .locals 5

    .prologue
    .line 34
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 37
    :try_start_0
    invoke-virtual {p0}, Lru/cn/tv/App;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {p0}, Lru/cn/tv/App;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 38
    .local v1, "pi":Landroid/content/pm/PackageInfo;
    const-string v2, "PeersTVApp"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Application started, version "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 42
    .end local v1    # "pi":Landroid/content/pm/PackageInfo;
    :goto_0
    invoke-static {p0}, Lru/cn/utils/Utils;->init(Landroid/content/Context;)V

    .line 43
    invoke-static {p0}, Lru/cn/utils/http/HttpClient;->initialize(Landroid/content/Context;)V

    .line 44
    invoke-static {p0}, Lru/cn/utils/customization/Config;->setupRestrictions(Landroid/content/Context;)V

    .line 46
    invoke-direct {p0}, Lru/cn/tv/App;->isMainProcess()Z

    move-result v0

    .line 47
    .local v0, "isMainProcess":Z
    if-eqz v0, :cond_0

    .line 48
    invoke-static {p0}, Lru/cn/domain/PurchaseManager;->Init(Landroid/content/Context;)V

    .line 51
    :cond_0
    invoke-static {p0, v0}, Lru/cn/domain/statistics/AnalyticsManager;->initialize(Landroid/app/Application;Z)V

    .line 53
    invoke-direct {p0}, Lru/cn/tv/App;->bindDependencies()V

    .line 54
    return-void

    .line 39
    .end local v0    # "isMainProcess":Z
    :catch_0
    move-exception v2

    goto :goto_0
.end method
