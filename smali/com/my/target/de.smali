.class public final Lcom/my/target/de;
.super Ljava/lang/Object;
.source "StandardAdBannerParser.java"


# instance fields
.field private final adConfig:Lcom/my/target/b;

.field private final context:Landroid/content/Context;

.field private final h:Lcom/my/target/dh;

.field private final i:Lcom/my/target/ae;

.field private final j:Lcom/my/target/be;


# direct methods
.method private constructor <init>(Lcom/my/target/dh;Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/my/target/de;->h:Lcom/my/target/dh;

    .line 45
    iput-object p2, p0, Lcom/my/target/de;->i:Lcom/my/target/ae;

    .line 46
    iput-object p3, p0, Lcom/my/target/de;->adConfig:Lcom/my/target/b;

    .line 47
    iput-object p4, p0, Lcom/my/target/de;->context:Landroid/content/Context;

    .line 48
    invoke-static {p1, p2, p3, p4}, Lcom/my/target/be;->a(Lcom/my/target/ak;Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/be;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/de;->j:Lcom/my/target/be;

    .line 49
    return-void
.end method

.method public static a(Lcom/my/target/dh;Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/de;
    .locals 1

    .prologue
    .line 30
    new-instance v0, Lcom/my/target/de;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/my/target/de;-><init>(Lcom/my/target/dh;Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method public final a(Lorg/json/JSONObject;Lcom/my/target/core/models/banners/c;)Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x0

    .line 53
    iget-object v1, p0, Lcom/my/target/de;->j:Lcom/my/target/be;

    invoke-virtual {v1, p1, p2, v3}, Lcom/my/target/be;->a(Lorg/json/JSONObject;Lcom/my/target/ah;Lcom/my/target/af;)V

    .line 55
    invoke-virtual {p2}, Lcom/my/target/core/models/banners/c;->getType()Ljava/lang/String;

    move-result-object v1

    .line 56
    const-string v2, "teaser"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 58
    invoke-virtual {p2}, Lcom/my/target/core/models/banners/c;->getImage()Lcom/my/target/common/models/ImageData;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/my/target/core/models/banners/c;->setIcon(Lcom/my/target/common/models/ImageData;)V

    .line 59
    invoke-virtual {p2, v3}, Lcom/my/target/core/models/banners/c;->setImage(Lcom/my/target/common/models/ImageData;)V

    .line 61
    const-string v1, "mainImageLink"

    const-string v2, ""

    invoke-virtual {p1, v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 62
    const-string v2, "mainImageWidth"

    invoke-virtual {p1, v2, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v2

    .line 63
    const-string v3, "mainImageHeight"

    invoke-virtual {p1, v3, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    .line 64
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 66
    invoke-static {v1, v2, v0}, Lcom/my/target/common/models/ImageData;->newImageData(Ljava/lang/String;II)Lcom/my/target/common/models/ImageData;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/my/target/core/models/banners/c;->setImage(Lcom/my/target/common/models/ImageData;)V

    .line 107
    :cond_0
    invoke-virtual {p2}, Lcom/my/target/core/models/banners/c;->getTimeout()I

    move-result v0

    if-gtz v0, :cond_1

    .line 109
    iget-object v0, p0, Lcom/my/target/de;->h:Lcom/my/target/dh;

    invoke-virtual {v0}, Lcom/my/target/dh;->x()I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/my/target/core/models/banners/c;->setTimeout(I)V

    .line 112
    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 69
    :cond_2
    const-string v2, "html"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 72
    const-string v1, "source"

    invoke-virtual {p1, v1, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 73
    if-nez v1, :cond_3

    .line 75
    const-string v1, "Required field"

    invoke-static {v1}, Lcom/my/target/az;->y(Ljava/lang/String;)Lcom/my/target/az;

    move-result-object v1

    const-string v2, "Banner with type \'html\' has no source field"

    invoke-virtual {v1, v2}, Lcom/my/target/az;->z(Ljava/lang/String;)Lcom/my/target/az;

    move-result-object v1

    .line 76
    invoke-virtual {p2}, Lcom/my/target/core/models/banners/c;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/my/target/az;->B(Ljava/lang/String;)Lcom/my/target/az;

    move-result-object v1

    iget-object v2, p0, Lcom/my/target/de;->i:Lcom/my/target/ae;

    .line 77
    invoke-virtual {v2}, Lcom/my/target/ae;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/my/target/az;->A(Ljava/lang/String;)Lcom/my/target/az;

    move-result-object v1

    iget-object v2, p0, Lcom/my/target/de;->adConfig:Lcom/my/target/b;

    .line 78
    invoke-virtual {v2}, Lcom/my/target/b;->getSlotId()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/my/target/az;->h(I)Lcom/my/target/az;

    move-result-object v1

    iget-object v2, p0, Lcom/my/target/de;->context:Landroid/content/Context;

    .line 79
    invoke-virtual {v1, v2}, Lcom/my/target/az;->e(Landroid/content/Context;)V

    goto :goto_0

    .line 82
    :cond_3
    invoke-static {v1}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 83
    iget-object v2, p0, Lcom/my/target/de;->j:Lcom/my/target/be;

    invoke-virtual {v2, v1, p1}, Lcom/my/target/be;->a(Ljava/lang/String;Lorg/json/JSONObject;)Z

    move-result v1

    .line 84
    if-nez v1, :cond_0

    goto :goto_0

    .line 89
    :cond_4
    const-string v2, "banner"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 91
    invoke-virtual {p2}, Lcom/my/target/core/models/banners/c;->getImage()Lcom/my/target/common/models/ImageData;

    move-result-object v1

    if-nez v1, :cond_0

    .line 93
    const-string v1, "Required field"

    invoke-static {v1}, Lcom/my/target/az;->y(Ljava/lang/String;)Lcom/my/target/az;

    move-result-object v1

    const-string v2, "Banner with type \'banner\' has no image"

    invoke-virtual {v1, v2}, Lcom/my/target/az;->z(Ljava/lang/String;)Lcom/my/target/az;

    move-result-object v1

    .line 94
    invoke-virtual {p2}, Lcom/my/target/core/models/banners/c;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/my/target/az;->B(Ljava/lang/String;)Lcom/my/target/az;

    move-result-object v1

    iget-object v2, p0, Lcom/my/target/de;->i:Lcom/my/target/ae;

    .line 95
    invoke-virtual {v2}, Lcom/my/target/ae;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/my/target/az;->A(Ljava/lang/String;)Lcom/my/target/az;

    move-result-object v1

    iget-object v2, p0, Lcom/my/target/de;->adConfig:Lcom/my/target/b;

    .line 96
    invoke-virtual {v2}, Lcom/my/target/b;->getSlotId()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/my/target/az;->h(I)Lcom/my/target/az;

    move-result-object v1

    iget-object v2, p0, Lcom/my/target/de;->context:Landroid/content/Context;

    .line 97
    invoke-virtual {v1, v2}, Lcom/my/target/az;->e(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 103
    :cond_5
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown banner type: \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto/16 :goto_0
.end method
