.class Lru/inetra/bytefog/service/NetworkInterfaceInfo;
.super Ljava/lang/Object;
.source "NetworkInterfaceInfo.java"


# instance fields
.field private id:Ljava/lang/String;

.field private linkSpeed:J

.field private networkPrefixes:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private type:I

.field private unicastAddresses:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/net/Inet4Address;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lru/inetra/bytefog/service/NetworkInterfaceInfo;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getLinkSpeed()J
    .locals 2

    .prologue
    .line 45
    iget-wide v0, p0, Lru/inetra/bytefog/service/NetworkInterfaceInfo;->linkSpeed:J

    return-wide v0
.end method

.method public getNetworkPrefixes()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 40
    iget-object v0, p0, Lru/inetra/bytefog/service/NetworkInterfaceInfo;->networkPrefixes:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 25
    iget v0, p0, Lru/inetra/bytefog/service/NetworkInterfaceInfo;->type:I

    return v0
.end method

.method public getUnicastAddresses()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/net/Inet4Address;",
            ">;"
        }
    .end annotation

    .prologue
    .line 33
    iget-object v0, p0, Lru/inetra/bytefog/service/NetworkInterfaceInfo;->unicastAddresses:Ljava/util/ArrayList;

    return-object v0
.end method

.method public setId(Ljava/lang/String;)V
    .locals 0
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 21
    iput-object p1, p0, Lru/inetra/bytefog/service/NetworkInterfaceInfo;->id:Ljava/lang/String;

    .line 22
    return-void
.end method

.method public setLinkSpeed(J)V
    .locals 1
    .param p1, "linkSpeed"    # J

    .prologue
    .line 49
    iput-wide p1, p0, Lru/inetra/bytefog/service/NetworkInterfaceInfo;->linkSpeed:J

    .line 50
    return-void
.end method

.method public setNetworkPrefixes(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 42
    .local p1, "networkPrefixes":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    iput-object p1, p0, Lru/inetra/bytefog/service/NetworkInterfaceInfo;->networkPrefixes:Ljava/util/ArrayList;

    return-void
.end method

.method public setType(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 29
    iput p1, p0, Lru/inetra/bytefog/service/NetworkInterfaceInfo;->type:I

    .line 30
    return-void
.end method

.method public setUnicastAddresses(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/net/Inet4Address;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 37
    .local p1, "unicastAddresses":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/net/Inet4Address;>;"
    iput-object p1, p0, Lru/inetra/bytefog/service/NetworkInterfaceInfo;->unicastAddresses:Ljava/util/ArrayList;

    .line 38
    return-void
.end method
