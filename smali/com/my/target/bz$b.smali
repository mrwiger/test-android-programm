.class Lcom/my/target/bz$b;
.super Landroid/view/GestureDetector;
.source "MraidWebView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/my/target/bz;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/bz$b$a;
    }
.end annotation


# instance fields
.field private iU:Lcom/my/target/bz$b$a;

.field private final ih:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 159
    new-instance v0, Landroid/view/GestureDetector$SimpleOnGestureListener;

    invoke-direct {v0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    invoke-direct {p0, p1, p2, v0}, Lcom/my/target/bz$b;-><init>(Landroid/content/Context;Landroid/view/View;Landroid/view/GestureDetector$SimpleOnGestureListener;)V

    .line 160
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/view/View;Landroid/view/GestureDetector$SimpleOnGestureListener;)V
    .locals 1

    .prologue
    .line 164
    invoke-direct {p0, p1, p3}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    .line 166
    iput-object p2, p0, Lcom/my/target/bz$b;->ih:Landroid/view/View;

    .line 168
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/my/target/bz$b;->setIsLongpressEnabled(Z)V

    .line 169
    return-void
.end method

.method private a(Landroid/view/MotionEvent;Landroid/view/View;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x0

    .line 207
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 215
    :cond_0
    :goto_0
    return v0

    .line 212
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    .line 213
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    .line 215
    cmpl-float v3, v1, v4

    if-ltz v3, :cond_0

    invoke-virtual {p2}, Landroid/view/View;->getWidth()I

    move-result v3

    int-to-float v3, v3

    cmpg-float v1, v1, v3

    if-gtz v1, :cond_0

    cmpl-float v1, v2, v4

    if-ltz v1, :cond_0

    .line 216
    invoke-virtual {p2}, Landroid/view/View;->getHeight()I

    move-result v1

    int-to-float v1, v1

    cmpg-float v1, v2, v1

    if-gtz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/view/MotionEvent;)V
    .locals 1

    .prologue
    .line 173
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 198
    :cond_0
    :goto_0
    return-void

    .line 176
    :pswitch_0
    iget-object v0, p0, Lcom/my/target/bz$b;->iU:Lcom/my/target/bz$b$a;

    if-eqz v0, :cond_1

    .line 178
    const-string v0, "Gestures: user clicked"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 179
    iget-object v0, p0, Lcom/my/target/bz$b;->iU:Lcom/my/target/bz$b$a;

    invoke-interface {v0}, Lcom/my/target/bz$b$a;->ba()V

    goto :goto_0

    .line 183
    :cond_1
    const-string v0, "View\'s onUserClick() is not registered."

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 187
    :pswitch_1
    invoke-virtual {p0, p1}, Lcom/my/target/bz$b;->onTouchEvent(Landroid/view/MotionEvent;)Z

    goto :goto_0

    .line 190
    :pswitch_2
    iget-object v0, p0, Lcom/my/target/bz$b;->ih:Landroid/view/View;

    invoke-direct {p0, p1, v0}, Lcom/my/target/bz$b;->a(Landroid/view/MotionEvent;Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 192
    invoke-virtual {p0, p1}, Lcom/my/target/bz$b;->onTouchEvent(Landroid/view/MotionEvent;)Z

    goto :goto_0

    .line 173
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public a(Lcom/my/target/bz$b$a;)V
    .locals 0

    .prologue
    .line 202
    iput-object p1, p0, Lcom/my/target/bz$b;->iU:Lcom/my/target/bz$b$a;

    .line 203
    return-void
.end method
