.class public Lcom/my/target/bg;
.super Ljava/lang/Object;
.source "CommonVideoParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/bg$a;
    }
.end annotation


# instance fields
.field private aB:Ljava/lang/String;

.field private final adConfig:Lcom/my/target/b;

.field private final bJ:Landroid/content/Context;

.field private final eC:Lcom/my/target/ae;

.field private final fg:Lcom/my/target/ak;


# direct methods
.method private constructor <init>(Lcom/my/target/ak;Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object p1, p0, Lcom/my/target/bg;->fg:Lcom/my/target/ak;

    .line 55
    iput-object p2, p0, Lcom/my/target/bg;->eC:Lcom/my/target/ae;

    .line 56
    iput-object p3, p0, Lcom/my/target/bg;->adConfig:Lcom/my/target/b;

    .line 57
    iput-object p4, p0, Lcom/my/target/bg;->bJ:Landroid/content/Context;

    .line 58
    return-void
.end method

.method private a(Lorg/json/JSONObject;Lcom/my/target/aj;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            "Lcom/my/target/aj",
            "<",
            "Lcom/my/target/common/models/VideoData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 165
    invoke-direct {p0, p1, p2}, Lcom/my/target/bg;->b(Lorg/json/JSONObject;Lcom/my/target/aj;)V

    .line 168
    iget-object v0, p0, Lcom/my/target/bg;->eC:Lcom/my/target/ae;

    invoke-virtual {v0}, Lcom/my/target/ae;->J()Ljava/lang/Boolean;

    move-result-object v0

    .line 169
    if-eqz v0, :cond_0

    .line 171
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p2, v0}, Lcom/my/target/aj;->setAllowClose(Z)V

    .line 173
    :cond_0
    iget-object v0, p0, Lcom/my/target/bg;->eC:Lcom/my/target/ae;

    invoke-virtual {v0}, Lcom/my/target/ae;->K()Ljava/lang/Boolean;

    move-result-object v0

    .line 174
    if-eqz v0, :cond_1

    .line 176
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p2, v0}, Lcom/my/target/aj;->setAllowPause(Z)V

    .line 178
    :cond_1
    iget-object v0, p0, Lcom/my/target/bg;->eC:Lcom/my/target/ae;

    invoke-virtual {v0}, Lcom/my/target/ae;->getAllowCloseDelay()F

    move-result v0

    .line 179
    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_2

    .line 181
    invoke-virtual {p2, v0}, Lcom/my/target/aj;->setAllowCloseDelay(F)V

    .line 183
    :cond_2
    return-void
.end method

.method public static b(Lcom/my/target/ak;Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/bg;
    .locals 1

    .prologue
    .line 39
    new-instance v0, Lcom/my/target/bg;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/my/target/bg;-><init>(Lcom/my/target/ak;Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)V

    return-object v0
.end method

.method private b(Lorg/json/JSONObject;Lcom/my/target/aj;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            "Lcom/my/target/aj",
            "<",
            "Lcom/my/target/common/models/VideoData;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-wide/high16 v2, -0x4010000000000000L    # -1.0

    const-wide/16 v10, 0x0

    .line 188
    iget-object v0, p0, Lcom/my/target/bg;->eC:Lcom/my/target/ae;

    invoke-virtual {v0}, Lcom/my/target/ae;->getPoint()F

    move-result v0

    float-to-double v0, v0

    .line 189
    cmpg-double v4, v0, v10

    if-gez v4, :cond_0

    .line 191
    const-string v0, "point"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;)D

    move-result-wide v0

    .line 194
    :cond_0
    invoke-static {v0, v1}, Ljava/lang/Double;->isNaN(D)Z

    move-result v4

    if-eqz v4, :cond_4

    move-wide v0, v2

    .line 206
    :cond_1
    :goto_0
    iget-object v4, p0, Lcom/my/target/bg;->eC:Lcom/my/target/ae;

    invoke-virtual {v4}, Lcom/my/target/ae;->getPointP()F

    move-result v4

    float-to-double v4, v4

    .line 207
    cmpg-double v6, v4, v10

    if-gez v6, :cond_2

    .line 209
    const-string v4, "pointP"

    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;)D

    move-result-wide v4

    .line 212
    :cond_2
    invoke-static {v4, v5}, Ljava/lang/Double;->isNaN(D)Z

    move-result v6

    if-eqz v6, :cond_5

    move-wide v4, v2

    .line 223
    :cond_3
    :goto_1
    cmpg-double v6, v0, v10

    if-gez v6, :cond_6

    cmpg-double v6, v4, v10

    if-gez v6, :cond_6

    .line 226
    const-wide/high16 v0, 0x4049000000000000L    # 50.0

    .line 228
    :goto_2
    double-to-float v2, v2

    invoke-virtual {p2, v2}, Lcom/my/target/aj;->setPoint(F)V

    .line 229
    double-to-float v0, v0

    invoke-virtual {p2, v0}, Lcom/my/target/aj;->setPointP(F)V

    .line 230
    return-void

    .line 200
    :cond_4
    cmpg-double v4, v0, v10

    if-gez v4, :cond_1

    .line 202
    const-string v4, "Bad value"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Wrong value "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " for point"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v4, v5}, Lcom/my/target/bg;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 218
    :cond_5
    cmpg-double v6, v4, v10

    if-gez v6, :cond_3

    .line 220
    const-string v6, "Bad value"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Wrong value "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " for pointP"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v6, v7}, Lcom/my/target/bg;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_6
    move-wide v2, v0

    move-wide v0, v4

    goto :goto_2
.end method

.method private d(Lorg/json/JSONObject;)Lcom/my/target/common/models/VideoData;
    .locals 6

    .prologue
    .line 234
    const-string v0, "src"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 235
    const-string v1, "width"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v1

    .line 236
    const-string v2, "height"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v2

    .line 237
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    if-lez v1, :cond_0

    if-lez v2, :cond_0

    .line 239
    invoke-static {v0, v1, v2}, Lcom/my/target/common/models/VideoData;->newVideoData(Ljava/lang/String;II)Lcom/my/target/common/models/VideoData;

    move-result-object v0

    .line 240
    const-string v1, "bitrate"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/my/target/common/models/VideoData;->setBitrate(I)V

    .line 248
    :goto_0
    return-object v0

    .line 245
    :cond_0
    const-string v3, "Bad value"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "bad mediafile object, src = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ", width = "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", height = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v3, v0}, Lcom/my/target/bg;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 253
    invoke-static {p1}, Lcom/my/target/az;->y(Ljava/lang/String;)Lcom/my/target/az;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/my/target/az;->z(Ljava/lang/String;)Lcom/my/target/az;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/bg;->adConfig:Lcom/my/target/b;

    .line 254
    invoke-virtual {v1}, Lcom/my/target/b;->getSlotId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/my/target/az;->h(I)Lcom/my/target/az;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/bg;->aB:Ljava/lang/String;

    .line 255
    invoke-virtual {v0, v1}, Lcom/my/target/az;->B(Ljava/lang/String;)Lcom/my/target/az;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/bg;->eC:Lcom/my/target/ae;

    .line 256
    invoke-virtual {v1}, Lcom/my/target/ae;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/my/target/az;->A(Ljava/lang/String;)Lcom/my/target/az;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/bg;->bJ:Landroid/content/Context;

    .line 257
    invoke-virtual {v0, v1}, Lcom/my/target/az;->e(Landroid/content/Context;)V

    .line 258
    return-void
.end method


# virtual methods
.method public a(Lorg/json/JSONObject;Lcom/my/target/am;)V
    .locals 4

    .prologue
    .line 62
    const-string v0, "closeActionText"

    invoke-virtual {p2}, Lcom/my/target/am;->getCloseActionText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/my/target/am;->setCloseActionText(Ljava/lang/String;)V

    .line 63
    const-string v0, "replayActionText"

    invoke-virtual {p2}, Lcom/my/target/am;->getReplayActionText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/my/target/am;->setReplayActionText(Ljava/lang/String;)V

    .line 64
    const-string v0, "closeDelayActionText"

    invoke-virtual {p2}, Lcom/my/target/am;->getCloseDelayActionText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/my/target/am;->setCloseDelayActionText(Ljava/lang/String;)V

    .line 65
    const-string v0, "automute"

    invoke-virtual {p2}, Lcom/my/target/am;->isAutoMute()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {p2, v0}, Lcom/my/target/am;->setAutoMute(Z)V

    .line 66
    const-string v0, "showPlayerControls"

    invoke-virtual {p2}, Lcom/my/target/am;->isShowPlayerControls()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {p2, v0}, Lcom/my/target/am;->setShowPlayerControls(Z)V

    .line 67
    const-string v0, "allowClose"

    invoke-virtual {p2}, Lcom/my/target/am;->isAllowClose()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {p2, v0}, Lcom/my/target/am;->setAllowClose(Z)V

    .line 68
    const-string v0, "allowReplay"

    invoke-virtual {p2}, Lcom/my/target/am;->isAllowReplay()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {p2, v0}, Lcom/my/target/am;->setAllowReplay(Z)V

    .line 69
    const-string v0, "allowBackButton"

    invoke-virtual {p2}, Lcom/my/target/am;->isAllowBackButton()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {p2, v0}, Lcom/my/target/am;->setAllowBackButton(Z)V

    .line 70
    const-string v0, "allowCloseDelay"

    invoke-virtual {p2}, Lcom/my/target/am;->getAllowCloseDelay()F

    move-result v1

    float-to-double v2, v1

    invoke-virtual {p1, v0, v2, v3}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v0

    .line 71
    const-wide/16 v2, 0x0

    cmpl-double v2, v0, v2

    if-ltz v2, :cond_0

    .line 73
    double-to-float v0, v0

    invoke-virtual {p2, v0}, Lcom/my/target/am;->setAllowCloseDelay(F)V

    .line 75
    :cond_0
    return-void
.end method

.method public a(Lorg/json/JSONObject;Lcom/my/target/aj;Lcom/my/target/am;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            "Lcom/my/target/aj",
            "<",
            "Lcom/my/target/common/models/VideoData;",
            ">;",
            "Lcom/my/target/am;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 81
    iget-object v0, p0, Lcom/my/target/bg;->fg:Lcom/my/target/ak;

    iget-object v1, p0, Lcom/my/target/bg;->eC:Lcom/my/target/ae;

    iget-object v5, p0, Lcom/my/target/bg;->adConfig:Lcom/my/target/b;

    iget-object v6, p0, Lcom/my/target/bg;->bJ:Landroid/content/Context;

    invoke-static {v0, v1, v5, v6}, Lcom/my/target/be;->a(Lcom/my/target/ak;Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/be;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/bg;->fg:Lcom/my/target/ak;

    invoke-virtual {v1}, Lcom/my/target/ak;->getClickArea()Lcom/my/target/af;

    move-result-object v1

    invoke-virtual {v0, p1, p2, v1}, Lcom/my/target/be;->a(Lorg/json/JSONObject;Lcom/my/target/ah;Lcom/my/target/af;)V

    .line 82
    const-string v0, "statistics"

    invoke-virtual {p2}, Lcom/my/target/aj;->getType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v3, v2

    .line 160
    :cond_0
    :goto_0
    return v3

    .line 86
    :cond_1
    invoke-virtual {p2}, Lcom/my/target/aj;->getId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/bg;->aB:Ljava/lang/String;

    .line 87
    invoke-virtual {p2}, Lcom/my/target/aj;->getDuration()F

    move-result v0

    .line 88
    cmpg-float v1, v0, v4

    if-gtz v1, :cond_2

    .line 90
    const-string v1, "Bad value"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "wrong videoBanner duration "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lcom/my/target/bg;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 94
    :cond_2
    const-string v1, "Close"

    .line 95
    if-eqz p3, :cond_10

    .line 97
    invoke-virtual {p3}, Lcom/my/target/am;->getCloseActionText()Ljava/lang/String;

    move-result-object v0

    .line 98
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_10

    .line 103
    :goto_1
    const-string v1, "closeActionText"

    invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 105
    invoke-virtual {p2, v0}, Lcom/my/target/aj;->setCloseActionText(Ljava/lang/String;)V

    .line 106
    const-string v1, "replayActionText"

    if-eqz p3, :cond_5

    invoke-virtual {p3}, Lcom/my/target/am;->getReplayActionText()Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/my/target/aj;->setReplayActionText(Ljava/lang/String;)V

    .line 107
    const-string v1, "closeDelayActionText"

    if-eqz p3, :cond_6

    invoke-virtual {p3}, Lcom/my/target/am;->getCloseDelayActionText()Ljava/lang/String;

    move-result-object v0

    :goto_3
    invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/my/target/aj;->setCloseDelayActionText(Ljava/lang/String;)V

    .line 108
    const-string v1, "allowReplay"

    if-eqz p3, :cond_7

    invoke-virtual {p3}, Lcom/my/target/am;->isAllowReplay()Z

    move-result v0

    :goto_4
    invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {p2, v0}, Lcom/my/target/aj;->setAllowReplay(Z)V

    .line 109
    const-string v1, "automute"

    if-eqz p3, :cond_8

    invoke-virtual {p3}, Lcom/my/target/am;->isAutoMute()Z

    move-result v0

    :goto_5
    invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {p2, v0}, Lcom/my/target/aj;->setAutoMute(Z)V

    .line 110
    const-string v1, "allowBackButton"

    if-eqz p3, :cond_9

    invoke-virtual {p3}, Lcom/my/target/am;->isAllowBackButton()Z

    move-result v0

    :goto_6
    invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {p2, v0}, Lcom/my/target/aj;->setAllowBackButton(Z)V

    .line 111
    const-string v1, "allowClose"

    if-eqz p3, :cond_a

    invoke-virtual {p3}, Lcom/my/target/am;->isAllowClose()Z

    move-result v0

    :goto_7
    invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {p2, v0}, Lcom/my/target/aj;->setAllowClose(Z)V

    .line 112
    const-string v1, "allowCloseDelay"

    if-eqz p3, :cond_b

    invoke-virtual {p3}, Lcom/my/target/am;->getAllowCloseDelay()F

    move-result v0

    :goto_8
    float-to-double v4, v0

    invoke-virtual {p1, v1, v4, v5}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v0

    double-to-float v0, v0

    invoke-virtual {p2, v0}, Lcom/my/target/aj;->setAllowCloseDelay(F)V

    .line 113
    if-eqz p3, :cond_c

    invoke-virtual {p3}, Lcom/my/target/am;->isShowPlayerControls()Z

    move-result v0

    :goto_9
    invoke-virtual {p2, v0}, Lcom/my/target/aj;->setShowPlayerControls(Z)V

    .line 114
    const-string v0, "autoplay"

    invoke-virtual {p2}, Lcom/my/target/aj;->isAutoPlay()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {p2, v0}, Lcom/my/target/aj;->setAutoPlay(Z)V

    .line 115
    const-string v0, "hasCtaButton"

    invoke-virtual {p2}, Lcom/my/target/aj;->isHasCtaButton()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {p2, v0}, Lcom/my/target/aj;->setHasCtaButton(Z)V

    .line 116
    const-string v0, "hasPause"

    invoke-virtual {p2}, Lcom/my/target/aj;->isAllowPause()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {p2, v0}, Lcom/my/target/aj;->setAllowPause(Z)V

    .line 118
    const-string v0, "previewLink"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 119
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 121
    const-string v1, "previewWidth"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v1

    .line 122
    const-string v4, "previewHeight"

    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v4

    .line 123
    invoke-static {v0, v1, v4}, Lcom/my/target/common/models/ImageData;->newImageData(Ljava/lang/String;II)Lcom/my/target/common/models/ImageData;

    move-result-object v0

    .line 124
    invoke-virtual {p2, v0}, Lcom/my/target/aj;->setPreview(Lcom/my/target/common/models/ImageData;)V

    .line 127
    :cond_3
    const-string v0, "mediafiles"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    .line 128
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-nez v0, :cond_d

    .line 130
    :cond_4
    const-string v0, "mediafiles array is empty"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 131
    const-string v0, "Required field"

    const-string v1, "unable to find mediaFiles in MediaBanner"

    invoke-direct {p0, v0, v1}, Lcom/my/target/bg;->f(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 106
    :cond_5
    const-string v0, "Replay"

    goto/16 :goto_2

    .line 107
    :cond_6
    const-string v0, "Ad can be skipped after %ds"

    goto/16 :goto_3

    :cond_7
    move v0, v2

    .line 108
    goto/16 :goto_4

    :cond_8
    move v0, v3

    .line 109
    goto/16 :goto_5

    :cond_9
    move v0, v2

    .line 110
    goto/16 :goto_6

    :cond_a
    move v0, v2

    .line 111
    goto/16 :goto_7

    :cond_b
    move v0, v4

    .line 112
    goto/16 :goto_8

    :cond_c
    move v0, v2

    .line 113
    goto :goto_9

    .line 135
    :cond_d
    invoke-direct {p0, p1, p2}, Lcom/my/target/bg;->a(Lorg/json/JSONObject;Lcom/my/target/aj;)V

    .line 137
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 138
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v5

    move v0, v3

    .line 139
    :goto_a
    if-ge v0, v5, :cond_f

    .line 141
    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    .line 142
    if-eqz v6, :cond_e

    .line 144
    invoke-direct {p0, v6}, Lcom/my/target/bg;->d(Lorg/json/JSONObject;)Lcom/my/target/common/models/VideoData;

    move-result-object v6

    .line 145
    if-eqz v6, :cond_e

    .line 147
    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 139
    :cond_e
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 151
    :cond_f
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/my/target/bg;->adConfig:Lcom/my/target/b;

    invoke-virtual {v0}, Lcom/my/target/b;->getVideoQuality()I

    move-result v0

    invoke-static {v4, v0}, Lcom/my/target/common/models/VideoData;->chooseBest(Ljava/util/List;I)Lcom/my/target/common/models/VideoData;

    move-result-object v0

    .line 154
    if-eqz v0, :cond_0

    .line 156
    invoke-virtual {p2, v0}, Lcom/my/target/aj;->setMediaData(Lcom/my/target/ag;)V

    move v3, v2

    .line 157
    goto/16 :goto_0

    :cond_10
    move-object v0, v1

    goto/16 :goto_1
.end method
