.class public Lru/cn/domain/Collections;
.super Ljava/lang/Object;
.source "Collections.java"


# instance fields
.field private final loader:Lru/cn/mvvm/RxLoader;


# direct methods
.method constructor <init>(Lru/cn/mvvm/RxLoader;)V
    .locals 0
    .param p1, "loader"    # Lru/cn/mvvm/RxLoader;

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lru/cn/domain/Collections;->loader:Lru/cn/mvvm/RxLoader;

    .line 26
    return-void
.end method

.method static final synthetic lambda$mapRubrics$2$Collections(Lru/cn/api/catalogue/replies/Rubric$UiHintType;Lru/cn/api/catalogue/replies/Rubric;)Z
    .locals 1
    .param p0, "hint"    # Lru/cn/api/catalogue/replies/Rubric$UiHintType;
    .param p1, "rubric"    # Lru/cn/api/catalogue/replies/Rubric;

    .prologue
    .line 84
    iget-object v0, p1, Lru/cn/api/catalogue/replies/Rubric;->uiHint:Lru/cn/api/catalogue/replies/Rubric$UiHintType;

    if-ne v0, p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static final synthetic lambda$rubric$1$Collections(Landroid/database/Cursor;)Lru/cn/api/catalogue/replies/Rubric;
    .locals 3
    .param p0, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 47
    invoke-interface {p0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 48
    invoke-interface {p0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 49
    const-string v1, "data"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 50
    .local v0, "d":Ljava/lang/String;
    invoke-static {v0}, Lru/cn/api/catalogue/replies/Rubric;->fromJson(Ljava/lang/String;)Lru/cn/api/catalogue/replies/Rubric;

    move-result-object v1

    return-object v1

    .line 53
    .end local v0    # "d":Ljava/lang/String;
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Rubric cursor is empty"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private mapRubricator(Landroid/database/Cursor;)Lru/cn/api/catalogue/replies/Rubricator;
    .locals 3
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 73
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    const/4 v2, 0x1

    if-ge v1, v2, :cond_0

    .line 74
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Rubrics is not available"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 77
    :cond_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 78
    const-string v1, "data"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 79
    .local v0, "s":Ljava/lang/String;
    invoke-static {v0}, Lru/cn/api/catalogue/replies/Rubricator;->fromJson(Ljava/lang/String;)Lru/cn/api/catalogue/replies/Rubricator;

    move-result-object v1

    return-object v1
.end method

.method private mapRubrics(Lru/cn/api/catalogue/replies/Rubricator;Lru/cn/api/catalogue/replies/Rubric$UiHintType;)Ljava/util/List;
    .locals 2
    .param p1, "rubricator"    # Lru/cn/api/catalogue/replies/Rubricator;
    .param p2, "hint"    # Lru/cn/api/catalogue/replies/Rubric$UiHintType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lru/cn/api/catalogue/replies/Rubricator;",
            "Lru/cn/api/catalogue/replies/Rubric$UiHintType;",
            ")",
            "Ljava/util/List",
            "<",
            "Lru/cn/api/catalogue/replies/Rubric;",
            ">;"
        }
    .end annotation

    .prologue
    .line 83
    iget-object v0, p1, Lru/cn/api/catalogue/replies/Rubricator;->rubrics:Ljava/util/List;

    invoke-static {v0}, Lcom/annimon/stream/Stream;->of(Ljava/lang/Iterable;)Lcom/annimon/stream/Stream;

    move-result-object v0

    new-instance v1, Lru/cn/domain/Collections$$Lambda$3;

    invoke-direct {v1, p2}, Lru/cn/domain/Collections$$Lambda$3;-><init>(Lru/cn/api/catalogue/replies/Rubric$UiHintType;)V

    .line 84
    invoke-virtual {v0, v1}, Lcom/annimon/stream/Stream;->filter(Lcom/annimon/stream/function/Predicate;)Lcom/annimon/stream/Stream;

    move-result-object v0

    sget-object v1, Lru/cn/domain/Collections$$Lambda$4;->$instance:Lcom/annimon/stream/function/Function;

    .line 85
    invoke-virtual {v0, v1}, Lcom/annimon/stream/Stream;->sortBy(Lcom/annimon/stream/function/Function;)Lcom/annimon/stream/Stream;

    move-result-object v0

    .line 86
    invoke-virtual {v0}, Lcom/annimon/stream/Stream;->toList()Ljava/util/List;

    move-result-object v0

    .line 83
    return-object v0
.end method

.method public static sortPredicate(Lru/cn/api/catalogue/replies/Rubric;)I
    .locals 4
    .param p0, "rubric"    # Lru/cn/api/catalogue/replies/Rubric;

    .prologue
    .line 59
    iget-object v0, p0, Lru/cn/api/catalogue/replies/Rubric;->uiHint:Lru/cn/api/catalogue/replies/Rubric$UiHintType;

    sget-object v1, Lru/cn/api/catalogue/replies/Rubric$UiHintType;->TOP:Lru/cn/api/catalogue/replies/Rubric$UiHintType;

    if-ne v0, v1, :cond_0

    .line 60
    const/4 v0, -0x3

    .line 69
    :goto_0
    return v0

    .line 62
    :cond_0
    iget-wide v0, p0, Lru/cn/api/catalogue/replies/Rubric;->id:J

    const-wide/32 v2, 0x25a285a

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 63
    const/4 v0, -0x2

    goto :goto_0

    .line 65
    :cond_1
    iget-wide v0, p0, Lru/cn/api/catalogue/replies/Rubric;->id:J

    const-wide/32 v2, 0x4b5008a

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    iget-wide v0, p0, Lru/cn/api/catalogue/replies/Rubric;->id:J

    const-wide/32 v2, 0x2d4476b

    cmp-long v0, v0, v2

    if-nez v0, :cond_3

    .line 67
    :cond_2
    const/4 v0, -0x1

    goto :goto_0

    .line 69
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method final bridge synthetic bridge$lambda$0$Collections(Landroid/database/Cursor;)Lru/cn/api/catalogue/replies/Rubricator;
    .locals 1

    invoke-direct {p0, p1}, Lru/cn/domain/Collections;->mapRubricator(Landroid/database/Cursor;)Lru/cn/api/catalogue/replies/Rubricator;

    move-result-object v0

    return-object v0
.end method

.method final synthetic lambda$rubrics$0$Collections(Lru/cn/api/catalogue/replies/Rubric$UiHintType;Lru/cn/api/catalogue/replies/Rubricator;)Ljava/util/List;
    .locals 1
    .param p1, "hint"    # Lru/cn/api/catalogue/replies/Rubric$UiHintType;
    .param p2, "rubricator"    # Lru/cn/api/catalogue/replies/Rubricator;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0, p2, p1}, Lru/cn/domain/Collections;->mapRubrics(Lru/cn/api/catalogue/replies/Rubricator;Lru/cn/api/catalogue/replies/Rubric$UiHintType;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public rubric(J)Lio/reactivex/Observable;
    .locals 3
    .param p1, "rubricId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lio/reactivex/Observable",
            "<",
            "Lru/cn/api/catalogue/replies/Rubric;",
            ">;"
        }
    .end annotation

    .prologue
    .line 42
    invoke-static {p1, p2}, Lru/cn/api/provider/TvContentProviderContract;->rubricatorUri(J)Landroid/net/Uri;

    move-result-object v0

    .line 44
    .local v0, "uri":Landroid/net/Uri;
    iget-object v1, p0, Lru/cn/domain/Collections;->loader:Lru/cn/mvvm/RxLoader;

    invoke-virtual {v1, v0}, Lru/cn/mvvm/RxLoader;->query(Landroid/net/Uri;)Lio/reactivex/Observable;

    move-result-object v1

    .line 45
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lru/cn/domain/Collections$$Lambda$2;->$instance:Lio/reactivex/functions/Function;

    .line 46
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v1

    .line 44
    return-object v1
.end method

.method public rubricator()Lio/reactivex/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Lru/cn/api/catalogue/replies/Rubricator;",
            ">;"
        }
    .end annotation

    .prologue
    .line 34
    invoke-static {}, Lru/cn/api/provider/TvContentProviderContract;->rubricatorUri()Landroid/net/Uri;

    move-result-object v0

    .line 36
    .local v0, "uri":Landroid/net/Uri;
    iget-object v1, p0, Lru/cn/domain/Collections;->loader:Lru/cn/mvvm/RxLoader;

    invoke-virtual {v1, v0}, Lru/cn/mvvm/RxLoader;->query(Landroid/net/Uri;)Lio/reactivex/Observable;

    move-result-object v1

    new-instance v2, Lru/cn/domain/Collections$$Lambda$1;

    invoke-direct {v2, p0}, Lru/cn/domain/Collections$$Lambda$1;-><init>(Lru/cn/domain/Collections;)V

    .line 37
    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v1

    .line 38
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    .line 36
    return-object v1
.end method

.method public rubrics(Lru/cn/api/catalogue/replies/Rubric$UiHintType;)Lio/reactivex/Observable;
    .locals 2
    .param p1, "hint"    # Lru/cn/api/catalogue/replies/Rubric$UiHintType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lru/cn/api/catalogue/replies/Rubric$UiHintType;",
            ")",
            "Lio/reactivex/Observable",
            "<",
            "Ljava/util/List",
            "<",
            "Lru/cn/api/catalogue/replies/Rubric;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 29
    invoke-virtual {p0}, Lru/cn/domain/Collections;->rubricator()Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lru/cn/domain/Collections$$Lambda$0;

    invoke-direct {v1, p0, p1}, Lru/cn/domain/Collections$$Lambda$0;-><init>(Lru/cn/domain/Collections;Lru/cn/api/catalogue/replies/Rubric$UiHintType;)V

    .line 30
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 29
    return-object v0
.end method
