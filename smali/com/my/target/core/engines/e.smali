.class public final Lcom/my/target/core/engines/e;
.super Lcom/my/target/core/engines/c;
.source "InterstitialAdImageEngine.java"


# instance fields
.field private final h:Lcom/my/target/core/models/banners/g;


# direct methods
.method private constructor <init>(Lcom/my/target/ads/InterstitialAd;Lcom/my/target/core/models/banners/g;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/my/target/core/engines/c;-><init>(Lcom/my/target/ads/InterstitialAd;)V

    .line 39
    iput-object p2, p0, Lcom/my/target/core/engines/e;->h:Lcom/my/target/core/models/banners/g;

    .line 40
    return-void
.end method

.method static a(Lcom/my/target/ads/InterstitialAd;Lcom/my/target/core/models/banners/g;)Lcom/my/target/core/engines/e;
    .locals 1

    .prologue
    .line 31
    new-instance v0, Lcom/my/target/core/engines/e;

    invoke-direct {v0, p0, p1}, Lcom/my/target/core/engines/e;-><init>(Lcom/my/target/ads/InterstitialAd;Lcom/my/target/core/models/banners/g;)V

    return-object v0
.end method

.method static synthetic a(Lcom/my/target/core/engines/e;)Lcom/my/target/core/models/banners/g;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/my/target/core/engines/e;->h:Lcom/my/target/core/models/banners/g;

    return-object v0
.end method

.method private a(Landroid/view/ViewGroup;)V
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 58
    new-instance v0, Lcom/my/target/dx;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/my/target/dx;-><init>(Landroid/content/Context;)V

    .line 59
    iget-object v1, p0, Lcom/my/target/core/engines/e;->h:Lcom/my/target/core/models/banners/g;

    invoke-virtual {v1}, Lcom/my/target/core/models/banners/g;->getOptimalLandscapeImage()Lcom/my/target/common/models/ImageData;

    move-result-object v1

    iget-object v2, p0, Lcom/my/target/core/engines/e;->h:Lcom/my/target/core/models/banners/g;

    .line 60
    invoke-virtual {v2}, Lcom/my/target/core/models/banners/g;->getOptimalPortraitImage()Lcom/my/target/common/models/ImageData;

    move-result-object v2

    iget-object v3, p0, Lcom/my/target/core/engines/e;->h:Lcom/my/target/core/models/banners/g;

    .line 61
    invoke-virtual {v3}, Lcom/my/target/core/models/banners/g;->getCloseIcon()Lcom/my/target/common/models/ImageData;

    move-result-object v3

    .line 59
    invoke-virtual {v0, v1, v2, v3}, Lcom/my/target/dx;->a(Lcom/my/target/common/models/ImageData;Lcom/my/target/common/models/ImageData;Lcom/my/target/common/models/ImageData;)V

    .line 62
    iget-object v1, p0, Lcom/my/target/core/engines/e;->h:Lcom/my/target/core/models/banners/g;

    invoke-virtual {v1}, Lcom/my/target/core/models/banners/g;->getAgeRestrictions()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/my/target/dx;->setAgeRestrictions(Ljava/lang/String;)V

    .line 63
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 64
    invoke-virtual {p1, v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 66
    invoke-virtual {v0}, Lcom/my/target/dx;->getImageView()Landroid/widget/ImageView;

    move-result-object v1

    new-instance v2, Lcom/my/target/core/engines/e$1;

    invoke-direct {v2, p0}, Lcom/my/target/core/engines/e$1;-><init>(Lcom/my/target/core/engines/e;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 82
    invoke-virtual {v0}, Lcom/my/target/dx;->getCloseButton()Lcom/my/target/by;

    move-result-object v0

    new-instance v1, Lcom/my/target/core/engines/e$2;

    invoke-direct {v1, p0}, Lcom/my/target/core/engines/e$2;-><init>(Lcom/my/target/core/engines/e;)V

    invoke-virtual {v0, v1}, Lcom/my/target/by;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 91
    iget-object v0, p0, Lcom/my/target/core/engines/e;->h:Lcom/my/target/core/models/banners/g;

    invoke-virtual {v0}, Lcom/my/target/core/models/banners/g;->getStatHolder()Lcom/my/target/ar;

    move-result-object v0

    const-string v1, "playbackStarted"

    invoke-virtual {v0, v1}, Lcom/my/target/ar;->w(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/my/target/cl;->a(Ljava/util/List;Landroid/content/Context;)V

    .line 92
    return-void
.end method


# virtual methods
.method public final a(Lcom/my/target/br;Landroid/widget/FrameLayout;)V
    .locals 0
    .param p1, "dialog"    # Lcom/my/target/br;
    .param p2, "rootLayout"    # Landroid/widget/FrameLayout;

    .prologue
    .line 45
    invoke-super {p0, p1, p2}, Lcom/my/target/core/engines/c;->a(Lcom/my/target/br;Landroid/widget/FrameLayout;)V

    .line 46
    invoke-direct {p0, p2}, Lcom/my/target/core/engines/e;->a(Landroid/view/ViewGroup;)V

    .line 47
    return-void
.end method

.method public final onActivityCreate(Lcom/my/target/common/MyTargetActivity;Landroid/content/Intent;Landroid/widget/FrameLayout;)V
    .locals 0
    .param p1, "activity"    # Lcom/my/target/common/MyTargetActivity;
    .param p2, "intent"    # Landroid/content/Intent;
    .param p3, "rootLayout"    # Landroid/widget/FrameLayout;

    .prologue
    .line 52
    invoke-super {p0, p1, p2, p3}, Lcom/my/target/core/engines/c;->onActivityCreate(Lcom/my/target/common/MyTargetActivity;Landroid/content/Intent;Landroid/widget/FrameLayout;)V

    .line 53
    invoke-direct {p0, p3}, Lcom/my/target/core/engines/e;->a(Landroid/view/ViewGroup;)V

    .line 54
    return-void
.end method
