.class public Lcom/google/obf/bp;
.super Ljava/lang/Object;
.source "IMASDK"

# interfaces
.implements Lcom/google/obf/aj;


# instance fields
.field private a:Lcom/google/obf/bt;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/obf/ak;Lcom/google/obf/ao;)I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/obf/bp;->a:Lcom/google/obf/bt;

    invoke-virtual {v0, p1, p2}, Lcom/google/obf/bt;->a(Lcom/google/obf/ak;Lcom/google/obf/ao;)I

    move-result v0

    return v0
.end method

.method public a(Lcom/google/obf/al;)V
    .locals 2

    .prologue
    .line 19
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Lcom/google/obf/al;->d(I)Lcom/google/obf/ar;

    move-result-object v0

    .line 20
    invoke-interface {p1}, Lcom/google/obf/al;->f()V

    .line 21
    iget-object v1, p0, Lcom/google/obf/bp;->a:Lcom/google/obf/bt;

    invoke-virtual {v1, p1, v0}, Lcom/google/obf/bt;->a(Lcom/google/obf/al;Lcom/google/obf/ar;)V

    .line 22
    return-void
.end method

.method public a(Lcom/google/obf/ak;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x7

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2
    :try_start_0
    new-instance v2, Lcom/google/obf/dw;

    const/16 v3, 0x1b

    new-array v3, v3, [B

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lcom/google/obf/dw;-><init>([BI)V

    .line 3
    new-instance v3, Lcom/google/obf/bs$b;

    invoke-direct {v3}, Lcom/google/obf/bs$b;-><init>()V

    .line 4
    const/4 v4, 0x1

    invoke-static {p1, v3, v2, v4}, Lcom/google/obf/bs;->a(Lcom/google/obf/ak;Lcom/google/obf/bs$b;Lcom/google/obf/dw;Z)Z

    move-result v4

    if-eqz v4, :cond_0

    iget v4, v3, Lcom/google/obf/bs$b;->b:I

    and-int/lit8 v4, v4, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    iget v3, v3, Lcom/google/obf/bs$b;->i:I

    if-ge v3, v6, :cond_1

    .line 18
    :cond_0
    :goto_0
    return v0

    .line 6
    :cond_1
    invoke-virtual {v2}, Lcom/google/obf/dw;->a()V

    .line 7
    iget-object v3, v2, Lcom/google/obf/dw;->a:[B

    const/4 v4, 0x0

    const/4 v5, 0x7

    invoke-interface {p1, v3, v4, v5}, Lcom/google/obf/ak;->c([BII)V

    .line 8
    invoke-static {v2}, Lcom/google/obf/bo;->a(Lcom/google/obf/dw;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 9
    new-instance v2, Lcom/google/obf/bo;

    invoke-direct {v2}, Lcom/google/obf/bo;-><init>()V

    iput-object v2, p0, Lcom/google/obf/bp;->a:Lcom/google/obf/bt;

    :goto_1
    move v0, v1

    .line 14
    goto :goto_0

    .line 10
    :cond_2
    invoke-virtual {v2}, Lcom/google/obf/dw;->a()V

    .line 11
    invoke-static {v2}, Lcom/google/obf/bv;->a(Lcom/google/obf/dw;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 12
    new-instance v2, Lcom/google/obf/bv;

    invoke-direct {v2}, Lcom/google/obf/bv;-><init>()V

    iput-object v2, p0, Lcom/google/obf/bp;->a:Lcom/google/obf/bt;
    :try_end_0
    .catch Lcom/google/obf/s; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 15
    :catch_0
    move-exception v1

    goto :goto_0

    .line 17
    :catchall_0
    move-exception v0

    throw v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/obf/bp;->a:Lcom/google/obf/bt;

    invoke-virtual {v0}, Lcom/google/obf/bt;->b()V

    .line 24
    return-void
.end method

.method public c()V
    .locals 0

    .prologue
    .line 25
    return-void
.end method
