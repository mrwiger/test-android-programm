.class public final Lcom/my/target/core/engines/f;
.super Lcom/my/target/core/engines/c;
.source "InterstitialAdPromoEngine.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/core/engines/f$a;
    }
.end annotation


# instance fields
.field private final e:Lcom/my/target/dv;

.field private final j:Lcom/my/target/core/models/banners/h;

.field private k:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/my/target/core/presenters/f;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/my/target/ads/InterstitialAd;Lcom/my/target/core/models/banners/h;Lcom/my/target/dv;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/my/target/core/engines/c;-><init>(Lcom/my/target/ads/InterstitialAd;)V

    .line 50
    iput-object p2, p0, Lcom/my/target/core/engines/f;->j:Lcom/my/target/core/models/banners/h;

    .line 51
    iput-object p3, p0, Lcom/my/target/core/engines/f;->e:Lcom/my/target/dv;

    .line 52
    return-void
.end method

.method public static a(Lcom/my/target/ads/InterstitialAd;Lcom/my/target/core/models/banners/h;Lcom/my/target/dv;)Lcom/my/target/core/engines/f;
    .locals 1

    .prologue
    .line 38
    new-instance v0, Lcom/my/target/core/engines/f;

    invoke-direct {v0, p0, p1, p2}, Lcom/my/target/core/engines/f;-><init>(Lcom/my/target/ads/InterstitialAd;Lcom/my/target/core/models/banners/h;Lcom/my/target/dv;)V

    return-object v0
.end method

.method static synthetic a(Lcom/my/target/core/engines/f;)Ljava/lang/ref/WeakReference;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/my/target/core/engines/f;->k:Ljava/lang/ref/WeakReference;

    return-object v0
.end method

.method private a(Landroid/view/ViewGroup;Z)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 179
    iget-object v0, p0, Lcom/my/target/core/engines/f;->j:Lcom/my/target/core/models/banners/h;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/my/target/core/presenters/f;->a(Lcom/my/target/core/models/banners/h;Landroid/content/Context;)Lcom/my/target/core/presenters/f;

    move-result-object v0

    .line 180
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/my/target/core/engines/f;->k:Ljava/lang/ref/WeakReference;

    .line 181
    invoke-virtual {v0, p2}, Lcom/my/target/core/presenters/f;->c(Z)V

    .line 182
    new-instance v1, Lcom/my/target/core/engines/f$a;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/my/target/core/engines/f$a;-><init>(Lcom/my/target/core/engines/f;B)V

    invoke-virtual {v0, v1}, Lcom/my/target/core/presenters/f;->a(Lcom/my/target/core/presenters/f$a;)V

    .line 183
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 184
    invoke-virtual {v0}, Lcom/my/target/core/presenters/f;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1, v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 186
    iget-object v0, p0, Lcom/my/target/core/engines/f;->j:Lcom/my/target/core/models/banners/h;

    invoke-virtual {v0}, Lcom/my/target/core/models/banners/h;->getStatHolder()Lcom/my/target/ar;

    move-result-object v0

    const-string v1, "playbackStarted"

    invoke-virtual {v0, v1}, Lcom/my/target/ar;->w(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/my/target/cl;->a(Ljava/util/List;Landroid/content/Context;)V

    .line 187
    iget-object v0, p0, Lcom/my/target/core/engines/f;->e:Lcom/my/target/dv;

    const-string v1, "impression"

    invoke-virtual {v0, v1}, Lcom/my/target/dv;->p(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/my/target/cl;->a(Ljava/util/List;Landroid/content/Context;)V

    .line 188
    return-void
.end method

.method static synthetic b(Lcom/my/target/core/engines/f;)Lcom/my/target/dv;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/my/target/core/engines/f;->e:Lcom/my/target/dv;

    return-object v0
.end method

.method static synthetic c(Lcom/my/target/core/engines/f;)Lcom/my/target/core/models/banners/h;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/my/target/core/engines/f;->j:Lcom/my/target/core/models/banners/h;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/my/target/br;Landroid/widget/FrameLayout;)V
    .locals 1
    .param p1, "dialog"    # Lcom/my/target/br;
    .param p2, "rootLayout"    # Landroid/widget/FrameLayout;

    .prologue
    .line 57
    invoke-super {p0, p1, p2}, Lcom/my/target/core/engines/c;->a(Lcom/my/target/br;Landroid/widget/FrameLayout;)V

    .line 58
    const/4 v0, 0x1

    invoke-direct {p0, p2, v0}, Lcom/my/target/core/engines/f;->a(Landroid/view/ViewGroup;Z)V

    .line 59
    return-void
.end method

.method public final aU()V
    .locals 1

    .prologue
    .line 85
    invoke-super {p0}, Lcom/my/target/core/engines/c;->aU()V

    .line 86
    iget-object v0, p0, Lcom/my/target/core/engines/f;->k:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 88
    iget-object v0, p0, Lcom/my/target/core/engines/f;->k:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/core/presenters/f;

    .line 89
    if-eqz v0, :cond_0

    .line 91
    invoke-virtual {v0}, Lcom/my/target/core/presenters/f;->destroy()V

    .line 94
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/my/target/core/engines/f;->k:Ljava/lang/ref/WeakReference;

    .line 95
    return-void
.end method

.method public final i(Z)V
    .locals 1
    .param p1, "hasFocus"    # Z

    .prologue
    .line 64
    invoke-super {p0, p1}, Lcom/my/target/core/engines/c;->i(Z)V

    .line 65
    iget-object v0, p0, Lcom/my/target/core/engines/f;->k:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/my/target/core/engines/f;->k:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/core/presenters/f;

    .line 68
    if-eqz v0, :cond_0

    .line 70
    if-eqz p1, :cond_1

    .line 72
    invoke-virtual {v0}, Lcom/my/target/core/presenters/f;->resume()V

    .line 80
    :cond_0
    :goto_0
    return-void

    .line 76
    :cond_1
    invoke-virtual {v0}, Lcom/my/target/core/presenters/f;->pause()V

    goto :goto_0
.end method

.method public final onActivityBackPressed()Z
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/my/target/core/engines/f;->k:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 168
    iget-object v0, p0, Lcom/my/target/core/engines/f;->k:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/core/presenters/f;

    .line 169
    if-eqz v0, :cond_0

    .line 171
    invoke-virtual {v0}, Lcom/my/target/core/presenters/f;->y()Z

    move-result v0

    .line 174
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final onActivityCreate(Lcom/my/target/common/MyTargetActivity;Landroid/content/Intent;Landroid/widget/FrameLayout;)V
    .locals 1
    .param p1, "activity"    # Lcom/my/target/common/MyTargetActivity;
    .param p2, "intent"    # Landroid/content/Intent;
    .param p3, "rootLayout"    # Landroid/widget/FrameLayout;

    .prologue
    .line 102
    invoke-super {p0, p1, p2, p3}, Lcom/my/target/core/engines/c;->onActivityCreate(Lcom/my/target/common/MyTargetActivity;Landroid/content/Intent;Landroid/widget/FrameLayout;)V

    .line 103
    const/4 v0, 0x0

    invoke-direct {p0, p3, v0}, Lcom/my/target/core/engines/f;->a(Landroid/view/ViewGroup;Z)V

    .line 104
    return-void
.end method

.method public final onActivityDestroy()V
    .locals 1

    .prologue
    .line 151
    invoke-super {p0}, Lcom/my/target/core/engines/c;->onActivityDestroy()V

    .line 152
    iget-object v0, p0, Lcom/my/target/core/engines/f;->k:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 154
    iget-object v0, p0, Lcom/my/target/core/engines/f;->k:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/core/presenters/f;

    .line 155
    if-eqz v0, :cond_0

    .line 157
    invoke-virtual {v0}, Lcom/my/target/core/presenters/f;->destroy()V

    .line 160
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/my/target/core/engines/f;->k:Ljava/lang/ref/WeakReference;

    .line 161
    return-void
.end method

.method public final onActivityPause()V
    .locals 1

    .prologue
    .line 109
    invoke-super {p0}, Lcom/my/target/core/engines/c;->onActivityPause()V

    .line 110
    iget-object v0, p0, Lcom/my/target/core/engines/f;->k:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 112
    iget-object v0, p0, Lcom/my/target/core/engines/f;->k:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/core/presenters/f;

    .line 113
    if-eqz v0, :cond_0

    .line 115
    invoke-virtual {v0}, Lcom/my/target/core/presenters/f;->pause()V

    .line 118
    :cond_0
    return-void
.end method

.method public final onActivityResume()V
    .locals 1

    .prologue
    .line 123
    invoke-super {p0}, Lcom/my/target/core/engines/c;->onActivityResume()V

    .line 124
    iget-object v0, p0, Lcom/my/target/core/engines/f;->k:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lcom/my/target/core/engines/f;->k:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/core/presenters/f;

    .line 127
    if-eqz v0, :cond_0

    .line 129
    invoke-virtual {v0}, Lcom/my/target/core/presenters/f;->resume()V

    .line 132
    :cond_0
    return-void
.end method

.method public final onActivityStop()V
    .locals 1

    .prologue
    .line 137
    invoke-super {p0}, Lcom/my/target/core/engines/c;->onActivityStop()V

    .line 138
    iget-object v0, p0, Lcom/my/target/core/engines/f;->k:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 140
    iget-object v0, p0, Lcom/my/target/core/engines/f;->k:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/core/presenters/f;

    .line 141
    if-eqz v0, :cond_0

    .line 143
    invoke-virtual {v0}, Lcom/my/target/core/presenters/f;->stop()V

    .line 146
    :cond_0
    return-void
.end method
