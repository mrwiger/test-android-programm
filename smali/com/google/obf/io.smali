.class public Lcom/google/obf/io;
.super Ljava/lang/Object;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/obf/io$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/google/obf/im;

.field private final b:Ljava/security/SecureRandom;


# direct methods
.method public constructor <init>(Lcom/google/obf/im;Ljava/security/SecureRandom;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/google/obf/io;->a:Lcom/google/obf/im;

    .line 3
    iput-object p2, p0, Lcom/google/obf/io;->b:Ljava/security/SecureRandom;

    .line 4
    return-void
.end method

.method static a([B)V
    .locals 2

    .prologue
    .line 5
    const/4 v0, 0x0

    :goto_0
    array-length v1, p0

    if-ge v0, v1, :cond_0

    .line 6
    aget-byte v1, p0, v0

    xor-int/lit8 v1, v1, 0x44

    int-to-byte v1, v1

    aput-byte v1, p0, v0

    .line 7
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 8
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)[B
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/io$a;
        }
    .end annotation

    .prologue
    .line 9
    :try_start_0
    iget-object v0, p0, Lcom/google/obf/io;->a:Lcom/google/obf/im;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Lcom/google/obf/im;->a(Ljava/lang/String;Z)[B

    move-result-object v0

    .line 10
    array-length v1, v0

    const/16 v2, 0x20

    if-eq v1, v2, :cond_0

    .line 11
    new-instance v0, Lcom/google/obf/io$a;

    invoke-direct {v0, p0}, Lcom/google/obf/io$a;-><init>(Lcom/google/obf/io;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 18
    :catch_0
    move-exception v0

    .line 19
    new-instance v1, Lcom/google/obf/io$a;

    invoke-direct {v1, p0, v0}, Lcom/google/obf/io$a;-><init>(Lcom/google/obf/io;Ljava/lang/Throwable;)V

    throw v1

    .line 12
    :cond_0
    const/4 v1, 0x4

    const/16 v2, 0x10

    .line 13
    :try_start_1
    invoke-static {v0, v1, v2}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 14
    const/16 v1, 0x10

    new-array v1, v1, [B

    .line 15
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 16
    invoke-static {v1}, Lcom/google/obf/io;->a([B)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    .line 17
    return-object v1
.end method

.method public a([BLjava/lang/String;)[B
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/io$a;
        }
    .end annotation

    .prologue
    const/16 v2, 0x10

    .line 20
    array-length v0, p1

    if-eq v0, v2, :cond_0

    .line 21
    new-instance v0, Lcom/google/obf/io$a;

    invoke-direct {v0, p0}, Lcom/google/obf/io$a;-><init>(Lcom/google/obf/io;)V

    throw v0

    .line 22
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/obf/io;->a:Lcom/google/obf/im;

    const/4 v1, 0x0

    invoke-interface {v0, p2, v1}, Lcom/google/obf/im;->a(Ljava/lang/String;Z)[B

    move-result-object v0

    .line 23
    array-length v1, v0

    if-gt v1, v2, :cond_1

    .line 24
    new-instance v0, Lcom/google/obf/io$a;

    invoke-direct {v0, p0}, Lcom/google/obf/io$a;-><init>(Lcom/google/obf/io;)V

    throw v0
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljavax/crypto/BadPaddingException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/security/InvalidAlgorithmParameterException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_6

    .line 37
    :catch_0
    move-exception v0

    .line 38
    new-instance v1, Lcom/google/obf/io$a;

    invoke-direct {v1, p0, v0}, Lcom/google/obf/io$a;-><init>(Lcom/google/obf/io;Ljava/lang/Throwable;)V

    throw v1

    .line 25
    :cond_1
    :try_start_1
    array-length v1, v0

    .line 26
    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 27
    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 28
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 29
    const/16 v2, 0x10

    new-array v2, v2, [B

    .line 30
    array-length v0, v0

    add-int/lit8 v0, v0, -0x10

    new-array v0, v0, [B

    .line 31
    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 32
    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 33
    new-instance v1, Ljavax/crypto/spec/SecretKeySpec;

    const-string v3, "AES"

    invoke-direct {v1, p1, v3}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 34
    const-string v3, "AES/CBC/PKCS5Padding"

    invoke-static {v3}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v3

    .line 35
    const/4 v4, 0x2

    new-instance v5, Ljavax/crypto/spec/IvParameterSpec;

    invoke-direct {v5, v2}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    invoke-virtual {v3, v4, v1, v5}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 36
    invoke-virtual {v3, v0}, Ljavax/crypto/Cipher;->doFinal([B)[B
    :try_end_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/security/InvalidKeyException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljavax/crypto/BadPaddingException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/security/InvalidAlgorithmParameterException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_6

    move-result-object v0

    return-object v0

    .line 39
    :catch_1
    move-exception v0

    .line 40
    new-instance v1, Lcom/google/obf/io$a;

    invoke-direct {v1, p0, v0}, Lcom/google/obf/io$a;-><init>(Lcom/google/obf/io;Ljava/lang/Throwable;)V

    throw v1

    .line 41
    :catch_2
    move-exception v0

    .line 42
    new-instance v1, Lcom/google/obf/io$a;

    invoke-direct {v1, p0, v0}, Lcom/google/obf/io$a;-><init>(Lcom/google/obf/io;Ljava/lang/Throwable;)V

    throw v1

    .line 43
    :catch_3
    move-exception v0

    .line 44
    new-instance v1, Lcom/google/obf/io$a;

    invoke-direct {v1, p0, v0}, Lcom/google/obf/io$a;-><init>(Lcom/google/obf/io;Ljava/lang/Throwable;)V

    throw v1

    .line 45
    :catch_4
    move-exception v0

    .line 46
    new-instance v1, Lcom/google/obf/io$a;

    invoke-direct {v1, p0, v0}, Lcom/google/obf/io$a;-><init>(Lcom/google/obf/io;Ljava/lang/Throwable;)V

    throw v1

    .line 47
    :catch_5
    move-exception v0

    .line 48
    new-instance v1, Lcom/google/obf/io$a;

    invoke-direct {v1, p0, v0}, Lcom/google/obf/io$a;-><init>(Lcom/google/obf/io;Ljava/lang/Throwable;)V

    throw v1

    .line 49
    :catch_6
    move-exception v0

    .line 50
    new-instance v1, Lcom/google/obf/io$a;

    invoke-direct {v1, p0, v0}, Lcom/google/obf/io$a;-><init>(Lcom/google/obf/io;Ljava/lang/Throwable;)V

    throw v1
.end method
