.class public final Lcom/google/android/exoplayer2/ext/ffmpeg/FfmpegDecoderException;
.super Lcom/google/android/exoplayer2/audio/AudioDecoderException;
.source "FfmpegDecoderException.java"


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer2/audio/AudioDecoderException;-><init>(Ljava/lang/String;)V

    .line 27
    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 0
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "cause"    # Ljava/lang/Throwable;

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Lcom/google/android/exoplayer2/audio/AudioDecoderException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 31
    return-void
.end method
