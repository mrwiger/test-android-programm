.class public interface abstract Lcom/my/target/bj;
.super Ljava/lang/Object;
.source "VastTags.java"


# static fields
.field public static final HEIGHT:Ljava/lang/String; = "height"

.field public static final WIDTH:Ljava/lang/String; = "width"

.field public static final adSlotID:Ljava/lang/String; = "adSlotID"

.field public static final apiFramework:Ljava/lang/String; = "apiFramework"

.field public static final fC:Ljava/lang/String; = "<?xml"

.field public static final fD:Ljava/lang/String; = "<VAST"

.field public static final fE:Ljava/lang/String; = "VAST"

.field public static final fF:Ljava/lang/String; = "VASTAdTagURI"

.field public static final fG:Ljava/lang/String; = "Ad"

.field public static final fH:Ljava/lang/String; = "InLine"

.field public static final fI:Ljava/lang/String; = "Wrapper"

.field public static final fJ:Ljava/lang/String; = "Impression"

.field public static final fK:Ljava/lang/String; = "Creatives"

.field public static final fL:Ljava/lang/String; = "Creative"

.field public static final fM:Ljava/lang/String; = "Linear"

.field public static final fN:Ljava/lang/String; = "Duration"

.field public static final fO:Ljava/lang/String; = "TrackingEvents"

.field public static final fP:Ljava/lang/String; = "Tracking"

.field public static final fQ:Ljava/lang/String; = "MediaFiles"

.field public static final fR:Ljava/lang/String; = "MediaFile"

.field public static final fS:Ljava/lang/String; = "VideoClicks"

.field public static final fT:Ljava/lang/String; = "ClickThrough"

.field public static final fU:Ljava/lang/String; = "ClickTracking"

.field public static final fV:Ljava/lang/String; = "CompanionClickThrough"

.field public static final fW:Ljava/lang/String; = "CompanionClickTracking"

.field public static final fX:Ljava/lang/String; = "Extensions"

.field public static final fY:Ljava/lang/String; = "Extension"

.field public static final fZ:Ljava/lang/String; = "linkTxt"

.field public static final gA:Ljava/lang/String; = "assetHeight"

.field public static final gB:Ljava/lang/String; = "expandedWidth"

.field public static final gC:Ljava/lang/String; = "expandedHeight"

.field public static final gD:Ljava/lang/String; = "StaticResource"

.field public static final gE:Ljava/lang/String; = "IFrameResource"

.field public static final gF:Ljava/lang/String; = "HTMLResource"

.field public static final gG:Ljava/lang/String; = "close"

.field public static final ga:Ljava/lang/String; = "skipoffset"

.field public static final gb:Ljava/lang/String; = "start"

.field public static final gc:Ljava/lang/String; = "firstQuartile"

.field public static final gd:Ljava/lang/String; = "midpoint"

.field public static final ge:Ljava/lang/String; = "thirdQuartile"

.field public static final gf:Ljava/lang/String; = "complete"

.field public static final gg:Ljava/lang/String; = "creativeView"

.field public static final gh:Ljava/lang/String; = "mute"

.field public static final gi:Ljava/lang/String; = "unmute"

.field public static final gj:Ljava/lang/String; = "pause"

.field public static final gk:Ljava/lang/String; = "resume"

.field public static final gl:Ljava/lang/String; = "fullscreen"

.field public static final gm:Ljava/lang/String; = "exitFullscreen"

.field public static final gn:Ljava/lang/String; = "skip"

.field public static final go:Ljava/lang/String; = "event"

.field public static final gp:Ljava/lang/String; = "offset"

.field public static final gq:Ljava/lang/String; = "progress"

.field public static final gr:Ljava/lang/String; = "bitrate"

.field public static final gs:Ljava/lang/String; = "width"

.field public static final gt:Ljava/lang/String; = "height"

.field public static final gu:Ljava/lang/String; = "none"

.field public static final gv:Ljava/lang/String; = "any"

.field public static final gw:Ljava/lang/String; = "all"

.field public static final gx:Ljava/lang/String; = "CompanionAds"

.field public static final gy:Ljava/lang/String; = "Companion"

.field public static final gz:Ljava/lang/String; = "assetWidth"

.field public static final id:Ljava/lang/String; = "id"

.field public static final required:Ljava/lang/String; = "required"

.field public static final s:Ljava/lang/String; = "error"

.field public static final type:Ljava/lang/String; = "type"
