.class Lru/cn/tv/stb/calendar/CalendarViewModel;
.super Lru/cn/mvvm/RxViewModel;
.source "CalendarViewModel.java"


# instance fields
.field private final dates:Landroid/arch/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/arch/lifecycle/MutableLiveData",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private final loader:Lru/cn/mvvm/RxLoader;


# direct methods
.method constructor <init>(Lru/cn/mvvm/RxLoader;)V
    .locals 1
    .param p1, "loader"    # Lru/cn/mvvm/RxLoader;

    .prologue
    .line 24
    invoke-direct {p0}, Lru/cn/mvvm/RxViewModel;-><init>()V

    .line 25
    iput-object p1, p0, Lru/cn/tv/stb/calendar/CalendarViewModel;->loader:Lru/cn/mvvm/RxLoader;

    .line 26
    new-instance v0, Landroid/arch/lifecycle/MutableLiveData;

    invoke-direct {v0}, Landroid/arch/lifecycle/MutableLiveData;-><init>()V

    iput-object v0, p0, Lru/cn/tv/stb/calendar/CalendarViewModel;->dates:Landroid/arch/lifecycle/MutableLiveData;

    .line 27
    return-void
.end method

.method private loadDates(J)V
    .locals 5
    .param p1, "channelId"    # J

    .prologue
    .line 45
    invoke-static {p1, p2}, Lru/cn/api/provider/TvContentProviderContract;->dates(J)Landroid/net/Uri;

    move-result-object v0

    .line 47
    .local v0, "datesUri":Landroid/net/Uri;
    iget-object v1, p0, Lru/cn/tv/stb/calendar/CalendarViewModel;->loader:Lru/cn/mvvm/RxLoader;

    .line 48
    invoke-virtual {v1, v0}, Lru/cn/mvvm/RxLoader;->query(Landroid/net/Uri;)Lio/reactivex/Observable;

    move-result-object v1

    .line 49
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    .line 50
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    new-instance v2, Lru/cn/tv/stb/calendar/CalendarViewModel$$Lambda$0;

    invoke-direct {v2, p0}, Lru/cn/tv/stb/calendar/CalendarViewModel$$Lambda$0;-><init>(Lru/cn/tv/stb/calendar/CalendarViewModel;)V

    new-instance v3, Lru/cn/tv/stb/calendar/CalendarViewModel$$Lambda$1;

    invoke-direct {v3, p0}, Lru/cn/tv/stb/calendar/CalendarViewModel$$Lambda$1;-><init>(Lru/cn/tv/stb/calendar/CalendarViewModel;)V

    .line 51
    invoke-virtual {v1, v2, v3}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v1

    .line 47
    invoke-virtual {p0, v1}, Lru/cn/tv/stb/calendar/CalendarViewModel;->bind(Lio/reactivex/disposables/Disposable;)V

    .line 54
    return-void
.end method


# virtual methods
.method public dates()Landroid/arch/lifecycle/LiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/arch/lifecycle/LiveData",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 30
    iget-object v0, p0, Lru/cn/tv/stb/calendar/CalendarViewModel;->dates:Landroid/arch/lifecycle/MutableLiveData;

    return-object v0
.end method

.method final synthetic lambda$loadDates$0$CalendarViewModel(Landroid/database/Cursor;)V
    .locals 1
    .param p1, "it"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 51
    iget-object v0, p0, Lru/cn/tv/stb/calendar/CalendarViewModel;->dates:Landroid/arch/lifecycle/MutableLiveData;

    invoke-virtual {v0, p1}, Landroid/arch/lifecycle/MutableLiveData;->setValue(Ljava/lang/Object;)V

    return-void
.end method

.method final synthetic lambda$loadDates$1$CalendarViewModel(Ljava/lang/Throwable;)V
    .locals 3
    .param p1, "throwable"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 52
    iget-object v0, p0, Lru/cn/tv/stb/calendar/CalendarViewModel;->dates:Landroid/arch/lifecycle/MutableLiveData;

    new-instance v1, Landroid/database/MatrixCursor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/String;

    invoke-direct {v1, v2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/arch/lifecycle/MutableLiveData;->setValue(Ljava/lang/Object;)V

    return-void
.end method

.method public setChannelId(J)V
    .locals 3
    .param p1, "channelId"    # J

    .prologue
    .line 34
    invoke-virtual {p0}, Lru/cn/tv/stb/calendar/CalendarViewModel;->unbindAll()V

    .line 36
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-eqz v0, :cond_0

    .line 37
    invoke-direct {p0, p1, p2}, Lru/cn/tv/stb/calendar/CalendarViewModel;->loadDates(J)V

    .line 42
    :goto_0
    return-void

    .line 40
    :cond_0
    iget-object v0, p0, Lru/cn/tv/stb/calendar/CalendarViewModel;->dates:Landroid/arch/lifecycle/MutableLiveData;

    new-instance v1, Landroid/database/MatrixCursor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/String;

    invoke-direct {v1, v2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/arch/lifecycle/MutableLiveData;->setValue(Ljava/lang/Object;)V

    goto :goto_0
.end method
