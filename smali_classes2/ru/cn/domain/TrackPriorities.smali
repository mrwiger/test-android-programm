.class public Lru/cn/domain/TrackPriorities;
.super Ljava/lang/Object;
.source "TrackPriorities.java"


# direct methods
.method public static allowed()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Lru/cn/api/iptv/replies/MediaLocation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 97
    sget-object v0, Lru/cn/domain/TrackPriorities$$Lambda$5;->$instance:Ljava/util/Comparator;

    return-object v0
.end method

.method public static byContractor(J)Ljava/util/Comparator;
    .locals 2
    .param p0, "contractorId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/Comparator",
            "<",
            "Lru/cn/api/iptv/replies/MediaLocation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 112
    new-instance v0, Lru/cn/domain/TrackPriorities$$Lambda$6;

    invoke-direct {v0, p0, p1}, Lru/cn/domain/TrackPriorities$$Lambda$6;-><init>(J)V

    return-object v0
.end method

.method public static byTerritory(J)Ljava/util/Comparator;
    .locals 2
    .param p0, "territoryId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/Comparator",
            "<",
            "Lru/cn/api/iptv/replies/MediaLocation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 33
    new-instance v0, Lru/cn/domain/TrackPriorities$$Lambda$1;

    invoke-direct {v0, p0, p1}, Lru/cn/domain/TrackPriorities$$Lambda$1;-><init>(J)V

    return-object v0
.end method

.method public static byTimezones(Ljava/util/Map;J)Ljava/util/Comparator;
    .locals 3
    .param p1, "territoryId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;J)",
            "Ljava/util/Comparator",
            "<",
            "Lru/cn/api/iptv/replies/MediaLocation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 45
    .local p0, "timezones":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/Long;Ljava/lang/Long;>;"
    new-instance v0, Lru/cn/domain/TrackPriorities$$Lambda$2;

    invoke-direct {v0, p0, p1, p2}, Lru/cn/domain/TrackPriorities$$Lambda$2;-><init>(Ljava/util/Map;J)V

    .line 57
    .local v0, "timezoneDiff":Lcom/annimon/stream/function/Function;, "Lcom/annimon/stream/function/Function<Ljava/lang/Long;Ljava/lang/Long;>;"
    new-instance v1, Lru/cn/domain/TrackPriorities$$Lambda$3;

    invoke-direct {v1, p1, p2, v0}, Lru/cn/domain/TrackPriorities$$Lambda$3;-><init>(JLcom/annimon/stream/function/Function;)V

    return-object v1
.end method

.method static final synthetic lambda$allowed$5$TrackPriorities(Lru/cn/api/iptv/replies/MediaLocation;Lru/cn/api/iptv/replies/MediaLocation;)I
    .locals 2
    .param p0, "lhs"    # Lru/cn/api/iptv/replies/MediaLocation;
    .param p1, "rhs"    # Lru/cn/api/iptv/replies/MediaLocation;

    .prologue
    .line 99
    iget-object v0, p0, Lru/cn/api/iptv/replies/MediaLocation;->access:Lru/cn/api/iptv/replies/MediaLocation$Access;

    iget-object v1, p1, Lru/cn/api/iptv/replies/MediaLocation;->access:Lru/cn/api/iptv/replies/MediaLocation$Access;

    if-eq v0, v1, :cond_1

    .line 100
    iget-object v0, p0, Lru/cn/api/iptv/replies/MediaLocation;->access:Lru/cn/api/iptv/replies/MediaLocation$Access;

    sget-object v1, Lru/cn/api/iptv/replies/MediaLocation$Access;->denied:Lru/cn/api/iptv/replies/MediaLocation$Access;

    if-ne v0, v1, :cond_0

    .line 101
    const/4 v0, 0x1

    .line 107
    :goto_0
    return v0

    .line 103
    :cond_0
    iget-object v0, p1, Lru/cn/api/iptv/replies/MediaLocation;->access:Lru/cn/api/iptv/replies/MediaLocation$Access;

    sget-object v1, Lru/cn/api/iptv/replies/MediaLocation$Access;->denied:Lru/cn/api/iptv/replies/MediaLocation$Access;

    if-ne v0, v1, :cond_1

    .line 104
    const/4 v0, -0x1

    goto :goto_0

    .line 107
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static final synthetic lambda$byContractor$6$TrackPriorities(JLru/cn/api/iptv/replies/MediaLocation;Lru/cn/api/iptv/replies/MediaLocation;)I
    .locals 6
    .param p0, "contractorId"    # J
    .param p2, "lhs"    # Lru/cn/api/iptv/replies/MediaLocation;
    .param p3, "rhs"    # Lru/cn/api/iptv/replies/MediaLocation;

    .prologue
    const-wide/16 v4, 0x0

    .line 114
    iget-wide v0, p2, Lru/cn/api/iptv/replies/MediaLocation;->contractorId:J

    iget-wide v2, p3, Lru/cn/api/iptv/replies/MediaLocation;->contractorId:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    iget-wide v0, p2, Lru/cn/api/iptv/replies/MediaLocation;->contractorId:J

    cmp-long v0, v0, v4

    if-lez v0, :cond_1

    iget-wide v0, p3, Lru/cn/api/iptv/replies/MediaLocation;->contractorId:J

    cmp-long v0, v0, v4

    if-lez v0, :cond_1

    .line 115
    iget-wide v0, p2, Lru/cn/api/iptv/replies/MediaLocation;->contractorId:J

    cmp-long v0, v0, p0

    if-nez v0, :cond_0

    .line 116
    const/4 v0, -0x1

    .line 122
    :goto_0
    return v0

    .line 118
    :cond_0
    iget-wide v0, p3, Lru/cn/api/iptv/replies/MediaLocation;->contractorId:J

    cmp-long v0, v0, p0

    if-nez v0, :cond_1

    .line 119
    const/4 v0, 0x1

    goto :goto_0

    .line 122
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static final synthetic lambda$byTerritory$1$TrackPriorities(JLru/cn/api/iptv/replies/MediaLocation;Lru/cn/api/iptv/replies/MediaLocation;)I
    .locals 8
    .param p0, "territoryId"    # J
    .param p2, "lhs"    # Lru/cn/api/iptv/replies/MediaLocation;
    .param p3, "rhs"    # Lru/cn/api/iptv/replies/MediaLocation;

    .prologue
    const-wide/16 v6, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 35
    iget-wide v4, p2, Lru/cn/api/iptv/replies/MediaLocation;->territoryId:J

    cmp-long v4, v4, p0

    if-eqz v4, :cond_0

    iget-wide v4, p2, Lru/cn/api/iptv/replies/MediaLocation;->territoryId:J

    cmp-long v4, v4, v6

    if-nez v4, :cond_3

    :cond_0
    move v0, v3

    .line 36
    .local v0, "lhsExpected":Z
    :goto_0
    iget-wide v4, p3, Lru/cn/api/iptv/replies/MediaLocation;->territoryId:J

    cmp-long v4, v4, p0

    if-eqz v4, :cond_1

    iget-wide v4, p3, Lru/cn/api/iptv/replies/MediaLocation;->territoryId:J

    cmp-long v4, v4, v6

    if-nez v4, :cond_4

    :cond_1
    move v1, v3

    .line 37
    .local v1, "rhsExpected":Z
    :goto_1
    if-eq v0, v1, :cond_5

    .line 38
    if-eqz v0, :cond_2

    const/4 v3, -0x1

    .line 40
    :cond_2
    :goto_2
    return v3

    .end local v0    # "lhsExpected":Z
    .end local v1    # "rhsExpected":Z
    :cond_3
    move v0, v2

    .line 35
    goto :goto_0

    .restart local v0    # "lhsExpected":Z
    :cond_4
    move v1, v2

    .line 36
    goto :goto_1

    .restart local v1    # "rhsExpected":Z
    :cond_5
    move v3, v2

    .line 40
    goto :goto_2
.end method

.method static final synthetic lambda$byTimezones$2$TrackPriorities(Ljava/util/Map;JLjava/lang/Long;)Ljava/lang/Long;
    .locals 7
    .param p0, "timezones"    # Ljava/util/Map;
    .param p1, "territoryId"    # J
    .param p3, "trackTerritoryId"    # Ljava/lang/Long;

    .prologue
    const/4 v2, 0x0

    .line 46
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {p0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 47
    .local v0, "base":Ljava/lang/Long;
    if-nez v0, :cond_1

    .line 54
    :cond_0
    :goto_0
    return-object v2

    .line 50
    :cond_1
    invoke-interface {p0, p3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    .line 51
    .local v1, "current":Ljava/lang/Long;
    if-eqz v1, :cond_0

    .line 54
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sub-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    goto :goto_0
.end method

.method static final synthetic lambda$byTimezones$3$TrackPriorities(JLcom/annimon/stream/function/Function;Lru/cn/api/iptv/replies/MediaLocation;Lru/cn/api/iptv/replies/MediaLocation;)I
    .locals 10
    .param p0, "territoryId"    # J
    .param p2, "timezoneDiff"    # Lcom/annimon/stream/function/Function;
    .param p3, "lhs"    # Lru/cn/api/iptv/replies/MediaLocation;
    .param p4, "rhs"    # Lru/cn/api/iptv/replies/MediaLocation;

    .prologue
    const-wide/16 v8, 0x0

    .line 58
    iget-wide v2, p3, Lru/cn/api/iptv/replies/MediaLocation;->territoryId:J

    .line 59
    .local v2, "lhsTerritoryId":J
    cmp-long v6, v2, v8

    if-nez v6, :cond_0

    .line 60
    move-wide v2, p0

    .line 63
    :cond_0
    iget-wide v4, p4, Lru/cn/api/iptv/replies/MediaLocation;->territoryId:J

    .line 64
    .local v4, "rhsTerritoryId":J
    cmp-long v6, v4, v8

    if-nez v6, :cond_1

    .line 65
    move-wide v4, p0

    .line 68
    :cond_1
    cmp-long v6, v2, v4

    if-eqz v6, :cond_3

    .line 69
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {p2, v6}, Lcom/annimon/stream/function/Function;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 70
    .local v0, "lhsDiff":Ljava/lang/Long;
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {p2, v6}, Lcom/annimon/stream/function/Function;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    .line 72
    .local v1, "rhsDiff":Ljava/lang/Long;
    if-eqz v0, :cond_3

    if-eqz v1, :cond_3

    invoke-virtual {v0, v1}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 73
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    cmp-long v6, v6, v8

    if-gez v6, :cond_2

    const/4 v6, -0x1

    .line 77
    .end local v0    # "lhsDiff":Ljava/lang/Long;
    .end local v1    # "rhsDiff":Ljava/lang/Long;
    :goto_0
    return v6

    .line 73
    .restart local v0    # "lhsDiff":Ljava/lang/Long;
    .restart local v1    # "rhsDiff":Ljava/lang/Long;
    :cond_2
    const/4 v6, 0x1

    goto :goto_0

    .line 77
    .end local v0    # "lhsDiff":Ljava/lang/Long;
    .end local v1    # "rhsDiff":Ljava/lang/Long;
    :cond_3
    const/4 v6, 0x0

    goto :goto_0
.end method

.method static final synthetic lambda$p2pTransport$8$TrackPriorities(Lru/cn/api/iptv/replies/MediaLocation;Lru/cn/api/iptv/replies/MediaLocation;)I
    .locals 2
    .param p0, "lhs"    # Lru/cn/api/iptv/replies/MediaLocation;
    .param p1, "rhs"    # Lru/cn/api/iptv/replies/MediaLocation;

    .prologue
    .line 143
    iget-object v0, p0, Lru/cn/api/iptv/replies/MediaLocation;->type:Lru/cn/api/iptv/replies/MediaLocation$Type;

    iget-object v1, p1, Lru/cn/api/iptv/replies/MediaLocation;->type:Lru/cn/api/iptv/replies/MediaLocation$Type;

    if-eq v0, v1, :cond_1

    .line 145
    iget-object v0, p0, Lru/cn/api/iptv/replies/MediaLocation;->type:Lru/cn/api/iptv/replies/MediaLocation$Type;

    sget-object v1, Lru/cn/api/iptv/replies/MediaLocation$Type;->magnet:Lru/cn/api/iptv/replies/MediaLocation$Type;

    if-ne v0, v1, :cond_0

    .line 146
    const/4 v0, -0x1

    .line 152
    :goto_0
    return v0

    .line 148
    :cond_0
    iget-object v0, p1, Lru/cn/api/iptv/replies/MediaLocation;->type:Lru/cn/api/iptv/replies/MediaLocation$Type;

    sget-object v1, Lru/cn/api/iptv/replies/MediaLocation$Type;->magnet:Lru/cn/api/iptv/replies/MediaLocation$Type;

    if-ne v0, v1, :cond_1

    .line 149
    const/4 v0, 0x1

    goto :goto_0

    .line 152
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static final synthetic lambda$paidPartner$4$TrackPriorities(JLru/cn/api/iptv/replies/MediaLocation;Lru/cn/api/iptv/replies/MediaLocation;)I
    .locals 4
    .param p0, "contractorId"    # J
    .param p2, "lhs"    # Lru/cn/api/iptv/replies/MediaLocation;
    .param p3, "rhs"    # Lru/cn/api/iptv/replies/MediaLocation;

    .prologue
    .line 84
    iget-wide v0, p2, Lru/cn/api/iptv/replies/MediaLocation;->contractorId:J

    iget-wide v2, p3, Lru/cn/api/iptv/replies/MediaLocation;->contractorId:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 85
    iget-wide v0, p2, Lru/cn/api/iptv/replies/MediaLocation;->contractorId:J

    cmp-long v0, v0, p0

    if-nez v0, :cond_0

    iget-object v0, p2, Lru/cn/api/iptv/replies/MediaLocation;->access:Lru/cn/api/iptv/replies/MediaLocation$Access;

    sget-object v1, Lru/cn/api/iptv/replies/MediaLocation$Access;->denied:Lru/cn/api/iptv/replies/MediaLocation$Access;

    if-ne v0, v1, :cond_0

    .line 86
    const/4 v0, -0x1

    .line 92
    :goto_0
    return v0

    .line 88
    :cond_0
    iget-wide v0, p3, Lru/cn/api/iptv/replies/MediaLocation;->contractorId:J

    cmp-long v0, v0, p0

    if-nez v0, :cond_1

    iget-object v0, p3, Lru/cn/api/iptv/replies/MediaLocation;->access:Lru/cn/api/iptv/replies/MediaLocation$Access;

    sget-object v1, Lru/cn/api/iptv/replies/MediaLocation$Access;->denied:Lru/cn/api/iptv/replies/MediaLocation$Access;

    if-ne v0, v1, :cond_1

    .line 89
    const/4 v0, 0x1

    goto :goto_0

    .line 92
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static final synthetic lambda$purchased$0$TrackPriorities(Lru/cn/api/iptv/replies/MediaLocation;Lru/cn/api/iptv/replies/MediaLocation;)I
    .locals 2
    .param p0, "lhs"    # Lru/cn/api/iptv/replies/MediaLocation;
    .param p1, "rhs"    # Lru/cn/api/iptv/replies/MediaLocation;

    .prologue
    .line 20
    iget-object v0, p0, Lru/cn/api/iptv/replies/MediaLocation;->access:Lru/cn/api/iptv/replies/MediaLocation$Access;

    iget-object v1, p1, Lru/cn/api/iptv/replies/MediaLocation;->access:Lru/cn/api/iptv/replies/MediaLocation$Access;

    if-eq v0, v1, :cond_1

    .line 21
    iget-object v0, p0, Lru/cn/api/iptv/replies/MediaLocation;->access:Lru/cn/api/iptv/replies/MediaLocation$Access;

    sget-object v1, Lru/cn/api/iptv/replies/MediaLocation$Access;->purchased:Lru/cn/api/iptv/replies/MediaLocation$Access;

    if-ne v0, v1, :cond_0

    .line 22
    const/4 v0, -0x1

    .line 28
    :goto_0
    return v0

    .line 24
    :cond_0
    iget-object v0, p1, Lru/cn/api/iptv/replies/MediaLocation;->access:Lru/cn/api/iptv/replies/MediaLocation$Access;

    sget-object v1, Lru/cn/api/iptv/replies/MediaLocation$Access;->purchased:Lru/cn/api/iptv/replies/MediaLocation$Access;

    if-ne v0, v1, :cond_1

    .line 25
    const/4 v0, 0x1

    goto :goto_0

    .line 28
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static final synthetic lambda$udpTransport$9$TrackPriorities(Lru/cn/api/iptv/replies/MediaLocation;Lru/cn/api/iptv/replies/MediaLocation;)I
    .locals 2
    .param p0, "lhs"    # Lru/cn/api/iptv/replies/MediaLocation;
    .param p1, "rhs"    # Lru/cn/api/iptv/replies/MediaLocation;

    .prologue
    .line 158
    iget-object v0, p0, Lru/cn/api/iptv/replies/MediaLocation;->type:Lru/cn/api/iptv/replies/MediaLocation$Type;

    iget-object v1, p1, Lru/cn/api/iptv/replies/MediaLocation;->type:Lru/cn/api/iptv/replies/MediaLocation$Type;

    if-eq v0, v1, :cond_1

    .line 160
    iget-object v0, p0, Lru/cn/api/iptv/replies/MediaLocation;->type:Lru/cn/api/iptv/replies/MediaLocation$Type;

    sget-object v1, Lru/cn/api/iptv/replies/MediaLocation$Type;->mcastudp:Lru/cn/api/iptv/replies/MediaLocation$Type;

    if-ne v0, v1, :cond_0

    .line 161
    const/4 v0, -0x1

    .line 168
    :goto_0
    return v0

    .line 163
    :cond_0
    iget-object v0, p1, Lru/cn/api/iptv/replies/MediaLocation;->type:Lru/cn/api/iptv/replies/MediaLocation$Type;

    sget-object v1, Lru/cn/api/iptv/replies/MediaLocation$Type;->mcastudp:Lru/cn/api/iptv/replies/MediaLocation$Type;

    if-ne v0, v1, :cond_1

    .line 164
    const/4 v0, 0x1

    goto :goto_0

    .line 168
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static final synthetic lambda$userStreams$7$TrackPriorities(Lru/cn/api/iptv/replies/MediaLocation;Lru/cn/api/iptv/replies/MediaLocation;)I
    .locals 6
    .param p0, "lhs"    # Lru/cn/api/iptv/replies/MediaLocation;
    .param p1, "rhs"    # Lru/cn/api/iptv/replies/MediaLocation;

    .prologue
    const-wide/16 v4, 0x0

    .line 129
    iget-wide v0, p0, Lru/cn/api/iptv/replies/MediaLocation;->contractorId:J

    iget-wide v2, p1, Lru/cn/api/iptv/replies/MediaLocation;->contractorId:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 130
    iget-wide v0, p0, Lru/cn/api/iptv/replies/MediaLocation;->contractorId:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    .line 131
    const/4 v0, -0x1

    .line 137
    :goto_0
    return v0

    .line 133
    :cond_0
    iget-wide v0, p1, Lru/cn/api/iptv/replies/MediaLocation;->contractorId:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_1

    .line 134
    const/4 v0, 0x1

    goto :goto_0

    .line 137
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static p2pTransport()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Lru/cn/api/iptv/replies/MediaLocation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 142
    sget-object v0, Lru/cn/domain/TrackPriorities$$Lambda$8;->$instance:Ljava/util/Comparator;

    return-object v0
.end method

.method public static paidPartner(J)Ljava/util/Comparator;
    .locals 2
    .param p0, "contractorId"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/Comparator",
            "<",
            "Lru/cn/api/iptv/replies/MediaLocation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 82
    new-instance v0, Lru/cn/domain/TrackPriorities$$Lambda$4;

    invoke-direct {v0, p0, p1}, Lru/cn/domain/TrackPriorities$$Lambda$4;-><init>(J)V

    return-object v0
.end method

.method public static purchased()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Lru/cn/api/iptv/replies/MediaLocation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 18
    sget-object v0, Lru/cn/domain/TrackPriorities$$Lambda$0;->$instance:Ljava/util/Comparator;

    return-object v0
.end method

.method public static udpTransport()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Lru/cn/api/iptv/replies/MediaLocation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 157
    sget-object v0, Lru/cn/domain/TrackPriorities$$Lambda$9;->$instance:Ljava/util/Comparator;

    return-object v0
.end method

.method public static userStreams()Ljava/util/Comparator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Comparator",
            "<",
            "Lru/cn/api/iptv/replies/MediaLocation;",
            ">;"
        }
    .end annotation

    .prologue
    .line 127
    sget-object v0, Lru/cn/domain/TrackPriorities$$Lambda$7;->$instance:Ljava/util/Comparator;

    return-object v0
.end method
