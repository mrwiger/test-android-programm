.class final enum Lru/cn/tv/stb/settings/SettingFragment$Type;
.super Ljava/lang/Enum;
.source "SettingFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/tv/stb/settings/SettingFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lru/cn/tv/stb/settings/SettingFragment$Type;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lru/cn/tv/stb/settings/SettingFragment$Type;

.field public static final enum ACCOUNT:Lru/cn/tv/stb/settings/SettingFragment$Type;

.field public static final enum ADD_PLAYLIST:Lru/cn/tv/stb/settings/SettingFragment$Type;

.field public static final enum ADVANCED_PLAYBACK:Lru/cn/tv/stb/settings/SettingFragment$Type;

.field public static final enum CACHING_LEVEL:Lru/cn/tv/stb/settings/SettingFragment$Type;

.field public static final enum CHANGE_PIN:Lru/cn/tv/stb/settings/SettingFragment$Type;

.field public static final enum DISABLE_PIN:Lru/cn/tv/stb/settings/SettingFragment$Type;

.field public static final enum DISABLE_PORNO:Lru/cn/tv/stb/settings/SettingFragment$Type;

.field public static final enum LOGIN:Lru/cn/tv/stb/settings/SettingFragment$Type;

.field public static final enum LOGOUT:Lru/cn/tv/stb/settings/SettingFragment$Type;

.field public static final enum MULTICAST:Lru/cn/tv/stb/settings/SettingFragment$Type;

.field public static final enum PLAYLIST:Lru/cn/tv/stb/settings/SettingFragment$Type;

.field public static final enum QUALITY_LEVEL:Lru/cn/tv/stb/settings/SettingFragment$Type;

.field public static final enum RENEW_PIN:Lru/cn/tv/stb/settings/SettingFragment$Type;

.field public static final enum SCALING_LEVEL:Lru/cn/tv/stb/settings/SettingFragment$Type;

.field public static final enum TECHNICAL_INFO:Lru/cn/tv/stb/settings/SettingFragment$Type;

.field public static final enum UDP_PROXY:Lru/cn/tv/stb/settings/SettingFragment$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 32
    new-instance v0, Lru/cn/tv/stb/settings/SettingFragment$Type;

    const-string v1, "DISABLE_PIN"

    invoke-direct {v0, v1, v3}, Lru/cn/tv/stb/settings/SettingFragment$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/tv/stb/settings/SettingFragment$Type;->DISABLE_PIN:Lru/cn/tv/stb/settings/SettingFragment$Type;

    .line 33
    new-instance v0, Lru/cn/tv/stb/settings/SettingFragment$Type;

    const-string v1, "CHANGE_PIN"

    invoke-direct {v0, v1, v4}, Lru/cn/tv/stb/settings/SettingFragment$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/tv/stb/settings/SettingFragment$Type;->CHANGE_PIN:Lru/cn/tv/stb/settings/SettingFragment$Type;

    .line 34
    new-instance v0, Lru/cn/tv/stb/settings/SettingFragment$Type;

    const-string v1, "RENEW_PIN"

    invoke-direct {v0, v1, v5}, Lru/cn/tv/stb/settings/SettingFragment$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/tv/stb/settings/SettingFragment$Type;->RENEW_PIN:Lru/cn/tv/stb/settings/SettingFragment$Type;

    .line 35
    new-instance v0, Lru/cn/tv/stb/settings/SettingFragment$Type;

    const-string v1, "DISABLE_PORNO"

    invoke-direct {v0, v1, v6}, Lru/cn/tv/stb/settings/SettingFragment$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/tv/stb/settings/SettingFragment$Type;->DISABLE_PORNO:Lru/cn/tv/stb/settings/SettingFragment$Type;

    .line 36
    new-instance v0, Lru/cn/tv/stb/settings/SettingFragment$Type;

    const-string v1, "ADD_PLAYLIST"

    invoke-direct {v0, v1, v7}, Lru/cn/tv/stb/settings/SettingFragment$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/tv/stb/settings/SettingFragment$Type;->ADD_PLAYLIST:Lru/cn/tv/stb/settings/SettingFragment$Type;

    .line 37
    new-instance v0, Lru/cn/tv/stb/settings/SettingFragment$Type;

    const-string v1, "PLAYLIST"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lru/cn/tv/stb/settings/SettingFragment$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/tv/stb/settings/SettingFragment$Type;->PLAYLIST:Lru/cn/tv/stb/settings/SettingFragment$Type;

    .line 38
    new-instance v0, Lru/cn/tv/stb/settings/SettingFragment$Type;

    const-string v1, "ADVANCED_PLAYBACK"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lru/cn/tv/stb/settings/SettingFragment$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/tv/stb/settings/SettingFragment$Type;->ADVANCED_PLAYBACK:Lru/cn/tv/stb/settings/SettingFragment$Type;

    .line 39
    new-instance v0, Lru/cn/tv/stb/settings/SettingFragment$Type;

    const-string v1, "CACHING_LEVEL"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lru/cn/tv/stb/settings/SettingFragment$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/tv/stb/settings/SettingFragment$Type;->CACHING_LEVEL:Lru/cn/tv/stb/settings/SettingFragment$Type;

    .line 40
    new-instance v0, Lru/cn/tv/stb/settings/SettingFragment$Type;

    const-string v1, "QUALITY_LEVEL"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lru/cn/tv/stb/settings/SettingFragment$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/tv/stb/settings/SettingFragment$Type;->QUALITY_LEVEL:Lru/cn/tv/stb/settings/SettingFragment$Type;

    .line 41
    new-instance v0, Lru/cn/tv/stb/settings/SettingFragment$Type;

    const-string v1, "SCALING_LEVEL"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lru/cn/tv/stb/settings/SettingFragment$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/tv/stb/settings/SettingFragment$Type;->SCALING_LEVEL:Lru/cn/tv/stb/settings/SettingFragment$Type;

    .line 42
    new-instance v0, Lru/cn/tv/stb/settings/SettingFragment$Type;

    const-string v1, "MULTICAST"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lru/cn/tv/stb/settings/SettingFragment$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/tv/stb/settings/SettingFragment$Type;->MULTICAST:Lru/cn/tv/stb/settings/SettingFragment$Type;

    .line 43
    new-instance v0, Lru/cn/tv/stb/settings/SettingFragment$Type;

    const-string v1, "UDP_PROXY"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lru/cn/tv/stb/settings/SettingFragment$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/tv/stb/settings/SettingFragment$Type;->UDP_PROXY:Lru/cn/tv/stb/settings/SettingFragment$Type;

    .line 44
    new-instance v0, Lru/cn/tv/stb/settings/SettingFragment$Type;

    const-string v1, "TECHNICAL_INFO"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lru/cn/tv/stb/settings/SettingFragment$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/tv/stb/settings/SettingFragment$Type;->TECHNICAL_INFO:Lru/cn/tv/stb/settings/SettingFragment$Type;

    .line 45
    new-instance v0, Lru/cn/tv/stb/settings/SettingFragment$Type;

    const-string v1, "LOGIN"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lru/cn/tv/stb/settings/SettingFragment$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/tv/stb/settings/SettingFragment$Type;->LOGIN:Lru/cn/tv/stb/settings/SettingFragment$Type;

    .line 46
    new-instance v0, Lru/cn/tv/stb/settings/SettingFragment$Type;

    const-string v1, "ACCOUNT"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lru/cn/tv/stb/settings/SettingFragment$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/tv/stb/settings/SettingFragment$Type;->ACCOUNT:Lru/cn/tv/stb/settings/SettingFragment$Type;

    .line 47
    new-instance v0, Lru/cn/tv/stb/settings/SettingFragment$Type;

    const-string v1, "LOGOUT"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lru/cn/tv/stb/settings/SettingFragment$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/tv/stb/settings/SettingFragment$Type;->LOGOUT:Lru/cn/tv/stb/settings/SettingFragment$Type;

    .line 31
    const/16 v0, 0x10

    new-array v0, v0, [Lru/cn/tv/stb/settings/SettingFragment$Type;

    sget-object v1, Lru/cn/tv/stb/settings/SettingFragment$Type;->DISABLE_PIN:Lru/cn/tv/stb/settings/SettingFragment$Type;

    aput-object v1, v0, v3

    sget-object v1, Lru/cn/tv/stb/settings/SettingFragment$Type;->CHANGE_PIN:Lru/cn/tv/stb/settings/SettingFragment$Type;

    aput-object v1, v0, v4

    sget-object v1, Lru/cn/tv/stb/settings/SettingFragment$Type;->RENEW_PIN:Lru/cn/tv/stb/settings/SettingFragment$Type;

    aput-object v1, v0, v5

    sget-object v1, Lru/cn/tv/stb/settings/SettingFragment$Type;->DISABLE_PORNO:Lru/cn/tv/stb/settings/SettingFragment$Type;

    aput-object v1, v0, v6

    sget-object v1, Lru/cn/tv/stb/settings/SettingFragment$Type;->ADD_PLAYLIST:Lru/cn/tv/stb/settings/SettingFragment$Type;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lru/cn/tv/stb/settings/SettingFragment$Type;->PLAYLIST:Lru/cn/tv/stb/settings/SettingFragment$Type;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lru/cn/tv/stb/settings/SettingFragment$Type;->ADVANCED_PLAYBACK:Lru/cn/tv/stb/settings/SettingFragment$Type;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lru/cn/tv/stb/settings/SettingFragment$Type;->CACHING_LEVEL:Lru/cn/tv/stb/settings/SettingFragment$Type;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lru/cn/tv/stb/settings/SettingFragment$Type;->QUALITY_LEVEL:Lru/cn/tv/stb/settings/SettingFragment$Type;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lru/cn/tv/stb/settings/SettingFragment$Type;->SCALING_LEVEL:Lru/cn/tv/stb/settings/SettingFragment$Type;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lru/cn/tv/stb/settings/SettingFragment$Type;->MULTICAST:Lru/cn/tv/stb/settings/SettingFragment$Type;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lru/cn/tv/stb/settings/SettingFragment$Type;->UDP_PROXY:Lru/cn/tv/stb/settings/SettingFragment$Type;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lru/cn/tv/stb/settings/SettingFragment$Type;->TECHNICAL_INFO:Lru/cn/tv/stb/settings/SettingFragment$Type;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lru/cn/tv/stb/settings/SettingFragment$Type;->LOGIN:Lru/cn/tv/stb/settings/SettingFragment$Type;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lru/cn/tv/stb/settings/SettingFragment$Type;->ACCOUNT:Lru/cn/tv/stb/settings/SettingFragment$Type;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lru/cn/tv/stb/settings/SettingFragment$Type;->LOGOUT:Lru/cn/tv/stb/settings/SettingFragment$Type;

    aput-object v2, v0, v1

    sput-object v0, Lru/cn/tv/stb/settings/SettingFragment$Type;->$VALUES:[Lru/cn/tv/stb/settings/SettingFragment$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lru/cn/tv/stb/settings/SettingFragment$Type;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 31
    const-class v0, Lru/cn/tv/stb/settings/SettingFragment$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lru/cn/tv/stb/settings/SettingFragment$Type;

    return-object v0
.end method

.method public static values()[Lru/cn/tv/stb/settings/SettingFragment$Type;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lru/cn/tv/stb/settings/SettingFragment$Type;->$VALUES:[Lru/cn/tv/stb/settings/SettingFragment$Type;

    invoke-virtual {v0}, [Lru/cn/tv/stb/settings/SettingFragment$Type;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lru/cn/tv/stb/settings/SettingFragment$Type;

    return-object v0
.end method
