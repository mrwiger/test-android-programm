.class Lcom/yandex/metrica/impl/ob/ee;
.super Lcom/yandex/metrica/impl/ak;
.source "SourceFile"


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lcom/yandex/metrica/impl/ob/el;

.field private c:Lcom/yandex/metrica/impl/ob/dv;

.field private m:Lcom/yandex/metrica/impl/ob/ct;

.field private n:Lcom/yandex/metrica/impl/ob/cs;

.field private o:Lcom/yandex/metrica/impl/ob/du;

.field private p:Lcom/yandex/metrica/impl/utils/b;

.field private q:Lcom/yandex/metrica/impl/ob/df;

.field private r:J

.field private s:J

.field private t:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/yandex/metrica/impl/ob/el;)V
    .locals 8

    .prologue
    .line 74
    .line 77
    invoke-static {p1}, Lcom/yandex/metrica/impl/ob/cq;->a(Landroid/content/Context;)Lcom/yandex/metrica/impl/ob/cq;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/cq;->f()Lcom/yandex/metrica/impl/ob/ct;

    move-result-object v3

    .line 78
    invoke-static {p1}, Lcom/yandex/metrica/impl/ob/cq;->a(Landroid/content/Context;)Lcom/yandex/metrica/impl/ob/cq;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/cq;->g()Lcom/yandex/metrica/impl/ob/cs;

    move-result-object v4

    new-instance v5, Lcom/yandex/metrica/impl/ob/du;

    invoke-direct {v5, p1}, Lcom/yandex/metrica/impl/ob/du;-><init>(Landroid/content/Context;)V

    new-instance v6, Lcom/yandex/metrica/impl/ob/ee$1;

    invoke-direct {v6}, Lcom/yandex/metrica/impl/ob/ee$1;-><init>()V

    new-instance v7, Lcom/yandex/metrica/impl/ob/df;

    .line 89
    invoke-static {p1}, Lcom/yandex/metrica/impl/ob/cq;->a(Landroid/content/Context;)Lcom/yandex/metrica/impl/ob/cq;

    move-result-object v0

    .line 90
    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/cq;->b()Lcom/yandex/metrica/impl/ob/cr;

    move-result-object v0

    invoke-direct {v7, v0}, Lcom/yandex/metrica/impl/ob/df;-><init>(Lcom/yandex/metrica/impl/ob/cr;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    .line 74
    invoke-direct/range {v0 .. v7}, Lcom/yandex/metrica/impl/ob/ee;-><init>(Landroid/content/Context;Lcom/yandex/metrica/impl/ob/el;Lcom/yandex/metrica/impl/ob/ct;Lcom/yandex/metrica/impl/ob/cs;Lcom/yandex/metrica/impl/ob/du;Lcom/yandex/metrica/impl/utils/b;Lcom/yandex/metrica/impl/ob/df;)V

    .line 92
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/yandex/metrica/impl/ob/el;Lcom/yandex/metrica/impl/ob/ct;Lcom/yandex/metrica/impl/ob/cs;Lcom/yandex/metrica/impl/ob/du;Lcom/yandex/metrica/impl/utils/b;Lcom/yandex/metrica/impl/ob/df;)V
    .locals 4

    .prologue
    const-wide/high16 v0, -0x8000000000000000L

    .line 365
    invoke-direct {p0}, Lcom/yandex/metrica/impl/ak;-><init>()V

    .line 70
    iput-wide v0, p0, Lcom/yandex/metrica/impl/ob/ee;->s:J

    .line 71
    iput-wide v0, p0, Lcom/yandex/metrica/impl/ob/ee;->t:J

    .line 366
    iput-object p1, p0, Lcom/yandex/metrica/impl/ob/ee;->a:Landroid/content/Context;

    .line 367
    iput-object p2, p0, Lcom/yandex/metrica/impl/ob/ee;->b:Lcom/yandex/metrica/impl/ob/el;

    .line 368
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ee;->b:Lcom/yandex/metrica/impl/ob/el;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/el;->o()Lcom/yandex/metrica/impl/ob/dv;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/ee;->c:Lcom/yandex/metrica/impl/ob/dv;

    .line 369
    iput-object p3, p0, Lcom/yandex/metrica/impl/ob/ee;->m:Lcom/yandex/metrica/impl/ob/ct;

    .line 370
    iput-object p4, p0, Lcom/yandex/metrica/impl/ob/ee;->n:Lcom/yandex/metrica/impl/ob/cs;

    .line 371
    iput-object p5, p0, Lcom/yandex/metrica/impl/ob/ee;->o:Lcom/yandex/metrica/impl/ob/du;

    .line 372
    iput-object p6, p0, Lcom/yandex/metrica/impl/ob/ee;->p:Lcom/yandex/metrica/impl/utils/b;

    .line 373
    iput-object p7, p0, Lcom/yandex/metrica/impl/ob/ee;->q:Lcom/yandex/metrica/impl/ob/df;

    .line 3352
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ee;->q:Lcom/yandex/metrica/impl/ob/df;

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v2, v3}, Lcom/yandex/metrica/impl/ob/df;->b(J)J

    move-result-wide v0

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/yandex/metrica/impl/ob/ee;->r:J

    .line 375
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ee;->b:Lcom/yandex/metrica/impl/ob/el;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/el;->C()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/ob/ee;->a(Ljava/util/List;)V

    .line 376
    return-void
.end method

.method private b(Ljava/util/List;)[Lcom/yandex/metrica/c$b$a;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/yandex/metrica/impl/ob/ea;",
            ">;)[",
            "Lcom/yandex/metrica/c$b$a;"
        }
    .end annotation

    .prologue
    .line 265
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 266
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/yandex/metrica/impl/ob/ea;

    .line 267
    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/ob/ee;->a(Lcom/yandex/metrica/impl/ob/ea;)Lcom/yandex/metrica/c$b$a;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 269
    :cond_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/yandex/metrica/c$b$a;

    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/yandex/metrica/c$b$a;

    return-object v0
.end method

.method private z()Z
    .locals 8

    .prologue
    .line 167
    const/4 v3, 0x0

    .line 168
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ee;->m:Lcom/yandex/metrica/impl/ob/ct;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/ee;->c:Lcom/yandex/metrica/impl/ob/dv;

    iget v1, v1, Lcom/yandex/metrica/impl/ob/dv;->j:I

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/ct;->b(I)Ljava/util/Map;

    move-result-object v1

    .line 169
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ee;->n:Lcom/yandex/metrica/impl/ob/cs;

    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/ee;->c:Lcom/yandex/metrica/impl/ob/dv;

    iget v2, v2, Lcom/yandex/metrica/impl/ob/dv;->j:I

    invoke-virtual {v0, v2}, Lcom/yandex/metrica/impl/ob/cs;->b(I)Ljava/util/Map;

    move-result-object v4

    .line 170
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 171
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 173
    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v5

    if-lez v5, :cond_3

    .line 174
    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->max(Ljava/util/Collection;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/yandex/metrica/impl/ob/ee;->s:J

    .line 177
    invoke-virtual {p0, v1}, Lcom/yandex/metrica/impl/ob/ee;->b(Ljava/util/Map;)Ljava/util/List;

    move-result-object v0

    move-object v1, v0

    .line 181
    :goto_0
    invoke-interface {v4}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 182
    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->max(Ljava/util/Collection;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/yandex/metrica/impl/ob/ee;->t:J

    .line 185
    invoke-virtual {p0, v4}, Lcom/yandex/metrica/impl/ob/ee;->c(Ljava/util/Map;)Ljava/util/List;

    move-result-object v0

    .line 189
    :goto_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-eqz v2, :cond_1

    .line 190
    :cond_0
    invoke-virtual {p0, v1, v0}, Lcom/yandex/metrica/impl/ob/ee;->a(Ljava/util/List;Ljava/util/List;)Lcom/yandex/metrica/c$b;

    move-result-object v0

    .line 191
    invoke-static {v0}, Lcom/yandex/metrica/impl/ob/d;->a(Lcom/yandex/metrica/impl/ob/d;)[B

    move-result-object v1

    .line 194
    :try_start_0
    invoke-static {v1}, Lcom/yandex/metrica/impl/s;->a([B)[B

    move-result-object v0

    .line 195
    const-string v2, "gzip"

    invoke-virtual {p0, v2}, Lcom/yandex/metrica/impl/ob/ee;->b(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 200
    :goto_2
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/ee;->p:Lcom/yandex/metrica/impl/utils/b;

    invoke-virtual {v1, v0}, Lcom/yandex/metrica/impl/utils/b;->a([B)[B

    move-result-object v0

    .line 203
    if-eqz v0, :cond_1

    .line 204
    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/ob/ee;->a([B)V

    .line 205
    const/4 v0, 0x1

    .line 208
    :goto_3
    return v0

    .line 198
    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_2

    :cond_1
    move v0, v3

    goto :goto_3

    :cond_2
    move-object v0, v2

    goto :goto_1

    :cond_3
    move-object v1, v0

    goto :goto_0
.end method


# virtual methods
.method a(Lcom/yandex/metrica/impl/ob/ea;)Lcom/yandex/metrica/c$b$a;
    .locals 4

    .prologue
    .line 292
    new-instance v0, Lcom/yandex/metrica/c$b$a;

    invoke-direct {v0}, Lcom/yandex/metrica/c$b$a;-><init>()V

    .line 293
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ob/ea;->a()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/yandex/metrica/c$b$a;->b:J

    .line 294
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ob/ea;->b()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/yandex/metrica/c$b$a;->c:J

    .line 295
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ob/ea;->d()Lorg/json/JSONArray;

    move-result-object v1

    .line 296
    if-eqz v1, :cond_0

    .line 297
    invoke-static {v1}, Lcom/yandex/metrica/impl/aq;->b(Lorg/json/JSONArray;)[Lcom/yandex/metrica/c$a;

    move-result-object v1

    iput-object v1, v0, Lcom/yandex/metrica/c$b$a;->d:[Lcom/yandex/metrica/c$a;

    .line 299
    :cond_0
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ob/ea;->c()Lorg/json/JSONArray;

    move-result-object v1

    .line 300
    if-eqz v1, :cond_1

    .line 301
    invoke-static {v1}, Lcom/yandex/metrica/impl/aq;->a(Lorg/json/JSONArray;)[Lcom/yandex/metrica/c$d;

    move-result-object v1

    iput-object v1, v0, Lcom/yandex/metrica/c$b$a;->e:[Lcom/yandex/metrica/c$d;

    .line 303
    :cond_1
    return-object v0
.end method

.method a(Lcom/yandex/metrica/impl/ob/eh;)Lcom/yandex/metrica/c$b$b;
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 274
    new-instance v3, Lcom/yandex/metrica/c$b$b;

    invoke-direct {v3}, Lcom/yandex/metrica/c$b$b;-><init>()V

    .line 275
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ob/eh;->c()Landroid/location/Location;

    move-result-object v4

    .line 276
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ob/eh;->a()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    iput-wide v6, v3, Lcom/yandex/metrica/c$b$b;->b:J

    .line 277
    invoke-virtual {v4}, Landroid/location/Location;->getTime()J

    move-result-wide v6

    iput-wide v6, v3, Lcom/yandex/metrica/c$b$b;->d:J

    .line 278
    iget-object v0, p1, Lcom/yandex/metrica/impl/ob/eh;->a:Lcom/yandex/metrica/impl/ob/dv$a;

    .line 2317
    sget-object v5, Lcom/yandex/metrica/impl/ob/ee$2;->a:[I

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/dv$a;->ordinal()I

    move-result v0

    aget v0, v5, v0

    packed-switch v0, :pswitch_data_0

    move v0, v1

    .line 278
    :goto_0
    iput v0, v3, Lcom/yandex/metrica/c$b$b;->l:I

    .line 279
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ob/eh;->b()J

    move-result-wide v6

    iput-wide v6, v3, Lcom/yandex/metrica/c$b$b;->c:J

    .line 280
    invoke-virtual {v4}, Landroid/location/Location;->getLatitude()D

    move-result-wide v6

    iput-wide v6, v3, Lcom/yandex/metrica/c$b$b;->e:D

    .line 281
    invoke-virtual {v4}, Landroid/location/Location;->getLongitude()D

    move-result-wide v6

    iput-wide v6, v3, Lcom/yandex/metrica/c$b$b;->f:D

    .line 282
    invoke-virtual {v4}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, v3, Lcom/yandex/metrica/c$b$b;->g:I

    .line 283
    invoke-virtual {v4}, Landroid/location/Location;->getBearing()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, v3, Lcom/yandex/metrica/c$b$b;->h:I

    .line 284
    invoke-virtual {v4}, Landroid/location/Location;->getSpeed()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, v3, Lcom/yandex/metrica/c$b$b;->i:I

    .line 285
    invoke-virtual {v4}, Landroid/location/Location;->getAltitude()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Math;->round(D)J

    move-result-wide v6

    long-to-int v0, v6

    iput v0, v3, Lcom/yandex/metrica/c$b$b;->j:I

    .line 286
    invoke-virtual {v4}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v0

    .line 3308
    const-string v4, "gps"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 286
    :goto_1
    iput v2, v3, Lcom/yandex/metrica/c$b$b;->k:I

    .line 287
    return-object v3

    :pswitch_0
    move v0, v1

    .line 2319
    goto :goto_0

    :pswitch_1
    move v0, v2

    .line 2321
    goto :goto_0

    .line 3310
    :cond_0
    const-string v2, "network"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3311
    const/4 v2, 0x2

    goto :goto_1

    :cond_1
    move v2, v1

    goto :goto_1

    .line 2317
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method a(Ljava/util/List;Ljava/util/List;)Lcom/yandex/metrica/c$b;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/yandex/metrica/impl/ob/eh;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/yandex/metrica/impl/ob/ea;",
            ">;)",
            "Lcom/yandex/metrica/c$b;"
        }
    .end annotation

    .prologue
    .line 244
    new-instance v1, Lcom/yandex/metrica/c$b;

    invoke-direct {v1}, Lcom/yandex/metrica/c$b;-><init>()V

    .line 246
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 2257
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2258
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/yandex/metrica/impl/ob/eh;

    .line 2259
    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/ob/ee;->a(Lcom/yandex/metrica/impl/ob/eh;)Lcom/yandex/metrica/c$b$b;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2261
    :cond_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/yandex/metrica/c$b$b;

    invoke-interface {v2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/yandex/metrica/c$b$b;

    .line 247
    iput-object v0, v1, Lcom/yandex/metrica/c$b;->b:[Lcom/yandex/metrica/c$b$b;

    .line 249
    :cond_1
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 250
    invoke-direct {p0, p2}, Lcom/yandex/metrica/impl/ob/ee;->b(Ljava/util/List;)[Lcom/yandex/metrica/c$b$a;

    move-result-object v0

    iput-object v0, v1, Lcom/yandex/metrica/c$b;->c:[Lcom/yandex/metrica/c$b$a;

    .line 253
    :cond_2
    return-object v1
.end method

.method b(Ljava/util/Map;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/yandex/metrica/impl/ob/eh;",
            ">;"
        }
    .end annotation

    .prologue
    .line 213
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 214
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 215
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    .line 216
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 217
    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 219
    :goto_1
    if-eqz v0, :cond_0

    .line 220
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 217
    :cond_1
    iget-object v4, p0, Lcom/yandex/metrica/impl/ob/ee;->o:Lcom/yandex/metrica/impl/ob/du;

    .line 218
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7, v0}, Lcom/yandex/metrica/impl/ob/du;->a(JLjava/lang/String;)Lcom/yandex/metrica/impl/ob/eh;

    move-result-object v0

    goto :goto_1

    .line 223
    :cond_2
    return-object v2
.end method

.method public b()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 96
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/ee;->b:Lcom/yandex/metrica/impl/ob/el;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/el;->p()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 108
    :cond_0
    :goto_0
    return v0

    .line 100
    :cond_1
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/ee;->b:Lcom/yandex/metrica/impl/ob/el;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/el;->q()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 104
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/ee;->t()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lcom/yandex/metrica/impl/bl;->a(Ljava/util/Collection;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 108
    invoke-direct {p0}, Lcom/yandex/metrica/impl/ob/ee;->z()Z

    move-result v0

    goto :goto_0
.end method

.method c(Ljava/util/Map;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/yandex/metrica/impl/ob/ea;",
            ">;"
        }
    .end annotation

    .prologue
    .line 228
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 229
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 230
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    .line 231
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 232
    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 234
    :goto_1
    if-eqz v0, :cond_0

    .line 235
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 232
    :cond_1
    iget-object v4, p0, Lcom/yandex/metrica/impl/ob/ee;->o:Lcom/yandex/metrica/impl/ob/du;

    .line 233
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7, v0}, Lcom/yandex/metrica/impl/ob/du;->b(JLjava/lang/String;)Lcom/yandex/metrica/impl/ob/ea;

    move-result-object v0

    goto :goto_1

    .line 238
    :cond_2
    return-object v2
.end method

.method public c()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 121
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/ee;->k()I

    move-result v0

    const/16 v3, 0xc8

    if-ne v0, v3, :cond_3

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/yandex/metrica/impl/ob/ee;->k:Z

    .line 122
    iget-boolean v0, p0, Lcom/yandex/metrica/impl/ob/ee;->k:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/ee;->o()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v2, v1

    .line 123
    :cond_1
    if-eqz v2, :cond_2

    .line 125
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ee;->m:Lcom/yandex/metrica/impl/ob/ct;

    iget-wide v2, p0, Lcom/yandex/metrica/impl/ob/ee;->s:J

    invoke-virtual {v0, v2, v3}, Lcom/yandex/metrica/impl/ob/ct;->c(J)I

    .line 128
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ee;->n:Lcom/yandex/metrica/impl/ob/cs;

    iget-wide v2, p0, Lcom/yandex/metrica/impl/ob/ee;->t:J

    invoke-virtual {v0, v2, v3}, Lcom/yandex/metrica/impl/ob/cs;->c(J)I

    .line 1357
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ee;->q:Lcom/yandex/metrica/impl/ob/df;

    iget-wide v2, p0, Lcom/yandex/metrica/impl/ob/ee;->r:J

    invoke-virtual {v0, v2, v3}, Lcom/yandex/metrica/impl/ob/df;->c(J)Lcom/yandex/metrica/impl/ob/df;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/df;->g()V

    .line 134
    :cond_2
    iget-boolean v0, p0, Lcom/yandex/metrica/impl/ob/ee;->k:Z

    return v0

    :cond_3
    move v0, v2

    .line 121
    goto :goto_0
.end method

.method protected d()Lcom/yandex/metrica/impl/ob/es;
    .locals 2

    .prologue
    .line 147
    new-instance v0, Lcom/yandex/metrica/impl/ob/ev;

    invoke-direct {v0}, Lcom/yandex/metrica/impl/ob/ev;-><init>()V

    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/ee;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/ev;->a(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/es;

    move-result-object v0

    return-object v0
.end method

.method public e()V
    .locals 4

    .prologue
    .line 113
    invoke-super {p0}, Lcom/yandex/metrica/impl/ak;->e()V

    .line 1158
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/ee;->s()Ljava/lang/String;

    move-result-object v0

    .line 1159
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    .line 1327
    const-string v0, "location"

    invoke-virtual {v1, v0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1328
    const-string v0, "deviceid"

    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/ee;->b:Lcom/yandex/metrica/impl/ob/el;

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/ob/el;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1329
    const-string v0, "device_type"

    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/ee;->b:Lcom/yandex/metrica/impl/ob/el;

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/ob/el;->A()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1330
    const-string v0, "uuid"

    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/ee;->b:Lcom/yandex/metrica/impl/ob/el;

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/ob/el;->q()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1331
    const-string v0, "analytics_sdk_version"

    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/ee;->b:Lcom/yandex/metrica/impl/ob/el;

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/ob/el;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1332
    const-string v0, "analytics_sdk_build_number"

    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/ee;->b:Lcom/yandex/metrica/impl/ob/el;

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/ob/el;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1333
    const-string v0, "analytics_sdk_build_type"

    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/ee;->b:Lcom/yandex/metrica/impl/ob/el;

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/ob/el;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1334
    const-string v0, "app_version_name"

    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/ee;->b:Lcom/yandex/metrica/impl/ob/el;

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/ob/el;->n()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1335
    const-string v0, "app_build_number"

    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/ee;->b:Lcom/yandex/metrica/impl/ob/el;

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/ob/el;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1336
    const-string v0, "os_version"

    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/ee;->b:Lcom/yandex/metrica/impl/ob/el;

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/ob/el;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1337
    const-string v0, "os_api_level"

    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/ee;->b:Lcom/yandex/metrica/impl/ob/el;

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/ob/el;->l()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1338
    const-string v0, "is_rooted"

    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/ee;->b:Lcom/yandex/metrica/impl/ob/el;

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/ob/el;->r()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1339
    const-string v0, "app_framework"

    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/ee;->b:Lcom/yandex/metrica/impl/ob/el;

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/ob/el;->s()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1340
    const-string v0, "app_id"

    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/ee;->b:Lcom/yandex/metrica/impl/ob/el;

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/ob/el;->I()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1341
    const-string v0, "app_platform"

    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/ee;->b:Lcom/yandex/metrica/impl/ob/el;

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/ob/el;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1342
    const-string v0, "android_id"

    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/ee;->b:Lcom/yandex/metrica/impl/ob/el;

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/ob/el;->z()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1343
    const-string v0, "request_id"

    iget-wide v2, p0, Lcom/yandex/metrica/impl/ob/ee;->r:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1344
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ee;->b:Lcom/yandex/metrica/impl/ob/el;

    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/ee;->a:Landroid/content/Context;

    invoke-virtual {v0, v2}, Lcom/yandex/metrica/impl/ob/el;->a(Landroid/content/Context;)Lcom/yandex/metrica/impl/GoogleAdvertisingIdGetter$a;

    move-result-object v2

    .line 1345
    if-nez v2, :cond_1

    const-string v0, ""

    .line 1346
    :goto_0
    const-string v3, "adv_id"

    if-nez v0, :cond_0

    const-string v0, ""

    :cond_0
    invoke-virtual {v1, v3, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1347
    const-string v3, "limit_ad_tracking"

    if-nez v2, :cond_2

    const-string v0, ""

    :goto_1
    invoke-virtual {v1, v3, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 1161
    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1163
    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/ob/ee;->a(Ljava/lang/String;)V

    .line 115
    return-void

    .line 1345
    :cond_1
    iget-object v0, v2, Lcom/yandex/metrica/impl/GoogleAdvertisingIdGetter$a;->a:Ljava/lang/String;

    goto :goto_0

    .line 1347
    :cond_2
    iget-object v0, v2, Lcom/yandex/metrica/impl/GoogleAdvertisingIdGetter$a;->b:Ljava/lang/Boolean;

    .line 1348
    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/ob/ee;->a(Ljava/lang/Boolean;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public u()Z
    .locals 3

    .prologue
    .line 152
    invoke-super {p0}, Lcom/yandex/metrica/impl/ak;->u()Z

    move-result v1

    .line 153
    const/16 v0, 0x190

    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/ee;->k()I

    move-result v2

    if-eq v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    and-int/2addr v0, v1

    .line 154
    return v0

    .line 153
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public y()V
    .locals 4

    .prologue
    .line 138
    iget-wide v0, p0, Lcom/yandex/metrica/impl/ob/ee;->s:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 140
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ee;->m:Lcom/yandex/metrica/impl/ob/ct;

    iget-wide v2, p0, Lcom/yandex/metrica/impl/ob/ee;->s:J

    invoke-virtual {v0, v2, v3}, Lcom/yandex/metrica/impl/ob/ct;->c(J)I

    .line 142
    :cond_0
    return-void
.end method
