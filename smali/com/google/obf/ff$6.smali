.class Lcom/google/obf/ff$6;
.super Ljava/lang/Object;
.source "IMASDK"

# interfaces
.implements Lcom/google/obf/fk;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/obf/ff;->b(Ljava/lang/reflect/Type;Ljava/lang/Class;)Lcom/google/obf/fk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/obf/fk",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Ljava/lang/Class;

.field final synthetic b:Ljava/lang/reflect/Type;

.field final synthetic c:Lcom/google/obf/ff;

.field private final d:Lcom/google/obf/fn;


# direct methods
.method constructor <init>(Lcom/google/obf/ff;Ljava/lang/Class;Ljava/lang/reflect/Type;)V
    .locals 1

    .prologue
    .line 1
    iput-object p1, p0, Lcom/google/obf/ff$6;->c:Lcom/google/obf/ff;

    iput-object p2, p0, Lcom/google/obf/ff$6;->a:Ljava/lang/Class;

    iput-object p3, p0, Lcom/google/obf/ff$6;->b:Ljava/lang/reflect/Type;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-static {}, Lcom/google/obf/fn;->a()Lcom/google/obf/fn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/obf/ff$6;->d:Lcom/google/obf/fn;

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 3
    :try_start_0
    iget-object v0, p0, Lcom/google/obf/ff$6;->d:Lcom/google/obf/fn;

    iget-object v1, p0, Lcom/google/obf/ff$6;->a:Ljava/lang/Class;

    invoke-virtual {v0, v1}, Lcom/google/obf/fn;->a(Ljava/lang/Class;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 4
    return-object v0

    .line 5
    :catch_0
    move-exception v0

    .line 6
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to invoke no-args constructor for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/obf/ff$6;->b:Ljava/lang/reflect/Type;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ". "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "Register an InstanceCreator with Gson for this type may fix this problem."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method
