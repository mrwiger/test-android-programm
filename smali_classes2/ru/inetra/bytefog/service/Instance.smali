.class public interface abstract Lru/inetra/bytefog/service/Instance;
.super Ljava/lang/Object;
.source "Instance.java"


# virtual methods
.method public abstract cancelAllRequests()V
.end method

.method public abstract convertBytefogMagnetToHlsPlaylistUri(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract isReady()Z
.end method

.method public abstract isStarting()Z
.end method

.method public abstract setListener(Lru/inetra/bytefog/service/InstanceListener;)V
.end method

.method public abstract start()V
.end method
