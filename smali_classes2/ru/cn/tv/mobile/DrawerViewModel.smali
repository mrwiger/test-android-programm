.class final Lru/cn/tv/mobile/DrawerViewModel;
.super Lru/cn/mvvm/RxViewModel;
.source "DrawerViewModel.java"


# instance fields
.field private final loader:Lru/cn/mvvm/RxLoader;

.field private final menuItems:Landroid/arch/lifecycle/MutableLiveData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/arch/lifecycle/MutableLiveData",
            "<",
            "Landroid/util/Pair",
            "<",
            "Lru/cn/api/catalogue/replies/Rubricator;",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lru/cn/mvvm/RxLoader;)V
    .locals 3
    .param p1, "loader"    # Lru/cn/mvvm/RxLoader;

    .prologue
    .line 25
    invoke-direct {p0}, Lru/cn/mvvm/RxViewModel;-><init>()V

    .line 26
    iput-object p1, p0, Lru/cn/tv/mobile/DrawerViewModel;->loader:Lru/cn/mvvm/RxLoader;

    .line 27
    new-instance v0, Landroid/arch/lifecycle/MutableLiveData;

    invoke-direct {v0}, Landroid/arch/lifecycle/MutableLiveData;-><init>()V

    iput-object v0, p0, Lru/cn/tv/mobile/DrawerViewModel;->menuItems:Landroid/arch/lifecycle/MutableLiveData;

    .line 30
    invoke-direct {p0}, Lru/cn/tv/mobile/DrawerViewModel;->rubrics()Lio/reactivex/Observable;

    move-result-object v0

    .line 31
    invoke-direct {p0}, Lru/cn/tv/mobile/DrawerViewModel;->intersections()Lio/reactivex/Observable;

    move-result-object v1

    sget-object v2, Lru/cn/tv/mobile/DrawerViewModel$$Lambda$0;->$instance:Lio/reactivex/functions/BiFunction;

    .line 29
    invoke-static {v0, v1, v2}, Lio/reactivex/Observable;->combineLatest(Lio/reactivex/ObservableSource;Lio/reactivex/ObservableSource;Lio/reactivex/functions/BiFunction;)Lio/reactivex/Observable;

    move-result-object v0

    .line 45
    invoke-static {}, Lio/reactivex/android/schedulers/AndroidSchedulers;->mainThread()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lru/cn/tv/mobile/DrawerViewModel$$Lambda$1;

    invoke-direct {v1, p0}, Lru/cn/tv/mobile/DrawerViewModel$$Lambda$1;-><init>(Lru/cn/tv/mobile/DrawerViewModel;)V

    new-instance v2, Lru/cn/tv/mobile/DrawerViewModel$$Lambda$2;

    invoke-direct {v2, p0}, Lru/cn/tv/mobile/DrawerViewModel$$Lambda$2;-><init>(Lru/cn/tv/mobile/DrawerViewModel;)V

    .line 46
    invoke-virtual {v0, v1, v2}, Lio/reactivex/Observable;->subscribe(Lio/reactivex/functions/Consumer;Lio/reactivex/functions/Consumer;)Lio/reactivex/disposables/Disposable;

    move-result-object v0

    .line 29
    invoke-virtual {p0, v0}, Lru/cn/tv/mobile/DrawerViewModel;->bind(Lio/reactivex/disposables/Disposable;)V

    .line 53
    return-void
.end method

.method private intersections()Lio/reactivex/Observable;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 67
    const-string v0, "intersections"

    .line 68
    .local v0, "selection":Ljava/lang/String;
    invoke-static {}, Lru/cn/api/provider/TvContentProviderContract;->channels()Landroid/net/Uri;

    move-result-object v1

    .line 70
    .local v1, "uri":Landroid/net/Uri;
    iget-object v2, p0, Lru/cn/tv/mobile/DrawerViewModel;->loader:Lru/cn/mvvm/RxLoader;

    invoke-virtual {v2, v1, v0}, Lru/cn/mvvm/RxLoader;->query(Landroid/net/Uri;Ljava/lang/String;)Lio/reactivex/Observable;

    move-result-object v2

    .line 71
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v2

    .line 70
    return-object v2
.end method

.method static final synthetic lambda$new$0$DrawerViewModel(Landroid/database/Cursor;Landroid/database/Cursor;)Landroid/util/Pair;
    .locals 5
    .param p0, "rubricsCursor"    # Landroid/database/Cursor;
    .param p1, "intersectionsCursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 34
    if-eqz p0, :cond_0

    invoke-interface {p0}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-ge v3, v0, :cond_1

    .line 35
    :cond_0
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "Rubrics is not available"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 38
    :cond_1
    invoke-interface {p0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 39
    const-string v3, "data"

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 40
    .local v2, "s":Ljava/lang/String;
    invoke-static {v2}, Lru/cn/api/catalogue/replies/Rubricator;->fromJson(Ljava/lang/String;)Lru/cn/api/catalogue/replies/Rubricator;

    move-result-object v1

    .line 42
    .local v1, "rubricator":Lru/cn/api/catalogue/replies/Rubricator;
    if-eqz p1, :cond_2

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-lez v3, :cond_2

    .line 43
    .local v0, "intersectionsAvailable":Z
    :goto_0
    new-instance v3, Landroid/util/Pair;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-direct {v3, v1, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v3

    .line 42
    .end local v0    # "intersectionsAvailable":Z
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private rubrics()Lio/reactivex/Observable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 60
    invoke-static {}, Lru/cn/api/provider/TvContentProviderContract;->rubricatorUri()Landroid/net/Uri;

    move-result-object v0

    .line 62
    .local v0, "uri":Landroid/net/Uri;
    iget-object v1, p0, Lru/cn/tv/mobile/DrawerViewModel;->loader:Lru/cn/mvvm/RxLoader;

    invoke-virtual {v1, v0}, Lru/cn/mvvm/RxLoader;->query(Landroid/net/Uri;)Lio/reactivex/Observable;

    move-result-object v1

    .line 63
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/reactivex/Observable;->subscribeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v1

    .line 62
    return-object v1
.end method


# virtual methods
.method final synthetic lambda$new$1$DrawerViewModel(Landroid/util/Pair;)V
    .locals 1
    .param p1, "rubricatorBooleanPair"    # Landroid/util/Pair;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 48
    iget-object v0, p0, Lru/cn/tv/mobile/DrawerViewModel;->menuItems:Landroid/arch/lifecycle/MutableLiveData;

    invoke-virtual {v0, p1}, Landroid/arch/lifecycle/MutableLiveData;->setValue(Ljava/lang/Object;)V

    .line 49
    return-void
.end method

.method final synthetic lambda$new$2$DrawerViewModel(Ljava/lang/Throwable;)V
    .locals 2
    .param p1, "throwable"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 51
    iget-object v0, p0, Lru/cn/tv/mobile/DrawerViewModel;->menuItems:Landroid/arch/lifecycle/MutableLiveData;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/arch/lifecycle/MutableLiveData;->setValue(Ljava/lang/Object;)V

    .line 52
    return-void
.end method

.method menuItems()Landroid/arch/lifecycle/LiveData;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/arch/lifecycle/LiveData",
            "<",
            "Landroid/util/Pair",
            "<",
            "Lru/cn/api/catalogue/replies/Rubricator;",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 56
    iget-object v0, p0, Lru/cn/tv/mobile/DrawerViewModel;->menuItems:Landroid/arch/lifecycle/MutableLiveData;

    return-object v0
.end method
