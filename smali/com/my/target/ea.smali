.class public final Lcom/my/target/ea;
.super Landroid/widget/LinearLayout;
.source "BodyView.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation


# instance fields
.field private final aP:Landroid/widget/TextView;

.field private final aQ:Landroid/widget/TextView;

.field private final aR:Landroid/widget/TextView;

.field private final aS:Landroid/widget/LinearLayout;

.field private final aT:Landroid/widget/TextView;

.field private final aU:Lcom/my/target/ca;

.field private final aV:Landroid/widget/TextView;

.field private final aW:Z

.field private final aX:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private aY:Landroid/widget/LinearLayout$LayoutParams;

.field private aZ:Landroid/widget/LinearLayout$LayoutParams;

.field private final aw:Lcom/my/target/cm;

.field private ba:Landroid/widget/LinearLayout$LayoutParams;

.field private bb:Landroid/widget/LinearLayout$LayoutParams;

.field private bc:Landroid/widget/LinearLayout$LayoutParams;

.field private bd:Landroid/widget/LinearLayout$LayoutParams;

.field private be:Landroid/view/View$OnClickListener;

.field private navigationType:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/my/target/cm;Z)V
    .locals 2

    .prologue
    .line 57
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 45
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/my/target/ea;->aX:Ljava/util/HashMap;

    .line 58
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/ea;->aP:Landroid/widget/TextView;

    .line 59
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/ea;->aQ:Landroid/widget/TextView;

    .line 60
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/ea;->aR:Landroid/widget/TextView;

    .line 61
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/ea;->aS:Landroid/widget/LinearLayout;

    .line 62
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/ea;->aT:Landroid/widget/TextView;

    .line 63
    new-instance v0, Lcom/my/target/ca;

    invoke-direct {v0, p1}, Lcom/my/target/ca;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/ea;->aU:Lcom/my/target/ca;

    .line 64
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/ea;->aV:Landroid/widget/TextView;

    .line 66
    iget-object v0, p0, Lcom/my/target/ea;->aP:Landroid/widget/TextView;

    const-string v1, "title_text"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 67
    iget-object v0, p0, Lcom/my/target/ea;->aR:Landroid/widget/TextView;

    const-string v1, "description_text"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 68
    iget-object v0, p0, Lcom/my/target/ea;->aT:Landroid/widget/TextView;

    const-string v1, "disclaimer_text"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 69
    iget-object v0, p0, Lcom/my/target/ea;->aU:Lcom/my/target/ca;

    const-string v1, "stars_view"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 70
    iget-object v0, p0, Lcom/my/target/ea;->aV:Landroid/widget/TextView;

    const-string v1, "votes_text"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 72
    iput-object p2, p0, Lcom/my/target/ea;->aw:Lcom/my/target/cm;

    .line 73
    iput-boolean p3, p0, Lcom/my/target/ea;->aW:Z

    .line 74
    return-void
.end method


# virtual methods
.method public final a(Lcom/my/target/af;Landroid/view/View$OnClickListener;)V
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ClickableViewAccessibility"
        }
    .end annotation

    .prologue
    .line 168
    iget-boolean v0, p1, Lcom/my/target/af;->cE:Z

    if-eqz v0, :cond_0

    .line 170
    invoke-virtual {p0, p2}, Lcom/my/target/ea;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 171
    const/4 v0, -0x1

    const v1, -0x3a1508

    invoke-static {p0, v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;II)V

    .line 196
    :goto_0
    return-void

    .line 174
    :cond_0
    iput-object p2, p0, Lcom/my/target/ea;->be:Landroid/view/View$OnClickListener;

    .line 176
    iget-object v0, p0, Lcom/my/target/ea;->aP:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 177
    iget-object v0, p0, Lcom/my/target/ea;->aQ:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 178
    iget-object v0, p0, Lcom/my/target/ea;->aR:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 179
    iget-object v0, p0, Lcom/my/target/ea;->aU:Lcom/my/target/ca;

    invoke-virtual {v0, p0}, Lcom/my/target/ca;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 180
    iget-object v0, p0, Lcom/my/target/ea;->aV:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 181
    invoke-virtual {p0, p0}, Lcom/my/target/ea;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 183
    iget-object v0, p0, Lcom/my/target/ea;->aX:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/my/target/ea;->aP:Landroid/widget/TextView;

    iget-boolean v2, p1, Lcom/my/target/af;->cs:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 184
    const-string v0, "store"

    iget-object v1, p0, Lcom/my/target/ea;->navigationType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 186
    iget-object v0, p0, Lcom/my/target/ea;->aX:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/my/target/ea;->aQ:Landroid/widget/TextView;

    iget-boolean v2, p1, Lcom/my/target/af;->cC:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 192
    :goto_1
    iget-object v0, p0, Lcom/my/target/ea;->aX:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/my/target/ea;->aR:Landroid/widget/TextView;

    iget-boolean v2, p1, Lcom/my/target/af;->ct:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 193
    iget-object v0, p0, Lcom/my/target/ea;->aX:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/my/target/ea;->aU:Lcom/my/target/ca;

    iget-boolean v2, p1, Lcom/my/target/af;->cw:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 194
    iget-object v0, p0, Lcom/my/target/ea;->aX:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/my/target/ea;->aV:Landroid/widget/TextView;

    iget-boolean v2, p1, Lcom/my/target/af;->cx:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 195
    iget-object v0, p0, Lcom/my/target/ea;->aX:Ljava/util/HashMap;

    iget-boolean v1, p1, Lcom/my/target/af;->cD:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 190
    :cond_1
    iget-object v0, p0, Lcom/my/target/ea;->aX:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/my/target/ea;->aQ:Landroid/widget/TextView;

    iget-boolean v2, p1, Lcom/my/target/af;->cB:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method final e(Z)V
    .locals 8

    .prologue
    const/16 v3, 0x8

    const/16 v7, 0x10

    const/4 v6, 0x4

    const/4 v5, 0x1

    const/4 v4, -0x2

    .line 232
    iget-object v0, p0, Lcom/my/target/ea;->aP:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setGravity(I)V

    .line 233
    iget-object v0, p0, Lcom/my/target/ea;->aP:Landroid/widget/TextView;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 234
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v4, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iput-object v0, p0, Lcom/my/target/ea;->aY:Landroid/widget/LinearLayout$LayoutParams;

    .line 236
    iget-object v0, p0, Lcom/my/target/ea;->aY:Landroid/widget/LinearLayout$LayoutParams;

    iput v5, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 237
    iget-object v0, p0, Lcom/my/target/ea;->aY:Landroid/widget/LinearLayout$LayoutParams;

    iget-object v1, p0, Lcom/my/target/ea;->aw:Lcom/my/target/cm;

    invoke-virtual {v1, v3}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 238
    iget-object v0, p0, Lcom/my/target/ea;->aY:Landroid/widget/LinearLayout$LayoutParams;

    iget-object v1, p0, Lcom/my/target/ea;->aw:Lcom/my/target/cm;

    invoke-virtual {v1, v3}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 239
    if-eqz p1, :cond_0

    .line 241
    iget-object v0, p0, Lcom/my/target/ea;->aY:Landroid/widget/LinearLayout$LayoutParams;

    iget-object v1, p0, Lcom/my/target/ea;->aw:Lcom/my/target/cm;

    invoke-virtual {v1, v6}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 247
    :goto_0
    iget-object v0, p0, Lcom/my/target/ea;->aP:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/my/target/ea;->aY:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 249
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v4, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iput-object v0, p0, Lcom/my/target/ea;->aZ:Landroid/widget/LinearLayout$LayoutParams;

    .line 251
    iget-object v0, p0, Lcom/my/target/ea;->aZ:Landroid/widget/LinearLayout$LayoutParams;

    iput v5, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 252
    iget-object v0, p0, Lcom/my/target/ea;->aQ:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/my/target/ea;->aZ:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 254
    iget-object v0, p0, Lcom/my/target/ea;->aR:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setGravity(I)V

    .line 255
    iget-object v0, p0, Lcom/my/target/ea;->aR:Landroid/widget/TextView;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 256
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v4, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iput-object v0, p0, Lcom/my/target/ea;->ba:Landroid/widget/LinearLayout$LayoutParams;

    .line 258
    if-eqz p1, :cond_1

    .line 260
    iget-object v0, p0, Lcom/my/target/ea;->ba:Landroid/widget/LinearLayout$LayoutParams;

    iget-object v1, p0, Lcom/my/target/ea;->aw:Lcom/my/target/cm;

    invoke-virtual {v1, v6}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 267
    :goto_1
    iget-object v0, p0, Lcom/my/target/ea;->ba:Landroid/widget/LinearLayout$LayoutParams;

    iput v5, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 268
    if-eqz p1, :cond_2

    .line 270
    iget-object v0, p0, Lcom/my/target/ea;->ba:Landroid/widget/LinearLayout$LayoutParams;

    iget-object v1, p0, Lcom/my/target/ea;->aw:Lcom/my/target/cm;

    invoke-virtual {v1, v6}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 271
    iget-object v0, p0, Lcom/my/target/ea;->ba:Landroid/widget/LinearLayout$LayoutParams;

    iget-object v1, p0, Lcom/my/target/ea;->aw:Lcom/my/target/cm;

    invoke-virtual {v1, v6}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 278
    :goto_2
    iget-object v0, p0, Lcom/my/target/ea;->aR:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/my/target/ea;->ba:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 280
    iget-object v0, p0, Lcom/my/target/ea;->aS:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 281
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v4, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iput-object v0, p0, Lcom/my/target/ea;->bc:Landroid/widget/LinearLayout$LayoutParams;

    .line 283
    iget-object v0, p0, Lcom/my/target/ea;->bc:Landroid/widget/LinearLayout$LayoutParams;

    iput v5, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 284
    iget-object v0, p0, Lcom/my/target/ea;->aS:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/my/target/ea;->bc:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 286
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v1, p0, Lcom/my/target/ea;->aw:Lcom/my/target/cm;

    const/16 v2, 0x49

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iget-object v2, p0, Lcom/my/target/ea;->aw:Lcom/my/target/cm;

    const/16 v3, 0xc

    invoke-virtual {v2, v3}, Lcom/my/target/cm;->n(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iput-object v0, p0, Lcom/my/target/ea;->bb:Landroid/widget/LinearLayout$LayoutParams;

    .line 287
    iget-object v0, p0, Lcom/my/target/ea;->bb:Landroid/widget/LinearLayout$LayoutParams;

    iget-object v1, p0, Lcom/my/target/ea;->aw:Lcom/my/target/cm;

    invoke-virtual {v1, v6}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 288
    iget-object v0, p0, Lcom/my/target/ea;->bb:Landroid/widget/LinearLayout$LayoutParams;

    iget-object v1, p0, Lcom/my/target/ea;->aw:Lcom/my/target/cm;

    invoke-virtual {v1, v6}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 289
    iget-object v0, p0, Lcom/my/target/ea;->aU:Lcom/my/target/ca;

    iget-object v1, p0, Lcom/my/target/ea;->bb:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v0, v1}, Lcom/my/target/ca;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 291
    iget-object v0, p0, Lcom/my/target/ea;->aV:Landroid/widget/TextView;

    const v1, -0x666667

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 292
    iget-object v0, p0, Lcom/my/target/ea;->aV:Landroid/widget/TextView;

    const/4 v1, 0x2

    const/high16 v2, 0x41600000    # 14.0f

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 294
    iget-object v0, p0, Lcom/my/target/ea;->aT:Landroid/widget/TextView;

    const v1, -0x666667

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 295
    iget-object v0, p0, Lcom/my/target/ea;->aT:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setGravity(I)V

    .line 296
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v4, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iput-object v0, p0, Lcom/my/target/ea;->bd:Landroid/widget/LinearLayout$LayoutParams;

    .line 298
    iget-object v0, p0, Lcom/my/target/ea;->bd:Landroid/widget/LinearLayout$LayoutParams;

    iput v5, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 299
    if-eqz p1, :cond_3

    .line 301
    iget-object v0, p0, Lcom/my/target/ea;->bd:Landroid/widget/LinearLayout$LayoutParams;

    iget-object v1, p0, Lcom/my/target/ea;->aw:Lcom/my/target/cm;

    invoke-virtual {v1, v6}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 302
    iget-object v0, p0, Lcom/my/target/ea;->bd:Landroid/widget/LinearLayout$LayoutParams;

    iget-object v1, p0, Lcom/my/target/ea;->aw:Lcom/my/target/cm;

    invoke-virtual {v1, v6}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 309
    :goto_3
    iget-object v0, p0, Lcom/my/target/ea;->bd:Landroid/widget/LinearLayout$LayoutParams;

    iput v5, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 310
    iget-object v0, p0, Lcom/my/target/ea;->aT:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/my/target/ea;->bd:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 312
    iget-object v0, p0, Lcom/my/target/ea;->aP:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/my/target/ea;->addView(Landroid/view/View;)V

    .line 313
    iget-object v0, p0, Lcom/my/target/ea;->aQ:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/my/target/ea;->addView(Landroid/view/View;)V

    .line 314
    iget-object v0, p0, Lcom/my/target/ea;->aS:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v0}, Lcom/my/target/ea;->addView(Landroid/view/View;)V

    .line 315
    iget-object v0, p0, Lcom/my/target/ea;->aR:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/my/target/ea;->addView(Landroid/view/View;)V

    .line 316
    iget-object v0, p0, Lcom/my/target/ea;->aT:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/my/target/ea;->addView(Landroid/view/View;)V

    .line 318
    iget-object v0, p0, Lcom/my/target/ea;->aS:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/my/target/ea;->aU:Lcom/my/target/ca;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 319
    iget-object v0, p0, Lcom/my/target/ea;->aS:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/my/target/ea;->aV:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 320
    return-void

    .line 245
    :cond_0
    iget-object v0, p0, Lcom/my/target/ea;->aY:Landroid/widget/LinearLayout$LayoutParams;

    iget-object v1, p0, Lcom/my/target/ea;->aw:Lcom/my/target/cm;

    const/16 v2, 0x20

    invoke-virtual {v1, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    goto/16 :goto_0

    .line 264
    :cond_1
    iget-object v0, p0, Lcom/my/target/ea;->ba:Landroid/widget/LinearLayout$LayoutParams;

    iget-object v1, p0, Lcom/my/target/ea;->aw:Lcom/my/target/cm;

    invoke-virtual {v1, v3}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    goto/16 :goto_1

    .line 275
    :cond_2
    iget-object v0, p0, Lcom/my/target/ea;->ba:Landroid/widget/LinearLayout$LayoutParams;

    iget-object v1, p0, Lcom/my/target/ea;->aw:Lcom/my/target/cm;

    invoke-virtual {v1, v7}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 276
    iget-object v0, p0, Lcom/my/target/ea;->ba:Landroid/widget/LinearLayout$LayoutParams;

    iget-object v1, p0, Lcom/my/target/ea;->aw:Lcom/my/target/cm;

    invoke-virtual {v1, v7}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    goto/16 :goto_2

    .line 306
    :cond_3
    iget-object v0, p0, Lcom/my/target/ea;->bd:Landroid/widget/LinearLayout$LayoutParams;

    iget-object v1, p0, Lcom/my/target/ea;->aw:Lcom/my/target/cm;

    invoke-virtual {v1, v7}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 307
    iget-object v0, p0, Lcom/my/target/ea;->bd:Landroid/widget/LinearLayout$LayoutParams;

    iget-object v1, p0, Lcom/my/target/ea;->aw:Lcom/my/target/cm;

    invoke-virtual {v1, v7}, Lcom/my/target/cm;->n(I)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    goto :goto_3
.end method

.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x1

    const/4 v2, -0x1

    .line 201
    iget-object v0, p0, Lcom/my/target/ea;->aX:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 203
    const/4 v0, 0x0

    .line 227
    :goto_0
    return v0

    .line 205
    :cond_0
    iget-object v0, p0, Lcom/my/target/ea;->aX:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 207
    goto :goto_0

    .line 209
    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_2
    :goto_1
    :pswitch_0
    move v0, v1

    .line 227
    goto :goto_0

    .line 212
    :pswitch_1
    const v0, -0x3a1508

    invoke-virtual {p0, v0}, Lcom/my/target/ea;->setBackgroundColor(I)V

    goto :goto_1

    .line 215
    :pswitch_2
    invoke-virtual {p0, v2}, Lcom/my/target/ea;->setBackgroundColor(I)V

    .line 216
    iget-object v0, p0, Lcom/my/target/ea;->be:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_2

    .line 218
    iget-object v0, p0, Lcom/my/target/ea;->be:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    goto :goto_1

    .line 222
    :pswitch_3
    invoke-virtual {p0, v2}, Lcom/my/target/ea;->setBackgroundColor(I)V

    goto :goto_1

    .line 209
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public final setBanner(Lcom/my/target/core/models/banners/h;)V
    .locals 9
    .param p1, "banner"    # Lcom/my/target/core/models/banners/h;

    .prologue
    const/high16 v8, 0x41900000    # 18.0f

    const/high16 v7, 0x41800000    # 16.0f

    const/4 v6, 0x0

    const/16 v5, 0x8

    const/4 v4, 0x2

    .line 78
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getNavigationType()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/ea;->navigationType:Ljava/lang/String;

    .line 79
    iget-object v0, p0, Lcom/my/target/ea;->aP:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 80
    iget-object v0, p0, Lcom/my/target/ea;->aR:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getDescription()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 81
    iget-object v0, p0, Lcom/my/target/ea;->aU:Lcom/my/target/ca;

    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getRating()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/my/target/ca;->setRating(F)V

    .line 82
    iget-object v0, p0, Lcom/my/target/ea;->aV:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getVotes()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 84
    const-string v0, "store"

    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getNavigationType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 86
    iget-object v0, p0, Lcom/my/target/ea;->aQ:Landroid/widget/TextView;

    const-string v1, "category_text"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 88
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getCategory()Ljava/lang/String;

    move-result-object v1

    .line 89
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getSubCategory()Ljava/lang/String;

    move-result-object v2

    .line 91
    const-string v0, ""

    .line 92
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 94
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 97
    :cond_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 99
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 102
    :cond_1
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 104
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 107
    :cond_2
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 109
    iget-object v1, p0, Lcom/my/target/ea;->aQ:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 110
    iget-object v0, p0, Lcom/my/target/ea;->aQ:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 117
    :goto_0
    iget-object v0, p0, Lcom/my/target/ea;->aS:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 119
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getVotes()I

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getRating()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_4

    .line 121
    iget-object v0, p0, Lcom/my/target/ea;->aS:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 127
    :goto_1
    iget-object v0, p0, Lcom/my/target/ea;->aQ:Landroid/widget/TextView;

    const v1, -0x333334

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 139
    :goto_2
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getDisclaimer()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 141
    iget-object v0, p0, Lcom/my/target/ea;->aT:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 149
    :goto_3
    iget-boolean v0, p0, Lcom/my/target/ea;->aW:Z

    if-eqz v0, :cond_7

    .line 151
    iget-object v0, p0, Lcom/my/target/ea;->aP:Landroid/widget/TextView;

    const/high16 v1, 0x42000000    # 32.0f

    invoke-virtual {v0, v4, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 152
    iget-object v0, p0, Lcom/my/target/ea;->aR:Landroid/widget/TextView;

    const/high16 v1, 0x41c00000    # 24.0f

    invoke-virtual {v0, v4, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 153
    iget-object v0, p0, Lcom/my/target/ea;->aT:Landroid/widget/TextView;

    invoke-virtual {v0, v4, v8}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 154
    iget-object v0, p0, Lcom/my/target/ea;->aQ:Landroid/widget/TextView;

    invoke-virtual {v0, v4, v8}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 163
    :goto_4
    return-void

    .line 114
    :cond_3
    iget-object v0, p0, Lcom/my/target/ea;->aQ:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 125
    :cond_4
    iget-object v0, p0, Lcom/my/target/ea;->aS:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_1

    .line 131
    :cond_5
    iget-object v0, p0, Lcom/my/target/ea;->aQ:Landroid/widget/TextView;

    const-string v1, "domain_text"

    invoke-static {v0, v1}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 132
    iget-object v0, p0, Lcom/my/target/ea;->aS:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 133
    iget-object v0, p0, Lcom/my/target/ea;->aQ:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getDomain()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 135
    iget-object v0, p0, Lcom/my/target/ea;->aS:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 136
    iget-object v0, p0, Lcom/my/target/ea;->aQ:Landroid/widget/TextView;

    const v1, -0xff540e

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_2

    .line 145
    :cond_6
    iget-object v0, p0, Lcom/my/target/ea;->aT:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 146
    iget-object v0, p0, Lcom/my/target/ea;->aT:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getDisclaimer()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 158
    :cond_7
    iget-object v0, p0, Lcom/my/target/ea;->aP:Landroid/widget/TextView;

    const/high16 v1, 0x41a00000    # 20.0f

    invoke-virtual {v0, v4, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 159
    iget-object v0, p0, Lcom/my/target/ea;->aR:Landroid/widget/TextView;

    invoke-virtual {v0, v4, v7}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 160
    iget-object v0, p0, Lcom/my/target/ea;->aT:Landroid/widget/TextView;

    const/high16 v1, 0x41600000    # 14.0f

    invoke-virtual {v0, v4, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 161
    iget-object v0, p0, Lcom/my/target/ea;->aQ:Landroid/widget/TextView;

    invoke-virtual {v0, v4, v7}, Landroid/widget/TextView;->setTextSize(IF)V

    goto :goto_4
.end method
