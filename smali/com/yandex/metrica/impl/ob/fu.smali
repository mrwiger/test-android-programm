.class public final Lcom/yandex/metrica/impl/ob/fu;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/Object;

.field private static volatile b:Lcom/yandex/metrica/impl/ob/fu;


# instance fields
.field private c:Lcom/yandex/metrica/impl/ob/fs;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/yandex/metrica/impl/ob/fu;->a:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v0, Lcom/yandex/metrica/impl/ob/fs;

    invoke-direct {v0, p1}, Lcom/yandex/metrica/impl/ob/fs;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/fu;->c:Lcom/yandex/metrica/impl/ob/fs;

    .line 36
    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/yandex/metrica/impl/ob/fu;
    .locals 2

    .prologue
    .line 22
    sget-object v0, Lcom/yandex/metrica/impl/ob/fu;->b:Lcom/yandex/metrica/impl/ob/fu;

    if-nez v0, :cond_1

    .line 23
    sget-object v1, Lcom/yandex/metrica/impl/ob/fu;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 24
    :try_start_0
    sget-object v0, Lcom/yandex/metrica/impl/ob/fu;->b:Lcom/yandex/metrica/impl/ob/fu;

    if-nez v0, :cond_0

    .line 25
    new-instance v0, Lcom/yandex/metrica/impl/ob/fu;

    invoke-direct {v0, p0}, Lcom/yandex/metrica/impl/ob/fu;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/yandex/metrica/impl/ob/fu;->b:Lcom/yandex/metrica/impl/ob/fu;

    .line 27
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 29
    :cond_1
    sget-object v0, Lcom/yandex/metrica/impl/ob/fu;->b:Lcom/yandex/metrica/impl/ob/fu;

    return-object v0

    .line 27
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fu;->c:Lcom/yandex/metrica/impl/ob/fs;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fs;->b()V

    .line 40
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fu;->c:Lcom/yandex/metrica/impl/ob/fs;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fs;->a()V

    .line 44
    return-void
.end method
