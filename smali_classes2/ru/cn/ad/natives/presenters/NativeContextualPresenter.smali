.class public Lru/cn/ad/natives/presenters/NativeContextualPresenter;
.super Ljava/lang/Object;
.source "NativeContextualPresenter.java"

# interfaces
.implements Lru/cn/ad/natives/adapters/NativeAdAdapter$Presenter;


# instance fields
.field private adView:Landroid/view/View;

.field private final containerView:Landroid/view/ViewGroup;

.field private final handler:Landroid/os/Handler;

.field private final templateView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "container"    # Landroid/view/ViewGroup;

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p2, p0, Lru/cn/ad/natives/presenters/NativeContextualPresenter;->containerView:Landroid/view/ViewGroup;

    .line 32
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lru/cn/ad/natives/presenters/NativeContextualPresenter;->handler:Landroid/os/Handler;

    .line 34
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 35
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v1, 0x7f0c00a9

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lru/cn/ad/natives/presenters/NativeContextualPresenter;->templateView:Landroid/view/View;

    .line 36
    return-void
.end method

.method private bindView(Lru/cn/ad/natives/adapters/NativeAdAdapter$ViewBinder;Landroid/widget/ImageView;I)V
    .locals 2
    .param p1, "binder"    # Lru/cn/ad/natives/adapters/NativeAdAdapter$ViewBinder;
    .param p2, "view"    # Landroid/widget/ImageView;
    .param p3, "type"    # I

    .prologue
    .line 100
    invoke-interface {p1, p2, p3}, Lru/cn/ad/natives/adapters/NativeAdAdapter$ViewBinder;->bind(Landroid/widget/ImageView;I)Z

    move-result v0

    .line 101
    .local v0, "binded":Z
    if-eqz v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 102
    return-void

    .line 101
    :cond_0
    const/16 v1, 0x8

    goto :goto_0
.end method

.method private bindView(Lru/cn/ad/natives/adapters/NativeAdAdapter$ViewBinder;Landroid/widget/TextView;I)V
    .locals 2
    .param p1, "binder"    # Lru/cn/ad/natives/adapters/NativeAdAdapter$ViewBinder;
    .param p2, "view"    # Landroid/widget/TextView;
    .param p3, "type"    # I

    .prologue
    .line 105
    invoke-interface {p1, p2, p3}, Lru/cn/ad/natives/adapters/NativeAdAdapter$ViewBinder;->bind(Landroid/widget/TextView;I)Z

    move-result v0

    .line 106
    .local v0, "binded":Z
    if-eqz v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p2, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 107
    return-void

    .line 106
    :cond_0
    const/16 v1, 0x8

    goto :goto_0
.end method

.method static final synthetic lambda$show$0$NativeContextualPresenter(Ljava/lang/Runnable;Landroid/view/View;)V
    .locals 0
    .param p0, "closeRunnable"    # Ljava/lang/Runnable;
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 76
    invoke-interface {p0}, Ljava/lang/Runnable;->run()V

    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 2

    .prologue
    .line 90
    iget-object v0, p0, Lru/cn/ad/natives/presenters/NativeContextualPresenter;->handler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 91
    iget-object v0, p0, Lru/cn/ad/natives/presenters/NativeContextualPresenter;->containerView:Landroid/view/ViewGroup;

    iget-object v1, p0, Lru/cn/ad/natives/presenters/NativeContextualPresenter;->adView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 92
    return-void
.end method

.method public presentingView()Landroid/view/View;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lru/cn/ad/natives/presenters/NativeContextualPresenter;->adView:Landroid/view/View;

    return-object v0
.end method

.method public show(Lru/cn/ad/natives/adapters/NativeAdAdapter$ViewBinder;Ljava/lang/Runnable;)V
    .locals 16
    .param p1, "binder"    # Lru/cn/ad/natives/adapters/NativeAdAdapter$ViewBinder;
    .param p2, "closeRunnable"    # Ljava/lang/Runnable;

    .prologue
    .line 40
    move-object/from16 v0, p0

    iget-object v13, v0, Lru/cn/ad/natives/presenters/NativeContextualPresenter;->templateView:Landroid/view/View;

    const v14, 0x7f090115

    invoke-virtual {v13, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/ImageView;

    .line 41
    .local v10, "logoView":Landroid/widget/ImageView;
    const/4 v13, 0x0

    invoke-virtual {v10, v13}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 42
    const/4 v13, 0x3

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v10, v13}, Lru/cn/ad/natives/presenters/NativeContextualPresenter;->bindView(Lru/cn/ad/natives/adapters/NativeAdAdapter$ViewBinder;Landroid/widget/ImageView;I)V

    .line 44
    move-object/from16 v0, p0

    iget-object v13, v0, Lru/cn/ad/natives/presenters/NativeContextualPresenter;->templateView:Landroid/view/View;

    const v14, 0x7f0901d7

    invoke-virtual {v13, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/TextView;

    .line 45
    .local v12, "titleView":Landroid/widget/TextView;
    const/4 v13, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v12, v13}, Lru/cn/ad/natives/presenters/NativeContextualPresenter;->bindView(Lru/cn/ad/natives/adapters/NativeAdAdapter$ViewBinder;Landroid/widget/TextView;I)V

    .line 47
    move-object/from16 v0, p0

    iget-object v13, v0, Lru/cn/ad/natives/presenters/NativeContextualPresenter;->templateView:Landroid/view/View;

    const v14, 0x7f09003f

    invoke-virtual {v13, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 48
    .local v5, "bodyView":Landroid/widget/TextView;
    const/4 v13, 0x2

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v5, v13}, Lru/cn/ad/natives/presenters/NativeContextualPresenter;->bindView(Lru/cn/ad/natives/adapters/NativeAdAdapter$ViewBinder;Landroid/widget/TextView;I)V

    .line 50
    move-object/from16 v0, p0

    iget-object v13, v0, Lru/cn/ad/natives/presenters/NativeContextualPresenter;->templateView:Landroid/view/View;

    const v14, 0x7f090033

    invoke-virtual {v13, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 51
    .local v4, "attribution":Landroid/widget/TextView;
    const/4 v13, 0x7

    move-object/from16 v0, p1

    invoke-interface {v0, v4, v13}, Lru/cn/ad/natives/adapters/NativeAdAdapter$ViewBinder;->bind(Landroid/widget/TextView;I)Z

    move-result v13

    if-nez v13, :cond_0

    .line 52
    const v13, 0x7f0e0025

    invoke-virtual {v4, v13}, Landroid/widget/TextView;->setText(I)V

    .line 58
    :cond_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lru/cn/ad/natives/presenters/NativeContextualPresenter;->templateView:Landroid/view/View;

    const v14, 0x7f09002b

    invoke-virtual {v13, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 59
    .local v2, "advertiserSeparator":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v13, v0, Lru/cn/ad/natives/presenters/NativeContextualPresenter;->templateView:Landroid/view/View;

    const v14, 0x7f09002a

    invoke-virtual {v13, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 60
    .local v3, "advertiserView":Landroid/widget/TextView;
    const/16 v13, 0xe

    move-object/from16 v0, p1

    invoke-interface {v0, v3, v13}, Lru/cn/ad/natives/adapters/NativeAdAdapter$ViewBinder;->bind(Landroid/widget/TextView;I)Z

    move-result v9

    .line 61
    .local v9, "hasAdvertiser":Z
    if-eqz v9, :cond_1

    const/4 v13, 0x0

    :goto_0
    invoke-virtual {v3, v13}, Landroid/widget/TextView;->setVisibility(I)V

    .line 62
    if-eqz v9, :cond_2

    const/4 v13, 0x0

    :goto_1
    invoke-virtual {v2, v13}, Landroid/view/View;->setVisibility(I)V

    .line 65
    move-object/from16 v0, p0

    iget-object v13, v0, Lru/cn/ad/natives/presenters/NativeContextualPresenter;->templateView:Landroid/view/View;

    const v14, 0x7f09008c

    invoke-virtual {v13, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Button;

    .line 66
    .local v8, "ctaButton":Landroid/widget/Button;
    invoke-interface/range {p1 .. p1}, Lru/cn/ad/natives/adapters/NativeAdAdapter$ViewBinder;->getAdType()I

    move-result v13

    const/4 v14, 0x2

    if-ne v13, v14, :cond_3

    .line 67
    const/4 v13, 0x0

    invoke-virtual {v8, v13}, Landroid/widget/Button;->setVisibility(I)V

    .line 68
    const/4 v13, 0x4

    move-object/from16 v0, p1

    invoke-interface {v0, v8, v13}, Lru/cn/ad/natives/adapters/NativeAdAdapter$ViewBinder;->bind(Landroid/widget/Button;I)Z

    .line 69
    const/4 v13, 0x1

    new-array v13, v13, [Landroid/view/View;

    const/4 v14, 0x0

    aput-object v8, v13, v14

    invoke-static {v13}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    .line 75
    .local v6, "clickableViews":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    :goto_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lru/cn/ad/natives/presenters/NativeContextualPresenter;->templateView:Landroid/view/View;

    const v14, 0x7f090076

    invoke-virtual {v13, v14}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    .line 76
    .local v7, "closeButton":Landroid/widget/ImageView;
    new-instance v13, Lru/cn/ad/natives/presenters/NativeContextualPresenter$$Lambda$0;

    move-object/from16 v0, p2

    invoke-direct {v13, v0}, Lru/cn/ad/natives/presenters/NativeContextualPresenter$$Lambda$0;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v7, v13}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 78
    move-object/from16 v0, p0

    iget-object v13, v0, Lru/cn/ad/natives/presenters/NativeContextualPresenter;->templateView:Landroid/view/View;

    move-object/from16 v0, p1

    invoke-interface {v0, v13, v6}, Lru/cn/ad/natives/adapters/NativeAdAdapter$ViewBinder;->registerTemplate(Landroid/view/View;Ljava/util/List;)Landroid/view/View;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lru/cn/ad/natives/presenters/NativeContextualPresenter;->adView:Landroid/view/View;

    .line 80
    new-instance v11, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v13, -0x1

    const/4 v14, -0x2

    invoke-direct {v11, v13, v14}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 82
    .local v11, "params":Landroid/widget/FrameLayout$LayoutParams;
    const/16 v13, 0x50

    iput v13, v11, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 83
    move-object/from16 v0, p0

    iget-object v13, v0, Lru/cn/ad/natives/presenters/NativeContextualPresenter;->containerView:Landroid/view/ViewGroup;

    move-object/from16 v0, p0

    iget-object v14, v0, Lru/cn/ad/natives/presenters/NativeContextualPresenter;->adView:Landroid/view/View;

    invoke-virtual {v13, v14, v11}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 85
    move-object/from16 v0, p0

    iget-object v13, v0, Lru/cn/ad/natives/presenters/NativeContextualPresenter;->handler:Landroid/os/Handler;

    const-wide/32 v14, 0xea60

    move-object/from16 v0, p2

    invoke-virtual {v13, v0, v14, v15}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 86
    return-void

    .line 61
    .end local v6    # "clickableViews":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    .end local v7    # "closeButton":Landroid/widget/ImageView;
    .end local v8    # "ctaButton":Landroid/widget/Button;
    .end local v11    # "params":Landroid/widget/FrameLayout$LayoutParams;
    :cond_1
    const/16 v13, 0x8

    goto :goto_0

    .line 62
    :cond_2
    const/16 v13, 0x8

    goto :goto_1

    .line 71
    .restart local v8    # "ctaButton":Landroid/widget/Button;
    :cond_3
    const/16 v13, 0x8

    invoke-virtual {v8, v13}, Landroid/widget/Button;->setVisibility(I)V

    .line 72
    const/4 v13, 0x3

    new-array v13, v13, [Landroid/view/View;

    const/4 v14, 0x0

    aput-object v10, v13, v14

    const/4 v14, 0x1

    aput-object v12, v13, v14

    const/4 v14, 0x2

    aput-object v5, v13, v14

    invoke-static {v13}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    .restart local v6    # "clickableViews":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    goto :goto_2
.end method
