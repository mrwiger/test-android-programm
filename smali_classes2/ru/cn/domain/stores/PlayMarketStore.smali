.class Lru/cn/domain/stores/PlayMarketStore;
.super Lru/cn/domain/stores/PurchaseStore;
.source "PlayMarketStore.java"


# instance fields
.field private mHelper:Lru/cn/domain/stores/iabhelper/IabHelper;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lru/cn/domain/stores/PurchaseStore;-><init>(Landroid/content/Context;)V

    .line 28
    new-instance v0, Lru/cn/domain/stores/iabhelper/IabHelper;

    const-string v1, "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAjKTjivu8ja6sdIAoLy26bpYrbmtF48LpVrX6aVvHo3TckATv9XbAqQ9XS6Ez3jz+kAwIL2EayB2aJZrip2/X9SXWZALnLBq6UE+OHpjdNVvcR54KzYGCPLkSueSOaMMrhqxBriJ33uF2FzBgGBQnM2VkFyrgfyMVqDsO7u1vydrvfqp7M5IOQM9MFeCW4CGIiRD3k6KfvOCgNku3+/fRg98LvLS3u7lxmZAFDUa4KF+Ij+6k9mzUhmei59RDyM92caT30MZK3SUfW57VFgmjK6uIAT/EwbLSLmBKnpm8cSzi2HVLOTyYNomUSANZ9an0UYjT5yAOANs7ggdWVBswhwIDAQAB"

    invoke-direct {v0, p1, v1}, Lru/cn/domain/stores/iabhelper/IabHelper;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 29
    .local v0, "inventoryQuery":Lru/cn/domain/stores/iabhelper/IabHelper;
    new-instance v1, Lru/cn/domain/stores/PlayMarketStore$1;

    invoke-direct {v1, p0, v0}, Lru/cn/domain/stores/PlayMarketStore$1;-><init>(Lru/cn/domain/stores/PlayMarketStore;Lru/cn/domain/stores/iabhelper/IabHelper;)V

    invoke-virtual {v0, v1}, Lru/cn/domain/stores/iabhelper/IabHelper;->startSetup(Lru/cn/domain/stores/iabhelper/IabHelper$OnIabSetupFinishedListener;)V

    .line 44
    return-void
.end method

.method static synthetic access$000(Lru/cn/domain/stores/PlayMarketStore;Lru/cn/domain/stores/iabhelper/IabHelper;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/domain/stores/PlayMarketStore;
    .param p1, "x1"    # Lru/cn/domain/stores/iabhelper/IabHelper;

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lru/cn/domain/stores/PlayMarketStore;->queryInventory(Lru/cn/domain/stores/iabhelper/IabHelper;)V

    return-void
.end method

.method static synthetic access$100(Lru/cn/domain/stores/PlayMarketStore;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/domain/stores/PlayMarketStore;

    .prologue
    .line 16
    invoke-direct {p0}, Lru/cn/domain/stores/PlayMarketStore;->dispose()V

    return-void
.end method

.method static synthetic access$200(Lru/cn/domain/stores/PlayMarketStore;)Lru/cn/domain/stores/iabhelper/IabHelper;
    .locals 1
    .param p0, "x0"    # Lru/cn/domain/stores/PlayMarketStore;

    .prologue
    .line 16
    iget-object v0, p0, Lru/cn/domain/stores/PlayMarketStore;->mHelper:Lru/cn/domain/stores/iabhelper/IabHelper;

    return-object v0
.end method

.method private dispose()V
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lru/cn/domain/stores/PlayMarketStore;->mHelper:Lru/cn/domain/stores/iabhelper/IabHelper;

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lru/cn/domain/stores/PlayMarketStore;->mHelper:Lru/cn/domain/stores/iabhelper/IabHelper;

    invoke-virtual {v0}, Lru/cn/domain/stores/iabhelper/IabHelper;->dispose()V

    .line 66
    const/4 v0, 0x0

    iput-object v0, p0, Lru/cn/domain/stores/PlayMarketStore;->mHelper:Lru/cn/domain/stores/iabhelper/IabHelper;

    .line 68
    :cond_0
    return-void
.end method

.method private queryInventory(Lru/cn/domain/stores/iabhelper/IabHelper;)V
    .locals 1
    .param p1, "inventoryQuery"    # Lru/cn/domain/stores/iabhelper/IabHelper;

    .prologue
    .line 47
    new-instance v0, Lru/cn/domain/stores/PlayMarketStore$2;

    invoke-direct {v0, p0, p1}, Lru/cn/domain/stores/PlayMarketStore$2;-><init>(Lru/cn/domain/stores/PlayMarketStore;Lru/cn/domain/stores/iabhelper/IabHelper;)V

    invoke-virtual {p1, v0}, Lru/cn/domain/stores/iabhelper/IabHelper;->queryInventoryAsync(Lru/cn/domain/stores/iabhelper/IabHelper$QueryInventoryFinishedListener;)V

    .line 61
    return-void
.end method


# virtual methods
.method public handleResult(IILandroid/content/Intent;)Z
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 98
    iget-object v0, p0, Lru/cn/domain/stores/PlayMarketStore;->mHelper:Lru/cn/domain/stores/iabhelper/IabHelper;

    if-eqz v0, :cond_0

    const/16 v0, 0x6f6

    if-ne p1, v0, :cond_0

    .line 99
    iget-object v0, p0, Lru/cn/domain/stores/PlayMarketStore;->mHelper:Lru/cn/domain/stores/iabhelper/IabHelper;

    invoke-virtual {v0, p1, p2, p3}, Lru/cn/domain/stores/iabhelper/IabHelper;->handleActivityResult(IILandroid/content/Intent;)Z

    move-result v0

    .line 102
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public purchase(Landroid/app/Activity;Ljava/lang/String;Lru/cn/domain/stores/PurchaseStore$OnPurchaseFinishListener;)V
    .locals 3
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "productId"    # Ljava/lang/String;
    .param p3, "listener"    # Lru/cn/domain/stores/PurchaseStore$OnPurchaseFinishListener;

    .prologue
    .line 73
    invoke-direct {p0}, Lru/cn/domain/stores/PlayMarketStore;->dispose()V

    .line 75
    new-instance v0, Lru/cn/domain/stores/iabhelper/IabHelper;

    iget-object v1, p0, Lru/cn/domain/stores/PlayMarketStore;->context:Landroid/content/Context;

    const-string v2, "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAjKTjivu8ja6sdIAoLy26bpYrbmtF48LpVrX6aVvHo3TckATv9XbAqQ9XS6Ez3jz+kAwIL2EayB2aJZrip2/X9SXWZALnLBq6UE+OHpjdNVvcR54KzYGCPLkSueSOaMMrhqxBriJ33uF2FzBgGBQnM2VkFyrgfyMVqDsO7u1vydrvfqp7M5IOQM9MFeCW4CGIiRD3k6KfvOCgNku3+/fRg98LvLS3u7lxmZAFDUa4KF+Ij+6k9mzUhmei59RDyM92caT30MZK3SUfW57VFgmjK6uIAT/EwbLSLmBKnpm8cSzi2HVLOTyYNomUSANZ9an0UYjT5yAOANs7ggdWVBswhwIDAQAB"

    invoke-direct {v0, v1, v2}, Lru/cn/domain/stores/iabhelper/IabHelper;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lru/cn/domain/stores/PlayMarketStore;->mHelper:Lru/cn/domain/stores/iabhelper/IabHelper;

    .line 76
    iget-object v0, p0, Lru/cn/domain/stores/PlayMarketStore;->mHelper:Lru/cn/domain/stores/iabhelper/IabHelper;

    new-instance v1, Lru/cn/domain/stores/PlayMarketStore$3;

    invoke-direct {v1, p0, p3, p1, p2}, Lru/cn/domain/stores/PlayMarketStore$3;-><init>(Lru/cn/domain/stores/PlayMarketStore;Lru/cn/domain/stores/PurchaseStore$OnPurchaseFinishListener;Landroid/app/Activity;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lru/cn/domain/stores/iabhelper/IabHelper;->startSetup(Lru/cn/domain/stores/iabhelper/IabHelper$OnIabSetupFinishedListener;)V

    .line 94
    return-void
.end method

.method public storeName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 107
    const-string v0, "google_play"

    return-object v0
.end method
