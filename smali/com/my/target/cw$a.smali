.class final Lcom/my/target/cw$a;
.super Ljava/lang/Object;
.source "VideoDialogView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/my/target/cw;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic aG:Lcom/my/target/cw;


# direct methods
.method private constructor <init>(Lcom/my/target/cw;)V
    .locals 0

    .prologue
    .line 688
    iput-object p1, p0, Lcom/my/target/cw$a;->aG:Lcom/my/target/cw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/my/target/cw;B)V
    .locals 0

    .prologue
    .line 688
    invoke-direct {p0, p1}, Lcom/my/target/cw$a;-><init>(Lcom/my/target/cw;)V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 693
    iget-object v0, p0, Lcom/my/target/cw$a;->aG:Lcom/my/target/cw;

    invoke-static {v0}, Lcom/my/target/cw;->a(Lcom/my/target/cw;)Lcom/my/target/cw$d;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 695
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 696
    invoke-static {}, Lcom/my/target/cw;->E()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 698
    iget-object v0, p0, Lcom/my/target/cw$a;->aG:Lcom/my/target/cw;

    invoke-static {v0}, Lcom/my/target/cw;->a(Lcom/my/target/cw;)Lcom/my/target/cw$d;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/my/target/cw$d;->a(Landroid/view/View;)V

    .line 721
    :cond_0
    :goto_0
    return-void

    .line 700
    :cond_1
    invoke-static {}, Lcom/my/target/cw;->F()I

    move-result v1

    if-ne v0, v1, :cond_2

    .line 702
    iget-object v0, p0, Lcom/my/target/cw$a;->aG:Lcom/my/target/cw;

    invoke-static {v0}, Lcom/my/target/cw;->a(Lcom/my/target/cw;)Lcom/my/target/cw$d;

    move-result-object v0

    invoke-interface {v0}, Lcom/my/target/cw$d;->k()V

    goto :goto_0

    .line 704
    :cond_2
    invoke-static {}, Lcom/my/target/cw;->G()I

    move-result v1

    if-ne v0, v1, :cond_3

    .line 706
    iget-object v0, p0, Lcom/my/target/cw$a;->aG:Lcom/my/target/cw;

    invoke-static {v0}, Lcom/my/target/cw;->a(Lcom/my/target/cw;)Lcom/my/target/cw$d;

    move-result-object v0

    invoke-interface {v0}, Lcom/my/target/cw$d;->onPauseClicked()V

    goto :goto_0

    .line 708
    :cond_3
    invoke-static {}, Lcom/my/target/cw;->H()I

    move-result v1

    if-ne v0, v1, :cond_4

    .line 710
    iget-object v0, p0, Lcom/my/target/cw$a;->aG:Lcom/my/target/cw;

    invoke-static {v0}, Lcom/my/target/cw;->a(Lcom/my/target/cw;)Lcom/my/target/cw$d;

    move-result-object v0

    invoke-interface {v0}, Lcom/my/target/cw$d;->onPlayClicked()V

    goto :goto_0

    .line 712
    :cond_4
    invoke-static {}, Lcom/my/target/cw;->I()I

    move-result v1

    if-ne v0, v1, :cond_5

    .line 714
    iget-object v0, p0, Lcom/my/target/cw$a;->aG:Lcom/my/target/cw;

    invoke-static {v0}, Lcom/my/target/cw;->a(Lcom/my/target/cw;)Lcom/my/target/cw$d;

    move-result-object v0

    invoke-interface {v0}, Lcom/my/target/cw$d;->l()V

    goto :goto_0

    .line 716
    :cond_5
    invoke-static {}, Lcom/my/target/cw;->J()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 718
    iget-object v0, p0, Lcom/my/target/cw$a;->aG:Lcom/my/target/cw;

    invoke-static {v0}, Lcom/my/target/cw;->a(Lcom/my/target/cw;)Lcom/my/target/cw$d;

    move-result-object v0

    invoke-interface {v0}, Lcom/my/target/cw$d;->m()V

    goto :goto_0
.end method
