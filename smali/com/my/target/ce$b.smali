.class Lcom/my/target/ce$b;
.super Lcom/my/target/ce$a;
.source "ClickHandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/my/target/ce;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation


# direct methods
.method private constructor <init>(Lcom/my/target/ah;)V
    .locals 0

    .prologue
    .line 138
    invoke-direct {p0, p1}, Lcom/my/target/ce$a;-><init>(Lcom/my/target/ah;)V

    .line 139
    return-void
.end method

.method synthetic constructor <init>(Lcom/my/target/ah;Lcom/my/target/ce$1;)V
    .locals 0

    .prologue
    .line 134
    invoke-direct {p0, p1}, Lcom/my/target/ce$b;-><init>(Lcom/my/target/ah;)V

    return-void
.end method

.method private a(Landroid/content/Intent;Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 234
    :try_start_0
    instance-of v0, p2, Landroid/app/Activity;

    if-nez v0, :cond_0

    .line 236
    const/high16 v0, 0x10000000

    invoke-virtual {p1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 238
    :cond_0
    invoke-virtual {p2, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 239
    const/4 v0, 0x1

    .line 244
    :goto_0
    return v0

    .line 241
    :catch_0
    move-exception v0

    .line 244
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 185
    if-nez p2, :cond_0

    .line 203
    :goto_0
    return v0

    .line 191
    :cond_0
    :try_start_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 192
    instance-of v2, p3, Landroid/app/Activity;

    if-nez v2, :cond_1

    .line 194
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 196
    :cond_1
    invoke-virtual {v1, p1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 197
    invoke-virtual {p3, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 198
    const/4 v0, 0x1

    goto :goto_0

    .line 200
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 208
    if-nez p2, :cond_0

    .line 227
    :goto_0
    return v0

    .line 214
    :cond_0
    :try_start_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 215
    instance-of v2, p3, Landroid/app/Activity;

    if-nez v2, :cond_1

    .line 217
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 219
    :cond_1
    invoke-virtual {v1, p1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 220
    invoke-virtual {p3, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 221
    const/4 v0, 0x1

    goto :goto_0

    .line 223
    :catch_0
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method protected r(Landroid/content/Context;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 144
    const-string v2, "store"

    iget-object v3, p0, Lcom/my/target/ce$b;->jK:Lcom/my/target/ah;

    invoke-virtual {v3}, Lcom/my/target/ah;->getNavigationType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 180
    :cond_0
    :goto_0
    return v0

    .line 149
    :cond_1
    iget-object v2, p0, Lcom/my/target/ce$b;->jK:Lcom/my/target/ah;

    invoke-virtual {v2}, Lcom/my/target/ah;->getBundleId()Ljava/lang/String;

    move-result-object v2

    .line 151
    if-eqz v2, :cond_0

    .line 156
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    .line 157
    if-eqz v3, :cond_0

    .line 162
    iget-object v4, p0, Lcom/my/target/ce$b;->jK:Lcom/my/target/ah;

    invoke-virtual {v4}, Lcom/my/target/ah;->getDeeplink()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v2, v4, p1}, Lcom/my/target/ce$b;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 164
    iget-object v0, p0, Lcom/my/target/ce$b;->jK:Lcom/my/target/ah;

    invoke-virtual {v0}, Lcom/my/target/ah;->getStatHolder()Lcom/my/target/ar;

    move-result-object v0

    const-string v2, "deeplinkClick"

    invoke-virtual {v0, v2}, Lcom/my/target/ar;->w(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/my/target/cl;->a(Ljava/util/List;Landroid/content/Context;)V

    move v0, v1

    .line 165
    goto :goto_0

    .line 168
    :cond_2
    iget-object v4, p0, Lcom/my/target/ce$b;->jK:Lcom/my/target/ah;

    invoke-virtual {v4}, Lcom/my/target/ah;->getUrlscheme()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v2, v4, p1}, Lcom/my/target/ce$b;->b(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 169
    invoke-direct {p0, v3, p1}, Lcom/my/target/ce$b;->a(Landroid/content/Intent;Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 171
    :cond_3
    iget-object v0, p0, Lcom/my/target/ce$b;->jK:Lcom/my/target/ah;

    invoke-virtual {v0}, Lcom/my/target/ah;->getStatHolder()Lcom/my/target/ar;

    move-result-object v0

    const-string v2, "click"

    invoke-virtual {v0, v2}, Lcom/my/target/ar;->w(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/my/target/cl;->a(Ljava/util/List;Landroid/content/Context;)V

    .line 173
    iget-object v0, p0, Lcom/my/target/ce$b;->jK:Lcom/my/target/ah;

    invoke-virtual {v0}, Lcom/my/target/ah;->getTrackingLink()Ljava/lang/String;

    move-result-object v0

    .line 174
    if-eqz v0, :cond_4

    invoke-static {v0}, Lcom/my/target/cn;->S(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 176
    invoke-static {v0}, Lcom/my/target/cn;->U(Ljava/lang/String;)Lcom/my/target/cn;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/my/target/cn;->y(Landroid/content/Context;)V

    :cond_4
    move v0, v1

    .line 178
    goto :goto_0
.end method
