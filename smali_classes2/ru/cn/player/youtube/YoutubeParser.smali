.class public Lru/cn/player/youtube/YoutubeParser;
.super Ljava/lang/Object;
.source "YoutubeParser.java"


# direct methods
.method public static getVideoURL(Ljava/lang/String;)Ljava/util/List;
    .locals 24
    .param p0, "youtubeURL"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lru/cn/player/youtube/YoutubeVideo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 42
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 43
    .local v19, "videos":Ljava/util/List;, "Ljava/util/List<Lru/cn/player/youtube/YoutubeVideo;>;"
    invoke-static/range {p0 .. p0}, Lru/cn/player/youtube/YoutubeParser;->getYouTubeVideoId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 44
    .local v18, "videoId":Ljava/lang/String;
    if-eqz v18, :cond_0

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    move-result v20

    if-nez v20, :cond_1

    .line 87
    :cond_0
    :goto_0
    return-object v19

    .line 49
    :cond_1
    :try_start_0
    new-instance v5, Lru/cn/utils/http/HttpClient;

    invoke-direct {v5}, Lru/cn/utils/http/HttpClient;-><init>()V

    .line 50
    .local v5, "client":Lru/cn/utils/http/HttpClient;
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "http://www.youtube.com/get_video_info?&video_id="

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Lru/cn/utils/http/HttpClient;->sendRequest(Ljava/lang/String;)V

    .line 51
    invoke-virtual {v5}, Lru/cn/utils/http/HttpClient;->getContent()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v12

    .line 56
    .local v12, "infoStr":Ljava/lang/String;
    const-string v20, "&"

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 57
    .local v4, "args":[Ljava/lang/String;
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 58
    .local v3, "argMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    array-length v0, v4

    move/from16 v21, v0

    const/16 v20, 0x0

    :goto_1
    move/from16 v0, v20

    move/from16 v1, v21

    if-ge v0, v1, :cond_3

    aget-object v2, v4, v20

    .line 59
    .local v2, "arg":Ljava/lang/String;
    const-string v22, "="

    move-object/from16 v0, v22

    invoke-virtual {v2, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v15

    .line 60
    .local v15, "valStrArr":[Ljava/lang/String;
    array-length v0, v15

    move/from16 v22, v0

    const/16 v23, 0x2

    move/from16 v0, v22

    move/from16 v1, v23

    if-lt v0, v1, :cond_2

    .line 61
    const/16 v22, 0x0

    aget-object v22, v15, v22

    const/16 v23, 0x1

    aget-object v23, v15, v23

    invoke-static/range {v23 .. v23}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    :cond_2
    add-int/lit8 v20, v20, 0x1

    goto :goto_1

    .line 52
    .end local v2    # "arg":Ljava/lang/String;
    .end local v3    # "argMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v4    # "args":[Ljava/lang/String;
    .end local v5    # "client":Lru/cn/utils/http/HttpClient;
    .end local v12    # "infoStr":Ljava/lang/String;
    .end local v15    # "valStrArr":[Ljava/lang/String;
    :catch_0
    move-exception v6

    .line 53
    .local v6, "e":Ljava/lang/Exception;
    goto :goto_0

    .line 66
    .end local v6    # "e":Ljava/lang/Exception;
    .restart local v3    # "argMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    .restart local v4    # "args":[Ljava/lang/String;
    .restart local v5    # "client":Lru/cn/utils/http/HttpClient;
    .restart local v12    # "infoStr":Ljava/lang/String;
    :cond_3
    const-string v20, "fmt_list"

    move-object/from16 v0, v20

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 67
    .local v7, "fmtList":Ljava/lang/String;
    new-instance v11, Landroid/util/SparseArray;

    invoke-direct {v11}, Landroid/util/SparseArray;-><init>()V

    .line 68
    .local v11, "formats":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lru/cn/player/youtube/Format;>;"
    if-eqz v7, :cond_4

    .line 69
    invoke-static {v7}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 70
    const-string v20, ","

    move-object/from16 v0, v20

    invoke-virtual {v7, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v10

    .line 71
    .local v10, "formatStrs":[Ljava/lang/String;
    array-length v0, v10

    move/from16 v21, v0

    const/16 v20, 0x0

    :goto_2
    move/from16 v0, v20

    move/from16 v1, v21

    if-ge v0, v1, :cond_4

    aget-object v9, v10, v20

    .line 72
    .local v9, "formatStr":Ljava/lang/String;
    invoke-static {v9}, Lru/cn/player/youtube/YoutubeParser;->parseFormat(Ljava/lang/String;)Lru/cn/player/youtube/Format;

    move-result-object v8

    .line 73
    .local v8, "format":Lru/cn/player/youtube/Format;
    iget v0, v8, Lru/cn/player/youtube/Format;->id:I

    move/from16 v22, v0

    move/from16 v0, v22

    invoke-virtual {v11, v0, v8}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 71
    add-int/lit8 v20, v20, 0x1

    goto :goto_2

    .line 78
    .end local v8    # "format":Lru/cn/player/youtube/Format;
    .end local v9    # "formatStr":Ljava/lang/String;
    .end local v10    # "formatStrs":[Ljava/lang/String;
    :cond_4
    const-string v20, "url_encoded_fmt_stream_map"

    move-object/from16 v0, v20

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    .line 79
    .local v13, "streamList":Ljava/lang/String;
    if-eqz v13, :cond_0

    .line 80
    const-string v20, ","

    move-object/from16 v0, v20

    invoke-virtual {v13, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v14

    .line 81
    .local v14, "streamStrs":[Ljava/lang/String;
    array-length v0, v14

    move/from16 v21, v0

    const/16 v20, 0x0

    :goto_3
    move/from16 v0, v20

    move/from16 v1, v21

    if-ge v0, v1, :cond_0

    aget-object v17, v14, v20

    .line 82
    .local v17, "videoData":Ljava/lang/String;
    move-object/from16 v0, v17

    invoke-static {v0, v11}, Lru/cn/player/youtube/YoutubeParser;->parseVideo(Ljava/lang/String;Landroid/util/SparseArray;)Lru/cn/player/youtube/YoutubeVideo;

    move-result-object v16

    .line 83
    .local v16, "video":Lru/cn/player/youtube/YoutubeVideo;
    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 81
    add-int/lit8 v20, v20, 0x1

    goto :goto_3
.end method

.method private static getYouTubeVideoId(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "youtubeUrl"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 18
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "http"

    invoke-virtual {p0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 38
    :cond_0
    :goto_0
    return-object v2

    .line 21
    :cond_1
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 22
    .local v1, "youtubeUri":Landroid/net/Uri;
    if-eqz v1, :cond_0

    .line 25
    const-string v3, "youtu.be"

    invoke-virtual {v1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 26
    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 28
    .local v0, "path":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    .line 31
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 34
    .end local v0    # "path":Ljava/lang/String;
    :cond_2
    invoke-virtual {v1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v3

    const-string v4, "youtube.com"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    const-string v4, "/watch"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 35
    const-string v2, "v"

    invoke-virtual {v1, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public static isFormatSupported(I)Z
    .locals 5
    .param p0, "formatId"    # I

    .prologue
    const/4 v2, 0x0

    .line 107
    const/4 v3, 0x3

    new-array v0, v3, [I

    fill-array-data v0, :array_0

    .line 115
    .local v0, "supportedFormatIds":[I
    array-length v4, v0

    move v3, v2

    :goto_0
    if-ge v3, v4, :cond_0

    aget v1, v0, v3

    .line 116
    .local v1, "supportedId":I
    if-ne p0, v1, :cond_1

    .line 117
    const/4 v2, 0x1

    .line 120
    .end local v1    # "supportedId":I
    :cond_0
    return v2

    .line 115
    .restart local v1    # "supportedId":I
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 107
    :array_0
    .array-data 4
        0x12
        0x16
        0x25
    .end array-data
.end method

.method public static isYoutubeURL(Ljava/lang/String;)Z
    .locals 6
    .param p0, "url"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 91
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 92
    .local v1, "youtubeUri":Landroid/net/Uri;
    if-nez v1, :cond_1

    .line 102
    :cond_0
    :goto_0
    return v3

    .line 95
    :cond_1
    invoke-virtual {v1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    .line 96
    .local v0, "host":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 99
    const-string v4, "youtu.be"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    move v3, v2

    .line 100
    goto :goto_0

    .line 102
    :cond_2
    const-string v4, "youtube.com"

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    const-string v5, "/watch"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    :goto_1
    move v3, v2

    goto :goto_0

    :cond_3
    move v2, v3

    goto :goto_1
.end method

.method private static parseFormat(Ljava/lang/String;)Lru/cn/player/youtube/Format;
    .locals 4
    .param p0, "format"    # Ljava/lang/String;

    .prologue
    .line 141
    const-string v2, "/"

    invoke-virtual {p0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 143
    .local v1, "formatVars":[Ljava/lang/String;
    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 144
    .local v0, "formatId":I
    new-instance v2, Lru/cn/player/youtube/Format;

    const/4 v3, 0x1

    aget-object v3, v1, v3

    invoke-direct {v2, v0, v3}, Lru/cn/player/youtube/Format;-><init>(ILjava/lang/String;)V

    return-object v2
.end method

.method private static parseVideo(Ljava/lang/String;Landroid/util/SparseArray;)Lru/cn/player/youtube/YoutubeVideo;
    .locals 12
    .param p0, "videoData"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/util/SparseArray",
            "<",
            "Lru/cn/player/youtube/Format;",
            ">;)",
            "Lru/cn/player/youtube/YoutubeVideo;"
        }
    .end annotation

    .prologue
    .local p1, "formats":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lru/cn/player/youtube/Format;>;"
    const/4 v8, 0x0

    .line 124
    const-string v7, "&"

    invoke-virtual {p0, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 125
    .local v2, "args":[Ljava/lang/String;
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 126
    .local v1, "argMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    array-length v9, v2

    move v7, v8

    :goto_0
    if-ge v7, v9, :cond_1

    aget-object v0, v2, v7

    .line 127
    .local v0, "arg":Ljava/lang/String;
    const-string v10, "="

    invoke-virtual {v0, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 128
    .local v3, "argsValues":[Ljava/lang/String;
    array-length v10, v3

    const/4 v11, 0x2

    if-lt v10, v11, :cond_0

    .line 129
    aget-object v10, v3, v8

    const/4 v11, 0x1

    aget-object v11, v3, v11

    invoke-interface {v1, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    :cond_0
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 133
    .end local v0    # "arg":Ljava/lang/String;
    .end local v3    # "argsValues":[Ljava/lang/String;
    :cond_1
    const-string v7, "itag"

    invoke-interface {v1, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 134
    .local v5, "formatId":I
    invoke-virtual {p1, v5}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/cn/player/youtube/Format;

    .line 136
    .local v4, "format":Lru/cn/player/youtube/Format;
    const-string v7, "url"

    invoke-interface {v1, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-static {v7}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 137
    .local v6, "videoURL":Ljava/lang/String;
    new-instance v7, Lru/cn/player/youtube/YoutubeVideo;

    invoke-direct {v7, v6, v4}, Lru/cn/player/youtube/YoutubeVideo;-><init>(Ljava/lang/String;Lru/cn/player/youtube/Format;)V

    return-object v7
.end method
