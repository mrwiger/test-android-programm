.class final Lcom/google/obf/ca;
.super Lcom/google/obf/cb;
.source "IMASDK"


# instance fields
.field private final b:Lcom/google/obf/dw;

.field private c:I

.field private d:I

.field private e:I

.field private f:J

.field private g:Lcom/google/obf/q;

.field private h:I

.field private i:J


# direct methods
.method public constructor <init>(Lcom/google/obf/ar;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1
    invoke-direct {p0, p1}, Lcom/google/obf/cb;-><init>(Lcom/google/obf/ar;)V

    .line 2
    new-instance v0, Lcom/google/obf/dw;

    const/16 v1, 0xf

    new-array v1, v1, [B

    invoke-direct {v0, v1}, Lcom/google/obf/dw;-><init>([B)V

    iput-object v0, p0, Lcom/google/obf/ca;->b:Lcom/google/obf/dw;

    .line 3
    iget-object v0, p0, Lcom/google/obf/ca;->b:Lcom/google/obf/dw;

    iget-object v0, v0, Lcom/google/obf/dw;->a:[B

    const/16 v1, 0x7f

    aput-byte v1, v0, v3

    .line 4
    iget-object v0, p0, Lcom/google/obf/ca;->b:Lcom/google/obf/dw;

    iget-object v0, v0, Lcom/google/obf/dw;->a:[B

    const/4 v1, -0x2

    aput-byte v1, v0, v4

    .line 5
    iget-object v0, p0, Lcom/google/obf/ca;->b:Lcom/google/obf/dw;

    iget-object v0, v0, Lcom/google/obf/dw;->a:[B

    const/4 v1, 0x2

    const/16 v2, -0x80

    aput-byte v2, v0, v1

    .line 6
    iget-object v0, p0, Lcom/google/obf/ca;->b:Lcom/google/obf/dw;

    iget-object v0, v0, Lcom/google/obf/dw;->a:[B

    const/4 v1, 0x3

    aput-byte v4, v0, v1

    .line 7
    iput v3, p0, Lcom/google/obf/ca;->c:I

    .line 8
    return-void
.end method

.method private a(Lcom/google/obf/dw;[BI)Z
    .locals 2

    .prologue
    .line 35
    invoke-virtual {p1}, Lcom/google/obf/dw;->b()I

    move-result v0

    iget v1, p0, Lcom/google/obf/ca;->d:I

    sub-int v1, p3, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 36
    iget v1, p0, Lcom/google/obf/ca;->d:I

    invoke-virtual {p1, p2, v1, v0}, Lcom/google/obf/dw;->a([BII)V

    .line 37
    iget v1, p0, Lcom/google/obf/ca;->d:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/obf/ca;->d:I

    .line 38
    iget v0, p0, Lcom/google/obf/ca;->d:I

    if-ne v0, p3, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Lcom/google/obf/dw;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 39
    :cond_0
    invoke-virtual {p1}, Lcom/google/obf/dw;->b()I

    move-result v1

    if-lez v1, :cond_1

    .line 40
    iget v1, p0, Lcom/google/obf/ca;->e:I

    shl-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/obf/ca;->e:I

    .line 41
    iget v1, p0, Lcom/google/obf/ca;->e:I

    invoke-virtual {p1}, Lcom/google/obf/dw;->f()I

    move-result v2

    or-int/2addr v1, v2

    iput v1, p0, Lcom/google/obf/ca;->e:I

    .line 42
    iget v1, p0, Lcom/google/obf/ca;->e:I

    const v2, 0x7ffe8001

    if-ne v1, v2, :cond_0

    .line 43
    iput v0, p0, Lcom/google/obf/ca;->e:I

    .line 44
    const/4 v0, 0x1

    .line 45
    :cond_1
    return v0
.end method

.method private c()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 46
    iget-object v0, p0, Lcom/google/obf/ca;->b:Lcom/google/obf/dw;

    iget-object v0, v0, Lcom/google/obf/dw;->a:[B

    .line 47
    iget-object v1, p0, Lcom/google/obf/ca;->g:Lcom/google/obf/q;

    if-nez v1, :cond_0

    .line 48
    const-wide/16 v2, -0x1

    invoke-static {v0, v4, v2, v3, v4}, Lcom/google/obf/dn;->a([BLjava/lang/String;JLjava/lang/String;)Lcom/google/obf/q;

    move-result-object v1

    iput-object v1, p0, Lcom/google/obf/ca;->g:Lcom/google/obf/q;

    .line 49
    iget-object v1, p0, Lcom/google/obf/ca;->a:Lcom/google/obf/ar;

    iget-object v2, p0, Lcom/google/obf/ca;->g:Lcom/google/obf/q;

    invoke-interface {v1, v2}, Lcom/google/obf/ar;->a(Lcom/google/obf/q;)V

    .line 50
    :cond_0
    invoke-static {v0}, Lcom/google/obf/dn;->b([B)I

    move-result v1

    iput v1, p0, Lcom/google/obf/ca;->h:I

    .line 51
    const-wide/32 v2, 0xf4240

    .line 52
    invoke-static {v0}, Lcom/google/obf/dn;->a([B)I

    move-result v0

    int-to-long v0, v0

    mul-long/2addr v0, v2

    iget-object v2, p0, Lcom/google/obf/ca;->g:Lcom/google/obf/q;

    iget v2, v2, Lcom/google/obf/q;->r:I

    int-to-long v2, v2

    div-long/2addr v0, v2

    long-to-int v0, v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/google/obf/ca;->f:J

    .line 53
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 9
    iput v0, p0, Lcom/google/obf/ca;->c:I

    .line 10
    iput v0, p0, Lcom/google/obf/ca;->d:I

    .line 11
    iput v0, p0, Lcom/google/obf/ca;->e:I

    .line 12
    return-void
.end method

.method public a(JZ)V
    .locals 1

    .prologue
    .line 13
    iput-wide p1, p0, Lcom/google/obf/ca;->i:J

    .line 14
    return-void
.end method

.method public a(Lcom/google/obf/dw;)V
    .locals 9

    .prologue
    const/16 v8, 0xf

    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 15
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/obf/dw;->b()I

    move-result v0

    if-lez v0, :cond_1

    .line 16
    iget v0, p0, Lcom/google/obf/ca;->c:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 17
    :pswitch_0
    invoke-direct {p0, p1}, Lcom/google/obf/ca;->b(Lcom/google/obf/dw;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 18
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/obf/ca;->d:I

    .line 19
    iput v4, p0, Lcom/google/obf/ca;->c:I

    goto :goto_0

    .line 20
    :pswitch_1
    iget-object v0, p0, Lcom/google/obf/ca;->b:Lcom/google/obf/dw;

    iget-object v0, v0, Lcom/google/obf/dw;->a:[B

    invoke-direct {p0, p1, v0, v8}, Lcom/google/obf/ca;->a(Lcom/google/obf/dw;[BI)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 21
    invoke-direct {p0}, Lcom/google/obf/ca;->c()V

    .line 22
    iget-object v0, p0, Lcom/google/obf/ca;->b:Lcom/google/obf/dw;

    invoke-virtual {v0, v6}, Lcom/google/obf/dw;->c(I)V

    .line 23
    iget-object v0, p0, Lcom/google/obf/ca;->a:Lcom/google/obf/ar;

    iget-object v1, p0, Lcom/google/obf/ca;->b:Lcom/google/obf/dw;

    invoke-interface {v0, v1, v8}, Lcom/google/obf/ar;->a(Lcom/google/obf/dw;I)V

    .line 24
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/obf/ca;->c:I

    goto :goto_0

    .line 25
    :pswitch_2
    invoke-virtual {p1}, Lcom/google/obf/dw;->b()I

    move-result v0

    iget v1, p0, Lcom/google/obf/ca;->h:I

    iget v2, p0, Lcom/google/obf/ca;->d:I

    sub-int/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 26
    iget-object v1, p0, Lcom/google/obf/ca;->a:Lcom/google/obf/ar;

    invoke-interface {v1, p1, v0}, Lcom/google/obf/ar;->a(Lcom/google/obf/dw;I)V

    .line 27
    iget v1, p0, Lcom/google/obf/ca;->d:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/obf/ca;->d:I

    .line 28
    iget v0, p0, Lcom/google/obf/ca;->d:I

    iget v1, p0, Lcom/google/obf/ca;->h:I

    if-ne v0, v1, :cond_0

    .line 29
    iget-object v1, p0, Lcom/google/obf/ca;->a:Lcom/google/obf/ar;

    iget-wide v2, p0, Lcom/google/obf/ca;->i:J

    iget v5, p0, Lcom/google/obf/ca;->h:I

    const/4 v7, 0x0

    invoke-interface/range {v1 .. v7}, Lcom/google/obf/ar;->a(JIII[B)V

    .line 30
    iget-wide v0, p0, Lcom/google/obf/ca;->i:J

    iget-wide v2, p0, Lcom/google/obf/ca;->f:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/obf/ca;->i:J

    .line 31
    iput v6, p0, Lcom/google/obf/ca;->c:I

    goto :goto_0

    .line 33
    :cond_1
    return-void

    .line 16
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public b()V
    .locals 0

    .prologue
    .line 34
    return-void
.end method
