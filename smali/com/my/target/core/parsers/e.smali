.class public final Lcom/my/target/core/parsers/e;
.super Ljava/lang/Object;
.source "InterstitialAdSectionParser.java"


# instance fields
.field private final adConfig:Lcom/my/target/b;

.field private final context:Landroid/content/Context;

.field private final z:Lcom/my/target/ae;


# direct methods
.method private constructor <init>(Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/my/target/core/parsers/e;->z:Lcom/my/target/ae;

    .line 32
    iput-object p2, p0, Lcom/my/target/core/parsers/e;->adConfig:Lcom/my/target/b;

    .line 33
    iput-object p3, p0, Lcom/my/target/core/parsers/e;->context:Landroid/content/Context;

    .line 34
    return-void
.end method

.method public static b(Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/core/parsers/e;
    .locals 1

    .prologue
    .line 20
    new-instance v0, Lcom/my/target/core/parsers/e;

    invoke-direct {v0, p0, p1, p2}, Lcom/my/target/core/parsers/e;-><init>(Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method public final a(Lorg/json/JSONObject;Lcom/my/target/dv;)V
    .locals 4

    .prologue
    .line 38
    iget-object v0, p0, Lcom/my/target/core/parsers/e;->z:Lcom/my/target/ae;

    iget-object v1, p0, Lcom/my/target/core/parsers/e;->adConfig:Lcom/my/target/b;

    iget-object v2, p0, Lcom/my/target/core/parsers/e;->context:Landroid/content/Context;

    invoke-static {v0, v1, v2}, Lcom/my/target/bf;->b(Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/bf;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/my/target/bf;->a(Lorg/json/JSONObject;Lcom/my/target/ak;)V

    .line 39
    const-string v0, "settings"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 40
    if-eqz v0, :cond_3

    .line 1048
    const-string v1, "close_icon_hd"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1049
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1051
    invoke-static {v1}, Lcom/my/target/common/models/ImageData;->newImageData(Ljava/lang/String;)Lcom/my/target/common/models/ImageData;

    move-result-object v1

    .line 1052
    invoke-virtual {p2, v1}, Lcom/my/target/dv;->setCloseIcon(Lcom/my/target/common/models/ImageData;)V

    .line 1055
    :cond_0
    const-string v1, "play_icon_hd"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1056
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1058
    invoke-static {v1}, Lcom/my/target/common/models/ImageData;->newImageData(Ljava/lang/String;)Lcom/my/target/common/models/ImageData;

    move-result-object v1

    .line 1059
    invoke-virtual {p2, v1}, Lcom/my/target/dv;->setPlayIcon(Lcom/my/target/common/models/ImageData;)V

    .line 1062
    :cond_1
    const-string v1, "store_icon_hd"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1063
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1065
    invoke-static {v1}, Lcom/my/target/common/models/ImageData;->newImageData(Ljava/lang/String;)Lcom/my/target/common/models/ImageData;

    move-result-object v1

    .line 1066
    invoke-virtual {p2, v1}, Lcom/my/target/dv;->setStoreIcon(Lcom/my/target/common/models/ImageData;)V

    .line 1068
    :cond_2
    const-string v1, "closeOnClick"

    invoke-virtual {p2}, Lcom/my/target/dv;->k()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {p2, v1}, Lcom/my/target/dv;->a(Z)V

    .line 1069
    const-string v1, "allowCloseDelay"

    invoke-virtual {p2}, Lcom/my/target/dv;->getAllowCloseDelay()F

    move-result v2

    float-to-double v2, v2

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v2

    double-to-float v1, v2

    invoke-virtual {p2, v1}, Lcom/my/target/dv;->setAllowCloseDelay(F)V

    .line 1070
    const-string v1, "style"

    invoke-virtual {p2}, Lcom/my/target/dv;->getStyle()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {p2, v1}, Lcom/my/target/dv;->setStyle(I)V

    .line 1072
    const-string v1, "video"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 1073
    if-eqz v0, :cond_3

    .line 1075
    iget-object v1, p0, Lcom/my/target/core/parsers/e;->z:Lcom/my/target/ae;

    iget-object v2, p0, Lcom/my/target/core/parsers/e;->adConfig:Lcom/my/target/b;

    iget-object v3, p0, Lcom/my/target/core/parsers/e;->context:Landroid/content/Context;

    invoke-static {p2, v1, v2, v3}, Lcom/my/target/bg;->b(Lcom/my/target/ak;Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/bg;

    move-result-object v1

    invoke-virtual {p2}, Lcom/my/target/dv;->getVideoSettings()Lcom/my/target/am;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/my/target/bg;->a(Lorg/json/JSONObject;Lcom/my/target/am;)V

    .line 44
    :cond_3
    return-void
.end method
