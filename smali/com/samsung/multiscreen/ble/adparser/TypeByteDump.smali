.class public Lcom/samsung/multiscreen/ble/adparser/TypeByteDump;
.super Lcom/samsung/multiscreen/ble/adparser/AdElement;
.source "TypeByteDump.java"


# instance fields
.field b:[B

.field type:I


# direct methods
.method public constructor <init>(I[BII)V
    .locals 2
    .param p1, "type"    # I
    .param p2, "data"    # [B
    .param p3, "pos"    # I
    .param p4, "len"    # I

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/samsung/multiscreen/ble/adparser/AdElement;-><init>()V

    .line 27
    iput p1, p0, Lcom/samsung/multiscreen/ble/adparser/TypeByteDump;->type:I

    .line 28
    new-array v0, p4, [B

    iput-object v0, p0, Lcom/samsung/multiscreen/ble/adparser/TypeByteDump;->b:[B

    .line 29
    iget-object v0, p0, Lcom/samsung/multiscreen/ble/adparser/TypeByteDump;->b:[B

    const/4 v1, 0x0

    invoke-static {p2, p3, v0, v1, p4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 30
    return-void
.end method


# virtual methods
.method public getBytes()[B
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/samsung/multiscreen/ble/adparser/TypeByteDump;->b:[B

    return-object v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 62
    iget v0, p0, Lcom/samsung/multiscreen/ble/adparser/TypeByteDump;->type:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 34
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    .line 35
    .local v1, "sb":Ljava/lang/StringBuffer;
    iget v3, p0, Lcom/samsung/multiscreen/ble/adparser/TypeByteDump;->type:I

    packed-switch v3, :pswitch_data_0

    .line 52
    :goto_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v3, p0, Lcom/samsung/multiscreen/ble/adparser/TypeByteDump;->b:[B

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 53
    if-lez v0, :cond_0

    .line 54
    const-string v3, ","

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 55
    :cond_0
    iget-object v3, p0, Lcom/samsung/multiscreen/ble/adparser/TypeByteDump;->b:[B

    aget-byte v3, v3, v0

    and-int/lit16 v2, v3, 0xff

    .line 56
    .local v2, "v":I
    invoke-static {v2}, Lcom/samsung/multiscreen/ble/adparser/TypeByteDump;->hex8(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 52
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 37
    .end local v0    # "i":I
    .end local v2    # "v":I
    :pswitch_0
    const-string v3, "Class of device: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 41
    :pswitch_1
    const-string v3, "Simple Pairing Hash C: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 45
    :pswitch_2
    const-string v3, "Simple Pairing Randomizer R: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 49
    :pswitch_3
    const-string v3, "TK Value: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    .line 58
    .restart local v0    # "i":I
    :cond_1
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v1}, Ljava/lang/String;-><init>(Ljava/lang/StringBuffer;)V

    return-object v3

    .line 35
    nop

    :pswitch_data_0
    .packed-switch 0xd
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
