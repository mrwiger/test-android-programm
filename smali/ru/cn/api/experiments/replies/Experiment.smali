.class public Lru/cn/api/experiments/replies/Experiment;
.super Ljava/lang/Object;
.source "Experiment.java"


# static fields
.field private static androidId:Ljava/lang/String;


# instance fields
.field public devices:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "devices"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lru/cn/api/experiments/replies/Device;",
            ">;"
        }
    .end annotation
.end field

.field public extras:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "extras"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public name:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "name"
    .end annotation
.end field

.field public percentage:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "percentage"
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    const/4 v0, 0x0

    sput-object v0, Lru/cn/api/experiments/replies/Experiment;->androidId:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const/16 v0, 0x64

    iput v0, p0, Lru/cn/api/experiments/replies/Experiment;->percentage:I

    .line 28
    return-void
.end method


# virtual methods
.method public isEligible(Landroid/content/Context;)Z
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 31
    sget-object v2, Lru/cn/api/experiments/replies/Experiment;->androidId:Ljava/lang/String;

    if-nez v2, :cond_0

    .line 32
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "android_id"

    invoke-static {v2, v3}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lru/cn/api/experiments/replies/Experiment;->androidId:Ljava/lang/String;

    .line 35
    :cond_0
    sget-object v2, Lru/cn/api/experiments/replies/Experiment;->androidId:Ljava/lang/String;

    if-eqz v2, :cond_2

    sget-object v2, Lru/cn/api/experiments/replies/Experiment;->androidId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    rem-int/lit8 v2, v2, 0x64

    iget v3, p0, Lru/cn/api/experiments/replies/Experiment;->percentage:I

    if-le v2, v3, :cond_2

    .line 43
    :cond_1
    :goto_0
    return v1

    .line 38
    :cond_2
    iget-object v2, p0, Lru/cn/api/experiments/replies/Experiment;->devices:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/api/experiments/replies/Device;

    .line 39
    .local v0, "device":Lru/cn/api/experiments/replies/Device;
    invoke-virtual {v0}, Lru/cn/api/experiments/replies/Device;->isCurrentDevice()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 40
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 48
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lru/cn/api/experiments/replies/Experiment;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "|"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lru/cn/api/experiments/replies/Experiment;->percentage:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%|devices="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lru/cn/api/experiments/replies/Experiment;->devices:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
