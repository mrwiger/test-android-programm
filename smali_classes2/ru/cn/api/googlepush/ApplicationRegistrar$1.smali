.class final Lru/cn/api/googlepush/ApplicationRegistrar$1;
.super Ljava/lang/Object;
.source "ApplicationRegistrar.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/api/googlepush/ApplicationRegistrar;->register(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Lru/cn/api/googlepush/ApplicationRegistrar$1;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 35
    const/4 v3, 0x0

    .line 38
    .local v3, "regId":Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lcom/google/android/gms/common/GoogleApiAvailability;->getInstance()Lcom/google/android/gms/common/GoogleApiAvailability;

    move-result-object v5

    iget-object v6, p0, Lru/cn/api/googlepush/ApplicationRegistrar$1;->val$context:Landroid/content/Context;

    invoke-virtual {v5, v6}, Lcom/google/android/gms/common/GoogleApiAvailability;->isGooglePlayServicesAvailable(Landroid/content/Context;)I

    move-result v4

    .line 39
    .local v4, "result":I
    if-nez v4, :cond_0

    .line 40
    iget-object v5, p0, Lru/cn/api/googlepush/ApplicationRegistrar$1;->val$context:Landroid/content/Context;

    invoke-static {v5}, Lcom/google/android/gms/iid/InstanceID;->getInstance(Landroid/content/Context;)Lcom/google/android/gms/iid/InstanceID;

    move-result-object v2

    .line 41
    .local v2, "instanceID":Lcom/google/android/gms/iid/InstanceID;
    const-string v5, "112505864152"

    const-string v6, "GCM"

    invoke-virtual {v2, v5, v6}, Lcom/google/android/gms/iid/InstanceID;->getToken(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 48
    .end local v2    # "instanceID":Lcom/google/android/gms/iid/InstanceID;
    .end local v4    # "result":I
    :cond_0
    :goto_0
    if-nez v3, :cond_1

    iget-object v5, p0, Lru/cn/api/googlepush/ApplicationRegistrar$1;->val$context:Landroid/content/Context;

    invoke-static {v5}, Lru/cn/api/googlepush/ApplicationRegistrar;->access$000(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 49
    invoke-static {v7}, Lru/cn/api/googlepush/ApplicationRegistrar;->access$102(Z)Z

    .line 59
    :goto_1
    return-void

    .line 43
    :catch_0
    move-exception v0

    .line 44
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {v0}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 54
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    :try_start_1
    iget-object v5, p0, Lru/cn/api/googlepush/ApplicationRegistrar$1;->val$context:Landroid/content/Context;

    invoke-static {v5, v3}, Lru/cn/api/googlepush/ApplicationRegistrar;->access$200(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 58
    :goto_2
    invoke-static {v7}, Lru/cn/api/googlepush/ApplicationRegistrar;->access$102(Z)Z

    goto :goto_1

    .line 55
    :catch_1
    move-exception v1

    .line 56
    .local v1, "ex":Ljava/lang/Exception;
    invoke-static {v1}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    goto :goto_2
.end method
