.class final Lcom/google/obf/ch;
.super Ljava/lang/Object;
.source "IMASDK"


# instance fields
.field public a:[B

.field public b:I

.field private final c:I

.field private d:Z

.field private e:Z


# direct methods
.method public constructor <init>(II)V
    .locals 3

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput p1, p0, Lcom/google/obf/ch;->c:I

    .line 3
    add-int/lit8 v0, p2, 0x3

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/google/obf/ch;->a:[B

    .line 4
    iget-object v0, p0, Lcom/google/obf/ch;->a:[B

    const/4 v1, 0x2

    const/4 v2, 0x1

    aput-byte v2, v0, v1

    .line 5
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 6
    iput-boolean v0, p0, Lcom/google/obf/ch;->d:Z

    .line 7
    iput-boolean v0, p0, Lcom/google/obf/ch;->e:Z

    .line 8
    return-void
.end method

.method public a(I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 10
    iget-boolean v0, p0, Lcom/google/obf/ch;->d:Z

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/obf/dl;->b(Z)V

    .line 11
    iget v0, p0, Lcom/google/obf/ch;->c:I

    if-ne p1, v0, :cond_2

    :goto_1
    iput-boolean v1, p0, Lcom/google/obf/ch;->d:Z

    .line 12
    iget-boolean v0, p0, Lcom/google/obf/ch;->d:Z

    if-eqz v0, :cond_0

    .line 13
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/obf/ch;->b:I

    .line 14
    iput-boolean v2, p0, Lcom/google/obf/ch;->e:Z

    .line 15
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 10
    goto :goto_0

    :cond_2
    move v1, v2

    .line 11
    goto :goto_1
.end method

.method public a([BII)V
    .locals 3

    .prologue
    .line 16
    iget-boolean v0, p0, Lcom/google/obf/ch;->d:Z

    if-nez v0, :cond_0

    .line 23
    :goto_0
    return-void

    .line 18
    :cond_0
    sub-int v0, p3, p2

    .line 19
    iget-object v1, p0, Lcom/google/obf/ch;->a:[B

    array-length v1, v1

    iget v2, p0, Lcom/google/obf/ch;->b:I

    add-int/2addr v2, v0

    if-ge v1, v2, :cond_1

    .line 20
    iget-object v1, p0, Lcom/google/obf/ch;->a:[B

    iget v2, p0, Lcom/google/obf/ch;->b:I

    add-int/2addr v2, v0

    mul-int/lit8 v2, v2, 0x2

    invoke-static {v1, v2}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v1

    iput-object v1, p0, Lcom/google/obf/ch;->a:[B

    .line 21
    :cond_1
    iget-object v1, p0, Lcom/google/obf/ch;->a:[B

    iget v2, p0, Lcom/google/obf/ch;->b:I

    invoke-static {p1, p2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 22
    iget v1, p0, Lcom/google/obf/ch;->b:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/obf/ch;->b:I

    goto :goto_0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 9
    iget-boolean v0, p0, Lcom/google/obf/ch;->e:Z

    return v0
.end method

.method public b(I)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 24
    iget-boolean v2, p0, Lcom/google/obf/ch;->d:Z

    if-nez v2, :cond_0

    .line 29
    :goto_0
    return v0

    .line 26
    :cond_0
    iget v2, p0, Lcom/google/obf/ch;->b:I

    sub-int/2addr v2, p1

    iput v2, p0, Lcom/google/obf/ch;->b:I

    .line 27
    iput-boolean v0, p0, Lcom/google/obf/ch;->d:Z

    .line 28
    iput-boolean v1, p0, Lcom/google/obf/ch;->e:Z

    move v0, v1

    .line 29
    goto :goto_0
.end method
