.class public final Lru/cn/mvvm/ViewModels;
.super Ljava/lang/Object;
.source "ViewModels.java"


# direct methods
.method public static get(Landroid/support/v4/app/Fragment;Ltoothpick/Scope;Ljava/lang/Class;)Landroid/arch/lifecycle/ViewModel;
    .locals 1
    .param p0, "fragment"    # Landroid/support/v4/app/Fragment;
    .param p1, "scope"    # Ltoothpick/Scope;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/arch/lifecycle/ViewModel;",
            ">(",
            "Landroid/support/v4/app/Fragment;",
            "Ltoothpick/Scope;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 13
    .local p2, "modelClass":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    new-instance v0, Lru/cn/mvvm/ToothpickFactory;

    invoke-direct {v0, p1}, Lru/cn/mvvm/ToothpickFactory;-><init>(Ltoothpick/Scope;)V

    .line 14
    invoke-static {p0, v0}, Landroid/arch/lifecycle/ViewModelProviders;->of(Landroid/support/v4/app/Fragment;Landroid/arch/lifecycle/ViewModelProvider$Factory;)Landroid/arch/lifecycle/ViewModelProvider;

    move-result-object v0

    .line 15
    invoke-virtual {v0, p2}, Landroid/arch/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroid/arch/lifecycle/ViewModel;

    move-result-object v0

    .line 13
    return-object v0
.end method

.method public static get(Landroid/support/v4/app/FragmentActivity;Ltoothpick/Scope;Ljava/lang/Class;)Landroid/arch/lifecycle/ViewModel;
    .locals 1
    .param p0, "activity"    # Landroid/support/v4/app/FragmentActivity;
    .param p1, "scope"    # Ltoothpick/Scope;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/arch/lifecycle/ViewModel;",
            ">(",
            "Landroid/support/v4/app/FragmentActivity;",
            "Ltoothpick/Scope;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 19
    .local p2, "modelClass":Ljava/lang/Class;, "Ljava/lang/Class<TT;>;"
    new-instance v0, Lru/cn/mvvm/ToothpickFactory;

    invoke-direct {v0, p1}, Lru/cn/mvvm/ToothpickFactory;-><init>(Ltoothpick/Scope;)V

    .line 20
    invoke-static {p0, v0}, Landroid/arch/lifecycle/ViewModelProviders;->of(Landroid/support/v4/app/FragmentActivity;Landroid/arch/lifecycle/ViewModelProvider$Factory;)Landroid/arch/lifecycle/ViewModelProvider;

    move-result-object v0

    .line 21
    invoke-virtual {v0, p2}, Landroid/arch/lifecycle/ViewModelProvider;->get(Ljava/lang/Class;)Landroid/arch/lifecycle/ViewModel;

    move-result-object v0

    .line 19
    return-object v0
.end method
