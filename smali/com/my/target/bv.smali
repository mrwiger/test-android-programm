.class public Lcom/my/target/bv;
.super Landroid/widget/ImageView;
.source "CacheImageView.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "AppCompatCustomView"
    }
.end annotation


# instance fields
.field private final in:Landroid/graphics/Paint;

.field private final io:Landroid/graphics/Rect;

.field private ip:Landroid/graphics/drawable/GradientDrawable;

.field private iq:Landroid/graphics/Bitmap;

.field private ir:Landroid/graphics/Rect;

.field private is:I

.field private it:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 41
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 30
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/my/target/bv;->in:Landroid/graphics/Paint;

    .line 42
    iget-object v0, p0, Lcom/my/target/bv;->in:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 43
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/my/target/bv;->io:Landroid/graphics/Rect;

    .line 44
    return-void
.end method


# virtual methods
.method public a(Landroid/graphics/Bitmap;Z)V
    .locals 4

    .prologue
    .line 78
    if-eqz p2, :cond_0

    .line 80
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/my/target/bv;->setAlpha(F)V

    .line 81
    invoke-virtual {p0, p1}, Lcom/my/target/bv;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 82
    invoke-virtual {p0}, Lcom/my/target/bv;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 88
    :goto_0
    return-void

    .line 86
    :cond_0
    invoke-virtual {p0, p1}, Lcom/my/target/bv;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public c(II)V
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/my/target/bv;->ip:Landroid/graphics/drawable/GradientDrawable;

    if-nez v0, :cond_0

    .line 108
    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    iput-object v0, p0, Lcom/my/target/bv;->ip:Landroid/graphics/drawable/GradientDrawable;

    .line 111
    :cond_0
    iget-object v0, p0, Lcom/my/target/bv;->ip:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/drawable/GradientDrawable;->setStroke(II)V

    .line 112
    invoke-virtual {p0}, Lcom/my/target/bv;->invalidate()V

    .line 113
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 222
    iget-object v0, p0, Lcom/my/target/bv;->iq:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 232
    :goto_0
    return-void

    .line 227
    :cond_0
    iget-object v0, p0, Lcom/my/target/bv;->ip:Landroid/graphics/drawable/GradientDrawable;

    if-eqz v0, :cond_1

    .line 229
    iget-object v0, p0, Lcom/my/target/bv;->ip:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/GradientDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 231
    :cond_1
    iget-object v0, p0, Lcom/my/target/bv;->iq:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/my/target/bv;->ir:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/my/target/bv;->io:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/my/target/bv;->in:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 5
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 206
    invoke-super/range {p0 .. p5}, Landroid/widget/ImageView;->onLayout(ZIIII)V

    .line 208
    iget-object v0, p0, Lcom/my/target/bv;->ip:Landroid/graphics/drawable/GradientDrawable;

    if-eqz v0, :cond_0

    .line 210
    iget-object v0, p0, Lcom/my/target/bv;->ip:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {p0}, Lcom/my/target/bv;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, Lcom/my/target/bv;->getPaddingTop()I

    move-result v2

    invoke-virtual {p0}, Lcom/my/target/bv;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/my/target/bv;->getHeight()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/GradientDrawable;->setBounds(IIII)V

    .line 213
    :cond_0
    iget-object v0, p0, Lcom/my/target/bv;->io:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/my/target/bv;->getPaddingLeft()I

    move-result v1

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 214
    iget-object v0, p0, Lcom/my/target/bv;->io:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/my/target/bv;->getPaddingTop()I

    move-result v1

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 215
    iget-object v0, p0, Lcom/my/target/bv;->io:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/my/target/bv;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/my/target/bv;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 216
    iget-object v0, p0, Lcom/my/target/bv;->io:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/my/target/bv;->getHeight()I

    move-result v1

    invoke-virtual {p0}, Lcom/my/target/bv;->getPaddingBottom()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 217
    return-void
.end method

.method protected onMeasure(II)V
    .locals 11
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/4 v2, 0x0

    const/high16 v10, 0x40000000    # 2.0f

    const/high16 v1, -0x80000000

    .line 129
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    .line 130
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v7

    .line 131
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    .line 132
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v8

    .line 134
    if-nez v3, :cond_a

    move v6, v1

    .line 139
    :goto_0
    if-nez v0, :cond_0

    move v0, v1

    .line 145
    :cond_0
    iget-object v3, p0, Lcom/my/target/bv;->iq:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_3

    .line 147
    iget-object v3, p0, Lcom/my/target/bv;->iq:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    .line 148
    iget-object v3, p0, Lcom/my/target/bv;->iq:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    move v5, v4

    move v4, v3

    .line 161
    :goto_1
    if-eqz v4, :cond_9

    .line 163
    int-to-float v3, v5

    int-to-float v9, v4

    div-float/2addr v3, v9

    .line 166
    :goto_2
    if-eqz v8, :cond_1

    .line 168
    int-to-float v2, v7

    int-to-float v9, v8

    div-float/2addr v2, v9

    .line 171
    :cond_1
    if-ne v6, v10, :cond_5

    if-ne v0, v10, :cond_5

    .line 173
    invoke-virtual {p0, v7, v8}, Lcom/my/target/bv;->setMeasuredDimension(II)V

    .line 201
    :cond_2
    :goto_3
    return-void

    .line 150
    :cond_3
    iget v3, p0, Lcom/my/target/bv;->it:I

    if-eqz v3, :cond_4

    iget v3, p0, Lcom/my/target/bv;->is:I

    if-eqz v3, :cond_4

    .line 152
    iget v4, p0, Lcom/my/target/bv;->is:I

    .line 153
    iget v3, p0, Lcom/my/target/bv;->it:I

    move v5, v4

    move v4, v3

    goto :goto_1

    .line 157
    :cond_4
    invoke-super {p0, p1, p2}, Landroid/widget/ImageView;->onMeasure(II)V

    goto :goto_3

    .line 175
    :cond_5
    if-ne v6, v1, :cond_7

    if-ne v0, v1, :cond_7

    .line 178
    cmpg-float v0, v3, v2

    if-gez v0, :cond_6

    .line 180
    invoke-static {v4, v8}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 181
    int-to-float v1, v0

    mul-float/2addr v1, v3

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 182
    invoke-virtual {p0, v1, v0}, Lcom/my/target/bv;->setMeasuredDimension(II)V

    goto :goto_3

    .line 186
    :cond_6
    invoke-static {v5, v8}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 187
    int-to-float v1, v0

    div-float/2addr v1, v3

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 188
    invoke-virtual {p0, v0, v1}, Lcom/my/target/bv;->setMeasuredDimension(II)V

    goto :goto_3

    .line 191
    :cond_7
    if-ne v6, v1, :cond_8

    if-ne v0, v10, :cond_8

    .line 193
    int-to-float v0, v8

    mul-float/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 194
    invoke-virtual {p0, v0, v8}, Lcom/my/target/bv;->setMeasuredDimension(II)V

    goto :goto_3

    .line 196
    :cond_8
    if-ne v6, v10, :cond_2

    if-ne v0, v1, :cond_2

    .line 198
    int-to-float v0, v7

    div-float/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 199
    invoke-virtual {p0, v7, v0}, Lcom/my/target/bv;->setMeasuredDimension(II)V

    goto :goto_3

    :cond_9
    move v3, v2

    goto :goto_2

    :cond_a
    move v6, v3

    goto :goto_0
.end method

.method public setImageBitmap(Landroid/graphics/Bitmap;)V
    .locals 4
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v3, 0x0

    .line 93
    iput-object p1, p0, Lcom/my/target/bv;->iq:Landroid/graphics/Bitmap;

    .line 94
    if-eqz p1, :cond_0

    .line 96
    invoke-virtual {p0, v3}, Lcom/my/target/bv;->setBackgroundColor(I)V

    .line 97
    new-instance v0, Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-direct {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lcom/my/target/bv;->ir:Landroid/graphics/Rect;

    .line 100
    :cond_0
    invoke-virtual {p0}, Lcom/my/target/bv;->requestLayout()V

    .line 101
    invoke-virtual {p0}, Lcom/my/target/bv;->invalidate()V

    .line 102
    return-void
.end method

.method public setImageData(Lcom/my/target/common/models/ImageData;)V
    .locals 1
    .param p1, "imageData"    # Lcom/my/target/common/models/ImageData;

    .prologue
    .line 48
    if-nez p1, :cond_0

    .line 50
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/my/target/bv;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 56
    :goto_0
    return-void

    .line 54
    :cond_0
    invoke-virtual {p1}, Lcom/my/target/common/models/ImageData;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/my/target/bv;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public final setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 61
    const-string v0, "Unable to set custom image drawable to generated view"

    invoke-static {v0}, Lcom/my/target/g;->c(Ljava/lang/String;)V

    .line 62
    return-void
.end method

.method public setImageResource(I)V
    .locals 1
    .param p1, "resId"    # I

    .prologue
    .line 67
    const-string v0, "Unable to set custom image resource to generated view"

    invoke-static {v0}, Lcom/my/target/g;->c(Ljava/lang/String;)V

    .line 68
    return-void
.end method

.method public setImageURI(Landroid/net/Uri;)V
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 73
    const-string v0, "Unable to set custom image uri to generated view"

    invoke-static {v0}, Lcom/my/target/g;->c(Ljava/lang/String;)V

    .line 74
    return-void
.end method

.method public setPlaceholderHeight(I)V
    .locals 0
    .param p1, "placeholderHeight"    # I

    .prologue
    .line 122
    iput p1, p0, Lcom/my/target/bv;->it:I

    .line 123
    return-void
.end method

.method public setPlaceholderWidth(I)V
    .locals 0
    .param p1, "placeholderWidth"    # I

    .prologue
    .line 117
    iput p1, p0, Lcom/my/target/bv;->is:I

    .line 118
    return-void
.end method
