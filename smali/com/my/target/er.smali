.class abstract Lcom/my/target/er;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "PromoAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Landroid/support/v7/widget/RecyclerView$ViewHolder;",
        ">",
        "Landroid/support/v7/widget/RecyclerView$Adapter",
        "<TT;>;"
    }
.end annotation


# instance fields
.field final context:Landroid/content/Context;

.field dl:Landroid/view/View$OnClickListener;

.field dm:Landroid/view/View$OnClickListener;

.field final interstitialAdCards:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/my/target/core/models/banners/e;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/util/List;Landroid/content/Context;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/my/target/core/models/banners/e;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/my/target/er;->interstitialAdCards:Ljava/util/List;

    .line 29
    iput-object p2, p0, Lcom/my/target/er;->context:Landroid/content/Context;

    .line 30
    return-void
.end method


# virtual methods
.method public getItemCount()I
    .locals 1

    .prologue
    .line 35
    .line 1064
    iget-object v0, p0, Lcom/my/target/er;->interstitialAdCards:Ljava/util/List;

    .line 35
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItemViewType(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 41
    .local p0, "this":Lcom/my/target/er;, "Lcom/my/target/er<TT;>;"
    if-nez p1, :cond_0

    .line 43
    const/4 v0, 0x1

    .line 52
    :goto_0
    return v0

    .line 46
    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;->getItemCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_1

    .line 48
    const/4 v0, 0x2

    goto :goto_0

    .line 52
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
