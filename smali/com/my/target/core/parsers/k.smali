.class public final Lcom/my/target/core/parsers/k;
.super Ljava/lang/Object;
.source "InstreamAudioAdSectionParser.java"


# instance fields
.field private final H:Lcom/my/target/bf;


# direct methods
.method private constructor <init>(Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    invoke-static {p1, p2, p3}, Lcom/my/target/bf;->b(Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/bf;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/core/parsers/k;->H:Lcom/my/target/bf;

    .line 27
    return-void
.end method

.method public static a(Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/core/parsers/k;
    .locals 1

    .prologue
    .line 19
    new-instance v0, Lcom/my/target/core/parsers/k;

    invoke-direct {v0, p0, p1, p2}, Lcom/my/target/core/parsers/k;-><init>(Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method public final a(Lorg/json/JSONObject;Lcom/my/target/fm;)V
    .locals 6

    .prologue
    .line 31
    iget-object v0, p0, Lcom/my/target/core/parsers/k;->H:Lcom/my/target/bf;

    invoke-virtual {v0, p1, p2}, Lcom/my/target/bf;->a(Lorg/json/JSONObject;Lcom/my/target/ak;)V

    .line 32
    const-string v0, "settings"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 33
    if-eqz v2, :cond_2

    .line 35
    invoke-virtual {p2}, Lcom/my/target/fm;->i()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/al;

    .line 37
    invoke-virtual {v0}, Lcom/my/target/al;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 38
    if-eqz v1, :cond_0

    .line 1048
    const-string v4, "connectionTimeout"

    invoke-virtual {v0}, Lcom/my/target/al;->P()I

    move-result v5

    invoke-virtual {v1, v4, v5}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v4

    invoke-virtual {v0, v4}, Lcom/my/target/al;->e(I)V

    .line 1049
    const-string v4, "maxBannersShow"

    invoke-virtual {v0}, Lcom/my/target/al;->Q()I

    move-result v5

    invoke-virtual {v1, v4, v5}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v1

    .line 1050
    if-eqz v1, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Lcom/my/target/al;->f(I)V

    goto :goto_0

    :cond_1
    const/4 v1, -0x1

    goto :goto_1

    .line 44
    :cond_2
    return-void
.end method
