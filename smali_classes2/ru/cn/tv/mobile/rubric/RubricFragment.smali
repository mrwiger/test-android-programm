.class public abstract Lru/cn/tv/mobile/rubric/RubricFragment;
.super Landroid/support/v4/app/Fragment;
.source "RubricFragment.java"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v4/app/Fragment;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field private final RUBRIC_LOADER_ID:I

.field protected listener:Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastSelectedListener;

.field private rubricId:J


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 22
    const/4 v0, 0x0

    iput v0, p0, Lru/cn/tv/mobile/rubric/RubricFragment;->RUBRIC_LOADER_ID:I

    .line 29
    return-void
.end method

.method private init()V
    .locals 4

    .prologue
    .line 61
    invoke-virtual {p0}, Lru/cn/tv/mobile/rubric/RubricFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lru/cn/tv/mobile/rubric/RubricFragment;->rubricId:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 62
    invoke-virtual {p0}, Lru/cn/tv/mobile/rubric/RubricFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/LoaderManager;->hasRunningLoaders()Z

    move-result v0

    if-nez v0, :cond_0

    .line 63
    invoke-virtual {p0}, Lru/cn/tv/mobile/rubric/RubricFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 65
    :cond_0
    return-void
.end method


# virtual methods
.method protected abstract layout()I
.end method

.method public final onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 7
    .param p1, "id"    # I
    .param p2, "bundle"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 69
    packed-switch p1, :pswitch_data_0

    move-object v0, v3

    .line 75
    :goto_0
    return-object v0

    .line 71
    :pswitch_0
    new-instance v0, Landroid/support/v4/content/CursorLoader;

    invoke-virtual {p0}, Lru/cn/tv/mobile/rubric/RubricFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-wide v4, p0, Lru/cn/tv/mobile/rubric/RubricFragment;->rubricId:J

    .line 72
    invoke-static {v4, v5}, Lru/cn/api/provider/TvContentProviderContract;->rubricatorUri(J)Landroid/net/Uri;

    move-result-object v2

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 69
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 33
    invoke-virtual {p0}, Lru/cn/tv/mobile/rubric/RubricFragment;->layout()I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 4
    .param p2, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 80
    .local p1, "loader":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 101
    :goto_0
    return-void

    .line 82
    :pswitch_0
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_0

    .line 83
    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    .line 84
    const-string v2, "data"

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 85
    .local v1, "d":Ljava/lang/String;
    invoke-static {v1}, Lru/cn/api/catalogue/replies/Rubric;->fromJson(Ljava/lang/String;)Lru/cn/api/catalogue/replies/Rubric;

    move-result-object v0

    .line 87
    .local v0, "currentRubric":Lru/cn/api/catalogue/replies/Rubric;
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    new-instance v3, Lru/cn/tv/mobile/rubric/RubricFragment$1;

    invoke-direct {v3, p0, v0}, Lru/cn/tv/mobile/rubric/RubricFragment$1;-><init>(Lru/cn/tv/mobile/rubric/RubricFragment;Lru/cn/api/catalogue/replies/Rubric;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->postAtFrontOfQueue(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 94
    .end local v0    # "currentRubric":Lru/cn/api/catalogue/replies/Rubric;
    .end local v1    # "d":Ljava/lang/String;
    :cond_0
    invoke-virtual {p0}, Lru/cn/tv/mobile/rubric/RubricFragment;->rubricInfoLoaderError()V

    goto :goto_0

    .line 80
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 20
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lru/cn/tv/mobile/rubric/RubricFragment;->onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public final onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 105
    .local p1, "loader":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 38
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 39
    invoke-direct {p0}, Lru/cn/tv/mobile/rubric/RubricFragment;->init()V

    .line 40
    return-void
.end method

.method protected abstract rubricInfoLoaded(Lru/cn/api/catalogue/replies/Rubric;)V
.end method

.method protected rubricInfoLoaderError()V
    .locals 0

    .prologue
    .line 108
    return-void
.end method

.method public setListener(Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastSelectedListener;)Lru/cn/tv/mobile/rubric/RubricFragment;
    .locals 0
    .param p1, "l"    # Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastSelectedListener;

    .prologue
    .line 111
    iput-object p1, p0, Lru/cn/tv/mobile/rubric/RubricFragment;->listener:Lru/cn/tv/mobile/telecasts/TelecastsListFragment$OnTelecastSelectedListener;

    .line 112
    return-object p0
.end method

.method public setRubricId(J)Lru/cn/tv/mobile/rubric/RubricFragment;
    .locals 3
    .param p1, "id"    # J

    .prologue
    .line 47
    invoke-virtual {p0}, Lru/cn/tv/mobile/rubric/RubricFragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lru/cn/tv/mobile/rubric/RubricFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/LoaderManager;->hasRunningLoaders()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 48
    invoke-virtual {p0}, Lru/cn/tv/mobile/rubric/RubricFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/LoaderManager;->destroyLoader(I)V

    .line 51
    :cond_0
    iput-wide p1, p0, Lru/cn/tv/mobile/rubric/RubricFragment;->rubricId:J

    .line 52
    invoke-direct {p0}, Lru/cn/tv/mobile/rubric/RubricFragment;->init()V

    .line 53
    return-object p0
.end method
