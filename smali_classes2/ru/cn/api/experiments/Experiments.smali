.class public final Lru/cn/api/experiments/Experiments;
.super Ljava/lang/Object;
.source "Experiments.java"


# static fields
.field private static eligibleExperiments:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lru/cn/api/experiments/replies/Experiment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lru/cn/api/experiments/Experiments;->eligibleExperiments:Ljava/util/Map;

    return-void
.end method

.method public static eligibleForExperiment(Ljava/lang/String;)Z
    .locals 1
    .param p0, "experimentName"    # Ljava/lang/String;

    .prologue
    .line 67
    sget-object v0, Lru/cn/api/experiments/Experiments;->eligibleExperiments:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static extras(Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .param p0, "experimentName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 71
    sget-object v1, Lru/cn/api/experiments/Experiments;->eligibleExperiments:Ljava/util/Map;

    invoke-interface {v1, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/api/experiments/replies/Experiment;

    .line 72
    .local v0, "experiment":Lru/cn/api/experiments/replies/Experiment;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lru/cn/api/experiments/replies/Experiment;->extras:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lru/cn/api/experiments/replies/Experiment;->extras:Ljava/util/List;

    :goto_0
    return-object v1

    :cond_0
    sget-object v1, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    goto :goto_0
.end method

.method public static initialize(Landroid/content/Context;)V
    .locals 9
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 35
    new-instance v6, Lretrofit2/Retrofit$Builder;

    invoke-direct {v6}, Lretrofit2/Retrofit$Builder;-><init>()V

    const-string v7, "http://firmware.cn.ru/peerstv/experiments/1/"

    .line 36
    invoke-virtual {v6, v7}, Lretrofit2/Retrofit$Builder;->baseUrl(Ljava/lang/String;)Lretrofit2/Retrofit$Builder;

    move-result-object v6

    .line 37
    invoke-static {}, Lretrofit2/converter/gson/GsonConverterFactory;->create()Lretrofit2/converter/gson/GsonConverterFactory;

    move-result-object v7

    invoke-virtual {v6, v7}, Lretrofit2/Retrofit$Builder;->addConverterFactory(Lretrofit2/Converter$Factory;)Lretrofit2/Retrofit$Builder;

    move-result-object v6

    .line 38
    invoke-static {}, Lretrofit2/adapter/rxjava2/RxJava2CallAdapterFactory;->create()Lretrofit2/adapter/rxjava2/RxJava2CallAdapterFactory;

    move-result-object v7

    invoke-virtual {v6, v7}, Lretrofit2/Retrofit$Builder;->addCallAdapterFactory(Lretrofit2/CallAdapter$Factory;)Lretrofit2/Retrofit$Builder;

    move-result-object v6

    .line 39
    invoke-virtual {v6}, Lretrofit2/Retrofit$Builder;->build()Lretrofit2/Retrofit;

    move-result-object v4

    .line 41
    .local v4, "retrofit":Lretrofit2/Retrofit;
    const-class v6, Lru/cn/api/experiments/retrofit/ExperimentsAPI;

    invoke-virtual {v4, v6}, Lretrofit2/Retrofit;->create(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lru/cn/api/experiments/retrofit/ExperimentsAPI;

    .line 43
    .local v5, "service":Lru/cn/api/experiments/retrofit/ExperimentsAPI;
    const/4 v2, 0x0

    .line 45
    .local v2, "experiments":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/experiments/replies/Experiment;>;"
    :try_start_0
    invoke-interface {v5}, Lru/cn/api/experiments/retrofit/ExperimentsAPI;->experiments()Lio/reactivex/Single;

    move-result-object v6

    invoke-virtual {v6}, Lio/reactivex/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lru/cn/api/experiments/replies/ExperimentsReply;

    .line 46
    .local v3, "response":Lru/cn/api/experiments/replies/ExperimentsReply;
    if-eqz v3, :cond_0

    .line 47
    iget-object v2, v3, Lru/cn/api/experiments/replies/ExperimentsReply;->experiments:Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 53
    .end local v3    # "response":Lru/cn/api/experiments/replies/ExperimentsReply;
    :cond_0
    :goto_0
    if-nez v2, :cond_1

    .line 64
    :goto_1
    return-void

    .line 49
    :catch_0
    move-exception v0

    .line 50
    .local v0, "e":Ljava/lang/Exception;
    const-string v6, "Experiments"

    const-string v7, "failed to initialize: "

    invoke-static {v6, v7, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 56
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_2
    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/cn/api/experiments/replies/Experiment;

    .line 57
    .local v1, "experiment":Lru/cn/api/experiments/replies/Experiment;
    invoke-virtual {v1, p0}, Lru/cn/api/experiments/replies/Experiment;->isEligible(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 58
    sget-object v7, Lru/cn/api/experiments/Experiments;->eligibleExperiments:Ljava/util/Map;

    iget-object v8, v1, Lru/cn/api/experiments/replies/Experiment;->name:Ljava/lang/String;

    invoke-interface {v7, v8, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 62
    .end local v1    # "experiment":Lru/cn/api/experiments/replies/Experiment;
    :cond_3
    const-string v7, "Experiments"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Found experiments: "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v6, Lru/cn/api/experiments/Experiments;->eligibleExperiments:Ljava/util/Map;

    .line 63
    invoke-interface {v6}, Ljava/util/Map;->size()I

    move-result v6

    if-nez v6, :cond_4

    const-string v6, "none"

    :goto_3
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 62
    invoke-static {v7, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 63
    :cond_4
    sget-object v6, Lru/cn/api/experiments/Experiments;->eligibleExperiments:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v6

    goto :goto_3
.end method
