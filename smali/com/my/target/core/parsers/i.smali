.class public final Lcom/my/target/core/parsers/i;
.super Ljava/lang/Object;
.source "NativeAppwallAdSectionParser.java"


# instance fields
.field private final m:Lcom/my/target/bf;


# direct methods
.method private constructor <init>(Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    invoke-static {p1, p2, p3}, Lcom/my/target/bf;->b(Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/bf;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/core/parsers/i;->m:Lcom/my/target/bf;

    .line 35
    return-void
.end method

.method public static a(Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/core/parsers/i;
    .locals 1

    .prologue
    .line 27
    new-instance v0, Lcom/my/target/core/parsers/i;

    invoke-direct {v0, p0, p1, p2}, Lcom/my/target/core/parsers/i;-><init>(Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method public final a(Lorg/json/JSONObject;Lcom/my/target/fg;)V
    .locals 8

    .prologue
    .line 39
    iget-object v0, p0, Lcom/my/target/core/parsers/i;->m:Lcom/my/target/bf;

    invoke-virtual {v0, p1, p2}, Lcom/my/target/bf;->a(Lorg/json/JSONObject;Lcom/my/target/ak;)V

    .line 40
    const-string v0, "settings"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 41
    if-eqz v0, :cond_1

    .line 1049
    const-string v1, "title"

    invoke-virtual {p2}, Lcom/my/target/fg;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/my/target/fg;->setTitle(Ljava/lang/String;)V

    .line 1050
    const-string v1, "icon_hd"

    invoke-virtual {p2}, Lcom/my/target/fg;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/my/target/fg;->b(Ljava/lang/String;)V

    .line 1051
    const-string v1, "bubble_icon_hd"

    invoke-virtual {p2}, Lcom/my/target/fg;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/my/target/fg;->c(Ljava/lang/String;)V

    .line 1052
    const-string v1, "label_icon_hd"

    invoke-virtual {p2}, Lcom/my/target/fg;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/my/target/fg;->d(Ljava/lang/String;)V

    .line 1053
    const-string v1, "goto_app_icon_hd"

    invoke-virtual {p2}, Lcom/my/target/fg;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/my/target/fg;->e(Ljava/lang/String;)V

    .line 1054
    const-string v1, "item_highlight_icon"

    invoke-virtual {p2}, Lcom/my/target/fg;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/my/target/fg;->f(Ljava/lang/String;)V

    .line 1056
    const-string v1, "icon_status"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    .line 1058
    if-eqz v1, :cond_1

    .line 1060
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v2

    .line 1061
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    .line 1063
    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    .line 1064
    if-eqz v3, :cond_0

    .line 1066
    invoke-virtual {p2}, Lcom/my/target/fg;->f()Ljava/util/ArrayList;

    move-result-object v4

    new-instance v5, Landroid/util/Pair;

    const-string v6, "value"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "icon_hd"

    .line 1067
    invoke-virtual {v3, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v5, v6, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1066
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1061
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 45
    :cond_1
    return-void
.end method
