.class public Lru/cn/peersay/RemoteCommandReceiver;
.super Landroid/content/BroadcastReceiver;
.source "RemoteCommandReceiver.java"


# static fields
.field private static final coldStartHandler:Lru/cn/peersay/controllers/RemoteIntentController;

.field private static final controllers:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lru/cn/peersay/controllers/RemoteIntentController;",
            ">;"
        }
    .end annotation
.end field

.field private static peersayInstalled:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lru/cn/peersay/RemoteCommandReceiver;->controllers:Ljava/util/List;

    .line 23
    new-instance v0, Lru/cn/peersay/controllers/PlayContentController;

    invoke-direct {v0}, Lru/cn/peersay/controllers/PlayContentController;-><init>()V

    sput-object v0, Lru/cn/peersay/RemoteCommandReceiver;->coldStartHandler:Lru/cn/peersay/controllers/RemoteIntentController;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method public static attachController(Lru/cn/peersay/controllers/RemoteIntentController;)V
    .locals 1
    .param p0, "controller"    # Lru/cn/peersay/controllers/RemoteIntentController;

    .prologue
    .line 26
    sget-object v0, Lru/cn/peersay/RemoteCommandReceiver;->controllers:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 27
    return-void
.end method

.method public static detachController(Lru/cn/peersay/controllers/RemoteIntentController;)V
    .locals 1
    .param p0, "controller"    # Lru/cn/peersay/controllers/RemoteIntentController;

    .prologue
    .line 30
    sget-object v0, Lru/cn/peersay/RemoteCommandReceiver;->controllers:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 31
    return-void
.end method

.method public static isPeersayInstalled(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 75
    sget-object v2, Lru/cn/peersay/RemoteCommandReceiver;->peersayInstalled:Ljava/lang/Boolean;

    if-eqz v2, :cond_0

    .line 76
    sget-object v2, Lru/cn/peersay/RemoteCommandReceiver;->peersayInstalled:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 87
    :goto_0
    return v2

    .line 79
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 80
    .local v1, "pm":Landroid/content/pm/PackageManager;
    const-string v2, "com.ntk.peersremoteservice"

    const/4 v3, 0x3

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    .line 81
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    sput-object v2, Lru/cn/peersay/RemoteCommandReceiver;->peersayInstalled:Ljava/lang/Boolean;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 87
    .end local v1    # "pm":Landroid/content/pm/PackageManager;
    :goto_1
    sget-object v2, Lru/cn/peersay/RemoteCommandReceiver;->peersayInstalled:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    goto :goto_0

    .line 83
    :catch_0
    move-exception v0

    .line 84
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    sput-object v2, Lru/cn/peersay/RemoteCommandReceiver;->peersayInstalled:Ljava/lang/Boolean;

    goto :goto_1
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 35
    invoke-virtual {p2}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v6

    .line 36
    .local v6, "type":Ljava/lang/String;
    const-string v7, "application/inetra.peerstv"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 72
    :goto_0
    return-void

    .line 39
    :cond_0
    invoke-static {p2}, Lru/cn/peersay/IntentMessage;->createFromIntent(Landroid/content/Intent;)Lru/cn/peersay/IntentMessage;

    move-result-object v3

    .line 40
    .local v3, "message":Lru/cn/peersay/IntentMessage;
    if-nez v3, :cond_1

    .line 41
    const-string v7, "RemoteCommandReceiver"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Malformed message "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {p2}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 45
    :cond_1
    const-string v7, "RemoteCommandReceiver"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Command received "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, v3, Lru/cn/peersay/IntentMessage;->extras:Landroid/os/Bundle;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 47
    const/4 v2, 0x0

    .line 48
    .local v2, "handled":Z
    sget-object v7, Lru/cn/peersay/RemoteCommandReceiver;->controllers:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/cn/peersay/controllers/RemoteIntentController;

    .line 49
    .local v1, "controller":Lru/cn/peersay/controllers/RemoteIntentController;
    invoke-interface {v1, p1, v3}, Lru/cn/peersay/controllers/RemoteIntentController;->handleMessage(Landroid/content/Context;Lru/cn/peersay/IntentMessage;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 50
    const/4 v2, 0x1

    .line 55
    .end local v1    # "controller":Lru/cn/peersay/controllers/RemoteIntentController;
    :cond_3
    if-nez v2, :cond_4

    .line 56
    sget-object v7, Lru/cn/peersay/RemoteCommandReceiver;->coldStartHandler:Lru/cn/peersay/controllers/RemoteIntentController;

    invoke-interface {v7, p1, v3}, Lru/cn/peersay/controllers/RemoteIntentController;->handleMessage(Landroid/content/Context;Lru/cn/peersay/IntentMessage;)Z

    move-result v2

    .line 60
    :cond_4
    if-nez v2, :cond_5

    .line 61
    iget-object v7, v3, Lru/cn/peersay/IntentMessage;->extras:Landroid/os/Bundle;

    const-string v8, "command"

    invoke-virtual {v7, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 62
    .local v0, "command":Ljava/lang/String;
    const-string v7, "tv.playback.state"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 63
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 64
    .local v5, "resultBundle":Landroid/os/Bundle;
    const-string v7, "state"

    const-string v8, "stopped"

    invoke-virtual {v5, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    invoke-virtual {v3, v5}, Lru/cn/peersay/IntentMessage;->setResult(Landroid/os/Bundle;)V

    .line 69
    .end local v0    # "command":Ljava/lang/String;
    .end local v5    # "resultBundle":Landroid/os/Bundle;
    :cond_5
    invoke-virtual {v3, v2}, Lru/cn/peersay/IntentMessage;->createResultIntent(Z)Landroid/content/Intent;

    move-result-object v4

    .line 70
    .local v4, "result":Landroid/content/Intent;
    invoke-virtual {p1, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 71
    const-string v7, "RemoteCommandReceiver"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Result sent "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v4}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method
