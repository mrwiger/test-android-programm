.class public Lru/cn/ad/AdapterLoader;
.super Ljava/lang/Object;
.source "AdapterLoader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/ad/AdapterLoader$LoadObserver;,
        Lru/cn/ad/AdapterLoader$LoadStepListener;,
        Lru/cn/ad/AdapterLoader$Listener;,
        Lru/cn/ad/AdapterLoader$Cache;
    }
.end annotation


# instance fields
.field private currentAdapter:Lru/cn/ad/AdAdapter;

.field private final factory:Lru/cn/ad/AdAdapter$Factory;

.field private isLoading:Z

.field private listener:Lru/cn/ad/AdapterLoader$Listener;

.field private loadObserver:Lru/cn/ad/AdAdapter$Listener;

.field private loadStepListener:Lru/cn/ad/AdapterLoader$LoadStepListener;

.field private loaderCache:Lru/cn/ad/AdapterLoader$Cache;

.field private networkIndex:I

.field private final networks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lru/cn/api/money_miner/replies/AdSystem;",
            ">;"
        }
    .end annotation
.end field

.field public final placeId:Ljava/lang/String;

.field private timeoutHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/util/List;Lru/cn/ad/AdAdapter$Factory;)V
    .locals 2
    .param p1, "placeId"    # Ljava/lang/String;
    .param p3, "factory"    # Lru/cn/ad/AdAdapter$Factory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lru/cn/api/money_miner/replies/AdSystem;",
            ">;",
            "Lru/cn/ad/AdAdapter$Factory;",
            ")V"
        }
    .end annotation

    .prologue
    .line 59
    .local p2, "networks":Ljava/util/List;, "Ljava/util/List<Lru/cn/api/money_miner/replies/AdSystem;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object p1, p0, Lru/cn/ad/AdapterLoader;->placeId:Ljava/lang/String;

    .line 61
    iput-object p2, p0, Lru/cn/ad/AdapterLoader;->networks:Ljava/util/List;

    .line 62
    iput-object p3, p0, Lru/cn/ad/AdapterLoader;->factory:Lru/cn/ad/AdAdapter$Factory;

    .line 64
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lru/cn/ad/AdapterLoader;->timeoutHandler:Landroid/os/Handler;

    .line 65
    new-instance v0, Lru/cn/ad/AdapterLoader$LoadObserver;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lru/cn/ad/AdapterLoader$LoadObserver;-><init>(Lru/cn/ad/AdapterLoader;Lru/cn/ad/AdapterLoader$1;)V

    iput-object v0, p0, Lru/cn/ad/AdapterLoader;->loadObserver:Lru/cn/ad/AdAdapter$Listener;

    .line 66
    return-void
.end method

.method static synthetic access$100(Lru/cn/ad/AdapterLoader;)Lru/cn/ad/AdAdapter;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/AdapterLoader;

    .prologue
    .line 14
    iget-object v0, p0, Lru/cn/ad/AdapterLoader;->currentAdapter:Lru/cn/ad/AdAdapter;

    return-object v0
.end method

.method static synthetic access$102(Lru/cn/ad/AdapterLoader;Lru/cn/ad/AdAdapter;)Lru/cn/ad/AdAdapter;
    .locals 0
    .param p0, "x0"    # Lru/cn/ad/AdapterLoader;
    .param p1, "x1"    # Lru/cn/ad/AdAdapter;

    .prologue
    .line 14
    iput-object p1, p0, Lru/cn/ad/AdapterLoader;->currentAdapter:Lru/cn/ad/AdAdapter;

    return-object p1
.end method

.method static synthetic access$200(Lru/cn/ad/AdapterLoader;Lru/cn/ad/AdAdapter;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/ad/AdapterLoader;
    .param p1, "x1"    # Lru/cn/ad/AdAdapter;

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lru/cn/ad/AdapterLoader;->completeLoading(Lru/cn/ad/AdAdapter;)V

    return-void
.end method

.method static synthetic access$300(Lru/cn/ad/AdapterLoader;)I
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/AdapterLoader;

    .prologue
    .line 14
    iget v0, p0, Lru/cn/ad/AdapterLoader;->networkIndex:I

    return v0
.end method

.method static synthetic access$302(Lru/cn/ad/AdapterLoader;I)I
    .locals 0
    .param p0, "x0"    # Lru/cn/ad/AdapterLoader;
    .param p1, "x1"    # I

    .prologue
    .line 14
    iput p1, p0, Lru/cn/ad/AdapterLoader;->networkIndex:I

    return p1
.end method

.method static synthetic access$400(Lru/cn/ad/AdapterLoader;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/ad/AdapterLoader;

    .prologue
    .line 14
    invoke-direct {p0}, Lru/cn/ad/AdapterLoader;->traverseAdapters()V

    return-void
.end method

.method private completeLoading(Lru/cn/ad/AdAdapter;)V
    .locals 2
    .param p1, "adapter"    # Lru/cn/ad/AdAdapter;

    .prologue
    .line 185
    iget-object v0, p0, Lru/cn/ad/AdapterLoader;->timeoutHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 186
    const/4 v0, 0x0

    iput-boolean v0, p0, Lru/cn/ad/AdapterLoader;->isLoading:Z

    .line 188
    iget-object v0, p0, Lru/cn/ad/AdapterLoader;->listener:Lru/cn/ad/AdapterLoader$Listener;

    if-eqz v0, :cond_0

    .line 189
    iget-object v0, p0, Lru/cn/ad/AdapterLoader;->listener:Lru/cn/ad/AdapterLoader$Listener;

    invoke-interface {v0, p1}, Lru/cn/ad/AdapterLoader$Listener;->onLoaded(Lru/cn/ad/AdAdapter;)V

    .line 192
    :cond_0
    iget-object v0, p0, Lru/cn/ad/AdapterLoader;->loadStepListener:Lru/cn/ad/AdapterLoader$LoadStepListener;

    if-eqz v0, :cond_1

    .line 193
    iget-object v0, p0, Lru/cn/ad/AdapterLoader;->loadStepListener:Lru/cn/ad/AdapterLoader$LoadStepListener;

    invoke-interface {v0}, Lru/cn/ad/AdapterLoader$LoadStepListener;->onLoadEnded()V

    .line 195
    :cond_1
    return-void
.end method

.method private createAdapter(Lru/cn/api/money_miner/replies/AdSystem;)Lru/cn/ad/AdAdapter;
    .locals 5
    .param p1, "system"    # Lru/cn/api/money_miner/replies/AdSystem;

    .prologue
    .line 151
    :try_start_0
    iget-object v2, p0, Lru/cn/ad/AdapterLoader;->factory:Lru/cn/ad/AdAdapter$Factory;

    invoke-interface {v2, p1}, Lru/cn/ad/AdAdapter$Factory;->create(Lru/cn/api/money_miner/replies/AdSystem;)Lru/cn/ad/AdAdapter;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 158
    :goto_0
    return-object v2

    .line 152
    :catch_0
    move-exception v0

    .line 153
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "network_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Lru/cn/api/money_miner/replies/AdSystem;->id:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 154
    .local v1, "message":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ";place_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lru/cn/ad/AdapterLoader;->placeId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 155
    sget-object v2, Lru/cn/domain/statistics/inetra/ErrorCode;->UNKNOWN_ERROR:Lru/cn/domain/statistics/inetra/ErrorCode;

    const-string v3, "MoneyMinerErrorDomain"

    const/16 v4, 0x66

    invoke-static {v2, v3, v4, v1}, Lru/cn/domain/statistics/inetra/InetraTracker;->error(Lru/cn/domain/statistics/inetra/ErrorCode;Ljava/lang/String;ILjava/lang/String;)V

    .line 158
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private interruptLoading(Lru/cn/ad/AdAdapter;)V
    .locals 2
    .param p1, "adAdapter"    # Lru/cn/ad/AdAdapter;

    .prologue
    .line 176
    iget-object v0, p0, Lru/cn/ad/AdapterLoader;->currentAdapter:Lru/cn/ad/AdAdapter;

    if-eq v0, p1, :cond_0

    .line 177
    const-string v0, "AdLoader"

    const-string v1, "Timeout handler not stopped"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 182
    :goto_0
    return-void

    .line 181
    :cond_0
    iget-object v0, p0, Lru/cn/ad/AdapterLoader;->loadObserver:Lru/cn/ad/AdAdapter$Listener;

    invoke-interface {v0}, Lru/cn/ad/AdAdapter$Listener;->onError()V

    goto :goto_0
.end method

.method private loadAdapter(Lru/cn/ad/AdAdapter;)V
    .locals 4
    .param p1, "adapter"    # Lru/cn/ad/AdAdapter;

    .prologue
    .line 162
    iput-object p1, p0, Lru/cn/ad/AdapterLoader;->currentAdapter:Lru/cn/ad/AdAdapter;

    .line 163
    iget-object v0, p0, Lru/cn/ad/AdapterLoader;->currentAdapter:Lru/cn/ad/AdAdapter;

    new-instance v1, Lru/cn/ad/AdEventReporter;

    iget-object v2, p0, Lru/cn/ad/AdapterLoader;->placeId:Ljava/lang/String;

    invoke-direct {v1, v2}, Lru/cn/ad/AdEventReporter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lru/cn/ad/AdAdapter;->setReporter(Lru/cn/ad/AdEventReporter;)V

    .line 164
    iget-object v0, p0, Lru/cn/ad/AdapterLoader;->currentAdapter:Lru/cn/ad/AdAdapter;

    iget-object v1, p0, Lru/cn/ad/AdapterLoader;->loadObserver:Lru/cn/ad/AdAdapter$Listener;

    invoke-virtual {v0, v1}, Lru/cn/ad/AdAdapter;->setListener(Lru/cn/ad/AdAdapter$Listener;)V

    .line 165
    iget-object v0, p0, Lru/cn/ad/AdapterLoader;->currentAdapter:Lru/cn/ad/AdAdapter;

    invoke-virtual {v0}, Lru/cn/ad/AdAdapter;->load()V

    .line 167
    iget-object v0, p0, Lru/cn/ad/AdapterLoader;->timeoutHandler:Landroid/os/Handler;

    new-instance v1, Lru/cn/ad/AdapterLoader$$Lambda$0;

    invoke-direct {v1, p0, p1}, Lru/cn/ad/AdapterLoader$$Lambda$0;-><init>(Lru/cn/ad/AdapterLoader;Lru/cn/ad/AdAdapter;)V

    const-wide/16 v2, 0x3a98

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 169
    iget-object v0, p0, Lru/cn/ad/AdapterLoader;->loadStepListener:Lru/cn/ad/AdapterLoader$LoadStepListener;

    if-eqz v0, :cond_0

    .line 170
    iget-object v0, p0, Lru/cn/ad/AdapterLoader;->loadStepListener:Lru/cn/ad/AdapterLoader$LoadStepListener;

    iget-object v1, p1, Lru/cn/ad/AdAdapter;->adSystem:Lru/cn/api/money_miner/replies/AdSystem;

    invoke-interface {v0, v1}, Lru/cn/ad/AdapterLoader$LoadStepListener;->onLoadStarted(Lru/cn/api/money_miner/replies/AdSystem;)V

    .line 172
    :cond_0
    return-void
.end method

.method private traverseAdapters()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 109
    iget-object v3, p0, Lru/cn/ad/AdapterLoader;->timeoutHandler:Landroid/os/Handler;

    invoke-virtual {v3, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 111
    const/4 v0, 0x0

    .line 112
    .local v0, "adapter":Lru/cn/ad/AdAdapter;
    const/4 v2, 0x0

    .line 113
    .local v2, "system":Lru/cn/api/money_miner/replies/AdSystem;
    :goto_0
    iget v3, p0, Lru/cn/ad/AdapterLoader;->networkIndex:I

    iget-object v4, p0, Lru/cn/ad/AdapterLoader;->networks:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v3, v4, :cond_0

    .line 114
    iget-object v3, p0, Lru/cn/ad/AdapterLoader;->networks:Ljava/util/List;

    iget v4, p0, Lru/cn/ad/AdapterLoader;->networkIndex:I

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "system":Lru/cn/api/money_miner/replies/AdSystem;
    check-cast v2, Lru/cn/api/money_miner/replies/AdSystem;

    .line 115
    .restart local v2    # "system":Lru/cn/api/money_miner/replies/AdSystem;
    invoke-direct {p0, v2}, Lru/cn/ad/AdapterLoader;->createAdapter(Lru/cn/api/money_miner/replies/AdSystem;)Lru/cn/ad/AdAdapter;

    move-result-object v0

    .line 116
    if-eqz v0, :cond_3

    .line 122
    :cond_0
    if-nez v0, :cond_4

    .line 123
    const/4 v3, 0x0

    iput-boolean v3, p0, Lru/cn/ad/AdapterLoader;->isLoading:Z

    .line 125
    iget-object v3, p0, Lru/cn/ad/AdapterLoader;->listener:Lru/cn/ad/AdapterLoader$Listener;

    if-eqz v3, :cond_1

    .line 126
    iget-object v3, p0, Lru/cn/ad/AdapterLoader;->listener:Lru/cn/ad/AdapterLoader$Listener;

    invoke-interface {v3}, Lru/cn/ad/AdapterLoader$Listener;->onError()V

    .line 129
    :cond_1
    iget-object v3, p0, Lru/cn/ad/AdapterLoader;->loadStepListener:Lru/cn/ad/AdapterLoader$LoadStepListener;

    if-eqz v3, :cond_2

    .line 130
    iget-object v3, p0, Lru/cn/ad/AdapterLoader;->loadStepListener:Lru/cn/ad/AdapterLoader$LoadStepListener;

    invoke-interface {v3}, Lru/cn/ad/AdapterLoader$LoadStepListener;->onLoadEnded()V

    .line 147
    :cond_2
    :goto_1
    return-void

    .line 119
    :cond_3
    iget v3, p0, Lru/cn/ad/AdapterLoader;->networkIndex:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lru/cn/ad/AdapterLoader;->networkIndex:I

    goto :goto_0

    .line 136
    :cond_4
    iget-object v3, p0, Lru/cn/ad/AdapterLoader;->loaderCache:Lru/cn/ad/AdapterLoader$Cache;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lru/cn/ad/AdapterLoader;->loaderCache:Lru/cn/ad/AdapterLoader$Cache;

    invoke-interface {v3, v2}, Lru/cn/ad/AdapterLoader$Cache;->cachedAdapter(Lru/cn/api/money_miner/replies/AdSystem;)Lru/cn/ad/AdAdapter;

    move-result-object v1

    .line 137
    .local v1, "cachedAdapter":Lru/cn/ad/AdAdapter;
    :cond_5
    if-eqz v1, :cond_7

    .line 138
    iget-object v3, p0, Lru/cn/ad/AdapterLoader;->loadStepListener:Lru/cn/ad/AdapterLoader$LoadStepListener;

    if-eqz v3, :cond_6

    .line 139
    iget-object v3, p0, Lru/cn/ad/AdapterLoader;->loadStepListener:Lru/cn/ad/AdapterLoader$LoadStepListener;

    invoke-interface {v3, v2}, Lru/cn/ad/AdapterLoader$LoadStepListener;->onLoadStarted(Lru/cn/api/money_miner/replies/AdSystem;)V

    .line 142
    :cond_6
    invoke-direct {p0, v1}, Lru/cn/ad/AdapterLoader;->completeLoading(Lru/cn/ad/AdAdapter;)V

    goto :goto_1

    .line 146
    :cond_7
    invoke-direct {p0, v0}, Lru/cn/ad/AdapterLoader;->loadAdapter(Lru/cn/ad/AdAdapter;)V

    goto :goto_1
.end method


# virtual methods
.method public isLoading()Z
    .locals 1

    .prologue
    .line 81
    iget-boolean v0, p0, Lru/cn/ad/AdapterLoader;->isLoading:Z

    return v0
.end method

.method final synthetic lambda$loadAdapter$0$AdapterLoader(Lru/cn/ad/AdAdapter;)V
    .locals 0
    .param p1, "adapter"    # Lru/cn/ad/AdAdapter;

    .prologue
    .line 167
    invoke-direct {p0, p1}, Lru/cn/ad/AdapterLoader;->interruptLoading(Lru/cn/ad/AdAdapter;)V

    return-void
.end method

.method public load()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 85
    iget-boolean v0, p0, Lru/cn/ad/AdapterLoader;->isLoading:Z

    if-eqz v0, :cond_0

    .line 98
    :goto_0
    return-void

    .line 88
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lru/cn/ad/AdapterLoader;->isLoading:Z

    .line 89
    const/4 v0, 0x0

    iput v0, p0, Lru/cn/ad/AdapterLoader;->networkIndex:I

    .line 92
    iget-object v0, p0, Lru/cn/ad/AdapterLoader;->currentAdapter:Lru/cn/ad/AdAdapter;

    if-eqz v0, :cond_1

    .line 93
    iget-object v0, p0, Lru/cn/ad/AdapterLoader;->currentAdapter:Lru/cn/ad/AdAdapter;

    invoke-virtual {v0, v1}, Lru/cn/ad/AdAdapter;->setListener(Lru/cn/ad/AdAdapter$Listener;)V

    .line 94
    iput-object v1, p0, Lru/cn/ad/AdapterLoader;->currentAdapter:Lru/cn/ad/AdAdapter;

    .line 97
    :cond_1
    invoke-direct {p0}, Lru/cn/ad/AdapterLoader;->traverseAdapters()V

    goto :goto_0
.end method

.method public setListener(Lru/cn/ad/AdapterLoader$Listener;)V
    .locals 0
    .param p1, "listener"    # Lru/cn/ad/AdapterLoader$Listener;

    .prologue
    .line 69
    iput-object p1, p0, Lru/cn/ad/AdapterLoader;->listener:Lru/cn/ad/AdapterLoader$Listener;

    .line 70
    return-void
.end method

.method public setLoadStepListener(Lru/cn/ad/AdapterLoader$LoadStepListener;)V
    .locals 0
    .param p1, "listener"    # Lru/cn/ad/AdapterLoader$LoadStepListener;

    .prologue
    .line 73
    iput-object p1, p0, Lru/cn/ad/AdapterLoader;->loadStepListener:Lru/cn/ad/AdapterLoader$LoadStepListener;

    .line 74
    return-void
.end method

.method public setLoaderCache(Lru/cn/ad/AdapterLoader$Cache;)V
    .locals 0
    .param p1, "cache"    # Lru/cn/ad/AdapterLoader$Cache;

    .prologue
    .line 77
    iput-object p1, p0, Lru/cn/ad/AdapterLoader;->loaderCache:Lru/cn/ad/AdapterLoader$Cache;

    .line 78
    return-void
.end method

.method public stop()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 101
    iget-object v0, p0, Lru/cn/ad/AdapterLoader;->currentAdapter:Lru/cn/ad/AdAdapter;

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Lru/cn/ad/AdapterLoader;->currentAdapter:Lru/cn/ad/AdAdapter;

    invoke-virtual {v0, v1}, Lru/cn/ad/AdAdapter;->setListener(Lru/cn/ad/AdAdapter$Listener;)V

    .line 103
    iput-object v1, p0, Lru/cn/ad/AdapterLoader;->currentAdapter:Lru/cn/ad/AdAdapter;

    .line 104
    const/4 v0, 0x0

    iput-boolean v0, p0, Lru/cn/ad/AdapterLoader;->isLoading:Z

    .line 106
    :cond_0
    return-void
.end method
