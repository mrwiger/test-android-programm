.class final Lcom/google/obf/ce$a;
.super Ljava/lang/Object;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/obf/ce;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation


# instance fields
.field private final a:Lcom/google/obf/ar;

.field private b:J

.field private c:Z

.field private d:I

.field private e:J

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:J

.field private l:J

.field private m:Z


# direct methods
.method public constructor <init>(Lcom/google/obf/ar;)V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/google/obf/ce$a;->a:Lcom/google/obf/ar;

    .line 3
    return-void
.end method

.method private a(I)V
    .locals 8

    .prologue
    .line 44
    iget-boolean v0, p0, Lcom/google/obf/ce$a;->m:Z

    if-eqz v0, :cond_0

    const/4 v4, 0x1

    .line 45
    :goto_0
    iget-wide v0, p0, Lcom/google/obf/ce$a;->b:J

    iget-wide v2, p0, Lcom/google/obf/ce$a;->k:J

    sub-long/2addr v0, v2

    long-to-int v5, v0

    .line 46
    iget-object v1, p0, Lcom/google/obf/ce$a;->a:Lcom/google/obf/ar;

    iget-wide v2, p0, Lcom/google/obf/ce$a;->l:J

    const/4 v7, 0x0

    move v6, p1

    invoke-interface/range {v1 .. v7}, Lcom/google/obf/ar;->a(JIII[B)V

    .line 47
    return-void

    .line 44
    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 4
    iput-boolean v0, p0, Lcom/google/obf/ce$a;->f:Z

    .line 5
    iput-boolean v0, p0, Lcom/google/obf/ce$a;->g:Z

    .line 6
    iput-boolean v0, p0, Lcom/google/obf/ce$a;->h:Z

    .line 7
    iput-boolean v0, p0, Lcom/google/obf/ce$a;->i:Z

    .line 8
    iput-boolean v0, p0, Lcom/google/obf/ce$a;->j:Z

    .line 9
    return-void
.end method

.method public a(JI)V
    .locals 3

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/google/obf/ce$a;->j:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/obf/ce$a;->g:Z

    if-eqz v0, :cond_1

    .line 33
    iget-boolean v0, p0, Lcom/google/obf/ce$a;->c:Z

    iput-boolean v0, p0, Lcom/google/obf/ce$a;->m:Z

    .line 34
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/obf/ce$a;->j:Z

    .line 43
    :cond_0
    :goto_0
    return-void

    .line 35
    :cond_1
    iget-boolean v0, p0, Lcom/google/obf/ce$a;->h:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/google/obf/ce$a;->g:Z

    if-eqz v0, :cond_0

    .line 36
    :cond_2
    iget-boolean v0, p0, Lcom/google/obf/ce$a;->i:Z

    if-eqz v0, :cond_3

    .line 37
    iget-wide v0, p0, Lcom/google/obf/ce$a;->b:J

    sub-long v0, p1, v0

    long-to-int v0, v0

    .line 38
    add-int/2addr v0, p3

    invoke-direct {p0, v0}, Lcom/google/obf/ce$a;->a(I)V

    .line 39
    :cond_3
    iget-wide v0, p0, Lcom/google/obf/ce$a;->b:J

    iput-wide v0, p0, Lcom/google/obf/ce$a;->k:J

    .line 40
    iget-wide v0, p0, Lcom/google/obf/ce$a;->e:J

    iput-wide v0, p0, Lcom/google/obf/ce$a;->l:J

    .line 41
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/obf/ce$a;->i:Z

    .line 42
    iget-boolean v0, p0, Lcom/google/obf/ce$a;->c:Z

    iput-boolean v0, p0, Lcom/google/obf/ce$a;->m:Z

    goto :goto_0
.end method

.method public a(JIIJ)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 10
    iput-boolean v2, p0, Lcom/google/obf/ce$a;->g:Z

    .line 11
    iput-boolean v2, p0, Lcom/google/obf/ce$a;->h:Z

    .line 12
    iput-wide p5, p0, Lcom/google/obf/ce$a;->e:J

    .line 13
    iput v2, p0, Lcom/google/obf/ce$a;->d:I

    .line 14
    iput-wide p1, p0, Lcom/google/obf/ce$a;->b:J

    .line 15
    const/16 v0, 0x20

    if-lt p4, v0, :cond_1

    .line 16
    iget-boolean v0, p0, Lcom/google/obf/ce$a;->j:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/obf/ce$a;->i:Z

    if-eqz v0, :cond_0

    .line 17
    invoke-direct {p0, p3}, Lcom/google/obf/ce$a;->a(I)V

    .line 18
    iput-boolean v2, p0, Lcom/google/obf/ce$a;->i:Z

    .line 19
    :cond_0
    const/16 v0, 0x22

    if-gt p4, v0, :cond_1

    .line 20
    iget-boolean v0, p0, Lcom/google/obf/ce$a;->j:Z

    if-nez v0, :cond_4

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/obf/ce$a;->h:Z

    .line 21
    iput-boolean v1, p0, Lcom/google/obf/ce$a;->j:Z

    .line 22
    :cond_1
    const/16 v0, 0x10

    if-lt p4, v0, :cond_5

    const/16 v0, 0x15

    if-gt p4, v0, :cond_5

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/obf/ce$a;->c:Z

    .line 23
    iget-boolean v0, p0, Lcom/google/obf/ce$a;->c:Z

    if-nez v0, :cond_2

    const/16 v0, 0x9

    if-gt p4, v0, :cond_3

    :cond_2
    move v2, v1

    :cond_3
    iput-boolean v2, p0, Lcom/google/obf/ce$a;->f:Z

    .line 24
    return-void

    :cond_4
    move v0, v2

    .line 20
    goto :goto_0

    :cond_5
    move v0, v2

    .line 22
    goto :goto_1
.end method

.method public a([BII)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 25
    iget-boolean v0, p0, Lcom/google/obf/ce$a;->f:Z

    if-eqz v0, :cond_0

    .line 26
    add-int/lit8 v0, p2, 0x2

    iget v2, p0, Lcom/google/obf/ce$a;->d:I

    sub-int/2addr v0, v2

    .line 27
    if-ge v0, p3, :cond_2

    .line 28
    aget-byte v0, p1, v0

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/obf/ce$a;->g:Z

    .line 29
    iput-boolean v1, p0, Lcom/google/obf/ce$a;->f:Z

    .line 31
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 28
    goto :goto_0

    .line 30
    :cond_2
    iget v0, p0, Lcom/google/obf/ce$a;->d:I

    sub-int v1, p3, p2

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/obf/ce$a;->d:I

    goto :goto_1
.end method
