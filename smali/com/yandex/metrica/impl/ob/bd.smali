.class public abstract Lcom/yandex/metrica/impl/ob/bd;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/yandex/metrica/impl/ob/bg;


# instance fields
.field private final a:Lcom/yandex/metrica/impl/ob/v;

.field private final b:Lcom/yandex/metrica/impl/ob/bo;

.field private final c:Lcom/yandex/metrica/impl/ob/bk;


# direct methods
.method public constructor <init>(Lcom/yandex/metrica/impl/ob/v;Lcom/yandex/metrica/impl/ob/bo;Lcom/yandex/metrica/impl/ob/bk;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/yandex/metrica/impl/ob/bd;->a:Lcom/yandex/metrica/impl/ob/v;

    .line 36
    iput-object p2, p0, Lcom/yandex/metrica/impl/ob/bd;->b:Lcom/yandex/metrica/impl/ob/bo;

    .line 37
    iput-object p3, p0, Lcom/yandex/metrica/impl/ob/bd;->c:Lcom/yandex/metrica/impl/ob/bk;

    .line 38
    return-void
.end method


# virtual methods
.method public final a()Lcom/yandex/metrica/impl/ob/bh;
    .locals 4

    .prologue
    .line 41
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bd;->b:Lcom/yandex/metrica/impl/ob/bo;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/bo;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 42
    new-instance v0, Lcom/yandex/metrica/impl/ob/bh;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/bd;->a:Lcom/yandex/metrica/impl/ob/v;

    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/bd;->b:Lcom/yandex/metrica/impl/ob/bo;

    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/bd;->c()Lcom/yandex/metrica/impl/ob/bi;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/yandex/metrica/impl/ob/bh;-><init>(Lcom/yandex/metrica/impl/ob/v;Lcom/yandex/metrica/impl/ob/bn;Lcom/yandex/metrica/impl/ob/bi;)V

    .line 44
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Lcom/yandex/metrica/impl/ob/bh;
    .locals 10

    .prologue
    const-wide/16 v8, 0x3e8

    .line 49
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bd;->b:Lcom/yandex/metrica/impl/ob/bo;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/bo;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bd;->a:Lcom/yandex/metrica/impl/ob/v;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/v;->n()Landroid/content/Context;

    move-result-object v0

    .line 1022
    const-string v1, "20799a27-fa80-4b36-b2db-0f8141f24180"

    invoke-static {v0, v1}, Lcom/yandex/metrica/YandexMetrica;->getReporter(Landroid/content/Context;Ljava/lang/String;)Lcom/yandex/metrica/IReporter;

    move-result-object v0

    .line 53
    const-string v1, "create session with non-empty storage"

    .line 54
    invoke-interface {v0, v1}, Lcom/yandex/metrica/IReporter;->reportEvent(Ljava/lang/String;)V

    .line 56
    :cond_0
    new-instance v0, Lcom/yandex/metrica/impl/ob/bh;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/bd;->a:Lcom/yandex/metrica/impl/ob/v;

    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/bd;->b:Lcom/yandex/metrica/impl/ob/bo;

    .line 2029
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    div-long/2addr v4, v8

    .line 1061
    iget-object v3, p0, Lcom/yandex/metrica/impl/ob/bd;->b:Lcom/yandex/metrica/impl/ob/bo;

    invoke-virtual {v3, v4, v5}, Lcom/yandex/metrica/impl/ob/bo;->c(J)Lcom/yandex/metrica/impl/ob/bo;

    move-result-object v3

    .line 2055
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    div-long/2addr v6, v8

    .line 1062
    invoke-virtual {v3, v6, v7}, Lcom/yandex/metrica/impl/ob/bo;->b(J)Lcom/yandex/metrica/impl/ob/bo;

    move-result-object v3

    .line 1063
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Lcom/yandex/metrica/impl/ob/bo;->d(J)Lcom/yandex/metrica/impl/ob/bo;

    move-result-object v3

    const-wide/16 v6, 0x0

    .line 1064
    invoke-virtual {v3, v6, v7}, Lcom/yandex/metrica/impl/ob/bo;->a(J)Lcom/yandex/metrica/impl/ob/bo;

    move-result-object v3

    const/4 v6, 0x1

    .line 1065
    invoke-virtual {v3, v6}, Lcom/yandex/metrica/impl/ob/bo;->a(Z)Lcom/yandex/metrica/impl/ob/bo;

    move-result-object v3

    .line 1066
    invoke-virtual {v3}, Lcom/yandex/metrica/impl/ob/bo;->g()V

    .line 1068
    iget-object v3, p0, Lcom/yandex/metrica/impl/ob/bd;->a:Lcom/yandex/metrica/impl/ob/v;

    invoke-virtual {v3}, Lcom/yandex/metrica/impl/ob/v;->j()Lcom/yandex/metrica/impl/ob/co;

    move-result-object v3

    iget-object v6, p0, Lcom/yandex/metrica/impl/ob/bd;->c:Lcom/yandex/metrica/impl/ob/bk;

    invoke-virtual {v6}, Lcom/yandex/metrica/impl/ob/bk;->a()Lcom/yandex/metrica/impl/ob/bp;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6}, Lcom/yandex/metrica/impl/ob/co;->a(JLcom/yandex/metrica/impl/ob/bp;)V

    .line 1071
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/bd;->c()Lcom/yandex/metrica/impl/ob/bi;

    move-result-object v3

    .line 56
    invoke-direct {v0, v1, v2, v3}, Lcom/yandex/metrica/impl/ob/bh;-><init>(Lcom/yandex/metrica/impl/ob/v;Lcom/yandex/metrica/impl/ob/bn;Lcom/yandex/metrica/impl/ob/bi;)V

    return-object v0
.end method

.method c()Lcom/yandex/metrica/impl/ob/bi;
    .locals 3

    .prologue
    .line 77
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bd;->c:Lcom/yandex/metrica/impl/ob/bk;

    .line 3039
    new-instance v1, Lcom/yandex/metrica/impl/ob/bi$a;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lcom/yandex/metrica/impl/ob/bi$a;-><init>(Lcom/yandex/metrica/impl/ob/bk;B)V

    .line 77
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bd;->b:Lcom/yandex/metrica/impl/ob/bo;

    .line 78
    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/bo;->f()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/yandex/metrica/impl/ob/bi$a;->a(Ljava/lang/Boolean;)Lcom/yandex/metrica/impl/ob/bi$a;

    move-result-object v0

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/bd;->b:Lcom/yandex/metrica/impl/ob/bo;

    .line 79
    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/bo;->d()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/bi$a;->c(Ljava/lang/Long;)Lcom/yandex/metrica/impl/ob/bi$a;

    move-result-object v0

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/bd;->b:Lcom/yandex/metrica/impl/ob/bo;

    .line 80
    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/bo;->c()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/bi$a;->b(Ljava/lang/Long;)Lcom/yandex/metrica/impl/ob/bi$a;

    move-result-object v0

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/bd;->b:Lcom/yandex/metrica/impl/ob/bo;

    .line 81
    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/bo;->b()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/bi$a;->a(Ljava/lang/Long;)Lcom/yandex/metrica/impl/ob/bi$a;

    move-result-object v0

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/bd;->b:Lcom/yandex/metrica/impl/ob/bo;

    .line 82
    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/bo;->e()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/bi$a;->d(Ljava/lang/Long;)Lcom/yandex/metrica/impl/ob/bi$a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/bi$a;->a()Lcom/yandex/metrica/impl/ob/bi;

    move-result-object v0

    .line 77
    return-object v0
.end method
