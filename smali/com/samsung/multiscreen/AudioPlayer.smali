.class public Lcom/samsung/multiscreen/AudioPlayer;
.super Lcom/samsung/multiscreen/Player;
.source "AudioPlayer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;,
        Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerMessageListener;,
        Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerInternalEvents;,
        Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerAttributes;
    }
.end annotation


# static fields
.field private static final AUDIO_PLAYER_CONTROL_RESPONSE:Ljava/lang/String; = "state"

.field private static final AUDIO_PLAYER_INTERNAL_RESPONSE:Ljava/lang/String; = "Audio State"

.field private static final AUDIO_PLAYER_QUEUE_EVENT_RESPONSE:Ljava/lang/String; = "queue"

.field private static final TAG:Ljava/lang/String; = "AudioPlayer"


# instance fields
.field private mAudioPlayerListener:Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;

.field private mList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/samsung/multiscreen/Service;Landroid/net/Uri;Ljava/lang/String;)V
    .locals 1
    .param p1, "service"    # Lcom/samsung/multiscreen/Service;
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "appName"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 64
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/multiscreen/Player;-><init>(Lcom/samsung/multiscreen/Service;Landroid/net/Uri;Ljava/lang/String;)V

    .line 34
    iput-object v0, p0, Lcom/samsung/multiscreen/AudioPlayer;->mAudioPlayerListener:Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;

    .line 65
    iput-object v0, p0, Lcom/samsung/multiscreen/AudioPlayer;->mList:Ljava/util/List;

    .line 66
    return-void
.end method

.method static synthetic access$100(Lcom/samsung/multiscreen/AudioPlayer;)Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/multiscreen/AudioPlayer;

    .prologue
    .line 24
    iget-object v0, p0, Lcom/samsung/multiscreen/AudioPlayer;->mAudioPlayerListener:Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;

    return-object v0
.end method


# virtual methods
.method public addOnMessageListener(Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;

    .prologue
    .line 520
    iput-object p1, p0, Lcom/samsung/multiscreen/AudioPlayer;->mAudioPlayerListener:Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;

    .line 521
    sget-object v0, Lcom/samsung/multiscreen/AudioPlayer;->mApplication:Lcom/samsung/multiscreen/Application;

    const-string v1, "playerNotice"

    new-instance v2, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerMessageListener;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerMessageListener;-><init>(Lcom/samsung/multiscreen/AudioPlayer;Lcom/samsung/multiscreen/AudioPlayer$1;)V

    invoke-virtual {v0, v1, v2}, Lcom/samsung/multiscreen/Application;->addOnMessageListener(Ljava/lang/String;Lcom/samsung/multiscreen/Channel$OnMessageListener;)V

    .line 522
    return-void
.end method

.method public addToList(Landroid/net/Uri;)V
    .locals 1
    .param p1, "ContentUrl"    # Landroid/net/Uri;

    .prologue
    const/4 v0, 0x0

    .line 258
    invoke-virtual {p0, p1, v0, v0, v0}, Lcom/samsung/multiscreen/AudioPlayer;->addToList(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    .line 259
    return-void
.end method

.method public addToList(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V
    .locals 4
    .param p1, "ContentUrl"    # Landroid/net/Uri;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "albumName"    # Ljava/lang/String;
    .param p4, "albumArt"    # Landroid/net/Uri;

    .prologue
    .line 272
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 273
    .local v0, "item":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v2, "uri"

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 274
    sget-object v2, Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerAttributes;->title:Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerAttributes;

    invoke-virtual {v2}, Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerAttributes;->name()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 275
    sget-object v2, Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerAttributes;->albumName:Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerAttributes;

    invoke-virtual {v2}, Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerAttributes;->name()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 276
    sget-object v2, Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerAttributes;->albumArt:Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerAttributes;

    invoke-virtual {v2}, Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerAttributes;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 278
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 279
    .local v1, "list":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;>;"
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 281
    invoke-virtual {p0, v1}, Lcom/samsung/multiscreen/AudioPlayer;->addToList(Ljava/util/List;)V

    .line 282
    return-void
.end method

.method public addToList(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 315
    .local p1, "audioList":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;>;"
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 316
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/multiscreen/AudioPlayer;->isDebug()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "AudioPlayer"

    const-string v2, "enQueue(): audioList is NULL."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 443
    :cond_1
    :goto_0
    return-void

    .line 323
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/multiscreen/AudioPlayer;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 324
    new-instance v1, Lcom/samsung/multiscreen/AudioPlayer$1;

    invoke-direct {v1, p0, p1}, Lcom/samsung/multiscreen/AudioPlayer$1;-><init>(Lcom/samsung/multiscreen/AudioPlayer;Ljava/util/List;)V

    invoke-virtual {p0, v1}, Lcom/samsung/multiscreen/AudioPlayer;->getDMPStatus(Lcom/samsung/multiscreen/Result;)V

    goto :goto_0

    .line 439
    :cond_3
    iget-object v1, p0, Lcom/samsung/multiscreen/AudioPlayer;->mAudioPlayerListener:Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;

    if-eqz v1, :cond_1

    .line 440
    new-instance v0, Lcom/samsung/multiscreen/ErrorCode;

    const-string v1, "PLAYER_ERROR_PLAYER_NOT_LOADED"

    invoke-direct {v0, v1}, Lcom/samsung/multiscreen/ErrorCode;-><init>(Ljava/lang/String;)V

    .line 441
    .local v0, "errorCode":Lcom/samsung/multiscreen/ErrorCode;
    iget-object v1, p0, Lcom/samsung/multiscreen/AudioPlayer;->mAudioPlayerListener:Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;

    invoke-virtual {v0}, Lcom/samsung/multiscreen/ErrorCode;->value()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0}, Lcom/samsung/multiscreen/ErrorCode;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/samsung/multiscreen/ErrorCode;->name()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v3, v4, v5}, Lcom/samsung/multiscreen/Error;->create(JLjava/lang/String;Ljava/lang/String;)Lcom/samsung/multiscreen/Error;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/samsung/multiscreen/AudioPlayer$OnAudioPlayerListener;->onError(Lcom/samsung/multiscreen/Error;)V

    goto :goto_0
.end method

.method public clearList()V
    .locals 5

    .prologue
    .line 225
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 227
    .local v0, "data":Lorg/json/JSONObject;
    :try_start_0
    const-string v2, "subEvent"

    sget-object v3, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;->clear:Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;

    invoke-virtual {v3}, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 228
    const-string v2, "playerType"

    sget-object v3, Lcom/samsung/multiscreen/Player$ContentType;->audio:Lcom/samsung/multiscreen/Player$ContentType;

    invoke-virtual {v3}, Lcom/samsung/multiscreen/Player$ContentType;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 232
    :goto_0
    sget-object v2, Lcom/samsung/multiscreen/AudioPlayer;->mApplication:Lcom/samsung/multiscreen/Application;

    const-string v3, "playerQueueEvent"

    invoke-virtual {v2, v3, v0}, Lcom/samsung/multiscreen/Application;->publish(Ljava/lang/String;Ljava/lang/Object;)V

    .line 233
    return-void

    .line 229
    :catch_0
    move-exception v1

    .line 230
    .local v1, "exception":Ljava/lang/Exception;
    const-string v2, "AudioPlayer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "clearQueue(): Error in parsing JSON object: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public getList()V
    .locals 5

    .prologue
    .line 211
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 213
    .local v0, "data":Lorg/json/JSONObject;
    :try_start_0
    const-string v2, "subEvent"

    sget-object v3, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;->fetch:Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;

    invoke-virtual {v3}, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 214
    const-string v2, "playerType"

    sget-object v3, Lcom/samsung/multiscreen/Player$ContentType;->audio:Lcom/samsung/multiscreen/Player$ContentType;

    invoke-virtual {v3}, Lcom/samsung/multiscreen/Player$ContentType;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 218
    :goto_0
    sget-object v2, Lcom/samsung/multiscreen/AudioPlayer;->mApplication:Lcom/samsung/multiscreen/Application;

    const-string v3, "playerQueueEvent"

    invoke-virtual {v2, v3, v0}, Lcom/samsung/multiscreen/Application;->publish(Ljava/lang/String;Ljava/lang/Object;)V

    .line 219
    return-void

    .line 215
    :catch_0
    move-exception v1

    .line 216
    .local v1, "exception":Ljava/lang/Exception;
    const-string v2, "AudioPlayer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "fetchQueue(): Error in parsing JSON object: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public playContent(Landroid/net/Uri;Lcom/samsung/multiscreen/Result;)V
    .locals 6
    .param p1, "contentUrl"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Lcom/samsung/multiscreen/Result",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "result":Lcom/samsung/multiscreen/Result;, "Lcom/samsung/multiscreen/Result<Ljava/lang/Boolean;>;"
    const/4 v2, 0x0

    .line 75
    move-object v0, p0

    move-object v1, p1

    move-object v3, v2

    move-object v4, v2

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/multiscreen/AudioPlayer;->playContent(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Lcom/samsung/multiscreen/Result;)V

    .line 76
    return-void
.end method

.method public playContent(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Lcom/samsung/multiscreen/Result;)V
    .locals 7
    .param p1, "contentUrl"    # Landroid/net/Uri;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "albumName"    # Ljava/lang/String;
    .param p4, "albumArtUrl"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Landroid/net/Uri;",
            "Lcom/samsung/multiscreen/Result",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 123
    .local p5, "result":Lcom/samsung/multiscreen/Result;, "Lcom/samsung/multiscreen/Result<Ljava/lang/Boolean;>;"
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 125
    .local v0, "contentInfo":Lorg/json/JSONObject;
    if-eqz p1, :cond_4

    :try_start_0
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_4

    .line 126
    const-string v3, "uri"

    invoke-virtual {v0, v3, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 133
    if-eqz p2, :cond_0

    .line 134
    sget-object v3, Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerAttributes;->title:Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerAttributes;

    invoke-virtual {v3}, Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerAttributes;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 136
    :cond_0
    if-eqz p3, :cond_1

    .line 137
    sget-object v3, Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerAttributes;->albumName:Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerAttributes;

    invoke-virtual {v3}, Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerAttributes;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 139
    :cond_1
    if-eqz p4, :cond_2

    .line 140
    sget-object v3, Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerAttributes;->albumArt:Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerAttributes;

    invoke-virtual {v3}, Lcom/samsung/multiscreen/AudioPlayer$AudioPlayerAttributes;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3, p4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 150
    :cond_2
    :goto_0
    sget-object v3, Lcom/samsung/multiscreen/Player$ContentType;->audio:Lcom/samsung/multiscreen/Player$ContentType;

    invoke-super {p0, v0, v3, p5}, Lcom/samsung/multiscreen/Player;->playContent(Lorg/json/JSONObject;Lcom/samsung/multiscreen/Player$ContentType;Lcom/samsung/multiscreen/Result;)V

    .line 151
    :cond_3
    :goto_1
    return-void

    .line 128
    :cond_4
    :try_start_1
    new-instance v2, Lcom/samsung/multiscreen/ErrorCode;

    const-string v3, "PLAYER_ERROR_INVALID_URI"

    invoke-direct {v2, v3}, Lcom/samsung/multiscreen/ErrorCode;-><init>(Ljava/lang/String;)V

    .line 129
    .local v2, "error":Lcom/samsung/multiscreen/ErrorCode;
    invoke-virtual {p0}, Lcom/samsung/multiscreen/AudioPlayer;->isDebug()Z

    move-result v3

    if-eqz v3, :cond_5

    const-string v3, "AudioPlayer"

    const-string v4, "There\'s no media url to launch!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    :cond_5
    if-eqz p5, :cond_3

    invoke-virtual {v2}, Lcom/samsung/multiscreen/ErrorCode;->value()I

    move-result v3

    int-to-long v4, v3

    invoke-virtual {v2}, Lcom/samsung/multiscreen/ErrorCode;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Lcom/samsung/multiscreen/ErrorCode;->name()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v3, v6}, Lcom/samsung/multiscreen/Error;->create(JLjava/lang/String;Ljava/lang/String;)Lcom/samsung/multiscreen/Error;

    move-result-object v3

    invoke-interface {p5, v3}, Lcom/samsung/multiscreen/Result;->onError(Lcom/samsung/multiscreen/Error;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 145
    .end local v2    # "error":Lcom/samsung/multiscreen/ErrorCode;
    :catch_0
    move-exception v1

    .line 146
    .local v1, "e":Ljava/lang/Exception;
    new-instance v2, Lcom/samsung/multiscreen/ErrorCode;

    const-string v3, "PLAYER_ERROR_UNKNOWN"

    invoke-direct {v2, v3}, Lcom/samsung/multiscreen/ErrorCode;-><init>(Ljava/lang/String;)V

    .line 147
    .restart local v2    # "error":Lcom/samsung/multiscreen/ErrorCode;
    invoke-virtual {p0}, Lcom/samsung/multiscreen/AudioPlayer;->isDebug()Z

    move-result v3

    if-eqz v3, :cond_6

    const-string v3, "AudioPlayer"

    const-string v4, "Unable to create JSONObject!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    :cond_6
    if-eqz p5, :cond_2

    invoke-virtual {v2}, Lcom/samsung/multiscreen/ErrorCode;->value()I

    move-result v3

    int-to-long v4, v3

    invoke-virtual {v2}, Lcom/samsung/multiscreen/ErrorCode;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Lcom/samsung/multiscreen/ErrorCode;->name()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v3, v6}, Lcom/samsung/multiscreen/Error;->create(JLjava/lang/String;Ljava/lang/String;)Lcom/samsung/multiscreen/Error;

    move-result-object v3

    invoke-interface {p5, v3}, Lcom/samsung/multiscreen/Result;->onError(Lcom/samsung/multiscreen/Error;)V

    goto :goto_0
.end method

.method public removeFromList(Landroid/net/Uri;)V
    .locals 5
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 241
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 243
    .local v0, "data":Lorg/json/JSONObject;
    :try_start_0
    const-string v2, "subEvent"

    sget-object v3, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;->dequeue:Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;

    invoke-virtual {v3}, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 244
    const-string v2, "playerType"

    sget-object v3, Lcom/samsung/multiscreen/Player$ContentType;->audio:Lcom/samsung/multiscreen/Player$ContentType;

    invoke-virtual {v3}, Lcom/samsung/multiscreen/Player$ContentType;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 245
    const-string v2, "uri"

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 249
    :goto_0
    sget-object v2, Lcom/samsung/multiscreen/AudioPlayer;->mApplication:Lcom/samsung/multiscreen/Application;

    const-string v3, "playerQueueEvent"

    invoke-virtual {v2, v3, v0}, Lcom/samsung/multiscreen/Application;->publish(Ljava/lang/String;Ljava/lang/Object;)V

    .line 250
    return-void

    .line 246
    :catch_0
    move-exception v1

    .line 247
    .local v1, "exception":Ljava/lang/Exception;
    const-string v2, "AudioPlayer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "deQueue(): Error in parsing JSON object: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public repeat()V
    .locals 3

    .prologue
    .line 172
    invoke-virtual {p0}, Lcom/samsung/multiscreen/AudioPlayer;->isDebug()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "AudioPlayer"

    const-string v1, "Send repeat"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 173
    :cond_0
    sget-object v0, Lcom/samsung/multiscreen/AudioPlayer;->mApplication:Lcom/samsung/multiscreen/Application;

    const-string v1, "playerControl"

    sget-object v2, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->repeat:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    invoke-virtual {v2}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/multiscreen/Application;->publish(Ljava/lang/String;Ljava/lang/Object;)V

    .line 174
    return-void
.end method

.method public seekTo(ILjava/util/concurrent/TimeUnit;)V
    .locals 6
    .param p1, "time"    # I
    .param p2, "timeUnit"    # Ljava/util/concurrent/TimeUnit;

    .prologue
    .line 163
    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    int-to-long v4, p1

    invoke-virtual {v2, v4, v5, p2}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    .line 164
    .local v0, "seconds":J
    invoke-virtual {p0}, Lcom/samsung/multiscreen/AudioPlayer;->isDebug()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "AudioPlayer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Send SeekTo : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " seconds"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    :cond_0
    sget-object v2, Lcom/samsung/multiscreen/AudioPlayer;->mApplication:Lcom/samsung/multiscreen/Application;

    const-string v3, "playerControl"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->seekTo:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    invoke-virtual {v5}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/samsung/multiscreen/Application;->publish(Ljava/lang/String;Ljava/lang/Object;)V

    .line 166
    return-void
.end method

.method public setRepeat(Lcom/samsung/multiscreen/Player$RepeatMode;)V
    .locals 4
    .param p1, "mode"    # Lcom/samsung/multiscreen/Player$RepeatMode;

    .prologue
    .line 183
    invoke-virtual {p0}, Lcom/samsung/multiscreen/AudioPlayer;->isDebug()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "AudioPlayer"

    const-string v1, "Send setRepeat"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    :cond_0
    sget-object v0, Lcom/samsung/multiscreen/AudioPlayer;->mApplication:Lcom/samsung/multiscreen/Application;

    const-string v1, "playerControl"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->setRepeat:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    invoke-virtual {v3}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/samsung/multiscreen/Player$RepeatMode;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/multiscreen/Application;->publish(Ljava/lang/String;Ljava/lang/Object;)V

    .line 185
    return-void
.end method

.method public setShuffle(Z)V
    .locals 4
    .param p1, "mode"    # Z

    .prologue
    .line 200
    invoke-virtual {p0}, Lcom/samsung/multiscreen/AudioPlayer;->isDebug()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "AudioPlayer"

    const-string v1, "Send setShuffle"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    :cond_0
    sget-object v0, Lcom/samsung/multiscreen/AudioPlayer;->mApplication:Lcom/samsung/multiscreen/Application;

    const-string v1, "playerControl"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->setShuffle:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    invoke-virtual {v3}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/multiscreen/Application;->publish(Ljava/lang/String;Ljava/lang/Object;)V

    .line 202
    return-void
.end method

.method public shuffle()V
    .locals 3

    .prologue
    .line 191
    invoke-virtual {p0}, Lcom/samsung/multiscreen/AudioPlayer;->isDebug()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "AudioPlayer"

    const-string v1, "Send shuffle"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 192
    :cond_0
    sget-object v0, Lcom/samsung/multiscreen/AudioPlayer;->mApplication:Lcom/samsung/multiscreen/Application;

    const-string v1, "playerControl"

    sget-object v2, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->shuffle:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    invoke-virtual {v2}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/multiscreen/Application;->publish(Ljava/lang/String;Ljava/lang/Object;)V

    .line 193
    return-void
.end method
