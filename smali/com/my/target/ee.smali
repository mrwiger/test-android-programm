.class public abstract Lcom/my/target/ee;
.super Landroid/widget/RelativeLayout;
.source "InterstitialPromoView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/ee$a;,
        Lcom/my/target/ee$b;
    }
.end annotation


# static fields
.field static final bw:I


# instance fields
.field protected ad:Lcom/my/target/ee$b;

.field bA:F

.field protected final bx:Lcom/my/target/ee$a;

.field private final by:Landroid/graphics/Bitmap;

.field private final bz:Landroid/graphics/Bitmap;

.field private orientation:I

.field final style:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    invoke-static {}, Lcom/my/target/cm;->bE()I

    move-result v0

    sput v0, Lcom/my/target/ee;->bw:I

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;I)V
    .locals 3

    .prologue
    const/16 v2, 0x1c

    .line 43
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 44
    iput p2, p0, Lcom/my/target/ee;->style:I

    .line 45
    invoke-static {p1}, Lcom/my/target/cm;->x(Landroid/content/Context;)Lcom/my/target/cm;

    move-result-object v0

    .line 47
    invoke-virtual {v0, v2}, Lcom/my/target/cm;->n(I)I

    move-result v1

    invoke-static {v1}, Lcom/my/target/core/resources/b;->getVolumeOnIcon(I)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/my/target/ee;->by:Landroid/graphics/Bitmap;

    .line 49
    invoke-virtual {v0, v2}, Lcom/my/target/cm;->n(I)I

    move-result v0

    invoke-static {v0}, Lcom/my/target/core/resources/b;->getVolumeOffIcon(I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/ee;->bz:Landroid/graphics/Bitmap;

    .line 50
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/my/target/ee;->setBackgroundColor(I)V

    .line 51
    new-instance v0, Lcom/my/target/ee$a;

    invoke-direct {v0, p0}, Lcom/my/target/ee$a;-><init>(Lcom/my/target/ee;)V

    iput-object v0, p0, Lcom/my/target/ee;->bx:Lcom/my/target/ee$a;

    .line 52
    return-void
.end method


# virtual methods
.method public abstract G()V
.end method

.method public abstract b(Lcom/my/target/core/models/banners/h;)V
.end method

.method public abstract e(I)V
.end method

.method public abstract f(Z)V
.end method

.method public abstract finish()V
.end method

.method public final g(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 134
    invoke-virtual {p0}, Lcom/my/target/ee;->getSoundButton()Lcom/my/target/by;

    move-result-object v0

    .line 135
    if-eqz v0, :cond_0

    .line 137
    if-eqz p1, :cond_1

    .line 139
    iget-object v1, p0, Lcom/my/target/ee;->bz:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1, v2}, Lcom/my/target/by;->b(Landroid/graphics/Bitmap;Z)V

    .line 140
    const-string v1, "sound_off"

    invoke-virtual {v0, v1}, Lcom/my/target/by;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 148
    :cond_0
    :goto_0
    return-void

    .line 144
    :cond_1
    iget-object v1, p0, Lcom/my/target/ee;->by:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1, v2}, Lcom/my/target/by;->b(Landroid/graphics/Bitmap;Z)V

    .line 145
    const-string v1, "sound_on"

    invoke-virtual {v0, v1}, Lcom/my/target/by;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected abstract getCloseButton()Landroid/view/View;
.end method

.method public getNumbersOfCurrentShowingCards()[I
    .locals 1

    .prologue
    .line 98
    const/4 v0, 0x0

    new-array v0, v0, [I

    return-object v0
.end method

.method protected abstract getSoundButton()Lcom/my/target/by;
.end method

.method public abstract isPaused()Z
.end method

.method public abstract isPlaying()Z
.end method

.method protected onAttachedToWindow()V
    .locals 3

    .prologue
    .line 162
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onAttachedToWindow()V

    .line 163
    const/4 v0, 0x1

    .line 164
    iget v1, p0, Lcom/my/target/ee;->bA:F

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    invoke-virtual {p0}, Lcom/my/target/ee;->isHardwareAccelerated()Z

    move-result v1

    if-nez v1, :cond_0

    .line 166
    const/4 v0, 0x0

    .line 168
    :cond_0
    iget-object v1, p0, Lcom/my/target/ee;->ad:Lcom/my/target/ee$b;

    if-eqz v1, :cond_1

    .line 170
    iget-object v1, p0, Lcom/my/target/ee;->ad:Lcom/my/target/ee$b;

    invoke-interface {v1, v0}, Lcom/my/target/ee$b;->d(Z)V

    .line 172
    :cond_1
    return-void
.end method

.method protected onMeasure(II)V
    .locals 2
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 177
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 1184
    int-to-float v0, v0

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 1187
    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    .line 1189
    const/4 v0, 0x2

    .line 1196
    :goto_0
    iget v1, p0, Lcom/my/target/ee;->orientation:I

    if-eq v0, v1, :cond_0

    .line 1198
    invoke-virtual {p0, v0}, Lcom/my/target/ee;->setLayoutOrientation(I)V

    .line 179
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/RelativeLayout;->onMeasure(II)V

    .line 180
    return-void

    .line 1193
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public abstract pause()V
.end method

.method public abstract play()V
.end method

.method public abstract resume()V
.end method

.method public setBanner(Lcom/my/target/core/models/banners/h;)V
    .locals 3
    .param p1, "banner"    # Lcom/my/target/core/models/banners/h;

    .prologue
    .line 56
    invoke-virtual {p1}, Lcom/my/target/core/models/banners/h;->getVideoBanner()Lcom/my/target/aj;

    move-result-object v0

    .line 57
    if-eqz v0, :cond_0

    .line 59
    invoke-virtual {v0}, Lcom/my/target/aj;->getDuration()F

    move-result v0

    iput v0, p0, Lcom/my/target/ee;->bA:F

    .line 61
    :cond_0
    invoke-virtual {p0}, Lcom/my/target/ee;->getSoundButton()Lcom/my/target/by;

    move-result-object v0

    .line 62
    if-eqz v0, :cond_1

    .line 64
    new-instance v1, Lcom/my/target/ee$1;

    invoke-direct {v1, p0}, Lcom/my/target/ee$1;-><init>(Lcom/my/target/ee;)V

    invoke-virtual {v0, v1}, Lcom/my/target/by;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 74
    iget-object v1, p0, Lcom/my/target/ee;->by:Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/my/target/by;->b(Landroid/graphics/Bitmap;Z)V

    .line 75
    const-string v1, "sound_on"

    invoke-virtual {v0, v1}, Lcom/my/target/by;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 78
    :cond_1
    invoke-virtual {p0}, Lcom/my/target/ee;->getCloseButton()Landroid/view/View;

    move-result-object v0

    .line 79
    if-eqz v0, :cond_2

    .line 81
    new-instance v1, Lcom/my/target/ee$2;

    invoke-direct {v1, p0}, Lcom/my/target/ee$2;-><init>(Lcom/my/target/ee;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 93
    :cond_2
    return-void
.end method

.method public abstract setClickArea(Lcom/my/target/af;)V
.end method

.method public setInterstitialPromoViewListener(Lcom/my/target/ee$b;)V
    .locals 0
    .param p1, "interstitialPromoViewListener"    # Lcom/my/target/ee$b;

    .prologue
    .line 121
    iput-object p1, p0, Lcom/my/target/ee;->ad:Lcom/my/target/ee$b;

    .line 122
    return-void
.end method

.method protected setLayoutOrientation(I)V
    .locals 0
    .param p1, "orientation"    # I

    .prologue
    .line 156
    iput p1, p0, Lcom/my/target/ee;->orientation:I

    .line 157
    return-void
.end method

.method public abstract setTimeChanged(F)V
.end method
