.class public Lcom/google/obf/bg;
.super Ljava/lang/Object;
.source "IMASDK"

# interfaces
.implements Lcom/google/obf/aj;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/obf/bg$a;
    }
.end annotation


# static fields
.field private static final a:I

.field private static final b:[B


# instance fields
.field private final c:I

.field private final d:Lcom/google/obf/bk;

.field private final e:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/obf/bg$a;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/google/obf/dw;

.field private final g:Lcom/google/obf/dw;

.field private final h:Lcom/google/obf/dw;

.field private final i:Lcom/google/obf/dw;

.field private final j:[B

.field private final k:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Lcom/google/obf/bc$a;",
            ">;"
        }
    .end annotation
.end field

.field private l:I

.field private m:I

.field private n:J

.field private o:I

.field private p:Lcom/google/obf/dw;

.field private q:J

.field private r:Lcom/google/obf/bg$a;

.field private s:I

.field private t:I

.field private u:I

.field private v:Lcom/google/obf/al;

.field private w:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 545
    const-string v0, "seig"

    invoke-static {v0}, Lcom/google/obf/ea;->c(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/google/obf/bg;->a:I

    .line 546
    const/16 v0, 0x10

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/obf/bg;->b:[B

    return-void

    :array_0
    .array-data 1
        -0x5et
        0x39t
        0x4ft
        0x52t
        0x5at
        -0x65t
        0x4ft
        0x14t
        -0x5et
        0x44t
        0x6ct
        0x42t
        0x7ct
        0x64t
        -0x73t
        -0xct
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/obf/bg;-><init>(I)V

    .line 2
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 3
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/obf/bg;-><init>(ILcom/google/obf/bk;)V

    .line 4
    return-void
.end method

.method public constructor <init>(ILcom/google/obf/bk;)V
    .locals 4

    .prologue
    const/16 v3, 0x10

    const/4 v1, 0x4

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 6
    iput-object p2, p0, Lcom/google/obf/bg;->d:Lcom/google/obf/bk;

    .line 7
    if-eqz p2, :cond_0

    move v0, v1

    :goto_0
    or-int/2addr v0, p1

    iput v0, p0, Lcom/google/obf/bg;->c:I

    .line 8
    new-instance v0, Lcom/google/obf/dw;

    invoke-direct {v0, v3}, Lcom/google/obf/dw;-><init>(I)V

    iput-object v0, p0, Lcom/google/obf/bg;->i:Lcom/google/obf/dw;

    .line 9
    new-instance v0, Lcom/google/obf/dw;

    sget-object v2, Lcom/google/obf/du;->a:[B

    invoke-direct {v0, v2}, Lcom/google/obf/dw;-><init>([B)V

    iput-object v0, p0, Lcom/google/obf/bg;->f:Lcom/google/obf/dw;

    .line 10
    new-instance v0, Lcom/google/obf/dw;

    invoke-direct {v0, v1}, Lcom/google/obf/dw;-><init>(I)V

    iput-object v0, p0, Lcom/google/obf/bg;->g:Lcom/google/obf/dw;

    .line 11
    new-instance v0, Lcom/google/obf/dw;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/obf/dw;-><init>(I)V

    iput-object v0, p0, Lcom/google/obf/bg;->h:Lcom/google/obf/dw;

    .line 12
    new-array v0, v3, [B

    iput-object v0, p0, Lcom/google/obf/bg;->j:[B

    .line 13
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/google/obf/bg;->k:Ljava/util/Stack;

    .line 14
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/obf/bg;->e:Landroid/util/SparseArray;

    .line 15
    invoke-direct {p0}, Lcom/google/obf/bg;->a()V

    .line 16
    return-void

    .line 7
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/google/obf/bg$a;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 509
    iget-object v2, p1, Lcom/google/obf/bg$a;->a:Lcom/google/obf/bm;

    .line 510
    iget-object v3, v2, Lcom/google/obf/bm;->l:Lcom/google/obf/dw;

    .line 511
    iget-object v0, v2, Lcom/google/obf/bm;->a:Lcom/google/obf/be;

    iget v0, v0, Lcom/google/obf/be;->a:I

    .line 512
    iget-object v4, v2, Lcom/google/obf/bm;->n:Lcom/google/obf/bl;

    if-eqz v4, :cond_0

    .line 513
    iget-object v0, v2, Lcom/google/obf/bm;->n:Lcom/google/obf/bl;

    .line 515
    :goto_0
    iget v4, v0, Lcom/google/obf/bl;->b:I

    .line 516
    iget-object v0, v2, Lcom/google/obf/bm;->j:[Z

    iget v2, p1, Lcom/google/obf/bg$a;->e:I

    aget-boolean v2, v0, v2

    .line 517
    iget-object v0, p0, Lcom/google/obf/bg;->h:Lcom/google/obf/dw;

    iget-object v5, v0, Lcom/google/obf/dw;->a:[B

    if-eqz v2, :cond_1

    const/16 v0, 0x80

    :goto_1
    or-int/2addr v0, v4

    int-to-byte v0, v0

    aput-byte v0, v5, v1

    .line 518
    iget-object v0, p0, Lcom/google/obf/bg;->h:Lcom/google/obf/dw;

    invoke-virtual {v0, v1}, Lcom/google/obf/dw;->c(I)V

    .line 519
    iget-object v0, p1, Lcom/google/obf/bg$a;->b:Lcom/google/obf/ar;

    .line 520
    iget-object v1, p0, Lcom/google/obf/bg;->h:Lcom/google/obf/dw;

    const/4 v5, 0x1

    invoke-interface {v0, v1, v5}, Lcom/google/obf/ar;->a(Lcom/google/obf/dw;I)V

    .line 521
    invoke-interface {v0, v3, v4}, Lcom/google/obf/ar;->a(Lcom/google/obf/dw;I)V

    .line 522
    if-nez v2, :cond_2

    .line 523
    add-int/lit8 v0, v4, 0x1

    .line 528
    :goto_2
    return v0

    .line 514
    :cond_0
    iget-object v4, p1, Lcom/google/obf/bg$a;->c:Lcom/google/obf/bk;

    iget-object v4, v4, Lcom/google/obf/bk;->m:[Lcom/google/obf/bl;

    aget-object v0, v4, v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 517
    goto :goto_1

    .line 524
    :cond_2
    invoke-virtual {v3}, Lcom/google/obf/dw;->g()I

    move-result v1

    .line 525
    const/4 v2, -0x2

    invoke-virtual {v3, v2}, Lcom/google/obf/dw;->d(I)V

    .line 526
    mul-int/lit8 v1, v1, 0x6

    add-int/lit8 v1, v1, 0x2

    .line 527
    invoke-interface {v0, v3, v1}, Lcom/google/obf/ar;->a(Lcom/google/obf/dw;I)V

    .line 528
    add-int/lit8 v0, v4, 0x1

    add-int/2addr v0, v1

    goto :goto_2
.end method

.method private static a(Lcom/google/obf/dw;)Landroid/util/Pair;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/obf/dw;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/obf/be;",
            ">;"
        }
    .end annotation

    .prologue
    .line 163
    const/16 v0, 0xc

    invoke-virtual {p0, v0}, Lcom/google/obf/dw;->c(I)V

    .line 164
    invoke-virtual {p0}, Lcom/google/obf/dw;->m()I

    move-result v0

    .line 165
    invoke-virtual {p0}, Lcom/google/obf/dw;->s()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    .line 166
    invoke-virtual {p0}, Lcom/google/obf/dw;->s()I

    move-result v2

    .line 167
    invoke-virtual {p0}, Lcom/google/obf/dw;->s()I

    move-result v3

    .line 168
    invoke-virtual {p0}, Lcom/google/obf/dw;->m()I

    move-result v4

    .line 169
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    new-instance v5, Lcom/google/obf/be;

    invoke-direct {v5, v1, v2, v3, v4}, Lcom/google/obf/be;-><init>(IIII)V

    invoke-static {v0, v5}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/util/List;)Lcom/google/obf/ab$a;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/obf/bc$b;",
            ">;)",
            "Lcom/google/obf/ab$a;"
        }
    .end annotation

    .prologue
    .line 529
    const/4 v1, 0x0

    .line 530
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v3

    .line 531
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_3

    .line 532
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/bc$b;

    .line 533
    iget v4, v0, Lcom/google/obf/bc$b;->aO:I

    sget v5, Lcom/google/obf/bc;->U:I

    if-ne v4, v5, :cond_1

    .line 534
    if-nez v1, :cond_0

    .line 535
    new-instance v1, Lcom/google/obf/ab$a;

    invoke-direct {v1}, Lcom/google/obf/ab$a;-><init>()V

    .line 536
    :cond_0
    iget-object v0, v0, Lcom/google/obf/bc$b;->aP:Lcom/google/obf/dw;

    iget-object v0, v0, Lcom/google/obf/dw;->a:[B

    .line 537
    invoke-static {v0}, Lcom/google/obf/bi;->a([B)Ljava/util/UUID;

    move-result-object v4

    .line 538
    if-nez v4, :cond_2

    .line 539
    const-string v0, "FragmentedMp4Extractor"

    const-string v4, "Skipped pssh atom (failed to extract uuid)"

    invoke-static {v0, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 541
    :cond_1
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 540
    :cond_2
    invoke-static {v0}, Lcom/google/obf/bi;->a([B)Ljava/util/UUID;

    move-result-object v4

    new-instance v5, Lcom/google/obf/ab$b;

    const-string v6, "video/mp4"

    invoke-direct {v5, v6, v0}, Lcom/google/obf/ab$b;-><init>(Ljava/lang/String;[B)V

    invoke-virtual {v1, v4, v5}, Lcom/google/obf/ab$a;->a(Ljava/util/UUID;Lcom/google/obf/ab$b;)V

    goto :goto_1

    .line 542
    :cond_3
    return-object v1
.end method

.method private static a(Landroid/util/SparseArray;)Lcom/google/obf/bg$a;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/obf/bg$a;",
            ">;)",
            "Lcom/google/obf/bg$a;"
        }
    .end annotation

    .prologue
    .line 497
    const/4 v1, 0x0

    .line 498
    const-wide v2, 0x7fffffffffffffffL

    .line 499
    invoke-virtual {p0}, Landroid/util/SparseArray;->size()I

    move-result v7

    .line 500
    const/4 v0, 0x0

    move v6, v0

    :goto_0
    if-ge v6, v7, :cond_1

    .line 501
    invoke-virtual {p0, v6}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/bg$a;

    .line 502
    iget v4, v0, Lcom/google/obf/bg$a;->e:I

    iget-object v5, v0, Lcom/google/obf/bg$a;->a:Lcom/google/obf/bm;

    iget v5, v5, Lcom/google/obf/bm;->d:I

    if-ne v4, v5, :cond_0

    move-wide v9, v2

    move-object v2, v1

    move-wide v0, v9

    .line 507
    :goto_1
    add-int/lit8 v3, v6, 0x1

    move v6, v3

    move-wide v9, v0

    move-object v1, v2

    move-wide v2, v9

    goto :goto_0

    .line 503
    :cond_0
    iget-object v4, v0, Lcom/google/obf/bg$a;->a:Lcom/google/obf/bm;

    iget-wide v4, v4, Lcom/google/obf/bm;->b:J

    .line 504
    cmp-long v8, v4, v2

    if-gez v8, :cond_2

    move-object v2, v0

    move-wide v0, v4

    .line 506
    goto :goto_1

    .line 508
    :cond_1
    return-object v1

    :cond_2
    move-wide v9, v2

    move-object v2, v1

    move-wide v0, v9

    goto :goto_1
.end method

.method private static a(Lcom/google/obf/dw;Landroid/util/SparseArray;I)Lcom/google/obf/bg$a;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/obf/dw;",
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/obf/bg$a;",
            ">;I)",
            "Lcom/google/obf/bg$a;"
        }
    .end annotation

    .prologue
    .line 252
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/obf/dw;->c(I)V

    .line 253
    invoke-virtual {p0}, Lcom/google/obf/dw;->m()I

    move-result v0

    .line 254
    invoke-static {v0}, Lcom/google/obf/bc;->b(I)I

    move-result v5

    .line 255
    invoke-virtual {p0}, Lcom/google/obf/dw;->m()I

    move-result v0

    .line 256
    and-int/lit8 v1, p2, 0x4

    if-nez v1, :cond_0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/bg$a;

    .line 257
    if-nez v0, :cond_1

    .line 258
    const/4 v0, 0x0

    .line 273
    :goto_1
    return-object v0

    .line 256
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 259
    :cond_1
    and-int/lit8 v1, v5, 0x1

    if-eqz v1, :cond_2

    .line 260
    invoke-virtual {p0}, Lcom/google/obf/dw;->u()J

    move-result-wide v2

    .line 261
    iget-object v1, v0, Lcom/google/obf/bg$a;->a:Lcom/google/obf/bm;

    iput-wide v2, v1, Lcom/google/obf/bm;->b:J

    .line 262
    iget-object v1, v0, Lcom/google/obf/bg$a;->a:Lcom/google/obf/bm;

    iput-wide v2, v1, Lcom/google/obf/bm;->c:J

    .line 263
    :cond_2
    iget-object v6, v0, Lcom/google/obf/bg$a;->d:Lcom/google/obf/be;

    .line 264
    and-int/lit8 v1, v5, 0x2

    if-eqz v1, :cond_3

    .line 265
    invoke-virtual {p0}, Lcom/google/obf/dw;->s()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    move v4, v1

    .line 266
    :goto_2
    and-int/lit8 v1, v5, 0x8

    if-eqz v1, :cond_4

    .line 267
    invoke-virtual {p0}, Lcom/google/obf/dw;->s()I

    move-result v1

    move v3, v1

    .line 268
    :goto_3
    and-int/lit8 v1, v5, 0x10

    if-eqz v1, :cond_5

    .line 269
    invoke-virtual {p0}, Lcom/google/obf/dw;->s()I

    move-result v1

    move v2, v1

    .line 270
    :goto_4
    and-int/lit8 v1, v5, 0x20

    if-eqz v1, :cond_6

    .line 271
    invoke-virtual {p0}, Lcom/google/obf/dw;->s()I

    move-result v1

    .line 272
    :goto_5
    iget-object v5, v0, Lcom/google/obf/bg$a;->a:Lcom/google/obf/bm;

    new-instance v6, Lcom/google/obf/be;

    invoke-direct {v6, v4, v3, v2, v1}, Lcom/google/obf/be;-><init>(IIII)V

    iput-object v6, v5, Lcom/google/obf/bm;->a:Lcom/google/obf/be;

    goto :goto_1

    .line 265
    :cond_3
    iget v1, v6, Lcom/google/obf/be;->a:I

    move v4, v1

    goto :goto_2

    .line 267
    :cond_4
    iget v1, v6, Lcom/google/obf/be;->b:I

    move v3, v1

    goto :goto_3

    .line 269
    :cond_5
    iget v1, v6, Lcom/google/obf/be;->c:I

    move v2, v1

    goto :goto_4

    .line 271
    :cond_6
    iget v1, v6, Lcom/google/obf/be;->d:I

    goto :goto_5
.end method

.method private a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 42
    iput v0, p0, Lcom/google/obf/bg;->l:I

    .line 43
    iput v0, p0, Lcom/google/obf/bg;->o:I

    .line 44
    return-void
.end method

.method private a(J)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/s;
        }
    .end annotation

    .prologue
    .line 101
    :goto_0
    iget-object v0, p0, Lcom/google/obf/bg;->k:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/obf/bg;->k:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/bc$a;

    iget-wide v0, v0, Lcom/google/obf/bc$a;->aP:J

    cmp-long v0, v0, p1

    if-nez v0, :cond_0

    .line 102
    iget-object v0, p0, Lcom/google/obf/bg;->k:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/bc$a;

    invoke-direct {p0, v0}, Lcom/google/obf/bg;->a(Lcom/google/obf/bc$a;)V

    goto :goto_0

    .line 103
    :cond_0
    invoke-direct {p0}, Lcom/google/obf/bg;->a()V

    .line 104
    return-void
.end method

.method private a(Lcom/google/obf/bc$a;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/s;
        }
    .end annotation

    .prologue
    .line 114
    iget v0, p1, Lcom/google/obf/bc$a;->aO:I

    sget v1, Lcom/google/obf/bc;->B:I

    if-ne v0, v1, :cond_1

    .line 115
    invoke-direct {p0, p1}, Lcom/google/obf/bg;->b(Lcom/google/obf/bc$a;)V

    .line 120
    :cond_0
    :goto_0
    return-void

    .line 116
    :cond_1
    iget v0, p1, Lcom/google/obf/bc$a;->aO:I

    sget v1, Lcom/google/obf/bc;->K:I

    if-ne v0, v1, :cond_2

    .line 117
    invoke-direct {p0, p1}, Lcom/google/obf/bg;->c(Lcom/google/obf/bc$a;)V

    goto :goto_0

    .line 118
    :cond_2
    iget-object v0, p0, Lcom/google/obf/bg;->k:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 119
    iget-object v0, p0, Lcom/google/obf/bg;->k:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/bc$a;

    invoke-virtual {v0, p1}, Lcom/google/obf/bc$a;->a(Lcom/google/obf/bc$a;)V

    goto :goto_0
.end method

.method private static a(Lcom/google/obf/bc$a;Landroid/util/SparseArray;I[B)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/obf/bc$a;",
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/obf/bg$a;",
            ">;I[B)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/s;
        }
    .end annotation

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/obf/bc$a;->aR:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 175
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 176
    iget-object v0, p0, Lcom/google/obf/bc$a;->aR:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/bc$a;

    .line 177
    iget v3, v0, Lcom/google/obf/bc$a;->aO:I

    sget v4, Lcom/google/obf/bc;->L:I

    if-ne v3, v4, :cond_0

    .line 178
    invoke-static {v0, p1, p2, p3}, Lcom/google/obf/bg;->b(Lcom/google/obf/bc$a;Landroid/util/SparseArray;I[B)V

    .line 179
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 180
    :cond_1
    return-void
.end method

.method private a(Lcom/google/obf/bc$b;J)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/s;
        }
    .end annotation

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/obf/bg;->k:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 106
    iget-object v0, p0, Lcom/google/obf/bg;->k:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/bc$a;

    invoke-virtual {v0, p1}, Lcom/google/obf/bc$a;->a(Lcom/google/obf/bc$b;)V

    .line 113
    :cond_0
    :goto_0
    return-void

    .line 107
    :cond_1
    iget v0, p1, Lcom/google/obf/bc$b;->aO:I

    sget v1, Lcom/google/obf/bc;->A:I

    if-ne v0, v1, :cond_2

    .line 108
    iget-object v0, p1, Lcom/google/obf/bc$b;->aP:Lcom/google/obf/dw;

    invoke-static {v0, p2, p3}, Lcom/google/obf/bg;->b(Lcom/google/obf/dw;J)Lcom/google/obf/af;

    move-result-object v0

    .line 109
    iget-object v1, p0, Lcom/google/obf/bg;->v:Lcom/google/obf/al;

    invoke-interface {v1, v0}, Lcom/google/obf/al;->a(Lcom/google/obf/aq;)V

    .line 110
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/obf/bg;->w:Z

    goto :goto_0

    .line 111
    :cond_2
    iget v0, p1, Lcom/google/obf/bc$b;->aO:I

    sget v1, Lcom/google/obf/bc;->aF:I

    if-ne v0, v1, :cond_0

    .line 112
    iget-object v0, p1, Lcom/google/obf/bc$b;->aP:Lcom/google/obf/dw;

    invoke-virtual {p0, v0, p2, p3}, Lcom/google/obf/bg;->a(Lcom/google/obf/dw;J)V

    goto :goto_0
.end method

.method private static a(Lcom/google/obf/bg$a;JILcom/google/obf/dw;)V
    .locals 29

    .prologue
    .line 278
    const/16 v2, 0x8

    move-object/from16 v0, p4

    invoke-virtual {v0, v2}, Lcom/google/obf/dw;->c(I)V

    .line 279
    invoke-virtual/range {p4 .. p4}, Lcom/google/obf/dw;->m()I

    move-result v2

    .line 280
    invoke-static {v2}, Lcom/google/obf/bc;->b(I)I

    move-result v3

    .line 281
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/obf/bg$a;->c:Lcom/google/obf/bk;

    .line 282
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/obf/bg$a;->a:Lcom/google/obf/bm;

    move-object/from16 v21, v0

    .line 283
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/google/obf/bm;->a:Lcom/google/obf/be;

    move-object/from16 v22, v0

    .line 284
    invoke-virtual/range {p4 .. p4}, Lcom/google/obf/dw;->s()I

    move-result v23

    .line 285
    and-int/lit8 v2, v3, 0x1

    if-eqz v2, :cond_0

    .line 286
    move-object/from16 v0, v21

    iget-wide v4, v0, Lcom/google/obf/bm;->b:J

    invoke-virtual/range {p4 .. p4}, Lcom/google/obf/dw;->m()I

    move-result v2

    int-to-long v6, v2

    add-long/2addr v4, v6

    move-object/from16 v0, v21

    iput-wide v4, v0, Lcom/google/obf/bm;->b:J

    .line 287
    :cond_0
    and-int/lit8 v2, v3, 0x4

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    move v8, v2

    .line 288
    :goto_0
    move-object/from16 v0, v22

    iget v14, v0, Lcom/google/obf/be;->d:I

    .line 289
    if-eqz v8, :cond_1

    .line 290
    invoke-virtual/range {p4 .. p4}, Lcom/google/obf/dw;->s()I

    move-result v14

    .line 291
    :cond_1
    and-int/lit16 v2, v3, 0x100

    if-eqz v2, :cond_4

    const/4 v2, 0x1

    move/from16 v20, v2

    .line 292
    :goto_1
    and-int/lit16 v2, v3, 0x200

    if-eqz v2, :cond_5

    const/4 v2, 0x1

    move/from16 v19, v2

    .line 293
    :goto_2
    and-int/lit16 v2, v3, 0x400

    if-eqz v2, :cond_6

    const/4 v2, 0x1

    move/from16 v18, v2

    .line 294
    :goto_3
    and-int/lit16 v2, v3, 0x800

    if-eqz v2, :cond_7

    const/4 v2, 0x1

    move v9, v2

    .line 295
    :goto_4
    const-wide/16 v2, 0x0

    .line 296
    iget-object v4, v12, Lcom/google/obf/bk;->n:[J

    if-eqz v4, :cond_10

    iget-object v4, v12, Lcom/google/obf/bk;->n:[J

    array-length v4, v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_10

    iget-object v4, v12, Lcom/google/obf/bk;->n:[J

    const/4 v5, 0x0

    aget-wide v4, v4, v5

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-nez v4, :cond_10

    .line 297
    iget-object v2, v12, Lcom/google/obf/bk;->o:[J

    const/4 v3, 0x0

    aget-wide v2, v2, v3

    const-wide/16 v4, 0x3e8

    iget-wide v6, v12, Lcom/google/obf/bk;->i:J

    invoke-static/range {v2 .. v7}, Lcom/google/obf/ea;->a(JJJ)J

    move-result-wide v2

    move-wide v10, v2

    .line 298
    :goto_5
    move-object/from16 v0, v21

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/google/obf/bm;->a(I)V

    .line 299
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/google/obf/bm;->e:[I

    move-object/from16 v24, v0

    .line 300
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/google/obf/bm;->f:[I

    move-object/from16 v25, v0

    .line 301
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/google/obf/bm;->g:[J

    move-object/from16 v26, v0

    .line 302
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/google/obf/bm;->h:[Z

    move-object/from16 v27, v0

    .line 303
    iget-wide v6, v12, Lcom/google/obf/bk;->i:J

    .line 305
    iget v2, v12, Lcom/google/obf/bk;->h:I

    sget v3, Lcom/google/obf/bk;->a:I

    if-ne v2, v3, :cond_8

    and-int/lit8 v2, p3, 0x1

    if-eqz v2, :cond_8

    const/4 v2, 0x1

    move v12, v2

    .line 306
    :goto_6
    const/4 v2, 0x0

    move/from16 v17, v2

    move-wide/from16 v2, p1

    :goto_7
    move/from16 v0, v17

    move/from16 v1, v23

    if-ge v0, v1, :cond_f

    .line 307
    if-eqz v20, :cond_9

    invoke-virtual/range {p4 .. p4}, Lcom/google/obf/dw;->s()I

    move-result v4

    move/from16 v16, v4

    .line 309
    :goto_8
    if-eqz v19, :cond_a

    invoke-virtual/range {p4 .. p4}, Lcom/google/obf/dw;->s()I

    move-result v4

    move v15, v4

    .line 310
    :goto_9
    if-nez v17, :cond_b

    if-eqz v8, :cond_b

    move v13, v14

    .line 312
    :goto_a
    if-eqz v9, :cond_d

    .line 313
    invoke-virtual/range {p4 .. p4}, Lcom/google/obf/dw;->m()I

    move-result v4

    .line 314
    mul-int/lit16 v4, v4, 0x3e8

    int-to-long v4, v4

    div-long/2addr v4, v6

    long-to-int v4, v4

    aput v4, v25, v17

    .line 317
    :goto_b
    const-wide/16 v4, 0x3e8

    .line 318
    invoke-static/range {v2 .. v7}, Lcom/google/obf/ea;->a(JJJ)J

    move-result-wide v4

    sub-long/2addr v4, v10

    aput-wide v4, v26, v17

    .line 319
    aput v15, v24, v17

    .line 320
    shr-int/lit8 v4, v13, 0x10

    and-int/lit8 v4, v4, 0x1

    if-nez v4, :cond_e

    if-eqz v12, :cond_2

    if-nez v17, :cond_e

    :cond_2
    const/4 v4, 0x1

    :goto_c
    aput-boolean v4, v27, v17

    .line 321
    move/from16 v0, v16

    int-to-long v4, v0

    add-long p1, v2, v4

    .line 322
    add-int/lit8 v2, v17, 0x1

    move/from16 v17, v2

    move-wide/from16 v2, p1

    goto :goto_7

    .line 287
    :cond_3
    const/4 v2, 0x0

    move v8, v2

    goto/16 :goto_0

    .line 291
    :cond_4
    const/4 v2, 0x0

    move/from16 v20, v2

    goto/16 :goto_1

    .line 292
    :cond_5
    const/4 v2, 0x0

    move/from16 v19, v2

    goto/16 :goto_2

    .line 293
    :cond_6
    const/4 v2, 0x0

    move/from16 v18, v2

    goto/16 :goto_3

    .line 294
    :cond_7
    const/4 v2, 0x0

    move v9, v2

    goto/16 :goto_4

    .line 305
    :cond_8
    const/4 v2, 0x0

    move v12, v2

    goto :goto_6

    .line 308
    :cond_9
    move-object/from16 v0, v22

    iget v4, v0, Lcom/google/obf/be;->b:I

    move/from16 v16, v4

    goto :goto_8

    .line 309
    :cond_a
    move-object/from16 v0, v22

    iget v4, v0, Lcom/google/obf/be;->c:I

    move v15, v4

    goto :goto_9

    .line 311
    :cond_b
    if-eqz v18, :cond_c

    invoke-virtual/range {p4 .. p4}, Lcom/google/obf/dw;->m()I

    move-result v4

    move v13, v4

    goto :goto_a

    :cond_c
    move-object/from16 v0, v22

    iget v4, v0, Lcom/google/obf/be;->d:I

    move v13, v4

    goto :goto_a

    .line 316
    :cond_d
    const/4 v4, 0x0

    aput v4, v25, v17

    goto :goto_b

    .line 320
    :cond_e
    const/4 v4, 0x0

    goto :goto_c

    .line 323
    :cond_f
    move-object/from16 v0, v21

    iput-wide v2, v0, Lcom/google/obf/bm;->o:J

    .line 324
    return-void

    :cond_10
    move-wide v10, v2

    goto/16 :goto_5
.end method

.method private static a(Lcom/google/obf/bl;Lcom/google/obf/dw;Lcom/google/obf/bm;)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/s;
        }
    .end annotation

    .prologue
    const/16 v3, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 216
    iget v5, p0, Lcom/google/obf/bl;->b:I

    .line 217
    invoke-virtual {p1, v3}, Lcom/google/obf/dw;->c(I)V

    .line 218
    invoke-virtual {p1}, Lcom/google/obf/dw;->m()I

    move-result v0

    .line 219
    invoke-static {v0}, Lcom/google/obf/bc;->b(I)I

    move-result v0

    .line 220
    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 221
    invoke-virtual {p1, v3}, Lcom/google/obf/dw;->d(I)V

    .line 222
    :cond_0
    invoke-virtual {p1}, Lcom/google/obf/dw;->f()I

    move-result v0

    .line 223
    invoke-virtual {p1}, Lcom/google/obf/dw;->s()I

    move-result v6

    .line 224
    iget v3, p2, Lcom/google/obf/bm;->d:I

    if-eq v6, v3, :cond_1

    .line 225
    new-instance v0, Lcom/google/obf/s;

    iget v1, p2, Lcom/google/obf/bm;->d:I

    const/16 v2, 0x29

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Length mismatch: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v0

    .line 227
    :cond_1
    if-nez v0, :cond_3

    .line 228
    iget-object v7, p2, Lcom/google/obf/bm;->j:[Z

    move v3, v2

    move v0, v2

    .line 229
    :goto_0
    if-ge v3, v6, :cond_4

    .line 230
    invoke-virtual {p1}, Lcom/google/obf/dw;->f()I

    move-result v8

    .line 231
    add-int v4, v0, v8

    .line 232
    if-le v8, v5, :cond_2

    move v0, v1

    :goto_1
    aput-boolean v0, v7, v3

    .line 233
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v0, v4

    goto :goto_0

    :cond_2
    move v0, v2

    .line 232
    goto :goto_1

    .line 235
    :cond_3
    if-le v0, v5, :cond_5

    .line 236
    :goto_2
    mul-int/2addr v0, v6

    add-int/2addr v0, v2

    .line 237
    iget-object v3, p2, Lcom/google/obf/bm;->j:[Z

    invoke-static {v3, v2, v6, v1}, Ljava/util/Arrays;->fill([ZIIZ)V

    .line 238
    :cond_4
    invoke-virtual {p2, v0}, Lcom/google/obf/bm;->b(I)V

    .line 239
    return-void

    :cond_5
    move v1, v2

    .line 235
    goto :goto_2
.end method

.method private static a(Lcom/google/obf/dw;ILcom/google/obf/bm;)V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/s;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 333
    add-int/lit8 v0, p1, 0x8

    invoke-virtual {p0, v0}, Lcom/google/obf/dw;->c(I)V

    .line 334
    invoke-virtual {p0}, Lcom/google/obf/dw;->m()I

    move-result v0

    .line 335
    invoke-static {v0}, Lcom/google/obf/bc;->b(I)I

    move-result v0

    .line 336
    and-int/lit8 v2, v0, 0x1

    if-eqz v2, :cond_0

    .line 337
    new-instance v0, Lcom/google/obf/s;

    const-string v1, "Overriding TrackEncryptionBox parameters is unsupported."

    invoke-direct {v0, v1}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v0

    .line 338
    :cond_0
    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 339
    :goto_0
    invoke-virtual {p0}, Lcom/google/obf/dw;->s()I

    move-result v2

    .line 340
    iget v3, p2, Lcom/google/obf/bm;->d:I

    if-eq v2, v3, :cond_2

    .line 341
    new-instance v0, Lcom/google/obf/s;

    iget v1, p2, Lcom/google/obf/bm;->d:I

    const/16 v3, 0x29

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Length mismatch: "

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    move v0, v1

    .line 338
    goto :goto_0

    .line 342
    :cond_2
    iget-object v3, p2, Lcom/google/obf/bm;->j:[Z

    invoke-static {v3, v1, v2, v0}, Ljava/util/Arrays;->fill([ZIIZ)V

    .line 343
    invoke-virtual {p0}, Lcom/google/obf/dw;->b()I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/google/obf/bm;->b(I)V

    .line 344
    invoke-virtual {p2, p0}, Lcom/google/obf/bm;->a(Lcom/google/obf/dw;)V

    .line 345
    return-void
.end method

.method private static a(Lcom/google/obf/dw;Lcom/google/obf/bm;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/s;
        }
    .end annotation

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x1

    .line 240
    invoke-virtual {p0, v3}, Lcom/google/obf/dw;->c(I)V

    .line 241
    invoke-virtual {p0}, Lcom/google/obf/dw;->m()I

    move-result v0

    .line 242
    invoke-static {v0}, Lcom/google/obf/bc;->b(I)I

    move-result v1

    .line 243
    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_0

    .line 244
    invoke-virtual {p0, v3}, Lcom/google/obf/dw;->d(I)V

    .line 245
    :cond_0
    invoke-virtual {p0}, Lcom/google/obf/dw;->s()I

    move-result v1

    .line 246
    if-eq v1, v2, :cond_1

    .line 247
    new-instance v0, Lcom/google/obf/s;

    const/16 v2, 0x28

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unexpected saio entry count: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v0

    .line 248
    :cond_1
    invoke-static {v0}, Lcom/google/obf/bc;->a(I)I

    move-result v0

    .line 249
    iget-wide v2, p1, Lcom/google/obf/bm;->c:J

    .line 250
    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/google/obf/dw;->k()J

    move-result-wide v0

    :goto_0
    add-long/2addr v0, v2

    iput-wide v0, p1, Lcom/google/obf/bm;->c:J

    .line 251
    return-void

    .line 250
    :cond_2
    invoke-virtual {p0}, Lcom/google/obf/dw;->u()J

    move-result-wide v0

    goto :goto_0
.end method

.method private static a(Lcom/google/obf/dw;Lcom/google/obf/bm;[B)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/s;
        }
    .end annotation

    .prologue
    const/16 v1, 0x10

    .line 325
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/obf/dw;->c(I)V

    .line 326
    const/4 v0, 0x0

    invoke-virtual {p0, p2, v0, v1}, Lcom/google/obf/dw;->a([BII)V

    .line 327
    sget-object v0, Lcom/google/obf/bg;->b:[B

    invoke-static {p2, v0}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_0

    .line 330
    :goto_0
    return-void

    .line 329
    :cond_0
    invoke-static {p0, v1, p1}, Lcom/google/obf/bg;->a(Lcom/google/obf/dw;ILcom/google/obf/bm;)V

    goto :goto_0
.end method

.method private static a(Lcom/google/obf/dw;Lcom/google/obf/dw;Lcom/google/obf/bm;)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/s;
        }
    .end annotation

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x4

    const/4 v8, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 346
    invoke-virtual {p0, v6}, Lcom/google/obf/dw;->c(I)V

    .line 347
    invoke-virtual {p0}, Lcom/google/obf/dw;->m()I

    move-result v0

    .line 348
    invoke-virtual {p0}, Lcom/google/obf/dw;->m()I

    move-result v3

    sget v4, Lcom/google/obf/bg;->a:I

    if-eq v3, v4, :cond_1

    .line 375
    :cond_0
    :goto_0
    return-void

    .line 350
    :cond_1
    invoke-static {v0}, Lcom/google/obf/bc;->a(I)I

    move-result v0

    if-ne v0, v1, :cond_2

    .line 351
    invoke-virtual {p0, v5}, Lcom/google/obf/dw;->d(I)V

    .line 352
    :cond_2
    invoke-virtual {p0}, Lcom/google/obf/dw;->m()I

    move-result v0

    if-eq v0, v1, :cond_3

    .line 353
    new-instance v0, Lcom/google/obf/s;

    const-string v1, "Entry count in sbgp != 1 (unsupported)."

    invoke-direct {v0, v1}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v0

    .line 354
    :cond_3
    invoke-virtual {p1, v6}, Lcom/google/obf/dw;->c(I)V

    .line 355
    invoke-virtual {p1}, Lcom/google/obf/dw;->m()I

    move-result v0

    .line 356
    invoke-virtual {p1}, Lcom/google/obf/dw;->m()I

    move-result v3

    sget v4, Lcom/google/obf/bg;->a:I

    if-ne v3, v4, :cond_0

    .line 358
    invoke-static {v0}, Lcom/google/obf/bc;->a(I)I

    move-result v0

    .line 359
    if-ne v0, v1, :cond_4

    .line 360
    invoke-virtual {p1}, Lcom/google/obf/dw;->k()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-nez v0, :cond_5

    .line 361
    new-instance v0, Lcom/google/obf/s;

    const-string v1, "Variable length decription in sgpd found (unsupported)"

    invoke-direct {v0, v1}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v0

    .line 362
    :cond_4
    if-lt v0, v8, :cond_5

    .line 363
    invoke-virtual {p1, v5}, Lcom/google/obf/dw;->d(I)V

    .line 364
    :cond_5
    invoke-virtual {p1}, Lcom/google/obf/dw;->k()J

    move-result-wide v4

    const-wide/16 v6, 0x1

    cmp-long v0, v4, v6

    if-eqz v0, :cond_6

    .line 365
    new-instance v0, Lcom/google/obf/s;

    const-string v1, "Entry count in sgpd != 1 (unsupported)."

    invoke-direct {v0, v1}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v0

    .line 366
    :cond_6
    invoke-virtual {p1, v8}, Lcom/google/obf/dw;->d(I)V

    .line 367
    invoke-virtual {p1}, Lcom/google/obf/dw;->f()I

    move-result v0

    if-ne v0, v1, :cond_7

    move v0, v1

    .line 368
    :goto_1
    if-eqz v0, :cond_0

    .line 370
    invoke-virtual {p1}, Lcom/google/obf/dw;->f()I

    move-result v3

    .line 371
    const/16 v4, 0x10

    new-array v4, v4, [B

    .line 372
    array-length v5, v4

    invoke-virtual {p1, v4, v2, v5}, Lcom/google/obf/dw;->a([BII)V

    .line 373
    iput-boolean v1, p2, Lcom/google/obf/bm;->i:Z

    .line 374
    new-instance v1, Lcom/google/obf/bl;

    invoke-direct {v1, v0, v3, v4}, Lcom/google/obf/bl;-><init>(ZI[B)V

    iput-object v1, p2, Lcom/google/obf/bm;->n:Lcom/google/obf/bl;

    goto :goto_0

    :cond_7
    move v0, v2

    .line 367
    goto :goto_1
.end method

.method private static a(I)Z
    .locals 1

    .prologue
    .line 543
    sget v0, Lcom/google/obf/bc;->S:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/obf/bc;->R:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/obf/bc;->C:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/obf/bc;->A:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/obf/bc;->T:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/obf/bc;->w:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/obf/bc;->x:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/obf/bc;->O:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/obf/bc;->y:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/obf/bc;->z:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/obf/bc;->U:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/obf/bc;->ac:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/obf/bc;->ad:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/obf/bc;->ah:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/obf/bc;->ae:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/obf/bc;->af:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/obf/bc;->ag:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/obf/bc;->Q:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/obf/bc;->N:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/obf/bc;->aF:I

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Lcom/google/obf/dw;)J
    .locals 2

    .prologue
    .line 170
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/obf/dw;->c(I)V

    .line 171
    invoke-virtual {p0}, Lcom/google/obf/dw;->m()I

    move-result v0

    .line 172
    invoke-static {v0}, Lcom/google/obf/bc;->a(I)I

    move-result v0

    .line 173
    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/obf/dw;->k()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/obf/dw;->u()J

    move-result-wide v0

    goto :goto_0
.end method

.method private static b(Lcom/google/obf/dw;J)Lcom/google/obf/af;
    .locals 23
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/s;
        }
    .end annotation

    .prologue
    .line 377
    const/16 v4, 0x8

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/google/obf/dw;->c(I)V

    .line 378
    invoke-virtual/range {p0 .. p0}, Lcom/google/obf/dw;->m()I

    move-result v4

    .line 379
    invoke-static {v4}, Lcom/google/obf/bc;->a(I)I

    move-result v4

    .line 380
    const/4 v5, 0x4

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/google/obf/dw;->d(I)V

    .line 381
    invoke-virtual/range {p0 .. p0}, Lcom/google/obf/dw;->k()J

    move-result-wide v8

    .line 383
    if-nez v4, :cond_0

    .line 384
    invoke-virtual/range {p0 .. p0}, Lcom/google/obf/dw;->k()J

    move-result-wide v4

    .line 385
    invoke-virtual/range {p0 .. p0}, Lcom/google/obf/dw;->k()J

    move-result-wide v6

    add-long v6, v6, p1

    move-wide v10, v6

    .line 388
    :goto_0
    const/4 v6, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/google/obf/dw;->d(I)V

    .line 389
    invoke-virtual/range {p0 .. p0}, Lcom/google/obf/dw;->g()I

    move-result v16

    .line 390
    move/from16 v0, v16

    new-array v0, v0, [I

    move-object/from16 v17, v0

    .line 391
    move/from16 v0, v16

    new-array v0, v0, [J

    move-object/from16 v18, v0

    .line 392
    move/from16 v0, v16

    new-array v0, v0, [J

    move-object/from16 v19, v0

    .line 393
    move/from16 v0, v16

    new-array v0, v0, [J

    move-object/from16 v20, v0

    .line 395
    const-wide/32 v6, 0xf4240

    invoke-static/range {v4 .. v9}, Lcom/google/obf/ea;->a(JJJ)J

    move-result-wide v12

    .line 396
    const/4 v6, 0x0

    move-wide v14, v10

    move v10, v6

    move-wide v6, v4

    move-wide v4, v12

    :goto_1
    move/from16 v0, v16

    if-ge v10, v0, :cond_2

    .line 397
    invoke-virtual/range {p0 .. p0}, Lcom/google/obf/dw;->m()I

    move-result v11

    .line 398
    const/high16 v12, -0x80000000

    and-int/2addr v12, v11

    .line 399
    if-eqz v12, :cond_1

    .line 400
    new-instance v4, Lcom/google/obf/s;

    const-string v5, "Unhandled indirect reference"

    invoke-direct {v4, v5}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v4

    .line 386
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/obf/dw;->u()J

    move-result-wide v4

    .line 387
    invoke-virtual/range {p0 .. p0}, Lcom/google/obf/dw;->u()J

    move-result-wide v6

    add-long v6, v6, p1

    move-wide v10, v6

    goto :goto_0

    .line 401
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/obf/dw;->k()J

    move-result-wide v12

    .line 402
    const v21, 0x7fffffff

    and-int v11, v11, v21

    aput v11, v17, v10

    .line 403
    aput-wide v14, v18, v10

    .line 404
    aput-wide v4, v20, v10

    .line 405
    add-long v4, v6, v12

    .line 406
    const-wide/32 v6, 0xf4240

    invoke-static/range {v4 .. v9}, Lcom/google/obf/ea;->a(JJJ)J

    move-result-wide v12

    .line 407
    aget-wide v6, v20, v10

    sub-long v6, v12, v6

    aput-wide v6, v19, v10

    .line 408
    const/4 v6, 0x4

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/google/obf/dw;->d(I)V

    .line 409
    aget v6, v17, v10

    int-to-long v6, v6

    add-long/2addr v14, v6

    .line 410
    add-int/lit8 v6, v10, 0x1

    move v10, v6

    move-wide v6, v4

    move-wide v4, v12

    goto :goto_1

    .line 411
    :cond_2
    new-instance v4, Lcom/google/obf/af;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    move-object/from16 v3, v20

    invoke-direct {v4, v0, v1, v2, v3}, Lcom/google/obf/af;-><init>([I[J[J[J)V

    return-object v4
.end method

.method private b(Lcom/google/obf/bc$a;)V
    .locals 11

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 121
    iget-object v0, p0, Lcom/google/obf/bg;->d:Lcom/google/obf/bk;

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    const-string v3, "Unexpected moov box."

    invoke-static {v0, v3}, Lcom/google/obf/dl;->b(ZLjava/lang/Object;)V

    .line 122
    iget-object v0, p1, Lcom/google/obf/bc$a;->aQ:Ljava/util/List;

    invoke-static {v0}, Lcom/google/obf/bg;->a(Ljava/util/List;)Lcom/google/obf/ab$a;

    move-result-object v0

    .line 123
    if-eqz v0, :cond_0

    .line 124
    iget-object v3, p0, Lcom/google/obf/bg;->v:Lcom/google/obf/al;

    invoke-interface {v3, v0}, Lcom/google/obf/al;->a(Lcom/google/obf/ab;)V

    .line 125
    :cond_0
    sget v0, Lcom/google/obf/bc;->M:I

    invoke-virtual {p1, v0}, Lcom/google/obf/bc$a;->e(I)Lcom/google/obf/bc$a;

    move-result-object v6

    .line 126
    new-instance v7, Landroid/util/SparseArray;

    invoke-direct {v7}, Landroid/util/SparseArray;-><init>()V

    .line 127
    const-wide/16 v4, -0x1

    .line 128
    iget-object v0, v6, Lcom/google/obf/bc$a;->aQ:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v8

    move v3, v2

    .line 129
    :goto_1
    if-ge v3, v8, :cond_4

    .line 130
    iget-object v0, v6, Lcom/google/obf/bc$a;->aQ:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/bc$b;

    .line 131
    iget v9, v0, Lcom/google/obf/bc$b;->aO:I

    sget v10, Lcom/google/obf/bc;->y:I

    if-ne v9, v10, :cond_3

    .line 132
    iget-object v0, v0, Lcom/google/obf/bc$b;->aP:Lcom/google/obf/dw;

    invoke-static {v0}, Lcom/google/obf/bg;->a(Lcom/google/obf/dw;)Landroid/util/Pair;

    move-result-object v9

    .line 133
    iget-object v0, v9, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v10

    iget-object v0, v9, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lcom/google/obf/be;

    invoke-virtual {v7, v10, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 136
    :cond_1
    :goto_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_2
    move v0, v2

    .line 121
    goto :goto_0

    .line 134
    :cond_3
    iget v9, v0, Lcom/google/obf/bc$b;->aO:I

    sget v10, Lcom/google/obf/bc;->N:I

    if-ne v9, v10, :cond_1

    .line 135
    iget-object v0, v0, Lcom/google/obf/bc$b;->aP:Lcom/google/obf/dw;

    invoke-static {v0}, Lcom/google/obf/bg;->b(Lcom/google/obf/dw;)J

    move-result-wide v4

    goto :goto_2

    .line 137
    :cond_4
    new-instance v6, Landroid/util/SparseArray;

    invoke-direct {v6}, Landroid/util/SparseArray;-><init>()V

    .line 138
    iget-object v0, p1, Lcom/google/obf/bc$a;->aR:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v8

    move v3, v2

    .line 139
    :goto_3
    if-ge v3, v8, :cond_6

    .line 140
    iget-object v0, p1, Lcom/google/obf/bc$a;->aR:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/bc$a;

    .line 141
    iget v9, v0, Lcom/google/obf/bc$a;->aO:I

    sget v10, Lcom/google/obf/bc;->D:I

    if-ne v9, v10, :cond_5

    .line 142
    sget v9, Lcom/google/obf/bc;->C:I

    invoke-virtual {p1, v9}, Lcom/google/obf/bc$a;->d(I)Lcom/google/obf/bc$b;

    move-result-object v9

    invoke-static {v0, v9, v4, v5, v2}, Lcom/google/obf/bd;->a(Lcom/google/obf/bc$a;Lcom/google/obf/bc$b;JZ)Lcom/google/obf/bk;

    move-result-object v0

    .line 143
    if-eqz v0, :cond_5

    .line 144
    iget v9, v0, Lcom/google/obf/bk;->g:I

    invoke-virtual {v6, v9, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 145
    :cond_5
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    .line 146
    :cond_6
    invoke-virtual {v6}, Landroid/util/SparseArray;->size()I

    move-result v4

    .line 147
    iget-object v0, p0, Lcom/google/obf/bg;->e:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-nez v0, :cond_8

    move v1, v2

    .line 148
    :goto_4
    if-ge v1, v4, :cond_7

    .line 149
    iget-object v3, p0, Lcom/google/obf/bg;->e:Landroid/util/SparseArray;

    invoke-virtual {v6, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/bk;

    iget v0, v0, Lcom/google/obf/bk;->g:I

    new-instance v5, Lcom/google/obf/bg$a;

    iget-object v8, p0, Lcom/google/obf/bg;->v:Lcom/google/obf/al;

    invoke-interface {v8, v1}, Lcom/google/obf/al;->d(I)Lcom/google/obf/ar;

    move-result-object v8

    invoke-direct {v5, v8}, Lcom/google/obf/bg$a;-><init>(Lcom/google/obf/ar;)V

    invoke-virtual {v3, v0, v5}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 150
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 151
    :cond_7
    iget-object v0, p0, Lcom/google/obf/bg;->v:Lcom/google/obf/al;

    invoke-interface {v0}, Lcom/google/obf/al;->f()V

    :goto_5
    move v3, v2

    .line 153
    :goto_6
    if-ge v3, v4, :cond_a

    .line 154
    invoke-virtual {v6, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/bk;

    .line 155
    iget-object v1, p0, Lcom/google/obf/bg;->e:Landroid/util/SparseArray;

    iget v2, v0, Lcom/google/obf/bk;->g:I

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/obf/bg$a;

    iget v2, v0, Lcom/google/obf/bk;->g:I

    invoke-virtual {v7, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/obf/be;

    invoke-virtual {v1, v0, v2}, Lcom/google/obf/bg$a;->a(Lcom/google/obf/bk;Lcom/google/obf/be;)V

    .line 156
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_6

    .line 152
    :cond_8
    iget-object v0, p0, Lcom/google/obf/bg;->e:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ne v0, v4, :cond_9

    :goto_7
    invoke-static {v1}, Lcom/google/obf/dl;->b(Z)V

    goto :goto_5

    :cond_9
    move v1, v2

    goto :goto_7

    .line 157
    :cond_a
    return-void
.end method

.method private static b(Lcom/google/obf/bc$a;Landroid/util/SparseArray;I[B)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/obf/bc$a;",
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/obf/bg$a;",
            ">;I[B)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/s;
        }
    .end annotation

    .prologue
    .line 181
    sget v0, Lcom/google/obf/bc;->z:I

    invoke-virtual {p0, v0}, Lcom/google/obf/bc$a;->f(I)I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 182
    new-instance v0, Lcom/google/obf/s;

    const-string v1, "Trun count in traf != 1 (unsupported)."

    invoke-direct {v0, v1}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v0

    .line 183
    :cond_0
    sget v0, Lcom/google/obf/bc;->x:I

    invoke-virtual {p0, v0}, Lcom/google/obf/bc$a;->d(I)Lcom/google/obf/bc$b;

    move-result-object v0

    .line 184
    iget-object v0, v0, Lcom/google/obf/bc$b;->aP:Lcom/google/obf/dw;

    invoke-static {v0, p1, p2}, Lcom/google/obf/bg;->a(Lcom/google/obf/dw;Landroid/util/SparseArray;I)Lcom/google/obf/bg$a;

    move-result-object v2

    .line 185
    if-nez v2, :cond_2

    .line 215
    :cond_1
    return-void

    .line 187
    :cond_2
    iget-object v3, v2, Lcom/google/obf/bg$a;->a:Lcom/google/obf/bm;

    .line 188
    iget-wide v0, v3, Lcom/google/obf/bm;->o:J

    .line 189
    invoke-virtual {v2}, Lcom/google/obf/bg$a;->a()V

    .line 190
    sget v4, Lcom/google/obf/bc;->w:I

    invoke-virtual {p0, v4}, Lcom/google/obf/bc$a;->d(I)Lcom/google/obf/bc$b;

    move-result-object v4

    .line 191
    if-eqz v4, :cond_3

    and-int/lit8 v4, p2, 0x2

    if-nez v4, :cond_3

    .line 192
    sget v0, Lcom/google/obf/bc;->w:I

    invoke-virtual {p0, v0}, Lcom/google/obf/bc$a;->d(I)Lcom/google/obf/bc$b;

    move-result-object v0

    iget-object v0, v0, Lcom/google/obf/bc$b;->aP:Lcom/google/obf/dw;

    invoke-static {v0}, Lcom/google/obf/bg;->c(Lcom/google/obf/dw;)J

    move-result-wide v0

    .line 193
    :cond_3
    sget v4, Lcom/google/obf/bc;->z:I

    invoke-virtual {p0, v4}, Lcom/google/obf/bc$a;->d(I)Lcom/google/obf/bc$b;

    move-result-object v4

    .line 194
    iget-object v4, v4, Lcom/google/obf/bc$b;->aP:Lcom/google/obf/dw;

    invoke-static {v2, v0, v1, p2, v4}, Lcom/google/obf/bg;->a(Lcom/google/obf/bg$a;JILcom/google/obf/dw;)V

    .line 195
    sget v0, Lcom/google/obf/bc;->ac:I

    invoke-virtual {p0, v0}, Lcom/google/obf/bc$a;->d(I)Lcom/google/obf/bc$b;

    move-result-object v0

    .line 196
    if-eqz v0, :cond_4

    .line 197
    iget-object v1, v2, Lcom/google/obf/bg$a;->c:Lcom/google/obf/bk;

    iget-object v1, v1, Lcom/google/obf/bk;->m:[Lcom/google/obf/bl;

    iget-object v2, v3, Lcom/google/obf/bm;->a:Lcom/google/obf/be;

    iget v2, v2, Lcom/google/obf/be;->a:I

    aget-object v1, v1, v2

    .line 198
    iget-object v0, v0, Lcom/google/obf/bc$b;->aP:Lcom/google/obf/dw;

    invoke-static {v1, v0, v3}, Lcom/google/obf/bg;->a(Lcom/google/obf/bl;Lcom/google/obf/dw;Lcom/google/obf/bm;)V

    .line 199
    :cond_4
    sget v0, Lcom/google/obf/bc;->ad:I

    invoke-virtual {p0, v0}, Lcom/google/obf/bc$a;->d(I)Lcom/google/obf/bc$b;

    move-result-object v0

    .line 200
    if-eqz v0, :cond_5

    .line 201
    iget-object v0, v0, Lcom/google/obf/bc$b;->aP:Lcom/google/obf/dw;

    invoke-static {v0, v3}, Lcom/google/obf/bg;->a(Lcom/google/obf/dw;Lcom/google/obf/bm;)V

    .line 202
    :cond_5
    sget v0, Lcom/google/obf/bc;->ah:I

    invoke-virtual {p0, v0}, Lcom/google/obf/bc$a;->d(I)Lcom/google/obf/bc$b;

    move-result-object v0

    .line 203
    if-eqz v0, :cond_6

    .line 204
    iget-object v0, v0, Lcom/google/obf/bc$b;->aP:Lcom/google/obf/dw;

    invoke-static {v0, v3}, Lcom/google/obf/bg;->b(Lcom/google/obf/dw;Lcom/google/obf/bm;)V

    .line 205
    :cond_6
    sget v0, Lcom/google/obf/bc;->ae:I

    invoke-virtual {p0, v0}, Lcom/google/obf/bc$a;->d(I)Lcom/google/obf/bc$b;

    move-result-object v0

    .line 206
    sget v1, Lcom/google/obf/bc;->af:I

    invoke-virtual {p0, v1}, Lcom/google/obf/bc$a;->d(I)Lcom/google/obf/bc$b;

    move-result-object v1

    .line 207
    if-eqz v0, :cond_7

    if-eqz v1, :cond_7

    .line 208
    iget-object v0, v0, Lcom/google/obf/bc$b;->aP:Lcom/google/obf/dw;

    iget-object v1, v1, Lcom/google/obf/bc$b;->aP:Lcom/google/obf/dw;

    invoke-static {v0, v1, v3}, Lcom/google/obf/bg;->a(Lcom/google/obf/dw;Lcom/google/obf/dw;Lcom/google/obf/bm;)V

    .line 209
    :cond_7
    iget-object v0, p0, Lcom/google/obf/bc$a;->aQ:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 210
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 211
    iget-object v0, p0, Lcom/google/obf/bc$a;->aQ:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/bc$b;

    .line 212
    iget v4, v0, Lcom/google/obf/bc$b;->aO:I

    sget v5, Lcom/google/obf/bc;->ag:I

    if-ne v4, v5, :cond_8

    .line 213
    iget-object v0, v0, Lcom/google/obf/bc$b;->aP:Lcom/google/obf/dw;

    invoke-static {v0, v3, p3}, Lcom/google/obf/bg;->a(Lcom/google/obf/dw;Lcom/google/obf/bm;[B)V

    .line 214
    :cond_8
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method private static b(Lcom/google/obf/dw;Lcom/google/obf/bm;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/s;
        }
    .end annotation

    .prologue
    .line 331
    const/4 v0, 0x0

    invoke-static {p0, v0, p1}, Lcom/google/obf/bg;->a(Lcom/google/obf/dw;ILcom/google/obf/bm;)V

    .line 332
    return-void
.end method

.method private static b(I)Z
    .locals 1

    .prologue
    .line 544
    sget v0, Lcom/google/obf/bc;->B:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/obf/bc;->D:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/obf/bc;->E:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/obf/bc;->F:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/obf/bc;->G:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/obf/bc;->K:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/obf/bc;->L:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/obf/bc;->M:I

    if-eq p0, v0, :cond_0

    sget v0, Lcom/google/obf/bc;->P:I

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Lcom/google/obf/ak;)Z
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const-wide/32 v10, 0x7fffffff

    const/4 v9, 0x0

    const/16 v8, 0x8

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 45
    iget v0, p0, Lcom/google/obf/bg;->o:I

    if-nez v0, :cond_1

    .line 46
    iget-object v0, p0, Lcom/google/obf/bg;->i:Lcom/google/obf/dw;

    iget-object v0, v0, Lcom/google/obf/dw;->a:[B

    invoke-interface {p1, v0, v1, v8, v2}, Lcom/google/obf/ak;->a([BIIZ)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 93
    :goto_0
    return v0

    .line 48
    :cond_0
    iput v8, p0, Lcom/google/obf/bg;->o:I

    .line 49
    iget-object v0, p0, Lcom/google/obf/bg;->i:Lcom/google/obf/dw;

    invoke-virtual {v0, v1}, Lcom/google/obf/dw;->c(I)V

    .line 50
    iget-object v0, p0, Lcom/google/obf/bg;->i:Lcom/google/obf/dw;

    invoke-virtual {v0}, Lcom/google/obf/dw;->k()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/obf/bg;->n:J

    .line 51
    iget-object v0, p0, Lcom/google/obf/bg;->i:Lcom/google/obf/dw;

    invoke-virtual {v0}, Lcom/google/obf/dw;->m()I

    move-result v0

    iput v0, p0, Lcom/google/obf/bg;->m:I

    .line 52
    :cond_1
    iget-wide v4, p0, Lcom/google/obf/bg;->n:J

    const-wide/16 v6, 0x1

    cmp-long v0, v4, v6

    if-nez v0, :cond_2

    .line 54
    iget-object v0, p0, Lcom/google/obf/bg;->i:Lcom/google/obf/dw;

    iget-object v0, v0, Lcom/google/obf/dw;->a:[B

    invoke-interface {p1, v0, v8, v8}, Lcom/google/obf/ak;->b([BII)V

    .line 55
    iget v0, p0, Lcom/google/obf/bg;->o:I

    add-int/2addr v0, v8

    iput v0, p0, Lcom/google/obf/bg;->o:I

    .line 56
    iget-object v0, p0, Lcom/google/obf/bg;->i:Lcom/google/obf/dw;

    invoke-virtual {v0}, Lcom/google/obf/dw;->u()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/obf/bg;->n:J

    .line 57
    :cond_2
    iget-wide v4, p0, Lcom/google/obf/bg;->n:J

    iget v0, p0, Lcom/google/obf/bg;->o:I

    int-to-long v6, v0

    cmp-long v0, v4, v6

    if-gez v0, :cond_3

    .line 58
    new-instance v0, Lcom/google/obf/s;

    const-string v1, "Atom size less than header length (unsupported)."

    invoke-direct {v0, v1}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v0

    .line 59
    :cond_3
    invoke-interface {p1}, Lcom/google/obf/ak;->c()J

    move-result-wide v4

    iget v0, p0, Lcom/google/obf/bg;->o:I

    int-to-long v6, v0

    sub-long/2addr v4, v6

    .line 60
    iget v0, p0, Lcom/google/obf/bg;->m:I

    sget v3, Lcom/google/obf/bc;->K:I

    if-ne v0, v3, :cond_4

    .line 61
    iget-object v0, p0, Lcom/google/obf/bg;->e:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v6

    move v3, v1

    .line 62
    :goto_1
    if-ge v3, v6, :cond_4

    .line 63
    iget-object v0, p0, Lcom/google/obf/bg;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/bg$a;

    iget-object v0, v0, Lcom/google/obf/bg$a;->a:Lcom/google/obf/bm;

    .line 64
    iput-wide v4, v0, Lcom/google/obf/bm;->c:J

    .line 65
    iput-wide v4, v0, Lcom/google/obf/bm;->b:J

    .line 66
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 67
    :cond_4
    iget v0, p0, Lcom/google/obf/bg;->m:I

    sget v3, Lcom/google/obf/bc;->h:I

    if-ne v0, v3, :cond_6

    .line 68
    iput-object v9, p0, Lcom/google/obf/bg;->r:Lcom/google/obf/bg$a;

    .line 69
    iget-wide v0, p0, Lcom/google/obf/bg;->n:J

    add-long/2addr v0, v4

    iput-wide v0, p0, Lcom/google/obf/bg;->q:J

    .line 70
    iget-boolean v0, p0, Lcom/google/obf/bg;->w:Z

    if-nez v0, :cond_5

    .line 71
    iget-object v0, p0, Lcom/google/obf/bg;->v:Lcom/google/obf/al;

    sget-object v1, Lcom/google/obf/aq;->f:Lcom/google/obf/aq;

    invoke-interface {v0, v1}, Lcom/google/obf/al;->a(Lcom/google/obf/aq;)V

    .line 72
    iput-boolean v2, p0, Lcom/google/obf/bg;->w:Z

    .line 73
    :cond_5
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/obf/bg;->l:I

    move v0, v2

    .line 74
    goto/16 :goto_0

    .line 75
    :cond_6
    iget v0, p0, Lcom/google/obf/bg;->m:I

    invoke-static {v0}, Lcom/google/obf/bg;->b(I)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 76
    invoke-interface {p1}, Lcom/google/obf/ak;->c()J

    move-result-wide v0

    iget-wide v4, p0, Lcom/google/obf/bg;->n:J

    add-long/2addr v0, v4

    const-wide/16 v4, 0x8

    sub-long/2addr v0, v4

    .line 77
    iget-object v3, p0, Lcom/google/obf/bg;->k:Ljava/util/Stack;

    new-instance v4, Lcom/google/obf/bc$a;

    iget v5, p0, Lcom/google/obf/bg;->m:I

    invoke-direct {v4, v5, v0, v1}, Lcom/google/obf/bc$a;-><init>(IJ)V

    invoke-virtual {v3, v4}, Ljava/util/Stack;->add(Ljava/lang/Object;)Z

    .line 78
    iget-wide v4, p0, Lcom/google/obf/bg;->n:J

    iget v3, p0, Lcom/google/obf/bg;->o:I

    int-to-long v6, v3

    cmp-long v3, v4, v6

    if-nez v3, :cond_7

    .line 79
    invoke-direct {p0, v0, v1}, Lcom/google/obf/bg;->a(J)V

    :goto_2
    move v0, v2

    .line 93
    goto/16 :goto_0

    .line 80
    :cond_7
    invoke-direct {p0}, Lcom/google/obf/bg;->a()V

    goto :goto_2

    .line 81
    :cond_8
    iget v0, p0, Lcom/google/obf/bg;->m:I

    invoke-static {v0}, Lcom/google/obf/bg;->a(I)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 82
    iget v0, p0, Lcom/google/obf/bg;->o:I

    if-eq v0, v8, :cond_9

    .line 83
    new-instance v0, Lcom/google/obf/s;

    const-string v1, "Leaf atom defines extended atom size (unsupported)."

    invoke-direct {v0, v1}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v0

    .line 84
    :cond_9
    iget-wide v4, p0, Lcom/google/obf/bg;->n:J

    cmp-long v0, v4, v10

    if-lez v0, :cond_a

    .line 85
    new-instance v0, Lcom/google/obf/s;

    const-string v1, "Leaf atom with length > 2147483647 (unsupported)."

    invoke-direct {v0, v1}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v0

    .line 86
    :cond_a
    new-instance v0, Lcom/google/obf/dw;

    iget-wide v4, p0, Lcom/google/obf/bg;->n:J

    long-to-int v3, v4

    invoke-direct {v0, v3}, Lcom/google/obf/dw;-><init>(I)V

    iput-object v0, p0, Lcom/google/obf/bg;->p:Lcom/google/obf/dw;

    .line 87
    iget-object v0, p0, Lcom/google/obf/bg;->i:Lcom/google/obf/dw;

    iget-object v0, v0, Lcom/google/obf/dw;->a:[B

    iget-object v3, p0, Lcom/google/obf/bg;->p:Lcom/google/obf/dw;

    iget-object v3, v3, Lcom/google/obf/dw;->a:[B

    invoke-static {v0, v1, v3, v1, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 88
    iput v2, p0, Lcom/google/obf/bg;->l:I

    goto :goto_2

    .line 89
    :cond_b
    iget-wide v0, p0, Lcom/google/obf/bg;->n:J

    cmp-long v0, v0, v10

    if-lez v0, :cond_c

    .line 90
    new-instance v0, Lcom/google/obf/s;

    const-string v1, "Skipping atom with length > 2147483647 (unsupported)."

    invoke-direct {v0, v1}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v0

    .line 91
    :cond_c
    iput-object v9, p0, Lcom/google/obf/bg;->p:Lcom/google/obf/dw;

    .line 92
    iput v2, p0, Lcom/google/obf/bg;->l:I

    goto :goto_2
.end method

.method private static c(Lcom/google/obf/dw;)J
    .locals 2

    .prologue
    .line 274
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/obf/dw;->c(I)V

    .line 275
    invoke-virtual {p0}, Lcom/google/obf/dw;->m()I

    move-result v0

    .line 276
    invoke-static {v0}, Lcom/google/obf/bc;->a(I)I

    move-result v0

    .line 277
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/obf/dw;->u()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/obf/dw;->k()J

    move-result-wide v0

    goto :goto_0
.end method

.method private c(Lcom/google/obf/ak;)V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 94
    iget-wide v0, p0, Lcom/google/obf/bg;->n:J

    long-to-int v0, v0

    iget v1, p0, Lcom/google/obf/bg;->o:I

    sub-int/2addr v0, v1

    .line 95
    iget-object v1, p0, Lcom/google/obf/bg;->p:Lcom/google/obf/dw;

    if-eqz v1, :cond_0

    .line 96
    iget-object v1, p0, Lcom/google/obf/bg;->p:Lcom/google/obf/dw;

    iget-object v1, v1, Lcom/google/obf/dw;->a:[B

    const/16 v2, 0x8

    invoke-interface {p1, v1, v2, v0}, Lcom/google/obf/ak;->b([BII)V

    .line 97
    new-instance v0, Lcom/google/obf/bc$b;

    iget v1, p0, Lcom/google/obf/bg;->m:I

    iget-object v2, p0, Lcom/google/obf/bg;->p:Lcom/google/obf/dw;

    invoke-direct {v0, v1, v2}, Lcom/google/obf/bc$b;-><init>(ILcom/google/obf/dw;)V

    invoke-interface {p1}, Lcom/google/obf/ak;->c()J

    move-result-wide v2

    invoke-direct {p0, v0, v2, v3}, Lcom/google/obf/bg;->a(Lcom/google/obf/bc$b;J)V

    .line 99
    :goto_0
    invoke-interface {p1}, Lcom/google/obf/ak;->c()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/google/obf/bg;->a(J)V

    .line 100
    return-void

    .line 98
    :cond_0
    invoke-interface {p1, v0}, Lcom/google/obf/ak;->b(I)V

    goto :goto_0
.end method

.method private c(Lcom/google/obf/bc$a;)V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/s;
        }
    .end annotation

    .prologue
    .line 158
    iget-object v0, p0, Lcom/google/obf/bg;->e:Landroid/util/SparseArray;

    iget v1, p0, Lcom/google/obf/bg;->c:I

    iget-object v2, p0, Lcom/google/obf/bg;->j:[B

    invoke-static {p1, v0, v1, v2}, Lcom/google/obf/bg;->a(Lcom/google/obf/bc$a;Landroid/util/SparseArray;I[B)V

    .line 159
    iget-object v0, p1, Lcom/google/obf/bc$a;->aQ:Ljava/util/List;

    invoke-static {v0}, Lcom/google/obf/bg;->a(Ljava/util/List;)Lcom/google/obf/ab$a;

    move-result-object v0

    .line 160
    if-eqz v0, :cond_0

    .line 161
    iget-object v1, p0, Lcom/google/obf/bg;->v:Lcom/google/obf/al;

    invoke-interface {v1, v0}, Lcom/google/obf/al;->a(Lcom/google/obf/ab;)V

    .line 162
    :cond_0
    return-void
.end method

.method private d(Lcom/google/obf/ak;)V
    .locals 10
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 412
    const/4 v1, 0x0

    .line 413
    const-wide v2, 0x7fffffffffffffffL

    .line 414
    iget-object v0, p0, Lcom/google/obf/bg;->e:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v5

    .line 415
    const/4 v0, 0x0

    move v4, v0

    :goto_0
    if-ge v4, v5, :cond_0

    .line 416
    iget-object v0, p0, Lcom/google/obf/bg;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, v4}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/bg$a;

    iget-object v0, v0, Lcom/google/obf/bg$a;->a:Lcom/google/obf/bm;

    .line 417
    iget-boolean v6, v0, Lcom/google/obf/bm;->m:Z

    if-eqz v6, :cond_3

    iget-wide v6, v0, Lcom/google/obf/bm;->c:J

    cmp-long v6, v6, v2

    if-gez v6, :cond_3

    .line 418
    iget-wide v2, v0, Lcom/google/obf/bm;->c:J

    .line 419
    iget-object v0, p0, Lcom/google/obf/bg;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, v4}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/bg$a;

    move-wide v8, v2

    move-object v2, v0

    move-wide v0, v8

    .line 420
    :goto_1
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move-wide v8, v0

    move-object v1, v2

    move-wide v2, v8

    goto :goto_0

    .line 421
    :cond_0
    if-nez v1, :cond_1

    .line 422
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/obf/bg;->l:I

    .line 429
    :goto_2
    return-void

    .line 424
    :cond_1
    invoke-interface {p1}, Lcom/google/obf/ak;->c()J

    move-result-wide v4

    sub-long/2addr v2, v4

    long-to-int v0, v2

    .line 425
    if-gez v0, :cond_2

    .line 426
    new-instance v0, Lcom/google/obf/s;

    const-string v1, "Offset to encryption data was negative."

    invoke-direct {v0, v1}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v0

    .line 427
    :cond_2
    invoke-interface {p1, v0}, Lcom/google/obf/ak;->b(I)V

    .line 428
    iget-object v0, v1, Lcom/google/obf/bg$a;->a:Lcom/google/obf/bm;

    invoke-virtual {v0, p1}, Lcom/google/obf/bm;->a(Lcom/google/obf/ak;)V

    goto :goto_2

    :cond_3
    move-wide v8, v2

    move-object v2, v1

    move-wide v0, v8

    goto :goto_1
.end method

.method private e(Lcom/google/obf/ak;)Z
    .locals 13
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v11, 0x4

    const/4 v12, 0x3

    const/4 v4, 0x2

    const/4 v8, 0x1

    const/4 v6, 0x0

    .line 430
    iget v0, p0, Lcom/google/obf/bg;->l:I

    if-ne v0, v12, :cond_4

    .line 431
    iget-object v0, p0, Lcom/google/obf/bg;->r:Lcom/google/obf/bg$a;

    if-nez v0, :cond_3

    .line 432
    iget-object v0, p0, Lcom/google/obf/bg;->e:Landroid/util/SparseArray;

    invoke-static {v0}, Lcom/google/obf/bg;->a(Landroid/util/SparseArray;)Lcom/google/obf/bg$a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/obf/bg;->r:Lcom/google/obf/bg$a;

    .line 433
    iget-object v0, p0, Lcom/google/obf/bg;->r:Lcom/google/obf/bg$a;

    if-nez v0, :cond_1

    .line 434
    iget-wide v0, p0, Lcom/google/obf/bg;->q:J

    invoke-interface {p1}, Lcom/google/obf/ak;->c()J

    move-result-wide v2

    sub-long/2addr v0, v2

    long-to-int v0, v0

    .line 435
    if-gez v0, :cond_0

    .line 436
    new-instance v0, Lcom/google/obf/s;

    const-string v1, "Offset to end of mdat was negative."

    invoke-direct {v0, v1}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v0

    .line 437
    :cond_0
    invoke-interface {p1, v0}, Lcom/google/obf/ak;->b(I)V

    .line 438
    invoke-direct {p0}, Lcom/google/obf/bg;->a()V

    .line 496
    :goto_0
    return v6

    .line 440
    :cond_1
    iget-object v0, p0, Lcom/google/obf/bg;->r:Lcom/google/obf/bg$a;

    iget-object v0, v0, Lcom/google/obf/bg$a;->a:Lcom/google/obf/bm;

    iget-wide v0, v0, Lcom/google/obf/bm;->b:J

    .line 441
    invoke-interface {p1}, Lcom/google/obf/ak;->c()J

    move-result-wide v2

    sub-long/2addr v0, v2

    long-to-int v0, v0

    .line 442
    if-gez v0, :cond_2

    .line 443
    const-string v0, "FragmentedMp4Extractor"

    const-string v1, "Ignoring negative offset to sample data."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v6

    .line 445
    :cond_2
    invoke-interface {p1, v0}, Lcom/google/obf/ak;->b(I)V

    .line 446
    :cond_3
    iget-object v0, p0, Lcom/google/obf/bg;->r:Lcom/google/obf/bg$a;

    iget-object v0, v0, Lcom/google/obf/bg$a;->a:Lcom/google/obf/bm;

    iget-object v0, v0, Lcom/google/obf/bm;->e:[I

    iget-object v1, p0, Lcom/google/obf/bg;->r:Lcom/google/obf/bg$a;

    iget v1, v1, Lcom/google/obf/bg$a;->e:I

    aget v0, v0, v1

    iput v0, p0, Lcom/google/obf/bg;->s:I

    .line 447
    iget-object v0, p0, Lcom/google/obf/bg;->r:Lcom/google/obf/bg$a;

    iget-object v0, v0, Lcom/google/obf/bg$a;->a:Lcom/google/obf/bm;

    iget-boolean v0, v0, Lcom/google/obf/bm;->i:Z

    if-eqz v0, :cond_5

    .line 448
    iget-object v0, p0, Lcom/google/obf/bg;->r:Lcom/google/obf/bg$a;

    invoke-direct {p0, v0}, Lcom/google/obf/bg;->a(Lcom/google/obf/bg$a;)I

    move-result v0

    iput v0, p0, Lcom/google/obf/bg;->t:I

    .line 449
    iget v0, p0, Lcom/google/obf/bg;->s:I

    iget v1, p0, Lcom/google/obf/bg;->t:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/obf/bg;->s:I

    .line 451
    :goto_1
    iput v11, p0, Lcom/google/obf/bg;->l:I

    .line 452
    iput v6, p0, Lcom/google/obf/bg;->u:I

    .line 453
    :cond_4
    iget-object v0, p0, Lcom/google/obf/bg;->r:Lcom/google/obf/bg$a;

    iget-object v9, v0, Lcom/google/obf/bg$a;->a:Lcom/google/obf/bm;

    .line 454
    iget-object v0, p0, Lcom/google/obf/bg;->r:Lcom/google/obf/bg$a;

    iget-object v5, v0, Lcom/google/obf/bg$a;->c:Lcom/google/obf/bk;

    .line 455
    iget-object v0, p0, Lcom/google/obf/bg;->r:Lcom/google/obf/bg$a;

    iget-object v1, v0, Lcom/google/obf/bg$a;->b:Lcom/google/obf/ar;

    .line 456
    iget-object v0, p0, Lcom/google/obf/bg;->r:Lcom/google/obf/bg$a;

    iget v7, v0, Lcom/google/obf/bg$a;->e:I

    .line 457
    iget v0, v5, Lcom/google/obf/bk;->p:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_7

    .line 458
    iget-object v0, p0, Lcom/google/obf/bg;->g:Lcom/google/obf/dw;

    iget-object v0, v0, Lcom/google/obf/dw;->a:[B

    .line 459
    aput-byte v6, v0, v6

    .line 460
    aput-byte v6, v0, v8

    .line 461
    aput-byte v6, v0, v4

    .line 462
    iget v0, v5, Lcom/google/obf/bk;->p:I

    .line 463
    iget v2, v5, Lcom/google/obf/bk;->p:I

    rsub-int/lit8 v2, v2, 0x4

    .line 464
    :goto_2
    iget v3, p0, Lcom/google/obf/bg;->t:I

    iget v10, p0, Lcom/google/obf/bg;->s:I

    if-ge v3, v10, :cond_8

    .line 465
    iget v3, p0, Lcom/google/obf/bg;->u:I

    if-nez v3, :cond_6

    .line 466
    iget-object v3, p0, Lcom/google/obf/bg;->g:Lcom/google/obf/dw;

    iget-object v3, v3, Lcom/google/obf/dw;->a:[B

    invoke-interface {p1, v3, v2, v0}, Lcom/google/obf/ak;->b([BII)V

    .line 467
    iget-object v3, p0, Lcom/google/obf/bg;->g:Lcom/google/obf/dw;

    invoke-virtual {v3, v6}, Lcom/google/obf/dw;->c(I)V

    .line 468
    iget-object v3, p0, Lcom/google/obf/bg;->g:Lcom/google/obf/dw;

    invoke-virtual {v3}, Lcom/google/obf/dw;->s()I

    move-result v3

    iput v3, p0, Lcom/google/obf/bg;->u:I

    .line 469
    iget-object v3, p0, Lcom/google/obf/bg;->f:Lcom/google/obf/dw;

    invoke-virtual {v3, v6}, Lcom/google/obf/dw;->c(I)V

    .line 470
    iget-object v3, p0, Lcom/google/obf/bg;->f:Lcom/google/obf/dw;

    invoke-interface {v1, v3, v11}, Lcom/google/obf/ar;->a(Lcom/google/obf/dw;I)V

    .line 471
    iget v3, p0, Lcom/google/obf/bg;->t:I

    add-int/lit8 v3, v3, 0x4

    iput v3, p0, Lcom/google/obf/bg;->t:I

    .line 472
    iget v3, p0, Lcom/google/obf/bg;->s:I

    add-int/2addr v3, v2

    iput v3, p0, Lcom/google/obf/bg;->s:I

    goto :goto_2

    .line 450
    :cond_5
    iput v6, p0, Lcom/google/obf/bg;->t:I

    goto :goto_1

    .line 473
    :cond_6
    iget v3, p0, Lcom/google/obf/bg;->u:I

    invoke-interface {v1, p1, v3, v6}, Lcom/google/obf/ar;->a(Lcom/google/obf/ak;IZ)I

    move-result v3

    .line 474
    iget v10, p0, Lcom/google/obf/bg;->t:I

    add-int/2addr v10, v3

    iput v10, p0, Lcom/google/obf/bg;->t:I

    .line 475
    iget v10, p0, Lcom/google/obf/bg;->u:I

    sub-int v3, v10, v3

    iput v3, p0, Lcom/google/obf/bg;->u:I

    goto :goto_2

    .line 478
    :cond_7
    :goto_3
    iget v0, p0, Lcom/google/obf/bg;->t:I

    iget v2, p0, Lcom/google/obf/bg;->s:I

    if-ge v0, v2, :cond_8

    .line 479
    iget v0, p0, Lcom/google/obf/bg;->s:I

    iget v2, p0, Lcom/google/obf/bg;->t:I

    sub-int/2addr v0, v2

    invoke-interface {v1, p1, v0, v6}, Lcom/google/obf/ar;->a(Lcom/google/obf/ak;IZ)I

    move-result v0

    .line 480
    iget v2, p0, Lcom/google/obf/bg;->t:I

    add-int/2addr v0, v2

    iput v0, p0, Lcom/google/obf/bg;->t:I

    goto :goto_3

    .line 482
    :cond_8
    invoke-virtual {v9, v7}, Lcom/google/obf/bm;->c(I)J

    move-result-wide v2

    const-wide/16 v10, 0x3e8

    mul-long/2addr v2, v10

    .line 483
    iget-boolean v0, v9, Lcom/google/obf/bm;->i:Z

    if-eqz v0, :cond_b

    move v0, v4

    .line 484
    :goto_4
    iget-object v4, v9, Lcom/google/obf/bm;->h:[Z

    aget-boolean v4, v4, v7

    if-eqz v4, :cond_c

    move v4, v8

    :goto_5
    or-int/2addr v4, v0

    .line 485
    iget-object v0, v9, Lcom/google/obf/bm;->a:Lcom/google/obf/be;

    iget v0, v0, Lcom/google/obf/be;->a:I

    .line 486
    const/4 v7, 0x0

    .line 487
    iget-boolean v10, v9, Lcom/google/obf/bm;->i:Z

    if-eqz v10, :cond_9

    .line 488
    iget-object v7, v9, Lcom/google/obf/bm;->n:Lcom/google/obf/bl;

    if-eqz v7, :cond_d

    .line 489
    iget-object v0, v9, Lcom/google/obf/bm;->n:Lcom/google/obf/bl;

    iget-object v0, v0, Lcom/google/obf/bl;->c:[B

    :goto_6
    move-object v7, v0

    .line 491
    :cond_9
    iget v5, p0, Lcom/google/obf/bg;->s:I

    invoke-interface/range {v1 .. v7}, Lcom/google/obf/ar;->a(JIII[B)V

    .line 492
    iget-object v0, p0, Lcom/google/obf/bg;->r:Lcom/google/obf/bg$a;

    iget v1, v0, Lcom/google/obf/bg$a;->e:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/obf/bg$a;->e:I

    .line 493
    iget-object v0, p0, Lcom/google/obf/bg;->r:Lcom/google/obf/bg$a;

    iget v0, v0, Lcom/google/obf/bg$a;->e:I

    iget v1, v9, Lcom/google/obf/bm;->d:I

    if-ne v0, v1, :cond_a

    .line 494
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/obf/bg;->r:Lcom/google/obf/bg$a;

    .line 495
    :cond_a
    iput v12, p0, Lcom/google/obf/bg;->l:I

    move v6, v8

    .line 496
    goto/16 :goto_0

    :cond_b
    move v0, v6

    .line 483
    goto :goto_4

    :cond_c
    move v4, v6

    .line 484
    goto :goto_5

    .line 490
    :cond_d
    iget-object v5, v5, Lcom/google/obf/bk;->m:[Lcom/google/obf/bl;

    aget-object v0, v5, v0

    iget-object v0, v0, Lcom/google/obf/bl;->c:[B

    goto :goto_6
.end method


# virtual methods
.method public final a(Lcom/google/obf/ak;Lcom/google/obf/ao;)I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 33
    :cond_0
    :goto_0
    iget v0, p0, Lcom/google/obf/bg;->l:I

    packed-switch v0, :pswitch_data_0

    .line 40
    invoke-direct {p0, p1}, Lcom/google/obf/bg;->e(Lcom/google/obf/ak;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 41
    const/4 v0, 0x0

    :goto_1
    return v0

    .line 34
    :pswitch_0
    invoke-direct {p0, p1}, Lcom/google/obf/bg;->b(Lcom/google/obf/ak;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 35
    const/4 v0, -0x1

    goto :goto_1

    .line 36
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/google/obf/bg;->c(Lcom/google/obf/ak;)V

    goto :goto_0

    .line 38
    :pswitch_2
    invoke-direct {p0, p1}, Lcom/google/obf/bg;->d(Lcom/google/obf/ak;)V

    goto :goto_0

    .line 33
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Lcom/google/obf/al;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 18
    iput-object p1, p0, Lcom/google/obf/bg;->v:Lcom/google/obf/al;

    .line 19
    iget-object v0, p0, Lcom/google/obf/bg;->d:Lcom/google/obf/bk;

    if-eqz v0, :cond_0

    .line 20
    new-instance v0, Lcom/google/obf/bg$a;

    invoke-interface {p1, v3}, Lcom/google/obf/al;->d(I)Lcom/google/obf/ar;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/obf/bg$a;-><init>(Lcom/google/obf/ar;)V

    .line 21
    iget-object v1, p0, Lcom/google/obf/bg;->d:Lcom/google/obf/bk;

    new-instance v2, Lcom/google/obf/be;

    invoke-direct {v2, v3, v3, v3, v3}, Lcom/google/obf/be;-><init>(IIII)V

    invoke-virtual {v0, v1, v2}, Lcom/google/obf/bg$a;->a(Lcom/google/obf/bk;Lcom/google/obf/be;)V

    .line 22
    iget-object v1, p0, Lcom/google/obf/bg;->e:Landroid/util/SparseArray;

    invoke-virtual {v1, v3, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 23
    iget-object v0, p0, Lcom/google/obf/bg;->v:Lcom/google/obf/al;

    invoke-interface {v0}, Lcom/google/obf/al;->f()V

    .line 24
    :cond_0
    return-void
.end method

.method protected a(Lcom/google/obf/dw;J)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/s;
        }
    .end annotation

    .prologue
    .line 376
    return-void
.end method

.method public final a(Lcom/google/obf/ak;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 17
    invoke-static {p1}, Lcom/google/obf/bj;->a(Lcom/google/obf/ak;)Z

    move-result v0

    return v0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/obf/bg;->e:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v2

    .line 26
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 27
    iget-object v0, p0, Lcom/google/obf/bg;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/bg$a;

    invoke-virtual {v0}, Lcom/google/obf/bg$a;->a()V

    .line 28
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 29
    :cond_0
    iget-object v0, p0, Lcom/google/obf/bg;->k:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->clear()V

    .line 30
    invoke-direct {p0}, Lcom/google/obf/bg;->a()V

    .line 31
    return-void
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 32
    return-void
.end method
