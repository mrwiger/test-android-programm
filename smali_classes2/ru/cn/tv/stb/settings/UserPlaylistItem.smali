.class public Lru/cn/tv/stb/settings/UserPlaylistItem;
.super Ljava/lang/Object;
.source "UserPlaylistItem.java"


# instance fields
.field public final location:Ljava/lang/String;

.field public final playlistId:I

.field public final title:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "playlistId"    # I
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "location"    # Ljava/lang/String;

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    iput p1, p0, Lru/cn/tv/stb/settings/UserPlaylistItem;->playlistId:I

    .line 11
    iput-object p2, p0, Lru/cn/tv/stb/settings/UserPlaylistItem;->title:Ljava/lang/String;

    .line 12
    iput-object p3, p0, Lru/cn/tv/stb/settings/UserPlaylistItem;->location:Ljava/lang/String;

    .line 13
    return-void
.end method
