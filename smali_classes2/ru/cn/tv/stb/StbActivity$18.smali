.class Lru/cn/tv/stb/StbActivity$18;
.super Ljava/lang/Object;
.source "StbActivity.java"

# interfaces
.implements Lru/cn/tv/player/controller/StbPlayerSettingsPanel$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/tv/stb/StbActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/tv/stb/StbActivity;


# direct methods
.method constructor <init>(Lru/cn/tv/stb/StbActivity;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 1364
    iput-object p1, p0, Lru/cn/tv/stb/StbActivity$18;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public audioTracks()V
    .locals 2

    .prologue
    .line 1383
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$18;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$3500(Lru/cn/tv/stb/StbActivity;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_1

    .line 1384
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$18;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$2200(Lru/cn/tv/stb/StbActivity;)Lru/cn/player/ITrackSelector;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$18;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$2200(Lru/cn/tv/stb/StbActivity;)Lru/cn/player/ITrackSelector;

    move-result-object v0

    invoke-interface {v0}, Lru/cn/player/ITrackSelector;->containsTracks()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1385
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$18;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$2100(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/player/controller/TracksAdapter;

    move-result-object v0

    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$18;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$2200(Lru/cn/tv/stb/StbActivity;)Lru/cn/player/ITrackSelector;

    move-result-object v1

    invoke-interface {v1}, Lru/cn/player/ITrackSelector;->getCurrentTrackIndex()I

    move-result v1

    invoke-virtual {v0, v1}, Lru/cn/tv/player/controller/TracksAdapter;->setSelectedPosition(I)V

    .line 1386
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$18;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$3600(Lru/cn/tv/stb/StbActivity;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$18;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$2100(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/player/controller/TracksAdapter;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1387
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$18;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$3600(Lru/cn/tv/stb/StbActivity;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$18;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$2200(Lru/cn/tv/stb/StbActivity;)Lru/cn/player/ITrackSelector;

    move-result-object v1

    invoke-interface {v1}, Lru/cn/player/ITrackSelector;->getCurrentTrackIndex()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    .line 1388
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$18;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$3500(Lru/cn/tv/stb/StbActivity;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 1389
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$18;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$2600(Lru/cn/tv/stb/StbActivity;)V

    .line 1394
    :cond_0
    :goto_0
    return-void

    .line 1392
    :cond_1
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$18;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$2500(Lru/cn/tv/stb/StbActivity;)V

    goto :goto_0
.end method

.method public fitModeChanged(Lru/cn/player/SimplePlayer$FitMode;)V
    .locals 1
    .param p1, "mode"    # Lru/cn/player/SimplePlayer$FitMode;

    .prologue
    .line 1368
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$18;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$000(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/player/SimplePlayerFragment;

    move-result-object v0

    invoke-virtual {v0, p1}, Lru/cn/tv/player/SimplePlayerFragment;->setFitMode(Lru/cn/player/SimplePlayer$FitMode;)Z

    .line 1369
    return-void
.end method

.method public subtitle()V
    .locals 2

    .prologue
    .line 1398
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$18;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$3500(Lru/cn/tv/stb/StbActivity;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_1

    .line 1399
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$18;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$2400(Lru/cn/tv/stb/StbActivity;)Lru/cn/player/ITrackSelector;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$18;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$2400(Lru/cn/tv/stb/StbActivity;)Lru/cn/player/ITrackSelector;

    move-result-object v0

    invoke-interface {v0}, Lru/cn/player/ITrackSelector;->containsTracks()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1400
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$18;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$2300(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/player/controller/TracksAdapter;

    move-result-object v0

    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$18;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$2400(Lru/cn/tv/stb/StbActivity;)Lru/cn/player/ITrackSelector;

    move-result-object v1

    invoke-interface {v1}, Lru/cn/player/ITrackSelector;->getCurrentTrackIndex()I

    move-result v1

    invoke-virtual {v0, v1}, Lru/cn/tv/player/controller/TracksAdapter;->setSelectedPosition(I)V

    .line 1401
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$18;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$3600(Lru/cn/tv/stb/StbActivity;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$18;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$2300(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/player/controller/TracksAdapter;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1402
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$18;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$3600(Lru/cn/tv/stb/StbActivity;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$18;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$2400(Lru/cn/tv/stb/StbActivity;)Lru/cn/player/ITrackSelector;

    move-result-object v1

    invoke-interface {v1}, Lru/cn/player/ITrackSelector;->getCurrentTrackIndex()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    .line 1403
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$18;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$3500(Lru/cn/tv/stb/StbActivity;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 1404
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$18;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$2600(Lru/cn/tv/stb/StbActivity;)V

    .line 1409
    :cond_0
    :goto_0
    return-void

    .line 1407
    :cond_1
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$18;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$2500(Lru/cn/tv/stb/StbActivity;)V

    goto :goto_0
.end method

.method public volume()V
    .locals 1

    .prologue
    .line 1413
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$18;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$100(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/player/controller/StbPlayerController;

    move-result-object v0

    invoke-virtual {v0}, Lru/cn/tv/player/controller/StbPlayerController;->hide()V

    .line 1414
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$18;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$2000(Lru/cn/tv/stb/StbActivity;)V

    .line 1415
    return-void
.end method

.method public zoomIn()V
    .locals 1

    .prologue
    .line 1373
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$18;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$000(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/player/SimplePlayerFragment;

    move-result-object v0

    invoke-virtual {v0}, Lru/cn/tv/player/SimplePlayerFragment;->zoomIn()V

    .line 1374
    return-void
.end method

.method public zoomOut()V
    .locals 1

    .prologue
    .line 1378
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$18;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$000(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/player/SimplePlayerFragment;

    move-result-object v0

    invoke-virtual {v0}, Lru/cn/tv/player/SimplePlayerFragment;->zoomOut()V

    .line 1379
    return-void
.end method
