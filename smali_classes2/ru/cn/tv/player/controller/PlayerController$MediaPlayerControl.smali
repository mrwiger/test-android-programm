.class public interface abstract Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;
.super Ljava/lang/Object;
.source "PlayerController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/tv/player/controller/PlayerController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "MediaPlayerControl"
.end annotation


# virtual methods
.method public abstract getBufferPosition()J
.end method

.method public abstract getCurrentPosition()I
.end method

.method public abstract getDuration()I
.end method

.method public abstract getLivePosition()I
.end method

.method public abstract isPlaying()Z
.end method

.method public abstract isSeekable()Z
.end method

.method public abstract pause()V
.end method

.method public abstract play()V
.end method

.method public abstract seekTo(I)V
.end method

.method public abstract selectMedia(JZ)V
.end method
