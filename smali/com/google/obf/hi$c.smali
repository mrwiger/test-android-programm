.class public final enum Lcom/google/obf/hi$c;
.super Ljava/lang/Enum;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/obf/hi;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "c"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/obf/hi$c;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic a:[Lcom/google/obf/hi$c;

.field public static final enum adBreakEnded:Lcom/google/obf/hi$c;

.field public static final enum adBreakReady:Lcom/google/obf/hi$c;

.field public static final enum adBreakStarted:Lcom/google/obf/hi$c;

.field public static final enum adMetadata:Lcom/google/obf/hi$c;

.field public static final enum adProgress:Lcom/google/obf/hi$c;

.field public static final enum adRemainingTime:Lcom/google/obf/hi$c;

.field public static final enum adsLoaded:Lcom/google/obf/hi$c;

.field public static final enum allAdsCompleted:Lcom/google/obf/hi$c;

.field public static final enum appStateChanged:Lcom/google/obf/hi$c;

.field public static final enum click:Lcom/google/obf/hi$c;

.field public static final enum companionView:Lcom/google/obf/hi$c;

.field public static final enum complete:Lcom/google/obf/hi$c;

.field public static final enum contentComplete:Lcom/google/obf/hi$c;

.field public static final enum contentPauseRequested:Lcom/google/obf/hi$c;

.field public static final enum contentResumeRequested:Lcom/google/obf/hi$c;

.field public static final enum contentTimeUpdate:Lcom/google/obf/hi$c;

.field public static final enum csi:Lcom/google/obf/hi$c;

.field public static final enum cuepointsChanged:Lcom/google/obf/hi$c;

.field public static final enum destroy:Lcom/google/obf/hi$c;

.field public static final enum discardAdBreak:Lcom/google/obf/hi$c;

.field public static final enum displayCompanions:Lcom/google/obf/hi$c;

.field public static final enum end:Lcom/google/obf/hi$c;

.field public static final enum error:Lcom/google/obf/hi$c;

.field public static final enum firstquartile:Lcom/google/obf/hi$c;

.field public static final enum fullscreen:Lcom/google/obf/hi$c;

.field public static final enum getViewability:Lcom/google/obf/hi$c;

.field public static final enum hide:Lcom/google/obf/hi$c;

.field public static final enum impression:Lcom/google/obf/hi$c;

.field public static final enum init:Lcom/google/obf/hi$c;

.field public static final enum initialized:Lcom/google/obf/hi$c;

.field public static final enum learnMore:Lcom/google/obf/hi$c;

.field public static final enum load:Lcom/google/obf/hi$c;

.field public static final enum loadStream:Lcom/google/obf/hi$c;

.field public static final enum loaded:Lcom/google/obf/hi$c;

.field public static final enum log:Lcom/google/obf/hi$c;

.field public static final enum midpoint:Lcom/google/obf/hi$c;

.field public static final enum mute:Lcom/google/obf/hi$c;

.field public static final enum pause:Lcom/google/obf/hi$c;

.field public static final enum play:Lcom/google/obf/hi$c;

.field public static final enum preSkipButton:Lcom/google/obf/hi$c;

.field public static final enum reportVastEvent:Lcom/google/obf/hi$c;

.field public static final enum requestAds:Lcom/google/obf/hi$c;

.field public static final enum requestNextAdBreak:Lcom/google/obf/hi$c;

.field public static final enum requestStream:Lcom/google/obf/hi$c;

.field public static final enum resume:Lcom/google/obf/hi$c;

.field public static final enum setPlaybackOptions:Lcom/google/obf/hi$c;

.field public static final enum showVideo:Lcom/google/obf/hi$c;

.field public static final enum skip:Lcom/google/obf/hi$c;

.field public static final enum skipButton:Lcom/google/obf/hi$c;

.field public static final enum skipShown:Lcom/google/obf/hi$c;

.field public static final enum skippableStateChanged:Lcom/google/obf/hi$c;

.field public static final enum start:Lcom/google/obf/hi$c;

.field public static final enum startTracking:Lcom/google/obf/hi$c;

.field public static final enum stop:Lcom/google/obf/hi$c;

.field public static final enum stopTracking:Lcom/google/obf/hi$c;

.field public static final enum streamInitialized:Lcom/google/obf/hi$c;

.field public static final enum thirdquartile:Lcom/google/obf/hi$c;

.field public static final enum timedMetadata:Lcom/google/obf/hi$c;

.field public static final enum timeupdate:Lcom/google/obf/hi$c;

.field public static final enum unmute:Lcom/google/obf/hi$c;

.field public static final enum videoClicked:Lcom/google/obf/hi$c;

.field public static final enum videoIconClicked:Lcom/google/obf/hi$c;

.field public static final enum viewability:Lcom/google/obf/hi$c;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 4
    new-instance v0, Lcom/google/obf/hi$c;

    const-string v1, "adBreakEnded"

    invoke-direct {v0, v1, v3}, Lcom/google/obf/hi$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hi$c;->adBreakEnded:Lcom/google/obf/hi$c;

    .line 5
    new-instance v0, Lcom/google/obf/hi$c;

    const-string v1, "adBreakReady"

    invoke-direct {v0, v1, v4}, Lcom/google/obf/hi$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hi$c;->adBreakReady:Lcom/google/obf/hi$c;

    .line 6
    new-instance v0, Lcom/google/obf/hi$c;

    const-string v1, "adBreakStarted"

    invoke-direct {v0, v1, v5}, Lcom/google/obf/hi$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hi$c;->adBreakStarted:Lcom/google/obf/hi$c;

    .line 7
    new-instance v0, Lcom/google/obf/hi$c;

    const-string v1, "adMetadata"

    invoke-direct {v0, v1, v6}, Lcom/google/obf/hi$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hi$c;->adMetadata:Lcom/google/obf/hi$c;

    .line 8
    new-instance v0, Lcom/google/obf/hi$c;

    const-string v1, "adProgress"

    invoke-direct {v0, v1, v7}, Lcom/google/obf/hi$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hi$c;->adProgress:Lcom/google/obf/hi$c;

    .line 9
    new-instance v0, Lcom/google/obf/hi$c;

    const-string v1, "adsLoaded"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/obf/hi$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hi$c;->adsLoaded:Lcom/google/obf/hi$c;

    .line 10
    new-instance v0, Lcom/google/obf/hi$c;

    const-string v1, "allAdsCompleted"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/obf/hi$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hi$c;->allAdsCompleted:Lcom/google/obf/hi$c;

    .line 11
    new-instance v0, Lcom/google/obf/hi$c;

    const-string v1, "appStateChanged"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/google/obf/hi$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hi$c;->appStateChanged:Lcom/google/obf/hi$c;

    .line 12
    new-instance v0, Lcom/google/obf/hi$c;

    const-string v1, "click"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/google/obf/hi$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hi$c;->click:Lcom/google/obf/hi$c;

    .line 13
    new-instance v0, Lcom/google/obf/hi$c;

    const-string v1, "complete"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/google/obf/hi$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hi$c;->complete:Lcom/google/obf/hi$c;

    .line 14
    new-instance v0, Lcom/google/obf/hi$c;

    const-string v1, "companionView"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/google/obf/hi$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hi$c;->companionView:Lcom/google/obf/hi$c;

    .line 15
    new-instance v0, Lcom/google/obf/hi$c;

    const-string v1, "contentComplete"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/google/obf/hi$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hi$c;->contentComplete:Lcom/google/obf/hi$c;

    .line 16
    new-instance v0, Lcom/google/obf/hi$c;

    const-string v1, "contentPauseRequested"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/google/obf/hi$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hi$c;->contentPauseRequested:Lcom/google/obf/hi$c;

    .line 17
    new-instance v0, Lcom/google/obf/hi$c;

    const-string v1, "contentResumeRequested"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/google/obf/hi$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hi$c;->contentResumeRequested:Lcom/google/obf/hi$c;

    .line 18
    new-instance v0, Lcom/google/obf/hi$c;

    const-string v1, "contentTimeUpdate"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/google/obf/hi$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hi$c;->contentTimeUpdate:Lcom/google/obf/hi$c;

    .line 19
    new-instance v0, Lcom/google/obf/hi$c;

    const-string v1, "csi"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/google/obf/hi$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hi$c;->csi:Lcom/google/obf/hi$c;

    .line 20
    new-instance v0, Lcom/google/obf/hi$c;

    const-string v1, "cuepointsChanged"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/google/obf/hi$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hi$c;->cuepointsChanged:Lcom/google/obf/hi$c;

    .line 21
    new-instance v0, Lcom/google/obf/hi$c;

    const-string v1, "discardAdBreak"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/google/obf/hi$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hi$c;->discardAdBreak:Lcom/google/obf/hi$c;

    .line 22
    new-instance v0, Lcom/google/obf/hi$c;

    const-string v1, "displayCompanions"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/google/obf/hi$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hi$c;->displayCompanions:Lcom/google/obf/hi$c;

    .line 23
    new-instance v0, Lcom/google/obf/hi$c;

    const-string v1, "destroy"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/google/obf/hi$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hi$c;->destroy:Lcom/google/obf/hi$c;

    .line 24
    new-instance v0, Lcom/google/obf/hi$c;

    const-string v1, "end"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/google/obf/hi$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hi$c;->end:Lcom/google/obf/hi$c;

    .line 25
    new-instance v0, Lcom/google/obf/hi$c;

    const-string v1, "error"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/google/obf/hi$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hi$c;->error:Lcom/google/obf/hi$c;

    .line 26
    new-instance v0, Lcom/google/obf/hi$c;

    const-string v1, "firstquartile"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/google/obf/hi$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hi$c;->firstquartile:Lcom/google/obf/hi$c;

    .line 27
    new-instance v0, Lcom/google/obf/hi$c;

    const-string v1, "fullscreen"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/google/obf/hi$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hi$c;->fullscreen:Lcom/google/obf/hi$c;

    .line 28
    new-instance v0, Lcom/google/obf/hi$c;

    const-string v1, "getViewability"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/google/obf/hi$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hi$c;->getViewability:Lcom/google/obf/hi$c;

    .line 29
    new-instance v0, Lcom/google/obf/hi$c;

    const-string v1, "hide"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lcom/google/obf/hi$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hi$c;->hide:Lcom/google/obf/hi$c;

    .line 30
    new-instance v0, Lcom/google/obf/hi$c;

    const-string v1, "impression"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lcom/google/obf/hi$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hi$c;->impression:Lcom/google/obf/hi$c;

    .line 31
    new-instance v0, Lcom/google/obf/hi$c;

    const-string v1, "init"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lcom/google/obf/hi$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hi$c;->init:Lcom/google/obf/hi$c;

    .line 32
    new-instance v0, Lcom/google/obf/hi$c;

    const-string v1, "initialized"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, Lcom/google/obf/hi$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hi$c;->initialized:Lcom/google/obf/hi$c;

    .line 33
    new-instance v0, Lcom/google/obf/hi$c;

    const-string v1, "load"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, Lcom/google/obf/hi$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hi$c;->load:Lcom/google/obf/hi$c;

    .line 34
    new-instance v0, Lcom/google/obf/hi$c;

    const-string v1, "loaded"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, Lcom/google/obf/hi$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hi$c;->loaded:Lcom/google/obf/hi$c;

    .line 35
    new-instance v0, Lcom/google/obf/hi$c;

    const-string v1, "loadStream"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Lcom/google/obf/hi$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hi$c;->loadStream:Lcom/google/obf/hi$c;

    .line 36
    new-instance v0, Lcom/google/obf/hi$c;

    const-string v1, "log"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, Lcom/google/obf/hi$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hi$c;->log:Lcom/google/obf/hi$c;

    .line 37
    new-instance v0, Lcom/google/obf/hi$c;

    const-string v1, "midpoint"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, Lcom/google/obf/hi$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hi$c;->midpoint:Lcom/google/obf/hi$c;

    .line 38
    new-instance v0, Lcom/google/obf/hi$c;

    const-string v1, "mute"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, Lcom/google/obf/hi$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hi$c;->mute:Lcom/google/obf/hi$c;

    .line 39
    new-instance v0, Lcom/google/obf/hi$c;

    const-string v1, "pause"

    const/16 v2, 0x23

    invoke-direct {v0, v1, v2}, Lcom/google/obf/hi$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hi$c;->pause:Lcom/google/obf/hi$c;

    .line 40
    new-instance v0, Lcom/google/obf/hi$c;

    const-string v1, "play"

    const/16 v2, 0x24

    invoke-direct {v0, v1, v2}, Lcom/google/obf/hi$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hi$c;->play:Lcom/google/obf/hi$c;

    .line 41
    new-instance v0, Lcom/google/obf/hi$c;

    const-string v1, "reportVastEvent"

    const/16 v2, 0x25

    invoke-direct {v0, v1, v2}, Lcom/google/obf/hi$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hi$c;->reportVastEvent:Lcom/google/obf/hi$c;

    .line 42
    new-instance v0, Lcom/google/obf/hi$c;

    const-string v1, "resume"

    const/16 v2, 0x26

    invoke-direct {v0, v1, v2}, Lcom/google/obf/hi$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hi$c;->resume:Lcom/google/obf/hi$c;

    .line 43
    new-instance v0, Lcom/google/obf/hi$c;

    const-string v1, "requestAds"

    const/16 v2, 0x27

    invoke-direct {v0, v1, v2}, Lcom/google/obf/hi$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hi$c;->requestAds:Lcom/google/obf/hi$c;

    .line 44
    new-instance v0, Lcom/google/obf/hi$c;

    const-string v1, "requestNextAdBreak"

    const/16 v2, 0x28

    invoke-direct {v0, v1, v2}, Lcom/google/obf/hi$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hi$c;->requestNextAdBreak:Lcom/google/obf/hi$c;

    .line 45
    new-instance v0, Lcom/google/obf/hi$c;

    const-string v1, "requestStream"

    const/16 v2, 0x29

    invoke-direct {v0, v1, v2}, Lcom/google/obf/hi$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hi$c;->requestStream:Lcom/google/obf/hi$c;

    .line 46
    new-instance v0, Lcom/google/obf/hi$c;

    const-string v1, "setPlaybackOptions"

    const/16 v2, 0x2a

    invoke-direct {v0, v1, v2}, Lcom/google/obf/hi$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hi$c;->setPlaybackOptions:Lcom/google/obf/hi$c;

    .line 47
    new-instance v0, Lcom/google/obf/hi$c;

    const-string v1, "showVideo"

    const/16 v2, 0x2b

    invoke-direct {v0, v1, v2}, Lcom/google/obf/hi$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hi$c;->showVideo:Lcom/google/obf/hi$c;

    .line 48
    new-instance v0, Lcom/google/obf/hi$c;

    const-string v1, "skip"

    const/16 v2, 0x2c

    invoke-direct {v0, v1, v2}, Lcom/google/obf/hi$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hi$c;->skip:Lcom/google/obf/hi$c;

    .line 49
    new-instance v0, Lcom/google/obf/hi$c;

    const-string v1, "skippableStateChanged"

    const/16 v2, 0x2d

    invoke-direct {v0, v1, v2}, Lcom/google/obf/hi$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hi$c;->skippableStateChanged:Lcom/google/obf/hi$c;

    .line 50
    new-instance v0, Lcom/google/obf/hi$c;

    const-string v1, "skipShown"

    const/16 v2, 0x2e

    invoke-direct {v0, v1, v2}, Lcom/google/obf/hi$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hi$c;->skipShown:Lcom/google/obf/hi$c;

    .line 51
    new-instance v0, Lcom/google/obf/hi$c;

    const-string v1, "start"

    const/16 v2, 0x2f

    invoke-direct {v0, v1, v2}, Lcom/google/obf/hi$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hi$c;->start:Lcom/google/obf/hi$c;

    .line 52
    new-instance v0, Lcom/google/obf/hi$c;

    const-string v1, "startTracking"

    const/16 v2, 0x30

    invoke-direct {v0, v1, v2}, Lcom/google/obf/hi$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hi$c;->startTracking:Lcom/google/obf/hi$c;

    .line 53
    new-instance v0, Lcom/google/obf/hi$c;

    const-string v1, "stop"

    const/16 v2, 0x31

    invoke-direct {v0, v1, v2}, Lcom/google/obf/hi$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hi$c;->stop:Lcom/google/obf/hi$c;

    .line 54
    new-instance v0, Lcom/google/obf/hi$c;

    const-string v1, "stopTracking"

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lcom/google/obf/hi$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hi$c;->stopTracking:Lcom/google/obf/hi$c;

    .line 55
    new-instance v0, Lcom/google/obf/hi$c;

    const-string v1, "streamInitialized"

    const/16 v2, 0x33

    invoke-direct {v0, v1, v2}, Lcom/google/obf/hi$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hi$c;->streamInitialized:Lcom/google/obf/hi$c;

    .line 56
    new-instance v0, Lcom/google/obf/hi$c;

    const-string v1, "thirdquartile"

    const/16 v2, 0x34

    invoke-direct {v0, v1, v2}, Lcom/google/obf/hi$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hi$c;->thirdquartile:Lcom/google/obf/hi$c;

    .line 57
    new-instance v0, Lcom/google/obf/hi$c;

    const-string v1, "timedMetadata"

    const/16 v2, 0x35

    invoke-direct {v0, v1, v2}, Lcom/google/obf/hi$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hi$c;->timedMetadata:Lcom/google/obf/hi$c;

    .line 58
    new-instance v0, Lcom/google/obf/hi$c;

    const-string v1, "timeupdate"

    const/16 v2, 0x36

    invoke-direct {v0, v1, v2}, Lcom/google/obf/hi$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hi$c;->timeupdate:Lcom/google/obf/hi$c;

    .line 59
    new-instance v0, Lcom/google/obf/hi$c;

    const-string v1, "unmute"

    const/16 v2, 0x37

    invoke-direct {v0, v1, v2}, Lcom/google/obf/hi$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hi$c;->unmute:Lcom/google/obf/hi$c;

    .line 60
    new-instance v0, Lcom/google/obf/hi$c;

    const-string v1, "viewability"

    const/16 v2, 0x38

    invoke-direct {v0, v1, v2}, Lcom/google/obf/hi$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hi$c;->viewability:Lcom/google/obf/hi$c;

    .line 61
    new-instance v0, Lcom/google/obf/hi$c;

    const-string v1, "videoClicked"

    const/16 v2, 0x39

    invoke-direct {v0, v1, v2}, Lcom/google/obf/hi$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hi$c;->videoClicked:Lcom/google/obf/hi$c;

    .line 62
    new-instance v0, Lcom/google/obf/hi$c;

    const-string v1, "videoIconClicked"

    const/16 v2, 0x3a

    invoke-direct {v0, v1, v2}, Lcom/google/obf/hi$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hi$c;->videoIconClicked:Lcom/google/obf/hi$c;

    .line 63
    new-instance v0, Lcom/google/obf/hi$c;

    const-string v1, "adRemainingTime"

    const/16 v2, 0x3b

    invoke-direct {v0, v1, v2}, Lcom/google/obf/hi$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hi$c;->adRemainingTime:Lcom/google/obf/hi$c;

    .line 64
    new-instance v0, Lcom/google/obf/hi$c;

    const-string v1, "learnMore"

    const/16 v2, 0x3c

    invoke-direct {v0, v1, v2}, Lcom/google/obf/hi$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hi$c;->learnMore:Lcom/google/obf/hi$c;

    .line 65
    new-instance v0, Lcom/google/obf/hi$c;

    const-string v1, "preSkipButton"

    const/16 v2, 0x3d

    invoke-direct {v0, v1, v2}, Lcom/google/obf/hi$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hi$c;->preSkipButton:Lcom/google/obf/hi$c;

    .line 66
    new-instance v0, Lcom/google/obf/hi$c;

    const-string v1, "skipButton"

    const/16 v2, 0x3e

    invoke-direct {v0, v1, v2}, Lcom/google/obf/hi$c;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/obf/hi$c;->skipButton:Lcom/google/obf/hi$c;

    .line 67
    const/16 v0, 0x3f

    new-array v0, v0, [Lcom/google/obf/hi$c;

    sget-object v1, Lcom/google/obf/hi$c;->adBreakEnded:Lcom/google/obf/hi$c;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/obf/hi$c;->adBreakReady:Lcom/google/obf/hi$c;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/obf/hi$c;->adBreakStarted:Lcom/google/obf/hi$c;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/obf/hi$c;->adMetadata:Lcom/google/obf/hi$c;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/obf/hi$c;->adProgress:Lcom/google/obf/hi$c;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/obf/hi$c;->adsLoaded:Lcom/google/obf/hi$c;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/obf/hi$c;->allAdsCompleted:Lcom/google/obf/hi$c;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/obf/hi$c;->appStateChanged:Lcom/google/obf/hi$c;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/obf/hi$c;->click:Lcom/google/obf/hi$c;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/obf/hi$c;->complete:Lcom/google/obf/hi$c;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/obf/hi$c;->companionView:Lcom/google/obf/hi$c;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/obf/hi$c;->contentComplete:Lcom/google/obf/hi$c;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/obf/hi$c;->contentPauseRequested:Lcom/google/obf/hi$c;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/obf/hi$c;->contentResumeRequested:Lcom/google/obf/hi$c;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/google/obf/hi$c;->contentTimeUpdate:Lcom/google/obf/hi$c;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/google/obf/hi$c;->csi:Lcom/google/obf/hi$c;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/google/obf/hi$c;->cuepointsChanged:Lcom/google/obf/hi$c;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/google/obf/hi$c;->discardAdBreak:Lcom/google/obf/hi$c;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/google/obf/hi$c;->displayCompanions:Lcom/google/obf/hi$c;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/google/obf/hi$c;->destroy:Lcom/google/obf/hi$c;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/google/obf/hi$c;->end:Lcom/google/obf/hi$c;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/google/obf/hi$c;->error:Lcom/google/obf/hi$c;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/google/obf/hi$c;->firstquartile:Lcom/google/obf/hi$c;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/google/obf/hi$c;->fullscreen:Lcom/google/obf/hi$c;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/google/obf/hi$c;->getViewability:Lcom/google/obf/hi$c;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/google/obf/hi$c;->hide:Lcom/google/obf/hi$c;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/google/obf/hi$c;->impression:Lcom/google/obf/hi$c;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/google/obf/hi$c;->init:Lcom/google/obf/hi$c;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/google/obf/hi$c;->initialized:Lcom/google/obf/hi$c;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/google/obf/hi$c;->load:Lcom/google/obf/hi$c;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/google/obf/hi$c;->loaded:Lcom/google/obf/hi$c;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/google/obf/hi$c;->loadStream:Lcom/google/obf/hi$c;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/google/obf/hi$c;->log:Lcom/google/obf/hi$c;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/google/obf/hi$c;->midpoint:Lcom/google/obf/hi$c;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/google/obf/hi$c;->mute:Lcom/google/obf/hi$c;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/google/obf/hi$c;->pause:Lcom/google/obf/hi$c;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/google/obf/hi$c;->play:Lcom/google/obf/hi$c;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/google/obf/hi$c;->reportVastEvent:Lcom/google/obf/hi$c;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/google/obf/hi$c;->resume:Lcom/google/obf/hi$c;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/google/obf/hi$c;->requestAds:Lcom/google/obf/hi$c;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/google/obf/hi$c;->requestNextAdBreak:Lcom/google/obf/hi$c;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/google/obf/hi$c;->requestStream:Lcom/google/obf/hi$c;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/google/obf/hi$c;->setPlaybackOptions:Lcom/google/obf/hi$c;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/google/obf/hi$c;->showVideo:Lcom/google/obf/hi$c;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/google/obf/hi$c;->skip:Lcom/google/obf/hi$c;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/google/obf/hi$c;->skippableStateChanged:Lcom/google/obf/hi$c;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/google/obf/hi$c;->skipShown:Lcom/google/obf/hi$c;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lcom/google/obf/hi$c;->start:Lcom/google/obf/hi$c;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lcom/google/obf/hi$c;->startTracking:Lcom/google/obf/hi$c;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lcom/google/obf/hi$c;->stop:Lcom/google/obf/hi$c;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Lcom/google/obf/hi$c;->stopTracking:Lcom/google/obf/hi$c;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, Lcom/google/obf/hi$c;->streamInitialized:Lcom/google/obf/hi$c;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, Lcom/google/obf/hi$c;->thirdquartile:Lcom/google/obf/hi$c;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, Lcom/google/obf/hi$c;->timedMetadata:Lcom/google/obf/hi$c;

    aput-object v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, Lcom/google/obf/hi$c;->timeupdate:Lcom/google/obf/hi$c;

    aput-object v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, Lcom/google/obf/hi$c;->unmute:Lcom/google/obf/hi$c;

    aput-object v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, Lcom/google/obf/hi$c;->viewability:Lcom/google/obf/hi$c;

    aput-object v2, v0, v1

    const/16 v1, 0x39

    sget-object v2, Lcom/google/obf/hi$c;->videoClicked:Lcom/google/obf/hi$c;

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    sget-object v2, Lcom/google/obf/hi$c;->videoIconClicked:Lcom/google/obf/hi$c;

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    sget-object v2, Lcom/google/obf/hi$c;->adRemainingTime:Lcom/google/obf/hi$c;

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    sget-object v2, Lcom/google/obf/hi$c;->learnMore:Lcom/google/obf/hi$c;

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    sget-object v2, Lcom/google/obf/hi$c;->preSkipButton:Lcom/google/obf/hi$c;

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    sget-object v2, Lcom/google/obf/hi$c;->skipButton:Lcom/google/obf/hi$c;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/obf/hi$c;->a:[Lcom/google/obf/hi$c;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 3
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/obf/hi$c;
    .locals 1

    .prologue
    .line 2
    const-class v0, Lcom/google/obf/hi$c;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/obf/hi$c;

    return-object v0
.end method

.method public static values()[Lcom/google/obf/hi$c;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lcom/google/obf/hi$c;->a:[Lcom/google/obf/hi$c;

    invoke-virtual {v0}, [Lcom/google/obf/hi$c;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/obf/hi$c;

    return-object v0
.end method
