.class interface abstract Ltoothpick/configuration/ReflectionConfiguration;
.super Ljava/lang/Object;
.source "ReflectionConfiguration.java"


# virtual methods
.method public abstract getFactory(Ljava/lang/Class;)Ltoothpick/Factory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Ltoothpick/Factory",
            "<TT;>;"
        }
    .end annotation
.end method

.method public abstract getMemberInjector(Ljava/lang/Class;)Ltoothpick/MemberInjector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Ltoothpick/MemberInjector",
            "<TT;>;"
        }
    .end annotation
.end method
