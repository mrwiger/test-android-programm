.class final Lru/cn/api/provider/CacheUtils;
.super Ljava/lang/Object;
.source "CacheUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/api/provider/CacheUtils$CacheName;
    }
.end annotation


# static fields
.field private static cacheLiveTimeMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lru/cn/api/provider/CacheUtils$CacheName;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private cacheMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lru/cn/api/provider/CacheUtils$CacheName;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const-wide/32 v4, 0xdbba00

    .line 14
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lru/cn/api/provider/CacheUtils;->cacheLiveTimeMap:Ljava/util/Map;

    .line 18
    sget-object v0, Lru/cn/api/provider/CacheUtils;->cacheLiveTimeMap:Ljava/util/Map;

    sget-object v1, Lru/cn/api/provider/CacheUtils$CacheName;->channels:Lru/cn/api/provider/CacheUtils$CacheName;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 19
    sget-object v0, Lru/cn/api/provider/CacheUtils;->cacheLiveTimeMap:Ljava/util/Map;

    sget-object v1, Lru/cn/api/provider/CacheUtils$CacheName;->user_data:Lru/cn/api/provider/CacheUtils$CacheName;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 20
    sget-object v0, Lru/cn/api/provider/CacheUtils;->cacheLiveTimeMap:Ljava/util/Map;

    sget-object v1, Lru/cn/api/provider/CacheUtils$CacheName;->rubricator:Lru/cn/api/provider/CacheUtils$CacheName;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 21
    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lru/cn/api/provider/CacheUtils;->cacheMap:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method deleteCacheExpireTime(Lru/cn/api/provider/CacheUtils$CacheName;)V
    .locals 1
    .param p1, "name"    # Lru/cn/api/provider/CacheUtils$CacheName;

    .prologue
    .line 37
    iget-object v0, p0, Lru/cn/api/provider/CacheUtils;->cacheMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    return-void
.end method

.method isCacheExpire(Lru/cn/api/provider/CacheUtils$CacheName;)Z
    .locals 6
    .param p1, "name"    # Lru/cn/api/provider/CacheUtils$CacheName;

    .prologue
    .line 26
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 27
    .local v1, "currentTimeMilliseconds":Ljava/lang/Long;
    iget-object v2, p0, Lru/cn/api/provider/CacheUtils;->cacheMap:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 28
    .local v0, "cacheTime":Ljava/lang/Long;
    if-eqz v0, :cond_0

    .line 29
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sget-object v2, Lru/cn/api/provider/CacheUtils;->cacheLiveTimeMap:Ljava/util/Map;

    .line 30
    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    add-long/2addr v2, v4

    .line 29
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Long;->compareTo(Ljava/lang/Long;)I

    move-result v2

    if-lez v2, :cond_1

    .line 31
    :cond_0
    const/4 v2, 0x1

    .line 33
    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method updateCacheExpireTime(Lru/cn/api/provider/CacheUtils$CacheName;)V
    .locals 4
    .param p1, "name"    # Lru/cn/api/provider/CacheUtils$CacheName;

    .prologue
    .line 41
    invoke-static {}, Lru/cn/utils/Utils;->getCalendar()Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 42
    .local v0, "currentTimeMilliseconds":Ljava/lang/Long;
    iget-object v1, p0, Lru/cn/api/provider/CacheUtils;->cacheMap:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    return-void
.end method
