.class Lru/cn/player/AudioFocus$1;
.super Ljava/lang/Object;
.source "AudioFocus.java"

# interfaces
.implements Landroid/media/AudioManager$OnAudioFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/player/AudioFocus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/player/AudioFocus;


# direct methods
.method constructor <init>(Lru/cn/player/AudioFocus;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/player/AudioFocus;

    .prologue
    .line 55
    iput-object p1, p0, Lru/cn/player/AudioFocus$1;->this$0:Lru/cn/player/AudioFocus;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAudioFocusChange(I)V
    .locals 2
    .param p1, "focusChange"    # I

    .prologue
    const/4 v1, 0x0

    .line 58
    packed-switch p1, :pswitch_data_0

    .line 87
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 60
    :pswitch_1
    iget-object v0, p0, Lru/cn/player/AudioFocus$1;->this$0:Lru/cn/player/AudioFocus;

    invoke-static {v0}, Lru/cn/player/AudioFocus;->access$000(Lru/cn/player/AudioFocus;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 61
    iget-object v0, p0, Lru/cn/player/AudioFocus$1;->this$0:Lru/cn/player/AudioFocus;

    invoke-static {v0}, Lru/cn/player/AudioFocus;->access$100(Lru/cn/player/AudioFocus;)Lru/cn/player/AbstractMediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lru/cn/player/AbstractMediaPlayer;->resume()V

    .line 62
    iget-object v0, p0, Lru/cn/player/AudioFocus$1;->this$0:Lru/cn/player/AudioFocus;

    invoke-static {v0, v1}, Lru/cn/player/AudioFocus;->access$002(Lru/cn/player/AudioFocus;Z)Z

    .line 68
    :cond_1
    :pswitch_2
    iget-object v0, p0, Lru/cn/player/AudioFocus$1;->this$0:Lru/cn/player/AudioFocus;

    invoke-static {v0}, Lru/cn/player/AudioFocus;->access$100(Lru/cn/player/AudioFocus;)Lru/cn/player/AbstractMediaPlayer;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lru/cn/player/AbstractMediaPlayer;->setVolume(F)V

    goto :goto_0

    .line 72
    :pswitch_3
    iget-object v0, p0, Lru/cn/player/AudioFocus$1;->this$0:Lru/cn/player/AudioFocus;

    invoke-static {v0, v1}, Lru/cn/player/AudioFocus;->access$202(Lru/cn/player/AudioFocus;Z)Z

    .line 73
    iget-object v0, p0, Lru/cn/player/AudioFocus$1;->this$0:Lru/cn/player/AudioFocus;

    invoke-static {v0}, Lru/cn/player/AudioFocus;->access$100(Lru/cn/player/AudioFocus;)Lru/cn/player/AbstractMediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lru/cn/player/AbstractMediaPlayer;->stop()V

    goto :goto_0

    .line 77
    :pswitch_4
    iget-object v0, p0, Lru/cn/player/AudioFocus$1;->this$0:Lru/cn/player/AudioFocus;

    invoke-static {v0}, Lru/cn/player/AudioFocus;->access$300(Lru/cn/player/AudioFocus;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lru/cn/player/AudioFocus$1;->this$0:Lru/cn/player/AudioFocus;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lru/cn/player/AudioFocus;->access$002(Lru/cn/player/AudioFocus;Z)Z

    .line 79
    iget-object v0, p0, Lru/cn/player/AudioFocus$1;->this$0:Lru/cn/player/AudioFocus;

    invoke-static {v0}, Lru/cn/player/AudioFocus;->access$100(Lru/cn/player/AudioFocus;)Lru/cn/player/AbstractMediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lru/cn/player/AbstractMediaPlayer;->pause()V

    goto :goto_0

    .line 84
    :pswitch_5
    iget-object v0, p0, Lru/cn/player/AudioFocus$1;->this$0:Lru/cn/player/AudioFocus;

    invoke-static {v0}, Lru/cn/player/AudioFocus;->access$100(Lru/cn/player/AudioFocus;)Lru/cn/player/AbstractMediaPlayer;

    move-result-object v0

    const v1, 0x3e4ccccd    # 0.2f

    invoke-virtual {v0, v1}, Lru/cn/player/AbstractMediaPlayer;->setVolume(F)V

    goto :goto_0

    .line 58
    nop

    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method
