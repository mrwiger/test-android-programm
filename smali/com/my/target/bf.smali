.class public Lcom/my/target/bf;
.super Ljava/lang/Object;
.source "CommonSectionParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/bf$a;
    }
.end annotation


# instance fields
.field private final adConfig:Lcom/my/target/b;

.field private final bJ:Landroid/content/Context;

.field private final eC:Lcom/my/target/ae;


# direct methods
.method private constructor <init>(Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/my/target/bf;->eC:Lcom/my/target/ae;

    .line 31
    iput-object p2, p0, Lcom/my/target/bf;->adConfig:Lcom/my/target/b;

    .line 32
    iput-object p3, p0, Lcom/my/target/bf;->bJ:Landroid/content/Context;

    .line 33
    return-void
.end method

.method public static b(Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/bf;
    .locals 1

    .prologue
    .line 21
    new-instance v0, Lcom/my/target/bf;

    invoke-direct {v0, p0, p1, p2}, Lcom/my/target/bf;-><init>(Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)V

    return-object v0
.end method

.method private b(Lorg/json/JSONObject;Lcom/my/target/ak;)V
    .locals 4

    .prologue
    .line 46
    const-string v0, "advertisingLabel"

    const-string v1, ""

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/my/target/ak;->setAdvertisingLabel(Ljava/lang/String;)V

    .line 48
    const-string v0, "clickArea"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 50
    const-string v0, "clickArea"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    .line 51
    if-gtz v0, :cond_1

    .line 53
    const-string v1, "Bad value"

    invoke-static {v1}, Lcom/my/target/az;->y(Ljava/lang/String;)Lcom/my/target/az;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Bad ClickArea mask "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " in section"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/my/target/az;->z(Ljava/lang/String;)Lcom/my/target/az;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/bf;->adConfig:Lcom/my/target/b;

    .line 54
    invoke-virtual {v1}, Lcom/my/target/b;->getSlotId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/my/target/az;->h(I)Lcom/my/target/az;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/bf;->eC:Lcom/my/target/ae;

    .line 55
    invoke-virtual {v1}, Lcom/my/target/ae;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/my/target/az;->A(Ljava/lang/String;)Lcom/my/target/az;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/bf;->bJ:Landroid/content/Context;

    .line 56
    invoke-virtual {v0, v1}, Lcom/my/target/az;->e(Landroid/content/Context;)V

    .line 76
    :cond_0
    :goto_0
    return-void

    .line 60
    :cond_1
    invoke-static {v0}, Lcom/my/target/af;->d(I)Lcom/my/target/af;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/my/target/ak;->setClickArea(Lcom/my/target/af;)V

    goto :goto_0

    .line 63
    :cond_2
    const-string v0, "extendedClickArea"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    const-string v0, "extendedClickArea"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 66
    if-eqz v0, :cond_3

    .line 68
    sget-object v0, Lcom/my/target/af;->cd:Lcom/my/target/af;

    invoke-virtual {p2, v0}, Lcom/my/target/ak;->setClickArea(Lcom/my/target/af;)V

    goto :goto_0

    .line 72
    :cond_3
    sget-object v0, Lcom/my/target/af;->ce:Lcom/my/target/af;

    invoke-virtual {p2, v0}, Lcom/my/target/ak;->setClickArea(Lcom/my/target/af;)V

    goto :goto_0
.end method


# virtual methods
.method public a(Lorg/json/JSONObject;Lcom/my/target/ak;)V
    .locals 1

    .prologue
    .line 37
    const-string v0, "settings"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 38
    if-eqz v0, :cond_0

    .line 40
    invoke-direct {p0, v0, p2}, Lcom/my/target/bf;->b(Lorg/json/JSONObject;Lcom/my/target/ak;)V

    .line 42
    :cond_0
    return-void
.end method
