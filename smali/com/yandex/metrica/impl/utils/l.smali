.class public final Lcom/yandex/metrica/impl/utils/l;
.super Lcom/yandex/metrica/impl/utils/c;
.source "SourceFile"


# static fields
.field private static final a:[I

.field private static final b:Lcom/yandex/metrica/impl/utils/l;

.field private static c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/yandex/metrica/impl/utils/l;->a:[I

    .line 32
    new-instance v0, Lcom/yandex/metrica/impl/utils/l;

    invoke-direct {v0}, Lcom/yandex/metrica/impl/utils/l;-><init>()V

    sput-object v0, Lcom/yandex/metrica/impl/utils/l;->b:Lcom/yandex/metrica/impl/utils/l;

    .line 34
    const-string v0, ""

    sput-object v0, Lcom/yandex/metrica/impl/utils/l;->c:Ljava/lang/String;

    return-void

    .line 28
    :array_0
    .array-data 4
        0x3
        0x6
        0x4
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/yandex/metrica/impl/utils/c;-><init>(Z)V

    .line 38
    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 45
    const-string v0, "[%s] : "

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/yandex/metrica/impl/utils/l;->c:Ljava/lang/String;

    .line 46
    return-void
.end method

.method public static f()Lcom/yandex/metrica/impl/utils/l;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/yandex/metrica/impl/utils/l;->b:Lcom/yandex/metrica/impl/utils/l;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/yandex/metrica/c$c$d$a;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 77
    .line 1083
    sget-object v2, Lcom/yandex/metrica/impl/utils/l;->a:[I

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget v4, v2, v1

    .line 1084
    iget v5, p1, Lcom/yandex/metrica/c$c$d$a;->d:I

    if-ne v5, v4, :cond_2

    const/4 v0, 0x1

    .line 77
    :cond_0
    if-eqz v0, :cond_1

    .line 78
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1090
    iget v0, p1, Lcom/yandex/metrica/c$c$d$a;->d:I

    const/4 v2, 0x3

    if-ne v0, v2, :cond_3

    iget-object v0, p1, Lcom/yandex/metrica/c$c$d$a;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1091
    sget-object v0, Lcom/yandex/metrica/impl/q$a;->o:Lcom/yandex/metrica/impl/q$a;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/q$a;->b()Ljava/lang/String;

    move-result-object v0

    .line 78
    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/utils/l;->a(Ljava/lang/String;)V

    .line 80
    :cond_1
    return-void

    .line 1083
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1092
    :cond_3
    iget v0, p1, Lcom/yandex/metrica/c$c$d$a;->d:I

    const/4 v2, 0x4

    if-ne v0, v2, :cond_5

    .line 1093
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v2, p1, Lcom/yandex/metrica/c$c$d$a;->e:Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1094
    iget-object v2, p1, Lcom/yandex/metrica/c$c$d$a;->f:[B

    if-eqz v2, :cond_4

    .line 1095
    new-instance v2, Ljava/lang/String;

    iget-object v3, p1, Lcom/yandex/metrica/c$c$d$a;->f:[B

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>([B)V

    .line 1096
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 1097
    const-string v3, " with value "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1098
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1101
    :cond_4
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1103
    :cond_5
    iget-object v0, p1, Lcom/yandex/metrica/c$c$d$a;->e:Ljava/lang/String;

    goto :goto_1
.end method

.method public a(Lcom/yandex/metrica/c$c$d;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 108
    iget-object v1, p1, Lcom/yandex/metrica/c$c$d;->d:[Lcom/yandex/metrica/c$c$d$a;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 109
    invoke-virtual {p0, v3, p2}, Lcom/yandex/metrica/impl/utils/l;->a(Lcom/yandex/metrica/c$c$d$a;Ljava/lang/String;)V

    .line 108
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 111
    :cond_0
    return-void
.end method

.method public a(Lcom/yandex/metrica/impl/i;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 64
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/i;->c()I

    move-result v0

    invoke-static {v0}, Lcom/yandex/metrica/impl/q;->b(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 65
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, p2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 66
    const-string v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 67
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/i;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 68
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/i;->c()I

    move-result v1

    invoke-static {v1}, Lcom/yandex/metrica/impl/q;->c(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/yandex/metrica/impl/i;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 69
    const-string v1, " with value "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 70
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/i;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/utils/l;->a(Ljava/lang/String;)V

    .line 74
    :cond_1
    return-void
.end method

.method c(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {v0, p1, p2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    const-string v0, "AppMetrica"

    return-object v0
.end method

.method e()Ljava/lang/String;
    .locals 2

    .prologue
    .line 60
    sget-object v0, Lcom/yandex/metrica/impl/utils/l;->c:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/yandex/metrica/impl/bj;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
