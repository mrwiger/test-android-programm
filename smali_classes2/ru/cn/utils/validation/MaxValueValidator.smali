.class public Lru/cn/utils/validation/MaxValueValidator;
.super Ljava/lang/Object;
.source "MaxValueValidator.java"

# interfaces
.implements Lru/cn/utils/validation/Validator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Number;",
        ":",
        "Ljava/lang/Comparable",
        "<TT;>;>",
        "Ljava/lang/Object;",
        "Lru/cn/utils/validation/Validator;"
    }
.end annotation


# instance fields
.field private final description:Ljava/lang/String;

.field private final maxValue:Ljava/lang/Number;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private final validationValue:Ljava/lang/Number;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Number;Ljava/lang/Number;Ljava/lang/String;)V
    .locals 0
    .param p3, "description"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;TT;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 11
    .local p0, "this":Lru/cn/utils/validation/MaxValueValidator;, "Lru/cn/utils/validation/MaxValueValidator<TT;>;"
    .local p1, "validationValue":Ljava/lang/Number;, "TT;"
    .local p2, "maxValue":Ljava/lang/Number;, "TT;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object p1, p0, Lru/cn/utils/validation/MaxValueValidator;->validationValue:Ljava/lang/Number;

    .line 13
    iput-object p2, p0, Lru/cn/utils/validation/MaxValueValidator;->maxValue:Ljava/lang/Number;

    .line 14
    iput-object p3, p0, Lru/cn/utils/validation/MaxValueValidator;->description:Ljava/lang/String;

    .line 15
    return-void
.end method


# virtual methods
.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    .local p0, "this":Lru/cn/utils/validation/MaxValueValidator;, "Lru/cn/utils/validation/MaxValueValidator<TT;>;"
    iget-object v0, p0, Lru/cn/utils/validation/MaxValueValidator;->description:Ljava/lang/String;

    return-object v0
.end method

.method public validate()Z
    .locals 2

    .prologue
    .line 19
    .local p0, "this":Lru/cn/utils/validation/MaxValueValidator;, "Lru/cn/utils/validation/MaxValueValidator<TT;>;"
    iget-object v0, p0, Lru/cn/utils/validation/MaxValueValidator;->validationValue:Ljava/lang/Number;

    check-cast v0, Ljava/lang/Comparable;

    iget-object v1, p0, Lru/cn/utils/validation/MaxValueValidator;->maxValue:Ljava/lang/Number;

    invoke-interface {v0, v1}, Ljava/lang/Comparable;->compareTo(Ljava/lang/Object;)I

    move-result v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
