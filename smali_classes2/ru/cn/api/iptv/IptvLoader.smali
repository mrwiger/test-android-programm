.class public Lru/cn/api/iptv/IptvLoader;
.super Ljava/lang/Object;
.source "IptvLoader.java"


# direct methods
.method public static getChannels(Lru/cn/api/registry/replies/Service;Z)Ljava/util/List;
    .locals 10
    .param p0, "service"    # Lru/cn/api/registry/replies/Service;
    .param p1, "preventCache"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lru/cn/api/registry/replies/Service;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Lru/cn/api/iptv/replies/MediaLocation;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 42
    invoke-virtual {p0}, Lru/cn/api/registry/replies/Service;->getLocation()Ljava/lang/String;

    move-result-object v1

    .line 43
    .local v1, "location":Ljava/lang/String;
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 44
    .local v5, "requestUri":Landroid/net/Uri;
    invoke-virtual {v5}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 45
    .local v0, "b":Landroid/net/Uri$Builder;
    if-eqz p1, :cond_0

    .line 46
    new-instance v7, Ljava/util/Random;

    invoke-direct {v7}, Ljava/util/Random;-><init>()V

    invoke-virtual {v7}, Ljava/util/Random;->nextInt()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Math;->abs(I)I

    move-result v3

    .line 47
    .local v3, "random":I
    const-string v7, "_"

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v7, v8}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 50
    .end local v3    # "random":I
    :cond_0
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v7}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    .line 52
    .local v6, "requestUrl":Ljava/lang/String;
    invoke-virtual {p0}, Lru/cn/api/registry/replies/Service;->getContractorId()J

    move-result-wide v8

    invoke-static {v8, v9}, Lru/cn/api/ServiceLocator;->iptv(J)Lru/cn/api/iptv/retrofit/IptvApi;

    move-result-object v7

    .line 53
    invoke-interface {v7, v6}, Lru/cn/api/iptv/retrofit/IptvApi;->locations(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v7

    new-instance v8, Lru/cn/api/iptv/IptvLoader$$Lambda$0;

    invoke-direct {v8, p0}, Lru/cn/api/iptv/IptvLoader$$Lambda$0;-><init>(Lru/cn/api/registry/replies/Service;)V

    .line 54
    invoke-virtual {v7, v8}, Lio/reactivex/Single;->doOnError(Lio/reactivex/functions/Consumer;)Lio/reactivex/Single;

    move-result-object v7

    .line 55
    invoke-virtual {v7}, Lio/reactivex/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lru/cn/api/iptv/replies/IptvReply;

    .line 58
    .local v4, "reply":Lru/cn/api/iptv/replies/IptvReply;
    iget-object v7, v4, Lru/cn/api/iptv/replies/IptvReply;->parsingExceptions:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-lez v7, :cond_1

    .line 59
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "url="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 60
    .local v2, "message":Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ";contractor="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0}, Lru/cn/api/registry/replies/Service;->getContractorId()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 62
    sget-object v7, Lru/cn/domain/statistics/inetra/ErrorCode;->CHANNELS_FORMAT_ERROR:Lru/cn/domain/statistics/inetra/ErrorCode;

    const-string v8, "IPTVError"

    const/16 v9, 0x3eb

    invoke-static {v7, v8, v9, v2}, Lru/cn/domain/statistics/inetra/InetraTracker;->error(Lru/cn/domain/statistics/inetra/ErrorCode;Ljava/lang/String;ILjava/lang/String;)V

    .line 66
    .end local v2    # "message":Ljava/lang/String;
    :cond_1
    iget-object v7, v4, Lru/cn/api/iptv/replies/IptvReply;->locations:Ljava/util/List;

    return-object v7
.end method

.method public static getUserChannels(Ljava/lang/String;)Ljava/util/List;
    .locals 5
    .param p0, "requestUrl"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lru/cn/api/iptv/replies/MediaLocation;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 26
    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Lru/cn/api/ServiceLocator;->iptv(J)Lru/cn/api/iptv/retrofit/IptvApi;

    move-result-object v2

    .line 27
    invoke-interface {v2, p0}, Lru/cn/api/iptv/retrofit/IptvApi;->locations(Ljava/lang/String;)Lio/reactivex/Single;

    move-result-object v2

    .line 28
    invoke-virtual {v2}, Lio/reactivex/Single;->blockingGet()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/cn/api/iptv/replies/IptvReply;

    .line 31
    .local v1, "reply":Lru/cn/api/iptv/replies/IptvReply;
    iget-object v2, v1, Lru/cn/api/iptv/replies/IptvReply;->parsingExceptions:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    .line 32
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "url="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 34
    .local v0, "message":Ljava/lang/String;
    sget-object v2, Lru/cn/domain/statistics/inetra/ErrorCode;->CHANNELS_FORMAT_ERROR:Lru/cn/domain/statistics/inetra/ErrorCode;

    const-string v3, "IPTVError"

    const/16 v4, 0x3eb

    invoke-static {v2, v3, v4, v0}, Lru/cn/domain/statistics/inetra/InetraTracker;->error(Lru/cn/domain/statistics/inetra/ErrorCode;Ljava/lang/String;ILjava/lang/String;)V

    .line 38
    .end local v0    # "message":Ljava/lang/String;
    :cond_0
    iget-object v2, v1, Lru/cn/api/iptv/replies/IptvReply;->locations:Ljava/util/List;

    return-object v2
.end method

.method static final synthetic lambda$getChannels$0$IptvLoader(Lru/cn/api/registry/replies/Service;Ljava/lang/Throwable;)V
    .locals 0
    .param p0, "service"    # Lru/cn/api/registry/replies/Service;
    .param p1, "error"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 54
    invoke-static {p0, p1}, Lru/cn/api/iptv/IptvLoader;->trackError(Lru/cn/api/registry/replies/Service;Ljava/lang/Throwable;)V

    return-void
.end method

.method private static trackError(Lru/cn/api/registry/replies/Service;Ljava/lang/Throwable;)V
    .locals 7
    .param p0, "iptvService"    # Lru/cn/api/registry/replies/Service;
    .param p1, "error"    # Ljava/lang/Throwable;

    .prologue
    const/4 v6, 0x0

    .line 70
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "contractor="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lru/cn/api/registry/replies/Service;->getContractorId()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 71
    .local v1, "errorMessage":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ";url="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lru/cn/api/registry/replies/Service;->getLocation()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 73
    instance-of v3, p1, Ljava/net/URISyntaxException;

    if-eqz v3, :cond_0

    .line 74
    sget-object v3, Lru/cn/domain/statistics/inetra/ErrorCode;->CHANNELS_INVALID_URL:Lru/cn/domain/statistics/inetra/ErrorCode;

    const-string v4, "playlist"

    invoke-static {v3, v4, v6, v1}, Lru/cn/domain/statistics/inetra/InetraTracker;->error(Lru/cn/domain/statistics/inetra/ErrorCode;Ljava/lang/String;ILjava/lang/String;)V

    .line 121
    .end local p1    # "error":Ljava/lang/Throwable;
    :goto_0
    return-void

    .line 78
    .restart local p1    # "error":Ljava/lang/Throwable;
    :cond_0
    instance-of v3, p1, Ljava/net/UnknownHostException;

    if-eqz v3, :cond_1

    .line 79
    sget-object v3, Lru/cn/domain/statistics/inetra/ErrorCode;->CHANNELS_DOMAIN_TIMEOUT:Lru/cn/domain/statistics/inetra/ErrorCode;

    const-string v4, "playlist"

    invoke-static {v3, v4, v6, v1}, Lru/cn/domain/statistics/inetra/InetraTracker;->error(Lru/cn/domain/statistics/inetra/ErrorCode;Ljava/lang/String;ILjava/lang/String;)V

    goto :goto_0

    .line 83
    :cond_1
    instance-of v3, p1, Lru/cn/utils/http/HttpException;

    if-eqz v3, :cond_2

    .line 84
    sget-object v3, Lru/cn/domain/statistics/inetra/ErrorCode;->CHANNELS_HTTP_RESPONSE_ERROR:Lru/cn/domain/statistics/inetra/ErrorCode;

    const-string v4, "playlist"

    check-cast p1, Lru/cn/utils/http/HttpException;

    .line 86
    .end local p1    # "error":Ljava/lang/Throwable;
    invoke-virtual {p1}, Lru/cn/utils/http/HttpException;->getStatusCode()I

    move-result v5

    .line 84
    invoke-static {v3, v4, v5, v1}, Lru/cn/domain/statistics/inetra/InetraTracker;->error(Lru/cn/domain/statistics/inetra/ErrorCode;Ljava/lang/String;ILjava/lang/String;)V

    goto :goto_0

    .line 88
    .restart local p1    # "error":Ljava/lang/Throwable;
    :cond_2
    instance-of v3, p1, Ljava/net/SocketTimeoutException;

    if-eqz v3, :cond_3

    .line 89
    sget-object v3, Lru/cn/domain/statistics/inetra/ErrorCode;->CHANNELS_SOCKET_TIMEOUT:Lru/cn/domain/statistics/inetra/ErrorCode;

    const-string v4, "playlist"

    invoke-static {v3, v4, v6, v1}, Lru/cn/domain/statistics/inetra/InetraTracker;->error(Lru/cn/domain/statistics/inetra/ErrorCode;Ljava/lang/String;ILjava/lang/String;)V

    goto :goto_0

    .line 93
    :cond_3
    instance-of v3, p1, Ljava/io/IOException;

    if-eqz v3, :cond_6

    .line 94
    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    .line 95
    .local v0, "cause":Ljava/lang/Throwable;
    instance-of v3, v0, Lru/cn/api/iptv/M3UParserException;

    if-eqz v3, :cond_4

    move-object v2, v0

    .line 96
    check-cast v2, Lru/cn/api/iptv/M3UParserException;

    .line 97
    .local v2, "exc":Lru/cn/api/iptv/M3UParserException;
    sget-object v3, Lru/cn/domain/statistics/inetra/ErrorCode;->CHANNELS_FORMAT_ERROR:Lru/cn/domain/statistics/inetra/ErrorCode;

    const-string v4, "M3UParserError"

    iget v5, v2, Lru/cn/api/iptv/M3UParserException;->code:I

    invoke-static {v3, v4, v5, v1}, Lru/cn/domain/statistics/inetra/InetraTracker;->error(Lru/cn/domain/statistics/inetra/ErrorCode;Ljava/lang/String;ILjava/lang/String;)V

    goto :goto_0

    .line 100
    .end local v2    # "exc":Lru/cn/api/iptv/M3UParserException;
    :cond_4
    instance-of v3, v0, Landroid/net/ParseException;

    if-eqz v3, :cond_5

    .line 101
    sget-object v3, Lru/cn/domain/statistics/inetra/ErrorCode;->CHANNELS_FORMAT_ERROR:Lru/cn/domain/statistics/inetra/ErrorCode;

    const-string v4, "IPTVParserError"

    invoke-static {v3, v4, v6, v1}, Lru/cn/domain/statistics/inetra/InetraTracker;->error(Lru/cn/domain/statistics/inetra/ErrorCode;Ljava/lang/String;ILjava/lang/String;)V

    goto :goto_0

    .line 105
    :cond_5
    sget-object v3, Lru/cn/domain/statistics/inetra/ErrorCode;->UNKNOWN_ERROR:Lru/cn/domain/statistics/inetra/ErrorCode;

    const-string v4, "playlist"

    invoke-static {v3, v4, v6, v1}, Lru/cn/domain/statistics/inetra/InetraTracker;->error(Lru/cn/domain/statistics/inetra/ErrorCode;Ljava/lang/String;ILjava/lang/String;)V

    goto :goto_0

    .line 109
    .end local v0    # "cause":Ljava/lang/Throwable;
    :cond_6
    instance-of v3, p1, Lru/cn/api/iptv/M3UParserException;

    if-eqz v3, :cond_7

    move-object v2, p1

    .line 110
    check-cast v2, Lru/cn/api/iptv/M3UParserException;

    .line 111
    .restart local v2    # "exc":Lru/cn/api/iptv/M3UParserException;
    sget-object v3, Lru/cn/domain/statistics/inetra/ErrorCode;->CHANNELS_FORMAT_ERROR:Lru/cn/domain/statistics/inetra/ErrorCode;

    const-string v4, "M3UParserError"

    iget v5, v2, Lru/cn/api/iptv/M3UParserException;->code:I

    invoke-static {v3, v4, v5, v1}, Lru/cn/domain/statistics/inetra/InetraTracker;->error(Lru/cn/domain/statistics/inetra/ErrorCode;Ljava/lang/String;ILjava/lang/String;)V

    goto :goto_0

    .line 114
    .end local v2    # "exc":Lru/cn/api/iptv/M3UParserException;
    :cond_7
    instance-of v3, p1, Landroid/net/ParseException;

    if-eqz v3, :cond_8

    .line 115
    sget-object v3, Lru/cn/domain/statistics/inetra/ErrorCode;->CHANNELS_FORMAT_ERROR:Lru/cn/domain/statistics/inetra/ErrorCode;

    const-string v4, "IPTVParserError"

    invoke-static {v3, v4, v6, v1}, Lru/cn/domain/statistics/inetra/InetraTracker;->error(Lru/cn/domain/statistics/inetra/ErrorCode;Ljava/lang/String;ILjava/lang/String;)V

    goto :goto_0

    .line 119
    :cond_8
    invoke-static {p1}, Lru/cn/utils/Logger;->logException(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
