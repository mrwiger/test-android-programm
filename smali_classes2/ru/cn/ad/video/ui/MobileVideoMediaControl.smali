.class public Lru/cn/ad/video/ui/MobileVideoMediaControl;
.super Ljava/lang/Object;
.source "MobileVideoMediaControl.java"

# interfaces
.implements Lru/cn/ad/video/ui/MediaControl;


# instance fields
.field private adAttributionView:Landroid/widget/TextView;

.field private adLinkButton:Landroid/view/View;

.field private adSkipButton:Landroid/widget/Button;

.field private adSkipWaitTime:Landroid/widget/TextView;

.field private adSkipWrapper:Landroid/view/View;

.field private adWrapper:Landroid/view/View;

.field private final attributionFormat:Ljava/lang/String;

.field private duration:J

.field private final formatter:Ljava/text/DateFormat;

.field private listener:Lru/cn/ad/video/ui/MediaControl$Listener;

.field private position:J

.field private final skipFormat:Ljava/lang/String;

.field private skipPosition:J

.field private skippable:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "mm:ss"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v0, p0, Lru/cn/ad/video/ui/MobileVideoMediaControl;->formatter:Ljava/text/DateFormat;

    .line 38
    const v0, 0x7f0e0024

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lru/cn/ad/video/ui/MobileVideoMediaControl;->skipFormat:Ljava/lang/String;

    .line 39
    const v0, 0x7f0e0020

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lru/cn/ad/video/ui/MobileVideoMediaControl;->attributionFormat:Ljava/lang/String;

    .line 41
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0c009f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lru/cn/ad/video/ui/MobileVideoMediaControl;->adWrapper:Landroid/view/View;

    .line 43
    iget-object v0, p0, Lru/cn/ad/video/ui/MobileVideoMediaControl;->adWrapper:Landroid/view/View;

    const v1, 0x7f090025

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lru/cn/ad/video/ui/MobileVideoMediaControl;->adSkipWrapper:Landroid/view/View;

    .line 44
    iget-object v0, p0, Lru/cn/ad/video/ui/MobileVideoMediaControl;->adWrapper:Landroid/view/View;

    const v1, 0x7f090022

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lru/cn/ad/video/ui/MobileVideoMediaControl;->adSkipButton:Landroid/widget/Button;

    .line 45
    iget-object v0, p0, Lru/cn/ad/video/ui/MobileVideoMediaControl;->adWrapper:Landroid/view/View;

    const v1, 0x7f090024

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lru/cn/ad/video/ui/MobileVideoMediaControl;->adSkipWaitTime:Landroid/widget/TextView;

    .line 46
    iget-object v0, p0, Lru/cn/ad/video/ui/MobileVideoMediaControl;->adWrapper:Landroid/view/View;

    const v1, 0x7f09001d

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lru/cn/ad/video/ui/MobileVideoMediaControl;->adAttributionView:Landroid/widget/TextView;

    .line 48
    iget-object v0, p0, Lru/cn/ad/video/ui/MobileVideoMediaControl;->adSkipButton:Landroid/widget/Button;

    new-instance v1, Lru/cn/ad/video/ui/MobileVideoMediaControl$1;

    invoke-direct {v1, p0}, Lru/cn/ad/video/ui/MobileVideoMediaControl$1;-><init>(Lru/cn/ad/video/ui/MobileVideoMediaControl;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 55
    iget-object v0, p0, Lru/cn/ad/video/ui/MobileVideoMediaControl;->adWrapper:Landroid/view/View;

    const v1, 0x7f090021

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lru/cn/ad/video/ui/MobileVideoMediaControl;->adLinkButton:Landroid/view/View;

    .line 56
    iget-object v0, p0, Lru/cn/ad/video/ui/MobileVideoMediaControl;->adLinkButton:Landroid/view/View;

    new-instance v1, Lru/cn/ad/video/ui/MobileVideoMediaControl$2;

    invoke-direct {v1, p0}, Lru/cn/ad/video/ui/MobileVideoMediaControl$2;-><init>(Lru/cn/ad/video/ui/MobileVideoMediaControl;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 62
    return-void
.end method

.method static synthetic access$000(Lru/cn/ad/video/ui/MobileVideoMediaControl;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/ad/video/ui/MobileVideoMediaControl;

    .prologue
    .line 17
    invoke-direct {p0}, Lru/cn/ad/video/ui/MobileVideoMediaControl;->skip()V

    return-void
.end method

.method static synthetic access$100(Lru/cn/ad/video/ui/MobileVideoMediaControl;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/ad/video/ui/MobileVideoMediaControl;

    .prologue
    .line 17
    invoke-direct {p0}, Lru/cn/ad/video/ui/MobileVideoMediaControl;->click()V

    return-void
.end method

.method private click()V
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lru/cn/ad/video/ui/MobileVideoMediaControl;->listener:Lru/cn/ad/video/ui/MediaControl$Listener;

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Lru/cn/ad/video/ui/MobileVideoMediaControl;->listener:Lru/cn/ad/video/ui/MediaControl$Listener;

    invoke-interface {v0}, Lru/cn/ad/video/ui/MediaControl$Listener;->onClicked()V

    .line 104
    :cond_0
    return-void
.end method

.method private skip()V
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lru/cn/ad/video/ui/MobileVideoMediaControl;->listener:Lru/cn/ad/video/ui/MediaControl$Listener;

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Lru/cn/ad/video/ui/MobileVideoMediaControl;->listener:Lru/cn/ad/video/ui/MediaControl$Listener;

    invoke-interface {v0}, Lru/cn/ad/video/ui/MediaControl$Listener;->onSkipped()V

    .line 99
    :cond_0
    return-void
.end method

.method private update()V
    .locals 13

    .prologue
    const/4 v10, 0x1

    const/4 v12, 0x0

    .line 107
    iget-boolean v4, p0, Lru/cn/ad/video/ui/MobileVideoMediaControl;->skippable:Z

    if-eqz v4, :cond_0

    .line 108
    iget-wide v4, p0, Lru/cn/ad/video/ui/MobileVideoMediaControl;->skipPosition:J

    iget-wide v6, p0, Lru/cn/ad/video/ui/MobileVideoMediaControl;->position:J

    cmp-long v4, v4, v6

    if-lez v4, :cond_1

    .line 109
    const-wide/16 v4, 0x1

    iget-wide v6, p0, Lru/cn/ad/video/ui/MobileVideoMediaControl;->skipPosition:J

    iget-wide v8, p0, Lru/cn/ad/video/ui/MobileVideoMediaControl;->position:J

    sub-long/2addr v6, v8

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    add-long/2addr v4, v6

    long-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    double-to-long v2, v4

    .line 110
    .local v2, "timeUntilSkip":J
    iget-object v4, p0, Lru/cn/ad/video/ui/MobileVideoMediaControl;->skipFormat:Ljava/lang/String;

    new-array v5, v10, [Ljava/lang/Object;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v5, v12

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 112
    .local v1, "text":Ljava/lang/String;
    iget-object v4, p0, Lru/cn/ad/video/ui/MobileVideoMediaControl;->adSkipWaitTime:Landroid/widget/TextView;

    invoke-virtual {v4, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 113
    iget-object v4, p0, Lru/cn/ad/video/ui/MobileVideoMediaControl;->adSkipWaitTime:Landroid/widget/TextView;

    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 126
    .end local v1    # "text":Ljava/lang/String;
    .end local v2    # "timeUntilSkip":J
    :cond_0
    :goto_0
    iget-object v4, p0, Lru/cn/ad/video/ui/MobileVideoMediaControl;->attributionFormat:Ljava/lang/String;

    new-array v5, v10, [Ljava/lang/Object;

    iget-object v6, p0, Lru/cn/ad/video/ui/MobileVideoMediaControl;->formatter:Ljava/text/DateFormat;

    new-instance v7, Ljava/util/Date;

    iget-wide v8, p0, Lru/cn/ad/video/ui/MobileVideoMediaControl;->duration:J

    iget-wide v10, p0, Lru/cn/ad/video/ui/MobileVideoMediaControl;->position:J

    sub-long/2addr v8, v10

    invoke-direct {v7, v8, v9}, Ljava/util/Date;-><init>(J)V

    .line 127
    invoke-virtual {v6, v7}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v12

    .line 126
    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 128
    .local v0, "adDurationText":Ljava/lang/String;
    iget-object v4, p0, Lru/cn/ad/video/ui/MobileVideoMediaControl;->adAttributionView:Landroid/widget/TextView;

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 129
    return-void

    .line 115
    .end local v0    # "adDurationText":Ljava/lang/String;
    :cond_1
    iget-wide v4, p0, Lru/cn/ad/video/ui/MobileVideoMediaControl;->position:J

    iget-wide v6, p0, Lru/cn/ad/video/ui/MobileVideoMediaControl;->skipPosition:J

    sub-long/2addr v4, v6

    const-wide/16 v6, 0x1388

    cmp-long v4, v4, v6

    if-lez v4, :cond_2

    .line 116
    iget-object v4, p0, Lru/cn/ad/video/ui/MobileVideoMediaControl;->adSkipWrapper:Landroid/view/View;

    const/high16 v5, 0x3f000000    # 0.5f

    invoke-virtual {v4, v5}, Landroid/view/View;->setAlpha(F)V

    .line 119
    :cond_2
    iget-object v4, p0, Lru/cn/ad/video/ui/MobileVideoMediaControl;->adSkipButton:Landroid/widget/Button;

    invoke-virtual {v4}, Landroid/widget/Button;->getVisibility()I

    move-result v4

    if-eqz v4, :cond_0

    .line 120
    iget-object v4, p0, Lru/cn/ad/video/ui/MobileVideoMediaControl;->adSkipButton:Landroid/widget/Button;

    invoke-virtual {v4, v12}, Landroid/widget/Button;->setVisibility(I)V

    .line 121
    iget-object v4, p0, Lru/cn/ad/video/ui/MobileVideoMediaControl;->adSkipWaitTime:Landroid/widget/TextView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public getView()Landroid/view/View;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lru/cn/ad/video/ui/MobileVideoMediaControl;->adWrapper:Landroid/view/View;

    return-object v0
.end method

.method public setClickable(Z)V
    .locals 2
    .param p1, "clickable"    # Z

    .prologue
    .line 83
    iget-object v1, p0, Lru/cn/ad/video/ui/MobileVideoMediaControl;->adLinkButton:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 84
    return-void

    .line 83
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setDuration(J)V
    .locals 1
    .param p1, "durationMs"    # J

    .prologue
    .line 66
    iput-wide p1, p0, Lru/cn/ad/video/ui/MobileVideoMediaControl;->duration:J

    .line 67
    return-void
.end method

.method public setListener(Lru/cn/ad/video/ui/MediaControl$Listener;)V
    .locals 0
    .param p1, "listener"    # Lru/cn/ad/video/ui/MediaControl$Listener;

    .prologue
    .line 93
    iput-object p1, p0, Lru/cn/ad/video/ui/MobileVideoMediaControl;->listener:Lru/cn/ad/video/ui/MediaControl$Listener;

    .line 94
    return-void
.end method

.method public setPosition(J)V
    .locals 1
    .param p1, "positionMs"    # J

    .prologue
    .line 71
    iput-wide p1, p0, Lru/cn/ad/video/ui/MobileVideoMediaControl;->position:J

    .line 72
    invoke-direct {p0}, Lru/cn/ad/video/ui/MobileVideoMediaControl;->update()V

    .line 73
    return-void
.end method

.method public setSkipPositionMs(J)V
    .locals 1
    .param p1, "skipPosition"    # J

    .prologue
    .line 77
    const/4 v0, 0x1

    iput-boolean v0, p0, Lru/cn/ad/video/ui/MobileVideoMediaControl;->skippable:Z

    .line 78
    iput-wide p1, p0, Lru/cn/ad/video/ui/MobileVideoMediaControl;->skipPosition:J

    .line 79
    return-void
.end method
