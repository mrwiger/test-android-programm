.class public Lcom/my/target/aa;
.super Ljava/lang/Object;
.source "MraidBridge.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/aa$e;,
        Lcom/my/target/aa$d;,
        Lcom/my/target/aa$i;,
        Lcom/my/target/aa$g;,
        Lcom/my/target/aa$c;,
        Lcom/my/target/aa$a;,
        Lcom/my/target/aa$h;,
        Lcom/my/target/aa$f;,
        Lcom/my/target/aa$b;
    }
.end annotation


# instance fields
.field private final aW:Ljava/lang/String;

.field private final aX:Landroid/webkit/WebViewClient;

.field private aY:Lcom/my/target/aa$a;

.field private aZ:Lcom/my/target/bz;

.field private ba:Z


# direct methods
.method private constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    new-instance v0, Lcom/my/target/aa$b;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/my/target/aa$b;-><init>(Lcom/my/target/aa;Lcom/my/target/aa$1;)V

    iput-object v0, p0, Lcom/my/target/aa;->aX:Landroid/webkit/WebViewClient;

    .line 51
    iput-object p1, p0, Lcom/my/target/aa;->aW:Ljava/lang/String;

    .line 52
    return-void
.end method

.method private a(Landroid/graphics/Rect;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 372
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p1, Landroid/graphics/Rect;->left:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p1, Landroid/graphics/Rect;->top:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/my/target/aa;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/my/target/aa;->o()V

    return-void
.end method

.method static synthetic b(Lcom/my/target/aa;)Lcom/my/target/aa$a;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/my/target/aa;->aY:Lcom/my/target/aa$a;

    return-object v0
.end method

.method private b(Landroid/graphics/Rect;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 378
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static e(Ljava/lang/String;)Lcom/my/target/aa;
    .locals 1

    .prologue
    .line 40
    new-instance v0, Lcom/my/target/aa;

    invoke-direct {v0, p0}, Lcom/my/target/aa;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private i(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 336
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mraidbridge.nativeComplete("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Lorg/json/JSONObject;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/my/target/aa;->j(Ljava/lang/String;)V

    .line 337
    return-void
.end method

.method private j(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 341
    iget-object v0, p0, Lcom/my/target/aa;->aZ:Lcom/my/target/bz;

    if-nez v0, :cond_0

    .line 343
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Attempted to inject Javascript into MRAID WebView while was not attached:\n\t"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 349
    :goto_0
    return-void

    .line 346
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "javascript:window."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 347
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Injecting Javascript into MRAID WebView:\n\t"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 348
    iget-object v1, p0, Lcom/my/target/aa;->aZ:Lcom/my/target/bz;

    invoke-virtual {v1, v0}, Lcom/my/target/bz;->loadUrl(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private o()V
    .locals 1

    .prologue
    .line 357
    iget-boolean v0, p0, Lcom/my/target/aa;->ba:Z

    if-eqz v0, :cond_1

    .line 367
    :cond_0
    :goto_0
    return-void

    .line 362
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/my/target/aa;->ba:Z

    .line 363
    iget-object v0, p0, Lcom/my/target/aa;->aY:Lcom/my/target/aa$a;

    if-eqz v0, :cond_0

    .line 365
    iget-object v0, p0, Lcom/my/target/aa;->aY:Lcom/my/target/aa$a;

    invoke-interface {v0}, Lcom/my/target/aa$a;->p()V

    goto :goto_0
.end method


# virtual methods
.method a(Landroid/net/Uri;)V
    .locals 6

    .prologue
    .line 145
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    .line 146
    invoke-virtual {p1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    .line 148
    const-string v2, "mytarget"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 150
    const-string v1, "onloadmraidjs"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 152
    const-string v0, "MraidBridge: JS call onLoad"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 154
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "MraidBridge: got mytarget scheme "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 212
    :cond_1
    :goto_0
    return-void

    .line 158
    :cond_2
    const-string v2, "mraid"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 160
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 162
    const/4 v1, 0x0

    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 164
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Got mraid command "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 165
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    .line 167
    const/4 v1, 0x0

    .line 168
    new-instance v3, Lcom/my/target/ab;

    iget-object v4, p0, Lcom/my/target/aa;->aW:Ljava/lang/String;

    invoke-direct {v3, v0, v4}, Lcom/my/target/ab;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    invoke-virtual {v3}, Lcom/my/target/ab;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/my/target/aa;->i(Ljava/lang/String;)V

    .line 171
    const-string/jumbo v0, "{"

    invoke-virtual {v2, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    .line 172
    const-string/jumbo v0, "}"

    invoke-virtual {v2, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    add-int/lit8 v5, v0, 0x1

    .line 175
    if-ltz v4, :cond_5

    if-lez v5, :cond_5

    if-ge v4, v5, :cond_5

    .line 178
    :try_start_0
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    if-gt v5, v0, :cond_5

    .line 180
    new-instance v0, Lorg/json/JSONObject;

    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 182
    :goto_1
    invoke-virtual {p0, v3, v0}, Lcom/my/target/aa;->a(Lcom/my/target/ab;Lorg/json/JSONObject;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 184
    :catch_0
    move-exception v0

    .line 186
    invoke-virtual {v3}, Lcom/my/target/ab;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/my/target/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 193
    :cond_4
    :try_start_1
    new-instance v0, Ljava/net/URI;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/URI;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/net/URISyntaxException; {:try_start_1 .. :try_end_1} :catch_1

    .line 205
    iget-object v0, p0, Lcom/my/target/aa;->aZ:Lcom/my/target/bz;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/my/target/aa;->aZ:Lcom/my/target/bz;

    invoke-virtual {v0}, Lcom/my/target/bz;->bd()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 207
    iget-object v0, p0, Lcom/my/target/aa;->aY:Lcom/my/target/aa$a;

    if-eqz v0, :cond_1

    .line 209
    iget-object v0, p0, Lcom/my/target/aa;->aY:Lcom/my/target/aa$a;

    invoke-interface {v0, p1}, Lcom/my/target/aa$a;->b(Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 195
    :catch_1
    move-exception v0

    .line 197
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Invalid MRAID URL: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 198
    const-string v0, ""

    const-string v1, "Mraid command sent an invalid URL"

    invoke-virtual {p0, v0, v1}, Lcom/my/target/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_5
    move-object v0, v1

    goto :goto_1
.end method

.method public a(Lcom/my/target/aa$a;)V
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/my/target/aa;->aY:Lcom/my/target/aa$a;

    .line 83
    return-void
.end method

.method public a(Lcom/my/target/ad;)V
    .locals 2

    .prologue
    .line 68
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mraidbridge.setScreenSize("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/my/target/ad;->x()Landroid/graphics/Rect;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/my/target/aa;->b(Landroid/graphics/Rect;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ");window.mraidbridge.setMaxSize("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 69
    invoke-virtual {p1}, Lcom/my/target/ad;->u()Landroid/graphics/Rect;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/my/target/aa;->b(Landroid/graphics/Rect;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ");window.mraidbridge.setCurrentPosition("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 70
    invoke-virtual {p1}, Lcom/my/target/ad;->v()Landroid/graphics/Rect;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/my/target/aa;->a(Landroid/graphics/Rect;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ");window.mraidbridge.setDefaultPosition("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 71
    invoke-virtual {p1}, Lcom/my/target/ad;->w()Landroid/graphics/Rect;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/my/target/aa;->a(Landroid/graphics/Rect;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 68
    invoke-direct {p0, v0}, Lcom/my/target/aa;->j(Ljava/lang/String;)V

    .line 72
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mraidbridge.fireSizeChangeEvent("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/my/target/ad;->v()Landroid/graphics/Rect;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/my/target/aa;->b(Landroid/graphics/Rect;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/my/target/aa;->j(Ljava/lang/String;)V

    .line 73
    return-void
.end method

.method public a(Lcom/my/target/bz;)V
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetJavaScriptEnabled"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 88
    iput-object p1, p0, Lcom/my/target/aa;->aZ:Lcom/my/target/bz;

    .line 89
    iget-object v0, p0, Lcom/my/target/aa;->aZ:Lcom/my/target/bz;

    invoke-virtual {v0}, Lcom/my/target/bz;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 91
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    .line 93
    const-string v0, "interstitial"

    iget-object v1, p0, Lcom/my/target/aa;->aW:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 95
    invoke-virtual {p1}, Lcom/my/target/bz;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setMediaPlaybackRequiresUserGesture(Z)V

    .line 99
    :cond_0
    iget-object v0, p0, Lcom/my/target/aa;->aZ:Lcom/my/target/bz;

    invoke-virtual {v0, v2}, Lcom/my/target/bz;->setScrollContainer(Z)V

    .line 100
    iget-object v0, p0, Lcom/my/target/aa;->aZ:Lcom/my/target/bz;

    invoke-virtual {v0, v2}, Lcom/my/target/bz;->setVerticalScrollBarEnabled(Z)V

    .line 101
    iget-object v0, p0, Lcom/my/target/aa;->aZ:Lcom/my/target/bz;

    invoke-virtual {v0, v2}, Lcom/my/target/bz;->setHorizontalScrollBarEnabled(Z)V

    .line 102
    iget-object v0, p0, Lcom/my/target/aa;->aZ:Lcom/my/target/bz;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Lcom/my/target/bz;->setBackgroundColor(I)V

    .line 104
    iget-object v0, p0, Lcom/my/target/aa;->aZ:Lcom/my/target/bz;

    iget-object v1, p0, Lcom/my/target/aa;->aX:Landroid/webkit/WebViewClient;

    invoke-virtual {v0, v1}, Lcom/my/target/bz;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 106
    iget-object v0, p0, Lcom/my/target/aa;->aZ:Lcom/my/target/bz;

    new-instance v1, Lcom/my/target/aa$f;

    invoke-direct {v1, p0, v3}, Lcom/my/target/aa$f;-><init>(Lcom/my/target/aa;Lcom/my/target/aa$1;)V

    invoke-virtual {v0, v1}, Lcom/my/target/bz;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 108
    iget-object v0, p0, Lcom/my/target/aa;->aZ:Lcom/my/target/bz;

    new-instance v1, Lcom/my/target/aa$h;

    invoke-direct {v1, p0, v3}, Lcom/my/target/aa$h;-><init>(Lcom/my/target/aa;Lcom/my/target/aa$1;)V

    invoke-virtual {v0, v1}, Lcom/my/target/bz;->setVisibilityChangedListener(Lcom/my/target/bz$a;)V

    .line 109
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 216
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mraidbridge.fireErrorEvent("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 217
    invoke-static {p2}, Lorg/json/JSONObject;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 218
    invoke-static {p1}, Lorg/json/JSONObject;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 216
    invoke-direct {p0, v0}, Lcom/my/target/aa;->j(Ljava/lang/String;)V

    .line 219
    return-void
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 77
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mraidbridge.setSupports("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-static {v1, p1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/my/target/aa;->j(Ljava/lang/String;)V

    .line 78
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 123
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mraidbridge.setIsViewable("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/my/target/aa;->j(Ljava/lang/String;)V

    .line 124
    return-void
.end method

.method a(Lcom/my/target/ab;Lorg/json/JSONObject;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 231
    invoke-virtual {p1}, Lcom/my/target/ab;->toString()Ljava/lang/String;

    move-result-object v3

    .line 232
    iget-boolean v2, p1, Lcom/my/target/ab;->bC:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/my/target/aa;->aZ:Lcom/my/target/bz;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/my/target/aa;->aZ:Lcom/my/target/bz;

    invoke-virtual {v2}, Lcom/my/target/bz;->bd()Z

    move-result v2

    if-nez v2, :cond_0

    .line 234
    const-string v1, "Cannot execute this command unless the user clicks"

    invoke-virtual {p0, v3, v1}, Lcom/my/target/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 331
    :goto_0
    return v0

    .line 238
    :cond_0
    iget-object v2, p0, Lcom/my/target/aa;->aY:Lcom/my/target/aa$a;

    if-nez v2, :cond_1

    .line 240
    const-string v1, "Invalid state to execute this command"

    invoke-virtual {p0, v3, v1}, Lcom/my/target/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 244
    :cond_1
    iget-object v2, p0, Lcom/my/target/aa;->aZ:Lcom/my/target/bz;

    if-nez v2, :cond_2

    .line 246
    const-string v1, "The current WebView is being destroyed"

    invoke-virtual {p0, v3, v1}, Lcom/my/target/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 250
    :cond_2
    const/4 v2, -0x1

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    :cond_3
    :goto_1
    packed-switch v2, :pswitch_data_0

    :goto_2
    move v0, v1

    .line 331
    goto :goto_0

    .line 250
    :sswitch_0
    const-string v4, "close"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    move v2, v0

    goto :goto_1

    :sswitch_1
    const-string v4, "setResizeProperties"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    move v2, v1

    goto :goto_1

    :sswitch_2
    const-string v4, "resize"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v2, 0x2

    goto :goto_1

    :sswitch_3
    const-string v4, "expand"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v2, 0x3

    goto :goto_1

    :sswitch_4
    const-string v4, "useCustomClose"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v2, 0x4

    goto :goto_1

    :sswitch_5
    const-string v4, "setExpandProperties"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v2, 0x5

    goto :goto_1

    :sswitch_6
    const-string v4, "open"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v2, 0x6

    goto :goto_1

    :sswitch_7
    const-string v4, "setOrientationProperties"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v2, 0x7

    goto :goto_1

    :sswitch_8
    const-string v4, "vpaidInit"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/16 v2, 0x8

    goto :goto_1

    :sswitch_9
    const-string v4, "vpaidEvent"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/16 v2, 0x9

    goto :goto_1

    :sswitch_a
    const-string v4, "playheadEvent"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/16 v2, 0xa

    goto :goto_1

    :sswitch_b
    const-string v4, "playVideo"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/16 v2, 0xb

    goto :goto_1

    :sswitch_c
    const-string v4, "storePicture"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/16 v2, 0xc

    goto/16 :goto_1

    :sswitch_d
    const-string v4, "createCalendarEvent"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/16 v2, 0xd

    goto/16 :goto_1

    :sswitch_e
    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/16 v2, 0xe

    goto/16 :goto_1

    .line 253
    :pswitch_0
    iget-object v0, p0, Lcom/my/target/aa;->aY:Lcom/my/target/aa$a;

    invoke-interface {v0}, Lcom/my/target/aa$a;->onClose()V

    goto/16 :goto_2

    .line 256
    :pswitch_1
    const-string v0, "resize is currently unsupported"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 259
    :pswitch_2
    const-string v0, "resize is currently unsupported"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 262
    :pswitch_3
    const-string v0, "expand is currently unsupported"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 266
    :pswitch_4
    if-nez p2, :cond_4

    .line 268
    const-string v1, "useCustomClose params cannot be null"

    invoke-virtual {p0, v3, v1}, Lcom/my/target/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 271
    :cond_4
    const-string v0, "useCustomClose"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 272
    iget-object v2, p0, Lcom/my/target/aa;->aY:Lcom/my/target/aa$a;

    invoke-interface {v2, v0}, Lcom/my/target/aa$a;->b(Z)V

    goto/16 :goto_2

    .line 275
    :pswitch_5
    if-nez p2, :cond_5

    .line 277
    const-string v1, "open params cannot be null"

    invoke-virtual {p0, v3, v1}, Lcom/my/target/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 280
    :cond_5
    const-string v0, "url"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 281
    iget-object v2, p0, Lcom/my/target/aa;->aY:Lcom/my/target/aa$a;

    invoke-interface {v2, v0}, Lcom/my/target/aa$a;->b(Landroid/net/Uri;)V

    goto/16 :goto_2

    .line 284
    :pswitch_6
    if-nez p2, :cond_6

    .line 286
    const-string v1, "setOrientationProperties params cannot be null"

    invoke-virtual {p0, v3, v1}, Lcom/my/target/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 289
    :cond_6
    const-string v1, "allowOrientationChange"

    invoke-virtual {p2, v1}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 290
    const-string v2, "forceOrientation"

    invoke-virtual {p2, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 291
    invoke-static {v2}, Lcom/my/target/ac;->l(Ljava/lang/String;)Lcom/my/target/ac;

    move-result-object v4

    .line 292
    if-nez v4, :cond_7

    .line 294
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "wrong orientation "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v3, v1}, Lcom/my/target/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 297
    :cond_7
    iget-object v0, p0, Lcom/my/target/aa;->aY:Lcom/my/target/aa$a;

    invoke-interface {v0, v1, v4}, Lcom/my/target/aa$a;->a(ZLcom/my/target/ac;)Z

    move-result v0

    goto/16 :goto_0

    .line 299
    :pswitch_7
    iget-object v0, p0, Lcom/my/target/aa;->aY:Lcom/my/target/aa$a;

    invoke-interface {v0}, Lcom/my/target/aa$a;->r()V

    goto/16 :goto_2

    .line 302
    :pswitch_8
    if-nez p2, :cond_8

    .line 304
    const-string v1, "vpaidEvent params cannot be null"

    invoke-virtual {p0, v3, v1}, Lcom/my/target/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 307
    :cond_8
    const-string v0, "event"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 308
    iget-object v1, p0, Lcom/my/target/aa;->aY:Lcom/my/target/aa$a;

    invoke-interface {v1, v0}, Lcom/my/target/aa$a;->k(Ljava/lang/String;)Z

    move-result v0

    goto/16 :goto_0

    .line 310
    :pswitch_9
    if-nez p2, :cond_9

    .line 312
    const-string v1, "playheadEvent params cannot be null"

    invoke-virtual {p0, v3, v1}, Lcom/my/target/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 315
    :cond_9
    const-string v0, "remain"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v0

    double-to-float v0, v0

    .line 316
    const-string v1, "duration"

    invoke-virtual {p2, v1}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    double-to-float v1, v2

    .line 317
    iget-object v2, p0, Lcom/my/target/aa;->aY:Lcom/my/target/aa$a;

    invoke-interface {v2, v0, v1}, Lcom/my/target/aa$a;->a(FF)Z

    move-result v0

    goto/16 :goto_0

    .line 319
    :pswitch_a
    const-string v1, "playVideo is currently unsupported"

    invoke-static {v1}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 322
    :pswitch_b
    const-string v1, "storePicture is currently unsupported"

    invoke-static {v1}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 325
    :pswitch_c
    const-string v1, "createCalendarEvent is currently unsupported"

    invoke-static {v1}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 328
    :pswitch_d
    const-string v1, "Unspecified MRAID Javascript command"

    invoke-virtual {p0, v3, v1}, Lcom/my/target/aa;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 250
    nop

    :sswitch_data_0
    .sparse-switch
        -0x71e3df8e -> :sswitch_8
        -0x706c8659 -> :sswitch_b
        -0x4cd72166 -> :sswitch_3
        -0x37b2634c -> :sswitch_2
        -0x2bba19a0 -> :sswitch_d
        0x0 -> :sswitch_e
        0x34264a -> :sswitch_6
        0x5a5ddf8 -> :sswitch_0
        0x7f3dfe1 -> :sswitch_7
        0x1b5f6cdd -> :sswitch_c
        0x253cb189 -> :sswitch_1
        0x35332378 -> :sswitch_9
        0x51334bef -> :sswitch_5
        0x6037d900 -> :sswitch_4
        0x6b2b2fe6 -> :sswitch_a
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch
.end method

.method public detach()V
    .locals 1

    .prologue
    .line 113
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/my/target/aa;->aZ:Lcom/my/target/bz;

    .line 114
    return-void
.end method

.method public f(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 56
    iget-object v0, p0, Lcom/my/target/aa;->aZ:Lcom/my/target/bz;

    if-nez v0, :cond_0

    .line 58
    const-string v0, "MRAID bridge called setContentHtml before WebView was attached"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 64
    :goto_0
    return-void

    .line 62
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/my/target/aa;->ba:Z

    .line 63
    iget-object v0, p0, Lcom/my/target/aa;->aZ:Lcom/my/target/bz;

    const-string v1, "https://ad.mail.ru/"

    const-string v3, "text/html"

    const-string v4, "UTF-8"

    const/4 v5, 0x0

    move-object v2, p1

    invoke-virtual/range {v0 .. v5}, Lcom/my/target/bz;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public g(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 118
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mraidbridge.setPlacementType("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Lorg/json/JSONObject;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/my/target/aa;->j(Ljava/lang/String;)V

    .line 119
    return-void
.end method

.method public h(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 128
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "mraidbridge.setState("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Lorg/json/JSONObject;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/my/target/aa;->j(Ljava/lang/String;)V

    .line 129
    return-void
.end method

.method public isVisible()Z
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/my/target/aa;->aZ:Lcom/my/target/bz;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/my/target/aa;->aZ:Lcom/my/target/bz;

    invoke-virtual {v0}, Lcom/my/target/bz;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public m()V
    .locals 1

    .prologue
    .line 133
    const-string v0, "mraidbridge.fireReadyEvent()"

    invoke-direct {p0, v0}, Lcom/my/target/aa;->j(Ljava/lang/String;)V

    .line 134
    return-void
.end method

.method n()Lcom/my/target/bz;
    .locals 1

    .prologue
    .line 224
    iget-object v0, p0, Lcom/my/target/aa;->aZ:Lcom/my/target/bz;

    return-object v0
.end method
