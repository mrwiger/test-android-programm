.class final Lcom/google/obf/fu$a;
.super Lcom/google/obf/ew;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/obf/fu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/obf/ew",
        "<",
        "Ljava/util/Map",
        "<TK;TV;>;>;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/obf/fu;

.field private final b:Lcom/google/obf/ew;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/obf/ew",
            "<TK;>;"
        }
    .end annotation
.end field

.field private final c:Lcom/google/obf/ew;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/obf/ew",
            "<TV;>;"
        }
    .end annotation
.end field

.field private final d:Lcom/google/obf/fk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/obf/fk",
            "<+",
            "Ljava/util/Map",
            "<TK;TV;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/obf/fu;Lcom/google/obf/eg;Ljava/lang/reflect/Type;Lcom/google/obf/ew;Ljava/lang/reflect/Type;Lcom/google/obf/ew;Lcom/google/obf/fk;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/obf/eg;",
            "Ljava/lang/reflect/Type;",
            "Lcom/google/obf/ew",
            "<TK;>;",
            "Ljava/lang/reflect/Type;",
            "Lcom/google/obf/ew",
            "<TV;>;",
            "Lcom/google/obf/fk",
            "<+",
            "Ljava/util/Map",
            "<TK;TV;>;>;)V"
        }
    .end annotation

    .prologue
    .line 1
    iput-object p1, p0, Lcom/google/obf/fu$a;->a:Lcom/google/obf/fu;

    invoke-direct {p0}, Lcom/google/obf/ew;-><init>()V

    .line 2
    new-instance v0, Lcom/google/obf/ga;

    invoke-direct {v0, p2, p4, p3}, Lcom/google/obf/ga;-><init>(Lcom/google/obf/eg;Lcom/google/obf/ew;Ljava/lang/reflect/Type;)V

    iput-object v0, p0, Lcom/google/obf/fu$a;->b:Lcom/google/obf/ew;

    .line 3
    new-instance v0, Lcom/google/obf/ga;

    invoke-direct {v0, p2, p6, p5}, Lcom/google/obf/ga;-><init>(Lcom/google/obf/eg;Lcom/google/obf/ew;Ljava/lang/reflect/Type;)V

    iput-object v0, p0, Lcom/google/obf/fu$a;->c:Lcom/google/obf/ew;

    .line 4
    iput-object p7, p0, Lcom/google/obf/fu$a;->d:Lcom/google/obf/fk;

    .line 5
    return-void
.end method

.method private a(Lcom/google/obf/em;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 71
    invoke-virtual {p1}, Lcom/google/obf/em;->i()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 72
    invoke-virtual {p1}, Lcom/google/obf/em;->m()Lcom/google/obf/er;

    move-result-object v0

    .line 73
    invoke-virtual {v0}, Lcom/google/obf/er;->p()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 74
    invoke-virtual {v0}, Lcom/google/obf/er;->a()Ljava/lang/Number;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 81
    :goto_0
    return-object v0

    .line 75
    :cond_0
    invoke-virtual {v0}, Lcom/google/obf/er;->o()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 76
    invoke-virtual {v0}, Lcom/google/obf/er;->f()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 77
    :cond_1
    invoke-virtual {v0}, Lcom/google/obf/er;->q()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 78
    invoke-virtual {v0}, Lcom/google/obf/er;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 79
    :cond_2
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 80
    :cond_3
    invoke-virtual {p1}, Lcom/google/obf/em;->j()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 81
    const-string v0, "null"

    goto :goto_0

    .line 82
    :cond_4
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method


# virtual methods
.method public a(Lcom/google/obf/ge;)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/obf/ge;",
            ")",
            "Ljava/util/Map",
            "<TK;TV;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 6
    invoke-virtual {p1}, Lcom/google/obf/ge;->f()Lcom/google/obf/gf;

    move-result-object v1

    .line 7
    sget-object v0, Lcom/google/obf/gf;->i:Lcom/google/obf/gf;

    if-ne v1, v0, :cond_0

    .line 8
    invoke-virtual {p1}, Lcom/google/obf/ge;->j()V

    .line 9
    const/4 v0, 0x0

    .line 33
    :goto_0
    return-object v0

    .line 10
    :cond_0
    iget-object v0, p0, Lcom/google/obf/fu$a;->d:Lcom/google/obf/fk;

    invoke-interface {v0}, Lcom/google/obf/fk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 11
    sget-object v2, Lcom/google/obf/gf;->a:Lcom/google/obf/gf;

    if-ne v1, v2, :cond_3

    .line 12
    invoke-virtual {p1}, Lcom/google/obf/ge;->a()V

    .line 13
    :goto_1
    invoke-virtual {p1}, Lcom/google/obf/ge;->e()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 14
    invoke-virtual {p1}, Lcom/google/obf/ge;->a()V

    .line 15
    iget-object v1, p0, Lcom/google/obf/fu$a;->b:Lcom/google/obf/ew;

    invoke-virtual {v1, p1}, Lcom/google/obf/ew;->read(Lcom/google/obf/ge;)Ljava/lang/Object;

    move-result-object v1

    .line 16
    iget-object v2, p0, Lcom/google/obf/fu$a;->c:Lcom/google/obf/ew;

    invoke-virtual {v2, p1}, Lcom/google/obf/ew;->read(Lcom/google/obf/ge;)Ljava/lang/Object;

    move-result-object v2

    .line 17
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 18
    if-eqz v2, :cond_1

    .line 19
    new-instance v0, Lcom/google/obf/eu;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "duplicate key: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/obf/eu;-><init>(Ljava/lang/String;)V

    throw v0

    .line 20
    :cond_1
    invoke-virtual {p1}, Lcom/google/obf/ge;->b()V

    goto :goto_1

    .line 22
    :cond_2
    invoke-virtual {p1}, Lcom/google/obf/ge;->b()V

    goto :goto_0

    .line 23
    :cond_3
    invoke-virtual {p1}, Lcom/google/obf/ge;->c()V

    .line 24
    :cond_4
    invoke-virtual {p1}, Lcom/google/obf/ge;->e()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 25
    sget-object v1, Lcom/google/obf/fh;->a:Lcom/google/obf/fh;

    invoke-virtual {v1, p1}, Lcom/google/obf/fh;->a(Lcom/google/obf/ge;)V

    .line 26
    iget-object v1, p0, Lcom/google/obf/fu$a;->b:Lcom/google/obf/ew;

    invoke-virtual {v1, p1}, Lcom/google/obf/ew;->read(Lcom/google/obf/ge;)Ljava/lang/Object;

    move-result-object v1

    .line 27
    iget-object v2, p0, Lcom/google/obf/fu$a;->c:Lcom/google/obf/ew;

    invoke-virtual {v2, p1}, Lcom/google/obf/ew;->read(Lcom/google/obf/ge;)Ljava/lang/Object;

    move-result-object v2

    .line 28
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 29
    if-eqz v2, :cond_4

    .line 30
    new-instance v0, Lcom/google/obf/eu;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "duplicate key: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/obf/eu;-><init>(Ljava/lang/String;)V

    throw v0

    .line 32
    :cond_5
    invoke-virtual {p1}, Lcom/google/obf/ge;->d()V

    goto/16 :goto_0
.end method

.method public a(Lcom/google/obf/gg;Ljava/util/Map;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/obf/gg;",
            "Ljava/util/Map",
            "<TK;TV;>;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 34
    if-nez p2, :cond_0

    .line 35
    invoke-virtual {p1}, Lcom/google/obf/gg;->f()Lcom/google/obf/gg;

    .line 70
    :goto_0
    return-void

    .line 37
    :cond_0
    iget-object v0, p0, Lcom/google/obf/fu$a;->a:Lcom/google/obf/fu;

    iget-boolean v0, v0, Lcom/google/obf/fu;->a:Z

    if-nez v0, :cond_2

    .line 38
    invoke-virtual {p1}, Lcom/google/obf/gg;->d()Lcom/google/obf/gg;

    .line 39
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 40
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/google/obf/gg;->a(Ljava/lang/String;)Lcom/google/obf/gg;

    .line 41
    iget-object v2, p0, Lcom/google/obf/fu$a;->c:Lcom/google/obf/ew;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, p1, v0}, Lcom/google/obf/ew;->write(Lcom/google/obf/gg;Ljava/lang/Object;)V

    goto :goto_1

    .line 43
    :cond_1
    invoke-virtual {p1}, Lcom/google/obf/gg;->e()Lcom/google/obf/gg;

    goto :goto_0

    .line 46
    :cond_2
    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {p2}, Ljava/util/Map;->size()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 47
    new-instance v4, Ljava/util/ArrayList;

    invoke-interface {p2}, Ljava/util/Map;->size()I

    move-result v0

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 48
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v2

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 49
    iget-object v6, p0, Lcom/google/obf/fu$a;->b:Lcom/google/obf/ew;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/obf/ew;->toJsonTree(Ljava/lang/Object;)Lcom/google/obf/em;

    move-result-object v6

    .line 50
    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 51
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 52
    invoke-virtual {v6}, Lcom/google/obf/em;->g()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {v6}, Lcom/google/obf/em;->h()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    const/4 v0, 0x1

    :goto_3
    or-int/2addr v0, v1

    move v1, v0

    .line 53
    goto :goto_2

    :cond_4
    move v0, v2

    .line 52
    goto :goto_3

    .line 54
    :cond_5
    if-eqz v1, :cond_7

    .line 55
    invoke-virtual {p1}, Lcom/google/obf/gg;->b()Lcom/google/obf/gg;

    .line 56
    :goto_4
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_6

    .line 57
    invoke-virtual {p1}, Lcom/google/obf/gg;->b()Lcom/google/obf/gg;

    .line 58
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/em;

    invoke-static {v0, p1}, Lcom/google/obf/fm;->a(Lcom/google/obf/em;Lcom/google/obf/gg;)V

    .line 59
    iget-object v0, p0, Lcom/google/obf/fu$a;->c:Lcom/google/obf/ew;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/obf/ew;->write(Lcom/google/obf/gg;Ljava/lang/Object;)V

    .line 60
    invoke-virtual {p1}, Lcom/google/obf/gg;->c()Lcom/google/obf/gg;

    .line 61
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 62
    :cond_6
    invoke-virtual {p1}, Lcom/google/obf/gg;->c()Lcom/google/obf/gg;

    goto/16 :goto_0

    .line 63
    :cond_7
    invoke-virtual {p1}, Lcom/google/obf/gg;->d()Lcom/google/obf/gg;

    .line 64
    :goto_5
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_8

    .line 65
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/em;

    .line 66
    invoke-direct {p0, v0}, Lcom/google/obf/fu$a;->a(Lcom/google/obf/em;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/obf/gg;->a(Ljava/lang/String;)Lcom/google/obf/gg;

    .line 67
    iget-object v0, p0, Lcom/google/obf/fu$a;->c:Lcom/google/obf/ew;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/obf/ew;->write(Lcom/google/obf/gg;Ljava/lang/Object;)V

    .line 68
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 69
    :cond_8
    invoke-virtual {p1}, Lcom/google/obf/gg;->e()Lcom/google/obf/gg;

    goto/16 :goto_0
.end method

.method public synthetic read(Lcom/google/obf/ge;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 83
    invoke-virtual {p0, p1}, Lcom/google/obf/fu$a;->a(Lcom/google/obf/ge;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public synthetic write(Lcom/google/obf/gg;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 84
    check-cast p2, Ljava/util/Map;

    invoke-virtual {p0, p1, p2}, Lcom/google/obf/fu$a;->a(Lcom/google/obf/gg;Ljava/util/Map;)V

    return-void
.end method
