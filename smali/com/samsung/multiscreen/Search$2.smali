.class Lcom/samsung/multiscreen/Search$2;
.super Ljava/lang/Object;
.source "Search.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/multiscreen/Search;->startDiscovery()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/multiscreen/Search;

.field final synthetic val$provider:Lcom/samsung/multiscreen/SearchProvider;


# direct methods
.method constructor <init>(Lcom/samsung/multiscreen/Search;Lcom/samsung/multiscreen/SearchProvider;)V
    .locals 0
    .param p1, "this$0"    # Lcom/samsung/multiscreen/Search;

    .prologue
    .line 406
    iput-object p1, p0, Lcom/samsung/multiscreen/Search$2;->this$0:Lcom/samsung/multiscreen/Search;

    iput-object p2, p0, Lcom/samsung/multiscreen/Search$2;->val$provider:Lcom/samsung/multiscreen/SearchProvider;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 1

    .prologue
    .line 408
    iget-object v0, p0, Lcom/samsung/multiscreen/Search$2;->val$provider:Lcom/samsung/multiscreen/SearchProvider;

    invoke-virtual {v0}, Lcom/samsung/multiscreen/SearchProvider;->start()V

    .line 409
    iget-object v0, p0, Lcom/samsung/multiscreen/Search$2;->this$0:Lcom/samsung/multiscreen/Search;

    invoke-static {v0}, Lcom/samsung/multiscreen/Search;->access$1200(Lcom/samsung/multiscreen/Search;)Lcom/samsung/multiscreen/Search$SearchListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/samsung/multiscreen/Search$SearchListener;->onStart()V

    .line 410
    iget-object v0, p0, Lcom/samsung/multiscreen/Search$2;->val$provider:Lcom/samsung/multiscreen/SearchProvider;

    invoke-virtual {v0}, Lcom/samsung/multiscreen/SearchProvider;->isSearching()Z

    move-result v0

    if-nez v0, :cond_0

    .line 413
    iget-object v0, p0, Lcom/samsung/multiscreen/Search$2;->this$0:Lcom/samsung/multiscreen/Search;

    invoke-static {v0}, Lcom/samsung/multiscreen/Search;->access$1200(Lcom/samsung/multiscreen/Search;)Lcom/samsung/multiscreen/Search$SearchListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/samsung/multiscreen/Search$SearchListener;->onStop()V

    .line 415
    :cond_0
    return-void
.end method
