.class Lru/inetra/bytefog/service/BytefogLibrary$EventHandler;
.super Landroid/os/Handler;
.source "BytefogLibrary.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/inetra/bytefog/service/BytefogLibrary;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EventHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lru/inetra/bytefog/service/BytefogLibrary;


# direct methods
.method public constructor <init>(Lru/inetra/bytefog/service/BytefogLibrary;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    .line 81
    iput-object p1, p0, Lru/inetra/bytefog/service/BytefogLibrary$EventHandler;->this$0:Lru/inetra/bytefog/service/BytefogLibrary;

    .line 82
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 83
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 87
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 88
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 101
    :cond_0
    :goto_0
    return-void

    .line 90
    :pswitch_0
    sget-object v1, Lru/inetra/bytefog/service/BytefogLibrary;->listener:Lru/inetra/bytefog/service/BytefogLibraryListener;

    if-eqz v1, :cond_0

    .line 91
    sget-object v1, Lru/inetra/bytefog/service/BytefogLibrary;->listener:Lru/inetra/bytefog/service/BytefogLibraryListener;

    iget v2, p1, Landroid/os/Message;->arg1:I

    invoke-interface {v1, v2}, Lru/inetra/bytefog/service/BytefogLibraryListener;->hlsErrorCallback(I)V

    goto :goto_0

    .line 94
    :pswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lru/inetra/bytefog/service/UpdateInfo;

    .line 95
    .local v0, "updateInfo":Lru/inetra/bytefog/service/UpdateInfo;
    sget-object v1, Lru/inetra/bytefog/service/BytefogLibrary;->listener:Lru/inetra/bytefog/service/BytefogLibraryListener;

    if-eqz v1, :cond_0

    .line 96
    sget-object v1, Lru/inetra/bytefog/service/BytefogLibrary;->listener:Lru/inetra/bytefog/service/BytefogLibraryListener;

    invoke-interface {v1, v0}, Lru/inetra/bytefog/service/BytefogLibraryListener;->onUpdateResult(Lru/inetra/bytefog/service/UpdateInfo;)V

    goto :goto_0

    .line 88
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
