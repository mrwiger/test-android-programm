.class public Lru/cn/tv/stb/settings/SettingsAdapter;
.super Landroid/widget/CursorAdapter;
.source "SettingsAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/tv/stb/settings/SettingsAdapter$ViewHolder;
    }
.end annotation


# instance fields
.field private final groupLayout:I

.field private final itemDisableLayout:I

.field private final itemEnabledLayout:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;III)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "c"    # Landroid/database/Cursor;
    .param p3, "itemEnabledLayout"    # I
    .param p4, "itemDisableLayout"    # I
    .param p5, "groupLayout"    # I

    .prologue
    .line 28
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Landroid/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;I)V

    .line 29
    iput p3, p0, Lru/cn/tv/stb/settings/SettingsAdapter;->itemEnabledLayout:I

    .line 30
    iput p4, p0, Lru/cn/tv/stb/settings/SettingsAdapter;->itemDisableLayout:I

    .line 31
    iput p5, p0, Lru/cn/tv/stb/settings/SettingsAdapter;->groupLayout:I

    .line 32
    return-void
.end method


# virtual methods
.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 87
    const/4 v0, 0x0

    return v0
.end method

.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 61
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/tv/stb/settings/SettingsAdapter$ViewHolder;

    .line 63
    .local v0, "holder":Lru/cn/tv/stb/settings/SettingsAdapter$ViewHolder;
    iget-object v2, v0, Lru/cn/tv/stb/settings/SettingsAdapter$ViewHolder;->title:Landroid/widget/TextView;

    if-eqz v2, :cond_0

    .line 64
    const-string v2, "title"

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 65
    .local v1, "title":Ljava/lang/String;
    iget-object v2, v0, Lru/cn/tv/stb/settings/SettingsAdapter$ViewHolder;->title:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 67
    .end local v1    # "title":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public getItemViewType(I)I
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 76
    invoke-virtual {p0, p1}, Lru/cn/tv/stb/settings/SettingsAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 77
    .local v0, "c":Landroid/database/Cursor;
    const-string v1, "item_type"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    return v1
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x3

    return v0
.end method

.method public isEnabled(I)Z
    .locals 2
    .param p1, "position"    # I

    .prologue
    const/4 v0, 0x1

    .line 82
    invoke-virtual {p0, p1}, Lru/cn/tv/stb/settings/SettingsAdapter;->getItemViewType(I)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cursor"    # Landroid/database/Cursor;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v4, 0x0

    .line 36
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 37
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const/4 v2, 0x0

    .line 38
    .local v2, "view":Landroid/view/View;
    const-string v3, "item_type"

    invoke-interface {p2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    packed-switch v3, :pswitch_data_0

    .line 50
    :goto_0
    if-eqz v2, :cond_0

    .line 51
    new-instance v0, Lru/cn/tv/stb/settings/SettingsAdapter$ViewHolder;

    const/4 v3, 0x0

    invoke-direct {v0, v3}, Lru/cn/tv/stb/settings/SettingsAdapter$ViewHolder;-><init>(Lru/cn/tv/stb/settings/SettingsAdapter$1;)V

    .line 52
    .local v0, "holder":Lru/cn/tv/stb/settings/SettingsAdapter$ViewHolder;
    const v3, 0x7f0901d7

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v0, Lru/cn/tv/stb/settings/SettingsAdapter$ViewHolder;->title:Landroid/widget/TextView;

    .line 53
    invoke-virtual {v2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 56
    .end local v0    # "holder":Lru/cn/tv/stb/settings/SettingsAdapter$ViewHolder;
    :cond_0
    return-object v2

    .line 40
    :pswitch_0
    iget v3, p0, Lru/cn/tv/stb/settings/SettingsAdapter;->groupLayout:I

    invoke-virtual {v1, v3, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 41
    goto :goto_0

    .line 43
    :pswitch_1
    iget v3, p0, Lru/cn/tv/stb/settings/SettingsAdapter;->itemEnabledLayout:I

    invoke-virtual {v1, v3, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 44
    goto :goto_0

    .line 46
    :pswitch_2
    iget v3, p0, Lru/cn/tv/stb/settings/SettingsAdapter;->itemDisableLayout:I

    invoke-virtual {v1, v3, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    goto :goto_0

    .line 38
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
