.class Lcom/yandex/metrica/impl/ob/eh;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Lcom/yandex/metrica/impl/ob/dv$a;

.field private b:Ljava/lang/Long;

.field private c:J

.field private d:Landroid/location/Location;


# direct methods
.method public constructor <init>(Lcom/yandex/metrica/impl/ob/dv$a;JLandroid/location/Location;)V
    .locals 6

    .prologue
    .line 30
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/yandex/metrica/impl/ob/eh;-><init>(Lcom/yandex/metrica/impl/ob/dv$a;JLandroid/location/Location;Ljava/lang/Long;)V

    .line 31
    return-void
.end method

.method public constructor <init>(Lcom/yandex/metrica/impl/ob/dv$a;JLandroid/location/Location;Ljava/lang/Long;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/yandex/metrica/impl/ob/eh;->a:Lcom/yandex/metrica/impl/ob/dv$a;

    .line 37
    iput-object p5, p0, Lcom/yandex/metrica/impl/ob/eh;->b:Ljava/lang/Long;

    .line 38
    iput-wide p2, p0, Lcom/yandex/metrica/impl/ob/eh;->c:J

    .line 39
    iput-object p4, p0, Lcom/yandex/metrica/impl/ob/eh;->d:Landroid/location/Location;

    .line 41
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Long;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/eh;->b:Ljava/lang/Long;

    return-object v0
.end method

.method public b()J
    .locals 2

    .prologue
    .line 64
    iget-wide v0, p0, Lcom/yandex/metrica/impl/ob/eh;->c:J

    return-wide v0
.end method

.method public c()Landroid/location/Location;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/eh;->d:Landroid/location/Location;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 46
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "LocationWrapper{collectionMode="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/eh;->a:Lcom/yandex/metrica/impl/ob/dv$a;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mIncrementalId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/eh;->b:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mReceiveTimestamp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/yandex/metrica/impl/ob/eh;->c:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mLocation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/eh;->d:Landroid/location/Location;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
