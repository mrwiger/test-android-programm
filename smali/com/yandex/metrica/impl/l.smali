.class public final Lcom/yandex/metrica/impl/l;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Landroid/content/Context;

.field private b:Landroid/content/ContentValues;

.field private c:Lcom/yandex/metrica/impl/ob/w;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/yandex/metrica/impl/l;->a:Landroid/content/Context;

    .line 45
    return-void
.end method

.method static synthetic a(Lcom/yandex/metrica/impl/l;)Landroid/content/ContentValues;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/yandex/metrica/impl/l;->b:Landroid/content/ContentValues;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/content/ContentValues;)Lcom/yandex/metrica/impl/l;
    .locals 0

    .prologue
    .line 48
    iput-object p1, p0, Lcom/yandex/metrica/impl/l;->b:Landroid/content/ContentValues;

    .line 49
    return-object p0
.end method

.method public a(Lcom/yandex/metrica/impl/ob/w;)Lcom/yandex/metrica/impl/l;
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lcom/yandex/metrica/impl/l;->c:Lcom/yandex/metrica/impl/ob/w;

    .line 54
    return-object p0
.end method

.method public a()V
    .locals 4

    .prologue
    .line 58
    .line 1063
    iget-object v0, p0, Lcom/yandex/metrica/impl/l;->c:Lcom/yandex/metrica/impl/ob/w;

    invoke-interface {v0}, Lcom/yandex/metrica/impl/ob/w;->i()Lcom/yandex/metrica/impl/bb;

    move-result-object v1

    .line 1064
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 1068
    :try_start_0
    const-string v2, "dId"

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/bb;->p()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1069
    const-string v2, "uId"

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/bb;->q()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1070
    const-string v2, "appVer"

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/bb;->n()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1071
    const-string v2, "appBuild"

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/bb;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1072
    const-string v2, "kitVer"

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/bb;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1073
    const-string v2, "clientKitVer"

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/bb;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1074
    const-string v2, "kitBuildNumber"

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/bb;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1075
    const-string v2, "kitBuildType"

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/bb;->h()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1076
    const-string v2, "osVer"

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/bb;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1077
    const-string v2, "osApiLev"

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/bb;->l()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1078
    const-string v2, "lang"

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/bb;->y()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1079
    const-string v2, "root"

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/bb;->r()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1080
    const-string v2, "app_debuggable"

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/bb;->T()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1086
    :goto_0
    iget-object v1, p0, Lcom/yandex/metrica/impl/l;->b:Landroid/content/ContentValues;

    const-string v2, "report_request_parameters"

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    return-void

    .line 1083
    :catch_0
    move-exception v0

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    goto :goto_0
.end method

.method a(Lcom/yandex/metrica/impl/bn;)V
    .locals 4

    .prologue
    .line 198
    iget-object v0, p0, Lcom/yandex/metrica/impl/l;->a:Landroid/content/Context;

    invoke-virtual {p1, v0}, Lcom/yandex/metrica/impl/bn;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 199
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 200
    iget-object v1, p0, Lcom/yandex/metrica/impl/l;->a:Landroid/content/Context;

    invoke-virtual {p1, v1}, Lcom/yandex/metrica/impl/bn;->c(Landroid/content/Context;)I

    move-result v1

    .line 202
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 203
    const-string v3, "ssid"

    invoke-virtual {v2, v3, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 204
    const-string v0, "state"

    invoke-virtual {v2, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 205
    iget-object v0, p0, Lcom/yandex/metrica/impl/l;->b:Landroid/content/ContentValues;

    const-string v1, "wifi_access_point"

    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 210
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public a(Lcom/yandex/metrica/impl/i;Lcom/yandex/metrica/impl/a$a;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 176
    iget-object v0, p0, Lcom/yandex/metrica/impl/l;->b:Landroid/content/ContentValues;

    const-string v3, "name"

    invoke-virtual {p1}, Lcom/yandex/metrica/impl/i;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    iget-object v0, p0, Lcom/yandex/metrica/impl/l;->b:Landroid/content/ContentValues;

    const-string v3, "value"

    invoke-virtual {p1}, Lcom/yandex/metrica/impl/i;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    iget-object v0, p0, Lcom/yandex/metrica/impl/l;->b:Landroid/content/ContentValues;

    const-string v3, "type"

    invoke-virtual {p1}, Lcom/yandex/metrica/impl/i;->c()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 179
    iget-object v0, p0, Lcom/yandex/metrica/impl/l;->b:Landroid/content/ContentValues;

    const-string v3, "custom_type"

    invoke-virtual {p1}, Lcom/yandex/metrica/impl/i;->d()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 180
    iget-object v0, p0, Lcom/yandex/metrica/impl/l;->b:Landroid/content/ContentValues;

    const-string v3, "error_environment"

    invoke-virtual {p1}, Lcom/yandex/metrica/impl/i;->i()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    iget-object v0, p0, Lcom/yandex/metrica/impl/l;->b:Landroid/content/ContentValues;

    const-string v3, "user_info"

    invoke-virtual {p1}, Lcom/yandex/metrica/impl/i;->k()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    iget-object v0, p0, Lcom/yandex/metrica/impl/l;->b:Landroid/content/ContentValues;

    const-string v3, "truncated"

    invoke-virtual {p1}, Lcom/yandex/metrica/impl/i;->o()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 183
    iget-object v3, p0, Lcom/yandex/metrica/impl/l;->b:Landroid/content/ContentValues;

    const-string v4, "connection_type"

    iget-object v5, p0, Lcom/yandex/metrica/impl/l;->a:Landroid/content/Context;

    .line 1248
    const-string v0, "connectivity"

    invoke-virtual {v5, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 2051
    const-string v6, "android.permission.ACCESS_NETWORK_STATE"

    invoke-static {v5, v6}, Lcom/yandex/metrica/impl/am;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v5

    .line 1250
    if-eqz v5, :cond_2

    .line 1251
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 1253
    if-eqz v0, :cond_2

    .line 1254
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v5

    if-ne v5, v1, :cond_1

    move v0, v1

    .line 183
    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2154
    iget-object v0, p0, Lcom/yandex/metrica/impl/l;->b:Landroid/content/ContentValues;

    const-string v1, "app_environment"

    iget-object v3, p2, Lcom/yandex/metrica/impl/a$a;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2158
    iget-object v0, p0, Lcom/yandex/metrica/impl/l;->b:Landroid/content/ContentValues;

    const-string v1, "app_environment_revision"

    iget-wide v4, p2, Lcom/yandex/metrica/impl/a$a;->b:J

    .line 2160
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 2158
    invoke-virtual {v0, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 3096
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 3098
    :try_start_0
    const-string v0, "enabled"

    iget-object v3, p0, Lcom/yandex/metrica/impl/l;->c:Lcom/yandex/metrica/impl/ob/w;

    invoke-interface {v3}, Lcom/yandex/metrica/impl/ob/w;->k()Lcom/yandex/metrica/CounterConfiguration;

    move-result-object v3

    invoke-virtual {v3}, Lcom/yandex/metrica/CounterConfiguration;->m()Z

    move-result v3

    invoke-virtual {v1, v0, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 3100
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/l;->b()Landroid/location/Location;

    move-result-object v3

    .line 3101
    if-eqz v3, :cond_0

    .line 4190
    const-string v0, "lat"

    invoke-virtual {v3}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    invoke-virtual {v1, v0, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 4192
    const-string v0, "lon"

    invoke-virtual {v3}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-virtual {v1, v0, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 4195
    const-string v0, "timestamp"

    invoke-virtual {v3}, Landroid/location/Location;->getTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v1, v0, v4}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 4196
    const-string v4, "precision"

    invoke-virtual {v3}, Landroid/location/Location;->hasAccuracy()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {v3}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    :goto_1
    invoke-virtual {v1, v4, v0}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 4197
    const-string v4, "direction"

    invoke-virtual {v3}, Landroid/location/Location;->hasBearing()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {v3}, Landroid/location/Location;->getBearing()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    :goto_2
    invoke-virtual {v1, v4, v0}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 4198
    const-string v4, "speed"

    invoke-virtual {v3}, Landroid/location/Location;->hasSpeed()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {v3}, Landroid/location/Location;->getSpeed()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    :goto_3
    invoke-virtual {v1, v4, v0}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 4199
    const-string v4, "altitude"

    invoke-virtual {v3}, Landroid/location/Location;->hasAltitude()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {v3}, Landroid/location/Location;->getAltitude()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    :goto_4
    invoke-virtual {v1, v4, v0}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 4200
    const-string v0, "provider"

    invoke-virtual {v3}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/yandex/metrica/impl/bj;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lorg/json/JSONObject;->putOpt(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 3105
    :cond_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/l;->b:Landroid/content/ContentValues;

    const-string v2, "location_info"

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 6165
    :goto_5
    iget-object v0, p0, Lcom/yandex/metrica/impl/l;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/yandex/metrica/impl/ob/gi;->a(Landroid/content/Context;)Lcom/yandex/metrica/impl/ob/gi;

    move-result-object v0

    .line 6166
    new-instance v1, Lcom/yandex/metrica/impl/l$2;

    invoke-direct {v1, p0}, Lcom/yandex/metrica/impl/l$2;-><init>(Lcom/yandex/metrica/impl/l;)V

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/gb;->a(Lcom/yandex/metrica/impl/ob/gk;)V

    .line 7145
    new-instance v1, Lcom/yandex/metrica/impl/l$1;

    invoke-direct {v1, p0}, Lcom/yandex/metrica/impl/l$1;-><init>(Lcom/yandex/metrica/impl/l;)V

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/gb;->a(Lcom/yandex/metrica/impl/ob/gd;)V

    .line 5192
    iget-object v0, p0, Lcom/yandex/metrica/impl/l;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/yandex/metrica/impl/bn;->a(Landroid/content/Context;)Lcom/yandex/metrica/impl/bn;

    move-result-object v0

    .line 8132
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/i;->g()Lorg/json/JSONArray;

    move-result-object v1

    .line 8133
    invoke-virtual {v0}, Lcom/yandex/metrica/impl/bn;->a()Lorg/json/JSONArray;

    move-result-object v2

    .line 8136
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-le v3, v4, :cond_7

    .line 8137
    iget-object v1, p0, Lcom/yandex/metrica/impl/l;->b:Landroid/content/ContentValues;

    const-string v3, "wifi_network_info"

    invoke-virtual {v2}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 5194
    :goto_6
    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/l;->a(Lcom/yandex/metrica/impl/bn;)V

    .line 188
    return-void

    .line 1257
    :cond_1
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    if-nez v0, :cond_2

    .line 1258
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 1262
    :cond_2
    const/4 v0, 0x2

    goto/16 :goto_0

    :cond_3
    move-object v0, v2

    .line 4196
    goto/16 :goto_1

    :cond_4
    move-object v0, v2

    .line 4197
    goto/16 :goto_2

    :cond_5
    move-object v0, v2

    .line 4198
    goto :goto_3

    :cond_6
    move-object v0, v2

    .line 4199
    goto :goto_4

    .line 8139
    :cond_7
    iget-object v2, p0, Lcom/yandex/metrica/impl/l;->b:Landroid/content/ContentValues;

    const-string v3, "wifi_network_info"

    invoke-virtual {v1}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_6

    :catch_0
    move-exception v0

    goto :goto_5
.end method

.method b()Landroid/location/Location;
    .locals 2

    .prologue
    .line 113
    const/4 v0, 0x0

    .line 114
    iget-object v1, p0, Lcom/yandex/metrica/impl/l;->c:Lcom/yandex/metrica/impl/ob/w;

    invoke-interface {v1}, Lcom/yandex/metrica/impl/ob/w;->k()Lcom/yandex/metrica/CounterConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Lcom/yandex/metrica/CounterConfiguration;->m()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 115
    iget-object v0, p0, Lcom/yandex/metrica/impl/l;->c:Lcom/yandex/metrica/impl/ob/w;

    invoke-interface {v0}, Lcom/yandex/metrica/impl/ob/w;->k()Lcom/yandex/metrica/CounterConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/CounterConfiguration;->t()Landroid/location/Location;

    move-result-object v0

    .line 118
    if-nez v0, :cond_0

    .line 119
    iget-object v0, p0, Lcom/yandex/metrica/impl/l;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/yandex/metrica/impl/ob/eb;->a(Landroid/content/Context;)Lcom/yandex/metrica/impl/ob/eb;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/eb;->d()Landroid/location/Location;

    move-result-object v0

    .line 122
    if-nez v0, :cond_0

    .line 123
    iget-object v0, p0, Lcom/yandex/metrica/impl/l;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/yandex/metrica/impl/ob/eb;->a(Landroid/content/Context;)Lcom/yandex/metrica/impl/ob/eb;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/eb;->e()Landroid/location/Location;

    move-result-object v0

    .line 127
    :cond_0
    return-object v0
.end method
