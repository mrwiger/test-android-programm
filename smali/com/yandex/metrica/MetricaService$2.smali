.class Lcom/yandex/metrica/MetricaService$2;
.super Lcom/yandex/metrica/IMetricaService$Stub;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/yandex/metrica/MetricaService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/yandex/metrica/MetricaService;


# direct methods
.method constructor <init>(Lcom/yandex/metrica/MetricaService;)V
    .locals 0
    .param p1, "this$0"    # Lcom/yandex/metrica/MetricaService;

    .prologue
    .line 136
    iput-object p1, p0, Lcom/yandex/metrica/MetricaService$2;->a:Lcom/yandex/metrica/MetricaService;

    invoke-direct {p0}, Lcom/yandex/metrica/IMetricaService$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public reportData(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "data"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 146
    iget-object v0, p0, Lcom/yandex/metrica/MetricaService$2;->a:Lcom/yandex/metrica/MetricaService;

    invoke-static {v0}, Lcom/yandex/metrica/MetricaService;->a(Lcom/yandex/metrica/MetricaService;)Lcom/yandex/metrica/impl/ae;

    move-result-object v0

    invoke-static {}, Lcom/yandex/metrica/MetricaService$2;->getCallingUid()I

    move-result v1

    invoke-interface {v0, v1, p1}, Lcom/yandex/metrica/impl/ae;->a(ILandroid/os/Bundle;)V

    .line 147
    return-void
.end method

.method public reportEvent(Ljava/lang/String;ILjava/lang/String;Landroid/os/Bundle;)V
    .locals 6
    .param p1, "event"    # Ljava/lang/String;
    .param p2, "type"    # I
    .param p3, "value"    # Ljava/lang/String;
    .param p4, "data"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 141
    iget-object v0, p0, Lcom/yandex/metrica/MetricaService$2;->a:Lcom/yandex/metrica/MetricaService;

    invoke-static {v0}, Lcom/yandex/metrica/MetricaService;->a(Lcom/yandex/metrica/MetricaService;)Lcom/yandex/metrica/impl/ae;

    move-result-object v0

    invoke-static {}, Lcom/yandex/metrica/MetricaService$2;->getCallingUid()I

    move-result v1

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-interface/range {v0 .. v5}, Lcom/yandex/metrica/impl/ae;->a(ILjava/lang/String;ILjava/lang/String;Landroid/os/Bundle;)V

    .line 142
    return-void
.end method
