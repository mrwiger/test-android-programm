.class public Lcom/google/obf/gq;
.super Lcom/google/obf/gy;
.source "IMASDK"

# interfaces
.implements Lcom/google/ads/interactivemedia/v3/api/AdsManager;


# instance fields
.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/ads/interactivemedia/v3/impl/data/CompanionData;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/google/obf/hj;Lcom/google/obf/hl;Lcom/google/ads/interactivemedia/v3/api/BaseDisplayContainer;Lcom/google/ads/interactivemedia/v3/api/player/ContentProgressProvider;Ljava/util/List;Ljava/util/SortedSet;Landroid/content/Context;Z)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/obf/hj;",
            "Lcom/google/obf/hl;",
            "Lcom/google/ads/interactivemedia/v3/api/BaseDisplayContainer;",
            "Lcom/google/ads/interactivemedia/v3/api/player/ContentProgressProvider;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;",
            "Ljava/util/SortedSet",
            "<",
            "Ljava/lang/Float;",
            ">;",
            "Landroid/content/Context;",
            "Z)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/ads/interactivemedia/v3/api/AdError;
        }
    .end annotation

    .prologue
    .line 1
    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v11, p8

    move/from16 v12, p9

    invoke-direct/range {v0 .. v12}, Lcom/google/obf/gq;-><init>(Ljava/lang/String;Lcom/google/obf/hj;Lcom/google/obf/hl;Lcom/google/ads/interactivemedia/v3/api/BaseDisplayContainer;Lcom/google/ads/interactivemedia/v3/api/player/ContentProgressProvider;Ljava/util/List;Ljava/util/SortedSet;Lcom/google/obf/ht;Lcom/google/obf/hc;Lcom/google/obf/gi;Landroid/content/Context;Z)V

    .line 2
    return-void
.end method

.method constructor <init>(Ljava/lang/String;Lcom/google/obf/hj;Lcom/google/obf/hl;Lcom/google/ads/interactivemedia/v3/api/BaseDisplayContainer;Lcom/google/ads/interactivemedia/v3/api/player/ContentProgressProvider;Ljava/util/List;Ljava/util/SortedSet;Lcom/google/obf/ht;Lcom/google/obf/hc;Lcom/google/obf/gi;Landroid/content/Context;Z)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/obf/hj;",
            "Lcom/google/obf/hl;",
            "Lcom/google/ads/interactivemedia/v3/api/BaseDisplayContainer;",
            "Lcom/google/ads/interactivemedia/v3/api/player/ContentProgressProvider;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;",
            "Ljava/util/SortedSet",
            "<",
            "Ljava/lang/Float;",
            ">;",
            "Lcom/google/obf/ht;",
            "Lcom/google/obf/hc;",
            "Lcom/google/obf/gi;",
            "Landroid/content/Context;",
            "Z)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/ads/interactivemedia/v3/api/AdError;
        }
    .end annotation

    .prologue
    .line 3
    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object/from16 v7, p10

    move-object/from16 v8, p11

    move/from16 v9, p12

    invoke-direct/range {v2 .. v9}, Lcom/google/obf/gy;-><init>(Ljava/lang/String;Lcom/google/obf/hj;Lcom/google/obf/hl;Lcom/google/ads/interactivemedia/v3/api/BaseDisplayContainer;Lcom/google/obf/gi;Landroid/content/Context;Z)V

    .line 4
    move-object/from16 v0, p6

    iput-object v0, p0, Lcom/google/obf/gq;->h:Ljava/util/List;

    .line 5
    if-eqz p7, :cond_1

    invoke-interface/range {p7 .. p7}, Ljava/util/SortedSet;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 6
    if-nez p5, :cond_0

    .line 7
    new-instance v2, Lcom/google/ads/interactivemedia/v3/api/AdError;

    sget-object v3, Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;->PLAY:Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;

    sget-object v4, Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;->PLAYLIST_NO_CONTENT_TRACKING:Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;

    const-string v5, "Unable to handle cue points, no content progress provider configured."

    invoke-direct {v2, v3, v4, v5}, Lcom/google/ads/interactivemedia/v3/api/AdError;-><init>(Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;Ljava/lang/String;)V

    throw v2

    .line 8
    :cond_0
    if-eqz p9, :cond_2

    .line 9
    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/google/obf/gq;->e:Lcom/google/obf/hc;

    .line 12
    :goto_0
    new-instance v2, Lcom/google/obf/hb;

    move-object/from16 v0, p7

    invoke-direct {v2, p2, v0, p1}, Lcom/google/obf/hb;-><init>(Lcom/google/obf/hj;Ljava/util/SortedSet;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/google/obf/gq;->d:Lcom/google/obf/hb;

    .line 13
    iget-object v2, p0, Lcom/google/obf/gq;->e:Lcom/google/obf/hc;

    iget-object v3, p0, Lcom/google/obf/gq;->d:Lcom/google/obf/hb;

    invoke-virtual {v2, v3}, Lcom/google/obf/hc;->a(Lcom/google/obf/hn$b;)V

    .line 14
    iget-object v2, p0, Lcom/google/obf/gq;->e:Lcom/google/obf/hc;

    invoke-virtual {v2}, Lcom/google/obf/hc;->b()V

    .line 15
    :cond_1
    if-eqz p8, :cond_3

    .line 16
    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/google/obf/gq;->c:Lcom/google/obf/ht;

    .line 23
    :goto_1
    iget-object v2, p0, Lcom/google/obf/gq;->c:Lcom/google/obf/ht;

    invoke-virtual {p0, v2}, Lcom/google/obf/gq;->addAdErrorListener(Lcom/google/ads/interactivemedia/v3/api/AdErrorEvent$AdErrorListener;)V

    .line 24
    iget-object v2, p0, Lcom/google/obf/gq;->c:Lcom/google/obf/ht;

    invoke-virtual {p2, v2, p1}, Lcom/google/obf/hj;->a(Lcom/google/obf/ht;Ljava/lang/String;)V

    .line 25
    return-void

    .line 10
    :cond_2
    new-instance v2, Lcom/google/obf/hc;

    .line 11
    invoke-virtual {p3}, Lcom/google/obf/hl;->a()J

    move-result-wide v4

    invoke-direct {v2, p5, v4, v5}, Lcom/google/obf/hc;-><init>(Lcom/google/ads/interactivemedia/v3/api/player/ContentProgressProvider;J)V

    iput-object v2, p0, Lcom/google/obf/gq;->e:Lcom/google/obf/hc;

    goto :goto_0

    .line 17
    :cond_3
    invoke-virtual {p3}, Lcom/google/obf/hl;->b()Lcom/google/obf/hi$a;

    move-result-object v2

    .line 18
    sget-object v3, Lcom/google/obf/gq$1;->a:[I

    invoke-virtual {v2}, Lcom/google/obf/hi$a;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 21
    new-instance v3, Lcom/google/ads/interactivemedia/v3/api/AdError;

    sget-object v4, Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;->PLAY:Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;

    sget-object v5, Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;->INTERNAL_ERROR:Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;

    const-string v6, "UI style not supported: "

    .line 22
    invoke-virtual {v2}, Lcom/google/obf/hi$a;->name()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_4

    invoke-virtual {v6, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_2
    invoke-direct {v3, v4, v5, v2}, Lcom/google/ads/interactivemedia/v3/api/AdError;-><init>(Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;Ljava/lang/String;)V

    throw v3

    .line 19
    :pswitch_0
    new-instance v2, Lcom/google/obf/hm;

    move-object v7, p4

    check-cast v7, Lcom/google/ads/interactivemedia/v3/api/AdDisplayContainer;

    move-object v3, p1

    move-object v4, p3

    move-object v5, p2

    move-object v6, p0

    move-object/from16 v8, p11

    invoke-direct/range {v2 .. v8}, Lcom/google/obf/hm;-><init>(Ljava/lang/String;Lcom/google/obf/hl;Lcom/google/obf/hj;Lcom/google/obf/gq;Lcom/google/ads/interactivemedia/v3/api/AdDisplayContainer;Landroid/content/Context;)V

    iput-object v2, p0, Lcom/google/obf/gq;->c:Lcom/google/obf/ht;

    goto :goto_1

    .line 22
    :cond_4
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 18
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private b(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/ads/interactivemedia/v3/impl/data/CompanionData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 49
    if-eqz p1, :cond_0

    .line 50
    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Lcom/google/obf/jl;->a(Ljava/util/Collection;)Lcom/google/obf/jl;

    move-result-object v0

    iput-object v0, p0, Lcom/google/obf/gq;->g:Ljava/util/List;

    .line 52
    :goto_0
    return-void

    .line 51
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/obf/gq;->g:Ljava/util/List;

    goto :goto_0
.end method


# virtual methods
.method protected a()V
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/obf/gq;->g:Ljava/util/List;

    .line 27
    invoke-super {p0}, Lcom/google/obf/gy;->a()V

    .line 28
    return-void
.end method

.method public bridge synthetic a(Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 71
    invoke-super {p0, p1, p2, p3}, Lcom/google/obf/gy;->a(Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;ILjava/lang/String;)V

    return-void
.end method

.method public bridge synthetic a(Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 70
    invoke-super {p0, p1, p2, p3}, Lcom/google/obf/gy;->a(Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;Ljava/lang/String;)V

    return-void
.end method

.method public a(Lcom/google/obf/hj$c;)V
    .locals 3

    .prologue
    .line 53
    iget-object v0, p1, Lcom/google/obf/hj$c;->a:Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;

    .line 54
    sget-object v1, Lcom/google/obf/gq$1;->b:[I

    invoke-virtual {v0}, Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 58
    :cond_0
    :goto_0
    sget-object v1, Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;->COMPLETED:Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;

    if-eq v0, v1, :cond_1

    sget-object v1, Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;->SKIPPED:Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventType;

    if-ne v0, v1, :cond_2

    .line 59
    :cond_1
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/obf/gq;->b(Ljava/util/Map;)V

    .line 60
    :cond_2
    invoke-super {p0, p1}, Lcom/google/obf/gy;->a(Lcom/google/obf/hj$c;)V

    .line 61
    return-void

    .line 55
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/obf/gq;->a()V

    .line 56
    iget-boolean v1, p0, Lcom/google/obf/gq;->f:Z

    if-nez v1, :cond_0

    .line 57
    sget-object v1, Lcom/google/obf/hi$c;->destroy:Lcom/google/obf/hi$c;

    invoke-virtual {p0, v1}, Lcom/google/obf/gq;->a(Lcom/google/obf/hi$c;)V

    goto :goto_0

    .line 54
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public a(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/ads/interactivemedia/v3/impl/data/CompanionData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/google/obf/gq;->b(Ljava/util/Map;)V

    .line 64
    return-void
.end method

.method public bridge synthetic addAdErrorListener(Lcom/google/ads/interactivemedia/v3/api/AdErrorEvent$AdErrorListener;)V
    .locals 0

    .prologue
    .line 75
    invoke-super {p0, p1}, Lcom/google/obf/gy;->addAdErrorListener(Lcom/google/ads/interactivemedia/v3/api/AdErrorEvent$AdErrorListener;)V

    return-void
.end method

.method public bridge synthetic addAdEventListener(Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventListener;)V
    .locals 0

    .prologue
    .line 73
    invoke-super {p0, p1}, Lcom/google/obf/gy;->addAdEventListener(Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventListener;)V

    return-void
.end method

.method public destroy()V
    .locals 1

    .prologue
    .line 67
    sget-object v0, Lcom/google/obf/hi$c;->destroy:Lcom/google/obf/hi$c;

    invoke-virtual {p0, v0}, Lcom/google/obf/gq;->a(Lcom/google/obf/hi$c;)V

    .line 68
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/obf/gq;->f:Z

    .line 69
    return-void
.end method

.method public bridge synthetic getAdProgress()Lcom/google/ads/interactivemedia/v3/api/player/VideoProgressUpdate;
    .locals 1

    .prologue
    .line 77
    invoke-super {p0}, Lcom/google/obf/gy;->getAdProgress()Lcom/google/ads/interactivemedia/v3/api/player/VideoProgressUpdate;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getCurrentAd()Lcom/google/ads/interactivemedia/v3/api/Ad;
    .locals 1

    .prologue
    .line 76
    invoke-super {p0}, Lcom/google/obf/gy;->getCurrentAd()Lcom/google/ads/interactivemedia/v3/api/Ad;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic init(Lcom/google/ads/interactivemedia/v3/api/AdsRenderingSettings;)V
    .locals 0

    .prologue
    .line 78
    invoke-super {p0, p1}, Lcom/google/obf/gy;->init(Lcom/google/ads/interactivemedia/v3/api/AdsRenderingSettings;)V

    return-void
.end method

.method public isCustomPlaybackUsed()Z
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/obf/gq;->c:Lcom/google/obf/ht;

    invoke-interface {v0}, Lcom/google/obf/ht;->e()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic removeAdErrorListener(Lcom/google/ads/interactivemedia/v3/api/AdErrorEvent$AdErrorListener;)V
    .locals 0

    .prologue
    .line 74
    invoke-super {p0, p1}, Lcom/google/obf/gy;->removeAdErrorListener(Lcom/google/ads/interactivemedia/v3/api/AdErrorEvent$AdErrorListener;)V

    return-void
.end method

.method public bridge synthetic removeAdEventListener(Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventListener;)V
    .locals 0

    .prologue
    .line 72
    invoke-super {p0, p1}, Lcom/google/obf/gy;->removeAdEventListener(Lcom/google/ads/interactivemedia/v3/api/AdEvent$AdEventListener;)V

    return-void
.end method

.method public start()V
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/google/obf/hi$c;->start:Lcom/google/obf/hi$c;

    invoke-virtual {p0, v0}, Lcom/google/obf/gq;->a(Lcom/google/obf/hi$c;)V

    .line 38
    return-void
.end method
