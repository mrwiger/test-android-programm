.class final Lru/cn/player/exoplayer/ExoTrackInfo;
.super Lru/cn/player/TrackInfo;
.source "ExoTrackInfo.java"


# instance fields
.field final groupIndex:I

.field public final id:Ljava/lang/String;

.field final rendererIndex:I

.field final sampleMimeType:Ljava/lang/String;

.field final trackIndex:I


# direct methods
.method private constructor <init>(IIILcom/google/android/exoplayer2/Format;)V
    .locals 2
    .param p1, "rendererIndex"    # I
    .param p2, "groupIndex"    # I
    .param p3, "trackIndex"    # I
    .param p4, "format"    # Lcom/google/android/exoplayer2/Format;

    .prologue
    .line 21
    iget v0, p4, Lcom/google/android/exoplayer2/Format;->height:I

    iget-object v1, p4, Lcom/google/android/exoplayer2/Format;->language:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lru/cn/player/TrackInfo;-><init>(ILjava/lang/String;)V

    .line 22
    iput p1, p0, Lru/cn/player/exoplayer/ExoTrackInfo;->rendererIndex:I

    .line 23
    iput p2, p0, Lru/cn/player/exoplayer/ExoTrackInfo;->groupIndex:I

    .line 24
    iput p3, p0, Lru/cn/player/exoplayer/ExoTrackInfo;->trackIndex:I

    .line 25
    iget-object v0, p4, Lcom/google/android/exoplayer2/Format;->sampleMimeType:Ljava/lang/String;

    iput-object v0, p0, Lru/cn/player/exoplayer/ExoTrackInfo;->sampleMimeType:Ljava/lang/String;

    .line 26
    iget-object v0, p4, Lcom/google/android/exoplayer2/Format;->id:Ljava/lang/String;

    iput-object v0, p0, Lru/cn/player/exoplayer/ExoTrackInfo;->id:Ljava/lang/String;

    .line 27
    return-void
.end method

.method static createTrackInfo(Lcom/google/android/exoplayer2/Format;III)Lru/cn/player/exoplayer/ExoTrackInfo;
    .locals 1
    .param p0, "format"    # Lcom/google/android/exoplayer2/Format;
    .param p1, "rendererIndex"    # I
    .param p2, "groupIndex"    # I
    .param p3, "trackIndex"    # I

    .prologue
    .line 34
    new-instance v0, Lru/cn/player/exoplayer/ExoTrackInfo;

    invoke-direct {v0, p1, p2, p3, p0}, Lru/cn/player/exoplayer/ExoTrackInfo;-><init>(IIILcom/google/android/exoplayer2/Format;)V

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 39
    if-ne p0, p1, :cond_1

    .line 44
    :cond_0
    :goto_0
    return v1

    .line 40
    :cond_1
    instance-of v3, p1, Lru/cn/player/exoplayer/ExoTrackInfo;

    if-nez v3, :cond_2

    move v1, v2

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 42
    check-cast v0, Lru/cn/player/exoplayer/ExoTrackInfo;

    .line 44
    .local v0, "that":Lru/cn/player/exoplayer/ExoTrackInfo;
    iget v3, p0, Lru/cn/player/exoplayer/ExoTrackInfo;->rendererIndex:I

    iget v4, v0, Lru/cn/player/exoplayer/ExoTrackInfo;->rendererIndex:I

    if-ne v3, v4, :cond_3

    iget v3, p0, Lru/cn/player/exoplayer/ExoTrackInfo;->groupIndex:I

    iget v4, v0, Lru/cn/player/exoplayer/ExoTrackInfo;->groupIndex:I

    if-ne v3, v4, :cond_3

    iget v3, p0, Lru/cn/player/exoplayer/ExoTrackInfo;->trackIndex:I

    iget v4, v0, Lru/cn/player/exoplayer/ExoTrackInfo;->trackIndex:I

    if-eq v3, v4, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 51
    iget v0, p0, Lru/cn/player/exoplayer/ExoTrackInfo;->rendererIndex:I

    .line 52
    .local v0, "result":I
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lru/cn/player/exoplayer/ExoTrackInfo;->groupIndex:I

    add-int v0, v1, v2

    .line 53
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lru/cn/player/exoplayer/ExoTrackInfo;->trackIndex:I

    add-int v0, v1, v2

    .line 54
    return v0
.end method
