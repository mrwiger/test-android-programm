.class Lcom/google/obf/gp$1;
.super Ljava/lang/Object;
.source "IMASDK"

# interfaces
.implements Lcom/google/obf/hj$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/obf/gp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/google/obf/gp;


# direct methods
.method constructor <init>(Lcom/google/obf/gp;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/google/obf/gp$1;->a:Lcom/google/obf/gp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;ILjava/lang/String;)V
    .locals 3

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/obf/gp$1;->a:Lcom/google/obf/gp;

    invoke-static {v0}, Lcom/google/obf/gp;->a(Lcom/google/obf/gp;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 25
    iget-object v0, p0, Lcom/google/obf/gp$1;->a:Lcom/google/obf/gp;

    invoke-static {v0}, Lcom/google/obf/gp;->a(Lcom/google/obf/gp;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ads/interactivemedia/v3/api/AdsRequest;

    invoke-interface {v0}, Lcom/google/ads/interactivemedia/v3/api/AdsRequest;->getUserRequestContext()Ljava/lang/Object;

    move-result-object v0

    .line 27
    :goto_0
    new-instance v1, Lcom/google/obf/gk;

    new-instance v2, Lcom/google/ads/interactivemedia/v3/api/AdError;

    invoke-direct {v2, p2, p3, p4}, Lcom/google/ads/interactivemedia/v3/api/AdError;-><init>(Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;ILjava/lang/String;)V

    invoke-direct {v1, v2, v0}, Lcom/google/obf/gk;-><init>(Lcom/google/ads/interactivemedia/v3/api/AdError;Ljava/lang/Object;)V

    .line 28
    iget-object v0, p0, Lcom/google/obf/gp$1;->a:Lcom/google/obf/gp;

    invoke-static {v0}, Lcom/google/obf/gp;->d(Lcom/google/obf/gp;)Lcom/google/obf/he;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/obf/he;->a(Lcom/google/ads/interactivemedia/v3/api/AdErrorEvent;)V

    .line 29
    return-void

    .line 26
    :cond_0
    iget-object v0, p0, Lcom/google/obf/gp$1;->a:Lcom/google/obf/gp;

    invoke-static {v0}, Lcom/google/obf/gp;->e(Lcom/google/obf/gp;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ads/interactivemedia/v3/api/StreamRequest;

    invoke-interface {v0}, Lcom/google/ads/interactivemedia/v3/api/StreamRequest;->getUserRequestContext()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/obf/gp$1;->a:Lcom/google/obf/gp;

    invoke-static {v0}, Lcom/google/obf/gp;->a(Lcom/google/obf/gp;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 31
    iget-object v0, p0, Lcom/google/obf/gp$1;->a:Lcom/google/obf/gp;

    invoke-static {v0}, Lcom/google/obf/gp;->a(Lcom/google/obf/gp;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ads/interactivemedia/v3/api/AdsRequest;

    invoke-interface {v0}, Lcom/google/ads/interactivemedia/v3/api/AdsRequest;->getUserRequestContext()Ljava/lang/Object;

    move-result-object v0

    .line 33
    :goto_0
    new-instance v1, Lcom/google/obf/gk;

    new-instance v2, Lcom/google/ads/interactivemedia/v3/api/AdError;

    invoke-direct {v2, p2, p3, p4}, Lcom/google/ads/interactivemedia/v3/api/AdError;-><init>(Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;Ljava/lang/String;)V

    invoke-direct {v1, v2, v0}, Lcom/google/obf/gk;-><init>(Lcom/google/ads/interactivemedia/v3/api/AdError;Ljava/lang/Object;)V

    .line 34
    iget-object v0, p0, Lcom/google/obf/gp$1;->a:Lcom/google/obf/gp;

    invoke-static {v0}, Lcom/google/obf/gp;->d(Lcom/google/obf/gp;)Lcom/google/obf/he;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/obf/he;->a(Lcom/google/ads/interactivemedia/v3/api/AdErrorEvent;)V

    .line 35
    return-void

    .line 32
    :cond_0
    iget-object v0, p0, Lcom/google/obf/gp$1;->a:Lcom/google/obf/gp;

    invoke-static {v0}, Lcom/google/obf/gp;->e(Lcom/google/obf/gp;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ads/interactivemedia/v3/api/StreamRequest;

    invoke-interface {v0}, Lcom/google/ads/interactivemedia/v3/api/StreamRequest;->getUserRequestContext()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Lcom/google/obf/hl;Ljava/lang/String;Z)V
    .locals 10

    .prologue
    .line 14
    iget-object v0, p0, Lcom/google/obf/gp$1;->a:Lcom/google/obf/gp;

    invoke-static {v0}, Lcom/google/obf/gp;->e(Lcom/google/obf/gp;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/ads/interactivemedia/v3/api/StreamRequest;

    .line 15
    :try_start_0
    iget-object v8, p0, Lcom/google/obf/gp$1;->a:Lcom/google/obf/gp;

    new-instance v9, Lcom/google/obf/gr;

    new-instance v0, Lcom/google/obf/hq;

    iget-object v1, p0, Lcom/google/obf/gp$1;->a:Lcom/google/obf/gp;

    .line 16
    invoke-static {v1}, Lcom/google/obf/gp;->b(Lcom/google/obf/gp;)Lcom/google/obf/hj;

    move-result-object v2

    iget-object v1, p0, Lcom/google/obf/gp$1;->a:Lcom/google/obf/gp;

    .line 17
    invoke-static {v1}, Lcom/google/obf/gp;->c(Lcom/google/obf/gp;)Landroid/content/Context;

    move-result-object v5

    move-object v1, p1

    move-object v3, p2

    move-object v6, p3

    move v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/obf/hq;-><init>(Ljava/lang/String;Lcom/google/obf/hj;Lcom/google/obf/hl;Lcom/google/ads/interactivemedia/v3/api/StreamRequest;Landroid/content/Context;Ljava/lang/String;Z)V

    .line 18
    invoke-interface {v4}, Lcom/google/ads/interactivemedia/v3/api/StreamRequest;->getUserRequestContext()Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v9, v0, v1}, Lcom/google/obf/gr;-><init>(Lcom/google/ads/interactivemedia/v3/api/StreamManager;Ljava/lang/Object;)V

    .line 19
    invoke-virtual {v8, v9}, Lcom/google/obf/gp;->a(Lcom/google/ads/interactivemedia/v3/api/AdsManagerLoadedEvent;)V
    :try_end_0
    .catch Lcom/google/ads/interactivemedia/v3/api/AdError; {:try_start_0 .. :try_end_0} :catch_0

    .line 23
    :goto_0
    return-void

    .line 21
    :catch_0
    move-exception v0

    .line 22
    iget-object v1, p0, Lcom/google/obf/gp$1;->a:Lcom/google/obf/gp;

    invoke-static {v1}, Lcom/google/obf/gp;->d(Lcom/google/obf/gp;)Lcom/google/obf/he;

    move-result-object v1

    new-instance v2, Lcom/google/obf/gk;

    invoke-interface {v4}, Lcom/google/ads/interactivemedia/v3/api/StreamRequest;->getUserRequestContext()Ljava/lang/Object;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Lcom/google/obf/gk;-><init>(Lcom/google/ads/interactivemedia/v3/api/AdError;Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Lcom/google/obf/he;->a(Lcom/google/ads/interactivemedia/v3/api/AdErrorEvent;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Lcom/google/obf/hl;Ljava/util/List;Ljava/util/SortedSet;Z)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/obf/hl;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;",
            "Ljava/util/SortedSet",
            "<",
            "Ljava/lang/Float;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 2
    iget-object v0, p0, Lcom/google/obf/gp$1;->a:Lcom/google/obf/gp;

    invoke-static {v0}, Lcom/google/obf/gp;->a(Lcom/google/obf/gp;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/google/ads/interactivemedia/v3/api/AdsRequest;

    .line 3
    :try_start_0
    iget-object v11, p0, Lcom/google/obf/gp$1;->a:Lcom/google/obf/gp;

    new-instance v12, Lcom/google/obf/gr;

    new-instance v0, Lcom/google/obf/gq;

    iget-object v1, p0, Lcom/google/obf/gp$1;->a:Lcom/google/obf/gp;

    .line 4
    invoke-static {v1}, Lcom/google/obf/gp;->b(Lcom/google/obf/gp;)Lcom/google/obf/hj;

    move-result-object v2

    .line 5
    invoke-interface {v10}, Lcom/google/ads/interactivemedia/v3/api/AdsRequest;->getAdDisplayContainer()Lcom/google/ads/interactivemedia/v3/api/AdDisplayContainer;

    move-result-object v4

    .line 6
    invoke-interface {v10}, Lcom/google/ads/interactivemedia/v3/api/AdsRequest;->getContentProgressProvider()Lcom/google/ads/interactivemedia/v3/api/player/ContentProgressProvider;

    move-result-object v5

    iget-object v1, p0, Lcom/google/obf/gp$1;->a:Lcom/google/obf/gp;

    .line 7
    invoke-static {v1}, Lcom/google/obf/gp;->c(Lcom/google/obf/gp;)Landroid/content/Context;

    move-result-object v8

    move-object v1, p1

    move-object v3, p2

    move-object/from16 v6, p3

    move-object/from16 v7, p4

    move/from16 v9, p5

    invoke-direct/range {v0 .. v9}, Lcom/google/obf/gq;-><init>(Ljava/lang/String;Lcom/google/obf/hj;Lcom/google/obf/hl;Lcom/google/ads/interactivemedia/v3/api/BaseDisplayContainer;Lcom/google/ads/interactivemedia/v3/api/player/ContentProgressProvider;Ljava/util/List;Ljava/util/SortedSet;Landroid/content/Context;Z)V

    .line 8
    invoke-interface {v10}, Lcom/google/ads/interactivemedia/v3/api/AdsRequest;->getUserRequestContext()Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v12, v0, v1}, Lcom/google/obf/gr;-><init>(Lcom/google/ads/interactivemedia/v3/api/AdsManager;Ljava/lang/Object;)V

    .line 9
    invoke-virtual {v11, v12}, Lcom/google/obf/gp;->a(Lcom/google/ads/interactivemedia/v3/api/AdsManagerLoadedEvent;)V
    :try_end_0
    .catch Lcom/google/ads/interactivemedia/v3/api/AdError; {:try_start_0 .. :try_end_0} :catch_0

    .line 13
    :goto_0
    return-void

    .line 11
    :catch_0
    move-exception v0

    .line 12
    iget-object v1, p0, Lcom/google/obf/gp$1;->a:Lcom/google/obf/gp;

    invoke-static {v1}, Lcom/google/obf/gp;->d(Lcom/google/obf/gp;)Lcom/google/obf/he;

    move-result-object v1

    new-instance v2, Lcom/google/obf/gk;

    invoke-interface {v10}, Lcom/google/ads/interactivemedia/v3/api/AdsRequest;->getUserRequestContext()Ljava/lang/Object;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Lcom/google/obf/gk;-><init>(Lcom/google/ads/interactivemedia/v3/api/AdError;Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Lcom/google/obf/he;->a(Lcom/google/ads/interactivemedia/v3/api/AdErrorEvent;)V

    goto :goto_0
.end method
