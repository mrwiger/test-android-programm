.class public final Lcom/my/target/core/engines/g;
.super Ljava/lang/Object;
.source "InterstitialSliderAdEngine.java"

# interfaces
.implements Lcom/my/target/br$a;
.implements Lcom/my/target/common/MyTargetActivity$ActivityEngine;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/core/engines/g$a;
    }
.end annotation


# instance fields
.field private b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/my/target/common/MyTargetActivity;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/my/target/br;",
            ">;"
        }
    .end annotation
.end field

.field private final m:Lcom/my/target/ads/InterstitialSliderAd;

.field private final n:Lcom/my/target/dw;

.field private final o:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private p:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/my/target/core/presenters/g;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/my/target/ads/InterstitialSliderAd;Lcom/my/target/dw;)V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object p1, p0, Lcom/my/target/core/engines/g;->m:Lcom/my/target/ads/InterstitialSliderAd;

    .line 55
    iput-object p2, p0, Lcom/my/target/core/engines/g;->n:Lcom/my/target/dw;

    .line 56
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/my/target/core/engines/g;->o:Ljava/util/ArrayList;

    .line 57
    return-void
.end method

.method public static a(Lcom/my/target/ads/InterstitialSliderAd;Lcom/my/target/dw;)Lcom/my/target/core/engines/g;
    .locals 1

    .prologue
    .line 41
    new-instance v0, Lcom/my/target/core/engines/g;

    invoke-direct {v0, p0, p1}, Lcom/my/target/core/engines/g;-><init>(Lcom/my/target/ads/InterstitialSliderAd;Lcom/my/target/dw;)V

    return-object v0
.end method

.method static synthetic a(Lcom/my/target/core/engines/g;)Ljava/lang/ref/WeakReference;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/my/target/core/engines/g;->p:Ljava/lang/ref/WeakReference;

    return-object v0
.end method

.method private a(Landroid/widget/FrameLayout;)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 203
    invoke-virtual {p1}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/core/presenters/g;->f(Landroid/content/Context;)Lcom/my/target/core/presenters/g;

    move-result-object v0

    .line 204
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/my/target/core/engines/g;->p:Ljava/lang/ref/WeakReference;

    .line 205
    new-instance v1, Lcom/my/target/core/engines/g$a;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/my/target/core/engines/g$a;-><init>(Lcom/my/target/core/engines/g;B)V

    invoke-virtual {v0, v1}, Lcom/my/target/core/presenters/g;->a(Lcom/my/target/core/presenters/g$a;)V

    .line 206
    iget-object v1, p0, Lcom/my/target/core/engines/g;->n:Lcom/my/target/dw;

    invoke-virtual {v0, v1}, Lcom/my/target/core/presenters/g;->a(Lcom/my/target/dw;)V

    .line 207
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 208
    invoke-virtual {v0}, Lcom/my/target/core/presenters/g;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1, v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 209
    return-void
.end method

.method static synthetic b(Lcom/my/target/core/engines/g;)Lcom/my/target/ads/InterstitialSliderAd;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/my/target/core/engines/g;->m:Lcom/my/target/ads/InterstitialSliderAd;

    return-object v0
.end method

.method static synthetic c(Lcom/my/target/core/engines/g;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/my/target/core/engines/g;->o:Ljava/util/ArrayList;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/my/target/br;Landroid/widget/FrameLayout;)V
    .locals 2
    .param p1, "dialog"    # Lcom/my/target/br;
    .param p2, "rootLayout"    # Landroid/widget/FrameLayout;

    .prologue
    .line 170
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/my/target/core/engines/g;->c:Ljava/lang/ref/WeakReference;

    .line 171
    iget-object v0, p0, Lcom/my/target/core/engines/g;->m:Lcom/my/target/ads/InterstitialSliderAd;

    invoke-virtual {v0}, Lcom/my/target/ads/InterstitialSliderAd;->isHideStatusBarInDialog()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 173
    invoke-virtual {p1}, Lcom/my/target/br;->aT()V

    .line 175
    :cond_0
    invoke-direct {p0, p2}, Lcom/my/target/core/engines/g;->a(Landroid/widget/FrameLayout;)V

    .line 176
    iget-object v0, p0, Lcom/my/target/core/engines/g;->m:Lcom/my/target/ads/InterstitialSliderAd;

    invoke-virtual {v0}, Lcom/my/target/ads/InterstitialSliderAd;->getListener()Lcom/my/target/ads/InterstitialSliderAd$InterstitialSliderAdListener;

    move-result-object v0

    .line 177
    if-eqz v0, :cond_1

    .line 179
    iget-object v1, p0, Lcom/my/target/core/engines/g;->m:Lcom/my/target/ads/InterstitialSliderAd;

    invoke-interface {v0, v1}, Lcom/my/target/ads/InterstitialSliderAd$InterstitialSliderAdListener;->onDisplay(Lcom/my/target/ads/InterstitialSliderAd;)V

    .line 181
    :cond_1
    return-void
.end method

.method public final aU()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 186
    iput-object v0, p0, Lcom/my/target/core/engines/g;->p:Ljava/lang/ref/WeakReference;

    .line 187
    iput-object v0, p0, Lcom/my/target/core/engines/g;->c:Ljava/lang/ref/WeakReference;

    .line 188
    iget-object v0, p0, Lcom/my/target/core/engines/g;->m:Lcom/my/target/ads/InterstitialSliderAd;

    invoke-virtual {v0}, Lcom/my/target/ads/InterstitialSliderAd;->getListener()Lcom/my/target/ads/InterstitialSliderAd$InterstitialSliderAdListener;

    move-result-object v0

    .line 189
    if-eqz v0, :cond_0

    .line 191
    iget-object v1, p0, Lcom/my/target/core/engines/g;->m:Lcom/my/target/ads/InterstitialSliderAd;

    invoke-interface {v0, v1}, Lcom/my/target/ads/InterstitialSliderAd$InterstitialSliderAdListener;->onDismiss(Lcom/my/target/ads/InterstitialSliderAd;)V

    .line 193
    :cond_0
    return-void
.end method

.method public final dismiss()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 85
    iget-object v0, p0, Lcom/my/target/core/engines/g;->b:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_1

    move-object v0, v1

    .line 86
    :goto_0
    if-eqz v0, :cond_2

    .line 88
    invoke-virtual {v0}, Lcom/my/target/common/MyTargetActivity;->finish()V

    .line 97
    :cond_0
    :goto_1
    return-void

    .line 85
    :cond_1
    iget-object v0, p0, Lcom/my/target/core/engines/g;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/common/MyTargetActivity;

    goto :goto_0

    .line 92
    :cond_2
    iget-object v0, p0, Lcom/my/target/core/engines/g;->c:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_3

    move-object v0, v1

    .line 93
    :goto_2
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/my/target/br;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 95
    invoke-virtual {v0}, Lcom/my/target/br;->dismiss()V

    goto :goto_1

    .line 92
    :cond_3
    iget-object v0, p0, Lcom/my/target/core/engines/g;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/br;

    goto :goto_2
.end method

.method public final i(Z)V
    .locals 0

    .prologue
    .line 199
    return-void
.end method

.method public final onActivityBackPressed()Z
    .locals 1

    .prologue
    .line 158
    const/4 v0, 0x1

    return v0
.end method

.method public final onActivityCreate(Lcom/my/target/common/MyTargetActivity;Landroid/content/Intent;Landroid/widget/FrameLayout;)V
    .locals 2
    .param p1, "activity"    # Lcom/my/target/common/MyTargetActivity;
    .param p3, "rootLayout"    # Landroid/widget/FrameLayout;

    .prologue
    const/16 v1, 0x400

    .line 109
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/my/target/core/engines/g;->b:Ljava/lang/ref/WeakReference;

    .line 110
    const v0, 0x1030006

    invoke-virtual {p1, v0}, Lcom/my/target/common/MyTargetActivity;->setTheme(I)V

    .line 111
    invoke-virtual {p1}, Lcom/my/target/common/MyTargetActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Landroid/view/Window;->setFlags(II)V

    .line 113
    invoke-direct {p0, p3}, Lcom/my/target/core/engines/g;->a(Landroid/widget/FrameLayout;)V

    .line 114
    iget-object v0, p0, Lcom/my/target/core/engines/g;->m:Lcom/my/target/ads/InterstitialSliderAd;

    invoke-virtual {v0}, Lcom/my/target/ads/InterstitialSliderAd;->getListener()Lcom/my/target/ads/InterstitialSliderAd$InterstitialSliderAdListener;

    move-result-object v0

    .line 115
    if-eqz v0, :cond_0

    .line 117
    iget-object v1, p0, Lcom/my/target/core/engines/g;->m:Lcom/my/target/ads/InterstitialSliderAd;

    invoke-interface {v0, v1}, Lcom/my/target/ads/InterstitialSliderAd$InterstitialSliderAdListener;->onDisplay(Lcom/my/target/ads/InterstitialSliderAd;)V

    .line 119
    :cond_0
    return-void
.end method

.method public final onActivityDestroy()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 146
    iput-object v0, p0, Lcom/my/target/core/engines/g;->p:Ljava/lang/ref/WeakReference;

    .line 147
    iput-object v0, p0, Lcom/my/target/core/engines/g;->b:Ljava/lang/ref/WeakReference;

    .line 148
    iget-object v0, p0, Lcom/my/target/core/engines/g;->m:Lcom/my/target/ads/InterstitialSliderAd;

    invoke-virtual {v0}, Lcom/my/target/ads/InterstitialSliderAd;->getListener()Lcom/my/target/ads/InterstitialSliderAd$InterstitialSliderAdListener;

    move-result-object v0

    .line 149
    if-eqz v0, :cond_0

    .line 151
    iget-object v1, p0, Lcom/my/target/core/engines/g;->m:Lcom/my/target/ads/InterstitialSliderAd;

    invoke-interface {v0, v1}, Lcom/my/target/ads/InterstitialSliderAd$InterstitialSliderAdListener;->onDismiss(Lcom/my/target/ads/InterstitialSliderAd;)V

    .line 153
    :cond_0
    return-void
.end method

.method public final onActivityOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 164
    const/4 v0, 0x0

    return v0
.end method

.method public final onActivityPause()V
    .locals 0

    .prologue
    .line 136
    return-void
.end method

.method public final onActivityResume()V
    .locals 0

    .prologue
    .line 141
    return-void
.end method

.method public final onActivityStart()V
    .locals 0

    .prologue
    .line 125
    return-void
.end method

.method public final onActivityStop()V
    .locals 0

    .prologue
    .line 131
    return-void
.end method

.method public final showDialog(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/my/target/core/engines/g;->c:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 73
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/my/target/br;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 75
    const-string v0, "InterstitialSliderAdEngine.showDialog: dialog already showing"

    invoke-static {v0}, Lcom/my/target/g;->c(Ljava/lang/String;)V

    .line 81
    :goto_1
    return-void

    .line 72
    :cond_0
    iget-object v0, p0, Lcom/my/target/core/engines/g;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/br;

    goto :goto_0

    .line 79
    :cond_1
    invoke-static {p0, p1}, Lcom/my/target/br;->a(Lcom/my/target/br$a;Landroid/content/Context;)Lcom/my/target/br;

    move-result-object v0

    .line 80
    invoke-virtual {v0}, Lcom/my/target/br;->show()V

    goto :goto_1
.end method
