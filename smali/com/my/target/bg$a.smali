.class interface abstract Lcom/my/target/bg$a;
.super Ljava/lang/Object;
.source "CommonVideoParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/my/target/bg;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x608
    name = "a"
.end annotation


# static fields
.field public static final HEIGHT:Ljava/lang/String; = "height"

.field public static final WIDTH:Ljava/lang/String; = "width"

.field public static final eA:Ljava/lang/String; = "hasPause"

.field public static final et:Ljava/lang/String; = "pointP"

.field public static final eu:Ljava/lang/String; = "point"

.field public static final ev:Ljava/lang/String; = "allowClose"

.field public static final ew:Ljava/lang/String; = "allowCloseDelay"

.field public static final fh:Ljava/lang/String; = "showPlayerControls"

.field public static final fi:Ljava/lang/String; = "replayActionText"

.field public static final fj:Ljava/lang/String; = "closeDelayActionText"

.field public static final fk:Ljava/lang/String; = "closeActionText"

.field public static final fl:Ljava/lang/String; = "automute"

.field public static final fm:Ljava/lang/String; = "autoplay"

.field public static final fn:Ljava/lang/String; = "hasCtaButton"

.field public static final fo:Ljava/lang/String; = "previewLink"

.field public static final fp:Ljava/lang/String; = "previewWidth"

.field public static final fq:Ljava/lang/String; = "previewHeight"

.field public static final fr:Ljava/lang/String; = "mediafiles"

.field public static final fs:Ljava/lang/String; = "src"

.field public static final ft:Ljava/lang/String; = "bitrate"

.field public static final fu:Ljava/lang/String; = "allowReplay"

.field public static final fv:Ljava/lang/String; = "allowBackButton"
