.class final enum Lru/cn/tv/playlists/UserPlaylistAddEditDialog$DialogTypes;
.super Ljava/lang/Enum;
.source "UserPlaylistAddEditDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/tv/playlists/UserPlaylistAddEditDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "DialogTypes"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lru/cn/tv/playlists/UserPlaylistAddEditDialog$DialogTypes;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lru/cn/tv/playlists/UserPlaylistAddEditDialog$DialogTypes;

.field public static final enum editplaylist:Lru/cn/tv/playlists/UserPlaylistAddEditDialog$DialogTypes;

.field public static final enum newPlaylist:Lru/cn/tv/playlists/UserPlaylistAddEditDialog$DialogTypes;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 35
    new-instance v0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog$DialogTypes;

    const-string v1, "newPlaylist"

    invoke-direct {v0, v1, v2}, Lru/cn/tv/playlists/UserPlaylistAddEditDialog$DialogTypes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog$DialogTypes;->newPlaylist:Lru/cn/tv/playlists/UserPlaylistAddEditDialog$DialogTypes;

    new-instance v0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog$DialogTypes;

    const-string v1, "editplaylist"

    invoke-direct {v0, v1, v3}, Lru/cn/tv/playlists/UserPlaylistAddEditDialog$DialogTypes;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog$DialogTypes;->editplaylist:Lru/cn/tv/playlists/UserPlaylistAddEditDialog$DialogTypes;

    .line 34
    const/4 v0, 0x2

    new-array v0, v0, [Lru/cn/tv/playlists/UserPlaylistAddEditDialog$DialogTypes;

    sget-object v1, Lru/cn/tv/playlists/UserPlaylistAddEditDialog$DialogTypes;->newPlaylist:Lru/cn/tv/playlists/UserPlaylistAddEditDialog$DialogTypes;

    aput-object v1, v0, v2

    sget-object v1, Lru/cn/tv/playlists/UserPlaylistAddEditDialog$DialogTypes;->editplaylist:Lru/cn/tv/playlists/UserPlaylistAddEditDialog$DialogTypes;

    aput-object v1, v0, v3

    sput-object v0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog$DialogTypes;->$VALUES:[Lru/cn/tv/playlists/UserPlaylistAddEditDialog$DialogTypes;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lru/cn/tv/playlists/UserPlaylistAddEditDialog$DialogTypes;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 34
    const-class v0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog$DialogTypes;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog$DialogTypes;

    return-object v0
.end method

.method public static values()[Lru/cn/tv/playlists/UserPlaylistAddEditDialog$DialogTypes;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lru/cn/tv/playlists/UserPlaylistAddEditDialog$DialogTypes;->$VALUES:[Lru/cn/tv/playlists/UserPlaylistAddEditDialog$DialogTypes;

    invoke-virtual {v0}, [Lru/cn/tv/playlists/UserPlaylistAddEditDialog$DialogTypes;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lru/cn/tv/playlists/UserPlaylistAddEditDialog$DialogTypes;

    return-object v0
.end method
