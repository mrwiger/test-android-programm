.class public final Lcom/google/obf/ff;
.super Ljava/lang/Object;
.source "IMASDK"


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/reflect/Type;",
            "Lcom/google/obf/ei",
            "<*>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/reflect/Type;",
            "Lcom/google/obf/ei",
            "<*>;>;)V"
        }
    .end annotation

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    iput-object p1, p0, Lcom/google/obf/ff;->a:Ljava/util/Map;

    .line 3
    return-void
.end method

.method private a(Ljava/lang/Class;)Lcom/google/obf/fk;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<-TT;>;)",
            "Lcom/google/obf/fk",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 20
    const/4 v0, 0x0

    :try_start_0
    new-array v0, v0, [Ljava/lang/Class;

    invoke-virtual {p1, v0}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v1

    .line 21
    invoke-virtual {v1}, Ljava/lang/reflect/Constructor;->isAccessible()Z

    move-result v0

    if-nez v0, :cond_0

    .line 22
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Ljava/lang/reflect/Constructor;->setAccessible(Z)V

    .line 23
    :cond_0
    new-instance v0, Lcom/google/obf/ff$8;

    invoke-direct {v0, p0, v1}, Lcom/google/obf/ff$8;-><init>(Lcom/google/obf/ff;Ljava/lang/reflect/Constructor;)V
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    .line 25
    :goto_0
    return-object v0

    .line 24
    :catch_0
    move-exception v0

    .line 25
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/reflect/Type;Ljava/lang/Class;)Lcom/google/obf/fk;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/reflect/Type;",
            "Ljava/lang/Class",
            "<-TT;>;)",
            "Lcom/google/obf/fk",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 26
    const-class v0, Ljava/util/Collection;

    invoke-virtual {v0, p2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 27
    const-class v0, Ljava/util/SortedSet;

    invoke-virtual {v0, p2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 28
    new-instance v0, Lcom/google/obf/ff$9;

    invoke-direct {v0, p0}, Lcom/google/obf/ff$9;-><init>(Lcom/google/obf/ff;)V

    .line 48
    :goto_0
    return-object v0

    .line 29
    :cond_0
    const-class v0, Ljava/util/EnumSet;

    invoke-virtual {v0, p2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 30
    new-instance v0, Lcom/google/obf/ff$10;

    invoke-direct {v0, p0, p1}, Lcom/google/obf/ff$10;-><init>(Lcom/google/obf/ff;Ljava/lang/reflect/Type;)V

    goto :goto_0

    .line 31
    :cond_1
    const-class v0, Ljava/util/Set;

    invoke-virtual {v0, p2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 32
    new-instance v0, Lcom/google/obf/ff$11;

    invoke-direct {v0, p0}, Lcom/google/obf/ff$11;-><init>(Lcom/google/obf/ff;)V

    goto :goto_0

    .line 33
    :cond_2
    const-class v0, Ljava/util/Queue;

    invoke-virtual {v0, p2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 34
    new-instance v0, Lcom/google/obf/ff$12;

    invoke-direct {v0, p0}, Lcom/google/obf/ff$12;-><init>(Lcom/google/obf/ff;)V

    goto :goto_0

    .line 35
    :cond_3
    new-instance v0, Lcom/google/obf/ff$13;

    invoke-direct {v0, p0}, Lcom/google/obf/ff$13;-><init>(Lcom/google/obf/ff;)V

    goto :goto_0

    .line 36
    :cond_4
    const-class v0, Ljava/util/Map;

    invoke-virtual {v0, p2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 37
    const-class v0, Ljava/util/concurrent/ConcurrentNavigableMap;

    invoke-virtual {v0, p2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 38
    new-instance v0, Lcom/google/obf/ff$14;

    invoke-direct {v0, p0}, Lcom/google/obf/ff$14;-><init>(Lcom/google/obf/ff;)V

    goto :goto_0

    .line 39
    :cond_5
    const-class v0, Ljava/util/concurrent/ConcurrentMap;

    invoke-virtual {v0, p2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 40
    new-instance v0, Lcom/google/obf/ff$2;

    invoke-direct {v0, p0}, Lcom/google/obf/ff$2;-><init>(Lcom/google/obf/ff;)V

    goto :goto_0

    .line 41
    :cond_6
    const-class v0, Ljava/util/SortedMap;

    invoke-virtual {v0, p2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 42
    new-instance v0, Lcom/google/obf/ff$3;

    invoke-direct {v0, p0}, Lcom/google/obf/ff$3;-><init>(Lcom/google/obf/ff;)V

    goto :goto_0

    .line 43
    :cond_7
    instance-of v0, p1, Ljava/lang/reflect/ParameterizedType;

    if-eqz v0, :cond_8

    const-class v0, Ljava/lang/String;

    check-cast p1, Ljava/lang/reflect/ParameterizedType;

    .line 44
    invoke-interface {p1}, Ljava/lang/reflect/ParameterizedType;->getActualTypeArguments()[Ljava/lang/reflect/Type;

    move-result-object v1

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-static {v1}, Lcom/google/obf/gd;->a(Ljava/lang/reflect/Type;)Lcom/google/obf/gd;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/obf/gd;->a()Ljava/lang/Class;

    move-result-object v1

    .line 45
    invoke-virtual {v0, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 46
    new-instance v0, Lcom/google/obf/ff$4;

    invoke-direct {v0, p0}, Lcom/google/obf/ff$4;-><init>(Lcom/google/obf/ff;)V

    goto/16 :goto_0

    .line 47
    :cond_8
    new-instance v0, Lcom/google/obf/ff$5;

    invoke-direct {v0, p0}, Lcom/google/obf/ff$5;-><init>(Lcom/google/obf/ff;)V

    goto/16 :goto_0

    .line 48
    :cond_9
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method private b(Ljava/lang/reflect/Type;Ljava/lang/Class;)Lcom/google/obf/fk;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/reflect/Type;",
            "Ljava/lang/Class",
            "<-TT;>;)",
            "Lcom/google/obf/fk",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 49
    new-instance v0, Lcom/google/obf/ff$6;

    invoke-direct {v0, p0, p2, p1}, Lcom/google/obf/ff$6;-><init>(Lcom/google/obf/ff;Ljava/lang/Class;Ljava/lang/reflect/Type;)V

    return-object v0
.end method


# virtual methods
.method public a(Lcom/google/obf/gd;)Lcom/google/obf/fk;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/obf/gd",
            "<TT;>;)",
            "Lcom/google/obf/fk",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 4
    invoke-virtual {p1}, Lcom/google/obf/gd;->b()Ljava/lang/reflect/Type;

    move-result-object v2

    .line 5
    invoke-virtual {p1}, Lcom/google/obf/gd;->a()Ljava/lang/Class;

    move-result-object v1

    .line 6
    iget-object v0, p0, Lcom/google/obf/ff;->a:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/ei;

    .line 7
    if-eqz v0, :cond_1

    .line 8
    new-instance v1, Lcom/google/obf/ff$1;

    invoke-direct {v1, p0, v0, v2}, Lcom/google/obf/ff$1;-><init>(Lcom/google/obf/ff;Lcom/google/obf/ei;Ljava/lang/reflect/Type;)V

    move-object v0, v1

    .line 19
    :cond_0
    :goto_0
    return-object v0

    .line 9
    :cond_1
    iget-object v0, p0, Lcom/google/obf/ff;->a:Ljava/util/Map;

    .line 10
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/ei;

    .line 11
    if-eqz v0, :cond_2

    .line 12
    new-instance v1, Lcom/google/obf/ff$7;

    invoke-direct {v1, p0, v0, v2}, Lcom/google/obf/ff$7;-><init>(Lcom/google/obf/ff;Lcom/google/obf/ei;Ljava/lang/reflect/Type;)V

    move-object v0, v1

    goto :goto_0

    .line 13
    :cond_2
    invoke-direct {p0, v1}, Lcom/google/obf/ff;->a(Ljava/lang/Class;)Lcom/google/obf/fk;

    move-result-object v0

    .line 14
    if-nez v0, :cond_0

    .line 16
    invoke-direct {p0, v2, v1}, Lcom/google/obf/ff;->a(Ljava/lang/reflect/Type;Ljava/lang/Class;)Lcom/google/obf/fk;

    move-result-object v0

    .line 17
    if-nez v0, :cond_0

    .line 19
    invoke-direct {p0, v2, v1}, Lcom/google/obf/ff;->b(Ljava/lang/reflect/Type;Ljava/lang/Class;)Lcom/google/obf/fk;

    move-result-object v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/obf/ff;->a:Ljava/util/Map;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
