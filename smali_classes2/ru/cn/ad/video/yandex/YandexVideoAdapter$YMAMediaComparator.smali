.class final Lru/cn/ad/video/yandex/YandexVideoAdapter$YMAMediaComparator;
.super Ljava/lang/Object;
.source "YandexVideoAdapter.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/ad/video/yandex/YandexVideoAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "YMAMediaComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/yandex/mobile/ads/video/models/ad/MediaFile;",
        ">;"
    }
.end annotation


# instance fields
.field private final mimeTypes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final optimalDimension:I


# direct methods
.method public constructor <init>(Ljava/util/List;I)V
    .locals 0
    .param p2, "optimalDimension"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 388
    .local p1, "mimeTypes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 389
    iput-object p1, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter$YMAMediaComparator;->mimeTypes:Ljava/util/List;

    .line 390
    iput p2, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter$YMAMediaComparator;->optimalDimension:I

    .line 391
    return-void
.end method


# virtual methods
.method public compare(Lcom/yandex/mobile/ads/video/models/ad/MediaFile;Lcom/yandex/mobile/ads/video/models/ad/MediaFile;)I
    .locals 5
    .param p1, "lhs"    # Lcom/yandex/mobile/ads/video/models/ad/MediaFile;
    .param p2, "rhs"    # Lcom/yandex/mobile/ads/video/models/ad/MediaFile;

    .prologue
    .line 395
    iget-object v2, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter$YMAMediaComparator;->mimeTypes:Ljava/util/List;

    invoke-virtual {p1}, Lcom/yandex/mobile/ads/video/models/ad/MediaFile;->getMimeType()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 396
    .local v0, "leftMimeIndex":I
    iget-object v2, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter$YMAMediaComparator;->mimeTypes:Ljava/util/List;

    invoke-virtual {p2}, Lcom/yandex/mobile/ads/video/models/ad/MediaFile;->getMimeType()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    .line 398
    .local v1, "rightMimeIndex":I
    if-eq v0, v1, :cond_0

    .line 399
    sub-int v2, v0, v1

    .line 405
    :goto_0
    return v2

    .line 402
    :cond_0
    invoke-virtual {p1}, Lcom/yandex/mobile/ads/video/models/ad/MediaFile;->getHeight()I

    move-result v2

    invoke-virtual {p2}, Lcom/yandex/mobile/ads/video/models/ad/MediaFile;->getHeight()I

    move-result v3

    if-eq v2, v3, :cond_1

    .line 403
    invoke-virtual {p1}, Lcom/yandex/mobile/ads/video/models/ad/MediaFile;->getHeight()I

    move-result v2

    iget v3, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter$YMAMediaComparator;->optimalDimension:I

    sub-int/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    invoke-virtual {p2}, Lcom/yandex/mobile/ads/video/models/ad/MediaFile;->getHeight()I

    move-result v3

    iget v4, p0, Lru/cn/ad/video/yandex/YandexVideoAdapter$YMAMediaComparator;->optimalDimension:I

    sub-int/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    sub-int/2addr v2, v3

    goto :goto_0

    .line 405
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 384
    check-cast p1, Lcom/yandex/mobile/ads/video/models/ad/MediaFile;

    check-cast p2, Lcom/yandex/mobile/ads/video/models/ad/MediaFile;

    invoke-virtual {p0, p1, p2}, Lru/cn/ad/video/yandex/YandexVideoAdapter$YMAMediaComparator;->compare(Lcom/yandex/mobile/ads/video/models/ad/MediaFile;Lcom/yandex/mobile/ads/video/models/ad/MediaFile;)I

    move-result v0

    return v0
.end method
