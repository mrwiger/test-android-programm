.class public final Lcom/yandex/metrica/impl/aq;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/yandex/metrica/impl/aq$g;,
        Lcom/yandex/metrica/impl/aq$h;,
        Lcom/yandex/metrica/impl/aq$d;,
        Lcom/yandex/metrica/impl/aq$a;,
        Lcom/yandex/metrica/impl/aq$e;,
        Lcom/yandex/metrica/impl/aq$c;,
        Lcom/yandex/metrica/impl/aq$f;,
        Lcom/yandex/metrica/impl/aq$b;
    }
.end annotation


# static fields
.field private static a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/yandex/metrica/impl/ob/bp;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static b:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/yandex/metrica/impl/ob/bp;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 61
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 62
    sget-object v1, Lcom/yandex/metrica/impl/ob/bp;->a:Lcom/yandex/metrica/impl/ob/bp;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    sget-object v1, Lcom/yandex/metrica/impl/ob/bp;->b:Lcom/yandex/metrica/impl/ob/bp;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/yandex/metrica/impl/aq;->a:Ljava/util/Map;

    .line 66
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    .line 67
    sget-object v1, Lcom/yandex/metrica/impl/ob/bp;->a:Lcom/yandex/metrica/impl/ob/bp;

    invoke-virtual {v0, v3, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 68
    sget-object v1, Lcom/yandex/metrica/impl/ob/bp;->b:Lcom/yandex/metrica/impl/ob/bp;

    invoke-virtual {v0, v4, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 69
    sput-object v0, Lcom/yandex/metrica/impl/aq;->b:Landroid/util/SparseArray;

    .line 70
    return-void
.end method

.method static a(Lcom/yandex/metrica/impl/ob/bp;)I
    .locals 1

    .prologue
    .line 200
    sget-object v0, Lcom/yandex/metrica/impl/aq;->a:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public static a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/yandex/metrica/c$c$d$a$b;
    .locals 2

    .prologue
    .line 337
    new-instance v0, Lcom/yandex/metrica/c$c$d$a$b;

    invoke-direct {v0}, Lcom/yandex/metrica/c$c$d$a$b;-><init>()V

    .line 339
    iput p0, v0, Lcom/yandex/metrica/c$c$d$a$b;->d:I

    .line 341
    if-eqz p1, :cond_0

    .line 342
    iput-object p1, v0, Lcom/yandex/metrica/c$c$d$a$b;->e:Ljava/lang/String;

    .line 345
    :cond_0
    invoke-static {p3}, Lcom/yandex/metrica/impl/aq;->c(Ljava/lang/String;)[Lcom/yandex/metrica/c$a;

    move-result-object v1

    .line 347
    if-eqz v1, :cond_1

    .line 348
    iput-object v1, v0, Lcom/yandex/metrica/c$c$d$a$b;->b:[Lcom/yandex/metrica/c$a;

    .line 350
    :cond_1
    invoke-static {p2}, Lcom/yandex/metrica/impl/aq;->a(Ljava/lang/String;)[Lcom/yandex/metrica/c$d;

    move-result-object v1

    iput-object v1, v0, Lcom/yandex/metrica/c$c$d$a$b;->c:[Lcom/yandex/metrica/c$d;

    .line 352
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 353
    invoke-static {p4}, Lcom/yandex/metrica/impl/aq;->b(Ljava/lang/String;)Lcom/yandex/metrica/c$c$d$a$b$a;

    move-result-object v1

    iput-object v1, v0, Lcom/yandex/metrica/c$c$d$a$b;->f:Lcom/yandex/metrica/c$c$d$a$b$a;

    .line 356
    :cond_2
    return-object v0
.end method

.method public static a(Ljava/lang/String;ILcom/yandex/metrica/c$c$f;)Lcom/yandex/metrica/c$c$d$b;
    .locals 1

    .prologue
    .line 190
    new-instance v0, Lcom/yandex/metrica/c$c$d$b;

    invoke-direct {v0}, Lcom/yandex/metrica/c$c$d$b;-><init>()V

    .line 192
    iput-object p2, v0, Lcom/yandex/metrica/c$c$d$b;->b:Lcom/yandex/metrica/c$c$f;

    .line 193
    iput-object p0, v0, Lcom/yandex/metrica/c$c$d$b;->c:Ljava/lang/String;

    .line 194
    iput p1, v0, Lcom/yandex/metrica/c$c$d$b;->d:I

    .line 196
    return-object v0
.end method

.method public static a(Lcom/yandex/metrica/impl/ob/gh;)Lcom/yandex/metrica/c$c$e;
    .locals 2

    .prologue
    .line 81
    new-instance v0, Lcom/yandex/metrica/c$c$e;

    invoke-direct {v0}, Lcom/yandex/metrica/c$c$e;-><init>()V

    .line 82
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/gh;->a()Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 83
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/gh;->a()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v0, Lcom/yandex/metrica/c$c$e;->b:I

    .line 85
    :cond_0
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/gh;->b()Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 86
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/gh;->b()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v0, Lcom/yandex/metrica/c$c$e;->c:I

    .line 88
    :cond_1
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/gh;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 89
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/gh;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/yandex/metrica/c$c$e;->d:Ljava/lang/String;

    .line 91
    :cond_2
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/gh;->c()Z

    move-result v1

    iput-boolean v1, v0, Lcom/yandex/metrica/c$c$e;->e:Z

    .line 92
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/gh;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 93
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/gh;->e()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/yandex/metrica/c$c$e;->f:Ljava/lang/String;

    .line 95
    :cond_3
    return-object v0
.end method

.method public static a(Landroid/content/ContentValues;)Lcom/yandex/metrica/c$c$f;
    .locals 3

    .prologue
    .line 73
    const-string v0, "start_time"

    .line 74
    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    const-string v1, "server_time_offset"

    .line 75
    invoke-virtual {p0, v1}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    const-string v2, "obtained_before_first_sync"

    .line 76
    invoke-virtual {p0, v2}, Landroid/content/ContentValues;->getAsBoolean(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v2

    .line 73
    invoke-static {v0, v1, v2}, Lcom/yandex/metrica/impl/aq;->a(Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;)Lcom/yandex/metrica/c$c$f;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Boolean;)Lcom/yandex/metrica/c$c$f;
    .locals 6

    .prologue
    .line 177
    invoke-virtual {p0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 1169
    new-instance v1, Lcom/yandex/metrica/c$c$f;

    invoke-direct {v1}, Lcom/yandex/metrica/c$c$f;-><init>()V

    .line 1170
    iput-wide v2, v1, Lcom/yandex/metrica/c$c$f;->b:J

    .line 2048
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    check-cast v0, Ljava/util/GregorianCalendar;

    .line 2049
    invoke-virtual {v0}, Ljava/util/GregorianCalendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v0

    .line 2051
    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v0

    div-int/lit16 v0, v0, 0x3e8

    .line 1171
    iput v0, v1, Lcom/yandex/metrica/c$c$f;->c:I

    .line 178
    if-eqz p1, :cond_0

    .line 179
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iput-wide v2, v1, Lcom/yandex/metrica/c$c$f;->d:J

    .line 181
    :cond_0
    if-eqz p2, :cond_1

    .line 182
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, v1, Lcom/yandex/metrica/c$c$f;->e:Z

    .line 184
    :cond_1
    return-object v1
.end method

.method public static a(Lorg/json/JSONObject;)Lcom/yandex/metrica/c$d;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 129
    :try_start_0
    new-instance v0, Lcom/yandex/metrica/c$d;

    invoke-direct {v0}, Lcom/yandex/metrica/c$d;-><init>()V

    .line 130
    const-string v1, "mac"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/yandex/metrica/c$d;->b:Ljava/lang/String;

    .line 131
    const-string v1, "signal_strength"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lcom/yandex/metrica/c$d;->c:I

    .line 132
    const-string v1, "ssid"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/yandex/metrica/c$d;->d:Ljava/lang/String;

    .line 133
    const-string v1, "is_connected"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/yandex/metrica/c$d;->e:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 138
    :goto_0
    return-object v0

    .line 136
    :catch_0
    move-exception v0

    new-instance v0, Lcom/yandex/metrica/c$d;

    invoke-direct {v0}, Lcom/yandex/metrica/c$d;-><init>()V

    .line 137
    const-string v1, "mac"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/yandex/metrica/c$d;->b:Ljava/lang/String;

    goto :goto_0
.end method

.method public static a(I)Lcom/yandex/metrica/impl/ob/bp;
    .locals 1

    .prologue
    .line 99
    sget-object v0, Lcom/yandex/metrica/impl/aq;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/yandex/metrica/impl/ob/bp;

    return-object v0
.end method

.method public static a()V
    .locals 0

    .prologue
    .line 395
    return-void
.end method

.method public static a(Landroid/content/Context;)[Lcom/yandex/metrica/c$c$c;
    .locals 6

    .prologue
    .line 398
    invoke-static {p0}, Lcom/yandex/metrica/impl/bn;->a(Landroid/content/Context;)Lcom/yandex/metrica/impl/bn;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/bn;->b()Ljava/util/List;

    move-result-object v3

    .line 399
    invoke-static {v3}, Lcom/yandex/metrica/impl/bl;->a(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 400
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    new-array v2, v0, [Lcom/yandex/metrica/c$c$c;

    .line 402
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 403
    new-instance v4, Lcom/yandex/metrica/c$c$c;

    invoke-direct {v4}, Lcom/yandex/metrica/c$c$c;-><init>()V

    .line 404
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/yandex/metrica/impl/bn$a;

    .line 405
    iget-object v5, v0, Lcom/yandex/metrica/impl/bn$a;->a:Ljava/lang/String;

    iput-object v5, v4, Lcom/yandex/metrica/c$c$c;->b:Ljava/lang/String;

    .line 406
    iget-object v0, v0, Lcom/yandex/metrica/impl/bn$a;->b:Ljava/lang/String;

    iput-object v0, v4, Lcom/yandex/metrica/c$c$c;->c:Ljava/lang/String;

    .line 407
    aput-object v4, v2, v1

    .line 402
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    move-object v0, v2

    .line 411
    :goto_1
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a(Ljava/lang/String;)[Lcom/yandex/metrica/c$d;
    .locals 2

    .prologue
    .line 103
    const/4 v0, 0x0

    .line 105
    :try_start_0
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1, p0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 106
    invoke-static {v1}, Lcom/yandex/metrica/impl/aq;->a(Lorg/json/JSONArray;)[Lcom/yandex/metrica/c$d;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 111
    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static a(Lorg/json/JSONArray;)[Lcom/yandex/metrica/c$d;
    .locals 3

    .prologue
    .line 115
    const/4 v0, 0x0

    .line 117
    :try_start_0
    invoke-virtual {p0}, Lorg/json/JSONArray;->length()I

    move-result v1

    new-array v0, v1, [Lcom/yandex/metrica/c$d;

    .line 118
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 119
    invoke-virtual {p0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    invoke-static {v2}, Lcom/yandex/metrica/impl/aq;->a(Lorg/json/JSONObject;)Lcom/yandex/metrica/c$d;

    move-result-object v2

    aput-object v2, v0, v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 118
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :catch_0
    move-exception v1

    .line 124
    :cond_0
    return-object v0
.end method

.method static b(Lorg/json/JSONObject;)Lcom/yandex/metrica/c$a;
    .locals 3

    .prologue
    .line 242
    new-instance v0, Lcom/yandex/metrica/c$a;

    invoke-direct {v0}, Lcom/yandex/metrica/c$a;-><init>()V

    .line 244
    const-string v1, "signal_strength"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 245
    const-string v1, "signal_strength"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v1

    .line 246
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 247
    iput v1, v0, Lcom/yandex/metrica/c$a;->c:I

    .line 250
    :cond_0
    const-string v1, "cell_id"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 251
    const-string v1, "cell_id"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lcom/yandex/metrica/c$a;->b:I

    .line 253
    :cond_1
    const-string v1, "lac"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 254
    const-string v1, "lac"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lcom/yandex/metrica/c$a;->d:I

    .line 256
    :cond_2
    const-string v1, "country_code"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 257
    const-string v1, "country_code"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lcom/yandex/metrica/c$a;->e:I

    .line 259
    :cond_3
    const-string v1, "operator_id"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 260
    const-string v1, "operator_id"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lcom/yandex/metrica/c$a;->f:I

    .line 262
    :cond_4
    const-string v1, "operator_name"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 263
    const-string v1, "operator_name"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/yandex/metrica/c$a;->g:Ljava/lang/String;

    .line 265
    :cond_5
    const-string v1, "is_connected"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 266
    const-string v1, "is_connected"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, v0, Lcom/yandex/metrica/c$a;->h:Z

    .line 268
    :cond_6
    const-string v1, "cell_type"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, v0, Lcom/yandex/metrica/c$a;->i:I

    .line 269
    const-string v1, "pci"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 270
    const-string v1, "pci"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lcom/yandex/metrica/c$a;->j:I

    .line 272
    :cond_7
    return-object v0
.end method

.method static b(Ljava/lang/String;)Lcom/yandex/metrica/c$c$d$a$b$a;
    .locals 4

    .prologue
    .line 144
    :try_start_0
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 145
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 146
    new-instance v0, Lcom/yandex/metrica/c$c$d$a$b$a;

    invoke-direct {v0}, Lcom/yandex/metrica/c$c$d$a$b$a;-><init>()V

    .line 147
    const-string v2, "ssid"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/yandex/metrica/c$c$d$a$b$a;->b:Ljava/lang/String;

    .line 148
    const-string v2, "state"

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v1

    .line 149
    packed-switch v1, :pswitch_data_0

    .line 165
    :goto_0
    return-object v0

    .line 154
    :pswitch_0
    const/4 v1, 0x1

    iput v1, v0, Lcom/yandex/metrica/c$c$d$a$b$a;->c:I

    goto :goto_0

    :catch_0
    move-exception v0

    .line 165
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 157
    :pswitch_1
    const/4 v1, 0x2

    iput v1, v0, Lcom/yandex/metrica/c$c$d$a$b$a;->c:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 149
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static b(Lorg/json/JSONArray;)[Lcom/yandex/metrica/c$a;
    .locals 3

    .prologue
    .line 224
    const/4 v0, 0x0

    .line 226
    :try_start_0
    invoke-virtual {p0}, Lorg/json/JSONArray;->length()I

    move-result v1

    new-array v0, v1, [Lcom/yandex/metrica/c$a;

    .line 227
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 228
    invoke-virtual {p0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    .line 229
    if-eqz v2, :cond_0

    .line 230
    invoke-static {v2}, Lcom/yandex/metrica/impl/aq;->b(Lorg/json/JSONObject;)Lcom/yandex/metrica/c$a;

    move-result-object v2

    aput-object v2, v0, v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 227
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :catch_0
    move-exception v1

    .line 236
    :cond_1
    return-object v0
.end method

.method public static c(Ljava/lang/String;)[Lcom/yandex/metrica/c$a;
    .locals 4

    .prologue
    .line 204
    const/4 v0, 0x0

    .line 207
    :try_start_0
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 208
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1, p0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 209
    invoke-static {v1}, Lcom/yandex/metrica/impl/aq;->b(Lorg/json/JSONArray;)[Lcom/yandex/metrica/c$a;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 219
    :cond_0
    :goto_0
    return-object v0

    .line 212
    :catch_0
    move-exception v1

    :try_start_1
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 213
    const/4 v1, 0x1

    new-array v1, v1, [Lcom/yandex/metrica/c$a;

    const/4 v3, 0x0

    invoke-static {v2}, Lcom/yandex/metrica/impl/aq;->b(Lorg/json/JSONObject;)Lcom/yandex/metrica/c$a;

    move-result-object v2

    aput-object v2, v1, v3
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-object v0, v1

    .line 217
    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method public static d(Ljava/lang/String;)Lcom/yandex/metrica/c$c$b;
    .locals 8

    .prologue
    .line 276
    const/4 v1, 0x0

    .line 279
    :try_start_0
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 280
    new-instance v2, Lcom/yandex/metrica/impl/utils/g$a;

    invoke-direct {v2, p0}, Lcom/yandex/metrica/impl/utils/g$a;-><init>(Ljava/lang/String;)V

    .line 282
    const-string v0, "lon"

    invoke-virtual {v2, v0}, Lcom/yandex/metrica/impl/utils/g$a;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "lat"

    .line 283
    invoke-virtual {v2, v0}, Lcom/yandex/metrica/impl/utils/g$a;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 286
    new-instance v0, Lcom/yandex/metrica/c$c$b;

    invoke-direct {v0}, Lcom/yandex/metrica/c$c$b;-><init>()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 289
    :try_start_1
    const-string v1, "lon"

    invoke-virtual {v2, v1}, Lcom/yandex/metrica/impl/utils/g$a;->getDouble(Ljava/lang/String;)D

    move-result-wide v4

    iput-wide v4, v0, Lcom/yandex/metrica/c$c$b;->c:D

    .line 290
    const-string v1, "lat"

    invoke-virtual {v2, v1}, Lcom/yandex/metrica/impl/utils/g$a;->getDouble(Ljava/lang/String;)D

    move-result-wide v4

    iput-wide v4, v0, Lcom/yandex/metrica/c$c$b;->b:D

    .line 293
    const-string v1, "altitude"

    invoke-virtual {v2, v1}, Lcom/yandex/metrica/impl/utils/g$a;->optInt(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lcom/yandex/metrica/c$c$b;->h:I

    .line 294
    const-string v1, "direction"

    invoke-virtual {v2, v1}, Lcom/yandex/metrica/impl/utils/g$a;->optInt(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lcom/yandex/metrica/c$c$b;->f:I

    .line 295
    const-string v1, "precision"

    invoke-virtual {v2, v1}, Lcom/yandex/metrica/impl/utils/g$a;->optInt(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lcom/yandex/metrica/c$c$b;->e:I

    .line 296
    const-string v1, "speed"

    invoke-virtual {v2, v1}, Lcom/yandex/metrica/impl/utils/g$a;->optInt(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lcom/yandex/metrica/c$c$b;->g:I

    .line 297
    const-string v1, "timestamp"

    invoke-virtual {v2, v1}, Lcom/yandex/metrica/impl/utils/g$a;->optLong(Ljava/lang/String;)J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    iput-wide v4, v0, Lcom/yandex/metrica/c$c$b;->d:J

    .line 299
    const-string v1, "provider"

    invoke-virtual {v2, v1}, Lcom/yandex/metrica/impl/utils/g$a;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 300
    const-string v1, "provider"

    invoke-virtual {v2, v1}, Lcom/yandex/metrica/impl/utils/g$a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 301
    const-string v2, "gps"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 302
    const/4 v1, 0x1

    iput v1, v0, Lcom/yandex/metrica/c$c$b;->i:I

    .line 313
    :cond_0
    :goto_0
    return-object v0

    .line 303
    :cond_1
    const-string v2, "network"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 304
    const/4 v1, 0x2

    iput v1, v0, Lcom/yandex/metrica/c$c$b;->i:I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0

    :catch_1
    move-exception v0

    move-object v0, v1

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method static e(Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 318
    const/4 v0, -0x1

    .line 320
    :try_start_0
    new-instance v1, Lcom/yandex/metrica/impl/utils/g$a;

    invoke-direct {v1, p0}, Lcom/yandex/metrica/impl/utils/g$a;-><init>(Ljava/lang/String;)V

    .line 321
    const-string v2, "enabled"

    invoke-virtual {v1, v2}, Lcom/yandex/metrica/impl/utils/g$a;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 322
    const-string v2, "enabled"

    invoke-virtual {v1, v2}, Lcom/yandex/metrica/impl/utils/g$a;->getBoolean(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 329
    :cond_0
    :goto_0
    return v0

    .line 322
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static f(Ljava/lang/String;)Lcom/yandex/metrica/c$c$d$a$a;
    .locals 3

    .prologue
    .line 361
    :try_start_0
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 362
    invoke-static {p0}, Lcom/yandex/metrica/impl/utils/s;->a(Ljava/lang/String;)Lcom/yandex/metrica/d;

    move-result-object v1

    .line 364
    new-instance v0, Lcom/yandex/metrica/c$c$d$a$a;

    invoke-direct {v0}, Lcom/yandex/metrica/c$c$d$a$a;-><init>()V

    .line 366
    invoke-virtual {v1}, Lcom/yandex/metrica/d;->a()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/yandex/metrica/c$c$d$a$a;->b:Ljava/lang/String;

    .line 367
    invoke-virtual {v1}, Lcom/yandex/metrica/d;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 368
    invoke-virtual {v1}, Lcom/yandex/metrica/d;->b()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/yandex/metrica/c$c$d$a$a;->c:Ljava/lang/String;

    .line 370
    :cond_0
    invoke-virtual {v1}, Lcom/yandex/metrica/d;->c()Ljava/util/Map;

    move-result-object v2

    invoke-static {v2}, Lcom/yandex/metrica/impl/bl;->a(Ljava/util/Map;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 371
    invoke-virtual {v1}, Lcom/yandex/metrica/d;->c()Ljava/util/Map;

    move-result-object v1

    invoke-static {v1}, Lcom/yandex/metrica/impl/utils/g;->a(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/yandex/metrica/c$c$d$a$a;->d:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 379
    :cond_1
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
