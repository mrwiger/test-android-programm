.class public interface abstract Lru/cn/player/ITrackSelector;
.super Ljava/lang/Object;
.source "ITrackSelector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/player/ITrackSelector$TrackNameGenerator;
    }
.end annotation


# virtual methods
.method public abstract adaptive()Z
.end method

.method public abstract containsTracks()Z
.end method

.method public abstract deactivatable()Z
.end method

.method public abstract disable()V
.end method

.method public abstract getCurrentTrackIndex()I
.end method

.method public abstract getTracksName(Lru/cn/player/ITrackSelector$TrackNameGenerator;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lru/cn/player/ITrackSelector$TrackNameGenerator;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract selectItem(I)V
.end method

.method public abstract setAdaptive()V
.end method
