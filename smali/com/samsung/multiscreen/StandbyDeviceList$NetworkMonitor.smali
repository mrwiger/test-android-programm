.class Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;
.super Ljava/lang/Object;
.source "StandbyDeviceList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/multiscreen/StandbyDeviceList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NetworkMonitor"
.end annotation


# instance fields
.field private final mConnectivityManager:Landroid/net/ConnectivityManager;

.field private mCurrentBSSID:Ljava/lang/String;

.field private mNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

.field private mNetworkInfo:Landroid/net/NetworkInfo;

.field final synthetic this$0:Lcom/samsung/multiscreen/StandbyDeviceList;


# direct methods
.method constructor <init>(Lcom/samsung/multiscreen/StandbyDeviceList;Landroid/content/Context;Lcom/samsung/multiscreen/Search$SearchListener;)V
    .locals 3
    .param p2, "appContext"    # Landroid/content/Context;
    .param p3, "searchListener"    # Lcom/samsung/multiscreen/Search$SearchListener;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;->this$0:Lcom/samsung/multiscreen/StandbyDeviceList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    invoke-static {p1, p3}, Lcom/samsung/multiscreen/StandbyDeviceList;->access$002(Lcom/samsung/multiscreen/StandbyDeviceList;Lcom/samsung/multiscreen/Search$SearchListener;)Lcom/samsung/multiscreen/Search$SearchListener;

    .line 62
    const-string v2, "connectivity"

    invoke-virtual {p2, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/ConnectivityManager;

    iput-object v2, p0, Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;->mConnectivityManager:Landroid/net/ConnectivityManager;

    .line 63
    iget-object v2, p0, Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;->mConnectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v2}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;->mNetworkInfo:Landroid/net/NetworkInfo;

    .line 64
    iget-object v2, p0, Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;->mNetworkInfo:Landroid/net/NetworkInfo;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;->mNetworkInfo:Landroid/net/NetworkInfo;

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 65
    const-string v2, "wifi"

    invoke-virtual {p2, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/WifiManager;

    .line 66
    .local v1, "wifiManager":Landroid/net/wifi/WifiManager;
    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    .line 67
    .local v0, "wifiInfo":Landroid/net/wifi/WifiInfo;
    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;->mCurrentBSSID:Ljava/lang/String;

    .line 71
    .end local v0    # "wifiInfo":Landroid/net/wifi/WifiInfo;
    .end local v1    # "wifiManager":Landroid/net/wifi/WifiManager;
    :goto_0
    invoke-direct {p0, p2}, Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;->registerNetworkChangeCallback(Landroid/content/Context;)V

    .line 72
    return-void

    .line 69
    :cond_0
    const-string v2, ""

    iput-object v2, p0, Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;->mCurrentBSSID:Ljava/lang/String;

    goto :goto_0
.end method

.method static synthetic access$100(Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;)Landroid/net/ConnectivityManager$NetworkCallback;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;->mNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

    return-object v0
.end method

.method static synthetic access$102(Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;Landroid/net/ConnectivityManager$NetworkCallback;)Landroid/net/ConnectivityManager$NetworkCallback;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;
    .param p1, "x1"    # Landroid/net/ConnectivityManager$NetworkCallback;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;->mNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

    return-object p1
.end method

.method static synthetic access$200(Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;)Landroid/net/NetworkInfo;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;->mNetworkInfo:Landroid/net/NetworkInfo;

    return-object v0
.end method

.method static synthetic access$202(Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;Landroid/net/NetworkInfo;)Landroid/net/NetworkInfo;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;
    .param p1, "x1"    # Landroid/net/NetworkInfo;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;->mNetworkInfo:Landroid/net/NetworkInfo;

    return-object p1
.end method

.method static synthetic access$300(Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;)Landroid/net/ConnectivityManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;->mConnectivityManager:Landroid/net/ConnectivityManager;

    return-object v0
.end method

.method static synthetic access$402(Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;->mCurrentBSSID:Ljava/lang/String;

    return-object p1
.end method

.method private registerNetworkChangeCallback(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 88
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor$1;

    invoke-direct {v1, p0, p1}, Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor$1;-><init>(Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;Landroid/content/Context;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 128
    invoke-virtual {v0}, Ljava/lang/Thread;->run()V

    .line 129
    return-void
.end method


# virtual methods
.method getCurrentBSSID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;->mCurrentBSSID:Ljava/lang/String;

    return-object v0
.end method

.method stopNetworkMonitoring()V
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;->mNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

    if-eqz v0, :cond_0

    .line 133
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/multiscreen/StandbyDeviceList$NetworkMonitor;->mNetworkCallback:Landroid/net/ConnectivityManager$NetworkCallback;

    .line 135
    :cond_0
    return-void
.end method
