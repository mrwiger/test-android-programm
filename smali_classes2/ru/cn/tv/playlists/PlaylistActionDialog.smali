.class public Lru/cn/tv/playlists/PlaylistActionDialog;
.super Landroid/support/v4/app/DialogFragment;
.source "PlaylistActionDialog.java"


# instance fields
.field private viewModel:Lru/cn/tv/playlists/PlaylistActionViewModel;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method

.method public static newInstance(JLjava/lang/String;Ljava/lang/String;)Lru/cn/tv/playlists/PlaylistActionDialog;
    .locals 4
    .param p0, "playlistId"    # J
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "location"    # Ljava/lang/String;

    .prologue
    .line 17
    new-instance v1, Lru/cn/tv/playlists/PlaylistActionDialog;

    invoke-direct {v1}, Lru/cn/tv/playlists/PlaylistActionDialog;-><init>()V

    .line 19
    .local v1, "fragment":Lru/cn/tv/playlists/PlaylistActionDialog;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 20
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "playlistId"

    invoke-virtual {v0, v2, p0, p1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 21
    const-string v2, "title"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 22
    const-string v2, "location"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    invoke-virtual {v1, v0}, Lru/cn/tv/playlists/PlaylistActionDialog;->setArguments(Landroid/os/Bundle;)V

    .line 25
    return-object v1
.end method


# virtual methods
.method final synthetic lambda$onCreateDialog$0$PlaylistActionDialog(Landroid/os/Bundle;Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "arguments"    # Landroid/os/Bundle;
    .param p2, "dialog"    # Landroid/content/DialogInterface;
    .param p3, "which"    # I

    .prologue
    .line 50
    const-string v1, "location"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 51
    .local v0, "location":Ljava/lang/String;
    iget-object v1, p0, Lru/cn/tv/playlists/PlaylistActionDialog;->viewModel:Lru/cn/tv/playlists/PlaylistActionViewModel;

    invoke-virtual {v1, v0}, Lru/cn/tv/playlists/PlaylistActionViewModel;->removePlaylist(Ljava/lang/String;)V

    .line 52
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 33
    invoke-static {}, Lru/cn/tv/App;->scope()Ltoothpick/Scope;

    move-result-object v2

    const-class v3, Lru/cn/tv/playlists/PlaylistActionViewModel;

    invoke-static {p0, v2, v3}, Lru/cn/mvvm/ViewModels;->get(Landroid/support/v4/app/Fragment;Ltoothpick/Scope;Ljava/lang/Class;)Landroid/arch/lifecycle/ViewModel;

    move-result-object v2

    check-cast v2, Lru/cn/tv/playlists/PlaylistActionViewModel;

    iput-object v2, p0, Lru/cn/tv/playlists/PlaylistActionDialog;->viewModel:Lru/cn/tv/playlists/PlaylistActionViewModel;

    .line 34
    invoke-virtual {p0}, Lru/cn/tv/playlists/PlaylistActionDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 35
    .local v0, "arguments":Landroid/os/Bundle;
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lru/cn/tv/playlists/PlaylistActionDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f0e0177

    .line 36
    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0e017c

    .line 37
    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0e001f

    new-instance v4, Lru/cn/tv/playlists/PlaylistActionDialog$1;

    invoke-direct {v4, p0, v0}, Lru/cn/tv/playlists/PlaylistActionDialog$1;-><init>(Lru/cn/tv/playlists/PlaylistActionDialog;Landroid/os/Bundle;)V

    .line 38
    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0e001e

    new-instance v4, Lru/cn/tv/playlists/PlaylistActionDialog$$Lambda$0;

    invoke-direct {v4, p0, v0}, Lru/cn/tv/playlists/PlaylistActionDialog$$Lambda$0;-><init>(Lru/cn/tv/playlists/PlaylistActionDialog;Landroid/os/Bundle;)V

    .line 49
    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 54
    .local v1, "b":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    return-object v2
.end method
