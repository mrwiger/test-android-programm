.class public final Lcom/my/target/common/models/ImageData;
.super Lcom/my/target/ag;
.source "ImageData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/common/models/ImageData$BitmapCache;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/my/target/ag",
        "<",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# static fields
.field private static final DEFAULT_CACHE_SIZE:I = 0x1e00000

.field private static final MIN_CACHE_SIZE:I = 0x500000

.field private static volatile memcache:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Lcom/my/target/common/models/ImageData;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private volatile useCache:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 26
    new-instance v0, Lcom/my/target/common/models/ImageData$BitmapCache;

    const/high16 v1, 0x1e00000

    invoke-direct {v0, v1}, Lcom/my/target/common/models/ImageData$BitmapCache;-><init>(I)V

    sput-object v0, Lcom/my/target/common/models/ImageData;->memcache:Landroid/util/LruCache;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 105
    invoke-direct {p0, p1}, Lcom/my/target/ag;-><init>(Ljava/lang/String;)V

    .line 106
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p1, "url"    # Ljava/lang/String;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 110
    invoke-direct {p0, p1}, Lcom/my/target/ag;-><init>(Ljava/lang/String;)V

    .line 111
    iput p2, p0, Lcom/my/target/common/models/ImageData;->width:I

    .line 112
    iput p3, p0, Lcom/my/target/common/models/ImageData;->height:I

    .line 113
    return-void
.end method

.method public static newImageData(Ljava/lang/String;)Lcom/my/target/common/models/ImageData;
    .locals 1
    .param p0, "url"    # Ljava/lang/String;

    .prologue
    .line 48
    new-instance v0, Lcom/my/target/common/models/ImageData;

    invoke-direct {v0, p0}, Lcom/my/target/common/models/ImageData;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static newImageData(Ljava/lang/String;II)Lcom/my/target/common/models/ImageData;
    .locals 1
    .param p0, "url"    # Ljava/lang/String;
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 53
    new-instance v0, Lcom/my/target/common/models/ImageData;

    invoke-direct {v0, p0, p1, p2}, Lcom/my/target/common/models/ImageData;-><init>(Ljava/lang/String;II)V

    return-object v0
.end method

.method public static setCacheSize(I)V
    .locals 2
    .param p0, "sizeInBytes"    # I

    .prologue
    .line 31
    const/high16 v0, 0x500000

    if-ge p0, v0, :cond_0

    .line 33
    const-string v0, "setting cache size ignored: size should be >=5242880"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 44
    :goto_0
    return-void

    .line 36
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_1

    .line 38
    sget-object v0, Lcom/my/target/common/models/ImageData;->memcache:Landroid/util/LruCache;

    invoke-virtual {v0, p0}, Landroid/util/LruCache;->resize(I)V

    goto :goto_0

    .line 42
    :cond_1
    new-instance v0, Lcom/my/target/common/models/ImageData$BitmapCache;

    invoke-direct {v0, p0}, Lcom/my/target/common/models/ImageData$BitmapCache;-><init>(I)V

    sput-object v0, Lcom/my/target/common/models/ImageData;->memcache:Landroid/util/LruCache;

    goto :goto_0
.end method


# virtual methods
.method public getBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 65
    invoke-virtual {p0}, Lcom/my/target/common/models/ImageData;->getData()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getData()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 76
    iget-boolean v0, p0, Lcom/my/target/common/models/ImageData;->useCache:Z

    if-eqz v0, :cond_0

    .line 78
    sget-object v0, Lcom/my/target/common/models/ImageData;->memcache:Landroid/util/LruCache;

    invoke-virtual {v0, p0}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 80
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lcom/my/target/ag;->getData()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method public bridge synthetic getData()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 21
    invoke-virtual {p0}, Lcom/my/target/common/models/ImageData;->getData()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public isUseCache()Z
    .locals 1

    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/my/target/common/models/ImageData;->useCache:Z

    return v0
.end method

.method public setBitmap(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 70
    invoke-virtual {p0, p1}, Lcom/my/target/common/models/ImageData;->setData(Landroid/graphics/Bitmap;)V

    .line 71
    return-void
.end method

.method public setData(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "data"    # Landroid/graphics/Bitmap;

    .prologue
    .line 86
    iget-boolean v0, p0, Lcom/my/target/common/models/ImageData;->useCache:Z

    if-eqz v0, :cond_1

    .line 88
    if-nez p1, :cond_0

    .line 90
    sget-object v0, Lcom/my/target/common/models/ImageData;->memcache:Landroid/util/LruCache;

    invoke-virtual {v0, p0}, Landroid/util/LruCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    :goto_0
    return-void

    .line 94
    :cond_0
    sget-object v0, Lcom/my/target/common/models/ImageData;->memcache:Landroid/util/LruCache;

    invoke-virtual {v0, p0, p1}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 99
    :cond_1
    invoke-super {p0, p1}, Lcom/my/target/ag;->setData(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public bridge synthetic setData(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 21
    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1}, Lcom/my/target/common/models/ImageData;->setData(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 145
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ImageData{url=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/common/models/ImageData;->url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", width="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/my/target/common/models/ImageData;->width:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", height="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/my/target/common/models/ImageData;->height:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", bitmap="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/my/target/common/models/ImageData;->getData()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public useCache(Z)V
    .locals 2
    .param p1, "use"    # Z

    .prologue
    .line 117
    iget-boolean v0, p0, Lcom/my/target/common/models/ImageData;->useCache:Z

    if-ne p1, v0, :cond_1

    .line 136
    :cond_0
    :goto_0
    return-void

    .line 121
    :cond_1
    iput-boolean p1, p0, Lcom/my/target/common/models/ImageData;->useCache:Z

    .line 122
    if-eqz p1, :cond_2

    .line 124
    invoke-super {p0}, Lcom/my/target/ag;->getData()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 125
    if-eqz v0, :cond_0

    .line 127
    const/4 v1, 0x0

    invoke-super {p0, v1}, Lcom/my/target/ag;->setData(Ljava/lang/Object;)V

    .line 128
    sget-object v1, Lcom/my/target/common/models/ImageData;->memcache:Landroid/util/LruCache;

    invoke-virtual {v1, p0, v0}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 133
    :cond_2
    sget-object v0, Lcom/my/target/common/models/ImageData;->memcache:Landroid/util/LruCache;

    invoke-virtual {v0, p0}, Landroid/util/LruCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 134
    invoke-super {p0, v0}, Lcom/my/target/ag;->setData(Ljava/lang/Object;)V

    goto :goto_0
.end method
