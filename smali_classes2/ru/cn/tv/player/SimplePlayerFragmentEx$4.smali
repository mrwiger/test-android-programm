.class Lru/cn/tv/player/SimplePlayerFragmentEx$4;
.super Ljava/lang/Object;
.source "SimplePlayerFragmentEx.java"

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/tv/player/SimplePlayerFragmentEx;->channelInfoLoaded(JLjava/lang/String;ZIZZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/tv/player/SimplePlayerFragmentEx;

.field final synthetic val$cnId:J

.field final synthetic val$isFavourite:Z


# direct methods
.method constructor <init>(Lru/cn/tv/player/SimplePlayerFragmentEx;JZ)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/tv/player/SimplePlayerFragmentEx;

    .prologue
    .line 270
    iput-object p1, p0, Lru/cn/tv/player/SimplePlayerFragmentEx$4;->this$0:Lru/cn/tv/player/SimplePlayerFragmentEx;

    iput-wide p2, p0, Lru/cn/tv/player/SimplePlayerFragmentEx$4;->val$cnId:J

    iput-boolean p4, p0, Lru/cn/tv/player/SimplePlayerFragmentEx$4;->val$isFavourite:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 5
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 273
    new-instance v0, Lru/cn/tv/FavouriteWorker;

    iget-wide v2, p0, Lru/cn/tv/player/SimplePlayerFragmentEx$4;->val$cnId:J

    iget-boolean v1, p0, Lru/cn/tv/player/SimplePlayerFragmentEx$4;->val$isFavourite:Z

    iget-object v4, p0, Lru/cn/tv/player/SimplePlayerFragmentEx$4;->this$0:Lru/cn/tv/player/SimplePlayerFragmentEx;

    invoke-virtual {v4}, Lru/cn/tv/player/SimplePlayerFragmentEx;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-direct {v0, v2, v3, v1, v4}, Lru/cn/tv/FavouriteWorker;-><init>(JZLandroid/content/Context;)V

    .line 274
    .local v0, "worker":Lru/cn/tv/FavouriteWorker;
    invoke-virtual {v0}, Lru/cn/tv/FavouriteWorker;->toggleFavourite()V

    .line 276
    iget-boolean v1, p0, Lru/cn/tv/player/SimplePlayerFragmentEx$4;->val$isFavourite:Z

    if-eqz v1, :cond_0

    .line 277
    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragmentEx$4;->this$0:Lru/cn/tv/player/SimplePlayerFragmentEx;

    invoke-static {v1}, Lru/cn/tv/player/SimplePlayerFragmentEx;->access$300(Lru/cn/tv/player/SimplePlayerFragmentEx;)Landroid/view/MenuItem;

    move-result-object v1

    const v2, 0x7f0802e4

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 282
    :goto_0
    const/4 v1, 0x1

    return v1

    .line 279
    :cond_0
    iget-object v1, p0, Lru/cn/tv/player/SimplePlayerFragmentEx$4;->this$0:Lru/cn/tv/player/SimplePlayerFragmentEx;

    invoke-static {v1}, Lru/cn/tv/player/SimplePlayerFragmentEx;->access$300(Lru/cn/tv/player/SimplePlayerFragmentEx;)Landroid/view/MenuItem;

    move-result-object v1

    const v2, 0x7f0802e2

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    goto :goto_0
.end method
