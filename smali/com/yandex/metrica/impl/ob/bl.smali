.class public Lcom/yandex/metrica/impl/ob/bl;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/yandex/metrica/impl/ob/bl$b;,
        Lcom/yandex/metrica/impl/ob/bl$a;
    }
.end annotation


# instance fields
.field private final a:Lcom/yandex/metrica/impl/ob/v;

.field private final b:Lcom/yandex/metrica/impl/ob/bl$a;

.field private final c:Lcom/yandex/metrica/impl/ob/bg;

.field private final d:Lcom/yandex/metrica/impl/ob/bg;

.field private e:Lcom/yandex/metrica/impl/ob/bh;

.field private f:Lcom/yandex/metrica/impl/ob/bl$b;


# direct methods
.method public constructor <init>(Lcom/yandex/metrica/impl/ob/v;Lcom/yandex/metrica/impl/ob/bl$a;)V
    .locals 2

    .prologue
    .line 46
    new-instance v0, Lcom/yandex/metrica/impl/ob/bf;

    invoke-direct {v0, p1}, Lcom/yandex/metrica/impl/ob/bf;-><init>(Lcom/yandex/metrica/impl/ob/v;)V

    new-instance v1, Lcom/yandex/metrica/impl/ob/be;

    invoke-direct {v1, p1}, Lcom/yandex/metrica/impl/ob/be;-><init>(Lcom/yandex/metrica/impl/ob/v;)V

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/yandex/metrica/impl/ob/bl;-><init>(Lcom/yandex/metrica/impl/ob/v;Lcom/yandex/metrica/impl/ob/bl$a;Lcom/yandex/metrica/impl/ob/bg;Lcom/yandex/metrica/impl/ob/bg;)V

    .line 48
    return-void
.end method

.method public constructor <init>(Lcom/yandex/metrica/impl/ob/v;Lcom/yandex/metrica/impl/ob/bl$a;Lcom/yandex/metrica/impl/ob/bg;Lcom/yandex/metrica/impl/ob/bg;)V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/bl;->f:Lcom/yandex/metrica/impl/ob/bl$b;

    .line 56
    iput-object p1, p0, Lcom/yandex/metrica/impl/ob/bl;->a:Lcom/yandex/metrica/impl/ob/v;

    .line 57
    iput-object p2, p0, Lcom/yandex/metrica/impl/ob/bl;->b:Lcom/yandex/metrica/impl/ob/bl$a;

    .line 59
    iput-object p3, p0, Lcom/yandex/metrica/impl/ob/bl;->c:Lcom/yandex/metrica/impl/ob/bg;

    .line 60
    iput-object p4, p0, Lcom/yandex/metrica/impl/ob/bl;->d:Lcom/yandex/metrica/impl/ob/bg;

    .line 61
    return-void
.end method

.method private a(Lcom/yandex/metrica/impl/ob/bh;Lcom/yandex/metrica/impl/i;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 166
    if-nez p1, :cond_0

    .line 173
    :goto_0
    return v0

    .line 169
    :cond_0
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ob/bh;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 170
    const/4 v0, 0x1

    goto :goto_0

    .line 172
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/yandex/metrica/impl/ob/bl;->b(Lcom/yandex/metrica/impl/ob/bh;Lcom/yandex/metrica/impl/i;)V

    goto :goto_0
.end method

.method private static b(Lcom/yandex/metrica/impl/ob/bh;)Lcom/yandex/metrica/impl/ob/bm;
    .locals 4

    .prologue
    .line 209
    new-instance v0, Lcom/yandex/metrica/impl/ob/bm;

    invoke-direct {v0}, Lcom/yandex/metrica/impl/ob/bm;-><init>()V

    .line 210
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/bh;->c()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/yandex/metrica/impl/ob/bm;->a(J)Lcom/yandex/metrica/impl/ob/bm;

    move-result-object v0

    .line 211
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/bh;->i()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/yandex/metrica/impl/ob/bm;->b(J)Lcom/yandex/metrica/impl/ob/bm;

    move-result-object v0

    .line 212
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/bh;->f()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/yandex/metrica/impl/ob/bm;->c(J)Lcom/yandex/metrica/impl/ob/bm;

    move-result-object v0

    .line 213
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/bh;->a()Lcom/yandex/metrica/impl/ob/bp;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/bm;->a(Lcom/yandex/metrica/impl/ob/bp;)Lcom/yandex/metrica/impl/ob/bm;

    move-result-object v0

    .line 209
    return-object v0
.end method

.method private b(Lcom/yandex/metrica/impl/ob/bh;Lcom/yandex/metrica/impl/i;)V
    .locals 3

    .prologue
    .line 179
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ob/bh;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 180
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bl;->b:Lcom/yandex/metrica/impl/ob/bl$a;

    .line 4357
    sget-object v1, Lcom/yandex/metrica/impl/q$a;->i:Lcom/yandex/metrica/impl/q$a;

    invoke-static {p2, v1}, Lcom/yandex/metrica/impl/i;->a(Lcom/yandex/metrica/impl/i;Lcom/yandex/metrica/impl/q$a;)Lcom/yandex/metrica/impl/i;

    move-result-object v1

    .line 180
    invoke-virtual {p0, p1}, Lcom/yandex/metrica/impl/ob/bl;->a(Lcom/yandex/metrica/impl/ob/bh;)Lcom/yandex/metrica/impl/ob/bm;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/yandex/metrica/impl/ob/bl$a;->a(Lcom/yandex/metrica/impl/i;Lcom/yandex/metrica/impl/ob/bm;)V

    .line 181
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/yandex/metrica/impl/ob/bh;->a(Z)V

    .line 183
    :cond_0
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ob/bh;->g()V

    .line 184
    return-void
.end method

.method private f(Lcom/yandex/metrica/impl/i;)V
    .locals 2

    .prologue
    .line 147
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bl;->f:Lcom/yandex/metrica/impl/ob/bl$b;

    if-nez v0, :cond_0

    .line 148
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bl;->c:Lcom/yandex/metrica/impl/ob/bg;

    invoke-interface {v0}, Lcom/yandex/metrica/impl/ob/bg;->a()Lcom/yandex/metrica/impl/ob/bh;

    move-result-object v0

    .line 149
    invoke-direct {p0, v0, p1}, Lcom/yandex/metrica/impl/ob/bl;->a(Lcom/yandex/metrica/impl/ob/bh;Lcom/yandex/metrica/impl/i;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 150
    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/bl;->e:Lcom/yandex/metrica/impl/ob/bh;

    .line 151
    sget-object v0, Lcom/yandex/metrica/impl/ob/bl$b;->c:Lcom/yandex/metrica/impl/ob/bl$b;

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/bl;->f:Lcom/yandex/metrica/impl/ob/bl$b;

    .line 163
    :cond_0
    :goto_0
    return-void

    .line 153
    :cond_1
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bl;->d:Lcom/yandex/metrica/impl/ob/bg;

    invoke-interface {v0}, Lcom/yandex/metrica/impl/ob/bg;->a()Lcom/yandex/metrica/impl/ob/bh;

    move-result-object v0

    .line 154
    invoke-direct {p0, v0, p1}, Lcom/yandex/metrica/impl/ob/bl;->a(Lcom/yandex/metrica/impl/ob/bh;Lcom/yandex/metrica/impl/i;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 155
    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/bl;->e:Lcom/yandex/metrica/impl/ob/bh;

    .line 156
    sget-object v0, Lcom/yandex/metrica/impl/ob/bl$b;->b:Lcom/yandex/metrica/impl/ob/bl$b;

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/bl;->f:Lcom/yandex/metrica/impl/ob/bl$b;

    goto :goto_0

    .line 158
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/bl;->e:Lcom/yandex/metrica/impl/ob/bh;

    .line 159
    sget-object v0, Lcom/yandex/metrica/impl/ob/bl$b;->a:Lcom/yandex/metrica/impl/ob/bl$b;

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/bl;->f:Lcom/yandex/metrica/impl/ob/bl$b;

    goto :goto_0
.end method


# virtual methods
.method public a()Lcom/yandex/metrica/impl/ob/bh;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bl;->e:Lcom/yandex/metrica/impl/ob/bh;

    return-object v0
.end method

.method a(Lcom/yandex/metrica/impl/ob/bh;)Lcom/yandex/metrica/impl/ob/bm;
    .locals 4

    .prologue
    .line 201
    new-instance v0, Lcom/yandex/metrica/impl/ob/bm;

    invoke-direct {v0}, Lcom/yandex/metrica/impl/ob/bm;-><init>()V

    .line 202
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ob/bh;->c()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/yandex/metrica/impl/ob/bm;->a(J)Lcom/yandex/metrica/impl/ob/bm;

    move-result-object v0

    .line 203
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ob/bh;->a()Lcom/yandex/metrica/impl/ob/bp;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/bm;->a(Lcom/yandex/metrica/impl/ob/bp;)Lcom/yandex/metrica/impl/ob/bm;

    move-result-object v0

    .line 204
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ob/bh;->i()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/yandex/metrica/impl/ob/bm;->b(J)Lcom/yandex/metrica/impl/ob/bm;

    move-result-object v0

    .line 205
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ob/bh;->d()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/yandex/metrica/impl/ob/bm;->c(J)Lcom/yandex/metrica/impl/ob/bm;

    move-result-object v0

    .line 201
    return-object v0
.end method

.method public a(Lcom/yandex/metrica/impl/i;)V
    .locals 2

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/yandex/metrica/impl/ob/bl;->f(Lcom/yandex/metrica/impl/i;)V

    .line 65
    sget-object v0, Lcom/yandex/metrica/impl/ob/bl$1;->a:[I

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/bl;->f:Lcom/yandex/metrica/impl/ob/bl$b;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/bl$b;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 81
    :goto_0
    return-void

    .line 67
    :pswitch_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bl;->e:Lcom/yandex/metrica/impl/ob/bh;

    invoke-direct {p0, v0, p1}, Lcom/yandex/metrica/impl/ob/bl;->a(Lcom/yandex/metrica/impl/ob/bh;Lcom/yandex/metrica/impl/i;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 68
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bl;->e:Lcom/yandex/metrica/impl/ob/bh;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/bh;->h()V

    goto :goto_0

    .line 70
    :cond_0
    invoke-virtual {p0, p1}, Lcom/yandex/metrica/impl/ob/bl;->e(Lcom/yandex/metrica/impl/i;)Lcom/yandex/metrica/impl/ob/bh;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/bl;->e:Lcom/yandex/metrica/impl/ob/bh;

    goto :goto_0

    .line 74
    :pswitch_1
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bl;->e:Lcom/yandex/metrica/impl/ob/bh;

    invoke-direct {p0, v0, p1}, Lcom/yandex/metrica/impl/ob/bl;->b(Lcom/yandex/metrica/impl/ob/bh;Lcom/yandex/metrica/impl/i;)V

    .line 75
    invoke-virtual {p0, p1}, Lcom/yandex/metrica/impl/ob/bl;->e(Lcom/yandex/metrica/impl/i;)Lcom/yandex/metrica/impl/ob/bh;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/bl;->e:Lcom/yandex/metrica/impl/ob/bh;

    goto :goto_0

    .line 78
    :pswitch_2
    invoke-virtual {p0, p1}, Lcom/yandex/metrica/impl/ob/bl;->e(Lcom/yandex/metrica/impl/i;)Lcom/yandex/metrica/impl/ob/bh;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/bl;->e:Lcom/yandex/metrica/impl/ob/bh;

    goto :goto_0

    .line 65
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(Lcom/yandex/metrica/impl/i;Z)V
    .locals 1

    .prologue
    .line 121
    invoke-virtual {p0, p1}, Lcom/yandex/metrica/impl/ob/bl;->c(Lcom/yandex/metrica/impl/i;)Lcom/yandex/metrica/impl/ob/bh;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/yandex/metrica/impl/ob/bh;->a(Z)V

    .line 122
    return-void
.end method

.method public b()Lcom/yandex/metrica/impl/ob/bm;
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 3029
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    .line 127
    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/bl;->a:Lcom/yandex/metrica/impl/ob/v;

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/ob/v;->j()Lcom/yandex/metrica/impl/ob/co;

    move-result-object v2

    sget-object v3, Lcom/yandex/metrica/impl/ob/bp;->b:Lcom/yandex/metrica/impl/ob/bp;

    invoke-virtual {v2, v0, v1, v3}, Lcom/yandex/metrica/impl/ob/co;->a(JLcom/yandex/metrica/impl/ob/bp;)V

    .line 128
    new-instance v2, Lcom/yandex/metrica/impl/ob/bm;

    invoke-direct {v2}, Lcom/yandex/metrica/impl/ob/bm;-><init>()V

    .line 129
    invoke-virtual {v2, v0, v1}, Lcom/yandex/metrica/impl/ob/bm;->a(J)Lcom/yandex/metrica/impl/ob/bm;

    move-result-object v0

    sget-object v1, Lcom/yandex/metrica/impl/ob/bp;->b:Lcom/yandex/metrica/impl/ob/bp;

    .line 130
    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/bm;->a(Lcom/yandex/metrica/impl/ob/bp;)Lcom/yandex/metrica/impl/ob/bm;

    move-result-object v0

    .line 131
    invoke-virtual {v0, v4, v5}, Lcom/yandex/metrica/impl/ob/bm;->b(J)Lcom/yandex/metrica/impl/ob/bm;

    move-result-object v0

    .line 132
    invoke-virtual {v0, v4, v5}, Lcom/yandex/metrica/impl/ob/bm;->c(J)Lcom/yandex/metrica/impl/ob/bm;

    move-result-object v0

    .line 128
    return-object v0
.end method

.method public b(Lcom/yandex/metrica/impl/i;)V
    .locals 2

    .prologue
    .line 84
    invoke-direct {p0, p1}, Lcom/yandex/metrica/impl/ob/bl;->f(Lcom/yandex/metrica/impl/i;)V

    .line 85
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bl;->f:Lcom/yandex/metrica/impl/ob/bl$b;

    sget-object v1, Lcom/yandex/metrica/impl/ob/bl$b;->a:Lcom/yandex/metrica/impl/ob/bl$b;

    if-eq v0, v1, :cond_0

    .line 86
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bl;->e:Lcom/yandex/metrica/impl/ob/bh;

    invoke-direct {p0, v0, p1}, Lcom/yandex/metrica/impl/ob/bl;->b(Lcom/yandex/metrica/impl/ob/bh;Lcom/yandex/metrica/impl/i;)V

    .line 88
    :cond_0
    sget-object v0, Lcom/yandex/metrica/impl/ob/bl$b;->a:Lcom/yandex/metrica/impl/ob/bl$b;

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/bl;->f:Lcom/yandex/metrica/impl/ob/bl$b;

    .line 89
    return-void
.end method

.method public c(Lcom/yandex/metrica/impl/i;)Lcom/yandex/metrica/impl/ob/bh;
    .locals 4

    .prologue
    .line 92
    invoke-direct {p0, p1}, Lcom/yandex/metrica/impl/ob/bl;->f(Lcom/yandex/metrica/impl/i;)V

    .line 93
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bl;->f:Lcom/yandex/metrica/impl/ob/bl$b;

    sget-object v1, Lcom/yandex/metrica/impl/ob/bl$b;->a:Lcom/yandex/metrica/impl/ob/bl$b;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bl;->e:Lcom/yandex/metrica/impl/ob/bh;

    invoke-direct {p0, v0, p1}, Lcom/yandex/metrica/impl/ob/bl;->a(Lcom/yandex/metrica/impl/ob/bh;Lcom/yandex/metrica/impl/i;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 94
    sget-object v0, Lcom/yandex/metrica/impl/ob/bl$b;->a:Lcom/yandex/metrica/impl/ob/bl$b;

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/bl;->f:Lcom/yandex/metrica/impl/ob/bl$b;

    .line 95
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/bl;->e:Lcom/yandex/metrica/impl/ob/bh;

    .line 97
    :cond_0
    sget-object v0, Lcom/yandex/metrica/impl/ob/bl$1;->a:[I

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/bl;->f:Lcom/yandex/metrica/impl/ob/bl$b;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/bl$b;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1187
    sget-object v0, Lcom/yandex/metrica/impl/ob/bl$b;->b:Lcom/yandex/metrica/impl/ob/bl$b;

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/bl;->f:Lcom/yandex/metrica/impl/ob/bl$b;

    .line 1188
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bl;->d:Lcom/yandex/metrica/impl/ob/bg;

    invoke-interface {v0}, Lcom/yandex/metrica/impl/ob/bg;->b()Lcom/yandex/metrica/impl/ob/bh;

    move-result-object v0

    .line 1190
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/bl;->a:Lcom/yandex/metrica/impl/ob/v;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/v;->D()Lcom/yandex/metrica/impl/ob/db;

    move-result-object v1

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/db;->d()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1191
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/bl;->b:Lcom/yandex/metrica/impl/ob/bl$a;

    .line 1361
    sget-object v2, Lcom/yandex/metrica/impl/q$a;->B:Lcom/yandex/metrica/impl/q$a;

    invoke-static {p1, v2}, Lcom/yandex/metrica/impl/i;->a(Lcom/yandex/metrica/impl/i;Lcom/yandex/metrica/impl/q$a;)Lcom/yandex/metrica/impl/i;

    move-result-object v2

    .line 1191
    invoke-static {v0}, Lcom/yandex/metrica/impl/ob/bl;->b(Lcom/yandex/metrica/impl/ob/bh;)Lcom/yandex/metrica/impl/ob/bm;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/yandex/metrica/impl/ob/bl$a;->a(Lcom/yandex/metrica/impl/i;Lcom/yandex/metrica/impl/ob/bm;)V

    .line 105
    :cond_1
    :goto_0
    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/bl;->e:Lcom/yandex/metrica/impl/ob/bh;

    .line 106
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bl;->e:Lcom/yandex/metrica/impl/ob/bh;

    :goto_1
    return-object v0

    .line 99
    :pswitch_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bl;->e:Lcom/yandex/metrica/impl/ob/bh;

    goto :goto_1

    .line 101
    :pswitch_1
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bl;->e:Lcom/yandex/metrica/impl/ob/bh;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/bh;->h()V

    .line 102
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bl;->e:Lcom/yandex/metrica/impl/ob/bh;

    goto :goto_1

    .line 1192
    :cond_2
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/i;->c()I

    move-result v1

    sget-object v2, Lcom/yandex/metrica/impl/q$a;->A:Lcom/yandex/metrica/impl/q$a;

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/q$a;->a()I

    move-result v2

    if-ne v1, v2, :cond_1

    .line 1193
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/bl;->b:Lcom/yandex/metrica/impl/ob/bl$a;

    invoke-static {v0}, Lcom/yandex/metrica/impl/ob/bl;->b(Lcom/yandex/metrica/impl/ob/bh;)Lcom/yandex/metrica/impl/ob/bm;

    move-result-object v2

    invoke-interface {v1, p1, v2}, Lcom/yandex/metrica/impl/ob/bl$a;->a(Lcom/yandex/metrica/impl/i;Lcom/yandex/metrica/impl/ob/bm;)V

    .line 1194
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/bl;->b:Lcom/yandex/metrica/impl/ob/bl$a;

    .line 2361
    sget-object v2, Lcom/yandex/metrica/impl/q$a;->B:Lcom/yandex/metrica/impl/q$a;

    invoke-static {p1, v2}, Lcom/yandex/metrica/impl/i;->a(Lcom/yandex/metrica/impl/i;Lcom/yandex/metrica/impl/q$a;)Lcom/yandex/metrica/impl/i;

    move-result-object v2

    .line 1194
    invoke-static {v0}, Lcom/yandex/metrica/impl/ob/bl;->b(Lcom/yandex/metrica/impl/ob/bh;)Lcom/yandex/metrica/impl/ob/bm;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/yandex/metrica/impl/ob/bl$a;->a(Lcom/yandex/metrica/impl/i;Lcom/yandex/metrica/impl/ob/bm;)V

    goto :goto_0

    .line 97
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public d(Lcom/yandex/metrica/impl/i;)Lcom/yandex/metrica/impl/ob/bm;
    .locals 1

    .prologue
    .line 111
    invoke-virtual {p0, p1}, Lcom/yandex/metrica/impl/ob/bl;->c(Lcom/yandex/metrica/impl/i;)Lcom/yandex/metrica/impl/ob/bh;

    move-result-object v0

    .line 113
    invoke-static {v0}, Lcom/yandex/metrica/impl/ob/bl;->b(Lcom/yandex/metrica/impl/ob/bh;)Lcom/yandex/metrica/impl/ob/bm;

    move-result-object v0

    return-object v0
.end method

.method e(Lcom/yandex/metrica/impl/i;)Lcom/yandex/metrica/impl/ob/bh;
    .locals 4

    .prologue
    .line 138
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bl;->c:Lcom/yandex/metrica/impl/ob/bg;

    invoke-interface {v0}, Lcom/yandex/metrica/impl/ob/bg;->b()Lcom/yandex/metrica/impl/ob/bh;

    move-result-object v0

    .line 139
    sget-object v1, Lcom/yandex/metrica/impl/ob/bl$b;->c:Lcom/yandex/metrica/impl/ob/bl$b;

    iput-object v1, p0, Lcom/yandex/metrica/impl/ob/bl;->f:Lcom/yandex/metrica/impl/ob/bl$b;

    .line 141
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/bl;->a:Lcom/yandex/metrica/impl/ob/v;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/yandex/metrica/impl/ob/v;->b(Z)V

    .line 142
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/bl;->b:Lcom/yandex/metrica/impl/ob/bl$a;

    .line 3361
    sget-object v2, Lcom/yandex/metrica/impl/q$a;->B:Lcom/yandex/metrica/impl/q$a;

    invoke-static {p1, v2}, Lcom/yandex/metrica/impl/i;->a(Lcom/yandex/metrica/impl/i;Lcom/yandex/metrica/impl/q$a;)Lcom/yandex/metrica/impl/i;

    move-result-object v2

    .line 142
    invoke-static {v0}, Lcom/yandex/metrica/impl/ob/bl;->b(Lcom/yandex/metrica/impl/ob/bh;)Lcom/yandex/metrica/impl/ob/bm;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/yandex/metrica/impl/ob/bl$a;->a(Lcom/yandex/metrica/impl/i;Lcom/yandex/metrica/impl/ob/bm;)V

    .line 143
    return-object v0
.end method
