.class public Lcom/my/target/az;
.super Ljava/lang/Object;
.source "LogMessage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/az$b;,
        Lcom/my/target/az$c;,
        Lcom/my/target/az$a;
    }
.end annotation


# static fields
.field private static dR:Ljava/lang/String;

.field public static dS:Z


# instance fields
.field private aB:Ljava/lang/String;

.field private dT:Ljava/lang/String;

.field private dU:Ljava/lang/String;

.field private dV:Ljava/lang/String;

.field private final name:Ljava/lang/String;

.field private slotId:I

.field private final type:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-string v0, "https://ad.mail.ru/sdk/log/"

    sput-object v0, Lcom/my/target/az;->dR:Ljava/lang/String;

    .line 30
    const/4 v0, 0x1

    sput-boolean v0, Lcom/my/target/az;->dS:Z

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/my/target/az;->name:Ljava/lang/String;

    .line 48
    iput-object p2, p0, Lcom/my/target/az;->type:Ljava/lang/String;

    .line 49
    return-void
.end method

.method static synthetic ap()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/my/target/az;->dR:Ljava/lang/String;

    return-object v0
.end method

.method public static y(Ljava/lang/String;)Lcom/my/target/az;
    .locals 2

    .prologue
    .line 34
    new-instance v0, Lcom/my/target/az;

    const-string v1, "error"

    invoke-direct {v0, p0, v1}, Lcom/my/target/az;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public A(Ljava/lang/String;)Lcom/my/target/az;
    .locals 0

    .prologue
    .line 65
    iput-object p1, p0, Lcom/my/target/az;->dU:Ljava/lang/String;

    .line 66
    return-object p0
.end method

.method public B(Ljava/lang/String;)Lcom/my/target/az;
    .locals 0

    .prologue
    .line 71
    iput-object p1, p0, Lcom/my/target/az;->aB:Ljava/lang/String;

    .line 72
    return-object p0
.end method

.method public C(Ljava/lang/String;)Lcom/my/target/az;
    .locals 0

    .prologue
    .line 77
    iput-object p1, p0, Lcom/my/target/az;->dV:Ljava/lang/String;

    .line 78
    return-object p0
.end method

.method ao()Ljava/lang/String;
    .locals 3

    .prologue
    .line 102
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 105
    :try_start_0
    const-string v1, "sdk"

    const-string v2, "myTarget"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 106
    const-string v1, "sdkver"

    const-string v2, "5.1.0"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 107
    const-string v1, "os"

    const-string v2, "Android"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 108
    const-string v1, "osver"

    sget-object v2, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 109
    const-string v1, "type"

    iget-object v2, p0, Lcom/my/target/az;->type:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 110
    const-string v1, "name"

    iget-object v2, p0, Lcom/my/target/az;->name:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 111
    iget-object v1, p0, Lcom/my/target/az;->dT:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 113
    const-string v1, "message"

    iget-object v2, p0, Lcom/my/target/az;->dT:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 115
    :cond_0
    iget v1, p0, Lcom/my/target/az;->slotId:I

    if-lez v1, :cond_1

    .line 117
    const-string v1, "slot"

    iget v2, p0, Lcom/my/target/az;->slotId:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 119
    :cond_1
    iget-object v1, p0, Lcom/my/target/az;->dU:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 121
    const-string v1, "url"

    iget-object v2, p0, Lcom/my/target/az;->dU:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 123
    :cond_2
    iget-object v1, p0, Lcom/my/target/az;->aB:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 125
    const-string v1, "bannerId"

    iget-object v2, p0, Lcom/my/target/az;->aB:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 127
    :cond_3
    iget-object v1, p0, Lcom/my/target/az;->dV:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 129
    const-string v1, "data"

    iget-object v2, p0, Lcom/my/target/az;->dV:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 136
    :cond_4
    :goto_0
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 132
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public e(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 83
    new-instance v0, Lcom/my/target/az$1;

    invoke-direct {v0, p0, p1}, Lcom/my/target/az$1;-><init>(Lcom/my/target/az;Landroid/content/Context;)V

    invoke-static {v0}, Lcom/my/target/h;->b(Ljava/lang/Runnable;)V

    .line 97
    return-void
.end method

.method public h(I)Lcom/my/target/az;
    .locals 0

    .prologue
    .line 59
    iput p1, p0, Lcom/my/target/az;->slotId:I

    .line 60
    return-object p0
.end method

.method public z(Ljava/lang/String;)Lcom/my/target/az;
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lcom/my/target/az;->dT:Ljava/lang/String;

    .line 54
    return-object p0
.end method
