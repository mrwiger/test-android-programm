.class public final Lcom/my/target/core/engines/j;
.super Ljava/lang/Object;
.source "InstreamAdEngine.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/core/engines/j$a;
    }
.end annotation


# instance fields
.field private A:Lcom/my/target/instreamads/InstreamAd$InstreamAdBanner;

.field private B:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/my/target/aj",
            "<",
            "Lcom/my/target/common/models/VideoData;",
            ">;>;"
        }
    .end annotation
.end field

.field private C:F

.field private D:I

.field private E:I

.field private F:Z

.field private final adConfig:Lcom/my/target/b;

.field private loadingTimeoutSeconds:I

.field private midpoints:[F

.field private final u:Lcom/my/target/instreamads/InstreamAd;

.field private final v:Lcom/my/target/fq;

.field private final w:Lcom/my/target/core/controllers/d;

.field private final x:Lcom/my/target/ce;

.field private y:Lcom/my/target/al;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/my/target/al",
            "<",
            "Lcom/my/target/common/models/VideoData;",
            ">;"
        }
    .end annotation
.end field

.field private z:Lcom/my/target/aj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/my/target/aj",
            "<",
            "Lcom/my/target/common/models/VideoData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/my/target/instreamads/InstreamAd;Lcom/my/target/fq;Lcom/my/target/b;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    new-array v0, v2, [F

    iput-object v0, p0, Lcom/my/target/core/engines/j;->midpoints:[F

    .line 66
    iput-object p1, p0, Lcom/my/target/core/engines/j;->u:Lcom/my/target/instreamads/InstreamAd;

    .line 67
    iput-object p2, p0, Lcom/my/target/core/engines/j;->v:Lcom/my/target/fq;

    .line 68
    iput-object p3, p0, Lcom/my/target/core/engines/j;->adConfig:Lcom/my/target/b;

    .line 69
    invoke-static {}, Lcom/my/target/core/controllers/d;->f()Lcom/my/target/core/controllers/d;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/core/engines/j;->w:Lcom/my/target/core/controllers/d;

    .line 70
    iget-object v0, p0, Lcom/my/target/core/engines/j;->w:Lcom/my/target/core/controllers/d;

    new-instance v1, Lcom/my/target/core/engines/j$a;

    invoke-direct {v1, p0, v2}, Lcom/my/target/core/engines/j$a;-><init>(Lcom/my/target/core/engines/j;B)V

    invoke-virtual {v0, v1}, Lcom/my/target/core/controllers/d;->a(Lcom/my/target/core/controllers/d$d;)V

    .line 71
    invoke-static {}, Lcom/my/target/ce;->bw()Lcom/my/target/ce;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/core/engines/j;->x:Lcom/my/target/ce;

    .line 72
    return-void
.end method

.method static synthetic a(Lcom/my/target/core/engines/j;)Lcom/my/target/al;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/my/target/core/engines/j;->y:Lcom/my/target/al;

    return-object v0
.end method

.method public static a(Lcom/my/target/instreamads/InstreamAd;Lcom/my/target/fq;Lcom/my/target/b;)Lcom/my/target/core/engines/j;
    .locals 1

    .prologue
    .line 43
    new-instance v0, Lcom/my/target/core/engines/j;

    invoke-direct {v0, p0, p1, p2}, Lcom/my/target/core/engines/j;-><init>(Lcom/my/target/instreamads/InstreamAd;Lcom/my/target/fq;Lcom/my/target/b;)V

    return-object v0
.end method

.method private a(Lcom/my/target/ae;Lcom/my/target/al;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/my/target/ae;",
            "Lcom/my/target/al",
            "<",
            "Lcom/my/target/common/models/VideoData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 346
    iget-object v0, p0, Lcom/my/target/core/engines/j;->w:Lcom/my/target/core/controllers/d;

    invoke-virtual {v0}, Lcom/my/target/core/controllers/d;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 347
    if-nez v0, :cond_0

    .line 349
    const-string v0, "can\'t load doAfter service: context is null"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 361
    :goto_0
    return-void

    .line 352
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "loading doAfter service: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/my/target/ae;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 353
    iget-object v1, p0, Lcom/my/target/core/engines/j;->adConfig:Lcom/my/target/b;

    iget v2, p0, Lcom/my/target/core/engines/j;->loadingTimeoutSeconds:I

    invoke-static {p1, v1, v2}, Lcom/my/target/fn;->newFactoryForAdService(Lcom/my/target/ae;Lcom/my/target/b;I)Lcom/my/target/c;

    move-result-object v1

    new-instance v2, Lcom/my/target/core/engines/j$1;

    invoke-direct {v2, p0, p2}, Lcom/my/target/core/engines/j$1;-><init>(Lcom/my/target/core/engines/j;Lcom/my/target/al;)V

    invoke-virtual {v1, v2}, Lcom/my/target/c;->a(Lcom/my/target/c$b;)Lcom/my/target/c;

    move-result-object v1

    .line 360
    invoke-virtual {v1, v0}, Lcom/my/target/c;->a(Landroid/content/Context;)Lcom/my/target/c;

    goto :goto_0
.end method

.method private a(Lcom/my/target/aj;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 444
    if-nez p1, :cond_0

    .line 446
    const-string v0, "can\'t send stat: banner is null"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 456
    :goto_0
    return-void

    .line 449
    :cond_0
    iget-object v0, p0, Lcom/my/target/core/engines/j;->w:Lcom/my/target/core/controllers/d;

    invoke-virtual {v0}, Lcom/my/target/core/controllers/d;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 450
    if-nez v0, :cond_1

    .line 452
    const-string v0, "can\'t send stat: context is null"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 455
    :cond_1
    invoke-virtual {p1}, Lcom/my/target/aj;->getStatHolder()Lcom/my/target/ar;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/my/target/ar;->w(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/my/target/cl;->a(Ljava/util/List;Landroid/content/Context;)V

    goto :goto_0
.end method

.method private a(Lcom/my/target/al;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 325
    iget-object v0, p0, Lcom/my/target/core/engines/j;->y:Lcom/my/target/al;

    if-ne p1, v0, :cond_1

    .line 327
    const-string v0, "midroll"

    invoke-virtual {p1}, Lcom/my/target/al;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 329
    iget-object v0, p0, Lcom/my/target/core/engines/j;->y:Lcom/my/target/al;

    iget v1, p0, Lcom/my/target/core/engines/j;->E:I

    invoke-virtual {v0, v1}, Lcom/my/target/al;->f(I)V

    .line 331
    :cond_0
    iput-object v2, p0, Lcom/my/target/core/engines/j;->y:Lcom/my/target/al;

    .line 332
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/my/target/core/engines/j;->F:Z

    .line 333
    iput-object v2, p0, Lcom/my/target/core/engines/j;->z:Lcom/my/target/aj;

    .line 334
    iput-object v2, p0, Lcom/my/target/core/engines/j;->A:Lcom/my/target/instreamads/InstreamAd$InstreamAdBanner;

    .line 335
    const/4 v0, -0x1

    iput v0, p0, Lcom/my/target/core/engines/j;->D:I

    .line 336
    iget-object v0, p0, Lcom/my/target/core/engines/j;->u:Lcom/my/target/instreamads/InstreamAd;

    invoke-virtual {v0}, Lcom/my/target/instreamads/InstreamAd;->getListener()Lcom/my/target/instreamads/InstreamAd$InstreamAdListener;

    move-result-object v0

    .line 337
    if-eqz v0, :cond_1

    .line 339
    invoke-virtual {p1}, Lcom/my/target/al;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/my/target/core/engines/j;->u:Lcom/my/target/instreamads/InstreamAd;

    invoke-interface {v0, v1, v2}, Lcom/my/target/instreamads/InstreamAd$InstreamAdListener;->onComplete(Ljava/lang/String;Lcom/my/target/instreamads/InstreamAd;)V

    .line 342
    :cond_1
    return-void
.end method

.method private a(Lcom/my/target/al;F)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/my/target/al",
            "<",
            "Lcom/my/target/common/models/VideoData;",
            ">;F)V"
        }
    .end annotation

    .prologue
    .line 230
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 231
    invoke-virtual {p1}, Lcom/my/target/al;->R()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/aj;

    .line 233
    invoke-virtual {v0}, Lcom/my/target/aj;->getPoint()F

    move-result v3

    cmpl-float v3, v3, p2

    if-nez v3, :cond_0

    .line 235
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 238
    :cond_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 239
    if-lez v0, :cond_2

    iget v2, p0, Lcom/my/target/core/engines/j;->D:I

    add-int/lit8 v0, v0, -0x1

    if-ge v2, v0, :cond_2

    .line 241
    iput-object v1, p0, Lcom/my/target/core/engines/j;->B:Ljava/util/List;

    .line 242
    invoke-direct {p0}, Lcom/my/target/core/engines/j;->g()V

    .line 257
    :goto_1
    return-void

    .line 246
    :cond_2
    invoke-virtual {p1, p2}, Lcom/my/target/al;->a(F)Ljava/util/ArrayList;

    move-result-object v0

    .line 247
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_3

    .line 249
    invoke-direct {p0, v0, p1, p2}, Lcom/my/target/core/engines/j;->a(Ljava/util/ArrayList;Lcom/my/target/al;F)V

    goto :goto_1

    .line 253
    :cond_3
    const-string v0, "There is no one midpoint service for point: "

    invoke-static {p2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 254
    invoke-direct {p0, p1, p2}, Lcom/my/target/core/engines/j;->b(Lcom/my/target/al;F)V

    goto :goto_1
.end method

.method static synthetic a(Lcom/my/target/core/engines/j;Lcom/my/target/al;Lcom/my/target/fq;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1367
    if-nez p2, :cond_2

    .line 1369
    if-eqz p3, :cond_0

    .line 1371
    const-string v0, "loading doAfter service failed: "

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 1373
    :cond_0
    iget-object v0, p0, Lcom/my/target/core/engines/j;->y:Lcom/my/target/al;

    if-ne p1, v0, :cond_1

    .line 1375
    iget v0, p0, Lcom/my/target/core/engines/j;->C:F

    invoke-direct {p0, p1, v0}, Lcom/my/target/core/engines/j;->b(Lcom/my/target/al;F)V

    .line 1377
    :cond_1
    :goto_0
    return-void

    .line 1380
    :cond_2
    invoke-virtual {p1}, Lcom/my/target/al;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/my/target/fq;->a(Ljava/lang/String;)Lcom/my/target/al;

    move-result-object v0

    .line 1381
    if-eqz v0, :cond_3

    .line 1383
    invoke-virtual {p1, v0}, Lcom/my/target/al;->a(Lcom/my/target/al;)V

    .line 1385
    :cond_3
    iget-object v0, p0, Lcom/my/target/core/engines/j;->y:Lcom/my/target/al;

    if-ne p1, v0, :cond_1

    .line 1387
    invoke-virtual {p1}, Lcom/my/target/al;->R()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/core/engines/j;->B:Ljava/util/List;

    .line 1388
    invoke-direct {p0}, Lcom/my/target/core/engines/j;->g()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/my/target/core/engines/j;Lcom/my/target/al;Lcom/my/target/fq;Ljava/lang/String;F)V
    .locals 2

    .prologue
    .line 37
    .line 1418
    if-nez p2, :cond_2

    .line 1420
    if-eqz p3, :cond_0

    .line 1422
    const-string v0, "loading midpoint services failed: "

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 1424
    :cond_0
    iget-object v0, p0, Lcom/my/target/core/engines/j;->y:Lcom/my/target/al;

    if-ne p1, v0, :cond_1

    iget v0, p0, Lcom/my/target/core/engines/j;->C:F

    cmpl-float v0, p4, v0

    if-nez v0, :cond_1

    .line 1426
    invoke-direct {p0, p1, p4}, Lcom/my/target/core/engines/j;->b(Lcom/my/target/al;F)V

    .line 1428
    :cond_1
    :goto_0
    return-void

    .line 1431
    :cond_2
    invoke-virtual {p1}, Lcom/my/target/al;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/my/target/fq;->a(Ljava/lang/String;)Lcom/my/target/al;

    move-result-object v0

    .line 1432
    if-eqz v0, :cond_3

    .line 1434
    invoke-virtual {p1, v0}, Lcom/my/target/al;->a(Lcom/my/target/al;)V

    .line 1436
    :cond_3
    iget-object v0, p0, Lcom/my/target/core/engines/j;->y:Lcom/my/target/al;

    if-ne p1, v0, :cond_1

    iget v0, p0, Lcom/my/target/core/engines/j;->C:F

    cmpl-float v0, p4, v0

    if-nez v0, :cond_1

    .line 1438
    invoke-direct {p0, p1, p4}, Lcom/my/target/core/engines/j;->a(Lcom/my/target/al;F)V

    goto :goto_0
.end method

.method private a(Ljava/util/ArrayList;Lcom/my/target/al;F)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/my/target/ae;",
            ">;",
            "Lcom/my/target/al",
            "<",
            "Lcom/my/target/common/models/VideoData;",
            ">;F)V"
        }
    .end annotation

    .prologue
    .line 396
    iget-object v0, p0, Lcom/my/target/core/engines/j;->w:Lcom/my/target/core/controllers/d;

    invoke-virtual {v0}, Lcom/my/target/core/controllers/d;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 397
    if-nez v0, :cond_0

    .line 399
    const-string v0, "can\'t load midpoint services: context is null"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 411
    :goto_0
    return-void

    .line 402
    :cond_0
    const-string v1, "loading midpoint services for point: "

    invoke-static {p3}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 403
    iget-object v1, p0, Lcom/my/target/core/engines/j;->adConfig:Lcom/my/target/b;

    iget v2, p0, Lcom/my/target/core/engines/j;->loadingTimeoutSeconds:I

    invoke-static {p1, v1, v2}, Lcom/my/target/fn;->newFactoryForAdServices(Ljava/util/List;Lcom/my/target/b;I)Lcom/my/target/c;

    move-result-object v1

    new-instance v2, Lcom/my/target/core/engines/j$2;

    invoke-direct {v2, p0, p2, p3}, Lcom/my/target/core/engines/j$2;-><init>(Lcom/my/target/core/engines/j;Lcom/my/target/al;F)V

    invoke-virtual {v1, v2}, Lcom/my/target/c;->a(Lcom/my/target/c$b;)Lcom/my/target/c;

    move-result-object v1

    .line 410
    invoke-virtual {v1, v0}, Lcom/my/target/c;->a(Landroid/content/Context;)Lcom/my/target/c;

    goto :goto_0
.end method

.method static synthetic b(Lcom/my/target/core/engines/j;)Lcom/my/target/aj;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/my/target/core/engines/j;->z:Lcom/my/target/aj;

    return-object v0
.end method

.method private b(Lcom/my/target/al;F)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/my/target/al",
            "<",
            "Lcom/my/target/common/models/VideoData;",
            ">;F)V"
        }
    .end annotation

    .prologue
    .line 300
    invoke-virtual {p1}, Lcom/my/target/al;->T()Lcom/my/target/ae;

    move-result-object v0

    .line 301
    if-eqz v0, :cond_1

    .line 303
    const-string v1, "midroll"

    invoke-virtual {p1}, Lcom/my/target/al;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 305
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/my/target/ae;->e(Z)V

    .line 306
    invoke-virtual {v0, p2}, Lcom/my/target/ae;->setPoint(F)V

    .line 307
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 308
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 309
    const-string v0, "using doAfter service for point: "

    invoke-static {p2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 310
    invoke-direct {p0, v1, p1, p2}, Lcom/my/target/core/engines/j;->a(Ljava/util/ArrayList;Lcom/my/target/al;F)V

    .line 321
    :goto_0
    return-void

    .line 314
    :cond_0
    invoke-direct {p0, v0, p1}, Lcom/my/target/core/engines/j;->a(Lcom/my/target/ae;Lcom/my/target/al;)V

    goto :goto_0

    .line 319
    :cond_1
    invoke-direct {p0, p1}, Lcom/my/target/core/engines/j;->a(Lcom/my/target/al;)V

    goto :goto_0
.end method

.method static synthetic c(Lcom/my/target/core/engines/j;)Lcom/my/target/instreamads/InstreamAd$InstreamAdBanner;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/my/target/core/engines/j;->A:Lcom/my/target/instreamads/InstreamAd$InstreamAdBanner;

    return-object v0
.end method

.method static synthetic d(Lcom/my/target/core/engines/j;)Z
    .locals 1

    .prologue
    .line 37
    iget-boolean v0, p0, Lcom/my/target/core/engines/j;->F:Z

    return v0
.end method

.method static synthetic e(Lcom/my/target/core/engines/j;)Z
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/my/target/core/engines/j;->F:Z

    return v0
.end method

.method static synthetic f(Lcom/my/target/core/engines/j;)Lcom/my/target/core/controllers/d;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/my/target/core/engines/j;->w:Lcom/my/target/core/controllers/d;

    return-object v0
.end method

.method static synthetic g(Lcom/my/target/core/engines/j;)Lcom/my/target/instreamads/InstreamAd;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/my/target/core/engines/j;->u:Lcom/my/target/instreamads/InstreamAd;

    return-object v0
.end method

.method private g()V
    .locals 3

    .prologue
    .line 261
    :goto_0
    iget-object v0, p0, Lcom/my/target/core/engines/j;->y:Lcom/my/target/al;

    if-nez v0, :cond_0

    .line 296
    :goto_1
    return-void

    .line 265
    :cond_0
    iget v0, p0, Lcom/my/target/core/engines/j;->E:I

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/my/target/core/engines/j;->B:Ljava/util/List;

    if-nez v0, :cond_2

    .line 267
    :cond_1
    iget-object v0, p0, Lcom/my/target/core/engines/j;->y:Lcom/my/target/al;

    iget v1, p0, Lcom/my/target/core/engines/j;->C:F

    invoke-direct {p0, v0, v1}, Lcom/my/target/core/engines/j;->b(Lcom/my/target/al;F)V

    goto :goto_1

    .line 271
    :cond_2
    iget v0, p0, Lcom/my/target/core/engines/j;->D:I

    add-int/lit8 v0, v0, 0x1

    .line 272
    iget-object v1, p0, Lcom/my/target/core/engines/j;->B:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_5

    .line 274
    iput v0, p0, Lcom/my/target/core/engines/j;->D:I

    .line 275
    iget-object v1, p0, Lcom/my/target/core/engines/j;->B:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/aj;

    .line 276
    const-string v1, "statistics"

    invoke-virtual {v0}, Lcom/my/target/aj;->getType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 278
    const-string v1, "playbackStarted"

    invoke-direct {p0, v0, v1}, Lcom/my/target/core/engines/j;->a(Lcom/my/target/aj;Ljava/lang/String;)V

    goto :goto_0

    .line 283
    :cond_3
    iget v1, p0, Lcom/my/target/core/engines/j;->E:I

    if-lez v1, :cond_4

    .line 285
    iget v1, p0, Lcom/my/target/core/engines/j;->E:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/my/target/core/engines/j;->E:I

    .line 287
    :cond_4
    iput-object v0, p0, Lcom/my/target/core/engines/j;->z:Lcom/my/target/aj;

    .line 288
    invoke-static {v0}, Lcom/my/target/instreamads/InstreamAd$InstreamAdBanner;->newBanner(Lcom/my/target/aj;)Lcom/my/target/instreamads/InstreamAd$InstreamAdBanner;

    move-result-object v1

    iput-object v1, p0, Lcom/my/target/core/engines/j;->A:Lcom/my/target/instreamads/InstreamAd$InstreamAdBanner;

    .line 289
    iget-object v1, p0, Lcom/my/target/core/engines/j;->w:Lcom/my/target/core/controllers/d;

    invoke-virtual {v1, v0}, Lcom/my/target/core/controllers/d;->play(Lcom/my/target/aj;)V

    goto :goto_1

    .line 294
    :cond_5
    iget-object v0, p0, Lcom/my/target/core/engines/j;->y:Lcom/my/target/al;

    iget v1, p0, Lcom/my/target/core/engines/j;->C:F

    invoke-direct {p0, v0, v1}, Lcom/my/target/core/engines/j;->b(Lcom/my/target/al;F)V

    goto :goto_1
.end method

.method static synthetic h(Lcom/my/target/core/engines/j;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/my/target/core/engines/j;->g()V

    return-void
.end method


# virtual methods
.method public final destroy()V
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Lcom/my/target/core/engines/j;->w:Lcom/my/target/core/controllers/d;

    invoke-virtual {v0}, Lcom/my/target/core/controllers/d;->destroy()V

    .line 221
    return-void
.end method

.method public final getPlayer()Lcom/my/target/instreamads/InstreamAdPlayer;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/my/target/core/engines/j;->w:Lcom/my/target/core/controllers/d;

    invoke-virtual {v0}, Lcom/my/target/core/controllers/d;->getPlayer()Lcom/my/target/instreamads/InstreamAdPlayer;

    move-result-object v0

    return-object v0
.end method

.method public final getVolume()F
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lcom/my/target/core/engines/j;->w:Lcom/my/target/core/controllers/d;

    invoke-virtual {v0}, Lcom/my/target/core/controllers/d;->getVolume()F

    move-result v0

    return v0
.end method

.method public final handleClick()V
    .locals 3

    .prologue
    .line 204
    iget-object v0, p0, Lcom/my/target/core/engines/j;->z:Lcom/my/target/aj;

    if-nez v0, :cond_0

    .line 206
    const-string v0, "can\'t handle click: no playing banner"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 216
    :goto_0
    return-void

    .line 209
    :cond_0
    iget-object v0, p0, Lcom/my/target/core/engines/j;->w:Lcom/my/target/core/controllers/d;

    invoke-virtual {v0}, Lcom/my/target/core/controllers/d;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 210
    if-nez v0, :cond_1

    .line 212
    const-string v0, "can\'t handle click: context is null"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 215
    :cond_1
    iget-object v1, p0, Lcom/my/target/core/engines/j;->x:Lcom/my/target/ce;

    iget-object v2, p0, Lcom/my/target/core/engines/j;->z:Lcom/my/target/aj;

    invoke-virtual {v1, v2, v0}, Lcom/my/target/ce;->a(Lcom/my/target/ah;Landroid/content/Context;)V

    goto :goto_0
.end method

.method public final pause()V
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/my/target/core/engines/j;->y:Lcom/my/target/al;

    if-eqz v0, :cond_0

    .line 168
    iget-object v0, p0, Lcom/my/target/core/engines/j;->w:Lcom/my/target/core/controllers/d;

    invoke-virtual {v0}, Lcom/my/target/core/controllers/d;->pause()V

    .line 170
    :cond_0
    return-void
.end method

.method public final resume()V
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/my/target/core/engines/j;->y:Lcom/my/target/al;

    if-eqz v0, :cond_0

    .line 176
    iget-object v0, p0, Lcom/my/target/core/engines/j;->w:Lcom/my/target/core/controllers/d;

    invoke-virtual {v0}, Lcom/my/target/core/controllers/d;->resume()V

    .line 178
    :cond_0
    return-void
.end method

.method public final setFullscreen(Z)V
    .locals 2
    .param p1, "fullscreen"    # Z

    .prologue
    .line 106
    const-string v0, "fullscreenOff"

    .line 107
    if-eqz p1, :cond_0

    .line 109
    const-string v0, "fullscreenOn"

    .line 111
    :cond_0
    iget-object v1, p0, Lcom/my/target/core/engines/j;->z:Lcom/my/target/aj;

    invoke-direct {p0, v1, v0}, Lcom/my/target/core/engines/j;->a(Lcom/my/target/aj;Ljava/lang/String;)V

    .line 112
    return-void
.end method

.method public final setLoadingTimeoutSeconds(I)V
    .locals 0
    .param p1, "loadingTimeoutSeconds"    # I

    .prologue
    .line 101
    iput p1, p0, Lcom/my/target/core/engines/j;->loadingTimeoutSeconds:I

    .line 102
    return-void
.end method

.method public final setMidpoints([F)V
    .locals 0
    .param p1, "midpoints"    # [F

    .prologue
    .line 96
    iput-object p1, p0, Lcom/my/target/core/engines/j;->midpoints:[F

    .line 97
    return-void
.end method

.method public final setPlayer(Lcom/my/target/instreamads/InstreamAdPlayer;)V
    .locals 1
    .param p1, "player"    # Lcom/my/target/instreamads/InstreamAdPlayer;

    .prologue
    .line 86
    iget-object v0, p0, Lcom/my/target/core/engines/j;->w:Lcom/my/target/core/controllers/d;

    invoke-virtual {v0, p1}, Lcom/my/target/core/controllers/d;->setPlayer(Lcom/my/target/instreamads/InstreamAdPlayer;)V

    .line 87
    return-void
.end method

.method public final setVolume(F)V
    .locals 1
    .param p1, "volume"    # F

    .prologue
    .line 76
    iget-object v0, p0, Lcom/my/target/core/engines/j;->w:Lcom/my/target/core/controllers/d;

    invoke-virtual {v0, p1}, Lcom/my/target/core/controllers/d;->setVolume(F)V

    .line 77
    return-void
.end method

.method public final skip()V
    .locals 2

    .prologue
    .line 191
    iget-object v0, p0, Lcom/my/target/core/engines/j;->z:Lcom/my/target/aj;

    const-string v1, "closedByUser"

    invoke-direct {p0, v0, v1}, Lcom/my/target/core/engines/j;->a(Lcom/my/target/aj;Ljava/lang/String;)V

    .line 192
    invoke-virtual {p0}, Lcom/my/target/core/engines/j;->stop()V

    .line 193
    return-void
.end method

.method public final skipBanner()V
    .locals 2

    .prologue
    .line 197
    iget-object v0, p0, Lcom/my/target/core/engines/j;->z:Lcom/my/target/aj;

    const-string v1, "closedByUser"

    invoke-direct {p0, v0, v1}, Lcom/my/target/core/engines/j;->a(Lcom/my/target/aj;Ljava/lang/String;)V

    .line 198
    iget-object v0, p0, Lcom/my/target/core/engines/j;->w:Lcom/my/target/core/controllers/d;

    invoke-virtual {v0}, Lcom/my/target/core/controllers/d;->stop()V

    .line 199
    invoke-direct {p0}, Lcom/my/target/core/engines/j;->g()V

    .line 200
    return-void
.end method

.method public final start(Ljava/lang/String;)V
    .locals 2
    .param p1, "sectionName"    # Ljava/lang/String;

    .prologue
    .line 116
    invoke-virtual {p0}, Lcom/my/target/core/engines/j;->stop()V

    .line 117
    iget-object v0, p0, Lcom/my/target/core/engines/j;->v:Lcom/my/target/fq;

    invoke-virtual {v0, p1}, Lcom/my/target/fq;->a(Ljava/lang/String;)Lcom/my/target/al;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/core/engines/j;->y:Lcom/my/target/al;

    .line 118
    iget-object v0, p0, Lcom/my/target/core/engines/j;->y:Lcom/my/target/al;

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/my/target/core/engines/j;->w:Lcom/my/target/core/controllers/d;

    iget-object v1, p0, Lcom/my/target/core/engines/j;->y:Lcom/my/target/al;

    invoke-virtual {v1}, Lcom/my/target/al;->P()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/my/target/core/controllers/d;->setConnectionTimeout(I)V

    .line 121
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/my/target/core/engines/j;->F:Z

    .line 122
    iget-object v0, p0, Lcom/my/target/core/engines/j;->y:Lcom/my/target/al;

    invoke-virtual {v0}, Lcom/my/target/al;->Q()I

    move-result v0

    iput v0, p0, Lcom/my/target/core/engines/j;->E:I

    .line 123
    const/4 v0, -0x1

    iput v0, p0, Lcom/my/target/core/engines/j;->D:I

    .line 124
    iget-object v0, p0, Lcom/my/target/core/engines/j;->y:Lcom/my/target/al;

    invoke-virtual {v0}, Lcom/my/target/al;->R()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/core/engines/j;->B:Ljava/util/List;

    .line 125
    invoke-direct {p0}, Lcom/my/target/core/engines/j;->g()V

    .line 131
    :goto_0
    return-void

    .line 129
    :cond_0
    const-string v0, "no section with name "

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final startMidroll(F)V
    .locals 5
    .param p1, "point"    # F

    .prologue
    const/4 v1, 0x0

    .line 135
    invoke-virtual {p0}, Lcom/my/target/core/engines/j;->stop()V

    .line 137
    iget-object v2, p0, Lcom/my/target/core/engines/j;->midpoints:[F

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_3

    aget v4, v2, v0

    .line 139
    invoke-static {v4, p1}, Ljava/lang/Float;->compare(FF)I

    move-result v4

    if-nez v4, :cond_1

    .line 141
    const/4 v0, 0x1

    .line 145
    :goto_1
    if-eqz v0, :cond_2

    .line 147
    iget-object v0, p0, Lcom/my/target/core/engines/j;->v:Lcom/my/target/fq;

    const-string v2, "midroll"

    invoke-virtual {v0, v2}, Lcom/my/target/fq;->a(Ljava/lang/String;)Lcom/my/target/al;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/core/engines/j;->y:Lcom/my/target/al;

    .line 148
    iget-object v0, p0, Lcom/my/target/core/engines/j;->y:Lcom/my/target/al;

    if-eqz v0, :cond_0

    .line 150
    iget-object v0, p0, Lcom/my/target/core/engines/j;->w:Lcom/my/target/core/controllers/d;

    iget-object v2, p0, Lcom/my/target/core/engines/j;->y:Lcom/my/target/al;

    invoke-virtual {v2}, Lcom/my/target/al;->P()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/my/target/core/controllers/d;->setConnectionTimeout(I)V

    .line 151
    iput-boolean v1, p0, Lcom/my/target/core/engines/j;->F:Z

    .line 152
    iget-object v0, p0, Lcom/my/target/core/engines/j;->y:Lcom/my/target/al;

    invoke-virtual {v0}, Lcom/my/target/al;->Q()I

    move-result v0

    iput v0, p0, Lcom/my/target/core/engines/j;->E:I

    .line 153
    const/4 v0, -0x1

    iput v0, p0, Lcom/my/target/core/engines/j;->D:I

    .line 154
    iput p1, p0, Lcom/my/target/core/engines/j;->C:F

    .line 155
    iget-object v0, p0, Lcom/my/target/core/engines/j;->y:Lcom/my/target/al;

    invoke-direct {p0, v0, p1}, Lcom/my/target/core/engines/j;->a(Lcom/my/target/al;F)V

    .line 162
    :cond_0
    :goto_2
    return-void

    .line 137
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 160
    :cond_2
    const-string v0, "attempt to start wrong midpoint, use one of InstreamAd.getMidPoints()"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto :goto_2

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public final stop()V
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/my/target/core/engines/j;->y:Lcom/my/target/al;

    if-eqz v0, :cond_0

    .line 184
    iget-object v0, p0, Lcom/my/target/core/engines/j;->w:Lcom/my/target/core/controllers/d;

    invoke-virtual {v0}, Lcom/my/target/core/controllers/d;->stop()V

    .line 185
    iget-object v0, p0, Lcom/my/target/core/engines/j;->y:Lcom/my/target/al;

    invoke-direct {p0, v0}, Lcom/my/target/core/engines/j;->a(Lcom/my/target/al;)V

    .line 187
    :cond_0
    return-void
.end method

.method public final swapPlayer(Lcom/my/target/instreamads/InstreamAdPlayer;)V
    .locals 1
    .param p1, "player"    # Lcom/my/target/instreamads/InstreamAdPlayer;

    .prologue
    .line 91
    iget-object v0, p0, Lcom/my/target/core/engines/j;->w:Lcom/my/target/core/controllers/d;

    invoke-virtual {v0, p1}, Lcom/my/target/core/controllers/d;->swapPlayer(Lcom/my/target/instreamads/InstreamAdPlayer;)V

    .line 92
    return-void
.end method
