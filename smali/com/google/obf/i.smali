.class final Lcom/google/obf/i;
.super Ljava/lang/Object;
.source "IMASDK"

# interfaces
.implements Lcom/google/obf/h;


# instance fields
.field private final a:Landroid/os/Handler;

.field private final b:Lcom/google/obf/j;

.field private final c:Ljava/util/concurrent/CopyOnWriteArraySet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArraySet",
            "<",
            "Lcom/google/obf/h$c;",
            ">;"
        }
    .end annotation
.end field

.field private final d:[[Lcom/google/obf/q;

.field private final e:[I

.field private f:Z

.field private g:I

.field private h:I


# direct methods
.method public constructor <init>(III)V
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "HandlerLeak"
        }
    .end annotation

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    const-string v0, "ExoPlayerImpl"

    const-string v1, "Init ExoPlayerLib/1.5.16"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/obf/i;->f:Z

    .line 4
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/obf/i;->g:I

    .line 5
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, Lcom/google/obf/i;->c:Ljava/util/concurrent/CopyOnWriteArraySet;

    .line 6
    new-array v0, p1, [[Lcom/google/obf/q;

    iput-object v0, p0, Lcom/google/obf/i;->d:[[Lcom/google/obf/q;

    .line 7
    new-array v0, p1, [I

    iput-object v0, p0, Lcom/google/obf/i;->e:[I

    .line 8
    new-instance v0, Lcom/google/obf/i$1;

    invoke-direct {v0, p0}, Lcom/google/obf/i$1;-><init>(Lcom/google/obf/i;)V

    iput-object v0, p0, Lcom/google/obf/i;->a:Landroid/os/Handler;

    .line 9
    new-instance v0, Lcom/google/obf/j;

    iget-object v1, p0, Lcom/google/obf/i;->a:Landroid/os/Handler;

    iget-boolean v2, p0, Lcom/google/obf/i;->f:Z

    iget-object v3, p0, Lcom/google/obf/i;->e:[I

    move v4, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/obf/j;-><init>(Landroid/os/Handler;Z[III)V

    iput-object v0, p0, Lcom/google/obf/i;->b:Lcom/google/obf/j;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 15
    iget v0, p0, Lcom/google/obf/i;->g:I

    return v0
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/obf/i;->b:Lcom/google/obf/j;

    invoke-virtual {v0, p1, p2}, Lcom/google/obf/j;->a(J)V

    .line 28
    return-void
.end method

.method a(Landroid/os/Message;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 40
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 62
    :cond_0
    return-void

    .line 41
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/obf/i;->d:[[Lcom/google/obf/q;

    iget-object v2, p0, Lcom/google/obf/i;->d:[[Lcom/google/obf/q;

    array-length v2, v2

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 42
    iget v0, p1, Landroid/os/Message;->arg1:I

    iput v0, p0, Lcom/google/obf/i;->g:I

    .line 43
    iget-object v0, p0, Lcom/google/obf/i;->c:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/h$c;

    .line 44
    iget-boolean v2, p0, Lcom/google/obf/i;->f:Z

    iget v3, p0, Lcom/google/obf/i;->g:I

    invoke-interface {v0, v2, v3}, Lcom/google/obf/h$c;->a(ZI)V

    goto :goto_0

    .line 47
    :pswitch_1
    iget v0, p1, Landroid/os/Message;->arg1:I

    iput v0, p0, Lcom/google/obf/i;->g:I

    .line 48
    iget-object v0, p0, Lcom/google/obf/i;->c:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/h$c;

    .line 49
    iget-boolean v2, p0, Lcom/google/obf/i;->f:Z

    iget v3, p0, Lcom/google/obf/i;->g:I

    invoke-interface {v0, v2, v3}, Lcom/google/obf/h$c;->a(ZI)V

    goto :goto_1

    .line 52
    :pswitch_2
    iget v0, p0, Lcom/google/obf/i;->h:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/obf/i;->h:I

    .line 53
    iget v0, p0, Lcom/google/obf/i;->h:I

    if-nez v0, :cond_0

    .line 54
    iget-object v0, p0, Lcom/google/obf/i;->c:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/h$c;

    .line 55
    invoke-interface {v0}, Lcom/google/obf/h$c;->a()V

    goto :goto_2

    .line 57
    :pswitch_3
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/obf/g;

    .line 58
    iget-object v1, p0, Lcom/google/obf/i;->c:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/obf/h$c;

    .line 59
    invoke-interface {v1, v0}, Lcom/google/obf/h$c;->a(Lcom/google/obf/g;)V

    goto :goto_3

    .line 40
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public a(Lcom/google/obf/h$a;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/obf/i;->b:Lcom/google/obf/j;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/obf/j;->a(Lcom/google/obf/h$a;ILjava/lang/Object;)V

    .line 35
    return-void
.end method

.method public a(Lcom/google/obf/h$c;)V
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lcom/google/obf/i;->c:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->add(Ljava/lang/Object;)Z

    .line 12
    return-void
.end method

.method public a(Z)V
    .locals 3

    .prologue
    .line 19
    iget-boolean v0, p0, Lcom/google/obf/i;->f:Z

    if-eq v0, p1, :cond_0

    .line 20
    iput-boolean p1, p0, Lcom/google/obf/i;->f:Z

    .line 21
    iget v0, p0, Lcom/google/obf/i;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/obf/i;->h:I

    .line 22
    iget-object v0, p0, Lcom/google/obf/i;->b:Lcom/google/obf/j;

    invoke-virtual {v0, p1}, Lcom/google/obf/j;->a(Z)V

    .line 23
    iget-object v0, p0, Lcom/google/obf/i;->c:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/h$c;

    .line 24
    iget v2, p0, Lcom/google/obf/i;->g:I

    invoke-interface {v0, p1, v2}, Lcom/google/obf/h$c;->a(ZI)V

    goto :goto_0

    .line 26
    :cond_0
    return-void
.end method

.method public varargs a([Lcom/google/obf/x;)V
    .locals 2

    .prologue
    .line 16
    iget-object v0, p0, Lcom/google/obf/i;->d:[[Lcom/google/obf/q;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 17
    iget-object v0, p0, Lcom/google/obf/i;->b:Lcom/google/obf/j;

    invoke-virtual {v0, p1}, Lcom/google/obf/j;->a([Lcom/google/obf/x;)V

    .line 18
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/obf/i;->b:Lcom/google/obf/j;

    invoke-virtual {v0}, Lcom/google/obf/j;->c()V

    .line 30
    return-void
.end method

.method public b(Lcom/google/obf/h$a;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/obf/i;->b:Lcom/google/obf/j;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/obf/j;->b(Lcom/google/obf/h$a;ILjava/lang/Object;)V

    .line 37
    return-void
.end method

.method public b(Lcom/google/obf/h$c;)V
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/google/obf/i;->c:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArraySet;->remove(Ljava/lang/Object;)Z

    .line 14
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/obf/i;->b:Lcom/google/obf/j;

    invoke-virtual {v0}, Lcom/google/obf/j;->d()V

    .line 32
    iget-object v0, p0, Lcom/google/obf/i;->a:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 33
    return-void
.end method

.method public d()J
    .locals 2

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/obf/i;->b:Lcom/google/obf/j;

    invoke-virtual {v0}, Lcom/google/obf/j;->b()J

    move-result-wide v0

    return-wide v0
.end method

.method public e()J
    .locals 2

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/obf/i;->b:Lcom/google/obf/j;

    invoke-virtual {v0}, Lcom/google/obf/j;->a()J

    move-result-wide v0

    return-wide v0
.end method
