.class Lcom/samsung/multiscreen/Player$4;
.super Ljava/lang/Object;
.source "Player.java"

# interfaces
.implements Lcom/samsung/multiscreen/Result;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/multiscreen/Player;->startPlay(Lorg/json/JSONObject;Lcom/samsung/multiscreen/Result;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/samsung/multiscreen/Result",
        "<",
        "Lcom/samsung/multiscreen/Player$DMPStatus;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/multiscreen/Player;

.field final synthetic val$contentUrl:Ljava/lang/String;

.field final synthetic val$data:Lorg/json/JSONObject;

.field final synthetic val$result:Lcom/samsung/multiscreen/Result;


# direct methods
.method constructor <init>(Lcom/samsung/multiscreen/Player;Lcom/samsung/multiscreen/Result;Lorg/json/JSONObject;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/samsung/multiscreen/Player;

    .prologue
    .line 476
    iput-object p1, p0, Lcom/samsung/multiscreen/Player$4;->this$0:Lcom/samsung/multiscreen/Player;

    iput-object p2, p0, Lcom/samsung/multiscreen/Player$4;->val$result:Lcom/samsung/multiscreen/Result;

    iput-object p3, p0, Lcom/samsung/multiscreen/Player$4;->val$data:Lorg/json/JSONObject;

    iput-object p4, p0, Lcom/samsung/multiscreen/Player$4;->val$contentUrl:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/samsung/multiscreen/Error;)V
    .locals 3
    .param p1, "error"    # Lcom/samsung/multiscreen/Error;

    .prologue
    .line 546
    iget-object v0, p0, Lcom/samsung/multiscreen/Player$4;->this$0:Lcom/samsung/multiscreen/Player;

    invoke-virtual {v0}, Lcom/samsung/multiscreen/Player;->isDebug()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Player"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "StartPlay() Error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/multiscreen/Error;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 548
    :cond_0
    iget-object v0, p0, Lcom/samsung/multiscreen/Player$4;->val$result:Lcom/samsung/multiscreen/Result;

    invoke-interface {v0, p1}, Lcom/samsung/multiscreen/Result;->onError(Lcom/samsung/multiscreen/Error;)V

    .line 549
    return-void
.end method

.method public onSuccess(Lcom/samsung/multiscreen/Player$DMPStatus;)V
    .locals 7
    .param p1, "status"    # Lcom/samsung/multiscreen/Player$DMPStatus;

    .prologue
    .line 479
    if-nez p1, :cond_2

    .line 480
    new-instance v1, Lcom/samsung/multiscreen/ErrorCode;

    const-string v2, "PLAYER_ERROR_INVALID_TV_RESPONSE"

    invoke-direct {v1, v2}, Lcom/samsung/multiscreen/ErrorCode;-><init>(Ljava/lang/String;)V

    .line 481
    .local v1, "error":Lcom/samsung/multiscreen/ErrorCode;
    iget-object v2, p0, Lcom/samsung/multiscreen/Player$4;->this$0:Lcom/samsung/multiscreen/Player;

    invoke-virtual {v2}, Lcom/samsung/multiscreen/Player;->isDebug()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "Player"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getDMPStatus() : Error: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Lcom/samsung/multiscreen/ErrorCode;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 482
    :cond_0
    iget-object v2, p0, Lcom/samsung/multiscreen/Player$4;->val$result:Lcom/samsung/multiscreen/Result;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/samsung/multiscreen/Player$4;->val$result:Lcom/samsung/multiscreen/Result;

    invoke-virtual {v1}, Lcom/samsung/multiscreen/ErrorCode;->value()I

    move-result v3

    int-to-long v4, v3

    invoke-virtual {v1}, Lcom/samsung/multiscreen/ErrorCode;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/samsung/multiscreen/ErrorCode;->name()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v3, v6}, Lcom/samsung/multiscreen/Error;->create(JLjava/lang/String;Ljava/lang/String;)Lcom/samsung/multiscreen/Error;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/samsung/multiscreen/Result;->onError(Lcom/samsung/multiscreen/Error;)V

    .line 542
    .end local v1    # "error":Lcom/samsung/multiscreen/ErrorCode;
    :cond_1
    :goto_0
    return-void

    .line 486
    :cond_2
    iget-object v2, p0, Lcom/samsung/multiscreen/Player$4;->this$0:Lcom/samsung/multiscreen/Player;

    invoke-virtual {v2}, Lcom/samsung/multiscreen/Player;->isDebug()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 487
    const-string v2, "Player"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DMP AppName : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p1, Lcom/samsung/multiscreen/Player$DMPStatus;->mAppName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 488
    const-string v2, "Player"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DMP Visible : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p1, Lcom/samsung/multiscreen/Player$DMPStatus;->mVisible:Ljava/lang/Boolean;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 489
    const-string v2, "Player"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DMP Running : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p1, Lcom/samsung/multiscreen/Player$DMPStatus;->mDMPRunning:Ljava/lang/Boolean;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 493
    :cond_3
    iget-object v2, p1, Lcom/samsung/multiscreen/Player$DMPStatus;->mDMPRunning:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_6

    iget-object v2, p1, Lcom/samsung/multiscreen/Player$DMPStatus;->mRunning:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 495
    iget-object v2, p1, Lcom/samsung/multiscreen/Player$DMPStatus;->mAppName:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p1, Lcom/samsung/multiscreen/Player$DMPStatus;->mAppName:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/multiscreen/Player$4;->this$0:Lcom/samsung/multiscreen/Player;

    invoke-static {v3}, Lcom/samsung/multiscreen/Player;->access$100(Lcom/samsung/multiscreen/Player;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 497
    iget-object v2, p1, Lcom/samsung/multiscreen/Player$DMPStatus;->mVisible:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 499
    :try_start_0
    iget-object v2, p0, Lcom/samsung/multiscreen/Player$4;->val$data:Lorg/json/JSONObject;

    const-string v3, "subEvent"

    sget-object v4, Lcom/samsung/multiscreen/Player$PlayerContentSubEvents;->CHANGEPLAYINGCONTENT:Lcom/samsung/multiscreen/Player$PlayerContentSubEvents;

    invoke-virtual {v4}, Lcom/samsung/multiscreen/Player$PlayerContentSubEvents;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 500
    iget-object v2, p0, Lcom/samsung/multiscreen/Player$4;->val$data:Lorg/json/JSONObject;

    const-string v3, "playerType"

    iget-object v4, p0, Lcom/samsung/multiscreen/Player$4;->this$0:Lcom/samsung/multiscreen/Player;

    invoke-static {v4}, Lcom/samsung/multiscreen/Player;->access$200(Lcom/samsung/multiscreen/Player;)Lcom/samsung/multiscreen/Player$ContentType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/multiscreen/Player$ContentType;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 505
    sget-object v2, Lcom/samsung/multiscreen/Player;->mApplication:Lcom/samsung/multiscreen/Application;

    const-string v3, "playerContentChange"

    iget-object v4, p0, Lcom/samsung/multiscreen/Player$4;->val$data:Lorg/json/JSONObject;

    invoke-virtual {v2, v3, v4}, Lcom/samsung/multiscreen/Application;->publish(Ljava/lang/String;Ljava/lang/Object;)V

    .line 507
    iget-object v2, p0, Lcom/samsung/multiscreen/Player$4;->val$result:Lcom/samsung/multiscreen/Result;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/samsung/multiscreen/Player$4;->val$result:Lcom/samsung/multiscreen/Result;

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/samsung/multiscreen/Result;->onSuccess(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 501
    :catch_0
    move-exception v0

    .line 502
    .local v0, "e":Ljava/lang/Exception;
    iget-object v2, p0, Lcom/samsung/multiscreen/Player$4;->this$0:Lcom/samsung/multiscreen/Player;

    invoke-virtual {v2}, Lcom/samsung/multiscreen/Player;->isDebug()Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "Player"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error while creating ChangePlayingContent Request : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 509
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_4
    iget-object v2, p0, Lcom/samsung/multiscreen/Player$4;->this$0:Lcom/samsung/multiscreen/Player;

    iget-object v3, p0, Lcom/samsung/multiscreen/Player$4;->val$contentUrl:Ljava/lang/String;

    new-instance v4, Lcom/samsung/multiscreen/Player$4$1;

    invoke-direct {v4, p0}, Lcom/samsung/multiscreen/Player$4$1;-><init>(Lcom/samsung/multiscreen/Player$4;)V

    invoke-static {v2, v3, v4}, Lcom/samsung/multiscreen/Player;->access$300(Lcom/samsung/multiscreen/Player;Ljava/lang/String;Lcom/samsung/multiscreen/Result;)V

    goto/16 :goto_0

    .line 537
    :cond_5
    iget-object v2, p0, Lcom/samsung/multiscreen/Player$4;->this$0:Lcom/samsung/multiscreen/Player;

    iget-object v3, p0, Lcom/samsung/multiscreen/Player$4;->val$contentUrl:Ljava/lang/String;

    iget-object v4, p0, Lcom/samsung/multiscreen/Player$4;->val$result:Lcom/samsung/multiscreen/Result;

    invoke-static {v2, v3, v4}, Lcom/samsung/multiscreen/Player;->access$300(Lcom/samsung/multiscreen/Player;Ljava/lang/String;Lcom/samsung/multiscreen/Result;)V

    goto/16 :goto_0

    .line 540
    :cond_6
    iget-object v2, p0, Lcom/samsung/multiscreen/Player$4;->this$0:Lcom/samsung/multiscreen/Player;

    iget-object v3, p0, Lcom/samsung/multiscreen/Player$4;->val$contentUrl:Ljava/lang/String;

    iget-object v4, p0, Lcom/samsung/multiscreen/Player$4;->val$result:Lcom/samsung/multiscreen/Result;

    invoke-static {v2, v3, v4}, Lcom/samsung/multiscreen/Player;->access$300(Lcom/samsung/multiscreen/Player;Ljava/lang/String;Lcom/samsung/multiscreen/Result;)V

    goto/16 :goto_0
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 476
    check-cast p1, Lcom/samsung/multiscreen/Player$DMPStatus;

    invoke-virtual {p0, p1}, Lcom/samsung/multiscreen/Player$4;->onSuccess(Lcom/samsung/multiscreen/Player$DMPStatus;)V

    return-void
.end method
