.class public Lru/cn/api/registry/replies/Service;
.super Ljava/lang/Object;
.source "Service.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/api/registry/replies/Service$Range;,
        Lru/cn/api/registry/replies/Service$Type;
    }
.end annotation


# static fields
.field private static final supportedApiVersions:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lru/cn/api/registry/replies/Service$Type;",
            "Lru/cn/api/registry/replies/Service$Range;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private contractor:Lru/cn/api/registry/replies/Contractor;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "contractor"
    .end annotation
.end field

.field public name:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "name"
    .end annotation
.end field

.field private selectedVersion:Lru/cn/api/registry/replies/APIVersion;

.field public serviceType:Lru/cn/api/registry/replies/Service$Type;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "type"
    .end annotation
.end field

.field public versions:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "apiVersions"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lru/cn/api/registry/replies/APIVersion;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 45
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lru/cn/api/registry/replies/Service;->supportedApiVersions:Ljava/util/Map;

    .line 46
    sget-object v0, Lru/cn/api/registry/replies/Service;->supportedApiVersions:Ljava/util/Map;

    sget-object v1, Lru/cn/api/registry/replies/Service$Type;->iptv:Lru/cn/api/registry/replies/Service$Type;

    new-instance v2, Lru/cn/api/registry/replies/Service$Range;

    invoke-direct {v2, v3, v4}, Lru/cn/api/registry/replies/Service$Range;-><init>(II)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    sget-object v0, Lru/cn/api/registry/replies/Service;->supportedApiVersions:Ljava/util/Map;

    sget-object v1, Lru/cn/api/registry/replies/Service$Type;->tv_guide:Lru/cn/api/registry/replies/Service$Type;

    new-instance v2, Lru/cn/api/registry/replies/Service$Range;

    invoke-direct {v2, v4}, Lru/cn/api/registry/replies/Service$Range;-><init>(I)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    sget-object v0, Lru/cn/api/registry/replies/Service;->supportedApiVersions:Ljava/util/Map;

    sget-object v1, Lru/cn/api/registry/replies/Service$Type;->auth:Lru/cn/api/registry/replies/Service$Type;

    new-instance v2, Lru/cn/api/registry/replies/Service$Range;

    invoke-direct {v2, v4}, Lru/cn/api/registry/replies/Service$Range;-><init>(I)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    sget-object v0, Lru/cn/api/registry/replies/Service;->supportedApiVersions:Ljava/util/Map;

    sget-object v1, Lru/cn/api/registry/replies/Service$Type;->user_data:Lru/cn/api/registry/replies/Service$Type;

    new-instance v2, Lru/cn/api/registry/replies/Service$Range;

    invoke-direct {v2, v3}, Lru/cn/api/registry/replies/Service$Range;-><init>(I)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    sget-object v0, Lru/cn/api/registry/replies/Service;->supportedApiVersions:Ljava/util/Map;

    sget-object v1, Lru/cn/api/registry/replies/Service$Type;->media_locator:Lru/cn/api/registry/replies/Service$Type;

    new-instance v2, Lru/cn/api/registry/replies/Service$Range;

    invoke-direct {v2, v3}, Lru/cn/api/registry/replies/Service$Range;-><init>(I)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    sget-object v0, Lru/cn/api/registry/replies/Service;->supportedApiVersions:Ljava/util/Map;

    sget-object v1, Lru/cn/api/registry/replies/Service$Type;->catalogue:Lru/cn/api/registry/replies/Service$Type;

    new-instance v2, Lru/cn/api/registry/replies/Service$Range;

    invoke-direct {v2, v3}, Lru/cn/api/registry/replies/Service$Range;-><init>(I)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    sget-object v0, Lru/cn/api/registry/replies/Service;->supportedApiVersions:Ljava/util/Map;

    sget-object v1, Lru/cn/api/registry/replies/Service$Type;->tracking:Lru/cn/api/registry/replies/Service$Type;

    new-instance v2, Lru/cn/api/registry/replies/Service$Range;

    invoke-direct {v2, v4}, Lru/cn/api/registry/replies/Service$Range;-><init>(I)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    sget-object v0, Lru/cn/api/registry/replies/Service;->supportedApiVersions:Ljava/util/Map;

    sget-object v1, Lru/cn/api/registry/replies/Service$Type;->money_miner:Lru/cn/api/registry/replies/Service$Type;

    new-instance v2, Lru/cn/api/registry/replies/Service$Range;

    invoke-direct {v2, v3}, Lru/cn/api/registry/replies/Service$Range;-><init>(I)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 11
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 107
    if-nez p1, :cond_1

    .line 126
    :cond_0
    :goto_0
    return v8

    .line 110
    :cond_1
    if-ne p0, p1, :cond_2

    move v8, v7

    .line 111
    goto :goto_0

    .line 113
    :cond_2
    instance-of v9, p1, Lru/cn/api/registry/replies/Service;

    if-eqz v9, :cond_0

    move-object v3, p1

    .line 114
    check-cast v3, Lru/cn/api/registry/replies/Service;

    .line 116
    .local v3, "service":Lru/cn/api/registry/replies/Service;
    invoke-virtual {v3}, Lru/cn/api/registry/replies/Service;->getLocation()Ljava/lang/String;

    move-result-object v6

    .line 117
    .local v6, "serviceLocation":Ljava/lang/String;
    invoke-virtual {p0}, Lru/cn/api/registry/replies/Service;->getLocation()Ljava/lang/String;

    move-result-object v2

    .line 118
    .local v2, "location":Ljava/lang/String;
    invoke-virtual {p0}, Lru/cn/api/registry/replies/Service;->getContractorId()J

    move-result-wide v0

    .line 119
    .local v0, "contractorId":J
    invoke-virtual {v3}, Lru/cn/api/registry/replies/Service;->getContractorId()J

    move-result-wide v4

    .line 121
    .local v4, "serviceContractorId":J
    iget-object v9, v3, Lru/cn/api/registry/replies/Service;->serviceType:Lru/cn/api/registry/replies/Service$Type;

    iget-object v10, p0, Lru/cn/api/registry/replies/Service;->serviceType:Lru/cn/api/registry/replies/Service$Type;

    invoke-virtual {v9, v10}, Lru/cn/api/registry/replies/Service$Type;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    if-eqz v2, :cond_3

    .line 122
    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    cmp-long v9, v0, v4

    if-nez v9, :cond_3

    :goto_1
    move v8, v7

    .line 121
    goto :goto_0

    :cond_3
    move v7, v8

    .line 122
    goto :goto_1
.end method

.method public getContractorId()J
    .locals 2

    .prologue
    .line 71
    iget-object v0, p0, Lru/cn/api/registry/replies/Service;->contractor:Lru/cn/api/registry/replies/Contractor;

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lru/cn/api/registry/replies/Service;->contractor:Lru/cn/api/registry/replies/Contractor;

    iget-wide v0, v0, Lru/cn/api/registry/replies/Contractor;->contractorId:J

    .line 74
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public getLocation()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 79
    iget-object v3, p0, Lru/cn/api/registry/replies/Service;->selectedVersion:Lru/cn/api/registry/replies/APIVersion;

    if-eqz v3, :cond_1

    .line 80
    iget-object v2, p0, Lru/cn/api/registry/replies/Service;->selectedVersion:Lru/cn/api/registry/replies/APIVersion;

    iget-object v2, v2, Lru/cn/api/registry/replies/APIVersion;->location:Ljava/lang/String;

    .line 102
    :cond_0
    :goto_0
    return-object v2

    .line 83
    :cond_1
    sget-object v3, Lru/cn/api/registry/replies/Service;->supportedApiVersions:Ljava/util/Map;

    iget-object v4, p0, Lru/cn/api/registry/replies/Service;->serviceType:Lru/cn/api/registry/replies/Service$Type;

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/cn/api/registry/replies/Service$Range;

    .line 86
    .local v1, "versionsRange":Lru/cn/api/registry/replies/Service$Range;
    if-eqz v1, :cond_0

    .line 89
    iget-object v3, p0, Lru/cn/api/registry/replies/Service;->versions:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/api/registry/replies/APIVersion;

    .line 90
    .local v0, "apiVersion":Lru/cn/api/registry/replies/APIVersion;
    iget v4, v0, Lru/cn/api/registry/replies/APIVersion;->majorVersion:I

    invoke-virtual {v1, v4}, Lru/cn/api/registry/replies/Service$Range;->isInRange(I)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 91
    iget-object v4, p0, Lru/cn/api/registry/replies/Service;->selectedVersion:Lru/cn/api/registry/replies/APIVersion;

    if-nez v4, :cond_3

    .line 92
    iput-object v0, p0, Lru/cn/api/registry/replies/Service;->selectedVersion:Lru/cn/api/registry/replies/APIVersion;

    goto :goto_1

    .line 93
    :cond_3
    iget v4, v0, Lru/cn/api/registry/replies/APIVersion;->majorVersion:I

    iget-object v5, p0, Lru/cn/api/registry/replies/Service;->selectedVersion:Lru/cn/api/registry/replies/APIVersion;

    iget v5, v5, Lru/cn/api/registry/replies/APIVersion;->majorVersion:I

    if-le v4, v5, :cond_2

    .line 94
    iput-object v0, p0, Lru/cn/api/registry/replies/Service;->selectedVersion:Lru/cn/api/registry/replies/APIVersion;

    goto :goto_1

    .line 99
    .end local v0    # "apiVersion":Lru/cn/api/registry/replies/APIVersion;
    :cond_4
    iget-object v3, p0, Lru/cn/api/registry/replies/Service;->selectedVersion:Lru/cn/api/registry/replies/APIVersion;

    if-eqz v3, :cond_0

    .line 102
    iget-object v2, p0, Lru/cn/api/registry/replies/Service;->selectedVersion:Lru/cn/api/registry/replies/APIVersion;

    iget-object v2, v2, Lru/cn/api/registry/replies/APIVersion;->location:Ljava/lang/String;

    goto :goto_0
.end method

.method public hashCode()I
    .locals 6

    .prologue
    .line 131
    iget-object v2, p0, Lru/cn/api/registry/replies/Service;->serviceType:Lru/cn/api/registry/replies/Service$Type;

    invoke-virtual {v2}, Lru/cn/api/registry/replies/Service$Type;->hashCode()I

    move-result v2

    mul-int/lit8 v0, v2, 0x1f

    .line 133
    .local v0, "hashCode":I
    invoke-virtual {p0}, Lru/cn/api/registry/replies/Service;->getLocation()Ljava/lang/String;

    move-result-object v1

    .line 134
    .local v1, "location":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 135
    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int/2addr v0, v2

    .line 136
    mul-int/lit8 v0, v0, 0x1f

    .line 139
    :cond_0
    iget-object v2, p0, Lru/cn/api/registry/replies/Service;->contractor:Lru/cn/api/registry/replies/Contractor;

    if-eqz v2, :cond_1

    .line 140
    int-to-long v2, v0

    iget-object v4, p0, Lru/cn/api/registry/replies/Service;->contractor:Lru/cn/api/registry/replies/Contractor;

    iget-wide v4, v4, Lru/cn/api/registry/replies/Contractor;->contractorId:J

    add-long/2addr v2, v4

    long-to-int v0, v2

    .line 143
    :cond_1
    return v0
.end method
