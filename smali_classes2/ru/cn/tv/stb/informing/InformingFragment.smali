.class public Lru/cn/tv/stb/informing/InformingFragment;
.super Landroid/support/v4/app/Fragment;
.source "InformingFragment.java"


# instance fields
.field private descriptionView:Landroid/widget/TextView;

.field private imageView:Landroid/widget/ImageView;

.field private titleView:Landroid/widget/TextView;

.field private viewModel:Lru/cn/tv/stb/informing/InformingViewModel;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method private setInforming(Lru/cn/tv/stb/informing/Informing;)V
    .locals 2
    .param p1, "informing"    # Lru/cn/tv/stb/informing/Informing;

    .prologue
    .line 53
    iget-object v0, p0, Lru/cn/tv/stb/informing/InformingFragment;->titleView:Landroid/widget/TextView;

    iget-object v1, p1, Lru/cn/tv/stb/informing/Informing;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 54
    iget-object v0, p0, Lru/cn/tv/stb/informing/InformingFragment;->descriptionView:Landroid/widget/TextView;

    iget-object v1, p1, Lru/cn/tv/stb/informing/Informing;->description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 55
    iget-object v0, p0, Lru/cn/tv/stb/informing/InformingFragment;->imageView:Landroid/widget/ImageView;

    iget v1, p1, Lru/cn/tv/stb/informing/Informing;->imageId:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 56
    return-void
.end method


# virtual methods
.method final bridge synthetic bridge$lambda$0$InformingFragment(Lru/cn/tv/stb/informing/Informing;)V
    .locals 0

    invoke-direct {p0, p1}, Lru/cn/tv/stb/informing/InformingFragment;->setInforming(Lru/cn/tv/stb/informing/Informing;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 28
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 29
    invoke-static {}, Lru/cn/tv/App;->scope()Ltoothpick/Scope;

    move-result-object v0

    const-class v1, Lru/cn/tv/stb/informing/InformingViewModel;

    invoke-static {p0, v0, v1}, Lru/cn/mvvm/ViewModels;->get(Landroid/support/v4/app/Fragment;Ltoothpick/Scope;Ljava/lang/Class;)Landroid/arch/lifecycle/ViewModel;

    move-result-object v0

    check-cast v0, Lru/cn/tv/stb/informing/InformingViewModel;

    iput-object v0, p0, Lru/cn/tv/stb/informing/InformingFragment;->viewModel:Lru/cn/tv/stb/informing/InformingViewModel;

    .line 30
    iget-object v0, p0, Lru/cn/tv/stb/informing/InformingFragment;->viewModel:Lru/cn/tv/stb/informing/InformingViewModel;

    invoke-virtual {v0}, Lru/cn/tv/stb/informing/InformingViewModel;->informings()Landroid/arch/lifecycle/LiveData;

    move-result-object v0

    new-instance v1, Lru/cn/tv/stb/informing/InformingFragment$$Lambda$0;

    invoke-direct {v1, p0}, Lru/cn/tv/stb/informing/InformingFragment$$Lambda$0;-><init>(Lru/cn/tv/stb/informing/InformingFragment;)V

    invoke-virtual {v0, p0, v1}, Landroid/arch/lifecycle/LiveData;->observe(Landroid/arch/lifecycle/LifecycleOwner;Landroid/arch/lifecycle/Observer;)V

    .line 31
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 36
    const v0, 0x7f0c0090

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 41
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 43
    const v0, 0x7f0900fb

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lru/cn/tv/stb/informing/InformingFragment;->titleView:Landroid/widget/TextView;

    .line 44
    const v0, 0x7f0900f9

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lru/cn/tv/stb/informing/InformingFragment;->descriptionView:Landroid/widget/TextView;

    .line 45
    const v0, 0x7f0900fa

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lru/cn/tv/stb/informing/InformingFragment;->imageView:Landroid/widget/ImageView;

    .line 46
    return-void
.end method

.method public setContractor(J)V
    .locals 1
    .param p1, "contractor"    # J

    .prologue
    .line 49
    iget-object v0, p0, Lru/cn/tv/stb/informing/InformingFragment;->viewModel:Lru/cn/tv/stb/informing/InformingViewModel;

    invoke-virtual {v0, p1, p2}, Lru/cn/tv/stb/informing/InformingViewModel;->setContractor(J)V

    .line 50
    return-void
.end method
