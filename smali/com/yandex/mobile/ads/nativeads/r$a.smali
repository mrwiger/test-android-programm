.class final Lcom/yandex/mobile/ads/nativeads/r$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/yandex/mobile/ads/nativeads/r;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation


# instance fields
.field private final a:Lcom/yandex/mobile/ads/nativeads/b;

.field private final b:Lcom/yandex/mobile/ads/nativeads/g;

.field private final c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/yandex/mobile/ads/nativeads/r;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/yandex/mobile/ads/nativeads/r;Lcom/yandex/mobile/ads/nativeads/b;Lcom/yandex/mobile/ads/nativeads/g;)V
    .locals 1

    .prologue
    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/yandex/mobile/ads/nativeads/r$a;->c:Ljava/lang/ref/WeakReference;

    .line 92
    iput-object p2, p0, Lcom/yandex/mobile/ads/nativeads/r$a;->a:Lcom/yandex/mobile/ads/nativeads/b;

    .line 93
    iput-object p3, p0, Lcom/yandex/mobile/ads/nativeads/r$a;->b:Lcom/yandex/mobile/ads/nativeads/g;

    .line 94
    return-void
.end method

.method synthetic constructor <init>(Lcom/yandex/mobile/ads/nativeads/r;Lcom/yandex/mobile/ads/nativeads/b;Lcom/yandex/mobile/ads/nativeads/g;B)V
    .locals 0

    .prologue
    .line 83
    invoke-direct {p0, p1, p2, p3}, Lcom/yandex/mobile/ads/nativeads/r$a;-><init>(Lcom/yandex/mobile/ads/nativeads/r;Lcom/yandex/mobile/ads/nativeads/b;Lcom/yandex/mobile/ads/nativeads/g;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/r$a;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/yandex/mobile/ads/nativeads/r;

    .line 99
    if-eqz v0, :cond_0

    .line 100
    iget-object v1, p0, Lcom/yandex/mobile/ads/nativeads/r$a;->a:Lcom/yandex/mobile/ads/nativeads/b;

    invoke-virtual {v1}, Lcom/yandex/mobile/ads/nativeads/b;->f()Lcom/yandex/mobile/ads/nativeads/g;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/yandex/mobile/ads/nativeads/r$a;->a:Lcom/yandex/mobile/ads/nativeads/b;

    invoke-virtual {v1}, Lcom/yandex/mobile/ads/nativeads/b;->f()Lcom/yandex/mobile/ads/nativeads/g;

    move-result-object v1

    .line 101
    :goto_0
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/yandex/mobile/ads/nativeads/r$a;->a:Lcom/yandex/mobile/ads/nativeads/b;

    invoke-virtual {v2}, Lcom/yandex/mobile/ads/nativeads/b;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 102
    invoke-static {v0}, Lcom/yandex/mobile/ads/nativeads/r;->a(Lcom/yandex/mobile/ads/nativeads/r;)Lcom/yandex/mobile/ads/as;

    move-result-object v2

    invoke-virtual {v2}, Lcom/yandex/mobile/ads/as;->c()V

    .line 104
    invoke-virtual {v1}, Lcom/yandex/mobile/ads/nativeads/g;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/yandex/mobile/ads/utils/j;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 105
    invoke-static {v0}, Lcom/yandex/mobile/ads/nativeads/r;->b(Lcom/yandex/mobile/ads/nativeads/r;)Lcom/yandex/mobile/ads/nativeads/l;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/yandex/mobile/ads/nativeads/l;->a(Ljava/lang/String;)V

    .line 108
    :cond_0
    return-void

    .line 100
    :cond_1
    iget-object v1, p0, Lcom/yandex/mobile/ads/nativeads/r$a;->b:Lcom/yandex/mobile/ads/nativeads/g;

    goto :goto_0
.end method
