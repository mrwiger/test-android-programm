.class Lcom/yandex/metrica/impl/ob/co$a;
.super Ljava/lang/Thread;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/yandex/metrica/impl/ob/co;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "a"
.end annotation


# instance fields
.field final synthetic a:Lcom/yandex/metrica/impl/ob/co;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/content/ContentValues;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/yandex/metrica/impl/ob/w;


# direct methods
.method public constructor <init>(Lcom/yandex/metrica/impl/ob/co;)V
    .locals 1

    .prologue
    .line 590
    iput-object p1, p0, Lcom/yandex/metrica/impl/ob/co$a;->a:Lcom/yandex/metrica/impl/ob/co;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 591
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/co$a;->b:Ljava/util/List;

    .line 592
    return-void
.end method


# virtual methods
.method declared-synchronized a()V
    .locals 1

    .prologue
    .line 624
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/co$a;->interrupt()V

    .line 625
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/co$a;->c:Lcom/yandex/metrica/impl/ob/w;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 626
    monitor-exit p0

    return-void

    .line 624
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized a(Lcom/yandex/metrica/impl/ob/w;)V
    .locals 1

    .prologue
    .line 629
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/yandex/metrica/impl/ob/co$a;->c:Lcom/yandex/metrica/impl/ob/w;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 630
    monitor-exit p0

    return-void

    .line 629
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized b()V
    .locals 1

    .prologue
    .line 633
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/co$a;->c:Lcom/yandex/metrica/impl/ob/w;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/co$a;->c:Lcom/yandex/metrica/impl/ob/w;

    invoke-interface {v0}, Lcom/yandex/metrica/impl/ob/w;->p()Z

    move-result v0

    if-nez v0, :cond_0

    .line 634
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/co$a;->c:Lcom/yandex/metrica/impl/ob/w;

    invoke-interface {v0}, Lcom/yandex/metrica/impl/ob/w;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 636
    :cond_0
    monitor-exit p0

    return-void

    .line 633
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public run()V
    .locals 3

    .prologue
    .line 596
    :goto_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v0

    if-nez v0, :cond_1

    .line 598
    :try_start_0
    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 599
    :try_start_1
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/co$a;->a:Lcom/yandex/metrica/impl/ob/co;

    invoke-static {v0}, Lcom/yandex/metrica/impl/ob/co;->a(Lcom/yandex/metrica/impl/ob/co;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 600
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V

    .line 602
    :cond_0
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 607
    :goto_1
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/co$a;->a:Lcom/yandex/metrica/impl/ob/co;

    invoke-static {v0}, Lcom/yandex/metrica/impl/ob/co;->b(Lcom/yandex/metrica/impl/ob/co;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 608
    :try_start_2
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/co$a;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 609
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/co$a;->b:Ljava/util/List;

    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/co$a;->a:Lcom/yandex/metrica/impl/ob/co;

    invoke-static {v2}, Lcom/yandex/metrica/impl/ob/co;->c(Lcom/yandex/metrica/impl/ob/co;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 610
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/co$a;->a:Lcom/yandex/metrica/impl/ob/co;

    invoke-static {v0}, Lcom/yandex/metrica/impl/ob/co;->c(Lcom/yandex/metrica/impl/ob/co;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 612
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/co$a;->a:Lcom/yandex/metrica/impl/ob/co;

    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/co$a;->a:Lcom/yandex/metrica/impl/ob/co;

    invoke-static {v2}, Lcom/yandex/metrica/impl/ob/co;->d(Lcom/yandex/metrica/impl/ob/co;)Landroid/content/ContentValues;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/yandex/metrica/impl/ob/co;->a(Lcom/yandex/metrica/impl/ob/co;Landroid/content/ContentValues;)V

    .line 613
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/co$a;->a:Lcom/yandex/metrica/impl/ob/co;

    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/co$a;->b:Ljava/util/List;

    invoke-static {v0, v2}, Lcom/yandex/metrica/impl/ob/co;->a(Lcom/yandex/metrica/impl/ob/co;Ljava/util/List;)V

    .line 615
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/co$a;->a:Lcom/yandex/metrica/impl/ob/co;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/yandex/metrica/impl/ob/co;->b(Lcom/yandex/metrica/impl/ob/co;Landroid/content/ContentValues;)Landroid/content/ContentValues;

    .line 616
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 619
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/co$a;->b()V

    goto :goto_0

    .line 602
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    .line 604
    :catch_0
    move-exception v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_1

    .line 616
    :catchall_1
    move-exception v0

    :try_start_5
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v0

    .line 621
    :cond_1
    return-void
.end method
