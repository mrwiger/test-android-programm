.class public Lru/cn/api/allowed/AllowedChannels;
.super Ljava/lang/Object;
.source "AllowedChannels.java"


# instance fields
.field private final ALLOWED_CONTENT_IDS:Ljava/lang/String;

.field private allowedContentIds:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final gson:Lcom/google/gson/Gson;

.field private final settings:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    const-string v1, "ALLOWED_CHANNEL_IDS"

    iput-object v1, p0, Lru/cn/api/allowed/AllowedChannels;->ALLOWED_CONTENT_IDS:Ljava/lang/String;

    .line 16
    new-instance v1, Lcom/google/gson/Gson;

    invoke-direct {v1}, Lcom/google/gson/Gson;-><init>()V

    iput-object v1, p0, Lru/cn/api/allowed/AllowedChannels;->gson:Lcom/google/gson/Gson;

    .line 17
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    invoke-static {v1}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lru/cn/api/allowed/AllowedChannels;->allowedContentIds:Ljava/util/List;

    .line 21
    const-string v1, "ALLOWED_CHANNEL"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lru/cn/api/allowed/AllowedChannels;->settings:Landroid/content/SharedPreferences;

    .line 22
    iget-object v1, p0, Lru/cn/api/allowed/AllowedChannels;->settings:Landroid/content/SharedPreferences;

    const-string v2, "ALLOWED_CHANNEL_IDS"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 23
    .local v0, "data":Ljava/lang/String;
    iget-object v2, p0, Lru/cn/api/allowed/AllowedChannels;->allowedContentIds:Ljava/util/List;

    if-nez v0, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    :goto_0
    invoke-interface {v2, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 24
    return-void

    .line 23
    :cond_0
    iget-object v1, p0, Lru/cn/api/allowed/AllowedChannels;->gson:Lcom/google/gson/Gson;

    const-class v3, [Ljava/lang/Long;

    invoke-virtual {v1, v0, v3}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    goto :goto_0
.end method

.method private save(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 56
    .local p1, "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    iget-object v0, p0, Lru/cn/api/allowed/AllowedChannels;->settings:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "ALLOWED_CHANNEL_IDS"

    iget-object v2, p0, Lru/cn/api/allowed/AllowedChannels;->gson:Lcom/google/gson/Gson;

    .line 57
    invoke-virtual {v2, p1}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 58
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 59
    return-void
.end method


# virtual methods
.method public contains(J)Z
    .locals 3
    .param p1, "id"    # J

    .prologue
    .line 27
    iget-object v0, p0, Lru/cn/api/allowed/AllowedChannels;->allowedContentIds:Ljava/util/List;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public ids()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 52
    iget-object v0, p0, Lru/cn/api/allowed/AllowedChannels;->allowedContentIds:Ljava/util/List;

    return-object v0
.end method

.method public insert(J)Z
    .locals 3
    .param p1, "id"    # J

    .prologue
    .line 31
    iget-object v0, p0, Lru/cn/api/allowed/AllowedChannels;->allowedContentIds:Ljava/util/List;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 32
    iget-object v0, p0, Lru/cn/api/allowed/AllowedChannels;->allowedContentIds:Ljava/util/List;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 33
    iget-object v0, p0, Lru/cn/api/allowed/AllowedChannels;->allowedContentIds:Ljava/util/List;

    invoke-direct {p0, v0}, Lru/cn/api/allowed/AllowedChannels;->save(Ljava/util/List;)V

    .line 34
    const/4 v0, 0x1

    .line 36
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public removeAll(Ljava/util/List;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 62
    .local p1, "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    const/4 v0, 0x0

    .line 67
    :goto_0
    return v0

    .line 65
    :cond_0
    iget-object v0, p0, Lru/cn/api/allowed/AllowedChannels;->allowedContentIds:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 66
    iget-object v0, p0, Lru/cn/api/allowed/AllowedChannels;->allowedContentIds:Ljava/util/List;

    invoke-direct {p0, v0}, Lru/cn/api/allowed/AllowedChannels;->save(Ljava/util/List;)V

    .line 67
    const/4 v0, 0x1

    goto :goto_0
.end method
