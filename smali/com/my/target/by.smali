.class public Lcom/my/target/by;
.super Landroid/view/View;
.source "IconButton.java"


# static fields
.field private static final iK:I = 0xa


# instance fields
.field private final density:F

.field private final iL:Landroid/graphics/ColorFilter;

.field private iM:Landroid/graphics/Bitmap;

.field private iN:I

.field private iO:I

.field private final in:Landroid/graphics/Paint;

.field private final padding:I

.field private final rect:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 35
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 36
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/my/target/by;->in:Landroid/graphics/Paint;

    .line 37
    iget-object v0, p0, Lcom/my/target/by;->in:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 38
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 39
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lcom/my/target/by;->density:F

    .line 40
    const/16 v0, 0xa

    invoke-static {v0, p1}, Lcom/my/target/cm;->a(ILandroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/my/target/by;->padding:I

    .line 41
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/my/target/by;->rect:Landroid/graphics/Rect;

    .line 42
    new-instance v0, Landroid/graphics/LightingColorFilter;

    const v1, -0x333334

    invoke-direct {v0, v1, v2}, Landroid/graphics/LightingColorFilter;-><init>(II)V

    iput-object v0, p0, Lcom/my/target/by;->iL:Landroid/graphics/ColorFilter;

    .line 43
    return-void
.end method


# virtual methods
.method public b(Landroid/graphics/Bitmap;Z)V
    .locals 3

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    .line 52
    iput-object p1, p0, Lcom/my/target/by;->iM:Landroid/graphics/Bitmap;

    .line 53
    iget-object v1, p0, Lcom/my/target/by;->iM:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_2

    .line 55
    if-eqz p2, :cond_1

    .line 57
    iget v1, p0, Lcom/my/target/by;->density:F

    cmpl-float v1, v1, v0

    if-lez v1, :cond_0

    const/high16 v0, 0x40000000    # 2.0f

    .line 58
    :cond_0
    iget-object v1, p0, Lcom/my/target/by;->iM:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v0

    iget v2, p0, Lcom/my/target/by;->density:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, Lcom/my/target/by;->iO:I

    .line 59
    iget-object v1, p0, Lcom/my/target/by;->iM:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float v0, v1, v0

    iget v1, p0, Lcom/my/target/by;->density:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/my/target/by;->iN:I

    .line 71
    :goto_0
    iget v0, p0, Lcom/my/target/by;->iN:I

    iget v1, p0, Lcom/my/target/by;->padding:I

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    iget v1, p0, Lcom/my/target/by;->iO:I

    iget v2, p0, Lcom/my/target/by;->padding:I

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/my/target/by;->setMeasuredDimension(II)V

    .line 72
    invoke-virtual {p0}, Lcom/my/target/by;->requestLayout()V

    .line 73
    return-void

    .line 63
    :cond_1
    iget-object v0, p0, Lcom/my/target/by;->iM:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/my/target/by;->iN:I

    .line 64
    iget-object v0, p0, Lcom/my/target/by;->iM:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/my/target/by;->iO:I

    goto :goto_0

    .line 69
    :cond_2
    const/4 v0, 0x0

    iput v0, p0, Lcom/my/target/by;->iO:I

    iput v0, p0, Lcom/my/target/by;->iN:I

    goto :goto_0
.end method

.method public getPadding()I
    .locals 1

    .prologue
    .line 47
    iget v0, p0, Lcom/my/target/by;->padding:I

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 109
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 110
    iget-object v0, p0, Lcom/my/target/by;->iM:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 112
    iget-object v0, p0, Lcom/my/target/by;->rect:Landroid/graphics/Rect;

    iget v1, p0, Lcom/my/target/by;->padding:I

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 113
    iget-object v0, p0, Lcom/my/target/by;->rect:Landroid/graphics/Rect;

    iget v1, p0, Lcom/my/target/by;->padding:I

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 114
    iget-object v0, p0, Lcom/my/target/by;->rect:Landroid/graphics/Rect;

    iget v1, p0, Lcom/my/target/by;->iN:I

    iget v2, p0, Lcom/my/target/by;->padding:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 115
    iget-object v0, p0, Lcom/my/target/by;->rect:Landroid/graphics/Rect;

    iget v1, p0, Lcom/my/target/by;->iO:I

    iget v2, p0, Lcom/my/target/by;->padding:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 117
    iget-object v0, p0, Lcom/my/target/by;->iM:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/my/target/by;->rect:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/my/target/by;->in:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 119
    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 2
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 103
    invoke-virtual {p0}, Lcom/my/target/by;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/my/target/by;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/my/target/by;->setMeasuredDimension(II)V

    .line 104
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/MotionEvent;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ClickableViewAccessibility"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v3, 0x0

    .line 79
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 96
    :pswitch_0
    invoke-super {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :goto_0
    return v0

    .line 82
    :pswitch_1
    iget-object v1, p0, Lcom/my/target/by;->in:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/my/target/by;->iL:Landroid/graphics/ColorFilter;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 83
    invoke-virtual {p0}, Lcom/my/target/by;->invalidate()V

    goto :goto_0

    .line 87
    :pswitch_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    cmpl-float v1, v1, v3

    if-ltz v1, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p0}, Lcom/my/target/by;->getMeasuredWidth()I

    move-result v2

    int-to-float v2, v2

    cmpg-float v1, v1, v2

    if-gtz v1, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    cmpl-float v1, v1, v3

    if-ltz v1, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-virtual {p0}, Lcom/my/target/by;->getMeasuredHeight()I

    move-result v2

    int-to-float v2, v2

    cmpg-float v1, v1, v2

    if-gtz v1, :cond_0

    .line 89
    invoke-virtual {p0}, Lcom/my/target/by;->performClick()Z

    .line 92
    :cond_0
    :pswitch_3
    iget-object v1, p0, Lcom/my/target/by;->in:Landroid/graphics/Paint;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 93
    invoke-virtual {p0}, Lcom/my/target/by;->invalidate()V

    goto :goto_0

    .line 79
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
