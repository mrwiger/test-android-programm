.class public Lru/cn/api/money_miner/replies/AdSystemParam;
.super Ljava/lang/Object;
.source "AdSystemParam.java"


# instance fields
.field public key:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "key"
    .end annotation
.end field

.field public value:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "value"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 16
    if-ne p1, p0, :cond_1

    .line 31
    :cond_0
    :goto_0
    return v1

    .line 19
    :cond_1
    instance-of v3, p1, Lru/cn/api/money_miner/replies/AdSystemParam;

    if-nez v3, :cond_2

    move v1, v2

    .line 20
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 22
    check-cast v0, Lru/cn/api/money_miner/replies/AdSystemParam;

    .line 24
    .local v0, "param":Lru/cn/api/money_miner/replies/AdSystemParam;
    iget-object v3, p0, Lru/cn/api/money_miner/replies/AdSystemParam;->key:Ljava/lang/String;

    iget-object v4, v0, Lru/cn/api/money_miner/replies/AdSystemParam;->key:Ljava/lang/String;

    if-eq v3, v4, :cond_3

    iget-object v3, p0, Lru/cn/api/money_miner/replies/AdSystemParam;->key:Ljava/lang/String;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lru/cn/api/money_miner/replies/AdSystemParam;->key:Ljava/lang/String;

    iget-object v4, v0, Lru/cn/api/money_miner/replies/AdSystemParam;->key:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 26
    :cond_3
    iget-object v3, p0, Lru/cn/api/money_miner/replies/AdSystemParam;->value:Ljava/lang/String;

    iget-object v4, v0, Lru/cn/api/money_miner/replies/AdSystemParam;->value:Ljava/lang/String;

    if-eq v3, v4, :cond_0

    iget-object v3, p0, Lru/cn/api/money_miner/replies/AdSystemParam;->value:Ljava/lang/String;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lru/cn/api/money_miner/replies/AdSystemParam;->value:Ljava/lang/String;

    iget-object v4, v0, Lru/cn/api/money_miner/replies/AdSystemParam;->value:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_4
    move v1, v2

    .line 31
    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 36
    iget-object v0, p0, Lru/cn/api/money_miner/replies/AdSystemParam;->key:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    iget-object v1, p0, Lru/cn/api/money_miner/replies/AdSystemParam;->value:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method
