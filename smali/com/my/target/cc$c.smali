.class Lcom/my/target/cc$c;
.super Ljava/lang/Object;
.source "VideoTextureView.java"

# interfaces
.implements Landroid/view/TextureView$SurfaceTextureListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/my/target/cc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "c"
.end annotation


# instance fields
.field final synthetic jF:Lcom/my/target/cc;

.field private final uri:Landroid/net/Uri;


# direct methods
.method constructor <init>(Lcom/my/target/cc;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 670
    iput-object p1, p0, Lcom/my/target/cc$c;->jF:Lcom/my/target/cc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/my/target/cc$c;->uri:Landroid/net/Uri;

    return-void
.end method


# virtual methods
.method public onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .locals 3
    .param p1, "surface"    # Landroid/graphics/SurfaceTexture;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 675
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "VideoTextureView: Surface available from callback, playing  force state "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/cc$c;->jF:Lcom/my/target/cc;

    .line 676
    invoke-static {v1}, Lcom/my/target/cc;->a(Lcom/my/target/cc;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " uri "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/cc$c;->uri:Landroid/net/Uri;

    .line 678
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " w= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " h = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 675
    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 680
    iget-object v0, p0, Lcom/my/target/cc$c;->jF:Lcom/my/target/cc;

    invoke-static {v0}, Lcom/my/target/cc;->a(Lcom/my/target/cc;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 709
    :pswitch_0
    iget-object v0, p0, Lcom/my/target/cc$c;->jF:Lcom/my/target/cc;

    invoke-static {v0}, Lcom/my/target/cc;->c(Lcom/my/target/cc;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 711
    iget-object v0, p0, Lcom/my/target/cc$c;->jF:Lcom/my/target/cc;

    invoke-static {v0}, Lcom/my/target/cc;->d(Lcom/my/target/cc;)Landroid/view/Surface;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 713
    iget-object v0, p0, Lcom/my/target/cc$c;->jF:Lcom/my/target/cc;

    invoke-static {v0}, Lcom/my/target/cc;->d(Lcom/my/target/cc;)Landroid/view/Surface;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    .line 715
    :cond_0
    iget-object v0, p0, Lcom/my/target/cc$c;->jF:Lcom/my/target/cc;

    new-instance v1, Landroid/view/Surface;

    invoke-direct {v1, p1}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    invoke-static {v0, v1}, Lcom/my/target/cc;->a(Lcom/my/target/cc;Landroid/view/Surface;)Landroid/view/Surface;

    .line 716
    iget-object v0, p0, Lcom/my/target/cc$c;->jF:Lcom/my/target/cc;

    iget-object v1, p0, Lcom/my/target/cc$c;->jF:Lcom/my/target/cc;

    invoke-static {v1}, Lcom/my/target/cc;->d(Lcom/my/target/cc;)Landroid/view/Surface;

    move-result-object v1

    iget-object v2, p0, Lcom/my/target/cc$c;->uri:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Lcom/my/target/cc;->a(Landroid/view/Surface;Landroid/net/Uri;)V

    .line 727
    :cond_1
    :goto_0
    :pswitch_1
    return-void

    .line 684
    :pswitch_2
    iget-object v0, p0, Lcom/my/target/cc$c;->jF:Lcom/my/target/cc;

    invoke-virtual {v0}, Lcom/my/target/cc;->isMuted()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 686
    iget-object v0, p0, Lcom/my/target/cc$c;->jF:Lcom/my/target/cc;

    invoke-virtual {v0}, Lcom/my/target/cc;->bj()V

    .line 693
    :goto_1
    iget-object v0, p0, Lcom/my/target/cc$c;->jF:Lcom/my/target/cc;

    invoke-static {v0}, Lcom/my/target/cc;->b(Lcom/my/target/cc;)Landroid/media/MediaPlayer;

    move-result-object v0

    if-nez v0, :cond_3

    .line 695
    iget-object v0, p0, Lcom/my/target/cc$c;->jF:Lcom/my/target/cc;

    new-instance v1, Landroid/view/Surface;

    invoke-direct {v1, p1}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    iget-object v2, p0, Lcom/my/target/cc$c;->uri:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Lcom/my/target/cc;->a(Landroid/view/Surface;Landroid/net/Uri;)V

    goto :goto_0

    .line 690
    :cond_2
    iget-object v0, p0, Lcom/my/target/cc$c;->jF:Lcom/my/target/cc;

    invoke-virtual {v0}, Lcom/my/target/cc;->bk()V

    goto :goto_1

    .line 699
    :cond_3
    iget-object v0, p0, Lcom/my/target/cc$c;->jF:Lcom/my/target/cc;

    invoke-static {v0}, Lcom/my/target/cc;->b(Lcom/my/target/cc;)Landroid/media/MediaPlayer;

    move-result-object v0

    new-instance v1, Landroid/view/Surface;

    invoke-direct {v1, p1}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setSurface(Landroid/view/Surface;)V

    .line 700
    iget-object v0, p0, Lcom/my/target/cc$c;->jF:Lcom/my/target/cc;

    invoke-static {v0}, Lcom/my/target/cc;->a(Lcom/my/target/cc;)I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 702
    iget-object v0, p0, Lcom/my/target/cc$c;->jF:Lcom/my/target/cc;

    iget-object v1, p0, Lcom/my/target/cc$c;->jF:Lcom/my/target/cc;

    invoke-static {v1}, Lcom/my/target/cc;->b(Lcom/my/target/cc;)Landroid/media/MediaPlayer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/my/target/cc;->a(Landroid/media/MediaPlayer;)V

    goto :goto_0

    .line 720
    :cond_4
    iget-object v0, p0, Lcom/my/target/cc$c;->jF:Lcom/my/target/cc;

    invoke-virtual {v0}, Lcom/my/target/cc;->bm()V

    .line 721
    iget-object v0, p0, Lcom/my/target/cc$c;->jF:Lcom/my/target/cc;

    invoke-static {v0}, Lcom/my/target/cc;->e(Lcom/my/target/cc;)Lcom/my/target/cc$a;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 723
    iget-object v0, p0, Lcom/my/target/cc$c;->jF:Lcom/my/target/cc;

    invoke-static {v0}, Lcom/my/target/cc;->e(Lcom/my/target/cc;)Lcom/my/target/cc$a;

    move-result-object v0

    invoke-interface {v0}, Lcom/my/target/cc$a;->bt()V

    goto :goto_0

    .line 680
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 3
    .param p1, "surface"    # Landroid/graphics/SurfaceTexture;

    .prologue
    const/4 v2, 0x0

    .line 737
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "VideoTextureView: Surface destroyed, state = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/cc$c;->jF:Lcom/my/target/cc;

    invoke-static {v1}, Lcom/my/target/cc;->a(Lcom/my/target/cc;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 738
    iget-object v0, p0, Lcom/my/target/cc$c;->jF:Lcom/my/target/cc;

    invoke-static {v0}, Lcom/my/target/cc;->d(Lcom/my/target/cc;)Landroid/view/Surface;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 740
    iget-object v0, p0, Lcom/my/target/cc$c;->jF:Lcom/my/target/cc;

    invoke-static {v0}, Lcom/my/target/cc;->d(Lcom/my/target/cc;)Landroid/view/Surface;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    .line 741
    iget-object v0, p0, Lcom/my/target/cc$c;->jF:Lcom/my/target/cc;

    invoke-static {v0, v2}, Lcom/my/target/cc;->a(Lcom/my/target/cc;Landroid/view/Surface;)Landroid/view/Surface;

    .line 744
    :cond_0
    iget-object v0, p0, Lcom/my/target/cc$c;->jF:Lcom/my/target/cc;

    invoke-static {v0}, Lcom/my/target/cc;->b(Lcom/my/target/cc;)Landroid/media/MediaPlayer;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 746
    iget-object v0, p0, Lcom/my/target/cc$c;->jF:Lcom/my/target/cc;

    invoke-static {v0}, Lcom/my/target/cc;->b(Lcom/my/target/cc;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/media/MediaPlayer;->setSurface(Landroid/view/Surface;)V

    .line 748
    iget-object v0, p0, Lcom/my/target/cc$c;->jF:Lcom/my/target/cc;

    invoke-static {v0}, Lcom/my/target/cc;->a(Lcom/my/target/cc;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 768
    iget-object v0, p0, Lcom/my/target/cc$c;->jF:Lcom/my/target/cc;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/my/target/cc;->k(Z)V

    .line 773
    :cond_1
    :goto_0
    :pswitch_0
    const/4 v0, 0x1

    return v0

    .line 751
    :pswitch_1
    iget-object v0, p0, Lcom/my/target/cc$c;->jF:Lcom/my/target/cc;

    invoke-static {v0}, Lcom/my/target/cc;->b(Lcom/my/target/cc;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    .line 752
    iget-object v0, p0, Lcom/my/target/cc$c;->jF:Lcom/my/target/cc;

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/my/target/cc;->a(Lcom/my/target/cc;I)I

    .line 753
    iget-object v0, p0, Lcom/my/target/cc$c;->jF:Lcom/my/target/cc;

    invoke-static {v0}, Lcom/my/target/cc;->e(Lcom/my/target/cc;)Lcom/my/target/cc$a;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 755
    iget-object v0, p0, Lcom/my/target/cc$c;->jF:Lcom/my/target/cc;

    invoke-static {v0}, Lcom/my/target/cc;->e(Lcom/my/target/cc;)Lcom/my/target/cc$a;

    move-result-object v0

    invoke-interface {v0}, Lcom/my/target/cc$a;->bt()V

    goto :goto_0

    .line 762
    :pswitch_2
    const-string v0, "Release MediaPlayer"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 763
    iget-object v0, p0, Lcom/my/target/cc$c;->jF:Lcom/my/target/cc;

    invoke-static {v0}, Lcom/my/target/cc;->b(Lcom/my/target/cc;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 764
    iget-object v0, p0, Lcom/my/target/cc$c;->jF:Lcom/my/target/cc;

    invoke-static {v0, v2}, Lcom/my/target/cc;->a(Lcom/my/target/cc;Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer;

    .line 765
    iget-object v0, p0, Lcom/my/target/cc$c;->jF:Lcom/my/target/cc;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/my/target/cc;->a(Lcom/my/target/cc;I)I

    goto :goto_0

    .line 748
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 0
    .param p1, "surface"    # Landroid/graphics/SurfaceTexture;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 732
    return-void
.end method

.method public onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V
    .locals 0
    .param p1, "surface"    # Landroid/graphics/SurfaceTexture;

    .prologue
    .line 779
    return-void
.end method
