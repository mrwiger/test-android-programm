.class final Lru/cn/api/parental/BlockedChannelRepo;
.super Ljava/lang/Object;
.source "BlockedChannelRepo.java"


# instance fields
.field private sqLiteDatabase:Landroid/database/sqlite/SQLiteDatabase;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    :try_start_0
    new-instance v1, Lru/cn/api/parental/BaseHelper;

    invoke-direct {v1, p1}, Lru/cn/api/parental/BaseHelper;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Lru/cn/api/parental/BaseHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    iput-object v1, p0, Lru/cn/api/parental/BlockedChannelRepo;->sqLiteDatabase:Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 19
    :goto_0
    return-void

    .line 16
    :catch_0
    move-exception v0

    .line 17
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    new-instance v1, Lru/cn/api/parental/BaseHelper;

    invoke-direct {v1, p1}, Lru/cn/api/parental/BaseHelper;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Lru/cn/api/parental/BaseHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    iput-object v1, p0, Lru/cn/api/parental/BlockedChannelRepo;->sqLiteDatabase:Landroid/database/sqlite/SQLiteDatabase;

    goto :goto_0
.end method


# virtual methods
.method delete([Ljava/lang/String;)I
    .locals 5
    .param p1, "channelId"    # [Ljava/lang/String;

    .prologue
    .line 30
    if-eqz p1, :cond_0

    .line 31
    :try_start_0
    iget-object v1, p0, Lru/cn/api/parental/BlockedChannelRepo;->sqLiteDatabase:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "BLOCKED_CHANNELS"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lru/cn/api/parental/DataBaseSchema$BlockedChannelContract;->CN_ID:Lru/cn/api/parental/DataBaseSchema$BlockedChannelContract;

    .line 32
    invoke-virtual {v4}, Lru/cn/api/parental/DataBaseSchema$BlockedChannelContract;->getColumnName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " LIKE?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 31
    invoke-virtual {v1, v2, v3, p1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 35
    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lru/cn/api/parental/BlockedChannelRepo;->sqLiteDatabase:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "BLOCKED_CHANNELS"

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v1

    goto :goto_0

    .line 37
    :catch_0
    move-exception v0

    .line 38
    .local v0, "e":Ljava/lang/RuntimeException;
    :goto_1
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Unable to delete blocked channel"

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 37
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method insert(Landroid/content/ContentValues;)V
    .locals 3
    .param p1, "contentValues"    # Landroid/content/ContentValues;

    .prologue
    .line 23
    :try_start_0
    iget-object v0, p0, Lru/cn/api/parental/BlockedChannelRepo;->sqLiteDatabase:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "BLOCKED_CHANNELS"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 26
    :goto_0
    return-void

    .line 24
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8
    .param p1, "whereClause"    # Ljava/lang/String;
    .param p2, "whereArgs"    # [Ljava/lang/String;
    .param p3, "orderBy"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 46
    if-eqz p1, :cond_0

    .line 47
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " LIKE?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 53
    .local v3, "columnName":Ljava/lang/String;
    :goto_0
    if-eqz p3, :cond_1

    .line 54
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " DESC"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 59
    .local v7, "orderBySort":Ljava/lang/String;
    :goto_1
    iget-object v0, p0, Lru/cn/api/parental/BlockedChannelRepo;->sqLiteDatabase:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "BLOCKED_CHANNELS"

    move-object v4, p2

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0

    .line 49
    .end local v3    # "columnName":Ljava/lang/String;
    .end local v7    # "orderBySort":Ljava/lang/String;
    :cond_0
    move-object v3, p1

    .restart local v3    # "columnName":Ljava/lang/String;
    goto :goto_0

    .line 56
    :cond_1
    const/4 v7, 0x0

    .restart local v7    # "orderBySort":Ljava/lang/String;
    goto :goto_1
.end method
