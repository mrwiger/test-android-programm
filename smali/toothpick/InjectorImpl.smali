.class public final Ltoothpick/InjectorImpl;
.super Ljava/lang/Object;
.source "InjectorImpl.java"

# interfaces
.implements Ltoothpick/Injector;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public inject(Ljava/lang/Object;Ltoothpick/Scope;)V
    .locals 2
    .param p2, "scope"    # Ltoothpick/Scope;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;",
            "Ltoothpick/Scope;",
            ")V"
        }
    .end annotation

    .prologue
    .line 21
    .local p1, "obj":Ljava/lang/Object;, "TT;"
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 23
    .local v0, "currentClass":Ljava/lang/Class;, "Ljava/lang/Class<-TT;>;"
    :cond_0
    invoke-static {v0}, Ltoothpick/registries/MemberInjectorRegistryLocator;->getMemberInjector(Ljava/lang/Class;)Ltoothpick/MemberInjector;

    move-result-object v1

    .line 24
    .local v1, "memberInjector":Ltoothpick/MemberInjector;, "Ltoothpick/MemberInjector<-TT;>;"
    if-eqz v1, :cond_1

    .line 25
    invoke-interface {v1, p1, p2}, Ltoothpick/MemberInjector;->inject(Ljava/lang/Object;Ltoothpick/Scope;)V

    .line 31
    :goto_0
    return-void

    .line 28
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v0

    .line 30
    if-nez v0, :cond_0

    goto :goto_0
.end method
