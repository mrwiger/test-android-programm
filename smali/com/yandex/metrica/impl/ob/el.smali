.class Lcom/yandex/metrica/impl/ob/el;
.super Lcom/yandex/metrica/impl/c;
.source "SourceFile"


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lcom/yandex/metrica/impl/ob/dg;

.field private c:Lcom/yandex/metrica/impl/ob/dl;

.field private d:Lcom/yandex/metrica/impl/interact/DeviceInfo;

.field private e:Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/yandex/metrica/impl/ob/dg;Lcom/yandex/metrica/impl/ob/dl;Lcom/yandex/metrica/impl/interact/DeviceInfo;Lcom/yandex/metrica/impl/ob/dv;)V
    .locals 2

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/yandex/metrica/impl/c;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/yandex/metrica/impl/ob/el;->a:Landroid/content/Context;

    .line 46
    iput-object p2, p0, Lcom/yandex/metrica/impl/ob/el;->b:Lcom/yandex/metrica/impl/ob/dg;

    .line 47
    iput-object p3, p0, Lcom/yandex/metrica/impl/ob/el;->c:Lcom/yandex/metrica/impl/ob/dl;

    .line 48
    iput-object p4, p0, Lcom/yandex/metrica/impl/ob/el;->d:Lcom/yandex/metrica/impl/interact/DeviceInfo;

    .line 49
    invoke-virtual {p0, p5}, Lcom/yandex/metrica/impl/ob/el;->a(Lcom/yandex/metrica/impl/ob/dv;)V

    .line 1054
    invoke-static {}, Lcom/yandex/metrica/impl/bd;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/ob/el;->k(Ljava/lang/String;)V

    .line 1055
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/el;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/el;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/yandex/metrica/impl/bl;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/ob/el;->g(Ljava/lang/String;)V

    .line 1056
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/el;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/el;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/yandex/metrica/impl/bl;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/ob/el;->f(Ljava/lang/String;)V

    .line 1057
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/el;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/yandex/metrica/impl/bd;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/ob/el;->b(Ljava/lang/String;)V

    .line 1058
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/el;->b:Lcom/yandex/metrica/impl/ob/dg;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/dg;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/ob/el;->h(Ljava/lang/String;)V

    .line 1059
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/el;->c:Lcom/yandex/metrica/impl/ob/dl;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/dl;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/ob/el;->i(Ljava/lang/String;)V

    .line 1060
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/el;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/yandex/metrica/impl/interact/DeviceInfo;->getInstance(Landroid/content/Context;)Lcom/yandex/metrica/impl/interact/DeviceInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/yandex/metrica/impl/interact/DeviceInfo;->deviceType:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/ob/el;->m(Ljava/lang/String;)V

    .line 1061
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/el;->d:Lcom/yandex/metrica/impl/interact/DeviceInfo;

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/ob/el;->a(Lcom/yandex/metrica/impl/interact/DeviceInfo;)V

    .line 1062
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/el;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/el;->e:Ljava/lang/String;

    .line 1063
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/el;->b:Lcom/yandex/metrica/impl/ob/dg;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/dg;->d()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/ob/el;->d(Ljava/util/List;)V

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/yandex/metrica/impl/ob/dv;)V
    .locals 6

    .prologue
    .line 33
    new-instance v2, Lcom/yandex/metrica/impl/ob/dg;

    .line 35
    invoke-static {p1}, Lcom/yandex/metrica/impl/ob/cq;->a(Landroid/content/Context;)Lcom/yandex/metrica/impl/ob/cq;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/cq;->d()Lcom/yandex/metrica/impl/ob/cr;

    move-result-object v0

    .line 36
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v0, v1}, Lcom/yandex/metrica/impl/ob/dg;-><init>(Lcom/yandex/metrica/impl/ob/cr;Ljava/lang/String;)V

    .line 37
    invoke-static {}, Lcom/yandex/metrica/impl/ob/dl;->a()Lcom/yandex/metrica/impl/ob/dl;

    move-result-object v3

    invoke-static {p1}, Lcom/yandex/metrica/impl/interact/DeviceInfo;->getInstance(Landroid/content/Context;)Lcom/yandex/metrica/impl/interact/DeviceInfo;

    move-result-object v4

    move-object v0, p0

    move-object v1, p1

    move-object v5, p2

    .line 33
    invoke-direct/range {v0 .. v5}, Lcom/yandex/metrica/impl/ob/el;-><init>(Landroid/content/Context;Lcom/yandex/metrica/impl/ob/dg;Lcom/yandex/metrica/impl/ob/dl;Lcom/yandex/metrica/impl/interact/DeviceInfo;Lcom/yandex/metrica/impl/ob/dv;)V

    .line 40
    return-void
.end method


# virtual methods
.method public I()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/el;->e:Ljava/lang/String;

    return-object v0
.end method
