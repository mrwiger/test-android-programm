.class Lru/cn/guides/GuidesFragment$PagerAdapter;
.super Landroid/support/v4/app/FragmentStatePagerAdapter;
.source "GuidesFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/guides/GuidesFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PagerAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/guides/GuidesFragment;


# direct methods
.method public constructor <init>(Lru/cn/guides/GuidesFragment;Landroid/support/v4/app/FragmentManager;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/guides/GuidesFragment;
    .param p2, "fm"    # Landroid/support/v4/app/FragmentManager;

    .prologue
    .line 26
    iput-object p1, p0, Lru/cn/guides/GuidesFragment$PagerAdapter;->this$0:Lru/cn/guides/GuidesFragment;

    .line 27
    invoke-direct {p0, p2}, Landroid/support/v4/app/FragmentStatePagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    .line 28
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lru/cn/guides/GuidesFragment$PagerAdapter;->this$0:Lru/cn/guides/GuidesFragment;

    iget-object v0, v0, Lru/cn/guides/GuidesFragment;->items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Landroid/support/v4/app/Fragment;
    .locals 4
    .param p1, "pos"    # I

    .prologue
    .line 32
    iget-object v1, p0, Lru/cn/guides/GuidesFragment$PagerAdapter;->this$0:Lru/cn/guides/GuidesFragment;

    iget-object v1, v1, Lru/cn/guides/GuidesFragment;->items:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/guides/GuidManager$GuidItem;

    .line 33
    .local v0, "item":Lru/cn/guides/GuidManager$GuidItem;
    iget v1, v0, Lru/cn/guides/GuidManager$GuidItem;->title:I

    iget v2, v0, Lru/cn/guides/GuidManager$GuidItem;->description:I

    iget v3, v0, Lru/cn/guides/GuidManager$GuidItem;->image:I

    invoke-static {v1, v2, v3}, Lru/cn/guides/GuidesFragmentItem;->newInstance(III)Lru/cn/guides/GuidesFragmentItem;

    move-result-object v1

    return-object v1
.end method
