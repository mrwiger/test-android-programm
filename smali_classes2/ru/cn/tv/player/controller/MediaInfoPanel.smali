.class public Lru/cn/tv/player/controller/MediaInfoPanel;
.super Landroid/widget/FrameLayout;
.source "MediaInfoPanel.java"


# instance fields
.field private channelImageView:Landroid/widget/ImageView;

.field private channelTitleView:Landroid/widget/TextView;

.field private onAirIndicator:Landroid/view/View;

.field private telecastTitleView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 36
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lru/cn/tv/player/controller/MediaInfoPanel;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 40
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lru/cn/tv/player/controller/MediaInfoPanel;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 44
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 46
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 47
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v1, 0x7f0c007d

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 49
    const v1, 0x7f09006b

    invoke-virtual {p0, v1}, Lru/cn/tv/player/controller/MediaInfoPanel;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lru/cn/tv/player/controller/MediaInfoPanel;->channelTitleView:Landroid/widget/TextView;

    .line 50
    const v1, 0x7f090091

    invoke-virtual {p0, v1}, Lru/cn/tv/player/controller/MediaInfoPanel;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lru/cn/tv/player/controller/MediaInfoPanel;->telecastTitleView:Landroid/widget/TextView;

    .line 51
    const v1, 0x7f090069

    invoke-virtual {p0, v1}, Lru/cn/tv/player/controller/MediaInfoPanel;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lru/cn/tv/player/controller/MediaInfoPanel;->channelImageView:Landroid/widget/ImageView;

    .line 52
    const v1, 0x7f090158

    invoke-virtual {p0, v1}, Lru/cn/tv/player/controller/MediaInfoPanel;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lru/cn/tv/player/controller/MediaInfoPanel;->onAirIndicator:Landroid/view/View;

    .line 53
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I
    .param p4, "defStyleRes"    # I
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 57
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 58
    return-void
.end method


# virtual methods
.method public setChannel(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "imageURL"    # Ljava/lang/String;

    .prologue
    .line 61
    iget-object v1, p0, Lru/cn/tv/player/controller/MediaInfoPanel;->channelTitleView:Landroid/widget/TextView;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 63
    if-eqz p2, :cond_0

    .line 64
    invoke-virtual {p0}, Lru/cn/tv/player/controller/MediaInfoPanel;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f040132

    invoke-static {v1, v2}, Lru/cn/utils/Utils;->resolveResourse(Landroid/content/Context;I)I

    move-result v0

    .line 65
    .local v0, "maskResId":I
    invoke-virtual {p0}, Lru/cn/tv/player/controller/MediaInfoPanel;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/picasso/Picasso;->with(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v1

    .line 66
    invoke-virtual {v1}, Lcom/squareup/picasso/RequestCreator;->fit()Lcom/squareup/picasso/RequestCreator;

    move-result-object v1

    new-instance v2, Lru/cn/utils/MaskTransformation;

    .line 67
    invoke-virtual {p0}, Lru/cn/tv/player/controller/MediaInfoPanel;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Lru/cn/utils/MaskTransformation;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v1, v2}, Lcom/squareup/picasso/RequestCreator;->transform(Lcom/squareup/picasso/Transformation;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v1

    const v2, 0x7f080095

    .line 68
    invoke-virtual {v1, v2}, Lcom/squareup/picasso/RequestCreator;->placeholder(I)Lcom/squareup/picasso/RequestCreator;

    move-result-object v1

    iget-object v2, p0, Lru/cn/tv/player/controller/MediaInfoPanel;->channelImageView:Landroid/widget/ImageView;

    .line 69
    invoke-virtual {v1, v2}, Lcom/squareup/picasso/RequestCreator;->into(Landroid/widget/ImageView;)V

    .line 73
    .end local v0    # "maskResId":I
    :goto_0
    return-void

    .line 71
    :cond_0
    iget-object v1, p0, Lru/cn/tv/player/controller/MediaInfoPanel;->channelImageView:Landroid/widget/ImageView;

    const v2, 0x7f080094

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method public setTelecast(Lru/cn/api/tv/replies/Telecast;)V
    .locals 14
    .param p1, "telecast"    # Lru/cn/api/tv/replies/Telecast;

    .prologue
    const/4 v6, 0x0

    .line 76
    if-eqz p1, :cond_2

    .line 77
    iget-object v7, p1, Lru/cn/api/tv/replies/Telecast;->date:Lru/cn/api/tv/replies/DateTime;

    invoke-virtual {v7}, Lru/cn/api/tv/replies/DateTime;->toCalendar()Ljava/util/Calendar;

    move-result-object v1

    .line 78
    .local v1, "telecastDate":Ljava/util/Calendar;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "dd MMMM, HH:mm "

    invoke-static {v1, v8}, Lru/cn/utils/Utils;->format(Ljava/util/Calendar;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p1, Lru/cn/api/tv/replies/Telecast;->title:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 80
    .local v5, "text":Ljava/lang/String;
    invoke-static {}, Lru/cn/utils/Utils;->getCalendar()Ljava/util/Calendar;

    move-result-object v4

    .line 81
    .local v4, "telecastEnd":Ljava/util/Calendar;
    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v8

    iget-wide v10, p1, Lru/cn/api/tv/replies/Telecast;->duration:J

    const-wide/16 v12, 0x3e8

    mul-long/2addr v10, v12

    add-long/2addr v8, v10

    invoke-virtual {v4, v8, v9}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 83
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 84
    .local v2, "now":J
    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v8

    cmp-long v7, v2, v8

    if-lez v7, :cond_0

    invoke-virtual {v4}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v8

    cmp-long v7, v2, v8

    if-gez v7, :cond_0

    const/4 v0, 0x1

    .line 85
    .local v0, "isLiveTelecast":Z
    :goto_0
    iget-object v8, p0, Lru/cn/tv/player/controller/MediaInfoPanel;->onAirIndicator:Landroid/view/View;

    if-eqz v0, :cond_1

    move v7, v6

    :goto_1
    invoke-virtual {v8, v7}, Landroid/view/View;->setVisibility(I)V

    .line 87
    iget-object v7, p0, Lru/cn/tv/player/controller/MediaInfoPanel;->telecastTitleView:Landroid/widget/TextView;

    invoke-virtual {v7, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 88
    iget-object v7, p0, Lru/cn/tv/player/controller/MediaInfoPanel;->telecastTitleView:Landroid/widget/TextView;

    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 92
    .end local v0    # "isLiveTelecast":Z
    .end local v1    # "telecastDate":Ljava/util/Calendar;
    .end local v2    # "now":J
    .end local v4    # "telecastEnd":Ljava/util/Calendar;
    .end local v5    # "text":Ljava/lang/String;
    :goto_2
    return-void

    .restart local v1    # "telecastDate":Ljava/util/Calendar;
    .restart local v2    # "now":J
    .restart local v4    # "telecastEnd":Ljava/util/Calendar;
    .restart local v5    # "text":Ljava/lang/String;
    :cond_0
    move v0, v6

    .line 84
    goto :goto_0

    .line 85
    .restart local v0    # "isLiveTelecast":Z
    :cond_1
    const/16 v7, 0x8

    goto :goto_1

    .line 90
    .end local v0    # "isLiveTelecast":Z
    .end local v1    # "telecastDate":Ljava/util/Calendar;
    .end local v2    # "now":J
    .end local v4    # "telecastEnd":Ljava/util/Calendar;
    .end local v5    # "text":Ljava/lang/String;
    :cond_2
    iget-object v6, p0, Lru/cn/tv/player/controller/MediaInfoPanel;->telecastTitleView:Landroid/widget/TextView;

    const/4 v7, 0x4

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2
.end method
