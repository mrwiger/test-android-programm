.class public Lcom/my/target/ca;
.super Landroid/view/View;
.source "StarsRatingView.java"


# instance fields
.field private hP:F

.field private final iV:Lcom/my/target/bs;

.field private iW:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 19
    new-instance v0, Lcom/my/target/bs;

    invoke-direct {v0}, Lcom/my/target/bs;-><init>()V

    iput-object v0, p0, Lcom/my/target/ca;->iV:Lcom/my/target/bs;

    .line 20
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 67
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 68
    iget-object v0, p0, Lcom/my/target/ca;->iV:Lcom/my/target/bs;

    invoke-virtual {v0, p1}, Lcom/my/target/bs;->draw(Landroid/graphics/Canvas;)V

    .line 69
    return-void
.end method

.method protected onMeasure(II)V
    .locals 5
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/high16 v4, -0x80000000

    .line 49
    iget v0, p0, Lcom/my/target/ca;->iW:I

    if-lez v0, :cond_0

    .line 51
    iget v0, p0, Lcom/my/target/ca;->iW:I

    .line 59
    :goto_0
    mul-int/lit8 v1, v0, 0x5

    int-to-float v1, v1

    iget v2, p0, Lcom/my/target/ca;->hP:F

    const/high16 v3, 0x40800000    # 4.0f

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    float-to-int v1, v1

    .line 60
    invoke-static {v1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 61
    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 60
    invoke-super {p0, v1, v0}, Landroid/view/View;->onMeasure(II)V

    .line 62
    return-void

    .line 55
    :cond_0
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 56
    iget-object v1, p0, Lcom/my/target/ca;->iV:Lcom/my/target/bs;

    invoke-virtual {v1, v0}, Lcom/my/target/bs;->setStarSize(I)V

    goto :goto_0
.end method

.method public setRating(D)V
    .locals 1
    .param p1, "rating"    # D

    .prologue
    .line 36
    double-to-float v0, p1

    invoke-virtual {p0, v0}, Lcom/my/target/ca;->setRating(F)V

    .line 37
    return-void
.end method

.method public setRating(F)V
    .locals 1
    .param p1, "rating"    # F

    .prologue
    .line 30
    invoke-static {p1}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/my/target/ca;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 31
    iget-object v0, p0, Lcom/my/target/ca;->iV:Lcom/my/target/bs;

    invoke-virtual {v0, p1}, Lcom/my/target/bs;->setRating(F)V

    .line 32
    return-void
.end method

.method public setStarSize(I)V
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 24
    iput p1, p0, Lcom/my/target/ca;->iW:I

    .line 25
    iget-object v0, p0, Lcom/my/target/ca;->iV:Lcom/my/target/bs;

    invoke-virtual {v0, p1}, Lcom/my/target/bs;->setStarSize(I)V

    .line 26
    return-void
.end method

.method public setStarsPadding(F)V
    .locals 1
    .param p1, "padding"    # F

    .prologue
    .line 41
    iget-object v0, p0, Lcom/my/target/ca;->iV:Lcom/my/target/bs;

    invoke-virtual {v0, p1}, Lcom/my/target/bs;->setStarsPadding(F)V

    .line 42
    iput p1, p0, Lcom/my/target/ca;->hP:F

    .line 43
    return-void
.end method
