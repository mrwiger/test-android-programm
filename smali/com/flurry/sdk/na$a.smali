.class public final Lcom/flurry/sdk/na$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/flurry/sdk/nh;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/flurry/sdk/na;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/flurry/sdk/nh",
        "<",
        "Lcom/flurry/sdk/na;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/io/InputStream;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 44
    .line 1075
    if-nez p1, :cond_1

    .line 1091
    :cond_0
    :goto_0
    return-object v0

    .line 1079
    :cond_1
    new-instance v2, Lcom/flurry/sdk/na$a$2;

    invoke-direct {v2, p0, p1}, Lcom/flurry/sdk/na$a$2;-><init>(Lcom/flurry/sdk/na$a;Ljava/io/InputStream;)V

    .line 1086
    new-instance v1, Lcom/flurry/sdk/na;

    const/4 v3, 0x0

    invoke-direct {v1, v3}, Lcom/flurry/sdk/na;-><init>(B)V

    .line 1088
    invoke-virtual {v2}, Ljava/io/DataInputStream;->readShort()S

    move-result v3

    .line 1089
    if-eqz v3, :cond_0

    .line 1093
    new-array v0, v3, [B

    .line 2013
    iput-object v0, v1, Lcom/flurry/sdk/na;->b:[B

    .line 3013
    iget-object v0, v1, Lcom/flurry/sdk/na;->b:[B

    .line 1094
    invoke-virtual {v2, v0}, Ljava/io/DataInputStream;->readFully([B)V

    .line 1096
    invoke-virtual {v2}, Ljava/io/DataInputStream;->readUnsignedShort()I

    move-object v0, v1

    .line 44
    goto :goto_0
.end method

.method public final synthetic a(Ljava/io/OutputStream;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 44
    check-cast p2, Lcom/flurry/sdk/na;

    .line 3052
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 3053
    :cond_0
    :goto_0
    return-void

    .line 3056
    :cond_1
    new-instance v0, Lcom/flurry/sdk/na$a$1;

    invoke-direct {v0, p0, p1}, Lcom/flurry/sdk/na$a$1;-><init>(Lcom/flurry/sdk/na$a;Ljava/io/OutputStream;)V

    .line 4013
    iget-object v1, p2, Lcom/flurry/sdk/na;->b:[B

    .line 3063
    array-length v1, v1

    .line 3065
    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 5013
    iget-object v1, p2, Lcom/flurry/sdk/na;->b:[B

    .line 3066
    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->write([B)V

    .line 3067
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeShort(I)V

    .line 3069
    invoke-virtual {v0}, Ljava/io/DataOutputStream;->flush()V

    goto :goto_0
.end method
