.class Lru/cn/player/androidplayer/AndroidTrackInfo;
.super Lru/cn/player/TrackInfo;
.source "AndroidTrackInfo.java"


# instance fields
.field final trackIndex:I


# direct methods
.method private constructor <init>(ILjava/lang/String;I)V
    .locals 0
    .param p1, "height"    # I
    .param p2, "language"    # Ljava/lang/String;
    .param p3, "trackIndex"    # I

    .prologue
    .line 13
    invoke-direct {p0, p1, p2}, Lru/cn/player/TrackInfo;-><init>(ILjava/lang/String;)V

    .line 14
    iput p3, p0, Lru/cn/player/androidplayer/AndroidTrackInfo;->trackIndex:I

    .line 15
    return-void
.end method

.method static createTrackInfo(Landroid/media/MediaPlayer$TrackInfo;I)Lru/cn/player/androidplayer/AndroidTrackInfo;
    .locals 3
    .param p0, "info"    # Landroid/media/MediaPlayer$TrackInfo;
    .param p1, "trackIndex"    # I
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 19
    new-instance v0, Lru/cn/player/androidplayer/AndroidTrackInfo;

    const/4 v1, 0x0

    invoke-virtual {p0}, Landroid/media/MediaPlayer$TrackInfo;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2, p1}, Lru/cn/player/androidplayer/AndroidTrackInfo;-><init>(ILjava/lang/String;I)V

    return-object v0
.end method
