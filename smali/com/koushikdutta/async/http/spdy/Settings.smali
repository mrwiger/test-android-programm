.class final Lcom/koushikdutta/async/http/spdy/Settings;
.super Ljava/lang/Object;
.source "Settings.java"


# instance fields
.field private persistValue:I

.field private persisted:I

.field private set:I

.field private final values:[I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    const/16 v0, 0xa

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/koushikdutta/async/http/spdy/Settings;->values:[I

    return-void
.end method


# virtual methods
.method public clear()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 81
    iput v1, p0, Lcom/koushikdutta/async/http/spdy/Settings;->persisted:I

    iput v1, p0, Lcom/koushikdutta/async/http/spdy/Settings;->persistValue:I

    iput v1, p0, Lcom/koushikdutta/async/http/spdy/Settings;->set:I

    .line 82
    iget-object v0, p0, Lcom/koushikdutta/async/http/spdy/Settings;->values:[I

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    .line 83
    return-void
.end method

.method flags(I)I
    .locals 2
    .param p1, "id"    # I

    .prologue
    .line 120
    const/4 v0, 0x0

    .line 121
    .local v0, "result":I
    invoke-virtual {p0, p1}, Lcom/koushikdutta/async/http/spdy/Settings;->isPersisted(I)Z

    move-result v1

    if-eqz v1, :cond_0

    or-int/lit8 v0, v0, 0x2

    .line 122
    :cond_0
    invoke-virtual {p0, p1}, Lcom/koushikdutta/async/http/spdy/Settings;->persistValue(I)Z

    move-result v1

    if-eqz v1, :cond_1

    or-int/lit8 v0, v0, 0x1

    .line 123
    :cond_1
    return v0
.end method

.method get(I)I
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 115
    iget-object v0, p0, Lcom/koushikdutta/async/http/spdy/Settings;->values:[I

    aget v0, v0, p1

    return v0
.end method

.method getHeaderTableSize()I
    .locals 3

    .prologue
    .line 139
    const/4 v0, 0x2

    .line 140
    .local v0, "bit":I
    iget v1, p0, Lcom/koushikdutta/async/http/spdy/Settings;->set:I

    and-int/2addr v1, v0

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/koushikdutta/async/http/spdy/Settings;->values:[I

    const/4 v2, 0x1

    aget v1, v1, v2

    :goto_0
    return v1

    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getInitialWindowSize(I)I
    .locals 3
    .param p1, "defaultValue"    # I

    .prologue
    .line 181
    const/16 v0, 0x80

    .line 182
    .local v0, "bit":I
    iget v1, p0, Lcom/koushikdutta/async/http/spdy/Settings;->set:I

    and-int/2addr v1, v0

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/koushikdutta/async/http/spdy/Settings;->values:[I

    const/4 v2, 0x7

    aget p1, v1, v2

    .end local p1    # "defaultValue":I
    :cond_0
    return p1
.end method

.method isPersisted(I)Z
    .locals 3
    .param p1, "id"    # I

    .prologue
    const/4 v1, 0x1

    .line 209
    shl-int v0, v1, p1

    .line 210
    .local v0, "bit":I
    iget v2, p0, Lcom/koushikdutta/async/http/spdy/Settings;->persisted:I

    and-int/2addr v2, v0

    if-eqz v2, :cond_0

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method isSet(I)Z
    .locals 3
    .param p1, "id"    # I

    .prologue
    const/4 v1, 0x1

    .line 109
    shl-int v0, v1, p1

    .line 110
    .local v0, "bit":I
    iget v2, p0, Lcom/koushikdutta/async/http/spdy/Settings;->set:I

    and-int/2addr v2, v0

    if-eqz v2, :cond_0

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public merge(Lcom/koushikdutta/async/http/spdy/Settings;)V
    .locals 3
    .param p1, "other"    # Lcom/koushikdutta/async/http/spdy/Settings;

    .prologue
    .line 218
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v1, 0xa

    if-ge v0, v1, :cond_1

    .line 219
    invoke-virtual {p1, v0}, Lcom/koushikdutta/async/http/spdy/Settings;->isSet(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 218
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 220
    :cond_0
    invoke-virtual {p1, v0}, Lcom/koushikdutta/async/http/spdy/Settings;->flags(I)I

    move-result v1

    invoke-virtual {p1, v0}, Lcom/koushikdutta/async/http/spdy/Settings;->get(I)I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/koushikdutta/async/http/spdy/Settings;->set(III)Lcom/koushikdutta/async/http/spdy/Settings;

    goto :goto_1

    .line 222
    :cond_1
    return-void
.end method

.method persistValue(I)Z
    .locals 3
    .param p1, "id"    # I

    .prologue
    const/4 v1, 0x1

    .line 203
    shl-int v0, v1, p1

    .line 204
    .local v0, "bit":I
    iget v2, p0, Lcom/koushikdutta/async/http/spdy/Settings;->persistValue:I

    and-int/2addr v2, v0

    if-eqz v2, :cond_0

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public set(III)Lcom/koushikdutta/async/http/spdy/Settings;
    .locals 3
    .param p1, "id"    # I
    .param p2, "idFlags"    # I
    .param p3, "value"    # I

    .prologue
    .line 86
    iget-object v1, p0, Lcom/koushikdutta/async/http/spdy/Settings;->values:[I

    array-length v1, v1

    if-lt p1, v1, :cond_0

    .line 104
    :goto_0
    return-object p0

    .line 90
    :cond_0
    const/4 v1, 0x1

    shl-int v0, v1, p1

    .line 91
    .local v0, "bit":I
    iget v1, p0, Lcom/koushikdutta/async/http/spdy/Settings;->set:I

    or-int/2addr v1, v0

    iput v1, p0, Lcom/koushikdutta/async/http/spdy/Settings;->set:I

    .line 92
    and-int/lit8 v1, p2, 0x1

    if-eqz v1, :cond_1

    .line 93
    iget v1, p0, Lcom/koushikdutta/async/http/spdy/Settings;->persistValue:I

    or-int/2addr v1, v0

    iput v1, p0, Lcom/koushikdutta/async/http/spdy/Settings;->persistValue:I

    .line 97
    :goto_1
    and-int/lit8 v1, p2, 0x2

    if-eqz v1, :cond_2

    .line 98
    iget v1, p0, Lcom/koushikdutta/async/http/spdy/Settings;->persisted:I

    or-int/2addr v1, v0

    iput v1, p0, Lcom/koushikdutta/async/http/spdy/Settings;->persisted:I

    .line 103
    :goto_2
    iget-object v1, p0, Lcom/koushikdutta/async/http/spdy/Settings;->values:[I

    aput p3, v1, p1

    goto :goto_0

    .line 95
    :cond_1
    iget v1, p0, Lcom/koushikdutta/async/http/spdy/Settings;->persistValue:I

    xor-int/lit8 v2, v0, -0x1

    and-int/2addr v1, v2

    iput v1, p0, Lcom/koushikdutta/async/http/spdy/Settings;->persistValue:I

    goto :goto_1

    .line 100
    :cond_2
    iget v1, p0, Lcom/koushikdutta/async/http/spdy/Settings;->persisted:I

    xor-int/lit8 v2, v0, -0x1

    and-int/2addr v1, v2

    iput v1, p0, Lcom/koushikdutta/async/http/spdy/Settings;->persisted:I

    goto :goto_2
.end method

.method size()I
    .locals 1

    .prologue
    .line 128
    iget v0, p0, Lcom/koushikdutta/async/http/spdy/Settings;->set:I

    invoke-static {v0}, Ljava/lang/Integer;->bitCount(I)I

    move-result v0

    return v0
.end method
