.class public Lcom/yandex/metrica/impl/ob/ai;
.super Lcom/yandex/metrica/impl/ob/ah;
.source "SourceFile"


# direct methods
.method protected constructor <init>(Lcom/yandex/metrica/impl/ob/v;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/yandex/metrica/impl/ob/ah;-><init>(Lcom/yandex/metrica/impl/ob/v;)V

    .line 42
    return-void
.end method


# virtual methods
.method public a(Lcom/yandex/metrica/impl/i;)Z
    .locals 6

    .prologue
    .line 45
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/ai;->a()Lcom/yandex/metrica/impl/ob/v;

    move-result-object v1

    .line 53
    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/v;->D()Lcom/yandex/metrica/impl/ob/db;

    move-result-object v0

    .line 54
    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/db;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/v;->C()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 55
    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/v;->J()Lcom/yandex/metrica/impl/ob/dd;

    move-result-object v2

    .line 56
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/ai;->b()Ljava/util/HashSet;

    move-result-object v0

    .line 58
    :try_start_0
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/ai;->c()Ljava/util/ArrayList;

    move-result-object v3

    .line 59
    invoke-static {v0, v3}, Lcom/yandex/metrica/impl/utils/d;->a(Ljava/util/Collection;Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 60
    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/v;->s()V

    .line 75
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 62
    :cond_1
    new-instance v4, Lorg/json/JSONArray;

    invoke-direct {v4}, Lorg/json/JSONArray;-><init>()V

    .line 63
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/yandex/metrica/impl/ob/dp;

    .line 64
    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/dp;->a()Lorg/json/JSONObject;

    move-result-object v0

    invoke-virtual {v4, v0}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_0

    .line 66
    :cond_2
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v3, "features"

    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1409
    new-instance v3, Lcom/yandex/metrica/impl/i;

    invoke-direct {v3, p1}, Lcom/yandex/metrica/impl/i;-><init>(Lcom/yandex/metrica/impl/i;)V

    .line 1410
    sget-object v5, Lcom/yandex/metrica/impl/q$a;->G:Lcom/yandex/metrica/impl/q$a;

    invoke-virtual {v5}, Lcom/yandex/metrica/impl/q$a;->a()I

    move-result v5

    invoke-virtual {v3, v5}, Lcom/yandex/metrica/impl/i;->a(I)Lcom/yandex/metrica/impl/i;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/yandex/metrica/impl/i;->c(Ljava/lang/String;)Lcom/yandex/metrica/impl/i;

    move-result-object v0

    .line 68
    invoke-virtual {v1, v0}, Lcom/yandex/metrica/impl/ob/v;->h(Lcom/yandex/metrica/impl/i;)V

    .line 69
    invoke-virtual {v4}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/yandex/metrica/impl/ob/dd;->c(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/dd;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0
.end method

.method b()Ljava/util/HashSet;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashSet",
            "<",
            "Lcom/yandex/metrica/impl/ob/dp;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 81
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/ai;->a()Lcom/yandex/metrica/impl/ob/v;

    move-result-object v1

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/v;->J()Lcom/yandex/metrica/impl/ob/dd;

    move-result-object v1

    .line 82
    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/dd;->b()Ljava/lang/String;

    move-result-object v2

    .line 83
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 95
    :goto_0
    return-object v0

    .line 87
    :cond_0
    :try_start_0
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 88
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3, v2}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 89
    const/4 v2, 0x0

    :goto_1
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v2, v4, :cond_1

    .line 90
    new-instance v4, Lcom/yandex/metrica/impl/ob/dp;

    invoke-virtual {v3, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/yandex/metrica/impl/ob/dp;-><init>(Lorg/json/JSONObject;)V

    invoke-virtual {v1, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 89
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 92
    goto :goto_0

    .line 95
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method c()Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/yandex/metrica/impl/ob/dp;",
            ">;"
        }
    .end annotation

    .prologue
    .line 104
    :try_start_0
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/ai;->a()Lcom/yandex/metrica/impl/ob/v;

    move-result-object v0

    .line 105
    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/v;->n()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 106
    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/v;->n()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const/16 v2, 0x4000

    .line 105
    invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    .line 109
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2048
    const/16 v1, 0x18

    invoke-static {v1}, Lcom/yandex/metrica/impl/bl;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2049
    new-instance v1, Lcom/yandex/metrica/impl/ob/do$a;

    invoke-direct {v1}, Lcom/yandex/metrica/impl/ob/do$a;-><init>()V

    move-object v2, v1

    .line 112
    :goto_0
    iget-object v1, v3, Landroid/content/pm/PackageInfo;->reqFeatures:[Landroid/content/pm/FeatureInfo;

    if-eqz v1, :cond_1

    .line 113
    iget-object v3, v3, Landroid/content/pm/PackageInfo;->reqFeatures:[Landroid/content/pm/FeatureInfo;

    array-length v4, v3

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v4, :cond_1

    aget-object v5, v3, v1

    .line 114
    invoke-virtual {v2, v5}, Lcom/yandex/metrica/impl/ob/do;->b(Landroid/content/pm/FeatureInfo;)Lcom/yandex/metrica/impl/ob/dp;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 113
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2051
    :cond_0
    new-instance v1, Lcom/yandex/metrica/impl/ob/do$b;

    invoke-direct {v1}, Lcom/yandex/metrica/impl/ob/do$b;-><init>()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v2, v1

    goto :goto_0

    .line 120
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    :cond_1
    return-object v0
.end method
