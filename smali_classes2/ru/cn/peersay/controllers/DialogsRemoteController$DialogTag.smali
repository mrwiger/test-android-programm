.class Lru/cn/peersay/controllers/DialogsRemoteController$DialogTag;
.super Ljava/lang/Object;
.source "DialogsRemoteController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/peersay/controllers/DialogsRemoteController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DialogTag"
.end annotation


# instance fields
.field public final id:J

.field public final listener:Lru/cn/peersay/controllers/DialogsRemoteController$DismissListener;

.field public final tagName:Ljava/lang/String;


# direct methods
.method constructor <init>(JLjava/lang/String;Lru/cn/peersay/controllers/DialogsRemoteController$DismissListener;)V
    .locals 1
    .param p1, "id"    # J
    .param p3, "tagName"    # Ljava/lang/String;
    .param p4, "listener"    # Lru/cn/peersay/controllers/DialogsRemoteController$DismissListener;

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-wide p1, p0, Lru/cn/peersay/controllers/DialogsRemoteController$DialogTag;->id:J

    .line 30
    iput-object p3, p0, Lru/cn/peersay/controllers/DialogsRemoteController$DialogTag;->tagName:Ljava/lang/String;

    .line 31
    iput-object p4, p0, Lru/cn/peersay/controllers/DialogsRemoteController$DialogTag;->listener:Lru/cn/peersay/controllers/DialogsRemoteController$DismissListener;

    .line 32
    return-void
.end method
