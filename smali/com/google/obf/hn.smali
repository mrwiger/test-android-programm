.class public abstract Lcom/google/obf/hn;
.super Ljava/lang/Object;
.source "IMASDK"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/obf/hn$a;,
        Lcom/google/obf/hn$b;
    }
.end annotation


# instance fields
.field private final a:Lcom/google/obf/hn$a;

.field protected final b:J

.field protected c:Z

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/obf/hn$b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(J)V
    .locals 1

    .prologue
    .line 1
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1, p2}, Lcom/google/obf/hn;-><init>(Lcom/google/obf/hn$a;J)V

    .line 2
    return-void
.end method

.method constructor <init>(Lcom/google/obf/hn$a;J)V
    .locals 2

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/obf/hn;->c:Z

    .line 5
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/obf/hn;->d:Ljava/util/List;

    .line 6
    iput-wide p2, p0, Lcom/google/obf/hn;->b:J

    .line 7
    if-eqz p1, :cond_0

    .line 8
    iput-object p1, p0, Lcom/google/obf/hn;->a:Lcom/google/obf/hn$a;

    .line 10
    :goto_0
    return-void

    .line 9
    :cond_0
    new-instance v0, Lcom/google/obf/hn$a;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1, p0}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    invoke-direct {v0, v1}, Lcom/google/obf/hn$a;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/obf/hn;->a:Lcom/google/obf/hn$a;

    goto :goto_0
.end method


# virtual methods
.method public abstract a()Lcom/google/ads/interactivemedia/v3/api/player/VideoProgressUpdate;
.end method

.method public a(Lcom/google/obf/hn$b;)V
    .locals 1

    .prologue
    .line 11
    iget-object v0, p0, Lcom/google/obf/hn;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 12
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 15
    iget-boolean v0, p0, Lcom/google/obf/hn;->c:Z

    if-eqz v0, :cond_0

    .line 19
    :goto_0
    return-void

    .line 17
    :cond_0
    iput-boolean v1, p0, Lcom/google/obf/hn;->c:Z

    .line 18
    iget-object v0, p0, Lcom/google/obf/hn;->a:Lcom/google/obf/hn$a;

    invoke-virtual {v0, v1}, Lcom/google/obf/hn$a;->b(I)Z

    goto :goto_0
.end method

.method public b(Lcom/google/obf/hn$b;)V
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lcom/google/obf/hn;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 14
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 20
    iget-boolean v0, p0, Lcom/google/obf/hn;->c:Z

    if-eqz v0, :cond_0

    .line 21
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/obf/hn;->c:Z

    .line 22
    iget-object v0, p0, Lcom/google/obf/hn;->a:Lcom/google/obf/hn$a;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/obf/hn$a;->c(I)Z

    .line 23
    :cond_0
    return-void
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 24
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 33
    :goto_0
    return v4

    .line 25
    :pswitch_0
    iget-object v0, p0, Lcom/google/obf/hn;->a:Lcom/google/obf/hn$a;

    invoke-virtual {v0, v4}, Lcom/google/obf/hn$a;->a(I)V

    goto :goto_0

    .line 27
    :pswitch_1
    invoke-virtual {p0}, Lcom/google/obf/hn;->a()Lcom/google/ads/interactivemedia/v3/api/player/VideoProgressUpdate;

    move-result-object v1

    .line 28
    iget-object v0, p0, Lcom/google/obf/hn;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/hn$b;

    .line 29
    invoke-interface {v0, v1}, Lcom/google/obf/hn$b;->a(Lcom/google/ads/interactivemedia/v3/api/player/VideoProgressUpdate;)V

    goto :goto_1

    .line 31
    :cond_0
    iget-object v0, p0, Lcom/google/obf/hn;->a:Lcom/google/obf/hn$a;

    iget-wide v2, p0, Lcom/google/obf/hn;->b:J

    invoke-virtual {v0, v4, v2, v3}, Lcom/google/obf/hn$a;->a(IJ)Z

    goto :goto_0

    .line 24
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
