.class final enum Lcom/samsung/multiscreen/Player$PlayerControlEvents;
.super Ljava/lang/Enum;
.source "Player.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/multiscreen/Player;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "PlayerControlEvents"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/multiscreen/Player$PlayerControlEvents;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/multiscreen/Player$PlayerControlEvents;

.field public static final enum FF:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

.field public static final enum RWD:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

.field public static final enum getControlStatus:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

.field public static final enum getVolume:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

.field public static final enum mute:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

.field public static final enum next:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

.field public static final enum pause:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

.field public static final enum play:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

.field public static final enum playMusic:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

.field public static final enum previous:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

.field public static final enum repeat:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

.field public static final enum seekTo:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

.field public static final enum setRepeat:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

.field public static final enum setShuffle:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

.field public static final enum setVolume:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

.field public static final enum shuffle:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

.field public static final enum stop:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

.field public static final enum stopMusic:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

.field public static final enum unMute:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

.field public static final enum volumeDown:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

.field public static final enum volumeUp:Lcom/samsung/multiscreen/Player$PlayerControlEvents;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 79
    new-instance v0, Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    const-string v1, "play"

    invoke-direct {v0, v1, v3}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->play:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    .line 80
    new-instance v0, Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    const-string v1, "pause"

    invoke-direct {v0, v1, v4}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->pause:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    .line 81
    new-instance v0, Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    const-string v1, "stop"

    invoke-direct {v0, v1, v5}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->stop:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    .line 82
    new-instance v0, Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    const-string v1, "mute"

    invoke-direct {v0, v1, v6}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->mute:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    .line 83
    new-instance v0, Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    const-string v1, "unMute"

    invoke-direct {v0, v1, v7}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->unMute:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    .line 84
    new-instance v0, Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    const-string v1, "setVolume"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->setVolume:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    .line 85
    new-instance v0, Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    const-string v1, "getControlStatus"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->getControlStatus:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    .line 86
    new-instance v0, Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    const-string v1, "getVolume"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->getVolume:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    .line 87
    new-instance v0, Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    const-string v1, "volumeUp"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->volumeUp:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    .line 88
    new-instance v0, Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    const-string v1, "volumeDown"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->volumeDown:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    .line 89
    new-instance v0, Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    const-string v1, "previous"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->previous:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    .line 90
    new-instance v0, Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    const-string v1, "next"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->next:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    .line 92
    new-instance v0, Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    const-string v1, "FF"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->FF:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    .line 93
    new-instance v0, Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    const-string v1, "RWD"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->RWD:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    .line 95
    new-instance v0, Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    const-string v1, "seekTo"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->seekTo:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    .line 96
    new-instance v0, Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    const-string v1, "repeat"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->repeat:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    .line 97
    new-instance v0, Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    const-string v1, "setRepeat"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->setRepeat:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    .line 99
    new-instance v0, Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    const-string v1, "shuffle"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->shuffle:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    .line 100
    new-instance v0, Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    const-string v1, "setShuffle"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->setShuffle:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    .line 103
    new-instance v0, Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    const-string v1, "playMusic"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->playMusic:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    .line 104
    new-instance v0, Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    const-string v1, "stopMusic"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->stopMusic:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    .line 77
    const/16 v0, 0x15

    new-array v0, v0, [Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    sget-object v1, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->play:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    aput-object v1, v0, v3

    sget-object v1, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->pause:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    aput-object v1, v0, v4

    sget-object v1, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->stop:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    aput-object v1, v0, v5

    sget-object v1, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->mute:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    aput-object v1, v0, v6

    sget-object v1, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->unMute:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->setVolume:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->getControlStatus:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->getVolume:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->volumeUp:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->volumeDown:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->previous:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->next:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->FF:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->RWD:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->seekTo:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->repeat:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->setRepeat:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->shuffle:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->setShuffle:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->playMusic:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->stopMusic:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->$VALUES:[Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 77
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/multiscreen/Player$PlayerControlEvents;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 77
    const-class v0, Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    return-object v0
.end method

.method public static values()[Lcom/samsung/multiscreen/Player$PlayerControlEvents;
    .locals 1

    .prologue
    .line 77
    sget-object v0, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->$VALUES:[Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    invoke-virtual {v0}, [Lcom/samsung/multiscreen/Player$PlayerControlEvents;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    return-object v0
.end method
