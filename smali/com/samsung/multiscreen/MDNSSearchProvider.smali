.class public Lcom/samsung/multiscreen/MDNSSearchProvider;
.super Lcom/samsung/multiscreen/SearchProvider;
.source "MDNSSearchProvider.java"


# static fields
.field private static final MAX_GET_SERVICE_INFO_WAIT_TIME:J = 0x3a98L

.field private static final SERVICE_CHECK_TIMEOUT:I = 0x7d0

.field static final SERVICE_TYPE:Ljava/lang/String; = "_samsungmsf._tcp.local."

.field private static final TAG:Ljava/lang/String; = "MDNSSearchProvider"


# instance fields
.field private final context:Landroid/content/Context;

.field private volatile jmdns:Ljavax/jmdns/JmDNS;

.field private volatile multicastLock:Landroid/net/wifi/WifiManager$MulticastLock;

.field private final serviceListener:Ljavax/jmdns/ServiceListener;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 109
    invoke-direct {p0}, Lcom/samsung/multiscreen/SearchProvider;-><init>()V

    .line 62
    new-instance v0, Lcom/samsung/multiscreen/MDNSSearchProvider$1;

    invoke-direct {v0, p0}, Lcom/samsung/multiscreen/MDNSSearchProvider$1;-><init>(Lcom/samsung/multiscreen/MDNSSearchProvider;)V

    iput-object v0, p0, Lcom/samsung/multiscreen/MDNSSearchProvider;->serviceListener:Ljavax/jmdns/ServiceListener;

    .line 111
    iput-object p1, p0, Lcom/samsung/multiscreen/MDNSSearchProvider;->context:Landroid/content/Context;

    .line 112
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/samsung/multiscreen/Search$SearchListener;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "searchListener"    # Lcom/samsung/multiscreen/Search$SearchListener;

    .prologue
    .line 115
    invoke-direct {p0, p2}, Lcom/samsung/multiscreen/SearchProvider;-><init>(Lcom/samsung/multiscreen/Search$SearchListener;)V

    .line 62
    new-instance v0, Lcom/samsung/multiscreen/MDNSSearchProvider$1;

    invoke-direct {v0, p0}, Lcom/samsung/multiscreen/MDNSSearchProvider$1;-><init>(Lcom/samsung/multiscreen/MDNSSearchProvider;)V

    iput-object v0, p0, Lcom/samsung/multiscreen/MDNSSearchProvider;->serviceListener:Ljavax/jmdns/ServiceListener;

    .line 117
    iput-object p1, p0, Lcom/samsung/multiscreen/MDNSSearchProvider;->context:Landroid/content/Context;

    .line 118
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/multiscreen/MDNSSearchProvider;)Ljavax/jmdns/JmDNS;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/multiscreen/MDNSSearchProvider;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/samsung/multiscreen/MDNSSearchProvider;->jmdns:Ljavax/jmdns/JmDNS;

    return-object v0
.end method

.method private acquireMulticastLock()Z
    .locals 4

    .prologue
    .line 299
    const/4 v1, 0x0

    .line 302
    .local v1, "success":Z
    :try_start_0
    iget-object v2, p0, Lcom/samsung/multiscreen/MDNSSearchProvider;->multicastLock:Landroid/net/wifi/WifiManager$MulticastLock;

    if-nez v2, :cond_1

    .line 303
    iget-object v2, p0, Lcom/samsung/multiscreen/MDNSSearchProvider;->context:Landroid/content/Context;

    const-string v3, "MDNSSearchProvider"

    invoke-static {v2, v3}, Lcom/samsung/multiscreen/util/NetUtil;->acquireMulticastLock(Landroid/content/Context;Ljava/lang/String;)Landroid/net/wifi/WifiManager$MulticastLock;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/multiscreen/MDNSSearchProvider;->multicastLock:Landroid/net/wifi/WifiManager$MulticastLock;

    .line 307
    :cond_0
    :goto_0
    const/4 v1, 0x1

    .line 312
    :goto_1
    return v1

    .line 304
    :cond_1
    iget-object v2, p0, Lcom/samsung/multiscreen/MDNSSearchProvider;->multicastLock:Landroid/net/wifi/WifiManager$MulticastLock;

    invoke-virtual {v2}, Landroid/net/wifi/WifiManager$MulticastLock;->isHeld()Z

    move-result v2

    if-nez v2, :cond_0

    .line 305
    iget-object v2, p0, Lcom/samsung/multiscreen/MDNSSearchProvider;->multicastLock:Landroid/net/wifi/WifiManager$MulticastLock;

    invoke-virtual {v2}, Landroid/net/wifi/WifiManager$MulticastLock;->acquire()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 308
    :catch_0
    move-exception v0

    .line 309
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {v0}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public static create(Landroid/content/Context;)Lcom/samsung/multiscreen/SearchProvider;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 152
    new-instance v0, Lcom/samsung/multiscreen/MDNSSearchProvider;

    invoke-direct {v0, p0}, Lcom/samsung/multiscreen/MDNSSearchProvider;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method static create(Landroid/content/Context;Lcom/samsung/multiscreen/Search$SearchListener;)Lcom/samsung/multiscreen/SearchProvider;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "searchListener"    # Lcom/samsung/multiscreen/Search$SearchListener;

    .prologue
    .line 157
    new-instance v0, Lcom/samsung/multiscreen/MDNSSearchProvider;

    invoke-direct {v0, p0, p1}, Lcom/samsung/multiscreen/MDNSSearchProvider;-><init>(Landroid/content/Context;Lcom/samsung/multiscreen/Search$SearchListener;)V

    return-object v0
.end method

.method private createJmDNS()Z
    .locals 6

    .prologue
    .line 251
    invoke-direct {p0}, Lcom/samsung/multiscreen/MDNSSearchProvider;->destroyJmDNS()Z

    .line 253
    const/4 v2, 0x0

    .line 256
    .local v2, "success":Z
    :try_start_0
    iget-object v3, p0, Lcom/samsung/multiscreen/MDNSSearchProvider;->context:Landroid/content/Context;

    invoke-static {v3}, Lcom/samsung/multiscreen/util/NetUtil;->getDeviceIpAddress(Landroid/content/Context;)Ljava/net/InetAddress;

    move-result-object v0

    .line 257
    .local v0, "deviceIpAddress":Ljava/net/InetAddress;
    invoke-static {v0}, Ljavax/jmdns/JmDNS;->create(Ljava/net/InetAddress;)Ljavax/jmdns/JmDNS;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/multiscreen/MDNSSearchProvider;->jmdns:Ljavax/jmdns/JmDNS;

    .line 258
    iget-object v3, p0, Lcom/samsung/multiscreen/MDNSSearchProvider;->jmdns:Ljavax/jmdns/JmDNS;

    const-string v4, "_samsungmsf._tcp.local."

    iget-object v5, p0, Lcom/samsung/multiscreen/MDNSSearchProvider;->serviceListener:Ljavax/jmdns/ServiceListener;

    invoke-virtual {v3, v4, v5}, Ljavax/jmdns/JmDNS;->addServiceListener(Ljava/lang/String;Ljavax/jmdns/ServiceListener;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 259
    const/4 v2, 0x1

    .line 264
    .end local v0    # "deviceIpAddress":Ljava/net/InetAddress;
    :goto_0
    return v2

    .line 260
    :catch_0
    move-exception v1

    .line 261
    .local v1, "e":Ljava/io/IOException;
    invoke-static {v1}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private declared-synchronized destroyJmDNS()Z
    .locals 5

    .prologue
    .line 268
    monitor-enter p0

    const/4 v1, 0x0

    .line 270
    .local v1, "success":Z
    :try_start_0
    iget-object v2, p0, Lcom/samsung/multiscreen/MDNSSearchProvider;->jmdns:Ljavax/jmdns/JmDNS;

    if-eqz v2, :cond_0

    .line 271
    iget-object v2, p0, Lcom/samsung/multiscreen/MDNSSearchProvider;->jmdns:Ljavax/jmdns/JmDNS;

    const-string v3, "_samsungmsf._tcp.local."

    iget-object v4, p0, Lcom/samsung/multiscreen/MDNSSearchProvider;->serviceListener:Ljavax/jmdns/ServiceListener;

    invoke-virtual {v2, v3, v4}, Ljavax/jmdns/JmDNS;->removeServiceListener(Ljava/lang/String;Ljavax/jmdns/ServiceListener;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 273
    :try_start_1
    iget-object v2, p0, Lcom/samsung/multiscreen/MDNSSearchProvider;->jmdns:Ljavax/jmdns/JmDNS;

    invoke-virtual {v2}, Ljavax/jmdns/JmDNS;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 274
    const/4 v1, 0x1

    .line 278
    :goto_0
    const/4 v2, 0x0

    :try_start_2
    iput-object v2, p0, Lcom/samsung/multiscreen/MDNSSearchProvider;->jmdns:Ljavax/jmdns/JmDNS;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 281
    :cond_0
    monitor-exit p0

    return v1

    .line 275
    :catch_0
    move-exception v0

    .line 276
    .local v0, "e":Ljava/io/IOException;
    :try_start_3
    invoke-static {v0}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 268
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method static getById(Landroid/content/Context;Ljava/lang/String;Lcom/samsung/multiscreen/Result;)Lcom/samsung/multiscreen/ProviderThread;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Lcom/samsung/multiscreen/Result",
            "<",
            "Lcom/samsung/multiscreen/Service;",
            ">;)",
            "Lcom/samsung/multiscreen/ProviderThread;"
        }
    .end annotation

    .prologue
    .line 162
    .local p2, "result":Lcom/samsung/multiscreen/Result;, "Lcom/samsung/multiscreen/Result<Lcom/samsung/multiscreen/Service;>;"
    new-instance v0, Lcom/samsung/multiscreen/MDNSSearchProvider$3;

    new-instance v1, Lcom/samsung/multiscreen/MDNSSearchProvider$2;

    invoke-direct {v1, p0, p2, p1}, Lcom/samsung/multiscreen/MDNSSearchProvider$2;-><init>(Landroid/content/Context;Lcom/samsung/multiscreen/Result;Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lcom/samsung/multiscreen/MDNSSearchProvider$3;-><init>(Ljava/lang/Runnable;)V

    .line 244
    .local v0, "thread":Lcom/samsung/multiscreen/ProviderThread;
    invoke-virtual {v0}, Lcom/samsung/multiscreen/ProviderThread;->start()V

    .line 246
    return-object v0
.end method

.method static getService(Ljavax/jmdns/JmDNS;Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/multiscreen/Service;
    .locals 9
    .param p0, "jmdns"    # Ljavax/jmdns/JmDNS;
    .param p1, "type"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 285
    const/4 v7, 0x2

    .local v7, "retry":I
    move v8, v7

    .line 287
    .end local v7    # "retry":I
    .local v8, "retry":I
    :goto_0
    add-int/lit8 v7, v8, -0x1

    .end local v8    # "retry":I
    .restart local v7    # "retry":I
    if-ltz v8, :cond_0

    .line 288
    const/4 v3, 0x0

    const-wide/16 v4, 0x1388

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-virtual/range {v0 .. v5}, Ljavax/jmdns/JmDNS;->getServiceInfo(Ljava/lang/String;Ljava/lang/String;ZJ)Ljavax/jmdns/ServiceInfo;

    move-result-object v6

    .line 289
    .local v6, "info":Ljavax/jmdns/ServiceInfo;
    if-eqz v6, :cond_1

    .line 290
    invoke-static {v6}, Lcom/samsung/multiscreen/Service;->create(Ljavax/jmdns/ServiceInfo;)Lcom/samsung/multiscreen/Service;

    move-result-object v0

    .line 294
    .end local v6    # "info":Ljavax/jmdns/ServiceInfo;
    :goto_1
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .restart local v6    # "info":Ljavax/jmdns/ServiceInfo;
    :cond_1
    move v8, v7

    .end local v7    # "retry":I
    .restart local v8    # "retry":I
    goto :goto_0
.end method

.method private releaseMulticastLock()Z
    .locals 3

    .prologue
    .line 317
    const/4 v1, 0x0

    .line 320
    .local v1, "success":Z
    :try_start_0
    iget-object v2, p0, Lcom/samsung/multiscreen/MDNSSearchProvider;->multicastLock:Landroid/net/wifi/WifiManager$MulticastLock;

    invoke-static {v2}, Lcom/samsung/multiscreen/util/NetUtil;->releaseMulticastLock(Landroid/net/wifi/WifiManager$MulticastLock;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 321
    const/4 v1, 0x1

    .line 326
    :goto_0
    return v1

    .line 322
    :catch_0
    move-exception v0

    .line 323
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {v0}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public start()V
    .locals 1

    .prologue
    .line 123
    iget-boolean v0, p0, Lcom/samsung/multiscreen/MDNSSearchProvider;->searching:Z

    if-eqz v0, :cond_0

    .line 124
    invoke-virtual {p0}, Lcom/samsung/multiscreen/MDNSSearchProvider;->stop()Z

    .line 127
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/multiscreen/MDNSSearchProvider;->clearServices()V

    .line 128
    invoke-direct {p0}, Lcom/samsung/multiscreen/MDNSSearchProvider;->acquireMulticastLock()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/samsung/multiscreen/MDNSSearchProvider;->createJmDNS()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/samsung/multiscreen/MDNSSearchProvider;->searching:Z

    .line 129
    return-void

    .line 128
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public stop()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 134
    iget-boolean v1, p0, Lcom/samsung/multiscreen/MDNSSearchProvider;->searching:Z

    if-nez v1, :cond_0

    .line 142
    :goto_0
    return v0

    .line 137
    :cond_0
    iput-boolean v0, p0, Lcom/samsung/multiscreen/MDNSSearchProvider;->searching:Z

    .line 139
    invoke-direct {p0}, Lcom/samsung/multiscreen/MDNSSearchProvider;->destroyJmDNS()Z

    .line 140
    invoke-direct {p0}, Lcom/samsung/multiscreen/MDNSSearchProvider;->releaseMulticastLock()Z

    .line 142
    const/4 v0, 0x1

    goto :goto_0
.end method
