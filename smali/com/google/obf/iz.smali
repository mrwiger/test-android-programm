.class final Lcom/google/obf/iz;
.super Ljava/lang/Object;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/obf/iz$a;
    }
.end annotation


# static fields
.field private static final a:Ljava/util/logging/Logger;

.field private static final b:Lcom/google/obf/iy;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 4
    const-class v0, Lcom/google/obf/iz;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/google/obf/iz;->a:Ljava/util/logging/Logger;

    .line 5
    invoke-static {}, Lcom/google/obf/iz;->a()Lcom/google/obf/iy;

    move-result-object v0

    sput-object v0, Lcom/google/obf/iz;->b:Lcom/google/obf/iy;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a()Lcom/google/obf/iy;
    .locals 2

    .prologue
    .line 3
    new-instance v0, Lcom/google/obf/iz$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/obf/iz$a;-><init>(Lcom/google/obf/iz$1;)V

    return-object v0
.end method

.method static a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 2
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
