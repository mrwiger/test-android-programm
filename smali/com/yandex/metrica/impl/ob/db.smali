.class public Lcom/yandex/metrica/impl/ob/db;
.super Lcom/yandex/metrica/impl/ob/de;
.source "SourceFile"


# instance fields
.field private final a:Lcom/yandex/metrica/impl/ob/fm;

.field private final b:Lcom/yandex/metrica/impl/ob/fm;

.field private final c:Lcom/yandex/metrica/impl/ob/fm;

.field private final d:Lcom/yandex/metrica/impl/ob/fm;


# direct methods
.method public constructor <init>(Lcom/yandex/metrica/impl/ob/cr;)V
    .locals 2

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/yandex/metrica/impl/ob/de;-><init>(Lcom/yandex/metrica/impl/ob/cr;)V

    .line 27
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "init_event_pref_key"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/db;->a:Lcom/yandex/metrica/impl/ob/fm;

    .line 28
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "first_event_pref_key"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/db;->b:Lcom/yandex/metrica/impl/ob/fm;

    .line 29
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "first_event_description_key"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/db;->c:Lcom/yandex/metrica/impl/ob/fm;

    .line 30
    new-instance v0, Lcom/yandex/metrica/impl/ob/fm;

    const-string v1, "preload_info_auto_tracking_enabled"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/fm;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/db;->d:Lcom/yandex/metrica/impl/ob/fm;

    .line 31
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/db;->a:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/yandex/metrica/impl/ob/db;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 34
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/db;->a:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "DONE"

    invoke-virtual {p0, v0, v1}, Lcom/yandex/metrica/impl/ob/db;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/de;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/de;->g()V

    .line 35
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/db;->d:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/yandex/metrica/impl/ob/db;->a(Ljava/lang/String;Z)Lcom/yandex/metrica/impl/ob/de;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/de;->g()V

    .line 71
    return-void
.end method

.method public b(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/db;->b:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/yandex/metrica/impl/ob/db;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 38
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/db;->b:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "DONE"

    invoke-virtual {p0, v0, v1}, Lcom/yandex/metrica/impl/ob/db;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/de;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/de;->g()V

    .line 39
    return-void
.end method

.method public b(Z)Z
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/db;->d:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/yandex/metrica/impl/ob/db;->b(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/db;->c:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/yandex/metrica/impl/ob/db;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/de;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/de;->g()V

    .line 59
    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/ob/db;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/db;->c:Lcom/yandex/metrica/impl/ob/fm;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/yandex/metrica/impl/ob/db;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/ob/db;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()V
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/db;->c:Lcom/yandex/metrica/impl/ob/fm;

    .line 1078
    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fm;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/yandex/metrica/impl/ob/db;->r(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/de;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/de;->g()V

    .line 67
    return-void
.end method
