.class public Lcom/flurry/sdk/no;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final b:Ljava/lang/String;

.field private static c:Lcom/flurry/sdk/no;


# instance fields
.field public a:J

.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/content/Context;",
            "Lcom/flurry/sdk/nm;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/flurry/sdk/np;

.field private final f:Ljava/lang/Object;

.field private g:Lcom/flurry/sdk/nm;

.field private h:Z

.field private i:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private j:Lcom/flurry/sdk/mh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/flurry/sdk/mh",
            "<",
            "Lcom/flurry/sdk/nq;",
            ">;"
        }
    .end annotation
.end field

.field private k:Lcom/flurry/sdk/mh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/flurry/sdk/mh",
            "<",
            "Lcom/flurry/sdk/mb;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/flurry/sdk/no;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/flurry/sdk/no;->b:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, Lcom/flurry/sdk/no;->d:Ljava/util/Map;

    .line 27
    new-instance v0, Lcom/flurry/sdk/np;

    invoke-direct {v0}, Lcom/flurry/sdk/np;-><init>()V

    iput-object v0, p0, Lcom/flurry/sdk/no;->e:Lcom/flurry/sdk/np;

    .line 28
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/flurry/sdk/no;->f:Ljava/lang/Object;

    .line 32
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/flurry/sdk/no;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 34
    new-instance v0, Lcom/flurry/sdk/no$1;

    invoke-direct {v0, p0}, Lcom/flurry/sdk/no$1;-><init>(Lcom/flurry/sdk/no;)V

    iput-object v0, p0, Lcom/flurry/sdk/no;->j:Lcom/flurry/sdk/mh;

    .line 42
    new-instance v0, Lcom/flurry/sdk/no$2;

    invoke-direct {v0, p0}, Lcom/flurry/sdk/no$2;-><init>(Lcom/flurry/sdk/no;)V

    iput-object v0, p0, Lcom/flurry/sdk/no;->k:Lcom/flurry/sdk/mh;

    .line 84
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/flurry/sdk/no;->a:J

    .line 86
    invoke-static {}, Lcom/flurry/sdk/mi;->a()Lcom/flurry/sdk/mi;

    move-result-object v0

    const-string v1, "com.flurry.android.sdk.ActivityLifecycleEvent"

    iget-object v2, p0, Lcom/flurry/sdk/no;->k:Lcom/flurry/sdk/mh;

    invoke-virtual {v0, v1, v2}, Lcom/flurry/sdk/mi;->a(Ljava/lang/String;Lcom/flurry/sdk/mh;)V

    .line 88
    invoke-static {}, Lcom/flurry/sdk/mi;->a()Lcom/flurry/sdk/mi;

    move-result-object v0

    const-string v1, "com.flurry.android.sdk.FlurrySessionTimerEvent"

    iget-object v2, p0, Lcom/flurry/sdk/no;->j:Lcom/flurry/sdk/mh;

    invoke-virtual {v0, v1, v2}, Lcom/flurry/sdk/mi;->a(Ljava/lang/String;Lcom/flurry/sdk/mh;)V

    .line 90
    return-void
.end method

.method public static declared-synchronized a()Lcom/flurry/sdk/no;
    .locals 2

    .prologue
    .line 93
    const-class v1, Lcom/flurry/sdk/no;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/flurry/sdk/no;->c:Lcom/flurry/sdk/no;

    if-nez v0, :cond_0

    .line 94
    new-instance v0, Lcom/flurry/sdk/no;

    invoke-direct {v0}, Lcom/flurry/sdk/no;-><init>()V

    sput-object v0, Lcom/flurry/sdk/no;->c:Lcom/flurry/sdk/no;

    .line 97
    :cond_0
    sget-object v0, Lcom/flurry/sdk/no;->c:Lcom/flurry/sdk/no;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 93
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(Lcom/flurry/sdk/no;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/flurry/sdk/no;->g()V

    return-void
.end method

.method static synthetic a(Lcom/flurry/sdk/no;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/flurry/sdk/no;->e(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic a(Lcom/flurry/sdk/no;Lcom/flurry/sdk/nm;)V
    .locals 4

    .prologue
    .line 22
    .line 2407
    iget-object v1, p0, Lcom/flurry/sdk/no;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 2408
    :try_start_0
    iget-object v0, p0, Lcom/flurry/sdk/no;->g:Lcom/flurry/sdk/nm;

    if-ne v0, p1, :cond_0

    .line 2409
    iget-object v0, p0, Lcom/flurry/sdk/no;->g:Lcom/flurry/sdk/nm;

    .line 3099
    invoke-static {}, Lcom/flurry/sdk/nr;->a()Lcom/flurry/sdk/nr;

    move-result-object v2

    .line 3100
    const-string v3, "ContinueSessionMillis"

    invoke-virtual {v2, v3, v0}, Lcom/flurry/sdk/ns;->b(Ljava/lang/String;Lcom/flurry/sdk/ns$a;)Z

    .line 3102
    sget v2, Lcom/flurry/sdk/nm$a;->a:I

    invoke-virtual {v0, v2}, Lcom/flurry/sdk/nm;->a(I)V

    .line 2410
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/flurry/sdk/no;->g:Lcom/flurry/sdk/nm;

    .line 2412
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static synthetic b(Lcom/flurry/sdk/no;)Z
    .locals 1

    .prologue
    .line 22
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/flurry/sdk/no;->h:Z

    return v0
.end method

.method private declared-synchronized c(Landroid/content/Context;Z)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 161
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/flurry/sdk/no;->e()Lcom/flurry/sdk/nm;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/flurry/sdk/no;->e()Lcom/flurry/sdk/nm;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flurry/sdk/nm;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p2, :cond_1

    .line 162
    iget-object v0, p0, Lcom/flurry/sdk/no;->e:Lcom/flurry/sdk/np;

    invoke-virtual {v0}, Lcom/flurry/sdk/np;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 163
    const/4 v0, 0x3

    sget-object v1, Lcom/flurry/sdk/no;->b:Ljava/lang/String;

    const-string v2, "A background session has already started. Not storing in context map because we use application context only."

    invoke-static {v0, v1, v2}, Lcom/flurry/sdk/mm;->a(ILjava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 278
    :goto_0
    monitor-exit p0

    return-void

    .line 166
    :cond_0
    const/4 v0, 0x3

    :try_start_1
    sget-object v3, Lcom/flurry/sdk/no;->b:Ljava/lang/String;

    const-string v4, "Returning from a paused background session."

    invoke-static {v0, v3, v4}, Lcom/flurry/sdk/mm;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 171
    :cond_1
    invoke-virtual {p0}, Lcom/flurry/sdk/no;->e()Lcom/flurry/sdk/nm;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/flurry/sdk/no;->e()Lcom/flurry/sdk/nm;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flurry/sdk/nm;->a()Z

    move-result v0

    if-nez v0, :cond_2

    if-eqz p2, :cond_2

    .line 172
    sget-object v0, Lcom/flurry/sdk/no;->b:Ljava/lang/String;

    const-string v1, "A Flurry background session can\'t be started while a foreground session is running."

    invoke-static {v0, v1}, Lcom/flurry/sdk/mm;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 161
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 178
    :cond_2
    :try_start_2
    invoke-virtual {p0}, Lcom/flurry/sdk/no;->e()Lcom/flurry/sdk/nm;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/flurry/sdk/no;->e()Lcom/flurry/sdk/nm;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flurry/sdk/nm;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    if-nez p2, :cond_3

    .line 179
    sget-object v0, Lcom/flurry/sdk/no;->b:Ljava/lang/String;

    const-string v1, "New session started while background session is running.  Ending background session, then will create foreground session."

    invoke-static {v0, v1}, Lcom/flurry/sdk/mm;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    iget-object v0, p0, Lcom/flurry/sdk/no;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 186
    invoke-static {}, Lcom/flurry/sdk/ly;->a()Lcom/flurry/sdk/ly;

    move-result-object v0

    .line 1098
    iget-object v0, v0, Lcom/flurry/sdk/ly;->a:Landroid/content/Context;

    .line 186
    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/flurry/sdk/no;->d(Landroid/content/Context;Z)V

    .line 189
    invoke-static {}, Lcom/flurry/sdk/ly;->a()Lcom/flurry/sdk/ly;

    move-result-object v0

    new-instance v1, Lcom/flurry/sdk/no$3;

    invoke-direct {v1, p0, p1}, Lcom/flurry/sdk/no$3;-><init>(Lcom/flurry/sdk/no;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/flurry/sdk/ly;->b(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 200
    :cond_3
    iget-object v0, p0, Lcom/flurry/sdk/no;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flurry/sdk/nm;

    .line 201
    if-eqz v0, :cond_5

    .line 202
    invoke-static {}, Lcom/flurry/sdk/mc;->a()Lcom/flurry/sdk/mc;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flurry/sdk/mc;->b()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 203
    const/4 v0, 0x3

    sget-object v1, Lcom/flurry/sdk/no;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Session already started with context:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/flurry/sdk/mm;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 205
    :cond_4
    sget-object v0, Lcom/flurry/sdk/no;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Session already started with context:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flurry/sdk/mm;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 211
    :cond_5
    iget-object v0, p0, Lcom/flurry/sdk/no;->e:Lcom/flurry/sdk/np;

    invoke-virtual {v0}, Lcom/flurry/sdk/np;->b()V

    .line 214
    invoke-virtual {p0}, Lcom/flurry/sdk/no;->e()Lcom/flurry/sdk/nm;

    move-result-object v0

    .line 217
    if-nez v0, :cond_8

    .line 221
    if-eqz p2, :cond_7

    .line 222
    new-instance v0, Lcom/flurry/sdk/nl;

    invoke-direct {v0}, Lcom/flurry/sdk/nl;-><init>()V

    .line 228
    :goto_1
    sget v2, Lcom/flurry/sdk/nm$a;->b:I

    invoke-virtual {v0, v2}, Lcom/flurry/sdk/nm;->a(I)V

    .line 230
    sget-object v2, Lcom/flurry/sdk/no;->b:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Flurry session started for context:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/flurry/sdk/mm;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    new-instance v2, Lcom/flurry/sdk/nn;

    invoke-direct {v2}, Lcom/flurry/sdk/nn;-><init>()V

    .line 233
    new-instance v3, Ljava/lang/ref/WeakReference;

    invoke-direct {v3, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v3, v2, Lcom/flurry/sdk/nn;->a:Ljava/lang/ref/WeakReference;

    .line 234
    iput-object v0, v2, Lcom/flurry/sdk/nn;->b:Lcom/flurry/sdk/nm;

    .line 235
    sget v3, Lcom/flurry/sdk/nn$a;->a:I

    iput v3, v2, Lcom/flurry/sdk/nn;->c:I

    .line 236
    invoke-virtual {v2}, Lcom/flurry/sdk/nn;->b()V

    move v5, v1

    move-object v1, v0

    move v0, v5

    .line 239
    :goto_2
    iget-object v2, p0, Lcom/flurry/sdk/no;->d:Ljava/util/Map;

    invoke-interface {v2, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1478
    iget-object v2, p0, Lcom/flurry/sdk/no;->f:Ljava/lang/Object;

    monitor-enter v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1479
    :try_start_3
    iput-object v1, p0, Lcom/flurry/sdk/no;->g:Lcom/flurry/sdk/nm;

    .line 1480
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 243
    :try_start_4
    iget-object v2, p0, Lcom/flurry/sdk/no;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 245
    sget-object v2, Lcom/flurry/sdk/no;->b:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Flurry session resumed for context:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/flurry/sdk/mm;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    new-instance v2, Lcom/flurry/sdk/nn;

    invoke-direct {v2}, Lcom/flurry/sdk/nn;-><init>()V

    .line 248
    new-instance v3, Ljava/lang/ref/WeakReference;

    invoke-direct {v3, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v3, v2, Lcom/flurry/sdk/nn;->a:Ljava/lang/ref/WeakReference;

    .line 249
    iput-object v1, v2, Lcom/flurry/sdk/nn;->b:Lcom/flurry/sdk/nm;

    .line 250
    sget v3, Lcom/flurry/sdk/nn$a;->c:I

    iput v3, v2, Lcom/flurry/sdk/nn;->c:I

    .line 251
    invoke-virtual {v2}, Lcom/flurry/sdk/nn;->b()V

    .line 253
    if-eqz v0, :cond_6

    .line 258
    invoke-static {}, Lcom/flurry/sdk/ly;->a()Lcom/flurry/sdk/ly;

    move-result-object v0

    new-instance v2, Lcom/flurry/sdk/no$4;

    invoke-direct {v2, p0, v1, p1}, Lcom/flurry/sdk/no$4;-><init>(Lcom/flurry/sdk/no;Lcom/flurry/sdk/nm;Landroid/content/Context;)V

    invoke-virtual {v0, v2}, Lcom/flurry/sdk/ly;->b(Ljava/lang/Runnable;)V

    .line 277
    :cond_6
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/flurry/sdk/no;->a:J

    goto/16 :goto_0

    .line 224
    :cond_7
    new-instance v0, Lcom/flurry/sdk/nm;

    invoke-direct {v0}, Lcom/flurry/sdk/nm;-><init>()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_1

    .line 1480
    :catchall_1
    move-exception v0

    :try_start_5
    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :try_start_6
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :cond_8
    move-object v1, v0

    move v0, v2

    goto :goto_2
.end method

.method private declared-synchronized d(Landroid/content/Context;Z)V
    .locals 4

    .prologue
    .line 317
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/flurry/sdk/no;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flurry/sdk/nm;

    .line 322
    if-eqz p2, :cond_0

    invoke-virtual {p0}, Lcom/flurry/sdk/no;->e()Lcom/flurry/sdk/nm;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/flurry/sdk/no;->e()Lcom/flurry/sdk/nm;

    move-result-object v1

    invoke-virtual {v1}, Lcom/flurry/sdk/nm;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/flurry/sdk/no;->e:Lcom/flurry/sdk/np;

    invoke-virtual {v1}, Lcom/flurry/sdk/np;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 323
    invoke-direct {p0}, Lcom/flurry/sdk/no;->g()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 359
    :goto_0
    monitor-exit p0

    return-void

    .line 327
    :cond_0
    if-nez v0, :cond_2

    .line 328
    :try_start_1
    invoke-static {}, Lcom/flurry/sdk/mc;->a()Lcom/flurry/sdk/mc;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flurry/sdk/mc;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 329
    const/4 v0, 0x3

    sget-object v1, Lcom/flurry/sdk/no;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Session cannot be ended, session not found for context:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/flurry/sdk/mm;->a(ILjava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 317
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 332
    :cond_1
    :try_start_2
    sget-object v0, Lcom/flurry/sdk/no;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Session cannot be ended, session not found for context:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flurry/sdk/mm;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 338
    :cond_2
    sget-object v1, Lcom/flurry/sdk/no;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Flurry session paused for context:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flurry/sdk/mm;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 340
    new-instance v1, Lcom/flurry/sdk/nn;

    invoke-direct {v1}, Lcom/flurry/sdk/nn;-><init>()V

    .line 341
    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v2, v1, Lcom/flurry/sdk/nn;->a:Ljava/lang/ref/WeakReference;

    .line 342
    iput-object v0, v1, Lcom/flurry/sdk/nn;->b:Lcom/flurry/sdk/nm;

    .line 343
    invoke-static {}, Lcom/flurry/sdk/lj;->a()Lcom/flurry/sdk/lj;

    invoke-static {}, Lcom/flurry/sdk/lj;->d()J

    move-result-wide v2

    iput-wide v2, v1, Lcom/flurry/sdk/nn;->d:J

    .line 344
    sget v2, Lcom/flurry/sdk/nn$a;->d:I

    iput v2, v1, Lcom/flurry/sdk/nn;->c:I

    .line 345
    invoke-virtual {v1}, Lcom/flurry/sdk/nn;->b()V

    .line 349
    invoke-direct {p0}, Lcom/flurry/sdk/no;->h()I

    move-result v1

    if-nez v1, :cond_4

    .line 350
    if-eqz p2, :cond_3

    .line 351
    invoke-direct {p0}, Lcom/flurry/sdk/no;->g()V

    .line 355
    :goto_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/flurry/sdk/no;->a:J

    goto :goto_0

    .line 353
    :cond_3
    iget-object v1, p0, Lcom/flurry/sdk/no;->e:Lcom/flurry/sdk/np;

    invoke-virtual {v0}, Lcom/flurry/sdk/nm;->b()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/flurry/sdk/np;->a(J)V

    goto :goto_1

    .line 357
    :cond_4
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/flurry/sdk/no;->a:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method

.method private declared-synchronized e(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 154
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-direct {p0, p1, v0}, Lcom/flurry/sdk/no;->c(Landroid/content/Context;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 155
    monitor-exit p0

    return-void

    .line 154
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/flurry/sdk/no;->b:Ljava/lang/String;

    return-object v0
.end method

.method private declared-synchronized g()V
    .locals 5

    .prologue
    .line 366
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/flurry/sdk/no;->h()I

    move-result v0

    .line 367
    if-lez v0, :cond_0

    .line 368
    const/4 v1, 0x5

    sget-object v2, Lcom/flurry/sdk/no;->b:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Session cannot be finalized, sessionContextCount:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/flurry/sdk/mm;->a(ILjava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 400
    :goto_0
    monitor-exit p0

    return-void

    .line 373
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lcom/flurry/sdk/no;->e()Lcom/flurry/sdk/nm;

    move-result-object v1

    .line 375
    if-nez v1, :cond_1

    .line 376
    const/4 v0, 0x5

    sget-object v1, Lcom/flurry/sdk/no;->b:Ljava/lang/String;

    const-string v2, "Session cannot be finalized, current session not found"

    invoke-static {v0, v1, v2}, Lcom/flurry/sdk/mm;->a(ILjava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 366
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 380
    :cond_1
    :try_start_2
    sget-object v2, Lcom/flurry/sdk/no;->b:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v0, "Flurry "

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/flurry/sdk/nm;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "background"

    :goto_1
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " session ended"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/flurry/sdk/mm;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 382
    new-instance v0, Lcom/flurry/sdk/nn;

    invoke-direct {v0}, Lcom/flurry/sdk/nn;-><init>()V

    .line 383
    iput-object v1, v0, Lcom/flurry/sdk/nn;->b:Lcom/flurry/sdk/nm;

    .line 384
    sget v2, Lcom/flurry/sdk/nn$a;->e:I

    iput v2, v0, Lcom/flurry/sdk/nn;->c:I

    .line 385
    invoke-static {}, Lcom/flurry/sdk/lj;->a()Lcom/flurry/sdk/lj;

    invoke-static {}, Lcom/flurry/sdk/lj;->d()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/flurry/sdk/nn;->d:J

    .line 386
    invoke-virtual {v0}, Lcom/flurry/sdk/nn;->b()V

    .line 390
    invoke-static {}, Lcom/flurry/sdk/ly;->a()Lcom/flurry/sdk/ly;

    move-result-object v0

    new-instance v2, Lcom/flurry/sdk/no$5;

    invoke-direct {v2, p0, v1}, Lcom/flurry/sdk/no$5;-><init>(Lcom/flurry/sdk/no;Lcom/flurry/sdk/nm;)V

    invoke-virtual {v0, v2}, Lcom/flurry/sdk/ly;->b(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 380
    :cond_2
    const-string v0, ""
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method private declared-synchronized h()I
    .locals 1

    .prologue
    .line 438
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/flurry/sdk/no;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 116
    monitor-enter p0

    :try_start_0
    instance-of v0, p1, Landroid/app/Activity;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 128
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 121
    :cond_1
    :try_start_1
    invoke-static {}, Lcom/flurry/sdk/mc;->a()Lcom/flurry/sdk/mc;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flurry/sdk/mc;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 126
    const/4 v0, 0x3

    sget-object v1, Lcom/flurry/sdk/no;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "bootstrap for context:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/flurry/sdk/mm;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 127
    invoke-direct {p0, p1}, Lcom/flurry/sdk/no;->e(Landroid/content/Context;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 116
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Landroid/content/Context;Z)V
    .locals 4

    .prologue
    .line 138
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/flurry/sdk/mc;->a()Lcom/flurry/sdk/mc;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flurry/sdk/mc;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    instance-of v0, p1, Landroid/app/Activity;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 151
    :goto_0
    monitor-exit p0

    return-void

    .line 149
    :cond_0
    const/4 v0, 0x3

    :try_start_1
    sget-object v1, Lcom/flurry/sdk/no;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Manual onStartSession for context:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/flurry/sdk/mm;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 150
    invoke-direct {p0, p1, p2}, Lcom/flurry/sdk/no;->c(Landroid/content/Context;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 138
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 6

    .prologue
    .line 417
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/flurry/sdk/no;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    .line 418
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 419
    new-instance v2, Lcom/flurry/sdk/nn;

    invoke-direct {v2}, Lcom/flurry/sdk/nn;-><init>()V

    .line 420
    new-instance v3, Ljava/lang/ref/WeakReference;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v3, v2, Lcom/flurry/sdk/nn;->a:Ljava/lang/ref/WeakReference;

    .line 421
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/flurry/sdk/nm;

    iput-object v0, v2, Lcom/flurry/sdk/nn;->b:Lcom/flurry/sdk/nm;

    .line 422
    sget v0, Lcom/flurry/sdk/nn$a;->d:I

    iput v0, v2, Lcom/flurry/sdk/nn;->c:I

    .line 423
    invoke-static {}, Lcom/flurry/sdk/lj;->a()Lcom/flurry/sdk/lj;

    invoke-static {}, Lcom/flurry/sdk/lj;->d()J

    move-result-wide v4

    iput-wide v4, v2, Lcom/flurry/sdk/nn;->d:J

    .line 424
    invoke-virtual {v2}, Lcom/flurry/sdk/nn;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 417
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 426
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/flurry/sdk/no;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 429
    invoke-static {}, Lcom/flurry/sdk/ly;->a()Lcom/flurry/sdk/ly;

    move-result-object v0

    new-instance v1, Lcom/flurry/sdk/no$6;

    invoke-direct {v1, p0}, Lcom/flurry/sdk/no$6;-><init>(Lcom/flurry/sdk/no;)V

    invoke-virtual {v0, v1}, Lcom/flurry/sdk/ly;->b(Ljava/lang/Runnable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 435
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized b(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 131
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1, v0}, Lcom/flurry/sdk/no;->a(Landroid/content/Context;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 132
    monitor-exit p0

    return-void

    .line 131
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Landroid/content/Context;Z)V
    .locals 4

    .prologue
    .line 291
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/flurry/sdk/mc;->a()Lcom/flurry/sdk/mc;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flurry/sdk/mc;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    instance-of v0, p1, Landroid/app/Activity;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    .line 309
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 296
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lcom/flurry/sdk/no;->e()Lcom/flurry/sdk/nm;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/flurry/sdk/no;->e()Lcom/flurry/sdk/nm;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flurry/sdk/nm;->a()Z

    move-result v0

    if-nez v0, :cond_2

    if-eqz p2, :cond_2

    .line 298
    sget-object v0, Lcom/flurry/sdk/no;->b:Ljava/lang/String;

    const-string v1, "No background session running, can\'t end session."

    invoke-static {v0, v1}, Lcom/flurry/sdk/mm;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 291
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 302
    :cond_2
    if-eqz p2, :cond_3

    :try_start_2
    iget-boolean v0, p0, Lcom/flurry/sdk/no;->h:Z

    if-nez v0, :cond_0

    .line 307
    :cond_3
    const/4 v0, 0x3

    sget-object v1, Lcom/flurry/sdk/no;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Manual onEndSession for context:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/flurry/sdk/mm;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 308
    invoke-virtual {p0, p1}, Lcom/flurry/sdk/no;->d(Landroid/content/Context;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public final declared-synchronized c(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 285
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1, v0}, Lcom/flurry/sdk/no;->b(Landroid/content/Context;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 286
    monitor-exit p0

    return-void

    .line 285
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()Z
    .locals 3

    .prologue
    .line 443
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/flurry/sdk/no;->e()Lcom/flurry/sdk/nm;

    move-result-object v0

    .line 444
    if-nez v0, :cond_0

    .line 445
    const/4 v0, 0x2

    sget-object v1, Lcom/flurry/sdk/no;->b:Ljava/lang/String;

    const-string v2, "Session not found. No active session"

    invoke-static {v0, v1, v2}, Lcom/flurry/sdk/mm;->a(ILjava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 446
    const/4 v0, 0x0

    .line 448
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    .line 443
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()I
    .locals 3

    .prologue
    .line 456
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/flurry/sdk/no;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 457
    sget v0, Lcom/flurry/sdk/nm$a;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 468
    :goto_0
    monitor-exit p0

    return v0

    .line 461
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lcom/flurry/sdk/no;->e()Lcom/flurry/sdk/nm;

    move-result-object v0

    .line 463
    if-nez v0, :cond_1

    .line 464
    const/4 v0, 0x2

    sget-object v1, Lcom/flurry/sdk/no;->b:Ljava/lang/String;

    const-string v2, "Session not found. No active session"

    invoke-static {v0, v1, v2}, Lcom/flurry/sdk/mm;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 465
    sget v0, Lcom/flurry/sdk/nm$a;->a:I

    goto :goto_0

    .line 468
    :cond_1
    invoke-virtual {v0}, Lcom/flurry/sdk/nm;->c()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    .line 456
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized d(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 312
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-direct {p0, p1, v0}, Lcom/flurry/sdk/no;->d(Landroid/content/Context;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 313
    monitor-exit p0

    return-void

    .line 312
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final e()Lcom/flurry/sdk/nm;
    .locals 2

    .prologue
    .line 472
    iget-object v1, p0, Lcom/flurry/sdk/no;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 473
    :try_start_0
    iget-object v0, p0, Lcom/flurry/sdk/no;->g:Lcom/flurry/sdk/nm;

    monitor-exit v1

    return-object v0

    .line 474
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
