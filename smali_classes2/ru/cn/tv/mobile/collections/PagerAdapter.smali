.class Lru/cn/tv/mobile/collections/PagerAdapter;
.super Landroid/support/v4/app/FragmentStatePagerAdapter;
.source "PagerAdapter.java"


# instance fields
.field private controller:Lru/cn/tv/mobile/collections/RubricFragmentController;


# direct methods
.method constructor <init>(Landroid/support/v4/app/FragmentManager;Lru/cn/tv/mobile/collections/RubricFragmentController;)V
    .locals 0
    .param p1, "fm"    # Landroid/support/v4/app/FragmentManager;
    .param p2, "controller"    # Lru/cn/tv/mobile/collections/RubricFragmentController;

    .prologue
    .line 12
    invoke-direct {p0, p1}, Landroid/support/v4/app/FragmentStatePagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    .line 13
    iput-object p2, p0, Lru/cn/tv/mobile/collections/PagerAdapter;->controller:Lru/cn/tv/mobile/collections/RubricFragmentController;

    .line 14
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lru/cn/tv/mobile/collections/PagerAdapter;->controller:Lru/cn/tv/mobile/collections/RubricFragmentController;

    if-nez v0, :cond_0

    .line 27
    const/4 v0, 0x0

    .line 29
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lru/cn/tv/mobile/collections/PagerAdapter;->controller:Lru/cn/tv/mobile/collections/RubricFragmentController;

    invoke-interface {v0}, Lru/cn/tv/mobile/collections/RubricFragmentController;->rubricCount()I

    move-result v0

    goto :goto_0
.end method

.method public getItem(I)Landroid/support/v4/app/Fragment;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 18
    iget-object v0, p0, Lru/cn/tv/mobile/collections/PagerAdapter;->controller:Lru/cn/tv/mobile/collections/RubricFragmentController;

    if-nez v0, :cond_0

    .line 19
    const/4 v0, 0x0

    .line 21
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lru/cn/tv/mobile/collections/PagerAdapter;->controller:Lru/cn/tv/mobile/collections/RubricFragmentController;

    invoke-interface {v0, p1}, Lru/cn/tv/mobile/collections/RubricFragmentController;->createRubricFragment(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    goto :goto_0
.end method

.method public getPageTitle(I)Ljava/lang/CharSequence;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 34
    iget-object v0, p0, Lru/cn/tv/mobile/collections/PagerAdapter;->controller:Lru/cn/tv/mobile/collections/RubricFragmentController;

    if-nez v0, :cond_0

    .line 35
    const/4 v0, 0x0

    .line 37
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lru/cn/tv/mobile/collections/PagerAdapter;->controller:Lru/cn/tv/mobile/collections/RubricFragmentController;

    invoke-interface {v0, p1}, Lru/cn/tv/mobile/collections/RubricFragmentController;->getTitle(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
