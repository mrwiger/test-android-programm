.class Lru/cn/domain/stores/iabhelper/IabHelper$2;
.super Ljava/lang/Object;
.source "IabHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/domain/stores/iabhelper/IabHelper;->queryInventoryAsync(ZLjava/util/List;Lru/cn/domain/stores/iabhelper/IabHelper$QueryInventoryFinishedListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/domain/stores/iabhelper/IabHelper;

.field final synthetic val$handler:Landroid/os/Handler;

.field final synthetic val$listener:Lru/cn/domain/stores/iabhelper/IabHelper$QueryInventoryFinishedListener;

.field final synthetic val$moreSkus:Ljava/util/List;

.field final synthetic val$querySkuDetails:Z


# direct methods
.method constructor <init>(Lru/cn/domain/stores/iabhelper/IabHelper;ZLjava/util/List;Lru/cn/domain/stores/iabhelper/IabHelper$QueryInventoryFinishedListener;Landroid/os/Handler;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/domain/stores/iabhelper/IabHelper;

    .prologue
    .line 608
    iput-object p1, p0, Lru/cn/domain/stores/iabhelper/IabHelper$2;->this$0:Lru/cn/domain/stores/iabhelper/IabHelper;

    iput-boolean p2, p0, Lru/cn/domain/stores/iabhelper/IabHelper$2;->val$querySkuDetails:Z

    iput-object p3, p0, Lru/cn/domain/stores/iabhelper/IabHelper$2;->val$moreSkus:Ljava/util/List;

    iput-object p4, p0, Lru/cn/domain/stores/iabhelper/IabHelper$2;->val$listener:Lru/cn/domain/stores/iabhelper/IabHelper$QueryInventoryFinishedListener;

    iput-object p5, p0, Lru/cn/domain/stores/iabhelper/IabHelper$2;->val$handler:Landroid/os/Handler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 610
    new-instance v3, Lru/cn/domain/stores/iabhelper/IabResult;

    const/4 v5, 0x0

    const-string v6, "Inventory refresh successful."

    invoke-direct {v3, v5, v6}, Lru/cn/domain/stores/iabhelper/IabResult;-><init>(ILjava/lang/String;)V

    .line 611
    .local v3, "result":Lru/cn/domain/stores/iabhelper/IabResult;
    const/4 v1, 0x0

    .line 613
    .local v1, "inv":Lru/cn/domain/stores/iabhelper/Inventory;
    :try_start_0
    iget-object v5, p0, Lru/cn/domain/stores/iabhelper/IabHelper$2;->this$0:Lru/cn/domain/stores/iabhelper/IabHelper;

    iget-boolean v6, p0, Lru/cn/domain/stores/iabhelper/IabHelper$2;->val$querySkuDetails:Z

    iget-object v7, p0, Lru/cn/domain/stores/iabhelper/IabHelper$2;->val$moreSkus:Ljava/util/List;

    invoke-virtual {v5, v6, v7}, Lru/cn/domain/stores/iabhelper/IabHelper;->queryInventory(ZLjava/util/List;)Lru/cn/domain/stores/iabhelper/Inventory;
    :try_end_0
    .catch Lru/cn/domain/stores/iabhelper/IabException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 618
    :goto_0
    iget-object v5, p0, Lru/cn/domain/stores/iabhelper/IabHelper$2;->this$0:Lru/cn/domain/stores/iabhelper/IabHelper;

    invoke-virtual {v5}, Lru/cn/domain/stores/iabhelper/IabHelper;->flagEndAsync()V

    .line 620
    move-object v4, v3

    .line 621
    .local v4, "result_f":Lru/cn/domain/stores/iabhelper/IabResult;
    move-object v2, v1

    .line 622
    .local v2, "inv_f":Lru/cn/domain/stores/iabhelper/Inventory;
    iget-object v5, p0, Lru/cn/domain/stores/iabhelper/IabHelper$2;->this$0:Lru/cn/domain/stores/iabhelper/IabHelper;

    iget-boolean v5, v5, Lru/cn/domain/stores/iabhelper/IabHelper;->mDisposed:Z

    if-nez v5, :cond_0

    iget-object v5, p0, Lru/cn/domain/stores/iabhelper/IabHelper$2;->val$listener:Lru/cn/domain/stores/iabhelper/IabHelper$QueryInventoryFinishedListener;

    if-eqz v5, :cond_0

    .line 623
    iget-object v5, p0, Lru/cn/domain/stores/iabhelper/IabHelper$2;->val$handler:Landroid/os/Handler;

    new-instance v6, Lru/cn/domain/stores/iabhelper/IabHelper$2$1;

    invoke-direct {v6, p0, v4, v2}, Lru/cn/domain/stores/iabhelper/IabHelper$2$1;-><init>(Lru/cn/domain/stores/iabhelper/IabHelper$2;Lru/cn/domain/stores/iabhelper/IabResult;Lru/cn/domain/stores/iabhelper/Inventory;)V

    invoke-virtual {v5, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 629
    :cond_0
    return-void

    .line 614
    .end local v2    # "inv_f":Lru/cn/domain/stores/iabhelper/Inventory;
    .end local v4    # "result_f":Lru/cn/domain/stores/iabhelper/IabResult;
    :catch_0
    move-exception v0

    .line 615
    .local v0, "ex":Lru/cn/domain/stores/iabhelper/IabException;
    invoke-virtual {v0}, Lru/cn/domain/stores/iabhelper/IabException;->getResult()Lru/cn/domain/stores/iabhelper/IabResult;

    move-result-object v3

    goto :goto_0
.end method
