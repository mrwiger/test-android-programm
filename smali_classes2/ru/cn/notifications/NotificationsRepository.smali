.class public Lru/cn/notifications/NotificationsRepository;
.super Ljava/lang/Object;
.source "NotificationsRepository.java"

# interfaces
.implements Lru/cn/notifications/INotificationsRepository;


# instance fields
.field private final LOAD_NOTIFICATION:I

.field private final content:Lio/reactivex/subjects/BehaviorSubject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/reactivex/subjects/BehaviorSubject",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mapper:Lru/cn/notifications/mapper/NotificationMapper;

.field private final observable:Landroid/database/ContentObserver;

.field private final resolver:Landroid/content/ContentResolver;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput v3, p0, Lru/cn/notifications/NotificationsRepository;->LOAD_NOTIFICATION:I

    .line 24
    invoke-static {}, Lio/reactivex/subjects/BehaviorSubject;->create()Lio/reactivex/subjects/BehaviorSubject;

    move-result-object v0

    iput-object v0, p0, Lru/cn/notifications/NotificationsRepository;->content:Lio/reactivex/subjects/BehaviorSubject;

    .line 25
    new-instance v0, Lru/cn/notifications/mapper/NotificationMapper;

    invoke-direct {v0}, Lru/cn/notifications/mapper/NotificationMapper;-><init>()V

    iput-object v0, p0, Lru/cn/notifications/NotificationsRepository;->mapper:Lru/cn/notifications/mapper/NotificationMapper;

    .line 27
    new-instance v0, Lru/cn/notifications/NotificationsRepository$1;

    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {v0, p0, v1}, Lru/cn/notifications/NotificationsRepository$1;-><init>(Lru/cn/notifications/NotificationsRepository;Landroid/os/Handler;)V

    iput-object v0, p0, Lru/cn/notifications/NotificationsRepository;->observable:Landroid/database/ContentObserver;

    .line 36
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lru/cn/notifications/NotificationsRepository;->resolver:Landroid/content/ContentResolver;

    .line 37
    iget-object v0, p0, Lru/cn/notifications/NotificationsRepository;->content:Lio/reactivex/subjects/BehaviorSubject;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/subjects/BehaviorSubject;->onNext(Ljava/lang/Object;)V

    .line 38
    return-void
.end method

.method static synthetic access$000(Lru/cn/notifications/NotificationsRepository;)Lio/reactivex/subjects/BehaviorSubject;
    .locals 1
    .param p0, "x0"    # Lru/cn/notifications/NotificationsRepository;

    .prologue
    .line 21
    iget-object v0, p0, Lru/cn/notifications/NotificationsRepository;->content:Lio/reactivex/subjects/BehaviorSubject;

    return-object v0
.end method

.method static final synthetic lambda$delete$4$NotificationsRepository(Ljava/lang/Throwable;)Ljava/lang/Integer;
    .locals 1
    .param p0, "it"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 52
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method static final synthetic lambda$load$8$NotificationsRepository(Ljava/lang/Throwable;)Ljava/util/List;
    .locals 1
    .param p0, "it"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 68
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static final synthetic lambda$update$6$NotificationsRepository(Ljava/lang/Throwable;)Ljava/lang/Integer;
    .locals 1
    .param p0, "it"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 60
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method private load()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Ljava/util/List",
            "<",
            "Lru/cn/notifications/entity/INotification;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 65
    new-instance v0, Lru/cn/notifications/NotificationsRepository$$Lambda$7;

    invoke-direct {v0, p0}, Lru/cn/notifications/NotificationsRepository$$Lambda$7;-><init>(Lru/cn/notifications/NotificationsRepository;)V

    invoke-static {v0}, Lio/reactivex/Observable;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/Observable;

    move-result-object v0

    iget-object v1, p0, Lru/cn/notifications/NotificationsRepository;->mapper:Lru/cn/notifications/mapper/NotificationMapper;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v1}, Lru/cn/notifications/NotificationsRepository$$Lambda$8;->get$Lambda(Lru/cn/notifications/mapper/NotificationMapper;)Lio/reactivex/functions/Function;

    move-result-object v1

    .line 67
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    sget-object v1, Lru/cn/notifications/NotificationsRepository$$Lambda$9;->$instance:Lio/reactivex/functions/Function;

    .line 68
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->onErrorReturn(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 69
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Observable;

    move-result-object v0

    .line 65
    return-object v0
.end method


# virtual methods
.method public delete(Lru/cn/notifications/entity/INotification;)Lio/reactivex/Single;
    .locals 2
    .param p1, "notification"    # Lru/cn/notifications/entity/INotification;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lru/cn/notifications/entity/INotification;",
            ")",
            "Lio/reactivex/Single",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 50
    invoke-static {p1}, Lio/reactivex/Single;->just(Ljava/lang/Object;)Lio/reactivex/Single;

    move-result-object v0

    new-instance v1, Lru/cn/notifications/NotificationsRepository$$Lambda$3;

    invoke-direct {v1, p0}, Lru/cn/notifications/NotificationsRepository$$Lambda$3;-><init>(Lru/cn/notifications/NotificationsRepository;)V

    .line 51
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->map(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    sget-object v1, Lru/cn/notifications/NotificationsRepository$$Lambda$4;->$instance:Lio/reactivex/functions/Function;

    .line 52
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->onErrorReturn(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    .line 53
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v0

    .line 50
    return-object v0
.end method

.method final synthetic lambda$delete$3$NotificationsRepository(Lru/cn/notifications/entity/INotification;)Ljava/lang/Integer;
    .locals 8
    .param p1, "it"    # Lru/cn/notifications/entity/INotification;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 51
    iget-object v0, p0, Lru/cn/notifications/NotificationsRepository;->resolver:Landroid/content/ContentResolver;

    invoke-static {}, Lru/cn/notifications/NotificationContract;->notifications()Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-interface {p1}, Lru/cn/notifications/entity/INotification;->message()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-interface {p1}, Lru/cn/notifications/entity/INotification;->createTime()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method final synthetic lambda$load$7$NotificationsRepository()Landroid/database/Cursor;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 66
    iget-object v0, p0, Lru/cn/notifications/NotificationsRepository;->resolver:Landroid/content/ContentResolver;

    invoke-static {}, Lru/cn/notifications/NotificationContract;->notifications()Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method final synthetic lambda$notifications$0$NotificationsRepository(Lio/reactivex/disposables/Disposable;)V
    .locals 4
    .param p1, "it"    # Lio/reactivex/disposables/Disposable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 43
    iget-object v0, p0, Lru/cn/notifications/NotificationsRepository;->resolver:Landroid/content/ContentResolver;

    invoke-static {}, Lru/cn/notifications/NotificationContract;->notifications()Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    iget-object v3, p0, Lru/cn/notifications/NotificationsRepository;->observable:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    return-void
.end method

.method final synthetic lambda$notifications$1$NotificationsRepository()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, Lru/cn/notifications/NotificationsRepository;->resolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lru/cn/notifications/NotificationsRepository;->observable:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    return-void
.end method

.method final synthetic lambda$notifications$2$NotificationsRepository(Ljava/lang/Integer;)Lio/reactivex/ObservableSource;
    .locals 1
    .param p1, "it"    # Ljava/lang/Integer;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 45
    invoke-direct {p0}, Lru/cn/notifications/NotificationsRepository;->load()Lio/reactivex/Observable;

    move-result-object v0

    return-object v0
.end method

.method final synthetic lambda$update$5$NotificationsRepository()Ljava/lang/Integer;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 59
    iget-object v0, p0, Lru/cn/notifications/NotificationsRepository;->resolver:Landroid/content/ContentResolver;

    invoke-static {}, Lru/cn/notifications/NotificationContract;->notifications()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v2, v2, v2}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public notifications()Lio/reactivex/Observable;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Observable",
            "<",
            "Ljava/util/List",
            "<",
            "Lru/cn/notifications/entity/INotification;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 42
    iget-object v0, p0, Lru/cn/notifications/NotificationsRepository;->content:Lio/reactivex/subjects/BehaviorSubject;

    new-instance v1, Lru/cn/notifications/NotificationsRepository$$Lambda$0;

    invoke-direct {v1, p0}, Lru/cn/notifications/NotificationsRepository$$Lambda$0;-><init>(Lru/cn/notifications/NotificationsRepository;)V

    .line 43
    invoke-virtual {v0, v1}, Lio/reactivex/subjects/BehaviorSubject;->doOnSubscribe(Lio/reactivex/functions/Consumer;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lru/cn/notifications/NotificationsRepository$$Lambda$1;

    invoke-direct {v1, p0}, Lru/cn/notifications/NotificationsRepository$$Lambda$1;-><init>(Lru/cn/notifications/NotificationsRepository;)V

    .line 44
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->doOnDispose(Lio/reactivex/functions/Action;)Lio/reactivex/Observable;

    move-result-object v0

    new-instance v1, Lru/cn/notifications/NotificationsRepository$$Lambda$2;

    invoke-direct {v1, p0}, Lru/cn/notifications/NotificationsRepository$$Lambda$2;-><init>(Lru/cn/notifications/NotificationsRepository;)V

    .line 45
    invoke-virtual {v0, v1}, Lio/reactivex/Observable;->flatMap(Lio/reactivex/functions/Function;)Lio/reactivex/Observable;

    move-result-object v0

    .line 42
    return-object v0
.end method

.method public update()Lio/reactivex/Single;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lio/reactivex/Single",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 58
    new-instance v0, Lru/cn/notifications/NotificationsRepository$$Lambda$5;

    invoke-direct {v0, p0}, Lru/cn/notifications/NotificationsRepository$$Lambda$5;-><init>(Lru/cn/notifications/NotificationsRepository;)V

    invoke-static {v0}, Lio/reactivex/Single;->fromCallable(Ljava/util/concurrent/Callable;)Lio/reactivex/Single;

    move-result-object v0

    sget-object v1, Lru/cn/notifications/NotificationsRepository$$Lambda$6;->$instance:Lio/reactivex/functions/Function;

    .line 60
    invoke-virtual {v0, v1}, Lio/reactivex/Single;->onErrorReturn(Lio/reactivex/functions/Function;)Lio/reactivex/Single;

    move-result-object v0

    .line 61
    invoke-static {}, Lio/reactivex/schedulers/Schedulers;->io()Lio/reactivex/Scheduler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lio/reactivex/Single;->observeOn(Lio/reactivex/Scheduler;)Lio/reactivex/Single;

    move-result-object v0

    .line 58
    return-object v0
.end method
