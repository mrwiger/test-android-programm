.class Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerMessageListener;
.super Ljava/lang/Object;
.source "PhotoPlayer.java"

# interfaces
.implements Lcom/samsung/multiscreen/Channel$OnMessageListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/multiscreen/PhotoPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OnPhotoPlayerMessageListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/multiscreen/PhotoPlayer;


# direct methods
.method private constructor <init>(Lcom/samsung/multiscreen/PhotoPlayer;)V
    .locals 0

    .prologue
    .line 380
    iput-object p1, p0, Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/PhotoPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/multiscreen/PhotoPlayer;Lcom/samsung/multiscreen/PhotoPlayer$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/multiscreen/PhotoPlayer;
    .param p2, "x1"    # Lcom/samsung/multiscreen/PhotoPlayer$1;

    .prologue
    .line 380
    invoke-direct {p0, p1}, Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerMessageListener;-><init>(Lcom/samsung/multiscreen/PhotoPlayer;)V

    return-void
.end method

.method private handlePlayerControlResponse(Ljava/lang/String;)V
    .locals 4
    .param p1, "response"    # Ljava/lang/String;

    .prologue
    .line 497
    :try_start_0
    sget-object v1, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->play:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    invoke-virtual {v1}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 498
    iget-object v1, p0, Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/PhotoPlayer;

    invoke-static {v1}, Lcom/samsung/multiscreen/PhotoPlayer;->access$100(Lcom/samsung/multiscreen/PhotoPlayer;)Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerListener;->onPlay()V

    .line 511
    :cond_0
    :goto_0
    return-void

    .line 499
    :cond_1
    sget-object v1, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->pause:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    invoke-virtual {v1}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 500
    iget-object v1, p0, Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/PhotoPlayer;

    invoke-static {v1}, Lcom/samsung/multiscreen/PhotoPlayer;->access$100(Lcom/samsung/multiscreen/PhotoPlayer;)Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerListener;->onPause()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 508
    :catch_0
    move-exception v0

    .line 509
    .local v0, "e":Ljava/lang/Exception;
    iget-object v1, p0, Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/PhotoPlayer;

    invoke-virtual {v1}, Lcom/samsung/multiscreen/PhotoPlayer;->isDebug()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "PhotoPlayer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error while parsing control response : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 501
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_2
    :try_start_1
    sget-object v1, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->stop:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    invoke-virtual {v1}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 502
    iget-object v1, p0, Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/PhotoPlayer;

    invoke-static {v1}, Lcom/samsung/multiscreen/PhotoPlayer;->access$100(Lcom/samsung/multiscreen/PhotoPlayer;)Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerListener;->onStop()V

    goto :goto_0

    .line 503
    :cond_3
    sget-object v1, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->next:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    invoke-virtual {v1}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 504
    iget-object v1, p0, Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/PhotoPlayer;

    invoke-static {v1}, Lcom/samsung/multiscreen/PhotoPlayer;->access$100(Lcom/samsung/multiscreen/PhotoPlayer;)Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerListener;->onNext()V

    goto :goto_0

    .line 505
    :cond_4
    sget-object v1, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->previous:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    invoke-virtual {v1}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 506
    iget-object v1, p0, Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/PhotoPlayer;

    invoke-static {v1}, Lcom/samsung/multiscreen/PhotoPlayer;->access$100(Lcom/samsung/multiscreen/PhotoPlayer;)Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerListener;->onPrevious()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private handlePlayerQueueEventResponse(Ljava/lang/String;)V
    .locals 6
    .param p1, "response"    # Ljava/lang/String;

    .prologue
    .line 515
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 518
    .local v2, "jsonObject":Lorg/json/JSONObject;
    const-string v3, "subEvent"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 519
    .local v1, "event":Ljava/lang/String;
    const-string v3, "subEvent"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->remove(Ljava/lang/String;)Ljava/lang/Object;

    .line 521
    if-nez v1, :cond_1

    .line 522
    iget-object v3, p0, Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/PhotoPlayer;

    invoke-virtual {v3}, Lcom/samsung/multiscreen/PhotoPlayer;->isDebug()Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "PhotoPlayer"

    const-string v4, "Sub-Event key missing from message."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 539
    .end local v1    # "event":Ljava/lang/String;
    .end local v2    # "jsonObject":Lorg/json/JSONObject;
    :cond_0
    :goto_0
    return-void

    .line 526
    .restart local v1    # "event":Ljava/lang/String;
    .restart local v2    # "jsonObject":Lorg/json/JSONObject;
    :cond_1
    sget-object v3, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;->enqueue:Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;

    invoke-virtual {v3}, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 527
    iget-object v3, p0, Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/PhotoPlayer;

    invoke-static {v3}, Lcom/samsung/multiscreen/PhotoPlayer;->access$100(Lcom/samsung/multiscreen/PhotoPlayer;)Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerListener;

    move-result-object v3

    invoke-interface {v3, v2}, Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerListener;->onAddToList(Lorg/json/JSONObject;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 536
    .end local v1    # "event":Ljava/lang/String;
    .end local v2    # "jsonObject":Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    .line 537
    .local v0, "e":Ljava/lang/Exception;
    iget-object v3, p0, Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/PhotoPlayer;

    invoke-virtual {v3}, Lcom/samsung/multiscreen/PhotoPlayer;->isDebug()Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "PhotoPlayer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Error while parsing list Event response : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 528
    .end local v0    # "e":Ljava/lang/Exception;
    .restart local v1    # "event":Ljava/lang/String;
    .restart local v2    # "jsonObject":Lorg/json/JSONObject;
    :cond_2
    :try_start_1
    sget-object v3, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;->dequeue:Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;

    invoke-virtual {v3}, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 529
    iget-object v3, p0, Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/PhotoPlayer;

    invoke-static {v3}, Lcom/samsung/multiscreen/PhotoPlayer;->access$100(Lcom/samsung/multiscreen/PhotoPlayer;)Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerListener;

    move-result-object v3

    invoke-interface {v3, v2}, Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerListener;->onRemoveFromList(Lorg/json/JSONObject;)V

    goto :goto_0

    .line 530
    :cond_3
    sget-object v3, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;->clear:Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;

    invoke-virtual {v3}, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 531
    iget-object v3, p0, Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/PhotoPlayer;

    invoke-static {v3}, Lcom/samsung/multiscreen/PhotoPlayer;->access$100(Lcom/samsung/multiscreen/PhotoPlayer;)Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerListener;

    move-result-object v3

    invoke-interface {v3}, Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerListener;->onClearList()V

    goto :goto_0

    .line 532
    :cond_4
    sget-object v3, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;->fetch:Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;

    invoke-virtual {v3}, Lcom/samsung/multiscreen/Player$PlayerQueueSubEvents;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string v3, "data"

    .line 533
    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 534
    iget-object v3, p0, Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/PhotoPlayer;

    invoke-static {v3}, Lcom/samsung/multiscreen/PhotoPlayer;->access$100(Lcom/samsung/multiscreen/PhotoPlayer;)Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerListener;

    move-result-object v3

    const-string v4, "data"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerListener;->onGetList(Lorg/json/JSONArray;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method


# virtual methods
.method public onMessage(Lcom/samsung/multiscreen/Message;)V
    .locals 20
    .param p1, "message"    # Lcom/samsung/multiscreen/Message;

    .prologue
    .line 387
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/PhotoPlayer;

    invoke-virtual {v15}, Lcom/samsung/multiscreen/PhotoPlayer;->isDebug()Z

    move-result v15

    if-eqz v15, :cond_0

    const-string v15, "PhotoPlayer"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "onMessage : "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, v16

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 389
    :cond_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/PhotoPlayer;

    invoke-static {v15}, Lcom/samsung/multiscreen/PhotoPlayer;->access$100(Lcom/samsung/multiscreen/PhotoPlayer;)Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerListener;

    move-result-object v15

    if-nez v15, :cond_2

    .line 390
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/PhotoPlayer;

    invoke-virtual {v15}, Lcom/samsung/multiscreen/PhotoPlayer;->isDebug()Z

    move-result v15

    if-eqz v15, :cond_1

    const-string v15, "PhotoPlayer"

    const-string v16, "Unregistered PlayerListener."

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 493
    :cond_1
    :goto_0
    return-void

    .line 395
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/multiscreen/Message;->getEvent()Ljava/lang/String;

    move-result-object v15

    const-string v16, "playerNotice"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_3

    .line 396
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/PhotoPlayer;

    invoke-virtual {v15}, Lcom/samsung/multiscreen/PhotoPlayer;->isDebug()Z

    move-result v15

    if-eqz v15, :cond_1

    const-string v15, "PhotoPlayer"

    const-string v16, "In-Valid Player Message"

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 403
    :cond_3
    :try_start_0
    new-instance v16, Lorg/json/JSONTokener;

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/multiscreen/Message;->getData()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-direct {v0, v15}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v16 .. v16}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lorg/json/JSONObject;

    .line 405
    .local v11, "response":Lorg/json/JSONObject;
    if-nez v11, :cond_4

    .line 406
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/PhotoPlayer;

    invoke-virtual {v15}, Lcom/samsung/multiscreen/PhotoPlayer;->isDebug()Z

    move-result v15

    if-eqz v15, :cond_1

    const-string v15, "PhotoPlayer"

    const-string v16, "NULL Response - Unable to parse JSON string."

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 490
    .end local v11    # "response":Lorg/json/JSONObject;
    :catch_0
    move-exception v3

    .line 491
    .local v3, "e":Ljava/lang/Exception;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/PhotoPlayer;

    invoke-virtual {v15}, Lcom/samsung/multiscreen/PhotoPlayer;->isDebug()Z

    move-result v15

    if-eqz v15, :cond_1

    const-string v15, "PhotoPlayer"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "Error while parsing response : "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual {v3}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 415
    .end local v3    # "e":Ljava/lang/Exception;
    .restart local v11    # "response":Lorg/json/JSONObject;
    :cond_4
    :try_start_1
    const-string v15, "subEvent"

    invoke-virtual {v11, v15}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_7

    .line 416
    const-string v15, "subEvent"

    invoke-virtual {v11, v15}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 417
    .local v12, "subEventData":Ljava/lang/String;
    const-string v15, "playerReady"

    invoke-virtual {v12, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_6

    .line 419
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/PhotoPlayer;

    iget-object v15, v15, Lcom/samsung/multiscreen/PhotoPlayer;->mAdditionalData:Lorg/json/JSONObject;

    if-eqz v15, :cond_5

    .line 420
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/PhotoPlayer;

    iget-object v15, v15, Lcom/samsung/multiscreen/PhotoPlayer;->mAdditionalData:Lorg/json/JSONObject;

    const-string v16, "playerType"

    sget-object v17, Lcom/samsung/multiscreen/Player$ContentType;->photo:Lcom/samsung/multiscreen/Player$ContentType;

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/multiscreen/Player$ContentType;->name()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v15 .. v17}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 421
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/PhotoPlayer;

    iget-object v15, v15, Lcom/samsung/multiscreen/PhotoPlayer;->mAdditionalData:Lorg/json/JSONObject;

    const-string v16, "subEvent"

    sget-object v17, Lcom/samsung/multiscreen/Player$PlayerContentSubEvents;->ADDITIONALMEDIAINFO:Lcom/samsung/multiscreen/Player$PlayerContentSubEvents;

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/multiscreen/Player$PlayerContentSubEvents;->name()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v15 .. v17}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 422
    sget-object v15, Lcom/samsung/multiscreen/Player;->mApplication:Lcom/samsung/multiscreen/Application;

    const-string v16, "playerContentChange"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/PhotoPlayer;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/samsung/multiscreen/PhotoPlayer;->mAdditionalData:Lorg/json/JSONObject;

    move-object/from16 v17, v0

    invoke-virtual/range {v15 .. v17}, Lcom/samsung/multiscreen/Application;->publish(Ljava/lang/String;Ljava/lang/Object;)V

    .line 425
    :cond_5
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/PhotoPlayer;

    invoke-static {v15}, Lcom/samsung/multiscreen/PhotoPlayer;->access$100(Lcom/samsung/multiscreen/PhotoPlayer;)Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerListener;

    move-result-object v15

    invoke-interface {v15}, Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerListener;->onPlayerInitialized()V

    goto/16 :goto_0

    .line 430
    :cond_6
    const-string v15, "playerChange"

    invoke-virtual {v12, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_1

    .line 435
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/PhotoPlayer;

    invoke-static {v15}, Lcom/samsung/multiscreen/PhotoPlayer;->access$100(Lcom/samsung/multiscreen/PhotoPlayer;)Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerListener;

    move-result-object v15

    sget-object v16, Lcom/samsung/multiscreen/Player$ContentType;->photo:Lcom/samsung/multiscreen/Player$ContentType;

    invoke-virtual/range {v16 .. v16}, Lcom/samsung/multiscreen/Player$ContentType;->name()Ljava/lang/String;

    move-result-object v16

    invoke-interface/range {v15 .. v16}, Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerListener;->onPlayerChange(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 437
    .end local v12    # "subEventData":Ljava/lang/String;
    :cond_7
    const-string v15, "playerType"

    invoke-virtual {v11, v15}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_b

    .line 438
    const-string v15, "playerType"

    invoke-virtual {v11, v15}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 439
    .local v10, "playerType":Ljava/lang/String;
    sget-object v15, Lcom/samsung/multiscreen/Player$ContentType;->photo:Lcom/samsung/multiscreen/Player$ContentType;

    invoke-virtual {v15}, Lcom/samsung/multiscreen/Player$ContentType;->name()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v10, v15}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_1

    .line 441
    const-string v15, "state"

    invoke-virtual {v11, v15}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_8

    .line 442
    const-string v15, "state"

    invoke-virtual {v11, v15}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerMessageListener;->handlePlayerControlResponse(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 443
    :cond_8
    const-string v15, "queue"

    invoke-virtual {v11, v15}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_9

    .line 444
    const-string v15, "queue"

    invoke-virtual {v11, v15}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerMessageListener;->handlePlayerQueueEventResponse(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 445
    :cond_9
    const-string v15, "currentPlaying"

    invoke-virtual {v11, v15}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_a

    .line 446
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/PhotoPlayer;

    invoke-static {v15}, Lcom/samsung/multiscreen/PhotoPlayer;->access$100(Lcom/samsung/multiscreen/PhotoPlayer;)Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerListener;

    move-result-object v15

    const-string v16, "currentPlaying"

    move-object/from16 v0, v16

    invoke-virtual {v11, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-interface {v15, v0, v10}, Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerListener;->onCurrentPlaying(Lorg/json/JSONObject;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 447
    :cond_a
    const-string v15, "error"

    invoke-virtual {v11, v15}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_1

    .line 448
    const-string v15, "error"

    invoke-virtual {v11, v15}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v5

    .line 450
    .local v5, "errorMessage":Ljava/lang/String;
    :try_start_2
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    .line 451
    .local v6, "errorValue":I
    new-instance v4, Lcom/samsung/multiscreen/ErrorCode;

    invoke-direct {v4, v6}, Lcom/samsung/multiscreen/ErrorCode;-><init>(I)V

    .line 452
    .local v4, "errorCode":Lcom/samsung/multiscreen/ErrorCode;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/PhotoPlayer;

    invoke-static {v15}, Lcom/samsung/multiscreen/PhotoPlayer;->access$100(Lcom/samsung/multiscreen/PhotoPlayer;)Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerListener;

    move-result-object v15

    invoke-virtual {v4}, Lcom/samsung/multiscreen/ErrorCode;->value()I

    move-result v16

    move/from16 v0, v16

    int-to-long v0, v0

    move-wide/from16 v16, v0

    invoke-virtual {v4}, Lcom/samsung/multiscreen/ErrorCode;->name()Ljava/lang/String;

    move-result-object v18

    invoke-virtual {v4}, Lcom/samsung/multiscreen/ErrorCode;->name()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v16 .. v19}, Lcom/samsung/multiscreen/Error;->create(JLjava/lang/String;Ljava/lang/String;)Lcom/samsung/multiscreen/Error;

    move-result-object v16

    invoke-interface/range {v15 .. v16}, Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerListener;->onError(Lcom/samsung/multiscreen/Error;)V
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_0

    .line 453
    .end local v4    # "errorCode":Lcom/samsung/multiscreen/ErrorCode;
    .end local v6    # "errorValue":I
    :catch_1
    move-exception v3

    .line 454
    .local v3, "e":Ljava/lang/NumberFormatException;
    :try_start_3
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/PhotoPlayer;

    invoke-static {v15}, Lcom/samsung/multiscreen/PhotoPlayer;->access$100(Lcom/samsung/multiscreen/PhotoPlayer;)Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerListener;

    move-result-object v15

    invoke-static {v5}, Lcom/samsung/multiscreen/Error;->create(Ljava/lang/String;)Lcom/samsung/multiscreen/Error;

    move-result-object v16

    invoke-interface/range {v15 .. v16}, Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerListener;->onError(Lcom/samsung/multiscreen/Error;)V

    goto/16 :goto_0

    .line 459
    .end local v3    # "e":Ljava/lang/NumberFormatException;
    .end local v5    # "errorMessage":Ljava/lang/String;
    .end local v10    # "playerType":Ljava/lang/String;
    :cond_b
    const-string v15, "state"

    invoke-virtual {v11, v15}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_11

    .line 460
    invoke-virtual {v11}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    .line 461
    .local v2, "data":Ljava/lang/String;
    sget-object v15, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->getControlStatus:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    invoke-virtual {v15}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->name()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v2, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_e

    .line 462
    new-instance v15, Lorg/json/JSONTokener;

    invoke-direct {v15, v2}, Lorg/json/JSONTokener;-><init>(Ljava/lang/String;)V

    invoke-virtual {v15}, Lorg/json/JSONTokener;->nextValue()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/json/JSONObject;

    .line 463
    .local v8, "jsonData":Lorg/json/JSONObject;
    const/4 v13, 0x0

    .line 464
    .local v13, "volumeLevel":I
    const/4 v15, 0x0

    invoke-static {v15}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    .line 465
    .local v9, "muteStatus":Ljava/lang/Boolean;
    sget-object v15, Lcom/samsung/multiscreen/Player$PlayerControlStatus;->volume:Lcom/samsung/multiscreen/Player$PlayerControlStatus;

    invoke-virtual {v15}, Lcom/samsung/multiscreen/Player$PlayerControlStatus;->name()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v8, v15}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_c

    .line 466
    sget-object v15, Lcom/samsung/multiscreen/Player$PlayerControlStatus;->volume:Lcom/samsung/multiscreen/Player$PlayerControlStatus;

    invoke-virtual {v15}, Lcom/samsung/multiscreen/Player$PlayerControlStatus;->name()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v8, v15}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v13

    .line 468
    :cond_c
    sget-object v15, Lcom/samsung/multiscreen/Player$PlayerControlStatus;->mute:Lcom/samsung/multiscreen/Player$PlayerControlStatus;

    invoke-virtual {v15}, Lcom/samsung/multiscreen/Player$PlayerControlStatus;->name()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v8, v15}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_d

    .line 469
    sget-object v15, Lcom/samsung/multiscreen/Player$PlayerControlStatus;->mute:Lcom/samsung/multiscreen/Player$PlayerControlStatus;

    invoke-virtual {v15}, Lcom/samsung/multiscreen/Player$PlayerControlStatus;->name()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v8, v15}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v15

    invoke-static {v15}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    .line 471
    :cond_d
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/PhotoPlayer;

    invoke-static {v15}, Lcom/samsung/multiscreen/PhotoPlayer;->access$100(Lcom/samsung/multiscreen/PhotoPlayer;)Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerListener;

    move-result-object v15

    invoke-interface {v15, v13, v9}, Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerListener;->onControlStatus(ILjava/lang/Boolean;)V

    goto/16 :goto_0

    .line 472
    .end local v8    # "jsonData":Lorg/json/JSONObject;
    .end local v9    # "muteStatus":Ljava/lang/Boolean;
    .end local v13    # "volumeLevel":I
    :cond_e
    sget-object v15, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->mute:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    invoke-virtual {v15}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->name()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v2, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_f

    .line 473
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/PhotoPlayer;

    invoke-static {v15}, Lcom/samsung/multiscreen/PhotoPlayer;->access$100(Lcom/samsung/multiscreen/PhotoPlayer;)Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerListener;

    move-result-object v15

    invoke-interface {v15}, Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerListener;->onMute()V

    goto/16 :goto_0

    .line 474
    :cond_f
    sget-object v15, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->unMute:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    invoke-virtual {v15}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->name()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v2, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_10

    .line 475
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/PhotoPlayer;

    invoke-static {v15}, Lcom/samsung/multiscreen/PhotoPlayer;->access$100(Lcom/samsung/multiscreen/PhotoPlayer;)Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerListener;

    move-result-object v15

    invoke-interface {v15}, Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerListener;->onUnMute()V

    goto/16 :goto_0

    .line 476
    :cond_10
    sget-object v15, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->getVolume:Lcom/samsung/multiscreen/Player$PlayerControlEvents;

    invoke-virtual {v15}, Lcom/samsung/multiscreen/Player$PlayerControlEvents;->name()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v2, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_1

    .line 477
    const-string v15, ":"

    invoke-virtual {v2, v15}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v7

    .line 478
    .local v7, "index":I
    add-int/lit8 v15, v7, 0x1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v16

    add-int/lit8 v16, v16, -0x2

    move/from16 v0, v16

    invoke-virtual {v2, v15, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v14

    .line 479
    .local v14, "volumeStr":Ljava/lang/String;
    invoke-static {v14}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v13

    .line 480
    .restart local v13    # "volumeLevel":I
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/PhotoPlayer;

    invoke-static {v15}, Lcom/samsung/multiscreen/PhotoPlayer;->access$100(Lcom/samsung/multiscreen/PhotoPlayer;)Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerListener;

    move-result-object v15

    invoke-interface {v15, v13}, Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerListener;->onVolumeChange(I)V

    goto/16 :goto_0

    .line 482
    .end local v2    # "data":Ljava/lang/String;
    .end local v7    # "index":I
    .end local v13    # "volumeLevel":I
    .end local v14    # "volumeStr":Ljava/lang/String;
    :cond_11
    const-string v15, "appStatus"

    invoke-virtual {v11, v15}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_1

    .line 483
    invoke-virtual {v11}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    .line 484
    .restart local v2    # "data":Ljava/lang/String;
    sget-object v15, Lcom/samsung/multiscreen/Player$PlayerApplicationStatusEvents;->suspend:Lcom/samsung/multiscreen/Player$PlayerApplicationStatusEvents;

    invoke-virtual {v15}, Lcom/samsung/multiscreen/Player$PlayerApplicationStatusEvents;->name()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v2, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_12

    .line 485
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/PhotoPlayer;

    invoke-static {v15}, Lcom/samsung/multiscreen/PhotoPlayer;->access$100(Lcom/samsung/multiscreen/PhotoPlayer;)Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerListener;

    move-result-object v15

    invoke-interface {v15}, Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerListener;->onApplicationSuspend()V

    goto/16 :goto_0

    .line 486
    :cond_12
    sget-object v15, Lcom/samsung/multiscreen/Player$PlayerApplicationStatusEvents;->resume:Lcom/samsung/multiscreen/Player$PlayerApplicationStatusEvents;

    invoke-virtual {v15}, Lcom/samsung/multiscreen/Player$PlayerApplicationStatusEvents;->name()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v2, v15}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v15

    if-eqz v15, :cond_1

    .line 487
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerMessageListener;->this$0:Lcom/samsung/multiscreen/PhotoPlayer;

    invoke-static {v15}, Lcom/samsung/multiscreen/PhotoPlayer;->access$100(Lcom/samsung/multiscreen/PhotoPlayer;)Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerListener;

    move-result-object v15

    invoke-interface {v15}, Lcom/samsung/multiscreen/PhotoPlayer$OnPhotoPlayerListener;->onApplicationResume()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_0
.end method
