.class public Lcom/yandex/metrica/impl/ob/fs;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/yandex/metrica/impl/ob/fs$a;,
        Lcom/yandex/metrica/impl/ob/fs$c;,
        Lcom/yandex/metrica/impl/ob/fs$b;
    }
.end annotation


# instance fields
.field private final a:Landroid/content/ServiceConnection;

.field private final b:Landroid/os/Handler;

.field private c:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/yandex/metrica/impl/ob/fs$c;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Landroid/content/Context;

.field private e:Z

.field private f:Ljava/net/ServerSocket;

.field private final g:Lcom/yandex/metrica/impl/ob/ft;

.field private h:Lcom/yandex/metrica/impl/ob/fv;

.field private i:Ljava/lang/Thread;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 239
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 138
    new-instance v0, Lcom/yandex/metrica/impl/ob/fs$1;

    invoke-direct {v0}, Lcom/yandex/metrica/impl/ob/fs$1;-><init>()V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/fs;->a:Landroid/content/ServiceConnection;

    .line 149
    new-instance v0, Lcom/yandex/metrica/impl/ob/fs$2;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/yandex/metrica/impl/ob/fs$2;-><init>(Lcom/yandex/metrica/impl/ob/fs;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/fs;->b:Landroid/os/Handler;

    .line 216
    new-instance v0, Lcom/yandex/metrica/impl/ob/fs$3;

    invoke-direct {v0, p0}, Lcom/yandex/metrica/impl/ob/fs$3;-><init>(Lcom/yandex/metrica/impl/ob/fs;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/fs;->c:Ljava/util/HashMap;

    .line 233
    new-instance v0, Lcom/yandex/metrica/impl/ob/ft;

    invoke-direct {v0}, Lcom/yandex/metrica/impl/ob/ft;-><init>()V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/fs;->g:Lcom/yandex/metrica/impl/ob/ft;

    .line 240
    iput-object p1, p0, Lcom/yandex/metrica/impl/ob/fs;->d:Landroid/content/Context;

    .line 1245
    invoke-static {}, Lcom/yandex/metrica/impl/ob/g;->a()Lcom/yandex/metrica/impl/ob/g;

    move-result-object v0

    const-class v1, Lcom/yandex/metrica/impl/ob/r;

    new-instance v2, Lcom/yandex/metrica/impl/ob/fs$5;

    invoke-direct {v2, p0}, Lcom/yandex/metrica/impl/ob/fs$5;-><init>(Lcom/yandex/metrica/impl/ob/fs;)V

    invoke-static {v2}, Lcom/yandex/metrica/impl/ob/k;->a(Lcom/yandex/metrica/impl/ob/j;)Lcom/yandex/metrica/impl/ob/k$a;

    move-result-object v2

    new-instance v3, Lcom/yandex/metrica/impl/ob/fs$4;

    invoke-direct {v3, p0}, Lcom/yandex/metrica/impl/ob/fs$4;-><init>(Lcom/yandex/metrica/impl/ob/fs;)V

    .line 1249
    invoke-virtual {v2, v3}, Lcom/yandex/metrica/impl/ob/k$a;->a(Lcom/yandex/metrica/impl/ob/h;)Lcom/yandex/metrica/impl/ob/k$a;

    move-result-object v2

    .line 1253
    invoke-virtual {v2}, Lcom/yandex/metrica/impl/ob/k$a;->a()Lcom/yandex/metrica/impl/ob/k;

    move-result-object v2

    .line 1245
    invoke-virtual {v0, p0, v1, v2}, Lcom/yandex/metrica/impl/ob/g;->a(Ljava/lang/Object;Ljava/lang/Class;Lcom/yandex/metrica/impl/ob/k;)V

    .line 1254
    invoke-static {}, Lcom/yandex/metrica/impl/ob/g;->a()Lcom/yandex/metrica/impl/ob/g;

    move-result-object v0

    const-class v1, Lcom/yandex/metrica/impl/ob/n;

    new-instance v2, Lcom/yandex/metrica/impl/ob/fs$6;

    invoke-direct {v2, p0}, Lcom/yandex/metrica/impl/ob/fs$6;-><init>(Lcom/yandex/metrica/impl/ob/fs;)V

    invoke-static {v2}, Lcom/yandex/metrica/impl/ob/k;->a(Lcom/yandex/metrica/impl/ob/j;)Lcom/yandex/metrica/impl/ob/k$a;

    move-result-object v2

    .line 1258
    invoke-virtual {v2}, Lcom/yandex/metrica/impl/ob/k$a;->a()Lcom/yandex/metrica/impl/ob/k;

    move-result-object v2

    .line 1254
    invoke-virtual {v0, p0, v1, v2}, Lcom/yandex/metrica/impl/ob/g;->a(Ljava/lang/Object;Ljava/lang/Class;Lcom/yandex/metrica/impl/ob/k;)V

    .line 1259
    invoke-static {}, Lcom/yandex/metrica/impl/ob/g;->a()Lcom/yandex/metrica/impl/ob/g;

    move-result-object v0

    const-class v1, Lcom/yandex/metrica/impl/ob/l;

    new-instance v2, Lcom/yandex/metrica/impl/ob/fs$7;

    invoke-direct {v2, p0}, Lcom/yandex/metrica/impl/ob/fs$7;-><init>(Lcom/yandex/metrica/impl/ob/fs;)V

    invoke-static {v2}, Lcom/yandex/metrica/impl/ob/k;->a(Lcom/yandex/metrica/impl/ob/j;)Lcom/yandex/metrica/impl/ob/k$a;

    move-result-object v2

    .line 1263
    invoke-virtual {v2}, Lcom/yandex/metrica/impl/ob/k$a;->a()Lcom/yandex/metrica/impl/ob/k;

    move-result-object v2

    .line 1259
    invoke-virtual {v0, p0, v1, v2}, Lcom/yandex/metrica/impl/ob/g;->a(Ljava/lang/Object;Ljava/lang/Class;Lcom/yandex/metrica/impl/ob/k;)V

    .line 1264
    invoke-static {}, Lcom/yandex/metrica/impl/ob/g;->a()Lcom/yandex/metrica/impl/ob/g;

    move-result-object v0

    const-class v1, Lcom/yandex/metrica/impl/ob/m;

    new-instance v2, Lcom/yandex/metrica/impl/ob/fs$8;

    invoke-direct {v2, p0}, Lcom/yandex/metrica/impl/ob/fs$8;-><init>(Lcom/yandex/metrica/impl/ob/fs;)V

    invoke-static {v2}, Lcom/yandex/metrica/impl/ob/k;->a(Lcom/yandex/metrica/impl/ob/j;)Lcom/yandex/metrica/impl/ob/k$a;

    move-result-object v2

    .line 1268
    invoke-virtual {v2}, Lcom/yandex/metrica/impl/ob/k$a;->a()Lcom/yandex/metrica/impl/ob/k;

    move-result-object v2

    .line 1264
    invoke-virtual {v0, p0, v1, v2}, Lcom/yandex/metrica/impl/ob/g;->a(Ljava/lang/Object;Ljava/lang/Class;Lcom/yandex/metrica/impl/ob/k;)V

    .line 1269
    invoke-static {}, Lcom/yandex/metrica/impl/ob/g;->a()Lcom/yandex/metrica/impl/ob/g;

    move-result-object v0

    const-class v1, Lcom/yandex/metrica/impl/ob/p;

    new-instance v2, Lcom/yandex/metrica/impl/ob/fs$9;

    invoke-direct {v2, p0}, Lcom/yandex/metrica/impl/ob/fs$9;-><init>(Lcom/yandex/metrica/impl/ob/fs;)V

    invoke-static {v2}, Lcom/yandex/metrica/impl/ob/k;->a(Lcom/yandex/metrica/impl/ob/j;)Lcom/yandex/metrica/impl/ob/k$a;

    move-result-object v2

    .line 1274
    invoke-virtual {v2}, Lcom/yandex/metrica/impl/ob/k$a;->a()Lcom/yandex/metrica/impl/ob/k;

    move-result-object v2

    .line 1269
    invoke-virtual {v0, p0, v1, v2}, Lcom/yandex/metrica/impl/ob/g;->a(Ljava/lang/Object;Ljava/lang/Class;Lcom/yandex/metrica/impl/ob/k;)V

    .line 242
    return-void
.end method

.method static synthetic a(Lcom/yandex/metrica/impl/ob/fs;)Landroid/content/ServiceConnection;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fs;->a:Landroid/content/ServiceConnection;

    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 456
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 457
    const-string v1, "uri"

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 458
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fs;->d:Landroid/content/Context;

    .line 6022
    const-string v2, "20799a27-fa80-4b36-b2db-0f8141f24180"

    invoke-static {v1, v2}, Lcom/yandex/metrica/YandexMetrica;->getReporter(Landroid/content/Context;Ljava/lang/String;)Lcom/yandex/metrica/IReporter;

    move-result-object v1

    .line 458
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "socket_"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Lcom/yandex/metrica/IReporter;->reportEvent(Ljava/lang/String;Ljava/util/Map;)V

    .line 459
    return-void
.end method

.method static synthetic b(Lcom/yandex/metrica/impl/ob/fs;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fs;->d:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic b(I)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 59
    invoke-static {p0}, Lcom/yandex/metrica/impl/ob/fs;->c(I)Ljava/util/HashMap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Lcom/yandex/metrica/impl/ob/fs;)Lcom/yandex/metrica/impl/ob/fv;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fs;->h:Lcom/yandex/metrica/impl/ob/fv;

    return-object v0
.end method

.method private static c(I)Ljava/util/HashMap;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 405
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 406
    const-string v1, "port"

    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 407
    return-object v0
.end method

.method static synthetic d(Lcom/yandex/metrica/impl/ob/fs;)Lcom/yandex/metrica/impl/ob/ft;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fs;->g:Lcom/yandex/metrica/impl/ob/ft;

    return-object v0
.end method


# virtual methods
.method a(I)Ljava/net/ServerSocket;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 401
    new-instance v0, Ljava/net/ServerSocket;

    invoke-direct {v0, p1}, Ljava/net/ServerSocket;-><init>(I)V

    return-object v0
.end method

.method public a()V
    .locals 6

    .prologue
    .line 278
    iget-boolean v0, p0, Lcom/yandex/metrica/impl/ob/fs;->e:Z

    if-eqz v0, :cond_0

    .line 279
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/fs;->b()V

    .line 281
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fs;->b:Landroid/os/Handler;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fs;->b:Landroid/os/Handler;

    const/16 v2, 0x64

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v3, p0, Lcom/yandex/metrica/impl/ob/fs;->h:Lcom/yandex/metrica/impl/ob/fv;

    .line 282
    invoke-virtual {v3}, Lcom/yandex/metrica/impl/ob/fv;->a()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    .line 281
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 284
    :cond_0
    return-void
.end method

.method a(Lcom/yandex/metrica/impl/ob/fv;)V
    .locals 0

    .prologue
    .line 308
    iput-object p1, p0, Lcom/yandex/metrica/impl/ob/fs;->h:Lcom/yandex/metrica/impl/ob/fv;

    .line 309
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 288
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fs;->b:Landroid/os/Handler;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 289
    return-void
.end method

.method public declared-synchronized c()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 297
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/yandex/metrica/impl/ob/fs;->e:Z

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fs;->h:Lcom/yandex/metrica/impl/ob/fv;

    if-eqz v0, :cond_0

    .line 299
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/yandex/metrica/impl/ob/fs;->e:Z

    .line 300
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/fs;->d()V

    .line 301
    const-string v0, "YMM-IB"

    invoke-static {v0, p0}, Lcom/yandex/metrica/impl/utils/j;->a(Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Thread;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/fs;->i:Ljava/lang/Thread;

    .line 302
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fs;->i:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 304
    :cond_0
    monitor-exit p0

    return-void

    .line 297
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method d()V
    .locals 4

    .prologue
    .line 313
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fs;->d:Landroid/content/Context;

    const-class v2, Lcom/yandex/metrica/MetricaService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 314
    const-string v1, "com.yandex.metrica.ACTION_BIND_TO_LOCAL_SERVER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 316
    :try_start_0
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fs;->d:Landroid/content/Context;

    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/fs;->a:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    .line 317
    if-nez v0, :cond_0

    .line 318
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fs;->d:Landroid/content/Context;

    .line 2022
    const-string v1, "20799a27-fa80-4b36-b2db-0f8141f24180"

    invoke-static {v0, v1}, Lcom/yandex/metrica/YandexMetrica;->getReporter(Landroid/content/Context;Ljava/lang/String;)Lcom/yandex/metrica/IReporter;

    move-result-object v0

    .line 318
    const-string v1, "socket_bind_has_failed"

    invoke-interface {v0, v1}, Lcom/yandex/metrica/IReporter;->reportEvent(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 324
    :cond_0
    :goto_0
    return-void

    .line 322
    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fs;->d:Landroid/content/Context;

    .line 3022
    const-string v1, "20799a27-fa80-4b36-b2db-0f8141f24180"

    invoke-static {v0, v1}, Lcom/yandex/metrica/YandexMetrica;->getReporter(Landroid/content/Context;Ljava/lang/String;)Lcom/yandex/metrica/IReporter;

    move-result-object v0

    .line 322
    const-string v1, "socket_bind_has_thrown_exception"

    invoke-interface {v0, v1}, Lcom/yandex/metrica/IReporter;->reportEvent(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public declared-synchronized e()V
    .locals 1

    .prologue
    .line 329
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/yandex/metrica/impl/ob/fs;->e:Z

    .line 330
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fs;->i:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    .line 331
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fs;->i:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 332
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/fs;->i:Ljava/lang/Thread;

    .line 334
    :cond_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fs;->f:Ljava/net/ServerSocket;

    if-eqz v0, :cond_1

    .line 335
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fs;->f:Ljava/net/ServerSocket;

    invoke-virtual {v0}, Ljava/net/ServerSocket;->close()V

    .line 336
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/fs;->f:Ljava/net/ServerSocket;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 341
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 329
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 341
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method f()Ljava/net/ServerSocket;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 375
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/fs;->h:Lcom/yandex/metrica/impl/ob/fv;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/ob/fv;->c()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-object v2, v0

    move-object v1, v0

    .line 378
    :goto_0
    if-nez v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 380
    :try_start_0
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 381
    if-eqz v0, :cond_0

    .line 382
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/yandex/metrica/impl/ob/fs;->a(I)Ljava/net/ServerSocket;
    :try_end_1
    .catch Ljava/net/SocketException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v1

    :cond_0
    move-object v2, v0

    .line 390
    goto :goto_0

    .line 386
    :catch_0
    move-exception v0

    move-object v0, v2

    :goto_1
    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/fs;->d:Landroid/content/Context;

    .line 5022
    const-string v4, "20799a27-fa80-4b36-b2db-0f8141f24180"

    invoke-static {v2, v4}, Lcom/yandex/metrica/YandexMetrica;->getReporter(Landroid/content/Context;Ljava/lang/String;)Lcom/yandex/metrica/IReporter;

    move-result-object v2

    .line 386
    const-string v4, "socket_port_already_in_use"

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-static {v5}, Lcom/yandex/metrica/impl/ob/fs;->c(I)Ljava/util/HashMap;

    move-result-object v5

    invoke-interface {v2, v4, v5}, Lcom/yandex/metrica/IReporter;->reportEvent(Ljava/lang/String;Ljava/util/Map;)V

    move-object v2, v0

    .line 390
    goto :goto_0

    :catch_1
    move-exception v0

    move-object v0, v2

    :goto_2
    move-object v2, v0

    goto :goto_0

    .line 392
    :cond_1
    return-object v1

    .line 390
    :catch_2
    move-exception v2

    goto :goto_2

    .line 386
    :catch_3
    move-exception v2

    goto :goto_1
.end method

.method public run()V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 345
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/fs;->f()Ljava/net/ServerSocket;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/fs;->f:Ljava/net/ServerSocket;

    .line 346
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fs;->f:Ljava/net/ServerSocket;

    if-eqz v0, :cond_8

    .line 347
    :cond_0
    :goto_0
    iget-boolean v0, p0, Lcom/yandex/metrica/impl/ob/fs;->e:Z

    if-eqz v0, :cond_8

    .line 349
    monitor-enter p0

    .line 350
    :try_start_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fs;->f:Ljava/net/ServerSocket;

    .line 351
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 352
    if-eqz v0, :cond_0

    .line 355
    :try_start_1
    invoke-virtual {v0}, Ljava/net/ServerSocket;->accept()Ljava/net/Socket;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    move-result-object v3

    .line 3413
    const/16 v0, 0x3e8

    :try_start_2
    invoke-virtual {v3, v0}, Ljava/net/Socket;->setSoTimeout(I)V

    .line 3415
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 3417
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/InputStreamReader;

    invoke-virtual {v3}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v1, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_6
    .catchall {:try_start_2 .. :try_end_2} :catchall_4

    .line 3418
    :try_start_3
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v4

    .line 3419
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 3420
    const-string v5, "GET /"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 3421
    const/16 v5, 0x2f

    invoke-virtual {v4, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    .line 3422
    const/16 v6, 0x20

    invoke-virtual {v4, v6, v5}, Ljava/lang/String;->indexOf(II)I

    move-result v6

    .line 3423
    if-lez v6, :cond_7

    .line 3424
    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 3427
    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 3428
    :goto_1
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 3429
    const-string v7, ": "

    invoke-virtual {v6, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v7

    .line 3430
    const/4 v8, 0x0

    invoke-virtual {v6, v8, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v7, v7, 0x2

    invoke-virtual {v6, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v8, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_1

    .line 3446
    :catch_0
    move-exception v0

    .line 3447
    :goto_2
    :try_start_4
    iget-object v4, p0, Lcom/yandex/metrica/impl/ob/fs;->d:Landroid/content/Context;

    .line 4022
    const-string v5, "20799a27-fa80-4b36-b2db-0f8141f24180"

    invoke-static {v4, v5}, Lcom/yandex/metrica/YandexMetrica;->getReporter(Landroid/content/Context;Ljava/lang/String;)Lcom/yandex/metrica/IReporter;

    move-result-object v4

    .line 3447
    const-string v5, "LocalHttpServer exception"

    invoke-interface {v4, v5, v0}, Lcom/yandex/metrica/IReporter;->reportError(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 3449
    if-eqz v1, :cond_1

    .line 3450
    :try_start_5
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 360
    :cond_1
    :goto_3
    if-eqz v3, :cond_0

    .line 362
    :try_start_6
    invoke-virtual {v3}, Ljava/net/Socket;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    goto/16 :goto_0

    .line 365
    :catch_1
    move-exception v0

    goto/16 :goto_0

    .line 351
    :catchall_0
    move-exception v0

    :try_start_7
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    throw v0

    .line 3434
    :cond_2
    :try_start_8
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/fs;->c:Ljava/util/HashMap;

    invoke-virtual {v5}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/yandex/metrica/impl/ob/fs$c;

    .line 3435
    if-eqz v0, :cond_4

    .line 3436
    invoke-virtual {v0, v5, v3}, Lcom/yandex/metrica/impl/ob/fs$c;->a(Landroid/net/Uri;Ljava/net/Socket;)Lcom/yandex/metrica/impl/ob/fs$b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fs$b;->a()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 3450
    :cond_3
    :goto_4
    :try_start_9
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    goto :goto_3

    .line 360
    :catch_2
    move-exception v0

    move-object v0, v3

    :goto_5
    if-eqz v0, :cond_0

    .line 362
    :try_start_a
    invoke-virtual {v0}, Ljava/net/Socket;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_3

    goto/16 :goto_0

    .line 365
    :catch_3
    move-exception v0

    goto/16 :goto_0

    .line 3438
    :cond_4
    :try_start_b
    const-string v0, "request_to_unknown_path"

    invoke-direct {p0, v0, v4}, Lcom/yandex/metrica/impl/ob/fs;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_0
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    goto :goto_4

    .line 3449
    :catchall_1
    move-exception v0

    :goto_6
    if-eqz v1, :cond_5

    .line 3450
    :try_start_c
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V

    .line 3452
    :cond_5
    throw v0
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_2
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    .line 360
    :catchall_2
    move-exception v0

    :goto_7
    if-eqz v3, :cond_6

    .line 362
    :try_start_d
    invoke-virtual {v3}, Ljava/net/Socket;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_4

    .line 367
    :cond_6
    :goto_8
    throw v0

    .line 3441
    :cond_7
    :try_start_e
    const-string v0, "invalid_route"

    invoke-direct {p0, v0, v4}, Lcom/yandex/metrica/impl/ob/fs;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_0
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    goto :goto_4

    :catch_4
    move-exception v1

    goto :goto_8

    .line 371
    :cond_8
    return-void

    .line 360
    :catchall_3
    move-exception v0

    move-object v3, v2

    goto :goto_7

    :catch_5
    move-exception v0

    move-object v0, v2

    goto :goto_5

    .line 3449
    :catchall_4
    move-exception v0

    move-object v1, v2

    goto :goto_6

    .line 3446
    :catch_6
    move-exception v0

    move-object v1, v2

    goto :goto_2
.end method
