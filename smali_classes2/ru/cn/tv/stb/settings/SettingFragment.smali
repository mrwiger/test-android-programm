.class public Lru/cn/tv/stb/settings/SettingFragment;
.super Landroid/support/v4/app/ListFragment;
.source "SettingFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/tv/stb/settings/SettingFragment$SettingFragmentListener;,
        Lru/cn/tv/stb/settings/SettingFragment$Type;
    }
.end annotation


# instance fields
.field private adapter:Lru/cn/tv/stb/settings/SettingsAdapter;

.field private listener:Lru/cn/tv/stb/settings/SettingFragment$SettingFragmentListener;

.field private viewModel:Lru/cn/tv/stb/settings/SettingsViewModel;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/support/v4/app/ListFragment;-><init>()V

    return-void
.end method

.method private getItemType(I)Lru/cn/tv/stb/settings/SettingFragment$Type;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 262
    iget-object v1, p0, Lru/cn/tv/stb/settings/SettingFragment;->adapter:Lru/cn/tv/stb/settings/SettingsAdapter;

    invoke-virtual {v1, p1}, Lru/cn/tv/stb/settings/SettingsAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 263
    .local v0, "c":Landroid/database/Cursor;
    const-string v1, "type"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lru/cn/tv/stb/settings/SettingFragment$Type;->valueOf(Ljava/lang/String;)Lru/cn/tv/stb/settings/SettingFragment$Type;

    move-result-object v1

    return-object v1
.end method

.method private showEditUserPlaylistDialog(Lru/cn/tv/stb/settings/UserPlaylistItem;)V
    .locals 5
    .param p1, "userPlaylistItem"    # Lru/cn/tv/stb/settings/UserPlaylistItem;

    .prologue
    .line 249
    iget v1, p1, Lru/cn/tv/stb/settings/UserPlaylistItem;->playlistId:I

    int-to-long v2, v1

    iget-object v1, p1, Lru/cn/tv/stb/settings/UserPlaylistItem;->title:Ljava/lang/String;

    iget-object v4, p1, Lru/cn/tv/stb/settings/UserPlaylistItem;->location:Ljava/lang/String;

    invoke-static {v2, v3, v1, v4}, Lru/cn/tv/playlists/PlaylistActionDialog;->newInstance(JLjava/lang/String;Ljava/lang/String;)Lru/cn/tv/playlists/PlaylistActionDialog;

    move-result-object v0

    .line 254
    .local v0, "actionDialog":Lru/cn/tv/playlists/PlaylistActionDialog;
    invoke-virtual {p0}, Lru/cn/tv/stb/settings/SettingFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "dialog"

    invoke-virtual {v0, v1, v2}, Lru/cn/tv/playlists/PlaylistActionDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 255
    return-void
.end method

.method private showListDialog(Lru/cn/tv/stb/settings/PreferenceItem;Lru/cn/tv/PreferenceListDialog$PreferenceListListener;)V
    .locals 5
    .param p1, "preferenceItem"    # Lru/cn/tv/stb/settings/PreferenceItem;
    .param p2, "preferenceListListener"    # Lru/cn/tv/PreferenceListDialog$PreferenceListListener;

    .prologue
    .line 238
    iget v1, p1, Lru/cn/tv/stb/settings/PreferenceItem;->titleId:I

    .line 239
    invoke-virtual {p0, v1}, Lru/cn/tv/stb/settings/SettingFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p1, Lru/cn/tv/stb/settings/PreferenceItem;->key:Ljava/lang/String;

    .line 241
    invoke-virtual {p0}, Lru/cn/tv/stb/settings/SettingFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget v4, p1, Lru/cn/tv/stb/settings/PreferenceItem;->optionsArrayId:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    .line 238
    invoke-static {v1, v2, v3}, Lru/cn/tv/PreferenceListDialog;->newInstance(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Lru/cn/tv/PreferenceListDialog;

    move-result-object v0

    .line 243
    .local v0, "scalingDialog":Lru/cn/tv/PreferenceListDialog;
    invoke-virtual {v0, p2}, Lru/cn/tv/PreferenceListDialog;->setListener(Lru/cn/tv/PreferenceListDialog$PreferenceListListener;)V

    .line 245
    invoke-virtual {p0}, Lru/cn/tv/stb/settings/SettingFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "dialog"

    invoke-virtual {v0, v1, v2}, Lru/cn/tv/PreferenceListDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 246
    return-void
.end method

.method private showMulticastFiltering()V
    .locals 5

    .prologue
    .line 187
    const v0, 0x7f0e0145

    .line 188
    .local v0, "message":I
    const v1, 0x7f0e0038

    .line 189
    .local v1, "positiveButtonText":I
    invoke-virtual {p0}, Lru/cn/tv/stb/settings/SettingFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "multicast_streams"

    invoke-static {v2, v3}, Lru/cn/domain/Preferences;->getBoolean(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 190
    const v0, 0x7f0e0146

    .line 193
    :cond_0
    new-instance v2, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lru/cn/tv/stb/settings/SettingFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 194
    invoke-virtual {v2, v0}, Landroid/support/v7/app/AlertDialog$Builder;->setMessage(I)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v2

    new-instance v3, Lru/cn/tv/stb/settings/SettingFragment$$Lambda$3;

    invoke-direct {v3, p0}, Lru/cn/tv/stb/settings/SettingFragment$$Lambda$3;-><init>(Lru/cn/tv/stb/settings/SettingFragment;)V

    .line 195
    invoke-virtual {v2, v1, v3}, Landroid/support/v7/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0e0039

    const/4 v4, 0x0

    .line 197
    invoke-virtual {v2, v3, v4}, Landroid/support/v7/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v2

    const/4 v3, 0x1

    .line 198
    invoke-virtual {v2, v3}, Landroid/support/v7/app/AlertDialog$Builder;->setCancelable(Z)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v2

    .line 199
    invoke-virtual {v2}, Landroid/support/v7/app/AlertDialog$Builder;->show()Landroid/support/v7/app/AlertDialog;

    .line 200
    return-void
.end method

.method private showPinLogin()V
    .locals 3

    .prologue
    .line 217
    new-instance v0, Lru/cn/tv/stb/login/PinAuthorizationDialog;

    invoke-direct {v0}, Lru/cn/tv/stb/login/PinAuthorizationDialog;-><init>()V

    .line 218
    .local v0, "dialog":Lru/cn/tv/stb/login/PinAuthorizationDialog;
    const/16 v1, 0x11

    invoke-virtual {v0, p0, v1}, Lru/cn/tv/stb/login/PinAuthorizationDialog;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 219
    invoke-virtual {p0}, Lru/cn/tv/stb/settings/SettingFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "dialog"

    invoke-virtual {v0, v1, v2}, Lru/cn/tv/stb/login/PinAuthorizationDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 220
    return-void
.end method

.method private showTechnicalInfo()V
    .locals 4

    .prologue
    .line 204
    invoke-static {}, Lru/cn/utils/FeedbackBuilder;->create()Lru/cn/utils/FeedbackBuilder;

    move-result-object v1

    .line 205
    invoke-virtual {p0}, Lru/cn/tv/stb/settings/SettingFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lru/cn/utils/FeedbackBuilder;->setContext(Landroid/content/Context;)Lru/cn/utils/FeedbackBuilder;

    move-result-object v1

    .line 206
    invoke-virtual {v1}, Lru/cn/utils/FeedbackBuilder;->buildInfo()Ljava/lang/String;

    move-result-object v0

    .line 208
    .local v0, "message":Ljava/lang/String;
    new-instance v1, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lru/cn/tv/stb/settings/SettingFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0e0158

    .line 209
    invoke-virtual {v1, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setTitle(I)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v1

    .line 210
    invoke-virtual {v1, v0}, Landroid/support/v7/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0e0034

    const/4 v3, 0x0

    .line 211
    invoke-virtual {v1, v2, v3}, Landroid/support/v7/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x1

    .line 212
    invoke-virtual {v1, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setCancelable(Z)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v1

    .line 213
    invoke-virtual {v1}, Landroid/support/v7/app/AlertDialog$Builder;->show()Landroid/support/v7/app/AlertDialog;

    .line 214
    return-void
.end method

.method private showUdpxyDialog()V
    .locals 3

    .prologue
    .line 182
    new-instance v0, Lru/cn/tv/UdpProxyDialog;

    invoke-direct {v0}, Lru/cn/tv/UdpProxyDialog;-><init>()V

    .line 183
    invoke-virtual {p0}, Lru/cn/tv/stb/settings/SettingFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "dialog"

    invoke-virtual {v0, v1, v2}, Lru/cn/tv/UdpProxyDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 184
    return-void
.end method


# virtual methods
.method final bridge synthetic bridge$lambda$0$SettingFragment(Lru/cn/tv/stb/settings/UserPlaylistItem;)V
    .locals 0

    invoke-direct {p0, p1}, Lru/cn/tv/stb/settings/SettingFragment;->showEditUserPlaylistDialog(Lru/cn/tv/stb/settings/UserPlaylistItem;)V

    return-void
.end method

.method final synthetic lambda$onCreate$0$SettingFragment(Landroid/database/Cursor;)V
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 70
    iget-object v0, p0, Lru/cn/tv/stb/settings/SettingFragment;->adapter:Lru/cn/tv/stb/settings/SettingsAdapter;

    invoke-virtual {v0, p1}, Lru/cn/tv/stb/settings/SettingsAdapter;->changeCursor(Landroid/database/Cursor;)V

    return-void
.end method

.method final synthetic lambda$onCreate$1$SettingFragment(Landroid/util/Pair;)V
    .locals 2
    .param p1, "pair"    # Landroid/util/Pair;

    .prologue
    .line 74
    if-eqz p1, :cond_0

    .line 75
    iget-object v0, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lru/cn/tv/stb/settings/PreferenceItem;

    iget-object v1, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Lru/cn/tv/PreferenceListDialog$PreferenceListListener;

    invoke-direct {p0, v0, v1}, Lru/cn/tv/stb/settings/SettingFragment;->showListDialog(Lru/cn/tv/stb/settings/PreferenceItem;Lru/cn/tv/PreferenceListDialog$PreferenceListListener;)V

    .line 77
    :cond_0
    return-void
.end method

.method final synthetic lambda$showMulticastFiltering$2$SettingFragment(Landroid/content/DialogInterface;I)V
    .locals 1
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 196
    iget-object v0, p0, Lru/cn/tv/stb/settings/SettingFragment;->viewModel:Lru/cn/tv/stb/settings/SettingsViewModel;

    invoke-virtual {v0}, Lru/cn/tv/stb/settings/SettingsViewModel;->toggleMulticastPreference()V

    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 224
    packed-switch p1, :pswitch_data_0

    .line 232
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/ListFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 235
    :cond_0
    :goto_0
    return-void

    .line 226
    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 227
    invoke-virtual {p0}, Lru/cn/tv/stb/settings/SettingFragment;->reloadData()V

    goto :goto_0

    .line 224
    :pswitch_data_0
    .packed-switch 0x11
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 67
    invoke-super {p0, p1}, Landroid/support/v4/app/ListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 69
    invoke-static {}, Lru/cn/tv/App;->scope()Ltoothpick/Scope;

    move-result-object v0

    const-class v1, Lru/cn/tv/stb/settings/SettingsViewModel;

    invoke-static {p0, v0, v1}, Lru/cn/mvvm/ViewModels;->get(Landroid/support/v4/app/Fragment;Ltoothpick/Scope;Ljava/lang/Class;)Landroid/arch/lifecycle/ViewModel;

    move-result-object v0

    check-cast v0, Lru/cn/tv/stb/settings/SettingsViewModel;

    iput-object v0, p0, Lru/cn/tv/stb/settings/SettingFragment;->viewModel:Lru/cn/tv/stb/settings/SettingsViewModel;

    .line 70
    iget-object v0, p0, Lru/cn/tv/stb/settings/SettingFragment;->viewModel:Lru/cn/tv/stb/settings/SettingsViewModel;

    invoke-virtual {v0}, Lru/cn/tv/stb/settings/SettingsViewModel;->settingsItems()Landroid/arch/lifecycle/LiveData;

    move-result-object v0

    new-instance v1, Lru/cn/tv/stb/settings/SettingFragment$$Lambda$0;

    invoke-direct {v1, p0}, Lru/cn/tv/stb/settings/SettingFragment$$Lambda$0;-><init>(Lru/cn/tv/stb/settings/SettingFragment;)V

    invoke-virtual {v0, p0, v1}, Landroid/arch/lifecycle/LiveData;->observe(Landroid/arch/lifecycle/LifecycleOwner;Landroid/arch/lifecycle/Observer;)V

    .line 72
    iget-object v0, p0, Lru/cn/tv/stb/settings/SettingFragment;->viewModel:Lru/cn/tv/stb/settings/SettingsViewModel;

    invoke-virtual {v0}, Lru/cn/tv/stb/settings/SettingsViewModel;->preferenceListDialog()Landroid/arch/lifecycle/LiveData;

    move-result-object v0

    new-instance v1, Lru/cn/tv/stb/settings/SettingFragment$$Lambda$1;

    invoke-direct {v1, p0}, Lru/cn/tv/stb/settings/SettingFragment$$Lambda$1;-><init>(Lru/cn/tv/stb/settings/SettingFragment;)V

    invoke-virtual {v0, p0, v1}, Landroid/arch/lifecycle/LiveData;->observe(Landroid/arch/lifecycle/LifecycleOwner;Landroid/arch/lifecycle/Observer;)V

    .line 78
    iget-object v0, p0, Lru/cn/tv/stb/settings/SettingFragment;->viewModel:Lru/cn/tv/stb/settings/SettingsViewModel;

    invoke-virtual {v0}, Lru/cn/tv/stb/settings/SettingsViewModel;->userPlaylistDialog()Landroid/arch/lifecycle/LiveData;

    move-result-object v0

    new-instance v1, Lru/cn/tv/stb/settings/SettingFragment$$Lambda$2;

    invoke-direct {v1, p0}, Lru/cn/tv/stb/settings/SettingFragment$$Lambda$2;-><init>(Lru/cn/tv/stb/settings/SettingFragment;)V

    invoke-virtual {v0, p0, v1}, Landroid/arch/lifecycle/LiveData;->observe(Landroid/arch/lifecycle/LifecycleOwner;Landroid/arch/lifecycle/Observer;)V

    .line 79
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 83
    const v0, 0x7f0c008b

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 6
    .param p1, "l"    # Landroid/widget/ListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    const/4 v3, 0x0

    .line 101
    invoke-virtual {p0}, Lru/cn/tv/stb/settings/SettingFragment;->getListView()Landroid/widget/ListView;

    move-result-object v4

    invoke-virtual {v4, p3, v3}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 103
    invoke-direct {p0, p3}, Lru/cn/tv/stb/settings/SettingFragment;->getItemType(I)Lru/cn/tv/stb/settings/SettingFragment$Type;

    move-result-object v1

    .line 104
    .local v1, "type":Lru/cn/tv/stb/settings/SettingFragment$Type;
    sget-object v4, Lru/cn/tv/stb/settings/SettingFragment$1;->$SwitchMap$ru$cn$tv$stb$settings$SettingFragment$Type:[I

    invoke-virtual {v1}, Lru/cn/tv/stb/settings/SettingFragment$Type;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 179
    :cond_0
    :goto_0
    return-void

    .line 106
    :pswitch_0
    iget-object v3, p0, Lru/cn/tv/stb/settings/SettingFragment;->listener:Lru/cn/tv/stb/settings/SettingFragment$SettingFragmentListener;

    if-eqz v3, :cond_0

    .line 107
    iget-object v3, p0, Lru/cn/tv/stb/settings/SettingFragment;->listener:Lru/cn/tv/stb/settings/SettingFragment$SettingFragmentListener;

    invoke-interface {v3}, Lru/cn/tv/stb/settings/SettingFragment$SettingFragmentListener;->disablePinClicked()V

    goto :goto_0

    .line 111
    :pswitch_1
    iget-object v3, p0, Lru/cn/tv/stb/settings/SettingFragment;->listener:Lru/cn/tv/stb/settings/SettingFragment$SettingFragmentListener;

    if-eqz v3, :cond_0

    .line 112
    iget-object v3, p0, Lru/cn/tv/stb/settings/SettingFragment;->listener:Lru/cn/tv/stb/settings/SettingFragment$SettingFragmentListener;

    invoke-interface {v3}, Lru/cn/tv/stb/settings/SettingFragment$SettingFragmentListener;->changePinClicked()V

    goto :goto_0

    .line 116
    :pswitch_2
    iget-object v3, p0, Lru/cn/tv/stb/settings/SettingFragment;->listener:Lru/cn/tv/stb/settings/SettingFragment$SettingFragmentListener;

    if-eqz v3, :cond_0

    .line 117
    iget-object v3, p0, Lru/cn/tv/stb/settings/SettingFragment;->listener:Lru/cn/tv/stb/settings/SettingFragment$SettingFragmentListener;

    invoke-interface {v3}, Lru/cn/tv/stb/settings/SettingFragment$SettingFragmentListener;->renewPinClicked()V

    goto :goto_0

    .line 121
    :pswitch_3
    invoke-virtual {p0}, Lru/cn/tv/stb/settings/SettingFragment;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "porno_disabled"

    invoke-static {v4, v5}, Lru/cn/domain/Preferences;->getBoolean(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    .line 122
    .local v0, "isPornoDisabled":Z
    invoke-virtual {p0}, Lru/cn/tv/stb/settings/SettingFragment;->getContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "porno_disabled"

    if-nez v0, :cond_1

    const/4 v3, 0x1

    :cond_1
    invoke-static {v4, v5, v3}, Lru/cn/domain/Preferences;->setBoolean(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 124
    invoke-virtual {p0}, Lru/cn/tv/stb/settings/SettingFragment;->reloadData()V

    .line 126
    iget-object v3, p0, Lru/cn/tv/stb/settings/SettingFragment;->listener:Lru/cn/tv/stb/settings/SettingFragment$SettingFragmentListener;

    if-eqz v3, :cond_0

    .line 127
    iget-object v3, p0, Lru/cn/tv/stb/settings/SettingFragment;->listener:Lru/cn/tv/stb/settings/SettingFragment$SettingFragmentListener;

    invoke-interface {v3}, Lru/cn/tv/stb/settings/SettingFragment$SettingFragmentListener;->disablePornoCat()V

    goto :goto_0

    .line 132
    .end local v0    # "isPornoDisabled":Z
    :pswitch_4
    invoke-static {}, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->newInstance()Lru/cn/tv/playlists/UserPlaylistAddEditDialog;

    move-result-object v2

    .line 133
    .local v2, "userPlaylistAddEditDialog":Lru/cn/tv/playlists/UserPlaylistAddEditDialog;
    invoke-virtual {p0}, Lru/cn/tv/stb/settings/SettingFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lru/cn/tv/playlists/UserPlaylistAddEditDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 137
    .end local v2    # "userPlaylistAddEditDialog":Lru/cn/tv/playlists/UserPlaylistAddEditDialog;
    :pswitch_5
    iget-object v3, p0, Lru/cn/tv/stb/settings/SettingFragment;->viewModel:Lru/cn/tv/stb/settings/SettingsViewModel;

    sget-object v4, Lru/cn/tv/stb/settings/SettingFragment$Type;->PLAYLIST:Lru/cn/tv/stb/settings/SettingFragment$Type;

    invoke-virtual {v3, v4, p3}, Lru/cn/tv/stb/settings/SettingsViewModel;->itemClicked(Lru/cn/tv/stb/settings/SettingFragment$Type;I)V

    goto :goto_0

    .line 141
    :pswitch_6
    iget-object v3, p0, Lru/cn/tv/stb/settings/SettingFragment;->viewModel:Lru/cn/tv/stb/settings/SettingsViewModel;

    sget-object v4, Lru/cn/tv/stb/settings/SettingFragment$Type;->SCALING_LEVEL:Lru/cn/tv/stb/settings/SettingFragment$Type;

    invoke-virtual {v3, v4, p3}, Lru/cn/tv/stb/settings/SettingsViewModel;->itemClicked(Lru/cn/tv/stb/settings/SettingFragment$Type;I)V

    goto :goto_0

    .line 145
    :pswitch_7
    invoke-direct {p0}, Lru/cn/tv/stb/settings/SettingFragment;->showUdpxyDialog()V

    goto :goto_0

    .line 149
    :pswitch_8
    iget-object v3, p0, Lru/cn/tv/stb/settings/SettingFragment;->viewModel:Lru/cn/tv/stb/settings/SettingsViewModel;

    sget-object v4, Lru/cn/tv/stb/settings/SettingFragment$Type;->ADVANCED_PLAYBACK:Lru/cn/tv/stb/settings/SettingFragment$Type;

    invoke-virtual {v3, v4, p3}, Lru/cn/tv/stb/settings/SettingsViewModel;->itemClicked(Lru/cn/tv/stb/settings/SettingFragment$Type;I)V

    goto :goto_0

    .line 153
    :pswitch_9
    iget-object v3, p0, Lru/cn/tv/stb/settings/SettingFragment;->viewModel:Lru/cn/tv/stb/settings/SettingsViewModel;

    sget-object v4, Lru/cn/tv/stb/settings/SettingFragment$Type;->CACHING_LEVEL:Lru/cn/tv/stb/settings/SettingFragment$Type;

    invoke-virtual {v3, v4, p3}, Lru/cn/tv/stb/settings/SettingsViewModel;->itemClicked(Lru/cn/tv/stb/settings/SettingFragment$Type;I)V

    goto :goto_0

    .line 157
    :pswitch_a
    iget-object v3, p0, Lru/cn/tv/stb/settings/SettingFragment;->viewModel:Lru/cn/tv/stb/settings/SettingsViewModel;

    sget-object v4, Lru/cn/tv/stb/settings/SettingFragment$Type;->QUALITY_LEVEL:Lru/cn/tv/stb/settings/SettingFragment$Type;

    invoke-virtual {v3, v4, p3}, Lru/cn/tv/stb/settings/SettingsViewModel;->itemClicked(Lru/cn/tv/stb/settings/SettingFragment$Type;I)V

    goto :goto_0

    .line 161
    :pswitch_b
    invoke-direct {p0}, Lru/cn/tv/stb/settings/SettingFragment;->showMulticastFiltering()V

    goto :goto_0

    .line 165
    :pswitch_c
    invoke-direct {p0}, Lru/cn/tv/stb/settings/SettingFragment;->showTechnicalInfo()V

    goto/16 :goto_0

    .line 169
    :pswitch_d
    invoke-direct {p0}, Lru/cn/tv/stb/settings/SettingFragment;->showPinLogin()V

    goto/16 :goto_0

    .line 173
    :pswitch_e
    iget-object v3, p0, Lru/cn/tv/stb/settings/SettingFragment;->viewModel:Lru/cn/tv/stb/settings/SettingsViewModel;

    invoke-virtual {v3}, Lru/cn/tv/stb/settings/SettingsViewModel;->logout()V

    goto/16 :goto_0

    .line 104
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 88
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/ListFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 90
    new-instance v0, Lru/cn/tv/stb/settings/SettingsAdapter;

    .line 91
    invoke-virtual {p0}, Lru/cn/tv/stb/settings/SettingFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x0

    const v3, 0x7f0c008d

    const v4, 0x7f0c008c

    const v5, 0x7f0c007e

    invoke-direct/range {v0 .. v5}, Lru/cn/tv/stb/settings/SettingsAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;III)V

    iput-object v0, p0, Lru/cn/tv/stb/settings/SettingFragment;->adapter:Lru/cn/tv/stb/settings/SettingsAdapter;

    .line 96
    invoke-virtual {p0}, Lru/cn/tv/stb/settings/SettingFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lru/cn/tv/stb/settings/SettingFragment;->adapter:Lru/cn/tv/stb/settings/SettingsAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 97
    return-void
.end method

.method public reloadData()V
    .locals 1

    .prologue
    .line 268
    iget-object v0, p0, Lru/cn/tv/stb/settings/SettingFragment;->viewModel:Lru/cn/tv/stb/settings/SettingsViewModel;

    invoke-virtual {v0}, Lru/cn/tv/stb/settings/SettingsViewModel;->reload()V

    .line 269
    return-void
.end method

.method public setSettingFragmentListener(Lru/cn/tv/stb/settings/SettingFragment$SettingFragmentListener;)V
    .locals 0
    .param p1, "settingFragmentListener"    # Lru/cn/tv/stb/settings/SettingFragment$SettingFragmentListener;

    .prologue
    .line 258
    iput-object p1, p0, Lru/cn/tv/stb/settings/SettingFragment;->listener:Lru/cn/tv/stb/settings/SettingFragment$SettingFragmentListener;

    .line 259
    return-void
.end method
