.class final Lcom/google/obf/aw;
.super Lcom/google/obf/av;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/obf/aw$a;
    }
.end annotation


# instance fields
.field private final b:Lcom/google/obf/dw;

.field private final c:Lcom/google/obf/dw;

.field private d:I

.field private e:Z

.field private f:I


# direct methods
.method public constructor <init>(Lcom/google/obf/ar;)V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0, p1}, Lcom/google/obf/av;-><init>(Lcom/google/obf/ar;)V

    .line 2
    new-instance v0, Lcom/google/obf/dw;

    sget-object v1, Lcom/google/obf/du;->a:[B

    invoke-direct {v0, v1}, Lcom/google/obf/dw;-><init>([B)V

    iput-object v0, p0, Lcom/google/obf/aw;->b:Lcom/google/obf/dw;

    .line 3
    new-instance v0, Lcom/google/obf/dw;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Lcom/google/obf/dw;-><init>(I)V

    iput-object v0, p0, Lcom/google/obf/aw;->c:Lcom/google/obf/dw;

    .line 4
    return-void
.end method

.method private b(Lcom/google/obf/dw;)Lcom/google/obf/aw$a;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/s;
        }
    .end annotation

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x0

    .line 43
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Lcom/google/obf/dw;->c(I)V

    .line 44
    invoke-virtual {p1}, Lcom/google/obf/dw;->f()I

    move-result v0

    and-int/lit8 v0, v0, 0x3

    add-int/lit8 v2, v0, 0x1

    .line 45
    const/4 v0, 0x3

    if-eq v2, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/obf/dl;->b(Z)V

    .line 46
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 47
    invoke-virtual {p1}, Lcom/google/obf/dw;->f()I

    move-result v0

    and-int/lit8 v6, v0, 0x1f

    move v0, v3

    .line 48
    :goto_1
    if-ge v0, v6, :cond_1

    .line 49
    invoke-static {p1}, Lcom/google/obf/du;->a(Lcom/google/obf/dw;)[B

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 50
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    move v0, v3

    .line 45
    goto :goto_0

    .line 51
    :cond_1
    invoke-virtual {p1}, Lcom/google/obf/dw;->f()I

    move-result v5

    move v0, v3

    .line 52
    :goto_2
    if-ge v0, v5, :cond_2

    .line 53
    invoke-static {p1}, Lcom/google/obf/du;->a(Lcom/google/obf/dw;)[B

    move-result-object v7

    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 54
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 55
    :cond_2
    const/high16 v5, 0x3f800000    # 1.0f

    .line 58
    if-lez v6, :cond_3

    .line 59
    new-instance v4, Lcom/google/obf/dv;

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    invoke-direct {v4, v0}, Lcom/google/obf/dv;-><init>([B)V

    .line 60
    add-int/lit8 v0, v2, 0x1

    mul-int/lit8 v0, v0, 0x8

    invoke-virtual {v4, v0}, Lcom/google/obf/dv;->a(I)V

    .line 61
    invoke-static {v4}, Lcom/google/obf/du;->a(Lcom/google/obf/dv;)Lcom/google/obf/du$b;

    move-result-object v0

    .line 62
    iget v3, v0, Lcom/google/obf/du$b;->b:I

    .line 63
    iget v4, v0, Lcom/google/obf/du$b;->c:I

    .line 64
    iget v5, v0, Lcom/google/obf/du$b;->d:F

    .line 65
    :goto_3
    new-instance v0, Lcom/google/obf/aw$a;

    invoke-direct/range {v0 .. v5}, Lcom/google/obf/aw$a;-><init>(Ljava/util/List;IIIF)V

    return-object v0

    :cond_3
    move v3, v4

    goto :goto_3
.end method


# virtual methods
.method protected a(Lcom/google/obf/dw;J)V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/s;
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v2, -0x1

    const/4 v11, 0x1

    const/4 v6, 0x0

    .line 12
    invoke-virtual {p1}, Lcom/google/obf/dw;->f()I

    move-result v1

    .line 13
    invoke-virtual {p1}, Lcom/google/obf/dw;->j()I

    move-result v3

    .line 14
    int-to-long v4, v3

    const-wide/16 v8, 0x3e8

    mul-long/2addr v4, v8

    add-long v8, p2, v4

    .line 15
    if-nez v1, :cond_1

    iget-boolean v3, p0, Lcom/google/obf/aw;->e:Z

    if-nez v3, :cond_1

    .line 16
    new-instance v1, Lcom/google/obf/dw;

    invoke-virtual {p1}, Lcom/google/obf/dw;->b()I

    move-result v3

    new-array v3, v3, [B

    invoke-direct {v1, v3}, Lcom/google/obf/dw;-><init>([B)V

    .line 17
    iget-object v3, v1, Lcom/google/obf/dw;->a:[B

    invoke-virtual {p1}, Lcom/google/obf/dw;->b()I

    move-result v4

    invoke-virtual {p1, v3, v6, v4}, Lcom/google/obf/dw;->a([BII)V

    .line 18
    invoke-direct {p0, v1}, Lcom/google/obf/aw;->b(Lcom/google/obf/dw;)Lcom/google/obf/aw$a;

    move-result-object v3

    .line 19
    iget v1, v3, Lcom/google/obf/aw$a;->b:I

    iput v1, p0, Lcom/google/obf/aw;->d:I

    .line 20
    const-string v1, "video/avc"

    .line 21
    invoke-virtual {p0}, Lcom/google/obf/aw;->a()J

    move-result-wide v4

    iget v6, v3, Lcom/google/obf/aw$a;->d:I

    iget v7, v3, Lcom/google/obf/aw$a;->e:I

    iget-object v8, v3, Lcom/google/obf/aw$a;->a:Ljava/util/List;

    iget v10, v3, Lcom/google/obf/aw$a;->c:F

    move v3, v2

    move v9, v2

    .line 22
    invoke-static/range {v0 .. v10}, Lcom/google/obf/q;->a(Ljava/lang/String;Ljava/lang/String;IIJIILjava/util/List;IF)Lcom/google/obf/q;

    move-result-object v0

    .line 23
    iget-object v1, p0, Lcom/google/obf/aw;->a:Lcom/google/obf/ar;

    invoke-interface {v1, v0}, Lcom/google/obf/ar;->a(Lcom/google/obf/q;)V

    .line 24
    iput-boolean v11, p0, Lcom/google/obf/aw;->e:Z

    .line 42
    :cond_0
    :goto_0
    return-void

    .line 25
    :cond_1
    if-ne v1, v11, :cond_0

    .line 26
    iget-object v1, p0, Lcom/google/obf/aw;->c:Lcom/google/obf/dw;

    iget-object v1, v1, Lcom/google/obf/dw;->a:[B

    .line 27
    aput-byte v6, v1, v6

    .line 28
    aput-byte v6, v1, v11

    .line 29
    const/4 v2, 0x2

    aput-byte v6, v1, v2

    .line 30
    iget v1, p0, Lcom/google/obf/aw;->d:I

    rsub-int/lit8 v1, v1, 0x4

    move v5, v6

    .line 32
    :goto_1
    invoke-virtual {p1}, Lcom/google/obf/dw;->b()I

    move-result v2

    if-lez v2, :cond_2

    .line 33
    iget-object v2, p0, Lcom/google/obf/aw;->c:Lcom/google/obf/dw;

    iget-object v2, v2, Lcom/google/obf/dw;->a:[B

    iget v3, p0, Lcom/google/obf/aw;->d:I

    invoke-virtual {p1, v2, v1, v3}, Lcom/google/obf/dw;->a([BII)V

    .line 34
    iget-object v2, p0, Lcom/google/obf/aw;->c:Lcom/google/obf/dw;

    invoke-virtual {v2, v6}, Lcom/google/obf/dw;->c(I)V

    .line 35
    iget-object v2, p0, Lcom/google/obf/aw;->c:Lcom/google/obf/dw;

    invoke-virtual {v2}, Lcom/google/obf/dw;->s()I

    move-result v2

    .line 36
    iget-object v3, p0, Lcom/google/obf/aw;->b:Lcom/google/obf/dw;

    invoke-virtual {v3, v6}, Lcom/google/obf/dw;->c(I)V

    .line 37
    iget-object v3, p0, Lcom/google/obf/aw;->a:Lcom/google/obf/ar;

    iget-object v4, p0, Lcom/google/obf/aw;->b:Lcom/google/obf/dw;

    const/4 v7, 0x4

    invoke-interface {v3, v4, v7}, Lcom/google/obf/ar;->a(Lcom/google/obf/dw;I)V

    .line 38
    add-int/lit8 v3, v5, 0x4

    .line 39
    iget-object v4, p0, Lcom/google/obf/aw;->a:Lcom/google/obf/ar;

    invoke-interface {v4, p1, v2}, Lcom/google/obf/ar;->a(Lcom/google/obf/dw;I)V

    .line 40
    add-int v5, v3, v2

    goto :goto_1

    .line 41
    :cond_2
    iget-object v1, p0, Lcom/google/obf/aw;->a:Lcom/google/obf/ar;

    iget v2, p0, Lcom/google/obf/aw;->f:I

    if-ne v2, v11, :cond_3

    move v4, v11

    :goto_2
    move-wide v2, v8

    move-object v7, v0

    invoke-interface/range {v1 .. v7}, Lcom/google/obf/ar;->a(JIII[B)V

    goto :goto_0

    :cond_3
    move v4, v6

    goto :goto_2
.end method

.method protected a(Lcom/google/obf/dw;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/obf/av$a;
        }
    .end annotation

    .prologue
    .line 5
    invoke-virtual {p1}, Lcom/google/obf/dw;->f()I

    move-result v0

    .line 6
    shr-int/lit8 v1, v0, 0x4

    and-int/lit8 v1, v1, 0xf

    .line 7
    and-int/lit8 v0, v0, 0xf

    .line 8
    const/4 v2, 0x7

    if-eq v0, v2, :cond_0

    .line 9
    new-instance v1, Lcom/google/obf/av$a;

    const/16 v2, 0x27

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Video format not supported: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/obf/av$a;-><init>(Ljava/lang/String;)V

    throw v1

    .line 10
    :cond_0
    iput v1, p0, Lcom/google/obf/aw;->f:I

    .line 11
    const/4 v0, 0x5

    if-eq v1, v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
