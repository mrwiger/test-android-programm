.class final Lcom/google/obf/y$a;
.super Ljava/lang/Object;
.source "IMASDK"

# interfaces
.implements Landroid/os/Handler$Callback;
.implements Landroid/view/Choreographer$FrameCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/obf/y;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "a"
.end annotation


# static fields
.field private static final b:Lcom/google/obf/y$a;


# instance fields
.field public volatile a:J

.field private final c:Landroid/os/Handler;

.field private final d:Landroid/os/HandlerThread;

.field private e:Landroid/view/Choreographer;

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    new-instance v0, Lcom/google/obf/y$a;

    invoke-direct {v0}, Lcom/google/obf/y$a;-><init>()V

    sput-object v0, Lcom/google/obf/y$a;->b:Lcom/google/obf/y$a;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "ChoreographerOwner:Handler"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/obf/y$a;->d:Landroid/os/HandlerThread;

    .line 4
    iget-object v0, p0, Lcom/google/obf/y$a;->d:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 5
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/obf/y$a;->d:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/google/obf/y$a;->c:Landroid/os/Handler;

    .line 6
    iget-object v0, p0, Lcom/google/obf/y$a;->c:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 7
    return-void
.end method

.method public static a()Lcom/google/obf/y$a;
    .locals 1

    .prologue
    .line 1
    sget-object v0, Lcom/google/obf/y$a;->b:Lcom/google/obf/y$a;

    return-object v0
.end method

.method private d()V
    .locals 1

    .prologue
    .line 23
    invoke-static {}, Landroid/view/Choreographer;->getInstance()Landroid/view/Choreographer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/obf/y$a;->e:Landroid/view/Choreographer;

    .line 24
    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 25
    iget v0, p0, Lcom/google/obf/y$a;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/obf/y$a;->f:I

    .line 26
    iget v0, p0, Lcom/google/obf/y$a;->f:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 27
    iget-object v0, p0, Lcom/google/obf/y$a;->e:Landroid/view/Choreographer;

    invoke-virtual {v0, p0}, Landroid/view/Choreographer;->postFrameCallback(Landroid/view/Choreographer$FrameCallback;)V

    .line 28
    :cond_0
    return-void
.end method

.method private f()V
    .locals 2

    .prologue
    .line 29
    iget v0, p0, Lcom/google/obf/y$a;->f:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/obf/y$a;->f:I

    .line 30
    iget v0, p0, Lcom/google/obf/y$a;->f:I

    if-nez v0, :cond_0

    .line 31
    iget-object v0, p0, Lcom/google/obf/y$a;->e:Landroid/view/Choreographer;

    invoke-virtual {v0, p0}, Landroid/view/Choreographer;->removeFrameCallback(Landroid/view/Choreographer$FrameCallback;)V

    .line 32
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/obf/y$a;->a:J

    .line 33
    :cond_0
    return-void
.end method


# virtual methods
.method public b()V
    .locals 2

    .prologue
    .line 8
    iget-object v0, p0, Lcom/google/obf/y$a;->c:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 9
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 10
    iget-object v0, p0, Lcom/google/obf/y$a;->c:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 11
    return-void
.end method

.method public doFrame(J)V
    .locals 5

    .prologue
    .line 12
    iput-wide p1, p0, Lcom/google/obf/y$a;->a:J

    .line 13
    iget-object v0, p0, Lcom/google/obf/y$a;->e:Landroid/view/Choreographer;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, p0, v2, v3}, Landroid/view/Choreographer;->postFrameCallbackDelayed(Landroid/view/Choreographer$FrameCallback;J)V

    .line 14
    return-void
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 15
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 22
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 16
    :pswitch_0
    invoke-direct {p0}, Lcom/google/obf/y$a;->d()V

    goto :goto_0

    .line 18
    :pswitch_1
    invoke-direct {p0}, Lcom/google/obf/y$a;->e()V

    goto :goto_0

    .line 20
    :pswitch_2
    invoke-direct {p0}, Lcom/google/obf/y$a;->f()V

    goto :goto_0

    .line 15
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
