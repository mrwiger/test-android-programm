.class final Lcom/google/obf/gb$6;
.super Lcom/google/obf/ew;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/obf/gb;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/obf/ew",
        "<",
        "Ljava/lang/Number;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lcom/google/obf/ew;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/obf/ge;)Ljava/lang/Number;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 2
    invoke-virtual {p1}, Lcom/google/obf/ge;->f()Lcom/google/obf/gf;

    move-result-object v0

    .line 3
    sget-object v1, Lcom/google/obf/gb$30;->a:[I

    invoke-virtual {v0}, Lcom/google/obf/gf;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 7
    :pswitch_0
    new-instance v1, Lcom/google/obf/eu;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Expecting number, got: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/obf/eu;-><init>(Ljava/lang/String;)V

    throw v1

    .line 4
    :pswitch_1
    invoke-virtual {p1}, Lcom/google/obf/ge;->j()V

    .line 5
    const/4 v0, 0x0

    .line 6
    :goto_0
    return-object v0

    :pswitch_2
    new-instance v0, Lcom/google/obf/fi;

    invoke-virtual {p1}, Lcom/google/obf/ge;->h()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/obf/fi;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 3
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Lcom/google/obf/gg;Ljava/lang/Number;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 8
    invoke-virtual {p1, p2}, Lcom/google/obf/gg;->a(Ljava/lang/Number;)Lcom/google/obf/gg;

    .line 9
    return-void
.end method

.method public synthetic read(Lcom/google/obf/ge;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 10
    invoke-virtual {p0, p1}, Lcom/google/obf/gb$6;->a(Lcom/google/obf/ge;)Ljava/lang/Number;

    move-result-object v0

    return-object v0
.end method

.method public synthetic write(Lcom/google/obf/gg;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 11
    check-cast p2, Ljava/lang/Number;

    invoke-virtual {p0, p1, p2}, Lcom/google/obf/gb$6;->a(Lcom/google/obf/gg;Ljava/lang/Number;)V

    return-void
.end method
