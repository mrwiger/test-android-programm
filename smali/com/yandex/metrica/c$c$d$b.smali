.class public final Lcom/yandex/metrica/c$c$d$b;
.super Lcom/yandex/metrica/impl/ob/d;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/yandex/metrica/c$c$d;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation


# instance fields
.field public b:Lcom/yandex/metrica/c$c$f;

.field public c:Ljava/lang/String;

.field public d:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 622
    invoke-direct {p0}, Lcom/yandex/metrica/impl/ob/d;-><init>()V

    .line 623
    invoke-virtual {p0}, Lcom/yandex/metrica/c$c$d$b;->d()Lcom/yandex/metrica/c$c$d$b;

    .line 624
    return-void
.end method


# virtual methods
.method public a(Lcom/yandex/metrica/impl/ob/b;)V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 637
    iget-object v0, p0, Lcom/yandex/metrica/c$c$d$b;->b:Lcom/yandex/metrica/c$c$f;

    if-eqz v0, :cond_0

    .line 638
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/yandex/metrica/c$c$d$b;->b:Lcom/yandex/metrica/c$c$f;

    invoke-virtual {p1, v0, v1}, Lcom/yandex/metrica/impl/ob/b;->a(ILcom/yandex/metrica/impl/ob/d;)V

    .line 640
    :cond_0
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/yandex/metrica/c$c$d$b;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/yandex/metrica/impl/ob/b;->a(ILjava/lang/String;)V

    .line 641
    iget v0, p0, Lcom/yandex/metrica/c$c$d$b;->d:I

    if-eqz v0, :cond_1

    .line 642
    const/4 v0, 0x5

    iget v1, p0, Lcom/yandex/metrica/c$c$d$b;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/yandex/metrica/impl/ob/b;->a(II)V

    .line 644
    :cond_1
    invoke-super {p0, p1}, Lcom/yandex/metrica/impl/ob/d;->a(Lcom/yandex/metrica/impl/ob/b;)V

    .line 645
    return-void
.end method

.method protected c()I
    .locals 3

    .prologue
    .line 649
    invoke-super {p0}, Lcom/yandex/metrica/impl/ob/d;->c()I

    move-result v0

    .line 650
    iget-object v1, p0, Lcom/yandex/metrica/c$c$d$b;->b:Lcom/yandex/metrica/c$c$f;

    if-eqz v1, :cond_0

    .line 651
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/yandex/metrica/c$c$d$b;->b:Lcom/yandex/metrica/c$c$f;

    .line 652
    invoke-static {v1, v2}, Lcom/yandex/metrica/impl/ob/b;->b(ILcom/yandex/metrica/impl/ob/d;)I

    move-result v1

    add-int/2addr v0, v1

    .line 654
    :cond_0
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/yandex/metrica/c$c$d$b;->c:Ljava/lang/String;

    .line 655
    invoke-static {v1, v2}, Lcom/yandex/metrica/impl/ob/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 656
    iget v1, p0, Lcom/yandex/metrica/c$c$d$b;->d:I

    if-eqz v1, :cond_1

    .line 657
    const/4 v1, 0x5

    iget v2, p0, Lcom/yandex/metrica/c$c$d$b;->d:I

    .line 658
    invoke-static {v1, v2}, Lcom/yandex/metrica/impl/ob/b;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 660
    :cond_1
    return v0
.end method

.method public d()Lcom/yandex/metrica/c$c$d$b;
    .locals 1

    .prologue
    .line 627
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/yandex/metrica/c$c$d$b;->b:Lcom/yandex/metrica/c$c$f;

    .line 628
    const-string v0, ""

    iput-object v0, p0, Lcom/yandex/metrica/c$c$d$b;->c:Ljava/lang/String;

    .line 629
    const/4 v0, 0x0

    iput v0, p0, Lcom/yandex/metrica/c$c$d$b;->d:I

    .line 630
    const/4 v0, -0x1

    iput v0, p0, Lcom/yandex/metrica/c$c$d$b;->a:I

    .line 631
    return-object p0
.end method
