.class public Lru/cn/api/provider/NotificationIdsStorage;
.super Ljava/lang/Object;
.source "NotificationIdsStorage.java"


# direct methods
.method private static add(Landroid/content/Context;J)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "notificationId"    # J

    .prologue
    .line 24
    const-string v4, "notification"

    const/4 v5, 0x0

    .line 25
    invoke-virtual {p0, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 27
    .local v2, "sharedPreferences":Landroid/content/SharedPreferences;
    const-string v4, "notificationIds"

    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v2, v4, v5}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    .line 28
    .local v1, "set":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 29
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    .line 31
    .local v3, "stringNotificationId":Ljava/lang/String;
    invoke-interface {v1, v3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 32
    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 35
    :cond_0
    const-string v4, "notificationIds"

    invoke-interface {v0, v4, v1}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    .line 36
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 37
    return-void
.end method

.method public static isNotificationIdExist(Landroid/content/Context;J)Z
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "notificationId"    # J

    .prologue
    const/4 v3, 0x0

    .line 81
    const-string v4, "notification"

    .line 82
    invoke-virtual {p0, v4, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 84
    .local v1, "sharedPreferences":Landroid/content/SharedPreferences;
    const-string v4, "notificationIds"

    invoke-interface {v1, v4}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 85
    const-string v4, "notificationIds"

    const/4 v5, 0x0

    invoke-interface {v1, v4, v5}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    .line 86
    .local v0, "set":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-eqz v0, :cond_0

    .line 87
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    .line 88
    .local v2, "stringNotificationId":Ljava/lang/String;
    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    .line 92
    .end local v0    # "set":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    .end local v2    # "stringNotificationId":Ljava/lang/String;
    :cond_0
    return v3
.end method

.method public static scheduleNotification(Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 22
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "item"    # Landroid/database/Cursor;

    .prologue
    .line 41
    move-object/from16 v11, p1

    check-cast v11, Lru/cn/api/provider/cursor/ScheduleItemCursor;

    .line 42
    .local v11, "telecastCursor":Lru/cn/api/provider/cursor/ScheduleItemCursor;
    invoke-virtual {v11}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->getTitle()Ljava/lang/String;

    move-result-object v14

    .line 43
    .local v14, "telecastTitle":Ljava/lang/String;
    invoke-virtual {v11}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->getChannelTitle()Ljava/lang/String;

    move-result-object v5

    .line 44
    .local v5, "channelTitle":Ljava/lang/String;
    invoke-virtual {v11}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->getTelecastId()J

    move-result-wide v12

    .line 45
    .local v12, "telecastId":J
    invoke-virtual {v11}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->getChannelId()J

    move-result-wide v6

    .line 46
    .local v6, "channelId":J
    invoke-virtual {v11}, Lru/cn/api/provider/cursor/ScheduleItemCursor;->getTime()J

    move-result-wide v16

    .line 48
    .local v16, "time":J
    move-object/from16 v0, p0

    invoke-static {v0, v12, v13}, Lru/cn/api/provider/NotificationIdsStorage;->add(Landroid/content/Context;J)V

    .line 51
    move-object/from16 v0, p0

    invoke-static {v0, v12, v13}, Lru/cn/api/provider/NotificationIdsStorage;->isNotificationIdExist(Landroid/content/Context;J)Z

    move-result v18

    if-eqz v18, :cond_0

    .line 52
    const v18, 0x7f0e015d

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 56
    .local v10, "notificationAddDelete":Ljava/lang/String;
    :goto_0
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const v19, 0x7f0e015f

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    const/16 v19, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v18

    .line 57
    invoke-virtual/range {v18 .. v18}, Landroid/widget/Toast;->show()V

    .line 59
    const v18, 0x7f0e00ea

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 60
    .local v15, "titleToShow":Ljava/lang/String;
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " - \""

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, "\" "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const v19, 0x7f0e00e9

    .line 62
    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 64
    .local v9, "message":Ljava/lang/String;
    new-instance v8, Landroid/content/Intent;

    invoke-direct {v8}, Landroid/content/Intent;-><init>()V

    .line 65
    .local v8, "intent":Landroid/content/Intent;
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 66
    .local v4, "args":Landroid/os/Bundle;
    const-string v18, "channelId"

    move-object/from16 v0, v18

    invoke-virtual {v4, v0, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 67
    const-string v18, "ru.cn.launcher"

    move-object/from16 v0, v18

    invoke-virtual {v8, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 68
    const-string v18, "ru.cn.peerstv.Notification"

    move-object/from16 v0, v18

    invoke-virtual {v8, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 70
    const-string v18, "notificationId"

    move-object/from16 v0, v18

    invoke-virtual {v8, v0, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 71
    const-string v18, "time"

    const-wide/16 v20, 0x3e8

    mul-long v20, v20, v16

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v8, v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 72
    const-string v18, "message"

    move-object/from16 v0, v18

    invoke-virtual {v8, v0, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 73
    const-string v18, "packageName"

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 74
    invoke-virtual {v8, v4}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 75
    const/16 v18, 0x20

    move/from16 v0, v18

    invoke-virtual {v8, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 76
    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 77
    return-void

    .line 54
    .end local v4    # "args":Landroid/os/Bundle;
    .end local v8    # "intent":Landroid/content/Intent;
    .end local v9    # "message":Ljava/lang/String;
    .end local v10    # "notificationAddDelete":Ljava/lang/String;
    .end local v15    # "titleToShow":Ljava/lang/String;
    :cond_0
    const v18, 0x7f0e015e

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    .restart local v10    # "notificationAddDelete":Ljava/lang/String;
    goto/16 :goto_0
.end method
