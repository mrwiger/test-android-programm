.class public Lcom/yandex/metrica/impl/ob/bt;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/yandex/metrica/impl/ob/cb;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/app/AlarmManager;

.field private c:Lcom/yandex/metrica/impl/utils/q;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 41
    const-string v0, "alarm"

    .line 43
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    new-instance v1, Lcom/yandex/metrica/impl/utils/p;

    invoke-direct {v1}, Lcom/yandex/metrica/impl/utils/p;-><init>()V

    .line 41
    invoke-direct {p0, p1, v0, v1}, Lcom/yandex/metrica/impl/ob/bt;-><init>(Landroid/content/Context;Landroid/app/AlarmManager;Lcom/yandex/metrica/impl/utils/q;)V

    .line 46
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Landroid/app/AlarmManager;Lcom/yandex/metrica/impl/utils/q;)V
    .locals 0

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    iput-object p1, p0, Lcom/yandex/metrica/impl/ob/bt;->a:Landroid/content/Context;

    .line 78
    iput-object p2, p0, Lcom/yandex/metrica/impl/ob/bt;->b:Landroid/app/AlarmManager;

    .line 79
    iput-object p3, p0, Lcom/yandex/metrica/impl/ob/bt;->c:Lcom/yandex/metrica/impl/utils/q;

    .line 80
    return-void
.end method

.method private static a(Landroid/content/Context;)Landroid/app/PendingIntent;
    .locals 3

    .prologue
    .line 64
    const v0, 0x756c4b

    .line 1070
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/yandex/metrica/ConfigurationService;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "com.yandex.metrica.configuration.ACTION_SCHEDULED_START"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 64
    const/high16 v2, 0x8000000

    invoke-static {p0, v0, v1, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 58
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bt;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/yandex/metrica/impl/ob/bt;->a(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v0

    .line 59
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/bt;->b:Landroid/app/AlarmManager;

    invoke-virtual {v1, v0}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 60
    return-void
.end method

.method public a(J)V
    .locals 7

    .prologue
    .line 51
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bt;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/yandex/metrica/impl/ob/bt;->a(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v6

    .line 52
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bt;->b:Landroid/app/AlarmManager;

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/bt;->c:Lcom/yandex/metrica/impl/utils/q;

    invoke-interface {v2}, Lcom/yandex/metrica/impl/utils/q;->b()J

    move-result-wide v2

    move-wide v4, p1

    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setRepeating(IJJLandroid/app/PendingIntent;)V

    .line 54
    return-void
.end method
