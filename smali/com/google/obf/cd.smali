.class final Lcom/google/obf/cd;
.super Lcom/google/obf/cb;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/obf/cd$a;
    }
.end annotation


# instance fields
.field private b:Z

.field private final c:Lcom/google/obf/ck;

.field private final d:[Z

.field private final e:Lcom/google/obf/cd$a;

.field private final f:Lcom/google/obf/ch;

.field private final g:Lcom/google/obf/ch;

.field private final h:Lcom/google/obf/ch;

.field private i:J

.field private j:J

.field private final k:Lcom/google/obf/dw;


# direct methods
.method public constructor <init>(Lcom/google/obf/ar;Lcom/google/obf/ck;ZZ)V
    .locals 3

    .prologue
    const/16 v2, 0x80

    .line 1
    invoke-direct {p0, p1}, Lcom/google/obf/cb;-><init>(Lcom/google/obf/ar;)V

    .line 2
    iput-object p2, p0, Lcom/google/obf/cd;->c:Lcom/google/obf/ck;

    .line 3
    const/4 v0, 0x3

    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/google/obf/cd;->d:[Z

    .line 4
    new-instance v0, Lcom/google/obf/cd$a;

    invoke-direct {v0, p1, p3, p4}, Lcom/google/obf/cd$a;-><init>(Lcom/google/obf/ar;ZZ)V

    iput-object v0, p0, Lcom/google/obf/cd;->e:Lcom/google/obf/cd$a;

    .line 5
    new-instance v0, Lcom/google/obf/ch;

    const/4 v1, 0x7

    invoke-direct {v0, v1, v2}, Lcom/google/obf/ch;-><init>(II)V

    iput-object v0, p0, Lcom/google/obf/cd;->f:Lcom/google/obf/ch;

    .line 6
    new-instance v0, Lcom/google/obf/ch;

    const/16 v1, 0x8

    invoke-direct {v0, v1, v2}, Lcom/google/obf/ch;-><init>(II)V

    iput-object v0, p0, Lcom/google/obf/cd;->g:Lcom/google/obf/ch;

    .line 7
    new-instance v0, Lcom/google/obf/ch;

    const/4 v1, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/obf/ch;-><init>(II)V

    iput-object v0, p0, Lcom/google/obf/cd;->h:Lcom/google/obf/ch;

    .line 8
    new-instance v0, Lcom/google/obf/dw;

    invoke-direct {v0}, Lcom/google/obf/dw;-><init>()V

    iput-object v0, p0, Lcom/google/obf/cd;->k:Lcom/google/obf/dw;

    .line 9
    return-void
.end method

.method private static a(Lcom/google/obf/ch;)Lcom/google/obf/dv;
    .locals 3

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/obf/ch;->a:[B

    iget v1, p0, Lcom/google/obf/ch;->b:I

    invoke-static {v0, v1}, Lcom/google/obf/du;->a([BI)I

    move-result v0

    .line 88
    new-instance v1, Lcom/google/obf/dv;

    iget-object v2, p0, Lcom/google/obf/ch;->a:[B

    invoke-direct {v1, v2, v0}, Lcom/google/obf/dv;-><init>([BI)V

    .line 89
    const/16 v0, 0x20

    invoke-virtual {v1, v0}, Lcom/google/obf/dv;->b(I)V

    .line 90
    return-object v1
.end method

.method private a(JIIJ)V
    .locals 19

    .prologue
    .line 55
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/obf/cd;->b:Z

    if-eqz v4, :cond_0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/obf/cd;->e:Lcom/google/obf/cd$a;

    invoke-virtual {v4}, Lcom/google/obf/cd$a;->a()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 56
    :cond_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/obf/cd;->f:Lcom/google/obf/ch;

    move/from16 v0, p4

    invoke-virtual {v4, v0}, Lcom/google/obf/ch;->b(I)Z

    .line 57
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/obf/cd;->g:Lcom/google/obf/ch;

    move/from16 v0, p4

    invoke-virtual {v4, v0}, Lcom/google/obf/ch;->b(I)Z

    .line 58
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/obf/cd;->b:Z

    if-nez v4, :cond_3

    .line 59
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/obf/cd;->f:Lcom/google/obf/ch;

    invoke-virtual {v4}, Lcom/google/obf/ch;->b()Z

    move-result v4

    if-eqz v4, :cond_1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/obf/cd;->g:Lcom/google/obf/ch;

    invoke-virtual {v4}, Lcom/google/obf/ch;->b()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 60
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 61
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/obf/cd;->f:Lcom/google/obf/ch;

    iget-object v4, v4, Lcom/google/obf/ch;->a:[B

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/obf/cd;->f:Lcom/google/obf/ch;

    iget v5, v5, Lcom/google/obf/ch;->b:I

    invoke-static {v4, v5}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v4

    invoke-interface {v12, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 62
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/obf/cd;->g:Lcom/google/obf/ch;

    iget-object v4, v4, Lcom/google/obf/ch;->a:[B

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/obf/cd;->g:Lcom/google/obf/ch;

    iget v5, v5, Lcom/google/obf/ch;->b:I

    invoke-static {v4, v5}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v4

    invoke-interface {v12, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 63
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/obf/cd;->f:Lcom/google/obf/ch;

    invoke-static {v4}, Lcom/google/obf/cd;->a(Lcom/google/obf/ch;)Lcom/google/obf/dv;

    move-result-object v4

    invoke-static {v4}, Lcom/google/obf/du;->a(Lcom/google/obf/dv;)Lcom/google/obf/du$b;

    move-result-object v15

    .line 64
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/obf/cd;->g:Lcom/google/obf/ch;

    invoke-static {v4}, Lcom/google/obf/cd;->a(Lcom/google/obf/ch;)Lcom/google/obf/dv;

    move-result-object v4

    invoke-static {v4}, Lcom/google/obf/du;->b(Lcom/google/obf/dv;)Lcom/google/obf/du$a;

    move-result-object v16

    .line 65
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/obf/cd;->a:Lcom/google/obf/ar;

    move-object/from16 v17, v0

    const/4 v4, 0x0

    const-string v5, "video/avc"

    const/4 v6, -0x1

    const/4 v7, -0x1

    const-wide/16 v8, -0x1

    iget v10, v15, Lcom/google/obf/du$b;->b:I

    iget v11, v15, Lcom/google/obf/du$b;->c:I

    const/4 v13, -0x1

    iget v14, v15, Lcom/google/obf/du$b;->d:F

    invoke-static/range {v4 .. v14}, Lcom/google/obf/q;->a(Ljava/lang/String;Ljava/lang/String;IIJIILjava/util/List;IF)Lcom/google/obf/q;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-interface {v0, v4}, Lcom/google/obf/ar;->a(Lcom/google/obf/q;)V

    .line 66
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/obf/cd;->b:Z

    .line 67
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/obf/cd;->e:Lcom/google/obf/cd$a;

    invoke-virtual {v4, v15}, Lcom/google/obf/cd$a;->a(Lcom/google/obf/du$b;)V

    .line 68
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/obf/cd;->e:Lcom/google/obf/cd$a;

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Lcom/google/obf/cd$a;->a(Lcom/google/obf/du$a;)V

    .line 69
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/obf/cd;->f:Lcom/google/obf/ch;

    invoke-virtual {v4}, Lcom/google/obf/ch;->a()V

    .line 70
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/obf/cd;->g:Lcom/google/obf/ch;

    invoke-virtual {v4}, Lcom/google/obf/ch;->a()V

    .line 80
    :cond_1
    :goto_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/obf/cd;->h:Lcom/google/obf/ch;

    move/from16 v0, p4

    invoke-virtual {v4, v0}, Lcom/google/obf/ch;->b(I)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 81
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/obf/cd;->h:Lcom/google/obf/ch;

    iget-object v4, v4, Lcom/google/obf/ch;->a:[B

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/obf/cd;->h:Lcom/google/obf/ch;

    iget v5, v5, Lcom/google/obf/ch;->b:I

    invoke-static {v4, v5}, Lcom/google/obf/du;->a([BI)I

    move-result v4

    .line 82
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/obf/cd;->k:Lcom/google/obf/dw;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/obf/cd;->h:Lcom/google/obf/ch;

    iget-object v6, v6, Lcom/google/obf/ch;->a:[B

    invoke-virtual {v5, v6, v4}, Lcom/google/obf/dw;->a([BI)V

    .line 83
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/obf/cd;->k:Lcom/google/obf/dw;

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Lcom/google/obf/dw;->c(I)V

    .line 84
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/obf/cd;->c:Lcom/google/obf/ck;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/obf/cd;->k:Lcom/google/obf/dw;

    move-wide/from16 v0, p5

    invoke-virtual {v4, v0, v1, v5}, Lcom/google/obf/ck;->a(JLcom/google/obf/dw;)V

    .line 85
    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/obf/cd;->e:Lcom/google/obf/cd$a;

    move-wide/from16 v0, p1

    move/from16 v2, p3

    invoke-virtual {v4, v0, v1, v2}, Lcom/google/obf/cd$a;->a(JI)V

    .line 86
    return-void

    .line 72
    :cond_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/obf/cd;->f:Lcom/google/obf/ch;

    invoke-virtual {v4}, Lcom/google/obf/ch;->b()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 73
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/obf/cd;->f:Lcom/google/obf/ch;

    invoke-static {v4}, Lcom/google/obf/cd;->a(Lcom/google/obf/ch;)Lcom/google/obf/dv;

    move-result-object v4

    invoke-static {v4}, Lcom/google/obf/du;->a(Lcom/google/obf/dv;)Lcom/google/obf/du$b;

    move-result-object v4

    .line 74
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/obf/cd;->e:Lcom/google/obf/cd$a;

    invoke-virtual {v5, v4}, Lcom/google/obf/cd$a;->a(Lcom/google/obf/du$b;)V

    .line 75
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/obf/cd;->f:Lcom/google/obf/ch;

    invoke-virtual {v4}, Lcom/google/obf/ch;->a()V

    goto :goto_0

    .line 76
    :cond_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/obf/cd;->g:Lcom/google/obf/ch;

    invoke-virtual {v4}, Lcom/google/obf/ch;->b()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 77
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/obf/cd;->g:Lcom/google/obf/ch;

    invoke-static {v4}, Lcom/google/obf/cd;->a(Lcom/google/obf/ch;)Lcom/google/obf/dv;

    move-result-object v4

    invoke-static {v4}, Lcom/google/obf/du;->b(Lcom/google/obf/dv;)Lcom/google/obf/du$a;

    move-result-object v4

    .line 78
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/obf/cd;->e:Lcom/google/obf/cd$a;

    invoke-virtual {v5, v4}, Lcom/google/obf/cd$a;->a(Lcom/google/obf/du$a;)V

    .line 79
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/obf/cd;->g:Lcom/google/obf/ch;

    invoke-virtual {v4}, Lcom/google/obf/ch;->a()V

    goto/16 :goto_0
.end method

.method private a(JIJ)V
    .locals 6

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/google/obf/cd;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/obf/cd;->e:Lcom/google/obf/cd$a;

    invoke-virtual {v0}, Lcom/google/obf/cd$a;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 44
    :cond_0
    iget-object v0, p0, Lcom/google/obf/cd;->f:Lcom/google/obf/ch;

    invoke-virtual {v0, p3}, Lcom/google/obf/ch;->a(I)V

    .line 45
    iget-object v0, p0, Lcom/google/obf/cd;->g:Lcom/google/obf/ch;

    invoke-virtual {v0, p3}, Lcom/google/obf/ch;->a(I)V

    .line 46
    :cond_1
    iget-object v0, p0, Lcom/google/obf/cd;->h:Lcom/google/obf/ch;

    invoke-virtual {v0, p3}, Lcom/google/obf/ch;->a(I)V

    .line 47
    iget-object v0, p0, Lcom/google/obf/cd;->e:Lcom/google/obf/cd$a;

    move-wide v1, p1

    move v3, p3

    move-wide v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/obf/cd$a;->a(JIJ)V

    .line 48
    return-void
.end method

.method private a([BII)V
    .locals 1

    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/google/obf/cd;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/obf/cd;->e:Lcom/google/obf/cd$a;

    invoke-virtual {v0}, Lcom/google/obf/cd$a;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 50
    :cond_0
    iget-object v0, p0, Lcom/google/obf/cd;->f:Lcom/google/obf/ch;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/obf/ch;->a([BII)V

    .line 51
    iget-object v0, p0, Lcom/google/obf/cd;->g:Lcom/google/obf/ch;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/obf/ch;->a([BII)V

    .line 52
    :cond_1
    iget-object v0, p0, Lcom/google/obf/cd;->h:Lcom/google/obf/ch;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/obf/ch;->a([BII)V

    .line 53
    iget-object v0, p0, Lcom/google/obf/cd;->e:Lcom/google/obf/cd$a;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/obf/cd$a;->a([BII)V

    .line 54
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 10
    iget-object v0, p0, Lcom/google/obf/cd;->d:[Z

    invoke-static {v0}, Lcom/google/obf/du;->a([Z)V

    .line 11
    iget-object v0, p0, Lcom/google/obf/cd;->f:Lcom/google/obf/ch;

    invoke-virtual {v0}, Lcom/google/obf/ch;->a()V

    .line 12
    iget-object v0, p0, Lcom/google/obf/cd;->g:Lcom/google/obf/ch;

    invoke-virtual {v0}, Lcom/google/obf/ch;->a()V

    .line 13
    iget-object v0, p0, Lcom/google/obf/cd;->h:Lcom/google/obf/ch;

    invoke-virtual {v0}, Lcom/google/obf/ch;->a()V

    .line 14
    iget-object v0, p0, Lcom/google/obf/cd;->e:Lcom/google/obf/cd$a;

    invoke-virtual {v0}, Lcom/google/obf/cd$a;->b()V

    .line 15
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/obf/cd;->i:J

    .line 16
    return-void
.end method

.method public a(JZ)V
    .locals 1

    .prologue
    .line 17
    iput-wide p1, p0, Lcom/google/obf/cd;->j:J

    .line 18
    return-void
.end method

.method public a(Lcom/google/obf/dw;)V
    .locals 12

    .prologue
    .line 19
    invoke-virtual {p1}, Lcom/google/obf/dw;->b()I

    move-result v0

    if-lez v0, :cond_0

    .line 20
    invoke-virtual {p1}, Lcom/google/obf/dw;->d()I

    move-result v0

    .line 21
    invoke-virtual {p1}, Lcom/google/obf/dw;->c()I

    move-result v8

    .line 22
    iget-object v9, p1, Lcom/google/obf/dw;->a:[B

    .line 23
    iget-wide v2, p0, Lcom/google/obf/cd;->i:J

    invoke-virtual {p1}, Lcom/google/obf/dw;->b()I

    move-result v1

    int-to-long v4, v1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/obf/cd;->i:J

    .line 24
    iget-object v1, p0, Lcom/google/obf/cd;->a:Lcom/google/obf/ar;

    invoke-virtual {p1}, Lcom/google/obf/dw;->b()I

    move-result v2

    invoke-interface {v1, p1, v2}, Lcom/google/obf/ar;->a(Lcom/google/obf/dw;I)V

    .line 25
    :goto_0
    iget-object v1, p0, Lcom/google/obf/cd;->d:[Z

    invoke-static {v9, v0, v8, v1}, Lcom/google/obf/du;->a([BII[Z)I

    move-result v10

    .line 26
    if-ne v10, v8, :cond_1

    .line 27
    invoke-direct {p0, v9, v0, v8}, Lcom/google/obf/cd;->a([BII)V

    .line 41
    :cond_0
    return-void

    .line 29
    :cond_1
    invoke-static {v9, v10}, Lcom/google/obf/du;->b([BI)I

    move-result v11

    .line 30
    sub-int v1, v10, v0

    .line 31
    if-lez v1, :cond_2

    .line 32
    invoke-direct {p0, v9, v0, v10}, Lcom/google/obf/cd;->a([BII)V

    .line 33
    :cond_2
    sub-int v4, v8, v10

    .line 34
    iget-wide v2, p0, Lcom/google/obf/cd;->i:J

    int-to-long v6, v4

    sub-long/2addr v2, v6

    .line 36
    if-gez v1, :cond_3

    neg-int v5, v1

    :goto_1
    iget-wide v6, p0, Lcom/google/obf/cd;->j:J

    move-object v1, p0

    .line 37
    invoke-direct/range {v1 .. v7}, Lcom/google/obf/cd;->a(JIIJ)V

    .line 38
    iget-wide v5, p0, Lcom/google/obf/cd;->j:J

    move-object v1, p0

    move v4, v11

    invoke-direct/range {v1 .. v6}, Lcom/google/obf/cd;->a(JIJ)V

    .line 39
    add-int/lit8 v0, v10, 0x3

    .line 40
    goto :goto_0

    .line 36
    :cond_3
    const/4 v5, 0x0

    goto :goto_1
.end method

.method public b()V
    .locals 0

    .prologue
    .line 42
    return-void
.end method
