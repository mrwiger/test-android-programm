.class public Lcom/yandex/metrica/impl/ob/dl;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/yandex/metrica/impl/ob/dl$c;,
        Lcom/yandex/metrica/impl/ob/dl$a;,
        Lcom/yandex/metrica/impl/ob/dl$b;
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/Object;

.field private final b:Lcom/yandex/metrica/impl/ob/dl$a;

.field private final c:Lcom/yandex/metrica/impl/ob/dn;

.field private d:Lcom/yandex/metrica/impl/ob/dk;


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/dl;->a:Ljava/lang/Object;

    .line 67
    new-instance v0, Lcom/yandex/metrica/impl/ob/dl$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/yandex/metrica/impl/ob/dl$a;-><init>(Lcom/yandex/metrica/impl/ob/dl;B)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/dl;->b:Lcom/yandex/metrica/impl/ob/dl$a;

    .line 68
    new-instance v0, Lcom/yandex/metrica/impl/ob/dn;

    invoke-direct {v0, p0}, Lcom/yandex/metrica/impl/ob/dn;-><init>(Lcom/yandex/metrica/impl/ob/dl;)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/dl;->c:Lcom/yandex/metrica/impl/ob/dn;

    .line 71
    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/yandex/metrica/impl/ob/dl;-><init>()V

    return-void
.end method

.method private a(Landroid/content/Context;Ljava/lang/String;Ljava/io/File;)Lcom/yandex/metrica/impl/ob/dk;
    .locals 3

    .prologue
    .line 132
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/16 v1, 0x2000

    invoke-virtual {v0, p2, v1}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    .line 133
    if-eqz v0, :cond_0

    .line 134
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    .line 1163
    invoke-virtual {p3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 134
    invoke-direct {p0, p1, v0}, Lcom/yandex/metrica/impl/ob/dl;->i(Landroid/content/Context;Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/dk;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 139
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a()Lcom/yandex/metrica/impl/ob/dl;
    .locals 1

    .prologue
    .line 62
    invoke-static {}, Lcom/yandex/metrica/impl/ob/dl$b;->a()Lcom/yandex/metrica/impl/ob/dl;

    move-result-object v0

    return-object v0
.end method

.method public static h(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .prologue
    .line 2062
    invoke-static {}, Lcom/yandex/metrica/impl/ob/dl$b;->a()Lcom/yandex/metrica/impl/ob/dl;

    move-result-object v0

    .line 284
    invoke-virtual {v0, p0}, Lcom/yandex/metrica/impl/ob/dl;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 285
    invoke-static {v1}, Lcom/yandex/metrica/impl/bj;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 286
    invoke-static {p0}, Lcom/yandex/metrica/impl/bf;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 287
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 288
    invoke-static {p0, v0}, Lcom/yandex/metrica/impl/bf;->a(Landroid/content/Context;Landroid/content/Intent;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 289
    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    invoke-static {v0}, Lcom/yandex/metrica/impl/bf;->a(Landroid/content/pm/PackageItemInfo;)I

    move-result v0

    .line 291
    if-lez v0, :cond_0

    const/16 v3, 0x1d

    if-ge v0, v3, :cond_0

    .line 2302
    :try_start_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 2303
    const-string v3, "DEVICE_ID"

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2306
    invoke-static {v1}, Lcom/yandex/metrica/impl/bj;->a(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 2307
    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "content://%s.MetricaContentProvider/DEVICE_ID"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2308
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v4, v3, v0, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    .line 296
    :cond_1
    return-object v1
.end method

.method private i(Landroid/content/Context;Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/dk;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 146
    :try_start_0
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 147
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 148
    iget-object v3, p0, Lcom/yandex/metrica/impl/ob/dl;->a:Ljava/lang/Object;

    monitor-enter v3
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 149
    :try_start_1
    invoke-static {p1, v2}, Lcom/yandex/metrica/impl/s;->a(Landroid/content/Context;Ljava/io/File;)Ljava/lang/String;

    move-result-object v4

    .line 150
    monitor-exit v3

    .line 151
    if-nez v4, :cond_1

    .line 158
    :cond_0
    :goto_0
    return-object v0

    .line 150
    :catchall_0
    move-exception v1

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v1

    .line 157
    :catch_0
    move-exception v1

    goto :goto_0

    .line 151
    :cond_1
    new-instance v1, Lcom/yandex/metrica/impl/ob/dk;

    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, v4}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->lastModified()J

    move-result-wide v4

    invoke-direct {v1, v3, v4, v5}, Lcom/yandex/metrica/impl/ob/dk;-><init>(Lorg/json/JSONObject;J)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    move-object v0, v1

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method private j(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 221
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/dl;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 222
    :try_start_0
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/dl;->c()Lcom/yandex/metrica/impl/ob/dk;

    move-result-object v0

    if-nez v0, :cond_4

    .line 223
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/yandex/metrica/impl/ob/dl;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/dk;

    move-result-object v0

    .line 224
    if-eqz v0, :cond_3

    .line 225
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/dl;->e()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 226
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, p1, v2}, Lcom/yandex/metrica/impl/ob/dl;->b(Landroid/content/Context;Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/dk;

    move-result-object v2

    .line 227
    invoke-virtual {v0, v2}, Lcom/yandex/metrica/impl/ob/dk;->a(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Lcom/yandex/metrica/impl/ob/dk;->e()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 228
    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/dl;->d:Lcom/yandex/metrica/impl/ob/dk;

    .line 229
    invoke-virtual {v2}, Lcom/yandex/metrica/impl/ob/dk;->c()Ljava/lang/String;

    move-result-object v0

    monitor-exit v1

    .line 245
    :goto_0
    return-object v0

    .line 231
    :cond_0
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/dl;->b()Lcom/yandex/metrica/impl/ob/dl$a;

    move-result-object v2

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/dk;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, p1, v0}, Lcom/yandex/metrica/impl/ob/dl$a;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    monitor-exit v1

    goto :goto_0

    .line 247
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 234
    :cond_1
    :try_start_1
    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/dk;->e()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 235
    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/dl;->d:Lcom/yandex/metrica/impl/ob/dk;

    .line 236
    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/dk;->c()Ljava/lang/String;

    move-result-object v0

    monitor-exit v1

    goto :goto_0

    .line 238
    :cond_2
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/dl;->b()Lcom/yandex/metrica/impl/ob/dl$a;

    move-result-object v2

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/dk;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, p1, v0}, Lcom/yandex/metrica/impl/ob/dl$a;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    monitor-exit v1

    goto :goto_0

    .line 242
    :cond_3
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/dl;->b()Lcom/yandex/metrica/impl/ob/dl$a;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/yandex/metrica/impl/ob/dl$a;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    monitor-exit v1

    goto :goto_0

    .line 245
    :cond_4
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/dl;->c()Lcom/yandex/metrica/impl/ob/dk;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/dk;->c()Ljava/lang/String;

    move-result-object v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method


# virtual methods
.method a(Landroid/content/Context;Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/dk;
    .locals 1

    .prologue
    .line 117
    const-string v0, "credentials.dat"

    .line 118
    invoke-virtual {p1, v0}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 117
    invoke-direct {p0, p1, p2, v0}, Lcom/yandex/metrica/impl/ob/dl;->a(Landroid/content/Context;Ljava/lang/String;Ljava/io/File;)Lcom/yandex/metrica/impl/ob/dk;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 217
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/yandex/metrica/impl/ob/dl;->j(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method b(Landroid/content/Context;Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/dk;
    .locals 3

    .prologue
    .line 124
    new-instance v0, Ljava/io/File;

    .line 125
    invoke-virtual {p1}, Landroid/content/Context;->getNoBackupFilesDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "credentials.dat"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 124
    invoke-direct {p0, p1, p2, v0}, Lcom/yandex/metrica/impl/ob/dl;->a(Landroid/content/Context;Ljava/lang/String;Ljava/io/File;)Lcom/yandex/metrica/impl/ob/dk;

    move-result-object v0

    return-object v0
.end method

.method b()Lcom/yandex/metrica/impl/ob/dl$a;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/dl;->b:Lcom/yandex/metrica/impl/ob/dl$a;

    return-object v0
.end method

.method c()Lcom/yandex/metrica/impl/ob/dk;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/dl;->d:Lcom/yandex/metrica/impl/ob/dk;

    return-object v0
.end method

.method public c(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 175
    invoke-direct {p0, p1, p2}, Lcom/yandex/metrica/impl/ob/dl;->j(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/dl;->c()Lcom/yandex/metrica/impl/ob/dk;

    move-result-object v0

    .line 99
    if-nez v0, :cond_0

    .line 100
    const/4 v0, 0x0

    .line 102
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/dk;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method d(Landroid/content/Context;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 185
    :try_start_0
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/dl;->a:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 186
    :try_start_1
    new-instance v0, Lcom/yandex/metrica/impl/ob/dk;

    new-instance v2, Lcom/yandex/metrica/impl/ob/dm;

    invoke-direct {v2, p1}, Lcom/yandex/metrica/impl/ob/dm;-><init>(Landroid/content/Context;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {v0, p2, v2, v4, v5}, Lcom/yandex/metrica/impl/ob/dk;-><init>(Ljava/lang/String;Lcom/yandex/metrica/impl/ob/dm;J)V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ob/dl;->d:Lcom/yandex/metrica/impl/ob/dk;

    .line 187
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/dl;->d:Lcom/yandex/metrica/impl/ob/dk;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/dk;->a()Ljava/lang/String;

    move-result-object v0

    .line 188
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/dl;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 189
    invoke-virtual {p0, p1, v0}, Lcom/yandex/metrica/impl/ob/dl;->e(Landroid/content/Context;Ljava/lang/String;)V

    .line 191
    :cond_0
    const-string v2, "credentials.dat"

    .line 1169
    iget-object v3, p0, Lcom/yandex/metrica/impl/ob/dl;->a:Ljava/lang/Object;

    monitor-enter v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1170
    :try_start_2
    invoke-static {p1, v2, v0}, Lcom/yandex/metrica/impl/s;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 1171
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 192
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 197
    :goto_0
    return-void

    .line 1171
    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v0

    .line 192
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :try_start_6
    throw v0
    :try_end_6
    .catch Lorg/json/JSONException; {:try_start_6 .. :try_end_6} :catch_0

    .line 197
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method e(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 201
    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/dl;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 202
    :try_start_0
    const-string v0, "credentials.dat"

    invoke-static {p1, v0, p2}, Lcom/yandex/metrica/impl/s;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method e()Z
    .locals 1

    .prologue
    .line 179
    const/16 v0, 0x15

    invoke-static {v0}, Lcom/yandex/metrica/impl/bl;->a(I)Z

    move-result v0

    return v0
.end method

.method f()Lcom/yandex/metrica/impl/ob/dn;
    .locals 1

    .prologue
    .line 279
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/dl;->c:Lcom/yandex/metrica/impl/ob/dn;

    return-object v0
.end method

.method f(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 208
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2022
    const-string v0, "20799a27-fa80-4b36-b2db-0f8141f24180"

    invoke-static {p1, v0}, Lcom/yandex/metrica/YandexMetrica;->getReporter(Landroid/content/Context;Ljava/lang/String;)Lcom/yandex/metrica/IReporter;

    move-result-object v0

    .line 209
    const-string v1, "saving_empty_device_id"

    new-instance v2, Lcom/yandex/metrica/impl/ob/dl$c;

    invoke-direct {v2, p1, p2}, Lcom/yandex/metrica/impl/ob/dl$c;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 210
    invoke-interface {v0, v1, v2}, Lcom/yandex/metrica/IReporter;->reportEvent(Ljava/lang/String;Ljava/util/Map;)V

    .line 214
    :goto_0
    return-void

    .line 212
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/yandex/metrica/impl/ob/dl;->d(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method

.method g(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v6, 0x0

    .line 252
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".MetricaContentProvider"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 253
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v1, v0, v3}, Landroid/content/pm/PackageManager;->resolveContentProvider(Ljava/lang/String;I)Landroid/content/pm/ProviderInfo;

    move-result-object v0

    .line 255
    if-eqz v0, :cond_0

    iget-boolean v0, v0, Landroid/content/pm/ProviderInfo;->enabled:Z

    if-nez v0, :cond_1

    :cond_0
    move-object v0, v6

    .line 275
    :goto_0
    return-object v0

    .line 259
    :cond_1
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "content://%s.MetricaContentProvider/DEVICE_ID"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p2, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 264
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 265
    if-eqz v1, :cond_2

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 266
    const-string v0, "DEVICE_ID"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 273
    :goto_1
    invoke-static {v1}, Lcom/yandex/metrica/impl/bl;->a(Landroid/database/Cursor;)V

    goto :goto_0

    .line 271
    :catch_0
    move-exception v0

    move-object v0, v6

    .line 273
    :goto_2
    invoke-static {v0}, Lcom/yandex/metrica/impl/bl;->a(Landroid/database/Cursor;)V

    move-object v0, v6

    .line 274
    goto :goto_0

    .line 273
    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_3
    invoke-static {v1}, Lcom/yandex/metrica/impl/bl;->a(Landroid/database/Cursor;)V

    .line 274
    throw v0

    .line 273
    :catchall_1
    move-exception v0

    goto :goto_3

    .line 271
    :catch_1
    move-exception v0

    move-object v0, v1

    goto :goto_2

    :cond_2
    move-object v0, v6

    goto :goto_1
.end method
