.class public Lru/cn/tv/UdpProxyDialog;
.super Landroid/support/v4/app/DialogFragment;
.source "UdpProxyDialog.java"


# instance fields
.field private addressText:Landroid/widget/EditText;

.field private enabled:Landroid/widget/CheckBox;

.field private portText:Landroid/widget/EditText;

.field private saveClickListener:Landroid/view/View$OnClickListener;

.field private settings:Lru/cn/domain/UDPProxySettings;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    .line 85
    new-instance v0, Lru/cn/tv/UdpProxyDialog$2;

    invoke-direct {v0, p0}, Lru/cn/tv/UdpProxyDialog$2;-><init>(Lru/cn/tv/UdpProxyDialog;)V

    iput-object v0, p0, Lru/cn/tv/UdpProxyDialog;->saveClickListener:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lru/cn/tv/UdpProxyDialog;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/UdpProxyDialog;

    .prologue
    .line 27
    iget-object v0, p0, Lru/cn/tv/UdpProxyDialog;->addressText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$100(Lru/cn/tv/UdpProxyDialog;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/UdpProxyDialog;

    .prologue
    .line 27
    iget-object v0, p0, Lru/cn/tv/UdpProxyDialog;->portText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$200(Lru/cn/tv/UdpProxyDialog;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/UdpProxyDialog;

    .prologue
    .line 27
    iget-object v0, p0, Lru/cn/tv/UdpProxyDialog;->enabled:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$300(Lru/cn/tv/UdpProxyDialog;)Lru/cn/domain/UDPProxySettings;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/UdpProxyDialog;

    .prologue
    .line 27
    iget-object v0, p0, Lru/cn/tv/UdpProxyDialog;->settings:Lru/cn/domain/UDPProxySettings;

    return-object v0
.end method

.method static synthetic access$400(Lru/cn/tv/UdpProxyDialog;Ljava/lang/String;Ljava/lang/String;)Lru/cn/utils/validation/CompositeValidator;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/UdpProxyDialog;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Lru/cn/tv/UdpProxyDialog;->getUdpProxyValidator(Ljava/lang/String;Ljava/lang/String;)Lru/cn/utils/validation/CompositeValidator;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lru/cn/tv/UdpProxyDialog;Landroid/content/Context;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/UdpProxyDialog;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Lru/cn/tv/UdpProxyDialog;->showErrorMessage(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method private getUdpProxyValidator(Ljava/lang/String;Ljava/lang/String;)Lru/cn/utils/validation/CompositeValidator;
    .locals 5
    .param p1, "address"    # Ljava/lang/String;
    .param p2, "port"    # Ljava/lang/String;

    .prologue
    .line 134
    new-instance v0, Lru/cn/utils/validation/NotEmptyValidator;

    const v2, 0x7f0e0172

    invoke-virtual {p0, v2}, Lru/cn/tv/UdpProxyDialog;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, p1, v2}, Lru/cn/utils/validation/NotEmptyValidator;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    .local v0, "addressValidator":Lru/cn/utils/validation/Validator;
    new-instance v1, Lru/cn/utils/validation/NetworkPortValidator;

    const v2, 0x7f0e0176

    invoke-virtual {p0, v2}, Lru/cn/tv/UdpProxyDialog;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p2, v2}, Lru/cn/utils/validation/NetworkPortValidator;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    .local v1, "portValidator":Lru/cn/utils/validation/Validator;
    new-instance v2, Lru/cn/utils/validation/CompositeValidator;

    const/4 v3, 0x2

    new-array v3, v3, [Lru/cn/utils/validation/Validator;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v4, 0x1

    aput-object v1, v3, v4

    invoke-direct {v2, v3}, Lru/cn/utils/validation/CompositeValidator;-><init>([Lru/cn/utils/validation/Validator;)V

    return-object v2
.end method

.method private showErrorMessage(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "description"    # Ljava/lang/String;

    .prologue
    .line 126
    new-instance v0, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-direct {v0, p1}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0e0174

    .line 127
    invoke-virtual {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;->setTitle(I)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 128
    invoke-virtual {v0, p2}, Landroid/support/v7/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0e0034

    const/4 v2, 0x0

    .line 129
    invoke-virtual {v0, v1, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v0

    .line 130
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->show()Landroid/support/v7/app/AlertDialog;

    .line 131
    return-void
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x0

    .line 37
    invoke-virtual {p0}, Lru/cn/tv/UdpProxyDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 39
    .local v1, "inflater":Landroid/view/LayoutInflater;
    new-instance v3, Lru/cn/domain/UDPProxySettings;

    invoke-virtual {p0}, Lru/cn/tv/UdpProxyDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-direct {v3, v4}, Lru/cn/domain/UDPProxySettings;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lru/cn/tv/UdpProxyDialog;->settings:Lru/cn/domain/UDPProxySettings;

    .line 41
    const v3, 0x7f0c0039

    invoke-virtual {v1, v3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 42
    .local v2, "view":Landroid/view/View;
    const v3, 0x7f090027

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    iput-object v3, p0, Lru/cn/tv/UdpProxyDialog;->addressText:Landroid/widget/EditText;

    .line 43
    const v3, 0x7f090173

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    iput-object v3, p0, Lru/cn/tv/UdpProxyDialog;->portText:Landroid/widget/EditText;

    .line 44
    const v3, 0x7f0900b4

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    iput-object v3, p0, Lru/cn/tv/UdpProxyDialog;->enabled:Landroid/widget/CheckBox;

    .line 46
    iget-object v3, p0, Lru/cn/tv/UdpProxyDialog;->addressText:Landroid/widget/EditText;

    iget-object v4, p0, Lru/cn/tv/UdpProxyDialog;->settings:Lru/cn/domain/UDPProxySettings;

    invoke-virtual {v4}, Lru/cn/domain/UDPProxySettings;->getAddress()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 47
    iget-object v4, p0, Lru/cn/tv/UdpProxyDialog;->portText:Landroid/widget/EditText;

    iget-object v3, p0, Lru/cn/tv/UdpProxyDialog;->settings:Lru/cn/domain/UDPProxySettings;

    invoke-virtual {v3}, Lru/cn/domain/UDPProxySettings;->getPort()I

    move-result v3

    if-lez v3, :cond_1

    iget-object v3, p0, Lru/cn/tv/UdpProxyDialog;->settings:Lru/cn/domain/UDPProxySettings;

    invoke-virtual {v3}, Lru/cn/domain/UDPProxySettings;->getPort()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    :goto_0
    invoke-virtual {v4, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 48
    iget-object v3, p0, Lru/cn/tv/UdpProxyDialog;->enabled:Landroid/widget/CheckBox;

    iget-object v4, p0, Lru/cn/tv/UdpProxyDialog;->settings:Lru/cn/domain/UDPProxySettings;

    invoke-virtual {v4}, Lru/cn/domain/UDPProxySettings;->isEnabled()Z

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 50
    iget-object v3, p0, Lru/cn/tv/UdpProxyDialog;->enabled:Landroid/widget/CheckBox;

    new-instance v4, Lru/cn/tv/UdpProxyDialog$1;

    invoke-direct {v4, p0}, Lru/cn/tv/UdpProxyDialog$1;-><init>(Lru/cn/tv/UdpProxyDialog;)V

    invoke-virtual {v3, v4}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 58
    iget-object v3, p0, Lru/cn/tv/UdpProxyDialog;->addressText:Landroid/widget/EditText;

    iget-object v4, p0, Lru/cn/tv/UdpProxyDialog;->enabled:Landroid/widget/CheckBox;

    invoke-virtual {v4}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 59
    iget-object v3, p0, Lru/cn/tv/UdpProxyDialog;->portText:Landroid/widget/EditText;

    iget-object v4, p0, Lru/cn/tv/UdpProxyDialog;->enabled:Landroid/widget/CheckBox;

    invoke-virtual {v4}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 61
    new-instance v3, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lru/cn/tv/UdpProxyDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v4, 0x7f0e0167

    .line 62
    invoke-virtual {p0, v4}, Lru/cn/tv/UdpProxyDialog;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/support/v7/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v3

    .line 63
    invoke-virtual {v3, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f0e0129

    .line 64
    invoke-virtual {v3, v4, v5}, Landroid/support/v7/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f0e0039

    .line 65
    invoke-virtual {v3, v4, v5}, Landroid/support/v7/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    move-result-object v3

    .line 66
    invoke-virtual {v3}, Landroid/support/v7/app/AlertDialog$Builder;->create()Landroid/support/v7/app/AlertDialog;

    move-result-object v0

    .line 68
    .local v0, "d":Landroid/support/v7/app/AlertDialog;
    iget-object v3, p0, Lru/cn/tv/UdpProxyDialog;->settings:Lru/cn/domain/UDPProxySettings;

    invoke-virtual {v3}, Lru/cn/domain/UDPProxySettings;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 69
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v3

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 72
    :cond_0
    return-object v0

    .line 47
    .end local v0    # "d":Landroid/support/v7/app/AlertDialog;
    :cond_1
    const-string v3, ""

    goto :goto_0
.end method

.method public onStart()V
    .locals 3

    .prologue
    .line 77
    invoke-super {p0}, Landroid/support/v4/app/DialogFragment;->onStart()V

    .line 78
    invoke-virtual {p0}, Lru/cn/tv/UdpProxyDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/support/v7/app/AlertDialog;

    .line 79
    .local v0, "d":Landroid/support/v7/app/AlertDialog;
    if-eqz v0, :cond_0

    .line 80
    const/4 v2, -0x1

    invoke-virtual {v0, v2}, Landroid/support/v7/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    .line 81
    .local v1, "positiveButton":Landroid/widget/Button;
    iget-object v2, p0, Lru/cn/tv/UdpProxyDialog;->saveClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 83
    .end local v1    # "positiveButton":Landroid/widget/Button;
    :cond_0
    return-void
.end method
