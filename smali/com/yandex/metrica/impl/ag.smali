.class public Lcom/yandex/metrica/impl/ag;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/yandex/metrica/impl/ae;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/yandex/metrica/impl/ag$a;
    }
.end annotation


# static fields
.field private static final c:Ljava/util/concurrent/Executor;

.field private static final d:Ljava/util/concurrent/ExecutorService;

.field private static final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/yandex/metrica/impl/ob/v;",
            ">;"
        }
    .end annotation
.end field

.field private static final f:Lcom/yandex/metrica/impl/ob/s;


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lcom/yandex/metrica/MetricaService$d;

.field private final g:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljava/lang/Thread;

.field private i:Lcom/yandex/metrica/impl/ob/eb;

.field private j:Lcom/yandex/metrica/impl/ob/gi;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 73
    new-instance v0, Lcom/yandex/metrica/impl/ob/er;

    invoke-direct {v0}, Lcom/yandex/metrica/impl/ob/er;-><init>()V

    sput-object v0, Lcom/yandex/metrica/impl/ag;->c:Ljava/util/concurrent/Executor;

    .line 74
    new-instance v0, Lcom/yandex/metrica/impl/utils/j;

    const-string v1, "YMM-MSTE"

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/utils/j;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    sput-object v0, Lcom/yandex/metrica/impl/ag;->d:Ljava/util/concurrent/ExecutorService;

    .line 76
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/yandex/metrica/impl/ag;->e:Ljava/util/Map;

    .line 77
    new-instance v0, Lcom/yandex/metrica/impl/ob/s;

    invoke-direct {v0}, Lcom/yandex/metrica/impl/ob/s;-><init>()V

    sput-object v0, Lcom/yandex/metrica/impl/ag;->f:Lcom/yandex/metrica/impl/ob/s;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/yandex/metrica/MetricaService$d;)V
    .locals 1

    .prologue
    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/yandex/metrica/impl/ag;->g:Ljava/util/Set;

    .line 87
    iput-object p1, p0, Lcom/yandex/metrica/impl/ag;->a:Landroid/content/Context;

    .line 88
    iput-object p2, p0, Lcom/yandex/metrica/impl/ag;->b:Lcom/yandex/metrica/MetricaService$d;

    .line 89
    return-void
.end method

.method private a(ILcom/yandex/metrica/impl/i;Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 573
    invoke-virtual {p2}, Lcom/yandex/metrica/impl/i;->n()Z

    move-result v0

    if-nez v0, :cond_0

    .line 574
    new-instance v0, Lcom/yandex/metrica/impl/ag$a;

    iget-object v2, p0, Lcom/yandex/metrica/impl/ag;->a:Landroid/content/Context;

    move-object v1, p0

    move-object v3, p2

    move-object v4, p3

    move v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/yandex/metrica/impl/ag$a;-><init>(Lcom/yandex/metrica/impl/ag;Landroid/content/Context;Lcom/yandex/metrica/impl/i;Landroid/os/Bundle;I)V

    .line 575
    sget-object v1, Lcom/yandex/metrica/impl/ag;->d:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v1, v0}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 577
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/yandex/metrica/CounterConfiguration;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 60
    invoke-static {p0, p1}, Lcom/yandex/metrica/impl/ag;->b(Lcom/yandex/metrica/CounterConfiguration;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/yandex/metrica/impl/ag;)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/yandex/metrica/impl/ag;->f()V

    return-void
.end method

.method static synthetic a(Lcom/yandex/metrica/impl/ag;Lcom/yandex/metrica/impl/ob/dv;Lcom/yandex/metrica/impl/ob/dq;)V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Lcom/yandex/metrica/impl/ag;->a(Lcom/yandex/metrica/impl/ob/dv;Lcom/yandex/metrica/impl/ob/dq;)V

    return-void
.end method

.method private a(Lcom/yandex/metrica/impl/ob/dv;Lcom/yandex/metrica/impl/ob/dq;)V
    .locals 1

    .prologue
    .line 593
    invoke-direct {p0}, Lcom/yandex/metrica/impl/ag;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 594
    :goto_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ag;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/yandex/metrica/impl/ob/eb;->a(Landroid/content/Context;)Lcom/yandex/metrica/impl/ob/eb;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/yandex/metrica/impl/ob/eb;->a(Lcom/yandex/metrica/impl/ob/dv;)V

    .line 595
    return-void

    :cond_0
    move-object p2, p1

    .line 593
    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 201
    invoke-direct {p0}, Lcom/yandex/metrica/impl/ag;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4312
    iget-object v0, p0, Lcom/yandex/metrica/impl/ag;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/yandex/metrica/impl/ob/eb;->a(Landroid/content/Context;)Lcom/yandex/metrica/impl/ob/eb;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/impl/ag;->i:Lcom/yandex/metrica/impl/ob/eb;

    .line 4313
    iget-object v0, p0, Lcom/yandex/metrica/impl/ag;->i:Lcom/yandex/metrica/impl/ob/eb;

    invoke-virtual {v0, p0}, Lcom/yandex/metrica/impl/ob/eb;->a(Ljava/lang/Object;)V

    .line 4317
    iget-object v0, p0, Lcom/yandex/metrica/impl/ag;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/yandex/metrica/impl/ob/gi;->a(Landroid/content/Context;)Lcom/yandex/metrica/impl/ob/gi;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/impl/ag;->j:Lcom/yandex/metrica/impl/ob/gi;

    .line 4318
    iget-object v0, p0, Lcom/yandex/metrica/impl/ag;->j:Lcom/yandex/metrica/impl/ob/gi;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/gi;->a()V

    .line 205
    :cond_0
    invoke-direct {p0}, Lcom/yandex/metrica/impl/ag;->g()Z

    move-result v0

    if-nez v0, :cond_1

    .line 207
    iget-object v0, p0, Lcom/yandex/metrica/impl/ag;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/yandex/metrica/impl/ob/fu;->a(Landroid/content/Context;)Lcom/yandex/metrica/impl/ob/fu;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fu;->a()V

    .line 210
    :cond_1
    iget-object v0, p0, Lcom/yandex/metrica/impl/ag;->g:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const-string v0, "com.yandex.metrica.ACTION_C_BG_L"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 211
    :cond_2
    iget-object v0, p0, Lcom/yandex/metrica/impl/ag;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/yandex/metrica/impl/ob/eb;->a(Landroid/content/Context;)Lcom/yandex/metrica/impl/ob/eb;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/eb;->a()V

    .line 213
    :cond_3
    return-void
.end method

.method static synthetic a(Lcom/yandex/metrica/impl/ob/v;)Z
    .locals 1

    .prologue
    .line 60
    .line 7487
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/v;->p()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    .line 60
    goto :goto_0
.end method

.method static synthetic a(Lcom/yandex/metrica/impl/ob/v;Lcom/yandex/metrica/impl/i;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 60
    .line 7512
    sget-object v1, Lcom/yandex/metrica/impl/q$a;->q:Lcom/yandex/metrica/impl/q$a;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/q$a;->a()I

    move-result v1

    invoke-virtual {p1}, Lcom/yandex/metrica/impl/i;->c()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 7513
    invoke-virtual {p0}, Lcom/yandex/metrica/impl/ob/v;->f()V

    .line 7517
    :goto_0
    return v0

    .line 7515
    :cond_0
    sget-object v1, Lcom/yandex/metrica/impl/q$a;->t:Lcom/yandex/metrica/impl/q$a;

    invoke-virtual {v1}, Lcom/yandex/metrica/impl/q$a;->a()I

    move-result v1

    invoke-virtual {p1}, Lcom/yandex/metrica/impl/i;->c()I

    move-result v2

    if-ne v1, v2, :cond_1

    .line 7516
    invoke-virtual {p0, p1}, Lcom/yandex/metrica/impl/ob/v;->b(Lcom/yandex/metrica/impl/i;)V

    goto :goto_0

    .line 7519
    :cond_1
    const/4 v0, 0x0

    .line 60
    goto :goto_0
.end method

.method private b(Landroid/content/Intent;I)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 268
    if-eqz p1, :cond_2

    .line 270
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-class v3, Lcom/yandex/metrica/CounterConfiguration;

    invoke-virtual {v3}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 5483
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_3

    :cond_0
    move v0, v1

    .line 4523
    :goto_0
    if-nez v0, :cond_2

    .line 4527
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lcom/yandex/metrica/impl/i;->b(Landroid/os/Bundle;)Lcom/yandex/metrica/impl/i;

    move-result-object v6

    .line 4530
    invoke-virtual {v6}, Lcom/yandex/metrica/impl/i;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4531
    const-string v0, "EXTRA_KEY_KEY_START_TYPE"

    sget-object v3, Lcom/yandex/metrica/impl/q$a;->a:Lcom/yandex/metrica/impl/q$a;

    .line 4532
    invoke-virtual {v3}, Lcom/yandex/metrica/impl/q$a;->a()I

    move-result v3

    .line 4531
    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 4533
    const-string v3, "EXTRA_KEY_KEY_START_EVENT"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 4534
    invoke-virtual {v6, v0}, Lcom/yandex/metrica/impl/i;->a(I)Lcom/yandex/metrica/impl/i;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/yandex/metrica/impl/i;->b(Ljava/lang/String;)Lcom/yandex/metrica/impl/i;

    move-result-object v0

    const-string v3, ""

    invoke-virtual {v0, v3}, Lcom/yandex/metrica/impl/i;->c(Ljava/lang/String;)Lcom/yandex/metrica/impl/i;

    .line 4537
    :cond_1
    invoke-virtual {v6}, Lcom/yandex/metrica/impl/i;->m()Z

    move-result v0

    .line 4538
    invoke-virtual {v6}, Lcom/yandex/metrica/impl/i;->n()Z

    move-result v3

    or-int/2addr v0, v3

    .line 4539
    if-nez v0, :cond_2

    .line 4545
    const-string v0, "EXTRA_KEY_LIB_CFG"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 4546
    if-eqz v0, :cond_4

    .line 4547
    :goto_1
    invoke-static {v0}, Lcom/yandex/metrica/CounterConfiguration;->b(Landroid/os/Bundle;)Lcom/yandex/metrica/CounterConfiguration;

    move-result-object v4

    .line 5491
    if-nez v4, :cond_5

    move v0, v1

    .line 4548
    :goto_2
    if-nez v0, :cond_2

    .line 4552
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getEncodedAuthority()Ljava/lang/String;

    move-result-object v0

    .line 4554
    invoke-static {v4, v0}, Lcom/yandex/metrica/impl/ag;->b(Lcom/yandex/metrica/CounterConfiguration;Ljava/lang/String;)V

    .line 4556
    invoke-virtual {p0, v4}, Lcom/yandex/metrica/impl/ag;->a(Lcom/yandex/metrica/CounterConfiguration;)V

    .line 4561
    :try_start_0
    iget-object v1, p0, Lcom/yandex/metrica/impl/ag;->a:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-static {v1, v4, v2, v0}, Lcom/yandex/metrica/impl/ob/t;->a(Landroid/content/Context;Lcom/yandex/metrica/CounterConfiguration;Ljava/lang/Integer;Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/t;

    move-result-object v3

    .line 4562
    new-instance v0, Lcom/yandex/metrica/impl/ob/v;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ag;->a:Landroid/content/Context;

    sget-object v2, Lcom/yandex/metrica/impl/ag;->c:Ljava/util/concurrent/Executor;

    sget-object v5, Lcom/yandex/metrica/impl/ag;->f:Lcom/yandex/metrica/impl/ob/s;

    invoke-direct/range {v0 .. v5}, Lcom/yandex/metrica/impl/ob/v;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Lcom/yandex/metrica/impl/ob/t;Lcom/yandex/metrica/CounterConfiguration;Lcom/yandex/metrica/impl/ob/s;)V

    .line 4563
    invoke-virtual {v0, v6}, Lcom/yandex/metrica/impl/ob/v;->a(Lcom/yandex/metrica/impl/i;)V

    .line 4565
    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/v;->e()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 276
    :cond_2
    :goto_3
    iget-object v0, p0, Lcom/yandex/metrica/impl/ag;->b:Lcom/yandex/metrica/MetricaService$d;

    invoke-interface {v0, p2}, Lcom/yandex/metrica/MetricaService$d;->a(I)V

    .line 277
    return-void

    :cond_3
    move v0, v2

    .line 5483
    goto :goto_0

    .line 4546
    :cond_4
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    goto :goto_1

    :cond_5
    move v0, v2

    .line 5491
    goto :goto_2

    :catch_0
    move-exception v0

    goto :goto_3
.end method

.method private static b(Lcom/yandex/metrica/CounterConfiguration;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 477
    invoke-virtual {p0}, Lcom/yandex/metrica/CounterConfiguration;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 478
    invoke-virtual {p0, p1}, Lcom/yandex/metrica/CounterConfiguration;->c(Ljava/lang/String;)V

    .line 480
    :cond_0
    return-void
.end method

.method private b(Lcom/yandex/metrica/impl/ob/dg;)V
    .locals 2

    .prologue
    .line 586
    invoke-static {p1}, Lcom/yandex/metrica/impl/ob/dv;->b(Lcom/yandex/metrica/impl/ob/dg;)Lcom/yandex/metrica/impl/ob/dv;

    move-result-object v0

    .line 587
    invoke-static {p1}, Lcom/yandex/metrica/impl/ob/dq;->a(Lcom/yandex/metrica/impl/ob/dg;)Lcom/yandex/metrica/impl/ob/dq;

    move-result-object v1

    .line 588
    invoke-direct {p0, v0, v1}, Lcom/yandex/metrica/impl/ag;->a(Lcom/yandex/metrica/impl/ob/dv;Lcom/yandex/metrica/impl/ob/dq;)V

    .line 589
    return-void
.end method

.method static synthetic b(Lcom/yandex/metrica/CounterConfiguration;)Z
    .locals 1

    .prologue
    .line 60
    .line 6491
    if-nez p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    .line 60
    goto :goto_0
.end method

.method static synthetic c()Ljava/util/Map;
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lcom/yandex/metrica/impl/ag;->e:Ljava/util/Map;

    return-object v0
.end method

.method private c(Lcom/yandex/metrica/CounterConfiguration;)V
    .locals 2

    .prologue
    .line 470
    iget-object v0, p0, Lcom/yandex/metrica/impl/ag;->a:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/yandex/metrica/CounterConfiguration;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/yandex/metrica/impl/ob/dl;->h(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 471
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 472
    invoke-virtual {p1, v0}, Lcom/yandex/metrica/CounterConfiguration;->e(Ljava/lang/String;)V

    .line 474
    :cond_0
    return-void
.end method

.method private d()V
    .locals 1

    .prologue
    .line 297
    iget-object v0, p0, Lcom/yandex/metrica/impl/ag;->i:Lcom/yandex/metrica/impl/ob/eb;

    if-eqz v0, :cond_0

    .line 298
    iget-object v0, p0, Lcom/yandex/metrica/impl/ag;->i:Lcom/yandex/metrica/impl/ob/eb;

    invoke-virtual {v0, p0}, Lcom/yandex/metrica/impl/ob/eb;->b(Ljava/lang/Object;)V

    .line 300
    :cond_0
    iget-object v0, p0, Lcom/yandex/metrica/impl/ag;->j:Lcom/yandex/metrica/impl/ob/gi;

    if-eqz v0, :cond_1

    .line 301
    iget-object v0, p0, Lcom/yandex/metrica/impl/ag;->j:Lcom/yandex/metrica/impl/ob/gi;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/gi;->b()V

    .line 303
    :cond_1
    return-void
.end method

.method private e()V
    .locals 3

    .prologue
    .line 580
    iget-object v0, p0, Lcom/yandex/metrica/impl/ag;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/yandex/metrica/impl/ob/cq;->a(Landroid/content/Context;)Lcom/yandex/metrica/impl/ob/cq;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/cq;->d()Lcom/yandex/metrica/impl/ob/cr;

    move-result-object v0

    .line 581
    new-instance v1, Lcom/yandex/metrica/impl/ob/dg;

    iget-object v2, p0, Lcom/yandex/metrica/impl/ag;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/yandex/metrica/impl/ob/dg;-><init>(Lcom/yandex/metrica/impl/ob/cr;Ljava/lang/String;)V

    .line 582
    invoke-direct {p0, v1}, Lcom/yandex/metrica/impl/ag;->b(Lcom/yandex/metrica/impl/ob/dg;)V

    .line 583
    return-void
.end method

.method private f()V
    .locals 3

    .prologue
    .line 599
    new-instance v0, Lcom/yandex/metrica/impl/ob/bs;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ag;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/ob/bs;-><init>(Landroid/content/Context;)V

    .line 600
    const-string v1, "YMM-CSL"

    new-instance v2, Lcom/yandex/metrica/impl/ag$2;

    invoke-direct {v2, v0}, Lcom/yandex/metrica/impl/ag$2;-><init>(Lcom/yandex/metrica/impl/ob/bs;)V

    invoke-static {v1, v2}, Lcom/yandex/metrica/impl/utils/j;->a(Ljava/lang/String;Ljava/lang/Runnable;)Ljava/lang/Thread;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/metrica/impl/ag;->h:Ljava/lang/Thread;

    .line 607
    iget-object v0, p0, Lcom/yandex/metrica/impl/ag;->h:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 608
    return-void
.end method

.method private g()Z
    .locals 2

    .prologue
    .line 611
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ag;->g:Ljava/util/Set;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 614
    sget-object v1, Lcom/yandex/metrica/MetricaService$a;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 615
    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private h()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 623
    iget-object v1, p0, Lcom/yandex/metrica/impl/ag;->g:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    if-gt v1, v0, :cond_0

    iget-object v1, p0, Lcom/yandex/metrica/impl/ag;->g:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    if-ne v1, v0, :cond_1

    iget-object v1, p0, Lcom/yandex/metrica/impl/ag;->g:Ljava/util/Set;

    const-string v2, "com.yandex.metrica.ACTION_BIND_TO_LOCAL_SERVER"

    .line 624
    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    .line 623
    goto :goto_0
.end method


# virtual methods
.method a(Lcom/yandex/metrica/impl/i;Lcom/yandex/metrica/CounterConfiguration;I)Lcom/yandex/metrica/impl/ob/t;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 498
    .line 499
    invoke-static {p1}, Lcom/yandex/metrica/impl/q;->a(Lcom/yandex/metrica/impl/i;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 500
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/i;->l()Ljava/lang/String;

    move-result-object v3

    .line 502
    iget-object v0, p0, Lcom/yandex/metrica/impl/ag;->a:Landroid/content/Context;

    .line 6329
    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 6330
    invoke-virtual {v0, v2}, Landroid/content/pm/PackageManager;->getInstalledApplications(I)Ljava/util/List;

    move-result-object v0

    .line 6331
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ApplicationInfo;

    .line 6332
    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6333
    const/4 v0, 0x1

    .line 502
    :goto_0
    if-eqz v0, :cond_3

    .line 503
    invoke-static {v3}, Lcom/yandex/metrica/impl/ob/t;->a(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/t;

    move-result-object v0

    .line 508
    :goto_1
    return-object v0

    :cond_1
    move v0, v2

    .line 6336
    goto :goto_0

    .line 506
    :cond_2
    iget-object v0, p0, Lcom/yandex/metrica/impl/ag;->a:Landroid/content/Context;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, p2, v2, v1}, Lcom/yandex/metrica/impl/ob/t;->a(Landroid/content/Context;Lcom/yandex/metrica/CounterConfiguration;Ljava/lang/Integer;Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/t;

    move-result-object v0

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_1
.end method

.method a(Lcom/yandex/metrica/impl/ob/t;Lcom/yandex/metrica/CounterConfiguration;Lcom/yandex/metrica/impl/i;)Lcom/yandex/metrica/impl/ob/v;
    .locals 6

    .prologue
    .line 340
    if-eqz p1, :cond_3

    .line 341
    sget-object v0, Lcom/yandex/metrica/impl/ag;->e:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ob/t;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/yandex/metrica/impl/ob/v;

    .line 343
    if-nez v0, :cond_2

    .line 344
    new-instance v0, Lcom/yandex/metrica/impl/ob/v;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ag;->a:Landroid/content/Context;

    sget-object v2, Lcom/yandex/metrica/impl/ag;->c:Ljava/util/concurrent/Executor;

    sget-object v5, Lcom/yandex/metrica/impl/ag;->f:Lcom/yandex/metrica/impl/ob/s;

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/yandex/metrica/impl/ob/v;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Lcom/yandex/metrica/impl/ob/t;Lcom/yandex/metrica/CounterConfiguration;Lcom/yandex/metrica/impl/ob/s;)V

    .line 347
    if-eqz p3, :cond_0

    invoke-static {p3}, Lcom/yandex/metrica/impl/q;->a(Lcom/yandex/metrica/impl/i;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 348
    :cond_0
    sget-object v1, Lcom/yandex/metrica/impl/ag;->e:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ob/t;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 359
    :cond_1
    :goto_0
    return-object v0

    .line 353
    :cond_2
    invoke-virtual {v0, p2}, Lcom/yandex/metrica/impl/ob/v;->b(Lcom/yandex/metrica/CounterConfiguration;)V

    goto :goto_0

    .line 359
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 94
    new-instance v0, Lcom/yandex/metrica/impl/be;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ag;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/yandex/metrica/impl/be;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/yandex/metrica/impl/ag;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/be;->a(Landroid/content/Context;)V

    .line 96
    invoke-static {}, Lcom/yandex/metrica/impl/utils/n;->a()Lcom/yandex/metrica/impl/utils/n;

    move-result-object v0

    iget-object v1, p0, Lcom/yandex/metrica/impl/ag;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/utils/n;->a(Landroid/content/Context;)V

    .line 1061
    sget-object v0, Lcom/yandex/metrica/impl/GoogleAdvertisingIdGetter$c;->a:Lcom/yandex/metrica/impl/GoogleAdvertisingIdGetter;

    .line 97
    iget-object v1, p0, Lcom/yandex/metrica/impl/ag;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/GoogleAdvertisingIdGetter;->a(Landroid/content/Context;)V

    .line 1216
    iget-object v0, p0, Lcom/yandex/metrica/impl/ag;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/yandex/metrica/impl/ob/cq;->a(Landroid/content/Context;)Lcom/yandex/metrica/impl/ob/cq;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/cq;->d()Lcom/yandex/metrica/impl/ob/cr;

    move-result-object v0

    .line 1217
    new-instance v1, Lcom/yandex/metrica/impl/ob/dg;

    iget-object v2, p0, Lcom/yandex/metrica/impl/ag;->a:Landroid/content/Context;

    .line 1218
    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/yandex/metrica/impl/ob/dg;-><init>(Lcom/yandex/metrica/impl/ob/cr;Ljava/lang/String;)V

    .line 1247
    invoke-static {}, Lcom/yandex/metrica/impl/ob/eq;->a()Lcom/yandex/metrica/impl/ob/eq;

    move-result-object v0

    iget-object v2, p0, Lcom/yandex/metrica/impl/ag;->a:Landroid/content/Context;

    .line 1248
    invoke-virtual {v1, v4}, Lcom/yandex/metrica/impl/ob/dg;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1249
    invoke-virtual {v1, v4}, Lcom/yandex/metrica/impl/ob/dg;->h(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1248
    invoke-virtual {v0, v2, v3, v4}, Lcom/yandex/metrica/impl/ob/eq;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 1221
    invoke-virtual {p0, v1}, Lcom/yandex/metrica/impl/ag;->a(Lcom/yandex/metrica/impl/ob/dg;)V

    .line 1223
    invoke-direct {p0}, Lcom/yandex/metrica/impl/ag;->f()V

    .line 1224
    invoke-direct {p0, v1}, Lcom/yandex/metrica/impl/ag;->b(Lcom/yandex/metrica/impl/ob/dg;)V

    .line 2230
    invoke-static {}, Lcom/yandex/metrica/impl/ob/g;->a()Lcom/yandex/metrica/impl/ob/g;

    move-result-object v0

    const-class v1, Lcom/yandex/metrica/impl/ob/o;

    new-instance v2, Lcom/yandex/metrica/impl/ag$1;

    invoke-direct {v2, p0}, Lcom/yandex/metrica/impl/ag$1;-><init>(Lcom/yandex/metrica/impl/ag;)V

    .line 2231
    invoke-static {v2}, Lcom/yandex/metrica/impl/ob/k;->a(Lcom/yandex/metrica/impl/ob/j;)Lcom/yandex/metrica/impl/ob/k$a;

    move-result-object v2

    .line 2237
    invoke-virtual {v2}, Lcom/yandex/metrica/impl/ob/k$a;->a()Lcom/yandex/metrica/impl/ob/k;

    move-result-object v2

    .line 2230
    invoke-virtual {v0, p0, v1, v2}, Lcom/yandex/metrica/impl/ob/g;->a(Ljava/lang/Object;Ljava/lang/Class;Lcom/yandex/metrica/impl/ob/k;)V

    .line 101
    invoke-static {}, Lcom/yandex/metrica/impl/ob/dl;->a()Lcom/yandex/metrica/impl/ob/dl;

    move-result-object v0

    iget-object v1, p0, Lcom/yandex/metrica/impl/ag;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/dl;->a(Landroid/content/Context;)Ljava/lang/String;

    .line 102
    return-void
.end method

.method public a(ILandroid/os/Bundle;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 196
    const-class v0, Lcom/yandex/metrica/CounterConfiguration;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 197
    invoke-static {p2}, Lcom/yandex/metrica/impl/i;->b(Landroid/os/Bundle;)Lcom/yandex/metrica/impl/i;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/yandex/metrica/impl/ag;->a(ILcom/yandex/metrica/impl/i;Landroid/os/Bundle;)V

    .line 198
    return-void
.end method

.method public a(ILjava/lang/String;ILjava/lang/String;Landroid/os/Bundle;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 188
    const-class v0, Lcom/yandex/metrica/CounterConfiguration;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p5, v0}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 189
    new-instance v0, Lcom/yandex/metrica/impl/i;

    invoke-direct {v0, p4, p2, p3}, Lcom/yandex/metrica/impl/i;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-direct {p0, p1, v0, p5}, Lcom/yandex/metrica/impl/ag;->a(ILcom/yandex/metrica/impl/i;Landroid/os/Bundle;)V

    .line 190
    return-void
.end method

.method public a(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 119
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 120
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 121
    iget-object v1, p0, Lcom/yandex/metrica/impl/ag;->g:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 123
    invoke-direct {p0}, Lcom/yandex/metrica/impl/ag;->e()V

    .line 126
    :cond_0
    invoke-direct {p0, v0}, Lcom/yandex/metrica/impl/ag;->a(Ljava/lang/String;)V

    .line 127
    return-void
.end method

.method public a(Landroid/content/Intent;I)V
    .locals 0

    .prologue
    .line 107
    invoke-direct {p0, p1, p2}, Lcom/yandex/metrica/impl/ag;->b(Landroid/content/Intent;I)V

    .line 108
    return-void
.end method

.method public a(Landroid/content/Intent;II)V
    .locals 0

    .prologue
    .line 113
    invoke-direct {p0, p1, p3}, Lcom/yandex/metrica/impl/ag;->b(Landroid/content/Intent;I)V

    .line 114
    return-void
.end method

.method a(Lcom/yandex/metrica/CounterConfiguration;)V
    .locals 2

    .prologue
    .line 459
    invoke-virtual {p1}, Lcom/yandex/metrica/CounterConfiguration;->h()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 460
    invoke-direct {p0, p1}, Lcom/yandex/metrica/impl/ag;->c(Lcom/yandex/metrica/CounterConfiguration;)V

    .line 467
    :cond_0
    :goto_0
    return-void

    .line 462
    :cond_1
    invoke-static {}, Lcom/yandex/metrica/impl/ob/dl;->a()Lcom/yandex/metrica/impl/ob/dl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/dl;->d()Ljava/lang/String;

    move-result-object v0

    .line 463
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p1}, Lcom/yandex/metrica/CounterConfiguration;->h()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_0

    .line 464
    invoke-direct {p0, p1}, Lcom/yandex/metrica/impl/ag;->c(Lcom/yandex/metrica/CounterConfiguration;)V

    goto :goto_0

    .line 463
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method a(Lcom/yandex/metrica/impl/ob/dg;)V
    .locals 4

    .prologue
    .line 253
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ob/dg;->n()Ljava/lang/String;

    move-result-object v0

    .line 254
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 255
    invoke-static {}, Lcom/yandex/metrica/impl/ob/g;->a()Lcom/yandex/metrica/impl/ob/g;

    move-result-object v0

    const-class v1, Lcom/yandex/metrica/impl/ob/p;

    invoke-virtual {v0, v1}, Lcom/yandex/metrica/impl/ob/g;->a(Ljava/lang/Class;)V

    .line 263
    :goto_0
    return-void

    .line 258
    :cond_0
    :try_start_0
    invoke-static {}, Lcom/yandex/metrica/impl/ob/g;->a()Lcom/yandex/metrica/impl/ob/g;

    move-result-object v1

    new-instance v2, Lcom/yandex/metrica/impl/ob/p;

    new-instance v3, Lcom/yandex/metrica/impl/ob/fv;

    invoke-direct {v3, v0}, Lcom/yandex/metrica/impl/ob/fv;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v3}, Lcom/yandex/metrica/impl/ob/p;-><init>(Lcom/yandex/metrica/impl/ob/fv;)V

    invoke-virtual {v1, v2}, Lcom/yandex/metrica/impl/ob/g;->b(Lcom/yandex/metrica/impl/ob/i;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 263
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method a(ZZ)V
    .locals 1

    .prologue
    .line 323
    iget-object v0, p0, Lcom/yandex/metrica/impl/ag;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/yandex/metrica/impl/ob/eb;->a(Landroid/content/Context;)Lcom/yandex/metrica/impl/ob/eb;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p2}, Lcom/yandex/metrica/impl/ob/eb;->a(Ljava/lang/Object;ZZ)V

    .line 324
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 181
    invoke-direct {p0}, Lcom/yandex/metrica/impl/ag;->d()V

    .line 182
    return-void
.end method

.method public b(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 132
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 133
    invoke-static {v0}, Lcom/yandex/metrica/impl/bj;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 134
    iget-object v1, p0, Lcom/yandex/metrica/impl/ag;->g:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 136
    invoke-direct {p0}, Lcom/yandex/metrica/impl/ag;->e()V

    .line 139
    :cond_0
    invoke-direct {p0, v0}, Lcom/yandex/metrica/impl/ag;->a(Ljava/lang/String;)V

    .line 140
    return-void
.end method

.method public c(Landroid/content/Intent;)V
    .locals 6

    .prologue
    .line 145
    if-eqz p1, :cond_8

    .line 146
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 147
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_5

    const/4 v0, 0x0

    move-object v2, v0

    .line 151
    :goto_0
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 152
    iget-object v0, p0, Lcom/yandex/metrica/impl/ag;->g:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 154
    invoke-direct {p0}, Lcom/yandex/metrica/impl/ag;->e()V

    .line 157
    :cond_0
    invoke-direct {p0}, Lcom/yandex/metrica/impl/ag;->h()Z

    move-result v0

    if-nez v0, :cond_1

    .line 158
    invoke-direct {p0}, Lcom/yandex/metrica/impl/ag;->d()V

    .line 2619
    :cond_1
    sget-object v0, Lcom/yandex/metrica/MetricaService$a;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    .line 161
    if-nez v0, :cond_8

    .line 162
    invoke-direct {p0}, Lcom/yandex/metrica/impl/ag;->g()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 164
    iget-object v0, p0, Lcom/yandex/metrica/impl/ag;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/yandex/metrica/impl/ob/fu;->a(Landroid/content/Context;)Lcom/yandex/metrica/impl/ob/fu;

    move-result-object v0

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/fu;->b()V

    .line 167
    :cond_2
    if-eqz v2, :cond_8

    .line 168
    sget-object v4, Lcom/yandex/metrica/impl/ag;->e:Ljava/util/Map;

    monitor-enter v4

    .line 3280
    :try_start_0
    new-instance v0, Ljava/util/HashMap;

    sget-object v1, Lcom/yandex/metrica/impl/ag;->e:Ljava/util/Map;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    .line 3282
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_3
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 3283
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 3284
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/yandex/metrica/impl/ob/v;

    .line 3331
    if-eqz v1, :cond_4

    if-eqz v0, :cond_4

    .line 3333
    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    :cond_4
    const/4 v3, 0x1

    .line 3286
    :goto_2
    if-eqz v3, :cond_3

    .line 3287
    sget-object v3, Lcom/yandex/metrica/impl/ag;->e:Ljava/util/Map;

    invoke-interface {v3, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3288
    if-eqz v0, :cond_3

    .line 3289
    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/v;->d()V

    goto :goto_1

    .line 172
    :catchall_0
    move-exception v0

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 147
    :cond_5
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getEncodedAuthority()Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    goto :goto_0

    .line 3333
    :cond_6
    const/4 v3, 0x0

    goto :goto_2

    .line 172
    :cond_7
    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 176
    :cond_8
    return-void
.end method
