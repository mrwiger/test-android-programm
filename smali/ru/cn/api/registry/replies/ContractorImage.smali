.class public Lru/cn/api/registry/replies/ContractorImage;
.super Ljava/lang/Object;
.source "ContractorImage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/api/registry/replies/ContractorImage$ProfileType;
    }
.end annotation


# instance fields
.field private height:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "height"
    .end annotation
.end field

.field private profileType:Lru/cn/api/registry/replies/ContractorImage$ProfileType;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "profile"
    .end annotation
.end field

.field private url:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "URL"
    .end annotation
.end field

.field private width:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "width"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput v0, p0, Lru/cn/api/registry/replies/ContractorImage;->width:I

    .line 39
    iput v0, p0, Lru/cn/api/registry/replies/ContractorImage;->height:I

    return-void
.end method


# virtual methods
.method public getProfileType()Lru/cn/api/registry/replies/ContractorImage$ProfileType;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lru/cn/api/registry/replies/ContractorImage;->profileType:Lru/cn/api/registry/replies/ContractorImage$ProfileType;

    return-object v0
.end method

.method public getUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lru/cn/api/registry/replies/ContractorImage;->url:Ljava/lang/String;

    return-object v0
.end method
