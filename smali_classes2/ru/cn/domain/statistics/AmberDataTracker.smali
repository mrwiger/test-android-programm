.class Lru/cn/domain/statistics/AmberDataTracker;
.super Ljava/lang/Object;
.source "AmberDataTracker.java"


# instance fields
.field private account:Ljava/lang/String;

.field private final appId:Ljava/lang/String;

.field private final prefs:Landroid/content/SharedPreferences;


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appId"    # Ljava/lang/String;

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const-string v0, "amber_data_prefs"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lru/cn/domain/statistics/AmberDataTracker;->prefs:Landroid/content/SharedPreferences;

    .line 25
    iget-object v0, p0, Lru/cn/domain/statistics/AmberDataTracker;->prefs:Landroid/content/SharedPreferences;

    const-string v1, "amber_data_account_1"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lru/cn/domain/statistics/AmberDataTracker;->account:Ljava/lang/String;

    .line 26
    iput-object p2, p0, Lru/cn/domain/statistics/AmberDataTracker;->appId:Ljava/lang/String;

    .line 27
    return-void
.end method


# virtual methods
.method trackLogin(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1, "userId"    # Ljava/lang/String;
    .param p2, "androidId"    # Ljava/lang/String;
    .param p3, "adId"    # Ljava/lang/String;
    .param p4, "ipv4"    # Ljava/lang/String;
    .param p5, "userAgent"    # Ljava/lang/String;

    .prologue
    .line 35
    iget-object v0, p0, Lru/cn/domain/statistics/AmberDataTracker;->account:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lru/cn/domain/statistics/AmberDataTracker;->account:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 48
    :goto_0
    return-void

    .line 38
    :cond_0
    iget-object v0, p0, Lru/cn/domain/statistics/AmberDataTracker;->appId:Ljava/lang/String;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-static/range {v0 .. v5}, Lru/cn/domain/statistics/AmberDataPixel;->convertParams(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v6

    .line 39
    .local v6, "params":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    invoke-static {v6}, Lru/cn/domain/statistics/AmberDataPixel;->trackingUrl(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v7

    .line 41
    .local v7, "trackingUrl":Ljava/lang/String;
    invoke-static {v7, p5}, Lru/cn/utils/http/HttpClient;->sendRequestAsync(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    const-string v0, "AmberData_Login"

    invoke-static {v0, v6}, Lru/cn/domain/statistics/inetra/InetraTracker;->tracking(Ljava/lang/String;Ljava/util/Map;)V

    .line 44
    iput-object p1, p0, Lru/cn/domain/statistics/AmberDataTracker;->account:Ljava/lang/String;

    .line 45
    iget-object v0, p0, Lru/cn/domain/statistics/AmberDataTracker;->prefs:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "amber_data_account_1"

    .line 46
    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 47
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0
.end method
