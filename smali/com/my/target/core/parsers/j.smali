.class public final Lcom/my/target/core/parsers/j;
.super Ljava/lang/Object;
.source "AudioBannerParser.java"


# instance fields
.field private final F:Lcom/my/target/ae;

.field private final G:Lcom/my/target/be;

.field private final adConfig:Lcom/my/target/b;

.field private final context:Landroid/content/Context;


# direct methods
.method private constructor <init>(Lcom/my/target/al;Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p2, p0, Lcom/my/target/core/parsers/j;->F:Lcom/my/target/ae;

    .line 44
    iput-object p3, p0, Lcom/my/target/core/parsers/j;->adConfig:Lcom/my/target/b;

    .line 45
    iput-object p4, p0, Lcom/my/target/core/parsers/j;->context:Landroid/content/Context;

    .line 46
    invoke-static {p1, p2, p3, p4}, Lcom/my/target/be;->a(Lcom/my/target/ak;Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/be;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/core/parsers/j;->G:Lcom/my/target/be;

    .line 47
    return-void
.end method

.method public static a(Lcom/my/target/al;Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)Lcom/my/target/core/parsers/j;
    .locals 1

    .prologue
    .line 30
    new-instance v0, Lcom/my/target/core/parsers/j;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/my/target/core/parsers/j;-><init>(Lcom/my/target/al;Lcom/my/target/ae;Lcom/my/target/b;Landroid/content/Context;)V

    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 270
    invoke-static {p1}, Lcom/my/target/az;->y(Ljava/lang/String;)Lcom/my/target/az;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/my/target/az;->z(Ljava/lang/String;)Lcom/my/target/az;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/core/parsers/j;->adConfig:Lcom/my/target/b;

    .line 271
    invoke-virtual {v1}, Lcom/my/target/b;->getSlotId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/my/target/az;->h(I)Lcom/my/target/az;

    move-result-object v0

    .line 272
    invoke-virtual {v0, p3}, Lcom/my/target/az;->B(Ljava/lang/String;)Lcom/my/target/az;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/core/parsers/j;->F:Lcom/my/target/ae;

    .line 273
    invoke-virtual {v1}, Lcom/my/target/ae;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/my/target/az;->A(Ljava/lang/String;)Lcom/my/target/az;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/core/parsers/j;->context:Landroid/content/Context;

    .line 274
    invoke-virtual {v0, v1}, Lcom/my/target/az;->e(Landroid/content/Context;)V

    .line 275
    return-void
.end method

.method private b(Lorg/json/JSONObject;Lcom/my/target/aj;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            "Lcom/my/target/aj",
            "<",
            "Lcom/my/target/common/models/AudioData;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 239
    const-string v0, "mediafiles"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 240
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-gtz v0, :cond_2

    .line 242
    :cond_0
    const-string v0, "mediafiles array is empty"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 265
    :cond_1
    :goto_0
    return v1

    :cond_2
    move v0, v1

    .line 246
    :goto_1
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 248
    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    .line 249
    if-eqz v3, :cond_4

    .line 251
    const-string v4, "src"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 252
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 254
    invoke-static {v4}, Lcom/my/target/common/models/AudioData;->newAudioData(Ljava/lang/String;)Lcom/my/target/common/models/AudioData;

    move-result-object v0

    .line 255
    const-string v1, "bitrate"

    invoke-virtual {v3, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/my/target/common/models/AudioData;->setBitrate(I)V

    .line 256
    invoke-virtual {p2, v0}, Lcom/my/target/aj;->setMediaData(Lcom/my/target/ag;)V

    .line 257
    const/4 v1, 0x1

    goto :goto_0

    .line 261
    :cond_3
    const-string v3, "Bad value"

    const-string v5, "bad mediafile object, src = "

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2}, Lcom/my/target/aj;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v3, v4, v5}, Lcom/my/target/core/parsers/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method


# virtual methods
.method public final a(Lorg/json/JSONObject;Lcom/my/target/aj;)Z
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONObject;",
            "Lcom/my/target/aj",
            "<",
            "Lcom/my/target/common/models/AudioData;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 51
    iget-object v0, p0, Lcom/my/target/core/parsers/j;->G:Lcom/my/target/be;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Lcom/my/target/be;->a(Lorg/json/JSONObject;Lcom/my/target/ah;Lcom/my/target/af;)V

    .line 52
    invoke-virtual {p2}, Lcom/my/target/aj;->getType()Ljava/lang/String;

    move-result-object v0

    const-string v1, "statistics"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 54
    const/4 v0, 0x1

    .line 85
    :goto_0
    return v0

    .line 57
    :cond_0
    const-string v0, "duration"

    const-wide/16 v2, 0x0

    invoke-virtual {p1, v0, v2, v3}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v0

    double-to-float v0, v0

    .line 58
    const/4 v1, 0x0

    cmpg-float v1, v0, v1

    if-gtz v1, :cond_1

    .line 60
    const-string v1, "Required field"

    const-string v2, "unable to set duration "

    invoke-static {v0}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/my/target/aj;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v0, v2}, Lcom/my/target/core/parsers/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    const/4 v0, 0x0

    goto :goto_0

    .line 64
    :cond_1
    const-string v0, "autoplay"

    invoke-virtual {p2}, Lcom/my/target/aj;->isAutoPlay()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {p2, v0}, Lcom/my/target/aj;->setAutoPlay(Z)V

    .line 65
    const-string v0, "hasCtaButton"

    invoke-virtual {p2}, Lcom/my/target/aj;->isHasCtaButton()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {p2, v0}, Lcom/my/target/aj;->setHasCtaButton(Z)V

    .line 66
    const-string v0, "adText"

    invoke-virtual {p2}, Lcom/my/target/aj;->getAdText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/my/target/aj;->setAdText(Ljava/lang/String;)V

    .line 1154
    iget-object v0, p0, Lcom/my/target/core/parsers/j;->F:Lcom/my/target/ae;

    invoke-virtual {v0}, Lcom/my/target/ae;->getPoint()F

    move-result v0

    float-to-double v0, v0

    .line 1155
    const-wide/16 v2, 0x0

    cmpg-double v2, v0, v2

    if-gez v2, :cond_2

    .line 1157
    const-string v0, "point"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;)D

    move-result-wide v0

    .line 1160
    :cond_2
    invoke-static {v0, v1}, Ljava/lang/Double;->isNaN(D)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 1162
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    .line 1172
    :cond_3
    :goto_1
    iget-object v2, p0, Lcom/my/target/core/parsers/j;->F:Lcom/my/target/ae;

    invoke-virtual {v2}, Lcom/my/target/ae;->getPointP()F

    move-result v2

    float-to-double v2, v2

    .line 1173
    const-wide/16 v4, 0x0

    cmpg-double v4, v2, v4

    if-gez v4, :cond_4

    .line 1175
    const-string v2, "pointP"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;)D

    move-result-wide v2

    .line 1178
    :cond_4
    invoke-static {v2, v3}, Ljava/lang/Double;->isNaN(D)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 1180
    const-wide/high16 v2, -0x4010000000000000L    # -1.0

    .line 1189
    :cond_5
    :goto_2
    const-wide/16 v4, 0x0

    cmpg-double v4, v0, v4

    if-gez v4, :cond_14

    const-wide/16 v4, 0x0

    cmpg-double v4, v2, v4

    if-gez v4, :cond_14

    .line 1191
    const-wide/high16 v2, -0x4010000000000000L    # -1.0

    .line 1192
    const-wide/high16 v0, 0x4049000000000000L    # 50.0

    .line 1194
    :goto_3
    double-to-float v2, v2

    invoke-virtual {p2, v2}, Lcom/my/target/aj;->setPoint(F)V

    .line 1195
    double-to-float v0, v0

    invoke-virtual {p2, v0}, Lcom/my/target/aj;->setPointP(F)V

    .line 1093
    iget-object v0, p0, Lcom/my/target/core/parsers/j;->F:Lcom/my/target/ae;

    invoke-virtual {v0}, Lcom/my/target/ae;->J()Ljava/lang/Boolean;

    move-result-object v0

    .line 1094
    if-eqz v0, :cond_b

    .line 1096
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p2, v0}, Lcom/my/target/aj;->setAllowClose(Z)V

    .line 1103
    :goto_4
    iget-object v0, p0, Lcom/my/target/core/parsers/j;->F:Lcom/my/target/ae;

    invoke-virtual {v0}, Lcom/my/target/ae;->L()Ljava/lang/Boolean;

    move-result-object v0

    .line 1104
    if-eqz v0, :cond_c

    .line 1106
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p2, v0}, Lcom/my/target/aj;->setAllowSeek(Z)V

    .line 1112
    :goto_5
    iget-object v0, p0, Lcom/my/target/core/parsers/j;->F:Lcom/my/target/ae;

    invoke-virtual {v0}, Lcom/my/target/ae;->M()Ljava/lang/Boolean;

    move-result-object v0

    .line 1113
    if-eqz v0, :cond_d

    .line 1115
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p2, v0}, Lcom/my/target/aj;->setAllowSkip(Z)V

    .line 1121
    :goto_6
    iget-object v0, p0, Lcom/my/target/core/parsers/j;->F:Lcom/my/target/ae;

    invoke-virtual {v0}, Lcom/my/target/ae;->N()Ljava/lang/Boolean;

    move-result-object v0

    .line 1122
    if-eqz v0, :cond_e

    .line 1124
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p2, v0}, Lcom/my/target/aj;->setAllowTrackChange(Z)V

    .line 1130
    :goto_7
    iget-object v0, p0, Lcom/my/target/core/parsers/j;->F:Lcom/my/target/ae;

    invoke-virtual {v0}, Lcom/my/target/ae;->K()Ljava/lang/Boolean;

    move-result-object v0

    .line 1131
    if-eqz v0, :cond_f

    .line 1133
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p2, v0}, Lcom/my/target/aj;->setAllowPause(Z)V

    .line 1140
    :goto_8
    iget-object v0, p0, Lcom/my/target/core/parsers/j;->F:Lcom/my/target/ae;

    invoke-virtual {v0}, Lcom/my/target/ae;->getAllowCloseDelay()F

    move-result v0

    .line 1141
    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_10

    .line 1143
    invoke-virtual {p2, v0}, Lcom/my/target/aj;->setAllowCloseDelay(F)V

    .line 70
    :goto_9
    const-string v0, "companionAds"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 71
    if-eqz v2, :cond_13

    .line 73
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    .line 74
    const/4 v0, 0x0

    :goto_a
    if-ge v0, v3, :cond_13

    .line 76
    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 77
    invoke-virtual {p2}, Lcom/my/target/aj;->getId()Ljava/lang/String;

    move-result-object v5

    .line 1200
    invoke-static {}, Lcom/my/target/ai;->newBanner()Lcom/my/target/ai;

    move-result-object v1

    .line 1201
    iget-object v6, p0, Lcom/my/target/core/parsers/j;->G:Lcom/my/target/be;

    const/4 v7, 0x0

    invoke-virtual {v6, v4, v1, v7}, Lcom/my/target/be;->a(Lorg/json/JSONObject;Lcom/my/target/ah;Lcom/my/target/af;)V

    .line 1202
    invoke-virtual {v1}, Lcom/my/target/ai;->getWidth()I

    move-result v6

    if-eqz v6, :cond_6

    invoke-virtual {v1}, Lcom/my/target/ai;->getHeight()I

    move-result v6

    if-nez v6, :cond_11

    .line 1204
    :cond_6
    const-string v4, "Required field"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Unable to add companion banner with width "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/my/target/ai;->getWidth()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " and height "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 1205
    invoke-virtual {v1}, Lcom/my/target/ai;->getHeight()I

    move-result v1

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1204
    invoke-direct {p0, v4, v1, v5}, Lcom/my/target/core/parsers/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1206
    const/4 v1, 0x0

    .line 78
    :cond_7
    :goto_b
    if-eqz v1, :cond_8

    .line 80
    invoke-virtual {p2, v1}, Lcom/my/target/aj;->addCompanion(Lcom/my/target/ai;)V

    .line 74
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 1166
    :cond_9
    const-wide/16 v2, 0x0

    cmpg-double v2, v0, v2

    if-gez v2, :cond_3

    .line 1168
    const-string v2, "Bad value"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Wrong value "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " for point"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lcom/my/target/aj;->getId()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v2, v3, v4}, Lcom/my/target/core/parsers/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1184
    :cond_a
    const-wide/16 v4, 0x0

    cmpg-double v4, v2, v4

    if-gez v4, :cond_5

    .line 1186
    const-string v4, "Bad value"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Wrong value "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " for pointP"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2}, Lcom/my/target/aj;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0, v4, v5, v6}, Lcom/my/target/core/parsers/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1100
    :cond_b
    const-string v0, "allowClose"

    invoke-virtual {p2}, Lcom/my/target/aj;->isAllowClose()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {p2, v0}, Lcom/my/target/aj;->setAllowClose(Z)V

    goto/16 :goto_4

    .line 1110
    :cond_c
    const-string v0, "allowSeek"

    invoke-virtual {p2}, Lcom/my/target/aj;->isAllowSeek()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {p2, v0}, Lcom/my/target/aj;->setAllowSeek(Z)V

    goto/16 :goto_5

    .line 1119
    :cond_d
    const-string v0, "allowSkip"

    invoke-virtual {p2}, Lcom/my/target/aj;->isAllowSkip()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {p2, v0}, Lcom/my/target/aj;->setAllowSkip(Z)V

    goto/16 :goto_6

    .line 1128
    :cond_e
    const-string v0, "allowTrackChange"

    invoke-virtual {p2}, Lcom/my/target/aj;->isAllowTrackChange()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {p2, v0}, Lcom/my/target/aj;->setAllowTrackChange(Z)V

    goto/16 :goto_7

    .line 1137
    :cond_f
    const-string v0, "hasPause"

    invoke-virtual {p2}, Lcom/my/target/aj;->isAllowPause()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {p2, v0}, Lcom/my/target/aj;->setAllowPause(Z)V

    goto/16 :goto_8

    .line 1147
    :cond_10
    const-string v0, "allowCloseDelay"

    invoke-virtual {p2}, Lcom/my/target/aj;->getAllowCloseDelay()F

    move-result v1

    float-to-double v2, v1

    invoke-virtual {p1, v0, v2, v3}, Lorg/json/JSONObject;->optDouble(Ljava/lang/String;D)D

    move-result-wide v0

    double-to-float v0, v0

    invoke-virtual {p2, v0}, Lcom/my/target/aj;->setAllowCloseDelay(F)V

    goto/16 :goto_9

    .line 1209
    :cond_11
    const-string v6, "assetWidth"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v1, v6}, Lcom/my/target/ai;->setAssetWidth(I)V

    .line 1210
    const-string v6, "assetHeight"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v1, v6}, Lcom/my/target/ai;->setAssetHeight(I)V

    .line 1211
    const-string v6, "expandedWidth"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v1, v6}, Lcom/my/target/ai;->setExpandedWidth(I)V

    .line 1212
    const-string v6, "expandedHeight"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v1, v6}, Lcom/my/target/ai;->setExpandedHeight(I)V

    .line 1214
    const-string v6, "staticResource"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/my/target/ai;->setStaticResource(Ljava/lang/String;)V

    .line 1215
    const-string v6, "iframeResource"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/my/target/ai;->setIframeResource(Ljava/lang/String;)V

    .line 1216
    const-string v6, "htmlResource"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/my/target/ai;->setHtmlResource(Ljava/lang/String;)V

    .line 1217
    const-string v6, "apiFramework"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/my/target/ai;->setApiFramework(Ljava/lang/String;)V

    .line 1218
    const-string v6, "adSlotID"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/my/target/ai;->setAdSlotID(Ljava/lang/String;)V

    .line 1219
    const-string v6, "required"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1220
    if-eqz v4, :cond_7

    .line 1222
    const-string v6, "all"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_12

    const-string v6, "any"

    .line 1223
    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_12

    const-string v6, "none"

    .line 1224
    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_12

    .line 1226
    const-string v6, "Bad value"

    const-string v7, "Wrong companion required attribute:"

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v6, v4, v5}, Lcom/my/target/core/parsers/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_b

    .line 1230
    :cond_12
    invoke-virtual {v1, v4}, Lcom/my/target/ai;->setRequired(Ljava/lang/String;)V

    goto/16 :goto_b

    .line 85
    :cond_13
    invoke-direct {p0, p1, p2}, Lcom/my/target/core/parsers/j;->b(Lorg/json/JSONObject;Lcom/my/target/aj;)Z

    move-result v0

    goto/16 :goto_0

    :cond_14
    move-wide v8, v2

    move-wide v2, v0

    move-wide v0, v8

    goto/16 :goto_3
.end method
