.class public final Lcom/my/target/core/models/banners/g;
.super Lcom/my/target/core/models/banners/d;
.source "InterstitialAdImageBanner.java"


# instance fields
.field private final landscapeImages:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/my/target/common/models/ImageData;",
            ">;"
        }
    .end annotation
.end field

.field private optimalLandscapeImage:Lcom/my/target/common/models/ImageData;

.field private optimalPortraitImage:Lcom/my/target/common/models/ImageData;

.field private final portraitImages:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/my/target/common/models/ImageData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/my/target/core/models/banners/d;-><init>()V

    .line 25
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/my/target/core/models/banners/g;->portraitImages:Ljava/util/List;

    .line 26
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/my/target/core/models/banners/g;->landscapeImages:Ljava/util/List;

    .line 32
    return-void
.end method

.method public static newBanner()Lcom/my/target/core/models/banners/g;
    .locals 1

    .prologue
    .line 22
    new-instance v0, Lcom/my/target/core/models/banners/g;

    invoke-direct {v0}, Lcom/my/target/core/models/banners/g;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final addLandscapeImage(Lcom/my/target/common/models/ImageData;)V
    .locals 1
    .param p1, "imageData"    # Lcom/my/target/common/models/ImageData;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/my/target/core/models/banners/g;->landscapeImages:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 72
    return-void
.end method

.method public final addPortraitImage(Lcom/my/target/common/models/ImageData;)V
    .locals 1
    .param p1, "imageData"    # Lcom/my/target/common/models/ImageData;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/my/target/core/models/banners/g;->portraitImages:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 67
    return-void
.end method

.method public final getLandscapeImages()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/my/target/common/models/ImageData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 41
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/my/target/core/models/banners/g;->landscapeImages:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public final getOptimalLandscapeImage()Lcom/my/target/common/models/ImageData;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/my/target/core/models/banners/g;->optimalLandscapeImage:Lcom/my/target/common/models/ImageData;

    return-object v0
.end method

.method public final getOptimalPortraitImage()Lcom/my/target/common/models/ImageData;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/my/target/core/models/banners/g;->optimalPortraitImage:Lcom/my/target/common/models/ImageData;

    return-object v0
.end method

.method public final getPortraitImages()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/my/target/common/models/ImageData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 36
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/my/target/core/models/banners/g;->portraitImages:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public final setOptimalLandscapeImage(Lcom/my/target/common/models/ImageData;)V
    .locals 0
    .param p1, "optimalLandscapeImage"    # Lcom/my/target/common/models/ImageData;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/my/target/core/models/banners/g;->optimalLandscapeImage:Lcom/my/target/common/models/ImageData;

    .line 62
    return-void
.end method

.method public final setOptimalPortraitImage(Lcom/my/target/common/models/ImageData;)V
    .locals 0
    .param p1, "optimalPortraitImage"    # Lcom/my/target/common/models/ImageData;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/my/target/core/models/banners/g;->optimalPortraitImage:Lcom/my/target/common/models/ImageData;

    .line 52
    return-void
.end method
