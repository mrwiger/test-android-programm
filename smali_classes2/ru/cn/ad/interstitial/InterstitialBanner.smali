.class public final Lru/cn/ad/interstitial/InterstitialBanner;
.super Ljava/lang/Object;
.source "InterstitialBanner.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/ad/interstitial/InterstitialBanner$Listener;
    }
.end annotation


# instance fields
.field private adapter:Lru/cn/ad/AdAdapter;

.field private listener:Lru/cn/ad/interstitial/InterstitialBanner$Listener;

.field private final loader:Lru/cn/ad/PlaceLoader;

.field private final placeId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "placeId"    # Ljava/lang/String;

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p2, p0, Lru/cn/ad/interstitial/InterstitialBanner;->placeId:Ljava/lang/String;

    .line 32
    new-instance v0, Lru/cn/ad/interstitial/DefaultInterstitialFactory;

    invoke-direct {v0, p1}, Lru/cn/ad/interstitial/DefaultInterstitialFactory;-><init>(Landroid/content/Context;)V

    .line 33
    .local v0, "factory":Lru/cn/ad/AdAdapter$Factory;
    new-instance v1, Lru/cn/ad/PlaceLoader;

    invoke-direct {v1, p1, p2, v0}, Lru/cn/ad/PlaceLoader;-><init>(Landroid/content/Context;Ljava/lang/String;Lru/cn/ad/AdAdapter$Factory;)V

    iput-object v1, p0, Lru/cn/ad/interstitial/InterstitialBanner;->loader:Lru/cn/ad/PlaceLoader;

    .line 35
    invoke-static {}, Lru/cn/ad/AdsManager;->preloader()Lru/cn/ad/AdsManager$AdapterCache;

    move-result-object v1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v1, p2, v2}, Lru/cn/ad/AdsManager$AdapterCache;->getAdapter(Ljava/lang/String;Ljava/util/List;)Lru/cn/ad/AdAdapter;

    move-result-object v1

    iput-object v1, p0, Lru/cn/ad/interstitial/InterstitialBanner;->adapter:Lru/cn/ad/AdAdapter;

    .line 36
    return-void
.end method

.method static synthetic access$000(Lru/cn/ad/interstitial/InterstitialBanner;)Lru/cn/ad/AdAdapter;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/interstitial/InterstitialBanner;

    .prologue
    .line 14
    iget-object v0, p0, Lru/cn/ad/interstitial/InterstitialBanner;->adapter:Lru/cn/ad/AdAdapter;

    return-object v0
.end method

.method static synthetic access$002(Lru/cn/ad/interstitial/InterstitialBanner;Lru/cn/ad/AdAdapter;)Lru/cn/ad/AdAdapter;
    .locals 0
    .param p0, "x0"    # Lru/cn/ad/interstitial/InterstitialBanner;
    .param p1, "x1"    # Lru/cn/ad/AdAdapter;

    .prologue
    .line 14
    iput-object p1, p0, Lru/cn/ad/interstitial/InterstitialBanner;->adapter:Lru/cn/ad/AdAdapter;

    return-object p1
.end method

.method static synthetic access$100(Lru/cn/ad/interstitial/InterstitialBanner;)Lru/cn/ad/interstitial/InterstitialBanner$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/interstitial/InterstitialBanner;

    .prologue
    .line 14
    iget-object v0, p0, Lru/cn/ad/interstitial/InterstitialBanner;->listener:Lru/cn/ad/interstitial/InterstitialBanner$Listener;

    return-object v0
.end method


# virtual methods
.method public isReady()Z
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lru/cn/ad/interstitial/InterstitialBanner;->adapter:Lru/cn/ad/AdAdapter;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setListener(Lru/cn/ad/interstitial/InterstitialBanner$Listener;)V
    .locals 0
    .param p1, "listener"    # Lru/cn/ad/interstitial/InterstitialBanner$Listener;

    .prologue
    .line 39
    iput-object p1, p0, Lru/cn/ad/interstitial/InterstitialBanner;->listener:Lru/cn/ad/interstitial/InterstitialBanner$Listener;

    .line 40
    return-void
.end method

.method public show()V
    .locals 3

    .prologue
    .line 73
    iget-object v0, p0, Lru/cn/ad/interstitial/InterstitialBanner;->adapter:Lru/cn/ad/AdAdapter;

    if-nez v0, :cond_0

    .line 107
    :goto_0
    return-void

    .line 76
    :cond_0
    iget-object v0, p0, Lru/cn/ad/interstitial/InterstitialBanner;->adapter:Lru/cn/ad/AdAdapter;

    new-instance v1, Lru/cn/ad/interstitial/InterstitialBanner$2;

    invoke-direct {v1, p0}, Lru/cn/ad/interstitial/InterstitialBanner$2;-><init>(Lru/cn/ad/interstitial/InterstitialBanner;)V

    invoke-virtual {v0, v1}, Lru/cn/ad/AdAdapter;->setListener(Lru/cn/ad/AdAdapter$Listener;)V

    .line 102
    iget-object v0, p0, Lru/cn/ad/interstitial/InterstitialBanner;->adapter:Lru/cn/ad/AdAdapter;

    new-instance v1, Lru/cn/ad/AdEventReporter;

    iget-object v2, p0, Lru/cn/ad/interstitial/InterstitialBanner;->placeId:Ljava/lang/String;

    invoke-direct {v1, v2}, Lru/cn/ad/AdEventReporter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lru/cn/ad/AdAdapter;->setReporter(Lru/cn/ad/AdEventReporter;)V

    .line 103
    iget-object v0, p0, Lru/cn/ad/interstitial/InterstitialBanner;->adapter:Lru/cn/ad/AdAdapter;

    invoke-virtual {v0}, Lru/cn/ad/AdAdapter;->show()V

    .line 106
    invoke-static {}, Lru/cn/ad/AdsManager;->preloader()Lru/cn/ad/AdsManager$AdapterCache;

    move-result-object v0

    iget-object v1, p0, Lru/cn/ad/interstitial/InterstitialBanner;->adapter:Lru/cn/ad/AdAdapter;

    invoke-interface {v0, v1}, Lru/cn/ad/AdsManager$AdapterCache;->consume(Lru/cn/ad/AdAdapter;)V

    goto :goto_0
.end method
