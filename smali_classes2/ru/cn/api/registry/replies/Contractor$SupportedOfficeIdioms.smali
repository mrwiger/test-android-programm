.class public final enum Lru/cn/api/registry/replies/Contractor$SupportedOfficeIdioms;
.super Ljava/lang/Enum;
.source "Contractor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/api/registry/replies/Contractor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SupportedOfficeIdioms"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lru/cn/api/registry/replies/Contractor$SupportedOfficeIdioms;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lru/cn/api/registry/replies/Contractor$SupportedOfficeIdioms;

.field public static final enum MOBILE:Lru/cn/api/registry/replies/Contractor$SupportedOfficeIdioms;

.field public static final enum TV:Lru/cn/api/registry/replies/Contractor$SupportedOfficeIdioms;

.field public static final enum WEB:Lru/cn/api/registry/replies/Contractor$SupportedOfficeIdioms;


# instance fields
.field private value:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 13
    new-instance v0, Lru/cn/api/registry/replies/Contractor$SupportedOfficeIdioms;

    const-string v1, "WEB"

    invoke-direct {v0, v1, v2, v2}, Lru/cn/api/registry/replies/Contractor$SupportedOfficeIdioms;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/cn/api/registry/replies/Contractor$SupportedOfficeIdioms;->WEB:Lru/cn/api/registry/replies/Contractor$SupportedOfficeIdioms;

    .line 14
    new-instance v0, Lru/cn/api/registry/replies/Contractor$SupportedOfficeIdioms;

    const-string v1, "TV"

    invoke-direct {v0, v1, v3, v3}, Lru/cn/api/registry/replies/Contractor$SupportedOfficeIdioms;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/cn/api/registry/replies/Contractor$SupportedOfficeIdioms;->TV:Lru/cn/api/registry/replies/Contractor$SupportedOfficeIdioms;

    .line 15
    new-instance v0, Lru/cn/api/registry/replies/Contractor$SupportedOfficeIdioms;

    const-string v1, "MOBILE"

    invoke-direct {v0, v1, v4, v4}, Lru/cn/api/registry/replies/Contractor$SupportedOfficeIdioms;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lru/cn/api/registry/replies/Contractor$SupportedOfficeIdioms;->MOBILE:Lru/cn/api/registry/replies/Contractor$SupportedOfficeIdioms;

    .line 12
    const/4 v0, 0x3

    new-array v0, v0, [Lru/cn/api/registry/replies/Contractor$SupportedOfficeIdioms;

    sget-object v1, Lru/cn/api/registry/replies/Contractor$SupportedOfficeIdioms;->WEB:Lru/cn/api/registry/replies/Contractor$SupportedOfficeIdioms;

    aput-object v1, v0, v2

    sget-object v1, Lru/cn/api/registry/replies/Contractor$SupportedOfficeIdioms;->TV:Lru/cn/api/registry/replies/Contractor$SupportedOfficeIdioms;

    aput-object v1, v0, v3

    sget-object v1, Lru/cn/api/registry/replies/Contractor$SupportedOfficeIdioms;->MOBILE:Lru/cn/api/registry/replies/Contractor$SupportedOfficeIdioms;

    aput-object v1, v0, v4

    sput-object v0, Lru/cn/api/registry/replies/Contractor$SupportedOfficeIdioms;->$VALUES:[Lru/cn/api/registry/replies/Contractor$SupportedOfficeIdioms;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 20
    iput p3, p0, Lru/cn/api/registry/replies/Contractor$SupportedOfficeIdioms;->value:I

    .line 21
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lru/cn/api/registry/replies/Contractor$SupportedOfficeIdioms;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 12
    const-class v0, Lru/cn/api/registry/replies/Contractor$SupportedOfficeIdioms;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lru/cn/api/registry/replies/Contractor$SupportedOfficeIdioms;

    return-object v0
.end method

.method public static values()[Lru/cn/api/registry/replies/Contractor$SupportedOfficeIdioms;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lru/cn/api/registry/replies/Contractor$SupportedOfficeIdioms;->$VALUES:[Lru/cn/api/registry/replies/Contractor$SupportedOfficeIdioms;

    invoke-virtual {v0}, [Lru/cn/api/registry/replies/Contractor$SupportedOfficeIdioms;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lru/cn/api/registry/replies/Contractor$SupportedOfficeIdioms;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 24
    iget v0, p0, Lru/cn/api/registry/replies/Contractor$SupportedOfficeIdioms;->value:I

    return v0
.end method
