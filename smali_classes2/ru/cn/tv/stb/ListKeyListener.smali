.class Lru/cn/tv/stb/ListKeyListener;
.super Ljava/lang/Object;
.source "ListKeyListener.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method protected keyDown()Z
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    return v0
.end method

.method protected keyLeft()Z
    .locals 1

    .prologue
    .line 17
    const/4 v0, 0x0

    return v0
.end method

.method protected keyRight()Z
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x0

    return v0
.end method

.method protected keyUp()Z
    .locals 1

    .prologue
    .line 13
    const/4 v0, 0x0

    return v0
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 6
    .param p1, "v"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 27
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v5

    if-nez v5, :cond_0

    move v0, v3

    .line 28
    .local v0, "down":Z
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v5

    if-nez v5, :cond_1

    move v2, v3

    .line 30
    .local v2, "uniqueDown":Z
    :goto_1
    invoke-virtual {p1}, Landroid/view/View;->isShown()Z

    move-result v5

    if-nez v5, :cond_2

    .line 52
    :goto_2
    return v4

    .end local v0    # "down":Z
    .end local v2    # "uniqueDown":Z
    :cond_0
    move v0, v4

    .line 27
    goto :goto_0

    .restart local v0    # "down":Z
    :cond_1
    move v2, v4

    .line 28
    goto :goto_1

    .line 34
    .restart local v2    # "uniqueDown":Z
    :cond_2
    const/4 v1, 0x0

    .line 35
    .local v1, "stopPropagation":Z
    sparse-switch p2, :sswitch_data_0

    :goto_3
    move v4, v1

    .line 52
    goto :goto_2

    .line 37
    :sswitch_0
    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lru/cn/tv/stb/ListKeyListener;->keyDown()Z

    move-result v5

    if-eqz v5, :cond_4

    :cond_3
    move v1, v3

    .line 38
    :goto_4
    goto :goto_3

    :cond_4
    move v1, v4

    .line 37
    goto :goto_4

    .line 40
    :sswitch_1
    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lru/cn/tv/stb/ListKeyListener;->keyUp()Z

    move-result v5

    if-eqz v5, :cond_6

    :cond_5
    move v1, v3

    .line 41
    :goto_5
    goto :goto_3

    :cond_6
    move v1, v4

    .line 40
    goto :goto_5

    .line 44
    :sswitch_2
    if-eqz v2, :cond_7

    invoke-virtual {p0}, Lru/cn/tv/stb/ListKeyListener;->keyLeft()Z

    move-result v5

    if-eqz v5, :cond_8

    :cond_7
    move v1, v3

    .line 45
    :goto_6
    goto :goto_3

    :cond_8
    move v1, v4

    .line 44
    goto :goto_6

    .line 48
    :sswitch_3
    if-eqz v2, :cond_9

    invoke-virtual {p0}, Lru/cn/tv/stb/ListKeyListener;->keyRight()Z

    move-result v5

    if-eqz v5, :cond_a

    :cond_9
    move v1, v3

    :goto_7
    goto :goto_3

    :cond_a
    move v1, v4

    goto :goto_7

    .line 35
    nop

    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_1
        0x14 -> :sswitch_0
        0x15 -> :sswitch_2
        0x16 -> :sswitch_3
        0x71 -> :sswitch_2
        0x72 -> :sswitch_3
    .end sparse-switch
.end method
