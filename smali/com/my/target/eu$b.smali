.class final Lcom/my/target/eu$b;
.super Lcom/my/target/er;
.source "RecyclerHorizontalView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/my/target/eu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/my/target/er",
        "<",
        "Lcom/my/target/eu$a;",
        ">;"
    }
.end annotation


# instance fields
.field private final dv:I


# direct methods
.method constructor <init>(Ljava/util/List;ILandroid/content/Context;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/my/target/core/models/banners/e;",
            ">;I",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .prologue
    .line 116
    invoke-direct {p0, p1, p3}, Lcom/my/target/er;-><init>(Ljava/util/List;Landroid/content/Context;)V

    .line 117
    iput p2, p0, Lcom/my/target/eu$b;->dv:I

    .line 118
    return-void
.end method


# virtual methods
.method public final synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 5

    .prologue
    .line 109
    check-cast p1, Lcom/my/target/eu$a;

    .line 2141
    invoke-virtual {p1}, Lcom/my/target/eu$a;->M()Lcom/my/target/ej;

    move-result-object v1

    .line 2142
    iget-object v0, p0, Lcom/my/target/eu$b;->interstitialAdCards:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/core/models/banners/e;

    .line 2156
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/e;->getImage()Lcom/my/target/common/models/ImageData;

    move-result-object v2

    .line 2157
    if-eqz v2, :cond_0

    .line 2159
    invoke-virtual {v1}, Lcom/my/target/ej;->getImageView()Lcom/my/target/bx;

    move-result-object v3

    .line 2160
    invoke-virtual {v2}, Lcom/my/target/common/models/ImageData;->getWidth()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/my/target/bx;->setPlaceholderWidth(I)V

    .line 2161
    invoke-virtual {v2}, Lcom/my/target/common/models/ImageData;->getHeight()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/my/target/bx;->setPlaceholderHeight(I)V

    .line 2162
    invoke-static {v2, v3}, Lcom/my/target/ch;->a(Lcom/my/target/common/models/ImageData;Landroid/widget/ImageView;)V

    .line 2165
    :cond_0
    invoke-virtual {v1}, Lcom/my/target/ej;->getTitleTextView()Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v0}, Lcom/my/target/core/models/banners/e;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2144
    iget-object v2, p0, Lcom/my/target/eu$b;->dl:Landroid/view/View$OnClickListener;

    invoke-virtual {v0}, Lcom/my/target/core/models/banners/e;->getClickArea()Lcom/my/target/af;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/my/target/ej;->a(Landroid/view/View$OnClickListener;Lcom/my/target/af;)V

    .line 109
    return-void
.end method

.method public final synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 109
    .line 3123
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 3125
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 3126
    if-eqz v0, :cond_0

    .line 3128
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 3129
    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 3131
    :cond_0
    new-instance v0, Lcom/my/target/ej;

    iget-object v2, p0, Lcom/my/target/eu$b;->context:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/my/target/ej;-><init>(Landroid/content/Context;)V

    .line 3132
    new-instance v2, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget v1, v1, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    const/high16 v3, 0x40200000    # 2.5f

    div-float/2addr v1, v3

    float-to-int v1, v1

    iget v3, p0, Lcom/my/target/eu$b;->dv:I

    mul-int/lit8 v3, v3, 0x2

    sub-int/2addr v1, v3

    const/4 v3, -0x1

    invoke-direct {v2, v1, v3}, Landroid/support/v7/widget/RecyclerView$LayoutParams;-><init>(II)V

    .line 3133
    iget v1, p0, Lcom/my/target/eu$b;->dv:I

    iget v3, p0, Lcom/my/target/eu$b;->dv:I

    invoke-virtual {v2, v1, v4, v3, v4}, Landroid/support/v7/widget/RecyclerView$LayoutParams;->setMargins(IIII)V

    .line 3134
    invoke-virtual {v0, v2}, Lcom/my/target/ej;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3135
    new-instance v1, Lcom/my/target/eu$a;

    invoke-direct {v1, v0}, Lcom/my/target/eu$a;-><init>(Lcom/my/target/ej;)V

    .line 109
    return-object v1
.end method

.method public final synthetic onViewRecycled(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 109
    check-cast p1, Lcom/my/target/eu$a;

    .line 1150
    invoke-virtual {p1}, Lcom/my/target/eu$a;->M()Lcom/my/target/ej;

    move-result-object v0

    .line 1151
    invoke-virtual {v0, v1, v1}, Lcom/my/target/ej;->a(Landroid/view/View$OnClickListener;Lcom/my/target/af;)V

    .line 109
    return-void
.end method
