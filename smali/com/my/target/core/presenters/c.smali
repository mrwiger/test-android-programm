.class public final Lcom/my/target/core/presenters/c;
.super Ljava/lang/Object;
.source "WebViewPresenter.java"

# interfaces
.implements Lcom/my/target/bt$a;
.implements Lcom/my/target/core/presenters/b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/core/presenters/c$a;,
        Lcom/my/target/core/presenters/c$b;
    }
.end annotation


# instance fields
.field private final R:Ljava/lang/String;

.field private final T:Lcom/my/target/di;

.field private U:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/my/target/core/models/banners/c;",
            ">;"
        }
    .end annotation
.end field

.field private V:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/my/target/core/models/banners/c;",
            ">;"
        }
    .end annotation
.end field

.field private Z:Lcom/my/target/core/presenters/b$a;

.field private final aj:Lcom/my/target/bt;

.field private final ak:Lcom/my/target/core/presenters/c$a;

.field private al:Lcom/my/target/core/presenters/c$b;

.field private am:Ljava/lang/String;

.field private final context:Landroid/content/Context;


# direct methods
.method private constructor <init>(Ljava/lang/String;Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object p2, p0, Lcom/my/target/core/presenters/c;->context:Landroid/content/Context;

    .line 55
    iput-object p1, p0, Lcom/my/target/core/presenters/c;->R:Ljava/lang/String;

    .line 56
    new-instance v0, Lcom/my/target/core/presenters/c$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/my/target/core/presenters/c$a;-><init>(B)V

    iput-object v0, p0, Lcom/my/target/core/presenters/c;->ak:Lcom/my/target/core/presenters/c$a;

    .line 57
    new-instance v0, Lcom/my/target/di;

    invoke-direct {v0, p2}, Lcom/my/target/di;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/core/presenters/c;->T:Lcom/my/target/di;

    .line 58
    new-instance v0, Lcom/my/target/bt;

    invoke-direct {v0, p2}, Lcom/my/target/bt;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/my/target/core/presenters/c;->aj:Lcom/my/target/bt;

    .line 59
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 60
    const/4 v1, 0x1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 61
    iget-object v1, p0, Lcom/my/target/core/presenters/c;->T:Lcom/my/target/di;

    iget-object v2, p0, Lcom/my/target/core/presenters/c;->aj:Lcom/my/target/bt;

    invoke-virtual {v1, v2}, Lcom/my/target/di;->addView(Landroid/view/View;)V

    .line 62
    iget-object v1, p0, Lcom/my/target/core/presenters/c;->aj:Lcom/my/target/bt;

    invoke-virtual {v1, v0}, Lcom/my/target/bt;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 63
    iget-object v0, p0, Lcom/my/target/core/presenters/c;->aj:Lcom/my/target/bt;

    invoke-virtual {v0, p0}, Lcom/my/target/bt;->setBannerWebViewListener(Lcom/my/target/bt$a;)V

    .line 64
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 354
    iget-object v0, p0, Lcom/my/target/core/presenters/c;->Z:Lcom/my/target/core/presenters/b$a;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/my/target/core/presenters/c;->U:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 356
    iget-object v0, p0, Lcom/my/target/core/presenters/c;->U:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/core/models/banners/c;

    .line 358
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/c;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 360
    iget-object v2, p0, Lcom/my/target/core/presenters/c;->Z:Lcom/my/target/core/presenters/b$a;

    invoke-interface {v2, v0, p2}, Lcom/my/target/core/presenters/b$a;->a(Lcom/my/target/ah;Ljava/lang/String;)V

    goto :goto_0

    .line 364
    :cond_1
    return-void
.end method

.method private a([Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 373
    iget-object v0, p0, Lcom/my/target/core/presenters/c;->Z:Lcom/my/target/core/presenters/b$a;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/my/target/core/presenters/c;->U:Ljava/util/List;

    if-eqz v0, :cond_2

    if-eqz p1, :cond_2

    .line 375
    iget-object v0, p0, Lcom/my/target/core/presenters/c;->U:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/core/models/banners/c;

    .line 377
    array-length v3, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, p1, v1

    .line 379
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/c;->getId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 381
    iget-object v4, p0, Lcom/my/target/core/presenters/c;->Z:Lcom/my/target/core/presenters/b$a;

    invoke-interface {v4, v0}, Lcom/my/target/core/presenters/b$a;->a(Lcom/my/target/ah;)V

    .line 377
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 386
    :cond_2
    return-void
.end method

.method public static b(Ljava/lang/String;Landroid/content/Context;)Lcom/my/target/core/presenters/c;
    .locals 1

    .prologue
    .line 38
    new-instance v0, Lcom/my/target/core/presenters/c;

    invoke-direct {v0, p0, p1}, Lcom/my/target/core/presenters/c;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    return-object v0
.end method

.method private b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 390
    iget-object v0, p0, Lcom/my/target/core/presenters/c;->al:Lcom/my/target/core/presenters/c$b;

    if-eqz v0, :cond_0

    .line 392
    iget-object v0, p0, Lcom/my/target/core/presenters/c;->al:Lcom/my/target/core/presenters/c$b;

    invoke-interface {v0, p1}, Lcom/my/target/core/presenters/c$b;->onNoAd(Ljava/lang/String;)V

    .line 394
    :cond_0
    return-void
.end method


# virtual methods
.method public final C()Lcom/my/target/di;
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lcom/my/target/core/presenters/c;->T:Lcom/my/target/di;

    return-object v0
.end method

.method public final J(Ljava/lang/String;)V
    .locals 1
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 341
    iget-object v0, p0, Lcom/my/target/core/presenters/c;->am:Ljava/lang/String;

    invoke-direct {p0, p1, v0}, Lcom/my/target/core/presenters/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 342
    return-void
.end method

.method public final a(Lcom/my/target/core/presenters/b$a;)V
    .locals 0

    .prologue
    .line 324
    iput-object p1, p0, Lcom/my/target/core/presenters/c;->Z:Lcom/my/target/core/presenters/b$a;

    .line 325
    return-void
.end method

.method public final a(Lcom/my/target/core/presenters/c$b;)V
    .locals 0

    .prologue
    .line 329
    iput-object p1, p0, Lcom/my/target/core/presenters/c;->al:Lcom/my/target/core/presenters/c$b;

    .line 330
    return-void
.end method

.method public final a(Lcom/my/target/v;)V
    .locals 4
    .param p1, "event"    # Lcom/my/target/v;

    .prologue
    const/4 v1, 0x0

    .line 233
    invoke-interface {p1}, Lcom/my/target/v;->getType()Ljava/lang/String;

    move-result-object v2

    const/4 v0, -0x1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 320
    .end local p1    # "event":Lcom/my/target/v;
    :cond_1
    :goto_1
    :pswitch_0
    return-void

    .line 233
    .restart local p1    # "event":Lcom/my/target/v;
    :sswitch_0
    const-string v3, "onReady"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :sswitch_1
    const-string v3, "onExpand"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :sswitch_2
    const-string v3, "onCollapse"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    :sswitch_3
    const-string v3, "onError"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x3

    goto :goto_0

    :sswitch_4
    const-string v3, "onAdError"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x4

    goto :goto_0

    :sswitch_5
    const-string v3, "onCloseClick"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x5

    goto :goto_0

    :sswitch_6
    const-string v3, "onComplete"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x6

    goto :goto_0

    :sswitch_7
    const-string v3, "onNoAd"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x7

    goto :goto_0

    :sswitch_8
    const-string v3, "onAdStart"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v0, 0x8

    goto :goto_0

    :sswitch_9
    const-string v3, "onAdStop"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v0, 0x9

    goto :goto_0

    :sswitch_a
    const-string v3, "onAdPause"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v0, 0xa

    goto :goto_0

    :sswitch_b
    const-string v3, "onAdResume"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v0, 0xb

    goto :goto_0

    :sswitch_c
    const-string v3, "onAdComplete"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v0, 0xc

    goto/16 :goto_0

    :sswitch_d
    const-string v3, "onAdClick"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v0, 0xd

    goto/16 :goto_0

    :sswitch_e
    const-string v3, "onStat"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v0, 0xe

    goto/16 :goto_0

    :sswitch_f
    const-string v3, "onSizeChange"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v0, 0xf

    goto/16 :goto_0

    :sswitch_10
    const-string v3, "onRequestNewAds"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/16 v0, 0x10

    goto/16 :goto_0

    .line 236
    :pswitch_1
    iget-object v0, p0, Lcom/my/target/core/presenters/c;->ak:Lcom/my/target/core/presenters/c$a;

    invoke-virtual {v0}, Lcom/my/target/core/presenters/c$a;->J()V

    .line 1398
    move-object p1, p0

    iget-object v0, p0, Lcom/my/target/core/presenters/c;->al:Lcom/my/target/core/presenters/c$b;

    if-eqz v0, :cond_2

    .line 1400
    iget-object v0, p1, Lcom/my/target/core/presenters/c;->al:Lcom/my/target/core/presenters/c$b;

    invoke-interface {v0}, Lcom/my/target/core/presenters/c$b;->bA()V

    .line 238
    :cond_2
    invoke-virtual {p0}, Lcom/my/target/core/presenters/c;->start()V

    goto/16 :goto_1

    :pswitch_2
    move-object v0, p1

    .line 246
    check-cast v0, Lcom/my/target/u;

    .line 247
    const-string v1, "JS error"

    .line 248
    invoke-virtual {v0}, Lcom/my/target/u;->j()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_6

    .line 250
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/my/target/u;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 252
    :goto_2
    iget-object v1, p0, Lcom/my/target/core/presenters/c;->aj:Lcom/my/target/bt;

    invoke-virtual {v1}, Lcom/my/target/bt;->getUrl()Ljava/lang/String;

    move-result-object v1

    .line 253
    const-string v2, "JS error"

    invoke-static {v2}, Lcom/my/target/az;->y(Ljava/lang/String;)Lcom/my/target/az;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/my/target/az;->z(Ljava/lang/String;)Lcom/my/target/az;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/my/target/az;->A(Ljava/lang/String;)Lcom/my/target/az;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/core/presenters/c;->am:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/my/target/az;->B(Ljava/lang/String;)Lcom/my/target/az;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/core/presenters/c;->context:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/my/target/az;->e(Landroid/content/Context;)V

    .line 255
    invoke-interface {p1}, Lcom/my/target/v;->getType()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onError"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 257
    iget-object v0, p0, Lcom/my/target/core/presenters/c;->ak:Lcom/my/target/core/presenters/c$a;

    invoke-virtual {v0}, Lcom/my/target/core/presenters/c$a;->isReady()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 259
    const-string v0, "JS error"

    invoke-direct {p0, v0}, Lcom/my/target/core/presenters/c;->b(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 263
    :cond_3
    const-string v0, "JS init error"

    invoke-direct {p0, v0}, Lcom/my/target/core/presenters/c;->b(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 271
    :pswitch_3
    iget-object v0, p0, Lcom/my/target/core/presenters/c;->ak:Lcom/my/target/core/presenters/c$a;

    invoke-virtual {v0, v1}, Lcom/my/target/core/presenters/c$a;->setStarted(Z)V

    .line 272
    iget-object v0, p0, Lcom/my/target/core/presenters/c;->ak:Lcom/my/target/core/presenters/c$a;

    invoke-virtual {v0, v1}, Lcom/my/target/core/presenters/c$a;->setPaused(Z)V

    .line 273
    const-string v0, "Ad completed"

    invoke-direct {p0, v0}, Lcom/my/target/core/presenters/c;->b(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 276
    :pswitch_4
    iget-object v0, p0, Lcom/my/target/core/presenters/c;->ak:Lcom/my/target/core/presenters/c$a;

    invoke-virtual {v0}, Lcom/my/target/core/presenters/c$a;->isReady()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 278
    iget-object v0, p0, Lcom/my/target/core/presenters/c;->ak:Lcom/my/target/core/presenters/c$a;

    invoke-virtual {v0, v1}, Lcom/my/target/core/presenters/c$a;->setStarted(Z)V

    .line 279
    const-string v0, "No ad"

    invoke-direct {p0, v0}, Lcom/my/target/core/presenters/c;->b(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 283
    :cond_4
    iget-object v0, p0, Lcom/my/target/core/presenters/c;->ak:Lcom/my/target/core/presenters/c$a;

    invoke-virtual {v0}, Lcom/my/target/core/presenters/c$a;->reset()V

    .line 284
    const-string v0, "JS init error"

    invoke-direct {p0, v0}, Lcom/my/target/core/presenters/c;->b(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 288
    :pswitch_5
    iget-object v0, p0, Lcom/my/target/core/presenters/c;->V:Ljava/util/List;

    if-eqz v0, :cond_5

    .line 290
    iget-object v0, p0, Lcom/my/target/core/presenters/c;->V:Ljava/util/List;

    iput-object v0, p0, Lcom/my/target/core/presenters/c;->U:Ljava/util/List;

    .line 292
    :cond_5
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/my/target/core/presenters/c;->V:Ljava/util/List;

    .line 293
    check-cast p1, Lcom/my/target/t;

    .end local p1    # "event":Lcom/my/target/v;
    invoke-virtual {p1}, Lcom/my/target/t;->i()[Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/my/target/core/presenters/c;->a([Ljava/lang/String;)V

    .line 294
    iget-object v0, p0, Lcom/my/target/core/presenters/c;->U:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 296
    iget-object v0, p0, Lcom/my/target/core/presenters/c;->U:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/core/models/banners/c;

    invoke-virtual {v0}, Lcom/my/target/core/models/banners/c;->getId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/core/presenters/c;->am:Ljava/lang/String;

    goto/16 :goto_1

    .line 308
    .restart local p1    # "event":Lcom/my/target/v;
    :pswitch_6
    check-cast p1, Lcom/my/target/s;

    .line 309
    .end local p1    # "event":Lcom/my/target/v;
    invoke-virtual {p1}, Lcom/my/target/s;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/my/target/s;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/my/target/core/presenters/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 312
    .restart local p1    # "event":Lcom/my/target/v;
    :pswitch_7
    check-cast p1, Lcom/my/target/z;

    .end local p1    # "event":Lcom/my/target/v;
    invoke-virtual {p1}, Lcom/my/target/z;->k()Ljava/util/List;

    move-result-object v0

    .line 2368
    iget-object v1, p0, Lcom/my/target/core/presenters/c;->context:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/my/target/cl;->b(Ljava/util/List;Landroid/content/Context;)V

    goto/16 :goto_1

    .line 3346
    .restart local p1    # "event":Lcom/my/target/v;
    :pswitch_8
    iget-object v0, p0, Lcom/my/target/core/presenters/c;->Z:Lcom/my/target/core/presenters/b$a;

    if-eqz v0, :cond_1

    .line 3348
    iget-object v0, p0, Lcom/my/target/core/presenters/c;->Z:Lcom/my/target/core/presenters/b$a;

    invoke-interface {v0}, Lcom/my/target/core/presenters/b$a;->f()V

    goto/16 :goto_1

    :cond_6
    move-object v0, v1

    goto/16 :goto_2

    .line 233
    nop

    :sswitch_data_0
    .sparse-switch
        -0x7ea0abc8 -> :sswitch_6
        -0x50755897 -> :sswitch_3
        -0x4fc450fc -> :sswitch_0
        -0x3c62dbbd -> :sswitch_7
        -0x3c607f2d -> :sswitch_e
        0x969e846 -> :sswitch_d
        0x988f4c6 -> :sswitch_4
        0xa1c48b4 -> :sswitch_a
        0xa4ee720 -> :sswitch_8
        0x21cc2dbb -> :sswitch_c
        0x2c8d7a50 -> :sswitch_f
        0x3a239584 -> :sswitch_9
        0x3d0dfd2f -> :sswitch_b
        0x421db559 -> :sswitch_1
        0x4aae51af -> :sswitch_5
        0x7d5bcec0 -> :sswitch_10
        0x7f6d46ac -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_8
    .end packed-switch
.end method

.method public final b(Lcom/my/target/dh;)V
    .locals 3

    .prologue
    .line 69
    invoke-virtual {p1}, Lcom/my/target/dh;->R()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/core/presenters/c;->U:Ljava/util/List;

    .line 70
    invoke-virtual {p1}, Lcom/my/target/dh;->getRawData()Lorg/json/JSONObject;

    move-result-object v0

    .line 71
    invoke-virtual {p1}, Lcom/my/target/dh;->getHtml()Ljava/lang/String;

    move-result-object v1

    .line 72
    if-nez v0, :cond_0

    .line 74
    const-string v0, "failed to load, null raw data"

    invoke-direct {p0, v0}, Lcom/my/target/core/presenters/c;->b(Ljava/lang/String;)V

    .line 83
    :goto_0
    return-void

    .line 77
    :cond_0
    if-nez v1, :cond_1

    .line 79
    const-string v0, "failed to load, null html"

    invoke-direct {p0, v0}, Lcom/my/target/core/presenters/c;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 82
    :cond_1
    iget-object v2, p0, Lcom/my/target/core/presenters/c;->aj:Lcom/my/target/bt;

    invoke-virtual {v2, v0, v1}, Lcom/my/target/bt;->a(Lorg/json/JSONObject;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final c(Lcom/my/target/dh;)V
    .locals 3

    .prologue
    .line 88
    invoke-virtual {p1}, Lcom/my/target/dh;->R()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/my/target/core/presenters/c;->V:Ljava/util/List;

    .line 89
    invoke-virtual {p1}, Lcom/my/target/dh;->getRawData()Lorg/json/JSONObject;

    move-result-object v0

    .line 90
    if-eqz v0, :cond_0

    .line 94
    :try_start_0
    iget-object v1, p0, Lcom/my/target/core/presenters/c;->aj:Lcom/my/target/bt;

    new-instance v2, Lcom/my/target/p;

    invoke-direct {v2, v0}, Lcom/my/target/p;-><init>(Lorg/json/JSONObject;)V

    invoke-virtual {v1, v2}, Lcom/my/target/bt;->a(Lcom/my/target/m;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 101
    :cond_0
    :goto_0
    return-void

    .line 98
    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final destroy()V
    .locals 2

    .prologue
    .line 217
    iget-object v0, p0, Lcom/my/target/core/presenters/c;->aj:Lcom/my/target/bt;

    invoke-virtual {v0}, Lcom/my/target/bt;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 219
    iget-object v0, p0, Lcom/my/target/core/presenters/c;->aj:Lcom/my/target/bt;

    invoke-virtual {v0}, Lcom/my/target/bt;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/my/target/core/presenters/c;->aj:Lcom/my/target/bt;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 221
    :cond_0
    iget-object v0, p0, Lcom/my/target/core/presenters/c;->aj:Lcom/my/target/bt;

    invoke-virtual {v0}, Lcom/my/target/bt;->destroy()V

    .line 222
    return-void
.end method

.method public final onError(Ljava/lang/String;)V
    .locals 0
    .param p1, "error"    # Ljava/lang/String;

    .prologue
    .line 335
    invoke-direct {p0, p1}, Lcom/my/target/core/presenters/c;->b(Ljava/lang/String;)V

    .line 336
    return-void
.end method

.method public final pause()V
    .locals 3

    .prologue
    .line 159
    iget-object v0, p0, Lcom/my/target/core/presenters/c;->ak:Lcom/my/target/core/presenters/c$a;

    invoke-virtual {v0}, Lcom/my/target/core/presenters/c$a;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 161
    iget-object v0, p0, Lcom/my/target/core/presenters/c;->ak:Lcom/my/target/core/presenters/c$a;

    invoke-virtual {v0}, Lcom/my/target/core/presenters/c$a;->isPaused()Z

    move-result v0

    if-nez v0, :cond_0

    .line 165
    :try_start_0
    iget-object v0, p0, Lcom/my/target/core/presenters/c;->aj:Lcom/my/target/bt;

    new-instance v1, Lcom/my/target/l;

    const-string v2, "pause"

    invoke-direct {v1, v2}, Lcom/my/target/l;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/my/target/bt;->a(Lcom/my/target/m;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 172
    iget-object v0, p0, Lcom/my/target/core/presenters/c;->ak:Lcom/my/target/core/presenters/c$a;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/my/target/core/presenters/c$a;->setPaused(Z)V

    .line 183
    :goto_0
    return-void

    .line 169
    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 176
    :cond_0
    const-string v0, "already paused"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 181
    :cond_1
    const-string v0, "not started"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final resume()V
    .locals 3

    .prologue
    .line 188
    iget-object v0, p0, Lcom/my/target/core/presenters/c;->ak:Lcom/my/target/core/presenters/c$a;

    invoke-virtual {v0}, Lcom/my/target/core/presenters/c$a;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 190
    iget-object v0, p0, Lcom/my/target/core/presenters/c;->ak:Lcom/my/target/core/presenters/c$a;

    invoke-virtual {v0}, Lcom/my/target/core/presenters/c$a;->isPaused()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 194
    :try_start_0
    iget-object v0, p0, Lcom/my/target/core/presenters/c;->aj:Lcom/my/target/bt;

    new-instance v1, Lcom/my/target/l;

    const-string v2, "resume"

    invoke-direct {v1, v2}, Lcom/my/target/l;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/my/target/bt;->a(Lcom/my/target/m;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 201
    iget-object v0, p0, Lcom/my/target/core/presenters/c;->ak:Lcom/my/target/core/presenters/c$a;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/my/target/core/presenters/c$a;->setPaused(Z)V

    .line 212
    :goto_0
    return-void

    .line 198
    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 205
    :cond_0
    const-string v0, "already started"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 210
    :cond_1
    const-string v0, "not started"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final start()V
    .locals 5

    .prologue
    .line 106
    iget-object v0, p0, Lcom/my/target/core/presenters/c;->ak:Lcom/my/target/core/presenters/c$a;

    invoke-virtual {v0}, Lcom/my/target/core/presenters/c$a;->isReady()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 108
    iget-object v0, p0, Lcom/my/target/core/presenters/c;->ak:Lcom/my/target/core/presenters/c$a;

    invoke-virtual {v0}, Lcom/my/target/core/presenters/c$a;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 110
    const-string v0, "already started"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 131
    :goto_0
    return-void

    .line 114
    :cond_0
    iget-object v0, p0, Lcom/my/target/core/presenters/c;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    .line 117
    :try_start_0
    iget-object v1, p0, Lcom/my/target/core/presenters/c;->aj:Lcom/my/target/bt;

    new-instance v2, Lcom/my/target/o;

    iget-object v3, p0, Lcom/my/target/core/presenters/c;->R:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4, v0}, Lcom/my/target/o;-><init>(Ljava/lang/String;[Ljava/lang/String;I)V

    invoke-virtual {v1, v2}, Lcom/my/target/bt;->a(Lcom/my/target/m;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 124
    iget-object v0, p0, Lcom/my/target/core/presenters/c;->ak:Lcom/my/target/core/presenters/c$a;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/my/target/core/presenters/c$a;->setStarted(Z)V

    goto :goto_0

    .line 121
    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 129
    :cond_1
    const-string v0, "not ready"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final stop()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 136
    iget-object v0, p0, Lcom/my/target/core/presenters/c;->ak:Lcom/my/target/core/presenters/c$a;

    invoke-virtual {v0}, Lcom/my/target/core/presenters/c$a;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 140
    :try_start_0
    iget-object v0, p0, Lcom/my/target/core/presenters/c;->aj:Lcom/my/target/bt;

    new-instance v1, Lcom/my/target/l;

    const-string v2, "stop"

    invoke-direct {v1, v2}, Lcom/my/target/l;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/my/target/bt;->a(Lcom/my/target/m;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 147
    iget-object v0, p0, Lcom/my/target/core/presenters/c;->ak:Lcom/my/target/core/presenters/c$a;

    invoke-virtual {v0, v3}, Lcom/my/target/core/presenters/c$a;->setPaused(Z)V

    .line 148
    iget-object v0, p0, Lcom/my/target/core/presenters/c;->ak:Lcom/my/target/core/presenters/c$a;

    invoke-virtual {v0, v3}, Lcom/my/target/core/presenters/c$a;->setStarted(Z)V

    .line 154
    :goto_0
    return-void

    .line 144
    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 152
    :cond_0
    const-string v0, "not started"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto :goto_0
.end method
