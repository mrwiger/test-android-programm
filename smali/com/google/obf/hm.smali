.class public Lcom/google/obf/hm;
.super Ljava/lang/Object;
.source "IMASDK"

# interfaces
.implements Lcom/google/obf/ht;


# instance fields
.field private final a:Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer;

.field private final b:Lcom/google/obf/go;

.field private final c:Lcom/google/obf/gq;

.field private final d:Lcom/google/obf/gs;

.field private final e:Lcom/google/obf/gm;

.field private f:Z

.field private g:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/obf/hl;Lcom/google/obf/hj;Lcom/google/obf/gq;Lcom/google/ads/interactivemedia/v3/api/AdDisplayContainer;Landroid/content/Context;)V
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/ads/interactivemedia/v3/api/AdError;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 1
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v7, v6

    move-object v8, p6

    invoke-direct/range {v0 .. v8}, Lcom/google/obf/hm;-><init>(Ljava/lang/String;Lcom/google/obf/hl;Lcom/google/obf/hj;Lcom/google/obf/gq;Lcom/google/ads/interactivemedia/v3/api/AdDisplayContainer;Lcom/google/obf/go;Lcom/google/obf/gs;Landroid/content/Context;)V

    .line 2
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/google/obf/hl;Lcom/google/obf/hj;Lcom/google/obf/gq;Lcom/google/ads/interactivemedia/v3/api/AdDisplayContainer;Lcom/google/obf/go;Lcom/google/obf/gs;Landroid/content/Context;)V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/ads/interactivemedia/v3/api/AdError;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4
    iput-boolean v2, p0, Lcom/google/obf/hm;->g:Z

    .line 5
    invoke-interface {p5}, Lcom/google/ads/interactivemedia/v3/api/AdDisplayContainer;->getPlayer()Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 6
    invoke-interface {p5}, Lcom/google/ads/interactivemedia/v3/api/AdDisplayContainer;->getPlayer()Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/obf/hm;->a:Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer;

    .line 7
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/obf/hm;->f:Z

    .line 13
    :goto_0
    if-eqz p6, :cond_2

    .line 14
    iput-object p6, p0, Lcom/google/obf/hm;->b:Lcom/google/obf/go;

    .line 17
    :goto_1
    iput-object p4, p0, Lcom/google/obf/hm;->c:Lcom/google/obf/gq;

    .line 18
    if-eqz p7, :cond_3

    .line 19
    iput-object p7, p0, Lcom/google/obf/hm;->d:Lcom/google/obf/gs;

    .line 21
    :goto_2
    new-instance v0, Lcom/google/obf/gm;

    iget-object v1, p0, Lcom/google/obf/hm;->b:Lcom/google/obf/go;

    invoke-direct {v0, p3, p1, v1}, Lcom/google/obf/gm;-><init>(Lcom/google/obf/hj;Ljava/lang/String;Lcom/google/obf/go;)V

    iput-object v0, p0, Lcom/google/obf/hm;->e:Lcom/google/obf/gm;

    .line 22
    return-void

    .line 8
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_1

    .line 9
    new-instance v0, Lcom/google/obf/hf;

    .line 10
    invoke-interface {p5}, Lcom/google/ads/interactivemedia/v3/api/AdDisplayContainer;->getAdContainer()Landroid/view/ViewGroup;

    move-result-object v1

    invoke-direct {v0, p8, v1}, Lcom/google/obf/hf;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;)V

    iput-object v0, p0, Lcom/google/obf/hm;->a:Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer;

    .line 11
    iput-boolean v2, p0, Lcom/google/obf/hm;->f:Z

    goto :goto_0

    .line 12
    :cond_1
    new-instance v0, Lcom/google/ads/interactivemedia/v3/api/AdError;

    sget-object v1, Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;->LOAD:Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;

    sget-object v2, Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;->INVALID_ARGUMENTS:Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;

    const-string v3, "Ad Player was not provided. SDK-owned ad playback requires API 16+"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/ads/interactivemedia/v3/api/AdError;-><init>(Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;Ljava/lang/String;)V

    throw v0

    .line 15
    :cond_2
    new-instance v0, Lcom/google/obf/go;

    iget-object v1, p0, Lcom/google/obf/hm;->a:Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer;

    .line 16
    invoke-virtual {p2}, Lcom/google/obf/hl;->a()J

    move-result-wide v2

    invoke-direct {v0, v1, v2, v3}, Lcom/google/obf/go;-><init>(Lcom/google/ads/interactivemedia/v3/api/player/AdProgressProvider;J)V

    iput-object v0, p0, Lcom/google/obf/hm;->b:Lcom/google/obf/go;

    goto :goto_1

    .line 20
    :cond_3
    new-instance v0, Lcom/google/obf/gs;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p5

    move-object v5, p8

    invoke-direct/range {v0 .. v5}, Lcom/google/obf/gs;-><init>(Ljava/lang/String;Lcom/google/obf/hl;Lcom/google/obf/hj;Lcom/google/ads/interactivemedia/v3/api/BaseDisplayContainer;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/obf/hm;->d:Lcom/google/obf/gs;

    goto :goto_2
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/obf/hm;->a:Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer;

    invoke-interface {v0}, Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer;->stopAd()V

    .line 62
    iget-object v0, p0, Lcom/google/obf/hm;->d:Lcom/google/obf/gs;

    invoke-virtual {v0}, Lcom/google/obf/gs;->a()V

    .line 63
    return-void
.end method

.method public a(Lcom/google/ads/interactivemedia/v3/impl/data/b;)V
    .locals 1

    .prologue
    .line 56
    iget-boolean v0, p0, Lcom/google/obf/hm;->g:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/impl/data/b;->canDisableUi()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 57
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/google/ads/interactivemedia/v3/impl/data/b;->setUiDisabled(Z)V

    .line 60
    :goto_0
    return-void

    .line 58
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/google/ads/interactivemedia/v3/impl/data/b;->setUiDisabled(Z)V

    .line 59
    iget-object v0, p0, Lcom/google/obf/hm;->d:Lcom/google/obf/gs;

    invoke-virtual {v0, p1}, Lcom/google/obf/gs;->a(Lcom/google/ads/interactivemedia/v3/impl/data/b;)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/obf/hm;->b:Lcom/google/obf/go;

    iget-object v1, p0, Lcom/google/obf/hm;->d:Lcom/google/obf/gs;

    invoke-virtual {v0, v1}, Lcom/google/obf/go;->a(Lcom/google/obf/hn$b;)V

    .line 24
    iget-object v0, p0, Lcom/google/obf/hm;->b:Lcom/google/obf/go;

    iget-object v1, p0, Lcom/google/obf/hm;->e:Lcom/google/obf/gm;

    invoke-virtual {v0, v1}, Lcom/google/obf/go;->a(Lcom/google/obf/hn$b;)V

    .line 25
    iput-boolean p1, p0, Lcom/google/obf/hm;->g:Z

    .line 26
    return-void
.end method

.method public a(Lcom/google/obf/hi$c;Lcom/google/ads/interactivemedia/v3/impl/data/l;)Z
    .locals 6

    .prologue
    .line 28
    sget-object v0, Lcom/google/obf/hm$1;->a:[I

    invoke-virtual {p1}, Lcom/google/obf/hi$c;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 43
    const/4 v0, 0x0

    .line 44
    :goto_0
    return v0

    .line 29
    :pswitch_0
    iget-object v0, p0, Lcom/google/obf/hm;->a:Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer;

    invoke-interface {v0}, Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer;->playAd()V

    .line 44
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 31
    :pswitch_1
    iget-object v0, p0, Lcom/google/obf/hm;->a:Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer;

    invoke-interface {v0}, Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer;->pauseAd()V

    goto :goto_1

    .line 33
    :pswitch_2
    iget-object v0, p0, Lcom/google/obf/hm;->a:Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer;

    invoke-interface {v0}, Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer;->resumeAd()V

    goto :goto_1

    .line 35
    :pswitch_3
    if-eqz p2, :cond_0

    iget-object v0, p2, Lcom/google/ads/interactivemedia/v3/impl/data/l;->videoUrl:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 36
    iget-object v0, p0, Lcom/google/obf/hm;->a:Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer;

    iget-object v1, p2, Lcom/google/ads/interactivemedia/v3/impl/data/l;->videoUrl:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer;->loadAd(Ljava/lang/String;)V

    goto :goto_1

    .line 37
    :cond_0
    iget-object v0, p0, Lcom/google/obf/hm;->c:Lcom/google/obf/gq;

    new-instance v1, Lcom/google/obf/gk;

    new-instance v2, Lcom/google/ads/interactivemedia/v3/api/AdError;

    sget-object v3, Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;->LOAD:Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;

    sget-object v4, Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;->INTERNAL_ERROR:Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;

    const-string v5, "Load message must contain video url."

    invoke-direct {v2, v3, v4, v5}, Lcom/google/ads/interactivemedia/v3/api/AdError;-><init>(Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorType;Lcom/google/ads/interactivemedia/v3/api/AdError$AdErrorCode;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Lcom/google/obf/gk;-><init>(Lcom/google/ads/interactivemedia/v3/api/AdError;)V

    invoke-virtual {v0, v1}, Lcom/google/obf/gq;->a(Lcom/google/ads/interactivemedia/v3/api/AdErrorEvent;)V

    goto :goto_1

    .line 39
    :pswitch_4
    iget-object v0, p0, Lcom/google/obf/hm;->b:Lcom/google/obf/go;

    invoke-virtual {v0}, Lcom/google/obf/go;->b()V

    goto :goto_1

    .line 41
    :pswitch_5
    iget-object v0, p0, Lcom/google/obf/hm;->b:Lcom/google/obf/go;

    invoke-virtual {v0}, Lcom/google/obf/go;->c()V

    goto :goto_1

    .line 28
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public b()V
    .locals 0

    .prologue
    .line 64
    return-void
.end method

.method public b(Lcom/google/obf/hi$c;Lcom/google/ads/interactivemedia/v3/impl/data/l;)Z
    .locals 2

    .prologue
    .line 45
    sget-object v0, Lcom/google/obf/hm$1;->a:[I

    invoke-virtual {p1}, Lcom/google/obf/hi$c;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 54
    const/4 v0, 0x0

    .line 55
    :goto_0
    return v0

    .line 46
    :pswitch_0
    iget-boolean v0, p0, Lcom/google/obf/hm;->f:Z

    if-nez v0, :cond_0

    .line 47
    iget-object v0, p0, Lcom/google/obf/hm;->a:Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer;

    check-cast v0, Lcom/google/obf/ho;

    invoke-interface {v0}, Lcom/google/obf/ho;->a()V

    .line 48
    :cond_0
    iget-object v0, p0, Lcom/google/obf/hm;->a:Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer;

    iget-object v1, p0, Lcom/google/obf/hm;->e:Lcom/google/obf/gm;

    invoke-interface {v0, v1}, Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer;->addCallback(Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer$VideoAdPlayerCallback;)V

    .line 55
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 50
    :pswitch_1
    iget-boolean v0, p0, Lcom/google/obf/hm;->f:Z

    if-nez v0, :cond_1

    .line 51
    iget-object v0, p0, Lcom/google/obf/hm;->a:Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer;

    check-cast v0, Lcom/google/obf/ho;

    invoke-interface {v0}, Lcom/google/obf/ho;->b()V

    .line 52
    :cond_1
    iget-object v0, p0, Lcom/google/obf/hm;->a:Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer;

    iget-object v1, p0, Lcom/google/obf/hm;->e:Lcom/google/obf/gm;

    invoke-interface {v0, v1}, Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer;->removeCallback(Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer$VideoAdPlayerCallback;)V

    goto :goto_1

    .line 45
    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public c()V
    .locals 0

    .prologue
    .line 65
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 68
    const-string v0, "SDK_DEBUG"

    const-string v1, "Destroying NativeVideoDisplay"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    iget-object v0, p0, Lcom/google/obf/hm;->b:Lcom/google/obf/go;

    invoke-virtual {v0}, Lcom/google/obf/go;->c()V

    .line 70
    iget-object v0, p0, Lcom/google/obf/hm;->b:Lcom/google/obf/go;

    iget-object v1, p0, Lcom/google/obf/hm;->d:Lcom/google/obf/gs;

    invoke-virtual {v0, v1}, Lcom/google/obf/go;->b(Lcom/google/obf/hn$b;)V

    .line 71
    iget-object v0, p0, Lcom/google/obf/hm;->b:Lcom/google/obf/go;

    iget-object v1, p0, Lcom/google/obf/hm;->e:Lcom/google/obf/gm;

    invoke-virtual {v0, v1}, Lcom/google/obf/go;->b(Lcom/google/obf/hn$b;)V

    .line 72
    iget-object v0, p0, Lcom/google/obf/hm;->d:Lcom/google/obf/gs;

    invoke-virtual {v0}, Lcom/google/obf/gs;->b()V

    .line 73
    iget-object v0, p0, Lcom/google/obf/hm;->a:Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer;

    iget-object v1, p0, Lcom/google/obf/hm;->e:Lcom/google/obf/gm;

    invoke-interface {v0, v1}, Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer;->removeCallback(Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer$VideoAdPlayerCallback;)V

    .line 74
    iget-object v0, p0, Lcom/google/obf/hm;->a:Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer;

    instance-of v0, v0, Lcom/google/obf/ho;

    if-eqz v0, :cond_0

    .line 75
    iget-object v0, p0, Lcom/google/obf/hm;->a:Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer;

    check-cast v0, Lcom/google/obf/ho;

    invoke-interface {v0}, Lcom/google/obf/ho;->c()V

    .line 76
    :cond_0
    return-void
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 77
    iget-boolean v0, p0, Lcom/google/obf/hm;->f:Z

    return v0
.end method

.method public getAdProgress()Lcom/google/ads/interactivemedia/v3/api/player/VideoProgressUpdate;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/obf/hm;->a:Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer;

    invoke-interface {v0}, Lcom/google/ads/interactivemedia/v3/api/player/VideoAdPlayer;->getAdProgress()Lcom/google/ads/interactivemedia/v3/api/player/VideoProgressUpdate;

    move-result-object v0

    return-object v0
.end method

.method public onAdError(Lcom/google/ads/interactivemedia/v3/api/AdErrorEvent;)V
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/obf/hm;->d:Lcom/google/obf/gs;

    invoke-virtual {v0}, Lcom/google/obf/gs;->a()V

    .line 67
    return-void
.end method
