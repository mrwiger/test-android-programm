.class Lcom/google/obf/hf$1;
.super Ljava/lang/Object;
.source "IMASDK"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/obf/hf;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/google/obf/h;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/google/obf/h;

.field final synthetic b:Lcom/google/obf/hf;


# direct methods
.method constructor <init>(Lcom/google/obf/hf;Lcom/google/obf/h;)V
    .locals 0

    .prologue
    .line 1
    iput-object p1, p0, Lcom/google/obf/hf$1;->b:Lcom/google/obf/hf;

    iput-object p2, p0, Lcom/google/obf/hf$1;->a:Lcom/google/obf/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 0

    .prologue
    .line 9
    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2
    iget-object v0, p0, Lcom/google/obf/hf$1;->b:Lcom/google/obf/hf;

    invoke-static {v0, v3}, Lcom/google/obf/hf;->a(Lcom/google/obf/hf;Z)Z

    .line 3
    iget-object v0, p0, Lcom/google/obf/hf$1;->b:Lcom/google/obf/hf;

    invoke-static {v0}, Lcom/google/obf/hf;->a(Lcom/google/obf/hf;)Lcom/google/obf/hf$f;

    move-result-object v0

    sget-object v1, Lcom/google/obf/hf$f;->c:Lcom/google/obf/hf$f;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/obf/hf$1;->b:Lcom/google/obf/hf;

    .line 4
    invoke-static {v0}, Lcom/google/obf/hf;->a(Lcom/google/obf/hf;)Lcom/google/obf/hf$f;

    move-result-object v0

    sget-object v1, Lcom/google/obf/hf$f;->d:Lcom/google/obf/hf$f;

    if-ne v0, v1, :cond_1

    .line 5
    :cond_0
    iget-object v0, p0, Lcom/google/obf/hf$1;->b:Lcom/google/obf/hf;

    invoke-interface {p1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/obf/hf;->a(Lcom/google/obf/hf;Landroid/view/Surface;Z)V

    .line 6
    :cond_1
    iget-object v0, p0, Lcom/google/obf/hf$1;->b:Lcom/google/obf/hf;

    invoke-static {v0}, Lcom/google/obf/hf;->a(Lcom/google/obf/hf;)Lcom/google/obf/hf$f;

    move-result-object v0

    sget-object v1, Lcom/google/obf/hf$f;->c:Lcom/google/obf/hf$f;

    if-ne v0, v1, :cond_2

    .line 7
    iget-object v0, p0, Lcom/google/obf/hf$1;->a:Lcom/google/obf/h;

    invoke-interface {v0, v3}, Lcom/google/obf/h;->a(Z)V

    .line 8
    :cond_2
    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 10
    iget-object v0, p0, Lcom/google/obf/hf$1;->b:Lcom/google/obf/hf;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/google/obf/hf;->a(Lcom/google/obf/hf;Landroid/view/Surface;Z)V

    .line 11
    iget-object v0, p0, Lcom/google/obf/hf$1;->a:Lcom/google/obf/h;

    invoke-interface {v0, v3}, Lcom/google/obf/h;->a(Z)V

    .line 12
    iget-object v0, p0, Lcom/google/obf/hf$1;->b:Lcom/google/obf/hf;

    invoke-static {v0, v3}, Lcom/google/obf/hf;->a(Lcom/google/obf/hf;Z)Z

    .line 13
    return-void
.end method
