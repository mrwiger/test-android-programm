.class public final Lru/cn/utils/Utils;
.super Ljava/lang/Object;
.source "Utils.java"


# static fields
.field private static androidId:Ljava/lang/String;

.field private static canPlayMulticast:Z

.field private static counterDPadKey:I

.field private static installID:Ljava/lang/String;

.field private static isLauncherInstalled:Z

.field private static isLeanback:Z

.field private static isMulticastCapable:Z

.field private static isTV:Z

.field private static shouldAvoidWebView:Z

.field private static signature:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 64
    const/4 v0, 0x0

    sput v0, Lru/cn/utils/Utils;->counterDPadKey:I

    .line 72
    sput-object v1, Lru/cn/utils/Utils;->androidId:Ljava/lang/String;

    .line 76
    sput-object v1, Lru/cn/utils/Utils;->signature:Ljava/lang/String;

    return-void
.end method

.method public static appendAdTargetingParams(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 13
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "baseLocation"    # Ljava/lang/String;

    .prologue
    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 566
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 568
    .local v0, "contentUri":Landroid/net/Uri;
    invoke-static {p0}, Lru/cn/utils/Utils;->getGadId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 569
    .local v1, "gadId":Ljava/lang/String;
    invoke-virtual {v0}, Landroid/net/Uri;->isOpaque()Z

    move-result v7

    if-nez v7, :cond_1

    .line 570
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v6

    .line 571
    .local v6, "uriBuilder":Landroid/net/Uri$Builder;
    const-string v7, "install_id"

    invoke-static {p0}, Lru/cn/utils/Utils;->getUUID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 573
    if-eqz v1, :cond_0

    .line 574
    const-string v7, "gad_id"

    invoke-virtual {v6, v7, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 577
    :cond_0
    invoke-virtual {v6}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v7}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    .line 595
    .end local v6    # "uriBuilder":Landroid/net/Uri$Builder;
    :goto_0
    return-object v7

    .line 579
    :cond_1
    invoke-virtual {v0}, Landroid/net/Uri;->getEncodedQuery()Ljava/lang/String;

    move-result-object v5

    .line 581
    .local v5, "query":Ljava/lang/String;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 582
    .local v4, "parts":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 583
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 586
    :cond_2
    const-string v7, "="

    new-array v8, v12, [Ljava/lang/String;

    const-string v9, "install_id"

    aput-object v9, v8, v10

    invoke-static {p0}, Lru/cn/utils/Utils;->getUUID(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v11

    invoke-static {v8}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v3

    .line 587
    .local v3, "installIdPart":Ljava/lang/String;
    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 588
    if-eqz v1, :cond_3

    .line 589
    const-string v7, "="

    new-array v8, v12, [Ljava/lang/String;

    const-string v9, "gad_id"

    aput-object v9, v8, v10

    aput-object v1, v8, v11

    invoke-static {v8}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    .line 590
    .local v2, "gadPart":Ljava/lang/String;
    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 593
    .end local v2    # "gadPart":Ljava/lang/String;
    :cond_3
    const-string v7, "&"

    invoke-static {v7, v4}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v5

    .line 596
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v7

    .line 597
    invoke-virtual {v7, v5}, Landroid/net/Uri$Builder;->encodedQuery(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v7

    .line 598
    invoke-virtual {v7}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v7

    .line 599
    invoke-virtual {v7}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    goto :goto_0
.end method

.method public static changeToTVMode(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x1

    .line 161
    sget v1, Lru/cn/utils/Utils;->counterDPadKey:I

    const/16 v2, 0x8

    if-ge v1, v2, :cond_0

    .line 170
    :goto_0
    return-void

    .line 164
    :cond_0
    sput-boolean v3, Lru/cn/utils/Utils;->isTV:Z

    .line 165
    const-string v1, "tv_interface"

    invoke-static {p0, v1, v3}, Lru/cn/domain/Preferences;->setBoolean(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 167
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 168
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 169
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private static checkCanPlayMulticast()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 341
    const-string v1, "NV312W"

    sget-object v2, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "NV-310-Wac:revB"

    sget-object v2, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    .line 342
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "NV310WAC"

    sget-object v2, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    .line 343
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "NV501WAC"

    sget-object v2, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    .line 344
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "NV501"

    sget-object v2, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    .line 345
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 390
    :cond_0
    :goto_0
    return v0

    .line 350
    :cond_1
    const-string v1, "TVIP"

    sget-object v2, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "tvip_s410"

    sget-object v2, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    .line 351
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 356
    :cond_2
    const-string v1, "Vermax HD100"

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "Vermax UHD200"

    sget-object v2, Landroid/os/Build;->MODEL:Ljava/lang/String;

    .line 357
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 362
    const-string v1, "NVIDIA"

    sget-object v2, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "foster"

    sget-object v2, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 367
    :cond_3
    const-string v1, "Asus"

    sget-object v2, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "fugu"

    sget-object v2, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 371
    :cond_4
    sget-boolean v1, Lru/cn/utils/Utils;->isTV:Z

    if-eqz v1, :cond_5

    .line 372
    sget-object v1, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    const-string v2, "rk30sdk"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 377
    sget-object v1, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    const-string v2, "rk31sdk"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 382
    const-string v1, "MBX"

    sget-object v2, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, "g18ref"

    sget-object v2, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    .line 383
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "f16ref"

    sget-object v2, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    .line 384
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "m201"

    sget-object v2, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    .line 385
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 390
    :cond_5
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method private static checkLauncherInstalled(Landroid/content/Context;)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 119
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 120
    .local v2, "pm":Landroid/content/pm/PackageManager;
    const/4 v1, 0x0

    .line 122
    .local v1, "packageInfo":Landroid/content/pm/PackageInfo;
    :try_start_0
    const-string v4, "ru.cn.launcher"

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 127
    :goto_0
    if-eqz v1, :cond_0

    const/4 v3, 0x1

    :cond_0
    return v3

    .line 123
    :catch_0
    move-exception v0

    .line 124
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-static {v0}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private static checkLeanback(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 114
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 115
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "android.software.leanback"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 114
    :goto_0
    return v0

    .line 115
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static checkMulticast()Z
    .locals 2

    .prologue
    .line 110
    new-instance v0, Ljava/io/File;

    const-string v1, "/proc/net/igmp"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    return v0
.end method

.method private static checkShouldAvoidWebView()Z
    .locals 2

    .prologue
    .line 423
    const-string v0, "LeMobile"

    sget-object v1, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "le_s2_ww"

    sget-object v1, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    .line 424
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 425
    const/4 v0, 0x1

    .line 427
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static dpToPx(ILandroid/content/res/Resources;)I
    .locals 3
    .param p0, "dpRes"    # I
    .param p1, "resources"    # Landroid/content/res/Resources;

    .prologue
    .line 440
    const/4 v0, 0x1

    .line 441
    invoke-virtual {p1, p0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    .line 440
    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method public static dpToPx(Landroid/content/Context;I)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "dp"    # I

    .prologue
    .line 435
    const/4 v0, 0x1

    int-to-float v1, p1

    .line 436
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    .line 435
    invoke-static {v0, v1, v2}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method private static encodedString(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "notEncoded"    # Ljava/lang/String;

    .prologue
    .line 323
    if-nez p0, :cond_0

    .line 324
    const-string v0, "na"

    .line 332
    :goto_0
    return-object v0

    .line 326
    :cond_0
    const-string v1, " "

    const-string v2, "_"

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 328
    .local v0, "encoded":Ljava/lang/String;
    :try_start_0
    const-string v1, "UTF-8"

    invoke-static {v0, v1}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 329
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static format(Ljava/util/Calendar;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "calendar"    # Ljava/util/Calendar;
    .param p1, "format"    # Ljava/lang/String;

    .prologue
    .line 225
    new-instance v0, Ljava/text/SimpleDateFormat;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 226
    .local v0, "sdf":Ljava/text/SimpleDateFormat;
    invoke-virtual {v0, p0}, Ljava/text/SimpleDateFormat;->setCalendar(Ljava/util/Calendar;)V

    .line 227
    invoke-virtual {p0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getAndroidId(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 445
    sget-object v0, Lru/cn/utils/Utils;->androidId:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 446
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "android_id"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lru/cn/utils/Utils;->androidId:Ljava/lang/String;

    .line 449
    :cond_0
    sget-object v0, Lru/cn/utils/Utils;->androidId:Ljava/lang/String;

    return-object v0
.end method

.method public static getAppVersionName(Landroid/content/Context;)Ljava/lang/String;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 263
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 264
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    .line 263
    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 265
    .local v1, "packageInfo":Landroid/content/pm/PackageInfo;
    iget-object v2, v1, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 268
    .end local v1    # "packageInfo":Landroid/content/pm/PackageInfo;
    :goto_0
    return-object v2

    .line 266
    :catch_0
    move-exception v0

    .line 267
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-static {v0}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    .line 268
    const-string v2, "unknown"

    goto :goto_0
.end method

.method public static getCalendar()Ljava/util/Calendar;
    .locals 1

    .prologue
    .line 174
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    return-object v0
.end method

.method public static getCalendar(III)Ljava/util/Calendar;
    .locals 6
    .param p0, "year"    # I
    .param p1, "month"    # I
    .param p2, "day"    # I

    .prologue
    const/4 v3, 0x0

    .line 208
    move v0, p0

    move v1, p1

    move v2, p2

    move v4, v3

    move v5, v3

    invoke-static/range {v0 .. v5}, Lru/cn/utils/Utils;->getCalendar(IIIIII)Ljava/util/Calendar;

    move-result-object v0

    return-object v0
.end method

.method public static getCalendar(IIII)Ljava/util/Calendar;
    .locals 7
    .param p0, "year"    # I
    .param p1, "month"    # I
    .param p2, "day"    # I
    .param p3, "timezone"    # I

    .prologue
    const/4 v3, 0x0

    .line 213
    move v0, p0

    move v1, p1

    move v2, p2

    move v4, v3

    move v5, v3

    move v6, p3

    invoke-static/range {v0 .. v6}, Lru/cn/utils/Utils;->getCalendar(IIIIIII)Ljava/util/Calendar;

    move-result-object v0

    return-object v0
.end method

.method public static getCalendar(IIIIII)Ljava/util/Calendar;
    .locals 3
    .param p0, "year"    # I
    .param p1, "month"    # I
    .param p2, "day"    # I
    .param p3, "hour"    # I
    .param p4, "minute"    # I
    .param p5, "second"    # I

    .prologue
    .line 185
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 186
    .local v0, "calendar":Ljava/util/Calendar;
    const/4 v1, 0x1

    invoke-virtual {v0, v1, p0}, Ljava/util/Calendar;->set(II)V

    .line 187
    const/4 v1, 0x2

    add-int/lit8 v2, p1, -0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 188
    const/4 v1, 0x5

    invoke-virtual {v0, v1, p2}, Ljava/util/Calendar;->set(II)V

    .line 189
    const/16 v1, 0xb

    invoke-virtual {v0, v1, p3}, Ljava/util/Calendar;->set(II)V

    .line 190
    const/16 v1, 0xc

    invoke-virtual {v0, v1, p4}, Ljava/util/Calendar;->set(II)V

    .line 191
    const/16 v1, 0xd

    invoke-virtual {v0, v1, p5}, Ljava/util/Calendar;->set(II)V

    .line 192
    const/16 v1, 0xe

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 194
    return-object v0
.end method

.method public static getCalendar(IIIIIII)Ljava/util/Calendar;
    .locals 4
    .param p0, "year"    # I
    .param p1, "month"    # I
    .param p2, "day"    # I
    .param p3, "hour"    # I
    .param p4, "minute"    # I
    .param p5, "second"    # I
    .param p6, "timezone"    # I

    .prologue
    .line 199
    invoke-static/range {p0 .. p5}, Lru/cn/utils/Utils;->getCalendar(IIIIII)Ljava/util/Calendar;

    move-result-object v0

    .line 201
    .local v0, "calendar":Ljava/util/Calendar;
    new-instance v1, Ljava/util/SimpleTimeZone;

    mul-int/lit16 v2, p6, 0x3e8

    invoke-static {p6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/util/SimpleTimeZone;-><init>(ILjava/lang/String;)V

    .line 202
    .local v1, "tz":Ljava/util/TimeZone;
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 204
    return-object v0
.end method

.method public static getCalendar(Ljava/lang/String;)Ljava/util/Calendar;
    .locals 4
    .param p0, "date"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/text/ParseException;
        }
    .end annotation

    .prologue
    .line 217
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "yyyy-MM-dd"

    .line 218
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 219
    .local v1, "format":Ljava/text/SimpleDateFormat;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 220
    .local v0, "c":Ljava/util/Calendar;
    invoke-virtual {v1, p0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 221
    return-object v0
.end method

.method public static getCalendar(Ljava/util/Date;)Ljava/util/Calendar;
    .locals 1
    .param p0, "date"    # Ljava/util/Date;

    .prologue
    .line 178
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 179
    .local v0, "c":Ljava/util/Calendar;
    invoke-virtual {v0, p0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 180
    return-object v0
.end method

.method public static getDeviceString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 296
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 297
    .local v0, "b":Ljava/lang/StringBuilder;
    sget-object v1, Landroid/os/Build;->BRAND:Ljava/lang/String;

    invoke-static {v1}, Lru/cn/utils/Utils;->encodedString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 298
    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 299
    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-static {v1}, Lru/cn/utils/Utils;->encodedString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 300
    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 301
    sget-object v1, Landroid/os/Build;->BOARD:Ljava/lang/String;

    invoke-static {v1}, Lru/cn/utils/Utils;->encodedString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 302
    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 303
    sget-object v1, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-static {v1}, Lru/cn/utils/Utils;->encodedString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 304
    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 305
    invoke-static {}, Lru/cn/utils/Utils;->getMainCPUABI()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lru/cn/utils/Utils;->encodedString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 306
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getDeviceTypeString(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 282
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 283
    .local v0, "b":Ljava/lang/StringBuilder;
    invoke-static {}, Lru/cn/utils/Utils;->isTV()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 284
    const-string v1, "stb"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 290
    :goto_0
    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 291
    sget-object v1, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-static {v1}, Lru/cn/utils/Utils;->encodedString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 292
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 285
    :cond_0
    invoke-static {p0}, Lru/cn/utils/Utils;->isTablet(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 286
    const-string v1, "tablet"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 288
    :cond_1
    const-string v1, "phone"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public static getGadId(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 551
    :try_start_0
    invoke-static {p0}, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient;->getAdvertisingIdInfo(Landroid/content/Context;)Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;

    move-result-object v0

    .line 552
    .local v0, "adInfo":Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;->isLimitAdTrackingEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 553
    invoke-virtual {v0}, Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;->getId()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 558
    .end local v0    # "adInfo":Lcom/google/android/gms/ads/identifier/AdvertisingIdClient$Info;
    :goto_0
    return-object v1

    .line 555
    :catch_0
    move-exception v1

    .line 558
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static getMainCPUABI()Ljava/lang/String;
    .locals 2

    .prologue
    .line 310
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_1

    .line 311
    sget-object v0, Landroid/os/Build;->SUPPORTED_ABIS:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_0

    sget-object v0, Landroid/os/Build;->SUPPORTED_ABIS:[Ljava/lang/String;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    .line 319
    :goto_0
    return-object v0

    .line 311
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 315
    :cond_1
    const-string v0, "x86"

    sget-object v1, Landroid/os/Build;->CPU_ABI2:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "arm64-v8a"

    sget-object v1, Landroid/os/Build;->CPU_ABI2:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 316
    :cond_2
    sget-object v0, Landroid/os/Build;->CPU_ABI2:Ljava/lang/String;

    goto :goto_0

    .line 319
    :cond_3
    sget-object v0, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    goto :goto_0
.end method

.method public static getOSString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 277
    sget-boolean v1, Lru/cn/utils/Utils;->isLeanback:Z

    if-eqz v1, :cond_0

    const-string v0, "AndroidTV"

    .line 278
    .local v0, "osName":Ljava/lang/String;
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lru/cn/utils/Utils;->getOSVersion()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 277
    .end local v0    # "osName":Ljava/lang/String;
    :cond_0
    const-string v0, "Android"

    goto :goto_0
.end method

.method private static getOSVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 273
    sget-object v0, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    return-object v0
.end method

.method public static getSignature()Ljava/lang/String;
    .locals 1

    .prologue
    .line 604
    sget-object v0, Lru/cn/utils/Utils;->signature:Ljava/lang/String;

    return-object v0
.end method

.method private static getSignature(Landroid/content/Context;)Ljava/lang/String;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "PackageManagerGetSignatures"
        }
    .end annotation

    .prologue
    .line 617
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 618
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x40

    .line 617
    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 619
    .local v0, "info":Landroid/content/pm/PackageInfo;
    iget-object v3, v0, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    array-length v4, v3

    const/4 v5, 0x0

    if-ge v5, v4, :cond_0

    aget-object v2, v3, v5

    .line 620
    .local v2, "signature":Landroid/content/pm/Signature;
    const-string v3, "SHA"

    invoke-static {v3}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v1

    .line 621
    .local v1, "md":Ljava/security/MessageDigest;
    invoke-virtual {v2}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/security/MessageDigest;->update([B)V

    .line 622
    invoke-virtual {v1}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v3

    const/4 v4, 0x2

    invoke-static {v3, v4}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 627
    .end local v0    # "info":Landroid/content/pm/PackageInfo;
    .end local v1    # "md":Ljava/security/MessageDigest;
    .end local v2    # "signature":Landroid/content/pm/Signature;
    :goto_0
    return-object v3

    .line 624
    :catch_0
    move-exception v3

    .line 627
    :cond_0
    :goto_1
    const/4 v3, 0x0

    goto :goto_0

    .line 624
    :catch_1
    move-exception v3

    goto :goto_1
.end method

.method public static getUUID(Landroid/content/Context;)Ljava/lang/String;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 245
    sget-object v2, Lru/cn/utils/Utils;->installID:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 246
    sget-object v2, Lru/cn/utils/Utils;->installID:Ljava/lang/String;

    .line 257
    :goto_0
    return-object v2

    .line 248
    :cond_0
    const-string v2, "uuid"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 250
    .local v1, "preferences":Landroid/content/SharedPreferences;
    const-string v2, "uuid"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lru/cn/utils/Utils;->installID:Ljava/lang/String;

    .line 251
    sget-object v2, Lru/cn/utils/Utils;->installID:Ljava/lang/String;

    if-nez v2, :cond_1

    .line 252
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lru/cn/utils/Utils;->installID:Ljava/lang/String;

    .line 253
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 254
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "uuid"

    sget-object v3, Lru/cn/utils/Utils;->installID:Ljava/lang/String;

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 255
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 257
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_1
    sget-object v2, Lru/cn/utils/Utils;->installID:Ljava/lang/String;

    goto :goto_0
.end method

.method public static init(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 82
    new-instance v0, Lcom/squareup/picasso/Picasso$Builder;

    invoke-direct {v0, p0}, Lcom/squareup/picasso/Picasso$Builder;-><init>(Landroid/content/Context;)V

    .line 83
    .local v0, "b":Lcom/squareup/picasso/Picasso$Builder;
    new-instance v2, Lcom/jakewharton/picasso/OkHttp3Downloader;

    invoke-direct {v2, p0}, Lcom/jakewharton/picasso/OkHttp3Downloader;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v2}, Lcom/squareup/picasso/Picasso$Builder;->downloader(Lcom/squareup/picasso/Downloader;)Lcom/squareup/picasso/Picasso$Builder;

    .line 85
    :try_start_0
    invoke-virtual {v0}, Lcom/squareup/picasso/Picasso$Builder;->build()Lcom/squareup/picasso/Picasso;

    move-result-object v2

    invoke-static {v2}, Lcom/squareup/picasso/Picasso;->setSingletonInstance(Lcom/squareup/picasso/Picasso;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 91
    :goto_0
    invoke-static {p0}, Lru/cn/utils/Utils;->checkLauncherInstalled(Landroid/content/Context;)Z

    move-result v2

    sput-boolean v2, Lru/cn/utils/Utils;->isLauncherInstalled:Z

    .line 92
    invoke-static {p0}, Lru/cn/utils/TVInterfaceMatcher;->isTV(Landroid/content/Context;)Z

    move-result v2

    sput-boolean v2, Lru/cn/utils/Utils;->isTV:Z

    .line 93
    sget-boolean v2, Lru/cn/utils/Utils;->isTV:Z

    if-eqz v2, :cond_0

    .line 95
    const-string v2, "multicast_streams"

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lru/cn/domain/Preferences;->overrideDefaults(Ljava/lang/String;Z)V

    .line 99
    invoke-static {}, Lru/cn/utils/Utils;->checkMulticast()Z

    move-result v2

    sput-boolean v2, Lru/cn/utils/Utils;->isMulticastCapable:Z

    .line 101
    invoke-static {p0}, Lru/cn/utils/Utils;->checkLeanback(Landroid/content/Context;)Z

    move-result v2

    sput-boolean v2, Lru/cn/utils/Utils;->isLeanback:Z

    .line 103
    :cond_0
    invoke-static {}, Lru/cn/utils/Utils;->checkCanPlayMulticast()Z

    move-result v2

    sput-boolean v2, Lru/cn/utils/Utils;->canPlayMulticast:Z

    .line 105
    invoke-static {}, Lru/cn/utils/Utils;->checkShouldAvoidWebView()Z

    move-result v2

    sput-boolean v2, Lru/cn/utils/Utils;->shouldAvoidWebView:Z

    .line 106
    invoke-static {p0}, Lru/cn/utils/Utils;->getSignature(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lru/cn/utils/Utils;->signature:Ljava/lang/String;

    .line 107
    return-void

    .line 86
    :catch_0
    move-exception v1

    .line 88
    .local v1, "e":Ljava/lang/IllegalStateException;
    invoke-static {v1}, Lcom/google/devtools/build/android/desugar/runtime/ThrowableExtension;->printStackTrace(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static isLarge(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 235
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v0, v0, 0xf

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isLauncherInstalled()Z
    .locals 1

    .prologue
    .line 135
    sget-boolean v0, Lru/cn/utils/Utils;->isLauncherInstalled:Z

    return v0
.end method

.method public static isSupportMulticast(Landroid/content/Context;)Z
    .locals 5
    .param p0, "c"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 394
    new-instance v4, Lru/cn/domain/UDPProxySettings;

    invoke-direct {v4, p0}, Lru/cn/domain/UDPProxySettings;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4}, Lru/cn/domain/UDPProxySettings;->isEnabled()Z

    move-result v0

    .line 395
    .local v0, "hasUDPProxy":Z
    if-eqz v0, :cond_1

    move v1, v2

    .line 418
    :cond_0
    :goto_0
    return v1

    .line 398
    :cond_1
    sget-boolean v4, Lru/cn/utils/Utils;->isMulticastCapable:Z

    if-nez v4, :cond_2

    move v1, v3

    .line 399
    goto :goto_0

    .line 402
    :cond_2
    const-string v4, "multicast_block"

    invoke-static {v4}, Lru/cn/api/experiments/Experiments;->eligibleForExperiment(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    move v1, v3

    .line 403
    goto :goto_0

    .line 406
    :cond_3
    invoke-static {p0}, Lru/cn/player/exoplayer/ExoPlayerUtils;->isUsable(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 407
    const-string v4, "multicast_streams"

    invoke-static {p0, v4}, Lru/cn/domain/Preferences;->getBoolean(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    .line 408
    .local v1, "multicastPreference":Z
    const-string v4, "multicast_streams"

    invoke-static {p0, v4}, Lru/cn/domain/Preferences;->hasKey(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 411
    if-eqz v1, :cond_4

    move v1, v2

    .line 412
    goto :goto_0

    .line 415
    .end local v1    # "multicastPreference":Z
    :cond_4
    sget-boolean v4, Lru/cn/utils/Utils;->canPlayMulticast:Z

    if-eqz v4, :cond_5

    move v1, v2

    .line 416
    goto :goto_0

    :cond_5
    move v1, v3

    .line 418
    goto :goto_0
.end method

.method public static isTV()Z
    .locals 1

    .prologue
    .line 131
    sget-boolean v0, Lru/cn/utils/Utils;->isTV:Z

    return v0
.end method

.method public static isTablet(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 231
    invoke-static {p0}, Lru/cn/utils/Utils;->isLarge(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lru/cn/utils/Utils;->isXLarge(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isXLarge(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 239
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v0, v0, 0xf

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static maskedDrawable(Landroid/content/Context;II)Landroid/graphics/drawable/Drawable;
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "resId"    # I
    .param p2, "maskId"    # I

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    const/4 v9, 0x0

    .line 463
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-static {v7, p1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v5

    .line 464
    .local v5, "source":Landroid/graphics/Bitmap;
    invoke-virtual {v5, v11}, Landroid/graphics/Bitmap;->setHasAlpha(Z)V

    .line 466
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    .line 467
    .local v6, "width":I
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    .line 469
    .local v1, "height":I
    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v6, v1, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 470
    .local v4, "result":Landroid/graphics/Bitmap;
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 472
    .local v2, "mask":Landroid/graphics/drawable/Drawable;
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    .line 473
    .local v3, "maskingPaint":Landroid/graphics/Paint;
    new-instance v7, Landroid/graphics/PorterDuffXfermode;

    sget-object v8, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v7, v8}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v3, v7}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 474
    invoke-virtual {v3, v11}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 476
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 477
    .local v0, "canvas":Landroid/graphics/Canvas;
    invoke-virtual {v2, v10, v10, v6, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 478
    invoke-virtual {v2, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 479
    invoke-virtual {v0, v5, v9, v9, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 481
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->recycle()V

    .line 483
    new-instance v7, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-direct {v7, v8, v4}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    return-object v7
.end method

.method public static maybePlayMulticast()Z
    .locals 1

    .prologue
    .line 336
    sget-boolean v0, Lru/cn/utils/Utils;->canPlayMulticast:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lru/cn/utils/Utils;->isMulticastCapable:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static registerDPadKey(Landroid/view/KeyEvent;)Z
    .locals 4
    .param p0, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v1, 0x0

    .line 139
    sget-boolean v2, Lru/cn/utils/Utils;->isTV:Z

    if-eqz v2, :cond_1

    .line 157
    :cond_0
    :goto_0
    return v1

    .line 142
    :cond_1
    invoke-virtual {p0}, Landroid/view/KeyEvent;->getSource()I

    move-result v2

    and-int/lit16 v2, v2, 0x201

    const/16 v3, 0x201

    if-ne v2, v3, :cond_0

    .line 143
    invoke-virtual {p0}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_0

    .line 144
    invoke-virtual {p0}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    .line 145
    .local v0, "keyCode":I
    const/16 v2, 0x15

    if-eq v0, v2, :cond_2

    const/16 v2, 0x16

    if-eq v0, v2, :cond_2

    const/16 v2, 0x14

    if-eq v0, v2, :cond_2

    const/16 v2, 0x13

    if-ne v0, v2, :cond_0

    .line 149
    :cond_2
    sget v2, Lru/cn/utils/Utils;->counterDPadKey:I

    add-int/lit8 v2, v2, 0x1

    sput v2, Lru/cn/utils/Utils;->counterDPadKey:I

    .line 151
    sget v2, Lru/cn/utils/Utils;->counterDPadKey:I

    const/16 v3, 0x8

    if-ne v2, v3, :cond_0

    .line 152
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static resolveResourse(Landroid/content/Context;I)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "res"    # I

    .prologue
    .line 453
    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    .line 454
    .local v1, "typedValue":Landroid/util/TypedValue;
    invoke-virtual {p0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    .line 455
    .local v0, "theme":Landroid/content/res/Resources$Theme;
    const/4 v2, 0x1

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 456
    iget p1, v1, Landroid/util/TypedValue;->resourceId:I

    .line 458
    .end local p1    # "res":I
    :cond_0
    return p1
.end method

.method public static shouldAvoidWebView()Z
    .locals 1

    .prologue
    .line 431
    sget-boolean v0, Lru/cn/utils/Utils;->shouldAvoidWebView:Z

    return v0
.end method

.method public static versionCompare(Ljava/lang/String;Ljava/lang/String;)I
    .locals 6
    .param p0, "str1"    # Ljava/lang/String;
    .param p1, "str2"    # Ljava/lang/String;

    .prologue
    .line 501
    const-string v4, "\\."

    invoke-virtual {p0, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 502
    .local v2, "vals1":[Ljava/lang/String;
    const-string v4, "\\."

    invoke-virtual {p1, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 503
    .local v3, "vals2":[Ljava/lang/String;
    const/4 v1, 0x0

    .line 505
    .local v1, "i":I
    :goto_0
    array-length v4, v2

    if-ge v1, v4, :cond_0

    array-length v4, v3

    if-ge v1, v4, :cond_0

    aget-object v4, v2, v1

    aget-object v5, v3, v1

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 506
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 510
    :cond_0
    array-length v4, v2

    if-ge v1, v4, :cond_1

    array-length v4, v3

    if-ge v1, v4, :cond_1

    .line 511
    aget-object v4, v2, v1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    aget-object v5, v3, v1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/Integer;->compareTo(Ljava/lang/Integer;)I

    move-result v0

    .line 512
    .local v0, "diff":I
    invoke-static {v0}, Ljava/lang/Integer;->signum(I)I

    move-result v4

    .line 516
    .end local v0    # "diff":I
    :goto_1
    return v4

    :cond_1
    array-length v4, v2

    array-length v5, v3

    sub-int/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Integer;->signum(I)I

    move-result v4

    goto :goto_1
.end method
