.class final Lcom/google/obf/bo;
.super Lcom/google/obf/bt;
.source "IMASDK"


# instance fields
.field private e:Lcom/google/obf/dp;

.field private f:Lcom/google/obf/do;

.field private g:Z


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Lcom/google/obf/bt;-><init>()V

    return-void
.end method

.method static a(Lcom/google/obf/dw;)Z
    .locals 4

    .prologue
    .line 2
    invoke-virtual {p0}, Lcom/google/obf/dw;->f()I

    move-result v0

    const/16 v1, 0x7f

    if-ne v0, v1, :cond_0

    .line 3
    invoke-virtual {p0}, Lcom/google/obf/dw;->k()J

    move-result-wide v0

    const-wide/32 v2, 0x464c4143

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 4
    :goto_0
    return v0

    .line 3
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/google/obf/ak;Lcom/google/obf/ao;)I
    .locals 11
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v3, -0x1

    const/4 v0, 0x0

    const/4 v10, 0x0

    .line 5
    invoke-interface {p1}, Lcom/google/obf/ak;->c()J

    move-result-wide v6

    .line 6
    iget-object v1, p0, Lcom/google/obf/bo;->b:Lcom/google/obf/bq;

    iget-object v2, p0, Lcom/google/obf/bo;->a:Lcom/google/obf/dw;

    invoke-virtual {v1, p1, v2}, Lcom/google/obf/bq;->a(Lcom/google/obf/ak;Lcom/google/obf/dw;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 32
    :goto_0
    return v3

    .line 8
    :cond_0
    iget-object v1, p0, Lcom/google/obf/bo;->a:Lcom/google/obf/dw;

    iget-object v1, v1, Lcom/google/obf/dw;->a:[B

    .line 9
    iget-object v2, p0, Lcom/google/obf/bo;->e:Lcom/google/obf/dp;

    if-nez v2, :cond_2

    .line 10
    new-instance v2, Lcom/google/obf/dp;

    const/16 v4, 0x11

    invoke-direct {v2, v1, v4}, Lcom/google/obf/dp;-><init>([BI)V

    iput-object v2, p0, Lcom/google/obf/bo;->e:Lcom/google/obf/dp;

    .line 11
    const/16 v2, 0x9

    iget-object v4, p0, Lcom/google/obf/bo;->a:Lcom/google/obf/dw;

    invoke-virtual {v4}, Lcom/google/obf/dw;->c()I

    move-result v4

    invoke-static {v1, v2, v4}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v1

    .line 12
    const/4 v2, 0x4

    const/16 v4, -0x80

    aput-byte v4, v1, v2

    .line 13
    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v8

    .line 14
    const-string v1, "audio/x-flac"

    iget-object v2, p0, Lcom/google/obf/bo;->e:Lcom/google/obf/dp;

    .line 15
    invoke-virtual {v2}, Lcom/google/obf/dp;->a()I

    move-result v2

    iget-object v4, p0, Lcom/google/obf/bo;->e:Lcom/google/obf/dp;

    invoke-virtual {v4}, Lcom/google/obf/dp;->b()J

    move-result-wide v4

    iget-object v6, p0, Lcom/google/obf/bo;->e:Lcom/google/obf/dp;

    iget v6, v6, Lcom/google/obf/dp;->f:I

    iget-object v7, p0, Lcom/google/obf/bo;->e:Lcom/google/obf/dp;

    iget v7, v7, Lcom/google/obf/dp;->e:I

    move-object v9, v0

    .line 16
    invoke-static/range {v0 .. v9}, Lcom/google/obf/q;->a(Ljava/lang/String;Ljava/lang/String;IIJIILjava/util/List;Ljava/lang/String;)Lcom/google/obf/q;

    move-result-object v0

    .line 17
    iget-object v1, p0, Lcom/google/obf/bo;->c:Lcom/google/obf/ar;

    invoke-interface {v1, v0}, Lcom/google/obf/ar;->a(Lcom/google/obf/q;)V

    .line 31
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/obf/bo;->a:Lcom/google/obf/dw;

    invoke-virtual {v0}, Lcom/google/obf/dw;->a()V

    move v3, v10

    .line 32
    goto :goto_0

    .line 18
    :cond_2
    aget-byte v2, v1, v10

    if-ne v2, v3, :cond_5

    .line 19
    iget-boolean v1, p0, Lcom/google/obf/bo;->g:Z

    if-nez v1, :cond_3

    .line 20
    iget-object v1, p0, Lcom/google/obf/bo;->f:Lcom/google/obf/do;

    if-eqz v1, :cond_4

    .line 21
    iget-object v1, p0, Lcom/google/obf/bo;->d:Lcom/google/obf/al;

    iget-object v2, p0, Lcom/google/obf/bo;->f:Lcom/google/obf/do;

    iget-object v3, p0, Lcom/google/obf/bo;->e:Lcom/google/obf/dp;

    iget v3, v3, Lcom/google/obf/dp;->e:I

    int-to-long v8, v3

    invoke-virtual {v2, v6, v7, v8, v9}, Lcom/google/obf/do;->a(JJ)Lcom/google/obf/aq;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/obf/al;->a(Lcom/google/obf/aq;)V

    .line 22
    iput-object v0, p0, Lcom/google/obf/bo;->f:Lcom/google/obf/do;

    .line 24
    :goto_2
    iput-boolean v4, p0, Lcom/google/obf/bo;->g:Z

    .line 25
    :cond_3
    iget-object v1, p0, Lcom/google/obf/bo;->c:Lcom/google/obf/ar;

    iget-object v2, p0, Lcom/google/obf/bo;->a:Lcom/google/obf/dw;

    iget-object v3, p0, Lcom/google/obf/bo;->a:Lcom/google/obf/dw;

    invoke-virtual {v3}, Lcom/google/obf/dw;->c()I

    move-result v3

    invoke-interface {v1, v2, v3}, Lcom/google/obf/ar;->a(Lcom/google/obf/dw;I)V

    .line 26
    iget-object v1, p0, Lcom/google/obf/bo;->a:Lcom/google/obf/dw;

    invoke-virtual {v1, v10}, Lcom/google/obf/dw;->c(I)V

    .line 27
    iget-object v1, p0, Lcom/google/obf/bo;->e:Lcom/google/obf/dp;

    iget-object v2, p0, Lcom/google/obf/bo;->a:Lcom/google/obf/dw;

    invoke-static {v1, v2}, Lcom/google/obf/dq;->a(Lcom/google/obf/dp;Lcom/google/obf/dw;)J

    move-result-wide v2

    .line 28
    iget-object v1, p0, Lcom/google/obf/bo;->c:Lcom/google/obf/ar;

    iget-object v5, p0, Lcom/google/obf/bo;->a:Lcom/google/obf/dw;

    invoke-virtual {v5}, Lcom/google/obf/dw;->c()I

    move-result v5

    move v6, v10

    move-object v7, v0

    invoke-interface/range {v1 .. v7}, Lcom/google/obf/ar;->a(JIII[B)V

    goto :goto_1

    .line 23
    :cond_4
    iget-object v1, p0, Lcom/google/obf/bo;->d:Lcom/google/obf/al;

    sget-object v2, Lcom/google/obf/aq;->f:Lcom/google/obf/aq;

    invoke-interface {v1, v2}, Lcom/google/obf/al;->a(Lcom/google/obf/aq;)V

    goto :goto_2

    .line 29
    :cond_5
    aget-byte v0, v1, v10

    and-int/lit8 v0, v0, 0x7f

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/obf/bo;->f:Lcom/google/obf/do;

    if-nez v0, :cond_1

    .line 30
    iget-object v0, p0, Lcom/google/obf/bo;->a:Lcom/google/obf/dw;

    invoke-static {v0}, Lcom/google/obf/do;->a(Lcom/google/obf/dw;)Lcom/google/obf/do;

    move-result-object v0

    iput-object v0, p0, Lcom/google/obf/bo;->f:Lcom/google/obf/do;

    goto :goto_1
.end method
