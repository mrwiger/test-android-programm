.class public abstract Lcom/google/ads/interactivemedia/v3/impl/data/k;
.super Ljava/lang/Object;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/ads/interactivemedia/v3/impl/data/k$a;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static builder()Lcom/google/ads/interactivemedia/v3/impl/data/k$a;
    .locals 1

    .prologue
    .line 45
    new-instance v0, Lcom/google/ads/interactivemedia/v3/impl/data/h$a;

    invoke-direct {v0}, Lcom/google/ads/interactivemedia/v3/impl/data/h$a;-><init>()V

    return-object v0
.end method

.method public static create(Lcom/google/ads/interactivemedia/v3/api/AdsRequest;Ljava/lang/String;Ljava/lang/String;Lcom/google/ads/interactivemedia/v3/api/ImaSdkSettings;Lcom/google/obf/gp$b;Z)Lcom/google/ads/interactivemedia/v3/impl/data/k;
    .locals 13

    .prologue
    .line 2
    invoke-interface {p0}, Lcom/google/ads/interactivemedia/v3/api/AdsRequest;->getAdTagUrl()Ljava/lang/String;

    move-result-object v2

    .line 3
    invoke-interface {p0}, Lcom/google/ads/interactivemedia/v3/api/AdsRequest;->getAdsResponse()Ljava/lang/String;

    move-result-object v3

    .line 4
    invoke-interface {p0}, Lcom/google/ads/interactivemedia/v3/api/AdsRequest;->getExtraParameters()Ljava/util/Map;

    move-result-object v4

    move-object v1, p0

    .line 5
    check-cast v1, Lcom/google/obf/gu;

    invoke-virtual {v1}, Lcom/google/obf/gu;->a()Lcom/google/obf/gu$a;

    move-result-object v5

    move-object v1, p0

    .line 6
    check-cast v1, Lcom/google/obf/gu;

    invoke-virtual {v1}, Lcom/google/obf/gu;->b()Ljava/lang/Float;

    move-result-object v6

    move-object v1, p0

    .line 7
    check-cast v1, Lcom/google/obf/gu;

    invoke-virtual {v1}, Lcom/google/obf/gu;->c()Ljava/util/List;

    move-result-object v7

    move-object v1, p0

    .line 8
    check-cast v1, Lcom/google/obf/gu;

    invoke-virtual {v1}, Lcom/google/obf/gu;->d()Ljava/lang/String;

    move-result-object v8

    move-object v1, p0

    .line 9
    check-cast v1, Lcom/google/obf/gu;

    invoke-virtual {v1}, Lcom/google/obf/gu;->e()Ljava/lang/Float;

    move-result-object v9

    .line 11
    invoke-interface {p0}, Lcom/google/ads/interactivemedia/v3/api/AdsRequest;->getAdDisplayContainer()Lcom/google/ads/interactivemedia/v3/api/AdDisplayContainer;

    move-result-object v1

    check-cast v1, Lcom/google/obf/gj;

    .line 12
    invoke-static {v1}, Lcom/google/ads/interactivemedia/v3/impl/data/k;->getCompanionSlots(Lcom/google/obf/gx;)Ljava/util/Map;

    move-result-object v1

    .line 13
    invoke-interface {p0}, Lcom/google/ads/interactivemedia/v3/api/AdsRequest;->getAdDisplayContainer()Lcom/google/ads/interactivemedia/v3/api/AdDisplayContainer;

    move-result-object v10

    invoke-interface {v10}, Lcom/google/ads/interactivemedia/v3/api/AdDisplayContainer;->getAdContainer()Landroid/view/ViewGroup;

    move-result-object v10

    .line 14
    invoke-static {v10}, Landroid/support/v4/view/ViewCompat;->isAttachedToWindow(Landroid/view/View;)Z

    move-result v11

    .line 16
    invoke-static {v10}, Lcom/google/ads/interactivemedia/v3/impl/data/a$a;->createFromLocationOnScreen(Landroid/view/View;)Lcom/google/ads/interactivemedia/v3/impl/data/a$a;

    move-result-object v10

    .line 17
    invoke-static {}, Lcom/google/ads/interactivemedia/v3/impl/data/k;->builder()Lcom/google/ads/interactivemedia/v3/impl/data/k$a;

    move-result-object v12

    invoke-interface {v12, v2}, Lcom/google/ads/interactivemedia/v3/impl/data/k$a;->adTagUrl(Ljava/lang/String;)Lcom/google/ads/interactivemedia/v3/impl/data/k$a;

    move-result-object v2

    invoke-interface {v2, v3}, Lcom/google/ads/interactivemedia/v3/impl/data/k$a;->adsResponse(Ljava/lang/String;)Lcom/google/ads/interactivemedia/v3/impl/data/k$a;

    move-result-object v2

    invoke-interface {v2, p1}, Lcom/google/ads/interactivemedia/v3/impl/data/k$a;->env(Ljava/lang/String;)Lcom/google/ads/interactivemedia/v3/impl/data/k$a;

    move-result-object v2

    .line 18
    invoke-interface {v2, p2}, Lcom/google/ads/interactivemedia/v3/impl/data/k$a;->network(Ljava/lang/String;)Lcom/google/ads/interactivemedia/v3/impl/data/k$a;

    move-result-object v2

    invoke-interface {v2, v4}, Lcom/google/ads/interactivemedia/v3/impl/data/k$a;->extraParameters(Ljava/util/Map;)Lcom/google/ads/interactivemedia/v3/impl/data/k$a;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-interface {v2, v0}, Lcom/google/ads/interactivemedia/v3/impl/data/k$a;->settings(Lcom/google/ads/interactivemedia/v3/api/ImaSdkSettings;)Lcom/google/ads/interactivemedia/v3/impl/data/k$a;

    move-result-object v2

    .line 19
    invoke-interface {v2, v5}, Lcom/google/ads/interactivemedia/v3/impl/data/k$a;->videoPlayActivation(Lcom/google/obf/gu$a;)Lcom/google/ads/interactivemedia/v3/impl/data/k$a;

    move-result-object v2

    invoke-interface {v2, v6}, Lcom/google/ads/interactivemedia/v3/impl/data/k$a;->contentDuration(Ljava/lang/Float;)Lcom/google/ads/interactivemedia/v3/impl/data/k$a;

    move-result-object v2

    .line 20
    invoke-interface {v2, v7}, Lcom/google/ads/interactivemedia/v3/impl/data/k$a;->contentKeywords(Ljava/util/List;)Lcom/google/ads/interactivemedia/v3/impl/data/k$a;

    move-result-object v2

    invoke-interface {v2, v8}, Lcom/google/ads/interactivemedia/v3/impl/data/k$a;->contentTitle(Ljava/lang/String;)Lcom/google/ads/interactivemedia/v3/impl/data/k$a;

    move-result-object v2

    .line 21
    invoke-interface {v2, v9}, Lcom/google/ads/interactivemedia/v3/impl/data/k$a;->vastLoadTimeout(Ljava/lang/Float;)Lcom/google/ads/interactivemedia/v3/impl/data/k$a;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/google/ads/interactivemedia/v3/impl/data/k$a;->companionSlots(Ljava/util/Map;)Lcom/google/ads/interactivemedia/v3/impl/data/k$a;

    move-result-object v1

    .line 22
    move-object/from16 v0, p4

    invoke-interface {v1, v0}, Lcom/google/ads/interactivemedia/v3/impl/data/k$a;->marketAppInfo(Lcom/google/obf/gp$b;)Lcom/google/ads/interactivemedia/v3/impl/data/k$a;

    move-result-object v1

    invoke-static/range {p5 .. p5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/ads/interactivemedia/v3/impl/data/k$a;->isTv(Ljava/lang/Boolean;)Lcom/google/ads/interactivemedia/v3/impl/data/k$a;

    move-result-object v1

    .line 23
    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/ads/interactivemedia/v3/impl/data/k$a;->isAdContainerAttachedToWindow(Ljava/lang/Boolean;)Lcom/google/ads/interactivemedia/v3/impl/data/k$a;

    move-result-object v1

    .line 24
    invoke-interface {v1, v10}, Lcom/google/ads/interactivemedia/v3/impl/data/k$a;->adContainerBounds(Lcom/google/ads/interactivemedia/v3/impl/data/a$a;)Lcom/google/ads/interactivemedia/v3/impl/data/k$a;

    move-result-object v1

    .line 25
    invoke-interface {v1}, Lcom/google/ads/interactivemedia/v3/impl/data/k$a;->build()Lcom/google/ads/interactivemedia/v3/impl/data/k;

    move-result-object v1

    .line 26
    return-object v1
.end method

.method private static getCompanionSlots(Lcom/google/obf/gx;)Ljava/util/Map;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/obf/gx;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/google/obf/gx;->a()Ljava/util/Map;

    move-result-object v2

    .line 47
    if-eqz v2, :cond_1

    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 48
    new-instance v3, Lcom/google/obf/jm$a;

    invoke-direct {v3}, Lcom/google/obf/jm$a;-><init>()V

    .line 49
    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 50
    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/ads/interactivemedia/v3/api/CompanionAdSlot;

    .line 51
    invoke-interface {v1}, Lcom/google/ads/interactivemedia/v3/api/CompanionAdSlot;->getWidth()I

    move-result v5

    invoke-interface {v1}, Lcom/google/ads/interactivemedia/v3/api/CompanionAdSlot;->getHeight()I

    move-result v1

    const/16 v6, 0x17

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "x"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Lcom/google/obf/jm$a;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/obf/jm$a;

    goto :goto_0

    .line 53
    :cond_0
    invoke-virtual {v3}, Lcom/google/obf/jm$a;->a()Lcom/google/obf/jm;

    move-result-object v0

    .line 54
    :goto_1
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public abstract adContainerBounds()Lcom/google/ads/interactivemedia/v3/impl/data/a$a;
.end method

.method public abstract adTagParameters()Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract adTagUrl()Ljava/lang/String;
.end method

.method public abstract adsResponse()Ljava/lang/String;
.end method

.method public abstract apiKey()Ljava/lang/String;
.end method

.method public abstract assetKey()Ljava/lang/String;
.end method

.method public abstract authToken()Ljava/lang/String;
.end method

.method public abstract companionSlots()Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract contentDuration()Ljava/lang/Float;
.end method

.method public abstract contentKeywords()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract contentSourceId()Ljava/lang/String;
.end method

.method public abstract contentTitle()Ljava/lang/String;
.end method

.method public abstract env()Ljava/lang/String;
.end method

.method public abstract extraParameters()Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract idType()Ljava/lang/String;
.end method

.method public abstract isAdContainerAttachedToWindow()Ljava/lang/Boolean;
.end method

.method public abstract isLat()Ljava/lang/String;
.end method

.method public abstract isTv()Ljava/lang/Boolean;
.end method

.method public abstract marketAppInfo()Lcom/google/obf/gp$b;
.end method

.method public abstract msParameter()Ljava/lang/String;
.end method

.method public abstract network()Ljava/lang/String;
.end method

.method public abstract rdid()Ljava/lang/String;
.end method

.method public abstract settings()Lcom/google/ads/interactivemedia/v3/api/ImaSdkSettings;
.end method

.method public abstract streamActivityMonitorId()Ljava/lang/String;
.end method

.method public abstract vastLoadTimeout()Ljava/lang/Float;
.end method

.method public abstract videoId()Ljava/lang/String;
.end method

.method public abstract videoPlayActivation()Lcom/google/obf/gu$a;
.end method
