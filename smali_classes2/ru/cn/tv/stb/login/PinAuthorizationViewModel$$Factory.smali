.class public final Lru/cn/tv/stb/login/PinAuthorizationViewModel$$Factory;
.super Ljava/lang/Object;
.source "PinAuthorizationViewModel$$Factory.java"

# interfaces
.implements Ltoothpick/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ltoothpick/Factory",
        "<",
        "Lru/cn/tv/stb/login/PinAuthorizationViewModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic createInstance(Ltoothpick/Scope;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 7
    invoke-virtual {p0, p1}, Lru/cn/tv/stb/login/PinAuthorizationViewModel$$Factory;->createInstance(Ltoothpick/Scope;)Lru/cn/tv/stb/login/PinAuthorizationViewModel;

    move-result-object v0

    return-object v0
.end method

.method public createInstance(Ltoothpick/Scope;)Lru/cn/tv/stb/login/PinAuthorizationViewModel;
    .locals 1
    .param p1, "scope"    # Ltoothpick/Scope;

    .prologue
    .line 10
    new-instance v0, Lru/cn/tv/stb/login/PinAuthorizationViewModel;

    invoke-direct {v0}, Lru/cn/tv/stb/login/PinAuthorizationViewModel;-><init>()V

    .line 11
    .local v0, "pinAuthorizationViewModel":Lru/cn/tv/stb/login/PinAuthorizationViewModel;
    return-object v0
.end method

.method public getTargetScope(Ltoothpick/Scope;)Ltoothpick/Scope;
    .locals 0
    .param p1, "scope"    # Ltoothpick/Scope;

    .prologue
    .line 16
    return-object p1
.end method

.method public hasProvidesSingletonInScopeAnnotation()Z
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x0

    return v0
.end method

.method public hasScopeAnnotation()Z
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x0

    return v0
.end method
