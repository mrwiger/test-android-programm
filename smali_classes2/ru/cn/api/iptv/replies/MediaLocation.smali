.class public Lru/cn/api/iptv/replies/MediaLocation;
.super Ljava/lang/Object;
.source "MediaLocation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/api/iptv/replies/MediaLocation$Access;,
        Lru/cn/api/iptv/replies/MediaLocation$Type;
    }
.end annotation


# instance fields
.field public access:Lru/cn/api/iptv/replies/MediaLocation$Access;

.field public adTags:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public adTargeting:Z

.field public allowedTill:I

.field public aspectH:F

.field public aspectW:F

.field public channelId:J

.field public contractorId:J

.field public cropX:I

.field public cropY:I

.field public groupTitle:Ljava/lang/String;

.field public hasRecords:Z

.field public hasTimeshift:Z

.field public final location:Ljava/lang/String;

.field public noAdsPartner:Z

.field public sourceUri:Ljava/lang/String;

.field public streamId:J

.field public territoryId:J

.field public timeshiftBaseUri:Ljava/lang/String;

.field public timezoneOffset:Ljava/lang/Long;

.field public title:Ljava/lang/String;

.field public final type:Lru/cn/api/iptv/replies/MediaLocation$Type;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lru/cn/api/iptv/replies/MediaLocation;-><init>(Ljava/lang/String;)V

    .line 54
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2
    .param p1, "location"    # Ljava/lang/String;

    .prologue
    const/16 v1, 0x64

    const/4 v0, 0x0

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput v1, p0, Lru/cn/api/iptv/replies/MediaLocation;->cropX:I

    .line 28
    iput v1, p0, Lru/cn/api/iptv/replies/MediaLocation;->cropY:I

    .line 30
    iput v0, p0, Lru/cn/api/iptv/replies/MediaLocation;->aspectW:F

    .line 31
    iput v0, p0, Lru/cn/api/iptv/replies/MediaLocation;->aspectH:F

    .line 33
    sget-object v0, Lru/cn/api/iptv/replies/MediaLocation$Access;->allowed:Lru/cn/api/iptv/replies/MediaLocation$Access;

    iput-object v0, p0, Lru/cn/api/iptv/replies/MediaLocation;->access:Lru/cn/api/iptv/replies/MediaLocation$Access;

    .line 57
    invoke-static {p1}, Lru/cn/api/iptv/replies/MediaLocation;->detectType(Ljava/lang/String;)Lru/cn/api/iptv/replies/MediaLocation$Type;

    move-result-object v0

    iput-object v0, p0, Lru/cn/api/iptv/replies/MediaLocation;->type:Lru/cn/api/iptv/replies/MediaLocation$Type;

    .line 58
    iput-object p1, p0, Lru/cn/api/iptv/replies/MediaLocation;->location:Ljava/lang/String;

    .line 59
    return-void
.end method

.method private static detectType(Ljava/lang/String;)Lru/cn/api/iptv/replies/MediaLocation$Type;
    .locals 1
    .param p0, "location"    # Ljava/lang/String;

    .prologue
    .line 62
    const-string v0, "udp://"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    sget-object v0, Lru/cn/api/iptv/replies/MediaLocation$Type;->mcastudp:Lru/cn/api/iptv/replies/MediaLocation$Type;

    .line 70
    :goto_0
    return-object v0

    .line 64
    :cond_0
    const-string v0, "magnet"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 65
    sget-object v0, Lru/cn/api/iptv/replies/MediaLocation$Type;->magnet:Lru/cn/api/iptv/replies/MediaLocation$Type;

    goto :goto_0

    .line 66
    :cond_1
    const-string v0, "http://"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "https://"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 67
    :cond_2
    sget-object v0, Lru/cn/api/iptv/replies/MediaLocation$Type;->http:Lru/cn/api/iptv/replies/MediaLocation$Type;

    goto :goto_0

    .line 70
    :cond_3
    sget-object v0, Lru/cn/api/iptv/replies/MediaLocation$Type;->undefined:Lru/cn/api/iptv/replies/MediaLocation$Type;

    goto :goto_0
.end method


# virtual methods
.method public getAspectRatio()F
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 74
    iget v1, p0, Lru/cn/api/iptv/replies/MediaLocation;->aspectW:F

    cmpl-float v1, v1, v0

    if-lez v1, :cond_0

    iget v1, p0, Lru/cn/api/iptv/replies/MediaLocation;->aspectH:F

    cmpl-float v1, v1, v0

    if-lez v1, :cond_0

    .line 75
    iget v0, p0, Lru/cn/api/iptv/replies/MediaLocation;->aspectW:F

    iget v1, p0, Lru/cn/api/iptv/replies/MediaLocation;->aspectH:F

    div-float/2addr v0, v1

    .line 78
    :cond_0
    return v0
.end method
