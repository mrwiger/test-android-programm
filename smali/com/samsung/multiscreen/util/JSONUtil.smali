.class public Lcom/samsung/multiscreen/util/JSONUtil;
.super Ljava/lang/Object;
.source "JSONUtil.java"


# static fields
.field private static containerFactory:Lorg/json/simple/parser/ContainerFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    new-instance v0, Lcom/samsung/multiscreen/util/JSONUtil$1;

    invoke-direct {v0}, Lcom/samsung/multiscreen/util/JSONUtil$1;-><init>()V

    sput-object v0, Lcom/samsung/multiscreen/util/JSONUtil;->containerFactory:Lorg/json/simple/parser/ContainerFactory;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static parse(Ljava/lang/String;)Ljava/util/Map;
    .locals 5
    .param p0, "data"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 69
    if-nez p0, :cond_0

    sget-object v4, Lcom/samsung/multiscreen/util/JSONUtil;->containerFactory:Lorg/json/simple/parser/ContainerFactory;

    invoke-interface {v4}, Lorg/json/simple/parser/ContainerFactory;->createObjectContainer()Ljava/util/Map;

    move-result-object v2

    .line 80
    :goto_0
    return-object v2

    .line 70
    :cond_0
    new-instance v3, Lorg/json/simple/parser/JSONParser;

    invoke-direct {v3}, Lorg/json/simple/parser/JSONParser;-><init>()V

    .line 72
    .local v3, "parser":Lorg/json/simple/parser/JSONParser;
    :try_start_0
    sget-object v4, Lcom/samsung/multiscreen/util/JSONUtil;->containerFactory:Lorg/json/simple/parser/ContainerFactory;

    invoke-virtual {v3, p0, v4}, Lorg/json/simple/parser/JSONParser;->parse(Ljava/lang/String;Lorg/json/simple/parser/ContainerFactory;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/json/simple/parser/ParseException; {:try_start_0 .. :try_end_0} :catch_1

    .local v2, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    goto :goto_0

    .line 73
    .end local v2    # "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    :catch_0
    move-exception v0

    .line 75
    .local v0, "cce":Ljava/lang/ClassCastException;
    sget-object v4, Lcom/samsung/multiscreen/util/JSONUtil;->containerFactory:Lorg/json/simple/parser/ContainerFactory;

    invoke-interface {v4}, Lorg/json/simple/parser/ContainerFactory;->createObjectContainer()Ljava/util/Map;

    move-result-object v2

    .line 79
    .restart local v2    # "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    goto :goto_0

    .line 76
    .end local v0    # "cce":Ljava/lang/ClassCastException;
    .end local v2    # "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    :catch_1
    move-exception v1

    .line 78
    .local v1, "e":Lorg/json/simple/parser/ParseException;
    sget-object v4, Lcom/samsung/multiscreen/util/JSONUtil;->containerFactory:Lorg/json/simple/parser/ContainerFactory;

    invoke-interface {v4}, Lorg/json/simple/parser/ContainerFactory;->createObjectContainer()Ljava/util/Map;

    move-result-object v2

    .restart local v2    # "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    goto :goto_0
.end method

.method public static parseArray(Ljava/lang/String;)Lorg/json/simple/JSONArray;
    .locals 3
    .param p0, "data"    # Ljava/lang/String;

    .prologue
    .line 107
    new-instance v1, Lorg/json/simple/parser/JSONParser;

    invoke-direct {v1}, Lorg/json/simple/parser/JSONParser;-><init>()V

    .line 109
    .local v1, "parser":Lorg/json/simple/parser/JSONParser;
    :try_start_0
    invoke-virtual {v1, p0}, Lorg/json/simple/parser/JSONParser;->parse(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/json/simple/JSONArray;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 111
    :goto_0
    return-object v2

    .line 110
    :catch_0
    move-exception v0

    .line 111
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Lorg/json/simple/JSONArray;

    invoke-direct {v2}, Lorg/json/simple/JSONArray;-><init>()V

    goto :goto_0
.end method

.method public static parseList(Ljava/lang/String;)Ljava/util/List;
    .locals 5
    .param p0, "data"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 127
    if-nez p0, :cond_0

    sget-object v4, Lcom/samsung/multiscreen/util/JSONUtil;->containerFactory:Lorg/json/simple/parser/ContainerFactory;

    invoke-interface {v4}, Lorg/json/simple/parser/ContainerFactory;->creatArrayContainer()Ljava/util/List;

    move-result-object v2

    .line 139
    :goto_0
    return-object v2

    .line 130
    :cond_0
    :try_start_0
    new-instance v3, Lorg/json/simple/parser/JSONParser;

    invoke-direct {v3}, Lorg/json/simple/parser/JSONParser;-><init>()V

    .line 131
    .local v3, "parser":Lorg/json/simple/parser/JSONParser;
    sget-object v4, Lcom/samsung/multiscreen/util/JSONUtil;->containerFactory:Lorg/json/simple/parser/ContainerFactory;

    invoke-virtual {v3, p0, v4}, Lorg/json/simple/parser/JSONParser;->parse(Ljava/lang/String;Lorg/json/simple/parser/ContainerFactory;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/json/simple/parser/ParseException; {:try_start_0 .. :try_end_0} :catch_1

    .local v2, "list":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;>;"
    goto :goto_0

    .line 132
    .end local v2    # "list":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;>;"
    .end local v3    # "parser":Lorg/json/simple/parser/JSONParser;
    :catch_0
    move-exception v0

    .line 134
    .local v0, "cce":Ljava/lang/ClassCastException;
    sget-object v4, Lcom/samsung/multiscreen/util/JSONUtil;->containerFactory:Lorg/json/simple/parser/ContainerFactory;

    invoke-interface {v4}, Lorg/json/simple/parser/ContainerFactory;->creatArrayContainer()Ljava/util/List;

    move-result-object v2

    .line 138
    .restart local v2    # "list":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;>;"
    goto :goto_0

    .line 135
    .end local v0    # "cce":Ljava/lang/ClassCastException;
    .end local v2    # "list":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;>;"
    :catch_1
    move-exception v1

    .line 137
    .local v1, "e":Lorg/json/simple/parser/ParseException;
    sget-object v4, Lcom/samsung/multiscreen/util/JSONUtil;->containerFactory:Lorg/json/simple/parser/ContainerFactory;

    invoke-interface {v4}, Lorg/json/simple/parser/ContainerFactory;->creatArrayContainer()Ljava/util/List;

    move-result-object v2

    .restart local v2    # "list":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;>;"
    goto :goto_0
.end method

.method public static parseObject(Ljava/lang/String;)Lorg/json/simple/JSONObject;
    .locals 3
    .param p0, "data"    # Ljava/lang/String;

    .prologue
    .line 91
    new-instance v1, Lorg/json/simple/parser/JSONParser;

    invoke-direct {v1}, Lorg/json/simple/parser/JSONParser;-><init>()V

    .line 93
    .local v1, "parser":Lorg/json/simple/parser/JSONParser;
    :try_start_0
    invoke-virtual {v1, p0}, Lorg/json/simple/parser/JSONParser;->parse(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/json/simple/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 95
    :goto_0
    return-object v2

    .line 94
    :catch_0
    move-exception v0

    .line 95
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Lorg/json/simple/JSONObject;

    invoke-direct {v2}, Lorg/json/simple/JSONObject;-><init>()V

    goto :goto_0
.end method

.method public static toJSONObjectMap(Ljava/lang/Object;)Ljava/util/Map;
    .locals 0
    .param p0, "object"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 152
    check-cast p0, Ljava/util/Map;

    .end local p0    # "object":Ljava/lang/Object;
    return-object p0
.end method

.method public static toJSONObjectMapList(Ljava/lang/Object;)Ljava/util/List;
    .locals 0
    .param p0, "object"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 178
    check-cast p0, Ljava/util/List;

    .end local p0    # "object":Ljava/lang/Object;
    return-object p0
.end method

.method public static toJSONString(Ljava/util/Map;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 204
    .local p0, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-static {p0}, Lorg/json/simple/JSONValue;->toJSONString(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static toObject(Ljava/util/Map;)Lorg/json/simple/JSONObject;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "Lorg/json/simple/JSONObject;"
        }
    .end annotation

    .prologue
    .line 191
    .local p0, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    new-instance v0, Lorg/json/simple/JSONObject;

    invoke-direct {v0}, Lorg/json/simple/JSONObject;-><init>()V

    .line 192
    .local v0, "jsonObject":Lorg/json/simple/JSONObject;
    invoke-virtual {v0, p0}, Lorg/json/simple/JSONObject;->putAll(Ljava/util/Map;)V

    .line 193
    return-object v0
.end method

.method public static toPropertyMap(Ljava/lang/Object;)Ljava/util/Map;
    .locals 0
    .param p0, "object"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 165
    check-cast p0, Ljava/util/Map;

    .end local p0    # "object":Ljava/lang/Object;
    return-object p0
.end method
