.class Lru/cn/tv/mobile/settings/MainPreferenceFragment$5;
.super Ljava/lang/Object;
.source "MainPreferenceFragment.java"

# interfaces
.implements Landroid/support/v7/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/tv/mobile/settings/MainPreferenceFragment;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/tv/mobile/settings/MainPreferenceFragment;

.field final synthetic val$advancedPlayback:Landroid/support/v7/preference/ListPreference;

.field final synthetic val$cachingLevel:Landroid/support/v7/preference/ListPreference;

.field final synthetic val$qualityLevel:Landroid/support/v7/preference/ListPreference;


# direct methods
.method constructor <init>(Lru/cn/tv/mobile/settings/MainPreferenceFragment;Landroid/support/v7/preference/ListPreference;Landroid/support/v7/preference/ListPreference;Landroid/support/v7/preference/ListPreference;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/tv/mobile/settings/MainPreferenceFragment;

    .prologue
    .line 120
    iput-object p1, p0, Lru/cn/tv/mobile/settings/MainPreferenceFragment$5;->this$0:Lru/cn/tv/mobile/settings/MainPreferenceFragment;

    iput-object p2, p0, Lru/cn/tv/mobile/settings/MainPreferenceFragment$5;->val$advancedPlayback:Landroid/support/v7/preference/ListPreference;

    iput-object p3, p0, Lru/cn/tv/mobile/settings/MainPreferenceFragment$5;->val$cachingLevel:Landroid/support/v7/preference/ListPreference;

    iput-object p4, p0, Lru/cn/tv/mobile/settings/MainPreferenceFragment$5;->val$qualityLevel:Landroid/support/v7/preference/ListPreference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/support/v7/preference/Preference;Ljava/lang/Object;)Z
    .locals 4
    .param p1, "preference"    # Landroid/support/v7/preference/Preference;
    .param p2, "newValue"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 124
    iget-object v2, p0, Lru/cn/tv/mobile/settings/MainPreferenceFragment$5;->val$advancedPlayback:Landroid/support/v7/preference/ListPreference;

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/support/v7/preference/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 125
    .local v0, "state":Z
    :goto_0
    iget-object v2, p0, Lru/cn/tv/mobile/settings/MainPreferenceFragment$5;->this$0:Lru/cn/tv/mobile/settings/MainPreferenceFragment;

    invoke-virtual {v2}, Lru/cn/tv/mobile/settings/MainPreferenceFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "advanced_playback"

    invoke-static {v2, v3, v0}, Lru/cn/domain/Preferences;->setBoolean(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 126
    iget-object v2, p0, Lru/cn/tv/mobile/settings/MainPreferenceFragment$5;->val$advancedPlayback:Landroid/support/v7/preference/ListPreference;

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/support/v7/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 127
    iget-object v2, p0, Lru/cn/tv/mobile/settings/MainPreferenceFragment$5;->val$advancedPlayback:Landroid/support/v7/preference/ListPreference;

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/support/v7/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 129
    iget-object v2, p0, Lru/cn/tv/mobile/settings/MainPreferenceFragment$5;->val$cachingLevel:Landroid/support/v7/preference/ListPreference;

    invoke-virtual {v2, v0}, Landroid/support/v7/preference/ListPreference;->setEnabled(Z)V

    .line 130
    iget-object v2, p0, Lru/cn/tv/mobile/settings/MainPreferenceFragment$5;->val$qualityLevel:Landroid/support/v7/preference/ListPreference;

    invoke-virtual {v2, v0}, Landroid/support/v7/preference/ListPreference;->setEnabled(Z)V

    .line 132
    return v1

    .end local v0    # "state":Z
    :cond_0
    move v0, v1

    .line 124
    goto :goto_0
.end method
