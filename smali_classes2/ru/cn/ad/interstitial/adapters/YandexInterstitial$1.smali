.class Lru/cn/ad/interstitial/adapters/YandexInterstitial$1;
.super Ljava/lang/Object;
.source "YandexInterstitial.java"

# interfaces
.implements Lcom/yandex/mobile/ads/InterstitialEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/ad/interstitial/adapters/YandexInterstitial;->onLoad()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/ad/interstitial/adapters/YandexInterstitial;


# direct methods
.method constructor <init>(Lru/cn/ad/interstitial/adapters/YandexInterstitial;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/ad/interstitial/adapters/YandexInterstitial;

    .prologue
    .line 32
    iput-object p1, p0, Lru/cn/ad/interstitial/adapters/YandexInterstitial$1;->this$0:Lru/cn/ad/interstitial/adapters/YandexInterstitial;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAdClosed()V
    .locals 0

    .prologue
    .line 66
    return-void
.end method

.method public onAdLeftApplication()V
    .locals 3

    .prologue
    .line 70
    iget-object v0, p0, Lru/cn/ad/interstitial/adapters/YandexInterstitial$1;->this$0:Lru/cn/ad/interstitial/adapters/YandexInterstitial;

    sget-object v1, Lru/cn/domain/statistics/inetra/AdvEvent;->ADV_EVENT_CLICK:Lru/cn/domain/statistics/inetra/AdvEvent;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lru/cn/ad/interstitial/adapters/YandexInterstitial;->reportEvent(Lru/cn/domain/statistics/inetra/AdvEvent;Ljava/util/Map;)V

    .line 71
    return-void
.end method

.method public onAdOpened()V
    .locals 0

    .prologue
    .line 74
    return-void
.end method

.method public onInterstitialDismissed()V
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lru/cn/ad/interstitial/adapters/YandexInterstitial$1;->this$0:Lru/cn/ad/interstitial/adapters/YandexInterstitial;

    invoke-static {v0}, Lru/cn/ad/interstitial/adapters/YandexInterstitial;->access$000(Lru/cn/ad/interstitial/adapters/YandexInterstitial;)Lru/cn/ad/AdAdapter$Listener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 36
    iget-object v0, p0, Lru/cn/ad/interstitial/adapters/YandexInterstitial$1;->this$0:Lru/cn/ad/interstitial/adapters/YandexInterstitial;

    invoke-static {v0}, Lru/cn/ad/interstitial/adapters/YandexInterstitial;->access$100(Lru/cn/ad/interstitial/adapters/YandexInterstitial;)Lru/cn/ad/AdAdapter$Listener;

    move-result-object v0

    invoke-interface {v0}, Lru/cn/ad/AdAdapter$Listener;->onAdEnded()V

    .line 37
    :cond_0
    return-void
.end method

.method public onInterstitialFailedToLoad(Lcom/yandex/mobile/ads/AdRequestError;)V
    .locals 5
    .param p1, "adRequestError"    # Lcom/yandex/mobile/ads/AdRequestError;

    .prologue
    .line 41
    invoke-virtual {p1}, Lcom/yandex/mobile/ads/AdRequestError;->getCode()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 50
    :goto_0
    iget-object v0, p0, Lru/cn/ad/interstitial/adapters/YandexInterstitial$1;->this$0:Lru/cn/ad/interstitial/adapters/YandexInterstitial;

    invoke-static {v0}, Lru/cn/ad/interstitial/adapters/YandexInterstitial;->access$200(Lru/cn/ad/interstitial/adapters/YandexInterstitial;)Lru/cn/ad/AdAdapter$Listener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 51
    iget-object v0, p0, Lru/cn/ad/interstitial/adapters/YandexInterstitial$1;->this$0:Lru/cn/ad/interstitial/adapters/YandexInterstitial;

    invoke-static {v0}, Lru/cn/ad/interstitial/adapters/YandexInterstitial;->access$300(Lru/cn/ad/interstitial/adapters/YandexInterstitial;)Lru/cn/ad/AdAdapter$Listener;

    move-result-object v0

    invoke-interface {v0}, Lru/cn/ad/AdAdapter$Listener;->onError()V

    .line 52
    :cond_0
    return-void

    .line 44
    :pswitch_0
    iget-object v0, p0, Lru/cn/ad/interstitial/adapters/YandexInterstitial$1;->this$0:Lru/cn/ad/interstitial/adapters/YandexInterstitial;

    sget-object v1, Lru/cn/domain/statistics/inetra/ErrorCode;->ADV_ERROR_LOAD_BANNER:Lru/cn/domain/statistics/inetra/ErrorCode;

    const-string v2, "YandexError"

    invoke-virtual {p1}, Lcom/yandex/mobile/ads/AdRequestError;->getCode()I

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Lru/cn/ad/interstitial/adapters/YandexInterstitial;->reportError(Lru/cn/domain/statistics/inetra/ErrorCode;Ljava/lang/String;ILjava/util/Map;)V

    goto :goto_0

    .line 41
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public onInterstitialLoaded()V
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lru/cn/ad/interstitial/adapters/YandexInterstitial$1;->this$0:Lru/cn/ad/interstitial/adapters/YandexInterstitial;

    invoke-static {v0}, Lru/cn/ad/interstitial/adapters/YandexInterstitial;->access$400(Lru/cn/ad/interstitial/adapters/YandexInterstitial;)Lru/cn/ad/AdAdapter$Listener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Lru/cn/ad/interstitial/adapters/YandexInterstitial$1;->this$0:Lru/cn/ad/interstitial/adapters/YandexInterstitial;

    invoke-static {v0}, Lru/cn/ad/interstitial/adapters/YandexInterstitial;->access$500(Lru/cn/ad/interstitial/adapters/YandexInterstitial;)Lru/cn/ad/AdAdapter$Listener;

    move-result-object v0

    invoke-interface {v0}, Lru/cn/ad/AdAdapter$Listener;->onAdLoaded()V

    .line 58
    :cond_0
    return-void
.end method

.method public onInterstitialShown()V
    .locals 3

    .prologue
    .line 62
    iget-object v0, p0, Lru/cn/ad/interstitial/adapters/YandexInterstitial$1;->this$0:Lru/cn/ad/interstitial/adapters/YandexInterstitial;

    sget-object v1, Lru/cn/domain/statistics/inetra/AdvEvent;->ADV_EVENT_START:Lru/cn/domain/statistics/inetra/AdvEvent;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lru/cn/ad/interstitial/adapters/YandexInterstitial;->reportEvent(Lru/cn/domain/statistics/inetra/AdvEvent;Ljava/util/Map;)V

    .line 63
    return-void
.end method
