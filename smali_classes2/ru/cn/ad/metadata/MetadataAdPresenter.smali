.class public Lru/cn/ad/metadata/MetadataAdPresenter;
.super Ljava/lang/Object;
.source "MetadataAdPresenter.java"


# instance fields
.field private final adContainer:Landroid/view/ViewGroup;

.field public final item:Lru/cn/player/metadata/MetadataItem;

.field private metadataView:Landroid/widget/Button;


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;Lru/cn/player/metadata/MetadataItem;)V
    .locals 0
    .param p1, "adContainer"    # Landroid/view/ViewGroup;
    .param p2, "item"    # Lru/cn/player/metadata/MetadataItem;

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lru/cn/ad/metadata/MetadataAdPresenter;->adContainer:Landroid/view/ViewGroup;

    .line 28
    iput-object p2, p0, Lru/cn/ad/metadata/MetadataAdPresenter;->item:Lru/cn/player/metadata/MetadataItem;

    .line 29
    return-void
.end method

.method private openUri(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "clickUri"    # Ljava/lang/String;

    .prologue
    .line 69
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 70
    .local v0, "browserIntent":Landroid/content/Intent;
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 71
    return-void
.end method


# virtual methods
.method public hide()V
    .locals 2

    .prologue
    .line 58
    iget-object v0, p0, Lru/cn/ad/metadata/MetadataAdPresenter;->metadataView:Landroid/widget/Button;

    if-eqz v0, :cond_0

    .line 59
    iget-object v0, p0, Lru/cn/ad/metadata/MetadataAdPresenter;->adContainer:Landroid/view/ViewGroup;

    iget-object v1, p0, Lru/cn/ad/metadata/MetadataAdPresenter;->metadataView:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 60
    const/4 v0, 0x0

    iput-object v0, p0, Lru/cn/ad/metadata/MetadataAdPresenter;->metadataView:Landroid/widget/Button;

    .line 62
    :cond_0
    return-void
.end method

.method final synthetic lambda$show$0$MetadataAdPresenter(Landroid/content/Context;Ljava/lang/String;Landroid/view/View;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "clickUri"    # Ljava/lang/String;
    .param p3, "view"    # Landroid/view/View;

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Lru/cn/ad/metadata/MetadataAdPresenter;->openUri(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method public presentingView()Landroid/view/View;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lru/cn/ad/metadata/MetadataAdPresenter;->metadataView:Landroid/widget/Button;

    return-object v0
.end method

.method public show()V
    .locals 8

    .prologue
    const/4 v7, -0x2

    .line 32
    iget-object v5, p0, Lru/cn/ad/metadata/MetadataAdPresenter;->adContainer:Landroid/view/ViewGroup;

    invoke-virtual {v5}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 34
    .local v2, "context":Landroid/content/Context;
    iget-object v5, p0, Lru/cn/ad/metadata/MetadataAdPresenter;->metadataView:Landroid/widget/Button;

    if-eqz v5, :cond_0

    .line 35
    iget-object v5, p0, Lru/cn/ad/metadata/MetadataAdPresenter;->adContainer:Landroid/view/ViewGroup;

    iget-object v6, p0, Lru/cn/ad/metadata/MetadataAdPresenter;->metadataView:Landroid/widget/Button;

    invoke-virtual {v5, v6}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 38
    :cond_0
    iget-object v5, p0, Lru/cn/ad/metadata/MetadataAdPresenter;->metadataView:Landroid/widget/Button;

    if-nez v5, :cond_2

    .line 39
    iget-object v5, p0, Lru/cn/ad/metadata/MetadataAdPresenter;->item:Lru/cn/player/metadata/MetadataItem;

    const-string v6, "X-TV-PEERS-BUTTON-TITLE"

    invoke-virtual {v5, v6}, Lru/cn/player/metadata/MetadataItem;->stringValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 40
    .local v0, "adTitle":Ljava/lang/String;
    iget-object v5, p0, Lru/cn/ad/metadata/MetadataAdPresenter;->item:Lru/cn/player/metadata/MetadataItem;

    const-string v6, "X-TV-PEERS-BUTTON-URI"

    invoke-virtual {v5, v6}, Lru/cn/player/metadata/MetadataItem;->stringValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 41
    .local v1, "clickUri":Ljava/lang/String;
    if-nez v0, :cond_1

    if-nez v1, :cond_1

    .line 55
    .end local v0    # "adTitle":Ljava/lang/String;
    .end local v1    # "clickUri":Ljava/lang/String;
    :goto_0
    return-void

    .line 44
    .restart local v0    # "adTitle":Ljava/lang/String;
    .restart local v1    # "clickUri":Ljava/lang/String;
    :cond_1
    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    .line 45
    .local v3, "inflater":Landroid/view/LayoutInflater;
    const v5, 0x7f0c003f

    const/4 v6, 0x0

    invoke-virtual {v3, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    iput-object v5, p0, Lru/cn/ad/metadata/MetadataAdPresenter;->metadataView:Landroid/widget/Button;

    .line 47
    iget-object v5, p0, Lru/cn/ad/metadata/MetadataAdPresenter;->metadataView:Landroid/widget/Button;

    invoke-virtual {v5, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 48
    iget-object v5, p0, Lru/cn/ad/metadata/MetadataAdPresenter;->metadataView:Landroid/widget/Button;

    new-instance v6, Lru/cn/ad/metadata/MetadataAdPresenter$$Lambda$0;

    invoke-direct {v6, p0, v2, v1}, Lru/cn/ad/metadata/MetadataAdPresenter$$Lambda$0;-><init>(Lru/cn/ad/metadata/MetadataAdPresenter;Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 51
    .end local v0    # "adTitle":Ljava/lang/String;
    .end local v1    # "clickUri":Ljava/lang/String;
    .end local v3    # "inflater":Landroid/view/LayoutInflater;
    :cond_2
    new-instance v4, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v4, v7, v7}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 53
    .local v4, "params":Landroid/widget/FrameLayout$LayoutParams;
    const/16 v5, 0x51

    iput v5, v4, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 54
    iget-object v5, p0, Lru/cn/ad/metadata/MetadataAdPresenter;->adContainer:Landroid/view/ViewGroup;

    iget-object v6, p0, Lru/cn/ad/metadata/MetadataAdPresenter;->metadataView:Landroid/widget/Button;

    invoke-virtual {v5, v6, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method
