.class public Lru/cn/api/tv/replies/Telecast;
.super Ljava/lang/Object;
.source "Telecast.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/api/tv/replies/Telecast$Channel;
    }
.end annotation


# instance fields
.field public URL:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "URL"
    .end annotation
.end field

.field public adTags:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "ad_tags"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public channel:Lru/cn/api/tv/replies/Telecast$Channel;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "channel"
    .end annotation
.end field

.field public date:Lru/cn/api/tv/replies/DateTime;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "date"
    .end annotation
.end field

.field public description:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "description"
    .end annotation
.end field

.field public duration:J
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "duration"
    .end annotation
.end field

.field public id:J
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "id"
    .end annotation
.end field

.field private parentId:J
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "parent_id"
    .end annotation
.end field

.field private telecastImages:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "telecastImages"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lru/cn/api/tv/replies/TelecastImage;",
            ">;"
        }
    .end annotation
.end field

.field public title:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "title"
    .end annotation
.end field

.field public viewsCount:I
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "views_count"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static fromJson(Ljava/lang/String;)Lru/cn/api/tv/replies/Telecast;
    .locals 4
    .param p0, "src"    # Ljava/lang/String;

    .prologue
    .line 75
    new-instance v1, Lcom/google/gson/GsonBuilder;

    invoke-direct {v1}, Lcom/google/gson/GsonBuilder;-><init>()V

    const-class v2, Lru/cn/api/tv/replies/TelecastImage$Profile;

    new-instance v3, Lru/cn/api/tv/retrofit/ImageProfileSerializer;

    invoke-direct {v3}, Lru/cn/api/tv/retrofit/ImageProfileSerializer;-><init>()V

    .line 76
    invoke-virtual {v1, v2, v3}, Lcom/google/gson/GsonBuilder;->registerTypeAdapter(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/GsonBuilder;

    move-result-object v1

    .line 77
    invoke-virtual {v1}, Lcom/google/gson/GsonBuilder;->create()Lcom/google/gson/Gson;

    move-result-object v0

    .line 79
    .local v0, "gson":Lcom/google/gson/Gson;
    const-class v1, Lru/cn/api/tv/replies/Telecast;

    invoke-virtual {v0, p0, v1}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lru/cn/api/tv/replies/Telecast;

    return-object v1
.end method


# virtual methods
.method public getImage(Lru/cn/api/tv/replies/TelecastImage$Profile;)Lru/cn/api/tv/replies/TelecastImage;
    .locals 4
    .param p1, "profile"    # Lru/cn/api/tv/replies/TelecastImage$Profile;

    .prologue
    const/4 v1, 0x0

    .line 59
    iget-object v2, p0, Lru/cn/api/tv/replies/Telecast;->telecastImages:Ljava/util/List;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lru/cn/api/tv/replies/Telecast;->telecastImages:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move-object v0, v1

    .line 67
    :goto_0
    return-object v0

    .line 62
    :cond_1
    iget-object v2, p0, Lru/cn/api/tv/replies/Telecast;->telecastImages:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/api/tv/replies/TelecastImage;

    .line 63
    .local v0, "image":Lru/cn/api/tv/replies/TelecastImage;
    iget-object v3, v0, Lru/cn/api/tv/replies/TelecastImage;->profile:Lru/cn/api/tv/replies/TelecastImage$Profile;

    if-ne v3, p1, :cond_2

    goto :goto_0

    .end local v0    # "image":Lru/cn/api/tv/replies/TelecastImage;
    :cond_3
    move-object v0, v1

    .line 67
    goto :goto_0
.end method

.method public isHighlight()Z
    .locals 4

    .prologue
    .line 71
    iget-wide v0, p0, Lru/cn/api/tv/replies/Telecast;->parentId:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toJson()Ljava/lang/String;
    .locals 4

    .prologue
    .line 83
    new-instance v1, Lcom/google/gson/GsonBuilder;

    invoke-direct {v1}, Lcom/google/gson/GsonBuilder;-><init>()V

    const-class v2, Lru/cn/api/tv/replies/TelecastImage$Profile;

    new-instance v3, Lru/cn/api/tv/retrofit/ImageProfileSerializer;

    invoke-direct {v3}, Lru/cn/api/tv/retrofit/ImageProfileSerializer;-><init>()V

    .line 84
    invoke-virtual {v1, v2, v3}, Lcom/google/gson/GsonBuilder;->registerTypeAdapter(Ljava/lang/reflect/Type;Ljava/lang/Object;)Lcom/google/gson/GsonBuilder;

    move-result-object v1

    .line 85
    invoke-virtual {v1}, Lcom/google/gson/GsonBuilder;->create()Lcom/google/gson/Gson;

    move-result-object v0

    .line 86
    .local v0, "gson":Lcom/google/gson/Gson;
    invoke-virtual {v0, p0}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
