.class public Lcom/google/obf/gx;
.super Ljava/lang/Object;
.source "IMASDK"

# interfaces
.implements Lcom/google/ads/interactivemedia/v3/api/BaseDisplayContainer;


# static fields
.field private static d:I


# instance fields
.field private a:Landroid/view/ViewGroup;

.field private b:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/google/ads/interactivemedia/v3/api/CompanionAdSlot;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/ads/interactivemedia/v3/api/CompanionAdSlot;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const/4 v0, 0x0

    sput v0, Lcom/google/obf/gx;->d:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/obf/gx;->b:Ljava/util/Collection;

    .line 3
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/obf/gx;->c:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public a()Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/ads/interactivemedia/v3/api/CompanionAdSlot;",
            ">;"
        }
    .end annotation

    .prologue
    .line 10
    iget-object v0, p0, Lcom/google/obf/gx;->c:Ljava/util/Map;

    if-nez v0, :cond_2

    .line 11
    new-instance v1, Lcom/google/obf/jm$a;

    invoke-direct {v1}, Lcom/google/obf/jm$a;-><init>()V

    .line 12
    iget-object v0, p0, Lcom/google/obf/gx;->b:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ads/interactivemedia/v3/api/CompanionAdSlot;

    .line 13
    if-eqz v0, :cond_0

    .line 14
    sget v3, Lcom/google/obf/gx;->d:I

    add-int/lit8 v4, v3, 0x1

    sput v4, Lcom/google/obf/gx;->d:I

    const/16 v4, 0x14

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "compSlot_"

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3, v0}, Lcom/google/obf/jm$a;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/obf/jm$a;

    goto :goto_0

    .line 16
    :cond_1
    invoke-virtual {v1}, Lcom/google/obf/jm$a;->a()Lcom/google/obf/jm;

    move-result-object v0

    iput-object v0, p0, Lcom/google/obf/gx;->c:Ljava/util/Map;

    .line 17
    :cond_2
    iget-object v0, p0, Lcom/google/obf/gx;->c:Ljava/util/Map;

    return-object v0
.end method

.method public getAdContainer()Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 4
    iget-object v0, p0, Lcom/google/obf/gx;->a:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public setAdContainer(Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 5
    iput-object p1, p0, Lcom/google/obf/gx;->a:Landroid/view/ViewGroup;

    .line 6
    return-void
.end method
