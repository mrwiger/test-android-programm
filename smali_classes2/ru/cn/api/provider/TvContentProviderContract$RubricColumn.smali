.class public final enum Lru/cn/api/provider/TvContentProviderContract$RubricColumn;
.super Ljava/lang/Enum;
.source "TvContentProviderContract.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/api/provider/TvContentProviderContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "RubricColumn"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lru/cn/api/provider/TvContentProviderContract$RubricColumn;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lru/cn/api/provider/TvContentProviderContract$RubricColumn;

.field public static final enum _id:Lru/cn/api/provider/TvContentProviderContract$RubricColumn;

.field public static final enum description:Lru/cn/api/provider/TvContentProviderContract$RubricColumn;

.field public static final enum has_subitems:Lru/cn/api/provider/TvContentProviderContract$RubricColumn;

.field public static final enum image:Lru/cn/api/provider/TvContentProviderContract$RubricColumn;

.field public static final enum title:Lru/cn/api/provider/TvContentProviderContract$RubricColumn;

.field public static final enum ui_hint:Lru/cn/api/provider/TvContentProviderContract$RubricColumn;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 49
    new-instance v0, Lru/cn/api/provider/TvContentProviderContract$RubricColumn;

    const-string v1, "_id"

    invoke-direct {v0, v1, v3}, Lru/cn/api/provider/TvContentProviderContract$RubricColumn;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/api/provider/TvContentProviderContract$RubricColumn;->_id:Lru/cn/api/provider/TvContentProviderContract$RubricColumn;

    new-instance v0, Lru/cn/api/provider/TvContentProviderContract$RubricColumn;

    const-string v1, "title"

    invoke-direct {v0, v1, v4}, Lru/cn/api/provider/TvContentProviderContract$RubricColumn;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/api/provider/TvContentProviderContract$RubricColumn;->title:Lru/cn/api/provider/TvContentProviderContract$RubricColumn;

    new-instance v0, Lru/cn/api/provider/TvContentProviderContract$RubricColumn;

    const-string v1, "description"

    invoke-direct {v0, v1, v5}, Lru/cn/api/provider/TvContentProviderContract$RubricColumn;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/api/provider/TvContentProviderContract$RubricColumn;->description:Lru/cn/api/provider/TvContentProviderContract$RubricColumn;

    new-instance v0, Lru/cn/api/provider/TvContentProviderContract$RubricColumn;

    const-string v1, "ui_hint"

    invoke-direct {v0, v1, v6}, Lru/cn/api/provider/TvContentProviderContract$RubricColumn;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/api/provider/TvContentProviderContract$RubricColumn;->ui_hint:Lru/cn/api/provider/TvContentProviderContract$RubricColumn;

    new-instance v0, Lru/cn/api/provider/TvContentProviderContract$RubricColumn;

    const-string v1, "has_subitems"

    invoke-direct {v0, v1, v7}, Lru/cn/api/provider/TvContentProviderContract$RubricColumn;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/api/provider/TvContentProviderContract$RubricColumn;->has_subitems:Lru/cn/api/provider/TvContentProviderContract$RubricColumn;

    new-instance v0, Lru/cn/api/provider/TvContentProviderContract$RubricColumn;

    const-string v1, "image"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lru/cn/api/provider/TvContentProviderContract$RubricColumn;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lru/cn/api/provider/TvContentProviderContract$RubricColumn;->image:Lru/cn/api/provider/TvContentProviderContract$RubricColumn;

    .line 48
    const/4 v0, 0x6

    new-array v0, v0, [Lru/cn/api/provider/TvContentProviderContract$RubricColumn;

    sget-object v1, Lru/cn/api/provider/TvContentProviderContract$RubricColumn;->_id:Lru/cn/api/provider/TvContentProviderContract$RubricColumn;

    aput-object v1, v0, v3

    sget-object v1, Lru/cn/api/provider/TvContentProviderContract$RubricColumn;->title:Lru/cn/api/provider/TvContentProviderContract$RubricColumn;

    aput-object v1, v0, v4

    sget-object v1, Lru/cn/api/provider/TvContentProviderContract$RubricColumn;->description:Lru/cn/api/provider/TvContentProviderContract$RubricColumn;

    aput-object v1, v0, v5

    sget-object v1, Lru/cn/api/provider/TvContentProviderContract$RubricColumn;->ui_hint:Lru/cn/api/provider/TvContentProviderContract$RubricColumn;

    aput-object v1, v0, v6

    sget-object v1, Lru/cn/api/provider/TvContentProviderContract$RubricColumn;->has_subitems:Lru/cn/api/provider/TvContentProviderContract$RubricColumn;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lru/cn/api/provider/TvContentProviderContract$RubricColumn;->image:Lru/cn/api/provider/TvContentProviderContract$RubricColumn;

    aput-object v2, v0, v1

    sput-object v0, Lru/cn/api/provider/TvContentProviderContract$RubricColumn;->$VALUES:[Lru/cn/api/provider/TvContentProviderContract$RubricColumn;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lru/cn/api/provider/TvContentProviderContract$RubricColumn;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 48
    const-class v0, Lru/cn/api/provider/TvContentProviderContract$RubricColumn;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lru/cn/api/provider/TvContentProviderContract$RubricColumn;

    return-object v0
.end method

.method public static values()[Lru/cn/api/provider/TvContentProviderContract$RubricColumn;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lru/cn/api/provider/TvContentProviderContract$RubricColumn;->$VALUES:[Lru/cn/api/provider/TvContentProviderContract$RubricColumn;

    invoke-virtual {v0}, [Lru/cn/api/provider/TvContentProviderContract$RubricColumn;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lru/cn/api/provider/TvContentProviderContract$RubricColumn;

    return-object v0
.end method
