.class Lru/cn/draggableview/DraggableView$1;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "DraggableView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/draggableview/DraggableView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private currentPointerId:I

.field final synthetic this$0:Lru/cn/draggableview/DraggableView;


# direct methods
.method constructor <init>(Lru/cn/draggableview/DraggableView;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/draggableview/DraggableView;

    .prologue
    .line 125
    iput-object p1, p0, Lru/cn/draggableview/DraggableView$1;->this$0:Lru/cn/draggableview/DraggableView;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 149
    iget-object v0, p0, Lru/cn/draggableview/DraggableView$1;->this$0:Lru/cn/draggableview/DraggableView;

    invoke-static {v0}, Lru/cn/draggableview/DraggableView;->access$200(Lru/cn/draggableview/DraggableView;)Lru/cn/draggableview/DraggableView$DraggableViewListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 150
    iget-object v0, p0, Lru/cn/draggableview/DraggableView$1;->this$0:Lru/cn/draggableview/DraggableView;

    invoke-static {v0}, Lru/cn/draggableview/DraggableView;->access$200(Lru/cn/draggableview/DraggableView;)Lru/cn/draggableview/DraggableView$DraggableViewListener;

    move-result-object v0

    invoke-interface {v0}, Lru/cn/draggableview/DraggableView$DraggableViewListener;->dragViewDoubleClicked()Z

    move-result v0

    .line 153
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x1

    .line 131
    iget-object v0, p0, Lru/cn/draggableview/DraggableView$1;->this$0:Lru/cn/draggableview/DraggableView;

    sget-object v1, Lru/cn/draggableview/DraggableView$DragOrientation;->ORIENTATION_UNSPEC:Lru/cn/draggableview/DraggableView$DragOrientation;

    invoke-static {v0, v1}, Lru/cn/draggableview/DraggableView;->access$002(Lru/cn/draggableview/DraggableView;Lru/cn/draggableview/DraggableView$DragOrientation;)Lru/cn/draggableview/DraggableView$DragOrientation;

    .line 132
    iget-object v0, p0, Lru/cn/draggableview/DraggableView$1;->this$0:Lru/cn/draggableview/DraggableView;

    invoke-static {v0}, Lru/cn/draggableview/DraggableView;->access$100(Lru/cn/draggableview/DraggableView;)Landroid/widget/Scroller;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_0

    .line 133
    iget-object v0, p0, Lru/cn/draggableview/DraggableView$1;->this$0:Lru/cn/draggableview/DraggableView;

    invoke-static {v0}, Lru/cn/draggableview/DraggableView;->access$100(Lru/cn/draggableview/DraggableView;)Landroid/widget/Scroller;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 135
    :cond_0
    return v2
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 6
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    const v5, 0x44a28000    # 1300.0f

    const v4, -0x3b5d8000    # -1300.0f

    const/4 v3, 0x1

    .line 158
    const/4 v0, 0x0

    .line 160
    .local v0, "handled":Z
    iget-object v1, p0, Lru/cn/draggableview/DraggableView$1;->this$0:Lru/cn/draggableview/DraggableView;

    invoke-virtual {v1}, Lru/cn/draggableview/DraggableView;->isMinimized()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 161
    iget-object v1, p0, Lru/cn/draggableview/DraggableView$1;->this$0:Lru/cn/draggableview/DraggableView;

    invoke-static {v1}, Lru/cn/draggableview/DraggableView;->access$000(Lru/cn/draggableview/DraggableView;)Lru/cn/draggableview/DraggableView$DragOrientation;

    move-result-object v1

    sget-object v2, Lru/cn/draggableview/DraggableView$DragOrientation;->ORIENTATION_HORIZONTAL:Lru/cn/draggableview/DraggableView$DragOrientation;

    if-ne v1, v2, :cond_1

    cmpg-float v1, p3, v4

    if-gtz v1, :cond_1

    .line 163
    iget-object v1, p0, Lru/cn/draggableview/DraggableView$1;->this$0:Lru/cn/draggableview/DraggableView;

    invoke-virtual {v1, v3}, Lru/cn/draggableview/DraggableView;->close(Z)V

    .line 164
    const/4 v0, 0x1

    .line 184
    :cond_0
    :goto_0
    return v0

    .line 166
    :cond_1
    iget-object v1, p0, Lru/cn/draggableview/DraggableView$1;->this$0:Lru/cn/draggableview/DraggableView;

    invoke-static {v1}, Lru/cn/draggableview/DraggableView;->access$000(Lru/cn/draggableview/DraggableView;)Lru/cn/draggableview/DraggableView$DragOrientation;

    move-result-object v1

    sget-object v2, Lru/cn/draggableview/DraggableView$DragOrientation;->ORIENTATION_HORIZONTAL:Lru/cn/draggableview/DraggableView$DragOrientation;

    if-ne v1, v2, :cond_0

    cmpl-float v1, p3, v5

    if-ltz v1, :cond_0

    .line 168
    iget-object v1, p0, Lru/cn/draggableview/DraggableView$1;->this$0:Lru/cn/draggableview/DraggableView;

    invoke-virtual {v1, v3}, Lru/cn/draggableview/DraggableView;->minimize(Z)V

    .line 169
    const/4 v0, 0x1

    goto :goto_0

    .line 172
    :cond_2
    iget-object v1, p0, Lru/cn/draggableview/DraggableView$1;->this$0:Lru/cn/draggableview/DraggableView;

    invoke-static {v1}, Lru/cn/draggableview/DraggableView;->access$000(Lru/cn/draggableview/DraggableView;)Lru/cn/draggableview/DraggableView$DragOrientation;

    move-result-object v1

    sget-object v2, Lru/cn/draggableview/DraggableView$DragOrientation;->ORIENTATION_VERTICAL:Lru/cn/draggableview/DraggableView$DragOrientation;

    if-ne v1, v2, :cond_3

    cmpg-float v1, p4, v4

    if-gtz v1, :cond_3

    .line 174
    iget-object v1, p0, Lru/cn/draggableview/DraggableView$1;->this$0:Lru/cn/draggableview/DraggableView;

    invoke-virtual {v1, v3}, Lru/cn/draggableview/DraggableView;->maximize(Z)V

    .line 175
    const/4 v0, 0x1

    goto :goto_0

    .line 177
    :cond_3
    iget-object v1, p0, Lru/cn/draggableview/DraggableView$1;->this$0:Lru/cn/draggableview/DraggableView;

    invoke-static {v1}, Lru/cn/draggableview/DraggableView;->access$000(Lru/cn/draggableview/DraggableView;)Lru/cn/draggableview/DraggableView$DragOrientation;

    move-result-object v1

    sget-object v2, Lru/cn/draggableview/DraggableView$DragOrientation;->ORIENTATION_VERTICAL:Lru/cn/draggableview/DraggableView$DragOrientation;

    if-ne v1, v2, :cond_0

    cmpl-float v1, p4, v5

    if-ltz v1, :cond_0

    .line 179
    iget-object v1, p0, Lru/cn/draggableview/DraggableView$1;->this$0:Lru/cn/draggableview/DraggableView;

    invoke-virtual {v1, v3}, Lru/cn/draggableview/DraggableView;->minimize(Z)V

    .line 180
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 4
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "distanceX"    # F
    .param p4, "distanceY"    # F

    .prologue
    const/4 v0, 0x0

    .line 190
    iget-object v1, p0, Lru/cn/draggableview/DraggableView$1;->this$0:Lru/cn/draggableview/DraggableView;

    invoke-virtual {v1}, Lru/cn/draggableview/DraggableView;->isMaximized()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lru/cn/draggableview/DraggableView$1;->this$0:Lru/cn/draggableview/DraggableView;

    invoke-static {v1}, Lru/cn/draggableview/DraggableView;->access$300(Lru/cn/draggableview/DraggableView;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 221
    :cond_0
    :goto_0
    return v0

    .line 194
    :cond_1
    iget-object v1, p0, Lru/cn/draggableview/DraggableView$1;->this$0:Lru/cn/draggableview/DraggableView;

    invoke-static {v1}, Lru/cn/draggableview/DraggableView;->access$000(Lru/cn/draggableview/DraggableView;)Lru/cn/draggableview/DraggableView$DragOrientation;

    move-result-object v1

    sget-object v2, Lru/cn/draggableview/DraggableView$DragOrientation;->ORIENTATION_UNSPEC:Lru/cn/draggableview/DraggableView$DragOrientation;

    if-ne v1, v2, :cond_2

    .line 195
    invoke-virtual {p2, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v1

    iput v1, p0, Lru/cn/draggableview/DraggableView$1;->currentPointerId:I

    .line 197
    iget-object v1, p0, Lru/cn/draggableview/DraggableView$1;->this$0:Lru/cn/draggableview/DraggableView;

    invoke-virtual {v1}, Lru/cn/draggableview/DraggableView;->isMinimized()Z

    move-result v1

    if-nez v1, :cond_5

    .line 198
    iget-object v1, p0, Lru/cn/draggableview/DraggableView$1;->this$0:Lru/cn/draggableview/DraggableView;

    sget-object v2, Lru/cn/draggableview/DraggableView$DragOrientation;->ORIENTATION_VERTICAL:Lru/cn/draggableview/DraggableView$DragOrientation;

    invoke-static {v1, v2}, Lru/cn/draggableview/DraggableView;->access$002(Lru/cn/draggableview/DraggableView;Lru/cn/draggableview/DraggableView$DragOrientation;)Lru/cn/draggableview/DraggableView$DragOrientation;

    .line 206
    :goto_1
    iget-object v1, p0, Lru/cn/draggableview/DraggableView$1;->this$0:Lru/cn/draggableview/DraggableView;

    invoke-static {v1}, Lru/cn/draggableview/DraggableView;->access$400(Lru/cn/draggableview/DraggableView;)Lru/cn/draggableview/Transformer;

    move-result-object v1

    iget-object v2, p0, Lru/cn/draggableview/DraggableView$1;->this$0:Lru/cn/draggableview/DraggableView;

    invoke-static {v2}, Lru/cn/draggableview/DraggableView;->access$000(Lru/cn/draggableview/DraggableView;)Lru/cn/draggableview/DraggableView$DragOrientation;

    move-result-object v2

    invoke-virtual {v1, v2}, Lru/cn/draggableview/Transformer;->setDragOrientation(Lru/cn/draggableview/DraggableView$DragOrientation;)V

    .line 207
    iget-object v1, p0, Lru/cn/draggableview/DraggableView$1;->this$0:Lru/cn/draggableview/DraggableView;

    invoke-static {v1}, Lru/cn/draggableview/DraggableView;->access$400(Lru/cn/draggableview/DraggableView;)Lru/cn/draggableview/Transformer;

    move-result-object v1

    invoke-virtual {v1, p1}, Lru/cn/draggableview/Transformer;->initDragVector(Landroid/view/MotionEvent;)V

    .line 210
    :cond_2
    iget-object v1, p0, Lru/cn/draggableview/DraggableView$1;->this$0:Lru/cn/draggableview/DraggableView;

    invoke-static {v1}, Lru/cn/draggableview/DraggableView;->access$000(Lru/cn/draggableview/DraggableView;)Lru/cn/draggableview/DraggableView$DragOrientation;

    move-result-object v1

    sget-object v2, Lru/cn/draggableview/DraggableView$DragOrientation;->ORIENTATION_VERTICAL:Lru/cn/draggableview/DraggableView$DragOrientation;

    if-ne v1, v2, :cond_3

    invoke-static {p4}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const/high16 v2, 0x41700000    # 15.0f

    cmpg-float v1, v1, v2

    if-ltz v1, :cond_0

    .line 213
    :cond_3
    iget v1, p0, Lru/cn/draggableview/DraggableView$1;->currentPointerId:I

    invoke-virtual {p2, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v2

    if-eq v1, v2, :cond_4

    .line 214
    invoke-virtual {p2, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    iput v0, p0, Lru/cn/draggableview/DraggableView$1;->currentPointerId:I

    .line 215
    iget-object v0, p0, Lru/cn/draggableview/DraggableView$1;->this$0:Lru/cn/draggableview/DraggableView;

    invoke-static {v0}, Lru/cn/draggableview/DraggableView;->access$400(Lru/cn/draggableview/DraggableView;)Lru/cn/draggableview/Transformer;

    move-result-object v0

    invoke-virtual {v0, p2}, Lru/cn/draggableview/Transformer;->initDragVector(Landroid/view/MotionEvent;)V

    .line 217
    :cond_4
    iget-object v0, p0, Lru/cn/draggableview/DraggableView$1;->this$0:Lru/cn/draggableview/DraggableView;

    invoke-static {v0}, Lru/cn/draggableview/DraggableView;->access$400(Lru/cn/draggableview/DraggableView;)Lru/cn/draggableview/Transformer;

    move-result-object v0

    invoke-virtual {v0, p2}, Lru/cn/draggableview/Transformer;->drag(Landroid/view/MotionEvent;)V

    .line 218
    iget-object v0, p0, Lru/cn/draggableview/DraggableView$1;->this$0:Lru/cn/draggableview/DraggableView;

    invoke-static {v0}, Lru/cn/draggableview/DraggableView;->access$400(Lru/cn/draggableview/DraggableView;)Lru/cn/draggableview/Transformer;

    move-result-object v0

    invoke-virtual {v0}, Lru/cn/draggableview/Transformer;->updateViews()V

    .line 219
    iget-object v0, p0, Lru/cn/draggableview/DraggableView$1;->this$0:Lru/cn/draggableview/DraggableView;

    invoke-static {v0}, Lru/cn/draggableview/DraggableView;->access$500(Lru/cn/draggableview/DraggableView;)V

    .line 221
    const/4 v0, 0x1

    goto :goto_0

    .line 199
    :cond_5
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    cmpg-float v1, v1, v2

    if-gez v1, :cond_6

    .line 200
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    .line 201
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    sub-float/2addr v2, v3

    .line 200
    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const/high16 v3, 0x40800000    # 4.0f

    div-float/2addr v2, v3

    cmpl-float v1, v1, v2

    if-lez v1, :cond_6

    .line 202
    iget-object v1, p0, Lru/cn/draggableview/DraggableView$1;->this$0:Lru/cn/draggableview/DraggableView;

    sget-object v2, Lru/cn/draggableview/DraggableView$DragOrientation;->ORIENTATION_VERTICAL:Lru/cn/draggableview/DraggableView$DragOrientation;

    invoke-static {v1, v2}, Lru/cn/draggableview/DraggableView;->access$002(Lru/cn/draggableview/DraggableView;Lru/cn/draggableview/DraggableView$DragOrientation;)Lru/cn/draggableview/DraggableView$DragOrientation;

    goto/16 :goto_1

    .line 204
    :cond_6
    iget-object v1, p0, Lru/cn/draggableview/DraggableView$1;->this$0:Lru/cn/draggableview/DraggableView;

    sget-object v2, Lru/cn/draggableview/DraggableView$DragOrientation;->ORIENTATION_HORIZONTAL:Lru/cn/draggableview/DraggableView$DragOrientation;

    invoke-static {v1, v2}, Lru/cn/draggableview/DraggableView;->access$002(Lru/cn/draggableview/DraggableView;Lru/cn/draggableview/DraggableView$DragOrientation;)Lru/cn/draggableview/DraggableView$DragOrientation;

    goto/16 :goto_1
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 140
    iget-object v0, p0, Lru/cn/draggableview/DraggableView$1;->this$0:Lru/cn/draggableview/DraggableView;

    invoke-static {v0}, Lru/cn/draggableview/DraggableView;->access$200(Lru/cn/draggableview/DraggableView;)Lru/cn/draggableview/DraggableView$DraggableViewListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lru/cn/draggableview/DraggableView$1;->this$0:Lru/cn/draggableview/DraggableView;

    invoke-static {v0}, Lru/cn/draggableview/DraggableView;->access$200(Lru/cn/draggableview/DraggableView;)Lru/cn/draggableview/DraggableView$DraggableViewListener;

    move-result-object v0

    invoke-interface {v0}, Lru/cn/draggableview/DraggableView$DraggableViewListener;->dragViewClicked()Z

    move-result v0

    .line 144
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
