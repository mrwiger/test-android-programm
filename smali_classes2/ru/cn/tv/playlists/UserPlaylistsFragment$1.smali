.class Lru/cn/tv/playlists/UserPlaylistsFragment$1;
.super Ljava/lang/Object;
.source "UserPlaylistsFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/tv/playlists/UserPlaylistsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lru/cn/tv/playlists/UserPlaylistsFragment;


# direct methods
.method constructor <init>(Lru/cn/tv/playlists/UserPlaylistsFragment;)V
    .locals 0
    .param p1, "this$0"    # Lru/cn/tv/playlists/UserPlaylistsFragment;

    .prologue
    .line 33
    iput-object p1, p0, Lru/cn/tv/playlists/UserPlaylistsFragment$1;->this$0:Lru/cn/tv/playlists/UserPlaylistsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 38
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v3, p0, Lru/cn/tv/playlists/UserPlaylistsFragment$1;->this$0:Lru/cn/tv/playlists/UserPlaylistsFragment;

    invoke-static {v3}, Lru/cn/tv/playlists/UserPlaylistsFragment;->access$000(Lru/cn/tv/playlists/UserPlaylistsFragment;)Lru/cn/tv/playlists/UserPlaylistsAdapter;

    move-result-object v3

    invoke-virtual {v3, p3}, Lru/cn/tv/playlists/UserPlaylistsAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 39
    .local v0, "cursor":Landroid/database/Cursor;
    const-string v3, "title"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 40
    .local v2, "title":Ljava/lang/String;
    const-string v3, "location"

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 42
    .local v1, "location":Ljava/lang/String;
    invoke-static {p4, p5, v2, v1}, Lru/cn/tv/playlists/UserPlaylistActionDialog;->newInstance(JLjava/lang/String;Ljava/lang/String;)Lru/cn/tv/playlists/UserPlaylistActionDialog;

    move-result-object v3

    iget-object v4, p0, Lru/cn/tv/playlists/UserPlaylistsFragment$1;->this$0:Lru/cn/tv/playlists/UserPlaylistsFragment;

    invoke-virtual {v4}, Lru/cn/tv/playlists/UserPlaylistsFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    const-string v5, "playlistAction"

    invoke-virtual {v3, v4, v5}, Lru/cn/tv/playlists/UserPlaylistActionDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 43
    return-void
.end method
