.class Lcom/yandex/metrica/impl/au$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/yandex/metrica/impl/ob/gk;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/yandex/metrica/impl/au;->a(Lcom/yandex/metrica/c$c;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic a:Lcom/yandex/metrica/c$c;

.field final synthetic b:Lcom/yandex/metrica/impl/au;


# direct methods
.method constructor <init>(Lcom/yandex/metrica/impl/au;Lcom/yandex/metrica/c$c;)V
    .locals 0

    .prologue
    .line 111
    iput-object p1, p0, Lcom/yandex/metrica/impl/au$1;->b:Lcom/yandex/metrica/impl/au;

    iput-object p2, p0, Lcom/yandex/metrica/impl/au$1;->a:Lcom/yandex/metrica/c$c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/yandex/metrica/impl/ob/gj;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 113
    iget-object v3, p0, Lcom/yandex/metrica/impl/au$1;->a:Lcom/yandex/metrica/c$c;

    .line 1132
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ob/gj;->c()Ljava/util/List;

    move-result-object v4

    .line 1133
    invoke-static {v4}, Lcom/yandex/metrica/impl/bl;->a(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1134
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, v3, Lcom/yandex/metrica/c$c;->f:[Ljava/lang/String;

    move v1, v2

    .line 1136
    :goto_0
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1137
    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1138
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1139
    iget-object v5, v3, Lcom/yandex/metrica/c$c;->f:[Ljava/lang/String;

    aput-object v0, v5, v1

    .line 1140
    iget-object v0, p0, Lcom/yandex/metrica/impl/au$1;->b:Lcom/yandex/metrica/impl/au;

    iget v5, v0, Lcom/yandex/metrica/impl/au;->r:I

    iget-object v6, v3, Lcom/yandex/metrica/c$c;->f:[Ljava/lang/String;

    aget-object v6, v6, v1

    invoke-static {v6}, Lcom/yandex/metrica/impl/ob/b;->b(Ljava/lang/String;)I

    move-result v6

    add-int/2addr v5, v6

    iput v5, v0, Lcom/yandex/metrica/impl/au;->r:I

    .line 1141
    iget-object v0, p0, Lcom/yandex/metrica/impl/au$1;->b:Lcom/yandex/metrica/impl/au;

    iget v5, v0, Lcom/yandex/metrica/impl/au;->r:I

    const/16 v6, 0x9

    invoke-static {v6}, Lcom/yandex/metrica/impl/ob/b;->g(I)I

    move-result v6

    add-int/2addr v5, v6

    iput v5, v0, Lcom/yandex/metrica/impl/au;->r:I

    .line 1136
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 114
    :cond_1
    iget-object v1, p0, Lcom/yandex/metrica/impl/au$1;->a:Lcom/yandex/metrica/c$c;

    .line 2118
    invoke-virtual {p1}, Lcom/yandex/metrica/impl/ob/gj;->a()Ljava/util/List;

    move-result-object v3

    .line 2119
    invoke-static {v3}, Lcom/yandex/metrica/impl/bl;->a(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2120
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/yandex/metrica/c$c$e;

    iput-object v0, v1, Lcom/yandex/metrica/c$c;->g:[Lcom/yandex/metrica/c$c$e;

    .line 2122
    :goto_1
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 2123
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/yandex/metrica/impl/ob/gh;

    .line 2124
    iget-object v4, v1, Lcom/yandex/metrica/c$c;->g:[Lcom/yandex/metrica/c$c$e;

    invoke-static {v0}, Lcom/yandex/metrica/impl/aq;->a(Lcom/yandex/metrica/impl/ob/gh;)Lcom/yandex/metrica/c$c$e;

    move-result-object v0

    aput-object v0, v4, v2

    .line 2125
    iget-object v0, p0, Lcom/yandex/metrica/impl/au$1;->b:Lcom/yandex/metrica/impl/au;

    iget v4, v0, Lcom/yandex/metrica/impl/au;->r:I

    iget-object v5, v1, Lcom/yandex/metrica/c$c;->g:[Lcom/yandex/metrica/c$c$e;

    aget-object v5, v5, v2

    invoke-static {v5}, Lcom/yandex/metrica/impl/ob/b;->b(Lcom/yandex/metrica/impl/ob/d;)I

    move-result v5

    add-int/2addr v4, v5

    iput v4, v0, Lcom/yandex/metrica/impl/au;->r:I

    .line 2126
    iget-object v0, p0, Lcom/yandex/metrica/impl/au$1;->b:Lcom/yandex/metrica/impl/au;

    iget v4, v0, Lcom/yandex/metrica/impl/au;->r:I

    const/16 v5, 0xa

    invoke-static {v5}, Lcom/yandex/metrica/impl/ob/b;->g(I)I

    move-result v5

    add-int/2addr v4, v5

    iput v4, v0, Lcom/yandex/metrica/impl/au;->r:I

    .line 2122
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 115
    :cond_2
    return-void
.end method
