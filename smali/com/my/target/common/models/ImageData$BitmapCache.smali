.class Lcom/my/target/common/models/ImageData$BitmapCache;
.super Landroid/util/LruCache;
.source "ImageData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/my/target/common/models/ImageData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "BitmapCache"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/util/LruCache",
        "<",
        "Lcom/my/target/common/models/ImageData;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1, "maxSize"    # I

    .prologue
    .line 152
    invoke-direct {p0, p1}, Landroid/util/LruCache;-><init>(I)V

    .line 153
    return-void
.end method


# virtual methods
.method protected sizeOf(Lcom/my/target/common/models/ImageData;Landroid/graphics/Bitmap;)I
    .locals 2
    .param p1, "key"    # Lcom/my/target/common/models/ImageData;
    .param p2, "value"    # Landroid/graphics/Bitmap;

    .prologue
    .line 158
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    .line 160
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getAllocationByteCount()I

    move-result v0

    .line 162
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getByteCount()I

    move-result v0

    goto :goto_0
.end method

.method protected bridge synthetic sizeOf(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 148
    check-cast p1, Lcom/my/target/common/models/ImageData;

    check-cast p2, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1, p2}, Lcom/my/target/common/models/ImageData$BitmapCache;->sizeOf(Lcom/my/target/common/models/ImageData;Landroid/graphics/Bitmap;)I

    move-result v0

    return v0
.end method
