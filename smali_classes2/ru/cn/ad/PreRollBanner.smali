.class public Lru/cn/ad/PreRollBanner;
.super Ljava/lang/Object;
.source "PreRollBanner.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/ad/PreRollBanner$Listener;
    }
.end annotation


# instance fields
.field private adapter:Lru/cn/ad/AdAdapter;

.field private listener:Lru/cn/ad/PreRollBanner$Listener;

.field private nativePresenter:Lru/cn/ad/natives/adapters/NativeAdAdapter$Presenter;

.field private final placeId:Ljava/lang/String;

.field private final placeLoader:Lru/cn/ad/PlaceLoader;

.field private settings:Lru/cn/ad/video/RenderingSettings;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lru/cn/ad/AdAdapter$Factory;Ljava/util/List;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "placeId"    # Ljava/lang/String;
    .param p3, "factory"    # Lru/cn/ad/AdAdapter$Factory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Lru/cn/ad/AdAdapter$Factory;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 33
    .local p4, "metaTags":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p2, p0, Lru/cn/ad/PreRollBanner;->placeId:Ljava/lang/String;

    .line 36
    new-instance v0, Lru/cn/ad/TagsAwareFactory;

    invoke-direct {v0, p3}, Lru/cn/ad/TagsAwareFactory;-><init>(Lru/cn/ad/AdAdapter$Factory;)V

    .line 37
    .local v0, "loaderFactory":Lru/cn/ad/TagsAwareFactory;
    invoke-virtual {v0, p4}, Lru/cn/ad/TagsAwareFactory;->setTags(Ljava/util/List;)V

    .line 39
    new-instance v1, Lru/cn/ad/PlaceLoader;

    invoke-direct {v1, p1, p2, v0}, Lru/cn/ad/PlaceLoader;-><init>(Landroid/content/Context;Ljava/lang/String;Lru/cn/ad/AdAdapter$Factory;)V

    iput-object v1, p0, Lru/cn/ad/PreRollBanner;->placeLoader:Lru/cn/ad/PlaceLoader;

    .line 40
    return-void
.end method

.method static synthetic access$002(Lru/cn/ad/PreRollBanner;Lru/cn/ad/AdAdapter;)Lru/cn/ad/AdAdapter;
    .locals 0
    .param p0, "x0"    # Lru/cn/ad/PreRollBanner;
    .param p1, "x1"    # Lru/cn/ad/AdAdapter;

    .prologue
    .line 15
    iput-object p1, p0, Lru/cn/ad/PreRollBanner;->adapter:Lru/cn/ad/AdAdapter;

    return-object p1
.end method

.method static synthetic access$100(Lru/cn/ad/PreRollBanner;)Lru/cn/ad/PreRollBanner$Listener;
    .locals 1
    .param p0, "x0"    # Lru/cn/ad/PreRollBanner;

    .prologue
    .line 15
    iget-object v0, p0, Lru/cn/ad/PreRollBanner;->listener:Lru/cn/ad/PreRollBanner$Listener;

    return-object v0
.end method


# virtual methods
.method public destroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 154
    iget-object v0, p0, Lru/cn/ad/PreRollBanner;->adapter:Lru/cn/ad/AdAdapter;

    if-eqz v0, :cond_0

    .line 155
    iget-object v0, p0, Lru/cn/ad/PreRollBanner;->adapter:Lru/cn/ad/AdAdapter;

    invoke-virtual {v0, v1}, Lru/cn/ad/AdAdapter;->setListener(Lru/cn/ad/AdAdapter$Listener;)V

    .line 156
    iget-object v0, p0, Lru/cn/ad/PreRollBanner;->adapter:Lru/cn/ad/AdAdapter;

    invoke-virtual {v0}, Lru/cn/ad/AdAdapter;->destroy()V

    .line 157
    iput-object v1, p0, Lru/cn/ad/PreRollBanner;->adapter:Lru/cn/ad/AdAdapter;

    .line 159
    :cond_0
    return-void
.end method

.method public getPlaceId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lru/cn/ad/PreRollBanner;->placeId:Ljava/lang/String;

    return-object v0
.end method

.method public isReady()Z
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lru/cn/ad/PreRollBanner;->adapter:Lru/cn/ad/AdAdapter;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public load()V
    .locals 2

    .prologue
    .line 63
    invoke-virtual {p0}, Lru/cn/ad/PreRollBanner;->isReady()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 64
    iget-object v0, p0, Lru/cn/ad/PreRollBanner;->listener:Lru/cn/ad/PreRollBanner$Listener;

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lru/cn/ad/PreRollBanner;->listener:Lru/cn/ad/PreRollBanner$Listener;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lru/cn/ad/PreRollBanner$Listener;->adLoadingFinished(Z)V

    .line 110
    :cond_0
    :goto_0
    return-void

    .line 69
    :cond_1
    iget-object v0, p0, Lru/cn/ad/PreRollBanner;->placeLoader:Lru/cn/ad/PlaceLoader;

    new-instance v1, Lru/cn/ad/PreRollBanner$1;

    invoke-direct {v1, p0}, Lru/cn/ad/PreRollBanner$1;-><init>(Lru/cn/ad/PreRollBanner;)V

    invoke-virtual {v0, v1}, Lru/cn/ad/PlaceLoader;->setListener(Lru/cn/ad/AdapterLoader$Listener;)V

    .line 86
    iget-object v0, p0, Lru/cn/ad/PreRollBanner;->placeLoader:Lru/cn/ad/PlaceLoader;

    new-instance v1, Lru/cn/ad/PreRollBanner$2;

    invoke-direct {v1, p0}, Lru/cn/ad/PreRollBanner$2;-><init>(Lru/cn/ad/PreRollBanner;)V

    invoke-virtual {v0, v1}, Lru/cn/ad/PlaceLoader;->setLoadStepListener(Lru/cn/ad/AdapterLoader$LoadStepListener;)V

    .line 109
    iget-object v0, p0, Lru/cn/ad/PreRollBanner;->placeLoader:Lru/cn/ad/PlaceLoader;

    invoke-virtual {v0}, Lru/cn/ad/PlaceLoader;->load()V

    goto :goto_0
.end method

.method public setListener(Lru/cn/ad/PreRollBanner$Listener;)V
    .locals 0
    .param p1, "listener"    # Lru/cn/ad/PreRollBanner$Listener;

    .prologue
    .line 51
    iput-object p1, p0, Lru/cn/ad/PreRollBanner;->listener:Lru/cn/ad/PreRollBanner$Listener;

    .line 52
    return-void
.end method

.method public setNativePresenter(Lru/cn/ad/natives/adapters/NativeAdAdapter$Presenter;)V
    .locals 0
    .param p1, "presenter"    # Lru/cn/ad/natives/adapters/NativeAdAdapter$Presenter;

    .prologue
    .line 55
    iput-object p1, p0, Lru/cn/ad/PreRollBanner;->nativePresenter:Lru/cn/ad/natives/adapters/NativeAdAdapter$Presenter;

    .line 56
    return-void
.end method

.method public setRenderingSettings(Lru/cn/ad/video/RenderingSettings;)V
    .locals 0
    .param p1, "settings"    # Lru/cn/ad/video/RenderingSettings;

    .prologue
    .line 59
    iput-object p1, p0, Lru/cn/ad/PreRollBanner;->settings:Lru/cn/ad/video/RenderingSettings;

    .line 60
    return-void
.end method

.method public show()V
    .locals 3

    .prologue
    .line 113
    iget-object v0, p0, Lru/cn/ad/PreRollBanner;->adapter:Lru/cn/ad/AdAdapter;

    if-nez v0, :cond_0

    .line 151
    :goto_0
    return-void

    .line 116
    :cond_0
    iget-object v0, p0, Lru/cn/ad/PreRollBanner;->adapter:Lru/cn/ad/AdAdapter;

    new-instance v1, Lru/cn/ad/PreRollBanner$3;

    invoke-direct {v1, p0}, Lru/cn/ad/PreRollBanner$3;-><init>(Lru/cn/ad/PreRollBanner;)V

    invoke-virtual {v0, v1}, Lru/cn/ad/AdAdapter;->setListener(Lru/cn/ad/AdAdapter$Listener;)V

    .line 142
    iget-object v0, p0, Lru/cn/ad/PreRollBanner;->adapter:Lru/cn/ad/AdAdapter;

    instance-of v0, v0, Lru/cn/ad/video/VideoAdAdapter;

    if-eqz v0, :cond_2

    .line 143
    iget-object v0, p0, Lru/cn/ad/PreRollBanner;->adapter:Lru/cn/ad/AdAdapter;

    check-cast v0, Lru/cn/ad/video/VideoAdAdapter;

    iget-object v1, p0, Lru/cn/ad/PreRollBanner;->settings:Lru/cn/ad/video/RenderingSettings;

    invoke-virtual {v0, v1}, Lru/cn/ad/video/VideoAdAdapter;->setRenderingSettings(Lru/cn/ad/video/RenderingSettings;)V

    .line 149
    :cond_1
    :goto_1
    iget-object v0, p0, Lru/cn/ad/PreRollBanner;->adapter:Lru/cn/ad/AdAdapter;

    new-instance v1, Lru/cn/ad/AdEventReporter;

    iget-object v2, p0, Lru/cn/ad/PreRollBanner;->placeId:Ljava/lang/String;

    invoke-direct {v1, v2}, Lru/cn/ad/AdEventReporter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lru/cn/ad/AdAdapter;->setReporter(Lru/cn/ad/AdEventReporter;)V

    .line 150
    iget-object v0, p0, Lru/cn/ad/PreRollBanner;->adapter:Lru/cn/ad/AdAdapter;

    invoke-virtual {v0}, Lru/cn/ad/AdAdapter;->show()V

    goto :goto_0

    .line 145
    :cond_2
    iget-object v0, p0, Lru/cn/ad/PreRollBanner;->adapter:Lru/cn/ad/AdAdapter;

    instance-of v0, v0, Lru/cn/ad/natives/adapters/NativeAdAdapter;

    if-eqz v0, :cond_1

    .line 146
    iget-object v0, p0, Lru/cn/ad/PreRollBanner;->adapter:Lru/cn/ad/AdAdapter;

    check-cast v0, Lru/cn/ad/natives/adapters/NativeAdAdapter;

    iget-object v1, p0, Lru/cn/ad/PreRollBanner;->nativePresenter:Lru/cn/ad/natives/adapters/NativeAdAdapter$Presenter;

    invoke-virtual {v0, v1}, Lru/cn/ad/natives/adapters/NativeAdAdapter;->setPresenter(Lru/cn/ad/natives/adapters/NativeAdAdapter$Presenter;)V

    goto :goto_1
.end method
