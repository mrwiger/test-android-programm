.class public final Lcom/my/target/core/engines/d;
.super Lcom/my/target/core/engines/c;
.source "InterstitialAdHtmlEngine.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/core/engines/d$a;
    }
.end annotation


# instance fields
.field private final d:Lcom/my/target/core/models/banners/f;

.field private final e:Lcom/my/target/dv;

.field private f:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/my/target/core/presenters/h;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/my/target/ads/InterstitialAd;Lcom/my/target/core/models/banners/f;Lcom/my/target/dv;)V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0, p1}, Lcom/my/target/core/engines/c;-><init>(Lcom/my/target/ads/InterstitialAd;)V

    .line 70
    iput-object p2, p0, Lcom/my/target/core/engines/d;->d:Lcom/my/target/core/models/banners/f;

    .line 71
    iput-object p3, p0, Lcom/my/target/core/engines/d;->e:Lcom/my/target/dv;

    .line 72
    return-void
.end method

.method static a(Lcom/my/target/ads/InterstitialAd;Lcom/my/target/core/models/banners/f;Lcom/my/target/dv;)Lcom/my/target/core/engines/d;
    .locals 1

    .prologue
    .line 44
    new-instance v0, Lcom/my/target/core/engines/d;

    invoke-direct {v0, p0, p1, p2}, Lcom/my/target/core/engines/d;-><init>(Lcom/my/target/ads/InterstitialAd;Lcom/my/target/core/models/banners/f;Lcom/my/target/dv;)V

    return-object v0
.end method

.method private a(Landroid/view/ViewGroup;)V
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 173
    const-string v0, "mraid"

    iget-object v1, p0, Lcom/my/target/core/engines/d;->d:Lcom/my/target/core/models/banners/f;

    invoke-virtual {v1}, Lcom/my/target/core/models/banners/f;->getType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 175
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/core/presenters/e;->c(Landroid/content/Context;)Lcom/my/target/core/presenters/e;

    move-result-object v0

    .line 181
    :goto_0
    iget-object v1, p0, Lcom/my/target/core/engines/d;->e:Lcom/my/target/dv;

    iget-object v2, p0, Lcom/my/target/core/engines/d;->d:Lcom/my/target/core/models/banners/f;

    invoke-interface {v0, v1, v2}, Lcom/my/target/core/presenters/h;->a(Lcom/my/target/dv;Lcom/my/target/core/models/banners/f;)V

    .line 182
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/my/target/core/engines/d;->f:Ljava/lang/ref/WeakReference;

    .line 183
    new-instance v1, Lcom/my/target/core/engines/d$a;

    iget-object v2, p0, Lcom/my/target/core/engines/d;->a:Lcom/my/target/ads/InterstitialAd;

    iget-object v3, p0, Lcom/my/target/core/engines/d;->d:Lcom/my/target/core/models/banners/f;

    invoke-direct {v1, p0, v2, v3}, Lcom/my/target/core/engines/d$a;-><init>(Lcom/my/target/core/engines/d;Lcom/my/target/ads/InterstitialAd;Lcom/my/target/core/models/banners/f;)V

    invoke-interface {v0, v1}, Lcom/my/target/core/presenters/h;->a(Lcom/my/target/core/presenters/h$a;)V

    .line 184
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 185
    invoke-interface {v0}, Lcom/my/target/core/presenters/h;->o()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1, v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 186
    return-void

    .line 179
    :cond_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/my/target/core/presenters/d;->b(Landroid/content/Context;)Lcom/my/target/core/presenters/d;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/my/target/br;Landroid/widget/FrameLayout;)V
    .locals 0
    .param p1, "dialog"    # Lcom/my/target/br;
    .param p2, "rootLayout"    # Landroid/widget/FrameLayout;

    .prologue
    .line 77
    invoke-super {p0, p1, p2}, Lcom/my/target/core/engines/c;->a(Lcom/my/target/br;Landroid/widget/FrameLayout;)V

    .line 78
    invoke-direct {p0, p2}, Lcom/my/target/core/engines/d;->a(Landroid/view/ViewGroup;)V

    .line 79
    return-void
.end method

.method public final aU()V
    .locals 1

    .prologue
    .line 105
    invoke-super {p0}, Lcom/my/target/core/engines/c;->aU()V

    .line 106
    iget-object v0, p0, Lcom/my/target/core/engines/d;->f:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lcom/my/target/core/engines/d;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/core/presenters/h;

    .line 109
    if-eqz v0, :cond_0

    .line 111
    invoke-interface {v0}, Lcom/my/target/core/presenters/h;->destroy()V

    .line 114
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/my/target/core/engines/d;->f:Ljava/lang/ref/WeakReference;

    .line 115
    return-void
.end method

.method public final i(Z)V
    .locals 1
    .param p1, "hasFocus"    # Z

    .prologue
    .line 84
    invoke-super {p0, p1}, Lcom/my/target/core/engines/c;->i(Z)V

    .line 85
    iget-object v0, p0, Lcom/my/target/core/engines/d;->f:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/my/target/core/engines/d;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/core/presenters/h;

    .line 88
    if-eqz v0, :cond_0

    .line 90
    if-eqz p1, :cond_1

    .line 92
    invoke-interface {v0}, Lcom/my/target/core/presenters/h;->resume()V

    .line 100
    :cond_0
    :goto_0
    return-void

    .line 96
    :cond_1
    invoke-interface {v0}, Lcom/my/target/core/presenters/h;->pause()V

    goto :goto_0
.end method

.method public final onActivityCreate(Lcom/my/target/common/MyTargetActivity;Landroid/content/Intent;Landroid/widget/FrameLayout;)V
    .locals 0
    .param p1, "activity"    # Lcom/my/target/common/MyTargetActivity;
    .param p2, "intent"    # Landroid/content/Intent;
    .param p3, "rootLayout"    # Landroid/widget/FrameLayout;

    .prologue
    .line 122
    invoke-super {p0, p1, p2, p3}, Lcom/my/target/core/engines/c;->onActivityCreate(Lcom/my/target/common/MyTargetActivity;Landroid/content/Intent;Landroid/widget/FrameLayout;)V

    .line 123
    invoke-direct {p0, p3}, Lcom/my/target/core/engines/d;->a(Landroid/view/ViewGroup;)V

    .line 125
    return-void
.end method

.method public final onActivityDestroy()V
    .locals 1

    .prologue
    .line 158
    invoke-super {p0}, Lcom/my/target/core/engines/c;->onActivityDestroy()V

    .line 159
    iget-object v0, p0, Lcom/my/target/core/engines/d;->f:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 161
    iget-object v0, p0, Lcom/my/target/core/engines/d;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/core/presenters/h;

    .line 162
    if-eqz v0, :cond_0

    .line 164
    invoke-interface {v0}, Lcom/my/target/core/presenters/h;->destroy()V

    .line 167
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/my/target/core/engines/d;->f:Ljava/lang/ref/WeakReference;

    .line 168
    return-void
.end method

.method public final onActivityPause()V
    .locals 1

    .prologue
    .line 130
    invoke-super {p0}, Lcom/my/target/core/engines/c;->onActivityPause()V

    .line 131
    iget-object v0, p0, Lcom/my/target/core/engines/d;->f:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/my/target/core/engines/d;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/core/presenters/h;

    .line 134
    if-eqz v0, :cond_0

    .line 136
    invoke-interface {v0}, Lcom/my/target/core/presenters/h;->pause()V

    .line 139
    :cond_0
    return-void
.end method

.method public final onActivityResume()V
    .locals 1

    .prologue
    .line 144
    invoke-super {p0}, Lcom/my/target/core/engines/c;->onActivityResume()V

    .line 145
    iget-object v0, p0, Lcom/my/target/core/engines/d;->f:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 147
    iget-object v0, p0, Lcom/my/target/core/engines/d;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/core/presenters/h;

    .line 148
    if-eqz v0, :cond_0

    .line 150
    invoke-interface {v0}, Lcom/my/target/core/presenters/h;->resume()V

    .line 153
    :cond_0
    return-void
.end method
