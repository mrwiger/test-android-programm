.class public Lcom/google/obf/kd;
.super Ljava/lang/Object;
.source "IMASDK"


# static fields
.field public static final a:[Ljava/lang/Object;

.field public static final b:[Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static final c:[Ljava/lang/String;

.field public static final d:[J

.field public static final e:[Ljava/lang/Long;

.field public static final f:[I

.field public static final g:[Ljava/lang/Integer;

.field public static final h:[S

.field public static final i:[Ljava/lang/Short;

.field public static final j:[B

.field public static final k:[Ljava/lang/Byte;

.field public static final l:[D

.field public static final m:[Ljava/lang/Double;

.field public static final n:[F

.field public static final o:[Ljava/lang/Float;

.field public static final p:[Z

.field public static final q:[Ljava/lang/Boolean;

.field public static final r:[C

.field public static final s:[Ljava/lang/Character;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 17
    new-array v0, v1, [Ljava/lang/Object;

    sput-object v0, Lcom/google/obf/kd;->a:[Ljava/lang/Object;

    .line 18
    new-array v0, v1, [Ljava/lang/Class;

    sput-object v0, Lcom/google/obf/kd;->b:[Ljava/lang/Class;

    .line 19
    new-array v0, v1, [Ljava/lang/String;

    sput-object v0, Lcom/google/obf/kd;->c:[Ljava/lang/String;

    .line 20
    new-array v0, v1, [J

    sput-object v0, Lcom/google/obf/kd;->d:[J

    .line 21
    new-array v0, v1, [Ljava/lang/Long;

    sput-object v0, Lcom/google/obf/kd;->e:[Ljava/lang/Long;

    .line 22
    new-array v0, v1, [I

    sput-object v0, Lcom/google/obf/kd;->f:[I

    .line 23
    new-array v0, v1, [Ljava/lang/Integer;

    sput-object v0, Lcom/google/obf/kd;->g:[Ljava/lang/Integer;

    .line 24
    new-array v0, v1, [S

    sput-object v0, Lcom/google/obf/kd;->h:[S

    .line 25
    new-array v0, v1, [Ljava/lang/Short;

    sput-object v0, Lcom/google/obf/kd;->i:[Ljava/lang/Short;

    .line 26
    new-array v0, v1, [B

    sput-object v0, Lcom/google/obf/kd;->j:[B

    .line 27
    new-array v0, v1, [Ljava/lang/Byte;

    sput-object v0, Lcom/google/obf/kd;->k:[Ljava/lang/Byte;

    .line 28
    new-array v0, v1, [D

    sput-object v0, Lcom/google/obf/kd;->l:[D

    .line 29
    new-array v0, v1, [Ljava/lang/Double;

    sput-object v0, Lcom/google/obf/kd;->m:[Ljava/lang/Double;

    .line 30
    new-array v0, v1, [F

    sput-object v0, Lcom/google/obf/kd;->n:[F

    .line 31
    new-array v0, v1, [Ljava/lang/Float;

    sput-object v0, Lcom/google/obf/kd;->o:[Ljava/lang/Float;

    .line 32
    new-array v0, v1, [Z

    sput-object v0, Lcom/google/obf/kd;->p:[Z

    .line 33
    new-array v0, v1, [Ljava/lang/Boolean;

    sput-object v0, Lcom/google/obf/kd;->q:[Ljava/lang/Boolean;

    .line 34
    new-array v0, v1, [C

    sput-object v0, Lcom/google/obf/kd;->r:[C

    .line 35
    new-array v0, v1, [Ljava/lang/Character;

    sput-object v0, Lcom/google/obf/kd;->s:[Ljava/lang/Character;

    return-void
.end method

.method public static a([Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/google/obf/kd;->a([Ljava/lang/Object;Ljava/lang/Object;I)I

    move-result v0

    return v0
.end method

.method public static a([Ljava/lang/Object;Ljava/lang/Object;I)I
    .locals 3

    .prologue
    const/4 v1, -0x1

    .line 2
    if-nez p0, :cond_1

    move v0, v1

    .line 15
    :cond_0
    :goto_0
    return v0

    .line 4
    :cond_1
    if-gez p2, :cond_5

    .line 5
    const/4 v0, 0x0

    .line 6
    :goto_1
    if-nez p1, :cond_3

    .line 7
    :goto_2
    array-length v2, p0

    if-ge v0, v2, :cond_4

    .line 8
    aget-object v2, p0, v0

    if-eqz v2, :cond_0

    .line 10
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 14
    :cond_2
    add-int/lit8 v0, v0, 0x1

    .line 11
    :cond_3
    array-length v2, p0

    if-ge v0, v2, :cond_4

    .line 12
    aget-object v2, p0, v0

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_0

    :cond_4
    move v0, v1

    .line 15
    goto :goto_0

    :cond_5
    move v0, p2

    goto :goto_1
.end method

.method public static b([Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 16
    invoke-static {p0, p1}, Lcom/google/obf/kd;->a([Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
