.class public final Lcom/google/obf/cm;
.super Ljava/lang/Object;
.source "IMASDK"

# interfaces
.implements Lcom/google/obf/aj;
.implements Lcom/google/obf/aq;


# instance fields
.field private a:Lcom/google/obf/al;

.field private b:Lcom/google/obf/ar;

.field private c:Lcom/google/obf/cn;

.field private d:I

.field private e:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/obf/ak;Lcom/google/obf/ao;)I
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 11
    iget-object v0, p0, Lcom/google/obf/cm;->c:Lcom/google/obf/cn;

    if-nez v0, :cond_1

    .line 12
    invoke-static {p1}, Lcom/google/obf/co;->a(Lcom/google/obf/ak;)Lcom/google/obf/cn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/obf/cm;->c:Lcom/google/obf/cn;

    .line 13
    iget-object v0, p0, Lcom/google/obf/cm;->c:Lcom/google/obf/cn;

    if-nez v0, :cond_0

    .line 14
    new-instance v0, Lcom/google/obf/s;

    const-string v1, "Error initializing WavHeader. Did you sniff first?"

    invoke-direct {v0, v1}, Lcom/google/obf/s;-><init>(Ljava/lang/String;)V

    throw v0

    .line 15
    :cond_0
    iget-object v0, p0, Lcom/google/obf/cm;->c:Lcom/google/obf/cn;

    invoke-virtual {v0}, Lcom/google/obf/cn;->b()I

    move-result v0

    iput v0, p0, Lcom/google/obf/cm;->d:I

    .line 16
    :cond_1
    iget-object v0, p0, Lcom/google/obf/cm;->c:Lcom/google/obf/cn;

    invoke-virtual {v0}, Lcom/google/obf/cn;->f()Z

    move-result v0

    if-nez v0, :cond_2

    .line 17
    iget-object v0, p0, Lcom/google/obf/cm;->c:Lcom/google/obf/cn;

    invoke-static {p1, v0}, Lcom/google/obf/co;->a(Lcom/google/obf/ak;Lcom/google/obf/cn;)V

    .line 18
    iget-object v11, p0, Lcom/google/obf/cm;->b:Lcom/google/obf/ar;

    const/4 v0, 0x0

    const-string v1, "audio/raw"

    iget-object v2, p0, Lcom/google/obf/cm;->c:Lcom/google/obf/cn;

    .line 19
    invoke-virtual {v2}, Lcom/google/obf/cn;->c()I

    move-result v2

    const v3, 0x8000

    iget-object v4, p0, Lcom/google/obf/cm;->c:Lcom/google/obf/cn;

    .line 20
    invoke-virtual {v4}, Lcom/google/obf/cn;->a()J

    move-result-wide v4

    iget-object v6, p0, Lcom/google/obf/cm;->c:Lcom/google/obf/cn;

    .line 21
    invoke-virtual {v6}, Lcom/google/obf/cn;->e()I

    move-result v6

    iget-object v7, p0, Lcom/google/obf/cm;->c:Lcom/google/obf/cn;

    .line 22
    invoke-virtual {v7}, Lcom/google/obf/cn;->d()I

    move-result v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/google/obf/cm;->c:Lcom/google/obf/cn;

    .line 23
    invoke-virtual {v10}, Lcom/google/obf/cn;->g()I

    move-result v10

    .line 24
    invoke-static/range {v0 .. v10}, Lcom/google/obf/q;->a(Ljava/lang/String;Ljava/lang/String;IIJIILjava/util/List;Ljava/lang/String;I)Lcom/google/obf/q;

    move-result-object v0

    .line 25
    invoke-interface {v11, v0}, Lcom/google/obf/ar;->a(Lcom/google/obf/q;)V

    .line 26
    iget-object v0, p0, Lcom/google/obf/cm;->a:Lcom/google/obf/al;

    invoke-interface {v0, p0}, Lcom/google/obf/al;->a(Lcom/google/obf/aq;)V

    .line 27
    :cond_2
    iget-object v0, p0, Lcom/google/obf/cm;->b:Lcom/google/obf/ar;

    const v1, 0x8000

    iget v2, p0, Lcom/google/obf/cm;->e:I

    sub-int/2addr v1, v2

    const/4 v2, 0x1

    invoke-interface {v0, p1, v1, v2}, Lcom/google/obf/ar;->a(Lcom/google/obf/ak;IZ)I

    move-result v0

    .line 28
    const/4 v1, -0x1

    if-eq v0, v1, :cond_3

    .line 29
    iget v1, p0, Lcom/google/obf/cm;->e:I

    add-int/2addr v1, v0

    iput v1, p0, Lcom/google/obf/cm;->e:I

    .line 30
    :cond_3
    iget v1, p0, Lcom/google/obf/cm;->e:I

    iget v2, p0, Lcom/google/obf/cm;->d:I

    div-int/2addr v1, v2

    iget v2, p0, Lcom/google/obf/cm;->d:I

    mul-int v5, v1, v2

    .line 31
    if-lez v5, :cond_4

    .line 32
    invoke-interface {p1}, Lcom/google/obf/ak;->c()J

    move-result-wide v2

    iget v1, p0, Lcom/google/obf/cm;->e:I

    int-to-long v6, v1

    sub-long/2addr v2, v6

    .line 33
    iget v1, p0, Lcom/google/obf/cm;->e:I

    sub-int/2addr v1, v5

    iput v1, p0, Lcom/google/obf/cm;->e:I

    .line 34
    iget-object v1, p0, Lcom/google/obf/cm;->b:Lcom/google/obf/ar;

    iget-object v4, p0, Lcom/google/obf/cm;->c:Lcom/google/obf/cn;

    .line 35
    invoke-virtual {v4, v2, v3}, Lcom/google/obf/cn;->b(J)J

    move-result-wide v2

    const/4 v4, 0x1

    iget v6, p0, Lcom/google/obf/cm;->e:I

    const/4 v7, 0x0

    .line 36
    invoke-interface/range {v1 .. v7}, Lcom/google/obf/ar;->a(JIII[B)V

    .line 37
    :cond_4
    const/4 v1, -0x1

    if-ne v0, v1, :cond_5

    .line 38
    const/4 v0, -0x1

    .line 39
    :goto_0
    return v0

    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/google/obf/al;)V
    .locals 1

    .prologue
    .line 3
    iput-object p1, p0, Lcom/google/obf/cm;->a:Lcom/google/obf/al;

    .line 4
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Lcom/google/obf/al;->d(I)Lcom/google/obf/ar;

    move-result-object v0

    iput-object v0, p0, Lcom/google/obf/cm;->b:Lcom/google/obf/ar;

    .line 5
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/obf/cm;->c:Lcom/google/obf/cn;

    .line 6
    invoke-interface {p1}, Lcom/google/obf/al;->f()V

    .line 7
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x1

    return v0
.end method

.method public a(Lcom/google/obf/ak;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 2
    invoke-static {p1}, Lcom/google/obf/co;->a(Lcom/google/obf/ak;)Lcom/google/obf/cn;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(J)J
    .locals 3

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/obf/cm;->c:Lcom/google/obf/cn;

    invoke-virtual {v0, p1, p2}, Lcom/google/obf/cn;->a(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 8
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/obf/cm;->e:I

    .line 9
    return-void
.end method

.method public c()V
    .locals 0

    .prologue
    .line 10
    return-void
.end method
