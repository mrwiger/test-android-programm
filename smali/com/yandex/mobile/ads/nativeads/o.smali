.class public Lcom/yandex/mobile/ads/nativeads/o;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lcom/yandex/mobile/ads/n;

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/yandex/mobile/ads/nativeads/h;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/yandex/mobile/ads/e;

.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/yandex/mobile/ads/nativeads/ab;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/yandex/mobile/ads/nativeads/k;Lcom/yandex/mobile/ads/n;Lcom/yandex/mobile/ads/e;)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p2, p0, Lcom/yandex/mobile/ads/nativeads/o;->b:Lcom/yandex/mobile/ads/n;

    .line 30
    iput-object p3, p0, Lcom/yandex/mobile/ads/nativeads/o;->d:Lcom/yandex/mobile/ads/e;

    .line 31
    invoke-virtual {p1}, Lcom/yandex/mobile/ads/nativeads/k;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/mobile/ads/nativeads/o;->a:Ljava/lang/String;

    .line 32
    invoke-virtual {p1}, Lcom/yandex/mobile/ads/nativeads/k;->b()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/mobile/ads/nativeads/o;->c:Ljava/util/List;

    .line 33
    invoke-virtual {p1}, Lcom/yandex/mobile/ads/nativeads/k;->c()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/yandex/mobile/ads/nativeads/o;->e:Ljava/util/List;

    .line 34
    return-void
.end method


# virtual methods
.method public a()Lcom/yandex/mobile/ads/n;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/o;->b:Lcom/yandex/mobile/ads/n;

    return-object v0
.end method

.method public b()Lcom/yandex/mobile/ads/e;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/o;->d:Lcom/yandex/mobile/ads/e;

    return-object v0
.end method

.method public c()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/yandex/mobile/ads/nativeads/ab;",
            ">;"
        }
    .end annotation

    .prologue
    .line 55
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/o;->e:Ljava/util/List;

    return-object v0
.end method

.method public d()Lcom/yandex/mobile/ads/nativeads/h;
    .locals 2

    .prologue
    .line 60
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/o;->c:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/yandex/mobile/ads/nativeads/h;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 65
    if-ne p0, p1, :cond_1

    .line 78
    .end local p1    # "o":Ljava/lang/Object;
    :cond_0
    :goto_0
    return v0

    .line 66
    .restart local p1    # "o":Ljava/lang/Object;
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    .line 68
    :cond_3
    check-cast p1, Lcom/yandex/mobile/ads/nativeads/o;

    .line 70
    .end local p1    # "o":Ljava/lang/Object;
    iget-object v2, p0, Lcom/yandex/mobile/ads/nativeads/o;->a:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/yandex/mobile/ads/nativeads/o;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/yandex/mobile/ads/nativeads/o;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 71
    goto :goto_0

    .line 70
    :cond_5
    iget-object v2, p1, Lcom/yandex/mobile/ads/nativeads/o;->a:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 72
    :cond_6
    iget-object v2, p0, Lcom/yandex/mobile/ads/nativeads/o;->b:Lcom/yandex/mobile/ads/n;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/yandex/mobile/ads/nativeads/o;->b:Lcom/yandex/mobile/ads/n;

    iget-object v3, p1, Lcom/yandex/mobile/ads/nativeads/o;->b:Lcom/yandex/mobile/ads/n;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 73
    goto :goto_0

    .line 72
    :cond_8
    iget-object v2, p1, Lcom/yandex/mobile/ads/nativeads/o;->b:Lcom/yandex/mobile/ads/n;

    if-nez v2, :cond_7

    .line 74
    :cond_9
    iget-object v2, p0, Lcom/yandex/mobile/ads/nativeads/o;->c:Ljava/util/List;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/yandex/mobile/ads/nativeads/o;->c:Ljava/util/List;

    iget-object v3, p1, Lcom/yandex/mobile/ads/nativeads/o;->c:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 75
    goto :goto_0

    .line 74
    :cond_b
    iget-object v2, p1, Lcom/yandex/mobile/ads/nativeads/o;->c:Ljava/util/List;

    if-nez v2, :cond_a

    .line 76
    :cond_c
    iget-object v2, p0, Lcom/yandex/mobile/ads/nativeads/o;->d:Lcom/yandex/mobile/ads/e;

    if-eqz v2, :cond_e

    iget-object v2, p0, Lcom/yandex/mobile/ads/nativeads/o;->d:Lcom/yandex/mobile/ads/e;

    iget-object v3, p1, Lcom/yandex/mobile/ads/nativeads/o;->d:Lcom/yandex/mobile/ads/e;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    :cond_d
    move v0, v1

    .line 77
    goto :goto_0

    .line 76
    :cond_e
    iget-object v2, p1, Lcom/yandex/mobile/ads/nativeads/o;->d:Lcom/yandex/mobile/ads/e;

    if-nez v2, :cond_d

    .line 78
    :cond_f
    iget-object v2, p0, Lcom/yandex/mobile/ads/nativeads/o;->e:Ljava/util/List;

    if-eqz v2, :cond_10

    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/o;->e:Ljava/util/List;

    iget-object v1, p1, Lcom/yandex/mobile/ads/nativeads/o;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    :cond_10
    iget-object v2, p1, Lcom/yandex/mobile/ads/nativeads/o;->e:Ljava/util/List;

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 84
    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/o;->a:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/o;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 85
    :goto_0
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/o;->b:Lcom/yandex/mobile/ads/n;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/o;->b:Lcom/yandex/mobile/ads/n;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    .line 86
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/o;->c:Ljava/util/List;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/o;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v0

    :goto_2
    add-int/2addr v0, v2

    .line 87
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/o;->d:Lcom/yandex/mobile/ads/e;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/yandex/mobile/ads/nativeads/o;->d:Lcom/yandex/mobile/ads/e;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_3
    add-int/2addr v0, v2

    .line 88
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/yandex/mobile/ads/nativeads/o;->e:Ljava/util/List;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/yandex/mobile/ads/nativeads/o;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 89
    return v0

    :cond_1
    move v0, v1

    .line 84
    goto :goto_0

    :cond_2
    move v0, v1

    .line 85
    goto :goto_1

    :cond_3
    move v0, v1

    .line 86
    goto :goto_2

    :cond_4
    move v0, v1

    .line 87
    goto :goto_3
.end method
