.class public Lcom/crashlytics/android/ndk/CrashlyticsNdk;
.super Lio/fabric/sdk/android/Kit;
.source "CrashlyticsNdk.java"

# interfaces
.implements Lcom/crashlytics/android/core/CrashlyticsNdkDataProvider;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lio/fabric/sdk/android/Kit",
        "<",
        "Ljava/lang/Void;",
        ">;",
        "Lcom/crashlytics/android/core/CrashlyticsNdkDataProvider;"
    }
.end annotation


# static fields
.field static final TAG:Ljava/lang/String; = "CrashlyticsNdk"


# instance fields
.field private controller:Lcom/crashlytics/android/ndk/NdkKitController;

.field private crashlyticsNdkData:Lcom/crashlytics/android/core/CrashlyticsNdkData;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lio/fabric/sdk/android/Kit;-><init>()V

    return-void
.end method

.method public static getInstance()Lcom/crashlytics/android/ndk/CrashlyticsNdk;
    .locals 1

    .prologue
    .line 38
    const-class v0, Lcom/crashlytics/android/ndk/CrashlyticsNdk;

    invoke-static {v0}, Lio/fabric/sdk/android/Fabric;->getKit(Ljava/lang/Class;)Lio/fabric/sdk/android/Kit;

    move-result-object v0

    check-cast v0, Lcom/crashlytics/android/ndk/CrashlyticsNdk;

    return-object v0
.end method


# virtual methods
.method protected bridge synthetic doInBackground()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 17
    invoke-virtual {p0}, Lcom/crashlytics/android/ndk/CrashlyticsNdk;->doInBackground()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected doInBackground()Ljava/lang/Void;
    .locals 4

    .prologue
    .line 78
    :try_start_0
    iget-object v1, p0, Lcom/crashlytics/android/ndk/CrashlyticsNdk;->controller:Lcom/crashlytics/android/ndk/NdkKitController;

    invoke-interface {v1}, Lcom/crashlytics/android/ndk/NdkKitController;->getNativeData()Lcom/crashlytics/android/core/CrashlyticsNdkData;

    move-result-object v1

    iput-object v1, p0, Lcom/crashlytics/android/ndk/CrashlyticsNdk;->crashlyticsNdkData:Lcom/crashlytics/android/core/CrashlyticsNdkData;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 82
    :goto_0
    const/4 v1, 0x0

    return-object v1

    .line 79
    :catch_0
    move-exception v0

    .line 80
    .local v0, "e":Ljava/io/IOException;
    invoke-static {}, Lio/fabric/sdk/android/Fabric;->getLogger()Lio/fabric/sdk/android/Logger;

    move-result-object v1

    const-string v2, "CrashlyticsNdk"

    const-string v3, "Could not process ndk data; "

    invoke-interface {v1, v2, v3, v0}, Lio/fabric/sdk/android/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public getCrashlyticsNdkData()Lcom/crashlytics/android/core/CrashlyticsNdkData;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/crashlytics/android/ndk/CrashlyticsNdk;->crashlyticsNdkData:Lcom/crashlytics/android/core/CrashlyticsNdkData;

    return-object v0
.end method

.method public getIdentifier()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    const-string v0, "com.crashlytics.sdk.android.crashlytics-ndk"

    return-object v0
.end method

.method public getVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    const-string v0, "2.0.5.27"

    return-object v0
.end method

.method protected onPreExecute()Z
    .locals 7

    .prologue
    .line 44
    const-string v2, "!SDK-VERSION-STRING!:com.crashlytics.sdk.android:crashlytics-ndk:2.0.5.27"

    .line 45
    .local v2, "googlePlaySdkVersionString":Ljava/lang/String;
    const-class v3, Lcom/crashlytics/android/core/CrashlyticsCore;

    invoke-static {v3}, Lio/fabric/sdk/android/Fabric;->getKit(Ljava/lang/Class;)Lio/fabric/sdk/android/Kit;

    move-result-object v1

    check-cast v1, Lcom/crashlytics/android/core/CrashlyticsCore;

    .line 46
    .local v1, "crashlyticsCore":Lcom/crashlytics/android/core/CrashlyticsCore;
    if-nez v1, :cond_0

    .line 47
    new-instance v3, Lio/fabric/sdk/android/services/concurrency/UnmetDependencyException;

    const-string v4, "CrashlyticsNdk requires Crashlytics"

    invoke-direct {v3, v4}, Lio/fabric/sdk/android/services/concurrency/UnmetDependencyException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 49
    :cond_0
    new-instance v0, Lcom/crashlytics/android/ndk/BreakpadController;

    .line 50
    invoke-virtual {p0}, Lcom/crashlytics/android/ndk/CrashlyticsNdk;->getContext()Landroid/content/Context;

    move-result-object v3

    new-instance v4, Lcom/crashlytics/android/ndk/JniNativeApi;

    invoke-direct {v4}, Lcom/crashlytics/android/ndk/JniNativeApi;-><init>()V

    new-instance v5, Lcom/crashlytics/android/ndk/NdkCrashFilesManager;

    new-instance v6, Lio/fabric/sdk/android/services/persistence/FileStoreImpl;

    invoke-direct {v6, p0}, Lio/fabric/sdk/android/services/persistence/FileStoreImpl;-><init>(Lio/fabric/sdk/android/Kit;)V

    invoke-direct {v5, v6}, Lcom/crashlytics/android/ndk/NdkCrashFilesManager;-><init>(Lio/fabric/sdk/android/services/persistence/FileStore;)V

    invoke-direct {v0, v3, v4, v5}, Lcom/crashlytics/android/ndk/BreakpadController;-><init>(Landroid/content/Context;Lcom/crashlytics/android/ndk/NativeApi;Lcom/crashlytics/android/ndk/CrashFilesManager;)V

    .line 54
    .local v0, "controller":Lcom/crashlytics/android/ndk/NdkKitController;
    new-instance v3, Lcom/crashlytics/android/core/CrashlyticsKitBinder;

    invoke-direct {v3}, Lcom/crashlytics/android/core/CrashlyticsKitBinder;-><init>()V

    invoke-virtual {p0, v0, v1, v3}, Lcom/crashlytics/android/ndk/CrashlyticsNdk;->onPreExecute(Lcom/crashlytics/android/ndk/NdkKitController;Lcom/crashlytics/android/core/CrashlyticsCore;Lcom/crashlytics/android/core/CrashlyticsKitBinder;)Z

    move-result v3

    return v3
.end method

.method onPreExecute(Lcom/crashlytics/android/ndk/NdkKitController;Lcom/crashlytics/android/core/CrashlyticsCore;Lcom/crashlytics/android/core/CrashlyticsKitBinder;)Z
    .locals 4
    .param p1, "controller"    # Lcom/crashlytics/android/ndk/NdkKitController;
    .param p2, "crashlyticsCore"    # Lcom/crashlytics/android/core/CrashlyticsCore;
    .param p3, "kitBinder"    # Lcom/crashlytics/android/core/CrashlyticsKitBinder;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/crashlytics/android/ndk/CrashlyticsNdk;->controller:Lcom/crashlytics/android/ndk/NdkKitController;

    .line 66
    invoke-interface {p1}, Lcom/crashlytics/android/ndk/NdkKitController;->initialize()Z

    move-result v0

    .line 67
    .local v0, "initSuccess":Z
    if-eqz v0, :cond_0

    .line 68
    invoke-virtual {p3, p2, p0}, Lcom/crashlytics/android/core/CrashlyticsKitBinder;->bindCrashEventDataProvider(Lcom/crashlytics/android/core/CrashlyticsCore;Lcom/crashlytics/android/ndk/CrashlyticsNdk;)V

    .line 69
    invoke-static {}, Lio/fabric/sdk/android/Fabric;->getLogger()Lio/fabric/sdk/android/Logger;

    move-result-object v1

    const-string v2, "CrashlyticsNdk"

    const-string v3, "Crashlytics NDK initialization successful"

    invoke-interface {v1, v2, v3}, Lio/fabric/sdk/android/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    :cond_0
    return v0
.end method
