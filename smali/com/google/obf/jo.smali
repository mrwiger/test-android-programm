.class public final Lcom/google/obf/jo;
.super Ljava/lang/Object;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/obf/jo$a;
    }
.end annotation


# direct methods
.method static a()Lcom/google/obf/jz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/google/obf/jz",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1
    sget-object v0, Lcom/google/obf/jo$a;->a:Lcom/google/obf/jz;

    return-object v0
.end method

.method static a([Ljava/lang/Object;III)Lcom/google/obf/jz;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">([TT;III)",
            "Lcom/google/obf/jz",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 11
    if-ltz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/obf/ja;->a(Z)V

    .line 12
    add-int v0, p1, p2

    .line 13
    array-length v1, p0

    invoke-static {p1, v0, v1}, Lcom/google/obf/ja;->a(III)V

    .line 14
    invoke-static {p3, p2}, Lcom/google/obf/ja;->b(II)I

    .line 15
    if-nez p2, :cond_1

    .line 16
    invoke-static {}, Lcom/google/obf/jo;->a()Lcom/google/obf/jz;

    move-result-object v0

    .line 17
    :goto_1
    return-object v0

    .line 11
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 17
    :cond_1
    new-instance v0, Lcom/google/obf/jo$a;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/obf/jo$a;-><init>([Ljava/lang/Object;III)V

    goto :goto_1
.end method

.method public static a(Ljava/util/Iterator;Ljava/util/Iterator;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Iterator",
            "<*>;",
            "Ljava/util/Iterator",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2
    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 3
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_2

    .line 10
    :cond_1
    :goto_0
    return v0

    .line 5
    :cond_2
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 6
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 7
    invoke-static {v1, v2}, Lcom/google/obf/ix;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_0

    .line 10
    :cond_3
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v0, 0x1

    goto :goto_0
.end method
