.class public Lcom/yandex/metrica/impl/ob/bw;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/yandex/metrica/impl/ob/bw$a;
    }
.end annotation


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lcom/yandex/metrica/impl/ob/bz;

.field private c:Landroid/content/pm/PackageManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/yandex/metrica/impl/ob/bz;)V
    .locals 1

    .prologue
    .line 133
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/yandex/metrica/impl/ob/bw;-><init>(Landroid/content/Context;Lcom/yandex/metrica/impl/ob/bz;Landroid/content/pm/PackageManager;)V

    .line 134
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/yandex/metrica/impl/ob/bz;Landroid/content/pm/PackageManager;)V
    .locals 0

    .prologue
    .line 191
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 192
    iput-object p1, p0, Lcom/yandex/metrica/impl/ob/bw;->a:Landroid/content/Context;

    .line 193
    iput-object p2, p0, Lcom/yandex/metrica/impl/ob/bw;->b:Lcom/yandex/metrica/impl/ob/bz;

    .line 194
    iput-object p3, p0, Lcom/yandex/metrica/impl/ob/bw;->c:Landroid/content/pm/PackageManager;

    .line 195
    return-void
.end method

.method private static a(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 178
    .line 180
    :try_start_0
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string v3, "com.yandex.metrica.configuration.ACTION_START"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 181
    const/16 v3, 0x80

    invoke-virtual {p0, v2, v3}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v2

    .line 182
    if-eqz v2, :cond_0

    invoke-interface {v2}, Ljava/util/List;->size()I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-ne v2, v0, :cond_0

    .line 186
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 182
    goto :goto_0

    :catch_0
    move-exception v0

    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 7

    .prologue
    .line 139
    const/4 v1, 0x0

    .line 141
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/bw;->b:Lcom/yandex/metrica/impl/ob/bz;

    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/bw;->a:Landroid/content/Context;

    invoke-virtual {v0, v2}, Lcom/yandex/metrica/impl/ob/bz;->a(Landroid/content/Context;)Ljava/util/List;

    move-result-object v0

    .line 1162
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1163
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 1164
    iget-object v4, v0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v4, v4, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    .line 1165
    iget-object v5, v0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v5, v5, Landroid/content/pm/ServiceInfo;->metaData:Landroid/os/Bundle;

    const-string v6, "metrica:api:level"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    .line 1166
    const/16 v6, 0x35

    if-lt v5, v6, :cond_0

    iget-object v5, p0, Lcom/yandex/metrica/impl/ob/bw;->c:Landroid/content/pm/PackageManager;

    .line 1167
    invoke-static {v5, v4}, Lcom/yandex/metrica/impl/ob/bw;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1168
    iget-object v5, p0, Lcom/yandex/metrica/impl/ob/bw;->a:Landroid/content/Context;

    invoke-static {v5}, Lcom/yandex/metrica/impl/ob/ci;->a(Landroid/content/Context;)Lcom/yandex/metrica/impl/ob/ci;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/yandex/metrica/impl/ob/ci;->a(Ljava/lang/String;)Lcom/yandex/metrica/impl/ob/ch;

    move-result-object v4

    .line 1169
    if-eqz v4, :cond_0

    .line 1170
    new-instance v5, Lcom/yandex/metrica/impl/ob/bw$a;

    iget-object v6, p0, Lcom/yandex/metrica/impl/ob/bw;->c:Landroid/content/pm/PackageManager;

    invoke-direct {v5, v6, v0, v4}, Lcom/yandex/metrica/impl/ob/bw$a;-><init>(Landroid/content/pm/PackageManager;Landroid/content/pm/ResolveInfo;Lcom/yandex/metrica/impl/ob/ch;)V

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 145
    :cond_1
    invoke-static {v2}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 146
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 147
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/yandex/metrica/impl/ob/bw$a;

    .line 152
    invoke-static {v0}, Lcom/yandex/metrica/impl/ob/bw$a;->b(Lcom/yandex/metrica/impl/ob/bw$a;)I

    move-result v2

    const/16 v3, 0x70

    if-lt v2, v3, :cond_2

    .line 153
    invoke-static {v0}, Lcom/yandex/metrica/impl/ob/bw$a;->c(Lcom/yandex/metrica/impl/ob/bw$a;)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v0, v0, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    .line 157
    :goto_1
    return-object v0

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method
