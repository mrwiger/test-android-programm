.class Lru/cn/tv/mobile/telecasts/TelecastsListFragment$1;
.super Ljava/lang/Object;
.source "TelecastsListFragment.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mScrollState:I

.field private needLoadMoreData:Z

.field final synthetic this$0:Lru/cn/tv/mobile/telecasts/TelecastsListFragment;


# direct methods
.method constructor <init>(Lru/cn/tv/mobile/telecasts/TelecastsListFragment;)V
    .locals 1
    .param p1, "this$0"    # Lru/cn/tv/mobile/telecasts/TelecastsListFragment;

    .prologue
    .line 86
    iput-object p1, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment$1;->this$0:Lru/cn/tv/mobile/telecasts/TelecastsListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    const/4 v0, -0x1

    iput v0, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment$1;->mScrollState:I

    .line 89
    const/4 v0, 0x0

    iput-boolean v0, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment$1;->needLoadMoreData:Z

    return-void
.end method


# virtual methods
.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 2
    .param p1, "view"    # Landroid/widget/AbsListView;
    .param p2, "firstVisibleItem"    # I
    .param p3, "visibleItemCount"    # I
    .param p4, "totalItemCount"    # I

    .prologue
    .line 106
    iget-object v0, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment$1;->this$0:Lru/cn/tv/mobile/telecasts/TelecastsListFragment;

    invoke-static {v0}, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->access$100(Lru/cn/tv/mobile/telecasts/TelecastsListFragment;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getFooterViewsCount()I

    move-result v0

    iget-object v1, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment$1;->this$0:Lru/cn/tv/mobile/telecasts/TelecastsListFragment;

    .line 107
    invoke-static {v1}, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->access$100(Lru/cn/tv/mobile/telecasts/TelecastsListFragment;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v1

    add-int/2addr v0, v1

    if-le p3, v0, :cond_0

    add-int v0, p2, p3

    if-lt v0, p4, :cond_0

    .line 110
    iget-boolean v0, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment$1;->needLoadMoreData:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment$1;->this$0:Lru/cn/tv/mobile/telecasts/TelecastsListFragment;

    iget-boolean v0, v0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->noMoreData:Z

    if-nez v0, :cond_0

    iget v0, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment$1;->mScrollState:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 111
    const/4 v0, 0x1

    iput-boolean v0, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment$1;->needLoadMoreData:Z

    .line 113
    iget-object v0, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment$1;->this$0:Lru/cn/tv/mobile/telecasts/TelecastsListFragment;

    invoke-static {v0}, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->access$100(Lru/cn/tv/mobile/telecasts/TelecastsListFragment;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getFooterViewsCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 114
    iget-object v0, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment$1;->this$0:Lru/cn/tv/mobile/telecasts/TelecastsListFragment;

    invoke-static {v0}, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->access$100(Lru/cn/tv/mobile/telecasts/TelecastsListFragment;)Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment$1;->this$0:Lru/cn/tv/mobile/telecasts/TelecastsListFragment;

    invoke-static {v1}, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->access$200(Lru/cn/tv/mobile/telecasts/TelecastsListFragment;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;)V

    .line 118
    :cond_0
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 1
    .param p1, "view"    # Landroid/widget/AbsListView;
    .param p2, "scrollState"    # I

    .prologue
    .line 94
    iput p2, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment$1;->mScrollState:I

    .line 95
    iget-boolean v0, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment$1;->needLoadMoreData:Z

    if-eqz v0, :cond_0

    if-nez p2, :cond_0

    iget-object v0, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment$1;->this$0:Lru/cn/tv/mobile/telecasts/TelecastsListFragment;

    iget-boolean v0, v0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->noMoreData:Z

    if-nez v0, :cond_0

    .line 97
    const/4 v0, 0x0

    iput-boolean v0, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment$1;->needLoadMoreData:Z

    .line 98
    iget-object v0, p0, Lru/cn/tv/mobile/telecasts/TelecastsListFragment$1;->this$0:Lru/cn/tv/mobile/telecasts/TelecastsListFragment;

    invoke-static {v0}, Lru/cn/tv/mobile/telecasts/TelecastsListFragment;->access$000(Lru/cn/tv/mobile/telecasts/TelecastsListFragment;)Lru/cn/tv/mobile/telecasts/TelecastsViewModel;

    move-result-object v0

    invoke-virtual {v0}, Lru/cn/tv/mobile/telecasts/TelecastsViewModel;->prefetch()V

    .line 100
    :cond_0
    return-void
.end method
