.class public Lcom/google/obf/kh;
.super Ljava/lang/Object;
.source "IMASDK"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Ljava/util/Set",
            "<",
            "Lcom/google/obf/kn",
            "<",
            "Lcom/google/obf/kl;",
            "Lcom/google/obf/kl;",
            ">;>;>;"
        }
    .end annotation
.end field


# instance fields
.field private b:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 267
    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    sput-object v0, Lcom/google/obf/kh;->a:Ljava/lang/ThreadLocal;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/obf/kh;->b:Z

    .line 27
    return-void
.end method

.method static a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/obf/kn;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ")",
            "Lcom/google/obf/kn",
            "<",
            "Lcom/google/obf/kl;",
            "Lcom/google/obf/kl;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2
    new-instance v0, Lcom/google/obf/kl;

    invoke-direct {v0, p0}, Lcom/google/obf/kl;-><init>(Ljava/lang/Object;)V

    .line 3
    new-instance v1, Lcom/google/obf/kl;

    invoke-direct {v1, p1}, Lcom/google/obf/kl;-><init>(Ljava/lang/Object;)V

    .line 4
    invoke-static {v0, v1}, Lcom/google/obf/kn;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/obf/kn;

    move-result-object v0

    return-object v0
.end method

.method static a()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/google/obf/kn",
            "<",
            "Lcom/google/obf/kl;",
            "Lcom/google/obf/kl;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1
    sget-object v0, Lcom/google/obf/kh;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method

.method private static a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Class;Lcom/google/obf/kh;Z[Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            "Ljava/lang/Class",
            "<*>;",
            "Lcom/google/obf/kh;",
            "Z[",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 55
    invoke-static {p0, p1}, Lcom/google/obf/kh;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 75
    :goto_0
    return-void

    .line 57
    :cond_0
    :try_start_0
    invoke-static {p0, p1}, Lcom/google/obf/kh;->d(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 58
    invoke-virtual {p2}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object v1

    .line 59
    const/4 v0, 0x1

    invoke-static {v1, v0}, Ljava/lang/reflect/AccessibleObject;->setAccessible([Ljava/lang/reflect/AccessibleObject;Z)V

    .line 60
    const/4 v0, 0x0

    :goto_1
    array-length v2, v1

    if-ge v0, v2, :cond_3

    iget-boolean v2, p3, Lcom/google/obf/kh;->b:Z

    if-eqz v2, :cond_3

    .line 61
    aget-object v2, v1, v0

    .line 62
    invoke-virtual {v2}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {p5, v3}, Lcom/google/obf/kd;->b([Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 63
    invoke-virtual {v2}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "$"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    if-nez p4, :cond_1

    .line 64
    invoke-virtual {v2}, Ljava/lang/reflect/Field;->getModifiers()I

    move-result v3

    invoke-static {v3}, Ljava/lang/reflect/Modifier;->isTransient(I)Z

    move-result v3

    if-nez v3, :cond_2

    .line 65
    :cond_1
    invoke-virtual {v2}, Ljava/lang/reflect/Field;->getModifiers()I

    move-result v3

    invoke-static {v3}, Ljava/lang/reflect/Modifier;->isStatic(I)Z

    move-result v3

    if-nez v3, :cond_2

    const-class v3, Lcom/google/obf/ki;

    .line 66
    invoke-virtual {v2, v3}, Ljava/lang/reflect/Field;->isAnnotationPresent(Ljava/lang/Class;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    if-nez v3, :cond_2

    .line 67
    :try_start_1
    invoke-virtual {v2, p0}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, p1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p3, v3, v2}, Lcom/google/obf/kh;->c(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/obf/kh;
    :try_end_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 71
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 69
    :catch_0
    move-exception v0

    .line 70
    :try_start_2
    new-instance v0, Ljava/lang/InternalError;

    const-string v1, "Unexpected IllegalAccessException"

    invoke-direct {v0, v1}, Ljava/lang/InternalError;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 74
    :catchall_0
    move-exception v0

    invoke-static {p0, p1}, Lcom/google/obf/kh;->e(Ljava/lang/Object;Ljava/lang/Object;)V

    throw v0

    .line 72
    :cond_3
    invoke-static {p0, p1}, Lcom/google/obf/kh;->e(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static varargs a(Ljava/lang/Object;Ljava/lang/Object;ZLjava/lang/Class;[Ljava/lang/String;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            "Z",
            "Ljava/lang/Class",
            "<*>;[",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 29
    if-ne p0, p1, :cond_0

    .line 30
    const/4 v0, 0x1

    .line 54
    :goto_0
    return v0

    .line 31
    :cond_0
    if-eqz p0, :cond_1

    if-nez p1, :cond_2

    :cond_1
    move v0, v6

    .line 32
    goto :goto_0

    .line 33
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 34
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    .line 35
    invoke-virtual {v0, p1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 37
    invoke-virtual {v2, p0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 44
    :cond_3
    :goto_1
    new-instance v3, Lcom/google/obf/kh;

    invoke-direct {v3}, Lcom/google/obf/kh;-><init>()V

    .line 45
    :try_start_0
    invoke-virtual {v2}, Ljava/lang/Class;->isArray()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 46
    invoke-virtual {v3, p0, p1}, Lcom/google/obf/kh;->c(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/obf/kh;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 54
    :cond_4
    invoke-virtual {v3}, Lcom/google/obf/kh;->b()Z

    move-result v0

    goto :goto_0

    .line 39
    :cond_5
    invoke-virtual {v2, p0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 41
    invoke-virtual {v0, p1}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    move-object v2, v0

    .line 42
    goto :goto_1

    :cond_6
    move v0, v6

    .line 43
    goto :goto_0

    :cond_7
    move-object v0, p0

    move-object v1, p1

    move v4, p2

    move-object v5, p4

    .line 47
    :try_start_1
    invoke-static/range {v0 .. v5}, Lcom/google/obf/kh;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Class;Lcom/google/obf/kh;Z[Ljava/lang/String;)V

    .line 48
    :goto_2
    invoke-virtual {v2}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v0

    if-eqz v0, :cond_4

    if-eq v2, p3, :cond_4

    .line 49
    invoke-virtual {v2}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v2

    move-object v0, p0

    move-object v1, p1

    move v4, p2

    move-object v5, p4

    .line 50
    invoke-static/range {v0 .. v5}, Lcom/google/obf/kh;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Class;Lcom/google/obf/kh;Z[Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 52
    :catch_0
    move-exception v0

    move v0, v6

    .line 53
    goto :goto_0

    :cond_8
    move-object v2, v0

    goto :goto_1
.end method

.method public static varargs a(Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 28
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {p0, p1, v0, v1, p2}, Lcom/google/obf/kh;->a(Ljava/lang/Object;Ljava/lang/Object;ZLjava/lang/Class;[Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static b(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 5
    invoke-static {}, Lcom/google/obf/kh;->a()Ljava/util/Set;

    move-result-object v0

    .line 6
    invoke-static {p0, p1}, Lcom/google/obf/kh;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/obf/kn;

    move-result-object v1

    .line 7
    invoke-virtual {v1}, Lcom/google/obf/kn;->a()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/obf/kn;->b()Ljava/lang/Object;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/obf/kn;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/obf/kn;

    move-result-object v2

    .line 8
    if-eqz v0, :cond_1

    .line 9
    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 10
    :goto_0
    return v0

    .line 9
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static d(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 11
    invoke-static {}, Lcom/google/obf/kh;->a()Ljava/util/Set;

    move-result-object v0

    .line 12
    if-nez v0, :cond_0

    .line 13
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 14
    sget-object v1, Lcom/google/obf/kh;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v1, v0}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 15
    :cond_0
    invoke-static {p0, p1}, Lcom/google/obf/kh;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/obf/kn;

    move-result-object v1

    .line 16
    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 17
    return-void
.end method

.method private static e(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 18
    invoke-static {}, Lcom/google/obf/kh;->a()Ljava/util/Set;

    move-result-object v0

    .line 19
    if-eqz v0, :cond_0

    .line 20
    invoke-static {p0, p1}, Lcom/google/obf/kh;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/obf/kn;

    move-result-object v1

    .line 21
    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 22
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 23
    sget-object v0, Lcom/google/obf/kh;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->remove()V

    .line 24
    :cond_0
    return-void
.end method

.method private f(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 88
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 89
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/obf/kh;->a(Z)V

    .line 107
    :goto_0
    return-void

    .line 90
    :cond_0
    instance-of v0, p1, [J

    if-eqz v0, :cond_1

    .line 91
    check-cast p1, [J

    check-cast p1, [J

    check-cast p2, [J

    check-cast p2, [J

    invoke-virtual {p0, p1, p2}, Lcom/google/obf/kh;->a([J[J)Lcom/google/obf/kh;

    goto :goto_0

    .line 92
    :cond_1
    instance-of v0, p1, [I

    if-eqz v0, :cond_2

    .line 93
    check-cast p1, [I

    check-cast p1, [I

    check-cast p2, [I

    check-cast p2, [I

    invoke-virtual {p0, p1, p2}, Lcom/google/obf/kh;->a([I[I)Lcom/google/obf/kh;

    goto :goto_0

    .line 94
    :cond_2
    instance-of v0, p1, [S

    if-eqz v0, :cond_3

    .line 95
    check-cast p1, [S

    check-cast p1, [S

    check-cast p2, [S

    check-cast p2, [S

    invoke-virtual {p0, p1, p2}, Lcom/google/obf/kh;->a([S[S)Lcom/google/obf/kh;

    goto :goto_0

    .line 96
    :cond_3
    instance-of v0, p1, [C

    if-eqz v0, :cond_4

    .line 97
    check-cast p1, [C

    check-cast p1, [C

    check-cast p2, [C

    check-cast p2, [C

    invoke-virtual {p0, p1, p2}, Lcom/google/obf/kh;->a([C[C)Lcom/google/obf/kh;

    goto :goto_0

    .line 98
    :cond_4
    instance-of v0, p1, [B

    if-eqz v0, :cond_5

    .line 99
    check-cast p1, [B

    check-cast p1, [B

    check-cast p2, [B

    check-cast p2, [B

    invoke-virtual {p0, p1, p2}, Lcom/google/obf/kh;->a([B[B)Lcom/google/obf/kh;

    goto :goto_0

    .line 100
    :cond_5
    instance-of v0, p1, [D

    if-eqz v0, :cond_6

    .line 101
    check-cast p1, [D

    check-cast p1, [D

    check-cast p2, [D

    check-cast p2, [D

    invoke-virtual {p0, p1, p2}, Lcom/google/obf/kh;->a([D[D)Lcom/google/obf/kh;

    goto :goto_0

    .line 102
    :cond_6
    instance-of v0, p1, [F

    if-eqz v0, :cond_7

    .line 103
    check-cast p1, [F

    check-cast p1, [F

    check-cast p2, [F

    check-cast p2, [F

    invoke-virtual {p0, p1, p2}, Lcom/google/obf/kh;->a([F[F)Lcom/google/obf/kh;

    goto :goto_0

    .line 104
    :cond_7
    instance-of v0, p1, [Z

    if-eqz v0, :cond_8

    .line 105
    check-cast p1, [Z

    check-cast p1, [Z

    check-cast p2, [Z

    check-cast p2, [Z

    invoke-virtual {p0, p1, p2}, Lcom/google/obf/kh;->a([Z[Z)Lcom/google/obf/kh;

    goto :goto_0

    .line 106
    :cond_8
    check-cast p1, [Ljava/lang/Object;

    check-cast p1, [Ljava/lang/Object;

    check-cast p2, [Ljava/lang/Object;

    check-cast p2, [Ljava/lang/Object;

    invoke-virtual {p0, p1, p2}, Lcom/google/obf/kh;->a([Ljava/lang/Object;[Ljava/lang/Object;)Lcom/google/obf/kh;

    goto/16 :goto_0
.end method


# virtual methods
.method public a(BB)Lcom/google/obf/kh;
    .locals 1

    .prologue
    .line 124
    iget-boolean v0, p0, Lcom/google/obf/kh;->b:Z

    if-nez v0, :cond_0

    .line 127
    :goto_0
    return-object p0

    .line 126
    :cond_0
    if-ne p1, p2, :cond_1

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/google/obf/kh;->b:Z

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public a(CC)Lcom/google/obf/kh;
    .locals 1

    .prologue
    .line 120
    iget-boolean v0, p0, Lcom/google/obf/kh;->b:Z

    if-nez v0, :cond_0

    .line 123
    :goto_0
    return-object p0

    .line 122
    :cond_0
    if-ne p1, p2, :cond_1

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/google/obf/kh;->b:Z

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public a(DD)Lcom/google/obf/kh;
    .locals 5

    .prologue
    .line 128
    iget-boolean v0, p0, Lcom/google/obf/kh;->b:Z

    if-nez v0, :cond_0

    .line 130
    :goto_0
    return-object p0

    :cond_0
    invoke-static {p1, p2}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v0

    invoke-static {p3, p4}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/obf/kh;->a(JJ)Lcom/google/obf/kh;

    move-result-object p0

    goto :goto_0
.end method

.method public a(FF)Lcom/google/obf/kh;
    .locals 2

    .prologue
    .line 131
    iget-boolean v0, p0, Lcom/google/obf/kh;->b:Z

    if-nez v0, :cond_0

    .line 133
    :goto_0
    return-object p0

    :cond_0
    invoke-static {p1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    invoke-static {p2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/obf/kh;->a(II)Lcom/google/obf/kh;

    move-result-object p0

    goto :goto_0
.end method

.method public a(II)Lcom/google/obf/kh;
    .locals 1

    .prologue
    .line 112
    iget-boolean v0, p0, Lcom/google/obf/kh;->b:Z

    if-nez v0, :cond_0

    .line 115
    :goto_0
    return-object p0

    .line 114
    :cond_0
    if-ne p1, p2, :cond_1

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/google/obf/kh;->b:Z

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public a(JJ)Lcom/google/obf/kh;
    .locals 1

    .prologue
    .line 108
    iget-boolean v0, p0, Lcom/google/obf/kh;->b:Z

    if-nez v0, :cond_0

    .line 111
    :goto_0
    return-object p0

    .line 110
    :cond_0
    cmp-long v0, p1, p3

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/google/obf/kh;->b:Z

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public a(SS)Lcom/google/obf/kh;
    .locals 1

    .prologue
    .line 116
    iget-boolean v0, p0, Lcom/google/obf/kh;->b:Z

    if-nez v0, :cond_0

    .line 119
    :goto_0
    return-object p0

    .line 118
    :cond_0
    if-ne p1, p2, :cond_1

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/google/obf/kh;->b:Z

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public a(ZZ)Lcom/google/obf/kh;
    .locals 1

    .prologue
    .line 134
    iget-boolean v0, p0, Lcom/google/obf/kh;->b:Z

    if-nez v0, :cond_0

    .line 137
    :goto_0
    return-object p0

    .line 136
    :cond_0
    if-ne p1, p2, :cond_1

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/google/obf/kh;->b:Z

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public a([B[B)Lcom/google/obf/kh;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 208
    iget-boolean v1, p0, Lcom/google/obf/kh;->b:Z

    if-nez v1, :cond_1

    .line 221
    :cond_0
    :goto_0
    return-object p0

    .line 210
    :cond_1
    if-eq p1, p2, :cond_0

    .line 212
    if-eqz p1, :cond_2

    if-nez p2, :cond_3

    .line 213
    :cond_2
    invoke-virtual {p0, v0}, Lcom/google/obf/kh;->a(Z)V

    goto :goto_0

    .line 215
    :cond_3
    array-length v1, p1

    array-length v2, p2

    if-eq v1, v2, :cond_4

    .line 216
    invoke-virtual {p0, v0}, Lcom/google/obf/kh;->a(Z)V

    goto :goto_0

    .line 218
    :cond_4
    :goto_1
    array-length v1, p1

    if-ge v0, v1, :cond_0

    iget-boolean v1, p0, Lcom/google/obf/kh;->b:Z

    if-eqz v1, :cond_0

    .line 219
    aget-byte v1, p1, v0

    aget-byte v2, p2, v0

    invoke-virtual {p0, v1, v2}, Lcom/google/obf/kh;->a(BB)Lcom/google/obf/kh;

    .line 220
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public a([C[C)Lcom/google/obf/kh;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 194
    iget-boolean v1, p0, Lcom/google/obf/kh;->b:Z

    if-nez v1, :cond_1

    .line 207
    :cond_0
    :goto_0
    return-object p0

    .line 196
    :cond_1
    if-eq p1, p2, :cond_0

    .line 198
    if-eqz p1, :cond_2

    if-nez p2, :cond_3

    .line 199
    :cond_2
    invoke-virtual {p0, v0}, Lcom/google/obf/kh;->a(Z)V

    goto :goto_0

    .line 201
    :cond_3
    array-length v1, p1

    array-length v2, p2

    if-eq v1, v2, :cond_4

    .line 202
    invoke-virtual {p0, v0}, Lcom/google/obf/kh;->a(Z)V

    goto :goto_0

    .line 204
    :cond_4
    :goto_1
    array-length v1, p1

    if-ge v0, v1, :cond_0

    iget-boolean v1, p0, Lcom/google/obf/kh;->b:Z

    if-eqz v1, :cond_0

    .line 205
    aget-char v1, p1, v0

    aget-char v2, p2, v0

    invoke-virtual {p0, v1, v2}, Lcom/google/obf/kh;->a(CC)Lcom/google/obf/kh;

    .line 206
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public a([D[D)Lcom/google/obf/kh;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 222
    iget-boolean v1, p0, Lcom/google/obf/kh;->b:Z

    if-nez v1, :cond_1

    .line 235
    :cond_0
    :goto_0
    return-object p0

    .line 224
    :cond_1
    if-eq p1, p2, :cond_0

    .line 226
    if-eqz p1, :cond_2

    if-nez p2, :cond_3

    .line 227
    :cond_2
    invoke-virtual {p0, v0}, Lcom/google/obf/kh;->a(Z)V

    goto :goto_0

    .line 229
    :cond_3
    array-length v1, p1

    array-length v2, p2

    if-eq v1, v2, :cond_4

    .line 230
    invoke-virtual {p0, v0}, Lcom/google/obf/kh;->a(Z)V

    goto :goto_0

    .line 232
    :cond_4
    :goto_1
    array-length v1, p1

    if-ge v0, v1, :cond_0

    iget-boolean v1, p0, Lcom/google/obf/kh;->b:Z

    if-eqz v1, :cond_0

    .line 233
    aget-wide v2, p1, v0

    aget-wide v4, p2, v0

    invoke-virtual {p0, v2, v3, v4, v5}, Lcom/google/obf/kh;->a(DD)Lcom/google/obf/kh;

    .line 234
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public a([F[F)Lcom/google/obf/kh;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 236
    iget-boolean v1, p0, Lcom/google/obf/kh;->b:Z

    if-nez v1, :cond_1

    .line 249
    :cond_0
    :goto_0
    return-object p0

    .line 238
    :cond_1
    if-eq p1, p2, :cond_0

    .line 240
    if-eqz p1, :cond_2

    if-nez p2, :cond_3

    .line 241
    :cond_2
    invoke-virtual {p0, v0}, Lcom/google/obf/kh;->a(Z)V

    goto :goto_0

    .line 243
    :cond_3
    array-length v1, p1

    array-length v2, p2

    if-eq v1, v2, :cond_4

    .line 244
    invoke-virtual {p0, v0}, Lcom/google/obf/kh;->a(Z)V

    goto :goto_0

    .line 246
    :cond_4
    :goto_1
    array-length v1, p1

    if-ge v0, v1, :cond_0

    iget-boolean v1, p0, Lcom/google/obf/kh;->b:Z

    if-eqz v1, :cond_0

    .line 247
    aget v1, p1, v0

    aget v2, p2, v0

    invoke-virtual {p0, v1, v2}, Lcom/google/obf/kh;->a(FF)Lcom/google/obf/kh;

    .line 248
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public a([I[I)Lcom/google/obf/kh;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 166
    iget-boolean v1, p0, Lcom/google/obf/kh;->b:Z

    if-nez v1, :cond_1

    .line 179
    :cond_0
    :goto_0
    return-object p0

    .line 168
    :cond_1
    if-eq p1, p2, :cond_0

    .line 170
    if-eqz p1, :cond_2

    if-nez p2, :cond_3

    .line 171
    :cond_2
    invoke-virtual {p0, v0}, Lcom/google/obf/kh;->a(Z)V

    goto :goto_0

    .line 173
    :cond_3
    array-length v1, p1

    array-length v2, p2

    if-eq v1, v2, :cond_4

    .line 174
    invoke-virtual {p0, v0}, Lcom/google/obf/kh;->a(Z)V

    goto :goto_0

    .line 176
    :cond_4
    :goto_1
    array-length v1, p1

    if-ge v0, v1, :cond_0

    iget-boolean v1, p0, Lcom/google/obf/kh;->b:Z

    if-eqz v1, :cond_0

    .line 177
    aget v1, p1, v0

    aget v2, p2, v0

    invoke-virtual {p0, v1, v2}, Lcom/google/obf/kh;->a(II)Lcom/google/obf/kh;

    .line 178
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public a([J[J)Lcom/google/obf/kh;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 152
    iget-boolean v1, p0, Lcom/google/obf/kh;->b:Z

    if-nez v1, :cond_1

    .line 165
    :cond_0
    :goto_0
    return-object p0

    .line 154
    :cond_1
    if-eq p1, p2, :cond_0

    .line 156
    if-eqz p1, :cond_2

    if-nez p2, :cond_3

    .line 157
    :cond_2
    invoke-virtual {p0, v0}, Lcom/google/obf/kh;->a(Z)V

    goto :goto_0

    .line 159
    :cond_3
    array-length v1, p1

    array-length v2, p2

    if-eq v1, v2, :cond_4

    .line 160
    invoke-virtual {p0, v0}, Lcom/google/obf/kh;->a(Z)V

    goto :goto_0

    .line 162
    :cond_4
    :goto_1
    array-length v1, p1

    if-ge v0, v1, :cond_0

    iget-boolean v1, p0, Lcom/google/obf/kh;->b:Z

    if-eqz v1, :cond_0

    .line 163
    aget-wide v2, p1, v0

    aget-wide v4, p2, v0

    invoke-virtual {p0, v2, v3, v4, v5}, Lcom/google/obf/kh;->a(JJ)Lcom/google/obf/kh;

    .line 164
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public a([Ljava/lang/Object;[Ljava/lang/Object;)Lcom/google/obf/kh;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 138
    iget-boolean v1, p0, Lcom/google/obf/kh;->b:Z

    if-nez v1, :cond_1

    .line 151
    :cond_0
    :goto_0
    return-object p0

    .line 140
    :cond_1
    if-eq p1, p2, :cond_0

    .line 142
    if-eqz p1, :cond_2

    if-nez p2, :cond_3

    .line 143
    :cond_2
    invoke-virtual {p0, v0}, Lcom/google/obf/kh;->a(Z)V

    goto :goto_0

    .line 145
    :cond_3
    array-length v1, p1

    array-length v2, p2

    if-eq v1, v2, :cond_4

    .line 146
    invoke-virtual {p0, v0}, Lcom/google/obf/kh;->a(Z)V

    goto :goto_0

    .line 148
    :cond_4
    :goto_1
    array-length v1, p1

    if-ge v0, v1, :cond_0

    iget-boolean v1, p0, Lcom/google/obf/kh;->b:Z

    if-eqz v1, :cond_0

    .line 149
    aget-object v1, p1, v0

    aget-object v2, p2, v0

    invoke-virtual {p0, v1, v2}, Lcom/google/obf/kh;->c(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/obf/kh;

    .line 150
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public a([S[S)Lcom/google/obf/kh;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 180
    iget-boolean v1, p0, Lcom/google/obf/kh;->b:Z

    if-nez v1, :cond_1

    .line 193
    :cond_0
    :goto_0
    return-object p0

    .line 182
    :cond_1
    if-eq p1, p2, :cond_0

    .line 184
    if-eqz p1, :cond_2

    if-nez p2, :cond_3

    .line 185
    :cond_2
    invoke-virtual {p0, v0}, Lcom/google/obf/kh;->a(Z)V

    goto :goto_0

    .line 187
    :cond_3
    array-length v1, p1

    array-length v2, p2

    if-eq v1, v2, :cond_4

    .line 188
    invoke-virtual {p0, v0}, Lcom/google/obf/kh;->a(Z)V

    goto :goto_0

    .line 190
    :cond_4
    :goto_1
    array-length v1, p1

    if-ge v0, v1, :cond_0

    iget-boolean v1, p0, Lcom/google/obf/kh;->b:Z

    if-eqz v1, :cond_0

    .line 191
    aget-short v1, p1, v0

    aget-short v2, p2, v0

    invoke-virtual {p0, v1, v2}, Lcom/google/obf/kh;->a(SS)Lcom/google/obf/kh;

    .line 192
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public a([Z[Z)Lcom/google/obf/kh;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 250
    iget-boolean v1, p0, Lcom/google/obf/kh;->b:Z

    if-nez v1, :cond_1

    .line 263
    :cond_0
    :goto_0
    return-object p0

    .line 252
    :cond_1
    if-eq p1, p2, :cond_0

    .line 254
    if-eqz p1, :cond_2

    if-nez p2, :cond_3

    .line 255
    :cond_2
    invoke-virtual {p0, v0}, Lcom/google/obf/kh;->a(Z)V

    goto :goto_0

    .line 257
    :cond_3
    array-length v1, p1

    array-length v2, p2

    if-eq v1, v2, :cond_4

    .line 258
    invoke-virtual {p0, v0}, Lcom/google/obf/kh;->a(Z)V

    goto :goto_0

    .line 260
    :cond_4
    :goto_1
    array-length v1, p1

    if-ge v0, v1, :cond_0

    iget-boolean v1, p0, Lcom/google/obf/kh;->b:Z

    if-eqz v1, :cond_0

    .line 261
    aget-boolean v1, p1, v0

    aget-boolean v2, p2, v0

    invoke-virtual {p0, v1, v2}, Lcom/google/obf/kh;->a(ZZ)Lcom/google/obf/kh;

    .line 262
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method protected a(Z)V
    .locals 0

    .prologue
    .line 265
    iput-boolean p1, p0, Lcom/google/obf/kh;->b:Z

    .line 266
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 264
    iget-boolean v0, p0, Lcom/google/obf/kh;->b:Z

    return v0
.end method

.method public c(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/obf/kh;
    .locals 1

    .prologue
    .line 76
    iget-boolean v0, p0, Lcom/google/obf/kh;->b:Z

    if-nez v0, :cond_1

    .line 87
    :cond_0
    :goto_0
    return-object p0

    .line 78
    :cond_1
    if-eq p1, p2, :cond_0

    .line 80
    if-eqz p1, :cond_2

    if-nez p2, :cond_3

    .line 81
    :cond_2
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/obf/kh;->a(Z)V

    goto :goto_0

    .line 83
    :cond_3
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 84
    invoke-virtual {v0}, Ljava/lang/Class;->isArray()Z

    move-result v0

    if-nez v0, :cond_4

    .line 85
    invoke-virtual {p1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/obf/kh;->b:Z

    goto :goto_0

    .line 86
    :cond_4
    invoke-direct {p0, p1, p2}, Lcom/google/obf/kh;->f(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method
