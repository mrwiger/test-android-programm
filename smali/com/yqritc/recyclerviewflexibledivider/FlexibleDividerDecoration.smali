.class public abstract Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration;
.super Landroid/support/v7/widget/RecyclerView$ItemDecoration;
.source "FlexibleDividerDecoration.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;,
        Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$SizeProvider;,
        Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$DrawableProvider;,
        Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$ColorProvider;,
        Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$PaintProvider;,
        Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$VisibilityProvider;,
        Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$DividerType;
    }
.end annotation


# static fields
.field private static final ATTRS:[I


# instance fields
.field protected mColorProvider:Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$ColorProvider;

.field protected mDividerType:Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$DividerType;

.field protected mDrawableProvider:Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$DrawableProvider;

.field private mPaint:Landroid/graphics/Paint;

.field protected mPaintProvider:Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$PaintProvider;

.field protected mPositionInsideItem:Z

.field protected mShowLastDivider:Z

.field protected mSizeProvider:Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$SizeProvider;

.field protected mVisibilityProvider:Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$VisibilityProvider;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 25
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x1010214

    aput v2, v0, v1

    sput-object v0, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration;->ATTRS:[I

    return-void
.end method

.method protected constructor <init>(Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;)V
    .locals 4
    .param p1, "builder"    # Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;

    .prologue
    .line 43
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$ItemDecoration;-><init>()V

    .line 33
    sget-object v2, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$DividerType;->DRAWABLE:Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$DividerType;

    iput-object v2, p0, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration;->mDividerType:Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$DividerType;

    .line 44
    invoke-static {p1}, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;->access$000(Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;)Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$PaintProvider;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 45
    sget-object v2, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$DividerType;->PAINT:Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$DividerType;

    iput-object v2, p0, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration;->mDividerType:Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$DividerType;

    .line 46
    invoke-static {p1}, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;->access$000(Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;)Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$PaintProvider;

    move-result-object v2

    iput-object v2, p0, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration;->mPaintProvider:Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$PaintProvider;

    .line 70
    :goto_0
    invoke-static {p1}, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;->access$500(Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;)Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$VisibilityProvider;

    move-result-object v2

    iput-object v2, p0, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration;->mVisibilityProvider:Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$VisibilityProvider;

    .line 71
    invoke-static {p1}, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;->access$600(Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration;->mShowLastDivider:Z

    .line 72
    invoke-static {p1}, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;->access$700(Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration;->mPositionInsideItem:Z

    .line 73
    return-void

    .line 47
    :cond_0
    invoke-static {p1}, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;->access$100(Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;)Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$ColorProvider;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 48
    sget-object v2, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$DividerType;->COLOR:Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$DividerType;

    iput-object v2, p0, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration;->mDividerType:Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$DividerType;

    .line 49
    invoke-static {p1}, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;->access$100(Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;)Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$ColorProvider;

    move-result-object v2

    iput-object v2, p0, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration;->mColorProvider:Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$ColorProvider;

    .line 50
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration;->mPaint:Landroid/graphics/Paint;

    .line 51
    invoke-direct {p0, p1}, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration;->setSizeProvider(Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;)V

    goto :goto_0

    .line 53
    :cond_1
    sget-object v2, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$DividerType;->DRAWABLE:Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$DividerType;

    iput-object v2, p0, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration;->mDividerType:Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$DividerType;

    .line 54
    invoke-static {p1}, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;->access$200(Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;)Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$DrawableProvider;

    move-result-object v2

    if-nez v2, :cond_2

    .line 55
    invoke-static {p1}, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;->access$300(Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;)Landroid/content/Context;

    move-result-object v2

    sget-object v3, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration;->ATTRS:[I

    invoke-virtual {v2, v3}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 56
    .local v0, "a":Landroid/content/res/TypedArray;
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 57
    .local v1, "divider":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 58
    new-instance v2, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$1;

    invoke-direct {v2, p0, v1}, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$1;-><init>(Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration;Landroid/graphics/drawable/Drawable;)V

    iput-object v2, p0, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration;->mDrawableProvider:Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$DrawableProvider;

    .line 67
    .end local v0    # "a":Landroid/content/res/TypedArray;
    .end local v1    # "divider":Landroid/graphics/drawable/Drawable;
    :goto_1
    invoke-static {p1}, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;->access$400(Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;)Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$SizeProvider;

    move-result-object v2

    iput-object v2, p0, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration;->mSizeProvider:Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$SizeProvider;

    goto :goto_0

    .line 65
    :cond_2
    invoke-static {p1}, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;->access$200(Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;)Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$DrawableProvider;

    move-result-object v2

    iput-object v2, p0, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration;->mDrawableProvider:Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$DrawableProvider;

    goto :goto_1
.end method

.method private getGroupIndex(ILandroid/support/v7/widget/RecyclerView;)I
    .locals 4
    .param p1, "position"    # I
    .param p2, "parent"    # Landroid/support/v7/widget/RecyclerView;

    .prologue
    .line 231
    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()Landroid/support/v7/widget/RecyclerView$LayoutManager;

    move-result-object v3

    instance-of v3, v3, Landroid/support/v7/widget/GridLayoutManager;

    if-eqz v3, :cond_0

    .line 232
    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()Landroid/support/v7/widget/RecyclerView$LayoutManager;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/GridLayoutManager;

    .line 233
    .local v0, "layoutManager":Landroid/support/v7/widget/GridLayoutManager;
    invoke-virtual {v0}, Landroid/support/v7/widget/GridLayoutManager;->getSpanSizeLookup()Landroid/support/v7/widget/GridLayoutManager$SpanSizeLookup;

    move-result-object v2

    .line 234
    .local v2, "spanSizeLookup":Landroid/support/v7/widget/GridLayoutManager$SpanSizeLookup;
    invoke-virtual {v0}, Landroid/support/v7/widget/GridLayoutManager;->getSpanCount()I

    move-result v1

    .line 235
    .local v1, "spanCount":I
    invoke-virtual {v2, p1, v1}, Landroid/support/v7/widget/GridLayoutManager$SpanSizeLookup;->getSpanGroupIndex(II)I

    move-result p1

    .line 238
    .end local v0    # "layoutManager":Landroid/support/v7/widget/GridLayoutManager;
    .end local v1    # "spanCount":I
    .end local v2    # "spanSizeLookup":Landroid/support/v7/widget/GridLayoutManager$SpanSizeLookup;
    .end local p1    # "position":I
    :cond_0
    return p1
.end method

.method private getLastDividerOffset(Landroid/support/v7/widget/RecyclerView;)I
    .locals 6
    .param p1, "parent"    # Landroid/support/v7/widget/RecyclerView;

    .prologue
    .line 188
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()Landroid/support/v7/widget/RecyclerView$LayoutManager;

    move-result-object v5

    instance-of v5, v5, Landroid/support/v7/widget/GridLayoutManager;

    if-eqz v5, :cond_1

    .line 189
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()Landroid/support/v7/widget/RecyclerView$LayoutManager;

    move-result-object v2

    check-cast v2, Landroid/support/v7/widget/GridLayoutManager;

    .line 190
    .local v2, "layoutManager":Landroid/support/v7/widget/GridLayoutManager;
    invoke-virtual {v2}, Landroid/support/v7/widget/GridLayoutManager;->getSpanSizeLookup()Landroid/support/v7/widget/GridLayoutManager$SpanSizeLookup;

    move-result-object v4

    .line 191
    .local v4, "spanSizeLookup":Landroid/support/v7/widget/GridLayoutManager$SpanSizeLookup;
    invoke-virtual {v2}, Landroid/support/v7/widget/GridLayoutManager;->getSpanCount()I

    move-result v3

    .line 192
    .local v3, "spanCount":I
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getAdapter()Landroid/support/v7/widget/RecyclerView$Adapter;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v7/widget/RecyclerView$Adapter;->getItemCount()I

    move-result v1

    .line 193
    .local v1, "itemCount":I
    add-int/lit8 v0, v1, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_1

    .line 194
    invoke-virtual {v4, v0, v3}, Landroid/support/v7/widget/GridLayoutManager$SpanSizeLookup;->getSpanIndex(II)I

    move-result v5

    if-nez v5, :cond_0

    .line 195
    sub-int v5, v1, v0

    .line 200
    .end local v0    # "i":I
    .end local v1    # "itemCount":I
    .end local v2    # "layoutManager":Landroid/support/v7/widget/GridLayoutManager;
    .end local v3    # "spanCount":I
    .end local v4    # "spanSizeLookup":Landroid/support/v7/widget/GridLayoutManager$SpanSizeLookup;
    :goto_1
    return v5

    .line 193
    .restart local v0    # "i":I
    .restart local v1    # "itemCount":I
    .restart local v2    # "layoutManager":Landroid/support/v7/widget/GridLayoutManager;
    .restart local v3    # "spanCount":I
    .restart local v4    # "spanSizeLookup":Landroid/support/v7/widget/GridLayoutManager$SpanSizeLookup;
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 200
    .end local v0    # "i":I
    .end local v1    # "itemCount":I
    .end local v2    # "layoutManager":Landroid/support/v7/widget/GridLayoutManager;
    .end local v3    # "spanCount":I
    .end local v4    # "spanSizeLookup":Landroid/support/v7/widget/GridLayoutManager$SpanSizeLookup;
    :cond_1
    const/4 v5, 0x1

    goto :goto_1
.end method

.method private setSizeProvider(Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;)V
    .locals 1
    .param p1, "builder"    # Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;

    .prologue
    .line 76
    invoke-static {p1}, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;->access$400(Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$Builder;)Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$SizeProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration;->mSizeProvider:Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$SizeProvider;

    .line 77
    iget-object v0, p0, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration;->mSizeProvider:Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$SizeProvider;

    if-nez v0, :cond_0

    .line 78
    new-instance v0, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$2;

    invoke-direct {v0, p0}, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$2;-><init>(Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration;)V

    iput-object v0, p0, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration;->mSizeProvider:Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$SizeProvider;

    .line 85
    :cond_0
    return-void
.end method

.method private wasDividerAlreadyDrawn(ILandroid/support/v7/widget/RecyclerView;)Z
    .locals 5
    .param p1, "position"    # I
    .param p2, "parent"    # Landroid/support/v7/widget/RecyclerView;

    .prologue
    const/4 v3, 0x0

    .line 212
    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()Landroid/support/v7/widget/RecyclerView$LayoutManager;

    move-result-object v4

    instance-of v4, v4, Landroid/support/v7/widget/GridLayoutManager;

    if-eqz v4, :cond_0

    .line 213
    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()Landroid/support/v7/widget/RecyclerView$LayoutManager;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/GridLayoutManager;

    .line 214
    .local v0, "layoutManager":Landroid/support/v7/widget/GridLayoutManager;
    invoke-virtual {v0}, Landroid/support/v7/widget/GridLayoutManager;->getSpanSizeLookup()Landroid/support/v7/widget/GridLayoutManager$SpanSizeLookup;

    move-result-object v2

    .line 215
    .local v2, "spanSizeLookup":Landroid/support/v7/widget/GridLayoutManager$SpanSizeLookup;
    invoke-virtual {v0}, Landroid/support/v7/widget/GridLayoutManager;->getSpanCount()I

    move-result v1

    .line 216
    .local v1, "spanCount":I
    invoke-virtual {v2, p1, v1}, Landroid/support/v7/widget/GridLayoutManager$SpanSizeLookup;->getSpanIndex(II)I

    move-result v4

    if-lez v4, :cond_0

    const/4 v3, 0x1

    .line 219
    .end local v0    # "layoutManager":Landroid/support/v7/widget/GridLayoutManager;
    .end local v1    # "spanCount":I
    .end local v2    # "spanSizeLookup":Landroid/support/v7/widget/GridLayoutManager$SpanSizeLookup;
    :cond_0
    return v3
.end method


# virtual methods
.method protected abstract getDividerBound(ILandroid/support/v7/widget/RecyclerView;Landroid/view/View;)Landroid/graphics/Rect;
.end method

.method public getItemOffsets(Landroid/graphics/Rect;Landroid/view/View;Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/RecyclerView$State;)V
    .locals 5
    .param p1, "rect"    # Landroid/graphics/Rect;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/support/v7/widget/RecyclerView;
    .param p4, "state"    # Landroid/support/v7/widget/RecyclerView$State;

    .prologue
    .line 145
    invoke-virtual {p3, p2}, Landroid/support/v7/widget/RecyclerView;->getChildAdapterPosition(Landroid/view/View;)I

    move-result v3

    .line 146
    .local v3, "position":I
    invoke-virtual {p3}, Landroid/support/v7/widget/RecyclerView;->getAdapter()Landroid/support/v7/widget/RecyclerView$Adapter;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v7/widget/RecyclerView$Adapter;->getItemCount()I

    move-result v1

    .line 147
    .local v1, "itemCount":I
    invoke-direct {p0, p3}, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration;->getLastDividerOffset(Landroid/support/v7/widget/RecyclerView;)I

    move-result v2

    .line 148
    .local v2, "lastDividerOffset":I
    iget-boolean v4, p0, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration;->mShowLastDivider:Z

    if-nez v4, :cond_1

    sub-int v4, v1, v2

    if-lt v3, v4, :cond_1

    .line 159
    :cond_0
    :goto_0
    return-void

    .line 153
    :cond_1
    invoke-direct {p0, v3, p3}, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration;->getGroupIndex(ILandroid/support/v7/widget/RecyclerView;)I

    move-result v0

    .line 154
    .local v0, "groupIndex":I
    iget-object v4, p0, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration;->mVisibilityProvider:Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$VisibilityProvider;

    invoke-interface {v4, v0, p3}, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$VisibilityProvider;->shouldHideDivider(ILandroid/support/v7/widget/RecyclerView;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 158
    invoke-virtual {p0, p1, v0, p3}, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration;->setItemOffsets(Landroid/graphics/Rect;ILandroid/support/v7/widget/RecyclerView;)V

    goto :goto_0
.end method

.method protected isReverseLayout(Landroid/support/v7/widget/RecyclerView;)Z
    .locals 2
    .param p1, "parent"    # Landroid/support/v7/widget/RecyclerView;

    .prologue
    .line 168
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()Landroid/support/v7/widget/RecyclerView$LayoutManager;

    move-result-object v0

    .line 169
    .local v0, "layoutManager":Landroid/support/v7/widget/RecyclerView$LayoutManager;
    instance-of v1, v0, Landroid/support/v7/widget/LinearLayoutManager;

    if-eqz v1, :cond_0

    .line 170
    check-cast v0, Landroid/support/v7/widget/LinearLayoutManager;

    .end local v0    # "layoutManager":Landroid/support/v7/widget/RecyclerView$LayoutManager;
    invoke-virtual {v0}, Landroid/support/v7/widget/LinearLayoutManager;->getReverseLayout()Z

    move-result v1

    .line 172
    :goto_0
    return v1

    .restart local v0    # "layoutManager":Landroid/support/v7/widget/RecyclerView$LayoutManager;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onDraw(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/RecyclerView$State;)V
    .locals 19
    .param p1, "c"    # Landroid/graphics/Canvas;
    .param p2, "parent"    # Landroid/support/v7/widget/RecyclerView;
    .param p3, "state"    # Landroid/support/v7/widget/RecyclerView$State;

    .prologue
    .line 89
    invoke-virtual/range {p2 .. p2}, Landroid/support/v7/widget/RecyclerView;->getAdapter()Landroid/support/v7/widget/RecyclerView$Adapter;

    move-result-object v8

    .line 90
    .local v8, "adapter":Landroid/support/v7/widget/RecyclerView$Adapter;
    if-nez v8, :cond_1

    .line 141
    :cond_0
    return-void

    .line 94
    :cond_1
    invoke-virtual {v8}, Landroid/support/v7/widget/RecyclerView$Adapter;->getItemCount()I

    move-result v15

    .line 95
    .local v15, "itemCount":I
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration;->getLastDividerOffset(Landroid/support/v7/widget/RecyclerView;)I

    move-result v17

    .line 96
    .local v17, "lastDividerOffset":I
    invoke-virtual/range {p2 .. p2}, Landroid/support/v7/widget/RecyclerView;->getChildCount()I

    move-result v18

    .line 97
    .local v18, "validChildCount":I
    const/16 v16, -0x1

    .line 98
    .local v16, "lastChildPosition":I
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_0
    move/from16 v0, v18

    if-ge v14, v0, :cond_0

    .line 99
    move-object/from16 v0, p2

    invoke-virtual {v0, v14}, Landroid/support/v7/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v10

    .line 100
    .local v10, "child":Landroid/view/View;
    move-object/from16 v0, p2

    invoke-virtual {v0, v10}, Landroid/support/v7/widget/RecyclerView;->getChildAdapterPosition(Landroid/view/View;)I

    move-result v11

    .line 102
    .local v11, "childPosition":I
    move/from16 v0, v16

    if-ge v11, v0, :cond_3

    .line 98
    :cond_2
    :goto_1
    add-int/lit8 v14, v14, 0x1

    goto :goto_0

    .line 106
    :cond_3
    move/from16 v16, v11

    .line 108
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration;->mShowLastDivider:Z

    if-nez v2, :cond_4

    sub-int v2, v15, v17

    if-ge v11, v2, :cond_2

    .line 113
    :cond_4
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v11, v1}, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration;->wasDividerAlreadyDrawn(ILandroid/support/v7/widget/RecyclerView;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 118
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v11, v1}, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration;->getGroupIndex(ILandroid/support/v7/widget/RecyclerView;)I

    move-result v13

    .line 119
    .local v13, "groupIndex":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration;->mVisibilityProvider:Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$VisibilityProvider;

    move-object/from16 v0, p2

    invoke-interface {v2, v13, v0}, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$VisibilityProvider;->shouldHideDivider(ILandroid/support/v7/widget/RecyclerView;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 123
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v13, v1, v10}, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration;->getDividerBound(ILandroid/support/v7/widget/RecyclerView;Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v9

    .line 124
    .local v9, "bounds":Landroid/graphics/Rect;
    sget-object v2, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$3;->$SwitchMap$com$yqritc$recyclerviewflexibledivider$FlexibleDividerDecoration$DividerType:[I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration;->mDividerType:Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$DividerType;

    invoke-virtual {v3}, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$DividerType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    goto :goto_1

    .line 126
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration;->mDrawableProvider:Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$DrawableProvider;

    move-object/from16 v0, p2

    invoke-interface {v2, v13, v0}, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$DrawableProvider;->drawableProvider(ILandroid/support/v7/widget/RecyclerView;)Landroid/graphics/drawable/Drawable;

    move-result-object v12

    .line 127
    .local v12, "drawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v12, v9}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 128
    move-object/from16 v0, p1

    invoke-virtual {v12, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_1

    .line 131
    .end local v12    # "drawable":Landroid/graphics/drawable/Drawable;
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration;->mPaintProvider:Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$PaintProvider;

    move-object/from16 v0, p2

    invoke-interface {v2, v13, v0}, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$PaintProvider;->dividerPaint(ILandroid/support/v7/widget/RecyclerView;)Landroid/graphics/Paint;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration;->mPaint:Landroid/graphics/Paint;

    .line 132
    iget v2, v9, Landroid/graphics/Rect;->left:I

    int-to-float v3, v2

    iget v2, v9, Landroid/graphics/Rect;->top:I

    int-to-float v4, v2

    iget v2, v9, Landroid/graphics/Rect;->right:I

    int-to-float v5, v2

    iget v2, v9, Landroid/graphics/Rect;->bottom:I

    int-to-float v6, v2

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto :goto_1

    .line 135
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration;->mColorProvider:Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$ColorProvider;

    move-object/from16 v0, p2

    invoke-interface {v3, v13, v0}, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$ColorProvider;->dividerColor(ILandroid/support/v7/widget/RecyclerView;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 136
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration;->mSizeProvider:Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$SizeProvider;

    move-object/from16 v0, p2

    invoke-interface {v3, v13, v0}, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration$SizeProvider;->dividerSize(ILandroid/support/v7/widget/RecyclerView;)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 137
    iget v2, v9, Landroid/graphics/Rect;->left:I

    int-to-float v3, v2

    iget v2, v9, Landroid/graphics/Rect;->top:I

    int-to-float v4, v2

    iget v2, v9, Landroid/graphics/Rect;->right:I

    int-to-float v5, v2

    iget v2, v9, Landroid/graphics/Rect;->bottom:I

    int-to-float v6, v2

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/yqritc/recyclerviewflexibledivider/FlexibleDividerDecoration;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_1

    .line 124
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected abstract setItemOffsets(Landroid/graphics/Rect;ILandroid/support/v7/widget/RecyclerView;)V
.end method
