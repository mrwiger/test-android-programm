.class public abstract Lru/cn/tv/FullScreenActivity;
.super Landroid/support/v7/app/AppCompatActivity;
.source "FullScreenActivity.java"


# instance fields
.field private dv:Landroid/view/View;

.field private dvHideRunnable:Ljava/lang/Runnable;

.field private dvOnSystemUiVisibilityChangeListener:Landroid/view/View$OnSystemUiVisibilityChangeListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Landroid/support/v7/app/AppCompatActivity;-><init>()V

    .line 17
    new-instance v0, Lru/cn/tv/FullScreenActivity$1;

    invoke-direct {v0, p0}, Lru/cn/tv/FullScreenActivity$1;-><init>(Lru/cn/tv/FullScreenActivity;)V

    iput-object v0, p0, Lru/cn/tv/FullScreenActivity;->dvHideRunnable:Ljava/lang/Runnable;

    .line 30
    new-instance v0, Lru/cn/tv/FullScreenActivity$2;

    invoke-direct {v0, p0}, Lru/cn/tv/FullScreenActivity$2;-><init>(Lru/cn/tv/FullScreenActivity;)V

    iput-object v0, p0, Lru/cn/tv/FullScreenActivity;->dvOnSystemUiVisibilityChangeListener:Landroid/view/View$OnSystemUiVisibilityChangeListener;

    return-void
.end method

.method static synthetic access$000(Lru/cn/tv/FullScreenActivity;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/FullScreenActivity;

    .prologue
    .line 12
    iget-object v0, p0, Lru/cn/tv/FullScreenActivity;->dv:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$100(Lru/cn/tv/FullScreenActivity;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/FullScreenActivity;

    .prologue
    .line 12
    iget-object v0, p0, Lru/cn/tv/FullScreenActivity;->dvHideRunnable:Ljava/lang/Runnable;

    return-object v0
.end method


# virtual methods
.method public enableImmersive(Z)V
    .locals 4
    .param p1, "immersive"    # Z

    .prologue
    const/16 v3, 0x13

    const/16 v2, 0x10

    .line 89
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Lru/cn/tv/FullScreenActivity;->isFullScreen()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 90
    iget-object v1, p0, Lru/cn/tv/FullScreenActivity;->dv:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getSystemUiVisibility()I

    move-result v0

    .line 91
    .local v0, "flags":I
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v1, v3, :cond_0

    .line 92
    or-int/lit16 v0, v0, 0x800

    .line 95
    :cond_0
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v1, v2, :cond_1

    .line 96
    or-int/lit16 v0, v0, 0x200

    .line 97
    or-int/lit16 v0, v0, 0x400

    .line 98
    or-int/lit8 v0, v0, 0x4

    .line 101
    :cond_1
    or-int/lit8 v0, v0, 0x2

    .line 103
    iget-object v1, p0, Lru/cn/tv/FullScreenActivity;->dv:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 120
    :goto_0
    return-void

    .line 105
    .end local v0    # "flags":I
    :cond_2
    iget-object v1, p0, Lru/cn/tv/FullScreenActivity;->dv:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getSystemUiVisibility()I

    move-result v0

    .line 106
    .restart local v0    # "flags":I
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v1, v3, :cond_3

    .line 107
    and-int/lit16 v0, v0, -0x801

    .line 110
    :cond_3
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v1, v2, :cond_4

    .line 111
    and-int/lit16 v0, v0, -0x201

    .line 112
    and-int/lit16 v0, v0, -0x401

    .line 113
    and-int/lit8 v0, v0, -0x5

    .line 116
    :cond_4
    and-int/lit8 v0, v0, -0x3

    .line 118
    iget-object v1, p0, Lru/cn/tv/FullScreenActivity;->dv:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setSystemUiVisibility(I)V

    goto :goto_0
.end method

.method public fullScreen(Z)V
    .locals 5
    .param p1, "f"    # Z

    .prologue
    .line 56
    invoke-virtual {p0}, Lru/cn/tv/FullScreenActivity;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 58
    .local v1, "attrs":Landroid/view/WindowManager$LayoutParams;
    invoke-virtual {p0}, Lru/cn/tv/FullScreenActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    .line 59
    .local v0, "actionBar":Landroid/support/v7/app/ActionBar;
    if-eqz p1, :cond_1

    .line 60
    if-eqz v0, :cond_0

    .line 61
    invoke-virtual {v0}, Landroid/support/v7/app/ActionBar;->hide()V

    .line 64
    :cond_0
    iget v3, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/lit16 v3, v3, 0x400

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 66
    const/4 v2, 0x1

    .line 67
    .local v2, "v":I
    iget-object v3, p0, Lru/cn/tv/FullScreenActivity;->dv:Landroid/view/View;

    invoke-virtual {v3, v2}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 69
    iget-object v3, p0, Lru/cn/tv/FullScreenActivity;->dv:Landroid/view/View;

    iget-object v4, p0, Lru/cn/tv/FullScreenActivity;->dvOnSystemUiVisibilityChangeListener:Landroid/view/View$OnSystemUiVisibilityChangeListener;

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnSystemUiVisibilityChangeListener(Landroid/view/View$OnSystemUiVisibilityChangeListener;)V

    .line 70
    const-string v3, "FullScreenActivity"

    const-string v4, "Fullscreen mode"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    .end local v2    # "v":I
    :goto_0
    invoke-virtual {p0}, Lru/cn/tv/FullScreenActivity;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 81
    return-void

    .line 72
    :cond_1
    if-eqz v0, :cond_2

    .line 73
    invoke-virtual {v0}, Landroid/support/v7/app/ActionBar;->show()V

    .line 75
    :cond_2
    iget v3, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    and-int/lit16 v3, v3, -0x401

    iput v3, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 76
    iget-object v3, p0, Lru/cn/tv/FullScreenActivity;->dv:Landroid/view/View;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 77
    iget-object v3, p0, Lru/cn/tv/FullScreenActivity;->dv:Landroid/view/View;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnSystemUiVisibilityChangeListener(Landroid/view/View$OnSystemUiVisibilityChangeListener;)V

    .line 78
    const-string v3, "FullScreenActivity"

    const-string v4, "Window mode"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public isFullScreen()Z
    .locals 1

    .prologue
    .line 49
    invoke-virtual {p0}, Lru/cn/tv/FullScreenActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    and-int/lit16 v0, v0, 0x400

    if-eqz v0, :cond_0

    .line 50
    const/4 v0, 0x1

    .line 52
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 44
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onCreate(Landroid/os/Bundle;)V

    .line 45
    invoke-virtual {p0}, Lru/cn/tv/FullScreenActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lru/cn/tv/FullScreenActivity;->dv:Landroid/view/View;

    .line 46
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 124
    invoke-virtual {p0}, Lru/cn/tv/FullScreenActivity;->isFullScreen()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 125
    invoke-virtual {p0}, Lru/cn/tv/FullScreenActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 126
    .local v0, "attrs":Landroid/view/WindowManager$LayoutParams;
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/lit16 v1, v1, 0x400

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 129
    .end local v0    # "attrs":Landroid/view/WindowManager$LayoutParams;
    :cond_0
    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->onResume()V

    .line 130
    return-void
.end method

.method public final toggleFullScreen()Z
    .locals 1

    .prologue
    .line 84
    invoke-virtual {p0}, Lru/cn/tv/FullScreenActivity;->isFullScreen()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lru/cn/tv/FullScreenActivity;->fullScreen(Z)V

    .line 85
    invoke-virtual {p0}, Lru/cn/tv/FullScreenActivity;->isFullScreen()Z

    move-result v0

    return v0

    .line 84
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
