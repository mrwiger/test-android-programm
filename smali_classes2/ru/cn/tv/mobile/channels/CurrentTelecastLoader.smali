.class public abstract Lru/cn/tv/mobile/channels/CurrentTelecastLoader;
.super Ljava/lang/Object;
.source "CurrentTelecastLoader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/tv/mobile/channels/CurrentTelecastLoader$RequestLoader;,
        Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Request;,
        Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Data;
    }
.end annotation


# static fields
.field private static applicationContext:Landroid/content/Context;

.field private static data:Landroid/support/v4/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/LongSparseArray",
            "<",
            "Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Data;",
            ">;"
        }
    .end annotation
.end field

.field private static handler:Landroid/os/Handler;

.field private static final pendingRequests:Landroid/support/v4/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/LongSparseArray",
            "<",
            "Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Request;",
            ">;"
        }
    .end annotation
.end field

.field private static shouldDelayRequests:Z


# instance fields
.field private channelId:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 44
    new-instance v0, Landroid/support/v4/util/LongSparseArray;

    invoke-direct {v0}, Landroid/support/v4/util/LongSparseArray;-><init>()V

    sput-object v0, Lru/cn/tv/mobile/channels/CurrentTelecastLoader;->data:Landroid/support/v4/util/LongSparseArray;

    .line 47
    const/4 v0, 0x1

    sput-boolean v0, Lru/cn/tv/mobile/channels/CurrentTelecastLoader;->shouldDelayRequests:Z

    .line 48
    new-instance v0, Landroid/support/v4/util/LongSparseArray;

    invoke-direct {v0}, Landroid/support/v4/util/LongSparseArray;-><init>()V

    sput-object v0, Lru/cn/tv/mobile/channels/CurrentTelecastLoader;->pendingRequests:Landroid/support/v4/util/LongSparseArray;

    .line 52
    new-instance v0, Landroid/os/Handler;

    new-instance v1, Lru/cn/tv/mobile/channels/CurrentTelecastLoader$1;

    invoke-direct {v1}, Lru/cn/tv/mobile/channels/CurrentTelecastLoader$1;-><init>()V

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    sput-object v0, Lru/cn/tv/mobile/channels/CurrentTelecastLoader;->handler:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 135
    sget-object v0, Lru/cn/tv/mobile/channels/CurrentTelecastLoader;->applicationContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 136
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sput-object v0, Lru/cn/tv/mobile/channels/CurrentTelecastLoader;->applicationContext:Landroid/content/Context;

    .line 138
    :cond_0
    return-void
.end method

.method static synthetic access$000()Landroid/support/v4/util/LongSparseArray;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lru/cn/tv/mobile/channels/CurrentTelecastLoader;->pendingRequests:Landroid/support/v4/util/LongSparseArray;

    return-object v0
.end method

.method static synthetic access$102(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 16
    sput-boolean p0, Lru/cn/tv/mobile/channels/CurrentTelecastLoader;->shouldDelayRequests:Z

    return p0
.end method

.method static synthetic access$200()Landroid/content/Context;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lru/cn/tv/mobile/channels/CurrentTelecastLoader;->applicationContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$400()Landroid/support/v4/util/LongSparseArray;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lru/cn/tv/mobile/channels/CurrentTelecastLoader;->data:Landroid/support/v4/util/LongSparseArray;

    return-object v0
.end method

.method static synthetic access$500(Lru/cn/tv/mobile/channels/CurrentTelecastLoader;)J
    .locals 2
    .param p0, "x0"    # Lru/cn/tv/mobile/channels/CurrentTelecastLoader;

    .prologue
    .line 16
    iget-wide v0, p0, Lru/cn/tv/mobile/channels/CurrentTelecastLoader;->channelId:J

    return-wide v0
.end method


# virtual methods
.method public reset()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 141
    iget-wide v2, p0, Lru/cn/tv/mobile/channels/CurrentTelecastLoader;->channelId:J

    cmp-long v1, v2, v6

    if-lez v1, :cond_1

    .line 142
    sget-object v2, Lru/cn/tv/mobile/channels/CurrentTelecastLoader;->pendingRequests:Landroid/support/v4/util/LongSparseArray;

    monitor-enter v2

    .line 143
    :try_start_0
    sget-object v1, Lru/cn/tv/mobile/channels/CurrentTelecastLoader;->pendingRequests:Landroid/support/v4/util/LongSparseArray;

    iget-wide v4, p0, Lru/cn/tv/mobile/channels/CurrentTelecastLoader;->channelId:J

    invoke-virtual {v1, v4, v5}, Landroid/support/v4/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Request;

    .line 144
    .local v0, "request":Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Request;
    if-eqz v0, :cond_0

    .line 145
    const/4 v1, 0x0

    iput-object v1, v0, Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Request;->target:Lru/cn/tv/mobile/channels/CurrentTelecastLoader;

    .line 147
    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 149
    iput-wide v6, p0, Lru/cn/tv/mobile/channels/CurrentTelecastLoader;->channelId:J

    .line 151
    .end local v0    # "request":Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Request;
    :cond_1
    return-void

    .line 147
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public setChannelId(J)V
    .locals 11
    .param p1, "channelId"    # J

    .prologue
    const-wide/16 v2, 0x0

    .line 154
    iget-wide v0, p0, Lru/cn/tv/mobile/channels/CurrentTelecastLoader;->channelId:J

    cmp-long v0, v0, p1

    if-eqz v0, :cond_0

    .line 155
    iput-wide p1, p0, Lru/cn/tv/mobile/channels/CurrentTelecastLoader;->channelId:J

    .line 158
    :cond_0
    sget-object v0, Lru/cn/tv/mobile/channels/CurrentTelecastLoader;->data:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v0, p1, p2}, Landroid/support/v4/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Data;

    .line 159
    .local v6, "d":Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Data;
    if-eqz v6, :cond_1

    invoke-virtual {v6}, Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Data;->isOutdated()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 160
    :cond_1
    sget-object v1, Lru/cn/tv/mobile/channels/CurrentTelecastLoader;->pendingRequests:Landroid/support/v4/util/LongSparseArray;

    monitor-enter v1

    .line 161
    :try_start_0
    sget-object v0, Lru/cn/tv/mobile/channels/CurrentTelecastLoader;->pendingRequests:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v0, p1, p2}, Landroid/support/v4/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Request;

    .line 162
    .local v7, "request":Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Request;
    if-nez v7, :cond_2

    .line 163
    new-instance v7, Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Request;

    .end local v7    # "request":Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Request;
    const/4 v0, 0x0

    invoke-direct {v7, v0}, Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Request;-><init>(Lru/cn/tv/mobile/channels/CurrentTelecastLoader$1;)V

    .line 164
    .restart local v7    # "request":Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Request;
    iput-wide p1, v7, Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Request;->channelId:J

    .line 165
    sget-object v0, Lru/cn/tv/mobile/channels/CurrentTelecastLoader;->pendingRequests:Landroid/support/v4/util/LongSparseArray;

    invoke-virtual {v0, p1, p2, v7}, Landroid/support/v4/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 168
    :cond_2
    iput-object p0, v7, Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Request;->target:Lru/cn/tv/mobile/channels/CurrentTelecastLoader;

    .line 169
    sget-boolean v0, Lru/cn/tv/mobile/channels/CurrentTelecastLoader;->shouldDelayRequests:Z

    if-eqz v0, :cond_3

    .line 170
    const/4 v0, 0x0

    sput-boolean v0, Lru/cn/tv/mobile/channels/CurrentTelecastLoader;->shouldDelayRequests:Z

    .line 171
    sget-object v0, Lru/cn/tv/mobile/channels/CurrentTelecastLoader;->handler:Landroid/os/Handler;

    const/4 v4, 0x1

    const-wide/16 v8, 0x15e

    invoke-virtual {v0, v4, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 173
    :cond_3
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 175
    const-string v1, "..."

    move-object v0, p0

    move-wide v4, v2

    invoke-virtual/range {v0 .. v5}, Lru/cn/tv/mobile/channels/CurrentTelecastLoader;->telecastTitle(Ljava/lang/String;JJ)V

    .line 179
    .end local v7    # "request":Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Request;
    :goto_0
    return-void

    .line 173
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 177
    :cond_4
    iget-object v1, v6, Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Data;->title:Ljava/lang/String;

    iget-wide v2, v6, Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Data;->time:J

    iget-wide v4, v6, Lru/cn/tv/mobile/channels/CurrentTelecastLoader$Data;->duration:J

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lru/cn/tv/mobile/channels/CurrentTelecastLoader;->telecastTitle(Ljava/lang/String;JJ)V

    goto :goto_0
.end method

.method protected abstract telecastTitle(Ljava/lang/String;JJ)V
.end method
