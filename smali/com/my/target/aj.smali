.class public final Lcom/my/target/aj;
.super Lcom/my/target/ah;
.source "MediaBanner.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/my/target/ag;",
        ">",
        "Lcom/my/target/ah;"
    }
.end annotation


# static fields
.field public static final DEFAULT_ALLOW_BACK_BUTTON:Z = true

.field public static final DEFAULT_ALLOW_PAUSE:Z = true

.field public static final DEFAULT_ALLOW_REPLAY:Z = true

.field public static final DEFAULT_ALLOW_SEEK:Z = false

.field public static final DEFAULT_ALLOW_SKIP:Z = false

.field public static final DEFAULT_ALLOW_TRACK_CHANGE:Z = false

.field public static final DEFAULT_AUTO_PLAY:Z = true

.field public static final DEFAULT_HAS_CTA:Z = true

.field public static final DEFAULT_POINT_P:F = 50.0f


# instance fields
.field private adText:Ljava/lang/String;

.field private allowBackButton:Z

.field private allowClose:Z

.field private allowCloseDelay:F

.field private allowPause:Z

.field private allowReplay:Z

.field private allowSeek:Z

.field private allowSkip:Z

.field private allowTrackChange:Z

.field private autoMute:Z

.field private autoPlay:Z

.field private closeActionText:Ljava/lang/String;

.field private closeDelayActionText:Ljava/lang/String;

.field private final companionBanners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/my/target/ai;",
            ">;"
        }
    .end annotation
.end field

.field private hasCtaButton:Z

.field private mediaData:Lcom/my/target/ag;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private point:F

.field private pointP:F

.field private preview:Lcom/my/target/common/models/ImageData;

.field private replayActionText:Ljava/lang/String;

.field private showPlayerControls:Z


# direct methods
.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 65
    invoke-direct {p0}, Lcom/my/target/ah;-><init>()V

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/my/target/aj;->companionBanners:Ljava/util/ArrayList;

    .line 44
    const-string v0, "Close"

    iput-object v0, p0, Lcom/my/target/aj;->closeActionText:Ljava/lang/String;

    .line 45
    const-string v0, "Replay"

    iput-object v0, p0, Lcom/my/target/aj;->replayActionText:Ljava/lang/String;

    .line 46
    const-string v0, "Ad can be skipped after %ds"

    iput-object v0, p0, Lcom/my/target/aj;->closeDelayActionText:Ljava/lang/String;

    .line 49
    iput-boolean v2, p0, Lcom/my/target/aj;->autoMute:Z

    .line 50
    iput-boolean v1, p0, Lcom/my/target/aj;->showPlayerControls:Z

    .line 51
    iput-boolean v1, p0, Lcom/my/target/aj;->autoPlay:Z

    .line 52
    iput-boolean v1, p0, Lcom/my/target/aj;->hasCtaButton:Z

    .line 53
    iput-boolean v1, p0, Lcom/my/target/aj;->allowReplay:Z

    .line 54
    iput-boolean v1, p0, Lcom/my/target/aj;->allowClose:Z

    .line 55
    iput-boolean v2, p0, Lcom/my/target/aj;->allowSeek:Z

    .line 56
    iput-boolean v2, p0, Lcom/my/target/aj;->allowSkip:Z

    .line 57
    iput-boolean v2, p0, Lcom/my/target/aj;->allowTrackChange:Z

    .line 58
    iput-boolean v1, p0, Lcom/my/target/aj;->allowBackButton:Z

    .line 59
    iput-boolean v1, p0, Lcom/my/target/aj;->allowPause:Z

    .line 60
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/my/target/aj;->allowCloseDelay:F

    .line 67
    return-void
.end method

.method public static newAudioBanner()Lcom/my/target/aj;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/my/target/aj",
            "<",
            "Lcom/my/target/common/models/AudioData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 34
    invoke-static {}, Lcom/my/target/aj;->newBanner()Lcom/my/target/aj;

    move-result-object v0

    return-object v0
.end method

.method public static newBanner()Lcom/my/target/aj;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lcom/my/target/ag;",
            ">()",
            "Lcom/my/target/aj",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 39
    new-instance v0, Lcom/my/target/aj;

    invoke-direct {v0}, Lcom/my/target/aj;-><init>()V

    return-object v0
.end method

.method public static newVideoBanner()Lcom/my/target/aj;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/my/target/aj",
            "<",
            "Lcom/my/target/common/models/VideoData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 29
    invoke-static {}, Lcom/my/target/aj;->newBanner()Lcom/my/target/aj;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public addCompanion(Lcom/my/target/ai;)V
    .locals 1
    .param p1, "companionBanner"    # Lcom/my/target/ai;

    .prologue
    .line 258
    .local p0, "this":Lcom/my/target/aj;, "Lcom/my/target/aj<TT;>;"
    iget-object v0, p0, Lcom/my/target/aj;->companionBanners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 259
    return-void
.end method

.method public getAdText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lcom/my/target/aj;->adText:Ljava/lang/String;

    return-object v0
.end method

.method public getAllowCloseDelay()F
    .locals 1

    .prologue
    .line 166
    iget v0, p0, Lcom/my/target/aj;->allowCloseDelay:F

    return v0
.end method

.method public getCloseActionText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/my/target/aj;->closeActionText:Ljava/lang/String;

    return-object v0
.end method

.method public getCloseDelayActionText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 283
    iget-object v0, p0, Lcom/my/target/aj;->closeDelayActionText:Ljava/lang/String;

    return-object v0
.end method

.method public getCompanionBanners()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/my/target/ai;",
            ">;"
        }
    .end annotation

    .prologue
    .line 253
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/my/target/aj;->companionBanners:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcom/my/target/aj;->mediaData:Lcom/my/target/ag;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/my/target/aj;->mediaData:Lcom/my/target/ag;

    invoke-virtual {v0}, Lcom/my/target/ag;->getHeight()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getMediaData()Lcom/my/target/ag;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 111
    iget-object v0, p0, Lcom/my/target/aj;->mediaData:Lcom/my/target/ag;

    return-object v0
.end method

.method public getPoint()F
    .locals 1

    .prologue
    .line 181
    iget v0, p0, Lcom/my/target/aj;->point:F

    return v0
.end method

.method public getPointP()F
    .locals 1

    .prologue
    .line 186
    iget v0, p0, Lcom/my/target/aj;->pointP:F

    return v0
.end method

.method public getPreview()Lcom/my/target/common/models/ImageData;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/my/target/aj;->preview:Lcom/my/target/common/models/ImageData;

    return-object v0
.end method

.method public getReplayActionText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/my/target/aj;->replayActionText:Ljava/lang/String;

    return-object v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/my/target/aj;->mediaData:Lcom/my/target/ag;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/my/target/aj;->mediaData:Lcom/my/target/ag;

    invoke-virtual {v0}, Lcom/my/target/ag;->getWidth()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isAllowBackButton()Z
    .locals 1

    .prologue
    .line 263
    iget-boolean v0, p0, Lcom/my/target/aj;->allowBackButton:Z

    return v0
.end method

.method public isAllowClose()Z
    .locals 1

    .prologue
    .line 176
    iget-boolean v0, p0, Lcom/my/target/aj;->allowClose:Z

    return v0
.end method

.method public isAllowPause()Z
    .locals 1

    .prologue
    .line 273
    iget-boolean v0, p0, Lcom/my/target/aj;->allowPause:Z

    return v0
.end method

.method public isAllowReplay()Z
    .locals 1

    .prologue
    .line 151
    iget-boolean v0, p0, Lcom/my/target/aj;->allowReplay:Z

    return v0
.end method

.method public isAllowSeek()Z
    .locals 1

    .prologue
    .line 223
    iget-boolean v0, p0, Lcom/my/target/aj;->allowSeek:Z

    return v0
.end method

.method public isAllowSkip()Z
    .locals 1

    .prologue
    .line 233
    iget-boolean v0, p0, Lcom/my/target/aj;->allowSkip:Z

    return v0
.end method

.method public isAllowTrackChange()Z
    .locals 1

    .prologue
    .line 243
    iget-boolean v0, p0, Lcom/my/target/aj;->allowTrackChange:Z

    return v0
.end method

.method public isAutoMute()Z
    .locals 1

    .prologue
    .line 126
    iget-boolean v0, p0, Lcom/my/target/aj;->autoMute:Z

    return v0
.end method

.method public isAutoPlay()Z
    .locals 1

    .prologue
    .line 136
    iget-boolean v0, p0, Lcom/my/target/aj;->autoPlay:Z

    return v0
.end method

.method public isHasCtaButton()Z
    .locals 1

    .prologue
    .line 141
    iget-boolean v0, p0, Lcom/my/target/aj;->hasCtaButton:Z

    return v0
.end method

.method public isShowPlayerControls()Z
    .locals 1

    .prologue
    .line 131
    iget-boolean v0, p0, Lcom/my/target/aj;->showPlayerControls:Z

    return v0
.end method

.method public setAdText(Ljava/lang/String;)V
    .locals 0
    .param p1, "adText"    # Ljava/lang/String;

    .prologue
    .line 218
    .local p0, "this":Lcom/my/target/aj;, "Lcom/my/target/aj<TT;>;"
    iput-object p1, p0, Lcom/my/target/aj;->adText:Ljava/lang/String;

    .line 219
    return-void
.end method

.method public setAllowBackButton(Z)V
    .locals 0
    .param p1, "allowBackButton"    # Z

    .prologue
    .line 268
    .local p0, "this":Lcom/my/target/aj;, "Lcom/my/target/aj<TT;>;"
    iput-boolean p1, p0, Lcom/my/target/aj;->allowBackButton:Z

    .line 269
    return-void
.end method

.method public setAllowClose(Z)V
    .locals 0
    .param p1, "allowClose"    # Z

    .prologue
    .line 171
    .local p0, "this":Lcom/my/target/aj;, "Lcom/my/target/aj<TT;>;"
    iput-boolean p1, p0, Lcom/my/target/aj;->allowClose:Z

    .line 172
    return-void
.end method

.method public setAllowCloseDelay(F)V
    .locals 0
    .param p1, "allowCloseDelay"    # F

    .prologue
    .line 161
    .local p0, "this":Lcom/my/target/aj;, "Lcom/my/target/aj<TT;>;"
    iput p1, p0, Lcom/my/target/aj;->allowCloseDelay:F

    .line 162
    return-void
.end method

.method public setAllowPause(Z)V
    .locals 0
    .param p1, "allowPause"    # Z

    .prologue
    .line 278
    .local p0, "this":Lcom/my/target/aj;, "Lcom/my/target/aj<TT;>;"
    iput-boolean p1, p0, Lcom/my/target/aj;->allowPause:Z

    .line 279
    return-void
.end method

.method public setAllowReplay(Z)V
    .locals 0
    .param p1, "allowReplay"    # Z

    .prologue
    .line 156
    .local p0, "this":Lcom/my/target/aj;, "Lcom/my/target/aj<TT;>;"
    iput-boolean p1, p0, Lcom/my/target/aj;->allowReplay:Z

    .line 157
    return-void
.end method

.method public setAllowSeek(Z)V
    .locals 0
    .param p1, "allowSeek"    # Z

    .prologue
    .line 228
    .local p0, "this":Lcom/my/target/aj;, "Lcom/my/target/aj<TT;>;"
    iput-boolean p1, p0, Lcom/my/target/aj;->allowSeek:Z

    .line 229
    return-void
.end method

.method public setAllowSkip(Z)V
    .locals 0
    .param p1, "allowSkip"    # Z

    .prologue
    .line 238
    .local p0, "this":Lcom/my/target/aj;, "Lcom/my/target/aj<TT;>;"
    iput-boolean p1, p0, Lcom/my/target/aj;->allowSkip:Z

    .line 239
    return-void
.end method

.method public setAllowTrackChange(Z)V
    .locals 0
    .param p1, "allowTrackChange"    # Z

    .prologue
    .line 248
    .local p0, "this":Lcom/my/target/aj;, "Lcom/my/target/aj<TT;>;"
    iput-boolean p1, p0, Lcom/my/target/aj;->allowTrackChange:Z

    .line 249
    return-void
.end method

.method public setAutoMute(Z)V
    .locals 0
    .param p1, "autoMute"    # Z

    .prologue
    .line 71
    .local p0, "this":Lcom/my/target/aj;, "Lcom/my/target/aj<TT;>;"
    iput-boolean p1, p0, Lcom/my/target/aj;->autoMute:Z

    .line 72
    return-void
.end method

.method public setAutoPlay(Z)V
    .locals 0
    .param p1, "autoPlay"    # Z

    .prologue
    .line 76
    .local p0, "this":Lcom/my/target/aj;, "Lcom/my/target/aj<TT;>;"
    iput-boolean p1, p0, Lcom/my/target/aj;->autoPlay:Z

    .line 77
    return-void
.end method

.method public setCloseActionText(Ljava/lang/String;)V
    .locals 0
    .param p1, "closeActionText"    # Ljava/lang/String;

    .prologue
    .line 81
    .local p0, "this":Lcom/my/target/aj;, "Lcom/my/target/aj<TT;>;"
    iput-object p1, p0, Lcom/my/target/aj;->closeActionText:Ljava/lang/String;

    .line 82
    return-void
.end method

.method public setCloseDelayActionText(Ljava/lang/String;)V
    .locals 0
    .param p1, "closeDelayActionText"    # Ljava/lang/String;

    .prologue
    .line 288
    .local p0, "this":Lcom/my/target/aj;, "Lcom/my/target/aj<TT;>;"
    iput-object p1, p0, Lcom/my/target/aj;->closeDelayActionText:Ljava/lang/String;

    .line 289
    return-void
.end method

.method public setHasCtaButton(Z)V
    .locals 0
    .param p1, "hasCtaButton"    # Z

    .prologue
    .line 86
    .local p0, "this":Lcom/my/target/aj;, "Lcom/my/target/aj<TT;>;"
    iput-boolean p1, p0, Lcom/my/target/aj;->hasCtaButton:Z

    .line 87
    return-void
.end method

.method public setMediaData(Lcom/my/target/ag;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 106
    .local p0, "this":Lcom/my/target/aj;, "Lcom/my/target/aj<TT;>;"
    .local p1, "data":Lcom/my/target/ag;, "TT;"
    iput-object p1, p0, Lcom/my/target/aj;->mediaData:Lcom/my/target/ag;

    .line 107
    return-void
.end method

.method public setPoint(F)V
    .locals 0
    .param p1, "point"    # F

    .prologue
    .line 191
    .local p0, "this":Lcom/my/target/aj;, "Lcom/my/target/aj<TT;>;"
    iput p1, p0, Lcom/my/target/aj;->point:F

    .line 192
    return-void
.end method

.method public setPointP(F)V
    .locals 0
    .param p1, "pointP"    # F

    .prologue
    .line 196
    .local p0, "this":Lcom/my/target/aj;, "Lcom/my/target/aj<TT;>;"
    iput p1, p0, Lcom/my/target/aj;->pointP:F

    .line 197
    return-void
.end method

.method public setPreview(Lcom/my/target/common/models/ImageData;)V
    .locals 0
    .param p1, "preview"    # Lcom/my/target/common/models/ImageData;

    .prologue
    .line 91
    .local p0, "this":Lcom/my/target/aj;, "Lcom/my/target/aj<TT;>;"
    iput-object p1, p0, Lcom/my/target/aj;->preview:Lcom/my/target/common/models/ImageData;

    .line 92
    return-void
.end method

.method public setReplayActionText(Ljava/lang/String;)V
    .locals 0
    .param p1, "replayActionText"    # Ljava/lang/String;

    .prologue
    .line 96
    .local p0, "this":Lcom/my/target/aj;, "Lcom/my/target/aj<TT;>;"
    iput-object p1, p0, Lcom/my/target/aj;->replayActionText:Ljava/lang/String;

    .line 97
    return-void
.end method

.method public setShowPlayerControls(Z)V
    .locals 0
    .param p1, "showPlayerControls"    # Z

    .prologue
    .line 101
    .local p0, "this":Lcom/my/target/aj;, "Lcom/my/target/aj<TT;>;"
    iput-boolean p1, p0, Lcom/my/target/aj;->showPlayerControls:Z

    .line 102
    return-void
.end method
