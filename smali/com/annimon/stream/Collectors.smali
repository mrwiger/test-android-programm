.class public final Lcom/annimon/stream/Collectors;
.super Ljava/lang/Object;
.source "Collectors.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/annimon/stream/Collectors$CollectorsImpl;
    }
.end annotation


# static fields
.field private static final DOUBLE_2ELEMENTS_ARRAY_SUPPLIER:Lcom/annimon/stream/function/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/annimon/stream/function/Supplier",
            "<[D>;"
        }
    .end annotation
.end field

.field private static final LONG_2ELEMENTS_ARRAY_SUPPLIER:Lcom/annimon/stream/function/Supplier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/annimon/stream/function/Supplier",
            "<[J>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    new-instance v0, Lcom/annimon/stream/Collectors$1;

    invoke-direct {v0}, Lcom/annimon/stream/Collectors$1;-><init>()V

    sput-object v0, Lcom/annimon/stream/Collectors;->LONG_2ELEMENTS_ARRAY_SUPPLIER:Lcom/annimon/stream/function/Supplier;

    .line 27
    new-instance v0, Lcom/annimon/stream/Collectors$2;

    invoke-direct {v0}, Lcom/annimon/stream/Collectors$2;-><init>()V

    sput-object v0, Lcom/annimon/stream/Collectors;->DOUBLE_2ELEMENTS_ARRAY_SUPPLIER:Lcom/annimon/stream/function/Supplier;

    return-void
.end method

.method static castIdentity()Lcom/annimon/stream/function/Function;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<A:",
            "Ljava/lang/Object;",
            "R:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/annimon/stream/function/Function",
            "<TA;TR;>;"
        }
    .end annotation

    .prologue
    .line 841
    new-instance v0, Lcom/annimon/stream/Collectors$41;

    invoke-direct {v0}, Lcom/annimon/stream/Collectors$41;-><init>()V

    return-object v0
.end method

.method private static hashMapSupplier()Lcom/annimon/stream/function/Supplier;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/annimon/stream/function/Supplier",
            "<",
            "Ljava/util/Map",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 830
    new-instance v0, Lcom/annimon/stream/Collectors$40;

    invoke-direct {v0}, Lcom/annimon/stream/Collectors$40;-><init>()V

    return-object v0
.end method

.method public static joining(Ljava/lang/CharSequence;)Lcom/annimon/stream/Collector;
    .locals 2
    .param p0, "delimiter"    # Ljava/lang/CharSequence;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            ")",
            "Lcom/annimon/stream/Collector",
            "<",
            "Ljava/lang/CharSequence;",
            "*",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 193
    const-string v0, ""

    const-string v1, ""

    invoke-static {p0, v0, v1}, Lcom/annimon/stream/Collectors;->joining(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Lcom/annimon/stream/Collector;

    move-result-object v0

    return-object v0
.end method

.method public static joining(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Lcom/annimon/stream/Collector;
    .locals 2
    .param p0, "delimiter"    # Ljava/lang/CharSequence;
    .param p1, "prefix"    # Ljava/lang/CharSequence;
    .param p2, "suffix"    # Ljava/lang/CharSequence;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/CharSequence;",
            ")",
            "Lcom/annimon/stream/Collector",
            "<",
            "Ljava/lang/CharSequence;",
            "*",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 205
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, p1, p2, v0}, Lcom/annimon/stream/Collectors;->joining(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/String;)Lcom/annimon/stream/Collector;

    move-result-object v0

    return-object v0
.end method

.method public static joining(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/String;)Lcom/annimon/stream/Collector;
    .locals 4
    .param p0, "delimiter"    # Ljava/lang/CharSequence;
    .param p1, "prefix"    # Ljava/lang/CharSequence;
    .param p2, "suffix"    # Ljava/lang/CharSequence;
    .param p3, "emptyValue"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/String;",
            ")",
            "Lcom/annimon/stream/Collector",
            "<",
            "Ljava/lang/CharSequence;",
            "*",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 222
    new-instance v0, Lcom/annimon/stream/Collectors$CollectorsImpl;

    new-instance v1, Lcom/annimon/stream/Collectors$9;

    invoke-direct {v1}, Lcom/annimon/stream/Collectors$9;-><init>()V

    new-instance v2, Lcom/annimon/stream/Collectors$10;

    invoke-direct {v2, p0, p1}, Lcom/annimon/stream/Collectors$10;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    new-instance v3, Lcom/annimon/stream/Collectors$11;

    invoke-direct {v3, p3, p2}, Lcom/annimon/stream/Collectors$11;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;)V

    invoke-direct {v0, v1, v2, v3}, Lcom/annimon/stream/Collectors$CollectorsImpl;-><init>(Lcom/annimon/stream/function/Supplier;Lcom/annimon/stream/function/BiConsumer;Lcom/annimon/stream/function/Function;)V

    return-object v0
.end method

.method public static toMap(Lcom/annimon/stream/function/Function;Lcom/annimon/stream/function/Function;)Lcom/annimon/stream/Collector;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/annimon/stream/function/Function",
            "<-TT;+TK;>;",
            "Lcom/annimon/stream/function/Function",
            "<-TT;+TV;>;)",
            "Lcom/annimon/stream/Collector",
            "<TT;*",
            "Ljava/util/Map",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 136
    .line 137
    .local p0, "keyMapper":Lcom/annimon/stream/function/Function;, "Lcom/annimon/stream/function/Function<-TT;+TK;>;"
    .local p1, "valueMapper":Lcom/annimon/stream/function/Function;, "Lcom/annimon/stream/function/Function<-TT;+TV;>;"
    invoke-static {}, Lcom/annimon/stream/Collectors;->hashMapSupplier()Lcom/annimon/stream/function/Supplier;

    move-result-object v0

    .line 136
    invoke-static {p0, p1, v0}, Lcom/annimon/stream/Collectors;->toMap(Lcom/annimon/stream/function/Function;Lcom/annimon/stream/function/Function;Lcom/annimon/stream/function/Supplier;)Lcom/annimon/stream/Collector;

    move-result-object v0

    return-object v0
.end method

.method public static toMap(Lcom/annimon/stream/function/Function;Lcom/annimon/stream/function/Function;Lcom/annimon/stream/function/Supplier;)Lcom/annimon/stream/Collector;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "K:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            "M::",
            "Ljava/util/Map",
            "<TK;TV;>;>(",
            "Lcom/annimon/stream/function/Function",
            "<-TT;+TK;>;",
            "Lcom/annimon/stream/function/Function",
            "<-TT;+TV;>;",
            "Lcom/annimon/stream/function/Supplier",
            "<TM;>;)",
            "Lcom/annimon/stream/Collector",
            "<TT;*TM;>;"
        }
    .end annotation

    .prologue
    .line 156
    .local p0, "keyMapper":Lcom/annimon/stream/function/Function;, "Lcom/annimon/stream/function/Function<-TT;+TK;>;"
    .local p1, "valueMapper":Lcom/annimon/stream/function/Function;, "Lcom/annimon/stream/function/Function<-TT;+TV;>;"
    .local p2, "mapFactory":Lcom/annimon/stream/function/Supplier;, "Lcom/annimon/stream/function/Supplier<TM;>;"
    new-instance v0, Lcom/annimon/stream/Collectors$CollectorsImpl;

    new-instance v1, Lcom/annimon/stream/Collectors$8;

    invoke-direct {v1, p0, p1}, Lcom/annimon/stream/Collectors$8;-><init>(Lcom/annimon/stream/function/Function;Lcom/annimon/stream/function/Function;)V

    invoke-direct {v0, p2, v1}, Lcom/annimon/stream/Collectors$CollectorsImpl;-><init>(Lcom/annimon/stream/function/Supplier;Lcom/annimon/stream/function/BiConsumer;)V

    return-object v0
.end method
