.class Lru/cn/tv/player/controller/PlayerController$RepeatedTouchListener;
.super Ljava/lang/Object;
.source "PlayerController.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/tv/player/controller/PlayerController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "RepeatedTouchListener"
.end annotation


# instance fields
.field mAction:Ljava/lang/Runnable;

.field private mHandler:Landroid/os/Handler;

.field private repeatTimeout:J

.field private view:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;J)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "timeout"    # J

    .prologue
    .line 224
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 254
    new-instance v0, Lru/cn/tv/player/controller/PlayerController$RepeatedTouchListener$1;

    invoke-direct {v0, p0}, Lru/cn/tv/player/controller/PlayerController$RepeatedTouchListener$1;-><init>(Lru/cn/tv/player/controller/PlayerController$RepeatedTouchListener;)V

    iput-object v0, p0, Lru/cn/tv/player/controller/PlayerController$RepeatedTouchListener;->mAction:Ljava/lang/Runnable;

    .line 225
    iput-wide p2, p0, Lru/cn/tv/player/controller/PlayerController$RepeatedTouchListener;->repeatTimeout:J

    .line 226
    iput-object p1, p0, Lru/cn/tv/player/controller/PlayerController$RepeatedTouchListener;->view:Landroid/view/View;

    .line 227
    return-void
.end method

.method static synthetic access$000(Lru/cn/tv/player/controller/PlayerController$RepeatedTouchListener;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/player/controller/PlayerController$RepeatedTouchListener;

    .prologue
    .line 219
    iget-object v0, p0, Lru/cn/tv/player/controller/PlayerController$RepeatedTouchListener;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$100(Lru/cn/tv/player/controller/PlayerController$RepeatedTouchListener;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/player/controller/PlayerController$RepeatedTouchListener;

    .prologue
    .line 219
    iget-object v0, p0, Lru/cn/tv/player/controller/PlayerController$RepeatedTouchListener;->view:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$200(Lru/cn/tv/player/controller/PlayerController$RepeatedTouchListener;)J
    .locals 2
    .param p0, "x0"    # Lru/cn/tv/player/controller/PlayerController$RepeatedTouchListener;

    .prologue
    .line 219
    iget-wide v0, p0, Lru/cn/tv/player/controller/PlayerController$RepeatedTouchListener;->repeatTimeout:J

    return-wide v0
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x1

    .line 231
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 251
    :goto_0
    :pswitch_0
    const/4 v0, 0x0

    :cond_0
    return v0

    .line 233
    :pswitch_1
    iget-object v1, p0, Lru/cn/tv/player/controller/PlayerController$RepeatedTouchListener;->mHandler:Landroid/os/Handler;

    if-nez v1, :cond_0

    .line 236
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lru/cn/tv/player/controller/PlayerController$RepeatedTouchListener;->mHandler:Landroid/os/Handler;

    .line 237
    iget-object v0, p0, Lru/cn/tv/player/controller/PlayerController$RepeatedTouchListener;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lru/cn/tv/player/controller/PlayerController$RepeatedTouchListener;->mAction:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 240
    :pswitch_2
    iget-object v1, p0, Lru/cn/tv/player/controller/PlayerController$RepeatedTouchListener;->mHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 243
    iget-object v0, p0, Lru/cn/tv/player/controller/PlayerController$RepeatedTouchListener;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lru/cn/tv/player/controller/PlayerController$RepeatedTouchListener;->mAction:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 244
    iput-object v2, p0, Lru/cn/tv/player/controller/PlayerController$RepeatedTouchListener;->mHandler:Landroid/os/Handler;

    goto :goto_0

    .line 247
    :pswitch_3
    iget-object v0, p0, Lru/cn/tv/player/controller/PlayerController$RepeatedTouchListener;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lru/cn/tv/player/controller/PlayerController$RepeatedTouchListener;->mAction:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 248
    iput-object v2, p0, Lru/cn/tv/player/controller/PlayerController$RepeatedTouchListener;->mHandler:Landroid/os/Handler;

    goto :goto_0

    .line 231
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method protected touched()V
    .locals 0

    .prologue
    .line 266
    return-void
.end method
