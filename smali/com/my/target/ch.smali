.class public Lcom/my/target/ch;
.super Ljava/lang/Object;
.source "ImageLoader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/ch$a;
    }
.end annotation


# static fields
.field private static final kd:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Landroid/widget/ImageView;",
            "Lcom/my/target/common/models/ImageData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final ke:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/my/target/common/models/ImageData;",
            ">;"
        }
    .end annotation
.end field

.field private kf:Lcom/my/target/ch$a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    sput-object v0, Lcom/my/target/ch;->kd:Ljava/util/WeakHashMap;

    return-void
.end method

.method private constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/my/target/common/models/ImageData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 127
    iput-object p1, p0, Lcom/my/target/ch;->ke:Ljava/util/List;

    .line 128
    return-void
.end method

.method static synthetic a(Lcom/my/target/ch;Lcom/my/target/ch$a;)Lcom/my/target/ch$a;
    .locals 0

    .prologue
    .line 30
    iput-object p1, p0, Lcom/my/target/ch;->kf:Lcom/my/target/ch$a;

    return-object p1
.end method

.method public static a(Lcom/my/target/common/models/ImageData;)Lcom/my/target/ch;
    .locals 2

    .prologue
    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 37
    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 38
    new-instance v1, Lcom/my/target/ch;

    invoke-direct {v1, v0}, Lcom/my/target/ch;-><init>(Ljava/util/List;)V

    return-object v1
.end method

.method public static a(Ljava/util/List;)Lcom/my/target/ch;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/my/target/common/models/ImageData;",
            ">;)",
            "Lcom/my/target/ch;"
        }
    .end annotation

    .prologue
    .line 43
    new-instance v0, Lcom/my/target/ch;

    invoke-direct {v0, p0}, Lcom/my/target/ch;-><init>(Ljava/util/List;)V

    return-object v0
.end method

.method private static a(Landroid/graphics/Bitmap;Landroid/widget/ImageView;)V
    .locals 1

    .prologue
    .line 106
    instance-of v0, p1, Lcom/my/target/bv;

    if-eqz v0, :cond_0

    .line 108
    check-cast p1, Lcom/my/target/bv;

    const/4 v0, 0x1

    invoke-virtual {p1, p0, v0}, Lcom/my/target/bv;->a(Landroid/graphics/Bitmap;Z)V

    .line 114
    :goto_0
    return-void

    .line 112
    :cond_0
    invoke-virtual {p1, p0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/my/target/ch;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/my/target/ch;->finish()V

    return-void
.end method

.method public static a(Lcom/my/target/common/models/ImageData;Landroid/widget/ImageView;)V
    .locals 3

    .prologue
    .line 49
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-eq v0, v1, :cond_1

    .line 51
    const-string v0, "[ImageLoader] method loadAndDisplay called from worker thread"

    invoke-static {v0}, Lcom/my/target/g;->b(Ljava/lang/String;)V

    .line 88
    :cond_0
    :goto_0
    return-void

    .line 55
    :cond_1
    sget-object v0, Lcom/my/target/ch;->kd:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eq v0, p0, :cond_0

    .line 59
    sget-object v0, Lcom/my/target/ch;->kd:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    invoke-virtual {p0}, Lcom/my/target/common/models/ImageData;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 62
    invoke-virtual {p0}, Lcom/my/target/common/models/ImageData;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/my/target/ch;->a(Landroid/graphics/Bitmap;Landroid/widget/ImageView;)V

    goto :goto_0

    .line 65
    :cond_2
    sget-object v0, Lcom/my/target/ch;->kd:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1, p0}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 67
    invoke-static {p0}, Lcom/my/target/ch;->a(Lcom/my/target/common/models/ImageData;)Lcom/my/target/ch;

    move-result-object v1

    new-instance v2, Lcom/my/target/ch$1;

    invoke-direct {v2, v0, p0}, Lcom/my/target/ch$1;-><init>(Ljava/lang/ref/WeakReference;Lcom/my/target/common/models/ImageData;)V

    invoke-virtual {v1, v2}, Lcom/my/target/ch;->a(Lcom/my/target/ch$a;)Lcom/my/target/ch;

    move-result-object v0

    .line 87
    invoke-virtual {p1}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/my/target/ch;->u(Landroid/content/Context;)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/my/target/ch;)Lcom/my/target/ch$a;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/my/target/ch;->kf:Lcom/my/target/ch$a;

    return-object v0
.end method

.method static synthetic b(Landroid/graphics/Bitmap;Landroid/widget/ImageView;)V
    .locals 0

    .prologue
    .line 30
    invoke-static {p0, p1}, Lcom/my/target/ch;->a(Landroid/graphics/Bitmap;Landroid/widget/ImageView;)V

    return-void
.end method

.method public static b(Lcom/my/target/common/models/ImageData;Landroid/widget/ImageView;)V
    .locals 2

    .prologue
    .line 93
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-eq v0, v1, :cond_1

    .line 95
    const-string v0, "[ImageLoader] method cancel called from worker thread"

    invoke-static {v0}, Lcom/my/target/g;->b(Ljava/lang/String;)V

    .line 102
    :cond_0
    :goto_0
    return-void

    .line 98
    :cond_1
    sget-object v0, Lcom/my/target/ch;->kd:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p0, :cond_0

    .line 100
    sget-object v0, Lcom/my/target/ch;->kd:Ljava/util/WeakHashMap;

    invoke-virtual {v0, p1}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method static synthetic bz()Ljava/util/WeakHashMap;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/my/target/ch;->kd:Ljava/util/WeakHashMap;

    return-object v0
.end method

.method private finish()V
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lcom/my/target/ch;->kf:Lcom/my/target/ch$a;

    if-nez v0, :cond_0

    .line 196
    :goto_0
    return-void

    .line 184
    :cond_0
    new-instance v0, Lcom/my/target/ch$3;

    invoke-direct {v0, p0}, Lcom/my/target/ch$3;-><init>(Lcom/my/target/ch;)V

    invoke-static {v0}, Lcom/my/target/h;->c(Ljava/lang/Runnable;)V

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/my/target/ch$a;)Lcom/my/target/ch;
    .locals 0

    .prologue
    .line 121
    iput-object p1, p0, Lcom/my/target/ch;->kf:Lcom/my/target/ch$a;

    .line 122
    return-object p0
.end method

.method public u(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 132
    iget-object v0, p0, Lcom/my/target/ch;->ke:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 134
    invoke-direct {p0}, Lcom/my/target/ch;->finish()V

    .line 147
    :goto_0
    return-void

    .line 137
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 138
    new-instance v1, Lcom/my/target/ch$2;

    invoke-direct {v1, p0, v0}, Lcom/my/target/ch$2;-><init>(Lcom/my/target/ch;Landroid/content/Context;)V

    invoke-static {v1}, Lcom/my/target/h;->a(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public v(Landroid/content/Context;)V
    .locals 6

    .prologue
    .line 152
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_1

    .line 154
    const-string v0, "[ImageLoader] method loadSync called from main thread"

    invoke-static {v0}, Lcom/my/target/g;->b(Ljava/lang/String;)V

    .line 176
    :cond_0
    return-void

    .line 158
    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 159
    invoke-static {}, Lcom/my/target/au;->ai()Lcom/my/target/au;

    move-result-object v3

    .line 160
    iget-object v0, p0, Lcom/my/target/ch;->ke:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/common/models/ImageData;

    .line 162
    invoke-virtual {v0}, Lcom/my/target/common/models/ImageData;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    if-nez v1, :cond_2

    .line 164
    invoke-virtual {v0}, Lcom/my/target/common/models/ImageData;->getUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1, v2}, Lcom/my/target/au;->f(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    .line 165
    if-eqz v1, :cond_2

    .line 167
    invoke-virtual {v0, v1}, Lcom/my/target/common/models/ImageData;->setData(Landroid/graphics/Bitmap;)V

    .line 168
    invoke-virtual {v0}, Lcom/my/target/common/models/ImageData;->getHeight()I

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {v0}, Lcom/my/target/common/models/ImageData;->getWidth()I

    move-result v5

    if-nez v5, :cond_2

    .line 170
    :cond_3
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    invoke-virtual {v0, v5}, Lcom/my/target/common/models/ImageData;->setHeight(I)V

    .line 171
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/my/target/common/models/ImageData;->setWidth(I)V

    goto :goto_0
.end method
