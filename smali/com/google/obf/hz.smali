.class public Lcom/google/obf/hz;
.super Landroid/widget/RelativeLayout;
.source "IMASDK"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/obf/hj$e;
.implements Lcom/google/obf/hn$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/obf/hz$b;,
        Lcom/google/obf/hz$a;
    }
.end annotation


# instance fields
.field private a:Landroid/widget/FrameLayout;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/obf/hz$a;",
            ">;"
        }
    .end annotation
.end field

.field private final c:F

.field private final d:Ljava/lang/String;

.field private e:Lcom/google/obf/hj;

.field private f:Z

.field private g:F

.field private h:Ljava/lang/String;

.field private i:Lcom/google/obf/hz$b;

.field private j:Lcom/google/obf/hw;

.field private k:Lcom/google/obf/hy;

.field private l:Lcom/google/obf/hv;

.field private m:I

.field private n:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/obf/hy;Lcom/google/obf/hj;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 1
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/google/obf/hz;-><init>(Landroid/content/Context;Lcom/google/obf/hy;Lcom/google/obf/hj;Ljava/lang/String;Lcom/google/obf/hv;Lcom/google/obf/hw;)V

    .line 2
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lcom/google/obf/hy;Lcom/google/obf/hj;Ljava/lang/String;Lcom/google/obf/hv;Lcom/google/obf/hw;)V
    .locals 1

    .prologue
    .line 3
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 4
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/obf/hz;->b:Ljava/util/List;

    .line 5
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/obf/hz;->f:Z

    .line 6
    iput-object p3, p0, Lcom/google/obf/hz;->e:Lcom/google/obf/hj;

    .line 7
    iput-object p4, p0, Lcom/google/obf/hz;->d:Ljava/lang/String;

    .line 8
    iput-object p2, p0, Lcom/google/obf/hz;->k:Lcom/google/obf/hy;

    .line 9
    iput-object p5, p0, Lcom/google/obf/hz;->l:Lcom/google/obf/hv;

    .line 10
    iput-object p6, p0, Lcom/google/obf/hz;->j:Lcom/google/obf/hw;

    .line 11
    invoke-virtual {p0}, Lcom/google/obf/hz;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lcom/google/obf/hz;->c:F

    .line 12
    invoke-direct {p0, p1}, Lcom/google/obf/hz;->d(Landroid/content/Context;)V

    .line 13
    iget-boolean v0, p2, Lcom/google/obf/hy;->b:Z

    if-eqz v0, :cond_0

    .line 14
    invoke-direct {p0, p1}, Lcom/google/obf/hz;->c(Landroid/content/Context;)V

    .line 15
    :cond_0
    iget-boolean v0, p0, Lcom/google/obf/hz;->f:Z

    invoke-virtual {p0, v0}, Lcom/google/obf/hz;->a(Z)V

    .line 16
    return-void
.end method

.method static synthetic a(Lcom/google/obf/hz;)Ljava/util/List;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/obf/hz;->b:Ljava/util/List;

    return-object v0
.end method

.method private a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/google/obf/hz;->f:Z

    if-nez v0, :cond_0

    .line 56
    iget-object v0, p0, Lcom/google/obf/hz;->l:Lcom/google/obf/hv;

    invoke-virtual {v0, p1}, Lcom/google/obf/hv;->a(Ljava/lang/String;)V

    .line 60
    :goto_0
    return-void

    .line 57
    :cond_0
    iget-object v0, p0, Lcom/google/obf/hz;->h:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 58
    iget-object v0, p0, Lcom/google/obf/hz;->l:Lcom/google/obf/hv;

    iget-object v1, p0, Lcom/google/obf/hz;->h:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x3

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\u00bb"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/obf/hv;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 59
    :cond_1
    iget-object v0, p0, Lcom/google/obf/hz;->l:Lcom/google/obf/hv;

    invoke-virtual {v0, p1}, Lcom/google/obf/hv;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/obf/hz;->l:Lcom/google/obf/hv;

    invoke-virtual {v0, p1}, Lcom/google/obf/hv;->b(Ljava/lang/String;)V

    .line 62
    return-void
.end method

.method private c(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x2

    .line 32
    invoke-virtual {p0, p1}, Lcom/google/obf/hz;->a(Landroid/content/Context;)V

    .line 33
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-direct {v0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/obf/hz;->a:Landroid/widget/FrameLayout;

    .line 34
    iget-object v0, p0, Lcom/google/obf/hz;->a:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/google/obf/hz;->j:Lcom/google/obf/hw;

    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 35
    iget-object v0, p0, Lcom/google/obf/hz;->k:Lcom/google/obf/hy;

    iget v0, v0, Lcom/google/obf/hy;->r:I

    iget v1, p0, Lcom/google/obf/hz;->c:F

    invoke-static {v0, v1}, Lcom/google/obf/hx;->a(IF)I

    move-result v0

    .line 36
    iget-object v1, p0, Lcom/google/obf/hz;->a:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0, v0, v4, v0}, Landroid/widget/FrameLayout;->setPadding(IIII)V

    .line 37
    iget-object v0, p0, Lcom/google/obf/hz;->a:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p0}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 38
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-direct {v0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 39
    iget-object v1, p0, Lcom/google/obf/hz;->a:Landroid/widget/FrameLayout;

    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 40
    iget-object v1, p0, Lcom/google/obf/hz;->k:Lcom/google/obf/hy;

    iget v1, v1, Lcom/google/obf/hy;->s:I

    iget v2, p0, Lcom/google/obf/hz;->c:F

    .line 41
    invoke-static {v1, v2}, Lcom/google/obf/hx;->a(IF)I

    move-result v1

    .line 42
    invoke-virtual {v0, v4, v4, v4, v1}, Landroid/widget/FrameLayout;->setPadding(IIII)V

    .line 43
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 44
    const/16 v2, 0xc

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 45
    const/16 v2, 0xb

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 46
    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 47
    invoke-virtual {p0, v0}, Lcom/google/obf/hz;->addView(Landroid/view/View;)V

    .line 48
    return-void
.end method

.method private d(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 49
    invoke-virtual {p0, p1}, Lcom/google/obf/hz;->b(Landroid/content/Context;)V

    .line 50
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 51
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 52
    iget-object v1, p0, Lcom/google/obf/hz;->l:Lcom/google/obf/hv;

    invoke-virtual {p0, v1, v0}, Lcom/google/obf/hz;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 53
    iget-object v0, p0, Lcom/google/obf/hz;->l:Lcom/google/obf/hv;

    new-instance v1, Lcom/google/obf/hz$1;

    invoke-direct {v1, p0}, Lcom/google/obf/hz$1;-><init>(Lcom/google/obf/hz;)V

    invoke-virtual {v0, v1}, Lcom/google/obf/hv;->a(Lcom/google/obf/hv$a;)V

    .line 54
    return-void
.end method


# virtual methods
.method public a()Landroid/view/View;
    .locals 0

    .prologue
    .line 17
    return-object p0
.end method

.method protected a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 114
    new-instance v0, Lcom/google/obf/hw;

    iget-object v1, p0, Lcom/google/obf/hz;->k:Lcom/google/obf/hy;

    invoke-direct {v0, p1, v1}, Lcom/google/obf/hw;-><init>(Landroid/content/Context;Lcom/google/obf/hy;)V

    iput-object v0, p0, Lcom/google/obf/hz;->j:Lcom/google/obf/hw;

    .line 115
    return-void
.end method

.method public a(Lcom/google/ads/interactivemedia/v3/api/Ad;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 65
    invoke-interface {p1}, Lcom/google/ads/interactivemedia/v3/api/Ad;->getAdPodInfo()Lcom/google/ads/interactivemedia/v3/api/AdPodInfo;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/ads/interactivemedia/v3/api/AdPodInfo;->getAdPosition()I

    move-result v0

    iput v0, p0, Lcom/google/obf/hz;->m:I

    .line 66
    invoke-interface {p1}, Lcom/google/ads/interactivemedia/v3/api/Ad;->getAdPodInfo()Lcom/google/ads/interactivemedia/v3/api/AdPodInfo;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/ads/interactivemedia/v3/api/AdPodInfo;->getTotalAds()I

    move-result v0

    iput v0, p0, Lcom/google/obf/hz;->n:I

    .line 67
    const-string v0, ""

    invoke-direct {p0, v0}, Lcom/google/obf/hz;->a(Ljava/lang/String;)V

    .line 68
    iget-object v0, p0, Lcom/google/obf/hz;->k:Lcom/google/obf/hy;

    iget-boolean v0, v0, Lcom/google/obf/hy;->m:Z

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, Lcom/google/obf/hz;->k:Lcom/google/obf/hy;

    iget-object v0, v0, Lcom/google/obf/hy;->n:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/obf/hz;->b(Ljava/lang/String;)V

    .line 70
    iget-object v0, p0, Lcom/google/obf/hz;->e:Lcom/google/obf/hj;

    new-instance v1, Lcom/google/obf/hi;

    sget-object v2, Lcom/google/obf/hi$b;->i18n:Lcom/google/obf/hi$b;

    sget-object v3, Lcom/google/obf/hi$c;->learnMore:Lcom/google/obf/hi$c;

    iget-object v4, p0, Lcom/google/obf/hz;->d:Ljava/lang/String;

    invoke-direct {v1, v2, v3, v4}, Lcom/google/obf/hi;-><init>(Lcom/google/obf/hi$b;Lcom/google/obf/hi$c;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/obf/hj;->b(Lcom/google/obf/hi;)V

    .line 71
    :cond_0
    invoke-interface {p1}, Lcom/google/ads/interactivemedia/v3/api/Ad;->isSkippable()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 72
    sget-object v0, Lcom/google/obf/hz$b;->b:Lcom/google/obf/hz$b;

    iput-object v0, p0, Lcom/google/obf/hz;->i:Lcom/google/obf/hz$b;

    .line 73
    iget-object v0, p0, Lcom/google/obf/hz;->a:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v6}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 74
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    .line 75
    const-string v1, "seconds"

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    iget-object v1, p0, Lcom/google/obf/hz;->e:Lcom/google/obf/hj;

    new-instance v2, Lcom/google/obf/hi;

    sget-object v3, Lcom/google/obf/hi$b;->i18n:Lcom/google/obf/hi$b;

    sget-object v4, Lcom/google/obf/hi$c;->preSkipButton:Lcom/google/obf/hi$c;

    iget-object v5, p0, Lcom/google/obf/hz;->d:Ljava/lang/String;

    invoke-direct {v2, v3, v4, v5, v0}, Lcom/google/obf/hi;-><init>(Lcom/google/obf/hi$b;Lcom/google/obf/hi$c;Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Lcom/google/obf/hj;->b(Lcom/google/obf/hi;)V

    .line 81
    :cond_1
    :goto_0
    invoke-virtual {p0, v6}, Lcom/google/obf/hz;->setVisibility(I)V

    .line 82
    return-void

    .line 78
    :cond_2
    sget-object v0, Lcom/google/obf/hz$b;->a:Lcom/google/obf/hz$b;

    iput-object v0, p0, Lcom/google/obf/hz;->i:Lcom/google/obf/hz$b;

    .line 79
    iget-object v0, p0, Lcom/google/obf/hz;->a:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_1

    .line 80
    iget-object v0, p0, Lcom/google/obf/hz;->a:Landroid/widget/FrameLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public a(Lcom/google/ads/interactivemedia/v3/api/player/VideoProgressUpdate;)V
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v9, 0x0

    .line 83
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/api/player/VideoProgressUpdate;->getDuration()F

    move-result v0

    cmpg-float v0, v0, v9

    if-gez v0, :cond_1

    .line 104
    :cond_0
    :goto_0
    return-void

    .line 85
    :cond_1
    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/api/player/VideoProgressUpdate;->getDuration()F

    move-result v0

    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/api/player/VideoProgressUpdate;->getCurrentTime()F

    move-result v2

    sub-float v2, v0, v2

    .line 86
    float-to-double v4, v2

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    iget v0, p0, Lcom/google/obf/hz;->g:F

    float-to-double v6, v0

    invoke-static {v6, v7}, Ljava/lang/Math;->floor(D)D

    move-result-wide v6

    cmpl-double v0, v4, v6

    if-eqz v0, :cond_3

    move v0, v1

    .line 87
    :goto_1
    if-eqz v0, :cond_2

    .line 88
    new-instance v3, Ljava/util/HashMap;

    const/4 v4, 0x4

    invoke-direct {v3, v4}, Ljava/util/HashMap;-><init>(I)V

    .line 89
    const-string v4, "minutes"

    float-to-int v5, v2

    div-int/lit8 v5, v5, 0x3c

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    const-string v4, "seconds"

    float-to-int v5, v2

    rem-int/lit8 v5, v5, 0x3c

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    const-string v4, "adPosition"

    iget v5, p0, Lcom/google/obf/hz;->m:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    const-string v4, "totalAds"

    iget v5, p0, Lcom/google/obf/hz;->n:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    iget-object v4, p0, Lcom/google/obf/hz;->e:Lcom/google/obf/hj;

    new-instance v5, Lcom/google/obf/hi;

    sget-object v6, Lcom/google/obf/hi$b;->i18n:Lcom/google/obf/hi$b;

    sget-object v7, Lcom/google/obf/hi$c;->adRemainingTime:Lcom/google/obf/hi$c;

    iget-object v8, p0, Lcom/google/obf/hz;->d:Ljava/lang/String;

    invoke-direct {v5, v6, v7, v8, v3}, Lcom/google/obf/hi;-><init>(Lcom/google/obf/hi$b;Lcom/google/obf/hi$c;Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v4, v5}, Lcom/google/obf/hj;->b(Lcom/google/obf/hi;)V

    .line 94
    :cond_2
    iput v2, p0, Lcom/google/obf/hz;->g:F

    .line 95
    iget-object v2, p0, Lcom/google/obf/hz;->i:Lcom/google/obf/hz$b;

    sget-object v3, Lcom/google/obf/hz$b;->b:Lcom/google/obf/hz$b;

    if-ne v2, v3, :cond_0

    .line 97
    const/high16 v2, 0x40a00000    # 5.0f

    invoke-virtual {p1}, Lcom/google/ads/interactivemedia/v3/api/player/VideoProgressUpdate;->getCurrentTime()F

    move-result v3

    sub-float/2addr v2, v3

    .line 98
    cmpg-float v3, v2, v9

    if-gtz v3, :cond_4

    .line 99
    iget-object v0, p0, Lcom/google/obf/hz;->e:Lcom/google/obf/hj;

    new-instance v1, Lcom/google/obf/hi;

    sget-object v2, Lcom/google/obf/hi$b;->i18n:Lcom/google/obf/hi$b;

    sget-object v3, Lcom/google/obf/hi$c;->skipButton:Lcom/google/obf/hi$c;

    iget-object v4, p0, Lcom/google/obf/hz;->d:Ljava/lang/String;

    invoke-direct {v1, v2, v3, v4}, Lcom/google/obf/hi;-><init>(Lcom/google/obf/hi$b;Lcom/google/obf/hi$c;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/obf/hj;->b(Lcom/google/obf/hi;)V

    goto/16 :goto_0

    .line 86
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 100
    :cond_4
    if-eqz v0, :cond_0

    .line 101
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    .line 102
    const-string v1, "seconds"

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    iget-object v1, p0, Lcom/google/obf/hz;->e:Lcom/google/obf/hj;

    new-instance v2, Lcom/google/obf/hi;

    sget-object v3, Lcom/google/obf/hi$b;->i18n:Lcom/google/obf/hi$b;

    sget-object v4, Lcom/google/obf/hi$c;->preSkipButton:Lcom/google/obf/hi$c;

    iget-object v5, p0, Lcom/google/obf/hz;->d:Ljava/lang/String;

    invoke-direct {v2, v3, v4, v5, v0}, Lcom/google/obf/hi;-><init>(Lcom/google/obf/hi$b;Lcom/google/obf/hi$c;Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Lcom/google/obf/hj;->b(Lcom/google/obf/hi;)V

    goto/16 :goto_0
.end method

.method public a(Lcom/google/obf/hi$c;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 18
    sget-object v0, Lcom/google/obf/hz$2;->a:[I

    invoke-virtual {p1}, Lcom/google/obf/hi$c;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 31
    :cond_0
    :goto_0
    return-void

    .line 19
    :pswitch_0
    invoke-direct {p0, p2}, Lcom/google/obf/hz;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 21
    :pswitch_1
    invoke-direct {p0, p2}, Lcom/google/obf/hz;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 23
    :pswitch_2
    iget-object v0, p0, Lcom/google/obf/hz;->j:Lcom/google/obf/hw;

    invoke-virtual {v0, p2}, Lcom/google/obf/hw;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 25
    :pswitch_3
    iget-object v0, p0, Lcom/google/obf/hz;->j:Lcom/google/obf/hw;

    invoke-virtual {v0, p2}, Lcom/google/obf/hw;->a(Ljava/lang/String;)V

    .line 26
    sget-object v0, Lcom/google/obf/hz$b;->c:Lcom/google/obf/hz$b;

    iput-object v0, p0, Lcom/google/obf/hz;->i:Lcom/google/obf/hz$b;

    .line 27
    iget-object v0, p0, Lcom/google/obf/hz;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/hz$a;

    .line 28
    invoke-interface {v0}, Lcom/google/obf/hz$a;->b()V

    goto :goto_1

    .line 18
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public a(Lcom/google/obf/hz$a;)V
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/obf/hz;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 64
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 105
    iput-boolean p1, p0, Lcom/google/obf/hz;->f:Z

    .line 106
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 107
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/obf/hz;->setVisibility(I)V

    .line 108
    return-void
.end method

.method protected b(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 116
    new-instance v0, Lcom/google/obf/hv;

    iget-object v1, p0, Lcom/google/obf/hz;->k:Lcom/google/obf/hy;

    invoke-direct {v0, p1, v1}, Lcom/google/obf/hv;-><init>(Landroid/content/Context;Lcom/google/obf/hy;)V

    iput-object v0, p0, Lcom/google/obf/hz;->l:Lcom/google/obf/hv;

    .line 117
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/obf/hz;->a:Landroid/widget/FrameLayout;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/obf/hz;->i:Lcom/google/obf/hz$b;

    sget-object v1, Lcom/google/obf/hz$b;->c:Lcom/google/obf/hz$b;

    if-ne v0, v1, :cond_0

    .line 110
    iget-object v0, p0, Lcom/google/obf/hz;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/obf/hz$a;

    .line 111
    invoke-interface {v0}, Lcom/google/obf/hz$a;->a()V

    goto :goto_0

    .line 113
    :cond_0
    return-void
.end method
