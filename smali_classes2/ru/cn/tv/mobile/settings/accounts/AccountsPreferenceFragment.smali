.class public Lru/cn/tv/mobile/settings/accounts/AccountsPreferenceFragment;
.super Landroid/support/v7/preference/PreferenceFragmentCompat;
.source "AccountsPreferenceFragment.java"


# instance fields
.field private mainScreen:Landroid/support/v7/preference/PreferenceScreen;

.field private viewModel:Lru/cn/tv/mobile/settings/accounts/AccountsViewModel;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Landroid/support/v7/preference/PreferenceFragmentCompat;-><init>()V

    return-void
.end method

.method private addContractor(Ljava/lang/String;JLjava/lang/String;)V
    .locals 6
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "contractorId"    # J
    .param p4, "imageUrl"    # Ljava/lang/String;

    .prologue
    .line 67
    invoke-virtual {p0}, Lru/cn/tv/mobile/settings/accounts/AccountsPreferenceFragment;->getPreferenceManager()Landroid/support/v7/preference/PreferenceManager;

    move-result-object v1

    invoke-virtual {p0}, Lru/cn/tv/mobile/settings/accounts/AccountsPreferenceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v7/preference/PreferenceManager;->createPreferenceScreen(Landroid/content/Context;)Landroid/support/v7/preference/PreferenceScreen;

    move-result-object v0

    .line 68
    .local v0, "account":Landroid/support/v7/preference/PreferenceScreen;
    const v1, 0x7f0c0066

    invoke-virtual {v0, v1}, Landroid/support/v7/preference/PreferenceScreen;->setLayoutResource(I)V

    .line 69
    invoke-virtual {v0, p1}, Landroid/support/v7/preference/PreferenceScreen;->setTitle(Ljava/lang/CharSequence;)V

    .line 71
    new-instance v1, Lru/cn/tv/mobile/settings/accounts/AccountsPreferenceFragment$1;

    invoke-direct {v1, p0, p1, p2, p3}, Lru/cn/tv/mobile/settings/accounts/AccountsPreferenceFragment$1;-><init>(Lru/cn/tv/mobile/settings/accounts/AccountsPreferenceFragment;Ljava/lang/String;J)V

    invoke-virtual {v0, v1}, Landroid/support/v7/preference/PreferenceScreen;->setOnPreferenceClickListener(Landroid/support/v7/preference/Preference$OnPreferenceClickListener;)V

    .line 84
    invoke-virtual {p0}, Lru/cn/tv/mobile/settings/accounts/AccountsPreferenceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/picasso/Picasso;->with(Landroid/content/Context;)Lcom/squareup/picasso/Picasso;

    move-result-object v1

    .line 85
    invoke-virtual {v1, p4}, Lcom/squareup/picasso/Picasso;->load(Ljava/lang/String;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v1

    const v2, 0x7f0802e0

    .line 86
    invoke-virtual {v1, v2}, Lcom/squareup/picasso/RequestCreator;->placeholder(I)Lcom/squareup/picasso/RequestCreator;

    move-result-object v1

    new-instance v2, Lru/cn/utils/MaskTransformation;

    .line 87
    invoke-virtual {p0}, Lru/cn/tv/mobile/settings/accounts/AccountsPreferenceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const v4, 0x7f080096

    invoke-direct {v2, v3, v4}, Lru/cn/utils/MaskTransformation;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v1, v2}, Lcom/squareup/picasso/RequestCreator;->transform(Lcom/squareup/picasso/Transformation;)Lcom/squareup/picasso/RequestCreator;

    move-result-object v1

    new-instance v2, Lru/cn/tv/mobile/settings/accounts/AccountsPreferenceFragment$2;

    invoke-direct {v2, p0, v0}, Lru/cn/tv/mobile/settings/accounts/AccountsPreferenceFragment$2;-><init>(Lru/cn/tv/mobile/settings/accounts/AccountsPreferenceFragment;Landroid/support/v7/preference/PreferenceScreen;)V

    .line 88
    invoke-virtual {v1, v2}, Lcom/squareup/picasso/RequestCreator;->into(Lcom/squareup/picasso/Target;)V

    .line 110
    iget-object v1, p0, Lru/cn/tv/mobile/settings/accounts/AccountsPreferenceFragment;->mainScreen:Landroid/support/v7/preference/PreferenceScreen;

    invoke-virtual {v1, v0}, Landroid/support/v7/preference/PreferenceScreen;->addPreference(Landroid/support/v7/preference/Preference;)Z

    .line 111
    return-void
.end method

.method private setContractors(Landroid/database/Cursor;)V
    .locals 7
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 43
    if-eqz p1, :cond_2

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v6

    if-lez v6, :cond_2

    .line 44
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 46
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v6

    if-nez v6, :cond_1

    .line 47
    const-string v6, "_id"

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 48
    .local v2, "contractorId":J
    const-string v6, "image"

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 49
    .local v1, "imageUrl":Ljava/lang/String;
    const-string v6, "name"

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 50
    .local v4, "name":Ljava/lang/String;
    const-string v6, "brand_name"

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 52
    .local v0, "brandName":Ljava/lang/String;
    move-object v5, v0

    .line 53
    .local v5, "title":Ljava/lang/String;
    if-nez v5, :cond_0

    .line 54
    move-object v5, v4

    .line 57
    :cond_0
    invoke-direct {p0, v5, v2, v3, v1}, Lru/cn/tv/mobile/settings/accounts/AccountsPreferenceFragment;->addContractor(Ljava/lang/String;JLjava/lang/String;)V

    .line 59
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_0

    .line 62
    .end local v0    # "brandName":Ljava/lang/String;
    .end local v1    # "imageUrl":Ljava/lang/String;
    .end local v2    # "contractorId":J
    .end local v4    # "name":Ljava/lang/String;
    .end local v5    # "title":Ljava/lang/String;
    :cond_1
    iget-object v6, p0, Lru/cn/tv/mobile/settings/accounts/AccountsPreferenceFragment;->mainScreen:Landroid/support/v7/preference/PreferenceScreen;

    invoke-virtual {p0, v6}, Lru/cn/tv/mobile/settings/accounts/AccountsPreferenceFragment;->setPreferenceScreen(Landroid/support/v7/preference/PreferenceScreen;)V

    .line 64
    :cond_2
    return-void
.end method


# virtual methods
.method final bridge synthetic bridge$lambda$0$AccountsPreferenceFragment(Landroid/database/Cursor;)V
    .locals 0

    invoke-direct {p0, p1}, Lru/cn/tv/mobile/settings/accounts/AccountsPreferenceFragment;->setContractors(Landroid/database/Cursor;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 30
    invoke-super {p0, p1}, Landroid/support/v7/preference/PreferenceFragmentCompat;->onCreate(Landroid/os/Bundle;)V

    .line 31
    invoke-virtual {p0}, Lru/cn/tv/mobile/settings/accounts/AccountsPreferenceFragment;->getPreferenceManager()Landroid/support/v7/preference/PreferenceManager;

    move-result-object v0

    invoke-virtual {p0}, Lru/cn/tv/mobile/settings/accounts/AccountsPreferenceFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/preference/PreferenceManager;->createPreferenceScreen(Landroid/content/Context;)Landroid/support/v7/preference/PreferenceScreen;

    move-result-object v0

    iput-object v0, p0, Lru/cn/tv/mobile/settings/accounts/AccountsPreferenceFragment;->mainScreen:Landroid/support/v7/preference/PreferenceScreen;

    .line 33
    invoke-static {}, Lru/cn/tv/App;->scope()Ltoothpick/Scope;

    move-result-object v0

    const-class v1, Lru/cn/tv/mobile/settings/accounts/AccountsViewModel;

    invoke-static {p0, v0, v1}, Lru/cn/mvvm/ViewModels;->get(Landroid/support/v4/app/Fragment;Ltoothpick/Scope;Ljava/lang/Class;)Landroid/arch/lifecycle/ViewModel;

    move-result-object v0

    check-cast v0, Lru/cn/tv/mobile/settings/accounts/AccountsViewModel;

    iput-object v0, p0, Lru/cn/tv/mobile/settings/accounts/AccountsPreferenceFragment;->viewModel:Lru/cn/tv/mobile/settings/accounts/AccountsViewModel;

    .line 34
    iget-object v0, p0, Lru/cn/tv/mobile/settings/accounts/AccountsPreferenceFragment;->viewModel:Lru/cn/tv/mobile/settings/accounts/AccountsViewModel;

    invoke-virtual {v0}, Lru/cn/tv/mobile/settings/accounts/AccountsViewModel;->contractors()Landroid/arch/lifecycle/LiveData;

    move-result-object v0

    new-instance v1, Lru/cn/tv/mobile/settings/accounts/AccountsPreferenceFragment$$Lambda$0;

    invoke-direct {v1, p0}, Lru/cn/tv/mobile/settings/accounts/AccountsPreferenceFragment$$Lambda$0;-><init>(Lru/cn/tv/mobile/settings/accounts/AccountsPreferenceFragment;)V

    invoke-virtual {v0, p0, v1}, Landroid/arch/lifecycle/LiveData;->observe(Landroid/arch/lifecycle/LifecycleOwner;Landroid/arch/lifecycle/Observer;)V

    .line 35
    return-void
.end method

.method public onCreatePreferences(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0
    .param p1, "bundle"    # Landroid/os/Bundle;
    .param p2, "s"    # Ljava/lang/String;

    .prologue
    .line 39
    return-void
.end method
