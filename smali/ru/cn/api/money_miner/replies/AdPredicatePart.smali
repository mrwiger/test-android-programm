.class public Lru/cn/api/money_miner/replies/AdPredicatePart;
.super Ljava/lang/Object;
.source "AdPredicatePart.java"


# instance fields
.field negative:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "negative_tags"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field positive:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "positive_tags"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 63
    instance-of v3, p1, Lru/cn/api/money_miner/replies/AdPredicatePart;

    if-nez v3, :cond_1

    move v1, v2

    .line 70
    :cond_0
    :goto_0
    return v1

    .line 66
    :cond_1
    if-eq p1, p0, :cond_0

    move-object v0, p1

    .line 69
    check-cast v0, Lru/cn/api/money_miner/replies/AdPredicatePart;

    .line 70
    .local v0, "part":Lru/cn/api/money_miner/replies/AdPredicatePart;
    iget-object v3, p0, Lru/cn/api/money_miner/replies/AdPredicatePart;->positive:Ljava/util/List;

    iget-object v4, v0, Lru/cn/api/money_miner/replies/AdPredicatePart;->positive:Ljava/util/List;

    if-eq v3, v4, :cond_2

    iget-object v3, p0, Lru/cn/api/money_miner/replies/AdPredicatePart;->positive:Ljava/util/List;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lru/cn/api/money_miner/replies/AdPredicatePart;->positive:Ljava/util/List;

    iget-object v4, v0, Lru/cn/api/money_miner/replies/AdPredicatePart;->positive:Ljava/util/List;

    .line 71
    invoke-interface {v3, v4}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_2
    iget-object v3, p0, Lru/cn/api/money_miner/replies/AdPredicatePart;->negative:Ljava/util/List;

    iget-object v4, v0, Lru/cn/api/money_miner/replies/AdPredicatePart;->negative:Ljava/util/List;

    if-eq v3, v4, :cond_0

    iget-object v3, p0, Lru/cn/api/money_miner/replies/AdPredicatePart;->negative:Ljava/util/List;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lru/cn/api/money_miner/replies/AdPredicatePart;->negative:Ljava/util/List;

    iget-object v4, v0, Lru/cn/api/money_miner/replies/AdPredicatePart;->negative:Ljava/util/List;

    .line 73
    invoke-interface {v3, v4}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method matchesConjuctive(Ljava/util/List;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, "tags":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    const/4 v5, 0x1

    .line 21
    iget-object v4, p0, Lru/cn/api/money_miner/replies/AdPredicatePart;->positive:Ljava/util/List;

    if-eqz v4, :cond_1

    .line 22
    iget-object v4, p0, Lru/cn/api/money_miner/replies/AdPredicatePart;->positive:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 23
    .local v2, "positiveTag":J
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {p1, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v4, v5

    .line 35
    .end local v2    # "positiveTag":J
    :goto_0
    return v4

    .line 28
    :cond_1
    iget-object v4, p0, Lru/cn/api/money_miner/replies/AdPredicatePart;->negative:Ljava/util/List;

    if-eqz v4, :cond_3

    .line 29
    iget-object v4, p0, Lru/cn/api/money_miner/replies/AdPredicatePart;->negative:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 30
    .local v0, "negativeTag":J
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {p1, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    move v4, v5

    .line 31
    goto :goto_0

    .line 35
    .end local v0    # "negativeTag":J
    :cond_3
    const/4 v4, 0x0

    goto :goto_0
.end method

.method matchesDisjunctive(Ljava/util/List;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, "tags":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Long;>;"
    const/4 v5, 0x0

    .line 44
    iget-object v4, p0, Lru/cn/api/money_miner/replies/AdPredicatePart;->positive:Ljava/util/List;

    if-eqz v4, :cond_1

    .line 45
    iget-object v4, p0, Lru/cn/api/money_miner/replies/AdPredicatePart;->positive:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 46
    .local v2, "positiveTag":J
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {p1, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    move v4, v5

    .line 58
    .end local v2    # "positiveTag":J
    :goto_0
    return v4

    .line 51
    :cond_1
    iget-object v4, p0, Lru/cn/api/money_miner/replies/AdPredicatePart;->negative:Ljava/util/List;

    if-eqz v4, :cond_3

    .line 52
    iget-object v4, p0, Lru/cn/api/money_miner/replies/AdPredicatePart;->negative:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 53
    .local v0, "negativeTag":J
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {p1, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    move v4, v5

    .line 54
    goto :goto_0

    .line 58
    .end local v0    # "negativeTag":J
    :cond_3
    const/4 v4, 0x1

    goto :goto_0
.end method
