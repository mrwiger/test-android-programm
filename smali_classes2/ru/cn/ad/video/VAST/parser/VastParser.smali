.class public Lru/cn/ad/video/VAST/parser/VastParser;
.super Ljava/lang/Object;
.source "VastParser.java"


# instance fields
.field private adSystem:Ljava/lang/String;

.field private adTitle:Ljava/lang/String;

.field private bannerId:Ljava/lang/String;

.field private final baseUri:Ljava/lang/String;

.field private clickThroughUrl:Ljava/lang/String;

.field private clickTrackingUrlList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private creativeId:Ljava/lang/String;

.field private duration:Ljava/lang/String;

.field private errorUri:Ljava/lang/String;

.field private eventTrackingMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private volatile hasWrapper:Z

.field private impressionTrackerUrlList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mediaFiles:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lru/cn/ad/video/VAST/parser/MediaFile;",
            ">;"
        }
    .end annotation
.end field

.field private skipOffset:Lru/cn/ad/video/VAST/parser/SkipOffset;

.field private vastAdTagUri:Ljava/lang/String;

.field private vastErrorUri:Ljava/lang/String;

.field private vastVersion:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/io/Reader;Ljava/lang/String;)V
    .locals 4
    .param p1, "dataReader"    # Ljava/io/Reader;
    .param p2, "baseURI"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lru/cn/ad/video/VAST/parser/VastParser;->clickTrackingUrlList:Ljava/util/List;

    .line 78
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lru/cn/ad/video/VAST/parser/VastParser;->impressionTrackerUrlList:Ljava/util/List;

    .line 79
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lru/cn/ad/video/VAST/parser/VastParser;->eventTrackingMap:Ljava/util/Map;

    .line 96
    iput-object p2, p0, Lru/cn/ad/video/VAST/parser/VastParser;->baseUri:Ljava/lang/String;

    .line 98
    if-eqz p1, :cond_0

    .line 100
    :try_start_0
    invoke-direct {p0, p1}, Lru/cn/ad/video/VAST/parser/VastParser;->readVAST(Ljava/io/Reader;)V
    :try_end_0
    .catch Lru/cn/ad/video/VAST/parser/VASTParseException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 109
    :goto_0
    return-void

    .line 101
    :catch_0
    move-exception v0

    .line 102
    .local v0, "e":Lru/cn/ad/video/VAST/parser/VASTParseException;
    throw v0

    .line 103
    .end local v0    # "e":Lru/cn/ad/video/VAST/parser/VASTParseException;
    :catch_1
    move-exception v0

    .line 104
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Lru/cn/ad/video/VAST/parser/VASTParseException;

    const-string v2, "Error parsing VAST XML"

    const/16 v3, 0x64

    invoke-direct {v1, v2, v3, v0}, Lru/cn/ad/video/VAST/parser/VASTParseException;-><init>(Ljava/lang/String;ILjava/lang/Throwable;)V

    throw v1

    .line 107
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lru/cn/ad/video/VAST/parser/VastParser;->hasWrapper:Z

    goto :goto_0
.end method

.method private addMediaFile(Lru/cn/ad/video/VAST/parser/MediaFile;)V
    .locals 1
    .param p1, "mf"    # Lru/cn/ad/video/VAST/parser/MediaFile;

    .prologue
    .line 581
    iget-object v0, p0, Lru/cn/ad/video/VAST/parser/VastParser;->mediaFiles:Ljava/util/List;

    if-nez v0, :cond_0

    .line 582
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lru/cn/ad/video/VAST/parser/VastParser;->mediaFiles:Ljava/util/List;

    .line 585
    :cond_0
    iget-object v0, p0, Lru/cn/ad/video/VAST/parser/VastParser;->mediaFiles:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 586
    return-void
.end method

.method private getWrappedVast(Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 3
    .param p1, "p"    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 371
    const/4 v0, 0x2

    const-string v1, "VASTAdTagURI"

    invoke-interface {p1, v0, v2, v1}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    .line 373
    invoke-direct {p0, p1}, Lru/cn/ad/video/VAST/parser/VastParser;->readURL(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lru/cn/ad/video/VAST/parser/VastParser;->vastAdTagUri:Ljava/lang/String;

    .line 375
    const/4 v0, 0x3

    const-string v1, "VASTAdTagURI"

    invoke-interface {p1, v0, v2, v1}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    .line 376
    return-void
.end method

.method private isSupportedVersion(Ljava/lang/String;)Z
    .locals 2
    .param p1, "version"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 621
    if-nez p1, :cond_1

    .line 630
    :cond_0
    :goto_0
    return v0

    .line 624
    :cond_1
    const-string v1, "2.0"

    invoke-static {p1, v1}, Lru/cn/utils/Utils;->versionCompare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-ltz v1, :cond_0

    .line 627
    const-string v1, "3.0"

    invoke-static {p1, v1}, Lru/cn/utils/Utils;->versionCompare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-gtz v1, :cond_0

    .line 630
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private readAd(Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 5
    .param p1, "p"    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    .line 145
    const-string v1, "Ad"

    invoke-interface {p1, v4, v3, v1}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    .line 147
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Ad"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 148
    const-string v1, "id"

    invoke-interface {p1, v3, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 149
    const-string v1, "id"

    invoke-interface {p1, v3, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lru/cn/ad/video/VAST/parser/VastParser;->bannerId:Ljava/lang/String;

    .line 153
    :cond_0
    :goto_0
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_2

    .line 154
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v1

    if-ne v1, v4, :cond_0

    .line 157
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 158
    .local v0, "name":Ljava/lang/String;
    const-string v1, "InLine"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 159
    const-string v1, "VASTXmlParser"

    const-string v2, "VAST file contains inline ad information."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    invoke-direct {p0, p1}, Lru/cn/ad/video/VAST/parser/VastParser;->readInLine(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_0

    .line 161
    :cond_1
    const-string v1, "Wrapper"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 162
    const-string v1, "VASTXmlParser"

    const-string v2, "VAST file contains wrapped ad information."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    const/4 v1, 0x1

    iput-boolean v1, p0, Lru/cn/ad/video/VAST/parser/VastParser;->hasWrapper:Z

    .line 164
    invoke-direct {p0, p1}, Lru/cn/ad/video/VAST/parser/VastParser;->readWrapper(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_0

    .line 167
    .end local v0    # "name":Ljava/lang/String;
    :cond_2
    return-void
.end method

.method private readCreative(Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 6
    .param p1, "p"    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    .line 293
    const-string v2, "Creative"

    invoke-interface {p1, v5, v4, v2}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    .line 294
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Creative"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 295
    const-string v2, "id"

    invoke-interface {p1, v4, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lru/cn/ad/video/VAST/parser/VastParser;->creativeId:Ljava/lang/String;

    .line 298
    :cond_0
    :goto_0
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v2

    const/4 v3, 0x3

    if-eq v2, v3, :cond_3

    .line 299
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v2

    if-ne v2, v5, :cond_0

    .line 303
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 304
    .local v0, "name":Ljava/lang/String;
    if-eqz v0, :cond_2

    const-string v2, "Linear"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 305
    const-string v2, "skipoffset"

    invoke-interface {p1, v4, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 306
    .local v1, "skipoffsetStr":Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 307
    invoke-static {v1}, Lru/cn/ad/video/VAST/parser/SkipOffset;->parseString(Ljava/lang/String;)Lru/cn/ad/video/VAST/parser/SkipOffset;

    move-result-object v2

    iput-object v2, p0, Lru/cn/ad/video/VAST/parser/VastParser;->skipOffset:Lru/cn/ad/video/VAST/parser/SkipOffset;

    .line 310
    :cond_1
    invoke-direct {p0, p1}, Lru/cn/ad/video/VAST/parser/VastParser;->readLinear(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_0

    .line 312
    .end local v1    # "skipoffsetStr":Ljava/lang/String;
    :cond_2
    invoke-direct {p0, p1}, Lru/cn/ad/video/VAST/parser/VastParser;->skip(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_0

    .line 315
    .end local v0    # "name":Ljava/lang/String;
    :cond_3
    return-void
.end method

.method private readCreatives(Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 4
    .param p1, "p"    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x2

    .line 319
    const/4 v1, 0x0

    const-string v2, "Creatives"

    invoke-interface {p1, v3, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    .line 320
    :cond_0
    :goto_0
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_2

    .line 321
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v1

    if-ne v1, v3, :cond_0

    .line 325
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 326
    .local v0, "name":Ljava/lang/String;
    if-eqz v0, :cond_1

    const-string v1, "Creative"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 327
    invoke-direct {p0, p1}, Lru/cn/ad/video/VAST/parser/VastParser;->readCreative(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_0

    .line 329
    :cond_1
    invoke-direct {p0, p1}, Lru/cn/ad/video/VAST/parser/VastParser;->skip(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_0

    .line 332
    .end local v0    # "name":Ljava/lang/String;
    :cond_2
    return-void
.end method

.method private readExtension(Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 4
    .param p1, "p"    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 353
    const/4 v1, 0x2

    const-string v2, "Extension"

    invoke-interface {p1, v1, v3, v2}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    .line 354
    const-string v1, "type"

    invoke-interface {p1, v3, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 356
    const-string v1, "type"

    invoke-interface {p1, v3, v1}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 357
    .local v0, "type":Ljava/lang/String;
    const-string v1, "AdTitle"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 358
    invoke-direct {p0, p1}, Lru/cn/ad/video/VAST/parser/VastParser;->readText(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lru/cn/ad/video/VAST/parser/VastParser;->adTitle:Ljava/lang/String;

    .line 366
    .end local v0    # "type":Ljava/lang/String;
    :goto_0
    const/4 v1, 0x3

    const-string v2, "Extension"

    invoke-interface {p1, v1, v3, v2}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    .line 367
    return-void

    .line 360
    .restart local v0    # "type":Ljava/lang/String;
    :cond_0
    invoke-direct {p0, p1}, Lru/cn/ad/video/VAST/parser/VastParser;->skip(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_0

    .line 363
    .end local v0    # "type":Ljava/lang/String;
    :cond_1
    invoke-direct {p0, p1}, Lru/cn/ad/video/VAST/parser/VastParser;->skip(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_0
.end method

.method private readExtensions(Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 4
    .param p1, "p"    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x2

    .line 336
    const/4 v1, 0x0

    const-string v2, "Extensions"

    invoke-interface {p1, v3, v1, v2}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    .line 337
    :cond_0
    :goto_0
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_2

    .line 338
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v1

    if-ne v1, v3, :cond_0

    .line 342
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 343
    .local v0, "name":Ljava/lang/String;
    if-eqz v0, :cond_1

    const-string v1, "Extension"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 344
    invoke-direct {p0, p1}, Lru/cn/ad/video/VAST/parser/VastParser;->readExtension(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_0

    .line 346
    :cond_1
    invoke-direct {p0, p1}, Lru/cn/ad/video/VAST/parser/VastParser;->skip(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_0

    .line 349
    .end local v0    # "name":Ljava/lang/String;
    :cond_2
    return-void
.end method

.method private readInLine(Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 7
    .param p1, "p"    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x0

    .line 426
    const-string v3, "InLine"

    invoke-interface {p1, v5, v4, v3}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    .line 427
    :cond_0
    :goto_0
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v3

    if-eq v3, v6, :cond_8

    .line 428
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v3

    if-ne v3, v5, :cond_0

    .line 432
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 433
    .local v0, "name":Ljava/lang/String;
    const-string v3, "Impression"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 434
    const-string v3, "Impression"

    invoke-interface {p1, v5, v4, v3}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    .line 435
    invoke-direct {p0, p1}, Lru/cn/ad/video/VAST/parser/VastParser;->readURL(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v2

    .line 436
    .local v2, "url":Ljava/lang/String;
    if-eqz v2, :cond_1

    .line 437
    iget-object v3, p0, Lru/cn/ad/video/VAST/parser/VastParser;->impressionTrackerUrlList:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 439
    :cond_1
    const-string v3, "Impression"

    invoke-interface {p1, v6, v4, v3}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 441
    .end local v2    # "url":Ljava/lang/String;
    :cond_2
    const-string v3, "Creatives"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 442
    invoke-direct {p0, p1}, Lru/cn/ad/video/VAST/parser/VastParser;->readCreatives(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_0

    .line 443
    :cond_3
    const-string v3, "AdSystem"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 444
    const-string v3, "AdSystem"

    invoke-interface {p1, v5, v4, v3}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    .line 445
    invoke-direct {p0, p1}, Lru/cn/ad/video/VAST/parser/VastParser;->readText(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lru/cn/ad/video/VAST/parser/VastParser;->adSystem:Ljava/lang/String;

    .line 446
    const-string v3, "AdSystem"

    invoke-interface {p1, v6, v4, v3}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 448
    :cond_4
    const-string v3, "Extensions"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 449
    invoke-direct {p0, p1}, Lru/cn/ad/video/VAST/parser/VastParser;->readExtensions(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_0

    .line 450
    :cond_5
    const-string v3, "Error"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 451
    const-string v3, "Error"

    invoke-interface {p1, v5, v4, v3}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    .line 452
    invoke-direct {p0, p1}, Lru/cn/ad/video/VAST/parser/VastParser;->readText(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v1

    .line 453
    .local v1, "tagText":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_6

    .line 454
    iput-object v1, p0, Lru/cn/ad/video/VAST/parser/VastParser;->errorUri:Ljava/lang/String;

    .line 456
    :cond_6
    const-string v3, "Error"

    invoke-interface {p1, v6, v4, v3}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 459
    .end local v1    # "tagText":Ljava/lang/String;
    :cond_7
    invoke-direct {p0, p1}, Lru/cn/ad/video/VAST/parser/VastParser;->skip(Lorg/xmlpull/v1/XmlPullParser;)V

    goto/16 :goto_0

    .line 462
    .end local v0    # "name":Ljava/lang/String;
    :cond_8
    return-void
.end method

.method private readLinear(Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 5
    .param p1, "p"    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x0

    const/4 v2, 0x2

    .line 267
    const-string v1, "Linear"

    invoke-interface {p1, v2, v3, v1}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    .line 268
    :cond_0
    :goto_0
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v1

    if-eq v1, v4, :cond_5

    .line 269
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 270
    .local v0, "name":Ljava/lang/String;
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v1

    if-ne v1, v2, :cond_0

    .line 274
    if-eqz v0, :cond_1

    const-string v1, "Duration"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 275
    const-string v1, "Duration"

    invoke-interface {p1, v2, v3, v1}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    .line 276
    invoke-direct {p0, p1}, Lru/cn/ad/video/VAST/parser/VastParser;->readText(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lru/cn/ad/video/VAST/parser/VastParser;->duration:Ljava/lang/String;

    .line 277
    const-string v1, "Duration"

    invoke-interface {p1, v4, v3, v1}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 279
    :cond_1
    if-eqz v0, :cond_2

    const-string v1, "TrackingEvents"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 280
    invoke-direct {p0, p1}, Lru/cn/ad/video/VAST/parser/VastParser;->readTrackingEvents(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_0

    .line 281
    :cond_2
    if-eqz v0, :cond_3

    const-string v1, "MediaFiles"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 282
    invoke-direct {p0, p1}, Lru/cn/ad/video/VAST/parser/VastParser;->readMediaFiles(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_0

    .line 283
    :cond_3
    if-eqz v0, :cond_4

    const-string v1, "VideoClicks"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 284
    invoke-direct {p0, p1}, Lru/cn/ad/video/VAST/parser/VastParser;->readVideoClicks(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_0

    .line 286
    :cond_4
    invoke-direct {p0, p1}, Lru/cn/ad/video/VAST/parser/VastParser;->skip(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_0

    .line 289
    .end local v0    # "name":Ljava/lang/String;
    :cond_5
    return-void
.end method

.method private readMediaFiles(Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 9
    .param p1, "p"    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x0

    .line 171
    const-string v5, "MediaFiles"

    invoke-interface {p1, v7, v6, v5}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    .line 172
    :cond_0
    :goto_0
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v5

    if-eq v5, v8, :cond_5

    .line 173
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v5

    if-ne v5, v7, :cond_0

    .line 177
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    .line 178
    .local v2, "name":Ljava/lang/String;
    if-eqz v2, :cond_4

    const-string v5, "MediaFile"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 179
    const-string v5, "MediaFile"

    invoke-interface {p1, v7, v6, v5}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    .line 180
    new-instance v1, Lru/cn/ad/video/VAST/parser/MediaFile;

    invoke-direct {v1}, Lru/cn/ad/video/VAST/parser/MediaFile;-><init>()V

    .line 182
    .local v1, "mediaFile":Lru/cn/ad/video/VAST/parser/MediaFile;
    const-string v5, "width"

    invoke-interface {p1, v6, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 183
    .local v4, "width":Ljava/lang/String;
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_1

    .line 184
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    iput v5, v1, Lru/cn/ad/video/VAST/parser/MediaFile;->width:I

    .line 187
    :cond_1
    const-string v5, "height"

    invoke-interface {p1, v6, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 188
    .local v0, "height":Ljava/lang/String;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_2

    .line 189
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    iput v5, v1, Lru/cn/ad/video/VAST/parser/MediaFile;->height:I

    .line 192
    :cond_2
    const-string v5, "type"

    invoke-interface {p1, v6, v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 193
    .local v3, "type":Ljava/lang/String;
    if-eqz v3, :cond_3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_3

    .line 194
    iput-object v3, v1, Lru/cn/ad/video/VAST/parser/MediaFile;->type:Ljava/lang/String;

    .line 197
    :cond_3
    invoke-direct {p0, p1}, Lru/cn/ad/video/VAST/parser/VastParser;->readURL(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v1, Lru/cn/ad/video/VAST/parser/MediaFile;->mediaFileUrl:Ljava/lang/String;

    .line 198
    invoke-direct {p0, v1}, Lru/cn/ad/video/VAST/parser/VastParser;->addMediaFile(Lru/cn/ad/video/VAST/parser/MediaFile;)V

    .line 199
    const-string v5, "MediaFile"

    invoke-interface {p1, v8, v6, v5}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 201
    .end local v0    # "height":Ljava/lang/String;
    .end local v1    # "mediaFile":Lru/cn/ad/video/VAST/parser/MediaFile;
    .end local v3    # "type":Ljava/lang/String;
    .end local v4    # "width":Ljava/lang/String;
    :cond_4
    invoke-direct {p0, p1}, Lru/cn/ad/video/VAST/parser/VastParser;->skip(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_0

    .line 204
    .end local v2    # "name":Ljava/lang/String;
    :cond_5
    return-void
.end method

.method private readText(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;
    .locals 4
    .param p1, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    .prologue
    .line 485
    const-string v0, ""

    .line 486
    .local v0, "result":Ljava/lang/String;
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    .line 487
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v0

    .line 488
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->nextTag()I

    .line 492
    :goto_0
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 490
    :cond_0
    const-string v1, "VASTXmlParser"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "No text: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private readTrackingEvents(Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 8
    .param p1, "p"    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x0

    .line 209
    const-string v4, "TrackingEvents"

    invoke-interface {p1, v6, v5, v4}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    .line 210
    :cond_0
    :goto_0
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v4

    if-eq v4, v7, :cond_4

    .line 211
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v4

    if-ne v4, v6, :cond_0

    .line 215
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    .line 216
    .local v2, "name":Ljava/lang/String;
    if-eqz v2, :cond_3

    const-string v4, "Tracking"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 217
    const-string v4, "event"

    invoke-interface {p1, v5, v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 218
    .local v0, "eventName":Ljava/lang/String;
    const-string v4, "Tracking"

    invoke-interface {p1, v6, v5, v4}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    .line 220
    iget-object v4, p0, Lru/cn/ad/video/VAST/parser/VastParser;->eventTrackingMap:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 221
    .local v1, "eventTrackingList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-nez v1, :cond_1

    .line 222
    new-instance v1, Ljava/util/ArrayList;

    .end local v1    # "eventTrackingList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 223
    .restart local v1    # "eventTrackingList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v4, p0, Lru/cn/ad/video/VAST/parser/VastParser;->eventTrackingMap:Ljava/util/Map;

    invoke-interface {v4, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 226
    :cond_1
    invoke-direct {p0, p1}, Lru/cn/ad/video/VAST/parser/VastParser;->readURL(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v3

    .line 227
    .local v3, "url":Ljava/lang/String;
    if-eqz v3, :cond_2

    .line 228
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 231
    :cond_2
    const-string v4, "Tracking"

    invoke-interface {p1, v7, v5, v4}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 233
    .end local v0    # "eventName":Ljava/lang/String;
    .end local v1    # "eventTrackingList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v3    # "url":Ljava/lang/String;
    :cond_3
    invoke-direct {p0, p1}, Lru/cn/ad/video/VAST/parser/VastParser;->skip(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_0

    .line 236
    .end local v2    # "name":Ljava/lang/String;
    :cond_4
    return-void
.end method

.method private readURL(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;
    .locals 2
    .param p1, "parser"    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    .prologue
    .line 497
    invoke-direct {p0, p1}, Lru/cn/ad/video/VAST/parser/VastParser;->readText(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v0

    .line 498
    .local v0, "referenceUri":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 499
    const/4 v1, 0x0

    .line 502
    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lru/cn/ad/video/VAST/parser/VastParser;->baseUri:Ljava/lang/String;

    invoke-static {v1, v0}, Lru/cn/ad/video/util/UriUtil;->resolve(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private readVAST(Ljava/io/Reader;)V
    .locals 7
    .param p1, "contentReader"    # Ljava/io/Reader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;,
            Lru/cn/ad/video/VAST/parser/VASTParseException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x0

    .line 114
    invoke-static {}, Landroid/util/Xml;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v0

    .line 115
    .local v0, "parser":Lorg/xmlpull/v1/XmlPullParser;
    const-string v2, "http://xmlpull.org/v1/doc/features.html#process-namespaces"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Lorg/xmlpull/v1/XmlPullParser;->setFeature(Ljava/lang/String;Z)V

    .line 116
    invoke-interface {v0, p1}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 117
    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->nextTag()I

    .line 118
    const-string v2, "VAST"

    invoke-interface {v0, v5, v4, v2}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    .line 121
    const-string v2, "version"

    invoke-interface {v0, v4, v2}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lru/cn/ad/video/VAST/parser/VastParser;->vastVersion:Ljava/lang/String;

    .line 122
    iget-object v2, p0, Lru/cn/ad/video/VAST/parser/VastParser;->vastVersion:Ljava/lang/String;

    invoke-direct {p0, v2}, Lru/cn/ad/video/VAST/parser/VastParser;->isSupportedVersion(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 123
    new-instance v2, Lru/cn/ad/video/VAST/parser/VASTParseException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unsupported version "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lru/cn/ad/video/VAST/parser/VastParser;->vastVersion:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x66

    invoke-direct {v2, v3, v4}, Lru/cn/ad/video/VAST/parser/VASTParseException;-><init>(Ljava/lang/String;I)V

    throw v2

    .line 126
    :cond_0
    :goto_0
    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v2

    if-eq v2, v6, :cond_3

    .line 127
    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v2

    if-ne v2, v5, :cond_0

    .line 130
    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Ad"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 131
    invoke-direct {p0, v0}, Lru/cn/ad/video/VAST/parser/VastParser;->readAd(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_0

    .line 132
    :cond_1
    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Error"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 133
    const-string v2, "Error"

    invoke-interface {v0, v5, v4, v2}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    .line 134
    invoke-direct {p0, v0}, Lru/cn/ad/video/VAST/parser/VastParser;->readText(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v1

    .line 135
    .local v1, "tagText":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 136
    iput-object v1, p0, Lru/cn/ad/video/VAST/parser/VastParser;->vastErrorUri:Ljava/lang/String;

    .line 138
    :cond_2
    const-string v2, "Error"

    invoke-interface {v0, v6, v4, v2}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 141
    .end local v1    # "tagText":Ljava/lang/String;
    :cond_3
    return-void
.end method

.method private readVideoClicks(Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 6
    .param p1, "p"    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x0

    .line 241
    const-string v2, "VideoClicks"

    invoke-interface {p1, v4, v3, v2}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    .line 242
    :cond_0
    :goto_0
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v2

    if-eq v2, v5, :cond_4

    .line 243
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v2

    if-ne v2, v4, :cond_0

    .line 246
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 247
    .local v0, "name":Ljava/lang/String;
    if-eqz v0, :cond_1

    const-string v2, "ClickThrough"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 248
    const-string v2, "ClickThrough"

    invoke-interface {p1, v4, v3, v2}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    .line 250
    invoke-direct {p0, p1}, Lru/cn/ad/video/VAST/parser/VastParser;->readURL(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lru/cn/ad/video/VAST/parser/VastParser;->clickThroughUrl:Ljava/lang/String;

    .line 251
    const-string v2, "ClickThrough"

    invoke-interface {p1, v5, v3, v2}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 252
    :cond_1
    if-eqz v0, :cond_3

    const-string v2, "ClickTracking"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 253
    const-string v2, "ClickTracking"

    invoke-interface {p1, v4, v3, v2}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    .line 254
    invoke-direct {p0, p1}, Lru/cn/ad/video/VAST/parser/VastParser;->readURL(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v1

    .line 255
    .local v1, "url":Ljava/lang/String;
    if-eqz v1, :cond_2

    .line 256
    iget-object v2, p0, Lru/cn/ad/video/VAST/parser/VastParser;->clickTrackingUrlList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 258
    :cond_2
    const-string v2, "ClickTracking"

    invoke-interface {p1, v5, v3, v2}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 260
    .end local v1    # "url":Ljava/lang/String;
    :cond_3
    invoke-direct {p0, p1}, Lru/cn/ad/video/VAST/parser/VastParser;->skip(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_0

    .line 263
    .end local v0    # "name":Ljava/lang/String;
    :cond_4
    return-void
.end method

.method private readWrapper(Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 7
    .param p1, "p"    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/xmlpull/v1/XmlPullParserException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x0

    .line 387
    const-string v3, "Wrapper"

    invoke-interface {p1, v5, v4, v3}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    .line 388
    :cond_0
    :goto_0
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v3

    if-eq v3, v6, :cond_8

    .line 389
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v3

    if-ne v3, v5, :cond_0

    .line 393
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v0

    .line 394
    .local v0, "name":Ljava/lang/String;
    const-string v3, "Impression"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 395
    const-string v3, "Impression"

    invoke-interface {p1, v5, v4, v3}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    .line 396
    invoke-direct {p0, p1}, Lru/cn/ad/video/VAST/parser/VastParser;->readURL(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v2

    .line 397
    .local v2, "url":Ljava/lang/String;
    if-eqz v2, :cond_1

    .line 398
    iget-object v3, p0, Lru/cn/ad/video/VAST/parser/VastParser;->impressionTrackerUrlList:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 400
    :cond_1
    const-string v3, "Impression"

    invoke-interface {p1, v6, v4, v3}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 402
    .end local v2    # "url":Ljava/lang/String;
    :cond_2
    const-string v3, "Creatives"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 403
    invoke-direct {p0, p1}, Lru/cn/ad/video/VAST/parser/VastParser;->readCreatives(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_0

    .line 404
    :cond_3
    const-string v3, "VASTAdTagURI"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 405
    invoke-direct {p0, p1}, Lru/cn/ad/video/VAST/parser/VastParser;->getWrappedVast(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_0

    .line 406
    :cond_4
    const-string v3, "Extensions"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 407
    invoke-direct {p0, p1}, Lru/cn/ad/video/VAST/parser/VastParser;->readExtensions(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_0

    .line 408
    :cond_5
    const-string v3, "Error"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 409
    const-string v3, "Error"

    invoke-interface {p1, v5, v4, v3}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    .line 410
    invoke-direct {p0, p1}, Lru/cn/ad/video/VAST/parser/VastParser;->readText(Lorg/xmlpull/v1/XmlPullParser;)Ljava/lang/String;

    move-result-object v1

    .line 411
    .local v1, "tagText":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_6

    .line 412
    iput-object v1, p0, Lru/cn/ad/video/VAST/parser/VastParser;->errorUri:Ljava/lang/String;

    .line 414
    :cond_6
    const-string v3, "Error"

    invoke-interface {p1, v6, v4, v3}, Lorg/xmlpull/v1/XmlPullParser;->require(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 417
    .end local v1    # "tagText":Ljava/lang/String;
    :cond_7
    invoke-direct {p0, p1}, Lru/cn/ad/video/VAST/parser/VastParser;->skip(Lorg/xmlpull/v1/XmlPullParser;)V

    goto :goto_0

    .line 420
    .end local v0    # "name":Ljava/lang/String;
    :cond_8
    return-void
.end method

.method private skip(Lorg/xmlpull/v1/XmlPullParser;)V
    .locals 4
    .param p1, "p"    # Lorg/xmlpull/v1/XmlPullParser;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xmlpull/v1/XmlPullParserException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 466
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    .line 467
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Expected start tagName with name"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 470
    :cond_0
    const/4 v0, 0x1

    .line 471
    .local v0, "depth":I
    :goto_0
    if-eqz v0, :cond_1

    .line 472
    invoke-interface {p1}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 477
    :pswitch_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 474
    :pswitch_1
    add-int/lit8 v0, v0, -0x1

    .line 475
    goto :goto_0

    .line 481
    :cond_1
    return-void

    .line 472
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public addClickTrackUrl(Ljava/lang/String;)V
    .locals 1
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 617
    iget-object v0, p0, Lru/cn/ad/video/VAST/parser/VastParser;->clickTrackingUrlList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 618
    return-void
.end method

.method public getAdSystem()Ljava/lang/String;
    .locals 1

    .prologue
    .line 593
    iget-object v0, p0, Lru/cn/ad/video/VAST/parser/VastParser;->adSystem:Ljava/lang/String;

    return-object v0
.end method

.method public getAdTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 569
    iget-object v0, p0, Lru/cn/ad/video/VAST/parser/VastParser;->adTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getBannerId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 565
    iget-object v0, p0, Lru/cn/ad/video/VAST/parser/VastParser;->bannerId:Ljava/lang/String;

    return-object v0
.end method

.method public getBaseUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 601
    iget-object v0, p0, Lru/cn/ad/video/VAST/parser/VastParser;->baseUri:Ljava/lang/String;

    return-object v0
.end method

.method public getClickThroughUrl()Ljava/lang/String;
    .locals 1

    .prologue
    .line 535
    iget-object v0, p0, Lru/cn/ad/video/VAST/parser/VastParser;->clickThroughUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getClickTrackingUrls()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 544
    iget-object v0, p0, Lru/cn/ad/video/VAST/parser/VastParser;->clickTrackingUrlList:Ljava/util/List;

    return-object v0
.end method

.method public getCreativeId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 557
    iget-object v0, p0, Lru/cn/ad/video/VAST/parser/VastParser;->creativeId:Ljava/lang/String;

    return-object v0
.end method

.method public getErrorUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 597
    iget-object v0, p0, Lru/cn/ad/video/VAST/parser/VastParser;->errorUri:Ljava/lang/String;

    return-object v0
.end method

.method public getEventTrackingUrls(Ljava/lang/String;)Ljava/util/List;
    .locals 1
    .param p1, "eventName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 553
    iget-object v0, p0, Lru/cn/ad/video/VAST/parser/VastParser;->eventTrackingMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public getImpressionTrackerUrls()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 511
    iget-object v0, p0, Lru/cn/ad/video/VAST/parser/VastParser;->impressionTrackerUrlList:Ljava/util/List;

    return-object v0
.end method

.method public getMediaFiles()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lru/cn/ad/video/VAST/parser/MediaFile;",
            ">;"
        }
    .end annotation

    .prologue
    .line 526
    iget-object v0, p0, Lru/cn/ad/video/VAST/parser/VastParser;->mediaFiles:Ljava/util/List;

    return-object v0
.end method

.method public getSkipOffset()Lru/cn/ad/video/VAST/parser/SkipOffset;
    .locals 1

    .prologue
    .line 561
    iget-object v0, p0, Lru/cn/ad/video/VAST/parser/VastParser;->skipOffset:Lru/cn/ad/video/VAST/parser/SkipOffset;

    return-object v0
.end method

.method public getVastAdTagUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 379
    iget-object v0, p0, Lru/cn/ad/video/VAST/parser/VastParser;->vastAdTagUri:Ljava/lang/String;

    return-object v0
.end method

.method public getVastErrorUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 605
    iget-object v0, p0, Lru/cn/ad/video/VAST/parser/VastParser;->vastErrorUri:Ljava/lang/String;

    return-object v0
.end method

.method public getVastVersion()Ljava/lang/String;
    .locals 1

    .prologue
    .line 589
    iget-object v0, p0, Lru/cn/ad/video/VAST/parser/VastParser;->vastVersion:Ljava/lang/String;

    return-object v0
.end method

.method public hasWrapper()Z
    .locals 1

    .prologue
    .line 577
    iget-boolean v0, p0, Lru/cn/ad/video/VAST/parser/VastParser;->hasWrapper:Z

    return v0
.end method

.method public setImpressionUri(Ljava/lang/String;)V
    .locals 1
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 609
    iget-object v0, p0, Lru/cn/ad/video/VAST/parser/VastParser;->impressionTrackerUrlList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 610
    return-void
.end method

.method public setVastAdTagUri(Ljava/lang/String;)V
    .locals 0
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 613
    iput-object p1, p0, Lru/cn/ad/video/VAST/parser/VastParser;->vastAdTagUri:Ljava/lang/String;

    .line 614
    return-void
.end method
