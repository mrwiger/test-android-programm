.class public final Lcom/my/target/cy;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "PromoCardImageAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/cy$b;,
        Lcom/my/target/cy$a;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$Adapter",
        "<",
        "Lcom/my/target/cy$b;",
        ">;"
    }
.end annotation


# instance fields
.field private cardClickListener:Landroid/view/View$OnClickListener;

.field private final context:Landroid/content/Context;

.field private final nativeAdCards:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/my/target/core/models/banners/b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/my/target/cy;->context:Landroid/content/Context;

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/my/target/cy;->nativeAdCards:Ljava/util/List;

    .line 41
    return-void
.end method


# virtual methods
.method public final dispose()V
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/my/target/cy;->nativeAdCards:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 149
    invoke-virtual {p0}, Lcom/my/target/cy;->notifyDataSetChanged()V

    .line 150
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/my/target/cy;->cardClickListener:Landroid/view/View$OnClickListener;

    .line 151
    return-void
.end method

.method public final getItemCount()I
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/my/target/cy;->nativeAdCards:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getItemViewType(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 52
    if-nez p1, :cond_0

    .line 54
    const/4 v0, 0x1

    .line 62
    :goto_0
    return v0

    .line 56
    :cond_0
    iget-object v0, p0, Lcom/my/target/cy;->nativeAdCards:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_1

    .line 58
    const/4 v0, 0x2

    goto :goto_0

    .line 62
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 30
    check-cast p1, Lcom/my/target/cy$b;

    .line 2092
    iget-object v0, p0, Lcom/my/target/cy;->nativeAdCards:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p2, v0, :cond_3

    .line 2094
    iget-object v0, p0, Lcom/my/target/cy;->nativeAdCards:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/core/models/banners/b;

    .line 2097
    :goto_0
    if-eqz v0, :cond_2

    .line 2099
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/b;->getImage()Lcom/my/target/common/models/ImageData;

    move-result-object v0

    .line 2101
    :goto_1
    if-eqz v0, :cond_0

    .line 2103
    invoke-static {p1}, Lcom/my/target/cy$b;->a(Lcom/my/target/cy$b;)Lcom/my/target/bv;

    move-result-object v1

    invoke-virtual {v0}, Lcom/my/target/common/models/ImageData;->getWidth()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/my/target/bv;->setPlaceholderWidth(I)V

    .line 2104
    invoke-static {p1}, Lcom/my/target/cy$b;->a(Lcom/my/target/cy$b;)Lcom/my/target/bv;

    move-result-object v1

    invoke-virtual {v0}, Lcom/my/target/common/models/ImageData;->getHeight()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/my/target/bv;->setPlaceholderHeight(I)V

    .line 2105
    invoke-virtual {v0}, Lcom/my/target/common/models/ImageData;->getData()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 2106
    if-eqz v1, :cond_1

    .line 2108
    invoke-static {p1}, Lcom/my/target/cy$b;->a(Lcom/my/target/cy$b;)Lcom/my/target/bv;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/my/target/bv;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 2115
    :cond_0
    :goto_2
    invoke-static {p1}, Lcom/my/target/cy$b;->a(Lcom/my/target/cy$b;)Lcom/my/target/bv;

    move-result-object v0

    const-string v1, "card_"

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/my/target/bv;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2116
    invoke-static {p1}, Lcom/my/target/cy$b;->b(Lcom/my/target/cy$b;)Landroid/widget/FrameLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/my/target/cy;->cardClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 30
    return-void

    .line 2112
    :cond_1
    invoke-static {p1}, Lcom/my/target/cy$b;->a(Lcom/my/target/cy$b;)Lcom/my/target/bv;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/my/target/ch;->a(Lcom/my/target/common/models/ImageData;Landroid/widget/ImageView;)V

    goto :goto_2

    :cond_2
    move-object v0, v1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method

.method public final synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, -0x1

    .line 30
    .line 3069
    new-instance v0, Lcom/my/target/cy$a;

    iget-object v1, p0, Lcom/my/target/cy;->context:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/my/target/cy$a;-><init>(Landroid/content/Context;)V

    .line 3070
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x2

    invoke-direct {v1, v2, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/my/target/cy$a;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 3072
    new-instance v1, Lcom/my/target/bv;

    iget-object v2, p0, Lcom/my/target/cy;->context:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/my/target/bv;-><init>(Landroid/content/Context;)V

    .line 3073
    const-string v2, "card_media_view"

    invoke-static {v1, v2}, Lcom/my/target/cm;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 3074
    iget-object v2, p0, Lcom/my/target/cy;->context:Landroid/content/Context;

    invoke-static {v2}, Lcom/my/target/cm;->x(Landroid/content/Context;)Lcom/my/target/cm;

    move-result-object v2

    .line 3075
    invoke-virtual {v2, v7}, Lcom/my/target/cm;->n(I)I

    move-result v3

    invoke-virtual {v2, v7}, Lcom/my/target/cm;->n(I)I

    move-result v4

    invoke-virtual {v2, v7}, Lcom/my/target/cm;->n(I)I

    move-result v5

    invoke-virtual {v2, v7}, Lcom/my/target/cm;->n(I)I

    move-result v2

    invoke-virtual {v1, v3, v4, v5, v2}, Lcom/my/target/bv;->setPadding(IIII)V

    .line 3076
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v2, v6, v6}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Lcom/my/target/cy$a;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 3078
    new-instance v2, Landroid/widget/FrameLayout;

    iget-object v3, p0, Lcom/my/target/cy;->context:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 3079
    invoke-virtual {p1}, Landroid/view/ViewGroup;->isClickable()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 3081
    const/4 v3, 0x0

    const v4, 0x44c5eaf8

    invoke-static {v2, v3, v4}, Lcom/my/target/cm;->a(Landroid/view/View;II)V

    .line 3083
    :cond_0
    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v3, v6, v6}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v2, v3}, Lcom/my/target/cy$a;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 3085
    new-instance v3, Lcom/my/target/cy$b;

    invoke-direct {v3, v0, v1, v2}, Lcom/my/target/cy$b;-><init>(Landroid/widget/FrameLayout;Lcom/my/target/bv;Landroid/widget/FrameLayout;)V

    .line 30
    return-object v3
.end method

.method public final synthetic onViewRecycled(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 30
    check-cast p1, Lcom/my/target/cy$b;

    .line 1123
    invoke-virtual {p1}, Lcom/my/target/cy$b;->getAdapterPosition()I

    move-result v0

    .line 1124
    if-lez v0, :cond_2

    iget-object v2, p0, Lcom/my/target/cy;->nativeAdCards:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 1126
    iget-object v2, p0, Lcom/my/target/cy;->nativeAdCards:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/my/target/core/models/banners/b;

    .line 1128
    :goto_0
    invoke-static {p1}, Lcom/my/target/cy$b;->a(Lcom/my/target/cy$b;)Lcom/my/target/bv;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/my/target/bv;->setImageData(Lcom/my/target/common/models/ImageData;)V

    .line 1130
    if-eqz v0, :cond_1

    .line 1132
    invoke-virtual {v0}, Lcom/my/target/core/models/banners/b;->getImage()Lcom/my/target/common/models/ImageData;

    move-result-object v0

    .line 1134
    :goto_1
    if-eqz v0, :cond_0

    .line 1136
    invoke-static {p1}, Lcom/my/target/cy$b;->a(Lcom/my/target/cy$b;)Lcom/my/target/bv;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/my/target/ch;->b(Lcom/my/target/common/models/ImageData;Landroid/widget/ImageView;)V

    .line 1138
    :cond_0
    invoke-static {p1}, Lcom/my/target/cy$b;->b(Lcom/my/target/cy$b;)Landroid/widget/FrameLayout;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 30
    return-void

    :cond_1
    move-object v0, v1

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method final setCardClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0
    .param p1, "cardClickListener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 155
    iput-object p1, p0, Lcom/my/target/cy;->cardClickListener:Landroid/view/View$OnClickListener;

    .line 156
    return-void
.end method

.method public final setCards(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/my/target/core/models/banners/b;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 143
    .local p1, "cards":Ljava/util/List;, "Ljava/util/List<Lcom/my/target/core/models/banners/b;>;"
    iget-object v0, p0, Lcom/my/target/cy;->nativeAdCards:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 144
    return-void
.end method
