.class public Lru/cn/api/authorization/Account;
.super Ljava/lang/Object;
.source "Account.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/api/authorization/Account$Token;
    }
.end annotation


# instance fields
.field private login:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/Expose;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "accountName"
    .end annotation
.end field

.field private token:Lru/cn/api/authorization/Account$Token;
    .annotation runtime Lcom/google/gson/annotations/Expose;
    .end annotation

    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "token"
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Lru/cn/api/authorization/Account$Token;)V
    .locals 0
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "token"    # Lru/cn/api/authorization/Account$Token;

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p1, p0, Lru/cn/api/authorization/Account;->login:Ljava/lang/String;

    .line 53
    iput-object p2, p0, Lru/cn/api/authorization/Account;->token:Lru/cn/api/authorization/Account$Token;

    .line 54
    return-void
.end method


# virtual methods
.method public getLogin()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lru/cn/api/authorization/Account;->login:Ljava/lang/String;

    return-object v0
.end method

.method public getToken()Lru/cn/api/authorization/Account$Token;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lru/cn/api/authorization/Account;->token:Lru/cn/api/authorization/Account$Token;

    return-object v0
.end method

.method public setToken(Lru/cn/api/authorization/Account$Token;)V
    .locals 0
    .param p1, "token"    # Lru/cn/api/authorization/Account$Token;

    .prologue
    .line 69
    iput-object p1, p0, Lru/cn/api/authorization/Account;->token:Lru/cn/api/authorization/Account$Token;

    .line 70
    return-void
.end method
