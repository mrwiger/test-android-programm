.class Lcom/my/target/bt$d;
.super Landroid/view/GestureDetector;
.source "BannerWebView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/my/target/bt;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "d"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/bt$d$a;
    }
.end annotation


# instance fields
.field private final ih:Landroid/view/View;

.field private ii:Lcom/my/target/bt$d$a;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 282
    new-instance v0, Landroid/view/GestureDetector$SimpleOnGestureListener;

    invoke-direct {v0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    invoke-direct {p0, p1, p2, v0}, Lcom/my/target/bt$d;-><init>(Landroid/content/Context;Landroid/view/View;Landroid/view/GestureDetector$SimpleOnGestureListener;)V

    .line 283
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/view/View;Landroid/view/GestureDetector$SimpleOnGestureListener;)V
    .locals 1

    .prologue
    .line 287
    invoke-direct {p0, p1, p3}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    .line 289
    iput-object p2, p0, Lcom/my/target/bt$d;->ih:Landroid/view/View;

    .line 291
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/my/target/bt$d;->setIsLongpressEnabled(Z)V

    .line 292
    return-void
.end method

.method private a(Landroid/view/MotionEvent;Landroid/view/View;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x0

    .line 330
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 338
    :cond_0
    :goto_0
    return v0

    .line 335
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    .line 336
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    .line 338
    cmpl-float v3, v1, v4

    if-ltz v3, :cond_0

    invoke-virtual {p2}, Landroid/view/View;->getWidth()I

    move-result v3

    int-to-float v3, v3

    cmpg-float v1, v1, v3

    if-gtz v1, :cond_0

    cmpl-float v1, v2, v4

    if-ltz v1, :cond_0

    .line 339
    invoke-virtual {p2}, Landroid/view/View;->getHeight()I

    move-result v1

    int-to-float v1, v1

    cmpg-float v1, v2, v1

    if-gtz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/view/MotionEvent;)V
    .locals 1

    .prologue
    .line 296
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 321
    :cond_0
    :goto_0
    return-void

    .line 299
    :pswitch_0
    iget-object v0, p0, Lcom/my/target/bt$d;->ii:Lcom/my/target/bt$d$a;

    if-eqz v0, :cond_1

    .line 301
    const-string v0, "Gestures: user clicked"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 302
    iget-object v0, p0, Lcom/my/target/bt$d;->ii:Lcom/my/target/bt$d$a;

    invoke-interface {v0}, Lcom/my/target/bt$d$a;->ba()V

    goto :goto_0

    .line 306
    :cond_1
    const-string v0, "View\'s onUserClick() is not registered."

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 310
    :pswitch_1
    invoke-virtual {p0, p1}, Lcom/my/target/bt$d;->onTouchEvent(Landroid/view/MotionEvent;)Z

    goto :goto_0

    .line 313
    :pswitch_2
    iget-object v0, p0, Lcom/my/target/bt$d;->ih:Landroid/view/View;

    invoke-direct {p0, p1, v0}, Lcom/my/target/bt$d;->a(Landroid/view/MotionEvent;Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 315
    invoke-virtual {p0, p1}, Lcom/my/target/bt$d;->onTouchEvent(Landroid/view/MotionEvent;)Z

    goto :goto_0

    .line 296
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public a(Lcom/my/target/bt$d$a;)V
    .locals 0

    .prologue
    .line 325
    iput-object p1, p0, Lcom/my/target/bt$d;->ii:Lcom/my/target/bt$d$a;

    .line 326
    return-void
.end method
