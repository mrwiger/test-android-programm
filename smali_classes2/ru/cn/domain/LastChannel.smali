.class public Lru/cn/domain/LastChannel;
.super Ljava/lang/Object;
.source "LastChannel.java"


# instance fields
.field private final kidsMode:Z

.field private pref:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "kidsMode"    # Z

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-boolean p2, p0, Lru/cn/domain/LastChannel;->kidsMode:Z

    .line 17
    const-string v0, "SimplePlayerFragment_last_watched_channel"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lru/cn/domain/LastChannel;->pref:Landroid/content/SharedPreferences;

    .line 18
    return-void
.end method


# virtual methods
.method public clearPrevChannel()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 59
    iget-object v1, p0, Lru/cn/domain/LastChannel;->pref:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 60
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "child_prevCnId"

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 61
    const-string v1, "child_cnId"

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 62
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 63
    return-void
.end method

.method public getLastChannel()J
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 43
    iget-boolean v0, p0, Lru/cn/domain/LastChannel;->kidsMode:Z

    if-eqz v0, :cond_0

    .line 44
    iget-object v0, p0, Lru/cn/domain/LastChannel;->pref:Landroid/content/SharedPreferences;

    const-string v1, "child_cnId"

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 46
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lru/cn/domain/LastChannel;->pref:Landroid/content/SharedPreferences;

    const-string v1, "cnId"

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    goto :goto_0
.end method

.method public getPrevChannel()J
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 51
    iget-boolean v0, p0, Lru/cn/domain/LastChannel;->kidsMode:Z

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lru/cn/domain/LastChannel;->pref:Landroid/content/SharedPreferences;

    const-string v1, "child_prevCnId"

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 54
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lru/cn/domain/LastChannel;->pref:Landroid/content/SharedPreferences;

    const-string v1, "prevCnId"

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    goto :goto_0
.end method

.method public saveLastChannel(J)V
    .locals 7
    .param p1, "cnId"    # J

    .prologue
    .line 21
    invoke-virtual {p0}, Lru/cn/domain/LastChannel;->getLastChannel()J

    move-result-wide v2

    .line 23
    .local v2, "lastChannelId":J
    cmp-long v1, v2, p1

    if-eqz v1, :cond_1

    .line 24
    iget-object v1, p0, Lru/cn/domain/LastChannel;->pref:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 25
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    iget-boolean v1, p0, Lru/cn/domain/LastChannel;->kidsMode:Z

    if-eqz v1, :cond_2

    .line 26
    const-string v1, "child_cnId"

    invoke-interface {v0, v1, p1, p2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 31
    :goto_0
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    .line 32
    iget-boolean v1, p0, Lru/cn/domain/LastChannel;->kidsMode:Z

    if-eqz v1, :cond_3

    .line 33
    const-string v1, "child_prevCnId"

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 38
    :cond_0
    :goto_1
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 40
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_1
    return-void

    .line 28
    .restart local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_2
    const-string v1, "cnId"

    invoke-interface {v0, v1, p1, p2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    goto :goto_0

    .line 35
    :cond_3
    const-string v1, "prevCnId"

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    goto :goto_1
.end method
