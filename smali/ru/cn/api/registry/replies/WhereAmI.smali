.class public Lru/cn/api/registry/replies/WhereAmI;
.super Ljava/lang/Object;
.source "WhereAmI.java"


# instance fields
.field public contractor:Lru/cn/api/registry/replies/Contractor;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "contractor"
    .end annotation
.end field

.field public ipAddress:Ljava/lang/String;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "ipAddress"
    .end annotation
.end field

.field public services:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "services"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lru/cn/api/registry/replies/Service;",
            ">;"
        }
    .end annotation
.end field

.field public territories:Ljava/util/List;
    .annotation runtime Lcom/google/gson/annotations/SerializedName;
        value = "territories"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lru/cn/api/registry/replies/Territory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 10
    const/4 v0, 0x0

    iput-object v0, p0, Lru/cn/api/registry/replies/WhereAmI;->contractor:Lru/cn/api/registry/replies/Contractor;

    .line 13
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lru/cn/api/registry/replies/WhereAmI;->territories:Ljava/util/List;

    .line 16
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lru/cn/api/registry/replies/WhereAmI;->services:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public getTerritory()Lru/cn/api/registry/replies/Territory;
    .locals 2

    .prologue
    .line 23
    iget-object v0, p0, Lru/cn/api/registry/replies/WhereAmI;->territories:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 24
    iget-object v0, p0, Lru/cn/api/registry/replies/WhereAmI;->territories:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lru/cn/api/registry/replies/Territory;

    .line 25
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
