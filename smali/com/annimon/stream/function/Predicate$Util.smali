.class public Lcom/annimon/stream/function/Predicate$Util;
.super Ljava/lang/Object;
.source "Predicate.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/annimon/stream/function/Predicate;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Util"
.end annotation


# direct methods
.method public static negate(Lcom/annimon/stream/function/Predicate;)Lcom/annimon/stream/function/Predicate;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/annimon/stream/function/Predicate",
            "<-TT;>;)",
            "Lcom/annimon/stream/function/Predicate",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 86
    .local p0, "p1":Lcom/annimon/stream/function/Predicate;, "Lcom/annimon/stream/function/Predicate<-TT;>;"
    new-instance v0, Lcom/annimon/stream/function/Predicate$Util$4;

    invoke-direct {v0, p0}, Lcom/annimon/stream/function/Predicate$Util$4;-><init>(Lcom/annimon/stream/function/Predicate;)V

    return-object v0
.end method

.method public static notNull()Lcom/annimon/stream/function/Predicate;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/annimon/stream/function/Predicate",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 101
    new-instance v0, Lcom/annimon/stream/function/Predicate$Util$5;

    invoke-direct {v0}, Lcom/annimon/stream/function/Predicate$Util$5;-><init>()V

    return-object v0
.end method
