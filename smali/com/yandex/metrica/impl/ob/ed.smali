.class Lcom/yandex/metrica/impl/ob/ed;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Lcom/yandex/metrica/impl/ob/dv;

.field private c:Lcom/yandex/metrica/impl/ob/ds;

.field private d:Landroid/location/Location;

.field private e:J

.field private f:Lcom/yandex/metrica/impl/utils/q;

.field private g:Lcom/yandex/metrica/impl/ob/en;

.field private h:Lcom/yandex/metrica/impl/ob/dr;


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/yandex/metrica/impl/ob/dv;Lcom/yandex/metrica/impl/ob/ds;Landroid/location/Location;JLcom/yandex/metrica/impl/utils/q;Lcom/yandex/metrica/impl/ob/en;Lcom/yandex/metrica/impl/ob/dr;)V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-object p1, p0, Lcom/yandex/metrica/impl/ob/ed;->a:Ljava/lang/String;

    .line 62
    iput-object p2, p0, Lcom/yandex/metrica/impl/ob/ed;->b:Lcom/yandex/metrica/impl/ob/dv;

    .line 63
    iput-object p3, p0, Lcom/yandex/metrica/impl/ob/ed;->c:Lcom/yandex/metrica/impl/ob/ds;

    .line 64
    iput-object p4, p0, Lcom/yandex/metrica/impl/ob/ed;->d:Landroid/location/Location;

    .line 65
    iput-wide p5, p0, Lcom/yandex/metrica/impl/ob/ed;->e:J

    .line 66
    iput-object p7, p0, Lcom/yandex/metrica/impl/ob/ed;->f:Lcom/yandex/metrica/impl/utils/q;

    .line 67
    iput-object p8, p0, Lcom/yandex/metrica/impl/ob/ed;->g:Lcom/yandex/metrica/impl/ob/en;

    .line 68
    iput-object p9, p0, Lcom/yandex/metrica/impl/ob/ed;->h:Lcom/yandex/metrica/impl/ob/dr;

    .line 69
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/yandex/metrica/impl/ob/dv;Lcom/yandex/metrica/impl/ob/ds;Lcom/yandex/metrica/impl/ob/en;Lcom/yandex/metrica/impl/ob/dr;)V
    .locals 11

    .prologue
    .line 45
    const/4 v5, 0x0

    const-wide/16 v6, 0x0

    new-instance v8, Lcom/yandex/metrica/impl/utils/p;

    invoke-direct {v8}, Lcom/yandex/metrica/impl/utils/p;-><init>()V

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v9, p4

    move-object/from16 v10, p5

    invoke-direct/range {v1 .. v10}, Lcom/yandex/metrica/impl/ob/ed;-><init>(Ljava/lang/String;Lcom/yandex/metrica/impl/ob/dv;Lcom/yandex/metrica/impl/ob/ds;Landroid/location/Location;JLcom/yandex/metrica/impl/utils/q;Lcom/yandex/metrica/impl/ob/en;Lcom/yandex/metrica/impl/ob/dr;)V

    .line 55
    return-void
.end method


# virtual methods
.method public a(Landroid/location/Location;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 72
    .line 1102
    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ed;->b:Lcom/yandex/metrica/impl/ob/dv;

    if-eqz v0, :cond_2

    .line 1106
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ed;->d:Landroid/location/Location;

    if-eqz v0, :cond_7

    .line 1128
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ed;->f:Lcom/yandex/metrica/impl/utils/q;

    invoke-interface {v0}, Lcom/yandex/metrica/impl/utils/q;->a()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/yandex/metrica/impl/ob/ed;->e:J

    sub-long/2addr v4, v6

    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ed;->b:Lcom/yandex/metrica/impl/ob/dv;

    iget-wide v6, v0, Lcom/yandex/metrica/impl/ob/dv;->g:J

    cmp-long v0, v4, v6

    if-lez v0, :cond_4

    move v0, v1

    .line 1175
    :goto_0
    iget-object v3, p0, Lcom/yandex/metrica/impl/ob/ed;->d:Landroid/location/Location;

    invoke-virtual {p1, v3}, Landroid/location/Location;->distanceTo(Landroid/location/Location;)F

    move-result v3

    .line 1144
    iget-object v4, p0, Lcom/yandex/metrica/impl/ob/ed;->b:Lcom/yandex/metrica/impl/ob/dv;

    iget v4, v4, Lcom/yandex/metrica/impl/ob/dv;->h:F

    cmpl-float v3, v3, v4

    if-lez v3, :cond_5

    move v3, v1

    .line 2156
    :goto_1
    iget-object v4, p0, Lcom/yandex/metrica/impl/ob/ed;->d:Landroid/location/Location;

    if-eqz v4, :cond_0

    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v4

    iget-object v6, p0, Lcom/yandex/metrica/impl/ob/ed;->d:Landroid/location/Location;

    invoke-virtual {v6}, Landroid/location/Location;->getTime()J

    move-result-wide v6

    sub-long/2addr v4, v6

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-ltz v4, :cond_6

    :cond_0
    move v4, v1

    .line 1110
    :goto_2
    if-nez v0, :cond_1

    if-eqz v3, :cond_2

    :cond_1
    if-eqz v4, :cond_2

    move v2, v1

    .line 72
    :cond_2
    :goto_3
    if-eqz v2, :cond_3

    .line 3092
    iput-object p1, p0, Lcom/yandex/metrica/impl/ob/ed;->d:Landroid/location/Location;

    .line 3093
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/yandex/metrica/impl/ob/ed;->e:J

    .line 3097
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ed;->c:Lcom/yandex/metrica/impl/ob/ds;

    iget-object v1, p0, Lcom/yandex/metrica/impl/ob/ed;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/yandex/metrica/impl/ob/ed;->b:Lcom/yandex/metrica/impl/ob/dv;

    invoke-interface {v0, v1, p1, v2}, Lcom/yandex/metrica/impl/ob/ds;->a(Ljava/lang/String;Landroid/location/Location;Lcom/yandex/metrica/impl/ob/dv;)V

    .line 4088
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ed;->g:Lcom/yandex/metrica/impl/ob/en;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/en;->a()V

    .line 5084
    iget-object v0, p0, Lcom/yandex/metrica/impl/ob/ed;->h:Lcom/yandex/metrica/impl/ob/dr;

    invoke-virtual {v0}, Lcom/yandex/metrica/impl/ob/dr;->a()V

    .line 81
    :cond_3
    return-void

    :cond_4
    move v0, v2

    .line 1128
    goto :goto_0

    :cond_5
    move v3, v2

    .line 1144
    goto :goto_1

    :cond_6
    move v4, v2

    .line 2156
    goto :goto_2

    :cond_7
    move v2, v1

    .line 1113
    goto :goto_3
.end method

.method public a(Lcom/yandex/metrica/impl/ob/dv;)V
    .locals 0

    .prologue
    .line 184
    iput-object p1, p0, Lcom/yandex/metrica/impl/ob/ed;->b:Lcom/yandex/metrica/impl/ob/dv;

    .line 185
    return-void
.end method
