.class public interface abstract Lru/cn/tv/player/SimplePlayerFragment$SimplePlayerFragmentListener;
.super Ljava/lang/Object;
.source "SimplePlayerFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/tv/player/SimplePlayerFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SimplePlayerFragmentListener"
.end annotation


# virtual methods
.method public abstract adStarted()V
.end method

.method public abstract adStopped()V
.end method

.method public abstract channelChanged(J)V
.end method

.method public abstract contractorChanged(J)V
.end method

.method public abstract hasSchedule(Z)V
.end method

.method public abstract minimize()V
.end method

.method public abstract onChannelInfoLoaded(JLjava/lang/String;ZIZ)V
.end method

.method public abstract onTelecastInfoLoaded(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract playing(Z)V
.end method

.method public abstract telecastChanged(J)V
.end method

.method public abstract videoSizeChanged(II)V
.end method
