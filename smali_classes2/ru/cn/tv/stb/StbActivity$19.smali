.class Lru/cn/tv/stb/StbActivity$19;
.super Ljava/lang/Object;
.source "StbActivity.java"

# interfaces
.implements Lru/cn/tv/player/SimplePlayerFragment$SimplePlayerFragmentListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lru/cn/tv/stb/StbActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field prevChannelId:J

.field final synthetic this$0:Lru/cn/tv/stb/StbActivity;


# direct methods
.method constructor <init>(Lru/cn/tv/stb/StbActivity;)V
    .locals 2
    .param p1, "this$0"    # Lru/cn/tv/stb/StbActivity;

    .prologue
    .line 1418
    iput-object p1, p0, Lru/cn/tv/stb/StbActivity$19;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1491
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lru/cn/tv/stb/StbActivity$19;->prevChannelId:J

    return-void
.end method


# virtual methods
.method public adStarted()V
    .locals 2

    .prologue
    .line 1450
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$19;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$100(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/player/controller/StbPlayerController;

    move-result-object v0

    invoke-virtual {v0}, Lru/cn/tv/player/controller/StbPlayerController;->hide()V

    .line 1451
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$19;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$1100(Lru/cn/tv/stb/StbActivity;)V

    .line 1452
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$19;->this$0:Lru/cn/tv/stb/StbActivity;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lru/cn/tv/stb/StbActivity;->access$2702(Lru/cn/tv/stb/StbActivity;Z)Z

    .line 1453
    return-void
.end method

.method public adStopped()V
    .locals 2

    .prologue
    .line 1445
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$19;->this$0:Lru/cn/tv/stb/StbActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lru/cn/tv/stb/StbActivity;->access$2702(Lru/cn/tv/stb/StbActivity;Z)Z

    .line 1446
    return-void
.end method

.method public channelChanged(J)V
    .locals 3
    .param p1, "cnId"    # J

    .prologue
    .line 1434
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$19;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$100(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/player/controller/StbPlayerController;

    move-result-object v0

    invoke-virtual {v0}, Lru/cn/tv/player/controller/StbPlayerController;->hide()V

    .line 1435
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$19;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$3900(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/stb/channels/ChannelsFragment;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lru/cn/tv/stb/channels/ChannelsFragment;->setCurrentChannel(J)V

    .line 1437
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$19;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$100(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/player/controller/StbPlayerController;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lru/cn/tv/player/controller/StbPlayerController;->setChannel(J)V

    .line 1438
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$19;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$100(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/player/controller/StbPlayerController;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lru/cn/tv/player/controller/StbPlayerController;->showChannelBlocked(Z)V

    .line 1440
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$19;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$100(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/player/controller/StbPlayerController;

    move-result-object v0

    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$19;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$000(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/player/SimplePlayerFragment;

    move-result-object v1

    invoke-virtual {v1}, Lru/cn/tv/player/SimplePlayerFragment;->getFitMode()Lru/cn/player/SimplePlayer$FitMode;

    move-result-object v1

    invoke-virtual {v0, v1}, Lru/cn/tv/player/controller/StbPlayerController;->setFitMode(Lru/cn/player/SimplePlayer$FitMode;)V

    .line 1441
    return-void
.end method

.method public contractorChanged(J)V
    .locals 0
    .param p1, "contractorId"    # J

    .prologue
    .line 1457
    return-void
.end method

.method public hasSchedule(Z)V
    .locals 0
    .param p1, "hasSchedule"    # Z

    .prologue
    .line 1537
    return-void
.end method

.method public minimize()V
    .locals 0

    .prologue
    .line 1542
    return-void
.end method

.method public onChannelInfoLoaded(JLjava/lang/String;ZIZ)V
    .locals 7
    .param p1, "channelId"    # J
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "isFavourite"    # Z
    .param p5, "number"    # I
    .param p6, "isDenied"    # Z

    .prologue
    const/4 v6, 0x5

    const/4 v0, 0x0

    .line 1496
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$19;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$4000(Lru/cn/tv/stb/StbActivity;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1497
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$19;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$100(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/player/controller/StbPlayerController;

    move-result-object v1

    invoke-virtual {v1}, Lru/cn/tv/player/controller/StbPlayerController;->hide()V

    .line 1500
    :cond_0
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$19;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$000(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/player/SimplePlayerFragment;

    move-result-object v1

    invoke-virtual {v1}, Lru/cn/tv/player/SimplePlayerFragment;->isPornoChannelPlayed()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$19;->this$0:Lru/cn/tv/stb/StbActivity;

    .line 1501
    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$000(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/player/SimplePlayerFragment;

    move-result-object v1

    invoke-virtual {v1}, Lru/cn/tv/player/SimplePlayerFragment;->isIntersectionChannelPlayed()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1502
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$19;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$4400(Lru/cn/tv/stb/StbActivity;)Lru/cn/domain/LastChannel;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lru/cn/domain/LastChannel;->saveLastChannel(J)V

    .line 1505
    :cond_1
    const-string v1, "StbActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Channel info loaded for cn id "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1506
    iget-wide v2, p0, Lru/cn/tv/stb/StbActivity$19;->prevChannelId:J

    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    iget-wide v2, p0, Lru/cn/tv/stb/StbActivity$19;->prevChannelId:J

    cmp-long v1, v2, p1

    if-eqz v1, :cond_3

    .line 1507
    :cond_2
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$19;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$1900(Lru/cn/tv/stb/StbActivity;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 1509
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$19;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1, v0}, Lru/cn/tv/stb/StbActivity;->access$4502(Lru/cn/tv/stb/StbActivity;Z)Z

    .line 1510
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$19;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$4600(Lru/cn/tv/stb/StbActivity;)Landroid/widget/TextView;

    move-result-object v1

    invoke-static {p5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1511
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$19;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$4600(Lru/cn/tv/stb/StbActivity;)Landroid/widget/TextView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1513
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$19;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$1900(Lru/cn/tv/stb/StbActivity;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 1514
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$19;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$1900(Lru/cn/tv/stb/StbActivity;)Landroid/os/Handler;

    move-result-object v1

    const-wide/16 v2, 0x2710

    invoke-virtual {v1, v6, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1516
    :cond_3
    iput-wide p1, p0, Lru/cn/tv/stb/StbActivity$19;->prevChannelId:J

    .line 1518
    if-eqz p6, :cond_5

    .line 1519
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$19;->this$0:Lru/cn/tv/stb/StbActivity;

    iget-object v2, p0, Lru/cn/tv/stb/StbActivity$19;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v2}, Lru/cn/tv/stb/StbActivity;->access$2900(Lru/cn/tv/stb/StbActivity;)Lru/cn/domain/tv/CurrentCategory$Type;

    move-result-object v2

    sget-object v3, Lru/cn/domain/tv/CurrentCategory$Type;->fav:Lru/cn/domain/tv/CurrentCategory$Type;

    if-ne v2, v3, :cond_4

    const/4 v0, 0x1

    :cond_4
    invoke-static {v1, v0}, Lru/cn/tv/stb/StbActivity;->access$4700(Lru/cn/tv/stb/StbActivity;Z)V

    .line 1521
    :cond_5
    return-void
.end method

.method public onTelecastInfoLoaded(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "description"    # Ljava/lang/String;

    .prologue
    .line 1525
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$19;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$4000(Lru/cn/tv/stb/StbActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1526
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$19;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$100(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/player/controller/StbPlayerController;

    move-result-object v0

    invoke-virtual {v0}, Lru/cn/tv/player/controller/StbPlayerController;->hide()V

    .line 1528
    :cond_0
    return-void
.end method

.method public playing(Z)V
    .locals 9
    .param p1, "playing"    # Z

    .prologue
    const v8, 0x7f0c006b

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1461
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$19;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1, p1}, Lru/cn/tv/stb/StbActivity;->access$4002(Lru/cn/tv/stb/StbActivity;Z)Z

    .line 1462
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$19;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$4000(Lru/cn/tv/stb/StbActivity;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1463
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$19;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$4100(Lru/cn/tv/stb/StbActivity;)V

    .line 1465
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$19;->this$0:Lru/cn/tv/stb/StbActivity;

    iget-object v2, p0, Lru/cn/tv/stb/StbActivity$19;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v2}, Lru/cn/tv/stb/StbActivity;->access$000(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/player/SimplePlayerFragment;

    move-result-object v2

    invoke-virtual {v2}, Lru/cn/tv/player/SimplePlayerFragment;->getAudioTrackInfoProvider()Lru/cn/player/ITrackSelector;

    move-result-object v2

    invoke-static {v1, v2}, Lru/cn/tv/stb/StbActivity;->access$2202(Lru/cn/tv/stb/StbActivity;Lru/cn/player/ITrackSelector;)Lru/cn/player/ITrackSelector;

    .line 1466
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$19;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$2200(Lru/cn/tv/stb/StbActivity;)Lru/cn/player/ITrackSelector;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$19;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$2200(Lru/cn/tv/stb/StbActivity;)Lru/cn/player/ITrackSelector;

    move-result-object v1

    invoke-interface {v1}, Lru/cn/player/ITrackSelector;->containsTracks()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1467
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$19;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$100(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/player/controller/StbPlayerController;

    move-result-object v1

    invoke-virtual {v1, v7}, Lru/cn/tv/player/controller/StbPlayerController;->showAudioTracks(Z)V

    .line 1468
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$19;->this$0:Lru/cn/tv/stb/StbActivity;

    new-instance v2, Lru/cn/tv/player/controller/TracksAdapter;

    iget-object v3, p0, Lru/cn/tv/stb/StbActivity$19;->this$0:Lru/cn/tv/stb/StbActivity;

    iget-object v4, p0, Lru/cn/tv/stb/StbActivity$19;->this$0:Lru/cn/tv/stb/StbActivity;

    .line 1469
    invoke-static {v4}, Lru/cn/tv/stb/StbActivity;->access$2200(Lru/cn/tv/stb/StbActivity;)Lru/cn/player/ITrackSelector;

    move-result-object v4

    iget-object v5, p0, Lru/cn/tv/stb/StbActivity$19;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v5}, Lru/cn/tv/stb/StbActivity;->access$4200(Lru/cn/tv/stb/StbActivity;)Lru/cn/player/ITrackSelector$TrackNameGenerator;

    move-result-object v5

    invoke-interface {v4, v5}, Lru/cn/player/ITrackSelector;->getTracksName(Lru/cn/player/ITrackSelector$TrackNameGenerator;)Ljava/util/List;

    move-result-object v4

    invoke-direct {v2, v3, v8, v4}, Lru/cn/tv/player/controller/TracksAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 1468
    invoke-static {v1, v2}, Lru/cn/tv/stb/StbActivity;->access$2102(Lru/cn/tv/stb/StbActivity;Lru/cn/tv/player/controller/TracksAdapter;)Lru/cn/tv/player/controller/TracksAdapter;

    .line 1474
    :goto_0
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$19;->this$0:Lru/cn/tv/stb/StbActivity;

    iget-object v2, p0, Lru/cn/tv/stb/StbActivity$19;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v2}, Lru/cn/tv/stb/StbActivity;->access$000(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/player/SimplePlayerFragment;

    move-result-object v2

    invoke-virtual {v2}, Lru/cn/tv/player/SimplePlayerFragment;->getSubtitlesTrackProvider()Lru/cn/player/ITrackSelector;

    move-result-object v2

    invoke-static {v1, v2}, Lru/cn/tv/stb/StbActivity;->access$2402(Lru/cn/tv/stb/StbActivity;Lru/cn/player/ITrackSelector;)Lru/cn/player/ITrackSelector;

    .line 1476
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$19;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$2400(Lru/cn/tv/stb/StbActivity;)Lru/cn/player/ITrackSelector;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$19;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$2400(Lru/cn/tv/stb/StbActivity;)Lru/cn/player/ITrackSelector;

    move-result-object v1

    invoke-interface {v1}, Lru/cn/player/ITrackSelector;->containsTracks()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1477
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$19;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$100(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/player/controller/StbPlayerController;

    move-result-object v1

    invoke-virtual {v1, v7}, Lru/cn/tv/player/controller/StbPlayerController;->showSubtitles(Z)V

    .line 1478
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$19;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$2400(Lru/cn/tv/stb/StbActivity;)Lru/cn/player/ITrackSelector;

    move-result-object v1

    iget-object v2, p0, Lru/cn/tv/stb/StbActivity$19;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v2}, Lru/cn/tv/stb/StbActivity;->access$4300(Lru/cn/tv/stb/StbActivity;)Lru/cn/player/ITrackSelector$TrackNameGenerator;

    move-result-object v2

    invoke-interface {v1, v2}, Lru/cn/player/ITrackSelector;->getTracksName(Lru/cn/player/ITrackSelector$TrackNameGenerator;)Ljava/util/List;

    move-result-object v0

    .line 1479
    .local v0, "subtitlesTracksName":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$19;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$2400(Lru/cn/tv/stb/StbActivity;)Lru/cn/player/ITrackSelector;

    move-result-object v1

    invoke-interface {v1}, Lru/cn/player/ITrackSelector;->deactivatable()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1480
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$19;->this$0:Lru/cn/tv/stb/StbActivity;

    const v2, 0x7f0e010b

    invoke-virtual {v1, v2}, Lru/cn/tv/stb/StbActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v6, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1482
    :cond_0
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$19;->this$0:Lru/cn/tv/stb/StbActivity;

    new-instance v2, Lru/cn/tv/player/controller/TracksAdapter;

    iget-object v3, p0, Lru/cn/tv/stb/StbActivity$19;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-direct {v2, v3, v8, v0}, Lru/cn/tv/player/controller/TracksAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    invoke-static {v1, v2}, Lru/cn/tv/stb/StbActivity;->access$2302(Lru/cn/tv/stb/StbActivity;Lru/cn/tv/player/controller/TracksAdapter;)Lru/cn/tv/player/controller/TracksAdapter;

    .line 1489
    .end local v0    # "subtitlesTracksName":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_1
    :goto_1
    return-void

    .line 1471
    :cond_2
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$19;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$100(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/player/controller/StbPlayerController;

    move-result-object v1

    invoke-virtual {v1, v6}, Lru/cn/tv/player/controller/StbPlayerController;->showAudioTracks(Z)V

    goto :goto_0

    .line 1485
    :cond_3
    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$19;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v1}, Lru/cn/tv/stb/StbActivity;->access$100(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/player/controller/StbPlayerController;

    move-result-object v1

    invoke-virtual {v1, v6}, Lru/cn/tv/player/controller/StbPlayerController;->showSubtitles(Z)V

    goto :goto_1
.end method

.method public telecastChanged(J)V
    .locals 7
    .param p1, "telecastId"    # J

    .prologue
    .line 1422
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$19;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$100(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/player/controller/StbPlayerController;

    move-result-object v0

    invoke-virtual {v0}, Lru/cn/tv/player/controller/StbPlayerController;->hide()V

    .line 1424
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$19;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$3700(Lru/cn/tv/stb/StbActivity;)Lru/cn/peersay/controllers/PlayContentController;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1425
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$19;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$000(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/player/SimplePlayerFragment;

    move-result-object v0

    invoke-virtual {v0}, Lru/cn/tv/player/SimplePlayerFragment;->getChannelId()J

    move-result-wide v2

    .line 1426
    .local v2, "channelId":J
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$19;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$3700(Lru/cn/tv/stb/StbActivity;)Lru/cn/peersay/controllers/PlayContentController;

    move-result-object v0

    iget-object v1, p0, Lru/cn/tv/stb/StbActivity$19;->this$0:Lru/cn/tv/stb/StbActivity;

    move-wide v4, p1

    invoke-virtual/range {v0 .. v5}, Lru/cn/peersay/controllers/PlayContentController;->onContentChanged(Landroid/content/Context;JJ)V

    .line 1429
    .end local v2    # "channelId":J
    :cond_0
    iget-object v0, p0, Lru/cn/tv/stb/StbActivity$19;->this$0:Lru/cn/tv/stb/StbActivity;

    invoke-static {v0}, Lru/cn/tv/stb/StbActivity;->access$3800(Lru/cn/tv/stb/StbActivity;)Lru/cn/tv/stb/StbViewModel;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lru/cn/tv/stb/StbViewModel;->setTelecast(J)V

    .line 1430
    return-void
.end method

.method public videoSizeChanged(II)V
    .locals 0
    .param p1, "w"    # I
    .param p2, "h"    # I

    .prologue
    .line 1532
    return-void
.end method
