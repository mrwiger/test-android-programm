.class final Lcom/samsung/multiscreen/HttpHelper$1;
.super Lcom/koushikdutta/async/http/AsyncHttpClient$StringCallback;
.source "HttpHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/multiscreen/HttpHelper;->createHttpCallback(Lcom/samsung/multiscreen/util/HttpUtil$ResultCreator;Lcom/samsung/multiscreen/Result;)Lcom/koushikdutta/async/http/AsyncHttpClient$StringCallback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$callback:Lcom/samsung/multiscreen/Result;

.field final synthetic val$resultCreator:Lcom/samsung/multiscreen/util/HttpUtil$ResultCreator;


# direct methods
.method constructor <init>(Lcom/samsung/multiscreen/Result;Lcom/samsung/multiscreen/util/HttpUtil$ResultCreator;)V
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/samsung/multiscreen/HttpHelper$1;->val$callback:Lcom/samsung/multiscreen/Result;

    iput-object p2, p0, Lcom/samsung/multiscreen/HttpHelper$1;->val$resultCreator:Lcom/samsung/multiscreen/util/HttpUtil$ResultCreator;

    invoke-direct {p0}, Lcom/koushikdutta/async/http/AsyncHttpClient$StringCallback;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompleted(Ljava/lang/Exception;Lcom/koushikdutta/async/http/AsyncHttpResponse;Ljava/lang/String;)V
    .locals 8
    .param p1, "exception"    # Ljava/lang/Exception;
    .param p2, "response"    # Lcom/koushikdutta/async/http/AsyncHttpResponse;
    .param p3, "data"    # Ljava/lang/String;

    .prologue
    .line 48
    if-eqz p1, :cond_0

    .line 50
    new-instance v1, Lcom/samsung/multiscreen/HttpHelper$1$1;

    invoke-direct {v1, p0, p1}, Lcom/samsung/multiscreen/HttpHelper$1$1;-><init>(Lcom/samsung/multiscreen/HttpHelper$1;Ljava/lang/Exception;)V

    .line 105
    .local v1, "runnable":Ljava/lang/Runnable;
    :goto_0
    invoke-static {v1}, Lcom/samsung/multiscreen/util/RunUtil;->runOnUI(Ljava/lang/Runnable;)V

    .line 106
    return-void

    .line 60
    .end local v1    # "runnable":Ljava/lang/Runnable;
    :cond_0
    :try_start_0
    invoke-interface {p2}, Lcom/koushikdutta/async/http/AsyncHttpResponse;->code()I

    move-result v2

    int-to-long v4, v2

    .line 61
    .local v4, "status":J
    invoke-static {p3}, Lcom/samsung/multiscreen/util/JSONUtil;->parse(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v3

    .line 63
    .local v3, "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const-wide/16 v6, 0xc8

    cmp-long v2, v4, v6

    if-eqz v2, :cond_1

    .line 64
    new-instance v1, Lcom/samsung/multiscreen/HttpHelper$1$2;

    move-object v2, p0

    move-object v6, p2

    invoke-direct/range {v1 .. v6}, Lcom/samsung/multiscreen/HttpHelper$1$2;-><init>(Lcom/samsung/multiscreen/HttpHelper$1;Ljava/util/Map;JLcom/koushikdutta/async/http/AsyncHttpResponse;)V

    .restart local v1    # "runnable":Ljava/lang/Runnable;
    goto :goto_0

    .line 80
    .end local v1    # "runnable":Ljava/lang/Runnable;
    :cond_1
    new-instance v1, Lcom/samsung/multiscreen/HttpHelper$1$3;

    invoke-direct {v1, p0, v3}, Lcom/samsung/multiscreen/HttpHelper$1$3;-><init>(Lcom/samsung/multiscreen/HttpHelper$1;Ljava/util/Map;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .restart local v1    # "runnable":Ljava/lang/Runnable;
    goto :goto_0

    .line 94
    .end local v1    # "runnable":Ljava/lang/Runnable;
    .end local v3    # "map":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v4    # "status":J
    :catch_0
    move-exception v0

    .line 95
    .local v0, "e":Ljava/lang/Exception;
    new-instance v1, Lcom/samsung/multiscreen/HttpHelper$1$4;

    invoke-direct {v1, p0, v0}, Lcom/samsung/multiscreen/HttpHelper$1$4;-><init>(Lcom/samsung/multiscreen/HttpHelper$1;Ljava/lang/Exception;)V

    .restart local v1    # "runnable":Ljava/lang/Runnable;
    goto :goto_0
.end method

.method public bridge synthetic onCompleted(Ljava/lang/Exception;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 42
    check-cast p2, Lcom/koushikdutta/async/http/AsyncHttpResponse;

    check-cast p3, Ljava/lang/String;

    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/multiscreen/HttpHelper$1;->onCompleted(Ljava/lang/Exception;Lcom/koushikdutta/async/http/AsyncHttpResponse;Ljava/lang/String;)V

    return-void
.end method
