.class public final Lcom/google/obf/at;
.super Ljava/lang/Object;
.source "IMASDK"

# interfaces
.implements Lcom/google/obf/aj;
.implements Lcom/google/obf/aq;


# static fields
.field private static final d:I


# instance fields
.field public a:I

.field public b:I

.field public c:J

.field private final e:Lcom/google/obf/dw;

.field private final g:Lcom/google/obf/dw;

.field private final h:Lcom/google/obf/dw;

.field private final i:Lcom/google/obf/dw;

.field private j:Lcom/google/obf/al;

.field private k:I

.field private l:I

.field private m:Lcom/google/obf/as;

.field private n:Lcom/google/obf/aw;

.field private o:Lcom/google/obf/au;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 97
    const-string v0, "FLV"

    invoke-static {v0}, Lcom/google/obf/ea;->c(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/google/obf/at;->d:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2
    new-instance v0, Lcom/google/obf/dw;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Lcom/google/obf/dw;-><init>(I)V

    iput-object v0, p0, Lcom/google/obf/at;->e:Lcom/google/obf/dw;

    .line 3
    new-instance v0, Lcom/google/obf/dw;

    const/16 v1, 0x9

    invoke-direct {v0, v1}, Lcom/google/obf/dw;-><init>(I)V

    iput-object v0, p0, Lcom/google/obf/at;->g:Lcom/google/obf/dw;

    .line 4
    new-instance v0, Lcom/google/obf/dw;

    const/16 v1, 0xb

    invoke-direct {v0, v1}, Lcom/google/obf/dw;-><init>(I)V

    iput-object v0, p0, Lcom/google/obf/at;->h:Lcom/google/obf/dw;

    .line 5
    new-instance v0, Lcom/google/obf/dw;

    invoke-direct {v0}, Lcom/google/obf/dw;-><init>()V

    iput-object v0, p0, Lcom/google/obf/at;->i:Lcom/google/obf/dw;

    .line 6
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/obf/at;->k:I

    .line 7
    return-void
.end method

.method private b(Lcom/google/obf/ak;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/16 v5, 0x9

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 40
    iget-object v2, p0, Lcom/google/obf/at;->g:Lcom/google/obf/dw;

    iget-object v2, v2, Lcom/google/obf/dw;->a:[B

    invoke-interface {p1, v2, v0, v5, v1}, Lcom/google/obf/ak;->a([BIIZ)Z

    move-result v2

    if-nez v2, :cond_0

    .line 57
    :goto_0
    return v0

    .line 42
    :cond_0
    iget-object v2, p0, Lcom/google/obf/at;->g:Lcom/google/obf/dw;

    invoke-virtual {v2, v0}, Lcom/google/obf/dw;->c(I)V

    .line 43
    iget-object v2, p0, Lcom/google/obf/at;->g:Lcom/google/obf/dw;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lcom/google/obf/dw;->d(I)V

    .line 44
    iget-object v2, p0, Lcom/google/obf/at;->g:Lcom/google/obf/dw;

    invoke-virtual {v2}, Lcom/google/obf/dw;->f()I

    move-result v3

    .line 45
    and-int/lit8 v2, v3, 0x4

    if-eqz v2, :cond_5

    move v2, v1

    .line 46
    :goto_1
    and-int/lit8 v3, v3, 0x1

    if-eqz v3, :cond_1

    move v0, v1

    .line 47
    :cond_1
    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/obf/at;->m:Lcom/google/obf/as;

    if-nez v2, :cond_2

    .line 48
    new-instance v2, Lcom/google/obf/as;

    iget-object v3, p0, Lcom/google/obf/at;->j:Lcom/google/obf/al;

    const/16 v4, 0x8

    invoke-interface {v3, v4}, Lcom/google/obf/al;->d(I)Lcom/google/obf/ar;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/obf/as;-><init>(Lcom/google/obf/ar;)V

    iput-object v2, p0, Lcom/google/obf/at;->m:Lcom/google/obf/as;

    .line 49
    :cond_2
    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/obf/at;->n:Lcom/google/obf/aw;

    if-nez v0, :cond_3

    .line 50
    new-instance v0, Lcom/google/obf/aw;

    iget-object v2, p0, Lcom/google/obf/at;->j:Lcom/google/obf/al;

    invoke-interface {v2, v5}, Lcom/google/obf/al;->d(I)Lcom/google/obf/ar;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/google/obf/aw;-><init>(Lcom/google/obf/ar;)V

    iput-object v0, p0, Lcom/google/obf/at;->n:Lcom/google/obf/aw;

    .line 51
    :cond_3
    iget-object v0, p0, Lcom/google/obf/at;->o:Lcom/google/obf/au;

    if-nez v0, :cond_4

    .line 52
    new-instance v0, Lcom/google/obf/au;

    const/4 v2, 0x0

    invoke-direct {v0, v2}, Lcom/google/obf/au;-><init>(Lcom/google/obf/ar;)V

    iput-object v0, p0, Lcom/google/obf/at;->o:Lcom/google/obf/au;

    .line 53
    :cond_4
    iget-object v0, p0, Lcom/google/obf/at;->j:Lcom/google/obf/al;

    invoke-interface {v0}, Lcom/google/obf/al;->f()V

    .line 54
    iget-object v0, p0, Lcom/google/obf/at;->j:Lcom/google/obf/al;

    invoke-interface {v0, p0}, Lcom/google/obf/al;->a(Lcom/google/obf/aq;)V

    .line 55
    iget-object v0, p0, Lcom/google/obf/at;->g:Lcom/google/obf/dw;

    invoke-virtual {v0}, Lcom/google/obf/dw;->m()I

    move-result v0

    add-int/lit8 v0, v0, -0x9

    add-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/obf/at;->l:I

    .line 56
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/obf/at;->k:I

    move v0, v1

    .line 57
    goto :goto_0

    :cond_5
    move v2, v0

    .line 45
    goto :goto_1
.end method

.method private c(Lcom/google/obf/ak;)V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 58
    iget v0, p0, Lcom/google/obf/at;->l:I

    invoke-interface {p1, v0}, Lcom/google/obf/ak;->b(I)V

    .line 59
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/obf/at;->l:I

    .line 60
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/obf/at;->k:I

    .line 61
    return-void
.end method

.method private d(Lcom/google/obf/ak;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 62
    iget-object v2, p0, Lcom/google/obf/at;->h:Lcom/google/obf/dw;

    iget-object v2, v2, Lcom/google/obf/dw;->a:[B

    const/16 v3, 0xb

    invoke-interface {p1, v2, v0, v3, v1}, Lcom/google/obf/ak;->a([BIIZ)Z

    move-result v2

    if-nez v2, :cond_0

    .line 71
    :goto_0
    return v0

    .line 64
    :cond_0
    iget-object v2, p0, Lcom/google/obf/at;->h:Lcom/google/obf/dw;

    invoke-virtual {v2, v0}, Lcom/google/obf/dw;->c(I)V

    .line 65
    iget-object v0, p0, Lcom/google/obf/at;->h:Lcom/google/obf/dw;

    invoke-virtual {v0}, Lcom/google/obf/dw;->f()I

    move-result v0

    iput v0, p0, Lcom/google/obf/at;->a:I

    .line 66
    iget-object v0, p0, Lcom/google/obf/at;->h:Lcom/google/obf/dw;

    invoke-virtual {v0}, Lcom/google/obf/dw;->j()I

    move-result v0

    iput v0, p0, Lcom/google/obf/at;->b:I

    .line 67
    iget-object v0, p0, Lcom/google/obf/at;->h:Lcom/google/obf/dw;

    invoke-virtual {v0}, Lcom/google/obf/dw;->j()I

    move-result v0

    int-to-long v2, v0

    iput-wide v2, p0, Lcom/google/obf/at;->c:J

    .line 68
    iget-object v0, p0, Lcom/google/obf/at;->h:Lcom/google/obf/dw;

    invoke-virtual {v0}, Lcom/google/obf/dw;->f()I

    move-result v0

    shl-int/lit8 v0, v0, 0x18

    int-to-long v2, v0

    iget-wide v4, p0, Lcom/google/obf/at;->c:J

    or-long/2addr v2, v4

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/obf/at;->c:J

    .line 69
    iget-object v0, p0, Lcom/google/obf/at;->h:Lcom/google/obf/dw;

    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Lcom/google/obf/dw;->d(I)V

    .line 70
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/obf/at;->k:I

    move v0, v1

    .line 71
    goto :goto_0
.end method

.method private e(Lcom/google/obf/ak;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 72
    const/4 v0, 0x1

    .line 73
    iget v1, p0, Lcom/google/obf/at;->a:I

    const/16 v2, 0x8

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/obf/at;->m:Lcom/google/obf/as;

    if-eqz v1, :cond_1

    .line 74
    iget-object v1, p0, Lcom/google/obf/at;->m:Lcom/google/obf/as;

    invoke-direct {p0, p1}, Lcom/google/obf/at;->f(Lcom/google/obf/ak;)Lcom/google/obf/dw;

    move-result-object v2

    iget-wide v4, p0, Lcom/google/obf/at;->c:J

    invoke-virtual {v1, v2, v4, v5}, Lcom/google/obf/as;->b(Lcom/google/obf/dw;J)V

    .line 86
    :cond_0
    :goto_0
    const/4 v1, 0x4

    iput v1, p0, Lcom/google/obf/at;->l:I

    .line 87
    const/4 v1, 0x2

    iput v1, p0, Lcom/google/obf/at;->k:I

    .line 88
    return v0

    .line 75
    :cond_1
    iget v1, p0, Lcom/google/obf/at;->a:I

    const/16 v2, 0x9

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/google/obf/at;->n:Lcom/google/obf/aw;

    if-eqz v1, :cond_2

    .line 76
    iget-object v1, p0, Lcom/google/obf/at;->n:Lcom/google/obf/aw;

    invoke-direct {p0, p1}, Lcom/google/obf/at;->f(Lcom/google/obf/ak;)Lcom/google/obf/dw;

    move-result-object v2

    iget-wide v4, p0, Lcom/google/obf/at;->c:J

    invoke-virtual {v1, v2, v4, v5}, Lcom/google/obf/aw;->b(Lcom/google/obf/dw;J)V

    goto :goto_0

    .line 77
    :cond_2
    iget v1, p0, Lcom/google/obf/at;->a:I

    const/16 v2, 0x12

    if-ne v1, v2, :cond_4

    iget-object v1, p0, Lcom/google/obf/at;->o:Lcom/google/obf/au;

    if-eqz v1, :cond_4

    .line 78
    iget-object v1, p0, Lcom/google/obf/at;->o:Lcom/google/obf/au;

    invoke-direct {p0, p1}, Lcom/google/obf/at;->f(Lcom/google/obf/ak;)Lcom/google/obf/dw;

    move-result-object v2

    iget-wide v4, p0, Lcom/google/obf/at;->c:J

    invoke-virtual {v1, v2, v4, v5}, Lcom/google/obf/au;->b(Lcom/google/obf/dw;J)V

    .line 79
    iget-object v1, p0, Lcom/google/obf/at;->o:Lcom/google/obf/au;

    invoke-virtual {v1}, Lcom/google/obf/au;->a()J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 80
    iget-object v1, p0, Lcom/google/obf/at;->m:Lcom/google/obf/as;

    if-eqz v1, :cond_3

    .line 81
    iget-object v1, p0, Lcom/google/obf/at;->m:Lcom/google/obf/as;

    iget-object v2, p0, Lcom/google/obf/at;->o:Lcom/google/obf/au;

    invoke-virtual {v2}, Lcom/google/obf/au;->a()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/google/obf/as;->a(J)V

    .line 82
    :cond_3
    iget-object v1, p0, Lcom/google/obf/at;->n:Lcom/google/obf/aw;

    if-eqz v1, :cond_0

    .line 83
    iget-object v1, p0, Lcom/google/obf/at;->n:Lcom/google/obf/aw;

    iget-object v2, p0, Lcom/google/obf/at;->o:Lcom/google/obf/au;

    invoke-virtual {v2}, Lcom/google/obf/au;->a()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/google/obf/aw;->a(J)V

    goto :goto_0

    .line 84
    :cond_4
    iget v0, p0, Lcom/google/obf/at;->b:I

    invoke-interface {p1, v0}, Lcom/google/obf/ak;->b(I)V

    .line 85
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f(Lcom/google/obf/ak;)Lcom/google/obf/dw;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 89
    iget v0, p0, Lcom/google/obf/at;->b:I

    iget-object v1, p0, Lcom/google/obf/at;->i:Lcom/google/obf/dw;

    invoke-virtual {v1}, Lcom/google/obf/dw;->e()I

    move-result v1

    if-le v0, v1, :cond_0

    .line 90
    iget-object v0, p0, Lcom/google/obf/at;->i:Lcom/google/obf/dw;

    iget-object v1, p0, Lcom/google/obf/at;->i:Lcom/google/obf/dw;

    invoke-virtual {v1}, Lcom/google/obf/dw;->e()I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    iget v2, p0, Lcom/google/obf/at;->b:I

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    new-array v1, v1, [B

    invoke-virtual {v0, v1, v3}, Lcom/google/obf/dw;->a([BI)V

    .line 92
    :goto_0
    iget-object v0, p0, Lcom/google/obf/at;->i:Lcom/google/obf/dw;

    iget v1, p0, Lcom/google/obf/at;->b:I

    invoke-virtual {v0, v1}, Lcom/google/obf/dw;->b(I)V

    .line 93
    iget-object v0, p0, Lcom/google/obf/at;->i:Lcom/google/obf/dw;

    iget-object v0, v0, Lcom/google/obf/dw;->a:[B

    iget v1, p0, Lcom/google/obf/at;->b:I

    invoke-interface {p1, v0, v3, v1}, Lcom/google/obf/ak;->b([BII)V

    .line 94
    iget-object v0, p0, Lcom/google/obf/at;->i:Lcom/google/obf/dw;

    return-object v0

    .line 91
    :cond_0
    iget-object v0, p0, Lcom/google/obf/at;->i:Lcom/google/obf/dw;

    invoke-virtual {v0, v3}, Lcom/google/obf/dw;->c(I)V

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/google/obf/ak;Lcom/google/obf/ao;)I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v0, -0x1

    .line 30
    :cond_0
    :goto_0
    iget v1, p0, Lcom/google/obf/at;->k:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 31
    :pswitch_0
    invoke-direct {p0, p1}, Lcom/google/obf/at;->b(Lcom/google/obf/ak;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 38
    :goto_1
    return v0

    .line 33
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/google/obf/at;->c(Lcom/google/obf/ak;)V

    goto :goto_0

    .line 35
    :pswitch_2
    invoke-direct {p0, p1}, Lcom/google/obf/at;->d(Lcom/google/obf/ak;)Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_1

    .line 37
    :pswitch_3
    invoke-direct {p0, p1}, Lcom/google/obf/at;->e(Lcom/google/obf/ak;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 38
    const/4 v0, 0x0

    goto :goto_1

    .line 30
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public a(Lcom/google/obf/al;)V
    .locals 0

    .prologue
    .line 24
    iput-object p1, p0, Lcom/google/obf/at;->j:Lcom/google/obf/al;

    .line 25
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 95
    const/4 v0, 0x0

    return v0
.end method

.method public a(Lcom/google/obf/ak;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x4

    const/4 v0, 0x0

    .line 8
    iget-object v1, p0, Lcom/google/obf/at;->e:Lcom/google/obf/dw;

    iget-object v1, v1, Lcom/google/obf/dw;->a:[B

    const/4 v2, 0x3

    invoke-interface {p1, v1, v0, v2}, Lcom/google/obf/ak;->c([BII)V

    .line 9
    iget-object v1, p0, Lcom/google/obf/at;->e:Lcom/google/obf/dw;

    invoke-virtual {v1, v0}, Lcom/google/obf/dw;->c(I)V

    .line 10
    iget-object v1, p0, Lcom/google/obf/at;->e:Lcom/google/obf/dw;

    invoke-virtual {v1}, Lcom/google/obf/dw;->j()I

    move-result v1

    sget v2, Lcom/google/obf/at;->d:I

    if-eq v1, v2, :cond_1

    .line 23
    :cond_0
    :goto_0
    return v0

    .line 12
    :cond_1
    iget-object v1, p0, Lcom/google/obf/at;->e:Lcom/google/obf/dw;

    iget-object v1, v1, Lcom/google/obf/dw;->a:[B

    const/4 v2, 0x2

    invoke-interface {p1, v1, v0, v2}, Lcom/google/obf/ak;->c([BII)V

    .line 13
    iget-object v1, p0, Lcom/google/obf/at;->e:Lcom/google/obf/dw;

    invoke-virtual {v1, v0}, Lcom/google/obf/dw;->c(I)V

    .line 14
    iget-object v1, p0, Lcom/google/obf/at;->e:Lcom/google/obf/dw;

    invoke-virtual {v1}, Lcom/google/obf/dw;->g()I

    move-result v1

    and-int/lit16 v1, v1, 0xfa

    if-nez v1, :cond_0

    .line 16
    iget-object v1, p0, Lcom/google/obf/at;->e:Lcom/google/obf/dw;

    iget-object v1, v1, Lcom/google/obf/dw;->a:[B

    invoke-interface {p1, v1, v0, v3}, Lcom/google/obf/ak;->c([BII)V

    .line 17
    iget-object v1, p0, Lcom/google/obf/at;->e:Lcom/google/obf/dw;

    invoke-virtual {v1, v0}, Lcom/google/obf/dw;->c(I)V

    .line 18
    iget-object v1, p0, Lcom/google/obf/at;->e:Lcom/google/obf/dw;

    invoke-virtual {v1}, Lcom/google/obf/dw;->m()I

    move-result v1

    .line 19
    invoke-interface {p1}, Lcom/google/obf/ak;->a()V

    .line 20
    invoke-interface {p1, v1}, Lcom/google/obf/ak;->c(I)V

    .line 21
    iget-object v1, p0, Lcom/google/obf/at;->e:Lcom/google/obf/dw;

    iget-object v1, v1, Lcom/google/obf/dw;->a:[B

    invoke-interface {p1, v1, v0, v3}, Lcom/google/obf/ak;->c([BII)V

    .line 22
    iget-object v1, p0, Lcom/google/obf/at;->e:Lcom/google/obf/dw;

    invoke-virtual {v1, v0}, Lcom/google/obf/dw;->c(I)V

    .line 23
    iget-object v1, p0, Lcom/google/obf/at;->e:Lcom/google/obf/dw;

    invoke-virtual {v1}, Lcom/google/obf/dw;->m()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b(J)J
    .locals 2

    .prologue
    .line 96
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/obf/at;->k:I

    .line 27
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/obf/at;->l:I

    .line 28
    return-void
.end method

.method public c()V
    .locals 0

    .prologue
    .line 29
    return-void
.end method
