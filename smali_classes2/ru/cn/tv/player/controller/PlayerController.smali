.class public Lru/cn/tv/player/controller/PlayerController;
.super Landroid/widget/FrameLayout;
.source "PlayerController.java"

# interfaces
.implements Landroid/os/Handler$Callback;
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lru/cn/tv/player/controller/PlayerController$RepeatedTouchListener;,
        Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;,
        Lru/cn/tv/player/controller/PlayerController$Companion;,
        Lru/cn/tv/player/controller/PlayerController$CompletionBehaviour;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/FrameLayout;",
        "Landroid/os/Handler$Callback;",
        "Landroid/support/v4/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field public alwaysShow:Z

.field public automaticTransition:Z

.field protected buttonFfwd:Landroid/widget/ImageButton;

.field protected buttonNext:Landroid/widget/ImageButton;

.field protected buttonPlayPause:Landroid/widget/ImageButton;

.field protected buttonPrev:Landroid/widget/ImageButton;

.field protected buttonRew:Landroid/widget/ImageButton;

.field protected companion:Lru/cn/tv/player/controller/PlayerController$Companion;

.field private completionBehaviour:Lru/cn/tv/player/controller/PlayerController$CompletionBehaviour;

.field private currentChannelCnId:J

.field protected currentTelecast:Lru/cn/api/tv/replies/Telecast;

.field protected favouriteStar:Lru/cn/tv/FavouriteStar;

.field protected hideTimeout:I

.field private mCurrentTime:Landroid/widget/TextView;

.field private mDragging:Z

.field private mEndTime:Landroid/widget/TextView;

.field private mFormatBuilder:Ljava/lang/StringBuilder;

.field private mFormatter:Ljava/util/Formatter;

.field private mHandler:Landroid/os/Handler;

.field private mPauseListener:Landroid/view/View$OnClickListener;

.field protected mPlayer:Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;

.field private mSeekListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field protected nextTelecast:Lru/cn/api/tv/replies/Telecast;

.field protected prevTelecast:Lru/cn/api/tv/replies/Telecast;

.field private rootView:Landroid/view/View;

.field protected seekBar:Lru/cn/view/ControllerSeekBar;

.field private seekPosition:I

.field private seekRepeatCount:I

.field private timeWrapper:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v5, 0x0

    .line 135
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 102
    const/16 v2, 0x1388

    iput v2, p0, Lru/cn/tv/player/controller/PlayerController;->hideTimeout:I

    .line 104
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v2, p0, Lru/cn/tv/player/controller/PlayerController;->mFormatBuilder:Ljava/lang/StringBuilder;

    .line 105
    new-instance v2, Ljava/util/Formatter;

    iget-object v3, p0, Lru/cn/tv/player/controller/PlayerController;->mFormatBuilder:Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;Ljava/util/Locale;)V

    iput-object v2, p0, Lru/cn/tv/player/controller/PlayerController;->mFormatter:Ljava/util/Formatter;

    .line 111
    const/4 v2, -0x1

    iput v2, p0, Lru/cn/tv/player/controller/PlayerController;->seekPosition:I

    .line 128
    iput-boolean v5, p0, Lru/cn/tv/player/controller/PlayerController;->alwaysShow:Z

    .line 129
    iput-boolean v5, p0, Lru/cn/tv/player/controller/PlayerController;->automaticTransition:Z

    .line 828
    new-instance v2, Lru/cn/tv/player/controller/PlayerController$$Lambda$0;

    invoke-direct {v2, p0}, Lru/cn/tv/player/controller/PlayerController$$Lambda$0;-><init>(Lru/cn/tv/player/controller/PlayerController;)V

    iput-object v2, p0, Lru/cn/tv/player/controller/PlayerController;->mPauseListener:Landroid/view/View$OnClickListener;

    .line 848
    new-instance v2, Lru/cn/tv/player/controller/PlayerController$3;

    invoke-direct {v2, p0}, Lru/cn/tv/player/controller/PlayerController$3;-><init>(Lru/cn/tv/player/controller/PlayerController;)V

    iput-object v2, p0, Lru/cn/tv/player/controller/PlayerController;->mSeekListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 137
    invoke-virtual {p0}, Lru/cn/tv/player/controller/PlayerController;->getLayout()I

    move-result v1

    .line 138
    .local v1, "layout":I
    if-nez v1, :cond_0

    .line 139
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Not set layout"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 142
    :cond_0
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2, p0}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v2, p0, Lru/cn/tv/player/controller/PlayerController;->mHandler:Landroid/os/Handler;

    .line 144
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 145
    .local v0, "inflater":Landroid/view/LayoutInflater;
    invoke-virtual {p0}, Lru/cn/tv/player/controller/PlayerController;->shouldHideRootContainer()Z

    move-result v2

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lru/cn/tv/player/controller/PlayerController;->rootView:Landroid/view/View;

    .line 146
    invoke-virtual {p0}, Lru/cn/tv/player/controller/PlayerController;->shouldHideRootContainer()Z

    move-result v2

    if-nez v2, :cond_1

    .line 147
    iget-object v2, p0, Lru/cn/tv/player/controller/PlayerController;->rootView:Landroid/view/View;

    invoke-virtual {p0, v2}, Lru/cn/tv/player/controller/PlayerController;->addView(Landroid/view/View;)V

    .line 149
    :cond_1
    return-void
.end method

.method static synthetic access$1000(Lru/cn/tv/player/controller/PlayerController;)V
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/player/controller/PlayerController;

    .prologue
    .line 39
    invoke-direct {p0}, Lru/cn/tv/player/controller/PlayerController;->updatePausePlay()V

    return-void
.end method

.method static synthetic access$302(Lru/cn/tv/player/controller/PlayerController;Z)Z
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/player/controller/PlayerController;
    .param p1, "x1"    # Z

    .prologue
    .line 39
    iput-boolean p1, p0, Lru/cn/tv/player/controller/PlayerController;->mDragging:Z

    return p1
.end method

.method static synthetic access$400(Lru/cn/tv/player/controller/PlayerController;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/player/controller/PlayerController;

    .prologue
    .line 39
    iget-object v0, p0, Lru/cn/tv/player/controller/PlayerController;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$500(Lru/cn/tv/player/controller/PlayerController;)Z
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/player/controller/PlayerController;

    .prologue
    .line 39
    invoke-direct {p0}, Lru/cn/tv/player/controller/PlayerController;->timeshiftable()Z

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lru/cn/tv/player/controller/PlayerController;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/player/controller/PlayerController;

    .prologue
    .line 39
    iget-object v0, p0, Lru/cn/tv/player/controller/PlayerController;->mCurrentTime:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$700(Lru/cn/tv/player/controller/PlayerController;I)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/player/controller/PlayerController;
    .param p1, "x1"    # I

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lru/cn/tv/player/controller/PlayerController;->stringForTime(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$802(Lru/cn/tv/player/controller/PlayerController;I)I
    .locals 0
    .param p0, "x0"    # Lru/cn/tv/player/controller/PlayerController;
    .param p1, "x1"    # I

    .prologue
    .line 39
    iput p1, p0, Lru/cn/tv/player/controller/PlayerController;->seekPosition:I

    return p1
.end method

.method static synthetic access$900(Lru/cn/tv/player/controller/PlayerController;)I
    .locals 1
    .param p0, "x0"    # Lru/cn/tv/player/controller/PlayerController;

    .prologue
    .line 39
    invoke-direct {p0}, Lru/cn/tv/player/controller/PlayerController;->setProgress()I

    move-result v0

    return v0
.end method

.method private loadNavigationItems()V
    .locals 10

    .prologue
    .line 764
    invoke-static {}, Lru/cn/domain/statistics/inetra/InetraTracker;->getCollectionId()J

    move-result-wide v2

    .line 766
    .local v2, "collectionId":J
    invoke-static {}, Lru/cn/domain/statistics/inetra/InetraTracker;->getViewFrom()Ljava/lang/Integer;

    move-result-object v5

    .line 767
    .local v5, "viewFrom":Ljava/lang/Integer;
    if-eqz v5, :cond_0

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v6

    if-nez v6, :cond_0

    .line 768
    iget-wide v2, p0, Lru/cn/tv/player/controller/PlayerController;->currentChannelCnId:J

    .line 772
    :cond_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 773
    .local v1, "bundle":Landroid/os/Bundle;
    const-string v6, "collectionId"

    invoke-virtual {v1, v6, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 774
    const-string v6, "telecastId"

    iget-object v7, p0, Lru/cn/tv/player/controller/PlayerController;->currentTelecast:Lru/cn/api/tv/replies/Telecast;

    iget-wide v8, v7, Lru/cn/api/tv/replies/Telecast;->id:J

    invoke-virtual {v1, v6, v8, v9}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 776
    invoke-virtual {p0}, Lru/cn/tv/player/controller/PlayerController;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    .line 777
    .local v0, "activity":Landroid/support/v4/app/FragmentActivity;
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v4

    .line 778
    .local v4, "loaderManager":Landroid/support/v4/app/LoaderManager;
    const/16 v6, 0xca

    invoke-virtual {v4, v6, v1, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 779
    return-void
.end method

.method private setProgress()I
    .locals 8

    .prologue
    const/4 v4, -0x1

    .line 790
    iget-object v5, p0, Lru/cn/tv/player/controller/PlayerController;->mPlayer:Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;

    if-eqz v5, :cond_0

    iget-boolean v5, p0, Lru/cn/tv/player/controller/PlayerController;->mDragging:Z

    if-eqz v5, :cond_2

    .line 791
    :cond_0
    const/4 v3, 0x0

    .line 825
    :cond_1
    :goto_0
    return v3

    .line 794
    :cond_2
    iget-object v5, p0, Lru/cn/tv/player/controller/PlayerController;->mPlayer:Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;

    invoke-interface {v5}, Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;->getCurrentPosition()I

    move-result v5

    div-int/lit16 v3, v5, 0x3e8

    .line 795
    .local v3, "position":I
    iget v5, p0, Lru/cn/tv/player/controller/PlayerController;->seekPosition:I

    if-eq v5, v4, :cond_3

    .line 796
    iget v5, p0, Lru/cn/tv/player/controller/PlayerController;->seekPosition:I

    div-int/lit16 v3, v5, 0x3e8

    .line 799
    :cond_3
    iget-object v5, p0, Lru/cn/tv/player/controller/PlayerController;->mPlayer:Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;

    invoke-interface {v5}, Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;->getDuration()I

    move-result v5

    div-int/lit16 v1, v5, 0x3e8

    .line 800
    .local v1, "duration":I
    iget-object v5, p0, Lru/cn/tv/player/controller/PlayerController;->seekBar:Lru/cn/view/ControllerSeekBar;

    if-eqz v5, :cond_5

    .line 801
    if-lez v1, :cond_5

    .line 802
    iget-object v5, p0, Lru/cn/tv/player/controller/PlayerController;->seekBar:Lru/cn/view/ControllerSeekBar;

    invoke-virtual {v5, v1}, Lru/cn/view/ControllerSeekBar;->setMax(I)V

    .line 804
    iget-object v5, p0, Lru/cn/tv/player/controller/PlayerController;->mPlayer:Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;

    invoke-interface {v5}, Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;->getBufferPosition()J

    move-result-wide v6

    long-to-int v5, v6

    div-int/lit16 v0, v5, 0x3e8

    .line 806
    .local v0, "bufferedPosition":I
    const-string v5, "PlayerController"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Set progress "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " of "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 807
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "(buffered position "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ")"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 806
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 809
    iget-object v5, p0, Lru/cn/tv/player/controller/PlayerController;->seekBar:Lru/cn/view/ControllerSeekBar;

    invoke-virtual {v5, v3}, Lru/cn/view/ControllerSeekBar;->setProgress(I)V

    .line 810
    iget-object v5, p0, Lru/cn/tv/player/controller/PlayerController;->seekBar:Lru/cn/view/ControllerSeekBar;

    invoke-virtual {v5, v0}, Lru/cn/view/ControllerSeekBar;->setSecondaryProgress(I)V

    .line 812
    iget-object v5, p0, Lru/cn/tv/player/controller/PlayerController;->mPlayer:Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;

    invoke-interface {v5}, Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;->getLivePosition()I

    move-result v2

    .line 813
    .local v2, "livePosition":I
    iget-object v5, p0, Lru/cn/tv/player/controller/PlayerController;->seekBar:Lru/cn/view/ControllerSeekBar;

    if-lez v2, :cond_4

    div-int/lit16 v4, v2, 0x3e8

    :cond_4
    invoke-virtual {v5, v4}, Lru/cn/view/ControllerSeekBar;->setLivePosition(I)V

    .line 817
    .end local v0    # "bufferedPosition":I
    .end local v2    # "livePosition":I
    :cond_5
    iget-object v4, p0, Lru/cn/tv/player/controller/PlayerController;->mEndTime:Landroid/widget/TextView;

    if-eqz v4, :cond_6

    .line 818
    iget-object v4, p0, Lru/cn/tv/player/controller/PlayerController;->mEndTime:Landroid/widget/TextView;

    invoke-direct {p0, v1}, Lru/cn/tv/player/controller/PlayerController;->stringForTime(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 821
    :cond_6
    iget-object v4, p0, Lru/cn/tv/player/controller/PlayerController;->mCurrentTime:Landroid/widget/TextView;

    if-eqz v4, :cond_1

    .line 822
    iget-object v4, p0, Lru/cn/tv/player/controller/PlayerController;->mCurrentTime:Landroid/widget/TextView;

    invoke-direct {p0, v3}, Lru/cn/tv/player/controller/PlayerController;->stringForTime(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method

.method private stringForTime(I)Ljava/lang/String;
    .locals 10
    .param p1, "timeSec"    # I

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 912
    rem-int/lit8 v2, p1, 0x3c

    .line 913
    .local v2, "seconds":I
    div-int/lit8 v3, p1, 0x3c

    rem-int/lit8 v1, v3, 0x3c

    .line 914
    .local v1, "minutes":I
    div-int/lit16 v0, p1, 0xe10

    .line 916
    .local v0, "hours":I
    iget-object v3, p0, Lru/cn/tv/player/controller/PlayerController;->mFormatBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 917
    if-lez v0, :cond_0

    .line 918
    iget-object v3, p0, Lru/cn/tv/player/controller/PlayerController;->mFormatter:Ljava/util/Formatter;

    const-string v4, "%d:%02d:%02d"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v9

    invoke-virtual {v3, v4, v5}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    move-result-object v3

    .line 919
    invoke-virtual {v3}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v3

    .line 921
    :goto_0
    return-object v3

    :cond_0
    iget-object v3, p0, Lru/cn/tv/player/controller/PlayerController;->mFormatter:Ljava/util/Formatter;

    const-string v4, "%02d:%02d"

    new-array v5, v9, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-virtual {v3, v4, v5}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method private timeshiftable()Z
    .locals 1

    .prologue
    .line 401
    invoke-virtual {p0}, Lru/cn/tv/player/controller/PlayerController;->seekable()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lru/cn/tv/player/controller/PlayerController;->mPlayer:Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;

    invoke-interface {v0}, Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;->getLivePosition()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updatePausePlay()V
    .locals 2

    .prologue
    .line 782
    iget-object v0, p0, Lru/cn/tv/player/controller/PlayerController;->mPlayer:Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;

    if-nez v0, :cond_0

    .line 787
    :goto_0
    return-void

    .line 786
    :cond_0
    iget-object v0, p0, Lru/cn/tv/player/controller/PlayerController;->buttonPlayPause:Landroid/widget/ImageButton;

    iget-object v1, p0, Lru/cn/tv/player/controller/PlayerController;->mPlayer:Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;

    invoke-interface {v1}, Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;->isPlaying()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setSelected(Z)V

    goto :goto_0
.end method


# virtual methods
.method public Ffwd()V
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v8, 0x2

    .line 581
    iget-object v5, p0, Lru/cn/tv/player/controller/PlayerController;->mPlayer:Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;

    if-nez v5, :cond_1

    .line 619
    :cond_0
    :goto_0
    return-void

    .line 585
    :cond_1
    iget-object v5, p0, Lru/cn/tv/player/controller/PlayerController;->mPlayer:Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;

    invoke-interface {v5}, Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;->getCurrentPosition()I

    move-result v0

    .line 586
    .local v0, "currentPosition":I
    iget-object v5, p0, Lru/cn/tv/player/controller/PlayerController;->mPlayer:Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;

    invoke-interface {v5}, Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;->getDuration()I

    move-result v1

    .line 587
    .local v1, "duration":I
    iget-object v5, p0, Lru/cn/tv/player/controller/PlayerController;->mPlayer:Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;

    invoke-interface {v5}, Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;->getLivePosition()I

    move-result v3

    .line 589
    .local v3, "livePosition":I
    if-ge v0, v1, :cond_0

    .line 593
    iget-object v5, p0, Lru/cn/tv/player/controller/PlayerController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v5, v8}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v5

    if-nez v5, :cond_6

    .line 594
    iput v2, p0, Lru/cn/tv/player/controller/PlayerController;->seekRepeatCount:I

    .line 600
    :goto_1
    iget v5, p0, Lru/cn/tv/player/controller/PlayerController;->seekRepeatCount:I

    if-nez v5, :cond_2

    .line 601
    iput v0, p0, Lru/cn/tv/player/controller/PlayerController;->seekPosition:I

    .line 604
    :cond_2
    const/16 v5, 0x2710

    iget v6, p0, Lru/cn/tv/player/controller/PlayerController;->seekRepeatCount:I

    div-int/lit8 v6, v6, 0xa

    invoke-static {v6, v8}, Ljava/lang/Math;->min(II)I

    move-result v6

    shl-int v4, v5, v6

    .line 605
    .local v4, "seek":I
    iget v5, p0, Lru/cn/tv/player/controller/PlayerController;->seekPosition:I

    add-int/2addr v5, v4

    iput v5, p0, Lru/cn/tv/player/controller/PlayerController;->seekPosition:I

    .line 607
    iget v5, p0, Lru/cn/tv/player/controller/PlayerController;->seekPosition:I

    if-le v5, v1, :cond_3

    .line 608
    iput v1, p0, Lru/cn/tv/player/controller/PlayerController;->seekPosition:I

    .line 611
    :cond_3
    if-lez v3, :cond_4

    if-ge v3, v1, :cond_4

    const/4 v2, 0x1

    .line 612
    .local v2, "isLive":Z
    :cond_4
    if-eqz v2, :cond_5

    iget v5, p0, Lru/cn/tv/player/controller/PlayerController;->seekPosition:I

    if-lt v5, v3, :cond_5

    .line 613
    iput v3, p0, Lru/cn/tv/player/controller/PlayerController;->seekPosition:I

    .line 616
    :cond_5
    iget-object v5, p0, Lru/cn/tv/player/controller/PlayerController;->mHandler:Landroid/os/Handler;

    const-wide/16 v6, 0x3e8

    invoke-virtual {v5, v8, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 618
    invoke-direct {p0}, Lru/cn/tv/player/controller/PlayerController;->setProgress()I

    goto :goto_0

    .line 596
    .end local v2    # "isLive":Z
    .end local v4    # "seek":I
    :cond_6
    iget v5, p0, Lru/cn/tv/player/controller/PlayerController;->seekRepeatCount:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lru/cn/tv/player/controller/PlayerController;->seekRepeatCount:I

    .line 597
    iget-object v5, p0, Lru/cn/tv/player/controller/PlayerController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v5, v8}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_1
.end method

.method public Rew()V
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x2

    .line 554
    iget-object v1, p0, Lru/cn/tv/player/controller/PlayerController;->mPlayer:Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;

    if-nez v1, :cond_0

    .line 578
    :goto_0
    return-void

    .line 558
    :cond_0
    iget-object v1, p0, Lru/cn/tv/player/controller/PlayerController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v1

    if-nez v1, :cond_3

    .line 559
    iput v3, p0, Lru/cn/tv/player/controller/PlayerController;->seekRepeatCount:I

    .line 565
    :goto_1
    iget v1, p0, Lru/cn/tv/player/controller/PlayerController;->seekRepeatCount:I

    if-nez v1, :cond_1

    .line 566
    iget-object v1, p0, Lru/cn/tv/player/controller/PlayerController;->mPlayer:Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;

    invoke-interface {v1}, Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;->getCurrentPosition()I

    move-result v1

    iput v1, p0, Lru/cn/tv/player/controller/PlayerController;->seekPosition:I

    .line 569
    :cond_1
    const/16 v1, 0x2710

    iget v2, p0, Lru/cn/tv/player/controller/PlayerController;->seekRepeatCount:I

    div-int/lit8 v2, v2, 0xa

    invoke-static {v2, v4}, Ljava/lang/Math;->min(II)I

    move-result v2

    shl-int v0, v1, v2

    .line 570
    .local v0, "seek":I
    iget v1, p0, Lru/cn/tv/player/controller/PlayerController;->seekPosition:I

    sub-int/2addr v1, v0

    iput v1, p0, Lru/cn/tv/player/controller/PlayerController;->seekPosition:I

    .line 571
    iget v1, p0, Lru/cn/tv/player/controller/PlayerController;->seekPosition:I

    if-gez v1, :cond_2

    .line 572
    iput v3, p0, Lru/cn/tv/player/controller/PlayerController;->seekPosition:I

    .line 575
    :cond_2
    iget-object v1, p0, Lru/cn/tv/player/controller/PlayerController;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 577
    invoke-direct {p0}, Lru/cn/tv/player/controller/PlayerController;->setProgress()I

    goto :goto_0

    .line 561
    .end local v0    # "seek":I
    :cond_3
    iget v1, p0, Lru/cn/tv/player/controller/PlayerController;->seekRepeatCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lru/cn/tv/player/controller/PlayerController;->seekRepeatCount:I

    .line 562
    iget-object v1, p0, Lru/cn/tv/player/controller/PlayerController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_1
.end method

.method protected final canGoLive()Z
    .locals 2

    .prologue
    .line 409
    invoke-virtual {p0}, Lru/cn/tv/player/controller/PlayerController;->seekable()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lru/cn/tv/player/controller/PlayerController;->mPlayer:Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;

    invoke-interface {v0}, Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;->getLivePosition()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lru/cn/tv/player/controller/PlayerController;->mPlayer:Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;

    invoke-interface {v0}, Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;->getLivePosition()I

    move-result v0

    iget-object v1, p0, Lru/cn/tv/player/controller/PlayerController;->mPlayer:Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;

    invoke-interface {v1}, Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;->getDuration()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final canStartOver()Z
    .locals 2

    .prologue
    .line 405
    invoke-virtual {p0}, Lru/cn/tv/player/controller/PlayerController;->seekable()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lru/cn/tv/player/controller/PlayerController;->mPlayer:Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;

    invoke-interface {v0}, Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;->getCurrentPosition()I

    move-result v0

    const v1, 0xea60

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public contentCompleted()V
    .locals 4

    .prologue
    .line 622
    invoke-virtual {p0}, Lru/cn/tv/player/controller/PlayerController;->hasNextMedia()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lru/cn/tv/player/controller/PlayerController;->automaticTransition:Z

    if-eqz v0, :cond_1

    .line 623
    iget-object v0, p0, Lru/cn/tv/player/controller/PlayerController;->mPlayer:Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;

    iget-object v1, p0, Lru/cn/tv/player/controller/PlayerController;->nextTelecast:Lru/cn/api/tv/replies/Telecast;

    iget-wide v2, v1, Lru/cn/api/tv/replies/Telecast;->id:J

    const/4 v1, 0x1

    invoke-interface {v0, v2, v3, v1}, Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;->selectMedia(JZ)V

    .line 627
    :cond_0
    :goto_0
    return-void

    .line 624
    :cond_1
    iget-object v0, p0, Lru/cn/tv/player/controller/PlayerController;->completionBehaviour:Lru/cn/tv/player/controller/PlayerController$CompletionBehaviour;

    if-eqz v0, :cond_0

    .line 625
    iget-object v0, p0, Lru/cn/tv/player/controller/PlayerController;->completionBehaviour:Lru/cn/tv/player/controller/PlayerController$CompletionBehaviour;

    iget-object v1, p0, Lru/cn/tv/player/controller/PlayerController;->nextTelecast:Lru/cn/api/tv/replies/Telecast;

    invoke-interface {v0, v1}, Lru/cn/tv/player/controller/PlayerController$CompletionBehaviour;->onCompleted(Lru/cn/api/tv/replies/Telecast;)V

    goto :goto_0
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 5
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v4, 0x4

    .line 469
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 481
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0

    .line 471
    :pswitch_0
    iget-object v0, p0, Lru/cn/tv/player/controller/PlayerController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0

    .line 475
    :pswitch_1
    invoke-virtual {p0}, Lru/cn/tv/player/controller/PlayerController;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lru/cn/tv/player/controller/PlayerController;->alwaysShow:Z

    if-nez v0, :cond_0

    .line 476
    iget-object v0, p0, Lru/cn/tv/player/controller/PlayerController;->mHandler:Landroid/os/Handler;

    iget v1, p0, Lru/cn/tv/player/controller/PlayerController;->hideTimeout:I

    int-to-long v2, v1

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 469
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, 0x4

    .line 448
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 449
    .local v0, "handled":Z
    if-eqz v0, :cond_0

    .line 450
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 464
    :cond_0
    :goto_0
    :pswitch_0
    return v0

    .line 452
    :pswitch_1
    iget-object v1, p0, Lru/cn/tv/player/controller/PlayerController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0

    .line 457
    :pswitch_2
    invoke-virtual {p0}, Lru/cn/tv/player/controller/PlayerController;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lru/cn/tv/player/controller/PlayerController;->alwaysShow:Z

    if-nez v1, :cond_0

    .line 458
    iget-object v1, p0, Lru/cn/tv/player/controller/PlayerController;->mHandler:Landroid/os/Handler;

    iget v2, p0, Lru/cn/tv/player/controller/PlayerController;->hideTimeout:I

    int-to-long v2, v2

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 450
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public doPauseResume()V
    .locals 1

    .prologue
    .line 485
    iget-object v0, p0, Lru/cn/tv/player/controller/PlayerController;->mPlayer:Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;

    if-nez v0, :cond_0

    .line 495
    :goto_0
    return-void

    .line 489
    :cond_0
    iget-object v0, p0, Lru/cn/tv/player/controller/PlayerController;->mPlayer:Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;

    invoke-interface {v0}, Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 490
    iget-object v0, p0, Lru/cn/tv/player/controller/PlayerController;->mPlayer:Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;

    invoke-interface {v0}, Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;->pause()V

    .line 494
    :goto_1
    invoke-direct {p0}, Lru/cn/tv/player/controller/PlayerController;->updatePausePlay()V

    goto :goto_0

    .line 492
    :cond_1
    iget-object v0, p0, Lru/cn/tv/player/controller/PlayerController;->mPlayer:Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;

    invoke-interface {v0}, Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;->play()V

    goto :goto_1
.end method

.method protected getLayout()I
    .locals 1

    .prologue
    .line 209
    const/4 v0, 0x0

    return v0
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 729
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    move v0, v1

    .line 759
    :cond_0
    :goto_0
    return v0

    .line 731
    :pswitch_0
    invoke-direct {p0}, Lru/cn/tv/player/controller/PlayerController;->setProgress()I

    .line 733
    iget-object v1, p0, Lru/cn/tv/player/controller/PlayerController;->mPlayer:Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lru/cn/tv/player/controller/PlayerController;->mDragging:Z

    if-nez v1, :cond_0

    .line 735
    invoke-virtual {p0}, Lru/cn/tv/player/controller/PlayerController;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lru/cn/tv/player/controller/PlayerController;->mPlayer:Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;

    .line 736
    invoke-interface {v1}, Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 737
    iget-object v1, p0, Lru/cn/tv/player/controller/PlayerController;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 743
    :pswitch_1
    iget-object v2, p0, Lru/cn/tv/player/controller/PlayerController;->mPlayer:Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;

    if-eqz v2, :cond_0

    .line 744
    iget-object v2, p0, Lru/cn/tv/player/controller/PlayerController;->mPlayer:Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;

    iget v3, p0, Lru/cn/tv/player/controller/PlayerController;->seekPosition:I

    invoke-interface {v2, v3}, Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;->seekTo(I)V

    .line 745
    const/4 v2, -0x1

    iput v2, p0, Lru/cn/tv/player/controller/PlayerController;->seekPosition:I

    .line 746
    iput v1, p0, Lru/cn/tv/player/controller/PlayerController;->seekRepeatCount:I

    goto :goto_0

    .line 751
    :pswitch_2
    invoke-direct {p0}, Lru/cn/tv/player/controller/PlayerController;->loadNavigationItems()V

    goto :goto_0

    .line 755
    :pswitch_3
    invoke-virtual {p0}, Lru/cn/tv/player/controller/PlayerController;->hide()V

    goto :goto_0

    .line 729
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected final hasNextMedia()Z
    .locals 1

    .prologue
    .line 417
    iget-object v0, p0, Lru/cn/tv/player/controller/PlayerController;->nextTelecast:Lru/cn/api/tv/replies/Telecast;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final hasPreviousMedia()Z
    .locals 1

    .prologue
    .line 413
    iget-object v0, p0, Lru/cn/tv/player/controller/PlayerController;->prevTelecast:Lru/cn/api/tv/replies/Telecast;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hide()V
    .locals 2

    .prologue
    .line 431
    iget-object v0, p0, Lru/cn/tv/player/controller/PlayerController;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 432
    iget-object v0, p0, Lru/cn/tv/player/controller/PlayerController;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 434
    iget-object v0, p0, Lru/cn/tv/player/controller/PlayerController;->rootView:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 435
    return-void
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 438
    iget-object v0, p0, Lru/cn/tv/player/controller/PlayerController;->rootView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final synthetic lambda$new$2$PlayerController(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 829
    invoke-virtual {p0}, Lru/cn/tv/player/controller/PlayerController;->doPauseResume()V

    .line 830
    invoke-virtual {p0}, Lru/cn/tv/player/controller/PlayerController;->updateControls()V

    .line 831
    return-void
.end method

.method final synthetic lambda$onFinishInflate$0$PlayerController(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 311
    invoke-virtual {p0}, Lru/cn/tv/player/controller/PlayerController;->nextMedia()V

    return-void
.end method

.method final synthetic lambda$onFinishInflate$1$PlayerController(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 315
    invoke-virtual {p0}, Lru/cn/tv/player/controller/PlayerController;->prevMedia()V

    return-void
.end method

.method public nextMedia()V
    .locals 4

    .prologue
    .line 520
    iget-object v0, p0, Lru/cn/tv/player/controller/PlayerController;->mPlayer:Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;

    if-nez v0, :cond_0

    .line 533
    :goto_0
    return-void

    .line 524
    :cond_0
    invoke-virtual {p0}, Lru/cn/tv/player/controller/PlayerController;->canGoLive()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 525
    iget-object v0, p0, Lru/cn/tv/player/controller/PlayerController;->mPlayer:Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;

    iget-object v1, p0, Lru/cn/tv/player/controller/PlayerController;->mPlayer:Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;

    invoke-interface {v1}, Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;->getLivePosition()I

    move-result v1

    invoke-interface {v0, v1}, Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;->seekTo(I)V

    .line 532
    :goto_1
    invoke-virtual {p0}, Lru/cn/tv/player/controller/PlayerController;->updateControls()V

    goto :goto_0

    .line 526
    :cond_1
    invoke-virtual {p0}, Lru/cn/tv/player/controller/PlayerController;->hasNextMedia()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 527
    iget-object v0, p0, Lru/cn/tv/player/controller/PlayerController;->mPlayer:Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;

    iget-object v1, p0, Lru/cn/tv/player/controller/PlayerController;->nextTelecast:Lru/cn/api/tv/replies/Telecast;

    iget-wide v2, v1, Lru/cn/api/tv/replies/Telecast;->id:J

    const/4 v1, 0x0

    invoke-interface {v0, v2, v3, v1}, Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;->selectMedia(JZ)V

    goto :goto_1

    .line 529
    :cond_2
    const-string v0, "PlayerController"

    const-string v1, "Unhandled next action"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method protected onChannelChanged(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "image"    # Ljava/lang/String;

    .prologue
    .line 717
    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 7
    .param p1, "id"    # I
    .param p2, "args"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 631
    const/4 v2, 0x0

    .line 632
    .local v2, "uri":Landroid/net/Uri;
    packed-switch p1, :pswitch_data_0

    .line 647
    :goto_0
    new-instance v0, Landroid/support/v4/content/CursorLoader;

    invoke-virtual {p0}, Lru/cn/tv/player/controller/PlayerController;->getContext()Landroid/content/Context;

    move-result-object v1

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    .line 634
    :pswitch_0
    const-string v0, "cnId"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lru/cn/api/provider/TvContentProviderContract;->channel(J)Landroid/net/Uri;

    move-result-object v2

    .line 635
    goto :goto_0

    .line 638
    :pswitch_1
    const-string v0, "telecastId"

    .line 639
    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 638
    invoke-static {v0, v1}, Lru/cn/api/provider/TvContentProviderContract;->telecastUri(J)Landroid/net/Uri;

    move-result-object v2

    .line 640
    goto :goto_0

    .line 643
    :pswitch_2
    const-string v0, "collectionId"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    const-string v4, "telecastId"

    .line 644
    invoke-virtual {p2, v4}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 643
    invoke-static {v0, v1, v4, v5}, Lru/cn/api/provider/TvContentProviderContract;->prevNextTelecasts(JJ)Landroid/net/Uri;

    move-result-object v2

    goto :goto_0

    .line 632
    nop

    :pswitch_data_0
    .packed-switch 0xc8
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onFinishInflate()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x1e

    .line 271
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 272
    const v0, 0x7f090166

    invoke-virtual {p0, v0}, Lru/cn/tv/player/controller/PlayerController;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lru/cn/tv/player/controller/PlayerController;->buttonPlayPause:Landroid/widget/ImageButton;

    .line 273
    const v0, 0x7f0900d3

    invoke-virtual {p0, v0}, Lru/cn/tv/player/controller/PlayerController;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lru/cn/tv/player/controller/PlayerController;->buttonFfwd:Landroid/widget/ImageButton;

    .line 274
    const v0, 0x7f09017f

    invoke-virtual {p0, v0}, Lru/cn/tv/player/controller/PlayerController;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lru/cn/tv/player/controller/PlayerController;->buttonRew:Landroid/widget/ImageButton;

    .line 275
    const v0, 0x7f09013e

    invoke-virtual {p0, v0}, Lru/cn/tv/player/controller/PlayerController;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lru/cn/tv/player/controller/PlayerController;->buttonNext:Landroid/widget/ImageButton;

    .line 276
    const v0, 0x7f090174

    invoke-virtual {p0, v0}, Lru/cn/tv/player/controller/PlayerController;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lru/cn/tv/player/controller/PlayerController;->buttonPrev:Landroid/widget/ImageButton;

    .line 278
    const v0, 0x7f09011a

    invoke-virtual {p0, v0}, Lru/cn/tv/player/controller/PlayerController;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lru/cn/view/ControllerSeekBar;

    iput-object v0, p0, Lru/cn/tv/player/controller/PlayerController;->seekBar:Lru/cn/view/ControllerSeekBar;

    .line 280
    const v0, 0x7f0901d6

    invoke-virtual {p0, v0}, Lru/cn/tv/player/controller/PlayerController;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lru/cn/tv/player/controller/PlayerController;->timeWrapper:Landroid/view/View;

    .line 281
    const v0, 0x7f0901d4

    invoke-virtual {p0, v0}, Lru/cn/tv/player/controller/PlayerController;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lru/cn/tv/player/controller/PlayerController;->mEndTime:Landroid/widget/TextView;

    .line 282
    const v0, 0x7f0901d5

    invoke-virtual {p0, v0}, Lru/cn/tv/player/controller/PlayerController;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lru/cn/tv/player/controller/PlayerController;->mCurrentTime:Landroid/widget/TextView;

    .line 283
    const v0, 0x7f0900d2

    invoke-virtual {p0, v0}, Lru/cn/tv/player/controller/PlayerController;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lru/cn/tv/FavouriteStar;

    iput-object v0, p0, Lru/cn/tv/player/controller/PlayerController;->favouriteStar:Lru/cn/tv/FavouriteStar;

    .line 285
    iget-object v0, p0, Lru/cn/tv/player/controller/PlayerController;->buttonPlayPause:Landroid/widget/ImageButton;

    iget-object v1, p0, Lru/cn/tv/player/controller/PlayerController;->mPauseListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 289
    iget-object v0, p0, Lru/cn/tv/player/controller/PlayerController;->buttonRew:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    .line 290
    iget-object v0, p0, Lru/cn/tv/player/controller/PlayerController;->buttonRew:Landroid/widget/ImageButton;

    new-instance v1, Lru/cn/tv/player/controller/PlayerController$1;

    iget-object v2, p0, Lru/cn/tv/player/controller/PlayerController;->buttonRew:Landroid/widget/ImageButton;

    invoke-direct {v1, p0, v2, v4, v5}, Lru/cn/tv/player/controller/PlayerController$1;-><init>(Lru/cn/tv/player/controller/PlayerController;Landroid/view/View;J)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 298
    :cond_0
    iget-object v0, p0, Lru/cn/tv/player/controller/PlayerController;->buttonFfwd:Landroid/widget/ImageButton;

    if-eqz v0, :cond_1

    .line 299
    iget-object v0, p0, Lru/cn/tv/player/controller/PlayerController;->buttonFfwd:Landroid/widget/ImageButton;

    new-instance v1, Lru/cn/tv/player/controller/PlayerController$2;

    iget-object v2, p0, Lru/cn/tv/player/controller/PlayerController;->buttonFfwd:Landroid/widget/ImageButton;

    invoke-direct {v1, p0, v2, v4, v5}, Lru/cn/tv/player/controller/PlayerController$2;-><init>(Lru/cn/tv/player/controller/PlayerController;Landroid/view/View;J)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 308
    :cond_1
    iget-object v0, p0, Lru/cn/tv/player/controller/PlayerController;->seekBar:Lru/cn/view/ControllerSeekBar;

    iget-object v1, p0, Lru/cn/tv/player/controller/PlayerController;->mSeekListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Lru/cn/view/ControllerSeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 310
    iget-object v0, p0, Lru/cn/tv/player/controller/PlayerController;->buttonNext:Landroid/widget/ImageButton;

    if-eqz v0, :cond_2

    .line 311
    iget-object v0, p0, Lru/cn/tv/player/controller/PlayerController;->buttonNext:Landroid/widget/ImageButton;

    new-instance v1, Lru/cn/tv/player/controller/PlayerController$$Lambda$1;

    invoke-direct {v1, p0}, Lru/cn/tv/player/controller/PlayerController$$Lambda$1;-><init>(Lru/cn/tv/player/controller/PlayerController;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 314
    :cond_2
    iget-object v0, p0, Lru/cn/tv/player/controller/PlayerController;->buttonPrev:Landroid/widget/ImageButton;

    if-eqz v0, :cond_3

    .line 315
    iget-object v0, p0, Lru/cn/tv/player/controller/PlayerController;->buttonPrev:Landroid/widget/ImageButton;

    new-instance v1, Lru/cn/tv/player/controller/PlayerController$$Lambda$2;

    invoke-direct {v1, p0}, Lru/cn/tv/player/controller/PlayerController$$Lambda$2;-><init>(Lru/cn/tv/player/controller/PlayerController;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 317
    :cond_3
    return-void
.end method

.method public onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 16
    .param p2, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 652
    .local p1, "loader":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    invoke-virtual/range {p1 .. p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v13

    packed-switch v13, :pswitch_data_0

    .line 704
    .end local p2    # "cursor":Landroid/database/Cursor;
    :cond_0
    :goto_0
    return-void

    .line 654
    .restart local p2    # "cursor":Landroid/database/Cursor;
    :pswitch_0
    if-eqz p2, :cond_0

    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->getCount()I

    move-result v13

    if-lez v13, :cond_0

    .line 655
    check-cast p2, Landroid/database/CursorWrapper;

    .line 656
    .end local p2    # "cursor":Landroid/database/Cursor;
    invoke-virtual/range {p2 .. p2}, Landroid/database/CursorWrapper;->getWrappedCursor()Landroid/database/Cursor;

    move-result-object v2

    check-cast v2, Lru/cn/api/provider/cursor/ChannelCursor;

    .line 657
    .local v2, "c":Lru/cn/api/provider/cursor/ChannelCursor;
    invoke-virtual {v2}, Lru/cn/api/provider/cursor/ChannelCursor;->moveToFirst()Z

    .line 658
    invoke-virtual {v2}, Lru/cn/api/provider/cursor/ChannelCursor;->getChannelId()J

    move-result-wide v4

    .line 659
    .local v4, "cnId":J
    invoke-virtual {v2}, Lru/cn/api/provider/cursor/ChannelCursor;->getFavourite()I

    move-result v3

    .line 660
    .local v3, "favourite":I
    invoke-virtual {v2}, Lru/cn/api/provider/cursor/ChannelCursor;->getIsPorno()I

    move-result v13

    const/4 v14, 0x1

    if-ne v13, v14, :cond_1

    const/4 v8, 0x1

    .line 661
    .local v8, "isPorno":Z
    :goto_1
    invoke-virtual {v2}, Lru/cn/api/provider/cursor/ChannelCursor;->getIsIntersections()I

    move-result v13

    const/4 v14, 0x1

    if-ne v13, v14, :cond_2

    const/4 v7, 0x1

    .line 663
    .local v7, "isIntersection":Z
    :goto_2
    invoke-virtual {v2}, Lru/cn/api/provider/cursor/ChannelCursor;->getTitle()Ljava/lang/String;

    move-result-object v12

    .line 664
    .local v12, "title":Ljava/lang/String;
    invoke-virtual {v2}, Lru/cn/api/provider/cursor/ChannelCursor;->getImage()Ljava/lang/String;

    move-result-object v6

    .line 665
    .local v6, "imageURL":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v6}, Lru/cn/tv/player/controller/PlayerController;->onChannelChanged(Ljava/lang/String;Ljava/lang/String;)V

    .line 667
    move-object/from16 v0, p0

    iget-object v13, v0, Lru/cn/tv/player/controller/PlayerController;->favouriteStar:Lru/cn/tv/FavouriteStar;

    if-eqz v13, :cond_0

    .line 668
    if-nez v8, :cond_0

    if-nez v7, :cond_0

    .line 669
    move-object/from16 v0, p0

    iget-object v13, v0, Lru/cn/tv/player/controller/PlayerController;->favouriteStar:Lru/cn/tv/FavouriteStar;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Lru/cn/tv/FavouriteStar;->setVisibility(I)V

    .line 670
    move-object/from16 v0, p0

    iget-object v14, v0, Lru/cn/tv/player/controller/PlayerController;->favouriteStar:Lru/cn/tv/FavouriteStar;

    if-lez v3, :cond_3

    const/4 v13, 0x1

    :goto_3
    invoke-virtual {v14, v4, v5, v13}, Lru/cn/tv/FavouriteStar;->favourite(JZ)V

    goto :goto_0

    .line 660
    .end local v6    # "imageURL":Ljava/lang/String;
    .end local v7    # "isIntersection":Z
    .end local v8    # "isPorno":Z
    .end local v12    # "title":Ljava/lang/String;
    :cond_1
    const/4 v8, 0x0

    goto :goto_1

    .line 661
    .restart local v8    # "isPorno":Z
    :cond_2
    const/4 v7, 0x0

    goto :goto_2

    .line 670
    .restart local v6    # "imageURL":Ljava/lang/String;
    .restart local v7    # "isIntersection":Z
    .restart local v12    # "title":Ljava/lang/String;
    :cond_3
    const/4 v13, 0x0

    goto :goto_3

    .line 677
    .end local v2    # "c":Lru/cn/api/provider/cursor/ChannelCursor;
    .end local v3    # "favourite":I
    .end local v4    # "cnId":J
    .end local v6    # "imageURL":Ljava/lang/String;
    .end local v7    # "isIntersection":Z
    .end local v8    # "isPorno":Z
    .end local v12    # "title":Ljava/lang/String;
    .restart local p2    # "cursor":Landroid/database/Cursor;
    :pswitch_1
    if-eqz p2, :cond_0

    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->getCount()I

    move-result v13

    if-lez v13, :cond_0

    .line 678
    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->moveToFirst()Z

    .line 679
    const-string v13, "data"

    move-object/from16 v0, p2

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    move-object/from16 v0, p2

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 680
    .local v11, "t":Ljava/lang/String;
    invoke-static {v11}, Lru/cn/api/tv/replies/Telecast;->fromJson(Ljava/lang/String;)Lru/cn/api/tv/replies/Telecast;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lru/cn/tv/player/controller/PlayerController;->currentTelecast:Lru/cn/api/tv/replies/Telecast;

    .line 681
    move-object/from16 v0, p0

    iget-object v13, v0, Lru/cn/tv/player/controller/PlayerController;->currentTelecast:Lru/cn/api/tv/replies/Telecast;

    iget-object v13, v13, Lru/cn/api/tv/replies/Telecast;->channel:Lru/cn/api/tv/replies/Telecast$Channel;

    iget-wide v14, v13, Lru/cn/api/tv/replies/Telecast$Channel;->channelId:J

    move-object/from16 v0, p0

    iput-wide v14, v0, Lru/cn/tv/player/controller/PlayerController;->currentChannelCnId:J

    .line 682
    move-object/from16 v0, p0

    iget-object v13, v0, Lru/cn/tv/player/controller/PlayerController;->currentTelecast:Lru/cn/api/tv/replies/Telecast;

    move-object/from16 v0, p0

    invoke-virtual {v0, v13}, Lru/cn/tv/player/controller/PlayerController;->onTelecastChanged(Lru/cn/api/tv/replies/Telecast;)V

    .line 684
    invoke-direct/range {p0 .. p0}, Lru/cn/tv/player/controller/PlayerController;->loadNavigationItems()V

    .line 686
    invoke-virtual/range {p0 .. p0}, Lru/cn/tv/player/controller/PlayerController;->updateControls()V

    goto/16 :goto_0

    .line 691
    .end local v11    # "t":Ljava/lang/String;
    :pswitch_2
    if-eqz p2, :cond_0

    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->getCount()I

    move-result v13

    if-lez v13, :cond_0

    .line 692
    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->moveToFirst()Z

    .line 694
    const-string v13, "prev_telecast"

    move-object/from16 v0, p2

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    move-object/from16 v0, p2

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 695
    .local v10, "prev":Ljava/lang/String;
    const-string v13, "next_telecast"

    move-object/from16 v0, p2

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    move-object/from16 v0, p2

    invoke-interface {v0, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 696
    .local v9, "next":Ljava/lang/String;
    invoke-static {v10}, Lru/cn/api/tv/replies/Telecast;->fromJson(Ljava/lang/String;)Lru/cn/api/tv/replies/Telecast;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lru/cn/tv/player/controller/PlayerController;->prevTelecast:Lru/cn/api/tv/replies/Telecast;

    .line 697
    invoke-static {v9}, Lru/cn/api/tv/replies/Telecast;->fromJson(Ljava/lang/String;)Lru/cn/api/tv/replies/Telecast;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lru/cn/tv/player/controller/PlayerController;->nextTelecast:Lru/cn/api/tv/replies/Telecast;

    .line 699
    invoke-virtual/range {p0 .. p0}, Lru/cn/tv/player/controller/PlayerController;->updateControls()V

    goto/16 :goto_0

    .line 652
    nop

    :pswitch_data_0
    .packed-switch 0xc8
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public bridge synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 39
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lru/cn/tv/player/controller/PlayerController;->onLoadFinished(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 709
    .local p1, "arg0":Landroid/support/v4/content/Loader;, "Landroid/support/v4/content/Loader<Landroid/database/Cursor;>;"
    return-void
.end method

.method protected onTelecastChanged(Lru/cn/api/tv/replies/Telecast;)V
    .locals 0
    .param p1, "telecast"    # Lru/cn/api/tv/replies/Telecast;

    .prologue
    .line 713
    return-void
.end method

.method protected pause()V
    .locals 1

    .prologue
    .line 509
    iget-object v0, p0, Lru/cn/tv/player/controller/PlayerController;->mPlayer:Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;

    if-nez v0, :cond_0

    .line 517
    :goto_0
    return-void

    .line 513
    :cond_0
    iget-object v0, p0, Lru/cn/tv/player/controller/PlayerController;->mPlayer:Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;

    invoke-interface {v0}, Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 514
    iget-object v0, p0, Lru/cn/tv/player/controller/PlayerController;->mPlayer:Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;

    invoke-interface {v0}, Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;->pause()V

    .line 516
    :cond_1
    invoke-direct {p0}, Lru/cn/tv/player/controller/PlayerController;->updatePausePlay()V

    goto :goto_0
.end method

.method public performFavStarClick()V
    .locals 1

    .prologue
    .line 720
    iget-object v0, p0, Lru/cn/tv/player/controller/PlayerController;->favouriteStar:Lru/cn/tv/FavouriteStar;

    invoke-virtual {v0}, Lru/cn/tv/FavouriteStar;->performClick()Z

    .line 721
    return-void
.end method

.method protected play()V
    .locals 1

    .prologue
    .line 498
    iget-object v0, p0, Lru/cn/tv/player/controller/PlayerController;->mPlayer:Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;

    if-nez v0, :cond_0

    .line 506
    :goto_0
    return-void

    .line 502
    :cond_0
    iget-object v0, p0, Lru/cn/tv/player/controller/PlayerController;->mPlayer:Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;

    invoke-interface {v0}, Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_1

    .line 503
    iget-object v0, p0, Lru/cn/tv/player/controller/PlayerController;->mPlayer:Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;

    invoke-interface {v0}, Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;->play()V

    .line 505
    :cond_1
    invoke-direct {p0}, Lru/cn/tv/player/controller/PlayerController;->updatePausePlay()V

    goto :goto_0
.end method

.method public prevMedia()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 536
    iget-object v0, p0, Lru/cn/tv/player/controller/PlayerController;->mPlayer:Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;

    if-nez v0, :cond_0

    .line 551
    :goto_0
    return-void

    .line 540
    :cond_0
    invoke-virtual {p0}, Lru/cn/tv/player/controller/PlayerController;->canStartOver()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 541
    iget-object v0, p0, Lru/cn/tv/player/controller/PlayerController;->mPlayer:Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;

    invoke-interface {v0, v4}, Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;->seekTo(I)V

    .line 550
    :goto_1
    invoke-virtual {p0}, Lru/cn/tv/player/controller/PlayerController;->updateControls()V

    goto :goto_0

    .line 542
    :cond_1
    invoke-virtual {p0}, Lru/cn/tv/player/controller/PlayerController;->hasPreviousMedia()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 543
    iget-object v0, p0, Lru/cn/tv/player/controller/PlayerController;->mPlayer:Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;

    iget-object v1, p0, Lru/cn/tv/player/controller/PlayerController;->prevTelecast:Lru/cn/api/tv/replies/Telecast;

    iget-wide v2, v1, Lru/cn/api/tv/replies/Telecast;->id:J

    invoke-interface {v0, v2, v3, v4}, Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;->selectMedia(JZ)V

    goto :goto_1

    .line 544
    :cond_2
    invoke-virtual {p0}, Lru/cn/tv/player/controller/PlayerController;->seekable()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 545
    iget-object v0, p0, Lru/cn/tv/player/controller/PlayerController;->mPlayer:Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;

    invoke-interface {v0, v4}, Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;->seekTo(I)V

    goto :goto_1

    .line 547
    :cond_3
    const-string v0, "PlayerController"

    const-string v1, "Unhandled prev action"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public reset()V
    .locals 2

    .prologue
    .line 443
    iget-object v0, p0, Lru/cn/tv/player/controller/PlayerController;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 444
    return-void
.end method

.method protected final seekable()Z
    .locals 1

    .prologue
    .line 397
    iget-object v0, p0, Lru/cn/tv/player/controller/PlayerController;->mPlayer:Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;

    invoke-interface {v0}, Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;->isSeekable()Z

    move-result v0

    return v0
.end method

.method public setAlwaysShow(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 724
    iput-boolean p1, p0, Lru/cn/tv/player/controller/PlayerController;->alwaysShow:Z

    .line 725
    return-void
.end method

.method public final setChannel(J)V
    .locals 5
    .param p1, "cnId"    # J

    .prologue
    const/4 v4, 0x0

    .line 152
    iget-wide v2, p0, Lru/cn/tv/player/controller/PlayerController;->currentChannelCnId:J

    cmp-long v2, v2, p1

    if-eqz v2, :cond_0

    .line 153
    iput-wide p1, p0, Lru/cn/tv/player/controller/PlayerController;->currentChannelCnId:J

    .line 155
    iget-object v2, p0, Lru/cn/tv/player/controller/PlayerController;->favouriteStar:Lru/cn/tv/FavouriteStar;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lru/cn/tv/FavouriteStar;->setVisibility(I)V

    .line 156
    invoke-virtual {p0}, Lru/cn/tv/player/controller/PlayerController;->updateControls()V

    .line 158
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 159
    .local v1, "bundle":Landroid/os/Bundle;
    const-string v2, "cnId"

    invoke-virtual {v1, v2, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 160
    invoke-virtual {p0}, Lru/cn/tv/player/controller/PlayerController;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    .line 161
    .local v0, "activity":Landroid/support/v4/app/FragmentActivity;
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v2

    const/16 v3, 0xc8

    invoke-virtual {v2, v3, v1, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    .line 164
    invoke-virtual {p0, v4, v4}, Lru/cn/tv/player/controller/PlayerController;->onChannelChanged(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    .end local v0    # "activity":Landroid/support/v4/app/FragmentActivity;
    .end local v1    # "bundle":Landroid/os/Bundle;
    :cond_0
    return-void
.end method

.method public final setCompanion(Lru/cn/tv/player/controller/PlayerController$Companion;)V
    .locals 1
    .param p1, "companion"    # Lru/cn/tv/player/controller/PlayerController$Companion;

    .prologue
    .line 197
    iput-object p1, p0, Lru/cn/tv/player/controller/PlayerController;->companion:Lru/cn/tv/player/controller/PlayerController$Companion;

    .line 199
    if-eqz p1, :cond_0

    .line 200
    invoke-virtual {p0}, Lru/cn/tv/player/controller/PlayerController;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 201
    invoke-interface {p1}, Lru/cn/tv/player/controller/PlayerController$Companion;->hide()V

    .line 206
    :cond_0
    :goto_0
    return-void

    .line 203
    :cond_1
    invoke-interface {p1}, Lru/cn/tv/player/controller/PlayerController$Companion;->show()V

    goto :goto_0
.end method

.method public setCompletionBehaviour(Lru/cn/tv/player/controller/PlayerController$CompletionBehaviour;)V
    .locals 0
    .param p1, "behaviour"    # Lru/cn/tv/player/controller/PlayerController$CompletionBehaviour;

    .prologue
    .line 193
    iput-object p1, p0, Lru/cn/tv/player/controller/PlayerController;->completionBehaviour:Lru/cn/tv/player/controller/PlayerController$CompletionBehaviour;

    .line 194
    return-void
.end method

.method public setMediaPlayer(Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;)V
    .locals 0
    .param p1, "player"    # Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;

    .prologue
    .line 320
    iput-object p1, p0, Lru/cn/tv/player/controller/PlayerController;->mPlayer:Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;

    .line 321
    return-void
.end method

.method public final setTelecast(J)V
    .locals 9
    .param p1, "id"    # J

    .prologue
    const/16 v7, 0xc9

    const/4 v6, 0x0

    .line 169
    iget-object v3, p0, Lru/cn/tv/player/controller/PlayerController;->currentTelecast:Lru/cn/api/tv/replies/Telecast;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lru/cn/tv/player/controller/PlayerController;->currentTelecast:Lru/cn/api/tv/replies/Telecast;

    iget-wide v4, v3, Lru/cn/api/tv/replies/Telecast;->id:J

    cmp-long v3, v4, p1

    if-nez v3, :cond_0

    .line 190
    :goto_0
    return-void

    .line 172
    :cond_0
    iget-object v3, p0, Lru/cn/tv/player/controller/PlayerController;->mHandler:Landroid/os/Handler;

    const/4 v4, 0x3

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 173
    invoke-virtual {p0}, Lru/cn/tv/player/controller/PlayerController;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    .line 174
    .local v0, "activity":Landroid/support/v4/app/FragmentActivity;
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v2

    .line 175
    .local v2, "loaderManager":Landroid/support/v4/app/LoaderManager;
    const/16 v3, 0xca

    invoke-virtual {v2, v3}, Landroid/support/v4/app/LoaderManager;->destroyLoader(I)V

    .line 177
    iput-object v6, p0, Lru/cn/tv/player/controller/PlayerController;->currentTelecast:Lru/cn/api/tv/replies/Telecast;

    .line 178
    iput-object v6, p0, Lru/cn/tv/player/controller/PlayerController;->nextTelecast:Lru/cn/api/tv/replies/Telecast;

    .line 179
    iput-object v6, p0, Lru/cn/tv/player/controller/PlayerController;->prevTelecast:Lru/cn/api/tv/replies/Telecast;

    .line 180
    invoke-virtual {p0, v6}, Lru/cn/tv/player/controller/PlayerController;->onTelecastChanged(Lru/cn/api/tv/replies/Telecast;)V

    .line 181
    invoke-virtual {p0}, Lru/cn/tv/player/controller/PlayerController;->updateControls()V

    .line 183
    const-wide/16 v4, -0x1

    cmp-long v3, p1, v4

    if-nez v3, :cond_1

    .line 184
    invoke-virtual {v2, v7}, Landroid/support/v4/app/LoaderManager;->destroyLoader(I)V

    goto :goto_0

    .line 186
    :cond_1
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 187
    .local v1, "bundle":Landroid/os/Bundle;
    const-string v3, "telecastId"

    invoke-virtual {v1, v3, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 188
    invoke-virtual {v2, v7, v1, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    goto :goto_0
.end method

.method protected shouldHideRootContainer()Z
    .locals 1

    .prologue
    .line 216
    const/4 v0, 0x1

    return v0
.end method

.method public show()V
    .locals 5

    .prologue
    const/4 v4, 0x4

    .line 421
    invoke-virtual {p0}, Lru/cn/tv/player/controller/PlayerController;->updateControls()V

    .line 422
    iget-object v0, p0, Lru/cn/tv/player/controller/PlayerController;->rootView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 424
    iget-object v0, p0, Lru/cn/tv/player/controller/PlayerController;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 425
    iget-boolean v0, p0, Lru/cn/tv/player/controller/PlayerController;->alwaysShow:Z

    if-nez v0, :cond_0

    .line 426
    iget-object v0, p0, Lru/cn/tv/player/controller/PlayerController;->mHandler:Landroid/os/Handler;

    iget v1, p0, Lru/cn/tv/player/controller/PlayerController;->hideTimeout:I

    int-to-long v2, v1

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 428
    :cond_0
    return-void
.end method

.method public updateControls()V
    .locals 13

    .prologue
    .line 327
    invoke-virtual {p0}, Lru/cn/tv/player/controller/PlayerController;->seekable()Z

    move-result v3

    .line 328
    .local v3, "seekable":Z
    invoke-direct {p0}, Lru/cn/tv/player/controller/PlayerController;->timeshiftable()Z

    move-result v6

    .line 330
    .local v6, "timeshiftable":Z
    if-eqz v6, :cond_10

    iget-object v9, p0, Lru/cn/tv/player/controller/PlayerController;->mPlayer:Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;

    invoke-interface {v9}, Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;->getLivePosition()I

    move-result v9

    iget-object v10, p0, Lru/cn/tv/player/controller/PlayerController;->mPlayer:Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;

    invoke-interface {v10}, Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;->getCurrentPosition()I

    move-result v10

    sub-int/2addr v9, v10

    const/16 v10, 0x2710

    if-le v9, v10, :cond_10

    const/4 v7, 0x1

    .line 332
    .local v7, "timeshifted":Z
    :goto_0
    invoke-virtual {p0}, Lru/cn/tv/player/controller/PlayerController;->hasPreviousMedia()Z

    move-result v2

    .line 333
    .local v2, "hasPreviousItem":Z
    invoke-virtual {p0}, Lru/cn/tv/player/controller/PlayerController;->hasNextMedia()Z

    move-result v1

    .line 336
    .local v1, "hasNextItem":Z
    if-eqz v7, :cond_0

    if-nez v1, :cond_0

    .line 337
    iget-object v9, p0, Lru/cn/tv/player/controller/PlayerController;->mPlayer:Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;

    invoke-interface {v9}, Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;->getDuration()I

    move-result v9

    iget-object v10, p0, Lru/cn/tv/player/controller/PlayerController;->mPlayer:Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;

    invoke-interface {v10}, Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;->getLivePosition()I

    move-result v10

    sub-int v5, v9, v10

    .line 338
    .local v5, "timeToEndLive":I
    iget-object v9, p0, Lru/cn/tv/player/controller/PlayerController;->mHandler:Landroid/os/Handler;

    const/4 v10, 0x3

    invoke-virtual {v9, v10}, Landroid/os/Handler;->removeMessages(I)V

    .line 339
    iget-object v9, p0, Lru/cn/tv/player/controller/PlayerController;->mHandler:Landroid/os/Handler;

    const/4 v12, 0x3

    if-lez v5, :cond_11

    int-to-long v10, v5

    :goto_1
    invoke-virtual {v9, v12, v10, v11}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 342
    .end local v5    # "timeToEndLive":I
    :cond_0
    iget-object v9, p0, Lru/cn/tv/player/controller/PlayerController;->buttonNext:Landroid/widget/ImageButton;

    if-eqz v9, :cond_2

    .line 343
    iget-object v10, p0, Lru/cn/tv/player/controller/PlayerController;->buttonNext:Landroid/widget/ImageButton;

    if-nez v2, :cond_1

    if-nez v1, :cond_1

    if-eqz v6, :cond_12

    :cond_1
    const/4 v9, 0x0

    :goto_2
    invoke-virtual {v10, v9}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 346
    :cond_2
    iget-object v9, p0, Lru/cn/tv/player/controller/PlayerController;->buttonPrev:Landroid/widget/ImageButton;

    if-eqz v9, :cond_4

    .line 347
    iget-object v10, p0, Lru/cn/tv/player/controller/PlayerController;->buttonPrev:Landroid/widget/ImageButton;

    if-nez v2, :cond_3

    if-nez v1, :cond_3

    if-eqz v6, :cond_13

    :cond_3
    const/4 v9, 0x0

    :goto_3
    invoke-virtual {v10, v9}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 350
    :cond_4
    iget-object v9, p0, Lru/cn/tv/player/controller/PlayerController;->buttonFfwd:Landroid/widget/ImageButton;

    if-eqz v9, :cond_6

    .line 351
    if-eqz v3, :cond_14

    if-eqz v6, :cond_5

    if-eqz v7, :cond_14

    :cond_5
    const/4 v4, 0x1

    .line 352
    .local v4, "seekableForward":Z
    :goto_4
    iget-object v9, p0, Lru/cn/tv/player/controller/PlayerController;->buttonFfwd:Landroid/widget/ImageButton;

    invoke-virtual {v9, v4}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 353
    iget-object v9, p0, Lru/cn/tv/player/controller/PlayerController;->buttonFfwd:Landroid/widget/ImageButton;

    invoke-virtual {v9, v4}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 356
    .end local v4    # "seekableForward":Z
    :cond_6
    iget-object v9, p0, Lru/cn/tv/player/controller/PlayerController;->buttonRew:Landroid/widget/ImageButton;

    if-eqz v9, :cond_7

    .line 357
    iget-object v9, p0, Lru/cn/tv/player/controller/PlayerController;->buttonRew:Landroid/widget/ImageButton;

    invoke-virtual {v9, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 358
    iget-object v9, p0, Lru/cn/tv/player/controller/PlayerController;->buttonRew:Landroid/widget/ImageButton;

    invoke-virtual {v9, v3}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 361
    :cond_7
    iget-object v9, p0, Lru/cn/tv/player/controller/PlayerController;->mPlayer:Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;

    invoke-interface {v9}, Lru/cn/tv/player/controller/PlayerController$MediaPlayerControl;->getDuration()I

    move-result v9

    if-lez v9, :cond_15

    const/4 v8, 0x0

    .line 362
    .local v8, "visibility":I
    :goto_5
    iget-object v9, p0, Lru/cn/tv/player/controller/PlayerController;->seekBar:Lru/cn/view/ControllerSeekBar;

    invoke-virtual {v9, v8}, Lru/cn/view/ControllerSeekBar;->setVisibility(I)V

    .line 363
    iget-object v9, p0, Lru/cn/tv/player/controller/PlayerController;->seekBar:Lru/cn/view/ControllerSeekBar;

    invoke-virtual {v9, v3}, Lru/cn/view/ControllerSeekBar;->setThumbVisible(Z)V

    .line 365
    iget-object v9, p0, Lru/cn/tv/player/controller/PlayerController;->timeWrapper:Landroid/view/View;

    if-eqz v9, :cond_16

    .line 366
    iget-object v9, p0, Lru/cn/tv/player/controller/PlayerController;->timeWrapper:Landroid/view/View;

    invoke-virtual {v9, v8}, Landroid/view/View;->setVisibility(I)V

    .line 377
    :cond_8
    :goto_6
    iget-object v9, p0, Lru/cn/tv/player/controller/PlayerController;->buttonPrev:Landroid/widget/ImageButton;

    if-eqz v9, :cond_b

    .line 378
    iget-object v10, p0, Lru/cn/tv/player/controller/PlayerController;->buttonPrev:Landroid/widget/ImageButton;

    if-nez v3, :cond_9

    if-eqz v2, :cond_18

    :cond_9
    const/4 v9, 0x1

    :goto_7
    invoke-virtual {v10, v9}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 379
    iget-object v10, p0, Lru/cn/tv/player/controller/PlayerController;->buttonPrev:Landroid/widget/ImageButton;

    if-nez v3, :cond_a

    if-eqz v2, :cond_19

    :cond_a
    const/4 v9, 0x1

    :goto_8
    invoke-virtual {v10, v9}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 382
    :cond_b
    iget-object v9, p0, Lru/cn/tv/player/controller/PlayerController;->buttonNext:Landroid/widget/ImageButton;

    if-eqz v9, :cond_e

    .line 383
    invoke-virtual {p0}, Lru/cn/tv/player/controller/PlayerController;->canGoLive()Z

    move-result v0

    .line 384
    .local v0, "canGoLive":Z
    iget-object v10, p0, Lru/cn/tv/player/controller/PlayerController;->buttonNext:Landroid/widget/ImageButton;

    if-nez v1, :cond_c

    if-eqz v7, :cond_1a

    if-eqz v0, :cond_1a

    :cond_c
    const/4 v9, 0x1

    :goto_9
    invoke-virtual {v10, v9}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 385
    iget-object v10, p0, Lru/cn/tv/player/controller/PlayerController;->buttonNext:Landroid/widget/ImageButton;

    if-nez v1, :cond_d

    if-eqz v7, :cond_1b

    if-eqz v0, :cond_1b

    :cond_d
    const/4 v9, 0x1

    :goto_a
    invoke-virtual {v10, v9}, Landroid/widget/ImageButton;->setFocusable(Z)V

    .line 388
    .end local v0    # "canGoLive":Z
    :cond_e
    invoke-direct {p0}, Lru/cn/tv/player/controller/PlayerController;->updatePausePlay()V

    .line 389
    iget-object v9, p0, Lru/cn/tv/player/controller/PlayerController;->mHandler:Landroid/os/Handler;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v9

    if-eqz v9, :cond_f

    .line 390
    iget-object v9, p0, Lru/cn/tv/player/controller/PlayerController;->mHandler:Landroid/os/Handler;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Landroid/os/Handler;->removeMessages(I)V

    .line 392
    :cond_f
    iget-object v9, p0, Lru/cn/tv/player/controller/PlayerController;->mHandler:Landroid/os/Handler;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 393
    return-void

    .line 330
    .end local v1    # "hasNextItem":Z
    .end local v2    # "hasPreviousItem":Z
    .end local v7    # "timeshifted":Z
    .end local v8    # "visibility":I
    :cond_10
    const/4 v7, 0x0

    goto/16 :goto_0

    .line 339
    .restart local v1    # "hasNextItem":Z
    .restart local v2    # "hasPreviousItem":Z
    .restart local v5    # "timeToEndLive":I
    .restart local v7    # "timeshifted":Z
    :cond_11
    const-wide/32 v10, 0xea60

    goto/16 :goto_1

    .line 343
    .end local v5    # "timeToEndLive":I
    :cond_12
    const/16 v9, 0x8

    goto/16 :goto_2

    .line 347
    :cond_13
    const/16 v9, 0x8

    goto/16 :goto_3

    .line 351
    :cond_14
    const/4 v4, 0x0

    goto/16 :goto_4

    .line 361
    :cond_15
    const/16 v8, 0x8

    goto :goto_5

    .line 368
    .restart local v8    # "visibility":I
    :cond_16
    iget-object v9, p0, Lru/cn/tv/player/controller/PlayerController;->mEndTime:Landroid/widget/TextView;

    if-eqz v9, :cond_17

    .line 369
    iget-object v9, p0, Lru/cn/tv/player/controller/PlayerController;->mEndTime:Landroid/widget/TextView;

    invoke-virtual {v9, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 372
    :cond_17
    iget-object v9, p0, Lru/cn/tv/player/controller/PlayerController;->mCurrentTime:Landroid/widget/TextView;

    if-eqz v9, :cond_8

    .line 373
    iget-object v9, p0, Lru/cn/tv/player/controller/PlayerController;->mCurrentTime:Landroid/widget/TextView;

    invoke-virtual {v9, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_6

    .line 378
    :cond_18
    const/4 v9, 0x0

    goto :goto_7

    .line 379
    :cond_19
    const/4 v9, 0x0

    goto :goto_8

    .line 384
    .restart local v0    # "canGoLive":Z
    :cond_1a
    const/4 v9, 0x0

    goto :goto_9

    .line 385
    :cond_1b
    const/4 v9, 0x0

    goto :goto_a
.end method
