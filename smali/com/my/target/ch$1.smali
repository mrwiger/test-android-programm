.class final Lcom/my/target/ch$1;
.super Ljava/lang/Object;
.source "ImageLoader.java"

# interfaces
.implements Lcom/my/target/ch$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/my/target/ch;->a(Lcom/my/target/common/models/ImageData;Landroid/widget/ImageView;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic kg:Ljava/lang/ref/WeakReference;

.field final synthetic kh:Lcom/my/target/common/models/ImageData;


# direct methods
.method constructor <init>(Ljava/lang/ref/WeakReference;Lcom/my/target/common/models/ImageData;)V
    .locals 0

    .prologue
    .line 68
    iput-object p1, p0, Lcom/my/target/ch$1;->kg:Ljava/lang/ref/WeakReference;

    iput-object p2, p0, Lcom/my/target/ch$1;->kh:Lcom/my/target/common/models/ImageData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bA()V
    .locals 3

    .prologue
    .line 72
    iget-object v0, p0, Lcom/my/target/ch$1;->kg:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 73
    if-eqz v0, :cond_0

    .line 75
    invoke-static {}, Lcom/my/target/ch;->bz()Ljava/util/WeakHashMap;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/my/target/common/models/ImageData;

    .line 76
    iget-object v2, p0, Lcom/my/target/ch$1;->kh:Lcom/my/target/common/models/ImageData;

    if-ne v2, v1, :cond_0

    .line 78
    invoke-static {}, Lcom/my/target/ch;->bz()Ljava/util/WeakHashMap;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    iget-object v1, p0, Lcom/my/target/ch$1;->kh:Lcom/my/target/common/models/ImageData;

    invoke-virtual {v1}, Lcom/my/target/common/models/ImageData;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 80
    if-eqz v1, :cond_0

    .line 82
    invoke-static {v1, v0}, Lcom/my/target/ch;->b(Landroid/graphics/Bitmap;Landroid/widget/ImageView;)V

    .line 86
    :cond_0
    return-void
.end method
