.class public final Lcom/my/target/fd;
.super Lcom/my/target/c;
.source "NativeAppwallAdFactory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/my/target/fd$a;,
        Lcom/my/target/fd$b;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/my/target/c",
        "<",
        "Lcom/my/target/fg;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>(Lcom/my/target/b;)V
    .locals 2

    .prologue
    .line 28
    new-instance v0, Lcom/my/target/fd$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/my/target/fd$a;-><init>(B)V

    invoke-direct {p0, v0, p1}, Lcom/my/target/c;-><init>(Lcom/my/target/c$a;Lcom/my/target/b;)V

    .line 29
    return-void
.end method

.method public static newFactory(Lcom/my/target/b;)Lcom/my/target/c;
    .locals 1
    .param p0, "adConfig"    # Lcom/my/target/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/my/target/b;",
            ")",
            "Lcom/my/target/c",
            "<",
            "Lcom/my/target/fg;",
            ">;"
        }
    .end annotation

    .prologue
    .line 23
    new-instance v0, Lcom/my/target/fd;

    invoke-direct {v0, p0}, Lcom/my/target/fd;-><init>(Lcom/my/target/b;)V

    return-object v0
.end method


# virtual methods
.method protected final a(Lcom/my/target/ae;Lcom/my/target/at;Landroid/content/Context;)Ljava/lang/String;
    .locals 4
    .param p1, "adService"    # Lcom/my/target/ae;
    .param p2, "request"    # Lcom/my/target/at;
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/my/target/fd;->adConfig:Lcom/my/target/b;

    invoke-virtual {v0}, Lcom/my/target/b;->getCachePeriod()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    .line 38
    const-string v0, "NativeAppwallAdFactory: check cached data"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 39
    const/4 v0, 0x0

    .line 40
    invoke-static {p3}, Lcom/my/target/cf;->t(Landroid/content/Context;)Lcom/my/target/cf;

    move-result-object v1

    .line 41
    if-eqz v1, :cond_0

    .line 43
    iget-object v0, p0, Lcom/my/target/fd;->adConfig:Lcom/my/target/b;

    invoke-virtual {v0}, Lcom/my/target/b;->getSlotId()I

    move-result v0

    iget-object v2, p0, Lcom/my/target/fd;->adConfig:Lcom/my/target/b;

    invoke-virtual {v2}, Lcom/my/target/b;->getCachePeriod()J

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3}, Lcom/my/target/cf;->a(IJ)Ljava/lang/String;

    move-result-object v0

    .line 45
    :cond_0
    if-eqz v0, :cond_1

    .line 47
    const-string v1, "NativeAppwallAdFactory: cached data loaded successfully"

    invoke-static {v1}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 48
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Lcom/my/target/ae;->d(Z)V

    .line 53
    :goto_0
    return-object v0

    .line 51
    :cond_1
    const-string v0, "NativeAppwallAdFactory: no cached data"

    invoke-static {v0}, Lcom/my/target/g;->a(Ljava/lang/String;)V

    .line 53
    :cond_2
    invoke-super {p0, p1, p2, p3}, Lcom/my/target/c;->a(Lcom/my/target/ae;Lcom/my/target/at;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
